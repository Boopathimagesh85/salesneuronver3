$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		accountcatalogaddgrid();
		firstfieldfocus();
		accountcatalogcrudactionenable();
	}
	ACDELETE = 0;
	retrievestatus = 0;
	$('#accountingcategorydataupdatesubbtn,#groupcloseaddform').hide();
	$('#accountingcategorysavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			accountcatalogaddgrid();
		});
	}
	{//validation for Add  
		$('#accountingcategorysavebutton').click(function(e) {
			if($("#gstdetails").val() == 'Yes') {
				if($("#taxmasterid").val() != ''  && $("#taxmasterid").val() !=  null) {
					$('#accountcatalogname').mouseout();
					if(e.which == 1 || e.which === undefined) {
						$("#accountingcategoryformaddwizard").validationEngine('validate');
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					}
				} else {
					alertpopup('Please select the GST Tax');
				}
			} else {
				$('#accountcatalogname').mouseout();
				if(e.which == 1 || e.which === undefined) {
					$("#accountingcategoryformaddwizard").validationEngine('validate');
					$(".mmvalidationnewclass .formError").addClass("errorwidth");
				}
			}
		});
		jQuery("#accountingcategoryformaddwizard").validationEngine( {
			onSuccess: function() {
				accountcatalogeaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#accountingcategorydataupdatesubbtn').click(function(e) {
			if($("#gstdetails").val() == 'Yes') {
				if($("#taxmasterid").val() != '' && $("#taxmasterid").val() !=  null) {
					$('#accountcatalogname').mouseout();
					if(e.which == 1 || e.which === undefined) {
						$("#accountingcategoryformeditwizard").validationEngine('validate');
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					}
				} else {
					alertpopup('Please select the GST Tax');
				}
			} else {
				$('#accountcatalogname').mouseout();
				if(e.which == 1 || e.which === undefined) {
					$("#accountingcategoryformeditwizard").validationEngine('validate');
					$(".mmvalidationnewclass .formError").addClass("errorwidth");
				}
			}
		});
		jQuery("#accountingcategoryformeditwizard").validationEngine({
			onSuccess: function() {
				accountcatalogupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$("#accountcatalogreloadicon").click(function() {
			clearform('cleardataform');
			$('#accountcatalogaddform').find('input, textarea, button, select').attr('disabled',true);
			$('#accountcatalogaddform').find("select").select2('disable');
			$('#accountingcategorydataupdatesubbtn').hide();
			$('#accountingcategorysavebutton').show();
			$("#deleteicon").show();
		});
		$( window ).resize(function() {
			maingridresizeheightset('accountcatalogaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	
	{//filter work
		//for toggle
		$('#accountcatalogviewtoggle').click(function() {
			if ($(".accountcatalogfilterslide").is(":visible")) {
				$('div.accountcatalogfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.accountcatalogfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#accountcatalogclosefiltertoggle').click(function(){
			$('div.accountcatalogfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#accountingcategoryfilterddcondvaluedivhid").hide();
		$("#accountingcategoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#accountingcategoryfiltercondvalue").focusout(function(){
				var value = $("#accountingcategoryfiltercondvalue").val();
				$("#accountingcategoryfinalfiltercondvalue").val(value);
				$("#accountingcategoryfilterfinalviewconid").val(value);
			});
			$("#accountingcategoryfilterddcondvalue").change(function(){
				var value = $("#accountingcategoryfilterddcondvalue").val();
				var fvalue = $("#accountingcategoryfilterddcondvalue option:selected").attr('data-ddid');
				accountingcategorymainfiltervalue=[];
				if( $('#s2id_accountingcategoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					accountingcategorymainfiltervalue=[];
					$('#accountingcategoryfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    accountingcategorymainfiltervalue.push($cvalue);
						$("#accountingcategoryfinalfiltercondvalue").val(accountcatalogmainfiltervalue);
					});
					$("#accountingcategoryfilterfinalviewconid").val(value);
				} else {
					$("#accountingcategoryfinalfiltercondvalue").val(fvalue);
					$("#accountingcategoryfilterfinalviewconid").val(value);
				}
			});
		}
		accountingcategoryfiltername = [];
		$("#accountingcategoryfilteraddcondsubbtn").click(function() {
			$("#accountingcategoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#accountingcategoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(accountcatalogaddgrid,'accountingcategory');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
			$("#parentaccountcatalogid").val("");
			$("#employeeiddivhid").attr('class','');
			$("#employeeiddivhid").attr('class','static-field large-12 medium-12 small-12 columns hidedisplay');
			$("#employeeid").attr('class','chzn-select');
			$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
			$("#hsnsaccode,#hsnsacdescription,#natureoftransactionid").val('');
			$("#taxmasterid").select2('val','');
			if($("#gstdetails").val() == 'Yes') {
				$("#gstdetailscboxid").trigger('click');
			}
		});
	}
	{//storage hierarchy
		$('#accountingcategorylistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentaccountcatalog').val(name);
			$('#parentaccountcatalogid').val(listid);
			if(level >= maxlevel){
				alertpopup('Maximum storage level reached');
				return false;
			}
			var editid = $('#accountcatalogprimarydataid').val();
			if(listid != editid) {
			} else {
				//alertpopup('You cant choose the same category as parentcategory');
				$('#groupledgerid,#groupledgertypeid').select2('val',"");
				$('#groupledgerid,#groupledgertypeid').prop("disabled", false);
				return false;
			}
			if(listid != ''){
				setgroupledgertype(listid);
			}
		});
	}
	{//gst tax work -- gowtham
		if($("#gstapplicableid").val() != 1) { // if gst not enabled
			$("#gstdetailsdivhid,#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
		} else { // if gst enabled
			$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
			$('#gstdetailscboxid').click(function() {
				var name = $(this).data('hidname');
				if ($(this).is(':checked')) {
					$('#'+name+'').val('Yes');
					$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").show();
				} else {
					$('#'+name+'').val('No');
					$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
					$("#hsnsaccode,#hsnsacdescription,#natureoftransactionid").val('');
					$("#taxmasterid").select2('val','');
				}
			});
		}	
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function accountcatalogaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $(window).width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#accountingcategorysortcolumn").val();
	var sortord = $("#accountingcategorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.accountingcategoryheadercolsort').hasClass('datasort') ? $('.accountingcategoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.accountingcategoryheadercolsort').hasClass('datasort') ? $('.accountingcategoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.accountingcategoryheadercolsort').hasClass('datasort') ? $('.accountingcategoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = '106';
	if(userviewid != '') {
		userviewid= '188';
	}
	var filterid = $("#accountingcategoryfilterid").val();
	var conditionname = $("#accountingcategoryconditionname").val();
	var filtervalue = $("#accountingcategoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=accountcatalog&primaryid=accountcatalogid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#accountingcategoryaddgrid').empty();
			$('#accountingcategoryaddgrid').append(data.content);
			$('#accountingcategoryaddgridfooter').empty();
			$('#accountingcategoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('accountingcategoryaddgrid');
			{//sorting
				$('.accountingcategoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.accountingcategoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.accountingcategoryheadercolsort').hasClass('datasort') ? $('.accountingcategoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.accountingcategoryheadercolsort').hasClass('datasort') ? $('.accountingcategoryheadercolsort.datasort').data('sortorder') : '';
					$("#accountingcategorysortorder").val(sortord);
					$("#accountingcategorysortcolumn").val(sortcol);
					var sortpos = $('#accountingcategoryaddgrid .gridcontent').scrollLeft();
					accountcatalogaddgrid(page,rowcount);
					$('#accountingcategoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('accountingcategoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					accountcatalogaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#accountingcategorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					accountcatalogaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#accountcatalogaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#accountingcategorypgrowcount').material_select();
		},
	});
}
function accountcatalogcrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#accountingcategorydataupdatesubbtn').hide();
			$('#accountingcategorysavebutton').show();
			firstfieldfocus();
			resetFields();
			$("#gstdetailscboxid").prop('checked',false);
			$('#gstdetails').val('No');
			$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
			$("#hsnsaccode,#hsnsacdescription,#natureoftransactionid,#accountingcategoryprimarydataid").val('');
			$("#taxmasterid").select2('val','');
			retrievestatus = 0;
			Materialize.updateTextFields();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#accountingcategoryaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#accountingcategorydataupdatesubbtn').show();
			$('#accountingcategorysavebutton').hide();
			retrievestatus = 1;
			accountcatalogeditformdatainformationshow(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#accountingcategoryaddgrid div.gridcontent div.active').attr('id');
		var parentstonecategorynameid = getgridcolvalue('accountingcategoryaddgrid',datarowid,'parentaccountcatalogid','');
		var parentcatval=$(parentstonecategorynameid).text().length;
		if(datarowid){		
			$("#accountingcategoryprimarydataid").val(datarowid);
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+2+"&datarowid="+datarowid+"&table=accountcatalog&fieldid=parentaccountcatalogid&table1=account&fieldid1=accountcatalogid&table2=''&fieldid2=''",
					success: function(msg) { 
						if(msg > 0){
							alertpopup("This account category already mapped into either account or accountcategory.So unable to delete this one");						
						}
							else{
						if(parentcatval>1){
				combainedmoduledeletealert('accountcatalogdeleteyes','Are you sure,you want to delete this stonecategory ?');
				}
				else{
				combainedmoduledeletealert('accountcatalogdeleteyes','Are you sure,you want to delete this stonecategory ?');		
				}
							if(ACDELETE == 0) {
								$("#accountcatalogdeleteyes").click(function(){
									var datarowid = $("#accountingcategoryprimarydataid").val();
									accountcatalogcorddelete(datarowid);
								});
							}
							ACDELETE =1;
					   }
					},
				});
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function accountcatalogrefreshgrid() {
		var page = $('ul#accountcatalogpgnum li.active').data('pagenum');
		var rowcount = $('ul#accountcatalogpgnumcnt li .page-text .active').data('rowcount');
		accountcatalogaddgrid(page,rowcount);
	}
}
//new data add submit function
function accountcatalogeaddformdata() {
	var formdata = $("#accountingcategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Accountingcategory/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				accountcatalogrefreshgrid();
				setTimeout(function(){parenttreereloadfun(0);},500);
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level of account reached !!! cannot create category further.');
			}
        },
    });
}
//old information show in form
function accountcatalogeditformdatainformationshow(datarowid) {
	var accountcatalogelementsname = $('#accountingcategoryelementsname').val();
	var accountcatalogelementstable = $('#accountingcategoryelementstable').val();
	var accountcatalogelementscolmn = $('#accountingcategoryelementscolmn').val();
	var accountcatalogelementpartable = $('#accountingcategoryelementspartabname').val();
	$.ajax(	{
		url:base_url+"Accountingcategory/fetchformdataeditdetails?accountcatalogprimarydataid="+datarowid+"&accountcatalogelementsname="+accountcatalogelementsname+"&accountcatalogelementstable="+accountcatalogelementstable+"&accountcatalogelementscolmn="+accountcatalogelementscolmn+"&accountcatalogelementpartable="+accountcatalogelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				accountcatalogrefreshgrid();
			} else if((data.fail) == 'sndefault') {
				alertpopup('Cannot Edit/Delete default records');
				resetFields();
				accountcatalogrefreshgrid();
				$("#groupsectionoverlay").addClass("closed");
			} else {
				var txtboxname = accountcatalogelementsname + ',accountingcategoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = accountcatalogelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				if(data['gstdetails'] == 'Yes'){
					$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").show();
				} else {
					$("#hsnsaccodedivhid,#hsnsacdescriptiondivhid,#taxmasteriddivhid").hide();
				}
				Materialize.updateTextFields();
			}
		}
	});
	parenttreereloadfun(datarowid);
	$('#accountingcategorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#accountingcategorysavebutton').hide();
}
//udate old information
function accountcatalogupdateformdata() {
	var parentaccountcatalogid=$("#parentaccountcatalogid").val();
	var editaccountcatalog=$("#accountingcategoryprimarydataid").val();
		if(parentaccountcatalogid!=editaccountcatalog){
			var formdata = $("#accountingcategoryaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
		    $.ajax({
		        url: base_url + "Accountingcategory/datainformationupdate",
		        data: "datas=" + datainformation,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
						$('#accountingcategorydataupdatesubbtn').hide();
						$('#accountcatalogsavebutton').show();
						accountcatalogrefreshgrid();
						retrievestatus = 0;
						setTimeout(function(){parenttreereloadfun(0);},500);
						$("#groupsectionoverlay").removeClass("effectbox");
						$("#groupsectionoverlay").addClass("closed");
						resetFields();
						alertpopup(savealert);			
		            }else if(nmsg == 'FAILURE'){
						alertpopup("Check your Parent name mapping. ");
					}else if(nmsg == 'LEVEL') {
						alertpopup('Maximum level reached !!! cannot create category further.');
					}
		        },
		    });
	}
	 else{
		alertpopup("You cant map the same name as parent name");
	}
}
//udate old information
function accountcatalogcorddelete(datarowid) {
	var accountcatalogelementstable = $('#accountingcategoryelementstable').val();
	var accountcatalogelementspartable = $('#accountingcategoryelementspartabname').val();
    $.ajax({
        url: base_url + "Accountingcategory/deleteinformationdata?accountcatalogprimarydataid="+datarowid+"&accountcatalogelementstable="+accountcatalogelementstable+"&accountcatalogparenttable="+accountcatalogelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	accountcatalogrefreshgrid();
            	$("#parentaccountcatalogid").val("");
				$("#basedeleteoverlayforcommodule").fadeOut();
				retrievestatus = 0;
				setTimeout(function(){parenttreereloadfun(0);},500);
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	accountcatalogrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            } else if(nmsg == "sndefault") {
				accountcatalogrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Cannot Edit/Delete default records');
			}
        },
    });
}
{// Tree reload
	function parenttreereloadfun(datarowid)	{
		var tablename = "accountcatalog";
		var mandfield = $('#treevalidationcheck').val();
		var fieldlab = $('#treefiledlabelcheck').val();
		var parentaccountcatalogid = $('#parentaccountcatalogid').val();
		$.ajax({
			url: base_url + "Accountingcategory/treedatafetchfun",
			data: "tabname="+tablename+'&mandval='+mandfield+'&fieldlabl='+fieldlab+'&rowid='+datarowid+'&primaryid='+parentaccountcatalogid,
			type: "POST",
			cache:false,
			success: function(data) {
				$('.'+tablename+'treediv').empty();
				$('.'+tablename+'treediv').append(data);
				$('#accountingcategorylistuldata li').click(function() {
					var level = parseInt($(this).attr('data-level'));
					var maxlevel = parseInt($('#maxcategorylevel').val());
					var name=$(this).attr('data-listname');
					var listid=$(this).attr('data-listid');
					$('#parentaccountcatalog').val(name);
					$('#parentaccountcatalogid').val(listid);
					if(level >= maxlevel){
						alertpopup('Maximum level reached');
						return false;
					}
					var editid = $('#accountcatalogprimarydataid').val();
					if(listid != editid) {
					} else {
						//alertpopup('You cant choose the same category as parentcategory');
						$('#groupledgerid,#groupledgertypeid').select2('val',"");
						$('#groupledgerid,#groupledgertypeid').prop("disabled", false);
						return false;
					}
					if(listid != ''){
				       setgroupledgertype(listid);
			        }
				});
				$(function() {
					$('#dl-menu').dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
					});
				});
				if(retrievestatus == 1) {			
					gettheparentcatalog(datarowid);
				}	
			},
		});
	}
}
//load the parent id
function gettheparentcatalog(datarowid){
	$.ajax({
		url:base_url+"Accountingcategory/getcatalogparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			if(data.parentcatalogid) {
				$('#parentaccountcatalogid').val(data.parentcatalogid);
				$('#parentaccountcatalog').val(data.catalogname);
			}
		}
	});
}
// set goup ledger & type based on parent category
function setgroupledgertype(listid){
	$.ajax({
		url:base_url+"Accountingcategory/setgroupledgertype?id="+listid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			if(data.groupledgerid == 1) {
				$('#groupledgerid').select2('val',"");
				$('#groupledgerid').prop("disabled", false);
			} else {
				$('#groupledgerid').select2('val',data.groupledgerid);
				$('#groupledgerid').prop("disabled", true);
			}
			if(data.groupledgertypeid == 1) {
				$('#groupledgertypeid').select2('val',"");
				$('#groupledgertypeid').prop("disabled", false);
			} else {
				$('#groupledgertypeid').select2('val',data.groupledgertypeid);
				$('#groupledgertypeid').prop("disabled", true);
			}
		}
	});
}
//Kumaresan - Accounting Category unique name
function accountcatalognameuniquecheck() {
	var primaryid = $("#accountingcategoryprimarydataid").val();
	var accname = $("#accountcatalogname").val();
	var elementpartable = $('#accountingcategoryelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Acc. Category name already exists!";
				}
			}else {
				return "Acc. Category name already exists!";
			}
		} 
	}
}
//Kumaresan - short Name unique check
function accountcatalogshortnamecheck() {
	var primaryid = $("#accountingcategoryprimarydataid").val();
	var accname = $("#catalogshortname").val();
	var fieldname = 'catalogshortname';
	var elementpartable = $('#accountingcategoryelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			} else {
				return "Short name already exists!";
			}
		}
	}
}