$(document).ready(function() {
	$(document).foundation();
    // Main div height width change
    maindivwidth();
	documentsdelid =[];
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);
	METHOD = 'ADD';
    {
    	//Grid Calling Functions
    	solutionsgrid();
    	//crud action
		crudactionenable();
		//default value set
		$("#solutionsfolingrideditspan1").show();
		{//grid width fix
			$("#tab1").click(function() {
				if(softwareindustryid !=4) {
					solutionsfoladdgrid1();
				}
			});
		}
	}
    {//inner-form-with-grid
		$("#solutionsfolingridadd1").click(function(){
			sectionpanelheight('solutionsfoloverlay');
			$("#solutionsfoloverlay").removeClass("closed");
			$("#solutionsfoloverlay").addClass("effectbox");
			$('#setdefaultcboxid,#publiccboxid').prop('checked', false);
			$("#setdefault,#public").val('No');
			$('#foldernamename,#description').val('');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#solutionsfolcancelbutton").click(function(){
			$('#foldernamename,#description').val('');
			$("#solutionsfoloverlay").removeClass("effectbox");
			$("#solutionsfoloverlay").addClass("closed");
		});
		$("#solutionsattingridadd2").click(function(){
			//Sectionpanel height set
			sectionpanelheight('solutionsattoverlay');
			$("#solutionsattoverlay").removeClass("closed");
			$("#solutionsattoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#solutionsattingriddel2").click(function() {
			var datarowid = $('#solutionsattaddgrid2 div.gridcontent div.active').attr('id');
			if(datarowid) {
				if(METHOD == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('solutionsattaddgrid2',datarowid);
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#solutionsattingridadd1").click(function(){
			//Sectionpanel height set
			sectionpanelheight('solutionsattoverlay');
			$("#solutionsattoverlay").removeClass("closed");
			$("#solutionsattoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#solutionsattcancelbutton").click(function(){
			$("#solutionsattaddbutton").show();
			$("#solutionsattupdatebutton").hide();
			$("#solutionsattoverlay").removeClass("effectbox");
			$("#solutionsattoverlay").addClass("closed");
		});
	}
	$("#crmfileinfoid").val('0');
	//close
    var addcloseinfo = ["closeaddform", "solutionsgriddisplay", "solutionsaddformdiv"]
    addclose(addcloseinfo);
	var addcloseviewcreation =["viewcloseformiconid","solutionsgriddisplay","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	//button show hide
	$("#solutionsattupdatebutton").hide();
	{// View creation Function
		//branch view by drop down change
		$('#dynamicdddataview').change(function(){
			solutionsgrid();
		});
  	}
	//folder name grid reload
	$('#tab2').click(function(){
		folderdatareload();
		solutionsgrid();
	});
	$('#formclearicon').click(function(){
		froalaset(froalaarray);
		$("#keywords").select2('val',' ');
		cleargriddata('solutionsattaddgrid1');
		$('#filenameattachdisplay').empty();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
	});
    $(".chzn-selectcomp").select2().select2('val', 'Mobile');
	{//validation for Campaign Add  
		$('#dataaddsbtn').click(function(e) {
			$('#tab2').trigger('click');
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			for(var j=0;j<datalength;j++) {
				gridformtogridvalidatefunction(gridnames[j]);
			}
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$("#foldernamename").addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#processoverlay").show();
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','knowledgebasecategoryid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernameid").addClass('chzn-select validate[required]');
			}
		});
	}                                                    
	{
		//Solution Category
		$("#solutioncategoryicon").click(function(){
			window.location =base_url+'Solutionscategory';
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{//toolbar action events
		$("#cloneicon").click(function() {
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
		    	solutionsattaddgrid1();
				solutionclonedatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('addclick','solutionsaddformdiv');
				$("#solutionsattingriddel1").show();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				$("#filenamedivhid").show();
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function(){
			refreshgrid();
			$('#filenameattachdisplay').empty();
		});	
		$( window ).resize(function() {
			maingridresizeheightset('solutionsgrid');
			sectionpanelheight('solutionsfoloverlay');
			sectionpanelheight('solutionsattoverlay');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
		    	solutionsattaddgrid1();
				solutioneditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','solutionsaddformdiv');	
				$("#solutionsattingriddel1").hide();
				$(".froala-element").css('pointer-events','none');
				$(".fr-element").attr("contenteditable", 'false');
				$("#solutionsattaddbutton").addClass("hidedisplay");				
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				$("#filenamedivhid").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					 $("#processoverlay").show();
					froalaset(froalaarray);
			    	solutionsattaddgrid1();
					solutioneditdatafetchfun(rdatarowid);
					Materialize.updateTextFields();
					showhideiconsfun('summryclick','solutionsaddformdiv');	
					$("#solutionsattingriddel1").hide();
					$(".froala-element").css('pointer-events','none');
					$(".fr-element").attr("contenteditable", 'false');
					$("#solutionsattaddbutton").addClass("hidedisplay");				
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					 },50);
					$("#filenamedivhid").hide();
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','solutionsaddformdiv');
			$(".froala-element").css('pointer-events','auto');
			$(".fr-element").attr("contenteditable", 'true');
			$("#solutionsattupdatebutton").removeClass("hidedisplay").removeClass("hidedisplayfwg").show();
			$("#solutionsattingriddel1").show();
			$("#filenamedivhid").show();
		});
		//update Campaign information
		$('#dataupdatesubbtn').click(function(e) {
			$('#tab2').trigger('click');
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				updateformdata();
			},
			onFailure: function() {
				$("#foldernamename").addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#foldernameid").addClass('validate[required]');
				var dropdownid =['1','knowledgebasecategoryid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernameid").addClass('validate[required]');
			}	
		});
	}
	{//Folder option
		$("#solutionsfoladdbutton").click(function() {
			$("#foldernamename").focusout();
			$('#solutionsfoladdgrid1validation').validationEngine('validate');
			Materialize.updateTextFields();
		});
		$('#solutionsfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				solutionsfolderinsertfun();
				Materialize.updateTextFields();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Solutions folder data fetch
		$("#solutionsfolingridedit1").click(function() {
			var datarowid = $('#solutionsfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				resetFields();
				//setTimeout(function(){
				sectionpanelheight('solutionsfoloverlay');
				$("#solutionsfoloverlay").removeClass("closed");
				$("#solutionsfoloverlay").addClass("effectbox");
				//},10);
				$("#solutionsfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#solutionsfoladdbutton").hide();
				solutionsfolderdatafetch(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
			} else {
				alertpopup('Please select a row');
			}
		});
		//Solutions update
		$("#solutionsfolupdatebutton").click(function() {
			$("#foldernamename").focusout();
			$('#solutionsfoleditgrid1validation').validationEngine('validate');
			setTimeout(function(){
				Materialize.updateTextFields();
				firstfieldfocus();
			},100);
		});
		$('#solutionsfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				solutionsfolderupdate();
				$("#solutionsfolingridedit1,#solutionsfolingriddel1").show();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Solutions folder delete
		$("#solutionsfolingriddel1").click(function() {
			var datarowid = $('#solutionsfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
		//folder reload
		$("#solutionsfolingridreload1").click(function() {
			//resetFields();
			$("#foldernamename").val('');
			$("#description").val('');
			$("#setdefaultcboxid").attr('checked',false);
			$("#publiccboxid").attr('checked',false);
			$("#setdefault").val('No');
			$("#public").val('No');
			folderrefreshgrid();
			$("#solutionsfolupdatebutton").hide().addClass('hidedisplayfwg');
			$("#solutionsfoladdbutton,#solutionsfolingriddel1").show();
			Materialize.updateTextFields();
		});
	}
	$("#solutionsattingriddel1").click(function(){
		var datarowid = $('#solutionsattaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {		
			if(METHOD == 'UPDATE') {
				alertpopup("A Record Under Edit Form");
			} else {
				/*delete grid data*/
				if(datarowid != 1) {
					var did = $('#conrowcolids').val();
					if(did != '') {
						$('#conrowcolids').val(datarowid);
					} else {
						$('#conrowcolids').val(did+','+datarowid);
					}
				}	
				deletegriddatarow('solutionsattaddgrid1',datarowid);
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{//dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		
		$('#solutionsattaddbutton,#solutionsattupdatebutton').click(function(){
			if($("#filenameattachdisplay").is(':empty')){
				alertpopup('Please Upload File...');
			} else {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				METHOD = 'ADD';
			}
		});
	}
	{// Redirected form Home
		add_fromredirect("solutionsaddsrc",28);
	}
	//Solutions-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique229"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				solutioneditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','solutionsaddformdiv');
				sessionStorage.removeItem("reportunique229");
			},50);	
		}
	}
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,solutionsgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			solutionsgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewfieldids").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// Solution View Grid
function solutionsgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#solutionspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#solutionspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $('#solutionsgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.solutionsheadercolsort').hasClass('datasort') ? $('.solutionsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.solutionsheadercolsort').hasClass('datasort') ? $('.solutionsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.solutionsheadercolsort').hasClass('datasort') ? $('.solutionsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=knowledgebase&primaryid=knowledgebaseid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#solutionsgrid').empty();
			$('#solutionsgrid').append(data.content);
			$('#solutionsgridfooter').empty();
			$('#solutionsgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('solutionsgrid');
			{//sorting
				$('.solutionsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.solutionsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#solutionspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.solutionsheadercolsort').hasClass('datasort') ? $('.solutionsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.solutionsheadercolsort').hasClass('datasort') ? $('.solutionsheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#solutionsgrid .gridcontent').scrollLeft();
					solutionsgrid(page,rowcount);
					$('#solutionsgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('solutionsheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					solutionsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#solutionspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					solutionsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}			
			{// Redirected form Home
				var solutionideditvalue = sessionStorage.getItem("solutionidforedit"); 
				if(solutionideditvalue != null) {
					setTimeout(function(){
						solutioneditdatafetchfun(solutionideditvalue);
						sessionStorage.removeItem("solutionidforedit");
					},100);
				}
			}
			//Material select
			$('#solutionspgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('solutionsgriddisplay', 'solutionsaddformdiv');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			//form field first focus
			firstfieldfocus();
			froalaset(froalaarray);
			if(softwareindustryid == 4) {
				solutionsattaddgrid1();
				cleargriddata('solutionsattaddgrid1');
				$('#tab1').trigger('click');
			} else {
				folderrefreshgrid();
				folderdatareload();
		    	solutionsattaddgrid1();
		    	cleargriddata('solutionsattaddgrid1');
		    	cleargriddata('folderrefreshgrid');
		    	$('#tab2').trigger('click');
			}
			$("#knowledgebasecategoryid").select2('val',2);
			$("#keywords").select2('val',' ');
			$('#filenameattachdisplay').empty();
			$("#crmfileinfoid").val('0');
			$("#solutionsattingriddel1").show();
			showhideiconsfun('addclick','solutionsaddformdiv');
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			getcurrentsytemdate("knowledgebasedate");
			Materialize.updateTextFields();
			$("#filenamedivhid").show();
			$('.fr-placeholder').css('height',0);
			$('.fr-placeholder').css('height', 150);
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if (datarowid)  {
				$("#processoverlay").show();
				if(softwareindustryid == 4) {
					solutionsattaddgrid1();
					cleargriddata('solutionsattaddgrid1');
					$('#tab1').trigger('click');
				} else {
					folderrefreshgrid();
					folderdatareload();
			    	solutionsattaddgrid1();
			    	cleargriddata('solutionsattaddgrid1');
			    	cleargriddata('folderrefreshgrid');
			    	$('#tab2').trigger('click');
				}
				froalaset(froalaarray);
				solutioneditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				$("#solutionsattingriddel1").show();
				showhideiconsfun('editclick','solutionsaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				$("#filenamedivhid").show();
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#solutionsgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#solutionspgnum li.active').data('pagenum');
		var rowcount = $('ul#solutionspgnumcnt li .page-text .active').data('rowcount');
		solutionsgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.folderlistpgnumcnt li .page-text .active').data('rowcount');
		solutionsfoladdgrid1(page,rowcount);
	}
}
//sms master folder  Grid
function solutionsfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#solutionsfoladdgrid1').width();
	var wheight = $('#solutionsfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Solutions/solutionsfoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=229',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#solutionsfoladdgrid1').empty();
			$('#solutionsfoladdgrid1').append(data.content);
			$('#solutionsfoladdgrid1footer').empty();
			$('#solutionsfoladdgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('solutionsfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					solutionsfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smsmasterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					solutionsfoladdgrid1(page,rowcount);
				});
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					solutionsfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#folderlistpgrowcount').material_select();
		},
	});
}
//new data add submit function
function newdataaddfun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(gridnames[j]) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(gridnames[j]) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				}
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	//editor data
	var editorname = $('#editornameinfo').val();
	var viewfieldids = $('#viewfieldids').val();
	var editordata = froalaeditoradd(editorname);
	var neweditordata = froalaeditordataget(editorname);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(editordata !=""){
		$.ajax( {
			url: base_url + "Solutions/newdatacreate",
			data: "datas=" +datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&datass="+neweditordata+"&viewfieldids="+viewfieldids,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#solutionsaddformdiv').hide();
					$('#solutionsgriddisplay').fadeIn(1000);
					refreshgrid();
					clearformgriddata();
					froalaset(froalaarray);
					$("#knowledgebasetags").select2('val',' ');
					alertpopup(savealert);
					$("#foldernameid").addClass('validate[required]');
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	} else { 
		alertpopup('Please Enter any Data in Solution Editor'); 
		$("#processoverlay").hide();
	}
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax( {
		url:base_url+"Solutions/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$(".ftab").trigger('click');
				$('#solutionsaddformdiv').hide();
				$('#solutionsgriddisplay').fadeIn(1000);
				refreshgrid();
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
				//For Left Menu Confirmation
				discardmsg = 0;
				$("#processoverlay").hide();
			} else {
				addslideup('solutionsgriddisplay', 'solutionsaddformdiv');				
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				solutionsproductdetail(datarowid);
				editordatafetch(froalaarray,data);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				$("#processoverlay").hide();
			}
		}
	});
}
function solutionsproductdetail(pid) {
	if(softwareindustryid == '4') {
		$gridname = 'solutionsattaddgrid1';
	} else {
		$gridname = 'solutionsattaddgrid2';
	}
	if(pid!='') {
		$.ajax({
			url:base_url+'Solutions/solutionsproductdetailfetch?primarydataid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				console.log(data.rows);
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata($gridname,data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize($gridname);
					var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
					gridfieldhide($gridname,hideprodgridcol);
				}
			},
		});
	}
}
{// Clear grid data
	function clearformgriddata() {
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++) {
			cleargriddata(gridnames[j]);
		}
	}
}
//update old information
function updateformdata() {
	var amp = '&';
	//editor data
	var editordata = '';
	var neweditordata = '';
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var neweditordata = froalaeditordataget(editorname);
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	var deleteid = $('#conrowcolids').val();
	var viewfieldids = $('#viewfieldids').val();
	if(editordata != ""){
	 $.ajax({
        url: base_url + "Solutions/datainformationupdate",
        data: "datas=" + datainformation+"&griddatas="+sendformadddata+amp+"numofrows="+noofrows+"&deleteid="+deleteid+"&viewfieldids="+viewfieldids,
		type: "POST",
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE')
            {
				resetFields();
				$('#solutionsaddformdiv').hide();
				$('#solutionsgriddisplay').fadeIn(1000);
				refreshgrid();
				froalaset(froalaarray);
				alertpopup(savealert);
				$("#foldernameid").addClass('validate[required]');
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
				$("#processoverlay").hide();
			}
        },
    });
	} else { 
		alertpopup('Please Enter the Records in Solution Editor');
		$("#processoverlay").hide();
	}
}
//delete operation
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Solutions/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
// Main view Grid
function solutionsattaddgrid1() {
	if(softwareindustryid == 4) {
		$tabgroupid = 196;
		$moduleid = 79;
		$gridname = 'solutionsattaddgrid1';
	} else{
		$tabgroupid = 73;
		$moduleid = 229;
		$gridname = 'solutionsattaddgrid2';
	}
	var wwidth = $("#"+$gridname+"").width();
	var wheight = $("#"+$gridname+"").height();
	$.ajax({
		url:base_url+"Solutions/localgirdheaderinformationfetch?tabgroupid="+$tabgroupid+"&moduleid="+$moduleid+"&width="+wwidth+"&height="+wheight+"&modulename=solutionattach",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#"+$gridname+"").empty();
			$("#"+$gridname+"").append(data.content);
			$("#"+$gridname+"footer").empty();
			$("#"+$gridname+"footer").append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize($gridname);
			var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
			gridfieldhide($gridname,hideprodgridcol);
		},
	});
}
{/* clear form */
	function clearformsol(cleardivid) {
		setTimeout(function() {
			$('#'+cleardivid+'').validationEngine('hideAll');
			$('#'+cleardivid+' .select2-choice').removeClass('error');
			$('#'+cleardivid+' :input').removeClass('error');
			$(':input','#'+cleardivid)
			.not(':button, :submit, :reset, :hidden')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');
			$('#'+cleardivid+' input[type=hidden]').val('');
			$('#'+cleardivid+' textarea').val('');
			$('#'+cleardivid+' .chzn-select').select2('val','');
			$('#'+cleardivid+' .captionddse').select2('val','');
			$('#'+cleardivid+' .commenttxtarea').val('');
			$('#'+cleardivid+' .overlay .cctxt').val('');
		},50);
		$("#crmfileinfoid").val('0');	
		filename = [];
		filesize = [];
		filetype = [];
		filepath = [];		
	}	
}
function gridformtogridvalidatefunction(gridnamevalue) {
	jQuery("#"+gridnamevalue+"validation").validationEngine({
			validateNonVisibleFields:false,
			onSuccess: function() {  
				var i = griddataid;
				var gridname = gridynamicname;
				var coldatas = $('#gridcolnames'+i+'').val();
				var columndata = coldatas.split(',');
				var coluiatas = $('#gridcoluitype'+i+'').val();
				var columnuidata = coluiatas.split(',');
				var datalength = columndata.length;
				/*Form to Grid*/
				if(METHOD == 'ADD' || METHOD == '') {
					formtogriddata(gridname,METHOD,'last','');
				} else if(METHOD == 'UPDATE') {
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
				}
				/* Hide columns */
				var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
				gridfieldhide('solutionsattaddgrid1',hideprodgridcol);
				/* Data row select event */
				datarowselectevt();
				clearform('gridformclear');
				$("#keywords").select2('val','');
				setTimeout(function(){
					fname_772 = [];
					fsize_772 = [];
					ftype_772 = [];
					fpath_772 = [];
					$("#crmfileinfoid").val('0');
					$('#filenameattachdisplay').empty();
					setTimeout(function(){
						$("#crmfileinfoid").val('0');
					});
					$("#solutionsattcancelbutton").trigger('click');
				});
			},
			onFailure: function(){
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
			}
		});
}
{// Edit Function
	function solutioneditdatafetchfun(datarowid){
		froalaset(froalaarray);
		cleargriddata('solutionsattaddgrid1');
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#solutionsattaddbutton").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		$('#filenameattachdisplay').empty();
		getformdata(datarowid);
		firstfieldfocus();
		$("#crmfileinfoid").val('0');
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function solutionclonedatafetchfun(datarowid){
		froalaset(froalaarray);
		cleargriddata('solutionsattaddgrid1');
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		$('#filenameattachdisplay').empty();
		getformdata(datarowid);
		firstfieldfocus();
		$("#crmfileinfoid").val('0');
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{//folder operation
	function solutionsfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Solutions/solutionsfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					//resetFields();
					folderrefreshgrid();
					cleargriddata('solutionsfoladdgrid1');
					$("#solutionsfoladdbutton,#solutionsfolingriddel1").show();
					$("#solutionsfolupdatebutton").hide();
					$("#solutionsfolcancelbutton").trigger('click');
					$('#dataaddsbtn').attr('disabled',false);
					alertpopup(savealert);
					folderdatareload();
					folderrefreshgrid();
					Materialize.updateTextFields();
				}
			},
		});
	}
	//Edit data Fetch
	function solutionsfolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Solutions/solutionsfolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes'){
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
					var ppublic = data.ppublic;
					if(ppublic == 'Yes'){
						$("#publiccboxid").prop('checked',true);
						$("#public").val('Yes');
					} else {
						$("#publiccboxid").prop('checked',false);
						$("#public").val('No');
					}
				}
			},
		});
	}
	//SMS templates folder add function
	function solutionsfolderupdate(){
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Solutions/solutionsfolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					//resetFields();
					folderrefreshgrid();
					cleargriddata('solutionsfoladdgrid1');
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					folderdatareload();
					$("#solutionsfoladdbutton,#solutionsfolingriddel1").show();
					$("#solutionsfolupdatebutton").hide();
					$("#solutionsfolcancelbutton").trigger('click');
					alertpopup(savealert);
				}
			},
		});
	}
	//delete folder 
	function deletefolderoperation(id){
		$.ajax({
			url:base_url+'Solutions/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#solutionsfolingriddel1').attr('disabled',false);
					alertpopup("Deleted successfully");
				} else if(nmsg == 'DEFAULT') {
					alertpopup("Can't delete default records.");
					folderrefreshgrid();
					$('#solutionsfolingriddel1').attr('disabled',false);
				}
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#foldernamename,#description').empty();
		$('#foldernameid').empty();
		$('#foldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Solutions/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=229',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#foldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes'){
							$('#foldernameid').select2('val',data[index]['datasid']).trigger('change');
						}
					});
				}
			},
		});
	}
}
//Unique Folder Name check - Kumaresan
function foldernamecheck() {
	var primaryid = $("#foldernameeditid").val();
	var foldernamename = $("#foldernamename").val();
	var nmsg = "";
	if( foldernamename != "" ) {
		$.ajax({
			url:base_url+"Solutions/foldernameunique",
			data:"&foldernamename="+foldernamename,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder Name already exists!";
				}
			} else {
				return "Folder Name already exists!";
			}
		} 
	}
}