$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}		
	{// Drop Down Change Function 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{//Date Range
		daterangefunction();
	}
	{//Grid Calling
        activityaddgrid();
		//crud action
		crudactionenable();
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{//inner-form-with-grid
		$(".sectionalertbuttonarea").addClass('hidedisplay');
		$("#activitiesinvingridadd1").click(function(){
			sectionpanelheight('activitiesinvoverlay');
			$("#activitiesinvoverlay").removeClass("closed");
			$("#activitiesinvoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$(".addsectionclose").click(function(){
			$("#activitiesinvoverlay").removeClass("effectbox");
			$("#activitiesinvoverlay").addClass("closed");
		});
	}
	{//hide Recurrence Fields
		$("#activityafteroccurencesdivhid").hide();
		$("#activityrecurrenceuntildivhid").hide();
		$("#activityrecurrencedayiddivhid").hide();
		$("#activityrecurrenceontheiddivhid").hide();
	}
	{
		//$(document).on('data-attribute-changed', function() {
		$("input").on("change keyup paste", function(){
			if($("form#dataaddform").attr( "formtype" ) === 'clone'){
				$("form#dataaddform").attr("changed", 'changed');
			}
		});
	}
	{//restrict text input in time picker
		$('#activitystarttime').timepicker({
			'step':15,
			'timeFormat': 'H:i',
			'scrollDefaultNow':true,
			'disableTextInput':true,//restrict text input in time picker
			'disableTouchKeyboard':true,
		});
		$('#activitystarttime').on('changeTime', function() {
		    $('#activitystarttime').focus();
		});
		$('#activityendtime').timepicker({
			'step':15,
			'timeFormat': 'H:i',
			'scrollDefaultNow':true,
			'disableTextInput':true,//restrict text input in time picker
			'disableTouchKeyboard':true,
		});
	}
	//invite user hide
	$("#activitiesinvaddbutton,#activitiesinvingriddel1").hide();
	{//Close Add Screen
		var addcloseactivitycreation =["closeaddform","activitycreationview","activitycreationaddform"]
		addclose(addcloseactivitycreation);
		var addcloseviewcreation =["viewcloseformiconid","activitycreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function() {
			activityaddgrid();
		});
	}
	{
		//Calendar
		$("#calendaricon").click(function(){
			window.location =base_url+'Calendar';
		});
	}
	{//field id assign
		$("#actvitycommonid").change(function() {
			var moduleid = $("#actvitymoduleid").val();
			var commonid = $("#actvitycommonid").val();
			$("#commonid").val(commonid);
			getmobilenumberemailid(commonid,moduleid);
		});
		$("#activityrecurrencedayid").trigger('change');
	}
	$('#formclearicon').click(function(){
		//invite user grid
		cleargriddata('activitiesinvaddgrid1');
		var elementname = $('#elementsname').val();
		elementdefvalueset(elementname);
		removevalidaterequired();
		endsonddvalidateremove();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		//Reset Date Picker
		daterangefunction();
	});
	{// For touch
		fortabtouch = 0;
	}
	{//Tool bar Icon Change Function View
		$("#cloneicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				activityclonedatafetchfun(datarowid);
				removevalidaterequired();
				endsonddvalidateremove();
				showhideiconsfun('editclick','activitycreationaddform');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				settemplatesid(datarowid);
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				$( "form#dataaddform" ).attr( "formtype", "clone");	
				$("#notifypatientcboxid").trigger('change');
				$("#notifydoctorcboxid").trigger('change');
				$("recurrencedivhid").hide();
				Materialize.updateTextFields();
				Operation = 0; //for pagination
			} else{
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
			resetFields();
			removevalidaterequired();
			endsonddvalidateremove();
			$("#activityreminderintervalid").select2('val', '');
		});
		$( window ).resize(function() {
			maingridresizeheightset('activityaddgrid');
			formheight();
			sectionpanelheight('activitiesinvoverlay');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				invitemoduledropdownload();
				froalaset(froalaarray);
				activityeditdatafetchfun(datarowid);
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				settemplatesid(datarowid);
				showhideiconsfun('summryclick','activitycreationaddform');	
				$(".froala-element").css('pointer-events','none');				
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','activitycreationaddform');
			$(".froala-element").css('pointer-events','auto');
			Materialize.updateTextFields();
		});
	}
	//patient-appointment session check
	{// Redirected form patient
		if(softwareindustryid == 2){
			var uniquepatientsession = sessionStorage.getItem("patientid");
			if(uniquepatientsession != '' && uniquepatientsession != null) {
				setTimeout(function() {
					$('#addicon').trigger('click');
					$("#contactid").select2('val',uniquepatientsession);
					sessionStorage.removeItem("patientid");
				},100);
			}
		}
	}
	{	//validation for activities Add  
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			
			var notifypatient = $("#notifypatient").val();
			var patientmode = $("#remindermodeid").val();
			if((notifypatient == 'Yes') && (patientmode == null)) {
				alertpopup('Please select the notify mode for name');
			} else {
				var notifydoctor = $("#notifydoctor").val();
				var doctormode = $("#reminderdoctormodeid").val();
				if((notifydoctor == 'Yes') && (doctormode == null)) {
					alertpopup('Please select the notify mode for assignto');
				} else {
					var recurrenceid = $("#recurrence").val();
					if(recurrenceid == 'Yes') {
						var recurrenceid = $("#activityrecurrencefreqid").val();
						if(recurrenceid){
							$("#formaddwizard").validationEngine('validate');
							Materialize.updateTextFields();
						} else {
							alertpopup('Please select the recurrence frequency');
						}
					} else{
						$("#formaddwizard").validationEngine('validate');
						Materialize.updateTextFields();
					}
				}
			}
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function()
			{
				var name = $("#crmactivityname").val();
				var date = $("#activitystartdate").val();
				var time = $("#activitystarttime").val();
				$.ajax({
					url:base_url+"Activity/activityvalidation",
					data: "name=" +name+"&date="+date+"&time="+time,
					type: "POST",
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
							activitynewdataaddfun();
						} else {   
							$("#activtyvalidateovrelay").fadeIn();
							$("#valactivityname").val(name);
							$("#valactivitytime").val(time);
						}	
					},
				});
			},
			onFailure: function() {
				var dropdownid =['2','activitytypeid','employeeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//activity validation form
		$("#activtycreatebtn").click(function(){
			$("#activtyvalidateovrelay").fadeOut();
			activitynewdataaddfun();
		});
		$("#activtycancelbtn").click(function(){
			$("#activtyvalidateovrelay").fadeOut();
		});
	}
	{//activity validation form	
		$("#actmobileemailcreate").click(function(){
			activitydataaddfun();
		});
		$("#actmobileemailupdate").click(function(){
			activitydataupdateinformationfun();
		});
		$("#actmobileemailcancel,#actmobemailclose").click(function(){
			$("#activtymobemailvalidateovrelay").fadeOut();
		});
	}
	{//update Activity information
		$('#dataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				var notifypatient = $("#notifypatient").val();
				var patientmode = $("#remindermodeid").val();
				if((notifypatient == 'Yes') && (patientmode == null)) {
					alertpopup('Please select the notify mode for name');
				} else {
					var notifydoctor = $("#notifydoctor").val();
					var doctormode = $("#reminderdoctormodeid").val();
					if((notifydoctor == 'Yes') && (doctormode == null)) {
						alertpopup('Please select the notify mode for assign to');
					} else {
						activityinviteuserchack();
					}
				}
			},
			onFailure: function() {
				var dropdownid =['2','activitytypeid','employeeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//module name based on user name
	$("#actvitymoduleid").change(function() {
		var value = $("#actvitymoduleid").val();
		if(value){
			$.ajax({
				url:base_url+"Activity/dropdownvaluefecth?moduleid="+value,
				dataType:'json',
				async :false,
				cache:false,
				success: function(data) {
					var fieldname = data.filedname;
					var tabname = data.tablename;
					usernamefetch(fieldname,tabname);//data is a table name
				},
			});
		}
	}); 
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	$("#activityrecurrencefreqid").change(function() {
		removevalidaterequired();
		var id = $("#activityrecurrencefreqid").val();
		if(id == '16' ) {
			$("#activityrecurrenceendsid").select2('val','');
			$("#activityrecurrenceontheiddivhid,#activityrecurrenceendsiddivhid").show();
			$("#activityrecurrencedayiddivhid,#activityrecurrenceuntildivhid,#activityafteroccurencesdivhid").hide();
			$("#activityrecurrenceeveryday,#activityrecurrenceendsid,#activityrecurrenceontheid").addClass('validate[required]');
		} else if(id == '14' || id == '19') {
			$("#activityrecurrenceendsid").select2('val','');
			$("#activityafteroccurences,#activityrecurrenceuntil").val('');
			$("#activityrecurrenceendsiddivhid").show();
			$("#activityrecurrenceontheiddivhid,#activityrecurrenceuntildivhid").hide();
			$("#activityrecurrencedayiddivhid,#activityafteroccurencesdivhid").hide();
			$("#activityrecurrenceeveryday,#activityrecurrenceendsid").addClass('validate[required]');
		} else if(id == '15') {
			$("#activityrecurrenceendsid").select2('val','');
			$("#activityrecurrenceontheiddivhid,#activityrecurrenceuntildivhid,#activityafteroccurencesdivhid").hide();
			$("#activityrecurrencedayiddivhid,#activityrecurrenceendsiddivhid").show();
			$("#activityrecurrenceeveryday,#activityrecurrenceendsid,#activityrecurrencedayid").addClass('validate[required]');
		}
	});
	//repeat on - multi select
	$("#activityrecurrencedayid").change(function() {
		var data = $('#activityrecurrencedayid').select2('data');
		var finalResult = [];
		for( item in $('#activityrecurrencedayid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#repeatonmultipleid").val(selectid);
	});
	{// Redirected form Home
		add_fromredirect("activityaddsrc",205);
	}
	//Activity-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique205"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				activityeditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','activitycreationaddform');	
				sessionStorage.removeItem("reportunique205");
			},50);	
		}
	}
	
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			//alert(viewfieldids);
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event	
		$('#dashboardicon').click(function(){
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//recurrences
		$("#activityrecurrenceendsid").change(function() {
			var ferq = $("#activityrecurrencefreqid").val();
			if(ferq == null) {
				alertpopup('Please select the frequency');
				$("#activityrecurrenceendsid").select2('val','');
			} else {
				endsonddvalidateremove();
				var value = $("#activityrecurrenceendsid").val();
				if(value == 2){
					$("#activityafteroccurences").val('');
					$("#activityrecurrenceuntil").val('');
					$("#activityafteroccurencesdivhid").hide();
					$("#activityrecurrenceuntildivhid").hide();
				} else if(value == 3){
					$("#activityrecurrenceuntil").val('');
					$("#activityafteroccurencesdivhid").show();
					$("#activityrecurrenceuntildivhid").hide();
					$("#activityafteroccurences").addClass('validate[required]');
				}else if(value == 4){
					$("#activityrecurrenceuntildivhid").show();
					$("#activityafteroccurences").val('');
					$("#activityafteroccurencesdivhid").hide();
					$("#activityrecurrenceuntil").addClass('validate[required]');
				}
			}
			
		});
	}
	{// Until Date Picker
		var dateformetdata = $('#activityrecurrenceuntil').attr('data-dateformater');
		$('#activityrecurrenceuntil').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#activityrecurrenceuntil').focus();
			}
		}); 
	}
	//module based view drop down load 
	$("#activityinvitemoduleid").change(function() {
		var invitemoduleid = $("#activityinvitemoduleid").val();
		if(invitemoduleid == '247' || invitemoduleid== '246' || invitemoduleid == '1'){
			$('#inviteviewname').empty();
			$('#inviteviewname').select2('val','');
			$("#inviteviewname").attr('readonly',true);
			activitiesinvaddgrid1();
		} else {
			$("#inviteviewname").attr('readonly',false);
			viewdropdownload(invitemoduleid);
		}
		if(invitemoduleid == 4){
			$("#activityremindermodeid option[value='4']").removeAttr('disabled');
		}
		if(invitemoduleid != 4 && invitemoduleid != undefined){
			$("#activityremindermodeid option[value='4']").prop('disabled', 'disabled');
		}
	});
	//view drop down change
	$("#inviteviewname").change(function() {
		activitiesinvaddgrid1();
	});
	//reminder validation
	$("#activityreminderintervalid").change(function() {
		var data = $('#activityreminderintervalid').select2('data');
		var finalResult = [];
		for( item in $('#activityreminderintervalid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#remindbeforeid").val(selectid);
		var reminder = $("#remindbeforeid").val();
	
		var sdate = $("#activitystartdate").val();
		var stime = $("#activitystarttime").val();
		var remindervalue = $("#activityreminderintervalid").find('option:selected').data('activityreminderintervalidhidden');
		if(sdate != '' && stime != '') {
			$.ajax({
				url:base_url+"Activity/reminderintervalcheck?reminderval="+reminder+"&sdate="+sdate+"&stime="+stime+"&remindervalue="+remindervalue,
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					if(data != 'True') {
						alertpopup('You cant create the Reminder for the selected date '+sdate+' time ' +stime);
						$("#activityreminderintervalid").select2('val','');
					}
				},
			});
		} else {
			$("#activityreminderintervalid").select2('val','');
			alertpopup("Please select the Start date and Start Time");
		}
	});
	//invite type change
	$("#activityremindermodeid").change(function() {
		var data = $('#activityremindermodeid').select2('data');
		var finalResult = [];
		for( item in $('#activityremindermodeid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#invitetypeid").val(selectid);
		//templates readonly mode set
		var id = $("#activityremindermodeid").val();
		if(id){
			$("#leadtemplateid").attr('readonly',false);
			$("#emailtemplatesid").attr('readonly',false);
		} else{
			$("#leadtemplateid").attr('readonly',true);
			$("#emailtemplatesid").attr('readonly',true);
		}
		//template required set
		if(finalResult.length == 0){
			$("#emailtemplatesid").removeClass('validate[required]');
			$("#leadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('2', finalResult) > -1) {
			$("#leadtemplateid").addClass('validate[required]');
		} else {
			$("#leadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('3', finalResult) > -1) {
			$("#emailtemplatesid").addClass('validate[required]');
		} else {
			$("#emailtemplatesid").removeClass('validate[required]');
		}
	});
	{//sms template assign
		$("#reminderleadtemplateid").change(function() {
			var smstempid = $("#reminderleadtemplateid").val();
		});
	}
	{//email invite template
		$("#leadtemplateid").change(function() {
			var smstempid = $("#leadtemplateid").val();
		});
	}
	{//mail template assign
		$("#reminderemailtemplatesid").change(function() {
			var mailtempid = $("#reminderemailtemplatesid").val();
		});
	}
	{//email template assign
		$("#emailtemplatesid").change(function() {
			var mailtempid = $("#emailtemplatesid").val();
		});
	}
	$("#mailicon").click(function() {
		var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {	
			sessionStorage.setItem("forcomposemailid",'');
			sessionStorage.setItem("forsubject",'');
			var fullurl = 'erpmail/?_task=mail&_action=compose';
			window.open(''+base_url+''+fullurl+'');
		} else {
			alertpopup("Please select a row");
		}	
	});
	{ //sms and c2c icon work -gowtham
		$("#mobilenumberoverlayclose").click(function() {
			$("#mobilenumbershow").hide();
		});	
		//sms icon click
		$("#smsicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#mobilenumbershow").show();
								$("#smsmoduledivhid").hide();
								$("#smsrecordid").val(datarowid);
								$("#smsmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'smsmobilenumid');
							} else if(mobilenumber.length == 1) {
								sessionStorage.setItem("mobilenumber",mobilenumber);
								sessionStorage.setItem("viewfieldids",viewfieldids);
								sessionStorage.setItem("recordis",datarowid);
								window.location = base_url+'Sms';
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").show();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'smsmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms module dd change
		$("#smsmodule").change(function() {
			var moduleid =	$("#smsmoduleid").val(); //main module
			var linkmoduleid =	$("#smsmodule").val(); // link module
			var recordid = $("#smsrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
			}
		});
		//call module dd chnge
		$("#callmodule").change(function() {
			var moduleid =	$("#callmoduleid").val(); //main module
			var linkmoduleid =	$("#callmodule").val(); // link module
			var recordid = $("#callrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
			}
		});
		//sms mobile number select
		$("#smsmobilenumid").change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#smsmobilenum").val(selectid);
		});
		//sms overlay mobile number submit 
		$("#mobilenumbersubmit").click(function() {
			var datarowid = $("#smsrecordid").val();
			var viewfieldids =$("#smsmoduleid").val();
			var mobilenumber =$("#smsmobilenum").val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem("mobilenumber",mobilenumber);
				sessionStorage.setItem("viewfieldids",viewfieldids);
				sessionStorage.setItem("recordis",datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}	
		});
		//c2c icon click
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1) {
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").show();
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'callmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		//c2c overlay close
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		//c2c mobile number submit.
		$("#callnumbersubmit").click(function()  {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
	}
	$("#activityendtime").change(function(){
		var startdate = $('#activitystartdate').datepicker('getDate');
		var enddate = $('#activityenddate').datepicker('getDate');
		var starttime = $('#activitystarttime').val();
		var endtime = $('#activityendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	      diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			if(splitst[0] >= splitet[0]) {
				if(splitst[1] >= splitet[1]) {
					$('#activityendtime').val('');
					alertpopup('Please select the end time based on the start time');
				}
			}
		}
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,activityaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	{ // For industry
		//recurrence hide
		var recurrence = $("#recurrence").val();
		if(recurrence == 'No') {
			$("#activityrecurrencefreqiddivhid").hide();
			$("#activityrecurrenceeverydaydivhid").hide();
			$("#activityrecurrenceendsiddivhid").hide();
		}
		$("#recurrencecboxid").change(function(){
			var recurrence = $("#recurrence").val();
			if(recurrence == 'Yes') {
				$("#activityrecurrencefreqiddivhid").show();
				$("#activityrecurrenceeverydaydivhid").show();
				$("#activityrecurrenceendsiddivhid").show();
				$('#activityrecurrenceeveryday').removeClass('validate[custom[integer],maxSize[3]]');
				$('#activityrecurrenceeveryday').addClass('validate[required,custom[integer],maxSize[3],min[1]]');
			} else if(recurrence == 'No') {
				$('#activityrecurrenceeveryday').removeClass('validate[required,custom[integer],maxSize[3],min[1]]');
				$('#activityrecurrenceeveryday').addClass('');
				$("#activityrecurrencefreqiddivhid").hide();
				$("#activityrecurrenceeverydaydivhid").hide();
				$("#activityrecurrenceendsiddivhid").hide();
			}
		});
		$("#contactid").change(function() {
			var patientid = $("#contactid").val();
			getmobileandemailid(patientid);
		});
		//reminder mode hide
		$("#remindermodeid option[value='4']").remove();
		$("#remindermodeiddivhid").hide();
		$("#reminderdoctormodeiddivhid").hide();
		$("#notifypatientdivhid").removeClass('large-6 small-6 ');
		$("#notifypatientdivhid").addClass('large-12 small-12');
		$("#notifydoctordivhid").removeClass('large-6 small-6');
		$("#notifydoctordivhid").addClass('large-12 small-12');
		$("#notifypatientcboxid").change(function(){
			var notifypatient = $("#notifypatient").val();
			var notifydoctor = $("#notifydoctor").val();
			if(notifypatient == 'Yes') {
				$("#notifypatientdivhid").removeClass('large-12');
				$("#notifypatientdivhid").addClass('large-6');
				$("#remindermodeiddivhid").removeClass('small-6 medium-6');
				$("#remindermodeiddivhid").addClass('small-12 medium-12');
				$("#remindermodeiddivhid").show();
			} else if(notifypatient == 'No') {
				$("#notifypatientdivhid").removeClass('large-6');
				$("#notifypatientdivhid").addClass('large-12');
				$("#remindermodeiddivhid").hide();
			}
			if(notifypatient !== 'No' || notifydoctor !== 'No' ) {
				$("#reminderleadtemplateid").attr('readonly',false);
				$("#reminderemailtemplatesid").attr('readonly',false);
			} else{
				$("#reminderleadtemplateid").attr('readonly',true);
				$("#reminderemailtemplatesid").attr('readonly',true);
			}
		});
		$("#notifydoctorcboxid").change(function(){
			var notifypatient = $("#notifypatient").val();
			var notifydoctor = $("#notifydoctor").val();
			if(notifydoctor == 'Yes') {
				$("#notifydoctordivhid").removeClass('large-12 small-6');
				$("#notifydoctordivhid").addClass('large-6 small-12');
				$("#reminderdoctormodeiddivhid").removeClass('small-6 medium-6');
				$("#notifydoctordivhid").addClass('small-12 medium-12');
				$("#reminderdoctormodeiddivhid").show();
			} else if(notifydoctor == 'No') {
				$("#notifydoctordivhid").removeClass('large-6');
				$("#notifydoctordivhid").addClass('large-12');
				$("#reminderdoctormodeiddivhid").hide();
			}
			if(notifypatient !== 'No' || notifydoctor !== 'No' ) {
				$("#reminderleadtemplateid").attr('readonly',false);
				$("#reminderemailtemplatesid").attr('readonly',false);
			} else{
				$("#reminderleadtemplateid").attr('readonly',true);
				$("#reminderemailtemplatesid").attr('readonly',true);
			}
		});
	}
	{// Redirected form notification overlay and widget and audit log
		var rdatarowid = sessionStorage.getItem("datarowid");		
		if(rdatarowid != '' && rdatarowid != null){
			 setTimeout(function() {
				 activityeditdatafetchfun(rdatarowid);
				showhideiconsfun('summryclick','activitycreationaddform');
				sessionStorage.removeItem("datarowid");
				Materialize.updateTextFields();
			},50);
		}
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Activities Add Grid
function activityaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#activitiespgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#activitiespgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $("#activityaddgrid").width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol) {
		sortcol = sortcol;
	} else {
		var sortcol = $('.activitiesheadercolsort').hasClass('datasort') ? $('.activitiesheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.activitiesheadercolsort').hasClass('datasort') ? $('.activitiesheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.activitiesheadercolsort').hasClass('datasort') ? $('.activitiesheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=crmactivity&primaryid=crmactivityid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#activityaddgrid').empty();
			$('#activityaddgrid').append(data.content);
			$('#activityaddgridfooter').empty();
			$('#activityaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('activityaddgrid');
			{//sorting
				$('.activitiesheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.activitiesheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#activitiespgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.activitiesheadercolsort').hasClass('datasort') ? $('.activitiesheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.activitiesheadercolsort').hasClass('datasort') ? $('.activitiesheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#activityaddgrid .gridcontent').scrollLeft();
					activityaddgrid(page,rowcount);
					$('#activityaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('activitiesheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					activityaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#activitiespgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					activityaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{// Redirected form Home
				var activityideditvalue = sessionStorage.getItem("activityidforedit"); 
				if(activityideditvalue != null){
					setTimeout(function(){
						activityeditdatafetchfun(activityideditvalue);
						sessionStorage.removeItem("activityidforedit");
					},100);
					Materialize.updateTextFields();
					$("#recurrencedivhid,#activityrecurrencefreqiddivhid,#activityrecurrenceeverydaydivhid,#activityrecurrenceendsiddivhid").hide();
					$("#activityrecurrenceontheiddivhid,#activityrecurrencedayiddivhid,#activityafteroccurencesdivhid,#activityrecurrenceuntildivhid").hide();
					//Add read only for recurrence
					$("#activityrecurrencefreqid").attr('readonly',true);
					$("#activityrecurrenceeveryday").attr('readonly',true);
					$("#activityrecurrenceendsid").attr('readonly',true);
					$("#activityafteroccurences").attr('readonly',true);
					$("#activityrecurrenceuntil").attr('readonly',true);
					$("#activityrecurrenceontheid").attr('readonly',true);
				}
			}
			{// Redirected form calendar
				var uniquelcalsession = sessionStorage.getItem("activityidforclone");
				if(uniquelcalsession != '' && uniquelcalsession != null) {
					setTimeout(function() { 
						activityclonedatafetchfun(uniquelcalsession);
						showhideiconsfun('editclick','activitycreationaddform');
						sessionStorage.removeItem("activityidforclone");
						Materialize.updateTextFields();
					},50);	
				}
			}
			//Material select
			$('#activitiespgrowcount').material_select();
		},
	});    
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function() {
			activitiesinvaddgrid1();
			addslideup('activitycreationview','activitycreationaddform');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			//form field first focus
			firstfieldfocus();
			removevalidaterequired();
			endsonddvalidateremove();
			recurrenceondaydataget();
			$("#actvitycommonid").empty();
			$("#recurrencedivhid").show();
			showhideiconsfun('addclick','activitycreationaddform');
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			if(softwareindustryid == 4){
				$("#actvitymoduleid").select2("val",203);
			}
			froalaset(froalaarray);
			if(softwareindustryid != 5) {
				invitemoduledropdownload();
				$("#activityreminderintervalid").select2('val', '');
				cleargriddata('activitiesinvaddgrid1');
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','205','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
				//show hide recurrence fields
				$("#activityrecurrenceontheiddivhid,#activityrecurrencedayiddivhid,#activityafteroccurencesdivhid,#activityrecurrenceuntildivhid").hide();
				//remove read only for recurrence
				$("#activityrecurrencefreqid").attr('readonly',false);
				$("#activityrecurrenceeveryday").attr('readonly',false);
				$("#activityrecurrenceendsid").attr('readonly',false);
				$("#activityafteroccurences").attr('readonly',false);
				$("#activityrecurrenceuntil").attr('readonly',false);
				$("#activityrecurrenceontheid").attr('readonly',false);
			}
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			$("form#dataaddform").removeAttr('formtype');
			$("form#dataaddform").removeAttr('changed');
			$("#notifypatientcboxid").trigger('change');
			$("#notifydoctorcboxid").trigger('change');
			$("#reminderleadtemplateid").attr('readonly',true);
			$("#reminderemailtemplatesid").attr('readonly',true);
			$("#leadtemplateid").attr('readonly',true);
			$("#emailtemplatesid").attr('readonly',true);
			$("#recurrencecboxid").trigger('change');
			Operation = 0; //for pagination
			setTimeout(function(){
				$("#employeeidddid").select2('val',1+':'+loggedinuser).trigger('change');
			},100);
		});
		$("#editicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				if(softwareindustryid != 5) {
					cleargriddata('activitiesinvaddgrid1');
					invitemoduledropdownload();
					removevalidaterequired();
					endsonddvalidateremove();
					showhideiconsfun('editclick','activitycreationaddform');
					templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','205','sms');
					templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
					templatedatafetch('leadtemplateid','templates','templatesid','templatesname','205','sms');
					templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','205','email');
					//show hide recurrence fields
					$("#recurrencedivhid,#activityrecurrencefreqiddivhid,#activityrecurrenceeverydaydivhid,#activityrecurrenceendsiddivhid").hide();
					$("#activityrecurrenceontheiddivhid,#activityrecurrencedayiddivhid,#activityafteroccurencesdivhid,#activityrecurrenceuntildivhid").hide();
					//Add read only for recurrence
					$("#activityrecurrencefreqid").attr('readonly',true);
					$("#activityrecurrenceeveryday").attr('readonly',true);
					$("#activityrecurrenceendsid").attr('readonly',true);
					$("#activityafteroccurences").attr('readonly',true);
					$("#activityrecurrenceuntil").attr('readonly',true);
					$("#activityrecurrenceontheid").attr('readonly',true);
				}
				froalaset(froalaarray);
				activityeditdatafetchfun(datarowid);
				if(softwareindustryid != 5) {
					settemplatesid(datarowid);
					$("#reminderleadtemplateid").attr('readonly',false);
					$("#reminderemailtemplatesid").attr('readonly',false);
					$("#leadtemplateid").attr('readonly',false);
					$("#emailtemplatesid").attr('readonly',false);
					$("#notifypatientcboxid").trigger('change');
					$("#notifydoctorcboxid").trigger('change');
				}
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#activityaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			activityrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#activitiespgnum li.active').data('pagenum');
		var rowcount = $('ul#activitiespgnumcnt li .page-text .active').data('rowcount');
		activityaddgrid(page,rowcount);
	}
}
//activity invite user grid
function activitiesinvaddgrid1(page,rowcount) {
	var maintabinfo='';
	var parentid='';
	var invitemoduleid = $("#activityinvitemoduleid").val();
	var viewid = $("#inviteviewname").val();
	if(viewid == null || viewid == '1' || viewid == '') { 
		viewid = '82'; 
	}
	if(invitemoduleid === null || invitemoduleid === '' || invitemoduleid === undefined) { 
		invitemoduleid = '4'; 
	}
	$.ajax({
		url:base_url+"Activity/parenttableget?moduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(pdata) {
			if(pdata != "") {
				maintabinfo = pdata;
				parentid = maintabinfo+"id";
			} else {
				maintabinfo = '';
				parentid = '';
			}
		},
	});
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#activitiesinvaddgrid1').width();
	var wheight = $('#activitiesinvaddgrid1').height();
	//col sort
	var sortcol = $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').attr('id') : '0';
	var footername = 'invitefooter';
	$.ajax({
		url:base_url+"Activity/inviteusergrigdatafetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+invitemoduleid+"&viewid="+viewid+"&checkbox=true"+'&footername='+footername,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#activitiesinvaddgrid1').empty();
			$('#activitiesinvaddgrid1').append(data.content);
			$('#activitiesinvaddgrid1footer').empty();
			$('#activitiesinvaddgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('activitiesinvaddgrid1');
			{//sorting
				$('.inviteuserlistheadercolsort').click(function(){
					$('.inviteuserlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#inviteuserlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#inviteuserlistpgnumcnt li .page-text .active').data('rowcount');
					activitiesinvaddgrid1(page,rowcount);
				});
				sortordertypereset('inviteuserlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					activitiesinvaddgrid1(page,rowcount);
				});
				$('#invitefooterpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					activitiesinvaddgrid1(page,rowcount);
				});
			}
			//Material select
			$('#invitefooterpgrowcount').material_select();
			//header check box
			$(".invite_headchkboxclass").click(function() {
				checkboxclass('invite_headchkboxclass','invite_rowchkboxclass');
				getcheckboxrowid(invitemoduleid);
			});
			//row based check box
			$(".invite_rowchkboxclass").click(function(){
				$('.invite_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(invitemoduleid);
			});
			checktheselectedinvitevalues(invitemoduleid);
		}
	});
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid(invitemoduleid) {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	inviteselectuserfetch(invitemoduleid,selected);
}
//new data add submit function
function activitynewdataaddfun() {
	var smscheckid = $('#invitetypeid').val();
	var inviteviewid = $('#inviteviewname').val();
	var invitemoduleid = $('#activityinvitemoduleid').val();
	if(smscheckid != '') {
		var leadid = $('#leadinviteusetid').val();
		var contactid = $('#contactinviteuserid').val();
		var userid = $('#userinviteuserid').val();
		var groupid = $('#groupinviteuserid').val();
		var rolesid = $('#rolesinviteuserid').val();
		var randsid = $('#randsinviteuserid').val();
		if((invitemoduleid == 201 && leadid.length > 0) || (invitemoduleid==203 && contactid.length > 0) || (invitemoduleid==4 && userid.length > 0)) {
			mobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid);
		} else {
			alertpopup('Please select the invite users');
		}		
	} else {
		$("#activtyvalidateovrelay").fadeOut();
		activitydataaddfun();
	}
}
//mobile and email validation
function mobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid) {
	$.ajax({
		url: base_url+"Activity/mobileandmailvalidation",
		data:"leadid="+leadid+"&contactid="+contactid+"&userid="+userid+"&groupid="+groupid+"&rolesid="+rolesid+"&randsid="+randsid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var emailid = data.emailid;
			$("#inviteemailid").val(jQuery.unique(emailid));
			var withoutemailid = data.withoutmail;
			var mobilenum = data.mobilenum;
			$("#invitemobilenumber").val(jQuery.unique(mobilenum));
			var withoutmobilenum = data.withoutmoble;
			var emailvalue=jQuery.trim(withoutemailid.replace(/,/g, " "));
			var mobilevalue=jQuery.trim(withoutmobilenum.replace(/,/g, " "));
			if(emailvalue != '' || mobilevalue != '') {
				$("#activtymobemailvalidateovrelay").fadeIn();
				$("#actmobileemailcreate").show();
				$("#actmobileemailupdate").hide();
				if(mobilevalue != '') {
					strwithoutmobilenum = withoutmobilenum.replace(/^,(.*),$/, '$1');
					$("#activitymobileshow").show();
					$("#withoutmobilenum").val(strwithoutmobilenum);
				} else {
					$("#withoutmobilenum").val('');
					$("#activitymobileshow").hide();
				}
				if(emailvalue != '') {
					strwithoutmail = withoutemailid.replace(/^,(.*),$/, '$1');
					$("#activitymailshow").show();
					$("#withoutmailid").val(strwithoutmail);
				} else {
					$("#withoutmailid").val('');
					$("#activitymailshow").hide();
				}
				Materialize.updateTextFields();
			} else {
				$("#activtymobemailvalidateovrelay").fadeOut();
				activitydataaddfun();
			}
		},
	});
}
//activity new data add after validation
function activitydataaddfun() {
	$("#processoverlay").show();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var amp = '&';
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Activity/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) {
        	var nmsg =  $.trim(msg);
            if (nmsg == "TRUE") {
				$("#processoverlay").hide();
				$('#activitycreationaddform').hide();
				$('#activitycreationview').fadeIn(1000);
				$(".ftab").trigger('click');
				$("#actvitycommonid").empty();
				resetFields();
				$("#activtymobemailvalidateovrelay").fadeOut();
				cleargriddata('activitiesinvaddgrid1');
				$("#activtyvalidateovrelay").fadeOut();
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
			} else {
				$("#processoverlay").hide();
				alertpopup('Error during create.Please refresh and try again');
			}
        },
    });
}
//old information show in form
function activityeditformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax({
		url:base_url+"Activity/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$(".ftab").trigger('click');
				$('#activitycreationaddform').hide();
				$('#activitycreationview').fadeIn(1000);
				refreshgrid();
				$("#processoverlay").hide();
			} else {
				addslideup('activitycreationview','activitycreationaddform');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				editordatafetch(froalaarray,data);
				$("#recurrencedivhid").hide();
				$("#actvitymoduleid").trigger('change');
				setTimeout(function(){
					var commonid = data.commonid;
					$("#actvitycommonid").select2('val',commonid);
					$("#commonid").val(commonid);
					if(data.mobilenumber != '') {
						$('#mobilenumber').val(data.mobilenumber);
					}
					if(data.emailaddress != '') {
						$('#emailaddress').val(data.emailaddress);
					}
					Materialize.updateTextFields();
				},100);
				$("#processoverlay").hide();
			}
		} 
	});
}
function activityinviteuserchack() {
	var smscheckid = $('#invitetypeid').val();
	if(smscheckid != '') {
		var leadid = $('#leadinviteusetid').val();
		var contactid = $('#contactinviteuserid').val();
		var userid = $('#userinviteuserid').val();
		var groupid = $('#groupinviteuserid').val();
		var rolesid = $('#rolesinviteuserid').val();
		var randsid = $('#randsinviteuserid').val();
		updatemobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid);
	} else {
		activitydataupdateinformationfun();
	}
}
//mobile and email validation
function updatemobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid) {
	$.ajax({
		url: base_url+"Activity/mobileandmailvalidation",
		data:"leadid="+leadid+"&contactid="+contactid+"&userid="+userid+"&groupid="+groupid+"&rolesid="+rolesid+"&randsid="+randsid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var emailid = data.emailid;
			$("#inviteemailid").val(jQuery.unique(emailid));
			var withoutemailid = data.withoutmail;
			var mobilenum = data.mobilenum;
			$("#invitemobilenumber").val(jQuery.unique(mobilenum));
			var withoutmobilenum = data.withoutmoble;
			var emailvalue=jQuery.trim(withoutemailid.replace(/,/g, " "));
			var mobilevalue=jQuery.trim(withoutmobilenum.replace(/,/g, " "));
			if(emailvalue != '' || mobilevalue != '') {
				$("#activtymobemailvalidateovrelay").fadeIn();
				$("#actmobileemailcreate").hide();
				$("#actmobileemailupdate").show();
				if(mobilevalue != '') {
					$("#activitymobileshow").show();
					$("#withoutmobilenum").val(withoutmobilenum);
				} else {
					$("#activitymobileshow").hide();
				}
				if(emailvalue != '') {
					$("#activitymailshow").show();
					$("#withoutmailid").val(withoutemailid);
				} else {
					$("#activitymailshow").hide();
				}
			} else {
				$("#activtymobemailvalidateovrelay").fadeOut();
				activitydataupdateinformationfun();
			}
		},
	});
}
//update old information
function activitydataupdateinformationfun() {
	$("#processoverlay").show();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	var amp = '&';
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Activity/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
        	var nmsg =  $.trim(msg);
            if (nmsg == "TRUE") {
				$("#processoverlay").hide();
				resetFields();
				$('#activitycreationaddform').hide();
				$('#activitycreationview').fadeIn(1000);
				$(".ftab").trigger('click');
				$("#actvitycommonid").empty();
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            } else {
				$("#processoverlay").hide();
				alertpopup('Error during update.Please refresh and try again');
			}
         },
    });
}
//Record delete function
function activityrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Activity/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup("Deleted successfully");
            } else if (nmsg.result == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
{//date Range
	function daterangefunction() {
		$('#activitystartdate').datetimepicker("destroy");
		$('#activityenddate').datetimepicker("destroy");
		var startdateformater = $('#activitystartdate').attr('data-dateformater');
		var startDateTextBox = $('#activitystartdate');
		var endDateTextBox = $('#activityenddate');
		var minimumdate = $("#activityrecurrencestartdate");
		var maximumdate = $("#activityrecurrenceuntil");
		startDateTextBox.datetimepicker({
			//timeFormat: 'HH:mm z',
			showTimepicker :false,
			dateFormat: startdateformater,
			changeMonth: true,
			changeYear: true,
			//minDate:null,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
						Materialize.updateTextFields();
				} else {
					endDateTextBox.val(dateText);
					Materialize.updateTextFields();
				}
				$('#activitystartdate').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				minimumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.activityenddateformError').remove();
					$('#activityenddate').removeClass('error');
					Materialize.updateTextFields();
				}
			}
		});
		endDateTextBox.datetimepicker({
			//timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			//minDate:0,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						Materialize.updateTextFields();
						maximumdate.datetimepicker('setDate', testEndDate);
				} else {
					Materialize.updateTextFields();
				}
				$('#activityenddate').focus();
			},
			onSelect: function (selectedDateTime) {
				minimumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				Materialize.updateTextFields();
			}
		});
	}
}
{// Edit Function 
	function activityeditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();		
		$("#actvitycommonid").empty();
		activityeditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function 
	function activityclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide();		
		activityeditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
//invite user module drop down load
function invitemoduledropdownload() {
	$('#activityinvitemoduleid').empty();
	$('#activityinvitemoduleid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Activity/moduledropdownloadfun",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#activityinvitemoduleid').append(newddappend);
			}
		},
	});
}
//view drop down load
function viewdropdownload(invitemoduleid) {
	$('#inviteviewname').empty();
	$('#inviteviewname').append('<option value = "" ></option>');
	$.ajax({
		url: base_url+"Activity/viewdropdownloadfun?invitemoduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#inviteviewname').append(newddappend);
			}	
			$('#inviteviewname').trigger('change');
		},
	});
}
//selected row data get - invite grid
function inviteselectuserfetch(invitemoduleid,datarowid) {
	var	inviteuser =  invitemoduleid+'|'+datarowid;
	if(datarowid.length > 0){
		inviteuser=inviteuser;
	} else{
		inviteuser='';
	}
	if(invitemoduleid == '201') {
		$("#leadinviteusetid").val(inviteuser);
	} else if(invitemoduleid == '203') {
		$("#contactinviteuserid").val(inviteuser);
	} else if(invitemoduleid == '4') {
		$("#userinviteuserid").val(inviteuser);
	} else if(invitemoduleid == '246') {
		$("#groupinviteuserid").val(inviteuser);
	} else if(invitemoduleid == '247') {
		$("#rolesinviteuserid").val(inviteuser);
	} else if(invitemoduleid == '1') {
		$("#randsinviteuserid").val(inviteuser);
	}
}
//check the selected invite user 
function checktheselectedinvitevalues(invitemoduleid) {
	var leadid = $("#leadinviteusetid").val();
	var contactid = $("#contactinviteuserid").val();
	var userid = $("#userinviteuserid").val();
	var groupid = $("#groupinviteuserid").val();
	var rolesid = $("#rolesinviteuserid").val();
	var randsid = $("#randsinviteuserid").val();
	if(invitemoduleid == '201') {
		if(leadid != '') {
			var lead = leadid.split('|');
			var lid = lead[1].split(',');
			checkboxcheck(invitemoduleid,lid);
		}
	} else if(invitemoduleid == '203') {
		if(contactid != '') {
			var contact = contactid.split('|');
			var cid = contact[1].split(',');
			checkboxcheck(invitemoduleid,cid);
		}
	} else if(invitemoduleid == '4') {
		if(userid != '') {
			var user = userid.split('|');
			var uid = user[1].split(',');
			checkboxcheck(invitemoduleid,uid);
		}
	} else if(invitemoduleid == '246') {
		if(groupid != '') {
			var group = groupid.split('|');
			var gid = group[1].split(',');
			checkboxcheck(invitemoduleid,gid);
		}
	} else if(invitemoduleid == '247') {
		if(rolesid != '') {
			var roles = rolesid.split('|');
			var rid = roles[1].split(',');
			checkboxcheck(invitemoduleid,rid);
		}
	} else if(invitemoduleid == '1') {
		if(randsid != '') {
			var rands = randsid.split('|');
			var rsid = rands[1].split(',');
			checkboxcheck(invitemoduleid,rsid);
		}
	}
}
function checkboxcheck(invitemoduleid,checkid) {
	//alert(checkid);
	for(var i=0;i <checkid.length;i++) {
		if(invitemoduleid == 201){
			$('#leads_rowchkbox'+checkid[i]).attr('checked',true);
		} else if(invitemoduleid == 203) {
			$('#contacts_rowchkbox'+checkid[i]).attr('checked',true);
		} else if(invitemoduleid == 4) {
			$('#employee_rowchkbox'+checkid[i]).attr('checked',true);
		}
	}
} 
//activity recurrence on the  
function recurrenceondaydataget() {
	$.ajax({
		url: base_url + "Activity/recurrecneonday",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#activityrecurrenceontheid').empty();
			$('#activityrecurrenceontheid').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#activityrecurrenceontheid')
					.append($("<option></option>")
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//record data fetch
function usernamefetch(fieldname,tabname) {
	$('#actvitycommonid').empty();
	$('#actvitycommonid').select2('val','');
	$.ajax({
		url:base_url+"Activity/recordnamefetch?fieldname="+fieldname+"&tabname="+tabname,
		dataType:'json',
		async :false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$('#actvitycommonid').append($("<option></option>"));
				$.each(data, function(index) {
					$('#actvitycommonid')
					.append($("<option></option>")
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
				});
			}
			$('#actvitycommonid').trigger('change');
		},
	});
}
{//SMS template and MAIL TEMPLATE value get
	function templatedatafetch(ddname,tablename,fieldid,fieldname,moduleid,type) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Activity/templateddvalfetch?tablename="+tablename+"&fieldid="+fieldid+"&fieldname="+fieldname+"&moduleid="+moduleid+"&type="+type,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes') {
							$('#'+ddname+'').select2('val',data[index]['datasid']);
						}
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//required class removed from reminder concept
	function removevalidaterequired() {
		$("#activityrecurrenceeveryday").removeClass('validate[required]');
		$("#activityrecurrenceendsid").removeClass('validate[required]');
		$("#activityrecurrencedayid").removeClass('validate[required]');
		$("#activityrecurrenceontheid").removeClass('validate[required]');
	}
	function endsonddvalidateremove() {
		$("#activityafteroccurences").removeClass('validate[required]');
		$("#activityrecurrenceuntil").removeClass('validate[required]');
	}
}
{//template name fetch
	function templatenamefetch(datarowid){
		$.ajax({
			url: base_url+"Activity/templatenamefetch?datarowid="+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var emailtemplate = data.emailtemplatesid;
					var leadtemplate = data.leadtemplateid;
					$("#reminderemailtemplatesid").select2('val',emailtemplate);
					$("#reminderleadtemplateid").select2('val',leadtemplate);
				} 	
			},
		});
	}
}
//patient based email id and mobile number
function getmobileandemailid(patientid) {
	$.ajax({
		url: base_url+"Activity/getmobileandemailid?patientid="+patientid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				var mobilenumber = data.mobilenumber;
				var emailid = data.emailid;
				var landlinenumber = data.landlinenumber;
				if(mobilenumber != ''){
					$("#mobilenumber").val(mobilenumber);
				} else {
					$("#mobilenumber").val(landlinenumber);
				}
				$("#emailaddress").val(emailid);
			}
		},
	});
}
function settemplatesid(id) {
	$.ajax({
		url: base_url+"Activity/gettemplatesid?actid="+id,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.status) != 'FAILED') {
				if(data.smsid && data.smsid != 1){
					$('#reminderleadtemplateid').select2('val',data.smsid);
				}
				if(data.emailid && data.emailid != 1){
					$('#reminderemailtemplatesid').select2('val',data.emailid);
				}
				if(data.invmodeid && data.invmodeid != 1){
					var modid = data.invmodeid;
					var arraymode = modid.split(',');
					$('#activityremindermodeid').select2('val',arraymode);
				}
				if(data.invsmsid && data.invsmsid != 1){
					$('#leadtemplateid').select2('val',data.invsmsid);
				}
				if(data.invemailid && data.invemailid != 1){
					$('#emailtemplatesid').select2('val',data.invemailid);
				}
				if(data.moduleid && data.moduleid != 1){
					$('#activityinvitemoduleid').select2('val',data.moduleid);
				}
				if(data.viewid && data.viewid != 1){
					$('#inviteviewname').select2('val',data.viewid);
				}
				
			}
		},
	});
}
//mobile number based email id
function getmobilenumberemailid(commonid,moduleid) {
	$.ajax({
		url: base_url+"Activity/getmobilenumberemailid?commonid="+commonid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if(data.mobile != 0 || data.mobile != ""){
				$("#mobilenumber").val(data.mobile);
			} else{
				$("#mobilenumber").val('');
			}
			if(data.emailid != 0 || data.emailid != ""){
				$("#emailaddress").val(data.emailid);
			} else{
				$("#emailaddress").val('');
			}
			Materialize.updateTextFields();
		},
	});
}