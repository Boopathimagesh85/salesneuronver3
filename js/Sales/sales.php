<script>
$(document).ready(function() {
	{
		var notH = 1,
		$pop = $('#createshortcut').hover(function(){notH^=1;});
		$(document).on('mousedown keydown', function( e ){
		if(notH||e.which==27) $pop.stop().fadeOut();
		});
	}
	/* Retrieved values from company settings */
	multitagbarcode = $("#multitagbarcode").val();
	untagwtvalidate = $("#untagwtvalidate").val();
	untagwtshow = $("#untagwtshow").val();
	stonepiecescalc = $('#stonepiecescalc').val();
	transactioncategoryfield = $("#transactioncategoryfield").val();
	transactionemployeeperson = $("#transactionemployeeperson").val();
	accountfieldcaps = $("#accountfieldcaps").val();
	chitbooktypeid = $("#chitbooktypeid").val();
	estimatequicktotal = $("#estimatequicktotal").val();
	estimatedefaultset = $("#estimatedefaultset").val(); // set default account and autofocus on stocktypeid for estimate
	caratwtcalcvalue = $("#caratwtcalcvalue").val(); // caratwt = grosswt * caratwtcalcvalue;
	pancardrestrictamt = $("#pancardrestrictamt").val();
	cashreceiptlimit = $("#cashreceiptlimit").val();
	cashissuelimit = $("#cashissuelimit").val();
	accountsummaryinfo = $("#accountsummaryinfo").val();
	printtemplatehideshow = $("#printtemplatehideshow").val();
	/* Concept - while receiving an item (create lot => val 1, direct link to itemtag => val 0) */
	receiveorderconcept = $('#receiveorderconcept').val();
	paymentsavebutton = $('#paymentsavebutton').val();
	/* Only for touch enable and disable. If it is 1(one), readonly true. Zero(0) means can change the touch value */
	touchchargesedit = $("#touchchargesedit").val();
	
	/* Discount percent or amount. zero (0) direct amount to discount. If 1 (one) is percentage discount, it will discount on wastage value */
	discountbilllevelpercent = $("#discountbilllevelpercent").val();
	discountamtwithsign = 0;
	discountoldamtwithsign = 0;

	salesauthuserrole = $("#salesauthuserrole").val();
	salesauthuserrole = salesauthuserrole.split(",");
	untagstoneenable = $("#untagstoneenable").val();
	mainsaveclicksuccess = '0';
	approvaloutrfid = $("#approvaloutrfid").val();
	rfidtagarray = new Array();
	rfidtagarrayincrem = 1;
	rfidbasedtagfetch = 1;
	estimatemainsavetrigger = 0;
	valuemathaboslute = '';
	adjustmentvaluemathabsolute = '';
	salespaymenthideshow = $("#salespaymenthideshow").val();
	discountauthmoduleview = $("#discountauthmoduleview").val();
	receiveordertemplateid = $("#receiveordertemplateid").val();
	rejectreviewordertemplateid = $("#rejectreviewordertemplateid").val();
	accountmobileunique = $('#accountmobileunique').val();
	oldjewelsstonedetails = $('#oldjewelsstonedetails').val();
	purchasemodstonedetails = $('#purchasemodstonedetails').val();
	billamountcalculationtrigger = $('#billamountcalculationtrigger').val();
	oldjewelsstonecalc = $('#oldjewelsstonecalc').val();
	ratedisplaymainview = $('#ratedisplaymainview').val();
	ratedisplaypurityvalue = $('#ratedisplaypurityvalue').val();
	salesreturnbillcustomers = $('#salesreturnbillcustomers').val();
	advanceorbalance = $('#advanceorbalance').val();
	questionaireapplicable = $('#questionaireapplicable').val();
	questionairegenpovendorarray = new Array();
	questionairerecorderadminarray = new Array();
	metalarraydata = converttoarray($('#all_metalarraydata').val());
	purityarraydata = converttoarray($('#all_purityarraydata').val());
	billleveltaxdetailids = new Array();
	adjustmentadditionvalue = 0;
	ordertrackmainviewclick = 0;
	salesfieldcommentrequired = 0;
	mainviewgrid_placeorderid = 1;
	select2serachaccountcontent = '';
	select2searchdataempty = 0;
	{ // Foundation Initialization
		$(document).foundation();
	}
	//sales date change
	salesdatechangeablefor = $("#salesdatechangeablefor").val();
	//daterangefunction('dateofstock','dateofstockto');
	{// form height
		var saleswindow = $(window).height();
		var saleformheight =  saleswindow - 49;
		$(".salesmdaddformcontainer").css("height",saleformheight);
	}
	$('#peronalsettingicon').click(function() {
    	window.location = base_url+'Personalsettings';
    });
    $('#logouticon').click(function() {
    	window.location = base_url+'Login/logout';
    });
	$('#dataupdatesubbtn').hide();
	{
		$('#summary-toggle').click(function() {
			$('.toggle-summarycontent').toggle();
		});
		$('#header-toggle').click(function() {
			$('.toggle-headercontent').toggle();
		});
	}
	$( window ).resize(function() {
		sectionpanelheight('transactionsectionoverlay');
		var saleswindow = $(window).height();
		var saleformheight =  saleswindow - 49;
		$(".salesmdaddformcontainer").css("height",saleformheight);
	});
	$("#transactionstockicon").click(function() {
		alertpopup('sales');
		$('#processoverlay').show();
		itemsalestagentry(52);
	});
	{//inner-form-with-grid
		$("#transactionsectionadd").click(function() {
			sectionpanelheight('transactionsectionoverlay');
			$("#transactionsectionoverlay").removeClass("closed");
			$("#transactionsectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#salesdetailbtncancel").click(function() {
			$("#transactionsectionoverlay").removeClass("effectbox");
			$("#transactionsectionoverlay").addClass("closed");
		});
	}
	productidshow = $('#productidshow').val();
	allrfideviceno = $('#allrfideviceno').val(); 
	allowedaccounttypeset = $('#allowedaccounttypeset').val();  
	if(allrfideviceno == '') {
		$('#rfiddeviceno-div').hide(); 
	}
	/* $(document).on( "click", "#searchaddaccount", function() {
		
	}); */
	{ // account/Parentaccount drop down data load 
		function repoFormatResult(repo) {
		    var desc = '';
		    if (repo.accountname) {
				/* if(repo.ddcreate == 'yes') {
					var accountaddition = '<span class="innerfrmicon" id="searchaddaccount" style="position:relative;top:6px;left:20px;cursor:pointer" tabindex="887"><i class="material-icons">person</i></span>';
				} else {
					var accountaddition = '';
				} */
				var accountaddition = '';
				if(accountypehideshow == 1) {
					if(repo.mobilenumber != '') {
						desc += '<small>' + repo.acctypeshortname +' - '+ repo.accountname +' - '+repo.mobilenumber+ '</small>';
					} else {
						desc += '<small>' + repo.acctypeshortname +' - '+ repo.accountname + '</small>';
					}
				} else {
					if(repo.mobilenumber != '') {
						desc += '<small>' + repo.accountname +' - '+repo.mobilenumber+ '  '+accountaddition +'</small>';
					} else {
						desc += '<small>' + repo.accountname + '  '+accountaddition +'</small>';
					}
				}
		    }
			return desc;
		}
		function repoFormatSelection(repo) {
			$("#accountstateid").val(repo.stateid);
			$("#selaccounttypeid").val(repo.accounttypeid);			
			if(repo.mobilenumber != '') {
				if(accountypehideshow == 1) {
					return repo.acctypeshortname+' - '+repo.accountname+' - '+repo.mobilenumber;
				} else {
					return repo.accountname+' - '+repo.mobilenumber;
				}
			} else {
				if(accountypehideshow == 1) {
					return repo.acctypeshortname+' - '+repo.accountname;
				} else {
					return repo.accountname;
				}
			}
		}
		var select2 = $('#accountid').select2({
		    placeholder: "Search for a Account",
		    minimumInputLength: 2,
		    ajax: {
			    url: base_url+"Sales/accountgroup_dd",
			    type: 'post',
			    dataType: 'json',
			    quietMillis: 10,
			    data: function (term, page) {
					select2serachaccountcontent = term;
			        return {
				        q: term,
				        type: $("#salestransactiontypeid").val(),
						stocktype: $("#paymentstocktypeid").val(),
						allowedaccounttypeid: allowedaccounttypeid,
						allowedaccounttypeset: allowedaccounttypeset,
				        page: page
			        };
			    },
			    results: function (data, page) {
					if(data == '') {
						select2searchdataempty = 1;
					}
					//alert(data[0].ddcreate);
			        var more = (page * 30) < data.length;
			        return { results: data, more: more };
			    }
		    },
			initSelection: function (element, callback) {
	           var id = $(element).val();
	           	if(id !== "") {
	               $.ajax(base_url+"Sales/accountgroup_dd", {
	                   data: {acid:id,q:'',type: $("#salestransactiontypeid").val(),stocktype: $("#paymentstocktypeid").val(),allowedaccounttypeid: allowedaccounttypeid,allowedaccounttypeset: allowedaccounttypeset},
	                   type: 'post',
	                   dataType: "json"
	               }).done(function(data) {
	                   callback({ id: data[0]['id'], text: data[0]['text'] });
	               });
	       		}
	        },
		    formatResult: repoFormatResult,
		    formatSelection: repoFormatSelection,
		    dropdownCssClass: "bigdrop",
		    escapeMarkup: function (m) { return m; }
		});
		var select2 = $('#journalaccountid').select2({
		    placeholder: "Search for a Account",
		    minimumInputLength: 2,
		    ajax: {
			    url: base_url+"Sales/accountgroup_dd",
			    type: 'post',
			    dataType: 'json',
			    quietMillis: 10,
			    data: function (term, page) {
			        return {
				        q: term,
				        type: $("#salestransactiontypeid").val(),
						stocktype: $("#paymentstocktypeid").val(),
						allowedaccounttypeid: allowedaccounttypeid,
						allowedaccounttypeset: allowedaccounttypeset,
				        page: page
			        };
			    },
			    results: function (data, page) {
			        var more = (page * 30) < data.length;
			        return { results: data, more: more };
			    }
		    },
			initSelection: function (element, callback) {
	           var id = $(element).val();
	           	if(id !== "") {
	               $.ajax(base_url+"Sales/accountgroup_dd", {
	                   data: {acid:id,q:'',type: $("#salestransactiontypeid").val(),stocktype: $("#paymentstocktypeid").val(),allowedaccounttypeid: allowedaccounttypeid,allowedaccounttypeset: allowedaccounttypeset},
	                   type: 'post',
	                   dataType: "json"
	               }).done(function(data) {
	                   callback({ id: data[0]['id'], text: data[0]['text'] });
	               });
	       		}
	        },
		    formatResult: repoFormatResult,
		    formatSelection: repoFormatSelection,
		    dropdownCssClass: "bigdrop",
		    escapeMarkup: function (m) { return m; }
		});
	}
	{// account dropdown icon set
		$("#s2id_accountid").css('display','inline-block').css('width','75%');
		var icontabindex = $("#s2id_accountid .select2-focusser").attr('tabindex');
		$("#accountid").after('<span class="innerfrmicon" id="addaccount" style="position:relative;top:0px;left:2px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">person</i></span><span class="innerfrmicon" id="accountsearchevent" style="position:relative;top:0px;right:-3px;cursor:pointer;margin-left:0px" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span><span class="innerfrmicon" id="editaccount" style="position:relative;top:0px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">edit</i>');
	}
	{// transactionmodeid dropdown icon set
		$("#s2id_transactionmodeid").css('display','inline-block').css('width','100%');
		var icontabindex = $("#s2id_transactionmodeid .select2-focusser").attr('tabindex');
		$("#transactionmodeid").after('<span class="innerfrmicon" id="billrefnoicon" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
		$("#transactionmodeid").after('<span class="innerfrmicon" id="wtcheckicon" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
	}
	{// salestransactiontype dropdown icon set
		$("#s2id_salestransactiontypeid").css('display','inline-block').css('width','95%');
		var icontabindex = $("#s2id_salestransactiontypeid .select2-focusser").attr('tabindex');
		$("#salestransactiontypeid").after('<span class="innerfrmicon" id="ttypesettings" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
		
	}
	generatecalciconafterload('rfidtagnumber','appoutrfidbtn','material-icons radio_button_checked innerfrmicon');
	generatecalciconafterload('itemtagnumber','tagnomultiscan','material-icons radio_button_checked innerfrmicon');
	{// employee dropdown icon set
		$("#s2id_employeepersonid").css('display','inline-block').css('width','70%');
		var icontabindex = $("#s2id_employeepersonid .select2-focusser").attr('tabindex');
		$("#employeepersonid").after('<span class="innerfrmicon" id="commenticon" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">textsms</i></span>');
	}
	{// payment employee dropdown icon set
		$("#s2id_paymentemployeepersonid").css('display','inline-block').css('width','70%');
		var paymenticontabindex = $("#s2id_employeepersonid .select2-focusser").attr('tabindex');
		$("#paymentemployeepersonid").after('<span class="innerfrmicon" id="paymentcommenticon" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+paymenticontabindex+'"><i class="material-icons">textsms</i></span>');
	}
	{// chit textbox dropdown icon set
		$("#chitbookno").css('display','inline-block').css('width','70%');
		var paymenticontabindex = $("#chitbookno").attr('tabindex');
		$("#chitbookno").after('<span class="innerfrmicon" id="chitbooknoicon" style="position:absolute;top:3px;cursor:pointer" tabindex="'+paymenticontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
	}
	{ // Append Customer Detail Information after Submit Transaction Type
		$("#salesaddformdiv .gridcaptionpos").append('<div><span class="accountinfodetails" style="top: -64px; left: 19em !important; position: relative;"><span class="" id="accountinformation" style="font-size: 1.05rem !important;"></span>&nbsp;&nbsp;<span class="borderstylesales" id="accountadvinfo" style="font-size: 1.05rem !important; display: none; background-color: yellow; height: 60px; top: 3px !important;"><span>ADV:</span><span id="accountadvamt" style="font-size: 1.05rem !important;" class="value"></span></span>&nbsp;&nbsp;<span class="borderstylesales" id="accountcreditinfo" style="font-size: 1.05rem !important; display: none; background-color: yellow; height: 60px; top: 3px !important;"><span>CRD:</span><span id="accountcreditamt" style="font-size: 1.05rem !important;" class="value"></span></span>&nbsp;&nbsp;<span class="borderstylesales" id="accountpaidinfo" style="font-size: 1.05rem !important; display: none; background-color: yellow; height: 60px; top: 3px !important;"><span>PAID AMT:</span><span id="sumpaidamt"  style="font-size: 1.05rem !important;" class="value"></span></span></span></div>');
	}
	{ // Auto Complete Address, Area, Postal Code, City by Kumaresan
		$("#accountprimaryaddress").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompleteaddress",
					success: function (data) {
						var array = $.map(data.address, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#accountprimaryaddress").val(ui.item.label);
            }
		});
		$("#primaryarea").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletearea",
					success: function (data) {
						var array = $.map(data.primaryarea, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#primaryarea").val(ui.item.label);
            }
		});
		$("#accountprimarypincode").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletepostalcode",
					success: function (data) {
						var array = $.map(data.pincode, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#accountprimarypincode").val(ui.item.label);
            }
		});
		$("#accountprimarycity").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletecity",
					success: function (data) {
						var array = $.map(data.city, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#accountprimarycity").val(ui.item.label);
            }
		});
	}
	round = $('#roundweight').val();
	roundweight = $('#roundweight').val();
	pround = $('#roundweight').val();
	roundamount = $('#roundamount').val();
	roundmelting = $('#roundmelting').val();
	dia_weight_round = $('#dia_weight_round').val();
	tranmodedefault = $('#tranmodedefault').val();
	customerwise = $('#customerwise').val();
	vendorwise = $('#vendorwise').val();
	tranmodedefault = checknan(tranmodedefault);
	chargesdonttriggergrosscalc = 0;
	stocktypedefault=0;
	purity_change_status=0;
	billdeletestatus=0;
	retrievestatus=0;
	retrievetagstatus=0;
	approvalnoeditstatus=0;
	billeditstatus = 0;
	billeditstatusclose = 0;
	mainsavestatus = 0;
	creditoverlayinnereditstatus = 0;
	creditoverlaytriggerplace = 0;
	defaultsetpaymentstocktype = 26;
	bhavcutbasedsummary = 0;
	j = 0;
	global_stocksummary =''
	addchargeoperation = 'ADD';
	operationstone='ADD';
	creditoperationstone = 'ADD';
	discountoperation = 0; // 0 for addition  and 1 for update
	transactiontype=$('#transactiontype').val();
	wastagechargetype=$('#wastagechargetype').val();
	makingchargetype=$('#makingchargetype').val();
	hallmarkchargetype=$('#hallmarkchargetype').val();
	barcodeoption = $('#barcodeoption').val();
	rfidshowhide = '';
	if(barcodeoption == '1'){rfidshowhide = 'rfidtagnumber';}
	oldjewelsumtaxapplyid = $('#oldjewelsumtaxapplyid').val();
	oldsalesreturntax = 0;
	billleveltaxautoremoval = 0;
	adjustmentamountkeypressed = 0;
	transactionmodestatus=0;
	netcalcheck = 0;
	accounttaxstatus=$('#accounttaxstatus').val();
	defaultchargeid = '';
	tagstoneentrydelete='';
	// initially first data set old item data
    olditemdatafirst=$('#olditemfirst').val();
	// used these var in auto issue/receipt setting
	amttypeir = ['currentbalanceamounthide','26','27','28','29','30','31','37','41','57','79'];
	puretypeir = ['currentbalancepureweighthide','58','38','49','70'];
	chargecalculation = $('#chargecalculation').val();
	approvaloutcalculation = $('#approvaloutcalculation').val();
	businesspurchasechargedata = $('#businesspurchasechargeid').val();
	businesschargedata = $('#businesschargeid').val();
	purchasewastage = $('#purchasewastage').val();
	salesrate = $('#salesrate').val();
	comppurchaserate = $('#comppurchaserate').val();
	metalpurityarray = $('#metalpurityarray').val();
	metalpurityarrayjson = {};
	metalpurityarrayjson = metalpurityarray;
	counterstatus = $("#defultcounter").val();
	paymentmodestatus = $("#paymentmodestatus").val();
	companytaxapplicable = $('#companytaxapplicable').val();
	discountpasswordstatus = $('#discountpasswordstatus').val();
	discountcalc = $('#discountcalc').val();
	taxcalc = $('#taxcalc').val();
	defaultcashinhandid = $('#defaultcashinhandid').val();
	defaultbankid = $('#defaultbank').val();
	taxchargeinclude = $('#taxchargeinclude').val();
	discountauthorization = $('#discountauthorization').val();
	purewtcalctype = parseFloat($('#purewtcalctype').val());
	oldjeweldetails = $('#oldjeweldetails').val();
	wastagecalctype = $('#wastagecalctype').val();
	gstapplicable = $('#gstapplicable').val();
	companygsttax = $('#companygsttax').val();
	povendormanagement = $('#povendormanagement').val();
	takeordercategory = $('#takeordercategory').val();
	accountypehideshow = $('#accountypehideshow').val();
	takeorderduedate = $('#takeorderduedate').val();
	poautomaticvendor = $('#poautomaticvendor').val();
	lottype = $('#lottype').val();
	purchasedisplay = $('#purchasedisplay').val();
	mainprintemplateid = 0;
	salesandoldstatus = 0;
	salesstatus = 0;
	salesoldstatus = 0;
	oldstatus = 0;
	onlyoldstatus = 0;
	returnstatus = 0;
	addsalesstatus = 0;
	userroleid = $('#userroleid').val();
	estimatestatus = 0;
	receivestatus = 0;
	placeorderstatus = 0;
	takeorderoldstatus = 0;
	takeorderreceivestatus = 0;
	takeorderstatus = 0;
	onlytakeorderstatus = 0;
	salesdetailcreatestatus = 0;
	onlytakeorderadvancestatus = 0;
	editpaymentstatus = 0;
	chargerequired = $('#chargerequired').val();
	wastagevisible = 0;
	touchvisible = 0;
	calculationdone = 0;
	operationstone='ADD';
	defaultvaluearray = {};
	hideshowvaluearray = {};
	cashcountervaluearray = {};
	discountvaluearray = {};
	estimatearray = new Array();
	oldjewelstatus = 1;
	salesreturnstatus = 1;
	discountusestatus = 0;
	salesdetailentryeditstatus = 0;
	allowedaccounttypeid = '';
	// For Sales Items
	metalarraydetails = '';
	billlevelrestrictmetalid = 1;
	metalstatus = 0;
	// For Old Items
	metaloldarraydetails = '';
	billlevelrestrictoldmetalid = 1;
	metaloldstatus = 0;
	// For Return Items
	metalreturnarraydetails = '';
	billlevelrestrictreturnmetalid = 1;
	metalreturnstatus = 0;
	Operation = 0; //for pagination
	pdfpreviewtemplateload(52);
	{// Grid Calling Function
		salesgrid();
		taxgrid();
		billtaxgrid();
		saledetailsgrid();
		stoneentrygrid();
		//bulksalesgrid();
	}
	{//Stone Cal Function Call
		compybasednetmccalc();
	}
	{//close
		var addcloseinfo = ["closeaddform", "salesgriddisplay", "salesaddformdiv"]
		addclose(addcloseinfo);		
	}
	if(userroleid == '2') { 
		generatecalciconafterload('additionalchargeamt','cdicon','material-icons radio_button_checked innerfrmicon');
	}
	generatecalciconafterload('ratepergram','ratedefaulticon','material-icons radio_button_checked innerfrmicon');
	generatecalciconafterload('sumarydiscountamount','discountautnicon','material-icons radio_button_checked innerfrmicon');
	generatecalciconafterload('creditno','issuereceipticon','material-icons radio_button_checked innerfrmicon');
	generatecalciconafterload('discount','itemdiscountautnicon','material-icons radio_button_checked innerfrmicon');
	{// View by drop down change
        $('#dynamicdddataview').change(function() {
            salesrefreshgrid();
        });
    }
	{//stone related works
	//Loaing Stoneentry Overlay on Click
		$("#stoneoverlayicon").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined) {
				datarowselectevt();
				var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
				var stocktype = $("#stocktypeid").find('option:selected').val();
				if(transactiontype == 20 && stocktype == 75 || transactiontype == 27 && stocktype == 85 || transactiontype == 28 && stocktype == 86 || stocktype == 19 && oldjewelsstonedetails == 1 || transactiontype == 11 && billamountcalculationtrigger == 1 || transactiontype == 9 && (stocktype == 17 || stocktype == 80 || stocktype == 81) && purchasemodstonedetails == 1) {
					$("#savestoneentrydata").removeClass('hidedisplay');
					$("#closestoneentryform").addClass('hidedisplay');
					$("#stoneleftform").removeClass('hidedisplay');
					$("#stoneleftgrid").removeClass('large-12');
					$("#stoneleftgrid").addClass('large-8');
					$("#tagstoneentryedit,#salestagstoneentrydelete").show();
					$("#stoneoverlay").fadeIn();
					if($("#hiddenstonedata").val() !='' && $("#hiddenstonedata").val() !=0){
						  getstonegridsummary();
					}
				} else {
					var stoneamt = $("#totamt").text();
					if(stoneamt != '' || stoneamt != 0) {
						if(transactiontype!=13) { // weight sales
							$("#savestoneentrydata").addClass('hidedisplay');
							$("#closestoneentryform").removeClass('hidedisplay');
							$("#stoneleftform").addClass('hidedisplay');
							$("#stoneleftgrid").addClass('large-12');
							$("#stoneleftgrid").removeClass('large-8');
							if(stocktype == 13) { // Partial tag
								$("#savestoneentrydata").removeClass('hidedisplay');
								$("#closestoneentryform").addClass('hidedisplay');
								$("#tagstoneentryedit").hide();
								$("#salestagstoneentrydelete").show();
							} else {
								$("#tagstoneentryedit,#salestagstoneentrydelete").hide();
							}
							$("#stoneoverlay").fadeIn();
						}
						if($("#hiddenstonedata").val() != '' && $("#hiddenstonedata").val() != 0){
							getstonegridsummary();
						}
					} else {
						alertpopup('There is no stone for this tagnumber');
					}
				}
			}
		});	
		//Stone Entry => Stone name change => Load Rate amount
		$('#stoneid').change(function() {
			var stocktype = $("#stocktypeid").find('option:selected').val();
			var gridname = 'stoneentrygrid';
			var colname = 'stoneid';
			var list = new Array();
			$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				list.push($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
			});
			var reorderval = $(this).find('option:selected').val();
			var isInList = false;
			for(var i = 0; i < list.length; i++){
				if(list[i] === reorderval) {
					isInList = true;
					break;
				}
			}
			if(isInList){
				 alertpopupdouble("stone already Exist. Kindly choose another Stone");
				 $(this).select2('val','');
				 return false;
			}
			var stoneid=$(this).val(); 
			var accid = $("#accountid").val(); 
			$.ajax({
				url:base_url +"Sales/getratefromstonename",
				data:{stoneid:stoneid,accid:accid},
				type: "POST",
				dataType:'json',	
				success: function(data) {
					$("#basicrate").val(data.stonerate);
					$("#purchaserate").val(data.purchaserate);
					$("#stoneentrycalctypeid").select2('val',data.stoneentrycalctypeid).trigger('change');
					if(stocktype == 86) {
						$("#basicrate,#purchaserate").val(0);
						$("#purchaserate-div,#basicrate-div").hide();
					} else {
						$("#purchaserate-div,#basicrate-div").show();
					}
					stoneentrycal();
					Materialize.updateTextFields();
				}
			});
		});
		$('#stoneentrycalctypeid').blur(function() {
			stoneentrycal();
		});
		$('#stoneentrycalctypeid').change(function() {
			var typeval = $(this).val();
			$('#stonegram,#stonepieces,#caratweight,#totalweight,#stonetotalamount').val(0);
			$('#stonegram,#caratweight,#stonepieces').prop('readonly',false);
			$("span","#stonepiecesdiv label").remove();
			$("span","#stonegramdiv label").remove();
			$("span","#caratweightdiv label").remove();
			$('#stonepieces').val(1);
			setTimeout(function(){
				if(typeval == 4) { // gram wise
					$('#stonegram').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.01]]');
					$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
					$('#caratweight').attr('data-validation-engine','');
					$('#caratweight').removeClass('error');
					$('#caratweight').prop('readonly',true);
					$("span","#caratweightdiv label").remove();
					$('#stonegramdiv label').append('<span class="mandatoryfildclass">*</span>');
					$("#caratweightdiv").hide();
					$("#stonegramdiv").show();
					Materialize.updateTextFields();
				} else if(typeval == 2) {  // carat wise
					$('#caratweight').attr('data-validation-engine','validate[required,decval['+dia_weight_round+'],custom[number],min[0.01]]');
					$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
					$('#stonegram').attr('data-validation-engine','');
					$('#stonegram').removeClass('error');
					$('#stonegram').prop('readonly',true);
					$("span","#stonegramdiv label").remove();
					$('#caratweightdiv label').append('<span class="mandatoryfildclass">*</span>');
					$("#stonegramdiv").hide();
					$("#caratweightdiv").show();
					Materialize.updateTextFields();
				} else if(typeval == 3) {  // piece wise
					$('#caratweight,#stonegram').attr('data-validation-engine','');
					$('#caratweight,#stonegram').removeClass('error');
					$("span","#caratweightdiv label").remove();
					$('#caratweight').prop('readonly',true);
					$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
					$('#stonepiecesdiv label').append('<span class="mandatoryfildclass">*</span>');
					$("span","#stonegramdiv label").remove();
					$("#caratweightdiv").hide();
					$("#stonepiecesdiv").show();
					if(stonepiecescalc == '0') {
						$("#stonegramdiv").hide();
						$("span","#stonegramdiv label").remove();
						$('#stonegram').prop('readonly',true);
					} else if(stonepiecescalc == '1') {
						$('#stonegram').attr('data-validation-engine','validate[decval['+round+'],custom[number],min[0]]');
						$('#stonegram').prop('readonly',false);
						$("#stonegramdiv").show();
					}
				}
			},100);
		});
		$('#stonegram').change(function() {
			var gramvalue = $(this).val();
			var caratval = gramvalue * 5;
			$('#caratweight').val(parseFloat(caratval).toFixed(roundweight));
			stoneentrycal();
		});
		// Stone Carat,Total weight and Total Amount Calculation
		$('#purchaserate,#basicrate,#stonepieces,#caratweight').blur(function() {
			stoneentrycal();
		});
		//close popup of the Stone Entry
		$("#closestoneentryform").click(function() {
			$("#stoneoverlay").fadeOut();
		});
		//Save the Stone-Entry Form
		$("#savestoneentrydata").click(function() {
			var stocktype = $("#stocktypeid").find('option:selected').val();
			var datalength = $('#stoneentrygrid .gridcontent div.data-content div').length;
			if(datalength > 0) {
				// Based on netwtcalctype value,we will calculate the val of Net Wt
				var attrid = $(this).attr('id');
				getstonegridsummary();
				var netwtcalctype = $("#netwtcalctype").val();
				var totnetwt ='';
				var totstoneamt ='';
				var totcswt = $("#totcswt").html();
				var totpearlwt = $("#totpearlwt").html();
				var totdiawt = $("#totdiawt").html();
				if(netwtcalctype == 2) {
					totnetwt = (parseFloat(totcswt)+parseFloat(totpearlwt)).toFixed(round);
					totstoneamt = $("#totamt").html();
				} else if(netwtcalctype == 3) {
					totnetwt = parseFloat($("#totwt").html()).toFixed(round);
					totstoneamt = $("#totamt").html();
				} else if(netwtcalctype == 4) { // G.wt - D
					totnetwt = parseFloat(totdiawt).toFixed(round);
					totstoneamt = $("#totamt").html();
				} else if(netwtcalctype == 5) { // G.wt
					totnetwt = 0;
					totstoneamt = $("#totamt").html();
				} else if(netwtcalctype == 6) { // G.wt - C.S => Stone amount = Total stone amount - stone amount [only reduced stone type weight] 
					totnetwt = (parseFloat($("#totwt").html()) - parseFloat($("#totcswt").html())).toFixed(round);
					totstoneamt = $("#totamt").html();
				}
				var charge_json = {}; //declared as object			
				charge_json = getgridrowsdata("stoneentrygrid");
				var stonegridata = JSON.stringify(charge_json);
				stonejsonconvertdata(stonegridata);
				$('#hiddenstonedata').val(JSON.stringify(charge_json));	
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").val(parseFloat(totnetwt).toFixed(roundweight));
				$("#stoneweight").trigger('change');
				$("#stonecharge").val(totstoneamt).trigger('change');
				$("#stonecharge").attr('readonly', true);
				if(stocktype != 19) {
					totalgrossamountcalculationwork();
				}
				$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
				$("#stoneoverlay").fadeOut();
			} else {
				$('#hiddenstonedata').val('');
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").val(parseFloat(0).toFixed(roundweight));
				$("#stoneweight").trigger('change');
				$("#stonecharge").val(0).trigger('change');
				$("#stonecharge").attr('readonly', true);
				totalgrossamountcalculationwork();
				$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
				$("#stoneoverlay").fadeOut();
				alertpopup('No Stone entry added');
			}
		});
		$("#salestagstoneentrydelete").click(function() {
			var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
			var stocktypeid = $("#stocktypeid").find('option:selected').val();
			transactiontype = typeof transactiontype == 'undefined' ? '1' : transactiontype;
			stocktypeid = typeof stocktypeid == 'undefined' ? '1' : stocktypeid;
			var datarowid = $('#stoneentrygrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				if(transactiontype == 11 && (stocktypeid == 11 || stocktypeid == 12 || stocktypeid == 13)) {
					alertpopupdouble('You cannot delete Stone details in Sales!');
				} else {
					deletegriddatarow('stoneentrygrid',datarowid);
					getstonegridsummary();
					var charge_json = {}; //declared as object			
					charge_json = getgridrowsdata("stoneentrygrid");
					var x = $.parseJSON($('#hiddenstonedetail').val());
					delete x['rows'][datarowid];
					$('#hiddenstonedetail').val(JSON.stringify(x));
					$('#hiddenstonedata').val(JSON.stringify(charge_json));
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		//stone
		$("#stonedetailshide").hide();
		$("#stonedetailsshow").click(function() {
			$(this).hide();
			$("#stonedetailshide").show();
			$("#stonedetails").show();
		});
		$("#stonedetailshide").click(function() {
			$(this).hide();
			$("#stonedetailsshow").show();
			$("#stonedetails").hide();
		});
	}
	{ // category tree - tree concept field
		$('#dl-promenucategory').dlmenu({
			animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
		// parent category select
		$('#prodcategorylistuldata li').click(function() {
			var name = $(this).attr('data-listname');
			var categoryid =$.trim($(this).attr('data-listid')); 
			var level = parseInt($(this).attr('data-level'));
			$('#categorynamehidden').val(name);
			if(categoryid != '') {
				$('#category').val(categoryid).trigger('change');
				$('#treecategoryname').val(name);
			} else {
				$('#category').val('');
				$('#treecategoryname').val('');
			}
		});
	}
	{ // Employee Person ID change
		if(transactionemployeeperson == '0') {
			$('#employeepersonid').on('change', function() {
				var ids = $(this).val();
				$('#multipleemployeename').val('');
				var arrayvalue = new Array();
				for(var i=0; i<ids.length ; i++) {
					var empname = $('#employeepersonid option').filter(function() {
						return this.value == ids[i];
					}).data('employeepersonidhidden');
					arrayvalue.push(empname);
				}
				$('#multipleemployeename').val(arrayvalue);
			});
		}
	}
	{//enter key events
		/* $('#grossweight,#stoneweight,#netweight,#pieces,#salescomment,#ratepergram,#melting,#touch,#referenceid').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			setTimeout(function() {
				var salesdetailid = $("#salesdetailid").val();
					if(salesdetailid) {
						$('#salesdetailupdate').trigger('click');
					} else {
						$('#salesdetailsubmit').trigger('click');
					}
				
			},500);
		}
		}); */
		//payment enter key event
		/* $('#paymenttotalamount,#chitbookno,#chitbookname,#chitamount,#totalpaidamount,#bankname,#paymentrefnumber,#paymentgrossweight,#payemntpurewtpayment,#newpaymenttouch,#paymentmelting,#paymentpureweight,#paymentratepergram,#approvalcode').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				setTimeout(function() {
					var salesdetailid = $("#salesdetailid").val();
					if(salesdetailid) {
						$('#paymentdetailupdate').trigger('click');
					} else {
						$('#paymentdetailsubmit').trigger('click');
					}
					
				},500);
			}
		}); */
	}
	$("#wastageclone,#makingchargeclone,#flatchargeclone,.chargedata,#makingcharge,#sumarydiscountamount,#discount,#wastageless,#lstchargeclone,#repairchargeclone").keypress(function(e){
		var keyCode = e.which;
		var start = e.target.selectionStart;
		var end = e.target.selectionEnd;
		if($(this).val().substring(start, end) == '') {
			// Not allow special 
			if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
				var newValue = this.value;
				if(keyCode != 8 && keyCode != 0) {
					if (hasDecimalPlace(newValue, round)) {
						e.preventDefault();
					}
				}
			} else {
				e.preventDefault();
			}
		} else {
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
			} else {
				e.preventDefault();
			}
		}
	});
	$("#orderadvamtsummary").keypress(function(e){
		 var keyCode = e.which;
		 var start = e.target.selectionStart;
		 var end = e.target.selectionEnd;
		 if($(this).val().substring(start, end) == '') {
			 // Not allow special 
			  if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
				  var newValue = this.value;
					if(keyCode != 8 && keyCode != 0){
						if (hasDecimalPlace(newValue, roundamount)) {
							e.preventDefault();
						}
					}		
			 }else{
				 e.preventDefault();
			 }
		 }else{
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
			}else{
				e.preventDefault();
			}
		 }
	});
	$("#roundvalue").keypress(function(e){
		 var keyCode = e.which;
		 var start = e.target.selectionStart;
		 var end = e.target.selectionEnd;
		 if($(this).val().substring(start, end) == '') {
			 // Not allow special 
			  if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
				  var newValue = this.value;
					if(keyCode != 8 && keyCode != 0){
						if (hasDecimalPlace(newValue, round)) {
							e.preventDefault();
						}else{adjustmentamountkeypressed = 1;}
					}		
			 }else{
				 e.preventDefault();
			 }
			 
		 }else{
			  
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
			adjustmentamountkeypressed = 1;
			}else{
				e.preventDefault();
			}
		 }
	});
	{//date
		if((salesdatechangeablefor == 2) && (loggedinuserrole == 2) )// Only Admin can modify sales date
		{
			// Admin Role
			mindate = null;
			maxdate = 0;
		} else { //Other Roles
			mindate =0;
			maxdate =0;
		}
		//for date picker
		$('#salesdate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate:mindate,
				maxDate:maxdate,
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
						//$("#billdate").text(checkdate);
					}
				},
				onClose: function () {
					$('#salesdate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});
		$('#journalbilldate').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate:mindate,
			maxDate:maxdate,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#journalbilldate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		$('#dateofstock').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate:null,
			maxDate:null,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$("#dateofstock").text(checkdate);
				}
			},
			onClose: function () {
				$('#dateofstock').focus();
			}
		}).click(function() {
				$(this).datetimepicker('show');
		});
		$('#dateofstockto').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate:null,
			maxDate:null,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$("#dateofstockto").text(checkdate);
				}
			},
			onClose: function () {
				$('#dateofstockto').focus();
			}
		}).click(function() {
				$(this).datetimepicker('show');
		});
		//for date picker
		$('#balanceduedate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate:0,
				//maxDate:'+1970/01/2',
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#balanceduedate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});
		//for order-due-date picker
		$('#orderduedate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate:0,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#orderduedate').focus();
				}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		// For vendor person - Max due date
		$('#vendororderduedate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate:0,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#vendororderduedate').focus();
				}
		}).click(function() {
			$(this).datetimepicker('show');
		});
        $('#ledgerfromdate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate: null, // display all date
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#ledgerfromdate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});
        $('#ledgertodate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate: null, // display all date
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#ledgertodate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});		
	
		$('#paymentreferencedate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,		
		maxDate:'+1970/01/2',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#paymentreferencedate').focus();
			}
		}).click(function() {
				$(this).datetimepicker('show');
		});
		
			
		$('#expiredate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,
		minDate:'0',
		maxDate:'+1970/01/2',
		yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#expiredate').focus();
			}
		}).click(function() {
				$(this).datetimepicker('show');
		});
	}
	{ //Sales detail grid Image Click code
		$(document).on("click",".tagimageoverlaypreview",function() {
			var imgvalue =  $(this).find('a').attr('href');
			if (imgvalue != "") {
				$('#salesdetailimageoverlaypreview').fadeIn();
				$('#salesdetailtagimagepreview').empty();
				var imagearray = imgvalue.split(",");
				for (i=0;i<imagearray.length;i++) {
					var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
					img.attr('src', base_url+imagearray[i]);
					img.appendTo('#salesdetailtagimagepreview');
				}
			} else {
				alertpopupdouble('No Image to preview');
			}
		});
		//Image preview close icon
		$(document).on("click","#salesdetailimagepreviewclose",function() {
			$('#salesdetailtagimagepreview').empty();
			$('#salesdetailimageoverlaypreview').fadeOut();
		});
	}
	{//sales outertoolbar events 
		//Add
		$("#addicon").click(function() {
			$('#processoverlay').show();
			addsalesstatus = 0;
			salesdetailcreatestatus = 0; 
			retrievetagstatus = 0;
			approvalnoeditstatus = 0;
			salesdetailentryeditstatus = 0;
			addslideup('salesgriddisplay','salesaddformdiv');
			// Metal checking datas
			metalarraydetails = '';
			billlevelrestrictmetalid = 1;
			metalstatus = 0;
			metaloldarraydetails = '';
			billlevelrestrictoldmetalid = 1;
			metaloldstatus = 0;
			metalreturnarraydetails = '';
			billlevelrestrictreturnmetalid = 1;
			metalreturnstatus = 0;
			// Metal Checking data closed
			salesfieldcommentrequired = 0;
			$("#notificationbadg").text();
			$('.billnetamountspan,.oradvamtspan,.billbalamountspan,.billnetwtspan,.sumbillnetwtspan').text('');
			$('#estimationnumber').attr('readonly',false);
			$('#itemtagnumber,#rfidtagnumber').prop('readonly','');
			$("#billdate,#sperson,#account,#transtype,.iropenamountspan,.iropenwtspan").text('');
			$("#salesaddformheader :input").prop("disabled", false);
			$("#paymentform :input").prop('disabled', false);
			$("#salescreateform :input").attr("readonly", false);
			$("#lottypeid,#purchasemode").prop('readonly','');
			$('#cashcounter').prop('disabled',true);
			//$('#salesdate').prop('disabled',false);
			$("#accountid").prop('disabled',false);
			$('#issuereceipticon').show();
			$('#creditno').css('width','80%');
			$('#orderadvamtsummary-div').hide();
			$('#addaccount,#accountsearchevent,#editaccount,#salesdetailentrydelete').show();
			$('#creditentryedit,#creditentrydelete,#issuereceiptpush,#creditentryadd').show();
			$("#ordermodeid,#ordernumber,#stocktypeid,#sumarytaxamount,#sumarydiscountamount,#paymentstocktypeid,#paymentproduct,#paymentpurity,#paymentcounter,#ordernumber,#transactionmodeid,#loadbilltype,#ratecuttypeid,#category").prop('disabled',false);
			$('#sumarydiscountamount,#roundvalue').attr('readonly',false);
			if(paymentsavebutton == 1) {
				$('#paymentdetailsubmit').hide();
			} else {
				$('#paymentdetailsubmit').show();
			}
			$('#salesdetailupdate,#paymentdetailupdate,#salesdetailsubmit,#paymentdetailedview').hide();
			$("#estimatearray,#summarytaxgriddata,#issuereceiptid,#wtissuereceiptid,#approvaloutreturnproductid,#approvaloutreturntagno,#approvalouttagnumberarray,#approvaloutrfidnumberarray,#paymentcomment,#salescomment,#orderadvcreditno,#creditoverlayhidden,#creditadjusthidden,#creditoverlayvaluehidden,#creditadjustvaluehidden,#creditoverlaystatus,#tagimage,#multipleemployeeid,#multipleemployeename").val('');
			estimatearray = []; //Empty array variable (for estimate scanning)
			clearform('salescreateform');
			discountoperation = 0; // for discount in billwise
             //set defaults 
			$("#accountid").select2('focus');
			if(mainsavestatus == 0) {
				$("#salestransactiontypeid").select2('val',transactiontype).trigger('change');
			}
			$("#issuereceipttypeid").select2('val',3);
			$("#salespersonid").select2('val',$('#hiddenemployeeid').val()).trigger('change');
			$('#employeename').val($('#hiddenemployeename').val());
			$('#employeeid').val($('#hiddenemployeeid').val());
			getcurrentsytemdate("salesdate");				
			$("#sumarydiscountamount,#creditfinalamt").val(0);
			discountamtwithsign = 0;
			discountoldamtwithsign = 0;
			//$("#itemdiscountcalname").text('');
			$("#itemdiscountcalname").text('(0)');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formclearicon,#stonedetailsshow,#salesdetailedit,#salesdetaildelete,#paymentdelete,#paymentedit").show();
			//$("#stonedetailsshow,#salesdetailedit,#salesdetaildelete,#paymentdelete,#paymentedit").show();
			$("#employeename").val($('#hiddenemployeename').val());
			$("#employeeid").val($('#hiddenemployeeid').val());
			$("#generatesalesid,#generatesalesnumber,#mainordernumber,#orderadvanceno,#mainordernumberid,#balancedescription,#billbasedproductid,#taxgriddata,#editinnerrowid,#issuereceiptmergetext,#untagvalidatemergetext,#untagvalidatecountermergetext").val('');
			$('#tagimagedisplay').empty();
			$("#creditno").attr('readonly',false);
			$("#createnewaccount").trigger("click");
			var date = $("#salesdate").val();
			//$("#billdate").text(date);
			// set default value from company setting
			//$("#transactionmodeid").select2('val',2).trigger('change');
			//opacity reset
			$(".itemdetailsopacity").css('opacity',1);
	    	$("#salesdetailcancel").hide(); // Cancel Button SHOW / HIDE
			cleargriddata('stoneentrygrid');
			cleargriddata('saledetailsgrid');
			$('#taxcategory').select2('val','').trigger('change');
			retrievealltaxdetailinformation();
			$("#spanaccopenamt,#spanaccopenwt,#spanbillclosewt,#spanbillcloseamt,#spanbillwt,#spanbillamt,#sumoldjewels,#sumsalesreturn,#sumbillamount,#spanaccclosewt,#spanacccloseamt,#spanpaidpurewt,#spanissuepurewt,#spanpaidgrswt,#spanissuegrswt,#accountadvamt,#accountcreditamt").text(0);
			$("#accountopeningamount,#accountopeningweight,#wastagelesspercent,#sumarygrossamount,#sumarypendingamount,#sumarynetamount,#sumarybillamount,#sumarysalesretamount,#sumaryoldjewelamount,#sumarytaxamount,#sumarydiscountamount,#finalsumarybillamount,#sumarypaidamount,#billgrossamount,#billnetamount,#oldjewelamtsummary,#prewastage,#premakingcharge,#preflatcharge,#prelstcharge,#purchasereturnsummary,#salesreturnsummary,#orderadvamtsummary,#orderadvamtsummaryhidden,#paymenttotalamounthidden,#billweighthidden,#spanpaidpurewthidden,#prepaymentpureweight,#paymentpureweight,#balpurewthidden,#prepaymenttotalwt,#editpaymentpureweight,#oldjewelwtsummary,#purchasereturnwtsummary,#salesreturnwtsummary,#billnetwt,#billgrosswt,#salesdetailcount,#liveadvanceclose,#orderadvadjustamthidden,#prebillnetamount,#sumaryissueamount,#accountopeningamountirid,#accountopeningweightirid,#finalamthidden,#finalwthidden,#purchasecheckgrwt,#spanpaidgrswthidden,#spanissuegrswthidden,#billforgrosswt,#billfornetwt").val(0);
			$("#discountpercent,#singlediscounttotal,#giftnumber,#giftremark,#giftdenomination,#summarysalesreturnroundvalue,#prerepaircharge,#itemwastagevalue,#itemmakingvalue,#itemflatvalue,#itemlstvalue,#itemrepairvalue,#itemchargesamtvalue").val('0');
			$("#sumgwt,#sumwastage,#sumgross,#sumswt,#summaking,#sumtax,#sumnwt,#sumcharge,#sumdiscount,#smpieces,#sumstone,#sumnetamount,#sumpendingamt,#sumpaidamt,#sumpurewt,#sumpurewttoamt,#sumamttopurewt,#paycash,#paycheque,#paycard,#paydd,#payadvamt,#paychit,#payvoucher,#payamtpurewt,#paypurewtamt,#paypurewt,#oldgrosswt,#oldstonewt,#olddustwt,#oldnetwt,#oldpieces,#oldwastageless,#oldgrossamt,#oldtaxamt,#oldnetamt,#oldpurewt,#srgrosswt,#srstonewt,#srnetwt,#srpieces,#srcharges,#srstoneamt,#srgrossamt,#srtaxamt,#srdiscount,#srnetamt,#srpurewt,#sumbilltax,#sumbilldiscount,#sumflatcharge,#sumissueamt,#billforpieces").text(0);
			retrievestatus = 0;
			purityshowhide('purity','');
			$("#s2id_accountid a span:first").text('');
			$("#salestransactiontypeid,#accountid,#salespersonid,#paymentirtypeid,#journalbillno").prop('disabled',false);
			$("#modeid").attr('readonly',false);
			$("#cashcounter").select2('val',$('#logincashcounterid').val()).trigger('change');
			$('#prodcategorylistbutton').prop('disabled',false);
			compybasednetmccalc();
			getcurrentrate();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
			billeditstatus = 0;
			billeditstatusclose = 0;
			mainprintemplateid = 0;
			salesandoldstatus = 0;
			salesstatus = 0;
			salesoldstatus = 0;
			oldstatus = 0;
			onlyoldstatus = 0;
			returnstatus = 0;
			setzero(['roundvalue']);
			//$('#roundvalue').prop('readonly','readonly');
			estimatestatus = 0;
			receivestatus = 0;
			placeorderstatus = 0;
			takeorderoldstatus = 0;
			takeorderreceivestatus = 0;
			takeorderstatus = 0;
			onlytakeorderstatus = 0;
			$("#ordernumber,#ordermodeid").removeAttr("disabled");
			onlytakeorderadvancestatus = 0;
			//$("#product,#purity").prop('disabled',false);
			editpaymentstatus = 0;
			$('#prepaymenttotalamount').val(0);
			wastagevisible = 0;
			touchvisible = 0;
			calculationdone = 0;
			oldjewelstatus = 1;
			salesreturnstatus = 1;
			$('.billamountdiv').show();
			Materialize.updateTextFields();
			$('#processoverlay').hide();
			$('#mainheaderoverlay').show();
		});
		//Edit
		$("#mainheadersubmit").click(function() {
			var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
			transactiontype = typeof transactiontype == 'undefined' ? '1' : transactiontype;
			if(transactiontype == 13 || transactiontype == 24) {
				$('#accountinformation').text('');
			} else {
				displayaccountdetails();
			}
			if(transactiontype == 11) {
				$('#accountpaidinfo').show();
			}
			$('#mainheaderoverlay').hide();
		});
		$("#mainheaderclose").click(function() {
			$('#closeaddform').trigger('click');
		});
		$("#editicon").click(function(){
			$("#processoverlay").show();
			approvalnoeditstatus = 1;
			discountoperation = 1; //for edit operation
			salesdetailentryeditstatus = 0;
			resetFields();
			resettransactionfields();
			$("#employeename").val($("#hiddenemployeename").val()) //While edit assign employee name
            var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var statusid = getgridcolvalue('salesgrid',datarowid,'statusname','');
				if(statusid == 'Cancel'){
					$("#processoverlay").hide();
					alertpopup("Unable to edit this bill.because this bill is already Cancelled!");
				} else {
					$.ajax({
						url:base_url+"Sales/checkeditsales",
						data: "salesid=" +datarowid+"&povendormanagement="+povendormanagement,
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['rows'] > 0) {
								$("#processoverlay").hide();
								if(data['id'] == 13) {
									alertpopup("Can't edit weight sales entry");
								} else if(data['id'] == 18) {
									alertpopup("Can't edit approval out entry");
								} else if(data['id'] == 11) {
									alertpopup("Can't edit this invoice.Bill Used in payment receipt/issue");
								} else if(data['id'] == 'return11') {
									alertpopup("Can't edit the invoice. Return item is processed.!");
								} else if(data['id'] == 9) {
									alertpopup("Can't edit this purchase bill bcoz this data based  stock entry created");
								} else if(data['id'] == 20 && data['cancelstatus'] == 1) {
									alertpopup("Can't edit order.This order items all are cancelled");
								} else if(data['id'] == 20) {
									alertpopup("Can't edit order.This order already used in either place order or delivery order");
								} else if(data['id'] == 'acp21') {
									alertpopup("Place Order has been accepted by vendor. So can't edit Place Order.");
								} else if(data['id'] == 'delpo27') {
									alertpopup("Vendor has delivered the items. So can't edit Generate Place Order");
								} else if(data['id'] == 21) {
									alertpopup("Can't edit order.This order already used in receive order");
								} else if(data['id'] == 28 || data['id'] == 29 || data['id'] == 30) {
									alertpopup("Can't edit Repair details. This order already used in either Place Repair or delivery repair");
								}
							} else {
								$("#processoverlay").show();
								operationstone='ADD';
								discountamtwithsign = 0;
								discountoldamtwithsign = 0;
								//opacity reset
								$('.itemdetailsopacity').css('opacity',1);
								$('#salesdetailedit,#salesdetaildelete').show();
								if(paymentsavebutton == 1) {
									$('#paymentdetailsubmit').hide();
								} else {
									$('#paymentdetailsubmit').show();
								}
								$('#formclearicon').hide();
								$('#orderadvamtsummary-div').hide();
								billdataretrive(datarowid);
								var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
								var stocktypeid=$('#stocktypeid').find('option:selected').val();
								addslideup('salesgriddisplay', 'salesaddformdiv');
								$("#salestransactiontypeid,#accountid,#transactionmodeid,#paymentirtypeid,#journalbillno,#cashcounter,#loadbilltype,#ratecuttypeid,#salesdate").prop('disabled',true);
								$('#addaccount,#accountsearchevent').hide();
								stoneentrygrid();
								$("#ordermodeid,#ordernumber").prop('disabled',true);
								$("#sumarytaxamount,#sumarydiscountamount,#paymentproduct,#paymentpurity,#paymentcounter,#ordernumber,#category").prop('disabled',false);
								$('#sumarydiscountamount,#roundvalue').attr('readonly',false);
								$("#itemtagnumber,#rfidtagnumber").prop('readonly','');
								if(salestransactiontypeid != 18) {
									$("#stocktypeid").prop('disabled',false);
								}
								$('#paymentstocktypeid').prop('disabled',false);
								$('#estimationnumber').attr('readonly',false);
								if(salestransactiontypeid == 18) {
									$('#salesdetailentrydelete').show();
								}else if(salestransactiontypeid == 21) {
									$('#salesdetailentryedit').hide();
									$('#salesdetailentrydelete').show();
								}else{
									$('#salesdetailentryedit,#salesdetailentrydelete').show();
								}
								$("#creditno").attr('readonly',true);
								$('#employeepersonid').select2('val',$('#hiddenemployeeid').val());
								var salesdetailidgridarray = getgridcolcolumnvalue('saledetailsgrid','','salesdetailid');
								$('#approvaloutreturnproductid').val(salesdetailidgridarray);
								if(salestransactiontypeid == 18){
									var itemtagnumberarray = getgridcolcolumnvalue('saledetailsgrid','','itemtagnumber');
									$('#approvalouttagnumberarray').val(itemtagnumberarray);
									var rfidnumberarray = getgridcolcolumnvalue('saledetailsgrid','','rfidtagnumber');
									$('#approvaloutrfidnumberarray').val(rfidnumberarray);
								}
								//For Keyboard Shortcut Variables
								addformupdate = 1;
								viewgridview = 0;
								addformview = 1;
								Operation = 1; //for pagination
								accounticonhideshow();
								finalsummarycalc(3);
								if(salestransactiontypeid == 18 && stocktypeid == 62) {
									$("#accountid").prop('disabled',false);
								} 
								/* if(salestransactiontypeid == 25) {
									$("#accountid").trigger('change');
								} */
								{// ratecut pi pr
									Materialize.updateTextFields();
									var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
									if((salestransactiontypeid == 22 && ratecuttypeid == 2) || (salestransactiontypeid == 23 && ratecuttypeid == 2)){
										$('div.gridcontent div.data-rows').removeClass('active');
										$('div.gridcontent #1').addClass('active');
										$("#salesdetailentryedit").trigger('click');
									}
								}
								{
									var paymentstocktypeid = getgridcolcolumnvalue('saledetailsgrid','','paymentstocktypeid');
									var purewttoamtcheck = '38';
									var amttopurewtcheck = '37';
									if(jQuery.inArray( purewttoamtcheck, paymentstocktypeid ) != '-1') { //check purewttoamt stocktype used or not.
										var ret_transactionmodeid = $('#transactionmodeid').find('option:selected').val();
										if(ret_transactionmodeid == '3') {
											$('#amountsummary').show();
										}
									}
									if(jQuery.inArray( amttopurewtcheck, paymentstocktypeid ) != '-1') { //check purewttoamt stocktype used or not.
										var ret_transactionmodeid = $('#transactionmodeid').find('option:selected').val();
										if(ret_transactionmodeid == '2') {
											$('#pureweightsummary').show();
										}
									}
								}
								$("#processoverlay").hide();
							}
						}
					});
				}
			} else {
				$("#processoverlay").hide();
				alertpopup(selectrowalert);
			}			
			Materialize.updateTextFields();
		});
		//Delete
		$("#deleteicon").click(function() {
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$.ajax({
					url:base_url+"Sales/checkdeletedata",
					data: "salesid=" +datarowid+"&povendormanagement="+povendormanagement,
					type: "POST",
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data['rows'] > 0) {
							if(data['id'] == 20) {
								alertpopup("Can't delete order.This order already used in either place order or delivery order");
							} else if(data['id'] == 28 || data['id'] == 29 || data['id'] == 30) {
								alertpopup("Can't delete Repair details. This entry already used in either place repair or delivery repair");
							} else if(data['id'] == 21) {
								alertpopup("Can't delete order.This order already used in receive order");
							} else if(data['id'] == 9) {
								alertpopup("Can't delete Purchase.This data based stock created");
							} else if(data['id'] == 18) {
								alertpopup("Can't delete Approval Out. This data based Approval Out Return created");
							} else if(data['id'] == 27) {
								alertpopup("Can't delete Generated Place Order. Those items were dispatched.");
							} else if(data['id'] == 11) {
								alertpopup("Can't able to delete. Return Item is processed.!");
							}
						} else {
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						}
					}
				});
			} else {
				alertpopup(selectrowalert);
			}	
		});
		// Push to pending delivery button
		$("#p2pdelvicon").click(function(){
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			resetFields();
			$('#currentstatusid,#newstatusid').val('');
			$('#tocounter').select2('val','');
			if(datarowid){
				$.ajax({
						url:base_url+"Sales/checktransactiontypedata",
						data:{salesid:datarowid,type:11},
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['status'] == 0){
								alertpopup("This action only apply for Sales Transaction type");
							}else 
							{
								loaditemdetail(datarowid);
								$("#p2pdeliveryoverlay").fadeIn();
							}
						}
					});
			}else {
				alertpopup(selectrowalert);
			}	
		});
		// close p2p delivery overlay
		$('#closep2pdeliveryoverlay').click(function(){
			$("#p2pdeliveryoverlay").fadeOut();
		});
		// itemdetail change in p2p overlay
		$("#itemdetail").change(function(){
			var deliverystatusname = $('#itemdetail').find('option:selected').data('deliverystatusname');
			var deliverystatusid = $('#itemdetail').find('option:selected').data('deliverystatusid');
			$('#currentstatus').val(deliverystatusname);
			$('#currentstatusid').val(deliverystatusid);
			if(deliverystatusid == 4 ){
				$('#newstatus').val('Delivery Pending');
				$('#newstatusid').val(2);
				$("#tocounter").attr('class','validate[required]');
				$("#deliveryrfidtagno").attr('class','validate[required,funcCall[rfitagnocheck]]');
				$('.deliveryhidediv').show();
			}else if(deliverystatusid == 2){
				$('#newstatus').val('Delivered');
				$('#newstatusid').val(4);
				$("#tocounter,#deliveryrfidtagno").attr('class','');
				$('.deliveryhidediv').hide();
			}
			Materialize.updateTextFields();
		});
		
		//push to pending submit button
		$('#addp2pbtn').click(function() {
			$("#p2pdeliveryformvalidate").validationEngine('validate');	
		});
		jQuery("#p2pdeliveryformvalidate").validationEngine( {
			onSuccess: function() {
				pushtopendingdeliverycreate();
			},
			onFailure: function() {	
				//alertpopup(validationalert);
			}
		});
		// Push to delivery button
		$("#p2ddelvicon").click(function(){
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			resetFields();
			if(datarowid){
				$.ajax({
						url:base_url+"Sales/checktransactiontypedata",
						data:{salesid:datarowid,type:11},
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['status'] == 0){
								alertpopup("This action only apply for Sales Transaction type");
							}else {
								$('#deliverystatusid').select2('val','');
								$("#p2ddeliveryoverlay").fadeIn();
							}
						}
					});
			}else {
				alertpopup(selectrowalert);
			}
		});
		// close p2d delivery overlay
		$('#closep2ddeliveryoverlay').click(function(){
			$("#p2ddeliveryoverlay").fadeOut();
		});
		// Process dd change in p2d overlay
		$("#process").change(function(){
			var value = $('#process').find('option:selected').val();
			$('#deliverystatusid,#pendingitemdetail').select2('val','');
			if(value != '') {
				if(value == 1) { // push to pending
					$("#deliverystatusid").attr('class','validate[required]');
					$("#deliverycomment").attr('class','materialize-textarea validate[required,maxSize[200]]');
					$('.deliverystatushidediv').show();
				}else if(value == 2) { // push to delivery
					$("#deliverystatusid,#deliverycomment").attr('class','');
					$('.deliverystatushidediv').hide();
				}
				loaditemtag(value);
			}
		});	
		//push to delivery submit button
		$('#addp2deliverybtn').click(function() {
			$("#p2ddeliveryformvalidate").validationEngine('validate');	
		});
		jQuery("#p2ddeliveryformvalidate").validationEngine( {
			onSuccess: function() {
				pushtodeliveryupdate();
			},
			onFailure: function() {	
				//alertpopup(validationalert);
			}
		});
		{ /** Start --- Order Form with grid overlay**/
			//for date picker
			$('#orderitemdate').datetimepicker({
					dateFormat: 'dd-mm-yy',
					showTimepicker :false,
					minDate:0,
					maxDate:0,
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$(this).next('span').remove('.btmerrmsg'); 
							$(this).removeClass('error');
							$(this).prev('div').remove();
						}
					},
					onClose: function () {
						$('#orderitemdate').focus();
					}
			}).click(function() {
					$(this).datetimepicker('show');
			});
			// change Order number grid load
			$("#poordernumberid").click(function() {
				var orderid = $(this).val();
				$('#posalesid').val(orderid);
				orderitemsgrid();
			});
			// Place Order Status overlay form
			$("#placeorderstatusicon").click(function() {
				var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
				var snumber = $('#salesgrid div.gridcontent div.active ul li.salesnumber-class').text();
				if(datarowid){
					$.ajax({
						url:base_url+"Sales/checktransactiontypedata",
						data:{salesid:datarowid,type:21},
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['status'] != '2'){
								alertpopup("This action only applicable for Place Order");
							} else {
								$('.headeraction-menu,.off-canvas-wrap').hide();
								addslideup('salesgriddisplay','salessettingsview');
								$('.ftaborderitemsoverlay').addClass('active');
								var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
								$('#posalesid').val(datarowid);
								$('#ordertransactionpage').val('placeorder');
								orderitemsgrid();
								loadplacenumbersbasedvendor(datarowid);
								getcurrentsytemdate("orderitemdate");
								$('#orderitemsstatusacc').select2('val',0);
								$('#poordernumberid').select2('val',datarowid).trigger('change');
								$("#orderitemdate").prop('disabled',true);
								$("#poordernumberid,#orderitemsstatusacc,#orderitemsstatuscomment,#adminorderitemsstatuscomment").prop('disabled',false);
								$('#adminorderitemstatuscomment-div').addClass('hidedisplay');
								$("#adminorderitemsstatuscomment").attr('data-validation-engine','');
								var findpostatus = $('#poordernumberid').find('option:selected').data('poorderstatusid');
								var ordertransactionpage = $('#ordertransactionpage').val();
								if(ordertransactionpage == 'placeorder') {
									if(findpostatus == 23) {
										if(userroleid != '3') {
											$('#adminorderitemstatuscomment-div').removeClass('hidedisplay');
											$("#adminorderitemsstatuscomment").attr('data-validation-engine','validate[required,maxSize[200]]');
											$('#orderitemsstatuscomment').val($('#poordernumberid').find('option:selected').data('postatuscomment'));
											$('#orderitemsstatusacc').select2('val',2);
											$("#poordernumberid,#orderitemsstatusacc,#orderitemsstatuscomment").prop('disabled',true);
											$("#adminorderitemsstatuscomment").prop('disabled',false);
										} else if(userroleid == 3) {
											$('#adminorderitemstatuscomment-div').removeClass('hidedisplay');
											$('#adminorderitemsstatuscomment').val($('#poordernumberid').find('option:selected').data('poadminstatuscomment'));
											$("#adminorderitemsstatuscomment").attr('data-validation-engine','').prop('disabled',true);
											$("#poordernumberid").prop('disabled',true);
										}
									} else {
										$('#adminorderitemstatuscomment-div').addClass('hidedisplay');
										$("#adminorderitemstatuscomment").attr('data-validation-engine','');
										$("#poordernumberid,#orderitemsstatusacc,#orderitemsstatuscomment,#adminorderitemsstatuscomment").prop('disabled',false);
									}
								}
								Materialize.updateTextFields();
							}
						}
					});
				}else {
					alertpopup(selectrowalert);
				}
			});
			$('#orderitemsoverlayclose').click(function() {
				resetFields();
				$('#posalesid,#poaccountid,#poaccountname,#ordertransactionpage').val('');
				addslideup('salessettingsview','salesgriddisplay');
				$('.headeraction-menu,.off-canvas-wrap').show();
				salesrefreshgrid();
			});
			$('#updateorderitemsbtn').click(function() {
				$("#orderitemsvalidate").validationEngine('validate');	
			});
			jQuery("#orderitemsvalidate").validationEngine({
				onSuccess: function() {
					var salesdetailid = getselectedrowids('orderitemsgrid');
					if(salesdetailid != '') {
						orderstatusoverlaysave();
					} else {
						alertpopup('Please select the product(s)');
					}
				},
				onFailure: function() {	
					//alertpopup(validationalert);
				}
			});
		} /** End --- Order Form with grid overlay**/
		{ // After rejected Place Order by vendor. Again admin person will assign new vendor with new due date for vendor
			$('#newvendorduedate').datetimepicker({
					dateFormat: 'dd-mm-yy',
					showTimepicker :false,
					minDate:0,
					maxDate:null,
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$(this).next('span').remove('.btmerrmsg'); 
							$(this).removeClass('error');
							$(this).prev('div').remove();
						}
					},
					onClose: function () {
						$('#newvendorduedate').focus();
					}
			}).click(function() {
					$(this).datetimepicker('show');
			});
			$('#assignnewvendoricon').click(function() {
				var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
				if(datarowid) {
					$.ajax({
						url:base_url+"Sales/checkplaceorderisrejected",
						data:{salesid:datarowid,type:21,gporder:27},
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['status'] == 1 || data['status'] == 2) {
								$('#reassigningvendoroverlay').fadeIn();
								$('#newvendoraccountid').select2('val','');
								getcurrentsytemdate("newvendorduedate");
								$('#transactiontypepage').val(data['status']);
								retrieverejectedproductdetails(datarowid,data['status']);
							} else if(data['status'] == 0) {
								alertpopup('Place Order / Generate Place Order is in Active Status');
							} else {
								alertpopup('Please select Place Order / Generate Place Order rejected records!');
							}
						}
					});
				} else {
					alertpopup(selectrowalert);
				}
			});
			// Overlay Cancel Click
			$('#reassigningvendorcancel').click(function() {
				$('#salesdetailproductid,#newvendoraccountid').select2('val','').trigger('change');
				$('#reassigningvendoroverlay').fadeOut();
			});
			// To show vendor and product details.
			$('#salesdetailproductid').change(function(){
				var salesdetailsid = $('#salesdetailproductid').find('option:selected').val();
				var transactionpage = $('#salesdetailproductid').find('option:selected').data('status');
				var defvendorid = $('#salesdetailproductid').find('option:selected').data('ordervendorid');
				var vendorduedate = $('#salesdetailproductid').find('option:selected').data('vendororderduedate');
				$('#newvendoraccountid').select2('val',defvendorid).trigger('change');
				$('#newvendorduedate').val(vendorduedate);
				if(transactionpage == 1) {
					$('#newvendoraccountid').prop('disabled',false);
				} else if(transactionpage == 2) {
					$('#newvendoraccountid').prop('disabled',true);
				}
				if(salesdetailsid) {
					retrieverejectedproductimagedetails(salesdetailsid);
				}
			});
			// Submit Call to update vendor details
			$('#reassigningvendorsave').click(function() {
				$("#reassigningvendorformvalidate").validationEngine('validate');	
			});
			jQuery("#reassigningvendorformvalidate").validationEngine({
				onSuccess: function() {
					updaterejectedproductdetails();
				},
				onFailure: function() {
				}
			});
		}
		{ /** Start --- Order Form with grid overlay - Generated place order accepted/rejected by customer side**/
			if(userroleid != '3') {
				$("#deliverplaceordericon").click(function() {
					var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						$.ajax({
							url:base_url+"Sales/checktransactiontypedata",
							data:{salesid:datarowid,type:27},
							type: "POST",
							dataType:'json',
							async:false,
							cache:false,
							success: function(data) {
								if(data['status'] != '3') {
									alertpopup("This action only apply for Generate Place Order.");
								} else {
									addslideup('salesgriddisplay','salessettingsview');
									$('.ftaborderitemsoverlay').addClass('active');
									var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
									$('#posalesid').val(datarowid);
									$('#ordertransactionpage').val('generatepo');
									orderitemsgrid();
									$('#adminorderitemstatuscomment-div').addClass('hidedisplay');
									$("#adminorderitemstatuscomment").attr('data-validation-engine','');
									$('.headeraction-menu,.off-canvas-wrap').hide();
									loadplacenumbersbasedvendor(datarowid);
									getcurrentsytemdate("orderitemdate");
									$('#orderitemsstatusacc').select2('val',0);
									$('#poordernumberid').select2('val',datarowid).trigger('change');
									$("#orderitemdate").prop('disabled',true);
									Materialize.updateTextFields();
								}
							}
						});
					}else {
						alertpopup(selectrowalert);
					}
				});
			}
		}/** End --- Order Form with grid overlay - Generated place order accepted/rejected by customer side**/
		// Close Place Order status overlay 
		$('#postatusoverlaycancel').click(function() {
			$("#postatusoverlayform").fadeOut();
		});
		// Place order status overlay submit action
		$('#postatusoverlaysave').click(function() {
			$("#postatusformvalidate").validationEngine('validate');	
		});
		jQuery("#postatusformvalidate").validationEngine( {
			onSuccess: function() {
				postatusoverlaysave();
			},
			onFailure: function() {	
				//alertpopup(validationalert);
			}
		});
		{// Delivery Place Order items via Mode
			//for date picker
			$('#deliverypodate').datetimepicker({
					dateFormat: 'dd-mm-yy',
					showTimepicker :false,
					minDate:0,
					maxDate:null,
					changeMonth: true,
					changeYear: true,
					showOtherMonths: true,
					selectOtherMonths: true,
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$(this).next('span').remove('.btmerrmsg'); 
							$(this).removeClass('error');
							$(this).prev('div').remove();
						}
					},
					onClose: function () {
						$('#deliverypodate').focus();
					}
			}).click(function() {
					$(this).datetimepicker('show');
			});
			$('#deliverypomodeid').change(function(){
				var deliverypomodeid = $(this).val();
				$("#delvendorname,#delemployeeid,#delcouriername,#delotp").attr('class','validate[]');
				if(deliverypomodeid == 2) {
					$('.delvendornamediv').show();
					$('#delvendorname').attr('class','validate[required]');
					$('.delemployeediv,.delcouriernamediv,.delotpdiv').hide();
					$('#delcouriername,#delotp').val('');
					$('#delemployeeid').select2('val','');
				} else if(deliverypomodeid == 3) {
					$('.delcouriernamediv').show();
					$('#delcouriername').attr('class','validate[required]');
					$('.delemployeediv,.delvendornamediv,.delotpdiv').hide();
					$('#delvendorname,#delotp').val('');
					$('#delemployeeid').select2('val','');
				} else if(deliverypomodeid == 4) {
					$('.delemployeediv,.delotpdiv').show();
					$('#delemployeeid,#delotp').attr('class','validate[required]');
					$('.delcouriernamediv,.delvendornamediv').hide();
					$('#delvendorname,#delcouriername').val('');
				}
			});
			if(userroleid == '3') {
				$("#deliverplaceordericon").click(function() {
					var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						$.ajax({
								url:base_url+"Sales/checktransactiontypedata",
								data:{salesid:datarowid,type:27},
								type: "POST",
								dataType:'json',
								async:false,
								cache:false,
								success: function(data) {
									if(data['status'] != '3'){
										alertpopup("This action only apply for Generate Place Order.");
									} else {
										$('#deliveryplaceordoverlay').fadeIn();
										checkdeliverypostatus(datarowid);
									}
								}
							});
					}else {
						alertpopup(selectrowalert);
					}
				});
			}
			$('#deliveryplaceordcancel').click(function() {
				$("#deliveryplaceordoverlay").fadeOut();
			});
			$('#deliveryplaceordsave').click(function() {
				$("#deliveryplaceordvalidate").validationEngine('validate');	
			});
			jQuery("#deliveryplaceordvalidate").validationEngine({
				onSuccess: function() {
					deliverypooverlaysave();
				},
				onFailure: function() {
				}
			});
		}
		{ //Vendor Items Status Update - Concept is after accepted the items, vendor will update the status about the items.
			$('#vendoritemstatusicon').click(function() {
				var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
				if(datarowid) {
					$.ajax({
						url:base_url+"Sales/checktransactiontypedata",
						data:{salesid:datarowid,type:21},
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['status'] == 0) {
								alertpopup("This action only apply for Place Order");
							} else {
								vendoritemstatusgrid();
								$('#vendoritemstatusgridoverlay').fadeIn();
								retrievevendorproductdetailinformation(datarowid);
							}
						}
					});
				}
			});
			$('#vendoritemstatusformtogrid').click(function() {
				$("#vendoritemstatusgridvalidation").validationEngine('validate');
			});
			jQuery("#vendoritemstatusgridvalidation").validationEngine({
				onSuccess: function() {
					var salesdetailid = $('#povendoritemlistid').find('option:selected').data('salesdetailid');
					var vendororderitemstatusname = $('#vendororderitemstatusid').find('option:selected').data('vendororderitemstatusidhidden');
					var vendororderitemstatusid = $('#vendororderitemstatusid').find('option:selected').val();
					if(salesdetailid != '') {
						$('#vendoritemstatusgrid .gridcontent div#'+salesdetailid+' ul .vendororderitemstatusname-class').text(vendororderitemstatusname);
						$('#vendoritemstatusgrid .gridcontent div#'+salesdetailid+' ul .vendororderitemstatusid-class').text(vendororderitemstatusid);
					}
					$('#povendoritemlistid,#vendororderitemstatusid').select2('val','').trigger('change');
				},
				onFailure: function() {
				}
			});
			$('#vendoritemstatusgridsubmit').click(function() {
				vendoritemstatusgridsubmit();
			});
			$('#vendoritemstatusgridclose').click(function() {
				$('#vendoritemstatusgridoverlay').fadeOut();
			});
		}
		// Order Tracking with status of each item in the grid
		$('#ordertrackingicon').click(function() {
			ordertrackmainviewclick = 0;
			ordertrackinggrid();
			$('#ordertrackinggridoverlay').fadeIn();
		});
		// Fetch order details
		$('#ordertrackinggridsubmit').click(function() {
			$("#ordertrackinggridformvalidation").validationEngine('validate');
		});
		jQuery("#ordertrackinggridformvalidation").validationEngine({
			onSuccess: function() {
				var categoryid = $('#ordtrackproductcatgid').find('option:selected').val();
				var accname = $('#ordtrackaccountid').find('option:selected').val();
				var accshort = $('#ordtrackaccountshrtid').find('option:selected').val();
				var mobno = $('#ordtrackaccmobilenumber').val();
				var orderno = $('#ordtrackaccordernumber').val();
				if(accname == 1 && accshort == 1 && mobno == '' && orderno == '' && categoryid == 1) {
					alertpopupdouble('Please give value in any field to search order details!');
					return false;
				} else {
					ordertrackmainviewclick = 1;
					ordertrackinggrid();
				}
				Materialize.updateTextFields();
			},
			onFailure: function() {
				
			}
		});
		// Order tracking clear grid
		$('#ordertrackinggridclear').click(function() {
			ordertrackmainviewclick = 0;
			$('#ordtrackaccountid,#ordtrackaccountshrtid,#ordtrackproductcatgid').select2('val',1);
			$('#ordtrackaccmobilenumber,#ordtrackaccordernumber').val('');
			ordertrackinggrid();
			Materialize.updateTextFields();
		});
		// Order tracking grid close
		$('#ordertrackinggridclose').click(function() {
			$('#ordtrackaccountid,#ordtrackaccountshrtid,#ordtrackproductcatgid').select2('val',1);
			$('#ordtrackaccmobilenumber,#ordtrackaccordernumber').val('');
			$('#ordertrackinggridoverlay').fadeOut();
		});
		// cancel order
		$("#cancelordericon").click(function(){
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			resetFields();
			//$('#currentstatusid,#newstatusid').val('');
			//$('#tocounter').select2('val','');
			if(datarowid){
				$.ajax({
					url:base_url+"Sales/checktransactiontypedata",
					data:{salesid:datarowid,type:20},
					type: "POST",
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data['status'] == 0) {
							alertpopup("This action only apply for Take Order");
						} else  {
							$('#cancelordernumber').val(data['ordernumber']);
							loadorderitemdetail(datarowid);
							$("#cancelorderoverlay").fadeIn();
							Materialize.updateTextFields();
						}
					}
				});
			} else {
				alertpopup(selectrowalert);
			}	
		});
		// close cancel order overlay
		$('#closecancelorderoverlay').click(function(){
			$("#cancelorderoverlay").fadeOut();
		});
		// cancel order item dd change in cancel order overlay
		$("#cancelitemdetail").change(function(){
			var orderstatusname = $('#cancelitemdetail').find('option:selected').data('orderstatusname');
			$('#cancelcomment').val('');
			$('#currentorderstatus').val(orderstatusname);
			Materialize.updateTextFields();
		});
		//push to delivery submit button
		$('#cancelorderbtn').click(function() {
			$("#cancelorderformvalidate").validationEngine('validate');	
		});
		jQuery("#cancelorderformvalidate").validationEngine( {
			onSuccess: function() {
				cancelorderupdate();
			},
			onFailure: function() {	
				//alertpopup(validationalert);
			}
		});
		//Generate Place Order Details from vendor side
		$('#generateplaceordericon').click(function() {
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$.ajax({
					url:base_url+"Sales/checktransactiontypedata",
					data:{salesid:datarowid,type:21},
					type: "POST",
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data['status'] != '2'){
							alertpopup("This action only apply for Place Order");
						} else {
							checkeditsales(datarowid);
						}
					}
				});
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Bill Cancel
		$("#cancelicon").click(function() {
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var statusid = getgridcolvalue('salesgrid',datarowid,'statusname','');
				if(statusid == 'Cancel'){
					alertpopup("Selected bill already cancelled!");
				}else{
					$.ajax({
						url:base_url+"Sales/checkdeletedata",
						data: "salesid=" +datarowid+"&povendormanagement="+povendormanagement,
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['rows'] > 0){
								if(data['id'] == 20) {
									alertpopup("Can't cancel order.This order already used in either place order or delivery order");
								} else if(data['id'] == 28 || data['id'] == 29 || data['id'] == 30) {
									alertpopup("Can't cancel Repair details. This entry already used in either place repair or delivery repair");
								} else if(data['id'] == 21) {
									alertpopup("Can't cancel order.This order already used in receive order");
								} else if(data['id'] == 9) {
									alertpopup("Can't cancel Purchase.This data based stock created");
								} else if(data['id'] == 18) {
									alertpopup("Can't cancel Approval Out. This data based Approval Out Return created");
								} else if(data['id'] == 27) {
									alertpopup("Can't delete Generated Place Order. Those items were dispatched.");
								} else if(data['id'] == 11) {
									alertpopup("Can't cancel Invoice. Return Item is processed.!");
								}
							} else {
								moduleeditoralertmsgcancel('salescancelyes','Are you sure,you want to cancel this bill?');
								$("#cancelledcomment").val('');
								$("#salescancelyes").click(function() {
									var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
									var cancelledcomment = $("#cancelledcomment").val();
									billcancel(datarowid,cancelledcomment);
									$('#salescancelyes').unbind();
								});
							}
						}
					});
				}
			} else {
				alertpopup(selectrowalert);
			}	
		});
		//sales delete
		$("#basedeleteyes").click(function(){
			/* if(billdeletestatus == 1)
			{
				var datarowid = $('#billgrid div.gridcontent div.active').attr('id');
			}else if(billdeletestatus == 2)
			{
				var datarowid = $('#generatesalesid').val();
			}
			else{ */
			   var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
            //}			
			mainsalesbilldelete(datarowid);
		});
		//detailed view
		$("#detailedviewicon").click(function(){
			resetFields();			
			var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
			if (datarowid) {				
				showhideiconsfun('summryclick','salesaddformheader');
				$('#paymentform :input').prop('disabled', true);
				$('#paymentdetailedview').show();
				//hide action icons
				$('#formclearicon,#stonedetailsshow,#salesdetailedit,#salesdetaildelete,#paymentedit,#paymentdelete').hide();
				//Function Call For Edit
				salesdataretrieve(datarowid,'');
				//GRA for salesinnergrid Shrink issue Hide/Sow Grid 
				$("#salesinnergridwidth").removeClass('hidedisplay');
			} else {
				alertpopup(selectrowalert);
			}		
			Materialize.updateTextFields();	
		});
	}
	{// sales formtoolbar events	
		// alert for if any bal/adv while acc is not selected
		$("#alertsspecialclose").click(function(){
			$('#alertsspecial').fadeOut('fast');
			$('#salesmaintab').trigger('click');
			select2ddenabledisable('accountid','enable');
			$('#accountid').select2('focus');			
		});
		//reset the sales form button
		$("#stockundo").click(function(){
			$('#salesdetailopmode,#salesdetaileditedrowid').val('');
			$('#stocktypeid').select2('focus');
			clearform('salesaddform');
			$('#salesdetailsubmit').val('ADD');
			Materialize.updateTextFields();			
		});		
		//Reload
		$("#reloadicon").click(function(){
			clearinlinesrchandrgrid('salesgrid');
		});	
		
	}
	{// Approval Out RFID change
		$("#approvalrfid").click(function(){
			$('#approvalnumber').select2('val','');
		});
	}
	{//add form submit validation engine
		$('#salesdetailsubmit').click(function() {
		 	var accountid = $("#accountid").val();
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			var stocktypeid = $('#stocktypeid').find('option:selected').val();
			var wastagespan = $.trim($('#wastagespan').text());
			var grosswt = $.trim($('#grossweight').val());
			var purityid = $('#purity').find('option:selected').val();
			
			// Restrict different metal for Sales, Estimate, Take Order (concept given by vishal and developed by Kumaresan)
			if(transactiontype == 11 || transactiontype == 16 || transactiontype == 20) {
				
				// Sales Item
				if(metalarraydetails != '' && (stocktypeid == 11 || stocktypeid == 12 || stocktypeid == 13 || stocktypeid == 83 || stocktypeid == 75 || stocktypeid == 89)) {
					var salesitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','all','count');
					var checkmetalarraydetails = metalarraydetails.indexOf(metalarraydata[purityarraydata.indexOf(purityid)]);
					if(billeditstatus == 0) {
						if(salesitemscount == 0 && checkmetalarraydetails != -1) {
							metalstatus = 1;
						} else if(salesitemscount == 0 && checkmetalarraydetails == -1) {
							metalstatus = 0;
						}
					} else {
						if(salesitemscount == 0 || billlevelrestrictmetalid != 1) {
							metalstatus = 1;
						} else {
							metalstatus = 0;
						}
					}
					if(metalstatus == 1) {
						if(salesitemscount == 0) {
							billlevelrestrictmetalid = metalarraydata[purityarraydata.indexOf(purityid)];
						} else {
							var newadditem = metalarraydata[purityarraydata.indexOf(purityid)];
							var otherstocktypecount = getpuritybasedmetalidgridcondition('saledetailsgrid','checkrepair','count');
							if(otherstocktypecount != 0) {
								$('#stocktypeid').select2('val',89).trigger('change');
								alertpopup('Kindly do not use Repair Items in Sales Bill.!');
								return false;
							}
							if(billlevelrestrictmetalid != newadditem) {
								if(transactiontype == 11 || transactiontype == 16) {
									$('#stocktypeid').select2('val',11).trigger('change');
									alertpopup('Kindly do not use different Metal in Sales item!.');
									return false;
								} else if(transactiontype == 20) {
									$('#stocktypeid').select2('val',75).trigger('change');
									alertpopup('Kindly do not use different Metal in Order item!.');
									return false;
								}
							}
						}
					}
				}
				
				// Old Items
				if(metaloldarraydetails != '' && stocktypeid == 19) {
					var salesolditemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','old','count');
					var checkoldmetalarraydetails = metaloldarraydetails.indexOf(metalarraydata[purityarraydata.indexOf(purityid)]);
					if(billeditstatus == 0) {
						if(salesolditemscount == 0 && checkoldmetalarraydetails != -1) {
							metaloldstatus = 1;
						} else if(salesolditemscount == 0 && checkoldmetalarraydetails == -1) {
							metaloldstatus = 0;
						}
					} else {
						if(salesolditemscount == 0 || billlevelrestrictoldmetalid != 1) {
							metaloldstatus = 1;
						} else {
							metaloldstatus = 0;
						}
					}
					if(metaloldstatus == 1) {
						if(salesolditemscount == 0) {
							billlevelrestrictoldmetalid = metalarraydata[purityarraydata.indexOf(purityid)];
						} else {
							var newoldadditem = metalarraydata[purityarraydata.indexOf(purityid)];
							if(billlevelrestrictoldmetalid != newoldadditem) {
								$('#stocktypeid').select2('val',19).trigger('change');
								alertpopup('Kindly do not use different Metal in Old item!.');
								return false;
							}
						}
					}
				}
				
				// Return Items
				if(metalreturnarraydetails != '' && stocktypeid == 20) {
					var salesreturnitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','return','count');
					var checkreturnmetalarraydetails = metalreturnarraydetails.indexOf(metalarraydata[purityarraydata.indexOf(purityid)]);
					if(billeditstatus == 0) {
						if(salesreturnitemscount == 0 && checkreturnmetalarraydetails != -1) {
							metalreturnstatus = 1;
						} else if(salesreturnitemscount == 0 && checkreturnmetalarraydetails == -1) {
							metalreturnstatus = 0;
						}
					} else {
						if(salesreturnitemscount == 0 || billlevelrestrictreturnmetalid != 1) {
							metalreturnstatus = 1;
						} else {
							metalreturnstatus = 0;
						}
					}
					if(metalreturnstatus == 1) {
						if(salesreturnitemscount == 0) {
							billlevelrestrictreturnmetalid = metalarraydata[purityarraydata.indexOf(purityid)];
						} else {
							var newreturnadditem = metalarraydata[purityarraydata.indexOf(purityid)];
							if(billlevelrestrictreturnmetalid != newreturnadditem) {
								$('#stocktypeid').select2('val',20).trigger('change');
								alertpopup('Kindly do not use different Metal in Return item!.');
								return false;
							}
						}
					}
				}
			}
			
			if(stocktypeid == 11 || stocktypeid == 12 || stocktypeid == 13 || stocktypeid == 75 || stocktypeid == 82) {
				if(chargerequired == 1) {
					if(wastagespan == 0 && grosswt != 0) {
						if(wastagevisible == 1) {
							alertpopup('Please give wastage charge for this product');
							return false;
						}
					}
				}
			}
			if(transactiontype == 11 || transactiontype == 16) {
				if(stocktypeid != 20) { // except sales return
					if($("#sdiscounttypeid").val() == 2) {
						var presumarydiscountamount = $('#discountdata').val();
						var val = $('#discount').val();
						if(parseFloat(val) > parseFloat(presumarydiscountamount)) {
							alertpopup('Please enter the amount less or equal to Discount Value');
							return false;
						}
					}
				}
			}
			if(transactiontype == 20 && stocktypeid == 75 && salesfieldcommentrequired == 1) {
				if($("#salescomment").val() == '') {
					alertpopup('Please give narration details for Order Items');
					return false;
				}
			}
			if(stocktypeid == 13) { // partial tag
			   var partialgrosswt = $("#partialgrossweight").val();
				if(parseFloat(partialgrosswt) <= parseFloat($("#grossweight").val())) {
					alertpopup('Kindly Enter less than '+partialgrosswt+' gms ');
					return false;
				}
			}
			if(transactiontype == 9 && stocktypeid != 73 && $("#weightcheck").val() == 1) {
				var purchasecheckvalidate = purchaseweightvalidation(0);
				if(purchasecheckvalidate == false){
				   return false;
				}
			}
			validategetweight(stocktypeid,'1','grossweight');
			validategetweight(stocktypeid,'2','netweight');
			validategetweight(stocktypeid,'3','pieces');
			if(accountid == '' && transactiontype !='13' && stocktypeid != '82') {
				alertpopup('Please select the account name');
			} else {
				$("#modeidtype").val(2);
				operationstone = $(this).data('id');
				$("#salesaddform").validationEngine('validate');	
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#salesaddform").validationEngine({
			onSuccess: function() {
				$('#processoverlay').show();
				$('#modeid').select2('val',2);
				$('#paymentmodeid').select2('val','');
				setzero(['paymenttotalamount']);
				var stocktype = $('#stocktypeid').find('option:selected').val();
				var employeepersonid = $('#employeepersonid').find('option:selected').val();
				var paymentstocktypeid = $('#paymentstocktypeid').find('option:selected').val();
				var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
				$('#paymentstocktypeid').select2('val','');
				if(stocktype == 19){
					$('#orderduedate,#vendororderduedate').val('0000-00-00');
				}
				var taxtypeid = $("#taxtypeid").val();
				var sdiscounttypeid = $('#sdiscounttypeid').val();
				var lottypeid = $('#lottypeid').val();
				var orderitemratefix = $('#orderitemratefix').find('option:selected').val();
				formdatatransfertogrid(operationstone,1);
				//getsalesdetailcalc(); // get local grid data based calcualtion
				if(rfidbasedtagfetch != 0){
					if(stocktype == 11 || stocktype == 12 || stocktype == 13 || stocktype == 17 || stocktype == 81 || stocktype == 75 || stocktype == 73 || stocktype == 86) {
						finalsummarycalc(1);
					} else {
						finalsummarycalc(0);
					} 
				} else if(rfidbasedtagfetch == 0 && rfidtagarrayincrem == rfidtagarray.length) {
					if(stocktype == 11 || stocktype == 12 || stocktype == 13 || stocktype == 17 || stocktype == 81 || stocktype == 75 || stocktype == 73 || stocktype == 86) {
						finalsummarycalc(1);
					} else {
						finalsummarycalc(0);
					}
				}
				/* Data row select event */
				datarowselectevt();
				$("#salescreateform :input").attr("readonly", true);
				$('#salespersonid,#salestransactiontypeid,#transactionmodeid,#salesdate').prop('disabled',true);
				clearform('salesaddform');
				if(transactiontype == '16') {
					clearform('paymentaddform');
				}
				clearform('productaddonform');
				$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
				$("#itemdiscountcalname").text('(0)');
				operationstone = 'ADD';
				$('#stocktypeid').select2('val',stocktype).trigger('change');
				$('#employeepersonid').select2('val',employeepersonid);
				$('#taxgriddata,#hiddenstonedetail,#salescomment,#tagimage').val('');
				$('#tagimagedisplay').empty();
				$('#partialgrossweight,#stockgrossweight,#stockpieces').val(0);
				if(transactionemployeeperson == '0') {
					$('#multipleemployeeid,#multipleemployeename').val('');
				}
				if(taxtypeid == 2){
					$('#taxcategory').select2('val','').trigger('change');
				}
				if(transactiontype != '16' || transactiontype != '13' || transactiontype != '18') {
					$('#paymentstocktypeid').select2('val',paymentstocktypeid).trigger('change');
				}
				if(transactiontype == 13){  // weight sales direct submit
					$('#dataaddsbtn').trigger('click');
				}
				$('#oldjewelnewrate').text('');
				accounticonhideshow();
				if(transactiontype == 20) { // takeorder
					if(stocktype == '19' || stocktype == '75' || stocktype == '82') {
						orderstocktypehideshow(stocktype);
					}
					$('#orderitemratefix').select2('val',orderitemratefix).trigger('change');
				}
				if(transactiontype == 11) { // sales
					if(stocktype == '19' || stocktype == '83') {
						deliveryorderstocktypehideshow(stocktype);
					}
				}
				if(transactiontype == 9) { // Purchase - After salesdetailsubmit field focus
					if(lottype == 4) { //Both option to trigger recently used lot type
						$('#lottypeid').select2('val',lottypeid).trigger('change');
					}
					if(purchasedisplay == 2) {
						$('#category').select2('focus');
					}else if(purchasedisplay == 1) {
						$('#product').select2('focus');
					} else {
						$('#purity').select2('focus');
					}
				}
				$('#processoverlay').hide();
				if(rfidtagarray.length > 0 ){
					 setTimeout(function() {
						if(rfidbasedtagfetch == 0 && rfidtagarrayincrem < rfidtagarray.length) {
							var tr = rfidtagarrayincrem;
							rfidtagarrayincrem++;
							$('#itemtagnumber').val(rfidtagarray[tr]).trigger('change');
						} else {
							rfidtagarrayincrem = 1;
							rfidbasedtagfetch = 1;
							rfidtagarray = [];
							$('#processoverlay').hide();
							alertpopup('Successfully Converted');
							if(estimatemainsavetrigger == 1){
								$('#dataaddsbtn').trigger('click');
							}
						}
					},35);
				}
			},
			onFailure: function() {
				var dropdownid =['4','stocktypeid','product','purity','counter'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{//add form submit validation engine
		if(paymentsavebutton == 1) {
			$('#paymenttotalamount').keypress(function(e) {
				if (e.which == 13) {
					$('#paymentdetailsubmit').trigger('click');
				}
			});
		}
		$('#paymentdetailsubmit').click(function() {
			var accountid = $("#accountid").val();
			var amount = $('#paymenttotalamount').val();
			var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
			var paymentstocktypeid=$('#paymentstocktypeid').find('option:selected').val();
			var cashtenthousand = $('#cashtenthousand').val();
			if(accountid) {
				if(salestransactiontypeid == 22 || salestransactiontypeid == 26 ) { //payment issue
					var paygrossweight = validategetweight(paymentstocktypeid,'5','paymentgrossweight');
					var paynetweight = validategetweight(paymentstocktypeid,'4','paymentnetweight');
					if(paygrossweight == false || paynetweight == false) {
						return false;
					}
				}
				var gridrowdata = getgridallrowids('saledetailsgrid');
				if(gridrowdata == '') {
					if(salestransactiontypeid == 22 ) { // payment issue
						if(paymentstocktypeid == 26) {
							if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
								var oldamount = 0;
							} else {
								//var oldamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
								var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','paymentstocktypeid','26'));
							}
							var famount = parseFloat(amount) + parseFloat(oldamount);
							if(cashtenthousand == 1) {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
							} else {
								if(famount > cashissuelimit) {
									var validamount = cashissuelimit - parseFloat(oldamount);
									if(oldamount > 0 ) {
										alertpopup('Please enter the cash amount below or equal to 10000');
									} else if(validamount == 0 ) {
										alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
									} else {
										alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
									}
								} else {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							}
						} else if(paymentstocktypeid == 37) {
							if(customerwise == 1 && $('#selaccounttypeid').val() == 6 || vendorwise == 1 && $('#selaccounttypeid').val() == 16) {
								var ctransactionmodeid =$('#transactionmodeid').find('option:selected').val();
								var crdbillwt = parseFloat($('#spanbillwt').text()).toFixed(roundweight);
								var crdbillamt = parseFloat($('#spanbillamt').text()).toFixed(round);
								var ppaymentpureweight = parseFloat($('#paymentpureweight').val()).toFixed(roundweight);
								var ppaymenttotalamt = parseFloat($('#paymenttotalamount').val()).toFixed(round);
								if(ctransactionmodeid == 3 && crdbillwt != ppaymentpureweight || ctransactionmodeid == 2 && crdbillamt != ppaymenttotalamt) {
									alertpopup('Kindly use the Same Bill Amt/Wt in Payment');
								} else {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else {
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else { //for other type not restrict
							$("#modeidtype").val(3);
							operationstone = $(this).data('id');
							$("#paymentaddform").validationEngine('validate');			
							$(".mmvalidationnewclass .formError").addClass("errorwidth");
						}
					} else if(salestransactiontypeid == 23) { //payment receipt
						if( paymentstocktypeid == 26) { // for cash - restrict below 2 lacks
							if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
								var oldamount = 0;
							} else {
								var oldamount = getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','stocktypename','Cash');
							}
							var famount = parseFloat(amount) + parseFloat(oldamount);
							if(famount > cashreceiptlimit) {
								var validamount = cashreceiptlimit - parseFloat(oldamount);
								if(oldamount > 0 ) {
									alertpopup('Please enter the cash amount below or equal to 10000');
								} else if(validamount == 0 ) {
									alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
								} else {
									alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else if(paymentstocktypeid == 37) {
							if(customerwise == 1 && $('#selaccounttypeid').val() == 6 || vendorwise == 1 && $('#selaccounttypeid').val() == 16) {
								var ctransactionmodeid =$('#transactionmodeid').find('option:selected').val();
								var crdbillwt = parseFloat($('#spanbillwt').text()).toFixed(roundweight);
								var crdbillamt = parseFloat($('#spanbillamt').text()).toFixed(round);
								var ppaymentpureweight = parseFloat($('#paymentpureweight').val()).toFixed(roundweight);
								var ppaymenttotalamt = parseFloat($('#paymenttotalamount').val()).toFixed(round);
								if(ctransactionmodeid == 3 && crdbillwt != ppaymentpureweight || ctransactionmodeid == 2 && crdbillamt != ppaymenttotalamt) {
									alertpopup('Kindly use the Same Bill Amt/Wt in Payment');
								} else {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else {
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else { //for other type not restrict
							$("#modeidtype").val(3);
							operationstone = $(this).data('id');
							$("#paymentaddform").validationEngine('validate');		
							$(".mmvalidationnewclass .formError").addClass("errorwidth");
						}
					} else if(salestransactiontypeid == 24 || salestransactiontypeid == 25 || salestransactiontypeid == 26) {
						if(salestransactiontypeid == 24 && paymentstocktypeid == 31) {
							$('#journalaccountid').attr('data-validation-engine','validate[required]');
						}
						$("#modeidtype").val(3);
						operationstone = $(this).data('id');
						$("#paymentaddform").validationEngine('validate');			
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					} else {
						alertpopup('You Must Add Sales Detail Entry Before Creation Payment Entry');
						return false;
					}
				} else {
					var salesdetailid = gridrowdata.toString();
					var transactionmodestatus = 0;
					var salesdetails=salesdetailid.split(",");
					$.each(salesdetails, function (index, value) {
						var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
						if(modestatus == '2') {
							transactionmodestatus = 1;
						}
					});
					if(transactionmodestatus == 0){
						if(salestransactiontypeid == 22) { // payment issue
							if( paymentstocktypeid == 26) {
								if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									//var oldamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','paymentstocktypeid','26'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount);
								if(cashtenthousand == 1) {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								} else {
									if(famount > cashissuelimit) {
										var validamount = cashissuelimit - parseFloat(oldamount);
										if(oldamount > 0 ) {
											alertpopup('Please enter the cash amount below or equal to 10000');
										} else if(validamount == 0 ) {
											alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
										} else {
											alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
										}
									} else {
										$("#modeidtype").val(3);
										operationstone = $(this).data('id');
										$("#paymentaddform").validationEngine('validate');			
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else if(salestransactiontypeid == 23) { //payment receipt
							if(paymentstocktypeid == 26) { // for cash - restrict below 2 lacks
								if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									var oldamount = getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','stocktypename','Cash');
								}
								var famount = parseFloat(amount) + parseFloat(oldamount);
								if(famount > cashreceiptlimit) {
									var validamount = cashreceiptlimit - parseFloat(oldamount);
									if(oldamount > 0 ) {
									alertpopup('Please enter the cash amount below or equal to 10000');
									} else if(validamount == 0 ) {
										alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
									} else {
										alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
									}
								} else { //for other type not restrict
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else if(salestransactiontypeid == 24 || salestransactiontypeid == 25 || salestransactiontypeid == 26) { // maddy
							if(salestransactiontypeid == 24 && paymentstocktypeid == 31) {
								$('#journalaccountid').attr('data-validation-engine','validate[required]');
							}
							$("#modeidtype").val(3);
							operationstone = $(this).data('id');
							$("#paymentaddform").validationEngine('validate');			
							$(".mmvalidationnewclass .formError").addClass("errorwidth");
						} else {
							alertpopup('You Must Add Sales Detail Entry Before Creation Payment Entry');
							return false;
						}
					} else {
						var issuereceipt = $('#issuereceiptid').val();
						if(issuereceipt == 2) { //for issue
							if( paymentstocktypeid == 26) { //only for cash
								if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-2'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount);
								
								if(cashtenthousand == 1)
								{
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}else{
										if(famount > cashissuelimit) {
										var validamount = cashissuelimit - parseFloat(oldamount);
										if(oldamount < 1) {
											alertpopup('Please enter the cash amount below or equal to 10000');
										} if(validamount == 0 ) {
											alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount using cash. Please choose other type');
										} else {
											alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
										}
									} else { //for other type not restrict
										$("#modeidtype").val(3);
										operationstone = $(this).data('id');
										$("#paymentaddform").validationEngine('validate');			
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
							 }
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} 
						else { //for receipt
							if( paymentstocktypeid == 26) { // for cash - restrict below 2 lacks
								if($('#salesinnergrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-3'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount);
								if(famount > cashreceiptlimit) {
									var validamount = cashreceiptlimit - parseFloat(oldamount);
									if(oldamount > 0 ) {
									alertpopup('Please enter the cash amount below or equal to 10000');
									} else if(validamount == 0 ) {
										alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
									} else {
										alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
									}
								} else { //for other type not restrict
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymentaddform").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else { //for other type not restrict
							
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymentaddform").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						}
					}
				}
			} else {
				alertpopup('Please select the account name');
			}
		
		});
		jQuery("#paymentaddform").validationEngine({
			onSuccess: function() {
				netcalcheck = 1;
				$('#processoverlay').show();
				$('#paymentmodeid').select2('val',3);
				$('#modeid').select2('val','');
				var stocktype = $('#stocktypeid').find('option:selected').val();
				var paymentstocktype = $('#paymentstocktypeid').find('option:selected').val();
				var paymentaccountid = $('#paymentaccountid').find('option:selected').val();
				var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				var issuereceiptid = $('#issuereceipttypeid').find('option:selected').val();
				$('#stocktypeid').select2('val','');
				if(paymentstocktype == 26 || paymentstocktype == 31 || paymentstocktype == 39 || paymentstocktype == 37 || paymentstocktype == 38 || paymentstocktype == 49) {
					$('#paymentreferencedate').val('0000-00-00');
				}
				if(paymentstocktype == 39 || paymentstocktype == 37 || paymentstocktype == 38 || paymentstocktype == 49) {
					$('#paymentaccountid').select2('val','');
				}
				if(salestransactiontypeid != 24 && paymentstocktype == 31) {
					$('#paymentaccountid').select2('val','');
				}
				if(paymentstocktype != 49) {
					$('#newpaymenttouch').val(0);
				}
				formdatatransfertogrid(operationstone,0);
				$('#creditno').attr("readonly", true);
				/* Data row select event */
				datarowselectevt();
				//clearform('salesaddform');
				clearform('paymentaddform');
				//clearform('productaddonform');
				$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
				operationstone = 'ADD';
				if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
					iraccounthideshow();
				} else {
					$('#stocktypeid').select2('val',stocktype).trigger('change');
				}
				if(salestransactiontypeid != 24 && paymentstocktype == 31) {
					paymentstocktype = defaultsetpaymentstocktype;
				} else {
					paymentstocktype = paymentstocktype;
				}
				$('#paymentstocktypeid').select2('val',paymentstocktype).trigger('change');
				$('#paymentaccountid').select2('val',paymentaccountid);
				$('#salesdetailcancel,#paymentdetailupdate').hide();
				if(paymentsavebutton == 1) {
					$('#paymentdetailsubmit').hide();
				} else {
					$('#paymentdetailsubmit').show();
				}
				$('#paymentcomment').val('');
				bhavcutbasedsummaryshowhide();
				$("#salescreateform :input").attr("readonly", true);
				$('#salespersonid,#paymentirtypeid,#loadbilltype,#salestransactiontypeid,#transactionmodeid,#salesdate,#ratecuttypeid').prop('disabled',true);
				finalsummarycalc(0);
				var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
				if((salestransactiontypeid == 22 && ratecuttypeid == 2) || (salestransactiontypeid == 23 && ratecuttypeid == 2)) { // ratecut in payment i/r is active
					$('#dataaddsbtn').trigger('click');
				}
				if(salestransactiontypeid == '24' || salestransactiontypeid == '26') {
					if(salestransactiontypeid == '24') {
						$('#paymentaccountid').select2('val',''); 
					}
					$('#issuereceipttypeid').select2('val',issuereceiptid).trigger('change');
				}
				$('#processoverlay').hide();
			},
			onFailure: function() {
					
				var dropdownid =['2','stocktypeid','product'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{//edit form submit validation engine
		$('#salesdetailupdate').click(function() {
			var accountid = $("#accountid").val();
			var stocktypeid = $('#stocktypeid').find('option:selected').val();
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			var selectedrow = $('#editinnerrowid').val();
			var wastagespan = $.trim($('#wastagespan').text());
			var grosswt = $.trim($('#grossweight').val());
			if(wastagevisible == 1) {
				if(stocktypeid == 11 || stocktypeid == 12 || stocktypeid == 13 || stocktypeid == 75 || stocktypeid == 82) {
					if(chargerequired == 1) {
						if(wastagespan == 0 && grosswt != 0) {
							alertpopup('Please give wastage charge for this product');
							return false;
						}
					}
				}
			}
			if(transactiontype == 11 || transactiontype == 16) {
				if(stocktypeid != 20) // except sales return
				{
					if($("#sdiscounttypeid").val() == 2) {
						var presumarydiscountamount = $('#discountdata').val();
						var val = $('#discount').val();
						if(parseFloat(val) > parseFloat(presumarydiscountamount)) {
							alertpopup('Please enter the amount less or equal to Discount Value');
							return false;
						}
					}
				}
			}
			if(stocktypeid == 13) { // partial tag
			   var partialgrosswt = $("#partialgrossweight").val();
				if(parseFloat(partialgrosswt) <= parseFloat($("#grossweight").val())) {
					alertpopup('Kindly Enter less than '+partialgrosswt+' gms ');
					return false;
				}
			}
			if(transactiontype == 20 && stocktypeid == 75 && salesfieldcommentrequired == 1) {
				if($("#salescomment").val() == '') {
					alertpopup('Please give narration details for Order Items');
					return false;
				}
			}
			if(transactiontype == 9 && stocktypeid != 73 && $("#weightcheck").val() == 1) {
				var purchasecheckvalidate = purchaseweightvalidation(1);
				if(purchasecheckvalidate == false){
				   return false;
				}
			}
			validategetweight(stocktypeid,'1','grossweight');
			validategetweight(stocktypeid,'2','netweight');
			validategetweight(stocktypeid,'3','pieces');
			if(stocktypeid != 82){
				if(accountid) {
					operationstone = $(this).data('id');
					$("#modeidtype").val(2);
					$("#editvalidate").validationEngine('validate');			
					$(".mmvalidationnewclass .formError").addClass("errorwidth");
				} else {
					alertpopup('Please select the account name');
				}
			}else{
				operationstone = $(this).data('id');
				$("#modeidtype").val(2);
				$("#editvalidate").validationEngine('validate');			
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#editvalidate").validationEngine({
			onSuccess: function() {
				retrievestatus=1;
				salesdetailentryeditstatus = 0;
				$('#processoverlay').show();	
				$('#modeid').select2('val',2);
				$('#paymentmodeid').select2('val','');
				retrievetagstatus=0;
				var paymentstocktypeid=$('#paymentstocktypeid').find('option:selected').val();
				var transactiontype=$('#salestransactiontypeid').find('option:selected').val();
				var taxtypeid = $("#taxtypeid").val();
				var sdiscounttypeid = $('#sdiscounttypeid').val();
				var employeepersonid=$('#employeepersonid').find('option:selected').val();
				var stocktype=$('#stocktypeid').find('option:selected').val();
				$('#paymentstocktypeid').select2('val','');
				if(stocktype == 19){
					$('#orderduedate,#vendororderduedate').val('0000-00-00');
				}else if(transactiontype == 18 || transactiontype == 19){
					$('#approvalnumber').prop('disabled', false);
				}
				setzero(['paymenttotalamount']);
				formdatatransfertogrid(operationstone,1);
				//getsalesdetailcalc(); // get local grid data based calcualtion
				if(stocktype == 11 || stocktype == 12 || stocktype == 13 || stocktype == 17 || stocktype == 81 || stocktype == 75) {
					finalsummarycalc(1);
				} else {
					finalsummarycalc(0);
				}
				$('#dataaddsbtn').show();
				/* Data row select event */
				datarowselectevt();
				clearform('salesaddform');
				if(transactiontype == '16') {
					clearform('paymentaddform');
				}
				if(transactiontype == 11 && stocktype == '83') {
					$('#itemtagnumber,#rfidtagnumber').val('');
				}
				clearform('productaddonform');
				$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
				operationstone = 'ADD';
				$('#salesdetailupdate').hide();
				$('#salesdetailsubmit,#salesdetailentryedit,#salesdetailentrydelete').show();
				if(paymentsavebutton == 1) {
					$('#paymentdetailsubmit').hide();
				} else {
					$('#paymentdetailsubmit').show();
				}
				$("#modeid").attr('readonly',false);
				$("#stocktypeid").prop('disabled',false);
				$('#itemtagnumber,#rfidtagnumber').prop('readonly','');
				$('#taxgriddata,#editinnerrowid,#hiddenstonedetail,#salescomment,#estimationnumber,#tagimage').val('');
				$('#tagimagedisplay').empty();
				$('#partialgrossweight,#stockgrossweight,#stockpieces').val(0);
				$('#paymentstocktypeid').select2('val',paymentstocktypeid).trigger('change');
				$('#product,#purity,#counter,#sumarytaxamount,#sumarydiscountamount,#ordernumber,#category,#ordernumber').prop('disabled',false);
				$('#stocktypeid').select2('val',stocktype).trigger('change');
				$('#employeepersonid').select2('val',employeepersonid);
				$("#itemdiscountcalname").text('(0)');
				if(transactionemployeeperson == '0') {
					$('#multipleemployeeid,#multipleemployeename').val('');
				}
				if(taxtypeid == 2) {	
					$('#taxcategory').select2('val','').trigger('change');
				}else{
					var sumarytaxamount = $('#sumarytaxamount').val();
					if(sumarytaxamount == 0){
						$('#taxcategory').select2('val','').trigger('change');
					}
				}
				$('#oldjewelnewrate').text('');
				/* if(sdiscounttypeid == 3) {
					$('#sumarydiscountamount').val(0).trigger('change');
				} */
				$('#estimationnumber,#roundvalue').attr('readonly',false);
				if(billeditstatus == 0) {
					$('#formclearicon').show();
				}
				accounticonhideshow();
				if(transactiontype == 20){ // takeorder
					if(stocktype == '19' || stocktype == '75' || stocktype == '82') {
						orderstocktypehideshow(stocktype);
					}
				}
				if(transactiontype == 11){ // sales
					if(stocktype == '19' || stocktype == '83') {
						deliveryorderstocktypehideshow(stocktype);
					}
				}
				if(transactiontype == 18 && stocktype == '62' || (transactiontype == 16 || transactiontype == 11) && (stocktype == '11' || stocktype == '13') ) {
					if(multitagbarcode == 2) {
						$('#tagnomultiscan').show();
						$("#itemtagnumber").css('display','inline').css('width','100%');
						$('label[for="itemtagnumber"]').css('width','100%');
					}
					$('#appoutrfidbtn').show();
					$("#rfidtagnumber").css('display','inline').css('width','90%');
					$('label[for="rfidtagnumber"]').css('width','90%');
				}
				if(transactioncategoryfield == '0') {
					$('#prodcategorylistbutton').prop('disabled',false);
				}
				$('#processoverlay').hide();
			},
			onFailure: function() {	
				var dropdownid =['4','stocktypeid','product','purity','counter'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{//edit form submit validation engine
		$('#paymentdetailupdate').click(function() {
			operationstone = $(this).data('id');
			$("#modeidtype").val(3);
			var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
			var amount = $('#paymenttotalamount').val();
			var cashtenthousand = $('#cashtenthousand').val();
			var paymentstocktypeid=$('#paymentstocktypeid').find('option:selected').val();
			var editpaymenttotalamount = $.trim($('#editpaymenttotalamount').val());
			if(accountid) {
				if(salestransactiontypeid == 22 || salestransactiontypeid == 26 ) { //payment issue
					var paygrossweight = validategetweight(paymentstocktypeid,'5','paymentgrossweight');
					var paynetweight = validategetweight(paymentstocktypeid,'4','paymentnetweight');
					if(paygrossweight == false || paynetweight == false) {
						return false;
					}
				}
				var gridrowdata = getgridallrowids('saledetailsgrid');
				if(gridrowdata == '') {
					if(salestransactiontypeid == 23) { // payment issue/receipt
						$("#modeidtype").val(3);
						$("#paymenteditvalidate").validationEngine('validate');			
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					}else if(salestransactiontypeid == 22 ) { // payment issue
						if(paymentstocktypeid == 26) {
							if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
								var oldamount = 0;
							} else {
								//var oldamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
								var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','paymentstocktypeid','26'));
							}
							var famount = parseFloat(amount) + parseFloat(oldamount) - parseFloat(editpaymenttotalamount);
							if(cashtenthousand == 1) {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymenteditvalidate").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}else{
								if(famount > cashissuelimit) {
									var validamount = cashissuelimit - parseFloat(oldamount);
									if(oldamount > 0 ) {
									alertpopup('Please enter the cash amount below or equal to 10000');
									} else if(validamount == 0 ) {
										alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
									} else {
										alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
									}
								} else {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymenteditvalidate").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							}
						} else { //for other type not restrict
							$("#modeidtype").val(3);
							operationstone = $(this).data('id');
							$("#paymenteditvalidate").validationEngine('validate');			
							$(".mmvalidationnewclass .formError").addClass("errorwidth");
						}
					} else if(salestransactiontypeid == 24 || salestransactiontypeid == 25 || salestransactiontypeid == 26) { // maddy
						$("#modeidtype").val(3);
						operationstone = $(this).data('id');
						$("#paymenteditvalidate").validationEngine('validate');			
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					} else{
						alertpopup('You Must Add Sales Detail Entry Before Creation Payment Entry');
						return false;
					}
				} else {
					var salesdetailid = gridrowdata.toString();
					var transactionmodestatus = 0;
					var salesdetails=salesdetailid.split(",");
					$.each(salesdetails, function (index, value) {
						var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
						if(modestatus == '2'){
							transactionmodestatus = 1;
						}
					});
					if(transactionmodestatus == 0) {
						if(salestransactiontypeid == 23) { // payment receipt
							if(paymentstocktypeid == 37) {
								if(customerwise == 1 && $('#selaccounttypeid').val() == 6 || vendorwise == 1 && $('#selaccounttypeid').val() == 16) {
									var ctransactionmodeid =$('#transactionmodeid').find('option:selected').val();
									var crdbillwt = parseFloat($('#spanbillwt').text()).toFixed(roundweight);
									var crdbillamt = parseFloat($('#spanbillamt').text()).toFixed(round);
									var ppaymentpureweight = parseFloat($('#paymentpureweight').val()).toFixed(roundweight);
									var ppaymenttotalamt = parseFloat($('#paymenttotalamount').val()).toFixed(round);
									if(ctransactionmodeid == 3 && crdbillwt != ppaymentpureweight || ctransactionmodeid == 2 && crdbillamt != ppaymenttotalamt) {
										alertpopup('Kindly use the Same Bill Amt/Wt in Payment');
									} else {
										$("#modeidtype").val(3);
										$("#paymenteditvalidate").validationEngine('validate');			
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
								} else {
									$("#modeidtype").val(3);
									$("#paymenteditvalidate").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else {
								$("#modeidtype").val(3);
								$("#paymenteditvalidate").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else if(salestransactiontypeid == 22 ) { // payment issue
							if( paymentstocktypeid == 26) {
								if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									//var oldamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','paymentstocktypeid','26'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount) - parseFloat(editpaymenttotalamount);
								if(cashtenthousand == 1) {
									$("#modeidtype").val(3);
									operationstone = $(this).data('id');
									$("#paymenteditvalidate").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								} else {
									if(famount > cashissuelimit) {
										var validamount = cashissuelimit - parseFloat(oldamount);
										if(oldamount > 0 ) {
											alertpopup('Please enter the cash amount below or equal to 10000');
										} else if(validamount == 0 ) {
											alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
										} else {
											alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
										}
									} else {
										$("#modeidtype").val(3);
										operationstone = $(this).data('id');
										$("#paymenteditvalidate").validationEngine('validate');			
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
								}
							} else if(paymentstocktypeid == 37) {
								if(customerwise == 1 && $('#selaccounttypeid').val() == 6 || vendorwise == 1 && $('#selaccounttypeid').val() == 16) {
									var ctransactionmodeid =$('#transactionmodeid').find('option:selected').val();
									var crdbillwt = parseFloat($('#spanbillwt').text()).toFixed(roundweight);
									var crdbillamt = parseFloat($('#spanbillamt').text()).toFixed(round);
									var ppaymentpureweight = parseFloat($('#paymentpureweight').val()).toFixed(roundweight);
									var ppaymenttotalamt = parseFloat($('#paymenttotalamount').val()).toFixed(round);
									if(ctransactionmodeid == 3 && crdbillwt != ppaymentpureweight || ctransactionmodeid == 2 && crdbillamt != ppaymenttotalamt) {
										alertpopup('Kindly use the Same Bill Amt/Wt in Payment');
									} else {
										$("#modeidtype").val(3);
										$("#paymenteditvalidate").validationEngine('validate');			
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
								} else {
									$("#modeidtype").val(3);
									$("#paymenteditvalidate").validationEngine('validate');			
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								operationstone = $(this).data('id');
								$("#paymenteditvalidate").validationEngine('validate');			
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else if(salestransactiontypeid == 24 || salestransactiontypeid == 25 || salestransactiontypeid == 26) {
							$("#modeidtype").val(3);
							operationstone = $(this).data('id');
							$("#paymenteditvalidate").validationEngine('validate');			
							$(".mmvalidationnewclass .formError").addClass("errorwidth");
						} else{
							alertpopup('You Must Add Sales Detail Entry Before Creation Payment Entry');
							return false;
						}
					} else { /** Below code to update payment details **/
						var issuereceipt = $('#issuereceiptid').val();
						if(issuereceipt == 2) { //for issue
							if(paymentstocktypeid == 26) { //only for cash
								if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-2'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount) - parseFloat(editpaymenttotalamount);
								if(cashtenthousand == 1) {
									$("#modeidtype").val(3);
									$("#paymenteditvalidate").validationEngine('validate');	
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								} else {
									if(famount > cashissuelimit) {
										var validamount = cashissuelimit - parseFloat(oldamount);
										if(oldamount < 1) {
											alertpopup('Please enter the cash amount below or equal to 10000');
										} if(validamount == 0 ) {
											alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount using cash. Please choose other type');
										} else {
											alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
										}
									} else { //for other type not restrict
										$("#modeidtype").val(3);
										$("#paymenteditvalidate").validationEngine('validate');	
										$(".mmvalidationnewclass .formError").addClass("errorwidth");
									}
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								$("#paymenteditvalidate").validationEngine('validate');	
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						} else { //for receipt
							if( paymentstocktypeid == 26) { // for cash - restrict below 2 lacks
								if($('#salesinnergrid .gridcontent div.data-content div.data-rows').length == 0) {
									var oldamount = 0;
								} else {
									var oldamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-3'));
								}
								var famount = parseFloat(amount) + parseFloat(oldamount);
								if(famount > cashreceiptlimit) {
									var validamount = cashreceiptlimit - parseFloat(oldamount);
									if(oldamount > 0 ) {
										alertpopup('Please enter the cash amount below or equal to 10000');
									} else if(validamount == 0 ) {
										alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
									} else {
										alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
									}
								} else { //for other type not restrict
									$("#modeidtype").val(3);
									$("#paymenteditvalidate").validationEngine('validate');	
									$(".mmvalidationnewclass .formError").addClass("errorwidth");
								}
							} else { //for other type not restrict
								$("#modeidtype").val(3);
								$("#paymenteditvalidate").validationEngine('validate');	
								$(".mmvalidationnewclass .formError").addClass("errorwidth");
							}
						}
					}
					/* else{
						$("#modeidtype").val(3);
						$("#paymenteditvalidate").validationEngine('validate');	
						$(".mmvalidationnewclass .formError").addClass("errorwidth");
					} */
				}
			} else {
				alertpopup('Please select the account name');
			}
		
		});
		jQuery("#paymenteditvalidate").validationEngine({
			onSuccess: function() {
				$('#processoverlay').show();
				retrievestatus=1;
				netcalcheck = 1;
				retrievetagstatus=0;
				salesdetailentryeditstatus = 0;
				var stocktype=$('#stocktypeid').find('option:selected').val();
				var paymentstocktype = $('#paymentstocktypeid').find('option:selected').val();
				var paymentaccountid = $('#paymentaccountid').find('option:selected').val();
				var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				var issuereceiptid = $('#issuereceipttypeid').find('option:selected').val();
				$('#stocktypeid').select2('val','');
				$('#modeid').select2('val','');
				$('#paymentmodeid').select2('val',3);
				if(paymentstocktype == 26 || paymentstocktype == 31 || paymentstocktype == 39 || paymentstocktype == 37 || paymentstocktype == 38 || paymentstocktype == 49){
					$('#paymentreferencedate').val('0000-00-00');
				}
				if(paymentstocktype == 39 || paymentstocktype == 37 || paymentstocktype == 38 || paymentstocktype == 49){
					$('#paymentaccountid').select2('val','');
				}
				if(salestransactiontypeid != 24 && paymentstocktype == 31)
				{
					$('#paymentaccountid').select2('val','');
				}
				if(paymentstocktype != 49) 
				{
					$('#newpaymenttouch').val(0);
				}
				formdatatransfertogrid(operationstone,0);
				$('#dataaddsbtn').show();
				/* Data row select event */
				datarowselectevt();
				//clearform('paymentaddform');
				$('#bankname,#paymentrefnumber,#approvalcode').val('');
				$('#cardtype').select2('val','');
				operationstone = 'ADD';
				$('#paymentdetailupdate').hide();
				$('#salesdetailentryedit,#salesdetailentrydelete').show();
				if(paymentsavebutton == 1) {
					$('#paymentdetailsubmit').hide();
				} else {
					$('#paymentdetailsubmit').show();
				}
				$("#paymentmodeid,#sumarydiscountamount,#roundvalue").attr('readonly',false);
				$("#paymentstocktypeid,#sumarytaxamount,#paymentproduct,#paymentpurity,#paymentcounter").prop('disabled',false);
				
				if(salestransactiontypeid == 22 || salestransactiontypeid == 23)
				{
					iraccounthideshow();
				}
				finalsummarycalc(0);
				var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
				if((salestransactiontypeid == 22 && ratecuttypeid == 2) || (salestransactiontypeid == 23 && ratecuttypeid == 2)){ // ratecut in payment i/r is active
					$('#dataaddsbtn').trigger('click');
				}
				$('#editpaymenttotalamount,#chitamount,#totalpaidamount,#chitgram,#grambonus,#editpaymentpureweight').val(0);
				$('#chitbookno,#chitbookname,#paymentcomment').val('');
				if(paymentstocktype == 49) {
					$('#paymentproduct,#paymentpurity,#paymentcounter').select2('val','');
					$('#paymentgrossweight,#paymentnetweight,#paymentmelting,#newpaymenttouch').val(0);
				}
				if(paymentstocktype == 37 || paymentstocktype == 38) {
					$('#paymentratepergram,#paymenttotalamount,#paymentpureweight').val(0);
				}
				if(salestransactiontypeid != 22 && salestransactiontypeid != 23) {
					$('#stocktypeid').select2('val',stocktype).trigger('change');
				}
				if(salestransactiontypeid != 24 && paymentstocktype == 31) {
					paymentstocktype = 26;
				}else{
					paymentstocktype = paymentstocktype;
				}
				$('#paymentstocktypeid').select2('val',paymentstocktype).trigger('change');
				$('#paymentaccountid').select2('val',paymentaccountid);
				$('#salesdetailentryedit,#salesdetailentrydelete').show();
				if(paymentsavebutton == 1) {
					$('#paymentdetailsubmit').hide();
				} else {
					$('#paymentdetailsubmit').show();
				}
				oldsalesreturntax = 0;
				if(salestransactiontypeid == '24' || salestransactiontypeid == '26') {
					if(salestransactiontypeid == '24') {
						$('#paymentaccountid').select2('val',''); 
					}
					$('#issuereceipttypeid').select2('val',issuereceiptid).trigger('change');
				}
				$('#processoverlay').hide();
			},
			onFailure: function() {	
				var dropdownid =['1','paymentstocktypeid'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{// dd change
	// stocktype vishal
	$('#stocktypeid').change(function() {
		var stktype = $('#stocktypeid').find('option:selected').val();
		if(stktype != '') {
		if(checkVariable('stocktypeid') == true) {
			$("#taxamount").attr('readonly',true);
			$("#grossamount").val(0).trigger('change');
			$('#prewastage,#premakingcharge,#preflatcharge,#prelstcharge,#prerepaircharge,#totalamount,#itemwastagevalue,#itemmakingvalue,#itemflatvalue,#itemlstvalue,#itemrepairvalue,#itemchargesamtvalue').val(0);
			$('#oldjewelnewrate').text('');
			$("#totpieces,#caratwt,#totcwt,#totwt,#totamt,#totdiapieces,#totcspieces,#totpearlpieces,#totdiacaratwt,#totcscaratwt,#totpearlcaratwt,#totdiawt,#totcswt,#totpearlwt,#totdiaamt,#totcsamt,#totpearlamt").html(0);
			var transaction = $('#salestransactiontypeid').find('option:selected').val();
			clearform('clearitemdetailstab');
			$('#wastage,#makingcharge,#flatcharge,#lstcharge,#wastageclone,#makingchargeclone,#lstchargeclone,#repaircharge,#totalchargeamount,#hflat,#cflat,#hpiece').val(0);
			$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
			$("#productaddonform .chargedetailsdiv").addClass('hidedisplay');
			$('#stocknetweight,#stockgrossweight,#stockpieces').val(0);
			$('#netweight_validatewt,#grossweight_validatewt,#pieces_validatewt').text('');
			$('#proditemrate-div').hide();
			$('#proditemrate').attr('data-validation-engine','');
			{ // transactionmode setting				
				var transactiontype = $('#salestransactiontypeid').find('option:selected').data('label');
				var transactionpremode = $('#transactionmodeid').find('option:selected').val();
				if(transactionpremode == 2 || transactionpremode == 3) {
					var transactionmode = transactionpremode;
				} else {
					var transactionmode = $('#salestransactiontypeid').find('option:selected').data('transactionmode');
				}
			}
			{ // fieldruleset vishal future
				var type = $('#stocktypeid').find('option:selected').data('label');
				$('#stocktypelabel').val(type);	
				var modeid = $('#modeid').val();
				var paymenttypes = ['26', '27', '28', '29', '30', '31', '37', '38','57','58','49','78','79'];
				if (jQuery.inArray(stktype, paymenttypes) !='-1') {
					var mtype = "payment_"+type+"";
				} else {
					var mtype = ""+transactiontype+"_"+type+"";
				}
				fieldruleset('stocktypeproperty',mtype);
				var calctype = $('#transactionmodeid').find('option:selected').val();
				if(calctype == 2) {
					$('#grossamount-div').show();
					$('#grossamount').attr('data-validation-engine','validate[custom[number],min[1],decval['+roundamount+']]');
				}else if(calctype == 3) {
					//wastagecalctype = 5;
					$('#grossamount-div').hide();
					$('#grossamount').attr('data-validation-engine','');
				}else if(calctype == 4) {
					$('#grossamount-div').show();
					$('#grossamount').attr('data-validation-engine','validate[custom[number],decval['+roundamount+']]');
				}
			}
			{ // load approval number details
				var accountid = $('#accountid').val();
				if(stktype == 25 || stktype == 77 || stktype == 65 || stktype == 66 || stktype ==74 || stktype == 16) {
					$('#approvalnumber').select2('val','');
					loadapprovalnumber(stktype,accountid);
				}
			}
			{ // sales return withbill or withoutbill bill no dd show hide sales estimate
				if(stktype == 20) {
					if(transaction == 11 || transaction == 16) {
						var salesbillingtype = $("#billingtype").val();
						if(salesbillingtype == 2) {
							$("#billnumberid-div").removeClass('hidedisplay');
							$('#billnumberid').attr('data-validation-engine','validate[required]');
							retrieveallbillnumberdetails();
						} else {
							$("#billnumberid-div").addClass('hidedisplay');
							$('#billnumberid').attr('data-validation-engine','');
						}
					}
				} else {
					$("#billnumberid-div").removeClass('hidedisplay');
					$("#billnumberid-div").addClass('hidedisplay');
					$('#billnumberid').attr('data-validation-engine','');
				}
			}
		}
		{ // assorted
			purityshowhide('purity','');
			if(retrievetagstatus == 0) {
				$('#pieces').val(1);//pieces.
			}
			if(approvalnoeditstatus == 0) {
				$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
			}
		}
		{ // stocktype change
			if(stktype == 19) { //for old jewel
				//$("#grossamount-div").show();
				$('#olditemdetails').prop('readonly',false);
				var oldjewelproducttype = $('#oldjewelproducttype').val();
				if(oldjewelproducttype == 1) {
					$("#product-div").hide();
					$("#product").attr('data-validation-engine','');	
				} else if(oldjewelproducttype == 2) {
					$("#product-div").show();
					$("#product").attr('data-validation-engine','validate[required]');
					oldjewelhideshow('product',3);
				} else if(oldjewelproducttype == 3) {
					$("#product-div").show();
					$("#product").attr('data-validation-engine','validate[required]');
					oldjewelsshowallproducts('product');
				}
				if($('#oldjewelvoucherstatus').val() == '1'){ // old item voucher
					$("#olditemvoucher-div").removeClass('hidedisplay');
				}else{
					$("#olditemvoucher-div").addClass('hidedisplay');
				}
			} else if(stktype == 17 || stktype == 80 ||  stktype == 81 ||  stktype == 73) { //for P.Amt
				if(stktype == 17 || stktype == 81 || (calctype == 2 && stktype == 73) || (calctype == 4 && stktype == 73)) {
					$("#grossamount-div").show();
					$('#totalamount-div').show();
					//$('#totalamount').prop('readonly',true);
					$('#totalamount').attr('data-validation-engine','');
				} else if(stktype == 80 || stktype == 73) {
					$("#grossamount-div").hide();
					$('#totalamount-div').hide();
					//$('#totalamount').prop('readonly',false);
					$('#totalamount').attr('data-validation-engine','');
				}
				if(stktype == 73){
					$("#product-div,#counter-div").show();
					$("#product,#counter").attr('data-validation-engine','validate[required]');
					$("#category-div").hide();
					$("#category").attr('data-validation-engine','');
					//bullionproductshowhide('product','');
				}else{
					var purchasedisplay = $('#purchasedisplay').val();
					if(purchasedisplay == 0) { //for product and storage hide
						$("#product-div,#counter-div,#category-div").hide();
						$("#product,#counter,#category").attr('data-validation-engine','');
						$("#product").select2('val','3').trigger('change');
					} else if(purchasedisplay == 1) { //for product and storage show
						$("#counter").attr('data-validation-engine','');
						if(counterstatus == 'YES') {
							$("#counter-div").show();
							$("#counter").attr('data-validation-engine','validate[required]');
						} else {
							$("#counter-div").hide();
							$("#counter").attr('data-validation-engine','');
						}
						$("#product-div").show();
						$("#product").attr('data-validation-engine','validate[required]');
						$("#category-div").hide();
						$("#category").attr('data-validation-engine','');
						if(retrievetagstatus == 0) {
							loadproductfromstock(20,'product'); // for showing all products send as salesreturn stocktype
						}
						//bullionproductshowhide('product','');
					} else if(purchasedisplay == 2) { // for category show
						$("#product-div,#counter-div").hide();
						$("#product,#counter").attr('data-validation-engine','');
						$("#product").select2('val','3').trigger('change');
						$("#category-div").show();
						$("#category").attr('data-validation-engine','validate[required]');
					}
				}
			} else {
				//$("#grossamount-div").show();;
				$("#product-div").show();
				$("#olditemvoucher-div").addClass('hidedisplay');
			}
			{ //tax discount div itemlevel bill level show hide
				if(transaction != 13) {
					//Tax type
					var salestaxtypeid = $("#taxtypeid").val();
					if(salestaxtypeid == 2) { // item level
						if(transactionpremode == 2){
							if(stktype == 19 || stktype == 73){
								$("#taxamount-div").hide(); // old jewel we have not yet done tax concept
							}else{
								$("#taxamount-div").show();
							}
						} else {
							$("#taxamount-div").hide();
						}
					} else if(salestaxtypeid == 3) { // bill level
						$("#taxamount-div").hide();
					}
					//Discount type
					var discounttypeid = $("#sdiscounttypeid").val();
					var discountdisplayid = $("#discountdisplayid").val();
					if(discounttypeid == 2) { // item level
						if(transactionpremode == 2) {
							if(discountdisplayid == 2) { // disable
								$('#discount-div').hide();
							} else {  // enable
								$('#discount-div').show();
							}
						} else {
							$('#discount-div').hide();
						}
					} else if(discounttypeid == 3) { // bill level
						$('#discount-div').hide();
					}
					//Both taxtype and discount type
					if(salestaxtypeid == 3 && discounttypeid == 3) {
						$('#totalamount-div').hide();
					} else {
						if(transactionpremode == 2){
							$('#totalamount-div').show();
						} else{
							$('#totalamount-div').hide();
						}
					}
				}
			}
			// weight sales and place order appout return, Place repair
			if(transaction == 13 || transaction == 21 || stktype == 65 || stktype == 66 || stktype == 24 || stktype == 25 || transaction == 29) {
			   $('#grossamount-div,#discount-div,#taxamount-div,#totalamount-div,#stonecharge-div').hide();
			   $('#grossamount,#totalamount').attr('data-validation-engine','');
			}
		}
		{//for counter load
			if(stktype == 19) { // old jewel
				countershowhide('counter',2);
				//counterloaddata(stktype);
			} else if(stktype == 20) { // sales return
				countershowhide('counter',3);
				countershowhide('balancecounter',3);
			} else if(stktype == 13) { // partial tag
				countershowhide('balancecounter',5);
				countershowhide('counter',0);
			} else if(stktype == 73) { // purchanse return
				countershowhide('counter',4);
			} else if(stktype == 17 || stktype == 80 || stktype == 81) {
				countershowhide('counter',10);
			} else { //other type
				countershowhide('counter',0);
			}
		}
		{ // for place order and take order and pur return load product
			// untag purchanse return sales return untag app out
			if(stktype == 12 || stktype == 73 || stktype == 20 || stktype == 75 || stktype == 63 || stktype == 24 || stktype == 25 || stktype == 86) {
				if(retrievetagstatus == 0) {
					loadproductfromstock(stktype,'product');
				}
			}
			{//app out return for rfid
				if(stktype == 65 && approvaloutrfid == 3) {
					$('#approvalrfid-div').show();
					$('#approvalrfid').select2('val','1').trigger('change');	 
				} else {
					$('#approvalrfid-div').hide();
				}
			}
		}
		{ // purewt  showhide
			if(stktype == 17) {
				//wastagecalctype = 5;
				$('#pureweight-div').hide();
				$('#pureweight').attr('data-validation-engine','');
			} else if(stktype == 80) {
				$('#pureweight-div').show();
				$('#pureweight').attr('data-validation-engine','validate[required,min[0.001]]');	
			} else if(stktype == 81) {
				$('#pureweight-div').show();
				$('#pureweight').attr('data-validation-engine','validate[required,min[0.001]]');						
			} else {
				if(transactionpremode == 2) { //amount
					$('#pureweight-div').hide();
					$('#pureweight').attr('data-validation-engine','');				
				} else  { //pure wt and pure wt with amt
				   $('#pureweight-div').show();
				   $('#pureweight').attr('data-validation-engine','validate[required,min[0.001]]');
				}
			}
			if(wastagecalctype == 5 || wastagecalctype == 3) {
				$('#paymenttouch-div').show();
				$('#paymenttouch').attr('data-validation-engine','validate[required,min[1],max[150],decval['+roundmelting+']]');
			} else {
				$('#paymenttouch-div').hide();
				$('#paymenttouch').attr('data-validation-engine','');
			}
			if(stktype == 19) { //old jewel
				var oldpurchasetouch = $('#salestransactiontypeid').find('option:selected').data('oldpurchasetouch');
				//** Touch Field enable for Old Stock type. For wastage calculation type - Touch **//
				if(oldpurchasetouch == 1 && (transactionmode == 3 || transactionmode == 4) && (wastagecalctype == 5 || wastagecalctype == 3)) {
					$('#paymenttouch-div').show();
					$('#paymenttouch').attr('data-validation-engine','validate[required,min[1],max[150],decval['+roundmelting+']]');
				} else {
					$('#paymenttouch-div').hide();
					$('#paymenttouch').attr('data-validation-engine','');
				}
			}
		}
		{// stone related show hide
			if(stktype == 11 || stktype == 13 || stktype == 62) { // tag,partial & approval out tag sales
				$('#stoneweighticon').show();
				$('#stoneoverlayicon').hide();
				$('#stonecharge').attr('readonly',true);
			} else if(stktype == 12 && transaction == 20) { // untag & take order
				$('#stoneoverlayicon').show();
				$('#stonecharge').attr('readonly',true);
				$('#stoneweighticon').hide();
			} else if(stktype == 12 && untagstoneenable == '1' ) { // tag,partial & approval out tag sales
				$('#stoneweighticon').show();
				$('#stoneoverlayicon').show();
				$('#stonecharge').attr('readonly',true);
			} else {
				$('#stoneweighticon').hide();
				$('#stoneoverlayicon').hide();
				$('#stonecharge').attr('readonly',false);
			}
			cleargriddata('stoneentrygrid');
		}
		{// for partial tag pending product name display
			if(stktype == 13){
				$("#balancecounter-div").removeClass('hidedisplay');
				$("#balaceproductid-divid").removeClass('hidedisplay');
				$("#balaceproductid,#balancecounter").attr('data-validation-engine','validate[required]');
			} else {
				$("#balancecounter-div").addClass('hidedisplay');
				$("#balaceproductid-divid").addClass('hidedisplay');
				$("#balaceproductid,#balancecounter").attr('data-validation-engine','');
			}
		}
		{// App Out tag & untag
			if((stktype == 62 && approvaloutcalculation == 'NO') || (stktype == 63 && approvaloutcalculation == 'NO' || stktype == 24 || stktype == 25)) {
				$('#ratepergram-div,#wastage-div,#makingcharge-div,#cdicon-div,#stonecharge-div,#grossamount-div').hide();
				$('#taxamount-div,#discount-div,#totalamount-div,#pureweight-div,#paymenttouch-div').hide();
			}
		}
		$('#ratedefaulticon').hide();
		{//for old jewel olditemdetail show hide autoset
			if(stktype == 19) { 
				if(olditemdatafirst != '') {
					$('#tagolditemdetails').select2('val',olditemdatafirst).trigger('change');	
				} else {
					$("#taxamount").attr('readonly',true);
				}
				if(oldjeweldetails == '1') { // old jewel details
				   $('#olditemdetails').select2('val',2).trigger('change');
					$("#olditemdetails-div").removeClass('hidedisplay');
				} else {
				   $("#olditemdetails-div").addClass('hidedisplay');
				   $("#olditemdetails-div").css("display", "none");
				   $('#olditemdetails').select2('val',2).trigger('change');
				}
			}
		}
		{ // ordernumber div show hide /  load otag receve orders / bill disc payment summary show hide
			if(transaction == '11' || transaction == '16') {
				if(stktype == '83') { // tag order
					$("#ordernumber-div").show();
					loadtakeordernumber(5); // load receive order number - For delivery
				} else if(stktype == '89') { // Repair Item
					$("#ordernumber-div").show();
					loadtakeordernumber(4);
				} else {
					$("#ordernumber-div").hide();
				}
			}
			if(transaction == 20) { // take Order
				if(takeorderduedate == '15') {
					getcurrentsytemdatewithndays("orderduedate",15);
				} else if(takeorderduedate == '7') {
					getcurrentsytemdatewithndays("orderduedate",7);
				}
				getcurrentsytemdate("vendororderduedate");
				if(stktype == 82) { // Receive order
					$("#ordernumber-div").show();
					$('#orderduedate,#orderitemratefix,#vendororderduedate,#ordervendorid').attr('data-validation-engine','');
					$('#orderduedate-div,#orderitemratefix-div,#vendororderduedate-div,#ordervendorid-div').hide();	
					loadtakeordernumber(3); // load place order number - For Receive
					$('#paymentformhideid,#summaryheaderform').hide();
					if(receiveorderconcept == 0) {
						$('#itemtagnumber-div').show();
						$('#itemtagnumber').attr('data-validation-engine','validate[required]');
					} else {
						$('#itemtagnumber-div').hide();
						$('#itemtagnumber').attr('data-validation-engine','');
					}
				} else {
					$('#grossamount-div').show();
					$("#ordernumber-div").hide();
					if(accountsummaryinfo == 1) {
						$('#summaryheaderform').hide();
					} else {
						$('#summaryheaderform').show();
					}
					$('#paymentformhideid').show();
					if(stktype == 75) { // Take Order
						$('#orderduedate-div,#vendororderduedate-div,#ordervendorid-div').show();	
						$('#orderduedate,#vendororderduedate').attr('data-validation-engine','validate[required]');
						$('#orderitemratefix-div').show();
						$('#orderitemratefix').attr('data-validation-engine','validate[required]');
						$('#orderitemratefix').select2('val',2);
						if(takeordercategory == 1) {
							$('#category-div').show();
							$('#category').select2('val','');
							$('#category').attr('data-validation-engine','validate[required]');
						} else {
							$('#category-div').hide();
							$('#category').attr('data-validation-engine','');
						}
						if(checkVariable('stonecharge') == true) {
							$("#stonecharge").attr('readonly', true);
						}
					} else {
						$('#orderitemratefix').select2('val',2);
						$('#orderduedate-div,#vendororderduedate-div,#ordervendorid-div').hide();	
						$('#orderduedate,#vendororderduedate').attr('data-validation-engine','');
						$('#orderitemratefix').attr('data-validation-engine','');
						$('#orderitemratefix-div').hide();
					}
				}
			} else if(transaction == 21 || transaction == 29) { // Place Order & Place Repair
				$('#grossamount-div').hide();
				$('#paymentformhideid').hide();
				if(stktype == 76 || stktype == 87) {
					$('#ordermodeid').select2('val',3).trigger('change');
					$('#ordermodeid-div').addClass('hidedisplay');
				}
			} else if(transaction == 11) { // Sales
				if(accountsummaryinfo == 1) {
					$('#summaryheaderform').hide();
				} else {
					$('#summaryheaderform').show();
				}
				$('#paymentformhideid').show();
			} else if(transaction == 27) { // Generate Place order
				if(stktype == 85) {
					$('#stocktypeid_div').addClass('hidedisplay');
					$("#ordernumber-div").show();
					loadtakeordernumber(4); // load place order number - For Generate PO
					$('#paymentformhideid,#summaryheaderform').hide();
				}
			} else if(transaction == 28) { // Take Repair
				if(stktype == 86) {
					if(takeorderduedate == '15') {
						getcurrentsytemdatewithndays("orderduedate",15);
					} else if(takeorderduedate == '7') {
						getcurrentsytemdatewithndays("orderduedate",7);
					}
					if(takeordercategory == 1) {
						$('#category-div').show();
						$('#category').select2('val','');
						$('#category').attr('data-validation-engine','validate[required]');
					} else {
						$('#category-div').hide();
						$('#category').attr('data-validation-engine','');
					}
				}
			} else if(transaction == 30) { // Receive Repair
				if(stktype == 88) {
					$('#stocktypeid_div').addClass('hidedisplay');
					$("#ordernumber-div").show();
					loadtakeordernumber(3); // load place repair number - For Receive repair
					if(takeordercategory == 1) {
						$('#category-div').show();
						$('#category').select2('val','');
						$('#category').attr('data-validation-engine','validate[required]');
					} else {
						$('#category-div').hide();
						$('#category').attr('data-validation-engine','');
					}
				}
			}
		}
		{ // set purchase product or lot wise purchasewastage
			if(transaction == 9) { // purchase
				if(purchasewastage == 'Yes') {
					$('#wastageweight-div').show();
					$('#paymenttouch').attr('data-validation-engine','validate[decval['+roundweight+']]');
				}else{
					$('#wastageweight-div').hide();
					$('#paymenttouch').attr('data-validation-engine','');
				}
				$("#purchasemode").select2('val',2);
				$('#purchasemode-div').hide();
				// lot type hide/show based on companysetting - Lot creation mode
				if(lottype == 4) { //  both
					$('#lottypeid-div').show();
				}else if(lottype == 2) { // automatic
					$('#lottypeid').select2('val',lottype);
					$('#lottypeid-div').hide();
				}else if(lottype == 3) { // Manual
					$('#lottypeid').select2('val',lottype);
					$('#lottypeid-div').hide();
				}
			}
		}
		{ // assorted
			if(addsalesstatus == 1){
				//setTimeout(function() {
				  $('#employeepersonid').select2('val',$('#hiddensalesmanid').val());
				//},100);
			} 
			/* if(salesdetailcreatestatus == 0) { 
				if(transaction == 23 || transaction == 22) {
					$('#paymentirtypeiddiv').show();
					$('#paymentirtypeid').select2('val',2).trigger('change');
				}else {
					$('#paymentirtypeiddiv').hide();
				}
			} */
			if(stktype == 20) {
				if(transaction == 11 || transaction == 16) {
					var salesbillingtype = $("#billingtype").val();
					if(salesbillingtype == 2) {
						$("#product-div,#purity-div,#counter-div,#grossweight-div,#stoneweight-div,#netweight-div,#pieces-div,#ratepergram-div,#wastage-div,#makingcharge-div,#repaircharge-div,#flatcharge-div,#cdicon-div,#grossamount-div,#taxamount-div,#discount-div,#totalamount-div,#stonecharge-div,#itemcaratweight-div").hide();
					}else{
						if(transactionpremode == 2){
							$("#taxamount-div,#discount-div,#totalamount-div").show();
							$('#taxamount').prop('readonly','');
						}
					}						
				}
			}
			if(stktype == 86 || stktype == 88 || stktype == 89) {
				$("#ratepergram-div").hide();
			}
			if(transaction == 13) { // weight sales
				$('#itemtagnumber').focus();
			}
			if(transaction == 16 && estimatequicktotal == 1 && stktype == 11) { // estimatequicktotal
				if(transactionpremode == 2 || transactionpremode == 4){
					$('#fasttotalfortagamt-div').show(); 
				}
				if(transactionpremode == 3){
					$('#fasttotalfortagwt-div').show();
					$('#fasttotalfortagamt-div').hide(); 				
				}
			} else {
				$('#fasttotalfortag-div').hide(); 
			}
			if(stktype == 65) {
				$('#addaccount,#accountsearchevent').hide();
				$("#product-div,#salesdetailentryedit").hide();
				
			} else if(stktype == 62) {
				if(retrievetagstatus == 0){
					$('#addaccount,#accountsearchevent').show();
				}
				if(approvaloutcalculation == 'NO') {
					$("#salesdetailentryedit").hide();
				} else {
					$("#salesdetailentryedit,#ratepergram-div").show();
					$("#ratepergram").attr('readonly', true);
				}
			} else if(stktype == 76) {
				$("#salesdetailentryedit").hide();
			} else {
				$("#salesdetailentryedit").show();
			}
		}
		Materialize.updateTextFields();
	 }
	});
	
	$('#paymentstocktypeid').change(function(){
		var pstktype = $('#paymentstocktypeid').find('option:selected').val();
		var transaction = $('#salestransactiontypeid').find('option:selected').val();
		if(pstktype != '') {
		   if(pstktype == 31 && salesdetailentryeditstatus == 0 && transaction != 24) { // credit no adj only one entry can be done at a time
				var gridrowdata = getgridallrowids('saledetailsgrid');
				var takeordersalesdetailid = gridrowdata.toString();
				var takeordersalesdetails=takeordersalesdetailid.split(",");
				$.each(takeordersalesdetails, function (index, value) {
					var takeordertypeid = getgridcolvalue('saledetailsgrid',value,'paymentstocktypeid','');
					if(takeordertypeid == '31'){
						alertpopup('You can Add only One Credit Adj Entry. If u Want to add more kindly edit the previous entry.');
						$('#paymentstocktypeid').select2('val',defaultsetpaymentstocktype).trigger('change');
						return false;
					}
				});
			}
			{// payment form defaults settings 
				if(checkVariable('paymentstocktypeid') == true) {
				{// defaults 
					$('#chitamount,#totalpaidamount,#paymentratepergram').val(0);
					$('#chitbookno,#chitbookname,#paymentcomment').val('');
					purityshowhide('paymentpurity','');
					$("#paymentpureweight").attr('readonly', true);
					$("label[for='paymenttotalamount']").html("P.Amt");
					getcurrentsytemdate("paymentreferencedate");
					$('#paymentemployeepersonid').select2('val',$('#hiddenemployeeid').val()).trigger('change');
					$("#paymentaccountdiv").removeClass("hidedisplay");
					$('#paymentaccountid,#chitschemetypeid').attr('data-validation-engine','');
					$("#paymentaccountid option").addClass("ddhidedisplay").attr('disabled', true);
					var pedingamount = $('#spanbillcloseamt').text();
					$("#paymentgrossamount-div").addClass('hidedisplay');
					$("#paymentproduct-div").hide();
					$('#paymentrefnumber').val('');
					$("#paymentrefnumber").attr('maxlength','');
					$('#paymentstocknetweight,#paymentstockgrossweight,#paymentstockpieces').val(0);
					$('#paymentnetweight_validatewt,#paymentgrossweight_validatewt,#paymentpieces_validatewt').text('');
					$("#journalaccountiddiv").addClass("hidedisplay");
				}
				{ // other defaults
					var stktype = $('#paymentstocktypeid').find('option:selected').val();
					var type = $('#paymentstocktypeid').find('option:selected').data('label');
					$('#stocktypelabel').val(type);				
					var transactiontype = $('#salestransactiontypeid').find('option:selected').data('label');
					var transactionpremode = $('#transactionmodeid').find('option:selected').val();
					if(transactionpremode == 2 || transactionpremode == 3) {
						var transactionmode = transactionpremode;
					}else{
						var transactionmode = $('#salestransactiontypeid').find('option:selected').data('transactionmode');
					}
					{// fieldruleset
						var modeid = $('#paymentmodeid').val();
						var paymenttypes = ['26', '27', '28', '29', '30', '31', '37', '38','57','58','49','78','79'];
						if (jQuery.inArray(stktype, paymenttypes) !='-1') {
							var mtype = "payment_"+type+"";
						} else {
							var mtype = ""+transactiontype+"_"+type+"";
						}
						paymentfieldruleset('stocktypeproperty',mtype);
					}
					if(stktype == '49'){ // purewt payment
						$('#newpaymenttouch').val(100);
					}
				}
				}
			}		
			{// stocktype based if loops
				if(stktype == 27){ //cheque
					$("#paymentrefnumber").attr('maxlength','6');
					$("label[for='paymentrefnumber']").html("Cheque No");
					$('#paymentrefnumber-div label').append('<span class="mandatoryfildclass">*</span>');
					$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
					$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
				} else if(stktype == 28){ // dd
					$("#paymentrefnumber").attr('maxlength','50');
					$("label[for='paymentrefnumber']").html("DD No");
					$('#paymentrefnumber-div label').append('<span class="mandatoryfildclass">*</span>');	
					$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
					$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
				} else if(stktype == 29){ //card
					$("#paymentrefnumber").attr('maxlength','50');
					$("label[for='paymentrefnumber']").html("Card No");
					$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
					$("#paymentaccountid").select2('val',defaultbankid).trigger('change');	
					$("#cardtype option").addClass("ddhidedisplay").attr('disabled', true);
					$('#cardtype').select2('val','');
					$("#cardtype option[data-cardgroupid=2]").removeClass("ddhidedisplay").attr('disabled', false);
				} else if(stktype == 79 ){ // NEFT / IMPS
					$("#paymentrefnumber").attr('maxlength','50');
					$("label[for='paymentrefnumber']").html("Ref No");
					$("#paymentaccountid option[data-accounttypeid=18]").removeClass("ddhidedisplay").attr('disabled', false);
					$("#paymentaccountid").select2('val',defaultbankid).trigger('change');
					$("#cardtype option").addClass("ddhidedisplay").attr('disabled', true);
					$('#cardtype').select2('val','');
					$("#cardtype option[data-cardgroupid=3]").removeClass("ddhidedisplay").attr('disabled', false);
				}else if(stktype == 26) { //cash
					$("#paymentrefnumber").attr('maxlength','');
					$("label[for='paymentrefnumber']").html("Number");
					$("#paymentaccountid option[data-accounttypeid=19]").removeClass("ddhidedisplay").attr('disabled', false);
					$("#paymentaccountid").select2('val',defaultcashinhandid).trigger('change');
				} else if(stktype == 39) { //Saving Chit
					$("#paymentrefnumber").attr('maxlength','');
					var schememethodid = $("#schememethodid").val();
					$("#chitbookname").prop('readonly',false);
					$('#paymentaccountid,#chitschemetypeid').attr('data-validation-engine','');
					if(schememethodid == 3) { // automatic
						$("#chitschemetypeid-div").hide();
						$("#chitbookname,#chitamount,#chitgram,#grambonus").prop('readonly',true);
						$("#chitamount-div,#totalpaidamount-div,#chitgram-div").show();
						$("#grambonus-div").hide();
					} else { //manual
						//$("#chitschemetypeid-div").show();
						$("#chitbookname,#chitamount,#totalpaidamount,#chitgram").prop('readonly',false);
						
					}
					$("#paymentaccountdiv").addClass("hidedisplay");
				}  else if(stktype == 31 ){ // advance
					$("#paymentrefnumber").attr('maxlength','50');
					$('#advanceavailable-div,#advancebalance-div').hide();
					$('#paymentaccountid').attr('data-validation-engine','');
					$("#paymentaccountdiv").addClass("hidedisplay");
					if(transaction == 24){
						$("#journalaccountiddiv").removeClass("hidedisplay");
						$('#paymenttotalamount').attr('data-validation-engine','validate[required,custom[number],min[1],decval['+roundamount+'],maxSize[15]]');
					}
				}  else if(stktype == 37){ // Amt To Purewt
					$("#paymentrefnumber").attr('maxlength','');
					$("label[for='paymentrefnumber']").html("Refer No");
					$('#paymentpureweight-div').show();
					$("#paymentpureweight").attr('readonly', true);
					$("#paymentpureweight").val(0);
					$("#paymenttotalamount").attr('readonly', false);
					$("#paymentaccountdiv").addClass("hidedisplay");
				} else if(stktype == 38){ // Purewt To Amt
					$("#paymentrefnumber").attr('maxlength','');
					$("label[for='paymentrefnumber']").html("Refer No");
					$('#paymentpureweight-div').show();
					$("#paymenttotalamount").val(0);
					$("#paymentpureweight").attr('readonly', false);
					$("#paymenttotalamount").attr('readonly', true);
					$("#paymentaccountdiv").addClass("hidedisplay");
			   } else if(stktype == 49 ){ //purewt payment
					$("#paymentrefnumber").attr('maxlength','');
					$("#paymentproduct-div").show();
					$('#paymentcounter-div').show();
					$('#paymentpureweight-div').show();
					$("#paymentaccountdiv").addClass("hidedisplay");
					$('#paymentpureweight').attr('data-validation-engine','validate[required,custom[number],min[0.001],decval['+round+']]');
			   }
			}
			{ // set netamt in payment form netamt from summary billnetamt
				if(transaction == 22 || transaction == 23 || stktype == 39 ) { // pi /pr / chit
					$('#paymenttotalamount').val(0);
				} else {
					summarybillbalamt = $('#sumpendingamt').text();
					$('#paymenttotalamount').val(Math.abs(summarybillbalamt).toFixed(roundamount)); 
				}	
			}
			$("#issuereceipttypeid").prop("disabled", true);
			if(stktype == '49' && transaction == '26' ) {
				$("#issuereceipttypeid").select2('val','').trigger('change').prop("disabled", false);
				$('#paymentpureweight-div').hide();
				$('#paymentpureweight').attr('data-validation-engine','');
				
			} else if(stktype == '31' && transaction == '24' ) {
				$("#issuereceipttypeid").select2('val','3').trigger('change').prop("disabled", false);
				$("#paymentaccountid").select2('val','').trigger('change');
				$("#journalaccountid").prop("disabled", false);
				$('#s2id_journalaccountid a span:first').text('');
				$("#journalaccountid").val('');
			} else if(stktype == '49' || stktype == '38' ) {
				var wtir = $('#wtissuereceiptid').val();
				$("#issuereceipttypeid").select2('val',wtir).trigger('change');		
			} else {
				var amtir = $('#issuereceiptid').val();
				if(transaction == 25) {
					setTimeout(function() {
						$("#issuereceipttypeid").select2('val',2).trigger('change');
					},20);
					var accid = $("#accountid").val();
					if(accid != '') {
						$("#paymentaccountid option[value="+accid+"]").addClass("ddhidedisplay").attr('disabled', true);
						$("#paymentaccountid").select2('val','').trigger('change');
					}
				} else {
					$("#issuereceipttypeid").select2('val',amtir).trigger('change');
				}				
			}
			var manualstatus = accountmanualstatus();
			if(manualstatus == 0) {
				if(stktype == '38' || stktype == '37'){
					$("#issuereceipttypeid").prop("disabled", true);
					if($('#accountclosingamount').val() < 0){
						$("#issuereceipttypeid").select2('val',2).trigger('change');
					}else if($('#accountclosingamount').val() > 0){
						$("#issuereceipttypeid").select2('val',3).trigger('change');
					}
				}
			}
			setTimeout(function() {	
				Materialize.updateTextFields();
			},10);	
		}	
	});
	// payment account change
	$("#paymentaccountid").change(function(){
		var pstktype = $('#paymentstocktypeid').find('option:selected').val();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		var paymentaccountname = $(this).find('option:selected').data('paymentaccountidhidden');
		var paymenttypes = ['27', '28', '29','79'];
		if(salestransactiontypeid == '25') {
			if (jQuery.inArray(pstktype, paymenttypes) !='-1') {
				$('#bankname').val(paymentaccountname);
			} 
		}
	});
	$("#journalaccountid").change(function(){
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		var accid = getgridcolcolumnvalue('saledetailsgrid','','paymentaccountid');
		var paymentaccountid = $('#journalaccountid').val();
		var irbilltype = $("#issuereceipttypeid").find('option:selected').val();
		
		if (jQuery.inArray(paymentaccountid, accid) == '-1' || (salesdetailentryeditstatus == 1) ) {
			if(salestransactiontypeid == '24' && irbilltype > 1) {
				fetchaccountdatasetjournal($(this).val());
			}else if(salestransactiontypeid == '24') {
				$("#issuereceipttypeid").select2('val',3);
				fetchaccountdatasetjournal($(this).val());
			}
		}else {
			$('#s2id_journalaccountid a span:first').text('');
			$("#journalaccountid").val('');
			alertpopup('Already you added this account.Please select any other account');
		} 
	});
	// IR type change
	$('#issuereceipttypeid').change(function(){
		var pstktype = $('#paymentstocktypeid').find('option:selected').val();
		var transaction = $('#salestransactiontypeid').find('option:selected').val();
		var irbilltype = $("#issuereceipttypeid").find('option:selected').val();
		if(pstktype == 31 && transaction != 24) { // Credit no Adj
			var manualstatus = accountmanualstatus();
			if(manualstatus == 1) {
				$('#credittotalamount,#credittotalpurewt').val(0);
				$('#creditentryedit,#creditentrydelete,#issuereceiptpush,#creditentryadd').show();
				$('#credittotalamount,#credittotalpurewt').val(0);
				clearceditoverlayform();
				if(salesdetailentryeditstatus == 0) {
					$('#issuereceiptclose').show();	
					issuereceiptdataget(0);
				} else if(salesdetailentryeditstatus == 1) {
					$("#processoverlay").show();
					$('#issuereceiptclose').hide();
					$('#creditoverlaystatus').val(0);
					issuereceiptlocalgrid();
					var hiddentaxdata = $.parseJSON($('#creditadjusthidden').val());
					loadinlinegriddata('issuereceiptlocalgrid',hiddentaxdata.rows,'json');
					var accountid = $('#accountid').val();
					issuereceiptgrid();
					issuereceiptgriddatafetch(accountid,0);
					creditoverlaytriggerplace = 1;
					$("#issuereceiptoverlay").show();
					creditfieldhideshow();
					$('#processid').select2('val',0);
					$('#accountledgerid').val(1);
					$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',irbilltype);
					creditsummaycalc();
					$("#processoverlay").hide();
				}
		    } else {
				$("#processoverlay").hide();
				//alertpopup('You cannot select credit no Adj for Non-Billwise A/c');  
			}
		} else if(pstktype == 31 && transaction == 24) {
			var accountid = $('#paymentaccountid').find('option:selected').val();
			if(accountid > 1 && irbilltype > 1 ){
				var manualstatus = journalaccountmanualstatus();
				if(manualstatus == 1){
					$('#credittotalamount,#credittotalpurewt').val(0);
					$('#creditentryedit,#creditentrydelete,#issuereceiptpush,#creditentryadd').show();
					$('#credittotalamount,#credittotalpurewt').val(0);
					clearceditoverlayform();
					if(salesdetailentryeditstatus == 0) {
						$('#issuereceiptclose').show();	
						issuereceiptdataget(0);
					} else if(salesdetailentryeditstatus == 1) {
						$("#processoverlay").show();
						$('#issuereceiptclose').hide();
						$('#creditoverlaystatus').val(0);
						issuereceiptlocalgrid();
						var hiddentaxdata = $.parseJSON($('#creditadjusthidden').val());
						loadinlinegriddata('issuereceiptlocalgrid',hiddentaxdata.rows,'json');
						issuereceiptgrid();
						issuereceiptgriddatafetch(accountid,0);
						creditoverlaytriggerplace = 1;
						$("#issuereceiptoverlay").show();
						creditfieldhideshow();
						$('#processid').select2('val',0);
						$('#accountledgerid').val(1);
						$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',irbilltype);
						creditsummaycalc();
						$("#processoverlay").hide();
					}
				}
		    } else if(irbilltype < 2  || irbilltype == 'undefined') {
				 alertpopup('Please Select Issue / Receipt. I.R Type');  
			}
		}
	});
		$('#transactionmanageid').change(function() {
			$('#salestransactiontypeid').find('option:selected').attr('data-additionalchargeid',$('#transactionmanageid').find('option:selected').data('additionalchargeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-pureadditionalchargeid',$('#transactionmanageid').find('option:selected').data('pureadditionalchargeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-transactionmodeid',$('#transactionmanageid').find('option:selected').data('transactionmodeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-stocktypeid',$('#transactionmanageid').find('option:selected').data('stocktypeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-salesmodeid',$('#transactionmanageid').find('option:selected').data('salesmodeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-rateaddition',$('#transactionmanageid').find('option:selected').data('rateaddition'));
			$('#salestransactiontypeid').find('option:selected').attr('data-discounttype',$('#transactionmanageid').find('option:selected').data('discounttype'));
			$('#salestransactiontypeid').find('option:selected').attr('data-discountcalc',$('#transactionmanageid').find('option:selected').data('discountcalc'));
			$('#salestransactiontypeid').find('option:selected').attr('data-discountdisplayid',$('#transactionmanageid').find('option:selected').data('discountdisplayid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-roundoffremove',$('#transactionmanageid').find('option:selected').data('roundoffremove'));
			$('#salestransactiontypeid').find('option:selected').attr('data-allowedaccounttypeid',$('#transactionmanageid').find('option:selected').data('allowedaccounttypeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-chargesedit',$('#transactionmanageid').find('option:selected').data('chargesedit'));
			$('#salestransactiontypeid').find('option:selected').attr('data-autotax',$('#transactionmanageid').find('option:selected').data('autotax'));
			$('#salestransactiontypeid').find('option:selected').attr('data-headername',$('#transactionmanageid').find('option:selected').data('headername'));			
			$('#salestransactiontypeid').find('option:selected').attr('data-taxtypeid',$('#transactionmanageid').find('option:selected').data('taxtypeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-printtemplatesid',$('#transactionmanageid').find('option:selected').data('allprinttemplateid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-stocktyperelation',$('#transactionmanageid').find('option:selected').data('allstocktypeid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-transactionmanageid',$('#transactionmanageid').find('option:selected').data('transactionmanageid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-serialnumbermastername',$('#transactionmanageid').find('option:selected').data('serialnumbermastername'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metalarraydetails',$('#transactionmanageid').find('option:selected').data('metalarraydetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metalserialnumberdetails',$('#transactionmanageid').find('option:selected').data('metalserialnumberdetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metalprinttempids',$('#transactionmanageid').find('option:selected').data('metalprinttempids'));
			$('#salestransactiontypeid').find('option:selected').attr('data-defoldpurserialmaster',$('#transactionmanageid').find('option:selected').data('defoldpurserialmaster'));
			$('#salestransactiontypeid').find('option:selected').attr('data-defsalesreturnserialmaster',$('#transactionmanageid').find('option:selected').data('defsalesreturnserialmaster'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metaloldarraydetails',$('#transactionmanageid').find('option:selected').data('metaloldarraydetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metaloldserialnumberdetails',$('#transactionmanageid').find('option:selected').data('metaloldserialnumberdetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metalreturnarraydetails',$('#transactionmanageid').find('option:selected').data('metalreturnarraydetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-metalreturnserialnumberdetails',$('#transactionmanageid').find('option:selected').data('metalreturnserialnumberdetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-repairserialnumberdetails',$('#transactionmanageid').find('option:selected').data('repairserialnumberdetails'));
			$('#salestransactiontypeid').find('option:selected').attr('data-repairmetalprintid',$('#transactionmanageid').find('option:selected').data('repairmetalprintid'));
			$('#salestransactiontypeid').find('option:selected').attr('data-roundoffamount',$('#transactionmanageid').find('option:selected').data('roundoffamount'));
			$('#salestransactiontypeid').find('option:selected').attr('data-adjustmentconcept',$('#transactionmanageid').find('option:selected').data('adjustmentconcept'));
			$('#salestransactiontypeid').find('option:selected').attr('data-stoneadjustment',$('#transactionmanageid').find('option:selected').data('stoneadjustment'));
			
			/* Advance Concept which is applicable for All Transaction */
			$('#salestransactiontypeid').find('option:selected').attr('data-advanceserialno',$('#transactionmanageid').find('option:selected').data('advanceserialno'));
			$('#salestransactiontypeid').find('option:selected').attr('data-advanceprintid',$('#transactionmanageid').find('option:selected').data('advanceprintid'));
			/* Receipt Concept which is applicable for All Transaction */
			$('#salestransactiontypeid').find('option:selected').attr('data-receiptserialno',$('#transactionmanageid').find('option:selected').data('receiptserialno'));
			$('#salestransactiontypeid').find('option:selected').attr('data-receiptprintid',$('#transactionmanageid').find('option:selected').data('receiptprintid'));
			/* Issue Concept which is applicable for All Transaction */
			$('#salestransactiontypeid').find('option:selected').attr('data-issueserialno',$('#transactionmanageid').find('option:selected').data('issueserialno'));
			$('#salestransactiontypeid').find('option:selected').attr('data-issueprintid',$('#transactionmanageid').find('option:selected').data('issueprintid'));
			/* Balance Serial Number Concept which is applicable for All Transaction */
			$('#salestransactiontypeid').find('option:selected').attr('data-balanceserialno',$('#transactionmanageid').find('option:selected').data('balanceserialno'));
			$('#salestransactiontypeid').find('option:selected').attr('data-balanceprintid',$('#transactionmanageid').find('option:selected').data('balanceprintid'));
			/** Touch is enabled for Pure WT **/
			$('#salestransactiontypeid').find('option:selected').attr('data-oldpurchasetouch',$('#transactionmanageid').find('option:selected').data('oldpurchasetouch'));
			$('#salestransactiontypeid').find('option:selected').attr('data-wtpaymenttouch',$('#transactionmanageid').find('option:selected').data('wtpaymenttouch'));
			//$('#salestransactiontypeid').trigger('change');
		});
		//  Transaction Type change - main
		$('#salestransactiontypeid').change(function() {
			var val = $(this).find('option:selected').val();
			var transactiontype = $(this).find('option:selected').val();
			{// defaults
				transactionmanageidshowhide();
				ttbasedstypeshowhide();
				clearform('salesaddform');
				clearform('paymentaddform');
				clearform('chargedataform');
				$('.chargedata,.extracalc').val(0);
				$('#creditno').val('');
				$('#accountopeningamount,#accountclosingamount,#accountopeningweight,#accountopeningamountirid,#accountopeningweightirid').val(0);
				$('#spanaccopenamt,#spanacccloseamt,#spanaccopenwt,#spanaccclosewt,#accountadvamt,#accountcreditamt').text(0);
				var header_name = $('#salestransactiontypeid').find('option:selected').data('headername');
				var printtemplatesid = $('#salestransactiontypeid').find('option:selected').data('printtemplatesid');
				var name = $('#salestransactiontypeid').find('option:selected').data('name');
				var tittlename = name.toUpperCase()+' S.Neuron ';
				$(document).attr("title",tittlename );
				salesdiscounttypeid = $('#salestransactiontypeid').find('option:selected').data('discounttype');
				var discountdisplayid = $('#salestransactiontypeid').find('option:selected').data('discountdisplayid');
				discaltype = $('#salestransactiontypeid').find('option:selected').data('discountcalc');
				salestaxtypeid = $('#salestransactiontypeid').find('option:selected').data('taxtypeid');
				allowedaccounttypeid = $('#salestransactiontypeid').find('option:selected').data('allowedaccounttypeid');
				metalarraydetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metalarraydetails'));
				metaloldarraydetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metaloldarraydetails'));
				metalreturnarraydetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metalreturnarraydetails'));
				if(touchchargesedit == 1) {
					$('#paymenttouch').attr('readonly', true);
				} else {
					$('#paymenttouch').attr('readonly', false);
				}
				$("#ratecuttypeid").select2('val','1').trigger('change');
				var tranactionmodeid = $('#salestransactiontypeid').find('option:selected').data('transactionmode');
				if(val == 26) {
					tranactionmodeid = 3;
				}
				$("#transactionmodeid").select2('val',tranactionmodeid).trigger('change');
				var defaultstocktypeset = $('#salestransactiontypeid').find('option:selected').data('stocktypeid');
				$("#taxtypeid").val(salestaxtypeid);
				$("#sdiscounttypeid").val(salesdiscounttypeid);
				$("#discountdisplayid").val(discountdisplayid);
				$("#discaltype").val(discaltype);
				$("#transactionheader").text(header_name);
				if(billeditstatus == 0) {
					$('#accountid,#accounttypeid').select2('val','');
				}
				// maddy
				if(val != 25) {
					accountshowhide('accounttypeid',val,tranactionmodeid); // transaction type based accounttype show hide
				}
				clearform('productaddonform');
				// Main Header to be Declare Transaction Type
				{
					var trantypeheadername = $('#salestransactiontypeid').find('option:selected').data('name');
					var trtypeid = $(this).find('option:selected').val();
					if(trtypeid == '11') {
						trantypeheadername = trantypeheadername;
					} else if(trtypeid == '9') {
						trantypeheadername = 'Purch.';
					} else if(trtypeid == '13') {
						trantypeheadername = 'W. Sales';
					} else if(trtypeid == '16') {
						trantypeheadername = 'Estim.';
					} else if(trtypeid == '18') {
						trantypeheadername = 'App. Out';
					} else if(trtypeid == '20') {
						trantypeheadername = 'Tak. Ord';
					} else if(trtypeid == '21') {
						trantypeheadername = 'Pla. Ord';
					} else if(trtypeid == '22') {
						trantypeheadername = 'Pay. Iss';
					} else if(trtypeid == '23') {
						trantypeheadername = 'Pay. Rec';
					} else if(trtypeid == '24') {
						trantypeheadername = 'Journal';
					} else if(trtypeid == '25') {
						trantypeheadername = 'Contra';
					} else if(trtypeid == '26') {
						trantypeheadername = 'Met I/R';
					} else if(trtypeid == '27') {
						trantypeheadername = 'Gen.P.Ord';
					} else if(trtypeid == '28') {
						trantypeheadername = 'Tak. Rpr';
					} else if(trtypeid == '29') {
						trantypeheadername = 'Pla. Rpr';
					} else if(trtypeid == '30') {
						trantypeheadername = 'Rec. Rpr';
					} else {
						trantypeheadername = trantypeheadername;
					}
					if(ratedisplaymainview == 1 && userroleid != 3 && deviceinfo != 'phone' && deviceinfo != 'tablet') {
						var puritydetails = retrievelastupdatepurityvalue(ratedisplaypurityvalue);
						if(puritydetails.length == 1) {
							$('.transactiontypehead').text(trantypeheadername);
							$('.salesgridcaptionpos').text(' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' ] ');                                                
						} else if(puritydetails.length == 2) {
							$('.transactiontypehead').text(trantypeheadername);
							$('.salesgridcaptionpos').text(' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' | '+puritydetails[1]['purityshortname']+' : '+puritydetails[1]['currentrate']+' ] ');
						} else if(puritydetails.length == 3) {
							$('.transactiontypehead').text(trantypeheadername);        
							$('.salesgridcaptionpos').text(' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' | '+puritydetails[1]['purityshortname']+' : '+puritydetails[1]['currentrate']+' | '+puritydetails[2]['purityshortname']+' : '+puritydetails[2]['currentrate']+' ] ');
						} else {
							$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase());			
						}
						/* if(puritydetails.length == 1) {
							$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase()+' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' ] ');
						} else if(puritydetails.length == 2) {
							$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase()+' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' | '+puritydetails[1]['purityshortname']+' : '+puritydetails[1]['currentrate']+' ] ');
						} else if(puritydetails.length == 3) {
							$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase()+' [ '+puritydetails[0]['purityshortname']+' : '+puritydetails[0]['currentrate']+' | '+puritydetails[1]['purityshortname']+' : '+puritydetails[1]['currentrate']+' | '+puritydetails[2]['purityshortname']+' : '+puritydetails[2]['currentrate']+' ] ');
						} else {
							$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase());
						} */
					} else {
						$('.salesgridcaptionpos').text(trantypeheadername.toUpperCase());
					}
				}
			}
			{ // Transaction Type based if loop
				$('#orderduedate-div,#vendororderduedate-div,#orderitemratefix-div,#ordervendorid-div').addClass('hidedisplay');
				$('#ordermodeid-div').addClass('hidedisplay');
				$("#stocktypeid_div").removeClass('hidedisplay');
				$('#orderduedate,#vendororderduedate,#orderitemratefix').attr('data-validation-engine','');
				$("#salesdetailentryedit").show();
				$("#calculationtypeid").removeClass('hidedisplay');
				$("#journalbilldatehdivid,#journalbillnohdivid").addClass('hidedisplay');
				wastagecalctype = $('#wastagecalctype').val();
				if(val == 13 || approvaloutrfid == 1){ // weight sales Sapna - arvind changes
					$('#appoutrfidbtn').hide();
					$("#rfidtagnumber").css('display','inline').css('width','');
					$('label[for="rfidtagnumber"]').css('width','');
				}
				if(val == 13) { // weight sales
					$("#salesdetailleftgrid").hide();
					$("#tabindexhideshow").attr('class','large-12 columns paddingzero');
					$("#accountiddiv,.addbtnclass,#calculationtypeid").addClass('hidedisplay');
					$('#stoneweighticon').show();
					$('#stonecharge').attr('readonly',true);
				} else if(val == 9) {//purchase
					//wastagecalctype = 5;
					$("#salesdetailleftgrid").show();
					$("#tabindexhideshow").attr('class','large-12 columns paddingzero');
					//$("#calculationtypeid").addClass('hidedisplay');
					$("#accountiddiv,.addbtnclass,#paymentstocktypeid-div").removeClass('hidedisplay');
					$('#stoneweighticon').hide();
					$('#stonecharge').attr('readonly',false);
					$('#ratepergram').attr('readonly',false);
				}  else if(val == 20) { // Take Order
					$('#orderduedate,#vendororderduedate,#orderitemratefix').attr('data-validation-engine','validate[required]');
					$('#orderduedate-div,#vendororderduedate-div,#orderitemratefix-div,#ordervendorid-div').removeClass('hidedisplay');
					$("#accountiddiv,.addbtnclass,#paymentstocktypeid-div").removeClass('hidedisplay');
				} else if(val == 28) { // Take Repair
					$('#orderduedate').attr('data-validation-engine','validate[required]');
					$('#orderduedate-div').removeClass('hidedisplay');
				} else if(val == 21 || val == 29) { // Place Order && Place Repair
					$('#ordermodeid-div').removeClass('hidedisplay');
					$("#salesdetailentryedit").hide();
					$('#salesdetailsubmit').hide();
					$('#salesdetailssubform,#salesprodformdetails').show();
					$("#accountiddiv,.addbtnclass,#paymentstocktypeid-div").removeClass('hidedisplay');
				} else if(val == 24) { // journal
					$("#accountiddiv").addClass('hidedisplay');
				} else if(val == 27) { // generate place order
					//wastagecalctype = 5;
					$('#netweight,#ratepergram').attr('disabled',true);
				} else {//other type
					$("#salesdetailleftgrid").show();
					$("#tabindexhideshow").attr('class','large-12 columns paddingzero');
					$("#accountiddiv,.addbtnclass,#calculationtypeid,#paymentstocktypeid-div").removeClass('hidedisplay');
					$('#stoneweighticon').show();
					$('#stonecharge').attr('readonly',true);				
				}
				{// salestransaction id based show/hide div
					$('#ratecuttypeid-div').hide(); 
					$('#billrefno-div').hide();
					$('#billrefnoicon,#wtcheckicon').hide(); 
					$('#billrefno,#billrefnooverlaytbox').val(''); 
					var transactionid = $('#salestransactiontypeid').find('option:selected').val();
					var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
					$('#maincommonotherwtsummary,#summaryheaderform,#paymentaddform').addClass('hidedisplay');
					$('.commonotherwtsummaryspan').text('0'); 
					//$('#discounthide').show();
					if(paymentsavebutton == 1) {
						$('#paymentdetailsubmit').hide();
					} else {
						$('#paymentdetailsubmit').show();
					}
					$("#transactionmodeid option ").removeClass("ddhidedisplay").attr('disabled', false);
					$('#paymentformhideid,#salesdetailleftgrid').removeClass('hidedisplay large-8 large-9 large-12');
					if(transactionid == 22 || transactionid == 23 || transactionid == 25 || transactionid == 24) { // payment issue/receipt
						//$('#salesdetailssubform').hide();
						$('#salesprodformdetails').hide();
						$("#paymentformhideid").show();
						//$('#discounthide').hide();
						$('#paymentaddform').removeClass('hidedisplay');
						$('#paymentformhideid').removeClass('hidedisplay large-8').addClass('large-12'); 
						$('#salesdetailleftgrid').removeClass('hidedisplay large-8').addClass('large-12');
						if(accountsummaryinfo == 1) {
							$('#summaryheaderform').hide();
						} else {
							$('#summaryheaderform').removeClass('hidedisplay large-8').addClass('large-12');
							$('#summaryheaderform .summarycontainer').removeClass('large-2').addClass('large-2');
						}
						if(transactionid == 25) { // contra
							$("#transactionmodeid option[value=3]").addClass("ddhidedisplay").attr('disabled', true);
							$("#transactionmodeid option[value=4]").addClass("ddhidedisplay").attr('disabled', true);
						} else if(transactionid == 22 || transactionid == 23) {
							$('#ratecuttypeid-div').show();
						}
					} else if (transactionid == 27) {
						if(accountsummaryinfo == 1) {
							$('#summaryheaderform').hide();
						} else {
							$('#summaryheaderform').removeClass('hidedisplay large-3').addClass('large-8');
						}
						$('#paymentformhideid').removeClass('hidedisplay large-8').addClass('large-8'); 
						$('#salesdetailleftgrid').removeClass('hidedisplay large-8').addClass('large-8'); 
					} else if (transactionid == 26) {
						//$('#salesdetailssubform').hide();
						$('#salesprodformdetails').hide();
						$("#paymentformhideid").show();
						//$('#discounthide').hide();
						$('#paymentaddform').removeClass('hidedisplay');
						$('#maincommonotherwtsummary').removeClass('hidedisplay large-12').addClass('large-12');
						$('#maincommonotherwtsummary .summarycontainer').removeClass('large-2').addClass('large-2');
					} else if (transactionid == 21) {
						$('#maincommonotherwtsummary').removeClass('hidedisplay large-9').addClass('large-8');
						$('#maincommonotherwtsummary .summarycontainer').removeClass('large-2').addClass('large-2');
					} else if (transactionid == 18) {
						$('#salesdetailssubform,#salesprodformdetails').show();
						$('#maincommonotherwtsummary').removeClass('hidedisplay large-9').addClass('large-8');
						$('#maincommonotherwtsummary .summarycontainer').removeClass('large-2').addClass('large-2');
					} else if (transactionid == 19) {
						$('#salesdetailssubform,#salesprodformdetails').show();
						$('#maincommonotherwtsummary').removeClass('hidedisplay large-9').addClass('large-12');
						$('#maincommonotherwtsummary .summarycontainer').removeClass('large-2').addClass('large-2');
					} else if (transactionid == 13) {
						$('#salesdetailssubform,#salesprodformdetails').show();
					} else if (transactionid == 16 || transactionid == 28) {
						$('#salesdetailssubform,#salesprodformdetails').show();
						$('#paymenthide').addClass('hidedisplay');
						//$('#discounthide').hide();
						$('#paymentdetailsubmit').hide();
						$('#paymentaddform,#paymentformhideid').removeClass('hidedisplay');
						if(accountsummaryinfo == 1) {
							$('#summaryheaderform').hide();
						} else {
							$('#summaryheaderform').removeClass('hidedisplay large-12').addClass('large-8');
							$('#summaryheaderform .summarycontainer').removeClass('large-2').addClass('large-2');
						}
						$('#paymentformhideid,#salesdetailleftgrid').addClass('large-8');
					} else {
						$('#salesdetailssubform,#salesprodformdetails').show();
						if(transactionmodeid == 3) {
							//$('#billwisediscounttaxform').addClass('hidedisplay');
						}
						if(transactionid == 9 || transactionid == 11 || transactionid == 20) {
							$('#paymenthide').removeClass('hidedisplay');
							$('#paymentformhideid,#salesdetailleftgrid').addClass('large-8');
						}
						//$('#discounthide').show();
						$('#paymentaddform').removeClass('hidedisplay');
						if(accountsummaryinfo == 1) {
							$('#summaryheaderform').hide();
						} else {
							$('#summaryheaderform').removeClass('hidedisplay large-9').addClass('large-2');
							$('#summaryheaderform .summarycontainer').removeClass('large-2').addClass('large-2');
						}
					}
					if(transactionid == 9 ) {
						$('#billrefno-div').show();
						if($('#weightcheck').val() == 1) {
							$('#wtcheckicon').show();
						} else if($('#weightcheck').val() == 0) {
							$('#wtcheckicon').hide();
						}
						$('#billrefnoicon').hide(); 
					} else if(transactionid == 19 || transactionid == 26) {
						$('#billrefno-div').show();  
					} else if(transactionid == 11 || transactionid == 20 || transactionid == 18 || transactionid == 21 ) {
						$('#billrefno-div,#wtcheckicon').hide();
						$('#billrefnoicon').show(); 
					}
					if(transactionid == 9 || transactionid == 11 || transactionid == 20 || transactionid == 28) {
						if(accountsummaryinfo == 1) {
							$('#summaryheaderform').hide();
						} else {
							$('#summaryheaderform').removeClass('hidedisplay large-12').addClass('large-8');
						}
					}
				}
				//$("#transactionmodeid").select2('val',tranactionmodeid).trigger('change'); 
		    }
			{// show hide paymentformhideid estimationnumber-div
				if(val == 11 && salespaymenthideshow == 1 || val == 9 && salespaymenthideshow == 1) { //Show hide payment div for loose stone items
					$('#paymenthide').addClass('hidedisplay');
				} else if(val == 20 || val == 22 || val == 23 || val == 24 || val == 25 || val == 26) {
					$('#paymenthide').removeClass('hidedisplay');
				}
				if(val == 9 || val == 11 || val == 20 || val == 21  || val == 16 || val == 28) { //  purchase, sales & take order
					$("#paymentformhideid").show();
				} else {
					$("#paymentformhideid").hide();
				}
				if(val == 11) {//for sales show estimation number
					$("#estimationnumber-div").show();
				} else {//other types hide estimation number
					$("#estimationnumber-div").hide();
				}
			}
			{ // discount tax show hide
				if(val == 11 || val == 9 || val == 16 || val == 20 || val == 28) { //sales ,purchase ,estmiate & take order, take repair
					if(val == 16) { // estmiate
						$('#billgrossamount-div,#sumarydiscountamount-div,#sumarytaxamount-div,#roundvalue-div,#billnetamount-div').removeClass('large-2 columns');				  
						$('#billgrossamount-div,#sumarydiscountamount-div,#sumarytaxamount-div,#roundvalue-div,#billnetamount-div').addClass('large-2 columns');				  
					} else {
						$('#billgrossamount-div,#sumarydiscountamount-div,#sumarytaxamount-div,#roundvalue-div,#billnetamount-div').removeClass('large-2 columns');	
						$('#billgrossamount-div,#sumarydiscountamount-div,#sumarytaxamount-div,#roundvalue-div,#billnetamount-div').addClass('large-2 columns');	
					}
					discountlimitsetuserrolewise();
					$('#transactionmodeid').prop('disabled',false);
				} else {
					$('#transactionmodeid').prop('disabled',true);
					$('#billgrossamount-div,#sumarydiscountamount-div,#sumarytaxamount-div,#roundvalue-div,#billnetamount-div').removeClass('static-field large-2 columns').addClass('static-field large-2 columns');	
				}
			}
			{// pi pr journal
				$('#innersalesicon').show();
				if(val == 22) { // payment issue
					$('#issuereceiptid,#wtissuereceiptid').val(2);
					$('#innersalesicon').hide();
					$('.billnetamountspan,.billnetwtspan').text('');
					$('#transactionmodeid').prop('disabled',false);
					$("#paymentformhideid").show();
				} else if(val == 23) { // payment receipt
				   $('#issuereceiptid,#wtissuereceiptid').val(3);
					$('#innersalesicon').hide();
					$('.billnetamountspan,.billnetwtspan').text('');
					$('.billnetamountspan,.billnetwtspan').text('');
					$('#transactionmodeid').prop('disabled',false);
					$("#paymentformhideid").show();
				} else if(val == 26) { // metal i/r
					$('#issuereceiptid,#wtissuereceiptid').val(3);
					$('#innersalesicon').hide();
					$('.billnetamountspan,.billnetwtspan').text('');
					$('.billnetamountspan,.billnetwtspan').text('');
					$('#transactionmodeid').prop('disabled',true);
					$("#paymentformhideid").show();
				} else if(val == 24) { // journal
					$('#issuereceiptid,#wtissuereceiptid').val(3);
					$('#innersalesicon').hide();
					$('.billnetamountspan,.billnetwtspan').text('');
					$('#transactionmodeid').prop('disabled',true);
					$("#paymentformhideid").show();
				} else if(val == 25) { // contra
					$('#issuereceiptid').val(3).trigger('change');
					$('#transactionmodeid').select2('val',2).trigger('change');
					$("#issuereceipttypeid").select2('val',2).trigger('change');
					$('#transactionmodeid').prop('disabled',true);
					$("#paymentformhideid").show();
					setTimeout(function(){
						$(".contrahide").hide();
					},30);
				} else if(val == 28) { // Take Repair
					$('#transactionmodeid').prop('disabled',true);
				}
			}
			{ //billeditstatus printemplate settings
				if(billeditstatus == 0) {
					var transactionmode = $('#transactionmodeid').find('option:selected').val();
					var setdefault ='Yes';
					var defaultid = '';	
					$('#printtempaltedivhid').removeClass('hidedisplay');
					if(printtemplatesid != '') {
						$("#printtemplateid option").addClass("ddhidedisplay").attr('disabled', true);
						$("#printtemplateid option[value=1]").removeClass("ddhidedisplay").attr('disabled', false);
						$("#printtemplateid option[data-transactiontype="+val+"]").removeClass("ddhidedisplay").attr('disabled', false);
						var pttypecount = $("#printtemplateid option[data-transactiontype="+val+"]").length;
						defaultid = $.trim($("#printtemplateid option[data-transactiontype="+val+"][data-setasdefault="+setdefault+"]").val());
						if(defaultid != '') { // set as default
							$('#printtemplateid').select2('val',defaultid);
							if(pttypecount == 1) {
								$('#printtempaltedivhid').addClass('hidedisplay');
							}
						} else {
							$('#printtemplateid').select2('val',printtemplatesid);
						}
						if(pttypecount == 0) {
							$('#printtempaltedivhid').addClass('hidedisplay');
						}
					} else {
						$("#printtemplateid option").addClass("ddhidedisplay").attr('disabled', true);
						$("#printtemplateid option[value='1']").removeClass("ddhidedisplay").attr('disabled', false);
						$('#printtemplateid').select2('val',1);	
					}	
				}
				if(printtemplatehideshow == 1) {
					$('#printtempaltedivhid').addClass('hidedisplay');
				}
			}	
			{// payment receipt - creditno fiels gide/show
				if(val == 23 || val == 22) { // payment receipt
				   //$('#creditnodiv,#loadbilltypediv').show();
				   $('#creditnodiv').show();
				   $('#loadbilltypediv').hide();
				} else {
					$('#creditnodiv,#loadbilltypediv').hide();
				}
			}
			{// weight sale hide main save
				var ratecuttypeid = $('#ratecuttypeid').find('option:selected').val();
				if((val == 22 && ratecuttypeid == 2) || (val == 23 && ratecuttypeid == 2)) {
					$('#dataaddsbtn').hide();
				}
				if(val == 13) {
					$('#dataaddsbtn').hide();
				} else {
					$('#dataaddsbtn').show();
					$('#accountid').focus();
				}
			}
			$('#modeid').select2('val',2);
			showhidecommonfields(transactiontype,2);
			$('#paymentmodeid').select2('val',3);
			hideinnergridcolumns();
			{ // estimate hide bill balance in summary
				if(val == 16 || val == 28) {
					$('.estimatehide').hide();
					if(val == 28) {
						$('.repairhideshow').show();
					}
				} else if(val == 24) {
					setTimeout(function(){
						$('.estimatehide').show();
						$(".journalhideshow").hide();
					},10);
				} else if(val == 25) {
					setTimeout(function(){
						$('.estimatehide').show();
						$(".contrahide").hide();
					},10);
				} else {
					$('.estimatehide').show();
					$(".journalhideshow").show();
					$(".contrahide").show();
				}
				if(val == 11 && salespaymenthideshow == 1 || val == 9 && salespaymenthideshow == 1) { //Hideshow bill balance for loose stone products
					$('.estimatehide').hide();
				} else if(val == 20) {
					$('.estimatehide').show();
				}
			}
			{ //#oldjewelamtsummary-div  #salesreturnsummary-div show hide
				if(val == 9) {
					$('#oldjewelamtsummary-div,#salesreturnsummary-div').hide();
					$('#purchasereturnsummary-div').show();
				}else if(val == 20) {
					$('#oldjewelamtsummary-div').show();
					$('#salesreturnsummary-div,#purchasereturnsummary-div').hide();
				} else if(val == 28) {
					$('#oldjewelamtsummary-div,#salesreturnsummary-div,#purchasereturnsummary-div').hide();
				} else {
					$('#oldjewelamtsummary-div,#salesreturnsummary-div').show();
					$('#purchasereturnsummary-div').hide();
				}
			}
			// maddy
			if(val == 25){
				$('#addaccount,#accountsearchevent,#editaccount').hide();
			}else{
				$('#addaccount,#accountsearchevent,#editaccount').show();
			}
			// image button hide/show
			if(val == 20 || val == 27 || val == 28) {  // Take Order & Repair, Generate Place Order
				if($('#orderimageoption').val() == 1) {
					$('#imageitem-div').removeClass('hidedisplay');
				} else if($('#orderimageoption').val() == 0) {
					$('#imageitem-div').addClass('hidedisplay');
				}
			} else {
				if($('#transactionimageoption').val() == 1) {
					$('#imageitem-div').removeClass('hidedisplay');
				}else if($('#orderimageoption').val() == 0) {
					$('#imageitem-div').addClass('hidedisplay');
				}
			}
			if(approvaloutrfid == 2 || approvaloutrfid == 3) {
				if(val == 18 || val == 11 || val == 16) {
					$('#appoutrfidbtn').show();
					$("#rfidtagnumber").css('display','inline').css('width','90%');
					$('label[for="rfidtagnumber"]').css('width','90%');
				} else {
					$('#appoutrfidbtn').hide();
					$("#rfidtagnumber").css('display','inline').css('width','');
					$('label[for="rfidtagnumber"]').css('width','');
				}
			}
			if(multitagbarcode == 2 && val == 18 || multitagbarcode == 2 && val == 11 || multitagbarcode == 2 && val == 16) {
				$('#tagnomultiscan').show();
				$("#itemtagnumber").css('display','inline').css('width','100%');
				$('label[for="itemtagnumber"]').css('width','100%');
			}else{
				$('#tagnomultiscan').hide();
				$("#itemtagnumber").css('display','inline').css('width','');
				$('label[for="rfidtagnumber"]').css('width','');
			}
			// bulk sales button hide/show
			/* if(val == 11 || val == 13 || val == 16 || val == 18) {
				$('#salesbulkicon').show();
			}else {
				$('#salesbulkicon').hide();
			} */
			if((estimatedefaultset == 1 && val == 16 )|| val == 24 ) {
				$('#accounttypeid').select2('val',$("#estimatedefaultacctype").val());
				$('#selaccounttypeid').val($("#estimatedefaultacctype").val());
				$('#accountid').val($("#estimatedefaultaccid").val());
				setTimeout(function() {
					if($("#estimatedefaultmobnum").val() == '') {
						$('#s2id_accountid a span:first').text($("#estimatedefaultaccname").val());
					} else {
						$('#s2id_accountid a span:first').text($("#estimatedefaultaccname").val()+' - '+$("#estimatedefaultmobnum").val());
					}
				},100);
				//$('#accountid').select2('val',$("#estimatedefaultaccid").val()).trigger('change');
				if(val == 16){
					$("#stocktypeid").select2('focus');
				}else{
					$("#paymentstocktypeid").select2('focus');
				}
			}
			//Trigger default stockttype set when transaction type chosen
			if(val == 16 || val == 11 || val == 18 || val == 20 || val == 13 || val == 21 || val == 28 || val == 29 || val == 30) {
				$('#stocktypeid').select2('val',defaultstocktypeset).trigger('change');
			}
			{ /* Hide show Calculation Type based on options available */
				$("#calculationtypeid").removeClass('hidedisplay');
				var ctypeinc = 0;
				$("#transactionmodeid option[value]").each(function() {
					ctypeinc++;
				});
				if(ctypeinc == 1) {
					$("#calculationtypeid").addClass('hidedisplay');
				}
			}
			Materialize.updateTextFields();
		});
		$('#ratecuttypeid').change(function() {
			ratecuttypechange();
			loadbilltrigger();
		});	
			
		/** gowtham code for place order - order mode change Start**/
		$("#ordermodeid").change(function() {
			var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
			var ordermodeid = $("#ordermodeid").find('option:selected').val();
			if(ordermodeid  == 2) {
				if(transactiontype == 21) {
					//$("#stocktypeid").select2('val',76).trigger('change');
					$("#stocktypeid").select2('val',76);
				} else if(transactiontype == 29) {
					$("#stocktypeid").select2('val',87);
				}
				$("#stocktypeid_div").addClass('hidedisplay');
				$("#ordernumber-div").attr('style','display:none');
			} else {
				if(transactiontype == 21) {
					$("#stocktypeid").select2('val',76);
				} else if(transactiontype == 29) {
					$("#stocktypeid").select2('val',87);
				}
				$("#stocktypeid_div").addClass('hidedisplay');
				$("#ordernumber-div").attr('style','');
				$('#product-div').hide();
				loadtakeordernumber(2); // load take order number
			}
		});
		/** gowtham code for place order - order mode change End**/
		// accountid change
		$('#accountid').change(function() {
			var planid = $('#hiddenplanstatus').val();
			var trans_type = $('#salestransactiontypeid').find('option:selected').val();
			var accountid = $(this).val();
			var salesid = $("#generatesalesid").val();
			var salesdate = $("#salesdate").val();
			$('#accountopeningamountirid,#accountopeningweightirid,#accountopeningamount,#accountopeningweight').val(0);
			$('#spanaccopenamt,#spanacccloseamt,#spanaccopenwt,#spanaccclosewt').text(0);
			$('#accopenamtspan,#acccloseamtspan,#spanaccopenwtsymbol').text('');
			$('#accountclosingwt,#spanissuepurewthidden,#prebillnetwt,#paymenttotalwthidden,#finalbillwt,#spanbillwt,#billnetwtwithsign,#billgrossamount,#billnetamount,#prepaymenttotalamount,#paymenttotalamount,#finalbillamount,#sumarytaxamount,#roundvalue,#sumarydiscountamount,#sumarypendingamount,#oldjewelamtsummary,#purchasereturnsummary,#salesreturnsummary,#orderadvamtsummary,#orderadvamtsummaryhidden,#billweighthidden,#spanpaidpurewthidden,#prepaymentpureweight,#paymentpureweight,#balpurewthidden,#prepaymenttotalwt,#editpaymentpureweight,#oldjewelwtsummary,#purchasereturnwtsummary,#salesreturnwtsummary,#billnetwt,#billgrosswt,#sumarypaidamount,#accountclosingamount,#salesdetailcount,#liveadvanceclose,#orderadvadjustamthidden,#paymenttotalamounthidden,#prebillnetamount,#sumaryissueamount,#billnetamountwithsign,#spanpaidgrswthidden,#spanissuegrswthidden,#billforgrosswt,#billfornetwt,#summarysalesreturnroundvalue').val(0);
			discountamtwithsign = 0;
			discountoldamtwithsign = 0;
			$('#spanbillamt,#sumpendingamt,#spanbillwt,#spanpaidpurewt,#spanbillclosewt,#sumpaidamt,#spanacccloseamt,#spanaccopenamt,#spanaccclosewt,#spanaccopenwt,#sumissueamt,#spanissuepurewt,#spanpaidgrswt,#spanissuegrswt,#billforpieces,#accountadvamt,#accountcreditamt').text(0);
			// Account Based Tax => Applicable or not 
			if(accountid != 1) {
				if(checkVariable('accountid') == true) {
					//var stocktype = $("#stocktypeid").find('option:selected').val();
					var stocktype = $('#salestransactiontypeid').find('option:selected').data('stocktypeid');
					if((trans_type == 21 && stocktype == 76) || (trans_type == 29 && stocktype == 87)) { // place order
						$('#ordermodeid').select2('val',3).trigger('change');
						$('#ordermodeid-div').addClass('hidedisplay');
						$('#stocktypeid').select2('val',stocktype).trigger('change');
					} else if(trans_type == 11) { // d order
						if(stocktype == 83) {
							loadtakeordernumber(5);
						}
					}
					// load approval number details
					if(stocktype == 25 || stocktype == 77 || stocktype == 65 || stocktype == 66 || stocktype ==74 || stocktype == 16) {
						$('#approvalnumber').select2('val','');
						loadapprovalnumber(stocktype,accountid);
					}
					// load account balance
					loadaccountopenclose(accountid);
					
					if(trans_type == 22 || trans_type == 23) { // payment issue/receipt
						var selectedaccounttype = $('#selaccounttypeid').val();
						if($('#customerwise').val() == 1 && selectedaccounttype == 6 || $('#vendorwise').val() == 1 && selectedaccounttype == 16) {
							$('#issuereceipticon').show();
							$('#creditno').css({'width':'100%'});
						} else {
							$('#issuereceipticon').hide();
							$('#creditno').css({'width':'100%'});
						}
						resetirfields();
						loadbilltrigger();
					 } else if(trans_type == 25) {
						if(accountid != '') {
							$("#paymentaccountid option[value="+accountid+"]").addClass("ddhidedisplay").attr('disabled', true);
							$("#paymentaccountid").select2('val','').trigger('change');
							$('#paymentstocktypeid').trigger('change');
						}
					} else if(trans_type == 20) { // take order
						//var accounttypeid = $("#accounttypeid").find('option:selected').val(); 
						var accounttypeid = $("#selaccounttypeid").val();
						if(accounttypeid == 16) {
							$('#stocktypeid').select2('val',82).trigger('change');
							orderstocktypehideshow(82);
							loadtakeordernumber(3);
							if(receiveordertemplateid > 1) {
								$('#printtemplateid').select2('val',receiveordertemplateid).trigger('change');
							}
						} else if(accounttypeid == 6) {
							$('#stocktypeid').select2('val',75).trigger('change');
							orderstocktypehideshow(75);
						}
					} else if(trans_type == 11 || trans_type == 27 || trans_type == 18) { //Sales, Generate PO, Approval Out
						var defaultstocktypeset = $('#salestransactiontypeid').find('option:selected').attr('data-stocktypeid');
						$('#stocktypeid').select2('val',defaultstocktypeset).trigger('change');
					} else if(trans_type == 30) { // Receive Repair
						loadtakeordernumber(3);
					}
				}
				Materialize.updateTextFields();
			}
		});
		$("#salespersonid").change(function() {
			if(checkVariable('salespersonid') == true) {
				var sperson = $('#salespersonid').find('option:selected').attr('data-name');
				var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
				$("#sperson").text(sperson);
				//get user based cash counter
				if(estimatestatus == 0) { // except estimate
					//getcashcounterdata($("#salespersonid").val());
				}
			} else {
				$("#sperson").text('');
			}
		});
		// Product Change Trigger
		$("#product").change(function() {
		    var accountid = $('#accountid').val();
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			var tagtypeid = $('#product').find('option:selected').data('tagtypeid');
			var lstpurityid = $(this).find('option:selected').data('purityid');
			var prodstoneapplicable = $('#product').find('option:selected').data('stoneapplicable');
			if(accountid == '' && transactiontype != 9 && transactiontype != 13){
				alertpopup('Select the account');
				$('#product').select2('val','');
				return false;
			} else {
				$('#ratepergram-div').show();
				$('#ratepergram').attr('data-validation-engine','validate[required,custom[number],min[1]]');
				var product = $.trim($('#product').find('option:selected').val());
				var stocktype = $('#stocktypeid').find('option:selected').val();
				var taxdetails=$(this).find('option:selected').data('taxid');
				var taxdataid=$('#product').find('option:selected').data('taxmasterid');
				if(stocktype != 19) {
					if(taxdataid ==1 || taxdataid == ''){
						$('#taxdetails').val('');
						$('#taxcategory').select2('val','');
					}else{
						$('#taxdetails').val(taxdetails);
					}
				}
				var tagnumber = $.trim($('#itemtagnumber').val());
				if(tagnumber == '' || tagnumber == 'null') {
					$('#netweight,#grossweight,#stoneweight,#ratepergram,#makingcharge,#makingchargeclone,#lstcharge,#lstchargeclone,#wastage,#wastageclone,#flatcharge,#repaircharge,#additionalchargeamt,#flatchargeclone,#repairchargeclone,#hflat,#hflatfinal,#cflat,#certfinal,#totalchargeamount,#stonecharge,#taxamount,#discount,#totalamount,#itemcaratweight').val('');
					$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan,#stonetotdiaweight').text(0);
					$('#itemdiscountcalname,#CC-FT,#HM-FT').text('(0)');
					setzero(['netweight','grossweight','stoneweight','ratepergram','makingcharge','makingchargeclone','lstcharge','lstchargeclone','wastage','wastageclone','flatcharge','additionalchargeamt','flatchargeclone','hflat','hflatfinal','cflat','certfinal','totalchargeamount','stonecharge','taxamount','discount','totalamount','itemcaratweight','repaircharge','repairchargeclone']); //sets zero on EMPTY
					if(product !='' && product != '1') {
						loadproductaddon();
					}
				}
				if(stocktype == 73 || stocktype == 12 || stocktype == 24) { // purchase return & untag
					if(retrievetagstatus == 0) {
						getpurity(stocktype,product,'purity');
					}
				} else if(stocktype == 17 || stocktype == 80 || stocktype == 81) {
					countershowhide('counter',10);
					var purchasedisplay = $('#purchasedisplay').val();
					if(purchasedisplay == 0 || purchasedisplay == 2 ) {
						var purityid = 'purity';
						$("#"+purityid+" option").removeClass("ddhidedisplay");
						$("#"+purityid+" optgroup").removeClass("ddhidedisplay");
						$("#"+purityid+" option").prop('disabled',false);
						$("#"+purityid+"").select2('val','');
					} else {
					    puritysetbasedonproduct();
					}
					if($('#accountid').val() != '') {
						$('#grossweight').trigger('change');
					}
				} else {
					puritysetbasedonproduct();
				}
				if(stocktype == 19 || stocktype == 13 || stocktype == 73 || stocktype == 12 || stocktype == 75 || stocktype == 20 || stocktype == 17 || stocktype == 80 || stocktype == 81 || stocktype == 24) {
					if(stocktype == 75 && checkVariable('stonecharge') == true) {
						$("#stonecharge").attr('readonly', true);
					}
				} else {	
					if(product !='' && product != '1') {
						loadproductbasedcounter('product','counter','counter-div','0');
					}
				}
				if(transactiontype != 13 && transactiontype != 21) { // weight sales and place order
					//check the product have stone or not
					var productstone = $('#product').find('option:selected').data('stoneapplicable');
					if(stocktype == 19 ) {
						$('#stoneweight').attr('readonly',false);
						if(oldjewelsstonedetails == 1 && productstone == 'YES') {
							$('#stoneweight').attr('readonly',true);
							$('#stoneweight-div').show();
							$('#stoneoverlayicon').show();
							$('#stonecharge-div').show();
							$('#stonecharge').attr('readonly',true);
							$('#stoneweighticon').hide();
						} else {
							stonechargehideshow(); //charges show hide based on product
						}
					} else if((stocktype == 17 || stocktype == 80 || stocktype == 81) && purchasemodstonedetails == 1 && prodstoneapplicable == 'Yes' && tagtypeid != 5) {
						$('#stoneweight-div').show();
						$('#stoneoverlayicon').show();
						$('#stonecharge-div').show();
						$('#stonecharge').attr('readonly',true);
						$('#stoneweighticon').hide();
					} else {
						stonechargehideshow(); //charges show hide based on product
					}
				}
				if(transactiontype == 9) { // for purchase & except purchase return
					if($('#product').val()) {
						var chargeid = $('#product').find('option:selected').data('purchasechargeid');
						wastagechargehideshow(chargeid);
						paymenttouchhideshow(chargeid);
						makingchargehideshow(chargeid);
						lstchargehideshow(chargeid);
						flatchargehideshow(chargeid);
						extrachargehideshow(chargeid);
						repairchargehideshow(stocktype);
					}
				} else if(transactiontype == 11 || transactiontype == 16 || transactiontype == 20 || transactiontype == 28) { // for Sales, Estimate, Take (Order, Repair)
					if(stocktype != 19) {
						if($('#product').val()) {
							var chargeid = $('#product').find('option:selected').data('chargeid');
							wastagechargehideshow(chargeid);
							paymenttouchhideshow(chargeid);
							makingchargehideshow(chargeid);
							lstchargehideshow(chargeid);
							flatchargehideshow(chargeid);
							extrachargehideshow(chargeid);
							repairchargehideshow(stocktype);
						}
					}
				}
				if(transactiontype == 20 || transactiontype == 28 || transactiontype == 30 || stocktype == 89) { // take order, repair
				   sizehideshow(product);
				}
				if(stocktype == 75 || stocktype == 17 ||stocktype == 80 || stocktype == 81 || stocktype == 86){
					$('#counter-div').hide();
				} else {
					if(stocktype != 19) {
						$('#counter-div').show();
					}
				}
				if(stocktype == 20 && tagtypeid == 5) {
					$('#ratepergram').val(0);
					$('#grossweight,#stoneweight,#netweight').attr('readonly',true);
				} else if(stocktype == 19) {
					$('#grossweight,#stoneweight').attr('readonly',false);
				} else if(stocktype == 17 ||stocktype == 80 ||stocktype == 81 ) {
					$('#grossweight,#stoneweight').attr('readonly',false);
				} else {
					$('#grossweight').attr('readonly',false);
					$('#stoneweight').attr('readonly',true);
				}
				if(stocktype == 86 || stocktype == 88 || stocktype == 89) { // Take Repair
					$('#ratepergram').val(0);
					$('#ratepergram-div').hide();
					$('#ratepergram').attr('data-validation-engine','');
					$('#grossweight,#stoneweight').attr('readonly',false);
				}
				else if((stocktype == 12 || stocktype == 17 || stocktype == 80 || stocktype == 81) && lstpurityid == 11 || stocktype == 17 && tagtypeid == 5 && purchasedisplay == 1) {
					$('#proditemrate-div,#itemcaratweight-div').hide();
					$('#proditemrate,#itemcaratweight').attr('data-validation-engine','');
					if((stocktype == 12 || stocktype == 17 || stocktype == 80 || stocktype == 81) && lstpurityid == 11) {
						$('#itemcaratweight-div').show();
						$('#itemcaratweight').attr('data-validation-engine','validate[required,min[0.01],custom[number],decval['+dia_weight_round+']]');
						$('#ratepergram-div,#grossweight-div,#netweight-div,#wastageweight-div').hide();
						$('#grossweight,#wastageweight,#netweight,#ratepergram').attr('data-validation-engine','');
					} else if(stocktype == 17 && tagtypeid == 5 && purchasedisplay == 1) {
						$('#proditemrate-div').show();
						$('#ratepergram-div,#grossweight-div,#netweight-div,#wastageweight-div').hide();
						$('#proditemrate').attr('data-validation-engine','validate[required,custom[number],min[1]]');
						$('#grossweight,#wastageweight,#netweight,#ratepergram').attr('data-validation-engine','');
					}
				} else {
					if(transactiontype == 9) { // purchase module
						$('#wastageweight-div').show();
						$('#wastageweight').attr('data-validation-engine','validate[custom[number],decval['+round+']]');
					} else {
						$('#wastageweight-div').hide();
						$('#wastageweight').attr('data-validation-engine','');
					}
					$('#proditemrate-div,#itemcaratweight-div').hide();
					$('#proditemrate,#itemcaratweight').attr('data-validation-engine','');
					$('#ratepergram-div,#grossweight-div,#netweight-div').show();
					$('#ratepergram').attr('data-validation-engine','validate[required,custom[number],min[1]]');
					$('#grossweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],decval['+round+']]');
					$('#netweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],funcCall[validategreater],decval['+round+']]');
				}
				Materialize.updateTextFields();
			}
       });
	   $("#category").change(function() {
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			var stocktype = $('#stocktypeid').find('option:selected').val();
			if(transactioncategoryfield == '1') {
				var categoryid = $(this).val();
			} else {
				var categoryid = $('#category').val();
			}
			if(transactiontype == 9) { //Purchase - category change grossweight trigger
				if(stocktype == 17 || stocktype == 80 || stocktype == 81) {
					getpuritybasedoncategory(categoryid);
					$('#grossweight').trigger('change');
				}
			}
			if(transactiontype == 20 || transactiontype == 28) {
				if((stocktype == 75 && takeordercategory == 1) || (stocktype == 86 && takeordercategory == 1)) {
					$('#product').select2('val','');
					if(transactioncategoryfield == '1') {
						var categoryid = $('#category').find('option:selected').val();
					} else {
						var categoryid = $('#category').val();
					}
					oldjewelhideshow('product',categoryid);
				}
			}
		});
		//Payment product Change
		$("#paymentproduct").change(function() {
		    var accountid = $('#accountid').val();
			var transactiontype = $('#salestransactiontypeid').val();
			$('#paymentgrossweight,#paymentnetweight,#paymentpureweight,#paymentlessweight').val(0);
			$('#paymentpieces').val(1);
			if(accountid == '' && transactiontype != 9){
				alertpopup('Select the account');
				$('#paymentproduct').select2('val','');
				return false;
			} else {   
				var product = $('#paymentproduct').val();
				var stocktype = $('#paymentstocktypeid').val();
				var taxdetails=$(this).find('option:selected').data('taxid');
				paymentpuritysetbasedonproduct();
				if(stocktype == 20 || stocktype == 19 || stocktype == 13 || stocktype == 73) {
				} else {
					loadproductbasedcounter('paymentproduct','paymentcounter','paymentcounter-div','1');
				}
				Materialize.updateTextFields();
			}
		});
		// purity
		$('#purity').change(function() {
			var melting = $('#purity').find('option:selected').data('melting');
			$('#melting').val(melting);
			var transactiontype = $("#salestransactiontypeid").val();
			if(transactiontype != 13) {
				puritychangesetrate();
			}
			var stocktypeid = $('#stocktypeid').val();
			if(stocktypeid == 13){ //partial tag
				var purity = $("#purity").val();
				getpendingproductname(purity);
			}
			if($('#stocktypeid').val() == 19){ // old
				if(retrievetagstatus == 0){
					$("#grossweight,#stoneweight,#dustweight,#netweight,#wastageless,#grossamount,#taxamount,#totalamount,#itemcaratweight").val(0);
					$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
					getdetailsforoldjewel();
				}
			}
			if(stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81) { // purchase - grossweight trigger change
				$('#grossweight').trigger('change');
			}
			if(stocktypeid == 73 || stocktypeid == 12 || stocktypeid == 24 ){ // purchase return & untag
				var purity = $("#purity").val();
				var product = $("#product").val();
				if(retrievetagstatus == 0){
					getcounter(stocktypeid,product,purity,'counter');
					if(counterstatus == 'NO'){
						getweight(stocktypeid,product,purity,'','1');
					}
				}
			}
			//ratepergramdivlsthide();
			Materialize.updateTextFields();
		});
		// purity
		$('#paymentpurity').change(function(){
			var melting = $('#paymentpurity').find('option:selected').data('melting');	
			$('#paymentmelting').val(parseFloat(melting).toFixed(roundmelting));
			if(counterstatus == 'NO') {
				var stocktypeid = $("#paymentstocktypeid").val();
				var product = $('#paymentproduct').val();
				var purity = $('#paymentpurity').val();
				getweight(stocktypeid,product,purity,'','2');
			}
			Materialize.updateTextFields();
		});
		$('#paymentcounter').change(function(){
			var stocktypeid = $("#paymentstocktypeid").val();
			var product = $('#paymentproduct').val();
			var purity = $('#paymentpurity').val();
			var counter = $('#paymentcounter').val();
			getweight(stocktypeid,product,purity,counter,'2');
		});
		$('#printtemplateid,#counter').change(function(){
			Materialize.updateTextFields();
		});
		
		$("#itemtagnumber").change(function() {
			var itemtagnumber = getgridcolcolumnvalue('saledetailsgrid','','itemtagnumber');
			var accountid = $('#accountid').val();
			var tagtypeid = 0; 
			setzero(['stonecharge']);
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			if(accountid == '' && transactiontype !='13') {
				$(this).val('');
				alertpopup('Please select the account name');
			} else{
				var val = $(this).val();
				var stocktypeid = $('#stocktypeid').val();	
				var planid=$('#hiddenplanstatus').val(); 
				if(stocktypeid == 11 || stocktypeid == 13 || stocktypeid == 62 || stocktypeid == 65 || stocktypeid == 74 || stocktypeid == 82){
				var msg= '';
				var tagnumber = $('#itemtagnumber').val();
				if($('#accountid').val() == ''){
					var accountid = '';
					var accounttype_id = '';
				}else{
					var accountid = $('#accountid').val();
					if(salestransactiontypeid == 9){
						var accounttype_id = 16;
					}else{
						var accounttype_id = 6;
					}
				}
				// coded for edit checking in main edit dont reove this vishal  
				var editmodetagcheck = 0;
				if(salesdetailentryeditstatus == 1) {
					 var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
					 var edititemtagnumber = getgridcolvalue('saledetailsgrid',selectedrow,'itemtagnumber','');
					 if(tagnumber == edititemtagnumber){
						 editmodetagcheck = 1;
					 }
				  } 
				if (jQuery.inArray(tagnumber, itemtagnumber) == '-1' || editmodetagcheck == 1) {
					var tagnumber = $.trim(tagnumber);
					var rfidtagnumber = '';
					var stonecount = 0;
					var stocktypeid = $('#stocktypeid').val();	
					var accountid = $('#accountid').val();
					var approvalouttagnumberarray = $('#approvalouttagnumberarray').val();
					var approvaloutrfidnumberarray = $('#approvaloutrfidnumberarray').val();
					if(checkValue(tagnumber) == true){
						//$("#processoverlay").show();
						$.ajax({
							url:base_url+"Sales/retrievetagdata",
							data:{tagnumber:tagnumber,stocktypeid:stocktypeid,accountid:accountid,rfidtagnumber:rfidtagnumber,accounttype_id:accounttype_id,transactiontype:transactiontype,approvalouttagnumberarray:approvalouttagnumberarray,approvaloutrfidnumberarray:approvaloutrfidnumberarray},
							dataType:'json',
							async:false,
							success :function(data) 
							{
								stonecount = data['stock']['stonecount'];
								msg = data['stock']['output'];
								if(data['stock']['output'] == 'UNSOLD') 
								{
									if(stocktypeid == 82) {  // Tag no link to receive order
										var product = $('#product').find('option:selected').val();
										var purity = $('#purity').find('option:selected').val();
										if(product == data['stock']['product'] && purity == data['stock']['purity'] && data['stock']['orderstatus'] == 'No') {
											$('#itemtagid').val(data['stock']['itemtagid']);
											$('#tagnumber').val(data['stock']['itemtagnumber']);
											$('#grossweight').val(data['stock']['grossweight']).trigger('change');
											$('#counter').select2('val',data['stock']['counter']);
											$('#orderitemsize').select2('val',data['stock']['size']);
											if(data['stock']['tagtypeid'] == 5) { // price tag
												$('#stonecharge').val(0);
												$('#stonecharge-div,#stoneoverlayicon').hide();
											}else{
												stonechargehideshow();
											}
											if(stonecount != 0){ 
											  stoneentrygrid();
											  getitemtagbasedstoneentry();
											  $("#stoneweight").trigger('change');
											} else {
												$('#stonecharge').val(0);
												$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
											}
										} else {
											if(product != data['stock']['product']) {
												alertpopup('Product Mismatch between take order and this Tag Number');
											} else if(purity != data['stock']['purity']) {
												alertpopup('purity Mismatch between take order and this Tag Number');
											} else if(data['stock']['orderstatus'] != 'No') {
												alertpopup('Tag no '+data['stock']['itemtagnumber']+' is already assigned to another order no');
											} else {
												alertpopup('Please Enter Proper Tag Number');
											}
											$('#itemtagnumber').val('');
											$('#itemtagnumber').focus();
											return false;
										}
									} else {
										if(data['stock']['orderstatus'] != 'No'){
											alertpopup('Tag no '+data['stock']['itemtagnumber']+'  is assigned to an order no . Kindly use order tag type to sale this');
											$('#tagnumber,#itemtagnumber').val('');
										} else if(data['stock']['tagtypeid'] == 5 && stocktypeid == 13) { // price tag with partial tag type check
											$('#tagnumber,#itemtagnumber').val('');
											alertpopup('Price tag wont applicable for Partial Tag');
										} else if(data['stock']['tagtypeid'] == 5 ) {// price tag 
											var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
											if(transactionmodeid == '2') {
												var elementsname=['10','itemtagid','itemtagnumber','purity','product','counter','grossweight','stoneweight','netweight','pieces','rfidtagnumber'];
												var txtboxname = elementsname;
												var dropdowns = ['3','purity','product','counter'];
												textboxsetvalue(txtboxname,txtboxname,data['stock'],dropdowns);
												$('#ratepergram').val('0');
												$('#grossamount').val(data['stock']['itemrate']).trigger('change');
												$('#totalamount').val(data['stock']['itemrate']);
												$('#taxamount').val(0);
												$('#tagnumber').val(data['stock']['itemtagnumber']);
												$('#tagimage').val(data['stock']['tagimage']);
												calculationdone = 1;
												tagtypeid = 5;
												itemdetailshideshow(1); // hide weight related and rate field
											} else {
												$('#tagnumber,#itemtagnumber').val('');
												alertpopup('Price tag wont applicable for PureWt Calculation type');
											}
										} else {
											var elementsname=['11','itemtagid','itemtagnumber','purity','product','ratepergram','counter','grossweight','stoneweight','netweight','pieces','rfidtagnumber'];
											var txtboxname = elementsname;
											var dropdowns = ['3','purity','product','counter'];
											textboxsetvalue(txtboxname,txtboxname,data['stock'],dropdowns);						
											//$('#ratepergram').attr('readonly',false);
											$("#purity").trigger('change');
											$('#processcounterid').val(data['stock']['counter']);
											if(stocktypeid == 13)// partial tag
											{
												$('#partialgrossweight').val(data['stock']['grossweight']);
											}else{
												$('#partialgrossweight').val(0);
											}
											$('#tagnumber').val(data['stock']['itemtagnumber']);
											$('#tagimage').val(data['stock']['tagimage']);
											calculationdone = 0;
											$("#productaddonform .chargedetailsdiv").addClass('hidedisplay');
											$('.hpiecefinaldiv,.hflatfinaldiv,.certfinaldiv').addClass('hidedisplay');
											$("#productaddonform .chargedetailsdiv .chargedata").val(0);
											$("#productaddonform .chargedetailsdiv .chargedata").attr('data-validation-engine','');
											// display charges based on product select
											if(salestransactiontypeid == 9){
												 var chargeid=$("#product").find('option:selected').data('purchasechargeid');
												if(purchasewastage == 'Yes') {
													if(data['charge']['chargewastageweight'] == '' || data['charge']['chargewastageweight'] == 0){
													}else{
														var chargewastageweight = data['charge']['chargewastageweight'];
														$('#wastageweight').val(chargewastageweight);
														setwastageweight();
													}
												}
											}else if(salestransactiontypeid == 18){
												if(accounttype_id == 6){
													var chargeid = $("#product").find('option:selected').data('chargeid');
												}else if(accounttype_id == 16){
													var chargeid = $("#product").find('option:selected').data('purchasechargeid');
												}
											}
											else{
												var chargeid=$("#product").find('option:selected').data('chargeid');
											 }
											if(chargeid != '' || chargeid != 1) {
												chargeid=chargeid.toString();
												var chargeiddata = ['2','3','4','5','6','7','14'];  // wastage & making charge ids
												if (chargeid.indexOf(',') > -1) {
													var chargedetails=chargeid.split(",");
													if(chargedetails == ''){
													}else{
														$.each(chargedetails, function (index, value) {
															if(jQuery.inArray( value, chargeiddata ) == -1) {  // except wastage & making charge
																$("#productaddonform .charge"+value).removeClass('hidedisplay');
																$("#productaddonform .charge"+value+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
															}
														});
													}
												} else {
													if(jQuery.inArray( chargeid, chargeiddata ) == -1) {  // except wastage & making charge
														$("#productaddonform .charge"+chargeid).removeClass('hidedisplay');
													}
												}
											}
											var chargekey=[];
											var chargevalue=[];
											if(transactiontype != 13) {
												if(data['charge']['chargedetails'] != ""  && data['charge']['chargedetails'] != 'null') {
													var chargedetails1=data['charge']['chargedetails'].split(",");
													$.each(chargedetails1, function (index, value) {
														if(data['charge']['chargeid'] == '') {
															var res=value.split(":");
															chargekey.push(res[0]); 
															chargevalue.push(0);
															$("#productaddonform input[keyword="+res[0]).attr('originalvalue',0);
															$("#productaddonform input[keyword="+res[0]).val(0);
															$("#productaddonform label span[id="+res[0]).text('('+0+')');
															if(value =='MC-FT' || value == 'MC-GM-N' || value == 'MC-PI' || value == 'MC-GM-G') {
																if(value =='MC-FT'){
																	 $('#makingchargekeylabel').text('/flat');
																 }else if(value =='MC-GM-N' || value == 'MC-GM-G'){
																	 $('#makingchargekeylabel').text('/gm');
																 }else if(value == 'MC-PI'){
																	 $('#makingchargekeylabel').text('/pcs');
																 }
																$("#makingchargeclone").attr('keyword',value);
																$("#makingchargespan,#makingcharge").attr('keyword',value);
																$("#makingchargeclone").val(0).trigger('change');
															} else if(value =='WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G') {
																if(value =='WS-PT-N'){
																	  $('#wastagekeylabel').text('%Nt');
																}else if(value =='WS-WT'){
																	  $('#wastagekeylabel').text('/flat');
																}else if(value =='WS-PT-G'){
																	  $('#wastagekeylabel').text('%Gr');
																}
																$("#wastageclone").attr('keyword',value);
																$("#wastagespan,#wastage").attr('keyword',value);
																$("#wastageclone,#paymenttouch").val(0).trigger('change');
															} else if(value =='FC' || value == 'FC-WT' ) {
																if(value =='FC'){
																	  $('#flatchargekeylabel').text('/AMT');
																}else if(value =='FC-WT'){
																	  $('#flatchargekeylabel').text('/WT');
																}
																$("#flatchargeclone").attr('keyword',value);
																$("#flatchargespan,#flatcharge").attr('keyword',value);
																$("#flatchargeclone").val(0).trigger('change');
															} else if(value =='TV') {
																$("#paymenttouch").val(0).trigger('change');
															} else {
																if(value =='HM-PI'){
																	 $('.hpiecefinaldiv').removeClass('hidedisplay');
																	 setzero(['hpiecefinal']);
																}else if(value =='HM-FT'){
																	 $('.hflatfinaldiv').removeClass('hidedisplay');
																	 setzero(['hflatfinal']);
																}else if(value =='CC-FT'){
																	 $('.certfinaldiv').removeClass('hidedisplay');
																	 setzero(['certfinal']);
																}
															}
														} else {
															var res=value.split(":"); 
															var rescalc = res[0].split('.');
															var chargecalccheck = '';
															if(chargecalculation == 1){ // Max based on company setting
																chargecalccheck = 'MAX';
															} else if(chargecalculation == 2) {  // Min based on company setting
																chargecalccheck = 'MIN';
															}
															if(chargecalccheck == rescalc[1]) { // check min or max charge value apply
																if(rescalc[0] =='MC-FT' || rescalc[0] == 'MC-GM-N' || rescalc[0] == 'MC-PI' || rescalc[0] == 'MC-GM-G') {
																	if(rescalc[0] =='MC-FT'){
																		$('#makingchargekeylabel').text('/flat');
																	} else if(rescalc[0] =='MC-GM-N' || rescalc[0] == 'MC-GM-G') {
																		$('#makingchargekeylabel').text('/gm');
																	} else if(rescalc[0] == 'MC-PI') {
																		$('#makingchargekeylabel').text('/pcs');
																	}
																	$("#makingcharge").attr('originalvalue',res[1]);
																	$("#makingchargeclone").attr('keyword',rescalc[0]);
																	$("#makingchargespan,#makingcharge").attr('keyword',rescalc[0]);
																	$('#makingchargeclonelabel').text(rescalc[0]);
																	$("#makingchargeclone").val(res[1]).trigger('change');
																	//$("#makingchargeclone").val(res[1]);
																	var mcharge = 'makingchargespan';
																	$("#salesaddform label span[id="+mcharge).text(res[1]);
																} else if(rescalc[0] =='WS-PT-N' || rescalc[0] == 'WS-WT' || rescalc[0] == 'WS-PT-G') {
																	if(rescalc[0] =='WS-PT-N') {
																		$('#wastagekeylabel').text('%Nt');
																	} else if(rescalc[0] =='WS-WT') {
																		$('#wastagekeylabel').text('/flat');
																	} else if(rescalc[0] =='WS-PT-G') {
																		$('#wastagekeylabel').text('%Gr');
																	}
																	$("#wastageclone").attr('keyword',rescalc[0]);
																	$("#wastagespan,#wastage").attr('keyword',rescalc[0]);
																	$('#wastageclonelabel').text(rescalc[0]);
																	if(wastagecalctype == 5 || wastagecalctype == 3) {
																		$("#wastageclone").val(0);
																		$("#paymenttouch").val(res[1]).trigger('change');
																		var touch = checknan($('#paymenttouch').val());
																		var melting = checknan($('#melting').val());
																		var wastagepercent =  parseFloat(parseFloat(touch) - parseFloat(melting)).toFixed(3);
																		$("#wastage").attr('originalvalue',wastagepercent);
																	} else {
																		$("#paymenttouch").val(0);
																		$("#wastage").attr('originalvalue',res[1]);
																		$("#wastageclone").val(res[1]).trigger('change');
																		var wastagecharge = 'wastagespan';
																		$("#salesaddform label span[id="+wastagecharge).text(res[1]);
																	}
																} else if(rescalc[0] =='FC' || rescalc[0] == 'FC-WT') {
																	if(rescalc[0] =='FC') {
																		$('#flatchargekeylabel').text('/AMT');
																	} else if(rescalc[0] =='FC-WT') {
																		$('#flatchargekeylabel').text('/WT');
																	}
																	$("#flatcharge").attr('originalvalue',res[1]);
																	$("#flatchargeclone").attr('keyword',rescalc[0]);
																	$("#flatchargespan,#flatcharge").attr('keyword',rescalc[0]);
																	$('#flatchargeclonelabel').text(rescalc[0]);
																	$("#flatchargeclone").val(res[1]).trigger('change');
																	var flatchargespan = 'flatchargespan';
																	$("#salesaddform label span[id="+flatchargespan).text(res[1]);
																} else if(rescalc[0] =='TV') {
																	$("#paymenttouch").val(res[1]).trigger('change');
																} else {
																	chargekey.push(rescalc[0]); 
																	chargevalue.push(res[1]);
																	if(rescalc[0] =='HM-PI') {
																		$('.hpiecefinaldiv').removeClass('hidedisplay');
																		setzero(['hpiecefinal']);
																	} else if(rescalc[0] =='HM-FT') {
																		$('.hflatfinaldiv').removeClass('hidedisplay');
																		setzero(['hflatfinal']);
																	} else if(rescalc[0] =='CC-FT') {
																		$('.certfinaldiv').removeClass('hidedisplay');
																		setzero(['certfinal']);
																	}
																	$("#productaddonform input[keyword="+rescalc[0]).attr('originalvalue',res[1]);
																	$("#productaddonform input[keyword="+rescalc[0]).val(res[1]);
																	$("#productaddonform label span[id="+rescalc[0]).text('('+res[1]+')');
																}
															}
														}
														loadlstcharge(value); // load lst charge
													});
													$('#ckeyword').val(chargekey);
													$('#ckeywordoriginalvalue').val(chargevalue);
													$('.chargedata').trigger("change");
												}
											}
											calculationdone = 1;
											//itemdetailshideshow(0); //  show weight related and rate field
										}
										var chargeid=$('#product').find('option:selected').data('chargeid');
								if(transactiontype == 11 || transactiontype == 16 || transactiontype == 20 || transactiontype == 18 && stocktypeid == 62 && approvaloutcalculation == 'YES') { // sales & purchase
											if(tagtypeid == 5) { // price tag
												$('#makingcharge-div,#flatcharge-div,#repaircharge-div,#cdicon-div,#wastage-div,#lstcharge-div').hide();
											}else{
												extrachargehideshow(chargeid);
												flatchargehideshow(chargeid);
												makingchargehideshow(chargeid);
												lstchargehideshow(chargeid);
												wastagechargehideshow(chargeid);
												paymenttouchhideshow(chargeid);
												repairchargehideshow(stocktypeid);
											}
										}
										if(tagtypeid == 5) { // price tag
											$('#stonecharge').val(0);
											$('#stonecharge-div,#stoneoverlayicon').hide();
										}else{
											stonechargehideshow();
										}
										if(stonecount != 0){
											if((stocktypeid!=12 || stocktypeid!=14 || stocktypeid!=16 || stocktypeid!=19 || stocktypeid!=20)) {
												stoneentrygrid();
												if(transactiontype!=13) {
													getitemtagbasedstoneentry();
												}
											}
										}else{
											$('#stonecharge').val(0);
											$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
										}
									}
								} 
							}
						});
						//$("#processoverlay").hide();
						//validate messages
						if(msg == 'UNSOLD') {
							if(tagtypeid != 5) { // except price tag
								showsavebutton();
								totalgrossamountcalculationwork();
								if(rfidbasedtagfetch == 0){$('#salesdetailsubmit').trigger('click');} 
							}else if(tagtypeid == 5) {
								pricetagnetcalc();
								taxautocalculation(stocktypeid);
								{//net amount trigger
									
								}
							}
							setzero(['discount']);
							var productstone=$('#product').find('option:selected').data('stoneapplicable');
							if(productstone == 'No') {
								$('#stoneweight-div').hide();
							}
							
						} else if(msg == 'SOLD') {
							var tagerrordisp = "Tag No : "+tagnumber+" is sold,enter a active tag";
							$('#itemtagnumber').focus();
							if(stocktypeid == 82){
								$('#itemtagnumber').val('');
								alertpopup(tagerrordisp);
							}else{
								tagerrorresetandalert(tagerrordisp);
								$('#itemtagnumber').focus();
							}
						} else if(msg == 'DELIVERY') {
							var tagerrordisp = "Tag No : "+tagnumber+" is a delivery related tag,enter a active tag";
							$('#itemtagnumber').focus();
							if(stocktypeid == 82){
								$('#itemtagnumber').val('');
								alertpopup(tagerrordisp);
							}else{
								tagerrorresetandalert(tagerrordisp);
								$('#itemtagnumber').focus();
							}
						} else if(msg == 'APPROVAL') {
							var tagerrordisp = "Tag No : "+tagnumber+" is a Approval related tag, enter a active tag";
							$('#itemtagnumber').focus();
							if(stocktypeid == 62){
								$('#itemtagnumber').val('');
								alertpopup(tagerrordisp);
							}else{
								tagerrorresetandalert(tagerrordisp);
								$('#itemtagnumber').focus();
							}
						} else {
							var tagerrordisp = "Tag No : "+tagnumber+" is invalid,enter correct tag number";
							$('#itemtagnumber').focus();
							if(stocktypeid == 82){
								$('#itemtagnumber').val('');
								alertpopup(tagerrordisp);
							}else{
								tagerrorresetandalert(tagerrordisp);
							}
						}
					}
				} else {
					$('#itemtagnumber').val('');
					alertpopup('Already you added this itemtag number.Please add new one');
				}
			 }
			Materialize.updateTextFields();
			}
		});
	}
		$("#rfidtagnumber").change(function(){
			var accountid = $('#accountid').val();
			setzero(['stonecharge']);
			var tagtypeid = 0;
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			if(accountid == '' && transactiontype !='13') {
				$(this).val('');
				alertpopup('Please selelct the account name');
			} else {
				// To retrieve the value from rfidref table
				var rfidtagno = $(this).val();
				rfidtagno = rfidtagno.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
				$(this).val(rfidtagno);
				rfitagnochecksales();
				// rfidref code ends here
				var rfidnum = getgridcolcolumnvalue('saledetailsgrid','','rfidtagnumber');
				var val = $(this).val();
				var stocktypeid = $('#stocktypeid').val();	
				var planid=$('#hiddenplanstatus').val(); 
				if($('#accountid').val() == ''){
					var accountid = '';
					var accounttype_id = '';
				}else{
					var accountid = $('#accountid').val();
					if(salestransactiontypeid == 9){
						var accounttype_id = 16;
					}else{
						var accounttype_id = 6;
					}
				}
				if(stocktypeid == 11 || stocktypeid == 13 || stocktypeid == 62 || stocktypeid == 65 || stocktypeid == 74){
				var msg= '';
				var tagnumber = '';
				var rfidtagnumber = $('#rfidtagnumber').val();
				var rfidtagnumber = $.trim(rfidtagnumber);
				var approvaloutrfidnumberarray = $('#approvaloutrfidnumberarray').val();
				var approvalouttagnumberarray = $('#approvalouttagnumberarray').val();
				/* // coded for edit checking in main edit dont reove this vishal 
				var editmodetagcheck = 0;
				if(salesdetailentryeditstatus == 1) {
					 var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
					 var editrfidtagnumber = getgridcolvalue('saledetailsgrid',selectedrow,'rfidtagnumber','');
					 if(rfidtagnumber == editrfidtagnumber){
						 editmodetagcheck = 1;
					 }
				  } */
				if (jQuery.inArray(rfidtagnumber, rfidnum) == '-1') {
					var stonecount = 0;
					var stocktypeid = $('#stocktypeid').val();	
					var accountid = $('#accountid').val();
					if(checkValue(rfidtagnumber) == true){
						//$("#processoverlay").show();
						$.ajax({
							url:base_url+"Sales/retrievetagdata",
							data:{tagnumber:tagnumber,stocktypeid:stocktypeid,accountid:accountid,rfidtagnumber:rfidtagnumber,accounttype_id:accounttype_id,transactiontype:transactiontype,approvaloutrfidnumberarray:approvaloutrfidnumberarray,approvalouttagnumberarray:approvalouttagnumberarray},
							dataType:'json',
							async:false,
							success :function(data) {
								stonecount = data['stock']['stonecount'];
								msg = data['stock']['output'];
								if(data['stock']['output'] == 'UNSOLD'){
									if(data['stock']['tagtypeid'] == 5 && stocktypeid == 13) { // price tag with partial tag type check
										$('#rfidtagnumber,#tagnumber').val('');
										alertpopup('Price tag wont applicable for Partial Tag');
									}else if(data['stock']['tagtypeid'] == 5 ){// price tag 
										var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
										if(transactionmodeid == '2') {
											var elementsname=['13','itemtagid','itemtagnumber','purity','product','counter','grossweight','stoneweight','netweight','pieces','comment','touch','rfidtagnumber'];
											var txtboxname = elementsname;
											var dropdowns = ['3','purity','product','counter'];
											textboxsetvalue(txtboxname,txtboxname,data['stock'],dropdowns);
											$('#ratepergram').val('0');
											$('#ratepergram').attr('readonly',true);
											$('#grossamount').val(data['stock']['itemrate']).trigger('change');
											$('#totalamount').val(data['stock']['itemrate']);
											$('#tagnumber').val(data['stock']['itemtagnumber']);
											$('#tagimage').val(data['stock']['tagimage']);
											tagtypeid = 5;
											itemdetailshideshow(1); // hide weight related and rate field
										} else {
											$('#tagnumber,#itemtagnumber').val('');
											alertpopup('Price tag wont applicable for PureWt Calculation type');
										}
									}  else {
										var elementsname=['13','itemtagid','itemtagnumber','purity','product','ratepergram','counter','grossweight','stoneweight','netweight','pieces','comment','touch','rfidtagnumber'];
										var txtboxname = elementsname;
										var dropdowns = ['3','purity','product','counter'];
										textboxsetvalue(txtboxname,txtboxname,data['stock'],dropdowns);						
										$('#ratepergram').attr('readonly',false);
										$("#purity").trigger('change');
										$('#processcounterid').val(data['stock']['counter']);
										if(stocktypeid == 13) // partial tag
										{
											$('#partialgrossweight').val(data['stock']['grossweight']);
										}else{
											$('#partialgrossweight').val(0);
										}
										$('#tagnumber').val(data['stock']['itemtagnumber']);
										$('#tagimage').val(data['stock']['tagimage']);
										//loadproductaddon();
										calculationdone = 0;
										$("#productaddonform .chargedetailsdiv").addClass('hidedisplay');
										$('.hpiecefinaldiv,.hflatfinaldiv,.certfinaldiv').addClass('hidedisplay');
										$("#productaddonform .chargedetailsdiv .chargedata").val(0);
										$("#productaddonform .chargedetailsdiv .chargedata").attr('data-validation-engine','');
										// display charges based on product select
										if(salestransactiontypeid == 9){
											 var chargeid=$("#product").find('option:selected').data('purchasechargeid');
											if(purchasewastage == 'Yes') {
												if(data['charge']['chargewastageweight'] == '' || data['charge']['chargewastageweight'] == 0){
												}else{
													var chargewastageweight = data['charge']['chargewastageweight'];
													$('#wastageweight').val(chargewastageweight);
													setwastageweight();
												}
											}
										}else if(salestransactiontypeid == 18){
											if(accounttype_id == 6){
												var chargeid=$("#product").find('option:selected').data('chargeid');
											}else if(accounttype_id == 16){
												var chargeid=$("#product").find('option:selected').data('purchasechargeid');
											}
										}
										else{
											var chargeid=$("#product").find('option:selected').data('chargeid');
										 }
										if(chargeid != '' || chargeid != 1) {
											chargeid=chargeid.toString();
											var chargeiddata = ['2','3','4','5','6','7','14'];  // wastage & making charge ids
											if (chargeid.indexOf(',') > -1) {
												var chargedetails=chargeid.split(",");
												if(chargedetails == ''){
												}else{
													$.each(chargedetails, function (index, value) {
														if(jQuery.inArray( value, chargeiddata ) == -1) {  // except wastage & making charge
															$("#productaddonform .charge"+value).removeClass('hidedisplay');
															$("#productaddonform .charge"+value+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
														}
													});
												}
											} else {
												if(jQuery.inArray( chargeid, chargeiddata ) == -1) {  // except wastage & making charge
													$("#productaddonform .charge"+chargeid).removeClass('hidedisplay');
												}
											}
										} 
										var chargekey=[];
										var chargevalue=[];
										if(transactiontype != 13) {
											if(data['charge']['chargedetails'] != ""  && data['charge']['chargedetails'] != 'null') {
												var chargedetails1=data['charge']['chargedetails'].split(",");
												$.each(chargedetails1, function (index, value) {
													if(data['charge']['chargeid'] == ''){
														var res=value.split(":");
														chargekey.push(res[0]); 
														chargevalue.push(0);
														$("#productaddonform input[keyword="+res[0]).attr('originalvalue',0);
														$("#productaddonform input[keyword="+res[0]).val(0);
														$("#productaddonform label span[id="+res[0]).text('('+0+')');
														if(value =='MC-FT' || value == 'MC-GM-N' || value == 'MC-PI' || value == 'MC-GM-G') {
															if(value =='MC-FT'){
																 $('#makingchargekeylabel').text('/flat');
															 }else if(value =='MC-GM-N' || value == 'MC-GM-G'){
																 $('#makingchargekeylabel').text('/gm');
															 }else if(value == 'MC-PI'){
																 $('#makingchargekeylabel').text('/pcs');
															 }
															$("#makingchargeclone").attr('keyword',value);
															$("#makingchargespan,#makingcharge").attr('keyword',value);
															$("#makingchargeclone").val(0).trigger('change');
														} else if(value =='WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G') {
															if(value =='WS-PT-N'){
																  $('#wastagekeylabel').text('%Nt');
															}else if(value =='WS-WT'){
																  $('#wastagekeylabel').text('/flat');
															}else if(value =='WS-PT-G'){
																  $('#wastagekeylabel').text('%Gr');
															}
															$("#wastageclone").attr('keyword',value);
															$("#wastagespan,#wastage").attr('keyword',value);
															$("#wastageclone,#paymenttouch").val(0).trigger('change');
														} else if(value =='FC' || value == 'FC-WT' ) {
															if(value =='FC'){
																  $('#flatchargekeylabel').text('/AMT');
															}else if(value =='FC-WT'){
																  $('#flatchargekeylabel').text('/WT');
															}
															$("#flatchargeclone").attr('keyword',value);
															$("#flatchargespan,#flatcharge").attr('keyword',value);
															$("#flatchargeclone").val(0).trigger('change');
														} else if(rescalc[0] =='TV') {
																$("#paymenttouch").val(res[1]).trigger('change');
														} else{
															if(value =='HM-PI'){
																 $('.hpiecefinaldiv').removeClass('hidedisplay');
																 setzero(['hpiecefinal']);
															}else if(value =='HM-FT'){
																 $('.hflatfinaldiv').removeClass('hidedisplay');
																 setzero(['hflatfinal']);
															}else if(value =='CC-FT'){
																 $('.certfinaldiv').removeClass('hidedisplay');
																 setzero(['certfinal']);
															}
														}
													} else {
														var res=value.split(":"); 
														var rescalc = res[0].split('.');
														var chargecalccheck = '';
														if(chargecalculation == 1){ // Max based on company setting
															chargecalccheck = 'MAX';
														} else if(chargecalculation == 2){  // Min based on company setting
															chargecalccheck = 'MIN';
														}
														if(chargecalccheck == rescalc[1]){ // check min or max charge value apply
															if(rescalc[0] =='MC-FT' || rescalc[0] == 'MC-GM-N' || rescalc[0] == 'MC-PI' || rescalc[0] == 'MC-GM-G') {
																	 if(rescalc[0] =='MC-FT'){
																		 $('#makingchargekeylabel').text('/flat');
																	 }else if(rescalc[0] =='MC-GM-N' || rescalc[0] == 'MC-GM-G'){
																		 $('#makingchargekeylabel').text('/gm');
																	 }else if(rescalc[0] == 'MC-PI'){
																		 $('#makingchargekeylabel').text('/pcs');
																	 }
																   $("#makingcharge").attr('originalvalue',res[1]);
																   $("#makingchargeclone").attr('keyword',rescalc[0]);
																   $("#makingchargespan,#makingcharge").attr('keyword',rescalc[0]);
																   $('#makingchargeclonelabel').text(rescalc[0]);
																	 $("#makingchargeclone").val(res[1]).trigger('change');
																  var mcharge = 'makingchargespan';
																  $("#salesaddform label span[id="+mcharge).text(res[1]);
															} else if(rescalc[0] =='WS-PT-N' || rescalc[0] == 'WS-WT' || rescalc[0] == 'WS-PT-G'){
																	  if(rescalc[0] =='WS-PT-N'){
																		  $('#wastagekeylabel').text('%Nt');
																	  }else if(rescalc[0] =='WS-WT'){
																		  $('#wastagekeylabel').text('/flat');
																	  }else if(rescalc[0] =='WS-PT-G'){
																		  $('#wastagekeylabel').text('%Gr');
																	  }
																   $("#wastageclone").attr('keyword',rescalc[0]);
																   $("#wastagespan,#wastage").attr('keyword',rescalc[0]);
																   $('#wastageclonelabel').text(rescalc[0]);
																   if(wastagecalctype == 5 || wastagecalctype == 3) {
																	  $("#wastageclone").val(0);
																	  $("#paymenttouch").val(res[1]).trigger('change');
																	    var touch = checknan($('#paymenttouch').val());
																		var melting = checknan($('#melting').val());
																		var wastagepercent =  parseFloat(parseFloat(touch) - parseFloat(melting)).toFixed(3);
																		$("#wastage").attr('originalvalue',wastagepercent);
																   }else{
																		$("#paymenttouch").val(0);
																		$("#wastage").attr('originalvalue',res[1]);
																		$("#wastageclone").val(res[1]).trigger('change');
																		var wastagecharge = 'wastagespan';
																		$("#salesaddform label span[id="+wastagecharge).text(res[1]);
																   }
															} else if(rescalc[0] =='FC' || rescalc[0] == 'FC-WT') {
																   if(rescalc[0] =='FC'){
																		  $('#flatchargekeylabel').text('/AMT');
																	}else if(rescalc[0] =='FC-WT'){
																		  $('#flatchargekeylabel').text('/WT');
																	}
																   $("#flatcharge").attr('originalvalue',res[1]);
																   $("#flatchargeclone").attr('keyword',rescalc[0]);
																   $("#flatchargespan,#flatcharge").attr('keyword',rescalc[0]);
																   $('#flatchargeclonelabel').text(rescalc[0]);
																  $("#flatchargeclone").val(res[1]).trigger('change');
																  var flatchargespan = 'flatchargespan';
																  $("#salesaddform label span[id="+flatchargespan).text(res[1]);
															} else if(rescalc[0] =='TV') {
																$("#paymenttouch").val(res[1]).trigger('change');
															} else {
																chargekey.push(rescalc[0]); 
																chargevalue.push(res[1]);
																if(rescalc[0] =='HM-PI'){
																	$('.hpiecefinaldiv').removeClass('hidedisplay');
																	setzero(['hpiecefinal']);
																}else if(rescalc[0] =='HM-FT'){
																	$('.hflatfinaldiv').removeClass('hidedisplay');
																	setzero(['hflatfinal']);
																}else if(rescalc[0] =='CC-FT'){
																	$('.certfinaldiv').removeClass('hidedisplay');
																	setzero(['certfinal']);
																}
																$("#productaddonform input[keyword="+rescalc[0]).attr('originalvalue',res[1]);
																$("#productaddonform input[keyword="+rescalc[0]).val(res[1]);
																$("#productaddonform label span[id="+rescalc[0]).text('('+res[1]+')');
															}
														}
													}
													loadlstcharge(value); // load lst charge
												});
												$('#ckeyword').val(chargekey);
												$('#ckeywordoriginalvalue').val(chargevalue);
												$('.chargedata').trigger("change");
											}
										}	
										calculationdone = 1;
										itemdetailshideshow(0); //  show weight related and rate field
									}
									var chargeid=$('#product').find('option:selected').data('chargeid');
									var transactiontype = $('#salestransactiontypeid').val();
										if(transactiontype == 11 || transactiontype == 16 || transactiontype == 20 || transactiontype == 18 && stocktypeid == 62 && approvaloutcalculation == 'YES') { // sales & purchase
											if(tagtypeid == 5) { // price tag
												$('#makingcharge-div,#flatcharge-div,#repaircharge-div,#cdicon-div,#wastage-div').hide();
											}else{
												extrachargehideshow(chargeid);
												flatchargehideshow(chargeid);
												makingchargehideshow(chargeid);
												lstchargehideshow(chargeid);
												wastagechargehideshow(chargeid);
												paymenttouchhideshow(chargeid);
												repairchargehideshow(stocktypeid);
											}
										}
										if(tagtypeid == 5) { // price tag
											$('#stonecharge').val(0);
											$('#stonecharge-div,#stoneoverlayicon').hide();
										}else{
											stonechargehideshow();
										}
										if(stonecount != 0){
											if((stocktypeid!=12 || stocktypeid!=14 || stocktypeid!=16 || stocktypeid!=19 || stocktypeid!=20)) {
											   stoneentrygrid();
												var transactiontype = $("#salestransactiontypeid").val();
												if(transactiontype!=13) {
													getitemtagbasedstoneentry();
												}
											}
										}else{
											$('#stonecharge').val(0);
											$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
										}
								} 
							}
						});
						//$("#processoverlay").hide();	
						//validate messages
						if(msg == 'UNSOLD') {
							if(tagtypeid != 5) { // except price tag
								showsavebutton();
								totalgrossamountcalculationwork();
							}
							else if(tagtypeid == 5) {
								pricetagnetcalc();
								taxautocalculation(stocktypeid);
							}
							setzero(['discount']);
							var productstone=$('#product').find('option:selected').data('stoneapplicable');
							if(productstone == 'No') {
								$('#stoneweight-div').hide();
							}
						} else if(msg == 'SOLD') {
							var tagerrordisp = "RFID Tag No : "+rfidtagnumber+" is sold,enter a active tag";
							$('#rfidtagnumber').focus();
							tagerrorresetandalertrfid(tagerrordisp);
							$('#rfidtagnumber').focus();
						} else if(msg == 'DELIVERY') {
							var tagerrordisp = "RFID Tag No : "+rfidtagnumber+" is a delivery related tag,enter a active tag";
							$('#itemtagnumber').focus();
							tagerrorresetandalert(tagerrordisp);
							$('#itemtagnumber').focus();
						}else {
							var tagerrordisp = "RFID Tag No : "+rfidtagnumber+" is invalid,enter correct tag number";
							$('#rfidtagnumber').focus();
							tagerrorresetandalertrfid(tagerrordisp);
							$('#rfidtagnumber').focus();
						}
					}
						
				} else {
					$('#rfidtagnumber').val('');
					alertpopup('Already you added this rfid number.Please add new one');
				}
			}
			Materialize.updateTextFields();
			}
		});
	{ // EzeTap
		//Notification close function
		
		$("#ezetapcommentclose").click(function(){
			$('#ezetapnotificationoverlay').fadeOut();			
		});	
		// Response Notification close function
		
		$("#ezetapresponseclose").click(function(){
			$('#ezetapresponseoverlay').fadeOut();			
		});	
	
		// Notification close function
			
		$("#ezetapcommentclose").click(function(){
			$('#ezetapnotificationoverlay').fadeOut();			
		});	
		// Response Notification close function
			
		$("#ezetapresponseclose").click(function(){
			$('#ezetapresponseoverlay').fadeOut();			
		});	
		
		//Amount Bulr Event => Calling EzeTap Device And Server.
		$("#totalamount").change(function(){
			// Company setting based check whether want to use Ezetap or Normal way
			
			if($("#needezetap").val()=="YES"){
				
				if($("#paymentmodetype").val()==29){ //EzeTap Card Payment
					takeCardPayment();
				}
				/* 1.Company setting based => Whether we will use Ezetap Cash & Cheque API or Normal Flow.  */
				if($("#needcashcheque").val()=="YES"){
					if($("#paymentmodetype").val()==26){//EzeTap Cash Payment
						takeCashPayment();
					}
					else if($("#paymentmodetype").val()==27){ //EzeTap Cheque Payment
						takeChequePayment();
					}	
					else if($("#paymentmodetype").val()==28){ //EzeTap DD Payment
						takeChequePayment();	
					}
				}
		   }
			Materialize.updateTextFields();	
		});
		//old item details change
		$("#olditemdetails").change(function(){
			if(approvalnoeditstatus != 1) {
				$("#grossweight").val('0');
				$("#stoneweight").val('0');
				$("#dustweight").val('0');
				$("#netweight").val('0');
				$("#wastageless").val('0');
				$("#grossamount").val('0');
				$("#taxamount").val('0');
				$("#totalamount").val('0');
				$("#pieces").val('1');
				$('#stonetotdiaweight').text(parseFloat(0).toFixed(2));
			}
			if($('#stocktypeid').val() == 19){
				if(retrievetagstatus == 0){	
					getdetailsforoldjewel();
				}
			}
			Materialize.updateTextFields();	
		});
		// Setting Mobile No & Email for Notification.
		$("#accnotify").click(function(){ 
			$("#ezetapform").validationEngine('validate');
		});

		jQuery("#ezetapform").validationEngine({ 
			onSuccess: function() { 
				$("#ezeaccmobileno").val($("#accmobileno").val());
				$("#ezeaccemail").val($("#accemail").val());
				$('#ezetapnotificationoverlay').fadeOut(); 
				
				checkezetaportstatus(function(portstatus){
				if(portstatus!=0){
				initialize();
				}else{
				alertpopup("Please start EzeTap server for Transaction.");
				}
				});
			},
			onFailure: function() {	
				
			}
		});
	}	
	{//auto calculate  - grosswt/netweight/stoneweight
		$(".weightcalculate").change(function(event) {
			setzero(['grossweight','stoneweight','dustweight','wastageweight','netweight','ratepergram','grossamount','additionalchargeamt','stonecharge','taxamount','discount','proditemrate','itemcaratweight']);
			var accountid = $('#accountid').val();
			var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
			if(accountid == '' && transactiontype == 9) {
				alertpopup('Select the account');
				$('.weightcalculate').val('');
				return false;
			} else {
			    var round = $('#roundweight').val();
				var fieldid = $(this).attr("id");
				var type = $('#'+fieldid+'').data('calc');
				var regxg =/^(\d*\.?\d*)$/;
				var stock_type = $('#stocktypeid').find('option:selected').val();
				if(type == 'GWT') {
					var productstonecalctypeid = $.trim($('#product').find('option:selected').attr('data-productstonecalctypeid'));
					var stocktypeid = $('#stocktypeid').find('option:selected').val();
					var grosswt = $('#'+fieldid+'').val();
					//validategetweight(stock_type,'1','grossweight');
					if(stock_type == 13) { // partial tag
						var partialgrosswt = $("#partialgrossweight").val();
						if(parseFloat(partialgrosswt) <= parseFloat($("#grossweight").val())) {
							$('#'+fieldid+'').val(0).trigger('change'); 
							alertpopup('Kindly Enter less than '+partialgrosswt+' gms ');
							return false;
						}
					}
					if (!regxg.test(grosswt)) {
					} else {
						if (grosswt.toString().indexOf('.') != -1) {
							var substr = grosswt.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						var stonewt = $('#stoneweight').val();
						var dustwt = $('#dustweight').val();
						if(transactiontype != 9) {
							var wastagewt = 0;
						} else {
							var wastagewt = $('#wastageweight').val();
						}
						if(productstonecalctypeid == 3 && (stocktypeid == 19 || stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81)) {
							var netwt = (parseFloat(grosswt)-parseFloat(dustwt)+parseFloat(wastagewt)).toFixed(round);
						} else {
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)-parseFloat(dustwt)+parseFloat(wastagewt)).toFixed(round);
						}
						var thisval = parseFloat($("#grossweight").val()).toFixed(round);
						$("#grossweight").val(thisval);
						$('#stoneweight').val(parseFloat(stonewt).toFixed(roundweight));
						$('#dustweight').val(parseFloat(dustwt).toFixed(roundweight));
						$('#netweight').val(netwt);
						showsavebutton();
						Materialize.updateTextFields();
					}
				} else if(type == 'SWT') {
					var grosswt = $('#grossweight').val();
					var dustwt = $('#dustweight').val();
					var stonewt = $('#'+fieldid+'').val();
					if(transactiontype != 9) {
							var wastagewt = 0;
						} else {
							var wastagewt = $('#wastageweight').val();
						}
					if (!regxg.test(stonewt)) {
					} else {
						if (stonewt.toString().indexOf('.') != -1) {
							var substr = stonewt.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						var thisval=parseFloat($("#stoneweight").val()).toFixed(round);
						if(transactiontype == 11 || transactiontype == 9 || transactiontype == 16 || transactiontype == 20) { // Product based net weight show
							var productstonecalctypeid = $.trim($('#product').find('option:selected').attr('data-productstonecalctypeid'));
							var stocktypeid = $('#stocktypeid').find('option:selected').val();
							if(productstonecalctypeid == 3) {
								if(stocktypeid == 19) {
									var netwt = (parseFloat(grosswt)-parseFloat(dustwt)+parseFloat(wastagewt)).toFixed(round);
								} else {
									var netwt = (parseFloat(grosswt)).toFixed(round);
								}
							} else {								
								var netwt = (parseFloat(grosswt)-parseFloat(stonewt)-parseFloat(dustwt)+parseFloat(wastagewt)).toFixed(round);
							}
						} else {
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)-parseFloat(dustwt)+parseFloat(wastagewt)).toFixed(round);
						}
						$('#stoneweight').val(thisval);
						$('#netweight').val(netwt);
						$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
						Materialize.updateTextFields();
					}
				} else if(type == 'DWT') {  // Dust weight
					var productstonecalctypeid = $.trim($('#product').find('option:selected').attr('data-productstonecalctypeid'));
					var grosswt = $('#grossweight').val();
					var stonewt = $('#stoneweight').val();
					var dustwt = $('#'+fieldid+'').val();
					if(transactiontype != 9) {
						var wastagewt = 0;
					} else {
						var wastagewt = $('#wastageweight').val();
					}
					if (!regxg.test(stonewt)) {
					} else {
						if(stonewt.toString().indexOf('.') != -1) {
							var substr = stonewt.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						var thisval=parseFloat($("#dustweight").val()).toFixed(round);
						if(productstonecalctypeid == 3) {
							var netwt = (parseFloat(grosswt)-parseFloat(dustwt)).toFixed(round);
						} else {
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)-parseFloat(dustwt)).toFixed(round);
						}
						$('#dustweight').val(thisval);
						$('#netweight').val(netwt);
						Materialize.updateTextFields();
					}
				} else if(type == 'WWT') {  // Wastage weight
					var productstonecalctypeid = $.trim($('#product').find('option:selected').attr('data-productstonecalctypeid'));
					var grosswt = $('#grossweight').val();
					var stonewt = $('#stoneweight').val();
					
					if(transactiontype != 9) {
						var wastagewt = 0;
					} else {
						var wastagewt = $('#'+fieldid+'').val();
					}
					if (!regxg.test(stonewt)) {
					} else {
						if(stonewt.toString().indexOf('.') != -1) {
							var substr = stonewt.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						var thisval=parseFloat($("#wastageweight").val()).toFixed(round);
						if(productstonecalctypeid == 3) {
							var netwt = (parseFloat(grosswt)+parseFloat(wastagewt)).toFixed(round);
						} else {
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)+parseFloat(wastagewt)).toFixed(round);
						}
						$('#netweight').val(netwt);
						Materialize.updateTextFields();
					}
				} else if(type == 'NWT') {
					var grosswt = $('#grossweight').val();
					var dustwt = $('#dustweight').val();
					var netwt = $('#netweight').val();
					if(transactiontype != 9) {
					var wastagewt = 0;
					} else {
						var wastagewt = $('#wastageweight').val();
					}
					var stoneweight = $('#stoneweight').val();
					//validategetweight(stock_type,'2','netweight');
					var fianlwt = 0;
					if (!regxg.test(netwt)) {
					} else {
						if (netwt.toString().indexOf('.') != -1) {
							var substr = netwt.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						if(purchasewastage == 'Yes') {
							if(parseFloat(wastagewt) < parseFloat(stoneweight)){
								fianlwt = parseFloat(stoneweight)- parseFloat(wastagewt);
							} else {
								fianlwt = parseFloat(wastagewt)- parseFloat(stoneweight);
							}
							var grossweight = parseFloat(netwt) - fianlwt;
							$('#grossweight').val(grossweight);
						} else {
							var thisval=parseFloat($("#netweight").val()).toFixed(round);
							var stonewt = (parseFloat(grosswt)-parseFloat(netwt)-parseFloat(dustwt)).toFixed(round);
							$('#stoneweight').val(stonewt);
							$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
							$('#netweight').val(thisval);
						}
						Materialize.updateTextFields();
					}
				} else if(type == 'CWT') {
					var itemcaratweight = $('#'+fieldid+'').val();
					if (!regxg.test(itemcaratweight)) {
					} else {
						if (itemcaratweight.toString().indexOf('.') != -1) {
							var substr = itemcaratweight.split('.');
							var roundlength=substr[1].length; 
						} else {
							var roundlength=0;
						}
						if(round < roundlength) {
							return false;
						}
						showsavebutton();
						Materialize.updateTextFields();
					}
				}
				if(stock_type != 19) {
					chargesdonttriggergrosscalc = 1;
					//tag is untag type
					var tagnumber = $('#itemtagnumber').val();
					if(tagnumber == '' || tagnumber == 'null' || tagnumber == 'undefined') {
						if(retrievetagstatus == 0) {
							loadproductaddon();
						} else {
							$("#wastageclone,#makingchargeclone,#lstchargeclone").trigger('change');
							$('.chargedata').trigger("change");
						}
					} else {
						// Partial Tag, Take Order & Repair, Receive order & sales return
						if(stock_type == 13 || stock_type == 75 || stock_type == 82 || stock_type == 20) {
							if(retrievetagstatus == 0) {
								loadproductaddon();
							} else {
								$("#wastageclone,#makingchargeclone,#lstchargeclone").trigger('change');
								$('.chargedata').trigger("change");
							}
						}
					}
					totalgrossamountcalculationwork();
					chargesdonttriggergrosscalc = 0;
				} else {
					var newtouchvalue = 0;
					var oldpurchasetouch = $('#salestransactiontypeid').find('option:selected').data('oldpurchasetouch');
					var transactionmode = $('#transactionmodeid').find('option:selected').val();
					if(oldpurchasetouch == 1 && (transactionmode == 3 || transactionmode == 4) && (wastagecalctype == 5 || wastagecalctype == 3)) {
						newtouchvalue = loadproductchargeforpayment();
						if(checkValue(newtouchvalue) == true && newtouchvalue != 0) {
							$("#paymenttouch").val(newtouchvalue);
						}
					}
					oldjewelcalc();
					if(oldjewelsstonedetails == 1) { totalgrossamountcalculationwork(); }
				}
			}
		});
	}
	{//account create
		$('#createnewaccount').click(function(){
			$('#accountcreate').fadeIn();
			$('#accountcreate').animate({scrollTop:0},'fast');	
			Materialize.updateTextFields();
		});
		$("#accountcreateclose").click(function()
		{			
			$("#accountcreate").fadeOut();
			$('#closeaccount').trigger("click");
		});
		//validate accountcreate.
		$("#accountsavebtn").click(function()
		{
			$("#accountcreateform").validationEngine('validate');
		});
		jQuery("#accountcreateform").validationEngine({
			onSuccess: function() {
				createnewaccount();
			},
			onFailure: function() {				
			}
		});
		//account update
		$("#accountupdatebtn").click(function() {
			$("#accountupdateform").validationEngine('validate');
		});
		jQuery("#accountupdateform").validationEngine({
			onSuccess: function() {
				updateaccount();
			},
			onFailure: function() {				
			}
		});
		$("#salesadd").click(function()
		{
			var val=$('#salestransactiontypeid').find('option:selected').val();
			if(val == 18 || val == 19){
				var accountid=$('#accountid').val();
				if(accountid == '' || accountid == 'null'){
					alertpopup('Please choose account');
					$('#accountid').select2('focus');
					return false;
				}
			}
			$("#salescreateform").validationEngine('validate');
		});
		jQuery("#salescreateform").validationEngine({
			onSuccess: function() {
				$('#accountcreate').fadeOut();
				$('#stocktypeid').focus();
			},
			onFailure: function() {		
                  		
			}
		});
		{ // ttypesettings no icon
		$('#ttypesettings').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('#ttypesettingscreate').show();
			}	
		});
		$('#closettypesettingsoverlay').click(function(){
			$('#ttypesettingscreate').hide();
		});
		}
		{ // billref no icon
		$('#billrefnoicon').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('#billrefnocreate').show();
			}	
		});
		$('#billrefnooverlaytbox').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				 e.preventDefault();
			}	
		});
		$('#closebillrefnooverlay').click(function(){
			$('#billrefno').val($('#billrefnooverlaytbox').val()); 
			$('#billrefnocreate').hide();
		});
		}
		
		{ // purchase rasi jewel wt check icon
			$('#wtcheckicon').on('click keypress',function(e){
				if(e.keyCode == 13 || e.keyCode === undefined){
					$('#wtcheckcreate').show();
					$('#purchasecheckgrwt').focus();
				}	
			});
			$('#wtchecksave').click(function(){
				$("#wtcheckform").validationEngine('validate');	
			});
			jQuery("#wtcheckform").validationEngine( {
				onSuccess: function() {
					var purchasecheckvalidate = purchaseweightvalidation(4);
					if(purchasecheckvalidate == false){
					   return false;
					}
					$('#wtcheckcreate').hide();
				},
				onFailure: function() {	
					//alertpopup(validationalert);
				}
			});
		}
		$('#addaccount').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined) {
				$('#accountcreate').show();
				clearform('accountcreateform');
				$("#accountsavebtn").removeClass('hidedisplay');
				$("#accountupdatebtn").addClass('hidedisplay');
				$("#s2id_accounttypeid").select2('focus');
				 setTimeout(function(){
				    var transactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				    var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
					accountshowhide('accounttypeid',transactiontypeid,transactionmodeid); // transaction type based accounttype show hide
					$("#accountprimarycountry").select2('val',2);
					$("#accountprimarystate").select2('val',2);
					if(select2searchdataempty == 1) {
						var contenttype = $.isNumeric(select2serachaccountcontent);
						if(contenttype == true) {
							$('#accountmobilenumber').val(select2serachaccountcontent);
						} else {
							$('#newaccountname').val(select2serachaccountcontent);
						}
						Materialize.updateTextFields();
					}
				},50); 
			}	
		});
		$("#editaccount").click(function(){
			var accountid = $("#accountid").val();
			var trans_type = $('#salestransactiontypeid').find('option:selected').val();
			if(accountid) {
				$('#accountcreate').show();
				$("#accountupdatebtn").removeClass('hidedisplay');
				$("#accountsavebtn").addClass('hidedisplay');
				fetchaccountdataset(accountid);
			} else {
				alertpopup('Please select account before edit');
			}
		});
		$('#closeaccount').click(function() {
			select2serachaccountcontent = '';
			select2searchdataempty = 0;
			$('#accountcreate').hide();
		});
		$('#cdicon,#additionalchargeamt').on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				var chargelength = $("#productaddonform .chargedetailsdiv:not('.hidedisplay')").length;
				if(chargelength == 0){
					alertpopup('Charges not available');
				}else{ 
					$('#cdiconoverlay').fadeIn();
					$("#productaddonform :input:enabled:visible:first").focus();
				}
			}
		});
		
		$('#additionalcharge').click(function(){
			$('#cdiconoverlay').fadeOut();
			$('#additionalchargeamt').focus(); 
		});
	}
	{//redirection from(reports/auditlog/widgets) 
		var uniquelreportsession = sessionStorage.getItem("reportunique20");		
		if(uniquelreportsession != '' && uniquelreportsession != null){	
			 setTimeout(function(){ 
				salesdataretrieve(uniquelreportsession,'');	
				showhideiconsfun('summryclick','salesaddformheader');	
				sessionStorage.removeItem("reportunique20");
			},50);	
		} 
	}
	
	{// chitcancel -  need to review
		{ //chitbookno no icon
		$('#chitbooknoicon').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('#chitbooknooverlaytbox').val($('#chitbookno').val());
				$('#chitbooknocreate').show();
				$('#chitbooknooverlaytbox').focus(); 
			}	
		});
		$('#closechitbooknooverlay').click(function(){
			$('#chitbooknooverlaytbox').val('');
			$('#chitbooknocreate').hide();
		});
		}
		$('#chitbookno').on('click',function(e){
			$('#chitbooknocreate').show();
			$('#chitbooknooverlaytbox').val($('#chitbookno').val());
			$('#chitbooknooverlaytbox').focus(); 			
		});
		$('#chitbooknooverlaytbox').on('keypress',function(e){
			if(e.keyCode == 13){
				 e.preventDefault();
				 $('#chitbooknooverlaytbox').val(''+$('#chitbooknooverlaytbox').val()+'|');
			}	
		});
		$('#submitchitbooknooverlay').click(function(){
			var schememethodid = $("#schememethodid").val();
			if(schememethodid == 3) { // automatic
				if($('#chitbooknooverlaytbox').val() == ''){
					alertpopup('Kindly Scan Chitbook and then submit. Else Click Close');
				}else{
					chitbookdetails($('#chitbooknooverlaytbox').val(),1);
				}
			}else{
				$('#chitbookno').val($('#chitbooknooverlaytbox').val());
				$('#chitbooknocreate').hide();
			}
		});
		$('#chitbookno').change(function(){
			var schememethodid = $("#schememethodid").val();
			if(schememethodid == 3) {// automatic
			    chitbookdetails($(this).val(),2);
			}
		});
		$("#chitamount,#totalpaidamount").keyup(function(){
			setzero(['chitamount','totalpaidamount']);
			var chitamount = $("#chitamount").val();
			var totalpaidamount = $("#totalpaidamount").val(); // BONUS
			if(chitamount < 0){
				$("#chitamount").val(0);
				chitamount = 0;
			}
			var netamount = parseFloat(chitamount)+parseFloat(totalpaidamount);
			$('#paymenttotalamount').val(netamount.toFixed(roundamount));
			Materialize.updateTextFields();
		});
		
		$('#salesaddchitbookrefresh').click(function(){
			$('.gramsvalhide,.interestvalhide').removeClass('hidedisplay');
			clearform('chitbookclear');
		});
		// Madasamy chitbook cancel - interest amount change
		$('#interestamount').change(function(){
			var interest=$(this).val();
			var chittotamt=$('#chittotalamount').val();
			var total=parseInt(interest)+parseInt(chittotamt);
			$('#chitcanceltotalamount').val(total);
			Materialize.updateTextFields();
		});
	
		$(".checkbalance").change(function(event){			
			setzero(['chitwastage','chitmakingcharge','chitaddon']);
			var fieldid = $(this).attr("id"); 
			var type = $('#'+fieldid+'').data('calc');
			var weight=$('#weightentered').val();
			var conversionrate = $('#conversionrate').val();
			if(type == 'chitwastage'){
			
			var chitwastage = $('#'+fieldid+'').val();
			var wastage=(parseFloat(conversionrate)*parseFloat(weight)*parseFloat(chitwastage))/100; 
			var chitmakingcharge = $('#chitmakingcharge').val();
			var chitaddon = $('#chitaddon').val();
			var addon=parseFloat(chitaddon)*parseFloat(weight);
			var netwt = parseFloat(conversionrate*weight)+parseFloat(wastage) + parseFloat(chitmakingcharge) + parseFloat(addon) ;
			$('#chitcanceltotalamount').val(parseFloat(netwt).toFixed(roundamount));
			} else if(type == 'chitmakingcharge') 
			{
				var chitmakingcharge = $('#'+fieldid+'').val();
				var chitwastage = $('#chitwastage').val();
				var wastage=(parseFloat(conversionrate)*parseFloat(weight)*parseFloat(chitwastage))/100; 
				var chitaddon = $('#chitaddon').val();
				var addon=parseFloat(chitaddon)*parseFloat(weight);
				var netwt = parseFloat(conversionrate*weight)+parseFloat(wastage) + parseFloat(chitmakingcharge) + parseFloat(addon) ;
				$('#chitcanceltotalamount').val(parseFloat(netwt).toFixed(roundamount));
			}	else if(type == 'chitaddon')
				{
				var chitaddon = $('#'+fieldid+'').val();
				var addon=parseFloat(chitaddon)*parseFloat(weight);
				var chitwastage = $('#chitwastage').val();
				var wastage=(parseFloat(conversionrate)*parseFloat(weight)*parseFloat(chitwastage))/100;
				var chitmakingcharge = $('#chitmakingcharge').val();	
				var netwt = parseFloat(conversionrate*weight)+parseFloat(wastage) + parseFloat(chitmakingcharge) + parseFloat(addon);
				$('#chitcanceltotalamount').val(parseFloat(netwt).toFixed(roundamount));		
			}
				Materialize.updateTextFields();
		});
	}	
	
	{
		$("#mainheadericon,#accountinformation").click(function(){
			$("#mainheaderoverlay").show();	
		});	
	}
	{
		$("#salespaymenticon").click(function(){
		$("#salesprodformdetails").toggle();	
		});	
	}
	{// rate overlay 
		$("#salesrateicon,.salesgridcaptionpos").click(function(){
		 var gridlength = $('#salesinnergrid .gridcontent div.data-content div.data-rows').length;
		   if(gridlength == 0){
				var purity=$('#rate_purityid').val();
				$('#purityrate').val(purity).trigger('change');
				var rate_item=$('#itemrate').val();
				$('#rate_mode_purewt').val(rate_item);
				$('#salesrateoverlay').fadeIn();	
				$('#purityrate').select2('focus');
          }else{
		        alertpopup('Remove Data in Detail List Grid Then only Update Rate');
				return false;
          }		  
		});
			
		$('#purityrate').change(function(){
			var purity = $(this).val();
			getratebasedonpurity();
			Materialize.updateTextFields(); 
		});
		$("#salesrateclose").click(function(){
			$('#salesrateoverlay').fadeOut();			
		});
		$("#itemrateaddsbtn").click(function(){
			$("#purityratevalidation").validationEngine('validate');
		  });
		$("#purityratevalidation").validationEngine({
			onSuccess: function() {									
					setTimeout(function(){
						rateupdate();
					},10);
				},
			onFailure: function() {
			}
		});
	}
	//delete stone setting
	$("#salesdetaildelete").click(function(){		 	
		var datarowid = $('#salesinnergrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			combainedmoduledeletealert('salesdetailsdeleteyes','Are you sure,you want to delete this sales detail?');
			$("#salesdetailsdeleteyes").click(function(){
				var stocktype= getgridcolvalue('salesinnergrid',datarowid,'stocktypeid','');
				salesdetaildelete(datarowid,stocktype);
				$(this).unbind();
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	$('.chargedata').change(function(){
		setzero(['grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge']);
		var chargedetails = new Array();
		var ckeywordcalvalue = new Array();
		var origcalfinalvalue = new Array();
		var ckeywordfinalkey = new Array();
		var ckeywordfinalvalue = new Array();
		var salestransactiontype=$('#salestransactiontypeid').find('option:selected').val();
		var stocktypeid=$('#stocktypeid').find('option:selected').val();
		var chargekeyword=$('#ckeyword').val();
		var chargekeyworddata=chargekeyword.split(",");
		var discounttotal=0;
		var taxablecharge=0;
		var nontaxablecharge=0;
		var grossamount = 0;
		var nontaxamt = 0;
		var taxamt = 0;
		var stonetotamt = 0;
		var productcalctype = $('#product').find('option:selected').data('calctypeid');
	    if(productcalctype  == '2'){
		 var netwt=$("#netweight").val();
	    }else{
		 var netwt = parseFloat($("#grossweight").val());	
		}
    	var grosswt=$("#grossweight").val();	
		var pieces = $('#pieces').val();
		var rate = $('#ratepergram').val();
		var melting = $('#melting').val();
	  	if(chargekeyworddata == ''){
	  	} else {
		 $.each(chargekeyworddata, function (index, value) {
		  	chargedetails.push($("#productaddonform input[keyword="+value).val());
		 	if($.trim($("#productaddonform input[keyword="+value).val()) == ''){
				var chargevalue=0;
				var orginalchargevalue=0;
				$("#productaddonform input[keyword="+value).attr('originalvalue',0);
		  	} else {
		  		var chargevalue=$("#productaddonform input[keyword="+value).val();
		   		var orginalchargevalue=$("#productaddonform input[keyword="+value).attr('originalvalue');
		  	}
		  switch(value) {
			case 'HM-FT':  // HALL MARK FLAT
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
                var originalfinalvalue=(parseFloat(orginalchargevalue)).toFixed(roundamount);
                break;
			case 'HM-PI':  // HALL MARK PER PIECE
				var calfinalvalue=(parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
                var originalfinalvalue=(parseFloat(pieces)*parseFloat(orginalchargevalue)).toFixed(roundamount);
                break;
			case 'CC-FT':  // CERTIFICATION  FLAT
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
                var originalfinalvalue=(parseFloat(orginalchargevalue)).toFixed(roundamount);
                break;
				default:
		  }
		  if(parseInt(chargevalue) <= parseInt(orginalchargevalue)) {// check discount value
		  	  var discountvalue=parseFloat(originalfinalvalue)-parseFloat(calfinalvalue);
          } else {
               var discountvalue=0;
          } 	
		  if(orginalchargevalue == ''){
				var discountvalue=0;
		  }
          $("#productaddonform input[keyword="+value).attr('calfinalvalue',calfinalvalue);
		   if(salestransactiontype == 9) {
			   $("#productaddonform input[keyword="+value).attr('origcalfinalvalue',0);
		   }else{
			$("#productaddonform input[keyword="+value).attr('origcalfinalvalue',originalfinalvalue);
		   }
		  $("#productaddonform input[keyword="+value).attr('discountvalue',discountvalue);
		 discounttotal = parseFloat(discounttotal)+parseFloat($("#productaddonform input[keyword="+value).attr('discountvalue'));
          ckeywordcalvalue.push($("#productaddonform input[keyword="+value).attr('calfinalvalue'));
		  origcalfinalvalue.push($("#productaddonform input[keyword="+value).attr('origcalfinalvalue'));
		  ckeywordfinalkey.push(value+':'+$("#productaddonform input[keyword="+value).attr('calfinalvalue'));
		  ckeywordfinalvalue.push(value+':'+$("#productaddonform input[keyword="+value).attr('origcalfinalvalue'));
		  if($("#productaddonform .chargedetailsdiv input[keyword="+value).hasClass( "charge" )) {
			     taxablecharge = parseFloat(taxablecharge)+parseFloat($("#productaddonform .charge[keyword="+value).attr('calfinalvalue'));
			  } else {
				  taxablecharge = parseFloat(taxablecharge)+0;
			  }
	 });
	} 
		var extracalckeyword = new Array();
	    var extracalcvalue = new Array();
		
		   $('.extracalc').each(function(){
			  extracalckeyword.push($(this).attr('keyword'));
			var keywordvalue=$(this).attr('keyword');
			switch(keywordvalue) {
			   case 'totalchargeamount':
				   var otherdetailscharge=taxablecharge;
			 break;
			  case 'additionalchargeamt':  // Charges Amt (NT)
			       var otherdetailscharge=taxablecharge;
			   break;
			   default:
			}
			$(this).val(otherdetailscharge);
			extracalcvalue.push($(this).val());
		}); 
		$('#otherdetails').val(extracalckeyword);
        $('#otherdetailsvalue').val(extracalcvalue);
		var taxdataid=$('#product').find('option:selected').data('taxmasterid');
		$('#ckeywordvalue').val(chargedetails);
		$('#ckeywordcalvalue').val(ckeywordcalvalue);
		$('#ckeywordfcalvalue').val(origcalfinalvalue);
		$('#ckeywordfinalkey').val(ckeywordfinalkey);
		$('#ckeywordfinalvalue').val(ckeywordfinalvalue);
		if(chargesdonttriggergrosscalc == 0){
			totalgrossamountcalculationwork();
		}
		Materialize.updateTextFields();
     });
	
	$('#salesdetailedit').click(function(){
		var datarowid = $('#salesinnergrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$("#transactionsectionoverlay").removeClass("closed");
			$("#transactionsectionoverlay").addClass("effectbox");
			$("#modeid").attr('readonly',true);
			$("#estimationnumber").attr('readonly',true);
			$("#product,#purity").prop('readonly',true);
			$('#salesdetailsubmit,#salesdetailedit,#salesdetaildelete,#paymentdetailsubmit').hide();
			$('#salesdetailupdate,#paymentdetailupdate').show();
			retrive(datarowid);
			compybasednetmccalc();
		} else {
			alertpopup(selectrowalert);
		}	
	});
	$('#unsavedbillicon').click(function(){
		$('#unsavedbilloverlay').fadeIn();
		setTimeout(function(){
			$("#unsavedbilloverlay .fwgpaging-box ").find('i').removeClass("icon24");
			$("#unsavedbilloverlay .fwgpaging-box ").find('i').addClass("icon24-w");
		},100);
		$('#billnameset').text("Unsaved Bill");
		$('#savedbillstatus').val('no');
		billgrid();
	});
	$('#savedbillicon').click(function(){
		$('#unsavedbilloverlay').fadeIn();
		setTimeout(function(){
			$("#unsavedbilloverlay .fwgpaging-box ").find('i').removeClass("icon24");
			$("#unsavedbilloverlay .fwgpaging-box ").find('i').addClass("icon24-w");
		},100);
		$('#billnameset').text("Saved Bill");
		$('#savedbillstatus').val('yes');
		billgrid();
	});
	$('#billcloseicon').click(function(){
		$('#unsavedbilloverlay').fadeOut();
	});
	$('#billediticon').click(function(){
		var datarowid = $('#billgrid div.gridcontent div.active').attr('id');
		if(datarowid) {	
			$('#transactionmodeid').select2('val',2).trigger('change');
			billdataretrive(datarowid);
			$('#unsavedbilloverlay').fadeOut();
		} else {
			alertpopup("Please select a row");
		}
	});
	$('#billdeleteicon').click(function(){
		var datarowid = $('#billgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				billdeletestatus=1;
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup(selectrowalert);
			}
	});
	
	$("#dataaddsbtn").click(function() {
		if(salesdetailentryeditstatus == 1) {
			alertpopup('Inner Edit process is on. Kindly Save that and then Do Bill save');
			return false;
		} else {
			var salesid=$('#generatesalesid').val();
			if(billeditstatus == 0) {
				getcurrentsytemdate("balanceduedate");
			}
			var salestransactiontypeid=$.trim($('#salestransactiontypeid').find('option:selected').val());
			var stocktypeid=$.trim($('#stocktypeid').find('option:selected').val());
			var gridrowdata = getgridallrowids('saledetailsgrid');
			transactionbasedbilltemphideshow();
			var grossamount = $('#grossamount').val();
			var pureweight = $('#pureweight').val();
			var sumarypendingamount = $('#sumarypendingamount').val();
			var sumarypendingwt = $('#balpurewthidden').val();
			var paymenttotalamount = $('#paymenttotalamounthidden').val();
			{// purchase wt check based on companysettings
				if(salestransactiontypeid == 9 && stocktypeid != 73  &&  $("#weightcheck").val() == 1 ) {
					var purchasecheckvalidate = purchaseweightvalidation(3);
					if(purchasecheckvalidate == false){
					   return false;
					}
				}
			}
			{// order tag sales - check for usage of advance
				/* if(salestransactiontypeid == 11){ // order tag
					var allstocktypeid = $.trim(getgridcolcolumnvalue('saledetailsgrid','','stocktypeid'));
					var deliveryorderstatus = 0;
					var ordercount = 0;
					var details=allstocktypeid.split(",");
					 $.each(details, function (index, value) {
							if(value == '83'){
							  deliveryorderstatus = 1;
							  ordercount++;
							}
							
						});
					if(parseFloat($('#orderadvamtsummary').val()) != parseFloat($('#orderadvamtsummaryhidden').val()) && $('#salesdetailcount').val() == ordercount && deliveryorderstatus == 1)
					{
						alertpopup('Please use order advance fully');
						return false;
					}
					if($('#orderadvcreditno').val() != '' && deliveryorderstatus == 0)
					{
						alertpopup('Please make order item');
						return false;
					}
					
				} */
			}
			var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
			if((salestransactiontypeid == 22 && ratecuttypeid == 2) || (salestransactiontypeid == 23 && ratecuttypeid == 2) || salestransactiontypeid == 13 || salestransactiontypeid == 26){ // ratecut in payment i/r is active
				$("#balaccountsave").trigger("click");
			} else { 
				if(gridrowdata != '') {
					{// restrict on mainsave for if only  payment entry given during sales etc
						var salesdetailid = gridrowdata.toString();
						var transactionmodestatus = 0;
						var salesdetails=salesdetailid.split(",");
						$.each(salesdetails, function (index, value) {
						var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
							if(modestatus == '2'){
									transactionmodestatus = 1;
							}
						});
					}
			
					if(transactionmodestatus == 0) {
						if(salestransactiontypeid == 22 || salestransactiontypeid == 23 || salestransactiontypeid == 25) { // payment issue/receipt & contra
							var manualstatus = accountmanualstatus();
							if((sumarypendingamount > 0 || sumarypendingamount < 0 || sumarypendingwt > 0 || sumarypendingwt < 0) &&  manualstatus == 1)
							{
								alertpopup('Kindly clear the bill balance completely before you could save.');
							}else{
								$("#balaccountsave").trigger("click");
							}
						} else if(salestransactiontypeid == 24 ) {
							var spanpaidpurewt = $('#spanpaidpurewt').text();
							var spanissuepurewt = $('#spanissuepurewt').text();
							var sumpaidamt = $('#sumpaidamt').text();
							var sumissueamt = $('#sumissueamt').text();
							var finalamt = parseFloat(sumpaidamt) - parseFloat(sumissueamt);
							var finalwt = parseFloat(spanpaidpurewt)- parseFloat(spanissuepurewt);
							if(finalamt > 0 || finalwt > 0 || finalamt < 0 || finalwt < 0) {
								alertpopup('Kindly enter Equal paid and issue Amount');
							} else {
								$("#balaccountsave").trigger("click");
							}
						}else{
							alertpopup('You Must Add Sales Detail Entry Before Creation Bill');
							return false;
						}
					} else {
						if(salestransactiontypeid == 16 || salestransactiontypeid == 18) { // estimate app out
							$("#balaccountsave").trigger("click");
						} else if(salestransactiontypeid == 20) { // Take order
							{// order advance calculation
								var sumpaidamt =  $('#sumpaidamt').text();
								var sumissueamt = $('#sumissueamt').text();
								var sumpaidwt =  $('#spanpaidpurewt').text();
								var sumissuewt = $('#spanissuepurewt').text();	
								var oldjewelwtsummary  = $("#oldjewelwtsummary").val(); 
								var oldjewelamtsummary  = $("#oldjewelamtsummary").val();
								var takeorderadvanceamt = parseFloat(oldjewelamtsummary) + ((parseFloat(sumpaidamt)) - (parseFloat(sumissueamt)));
								var takeorderadvancewt = parseFloat(oldjewelwtsummary) + ((parseFloat(sumpaidwt)) - (parseFloat(sumissuewt)));
								var finalordersummary = 0;
								var takeordermodestatus = 0;
								var receivedordermodestatus = 0;
								var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
							}
							var takeordersalesdetailid = gridrowdata.toString();
							var takeordersalesdetails=takeordersalesdetailid.split(",");
							$.each(takeordersalesdetails, function (index, value) {
								var takeordertypeid = getgridcolvalue('saledetailsgrid',value,'stocktypeid','');
								if(takeordertypeid == '75'){
										takeordermodestatus = 1;
								}
								if(takeordertypeid == '82'){
									receivedordermodestatus = 1;
								}
							});
						 
							if(takeordermodestatus == 0 && receivedordermodestatus == 0) { // no order item
							   alertpopup('No order Item entered');
							   return false;
							} else if(takeordermodestatus == 1) {
								var accid=$("#accountid").val();
								var placeorderdefaultaccid=$("#placeorderdefaultaccid").val();						
								if(placeorderdefaultaccid != accid && ((parseFloat(takeorderadvanceamt) <= 0 && transactionmodeid == 2) || (parseFloat(takeorderadvancewt) <= 0 && transactionmodeid == 3) || (parseFloat(takeorderadvancewt) <= 0 && transactionmodeid == 4))){
									alertpopup('Kindly Give Advance For Order');
									return false;
								} else {
									$("#balaccountsave").trigger("click");
								}
							} else if(receivedordermodestatus == 1) {
								$("#balaccountsave").trigger("click");
							}
						} else if(salestransactiontypeid == 11) { //for sales - government regulation
							var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
							setzero['billgrossamount','sumarytaxamount','sumarydiscountamount'];
							var salesamount = $("#billgrossamount").val();
							var taxamout = $("#sumarytaxamount").val();
							var discountamount = $("#sumarydiscountamount").val();
							var saleamount = parseFloat(salesamount)+parseFloat(taxamout)-parseFloat(discountamount);
							if(saleamount >= pancardrestrictamt) { //if sale amount is greaterthan or equal to 5lack customer need to pay pancard number
								$("#processoverly").show();
								$("#pancardoverlay").show();
								getpannumberbasedonaccount($("#accountid").val());
								$("#pancardnumber").focus();
							} else { //if its less than 2.5 lacks no need of pancard num. its directly saved
								if(sumarypendingamount > 0 || sumarypendingamount < 0 || sumarypendingwt > 0 || sumarypendingwt < 0){
									var oldpurchasecredittype = $('#oldpurchasecredittype').val();
									var gridcountdetailsofoldpurchase = getgridcolvaluewithcontition('saledetailsgrid','','stocktypeid','count','stocktypeid','19');
									var gridcountdetailsofsalesreturn = getgridcolvaluewithcontition('saledetailsgrid','','stocktypeid','count','stocktypeid','20');
									var gridcountdetailsofsalestrantype = getsalesstocktypegridallrowids('saledetailsgrid','all').length;
									gridcountdetailsofsalestrantype = typeof gridcountdetailsofsalestrantype == 'undefined' ? '0' : gridcountdetailsofsalestrantype;
									if(gridcountdetailsofoldpurchase > 0 && gridcountdetailsofsalesreturn == 0 && gridcountdetailsofsalestrantype == 0 && oldpurchasecredittype == 1) {
										oldpurchasecredittype = $('#oldpurchasecredittype').val();
									} else {
										oldpurchasecredittype = 0;
									}
									savealertpopuphideshow(transactionmodeid,sumarypendingamount,sumarypendingwt,oldpurchasecredittype);
								}else{
									$("#balaccountsave").trigger("click");
								}
							}
						} else if(salestransactiontypeid == 9) {
							var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
							if(sumarypendingamount > 0 || sumarypendingamount < 0 || sumarypendingwt > 0 || sumarypendingwt < 0){
								savealertpopuphideshow(transactionmodeid,sumarypendingamount,sumarypendingwt,0);
							} else {
								$("#balaccountsave").trigger("click");
							}
						} else {
							$("#balaccountsave").trigger("click");
						}
					}
				} else {
					alertpopup('No records in sales or payment list,please do entries and then save');
				}
			}
		}
	});
	//pancard overlay work for government regulation - start
	$("#pancardcancel").click(function() {
		$("#pancardoverlay").hide();
	});
	$("#pancardsubmit").click(function() {
		$("#pancardoverlayformvalidation").validationEngine('validate');
	});
	$("#pancardoverlayformvalidation").validationEngine({
		onSuccess: function() {
			$("#pancardoverlay").hide();
			$("#balaccountsave").trigger("click");
		},
		onFailure: function() {
			alertpopup('Please Choose required fields ');
		}
	});
	//pancard overlay work for government regulation - end
	$("#balaccountsave").click(function() {
		$("#salesbalanceform").validationEngine('validate');
	});
	$("#salesbalanceform").validationEngine({
		onSuccess: function() {
			var salestransactiontypeid = $.trim($('#salestransactiontypeid').find('option:selected').val());
			var stocktypeid = $.trim($('#stocktypeid').find('option:selected').val());
			$("#processoverlay").show();
			var salesid = $('#generatesalesid').val();
	   		var salesnumber = $('#generatesalesnumber').val();
			var printtemplateid = 1;
			if(salestransactiontypeid == 11) {
				printtemplateid = mainprintemplateid;
			} else if(receivestatus == 1) { // takeorder - receive
				printtemplateid = 1;
			} else {
				printtemplateid = $.trim($('#printtemplateid').find('option:selected').val());
			}
			if(mainsaveclicksuccess == 0) {
				if(printtemplateid === undefined) {
					printtemplateid = 1;
				} else if(printtemplateid != 1 && printtemplateid !='') {
					printtemplateid = printtemplateid;
				} else {
					printtemplateid = 1;
				}
				if(salestransactiontypeid == 27 && questionaireapplicable == 1) {
					$('#questionairegenpovendoroverlay').fadeIn();
					$('#question1,#question2,#question3,#question4,#question5').select2('val','NO').trigger('change');
					$('#vendorquestionaireprintid').val(printtemplateid);
				} else if(salestransactiontypeid == 20 && stocktypeid == 82 && questionaireapplicable == 1) {
					$('#questionairerecordadminoverlay').fadeIn();
					$('#questionrecord1,#questionrecord2,#questionrecord3,#questionrecord4,#questionrecord5,#questionrecord6').select2('val','NO').trigger('change');
					$('#adminquestionaireprintid').val(printtemplateid);
				} else {
					salenumbergenerate(printtemplateid);
				}
			} else {
				alertpopup('Already Clicked Save and Process is going on');
			}
			$("#processoverlay").hide();
		},
		onFailure: function() {
			alertpopup('Please Choose required fields ');
		}
	});
	//balance -account close
	$("#balaccountclose").click(function() {
		clearform('salesbalanceform');
		$("#salesbalanceoverlay").fadeOut();
	});	
	$('#closeaddform').click(function() {
		var ids = getgridallrowids('saledetailsgrid');
		salesrefreshgrid();
		mainsavestatus = 0;
		billeditstatus = 0;
		if(ids != '') {
			alertpopupdouble('Data in Inner Grid');
		}
	});
	$('#alertsfcloseyes').click(function() {
		$('#mainheaderoverlay').hide();
		retrievealltaxdetailinformation();
	});
	/* $('#alertsfcloseyes').click(function() {
		var datarowid = $('#generatesalesid').val();
		$('#s2id_accountid a span:first').text('');
		$("#accountid").val('');
		$('#ordermodeid').select2('val','');
		if(datarowid != '') {
			 if(billeditstatusclose == 0) {
		       mainsalesbilldelete(datarowid);
			 }
		}
	}); */
	// Questionaire trigger - Vendor Page save calls main submit
	$('#questionairegenpovendorformsubmit').click(function() {
		$("#questionairegenpovendorformvalidation").validationEngine('validate');	
	});
	jQuery("#questionairegenpovendorformvalidation").validationEngine({
		onSuccess: function() {
			var printtemplateid = $('#vendorquestionaireprintid').val();
			questionairegenpovendorarray = [];
			questionairegenpovendorarray.push($('#question1').find('option:selected').val());
			questionairegenpovendorarray.push($('#question2').find('option:selected').val());
			questionairegenpovendorarray.push($('#question3').find('option:selected').val());
			questionairegenpovendorarray.push($('#question4').find('option:selected').val());
			questionairegenpovendorarray.push($('#question5').find('option:selected').val());
			$('#questionairegenpovendoroverlay').fadeOut();
			salenumbergenerate(printtemplateid);
		},
		onFailure: function() {
			var dropdownid = [];
			dropdownfailureerror(dropdownid);
		}
	});
	// Questionaire trigger - Vendor Page (cancel button)
	$('#questionairegenpovendorformclose').click(function() {
		$('#questionairegenpovendoroverlay').fadeOut();
	});
	// Questionaire trigger - Admin Page save calls main submit
	$('#questionairerecordadminformsubmit').click(function() {
		$("#questionairerecordadminformvalidation").validationEngine('validate');	
	});
	jQuery("#questionairerecordadminformvalidation").validationEngine({
		onSuccess: function() {
			var printtemplateid = $('#adminquestionaireprintid').val();
			questionairerecorderadminarray = [];
			questionairerecorderadminarray.push($('#questionrecord1').find('option:selected').val());
			questionairerecorderadminarray.push($('#questionrecord2').find('option:selected').val());
			questionairerecorderadminarray.push($('#questionrecord3').find('option:selected').val());
			questionairerecorderadminarray.push($('#questionrecord4').find('option:selected').val());
			questionairerecorderadminarray.push($('#questionrecord5').find('option:selected').val());
			questionairerecorderadminarray.push($('#questionrecord6').find('option:selected').val());
			$('#questionairerecordadminoverlay').fadeOut();
			salenumbergenerate(printtemplateid);
		},
		onFailure: function() {
			var dropdownid = [];
			dropdownfailureerror(dropdownid);
		}
	});
	// Questionaire trigger - Admin Page (cancel button)
	$('#questionairerecordadminformclose').click(function() {
		$('#questionairerecordadminoverlay').fadeOut();
	});
	$('#datadeletesbtn').click(function() {
		var datarowid = $('#generatesalesid').val();
		if(datarowid != '') {
			billdeletestatus=2;
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
		} else {
			alertpopup('Unable to delete Sales data.Becuase sales data not available');
		}
	});
	$('#formclearicon').click(function() {
		clearform('salesaddform');
		clearform('productaddonform');
		clearform('accountcreateform');
		$('#addicon').trigger("click");
		$("#productaddonform label span[class='orginalvalclass']").text('');
	});
	$('#voucherno').change(function(){  // only get rough estimate sales details
		var salesdetailid=$(this).val();
		if(checkValue(salesdetailid) == true ){
		   getsalesdetails(salesdetailid);
		}
	});
	$('#salesdiscounticon').click(function(){
		$('#discamount').val('');
		$("#salesdiscountoverlay").fadeIn();
		$('#discamount').focus();
	});
	$('#discountclose').click(function(){
		$("#salesdiscountoverlay").fadeOut();
	});
	$('#discountsave').click(function(){
		$("#salesdiscountform").validationEngine('validate');
		
	});
	$('#issuereceipticon').click(function() {
		var accountid = $("#accountid").val();
		var manualstatus = 0;
		if(accountid == '') {
			alertpopup('Please select account');
		} else {
			$('#processoverlay').show();
			var manualstatus = accountmanualstatus();
			if(manualstatus == 0) { 
				$('#processoverlay').hide(); 
				//  alertpopup('You cannot select credit no for Non-Billwise A/c');
			} else {
				creditoverlaydataset();
				$('#processoverlay').hide();
				$('#creditreference').val(3).trigger('change');
			}
		}
	});
	$('#taxgridsubmit').click(function(){
		var salestaxtypeid = $("#taxtypeid").val();
		var stocktypeid=$('#stocktypeid').find('option:selected').val();
		if((oldsalesreturntax == '1' && stocktypeid == '20') || (oldsalesreturntax == '1' && stocktypeid == '19') || (oldsalesreturntax == '1' && stocktypeid == '73') ){
			salestaxtypeid = 2;
		}
		var taxcategoryid = $.trim($('#taxcategory').find('option:selected').val());
		if(taxcategoryid == '' )
		{
		  $('#taxamount,#summarytaxamount').val(0); 
		  $('#taxdetails,#taxgriddata,#summarytaxgriddata').val('');
		  billleveltaxautoremoval = 1;
		}else
		{billleveltaxautoremoval = 0;}
		
		if(salestaxtypeid == '2'){
			var rowid = getgridallrowids('taxgrid');
			if(rowid == ''){oldsalesreturntax = 2;}
			totalgrossamountcalculationwork();
		}
		else{
			finalsummarycalc(0);
		}
		oldsalesreturntax = 0;
	});
	$("#salesdiscountform").validationEngine({
		onSuccess: function() {
			var value=$('#discamount').val();
			var previousamt=$('#discamount').attr('prediscamount');
			var datarowid = $('#generatesalesid').val();
			if(datarowid != '') {
				if(parseInt(value) == parseInt(previousamt)) {
					alertpopup('Discount is same');
				} else {
					discountadd(datarowid,value);
				}
			} else {
				alertpopup('No records in sales or payment list,please do entries and then save');
			}
		},
		onFailure: function() {					
		}
	});
	//prints
	$('#printicon').click(function(){
		var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
		var snumber = $('#salesgrid div.gridcontent div.active ul li.salesnumber-class').text();
		if (datarowid) {
			$('#printpdfid').val(datarowid);
			$('#printsnumber').val(snumber);
			$("#templatepdfoverlay").fadeIn();
			setdefaultprinttemplate(datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#pdftemplateprview").change(function() {
		var defaultpdftemplateid = $('#defaultpdftemplateid').val();
		var newtemplateid = $(this).val();
		$('#printmultipleview-div').addClass('hidedisplay');
		if(newtemplateid != defaultpdftemplateid) {
			$('#printmultipleview-div').addClass('hidedisplay');
			$('#printmultipleview').select2('val','0').trigger('change');
		} else {
			$('#printmultipleview-div').removeClass('hidedisplay');
			$('#printmultipleview').select2('val',1).trigger('change');
		}
		var stockdate = $("#pdftemplateprview").find("option:selected").data("stockdate");
		if(stockdate == 'Yes') {
			$("#dateofstockdivhid,#dateofstocktodivhid").removeClass('hidedisplay');
		} else {
			$("#dateofstockdivhid,#dateofstocktodivhid").addClass('hidedisplay');
		}
	});
	$('#billprinticon').click(function(){
		var datarowid = $('#billgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$('#printpdfid').val(datarowid);
			$("#templatepdfoverlay").fadeIn();			
			setdefaultprinttemplate(datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
	$('#netpurewt').change(function(){
		var paymode = $('#stocktypeid').val();
			if(paymode != '49')
			{
		      $('#pureweight').val($(this).val());
			}
	});
	$('#approvalnumber').change(function(){
		var approvalnumber= $(this).find('option:selected').val();
		var stocktypeid = $("#stocktypeid").find('option:selected').val();
		var approvalrfid = $("#approvalrfid").find('option:selected').val(); 
		if(approvalnumber != '1' && approvalnumber != ''){
			if((approvaloutrfid == 3 && approvalrfid == 0 && stocktypeid == 65) || stocktypeid == 66 || stocktypeid == 25){
				approvalreturngrid();
				$("#approvalreturnoverlay").show();
				approvalreturngriddatafetch(approvalnumber,stocktypeid);
			} else {
				$('#appoutrfidbtn').trigger('click');
			}
		}
	});
	$('#ordernumber').change(function() {
		$('#product,#purity,#counter').select2('val','');
		$('#grossweight,#netweight,#pieces,#ratepergram,#itemcaratweight').val('');
		var ordernumberid = $(this).val();
		var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		var stocktypeid = $("#stocktypeid").find('option:selected').val();
		var salesdetailid = $('#ordernumber').find('option:selected').attr('salesdetailid');
		var accountid = $('#ordernumber').find('option:selected').data('accountid');
		var accountname = $('#ordernumber').find('option:selected').data('accountname');
		var takeordernumber = $('#ordernumber').find('option:selected').data('ordernumber');
		
		var ordernumber = getgridcolcolumnvalue('saledetailsgrid','','ordernumbername');
		if($('#accountid').val() == '') {
			$('#ordernumber').select2('val','');	
			alertpopup('Please select the account');
			return false;
		}
		$('#mainordernumber').val(takeordernumber);
		$('#mainordernumberid').val(ordernumberid);
		
		$('#referencenumber').val(salesdetailid);
		// For placeorder - display takorder detail in overlay || Place Repair - Take repair Details in overlay
		if(transactiontypeid == 21 || transactiontypeid == 30) {
			$("#ordergridoverlay").fadeIn();
			$("#processoverlay").fadeIn();
			ordergriddatafetch();
			$("#processoverlay").fadeOut();
		} else if(transactiontypeid == 27) {//for generate place order - display place order details.
			if(questionaireapplicable == 0) {
				$("#ordergridoverlay").fadeIn();
				$("#processoverlay").fadeIn();
				ordergriddatafetch();
				$("#processoverlay").fadeOut();
			} else {
				var gpoitemscount = $('#saledetailsgrid .gridcontent div.data-content div.data-rows').length;
				if(questionaireapplicable == 1 && gpoitemscount == 0) {
					$("#ordergridoverlay").fadeIn();
					$("#processoverlay").fadeIn();
					ordergriddatafetch();
					$("#processoverlay").fadeOut();
				} else {
					alertpopupdouble('Cannot add multiple items to grid. Use single item to give Questionaire details.');
					return false;
				}
			}
		} else if(transactiontypeid == 20 || transactiontypeid == 29) {//for receiveorder - display detail in overlay
			$("#ordergridoverlay").fadeIn();
			$("#processoverlay").fadeIn();
			ordergriddatafetch();
			$("#processoverlay").fadeOut();
		} else if(transactiontypeid == 11 || transactiontypeid == 16) {
			if(stocktypeid == 83 || stocktypeid == 89) { // order tag & repair item
				if(takeordernumber === undefined) {
				
				} else if(ordernumber != '') {
				   /** as of now we are multiple orders from multiple items, so we removed the code **/
				  /* if (jQuery.inArray(takeordernumber, ordernumber) == '-1') {
					   $('#ordernumber').select2('val','');	
					   alertpopup('Already order number used');
						return false;
				   } */
				}
				$("#ordergridoverlay").fadeIn();
				$("#processoverlay").fadeIn();
				ordergriddatafetch();
				$("#processoverlay").fadeOut();
			}
		} else {
			if(approvalnoeditstatus == 0) {
				setsalesdetails(salesdetailid);
			}
		}
	});
	$('#interestcalc').change(function(){
		var value = $(this).val();
		var totalpaidamount=$('#totalpaidamount').val();
		var calcvalue=0;
		var interestval=0;
		var interestcalcval=0;
		if(value.match('^-') && value.match('%$')){
			var res=value.split('-'); 
			calcvalue=res[1].split('%'); 
			interestcalcval = parseFloat((parseFloat(calcvalue[0]) * parseFloat(totalpaidamount)) / 100).toFixed(roundamount) ;
			$('#interestamount').val('-'+interestcalcval);
		   interestval = parseFloat(parseFloat(totalpaidamount) - parseFloat(interestcalcval)).toFixed(roundamount);
		}else if(value.match('%$')){
			var res=value.split('%'); 
			calcvalue=res[0]; 
			interestcalcval = parseFloat((parseFloat(calcvalue) * parseFloat(totalpaidamount)) / 100).toFixed(roundamount) ;
			$('#interestamount').val(interestcalcval);
		   interestval = parseFloat(parseFloat(totalpaidamount) + parseFloat(interestcalcval)).toFixed(roundamount);
		}
		else if(value.match('^-')){
			var res=value.split('-'); 
			calcvalue=res[1];
			$('#interestamount').val(value);
			interestval = parseFloat(parseFloat(totalpaidamount) - parseFloat(calcvalue)).toFixed(roundamount);
		}else{
			calcvalue=value;
			$('#interestamount').val(value);
			interestval = parseFloat(parseFloat(calcvalue)+parseFloat(totalpaidamount)).toFixed(roundamount);
		}
		$('#totalamount').val(interestval);
		Materialize.updateTextFields();
		
	});
	
	$('#paymenttouch,#stonecharge').change(function() {
		var stocktypeid = $("#stocktypeid").find('option:selected').val();
		setzero(['grossweight','stoneweight','dustweight','wastageweight','netweight','grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge','ratepergram','proditemrate','itemcaratweight']);
		if(wastagecalctype == 5 || wastagecalctype == 3) {
			var touch = checknan($('#paymenttouch').val());
			var melting = checknan($('#melting').val());
			var wastagepercent = 0;
			if(parseFloat(touch) >= parseFloat(melting)) {
				var wastagepercent =  parseFloat(parseFloat(touch) - parseFloat(melting)).toFixed(3);
				$("#wastageclone").val(wastagepercent).trigger('change');
			} else if(parseFloat(touch) < parseFloat(melting)) {
				$("#wastageclone").val(0);
			}
		}
		setTimeout(function() {
			if(stocktypeid == 19 && oldjewelsstonedetails == 1) {
				oldjewelcalc();
			}
			totalgrossamountcalculationwork();
		},10);
	});
	// Rate / Gm change function
	$('#ratepergram').change(function(){
		$("#modeidtype").val(2);
		setzero(['netweight','grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge','ratepergram','wastageclone','makingchargeclone']);
		var stocktypeid=$('#stocktypeid').find('option:selected').val();
		var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
		var val = $(this).val();
		if(stocktypeid == 19) {
			oldjewelcalc();
			if(oldjewelsstonedetails == 1) { totalgrossamountcalculationwork(); }
		} else if(stocktypeid != 19) {
			chargesdonttriggergrosscalc = 1;
			$('.chargedata,#wastageclone,#makingchargeclone,#flatchargeclone,#repairchargeclone').trigger("change");
			totalgrossamountcalculationwork();
			chargesdonttriggergrosscalc = 0;
		}
	});
	// Item Rate - Change function
	$('#proditemrate').change(function() {
		setzero(['netweight','grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge','ratepergram','wastageclone','makingchargeclone']);
		var stocktypeid=$('#stocktypeid').find('option:selected').val();
		var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
		var val = $(this).val();
		if(stocktypeid == 17 && tagtypeid == 5) {
			if(val > 0) {
				chargesdonttriggergrosscalc = 1;
				$('.chargedata,#wastageclone,#makingchargeclone,#flatchargeclone,#repairchargeclone').trigger("change");
				totalgrossamountcalculationwork();
				chargesdonttriggergrosscalc = 0;
				var grossamount = $('#grossamount').val();
				if(grossamount > 0 && salesdetailentryeditstatus == 0) {
					$('#salesdetailsubmit').show();
					$('#salesdetailupdate').hide();
				} else if(grossamount > 0 && salesdetailentryeditstatus == 1) {
					$('#salesdetailsubmit').hide();
					$('#salesdetailupdate').show();
				} else {
					$('#salesdetailsubmit').hide();
					$('#salesdetailupdate').hide();
				}
			}
		}
	});
	//payment rateper gram
	$('#paymentratepergram,#paymenttotalamount,#paymentpureweight').change(function(){
		setzero(['paymentratepergram']);
		var type = $('#paymentstocktypeid').val();
		if(type == 37 || type == 38){
			var fieldid = $(this).attr("id");			
			paymentcalculateratecut(fieldid,type);
		}
	}); 
	
	$('#paymentpureweight').change(function() {
		var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		var paymentpureweight = $('#paymentpureweight').val();
		if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
			var a = paymentweightvalidation(paymentpureweight,0);
		}
	});
	$('#paymenttotalamount').change(function() {
		var attrid = $(this).attr('id');
		var paymentstocktypeid = $('#paymentstocktypeid').find('option:selected').val();
		var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		var amount = $('#paymenttotalamount').val();
		var cashtenthousand = $('#cashtenthousand').val();
		if(salestransactiontypeid != 22) {
				if( paymentstocktypeid == 26) {
					if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
						var oldamount = 0;
					} else {
						var oldamount = getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypename','Cash');
					}
					var famount = parseFloat(amount) + parseFloat(oldamount);
					if(famount > cashreceiptlimit) {
						var validamount = cashreceiptlimit - parseFloat(oldamount);
						$('#paymenttotalamount').val('');
						if(oldamount == 0) {
							alertpopup('Please enter the cash amount below or equal to 2 lacks');
						} if(validamount == 0 ) {
							alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount using cash. Please choose other type');
						} else {
							alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
						}
					}
				}
			} else {
				if( paymentstocktypeid == 26) {
					if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
						var oldamount = 0;
					} else {
						var oldamount = getgridcolvalue('saledetailsgrid','','totalamount','sum');
					}
					var famount = parseFloat(amount) + parseFloat(oldamount);
					if(cashtenthousand ==0)
					{
						if(famount > cashissuelimit) {
							var validamount = cashissuelimit - parseFloat(oldamount);
							$('#paymenttotalamount').val(0);
							if(oldamount == 0 ) {
								alertpopup('Please enter the cash amount below or equal to 10000');
							} if(validamount == 0 ) {
								alertpopup('You already added '+oldamount+' Rs. You cant add anymore amount in this issue. Please save it');
							} else {
								alertpopup('You already added '+oldamount+' Rs. Please enter the cash amount below or equal to '+validamount+' Rs');
							}
						}
					}
				}
			}
			if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
				var a = paymentamountvalidation(amount,0);
			}
	});
	
	// vishal
	$('.payemntpurewtpayment').change(function(){
		var paymode = $('#paymentstocktypeid').val();
		var fieldid = $(this).attr("id");
		var salestransactiontypeid = $("#salestransactiontypeid").val();
		/* if(salestransactiontypeid == 22) { //payment issue
			if(fieldid == 'paymentgrossweight'){
				//validategetweight(paymode,'5',fieldid);
			}else if(fieldid == 'paymentnetweight'){
				//validategetweight(paymode,'4',fieldid);
			}
		} */
		if(paymode == '49' && salestransactiontypeid != 26)	{
			var newtouchvalue = 0;
			var producttypekacha = $('#paymentproduct').find('option:selected').data('kacha');
			var wtpaymenttouch = $("#salestransactiontypeid").find('option:selected').data('wtpaymenttouch');
			var productid = $('#paymentproduct').find('option:selected').val();
			if(fieldid == 'paymentgrossweight')	{
				$('#paymentnetweight').val($('#paymentgrossweight').val());
			}
			var pnetwt = $('#paymentnetweight').val();
			var plesswt = $('#paymentlessweight').val();
			var pfinalwt = parseFloat(pnetwt - plesswt).toFixed(5);
			var ptouch  = $('#newpaymenttouch').val();
			var melting  = $('#paymentmelting').val();
			// touch value include
			if(productid != '' && wtpaymenttouch == 1 && (wastagecalctype == 5 || wastagecalctype == 3)) {
				newtouchvalue = loadproductchargeforpayment();
				if(producttypekacha == 1) { // Normal Sale Product
					if(checkValue(newtouchvalue) == true && newtouchvalue != 0) {
						$('#newpaymenttouch').val(newtouchvalue);
						var ppurewt = (parseFloat(pfinalwt) * (parseFloat(newtouchvalue)/100)).toFixed(5);
						var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
					} else {
						var ppurewt = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5);
						var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
					}
				} else if(producttypekacha == 2) { // Only for Kacha Product and in product table it is stored as 2
					if(checkValue(ptouch) == true && ptouch != 0) {
						var meltingvalue = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5);
						var ppurewt = (parseFloat(meltingvalue) * (parseFloat(ptouch)/100)).toFixed(5);
						var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
					} else {
						var ppurewt = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5);
						var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
					}
				} else if(producttypekacha == 3) { // Bullion Product
					if(checkValue(newtouchvalue) == true && newtouchvalue != 0) {
						var meltingvalue = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5);
						var ppurewt = (parseFloat(meltingvalue) * (parseFloat(newtouchvalue)/100)).toFixed(5);
						var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
					} else {
						if(checkValue(ptouch) == true && ptouch != 0){
							var meltingvalue = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5); 
							var ppurewt = (parseFloat(meltingvalue) * (parseFloat(ptouch)/100)).toFixed(5); 
							var ppurewt = parseFloat(ppurewt).toFixed(roundweight); 
						} else {
							var ppurewt = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5); 
							var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
						}
					}
				}
			} else {
				if(checkValue(ptouch) == true && ptouch != 0){
					var meltingvalue = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5); 
					var ppurewt = (parseFloat(meltingvalue) * (parseFloat(ptouch)/100)).toFixed(5); 
					var ppurewt = parseFloat(ppurewt).toFixed(roundweight); 
				} else {
					var ppurewt = (parseFloat(pfinalwt) * (parseFloat(melting)/100)).toFixed(5); 
					var ppurewt = parseFloat(ppurewt).toFixed(roundweight);
				}
			}
			$('#paymentpureweight').val(checknan(ppurewt));  
			Materialize.updateTextFields();
		}else if( salestransactiontypeid == 26){
			$('#paymentpureweight').val(0);  
			if(fieldid == 'paymentgrossweight')	{
				$('#paymentnetweight').val($('#paymentgrossweight').val());
			}
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,salesgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			salesgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	{//item detail based tax overlay - tax amount on click and focus
		$('#taxamount').on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				var stocktypeid=$('#stocktypeid').find('option:selected').val();
				oldsalesreturntax = 1;
				taxoverlaydisplay(stocktypeid);
			}
		});
		//bill based tax overlay
		$("#sumarytaxamount").click(function() {
			var taxamount = $('#sumarytaxamount').val();
			var billgrossamount = $('#billgrossamount').val();
			var length = $('#saledetailsgrid .gridcontent div.data-content div.data-rows').length;
			if(length >=1 ) {
				if(billgrossamount == 0) {
					alertpopup('Tax is not applicable');
				} else {
					billtaxoverlaydisplay();
				}
			} else {
				alertpopup('Please add sales details');	
			}
		});
	}
	// Item Level Tax Category change and Load Accordingly
	$("#taxcategory").change(function() {
		var id = $("#taxcategory").find('option:selected').val();
		var transaction = '';
		var tagtype = '';
		var netamount = '';
		if( typeof id === 'undefined' || id === null || id == '' ){
			cleargriddata('taxgrid');
			//$('#taxdetails,#taxgriddata,#summarytaxgriddata').val('');
			$('#taxdetails,#taxgriddata').val('');
		} else {
			cleargriddata('taxgrid');
			var transaction = $('#salestransactiontypeid').find('option:selected').val();
			var tagtype = $('#stocktypeid').find('option:selected').val();
			tagtype = typeof tagtype == 'undefined' ? '1' : tagtype;
			var netamount =  taxamtcalcfromgrossamt(tagtype);
			var netamount =  checknan(netamount);
			if(tagtype == 86 || tagtype == 88 || tagtype == 89) {
				$("#taxcategory").select2('val',4);
				id = 4;
			}
			$.ajax({
				url:base_url+'Sales/taxmasterload?taxmasterid='+id+'&type='+tagtype+'&transmethod='+transaction+"&netamount="+netamount,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('taxgrid',data.rows,'json');
						$('#hiddentaxdata').val(JSON.stringify(data));
						tax_data_total();
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('taxgrid');
					}
					Materialize.updateTextFields();
				},
			});
		}
	});
	$("#taxdeleteicon").click(function() {
		var id = $('#taxgrid div.gridcontent div.active').attr('id');
		if(id) {
			/*delete grid data*/
			deletegriddatarow('taxgrid',id);
			//tax_data_total();
			/* setTimeout(function() {
				
			},200); */
				if($('#taxgrid .gridcontent div.wrappercontent div').length == 0) {
					$('#taxcategory').select2('val','').trigger('change');
					
				}
		} else {
			alertpopupdouble('Select Charge Record');
		}
	});
	//Item Level Tax Overlay FadeOut
	$("#taxclose").click(function() {
		var taxid = $("#taxcatid").val();
		var product = $("#productid").val();
		var taxtypeid = $("#taxtypeid").val();
		if(taxtypeid == 3) { // bill level tax
			var sumarytaxamount = $('#sumarytaxamount').val();
			if(sumarytaxamount == 0){
				$('#taxcategory').select2('val','').trigger('change');
			}else{
				
			}
		}else if(taxtypeid == 2) { // item level tax
			var taxamount = $('#taxamount').val();
			if(taxamount == 0){
				$('#taxcategory').select2('val','').trigger('change');
			}
		}				
		$("#taxoverlay").fadeOut();
	});
	// Bill Level Tax Overlay FadeOut
	$("#billtaxclose").click(function() {
		$("#billtaxoverlay").fadeOut();
	});
	/* //discount
	$("#discount").click(function(){
		if(discountpasswordstatus == 1){
			$('#discountpwdoverlay').show();
		}else{				
			//discountoverlayshow();
		}
	}); */
	$("#discounttypeid").change(function() {
		var type = $(this).find('option:selected').val();
		if(type == ''){
		} else {
			if(type == 4) {
				$(".discountcommon").addClass('hidedisplay');
				$(".discountvoucher").removeClass('hidedisplay');
				$("#giftdenomination").addClass('validate[custom[number],required]');
				$("#giftunit").addClass('validate[custom[number],required]');
				$("#giftnumber").addClass('validate[custom[number],required]');
				var discount_data = $('#discountdata').val();
				var parse_discount = $.parseJSON(discount_data);
				if(parse_discount == null) {
					$("#giftunit").val(1);
					Materialize.updateTextFields();
				} else {
					$("#giftunit").val(1);
				}
				$("#discountpercent,#singlediscounttotal").val('');
			} else {
				/* if(discountauthorization == '1'){
					var employeeid = $('#salespersonid').find('option:selected').val();
					getdiscountvalue(type,employeeid,salesdiscounttypeid);
				} */
				$(".discountcommon").removeClass('hidedisplay');
				$(".discountvoucher").addClass('hidedisplay');
				$("#giftdenomination").removeClass('validate[custom[number],required]');
				$("#giftunit").removeClass('validate[custom[number],required]');
				$("#giftnumber").removeClass('validate[custom[number],required]');
				$("#giftdenomination,#giftunit,#giftnumber,#giftremark,#singlediscounttotal").val('');
			}
		}
	});
	$("#giftdenomination,#giftunit").change(function() {
		giftdiscount_total();
	});
	//bill number overlay start
	$("#billnumberid").change(function() {
		var salesid= $("#billnumberid").val();
		var accountid = $('#accountid').val();
		if(accountid == '') {
			alertpopup('Select the account');
			$('#billnumberid').select2('val','');
			return false;
		} else {
			if(salesid) {
				billnumbergrid();
				$("#billnumberoverlay").show();
				billnumbergriddatafetch(salesid);
			}
		}
	});
	$("#billnumberclose").click(function() {
 	 	cleargriddata('billnumbergrid');
 	 	$("#billnumberid").select2('val','');
 		$("#billnumberoverlay").hide();
 	});
	//Old  jewel voucher overlay
	$("#olditemvouchericon").click(function(){
		cleargriddata('oldjewelvouchergrid');
		var accountid = $('#accountid').val();
		if(accountid == ''){
			alertpopup('Select the account');
			return false;
		}else{
			oldjewelvouchergrid();
			$("#oldjewelvoucheroverlay").show();
			oldjewelvouchergriddatafetch(accountid);
		}
 	});
	$("#oldjewelvoucherclose").click(function() {
 	 	cleargriddata('oldjewelvouchergrid');
 	 	$("#oldjewelvoucheroverlay").hide();
 	});
 	$("#approvalreturnclose").click(function() {
 	 	cleargriddata('approvalreturngrid');
		$('#approvalnumber').select2('val','').trigger('change');
 	 	$("#approvalreturnoverlay").hide();
 	});
	// Credit No Trigger Change in overlay for Payment Issue / Receipt
	$("#creditreference").change(function() {
 	 	var value = $(this).val();
		$('#creditpaymentirtypeid').val(2).trigger('change');
		$('#overlaycreditno-div').removeClass('hidedisplay');
		$('#overlaycreditno').attr('data-validation-engine','validate[required]');
		if(value == 3) {
			$('#overlaycreditno-div').addClass('hidedisplay');
			$('#overlaycreditno').attr('data-validation-engine','');
		} else if(value == 2) {
			$('#overlaycreditno-div').removeClass('hidedisplay');
			$('#overlaycreditno').attr('data-validation-engine','validate[required]');
		}
 	});
 	$("#issuereceiptclose").click(function() {
 	 	if(creditoverlayinnereditstatus == 1) // if in overlay inner edit is active
		{
			alertpopupdouble('Inner edit is active and not yet updated. kindly update and then submit');
		} else {
			var transaction = $('#salestransactiontypeid').find('option:selected').val();
			$('#creditentryadd,#creditentryedit').show();
			$('#creditentryupdate').hide();
			clearceditoverlayform();
			cleargriddata('issuereceiptgrid');
			if(transaction == 24){
				$('#paymentstocktypeid').select2('val',31).trigger('change');
			}else{
				if($('#paymentstocktypeid').find('option:selected').val() == '31') {
					$('#paymentstocktypeid').select2('val',defaultsetpaymentstocktype).trigger('change');
				}
			}
			creditoverlaytriggerplace = 0;
			$("#issuereceiptoverlay").hide();
		}
 	});
	$("#ordergridclose").click(function() {
 	 	cleargriddata('ordergrid');
		$('#ordernumber').select2('val','');
		$("#ordergridoverlay").hide();
 	});
	// issue/receipt overlay submit
	$("#issuereceiptsubmit").click(function() {
		var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		var ids = getgridallrowids('issuereceiptlocalgrid');
		var finalamt = 0;
		var finalwt = 0;
		var paymentmodeid = 0;
		if(ids != '') {
			if(creditoverlayinnereditstatus ==1) { // if in overlay inner edit is active
				alertpopupdouble('Inner edit is active and not yet updated. kindly update and then submit');
			} else {
				var creditoverlaystatus = $('#creditoverlaystatus').val();
				if(creditoverlaystatus == 1) { // Paument issue/receipt - load bill type
					var creditoverlay ={}; //declared as object
					creditoverlay = getgridrowsdata('issuereceiptlocalgrid');
					var creditgridata = JSON.stringify(creditoverlay);
					$('#creditoverlayvaluehidden').val(creditgridata);
					setcreditnoval();
					jsonconvertdata(creditgridata,1);
					setcreditsummary();
					Materialize.updateTextFields();
					creditoverlaytriggerplace = 0;
					if(transactiontypeid == '22' || transactiontypeid == '23') {
						$('#mainheadersubmit').trigger('click');
					}
					$("#issuereceiptoverlay").hide();
				} else if(creditoverlaystatus == 0) { // Credit no Adj
					var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
					var a = 0;
					if(transactionmodeid == 2) {
						var a = paymentamountvalidation($('#credittotalamount').val(),1);
					}else if(transactionmodeid == 3) {
						var a = paymentweightvalidation($('#credittotalpurewt').val(),1);
					}
					if(a == 0) {
						var creditoverlay ={}; //declared as object
						creditoverlay = getgridrowsdata('issuereceiptlocalgrid');
						var creditgridata = JSON.stringify(creditoverlay);
						$('#creditadjustvaluehidden').val(creditgridata);
						var result = JSON.parse(creditgridata);
						var list = new Array();
						$.each(result, function(index) {
							list.push(result[index].overlaycreditno);
						});
						$('#paymentrefnumber').val(list);
						$('#paymenttotalamount').val($('#credittotalamount').val());
						$('#paymentpureweight').val($('#credittotalpurewt').val());
						jsonconvertdata(creditgridata,0);
						$("#salescreateform :input").attr("readonly", true);
						$('#salespersonid,#paymentirtypeid,#loadbilltype,#salestransactiontypeid,#transactionmodeid,#ratecuttypeid').prop('disabled',true);
						$("#salesdate").prop("disabled", true);
						creditoverlaytriggerplace = 0;
						$("#issuereceiptoverlay").hide();
					}
				}
			}
		} else {
			alertpopupdouble('Please Enter The data in Adjustment Entry');
			return false;
		}
	});
	// credit no data push
	$("#issuereceiptpush").click(function() {
		var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
		var checkedlength = $('#issuereceiptgrid .gridcontent div.data-content div input:checked').length;
		var salesdetailsid = '';
		var salesdetailsarray = [];
		if(checkedlength != 0) {
			var checkadvanceentry = getgridprocessvalue('issuereceiptlocalgrid');
			if(checkadvanceentry > 0) {
				alertpopupdouble('You cannot add both Advance and Credit Entry.!');
				return false;
			}
			var salesdetailsid =  $("#issuereceiptproductid").val();
			salesdetailsid = salesdetailsid.split(',');
			var k=0;
			for(var k=0;k< salesdetailsid.length;k++) {
				salesdetailsarray.push(salesdetailsid[k]);
			}
			salesdetailsid = $('#issuereceiptgrid div.gridcontent div.active').attr('id');
			var criteriacnt = $('#issuereceiptgrid .gridcontent div.data-content div').length;
			var griddata = getgridrowsdata('issuereceiptgrid');
			var j = $('#issuereceiptlocalgrid div.gridcontent div.data-content div').length; 
			for(var i=0;i< criteriacnt;i++) {
				if(jQuery.inArray(griddata[i]['accountledgerid'], salesdetailsarray) !='-1') {
					salesaddinlinegriddata('issuereceiptlocalgrid',j+1,griddata[i],'json');
					j++;
				}
			}
			creditentrynavigate();
			if(transactiontype == 23) {
				advanceorbalance = '3';
			} else if(transactiontype == 22) {
				advanceorbalance = '2';
			} else {
				advanceorbalance = '';
			}
		} else {
			alertpopupdouble('Please Select a row from Pending Credit / Debit Entry to Push it to Adjustment Entry');
			return false;
		}	  
	});
	// credit entry delete
	$('#creditentrydelete').click(function() {
		var datarowid = $('#issuereceiptlocalgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var processid = getgridcolvalue('issuereceiptlocalgrid',datarowid,'processid','');
			/*delete grid data*/
			deletegriddatarow('issuereceiptlocalgrid',datarowid);
			if(processid == 1) { // auto
				creditentrynavigate();
			}else{
				creditsummaycalc();
			}
		}else {
			alertpopupdouble(selectrowalert);
		}
	});
	// credit entry edit
	$('#creditentryedit').click(function() { 
		var datarowid = $('#issuereceiptlocalgrid div.gridcontent div.active').attr('id');
		$('#editissuereceiptlocalgridid').val(datarowid);
		if(datarowid) {
			var editoverlaystatus = getgridcolvalue('issuereceiptlocalgrid',datarowid,'editoverlaystatus','');
			if(editoverlaystatus == 1) {
				alertpopupdouble('Cannot edit this during main edit.Delete this entry here and add again from Pending Credit / Debit Entry');
			}else {
				creditoverlayinnereditstatus = 1;
				gridtoformdataset('issuereceiptlocalgrid',datarowid);
				var processid = getgridcolvalue('issuereceiptlocalgrid',datarowid,'processid','');
				if(processid == 1) { // auto
					$('#overlaycreditno').attr('readonly',true);
					$('#creditreference,#creditpaymentirtypeid').prop('disabled',true);
				}else if(processid == 0) { // manual
					$('#overlaycreditno').attr('readonly',false);
					$('#creditreference,#creditpaymentirtypeid').prop('disabled',false);

				}
				var creditoverlaystatus = $('#creditoverlaystatus').val();
				if(creditoverlaystatus == 1) { // Paument issue/receipt - load bill type
					var loadbilltype = $('#loadbilltype').find('option:selected').val();
				}else if(creditoverlaystatus == 0) {
					var loadbilltype = $('#issuereceipttypeid').find('option:selected').val();

				}
				if(loadbilltype == 3) { // receipt
					var finalamt = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalamtreceipt','');
					var finalwt = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalweightreceipt','');
				}else if(loadbilltype == 2) { // issue
					var finalamt = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalamtissue','');
					var finalwt = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalweightissue','');
				}
				var finalamthidden = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalamthidden','');
				var finalwthidden = getgridcolvalue('issuereceiptlocalgrid',datarowid,'finalwthidden','');
				$('#finalamthidden').val(finalamthidden);
				$('#finalwthidden').val(finalwthidden);
				$('#creditentryadd,#creditentryedit,#issuereceiptpush,#creditentrydelete').hide();
				$('#creditentryupdate').show();
				Materialize.updateTextFields();
			}
		}else {
			alertpopupdouble(selectrowalert);
		}
	});
	//Credit-Entry Add/Update button Click
	$('.creditcrud').click(function() {
		var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
		var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
		if(transactiontype == '9' || transactiontype == '11' || transactiontype == '20' || transactiontype == '24' || transactiontype == '22' || transactiontype == '23') {
			var creditreference = $("#creditreference").find('option:selected').val();
			var countlength = $('#issuereceiptlocalgrid div.gridcontent div.data-content div').length;
			if(creditreference == '3') {
				if(countlength > 0) {
					alertpopupdouble('You cannot add more than one Advance Entries to Grid.!');
					return false;
				}
				if(transactiontype == '23') {
					advanceorbalance = '2';
				} else if(transactiontype == '22') {
					advanceorbalance = '3';
				}
			} else if(creditreference == '2') {
				if(transactiontype == '23') {
					advanceorbalance = '3';
				} else if(transactiontype == '22') {
					advanceorbalance = '2';
				}
			}
			if(transactiontype == '9' || transactiontype == '11' || transactiontype == '20') {
				if(creditreference == '3') {
					alertpopupdouble('You cannot add Advance Entries to Grid.!');
					return false;
				}
				advanceorbalance = '1';
			}
			var checkadvanceentry = getgridprocessvalue('issuereceiptlocalgrid');
			if(checkadvanceentry > 0) {
				alertpopupdouble('You cannot add both Advance and Credit Entry.!');
				return false;
			}
			if(transactionmodeid == '2') {
				$('#mainfinalamthidden').attr('data-validation-engine','validate[required,decval['+roundamount+'],custom[number],min[0.1]]');
			} else if(transactionmodeid == '3') {
				$('#mainfinalwthidden').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.1]]');
			}
			creditoperationstone = $(this).data('id');
			$("#issuereceiptvalidation").validationEngine('validate');
		} else {
			creditoperationstone = $(this).data('id');
			$("#issuereceiptvalidation").validationEngine('validate');
		}
	});
	jQuery("#issuereceiptvalidation").validationEngine({ 
		onSuccess: function() {
			var gridname ='issuereceiptlocalgrid';
			$('#creditsalesdate').val($('#salesdate').val());
			/*Form to Grid*/
			if(creditoperationstone == 'ADD' || creditoperationstone == '') {
				creditformtogriddata_jewel(gridname,creditoperationstone,'last','');
			} else if(creditoperationstone == 'UPDATE') {
				var selectedrow = $('#editissuereceiptlocalgridid').val();
				creditformtogriddata_jewel(gridname,creditoperationstone,'',selectedrow);
			}
			$('#processid').select2('val',0);
			$('#accountledgerid').val(1);
			var creditoverlaystatus = $('#creditoverlaystatus').val();
			if(creditoverlaystatus == 1) { // Paument issue/receipt - load bill type
				var loadbilltype = $("#loadbilltype").find('option:selected').val();
			}else if(creditoverlaystatus == 0) {
				var loadbilltype = $("#issuereceipttypeid").find('option:selected').val();
			}
			$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',loadbilltype);
			creditsummaycalc();
			clearceditoverlayform();
			$('#overlaycreditno').attr('readonly',false);
			$('#creditreference,#creditpaymentirtypeid').prop('disabled',false);
			datarowselectevt();
			creditoperationstone = 'ADD';
			$('#creditentryupdate').hide();
			$('#creditentryadd,#creditentryedit,#issuereceiptpush,#creditentrydelete').show();
			$('#creditreference').val(3).trigger('change');
			creditoverlayinnereditstatus = 0;
		},
		onFailure: function() {
			var dropdownid =[''];
			dropdownfailureerror(dropdownid);
		}
	});
	$('#mainfinalamthidden').change(function() {
		var processid = $('#processid').find('option:selected').val();
		var val = $(this).val();
		var finalamthidden = $('#finalamthidden').val();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		var irtypemain = $('#loadbilltype').find('option:selected').val();
		if(processid == 1) { // auto
			if((creditoverlaytriggerplace == 2 && irtypemain == 2 && salestransactiontypeid == '23') || (creditoverlaytriggerplace == 2 && irtypemain == 3 && salestransactiontypeid == '22') || (creditoverlaytriggerplace == 1 ))
			{
				if(parseFloat(val) > parseFloat(Math.abs(finalamthidden))) {
					alertpopupdouble('Please Enter greater than or equal to '+Math.abs(finalamthidden)+' Rs');
					$(this).val(finalamthidden);
				}
			}
		}
		Materialize.updateTextFields();
	});
	$('#mainfinalwthidden').change(function() {
		var val = $(this).val();
		var processid = $('#processid').find('option:selected').val();
		var finalwthidden = $('#finalwthidden').val();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		var irtypemain = $('#loadbilltype').find('option:selected').val();
		if(processid == 1) { // auto
			if((creditoverlaytriggerplace == 2 && irtypemain == 2 && salestransactiontypeid == '23') || (creditoverlaytriggerplace == 2 && irtypemain == 3 && salestransactiontypeid == '22') || (creditoverlaytriggerplace == 1 ))
			{
				if(parseFloat(val) > parseFloat(Math.abs(finalwthidden))) {
					alertpopupdouble('Please Enter greater than or equal to '+Math.abs(finalwthidden)+' Gms');
					$(this).val(finalwthidden);
				}
			}
		}
		Materialize.updateTextFields();
	});
 	$("#billnumbergridsubmit").click(function() {
 		var billnumberid = $("#billnumberid").val();
 		var salesreturntype = $("#salesreturntype").val();
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
 		$("#salesbasedproductid").val(salesdetailsid);
		var salesdetailsid = '';
		var salesdetailsarray = [];
 		if(salesreturntype == 3) {// for mulitple sales return type
 			salesdetailsid =  $("#billbasedproductid").val();
			salesdetailsid = salesdetailsid.split(',');
			var k=0;
			for(var k=0;k< salesdetailsid.length;k++) {
				salesdetailsarray.push(salesdetailsid[k]);
			}
			salesdetailsid = $('#billnumbergrid div.gridcontent div.active').attr('id');
			var criteriacnt = $('#billnumbergrid .gridcontent div.data-content div').length;
			var griddata = getgridrowsdata('billnumbergrid');
			var j = $('#saledetailsgrid div.gridcontent div.data-content div').length; 
			for(var i=0;i< criteriacnt;i++) {
				if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) !='-1') {
					if(metalreturnarraydetails != '') {
						var salesreturnitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','return','count');
						if(salesreturnitemscount == 0) {
							billlevelrestrictreturnmetalid = metalarraydata[purityarraydata.indexOf(griddata[i]['purity'])];
							salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
							metalstatus = 1;
						} else if(salesreturnitemscount > 0) {
							if(billlevelrestrictreturnmetalid != metalarraydata[purityarraydata.indexOf(griddata[i]['purity'])]) {
								$('#stocktypeid').select2('val',20).trigger('change');
								alertpopupdouble('Kindly do not use different Metal in Return item!.');
								$("#billnumberoverlay").hide();
								return false;
							} else {
								salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
								metalstatus = 1;
							}
						}
					} else {
						salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
						metalstatus = 0;
					}
					j++;
				}
			}
			datarowselectevt();
			//getsalesdetailcalc(); // get local grid data based calcualtion
			finalsummarycalc(0);
			$("#billnumberid").select2('val','').trigger('change');
 		} else {// for single sales return type
 			/* salesdetailsid =  $('#billnumbergrid div.gridcontent div.active').attr('id');
 			retrivesalesreturn(salesdetailsid); */
 		}
 		cleargriddata('billnumbergrid');
 		$("#billnumberoverlay").hide();		
 	});
	// old jewel voucher grid submit
	$("#oldjewelvouchergridsubmit").click(function() {
	  var salesdetailsid =  $("#oldjewelvoucherproductid").val();
	  var oldjewelvoucherbillno =  $("#oldjewelvoucherbillno").val();
	  var salesdetailsarray = [];
	   if(salesdetailsid != ''){
		   salesdetailsid = salesdetailsid.split(',');
			var k=0;
			for(var k=0;k< salesdetailsid.length;k++){
				salesdetailsarray.push(salesdetailsid[k]);
			}
			salesdetailsid = $('#oldjewelvouchergrid div.gridcontent div.active').attr('id');
			var criteriacnt = $('#oldjewelvouchergrid .gridcontent div.data-content div').length;
			var griddata = getgridrowsdata('oldjewelvouchergrid');
			var j = $('#saledetailsgrid div.gridcontent div.data-content div').length; 
			for(var i=0;i< criteriacnt;i++){
				if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) !='-1') {
					salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
					j++;
				}
				
			}
			datarowselectevt();
			//getsalesdetailcalc(); // get local grid data based calcualtion
			finalsummarycalc(0);
	 
	   }else{
		    alertpopupdouble('Please choose the product details');
	   }
 		cleargriddata('oldjewelvouchergrid');
 		$("#oldjewelvoucheroverlay").hide(); 		
 	});
	// approval out return grid submit
	$("#approvalreturngridsubmit").click(function() {
 	  var salesdetailsid =  $("#approvaloutreturnproductid").val();
	  var approvaloutbillno =  $("#approvalnumber").val();
	  var approvalouttagno = $('#approvaloutreturntagno').val();
	  var stypeid = $('#stocktypeid').find('option:selected').val();
	  var salesdetailsarray = [];
	  var checkedlength = $('#approvalreturngrid .gridcontent div.data-content div input:checked').length;
	   if(checkedlength != 0){
		   salesdetailsid = salesdetailsid.split(',');
			var k=0;
			for(var k=0;k< salesdetailsid.length;k++){
				salesdetailsarray.push(salesdetailsid[k]);
			}
			salesdetailsid = $('#approvalreturngrid div.gridcontent div.active').attr('id');
			var criteriacnt = $('#approvalreturngrid .gridcontent div.data-content div').length;
			var griddata = getgridrowsdata('approvalreturngrid');
			var j = $('#saledetailsgrid div.gridcontent div.data-content div').length; 
			for(var i=0;i< criteriacnt;i++){
				if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) !='-1') {
					salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
					j++;
				}
				
			}
			datarowselectevt();
			$("#approvalnumber").select2('val','').trigger('change');
			accounticonhideshow();
			cleargriddata('approvalreturngrid');
 		$("#approvalreturnoverlay").hide(); 
	   
	   }else{
			$("#approvalnumber").select2('val','');
		    alertpopupdouble('Please choose the product details');
			return false;
	   }
 	});
	// order grid submit
	$("#ordergridsubmit").click(function() {
		var ratearray = $('#ratearray').val();
		var salesdetailsid =  $("#orderproductid").val();
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
		var salesdetailsarray = [];
		var checkedlength = $('#ordergrid .gridcontent div.data-content div input:checked').length;
		if(checkedlength != 0) {
		   if((stocktypeid == 82 || stocktypeid == 85 || stocktypeid == 83 || stocktypeid == 87 || stocktypeid == 88 || stocktypeid == 89) && checkedlength > 1) {
				alertpopupdouble('Please choose the single product details');
				return false;
			} else {
				salesdetailsid = salesdetailsid.split(',');
				var k = 0;
				for(var k=0;k< salesdetailsid.length;k++) {
					salesdetailsarray.push(salesdetailsid[k]);
				}
				salesdetailsid = $('#ordergrid div.gridcontent div.active').attr('id');
				var criteriacnt = $('#ordergrid .gridcontent div.data-content div').length;
				var griddata = getgridrowsdata('ordergrid');
				var j = $('#saledetailsgrid div.gridcontent div.data-content div').length;
				if(stocktypeid == 83) {
					for(var i=0;i< criteriacnt;i++) {
						var rowsinc = j+1;
						if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) !='-1') {
							if(metalarraydetails != '' && stocktypeid == 83) {
								var salesitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','all','count');
								if(salesitemscount == 0) {
									billlevelrestrictmetalid = metalarraydata[purityarraydata.indexOf(griddata[i]['purity'])];
									salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
									$('div.gridcontent div.data-rows').removeClass('active');
									$('div.gridcontent div #'+rowsinc+'').addClass('active');
									setTimeout(function(){
										$("#salesdetailentryedit").trigger('click');
									},10);
									setTimeout(function(){
										$('#ratepergram').trigger('change');
										$('#salesdetailupdate').trigger('click');
									},100);
									metalstatus = 1;
								} else if(salesitemscount > 0) {
									if(billlevelrestrictmetalid != metalarraydata[purityarraydata.indexOf(griddata[i]['purity'])]) {
										$('#stocktypeid').select2('val',83).trigger('change');
										alertpopupdouble('Kindly do not use different Metal in sales item!.');
										$("#ordergridoverlay").hide();
										return false;
									} else {
										salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
										$('div.gridcontent div.data-rows').removeClass('active');
										$('div.gridcontent div #'+rowsinc+'').addClass('active');
										setTimeout(function(){
											$("#salesdetailentryedit").trigger('click');
										},10);
										setTimeout(function(){
											$('#ratepergram').trigger('change');
											$('#salesdetailupdate').trigger('click');
										},100);
										metalstatus = 1;
									}
								}
							} else {
								salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
								$('div.gridcontent div.data-rows').removeClass('active');
								$('div.gridcontent div #'+rowsinc+'').addClass('active');
								setTimeout(function(){
									$("#salesdetailentryedit").trigger('click');
								},10);
								setTimeout(function(){
									$('#ratepergram').trigger('change');
									$('#salesdetailupdate').trigger('click');
								},100);
							}
							j++;
						}
					}
				} else if(stocktypeid == 89) {
					for(var i=0;i< criteriacnt;i++) {
						var rowsinc = j+1;
						if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) !='-1') {
							if(metalarraydetails != '' && stocktypeid == 89) {
								var salesitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','checksales','count');
								if(salesitemscount == 0) {
									billlevelrestrictmetalid = metalarraydata[purityarraydata.indexOf(griddata[i]['purity'])];
									salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
									$('div.gridcontent div.data-rows').removeClass('active');
									$('div.gridcontent div #'+rowsinc+'').addClass('active');
									setTimeout(function(){
										$("#salesdetailentryedit").trigger('click');
									},10);
									metalstatus = 1;
								} else if(salesitemscount > 0) {
									$('#stocktypeid').select2('val',11).trigger('change');
									alertpopup('Kindly do not use Repair Items in Sales Bill!.');
									$("#ordergridoverlay").hide();
									return false;
								}
							} else {
								$('div.gridcontent div.data-rows').removeClass('active');
								$('div.gridcontent div #'+rowsinc+'').addClass('active');
								setTimeout(function(){
									$("#salesdetailentryedit").trigger('click');
								},10);
							}
						}
					}
				} else {
					for(var i=0;i< criteriacnt;i++) {
						if(jQuery.inArray(griddata[i]['salesdetailid'], salesdetailsarray) != '-1') {
							salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
							j++;
						}
					}
				}
				datarowselectevt();
				var salesdetailcount = $('#ordernumber').find('option:selected').data('salesdetailcount');
				var ordernumber = $('#ordernumber').val();
				$('#salesdetailcount').val(salesdetailcount);
				$("#ordernumber").select2('val','').trigger('change');
				$("#ordernumber").select2('val','');
				accounticonhideshow();
				if(stocktypeid == 82) { /** Receive P.Order **/
					orderstocktypehideshow(82);
					$("#salesdetailentryedit").trigger('click');
					setTimeout(function(){
						loadproductaddon();
						$('#grossweight').focus();
						$('#grossweight').trigger('change');
					},100);
				}
				if(stocktypeid == 88 || stocktypeid == 89) { /** Receive & Delivery P.Repair **/
					if(j != 0) {
						$('div.gridcontent div.data-rows').removeClass('active');
						$('div.gridcontent div #'+j+'').addClass('active');
						$("#salesdetailentryedit").trigger('click');
						setTimeout(function() {
							$('#repairchargeclone').trigger('change');
						},100);
						j++;
					}
				}
				if(stocktypeid == 85) { /** Generate P.Order **/
					var selaccountypeid = $('#selaccounttypeid').val();
					if(j != 0) {
						$('div.gridcontent div.data-rows').removeClass('active');
						$('div.gridcontent div #'+j+'').addClass('active');
						$("#salesdetailentryedit").trigger('click');
						$('#wastagespan,#makingchargespan,#lstchargespan,#flatchargespan,#repairchargespan').text(0);
						$('#CC-FT,#HM-FT').text('(0)');
						$('#makingcharge,#makingchargeclone,#lstcharge,#lstchargeclone,#wastage,#wastageclone,#flatcharge,#repaircharge,#additionalchargeamt,#flatchargeclone,#hflat,#hflatfinal,#cflat,#certfinal,#totalchargeamount,#grossamount,#repaircharge,#repairchargeclone').val(0); //sets zero on EMPTY
						$('#purity-div').show();
						setTimeout(function(){
							loadproductaddon();
							$('#grossweight').focus();
							$('#grossweight').trigger('change');
						},100);
						if(selaccountypeid == 16) {
							if($('#product').val()) {
								var chargeid=$('#product').find('option:selected').data('purchasechargeid');
								wastagechargehideshow(chargeid);
								paymenttouchhideshow(chargeid);
								makingchargehideshow(chargeid);
								lstchargehideshow(chargeid);
								flatchargehideshow(chargeid);
								extrachargehideshow(chargeid);
								repairchargehideshow(stocktypeid);
							}
						}
						j++;
					}
				}
				if(stocktypeid == 83) { /** Order tag - delivery **/
					deliveryorderstocktypehideshow(83);
					finalsummarycalc(1);
					$('#mainordernumberid').val(ordernumber);
					//getorderadvanceamt();
					$('#liveadvanceclose').val(0);
				}
				if(stocktypeid == 76 || stocktypeid == 87) { /** Place Order **/
					finalsummarycalc(0);
				}
				$('#salestransactiontypeid,#transactionmodeid').prop('disabled',true);
				cleargriddata('ordergrid');
				$("#ordergridoverlay").hide();
			}
	   	} else {
			//$("#ordernumber").select2('val','');
		    alertpopupdouble('Please choose the product details');
			return false;
		}
 	});
	// bulk sales grid submit
	$("#bulksalesgridsubmit").click(function() {
 	  var salesdetailsid =  $("#bulksalesrgridcloseproductid").val();
	  var salesdetailsarray = [];
	  var checkedlength = $('#bulksalesgrid .gridcontent div.data-content div input:checked').length;
	  if(checkedlength != 0){
				salesdetailsid = salesdetailsid.split(',');
				var k=0;
				for(var k=0;k< salesdetailsid.length;k++){
					salesdetailsarray.push(salesdetailsid[k]);
				}
				salesdetailsid = $('#bulksalesgrid div.gridcontent div.active').attr('id');
				var criteriacnt = $('#bulksalesgrid .gridcontent div.data-content div').length;
				var griddata = getgridrowsdata('bulksalesgrid');
				var j = $('#saledetailsgrid div.gridcontent div.data-content div').length; 
				for(var i=0;i< criteriacnt;i++){
					if(jQuery.inArray(griddata[i]['itemtagid'], salesdetailsarray) !='-1') {
						salesaddinlinegriddata('saledetailsgrid',j+1,griddata[i],'json');
						j++;
					}
				}
				datarowselectevt();
				finalsummarycalc(1);
				$('#salestransactiontypeid,#transactionmodeid').prop('disabled',true);
				cleargriddata('bulksalesgrid');
				$("#bulksalesgridoverlay").fadeOut();
	  }else{
			alertpopupdouble('Please choose the product details');
			return false;
	   }
 	});
	$('#wastagespan,#wastage').on('click keypress',function(e){
		//if(userroleid == '2') { // admine role
		if(jQuery.inArray( userroleid, salesauthuserrole ) != -1) { //Userrole who has having permission to edit
			if(e.keyCode == 13 || e.keyCode === undefined){
				var transactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				/* if(transactiontypeid == 9){
				   alertpopup('Wastage charge not applicable for Purchase');
				}else{ */
				$('.wastagecloneconversion').addClass('hidedisplay');
				if(wastagecalctype == 1 && transactiontypeid == 11) {
					$('.wastagecloneconversion').removeClass('hidedisplay');
				}
				$('#wastageiconoverlay').fadeIn();
				$('#wastageclone').focus();
			   //}	
			}
		}
	});
	$('#wastagechargeiconclose').click(function(){
		$('#wastageiconoverlay').fadeOut();
		$('#wastage').focus();
	});
	// Wastage value change in overlay
	$('#wastageclone').change(function() {
	    var chargeiddata = ['5','6','7'];
		var chargeid=defaultchargeid.toString();
		var chargedetails=chargeid.split(",");
		var keyword = '';
		var val = '';
		keyword = $(this).attr('keyword');
		if(keyword == '') {
			if(chargedetails == '') {
				
			} else {
				$.each(chargedetails, function (index, value) {
					if(jQuery.inArray( value, chargeiddata ) != -1) {  
						if(value == 5) {
							keyword = 'WS-PT-N';
						} else if(value == 6) {
							keyword = 'WS-WT';	
						} else if(value == 7) {
							keyword = 'WS-PT-G';	
						}
					}
				});
			}
		} else {
		}
		val = $.trim($(this).val());
		if(keyword == 'WS-PT-N' || keyword == 'WS-PT-G') {
			if(val > 100){
				$('#wastageclone').val(0).trigger('change');
				alertpopupdouble('Please enter percentage value below 100');
				return false;
			}
		}
		if(wastagecalctype == '2') { // g.wt +ws.wt -s.wt = n. for weight
			if(val !='') { 
				//setTimeout(function() {
				wastagecalctypewithnetweight(val,keyword);
				//},5);	
			} else {
				$('#wastageclone').val(0);
				wastagecalctypewithnetweight(0,keyword);
			}
		} else if(wastagecalctype == '4') {
			if(val !='') {
				//setTimeout(function() {
				wastagecalctypewithmelting(val,keyword);
				//},5);	
			} else {
				$('#wastageclone').val(0);
				wastagecalctypewithmelting(0,keyword);
			}
		} else if(wastagecalctype == '3' || wastagecalctype == '5') {
			if(val !='') {
				//setTimeout(function() {
				wastagecalctypewithtouch(val,keyword);
				//},5);	
			} else {
				$('#wastageclone').val(0);
				wastagecalctypewithtouch(0,keyword);
			}
		}/* else if(wastagecalctype == '5') {
			$('#wastage,#prewastage,#wastageclone').val(0);
		} */else { //for amount
			if(val !='') {	
				wastagechargecalc(val,keyword);
			}else{
				$('#wastageclone').val(0);
				wastagechargecalc(0,keyword);
			}
		}
		if(val != '') {
			$('#wastagespan').text(parseFloat(val));
			$('#wastageclone').val(parseFloat(val));
		} else {
			$('#wastagespan').text(0);
			$('#wastageclone').val(0);
		}
	});
	// Direct Amount - Trigger change wastage clone value
	$('#wastagecloneconversion').change(function() {
		var value = parseFloat($.trim($(this).val())).toFixed(roundamount);
		$('#wastagecloneconversion').val(value);
		var keyword = $('#wastagespan').attr('keyword');
		if(keyword != '')	{
			wastagechargereversecalc(value,keyword);
			totalgrossamountcalculationwork();
		}
	});
	$('#wastage,#makingcharge,#flatcharge').change(function() {
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
		
	});
	$('#makingchargespan,#makingcharge').on('click keypress',function(e){
		//if(userroleid == '2') { // admin role
		if(jQuery.inArray( userroleid, salesauthuserrole ) != -1) { //Userrole who has having permission to edit
			if(e.keyCode == 13 || e.keyCode === undefined) {
				var transactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				$('.makingcloneconversion').addClass('hidedisplay');
				if(wastagecalctype == 1 && transactiontypeid == 11) {
					$('.makingcloneconversion').removeClass('hidedisplay');
				}
				$('#makingchargeiconoverlay').fadeIn();
				$('#makingchargeclone').focus();	
			}
		}
	});
	$('#makingchargeiconclose').click(function(){
		$('#makingchargeiconoverlay').fadeOut();
		$('#makingcharge').focus();
	});
	$('#lstchargespan,#lstcharge').on('click keypress',function(e){
		//if(userroleid == '2') { // admin role
		if(jQuery.inArray( userroleid, salesauthuserrole ) != -1) { //Userrole who has having permission to edit
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('#lstchargeiconoverlay').fadeIn();
				$('#lstchargeclone').focus();	
			}
		}
	});
	$('#lstchargeiconclose').click(function(){
		$('#lstchargeiconoverlay').fadeOut();
		$('#lstcharge').focus();
	});
	$('#flatchargespan,#flatcharge').on('click keypress',function(e){
		//if(userroleid == '2') { // admin role
		if(jQuery.inArray( userroleid, salesauthuserrole ) != -1) { //Userrole who has having permission to edit
			if(e.keyCode == 13 || e.keyCode === undefined) {
				var transactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				$('.flatchargecloneconversion').addClass('hidedisplay');
				if(wastagecalctype == 1 && transactiontypeid == 11) {
					$('.flatchargecloneconversion').removeClass('hidedisplay');
				}
				$('#flatchargeiconoverlay').fadeIn();
				$('#flatchargeclone').focus();	
			}
		}
	});
	$('#flatchargeiconclose').click(function(){
		$('#flatchargeiconoverlay').fadeOut();
		$('#flatcharge').focus();
	});
	// Repair Charge Keypress actions
	$('#repairchargespan,#repaircharge').on('click keypress',function(e) {
		if(jQuery.inArray( userroleid, salesauthuserrole ) != -1) { //Userrole who has having permission to edit
			if(e.keyCode == 13 || e.keyCode === undefined) {
				$('#repairchargeiconoverlay').fadeIn();
				$('#repairchargeclone').focus();	
			}
		}
	});
	$('#repairchargeiconclose').click(function() {
		$('#repairchargeiconoverlay').fadeOut();
		$('#repaircharge').focus();
	});
	$('#repairchargeclone').change(function() {
		var chargeiddata = ['20'];
		var keyword = 'RC';
		var val = '';
		val = $.trim($(this).val());
		if(val != '') {
			repairchargecalc(val,keyword);
			$('#repairchargespan').text(parseFloat(val));
			$('#repairchargeclone').val(parseFloat(val));
		} else {
			$('#repairchargespan').text(0);
			$('#repairchargeclone').val(0);
			repairchargecalc(0,keyword);
		}
	});
	/* $('#flatchargeclone').keypress(function(){
		if(e.keyCode == 13 ){
			$('#flatchargeiconoverlay').fadeOut();
			$('#flatcharge').focus();
		}
	});
	$('#makingchargeclone').keypress(function(){
		if(e.keyCode == 13 ){
			$('#makingchargeiconoverlay').fadeOut();
			$('#makingcharge').focus();
		}
	});
	$('#wastageclone').keypress(function(){
		if(e.keyCode == 13 ){
			$('#wastageiconoverlay').fadeOut();
			$('#wastage').focus();
		}
	}); */
	$('#makingchargeclone').change(function() {
		var chargeiddata = ['2','3','4','14'];
		var chargeid = defaultchargeid.toString();
		var chargedetails=chargeid.split(",");
		var keyword = '';
		var val = '';
		keyword = $(this).attr('keyword');
		if(keyword == '') {
			if(chargedetails == '') {
			}else{
				$.each(chargedetails, function (index, value) {
					if(jQuery.inArray( value, chargeiddata ) != -1) {  
						if(value == 2) {
							keyword = 'MC-GM-N';
						} else if(value == 3) {
							keyword = 'MC-FT';	
						} else if(value == 4) {
							keyword = 'MC-PI';	
						} else if(value == 14) {
							keyword = 'MC-GM-G';	
						}
					}
				});
			}
		}else{
		}
		val = $.trim($(this).val());
		if(val != '') {
			makingchargecalc(val,keyword);
			$('#makingchargespan').text(parseFloat(val));
			$('#makingchargeclone').val(parseFloat(val));
		} else {
			$('#makingchargespan').text(0);
			$('#makingchargeclone').val(0);
			makingchargecalc(0,keyword);
		}
	 });
	 // Direct Amount - Trigger change making clone value
	$('#makingcloneconversion').change(function() {
		var value = parseFloat($.trim($(this).val())).toFixed(roundamount);
		$('#makingcloneconversion').val(value);
		var keyword = $('#makingchargespan').attr('keyword');
		if(keyword != '')	{
			makingchargereversecalc(value,keyword);
		}
	});
	 $('#lstchargeclone').change(function() {
		var chargeiddata = ['16','17','18'];
		var chargeid=defaultchargeid.toString();
		var chargedetails=chargeid.split(",");
		var keyword = '';
		var val = '';
		keyword = $(this).attr('keyword');
		if(keyword == '') {
		 if(chargedetails == ''){
			}else{
				$.each(chargedetails, function (index, value) {
					if(jQuery.inArray( value, chargeiddata ) != -1) {  
							if(value == 16){
							  keyword = 'LST-G';
							}else if(value == 17){
							  keyword = 'LST-P';	
							}else if(value == 18){
							  keyword = 'LST-C';	
							}
						}
				});
			}
		}else{
		}
		val = $.trim($(this).val());
		if(val != ''){
			lstchargecalc(val,keyword);
			$('#lstchargespan').text(parseFloat(val));
			$('#lstchargeclone').val(parseFloat(val));
		}else{
			$('#lstchargespan').text(0);
			$('#lstchargeclone').val(0);
			lstchargecalc(0,keyword);
		}
		
	 });
	$('#flatchargeclone').change(function() {
		var chargeiddata = ['19','12'];
		var chargeid=defaultchargeid.toString();
		var chargedetails=chargeid.split(",");
		var keyword = '';
		var val = '';
		keyword = $(this).attr('keyword');
		if(keyword == '') {
			if(chargedetails == '') {
				} else {
					$.each(chargedetails, function (index, value) {
						if(jQuery.inArray( value, chargeiddata ) != -1) {
							if(value == 12) {
								keyword = 'FC';
							} else if(value == 19) {
								keyword = 'FC-WT';
							}
						}
					});
				}
			} else {
		}
		val = $.trim($(this).val());
		if(val != '') {
			flatchargecalc(val,keyword);
			$('#flatchargespan').text(parseFloat(val));
			$('#flatchargeclone').val(parseFloat(val));
		} else {
			$('#flatchargespan').text(0);
			$('#flatchargeclone').val(0);
			flatchargecalc(0,keyword);
		}
	});
	 // Direct Amount - Trigger change making clone value
	$('#flatchargecloneconversion').change(function() {
		var value = parseFloat($.trim($(this).val())).toFixed(roundamount);
		$('#flatchargecloneconversion').val(value);
		var keyword = $('#flatchargeclone').attr('keyword');
		if(keyword != '')	{
			flatchargereversecalc(value,keyword);
		}
	});
	$('#commenticon').on('click keypress', function(e) {
		if(e.keyCode == 13 || e.keyCode === undefined){
		$('#salescomementboxoverlay').fadeIn();		
		}
	});
	$('#paymentcommenticon').on('click keypress', function(e) {
		if(e.keyCode == 13 || e.keyCode === undefined){
		$('#paymentcomementboxoverlay').fadeIn();		
		}
	});
	$('#salescommenticonclose').click(function(){
		$('#salescomementboxoverlay').fadeOut();
	});
	$('#paymentcommenticonclose').click(function(){
		$('#paymentcomementboxoverlay').fadeOut();
	});
	$('#billcommenticon').on('click keypress', function(e) {
		if(e.keyCode == 13 || e.keyCode === undefined){
		$('#billcomementboxoverlay').fadeIn();		
		}
	});
	$('#billcommenticonclose').click(function(){
		$('#billcomementboxoverlay').fadeOut();
	});
	/* {//summary details show hide based on payment
		$("#sumpaidamt").click(function() {
			$("#salessummary").attr('class','hidedisplay');
			$("#paymentsummarydetails").attr('class','');
		});
		$("#closepaymentsummary").click(function() {
			$("#salessummary").attr('class','');
			$("#paymentsummarydetails").attr('class','hidedisplay');
		});
	}
	{//summary details show hide based on payment
		$("#sumoldjewels").click(function() {
			$("#salessummary").attr('class','hidedisplay');
			$("#oldjewelsummarydetails").attr('class','');
		});
		$("#closeoldjewelsummary").click(function() {
			$("#salessummary").attr('class','');
			$("#oldjewelsummarydetails").attr('class','hidedisplay');
		});
	}
	{//summary details show hide based on payment
		$("#sumsalesreturn").click(function() {
			$("#salessummary").attr('class','hidedisplay');
			$("#salesreturnsummarydetails").attr('class','');
		});
		$("#closesalesreturnsummary").click(function() {
			$("#salessummary").attr('class','');
			$("#salesreturnsummarydetails").attr('class','hidedisplay');
		});
	}
	{//summary details show hide based on payment - purchase return
		$("#sumpurchasereturn").click(function() {
			$("#salessummary").attr('class','hidedisplay');
			$("#purchasereturnsummarydetails").attr('class','');
		});
		$("#closepurchasereturnsummary").click(function() {
			$("#salessummary").attr('class','');
			$("#purchasereturnsummarydetails").attr('class','hidedisplay');
		});
	} */
	{//account selection overlay
		$("#accountsearchevent").on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				$("#processoverlay").show();
				clearform('acccondclear');
				$("#accfilterconddisplay").empty();
				$("#accfilterid").val('');
				$("#accconditionname").val('');
				$("#accfiltervalue").val('');
				accountloadcolumnname('91');
				/* hide process */
				$("#accountselectoverlay").show();
				$("#accddcondvaluedivhid").hide();
				$("#acccondvaluedivhid").show();
				$("#accdatecondvaluedivhid").hide();
				$("#s2id_accountcondcolumn").select2('focus');
				accountsearchgrid();
				$("#processoverlay").hide();
			}
		});
		$("#accoutsearchclose").click(function(){
			clearform('acccondclear');
			$("#accountselectoverlay").show();
			$("#accddcondvaluedivhid").hide();
			$("#acccondvaluedivhid").show();
			$("#accdatecondvaluedivhid").hide();
			$("#accountselectoverlay").hide();
		});
		$("#accountsearchsubmit").click(function() {
			var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
			var stocktypeid = $('#stocktypeid').find('option:selected').val();
			var datarowid = $('#accountsearchgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var accountname = getgridcolvalue('accountsearchgrid',datarowid,'newaccountname','');
				var mobilenumber = getgridcolvalue('accountsearchgrid',datarowid,'mobilenumber','');
				var primarystateid = accountsearchsubmitretrievestateid(getgridcolvalue('accountsearchgrid',datarowid,'statename',''));
				$("#accountstateid").val(primarystateid);
				$("#accountid").val(datarowid);
				if(mobilenumber != '') {
					newaccountname = accountname+' - '+mobilenumber;
				} else {
					newaccountname = accountname;
				}
				$('#s2id_accountid a span:first').text(newaccountname);
				resetirfields();
				loadaccountopenclose($("#accountid").val());
				if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
					loadbilltrigger();
				} else if(salestransactiontypeid == 21 || salestransactiontypeid == 29) { // place order && place repair
					if(salestransactiontypeid == 21) {
						$('#stocktypeid').select2('val',76).trigger('change');
					} else {
						$('#stocktypeid').select2('val',87).trigger('change');
					}
					$('#ordermodeid').select2('val',3).trigger('change');
					$('#ordermodeid-div').addClass('hidedisplay');
				} else  if(salestransactiontypeid == 20) { // take order
					var accounttypeid = $("#selaccounttypeid").val();
					if(accounttypeid == 16) {
						$('#stocktypeid').select2('val',82).trigger('change');
						orderstocktypehideshow(82);
						loadtakeordernumber(3);
					} else if(accounttypeid == 6) {
						$('#stocktypeid').select2('val',75).trigger('change');
						orderstocktypehideshow(75);
					}
				} else if(salestransactiontypeid == 11) {
					if(stocktypeid == 83){
						loadtakeordernumber(5);
					}
				}
				$("#accountselectoverlay").hide();
			} else {
				alertpopupdouble("please select the account");
			}
		});
	}
	{//account condition drop down change
		$("#accountcondcolumn").change(function() {
			//Based on reportcondition dropdwon seletection, the condition will load from uitype table
			$("#acccondition").empty();
			$("#acccondition").select2('val','')
			$('#acccondition').append($("<option></option>"));
			var fieldlable = $("#accountcondcolumn").val();
			$('.viewcleardataform').validationEngine('hideAll');
			var data = $("#accountcondcolumn").find('option:selected').data('uitypeid');
			var moduleid = '91';
			accconditiondropdownload(data);
			if(data == '2' || data == '3' || data == '4'|| data == '5' || data == '6'|| data == '7'|| data == '10' || data == '11' || data == '12'|| data == '14'|| data == '30') {
				showhideintextbox('accddcondvalue','acccondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#acccondvalue").val('');
				//$('#acccondvalue').timepicker('remove');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#acccondvalue").addClass('validate[required,maxSize[100]]'); 
				$("#accddcondvalue").removeClass('validate[required]');
				$("#accdatecondvalue").removeClass('validate[required]');
			} else if(data == '17' || data == '28') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").select2('val',"");
				$("#accddcondvalue").addClass('validate[required]');
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdpicklistddvalue(fieldname,data,'accddcondvalue',moduleid);
			} else if(data == '18' || data == '19' || data == '21' || data == '22' || data == '25' || data == '26' || data == '29'  || data == '31') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").addClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvalue(fieldname,'accddcondvalue',moduleid); 
			} else if(data == '20') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").addClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				empgroupdrodownset('accddcondvalue');
			}  else if(data == '32') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").addClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvalue('employeename','accddcondvalue'); 
			} else if(data == '13') {
				$("#acccondvalue").datepicker("disable");
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accddcondvalue").addClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				checkboxvalueget('accddcondvalue');
			} else if(data == '8') {
				//for hide
				$('#accdatecondvalue').datetimepicker("destroy");
				$("#accddcondvaluedivhid").hide();
				$("#acccondvaluedivhid").hide();
				$("#accdatecondvaluedivhid").show();
				$('#accdatecondvalue').val('');
				$("#accddcondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$('#accdatecondvalue').addClass('validate[required]');				
				if(fieldlable == 'Date of Birth' || fieldlable == 'Start Date' || fieldlable == 'End Date') {
					var dateformetdata = $('#accdatecondvalue').attr('data-dateformater');
					$('#accdatecondvalue').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: null,
						changeMonth: true,
						changeYear: true,
						maxDate:0,
						yearRange : '1947:c+100',
						onSelect: function () {
							var checkdate = $(this).val();
							if(checkdate != '') {
								$("#finalacccondvalue").val(checkdate);
								$("#finalacccondvaluename").val(checkdate);
							}
						},
						onClose: function () {
							$(this).focus();$(this).focusout();$(this).focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				} else {
					var dateformetdata = $('#accdatecondvalue').attr('data-dateformater');
					$('#accdatecondvalue').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: 0,
						onSelect: function () {
							var checkdate = $(this).val();
							$('#accdatecondvalue').focusout();
							$("#finalacccondvalue").val(checkdate);
							$("#finalacccondvaluename").val(checkdate);
						},
						onClose: function () {
							$('#accdatecondvalue').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				}
			} else if(data == '9') {
				$('#accdatecondvalue').datetimepicker("destroy");
				//for hide
				$("#accddcondvaluedivhid").hide();
				$("#acccondvaluedivhid").show();
				$("#accdatecondvaluedivhid").hide();
				$('#acccondvalue').val('');
				$("#accddcondvalue").removeClass('validate[required]');
				$("#acccondvalue").addClass('validate[required,maxSize[100]]');
				$('#accdatecondvalue').removeClass('validate[required]');	
				$('#acccondvalue').timepicker({
					'step':15,
					'timeFormat': 'H:i',
					'scrollDefaultNow': true,
				});
				var checkdate = $("#acccondvalue").val();
				if(checkdate != ''){
					$("#finalacccondvalue").val(checkdate);
					$("#finalacccondvaluename").val(checkdate);
				}
			} else if(data == '27') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").addClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvaluewithcond(fieldname,'accddcondvalue'); 
			} else if(data == '23') {
				showhideintextbox('acccondvalue','accddcondvalue');
				$("#accdatecondvaluedivhid").hide();
				$("#accddcondvalue").addClass('validate[required]');
				$("#accddcondvalue").select2('val',"");
				$("#accdatecondvalue").removeClass('validate[required]');
				$("#acccondvalue").removeClass('validate[required,maxSize[100]]');
				$("#acccondvalue").datepicker("disable");
				var fieldname = $("#accountcondcolumn").find('option:selected').data('indexname');
				attributepurityvalueget(fieldname,'accddcondvalue'); 
			} else {
				$("#accountcondcolumn").select2('val','');
				alertpopup('Please choose another field name');
			}
		});
	}
	{
		//field value set
		$("#acccondvalue").focusout(function(){
			var value = $("#acccondvalue").val();
			$("#finalacccondvalue").val(value);
			$("#finalacccondvaluename").val(value);
		});
		$("#accddcondvalue").change(function(){
			var value = $("#accddcondvalue").val();
			var dataid = $("#accddcondvalue").find('option:selected').data('ddid');
			$("#finalacccondvalue").val(dataid);
			$("#finalacccondvaluename").val(value);
		});
	}
	accfiltername = [];
	$("#accaddcondsubbtn").click(function() {
		$("#accountformconditionvalidation").validationEngine("validate");	
	});
	$("#accountformconditionvalidation").validationEngine({
		onSuccess: function() {
			accountfilterwork(accountsearchgrid);
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#flatchargespan").bind('dblclick',
		function(){
		   $(this).attr('contentEditable',true);
		}
	);
	$('#flatchargespan').keyup(function(){
		setzero(['flatcharge']);
		var finalvalue = '';
		finalvalue = $(this).text();
		if(finalvalue != ''){
			finalvalue = $(this).text();
		}else{
			finalvalue = 0;
		}
		var calfinalvalue=0;
	  	calfinalvalue = parseFloat(finalvalue).toFixed(roundamount);
		$('#flatcharge').val(calfinalvalue).trigger('change');
	});
	$("#flatchargespan").focusout(function() {
		var val = $(this).text();
		if(val == ""){
			$(this).text('0');
			var calfinalvalue=0;
			$('#flatcharge').val(calfinalvalue).trigger('change');
			$('#flatchargespan').text(val);
		}
	});
	$('#ratedefaulticon').on('click keypress',function(e){ 
		if(e.keyCode == 13 || e.keyCode === undefined){
			$('#ratedefaulticonoverlay').fadeIn();
			$('#rateclone').focus();
			$('.ratedefaultcalc').trigger('change');
	    }
	});
	$('#ratedefaulticonclose').click(function(){
		$('#ratedefaulticonoverlay').fadeOut();
		$('#ratepergram').focus();
	});
	/* $('.ratedefaultcalc').change(function(){
		var rate = $('#rateclone').val();
		var rateaddition = $('#rateaddition').val();
		var finalrate = parseFloat(parseFloat(rate) + (parseFloat(rate) * (rateaddition/100))).toFixed(roundamount);
		$('#finalrateclone').val(finalrate);
		$('#ratepergram').val(finalrate).trigger('change');
	}); */
	//estimation number change
	$("#estimationnumber").change(function(){
		var estimationnumber = $("#estimationnumber").val();
		var estimatearraydata = $("#estimatearray").val();
		if( estimatearraydata.match(new RegExp("(?:^|,)"+estimationnumber+"(?:,|$)"))) {
			$("#estimationnumber").val('');
			alertpopup('Already this estimation available.Added new one');
			return false;
		}else {
			 if(checkValue(estimationnumber) == true) {
				//chequeestimationnumber(estimationnumber);
				getestimatedata(estimationnumber);
			} else {
			}
		}
	});
	$('#counter').change(function(){
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
		var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
		var product = $("#product").val();
		var purity = $("#purity").val();
		var counter = $(this).val();
		if(retrievetagstatus == 0){
			if(transactiontype != 20 && counterstatus == 'YES') {
			  getweight(stocktypeid,product,purity,counter,'1');
			}
		}
	 
	});
	//Pieces trigger change
	$('#pieces').change(function() {
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
		var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
		//validategetweight(stocktypeid,'3','pieces');
		if(stocktypeid == '75') { // take order
			$("#wastageclone,#makingchargeclone").trigger('change');	
		} else if(stocktypeid == '13') { //P.tag - Changed pieces validate as 1
			$('#pieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
		} else if(stocktypeid == 17 && tagtypeid == 5) {
			$("#proditemrate").trigger('change');	
		}
		if(checkVariable('additionalchargeamt') == true) {
			if(checkVariable('hpiece') == true) {
				$('#hpiece').trigger('change');
			}
			if(checkVariable('makingcharge') == true) {
				var keyvalue = $('#makingcharge').attr('keyword');
				if(keyvalue == 'MC-PI') {
					$('#makingchargeclone').trigger('change');
				}
			}
		}
	});
	// Hallmark charges change function
	$('#hpiece,#hpiecefinal').change(function(){
		if($(this).val() != '') {
			var chargevalue = $(this).val();
		}else{
			var chargevalue = 0;
		}
		var pieces = $('#pieces').val();
		var attrid = $(this).attr('id');
		var hmarkspanlabel = 'HM-PI';
		if(attrid == 'hpiece'){
			var calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
			$('#hpiecefinal').val(parseFloat(calfinalvalue));
			$("#productaddonform label span[id="+hmarkspanlabel).text('('+parseFloat(chargevalue)+')');
		}else if(attrid == 'hpiecefinal'){
			var calfinalvalue=parseFloat(chargevalue)/parseFloat(pieces);
			$('#hpiece').val(parseFloat(calfinalvalue)).trigger('change');
			$("#productaddonform label span[id="+hmarkspanlabel).text('('+parseFloat(calfinalvalue)+')');
		}
		$(this).val(parseFloat(chargevalue));
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
		
	});
	$('#hflat,#hflatfinal').change(function(){
		if($(this).val() != '') {
			var chargevalue = $(this).val();
		}else{
			var chargevalue = 0;
		}
		var attrid = $(this).attr('id');
		var hmarkspanlabel = 'HM-FT';
		var calfinalvalue=0;
		calfinalvalue=parseFloat(chargevalue);
	
		if(attrid == 'hflat'){
			$('#hflatfinal').val(calfinalvalue);
			$("#productaddonform label span[id="+hmarkspanlabel).text('('+calfinalvalue+')');
		}else if(attrid == 'hflatfinal'){
			$('#hflat').val(calfinalvalue).trigger('change');
			$("#productaddonform label span[id="+hmarkspanlabel).text('('+calfinalvalue+')');
		}
		$(this).val(calfinalvalue);
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
		
	});
	$('#cflat,#certfinal').change(function(){
		if($(this).val() != '') {
			var chargevalue = $(this).val();
		}else{
			var chargevalue = 0;
		}
		var attrid = $(this).attr('id');
		var hmarkspanlabel = 'CC-FT';
		var calfinalvalue=parseFloat(chargevalue);
		if(attrid == 'cflat'){
				$('#certfinal').val(calfinalvalue);
				$("#productaddonform label span[id="+hmarkspanlabel).text('('+calfinalvalue+')');
		}else if(attrid == 'certfinal'){
			$('#cflat').val(calfinalvalue).trigger('change');
				$("#productaddonform label span[id="+hmarkspanlabel).text('('+calfinalvalue+')');
		}
		$(this).val(calfinalvalue);
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
		
	});
	$('#discountpasswordtab').click(function(){
		$("#discountpwdoverlayform").validationEngine('validate');			
        $(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#discountpwdoverlayform").validationEngine({
			onSuccess: function() {
				var password = $.trim($('#discountpassword').val());
				 	checkdiscountpassword(password);								
			},
			onFailure: function() {
				
			}
	});
	$('#discountpasswordcancel').click(function(){
		$('#discountpassword').val('');
		$('#discountpwdoverlay').hide();
	});
		// Round off down/up/normal
	(function() {
	  /**
	   * Decimal adjustment of a number.
	   *
	   * @param {String}  type  The type of adjustment.
	   * @param {Number}  value The number.
	   * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
	   * @returns {Number} The adjusted value.
	   */
	  function decimalAdjust(type, value, exp) {
		// If the exp is undefined or zero...
		if (typeof exp === 'undefined' || +exp === 0) {
		  return Math[type](value);
		}
		value = +value;
		exp = +exp;
		// If the value is not a number or the exp is not an integer...
		if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
		  return NaN;
		}
		// Shift
		value = value.toString().split('e');
		value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
		// Shift back
		value = value.toString().split('e');
		return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
	  }

	  // Decimal round
	  if (!Math.round10) {
		Math.round10 = function(value, exp) {
		  return decimalAdjust('round', value, exp);
		};
	  }
	  // Decimal floor
	  if (!Math.floor10) {
		Math.floor10 = function(value, exp) {
		  return decimalAdjust('floor', value, exp);
		};
	  }
	  // Decimal ceil
	  if (!Math.ceil10) {
		Math.ceil10 = function(value, exp) {
		  return decimalAdjust('ceil', value, exp);
		};
	  }
	})();
    $('#wastageless').change(function(){
		oldjewelcalc();
		if(oldjewelsstonedetails == 1) { totalgrossamountcalculationwork(); }
	});
    $('#transactionmodeid').change(function() {
		var calctype = $(this).find('option:selected').val();
		if(calctype > 0) {
			$('#accountid').val('');
			$("#s2id_accountid a span:first").text('');
			$('#accountid').trigger('change');
			var calctype = $(this).find('option:selected').val();
			var val=$('#salestransactiontypeid').find('option:selected').val();
			var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
			resetirfields();
			var purchasestktype = '';
			if(calctype == 2) { // amount
				$('#amountsummary').show();
				$('#discounthide').show();
				$('#grossamount-div').show();
				$('#weightsummaryhide,#pureweightsummary').hide();
				$('#grossamount').attr('data-validation-engine','validate[custom[number],min[1],decval[5]]');
				if(val == 25) { //Contra - Payment type
					var payment_payment = '26,27,28,29,79';
				} else if(val == 9) { // Purchase - Payment type
					var payment_payment = '26,27,28,29,31,37,79';
				} else if(val == 22) { // Payment issue - payment type
					var payment_payment = '26,27,28,29,31,37,79';
				} else { // payment type
					var payment_payment = '26,27,28,29,31,39,37,79';
				}
				var purchasestktype = '17';
				var purchasestktypedisable1 = '80';
				var purchasestktypedisable2 = '81';
			} else if(calctype == 3) { // purewt
				$('#discounthide').hide();
				$('#weightsummaryhide').show();
				$('#grossamount-div').hide();
				$('#amountsummary').hide();
				$('#pureweightsummary').show();
				$('#grossamount').attr('data-validation-engine','');
				if(val == 9) {
					$('#oldjewelwtsummary-div,#salesreturnwtsummary-div').hide();
					$('#purchasereturnwtsummary-div').show();
				} else {
					$('#oldjewelwtsummary-div,#salesreturnwtsummary-div').show();
					$('#purchasereturnwtsummary-div').hide();
				}
				var payment_payment = '49,38,31,38';   // payment type
				var purchasestktype = '80';
				var purchasestktypedisable1 = '17';
				var purchasestktypedisable2 = '81';
				if(val == 26) { //metal i/r
					var payment_payment = '49';   // payment type
				}
			} else if(calctype == 4) { // amount - purewt
				$('#pureweightsummary,#amountsummary').show();
				$('#grossamount-div').show();
				$('#discounthide,#weightsummaryhide').show();
				$('#grossamount').attr('data-validation-engine','validate[custom[number],decval[5]]');
				if(val == 9) { //Purchase - payment type
					var payment_payment = '26,27,28,29,37,38,49,79';
				} else if(val == 22) { // Payment issue - payment type
					var payment_payment = '26,27,28,29,79,37,38,49';
				} else { // payment type
					var payment_payment = '26,27,28,29,79,39,37,38,49';
				}
				var purchasestktype = '81';
				var purchasestktypedisable1 = '80';
				var purchasestktypedisable2 = '17';
			}
			if(val == 24) { //metal i/r
				var payment_payment = '31';   // payment type
			}
			if(val == 22 || val == 23 || val == 25 || val == 26 || val == 24) {
				$('#discounthide,#weightsummaryhide').hide();
			} else if(val == 9) {
				$("#stocktypeid").select2('val','');
				$("#stocktypeid option[value="+purchasestktypedisable1+"]").addClass("ddhidedisplay").prop('disabled',true);
				$("#stocktypeid option[value="+purchasestktypedisable2+"]").addClass("ddhidedisplay").prop('disabled',true);
				$("#stocktypeid option[value="+purchasestktype+"]").removeClass("ddhidedisplay").prop('disabled',false);
				$("#stocktypeid option[value=73]").removeClass("ddhidedisplay").prop('disabled',false);
				$("#taxtypeid").val($('#salestransactiontypeid').find('option:selected').data('taxtypeid'));
				$("#sdiscounttypeid").val($('#salestransactiontypeid').find('option:selected').data('discounttype'));
				$('#stocktypeid').select2('val',purchasestktype);
			}
			{ // paymentstocktype show hide based on calc type
				if( ratecuttypeid == 2){
					ratecuttypechange();
				} else {
					var ddname = 'paymentstocktypeid';
					$("#paymentstocktypeid").select2('val','');
					$("#"+ddname+" option ").addClass("ddhidedisplay");
					$("#"+ddname+" option ").prop('disabled',true);
					var payment = payment_payment.split(',');
					for(i=0;i<(payment.length);i++) {
						$("#"+ddname+" option[value="+payment[i]+"]").removeClass("ddhidedisplay");
						$("#"+ddname+" option[value="+payment[i]+"]").prop('disabled',false);
					}
					defaultsetpaymentstocktype = payment[0];
					$('#paymentstocktypeid').select2('val',payment[0]).trigger('change');
					if((val == 22 && ratecuttypeid == 1) || (val == 23 && ratecuttypeid == 1)){
						var manualstatus = accountmanualstatus();
						if(manualstatus == 1){
							$("#paymentstocktypeid option[value=37]").addClass("ddhidedisplay");
							$("#paymentstocktypeid option[value=37]").prop('disabled',true);
							$("#paymentstocktypeid option[value=38]").addClass("ddhidedisplay");
							$("#paymentstocktypeid option[value=38]").prop('disabled',true);
						}
					}	
				}
			}
			$('#stocktypeid').trigger('change');
		}
	});
	//discount limit calculation
	$("#sumarydiscountlimit,#billsumarydiscountlimit").change(function(){
		billdiscountcalculation();
	});
	
	$("#sumarydiscountamount").change(function() {

		if($(this).val() == ''){$(this).val(0);$("#discountpercentvalue").text(''); };
		var gridrowdata = getgridallrowids('saledetailsgrid');
		var billgrossamount = $('#billgrossamount').val();
		var salesdetailid = gridrowdata.toString();
		var transactionmodestatus = 0;
		var salesdetails=salesdetailid.split(",");
		$.each(salesdetails, function (index, value) {
			var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
			if(modestatus == '2') {
				transactionmodestatus = 1;
			}
		});
		if(transactionmodestatus == 0) {
			$("#sumarydiscountamount").val(0);
			$("#discountpercentvalue").text(''); 
			alertpopup('You Must Add Sales Detail Entry Before Creation Discount');
			return false;
		}else {
			if(billgrossamount == 0)
			{
				$("#sumarydiscountamount").val(0);
				$("#discountpercentvalue").text(''); 
			}else{
				var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				if(discountbilllevelpercent == 1 ) { // Direct wastage percent
					if(salestransactiontypeid == 11 ||  salestransactiontypeid == 16 || salestransactiontypeid == 20) {
						var discountpercenttb = $("#sumarydiscountamount").val();
						var wastageamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','83'));
						var finaldiscountamtvalue =  parseFloat(wastageamount * (discountpercenttb/100)).toFixed(roundamount);
						$("#sumarydiscountamount").val(finaldiscountamtvalue);
						$("#discountpercentvalue").text(discountpercenttb+'%'); 
					}
				} else if(discountbilllevelpercent == 2) { // Netwt * Rate * Discount Percentage
					if(salestransactiontypeid == 11 ||  salestransactiontypeid == 16 || salestransactiontypeid == 20) {
						var discountpercenttb = $("#sumarydiscountamount").val();
						var count = $('#saledetailsgrid .gridcontent div.data-content div.data-rows').length;
						
						var ratepergram = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','83'));
						
						var ratepergramcount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','83'));
						
						var netweight = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','83'));
						var finaldiscountamtvalue =  parseFloat((parseFloat((ratepergram/ratepergramcount) * netweight)) * (discountpercenttb/100)).toFixed(roundamount);
						$("#sumarydiscountamount").val(finaldiscountamtvalue);
						$("#discountpercentvalue").text(discountpercenttb+'%'); 
					}

				}
			}
			finalsummarycalc(0);
		}
	});
	// item level discount
	$("#discount").change(function() {
		var val = 0;
		if($(this).val() == ''){
			val = 0;
			$(this).val(0);
			//totalgrossamountcalculationwork();
		} else {
			val = $(this).val();
			$(this).val(parseFloat(val).toFixed(roundamount));
			var presumarydiscountamount = $('#discountdata').val();
			var netamount = $('#totalamount').val();
			var stocktypeid = $('#stocktypeid').find('option:selected').val();
			if(discountbilllevelpercent == 2) { // Discount - Calculation (Netwt * rate * percentage value)
				var netweight = $('#netweight').val();
				var ratepergram = $('#ratepergram').val();
				var finaldiscountamtvalue =  parseFloat(netweight * ratepergram * (val/100)).toFixed(roundamount);
				if(stocktypeid == 20 && parseFloat(finaldiscountamtvalue) >= parseFloat(netamount)) {
					$("#discount").val(0);

					alertpopup('Please enter the amount less to Net Amount');
					return false;
				} else {
					$("#discount").val(finaldiscountamtvalue);
				}
				if(stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81) { }// purchase no discount check
				if(stocktypeid != 20) {
					if(parseFloat(finaldiscountamtvalue) > parseFloat(presumarydiscountamount)) {
						$("#discount").val(0);
						alertpopup('Please enter the amount less or equal to Discount Value');
						return false;
					} else {
						$("#discount").val(finaldiscountamtvalue);
					}
				}
			} else if(discountbilllevelpercent == 1) { //Direct wastage percentage value
				var wastage = $('#wastage').val();
				var finaldiscountamtvalue =  parseFloat(wastage * (val/100)).toFixed(roundamount);
				if(stocktypeid == 20 && parseFloat(finaldiscountamtvalue) >= parseFloat(netamount)) {
					$("#discount").val(0);
					alertpopup('Please enter the amount less to Net Amount');
					return false;

				} else {
					$("#discount").val(finaldiscountamtvalue);
				}
				if(stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81) { }// purchase no discount check
				if(stocktypeid != 20) {
					if(parseFloat(finaldiscountamtvalue) > parseFloat(presumarydiscountamount)) {
						$("#discount").val(0);

						alertpopup('Please enter the amount less or equal to Discount Value');
						return false;
					} else {
						$("#discount").val(finaldiscountamtvalue);
					}
				}
			} else { // Normal discount
				if(stocktypeid == 20 && parseFloat(val) >= parseFloat(netamount)) {
					$("#discount").val(0);

					//totalgrossamountcalculationwork();
					alertpopup('Please enter the amount less to Net Amount');
					return false;
				}else if(stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81){ // purchase no discount check
					//totalgrossamountcalculationwork();	
				}else{
					if(stocktypeid != 20 ){
						if(parseFloat(val) > parseFloat(presumarydiscountamount)) {
							$("#discount").val(0);
							//totalgrossamountcalculationwork();
							alertpopup('Please enter the amount less or equal to Discount Value');
							return false;
						}else {
							//totalgrossamountcalculationwork();
						}
					} else {//totalgrossamountcalculationwork();}
					
					}
				}
			}
			




		}
		totalgrossamountcalculationwork();
	});
	//Place order overlay work
	$("#takeorderdetailclose").click(function() {
		$("#ordernumber").select2('val','');
		$("#takeorderdetailoverlay").fadeOut();
	});
	
	
	//discount auth work
	$("#discountautnicon").click(function() {
		var gridrowdata = getgridallrowids('saledetailsgrid');
		var salesdetailid = gridrowdata.toString();
		var transactionmodestatus = 0;
		var salesdetails=salesdetailid.split(",");
		$.each(salesdetails, function (index, value) {
		var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
		if(modestatus == '2'){
				transactionmodestatus = 1;
			}
		});
		if(salesdetailentryeditstatus == 1) 
		{
			alertpopup('Unable to use.Because this is edit mode');
			return false;
		}
		if(transactionmodestatus == 0) {
			alertpopup('You Must Add Sales Detail Entry Before Creation Discount');
			return false;
		}else {
			var billgrossamount = $('#billgrossamount').val();
			if(billgrossamount == 0)
			{
				alertpopup('Discount is not applicable');
			} else
			{
				$("#discountauthovrelay").show();
				$('#discountauthemployeeid').select2('val','');
				$('#discountauthpassword,#discountauthlimit').val('');
				$('#discountauthlimit').prop('readonly','readonly');
				$("#discountauthemployeeid").select2('val',$('#hiddenemployeeid').val());
			}
		}
	});
	//discount auth work
	$("#itemdiscountautnicon").click(function() {
		var stocktypeid= $('#stocktypeid').find('option:selected').val();
		  if(stocktypeid == 20){
			  alertpopup('Discount Authorization is not applicable');
		  }else{
			$("#discountauthovrelay").show();
			$('#discountauthemployeeid').select2('val','');
			$('#discountauthpassword,#discountauthlimit').val('');
			$('#discountauthlimit').prop('readonly','readonly');
			$("#discountauthemployeeid").select2('val',$('#hiddenemployeeid').val());
		  }
	});	
	$("#discountauthovrelayclose").click(function() {
		$("#discountauthovrelay").hide();
		$("#discountauthemployeeid").select2('val','');
		$("#discountauthpassword,#discountauthlimit").val('')
	});
	$("#discountauthemployeeid").change(function() {
		$('#discountauthpassword,#discountauthlimit').val('');
	});
	$("#discountauthpassword").change(function() {
		$("#processoverlay").show();
		var newpassword = $("#discountauthpassword").val();
		var empid = $("#discountauthemployeeid").find('option:selected').val();
		var discaltype =  $("#discaltype").val();
		var discountmode = $("#sdiscounttypeid").val();
		var category = '';
		var purity = '';
		var metal = '';
		if(discountmode == 2)
		{
			var categoryid = $('#product').find('option:selected').data('categoryid');
			var purityid= $('#purity').find('option:selected').val();
			var metalid = $('#purity').find('option:selected').data('metalid');
			if($.trim(discountvaluearray) != '')
			{
				discountvaluearray.forEach(function(entry) {
					if (entry.categoryid == categoryid && entry.purityid == purityid && entry.metalid == metalid) {
						category = categoryid;
						purity = purityid;
						metal = metalid;
					}else if (entry.purityid == purityid && entry.metalid == metalid && entry.categoryid == 1) {
						category = 1;
						purity = purityid;
						metal = metalid;
					}else if (entry.purityid == purityid && entry.metalid == 1 && entry.categoryid == 1) {
						category = 1;
						purity = purityid;
						metal = 1;
					}else if (entry.purityid == 1 && entry.metalid == metalid && entry.categoryid == 1) {
						category = 1;
						purity = 1;
						metal = metalid;
					}else if (entry.purityid == purityid && entry.metalid == 1 && entry.categoryid == categoryid) {
						category = categoryid;
						purity = purityid;
						metal = 1;
					}else if (entry.purityid == 1 && entry.metalid == metalid && entry.categoryid == categoryid) {
						category = categoryid;
						purity = 1;
						metal = metalid;
					}else if (entry.purityid == 1 && entry.metalid == 1 && entry.categoryid == categoryid) {
						category = categoryid;
						purity = 1;
						metal = 1;
					}else if (entry.categoryid == '1' && entry.purityid == '1' && entry.metalid == '1') {
						category = 1;
						purity = 1;
						metal = 1;
					}
				});
			}
		}else if(discountmode == 3)
		{
			category = 1;
			purity = 1;
			metal = 1;
		}
		if(empid != '' && empid != 1) {
			$.ajax({
				url: base_url + "Sales/discountauthpasswordget?employeeid="+empid+"&discountmode="+discountmode+"&discaltype="+discaltype+"&newpassword="+newpassword+"&categoryid="+category+"&purityid="+purity+"&metalid="+metal,
				dataType: 'json',
				success: function(msg) {
					$("#processoverlay").hide();
					if(msg['status'] == 'FAILURE') {
						$("#discountauthpassword").val('');
						$("#discountauthlimit").val(0);
						alertpopup('Please Enter the correct discount authorization password');
						return false;
					} else if(msg['status'] == 'NOUSER') {
						$("#discountauthpassword").val('');
						alertpopup('Please make discount data in discount authorization');
					}else if(msg['status'] == 'SUCCESS') {
						if(discountmode == 2) { // item level
							$('#discountauthlimit,#discamt').val(msg['limit']);
							$("#sumarydiscountlimit").val(msg['limit']);
						}else if(discountmode == 3) { // bill level
							$('#discountauthlimit,#billsumarydiscountlimit,#billdiscamt').val(msg['limit']);
						}
						if(msg['roleid'] == 2) {
							$('#discountauthlimit').prop('readonly','');
						}else{
							$('#discountauthlimit').prop('readonly','readonly');
						}
					}
				},
			});
		}else{
			$("#processoverlay").hide();
		}
	});
	$("#discountauthlimit").focusout(function() {
		var discaltype = $("#discaltype").val();
		if($("#discountauthlimit").val() == ''){
			$("#discountauthlimit").val(0);
		}
		if(discaltype == '3') {
			if($("#discountauthlimit").val() > 100) {
				$("#discountauthlimit").val(0);
				alertpopup('Please enter the discount limit value less than the 100');
				return false;
			}
		}
	});
	{//Discount auth add
		$('#discountauthsubmit').click(function(){
			$("#discountautnvalidation").validationEngine('validate');			
		});
		jQuery("#discountautnvalidation").validationEngine({
			onSuccess: function() {
				var discountauthlimit = $.trim($("#discountauthlimit").val());
				var sdiscounttypeid = $('#sdiscounttypeid').val();
				if(discountauthlimit != ''){
					var discountauthemployeeid = $("#discountauthemployeeid").val();
					$("#discountempid").val(discountauthemployeeid);
					if(sdiscounttypeid == 2){
						$("#discamt").val(discountauthlimit);
						$('#discount').trigger('change');
						$("#sumarydiscountlimit").val(discountauthlimit).trigger('change');
					}else if(sdiscounttypeid == 3){
						$("#billdiscamt").val(discountauthlimit);
						$('#sumarydiscountamount').trigger('change');
						$("#billsumarydiscountlimit").val(discountauthlimit).trigger('change');
					}
				}
				$("#discountauthovrelay").hide();							
			},
			onFailure: function() {
				alertpopup('Please enter the required fields');
			}
		});
	}
	
	{ // round value apply both (negative and postive )
	  $('#roundvalue').change(function(){
			
		 if($.trim($('#billgrossamount').val()) == ''){
			 var billgrossamount =0;
		  }else{
			var billgrossamount = $('#billgrossamount').val();
		  }
		  if($.trim($('#sumarytaxamount').val()) == ''){
			 var sumarytaxamount =0;
		  }else{
			var sumarytaxamount = $('#sumarytaxamount').val();
		  }
		  if($.trim($('#sumarydiscountamount').val()) == ''){
			 var sumarydiscountamount =0;
		  }else{
			var sumarydiscountamount = $('#sumarydiscountamount').val();
		  }
		  var oldjewelamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','19'));
		  var salesreturnamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','20'));
		  var salesreturnroundoffamt = roundoff(salesreturnamt);
		  var diff_salesreturn = salesreturnroundoffamt - salesreturnamt;
		  $('#summarysalesreturnroundvalue').val(diff_salesreturn.toFixed(roundamount));
		  salesreturnamt = parseFloat(salesreturnamt) + parseFloat(diff_salesreturn);
		  var paymenttotal = parseFloat(billgrossamount)+parseFloat(sumarytaxamount)-parseFloat(sumarydiscountamount)-parseFloat(oldjewelamt)-parseFloat(salesreturnamt);
		  if(Math.abs($('#roundvalue').val()) >= Math.abs(paymenttotal)) {
			  var roundvalue = paymenttotal;
		  }else{
			var roundvalue = $('#roundvalue').val();
		  }
		  if($.trim($(this).val()) == ''){
			   var val = 0;
			   $(this).val((0).toFixed(roundamount));
		   }else{
				var val = parseFloat($(this).val()); 
				 $(this).val((val).toFixed(roundamount));
		   }
		  var finalamt = 0;
		  var finalval = 0;
		  if(isNaN(val)){
			  finalval = 0;
		  }
		  else if(val > 0){
			  finalval = val;
		  }else if(val < 0){
			  finalval = Math.abs(val);
		  }
		  var textVal = $(this).val();
			if (textVal.indexOf('-') != -1) {
				if(textVal.match(/\-/g).length > 1 )
				{
					alertpopup('you cannot enter minus(-) or Dot(.) more than once');
					$(this).val((0).toFixed(roundamount));
					finalsummarycalc(0);
					return false;
				}
			}
		  if(parseInt(finalval) > parseInt(Math.abs(roundvalue))) {
			  var a = parseInt(roundvalue).toFixed(roundamount);
			  alertpopup('Please Enter the value between '+a+' and '+a+'');
			  $(this).val((0).toFixed(roundamount));
			  finalsummarycalc(0);
			  return false;
		  }else{
			  finalsummarycalc(0);
		  }
				
		});
	}
	// order advance amt adjustamt
	{
		$('#orderadvamtsummary').change(function(){
			if($.trim($(this).val()) == ''){
			   var val = 0;
			   $(this).val(0);
		   }else{
				var val = $(this).val();
		   }
		   var gridrowdata = getgridallrowids('saledetailsgrid');
			var billgrossamount = $('#billgrossamount').val();
			var orderadvamtsummaryhidden = $('#orderadvamtsummaryhidden').val();
			var salesdetailid = gridrowdata.toString();
			var transactionmodestatus = 0;
			var salesdetails=salesdetailid.split(",");
			$.each(salesdetails, function (index, value) {
			var modestatus = getgridcolvalue('saledetailsgrid',value,'modeid','');
			if(modestatus == '2'){
					transactionmodestatus = 1;
				}
			});
			if(transactionmodestatus == 0) {
				$("#orderadvamtsummary").val(0);
				alertpopup('You Must Add Sales Detail Entry Before make advance');
				return false;
			}else {
				if(billgrossamount == 0)
				{
					$("#orderadvamtsummary").val(0);
				}else{
					
				}
				if(parseFloat(val) > parseFloat(orderadvamtsummaryhidden))
				{
					$("#orderadvamtsummary").val(0);
					alertpopup('Please make payment below advance amount');
					return false;
				}
				finalsummarycalc(0);
			}
		});
	}
	
	//Salesdetail-Entry Edit-Form inneredit
	$("#salesdetailentryedit").click(function() {
		var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
		$('#editinnerrowid').val(selectedrow);
		var oldjewelvoucherstatus = getgridcolvalue('saledetailsgrid',selectedrow,'statusoldjewelvoucher','');
		var oldjewelvoucheredit = $('#oldjewelvoucheredit').val();
		if(oldjewelvoucheredit == 1) {
			if(oldjewelvoucherstatus == 1) {
				alertpopup('Unable To Edit Old jewel voucher');
				return false;
			}
		}
		var salesbillingtypechk = $("#billingtype").val();
		var salesbillingtypestockname = getgridcolvalue('saledetailsgrid',selectedrow,'stocktypename','');
		if(salesbillingtypechk == 2 && salesbillingtypestockname == 'Return') {
			alertpopup('Unable To Edit Return Item Details!');
			return false;
		}
		var paymenttypeid = $('#paymentstocktypeid').find('option:selected').val();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		if(selectedrow) {
			$('#dataaddsbtn').hide();
			var paymentmodeid = getgridcolvalue('saledetailsgrid',selectedrow,'paymentmodeid','');
			retrievetagstatus = 1;
			salesdetailentryeditstatus = 1;
			var selectstocktypeid = getgridcolvalue('saledetailsgrid',selectedrow,'stocktypeid','');
			var selectpaymentstocktypeid = getgridcolvalue('saledetailsgrid',selectedrow,'paymentstocktypeid','');
			var purityid = getgridcolvalue('saledetailsgrid',selectedrow,'purity','');
			var product = getgridcolvalue('saledetailsgrid',selectedrow,'product','');
			var balaceproductid = getgridcolvalue('saledetailsgrid',selectedrow,'balaceproductid','');
			var hiddenstonedata = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'hiddenstonedetail',''));
			var taxgriddata = getgridcolvalue('saledetailsgrid',selectedrow,'taxgriddata','');
			$('#hiddenstonedetail').val(hiddenstonedata);
			$('#taxgriddata').val(taxgriddata);
			var taxgriddata = getgridcolvalue('saledetailsgrid',selectedrow,'taxgriddata','');
			var grosswt = getgridcolvalue('saledetailsgrid',selectedrow,'grossweight','');
			var modeid = getgridcolvalue('saledetailsgrid',selectedrow,'modeid','');
			var creditadjustvaluehidden = getgridcolvalue('saledetailsgrid',selectedrow,'creditadjustvaluehidden','');
			$('#creditadjustvaluehidden').val(creditadjustvaluehidden);
			var creditadjusthidden = getgridcolvalue('saledetailsgrid',selectedrow,'creditadjusthidden','');
			$('#creditadjusthidden').val(creditadjusthidden);
			var mainsalesdetailid = getgridcolvalue('saledetailsgrid',selectedrow,'mainsalesdetailid','');
			$('#mainsalesdetailid').val(mainsalesdetailid);
			oldsalesreturntax = getgridcolvalue('saledetailsgrid',selectedrow,'mainsalesdetailid','');
			if(selectstocktypeid == '0') {ratepergram
				//Nothing
			} else if(selectstocktypeid == '') {
				//Nothing
			} else {
				$('#stocktypeid').select2('val',selectstocktypeid).trigger('change');
			}
			$('#paymentstocktypeid').select2('val',selectpaymentstocktypeid).trigger('change');
			if(modeid == 2 ) {
				if(selectstocktypeid != 82) {
					$('#itemtagnumber').prop('readonly','readonly');
				}
				$('#rfidtagnumber').prop('readonly','readonly');
				$('#product,#purity,#counter,#category').prop('disabled',false);
				$('#product option,#purity option,#counter option,#category option').prop('disabled',false);
				$('#sumarytaxamount,#sumarydiscountamount,#ordernumber').prop('disabled',true);
				$('#paymentstocktypeid').select2('val',paymenttypeid);			
				$('#estimationnumber').attr('readonly',true);
				if(selectstocktypeid == 13) {
					getpendingproductname(purityid);
				}
				$('#paymentdetailsubmit').hide();
			} else if(paymentmodeid == 3) {
				var editpaymenttotalamount = getgridcolvalue('saledetailsgrid',selectedrow,'paymenttotalamount','');
				var editpaymentpureweight = getgridcolvalue('saledetailsgrid',selectedrow,'paymentpureweight','');
				$('#editpaymenttotalamount').val(editpaymenttotalamount);
				$('#editpaymentpureweight').val(editpaymentpureweight);
				$('#paymentproduct option,#paymentpurity option,#paymentcounter option').prop('disabled',false);
				$('#paymentproduct,#paymentpurity,#paymentcounter').prop('disabled',false);
			}
			sales_gridtoformdataset('saledetailsgrid',selectedrow,modeid);
			$('#product,#purity,#counter,#paymentproduct,#paymentpurity,#paymentcounter,#category').prop('disabled',true);
			if(modeid == 2) {
				$('#paymentstocktypeid').select2('val',paymenttypeid);
				$('#salesdetailupdate').show();
				$('#salesdetailsubmit').hide();
				if(selectstocktypeid == 20) {
					$("#billnumberid-div").addClass('hidedisplay');
					$('#billnumberid').attr('data-validation-engine','');
				}
				//var stonewt = getgridcolvalue('saledetailsgrid',selectedrow,'stoneweight','');
				if(hiddenstonedata != '') {
					hiddenstonedata = $.parseJSON(hiddenstonedata);
					loadinlinegriddata('stoneentrygrid',hiddenstonedata.rows,'json');
				}
				var taxamount = getgridcolvalue('saledetailsgrid',selectedrow,'taxamount','');
				if(parseFloat(taxamount) != 0) {
					var hiddentaxdata = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'hiddentaxdata',''));
					if(hiddentaxdata !='') {
						cleargriddata('taxgrid');
						$('#hiddentaxdata').val(hiddentaxdata);
						hiddentaxdata = $.parseJSON(hiddentaxdata);
						loadinlinegriddata('taxgrid',hiddentaxdata.rows,'json');
					}
				}
				if(selectstocktypeid == 13) {
					$('#balaceproductid').select2('val',balaceproductid);
				} else if(selectstocktypeid == 82 || selectstocktypeid == 83 || selectstocktypeid == 88 || selectstocktypeid == 89) {
					var ordernumbername = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'ordernumbername',''));
					var ordernumber = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'ordernumber',''));
					$('#ordernumber').append($("<option value='"+ordernumber+"' data-ordernumberhidden='"+ordernumbername+"'>"+ordernumbername+"</option>"));
					$('#ordernumber').select2('val',ordernumber);
					$('#ordernumber').prop('disabled',true);
				}
				if(selectstocktypeid == 66 || selectstocktypeid == 25) {
					var approvalnumber = getgridcolvalue('saledetailsgrid',selectedrow,'approvalnumber','');
					$('#approvalnumber').select2('val',approvalnumber).attr('disabled', true);
					getapprovaloutuntagweight();
				} else {
					var product = $("#product").val();
					var purity = $("#purity").val();
					var counter = $("#counter").val();
					if(counterstatus == 'YES') {
						getweight(selectstocktypeid,product,purity,counter,'1');
					}else{
						getweight(selectstocktypeid,product,purity,'','1');
					}
				}
			}
			if(paymentmodeid == 3) {
				$('#paymentdetailupdate').show();
				$('#paymentdetailsubmit').hide();
				$('#paymentstocktypeid,#sumarytaxamount').prop('disabled',true);
				$('#sumarydiscountamount').attr('readonly',true);
				var product = $('#paymentproduct').val();
				var purity = $('#paymentpurity').val();
				var counter = $('#paymentcounter').val();
				if(counterstatus == 'YES') {
					getweight(selectpaymentstocktypeid,product,purity,counter,'2');
				}else{
					getweight(selectpaymentstocktypeid,product,purity,'','2');
				}
				if(salestransactiontypeid == 24 && selectpaymentstocktypeid == 31 ){
				  var paymentaccountid = getgridcolvalue('saledetailsgrid',selectedrow,'paymentaccountid','');
				  $('#journalaccountid').select2('val',paymentaccountid).trigger('change');	
				  $('#issuereceipttypeid,#journalaccountid').prop('disabled',true);
				}
			}
			$('#roundvalue').attr('readonly',true);
			$('#salesdetailentryedit,#salesdetailentrydelete').hide();
			operationstone = 'UPDATE';
			if(salestransactiontypeid == 9 || salestransactiontypeid == 27) {
				var chargeid = $('#product').find('option:selected').data('purchasechargeid');
			} else {
				var chargeid = $('#product').find('option:selected').data('chargeid');
			}
			if(modeid == 2) {
				$('#stocktypeid').prop('disabled',true);
				if(salestransactiontypeid == 16 && estimatequicktotal == 1 && selectstocktypeid == 11) { // estimatequicktotal
					var netamount = getgridcolvalue('saledetailsgrid',selectedrow,'totalamount','');
					var netpureweight = getgridcolvalue('saledetailsgrid',selectedrow,'pureweight','');
					$('#fasttotalfortagamt').val(netamount);
					$('#fasttotalfortagwt').val(netpureweight);
				}
				if(selectstocktypeid == 11 && grosswt == 0) {
					$('#makingcharge-div,#flatcharge-div,#repaircharge-div,#cdicon-div,#wastage-div,#stonecharge-div,#stoneoverlayicon').hide();
					$('#stonecharge').val(0);
				} else {
					if(selectstocktypeid == 19) {
						$("#stoneweight").attr('readonly',false);
						$('#olditemdetails').prop('readonly',true);
						$('#olditemvoucher-div').addClass('hidedisplay');
					} else {
						if((selectstocktypeid == 62 && approvaloutcalculation == 'NO') || (selectstocktypeid == 63 && approvaloutcalculation == 'NO' || selectstocktypeid == 24 || selectstocktypeid == 25 || selectstocktypeid == 65 || selectstocktypeid == 66)){ 
							//nothing
						} else {
							if(salestransactiontypeid == 18 && selectstocktypeid == 62 || (salestransactiontypeid == 16 || salestransactiontypeid == 11) && (selectstocktypeid == 11 || selectstocktypeid == 13)) {
								if(multitagbarcode == 2) {
									$('#tagnomultiscan').hide();
									$("#itemtagnumber").css('display','inline').css('width','');
									$('label[for="rfidtagnumber"]').css('width','');
								}
								$('#appoutrfidbtn').hide();
								$("#rfidtagnumber").css('display','inline').css('width','');
								$('label[for="rfidtagnumber"]').css('width','');
							}
							extrachargehideshow(chargeid);
							flatchargehideshow(chargeid);
							makingchargehideshow(chargeid);
							lstchargehideshow(chargeid);
							wastagechargehideshow(chargeid);
							paymenttouchhideshow(chargeid);
							repairchargehideshow(selectstocktypeid);
							stonechargehideshow();
						}
					}
				}
			}
			var grossweight = $('#grossweight').val();
			if(grossweight == 0) { // price tag
				itemdetailshideshow(1); // hide weight related field
			} else {
				itemdetailshideshow(0);  // // show weight related field
			}
			if(salestransactiontypeid == 11 || salestransactiontypeid == 16) {
				var prodstoneapplicable = $('#product').find('option:selected').data('stoneapplicable');
				if(selectstocktypeid == '19' && oldjewelsstonedetails == '1' && prodstoneapplicable == 'Yes') {
					$('#stoneweight-div').show();
					$('#stoneoverlayicon').show();
					$('#stonecharge-div').show();
					$('#stonecharge').attr('readonly',true);
					$('#stoneweighticon').hide();
				} else {
					stoneweighthideshow();
				}
			} else {
				stoneweighthideshow();
			}
			if(salestransactiontypeid == 9) {
				if(selectstocktypeid == 17 && modeid == 2) {
					if(checkVariable('stonecharge') == true) {
						var stonecharge = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'stonecharge',''));
						$("#stonecharge").val(stonecharge);
					}
				}
				var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
				var lstpurityid = $('#product').find('option:selected').data('purityid');
				$('#proditemrate-div,#itemcaratweight-div').hide();
				$('#proditemrate,#itemcaratweight').attr('data-validation-engine','');
				if(selectstocktypeid == 17 && tagtypeid == 5 && purchasedisplay == 1) {
					$('#proditemrate-div').show();
					$('#proditemrate').attr('data-validation-engine','validate[required,custom[number],min[1]]');
					$('#ratepergram-div,#grossweight-div,#netweight-div,#wastageweight-div').hide();
					$('#grossweight,#wastageweight,#netweight,#ratepergram').attr('data-validation-engine','');
				} else if((selectstocktypeid == 17 || selectstocktypeid == 80 || selectstocktypeid == 81) && lstpurityid == 11) {
					$('#itemcaratweight-div').show();
					$('#itemcaratweight').attr('data-validation-engine','validate[required,min[0.01],custom[number],decval['+dia_weight_round+']]');
					$('#ratepergram-div,#grossweight-div,#netweight-div,#wastageweight-div').hide();
					$('#grossweight,#wastageweight,#netweight,#ratepergram').attr('data-validation-engine','');
				} else {
					$('#proditemrate-div,#itemcaratweight-div').hide();
					$('#proditemrate,#itemcaratweight').attr('data-validation-engine','');
					$('#ratepergram-div,#grossweight-div,#netweight-div,#wastageweight-div').show();
					$('#ratepergram').attr('data-validation-engine','validate[required,custom[number],min[1]]');
					$('#grossweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],decval['+round+']]');
					$('#wastageweight').attr('data-validation-engine','validate[custom[number],decval['+round+']]');
					$('#netweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],funcCall[validategreater],decval['+round+']]');
				}
				var prodstoneapplicable = $('#product').find('option:selected').data('stoneapplicable');
				if((selectstocktypeid == 17 || selectstocktypeid == 80 || selectstocktypeid == 81) && modeid == 2 && purchasemodstonedetails == 1 && prodstoneapplicable == 'Yes') {
					$('#stoneoverlayicon').show();
					$('#stonecharge-div').show();
					$('#stonecharge').attr('readonly',true);
					$('#stoneweighticon').hide();
				}
			}
			if(salestransactiontypeid == 16 || salestransactiontypeid == 11) {
				var lstpurityid = $('#product').find('option:selected').data('purityid');
				if(selectstocktypeid == 12 && lstpurityid == 11) {
					$('#itemcaratweight-div').show();
					$('#ratepergram-div').hide();
					$('#itemcaratweight').attr('data-validation-engine','validate[required,min[0.01],custom[number],decval['+dia_weight_round+']]');
					$('#grossweight-div,#netweight-div').addClass('hidedisplay');
					$('#grossweight,#netweight,#ratepergram').attr('data-validation-engine','');
				} else {
					$('#itemcaratweight-div').hide();
					$('#ratepergram-div').show();
					$('#itemcaratweight').attr('data-validation-engine','');
					$('#grossweight-div,#netweight-div').removeClass('hidedisplay');
					$('#ratepergram').attr('data-validation-engine','validate[required,custom[number],min[1]]');
					$('#grossweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],decval['+round+']]');
					$('#netweight').attr('data-validation-engine','validate[required,custom[number],min[0.1],funcCall[validategreater],decval['+round+']]');
				}
			}
			if(salestransactiontypeid == 28 || salestransactiontypeid == 30 || selectstocktypeid == 89) { // Take Repair
				$('#ratepergram-div').show();
				$('#ratepergram').attr('data-validation-engine','validate[required,custom[number],min[1]]');
				if(selectstocktypeid == 86 || selectstocktypeid == 88 || selectstocktypeid == 89) {
					$('#ratepergram-div').hide();
					$('#ratepergram').attr('data-validation-engine','');
				}
			}
			if(selectstocktypeid == 75 || selectstocktypeid == 82 || selectstocktypeid == 83 || selectstocktypeid == 86 || selectstocktypeid == 88 || selectstocktypeid == 89) {
				sizehideshow(product);
				if(selectstocktypeid == 75 && checkVariable('stonecharge') == true) {
					$("#stonecharge").attr('readonly', true);
				}
			}
			if(selectstocktypeid == 19) {
				var rateless = $('#ratelesscalc').val();
				var rate = $("#ratepergram").val();
				var newrate =  (parseFloat(rate)-parseFloat(rateless));
				if(rateless != 0){
					$('#oldjewelnewrate').text('('+newrate+')');
				}
			}
			if(selectstocktypeid == 20) {
				$('#itemdiscountcalname').text('(0)');
			}
			//Image
			var image = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'tagimage',''));
			if(checkValue(image) == true) {
				$('#tagimagedisplay').empty();
				var imagesplit = [];
				var removetext = 'radio_button_checked';
				image = image.replace(removetext,'');
				imagesplit = image.split(',');
				var imagecount = image.split(',').length;
				for(var i=0; i< imagecount; i++) {
					$('#tagimagedisplay').append('<div data-imgpath="'+imagesplit[i]+'"><img id="companylogodynamic" style="height:100%;width:100%" src="'+base_url+imagesplit[i]+'"><i class="material-icons documentslogodownloadclsbtn" id="removeimageusingcloseicon" data-imagepath="'+imagesplit[i]+'">close</i></div>');
				}
			} else {
				$('#tagimage').val('');
				$('#tagimagedisplay').empty();
			}
			if(modeid == '2' && transactionemployeeperson == '0') {
				$('#employeepersonid').trigger('change');
			}
			if(modeid == '2' && takeordercategory == '1' && transactioncategoryfield == '0') {
				var categoryvalue = $.trim(getgridcolvalue('saledetailsgrid',selectedrow,'categoryname',''));
				setTimeout(function() {
					$('#categorynamehidden').val(categoryvalue);
					$('#prodcategorylistbutton').prop('disabled',true);
				},10);
			}
			Materialize.updateTextFields();
		} else {
			alertpopup('Please Select The Row To Edit');
		}
	});
	$("#salesdetailentrydelete").click(function() {
		var datarowid = $('#saledetailsgrid div.gridcontent div.active').attr('id');
		var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		if(datarowid) {		
			if(operationstone == 'UPDATE') {
				alertpopup("A Record Under Edit Form");
			} else {
				$("#processoverlay").show();
				var estimationnumber = getgridcolvalue('saledetailsgrid',datarowid,'estimationnumber','');
				var salesdetailid = getgridcolvalue('saledetailsgrid',datarowid,'salesdetailid','');
				var stocktype = getgridcolvalue('saledetailsgrid',datarowid,'stocktypeid','');
				/*delete grid data*/
				deletegriddatarow('saledetailsgrid',datarowid);
				sequenceascendingorder('saledetailsgrid');
				var estimategridarray = getgridcolcolumnvalue('saledetailsgrid','','estimationnumber');
				var salesdetailidgridarray = getgridcolcolumnvalue('saledetailsgrid','','salesdetailid');
				bhavcutbasedsummaryshowhide();
				var sdiscounttypeid = $("#sdiscounttypeid").val();
				if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
					iraccounthideshow();
				} else {
					accounticonhideshow();
				}
				finalsummarycalc(0);
				var estimatestatus = 1;
				var estimatedata = $('#estimatearray').val();
				if(estimategridarray != '') {
					var estimatefinal=estimategridarray.toString().split(",");
					$.each(estimatefinal, function (index, value) {
						if(value == estimationnumber) {
							estimatestatus = 0;
						}
					}); 
				}
				if(estimatestatus == 1) {
					estimatearray = new Array();
					var chargedetails1=estimatedata.toString().split(",");
					$.each(chargedetails1, function (index, res) {
							if(res != estimationnumber) {
								estimatearray.push(res);
							}
					});
					$('#estimatearray').val(estimatearray);	
				}
				var salesdetailstatus = 1;
				var salesdetaildata = $('#approvaloutreturnproductid').val();
				if(salesdetailidgridarray != '') {
					var estimatefinal=salesdetailidgridarray.toString().split(",");
					$.each(estimatefinal, function (index, value) {
						if(value == salesdetailid) {
							salesdetailstatus = 0;
						}
					}); 
				}
				if(salesdetailstatus == 1) {
					salesdetailarray = new Array();
					var chargedetails1=salesdetaildata.toString().split(",");
					$.each(chargedetails1, function (index, res) {
						if(res != salesdetailid) {
							salesdetailarray.push(res);
						}
					});
					$('#approvaloutreturnproductid').val(salesdetailarray);	
				}
				if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
					if(approvalnoeditstatus == 0) {
						$("#salestransactiontypeid").prop('disabled',false);
					}
					estimatearray = new Array();
					salesdetailarray = new Array();
					$('#estimatearray').val('');
					$("#discountcalname").text('(0)');
				}
				if(salestransactiontypeid == 20){ // takeorder
					if($('#generatesalesid').val() == '') {
						orderstocktypehideshow(stocktype);
					}
				}
				if(salestransactiontypeid == 11){ // sales
					if($('#generatesalesid').val() == '') {
						//if(stocktype == 19 || stocktype == 83) {
							deliveryorderstocktypehideshow(stocktype);
						//}
					}
				}
				if(stocktype == 83) {
					$('#liveadvanceclose').val(0);
				}
				if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
					if(stocktype == 85) {
						$("#salestransactiontypeid,#transactionmodeid").prop('disabled',true);
						$('#addaccount,#accountsearchevent').hide();
					}
				}
				var stocktypeid = $("#stocktypeid").find('option:selected').val();
				if(stocktypeid == 12 || stocktypeid == 73){
					$('#stocktypeid').select2('val',stocktypeid).trigger('change');
				}
				$("#processoverlay").hide();
			}
		} else {
			alertpopup(selectrowalert);
		}
	});
	$("#grossamount").change(function() { // gross amount based hide/save button
		var grossamount = $.trim($('#grossamount').val());
		var sdiscounttypeid = $("#sdiscounttypeid").val();
		var stocktypeid = $("#stocktypeid").find('option:selected').val();
		if(retrievetagstatus==0) {
			if(grossamount =='' || grossamount == 0) {
				$('#salesdetailsubmit').hide();
			}else{
				$('#salesdetailsubmit').show();
			}
		}
		if(grossamount !='' && grossamount != 0) {
			if(sdiscounttypeid == 2) { // item level discount
			   if(stocktypeid == 19 || stocktypeid == 20) { // old jewel & sales return
					$("#sumarydiscountlimit,#discamt").val(0);
			   }else {
					if(retrievetagstatus==0) {
						setdiscountdata();
					}
					setTimeout(function(){
						billdiscountcalculation();
					},5);
					
			   }
			}
		}
	});
	$("#grosspureweight").change(function() { // grosspureweight based hide/save button
		var grosspureweight = $.trim($('#grosspureweight').val());
		if(retrievetagstatus==0) {
			if(grosspureweight =='' || grosspureweight == 0) {
				$('#salesdetailsubmit').hide();
			}else{
				$('#salesdetailsubmit').show();
			}
		}
	});
	$("#pureweight").change(function() { // pureweight based hide/save button
		var pureweight = $.trim($('#pureweight').val());
		if(retrievetagstatus==0) {
			if(pureweight =='' || pureweight == 0) {
				$('#salesdetailsubmit').hide();
			}else{
				$('#salesdetailsubmit').show();
			}
		}
	});
	// sales date change
	$('#salesdate').change(function() {
		var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		if(salestransactiontypeid == 22 || salestransactiontypeid == 23) // payment issue & receipt
		{
			$('#accountid').val('');
			$('#s2id_accountid a span:first').text('');
			resetirfields();
		}
	});
	// load bill type change
	$('#loadbilltype').change(function() {
		var accountid = $("#accountid").val();
		var manualstatus = accountmanualstatus();
		if(accountid == '' && manualstatus == 1) {
			alertpopup('Please select account');
		} else {
			$('#credittotalamount,#credittotalpurewt').val(0);
			clearceditoverlayform();
			issuereceiptdataget(1);
			$('#creditno').val('');
			setcreditsummary();
			$('#creditreference').val(3).trigger('change');
		}
	});
	// image button
	$(document).on("click","#imageitemicon",function() {
		$('#imageformspan').fadeIn();
	});
	// image button close
	$('#itemimageclose').click(function() {
		$('#imageformspan').fadeOut();
	});
	{// File upload				
		fileuploadmoname = 'tagfileupload';
		$('#tagimagedisplay').empty();
		var companysettings = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "png,jpg,jpeg,bmp,gif",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) {
				if(data != 'Size') {
					var snarr =$.parseJSON(data);
					var arr = data.split(',');
					var name = $('.dyimageupload').data('imgattr');
					if($('#tagimage').val() == '') {
						$('#tagimage').val(snarr.path);
					} else {
						$('#tagimage').val(snarr.path+','+$('#tagimage').val());
					}
					$('#tagimagedisplay').append('<div data-imgpath="'+snarr.path+'"><img id="companylogodynamic" style="height:100%;width:100%" src="'+base_url+snarr.path+'"><i class="material-icons documentslogodownloadclsbtn" id="removeimageusingcloseicon" data-imagepath="'+snarr.path+'">close</i></div>');
					alertpopupdouble('Your file is uploaded successfully.');	
					$('#savecapturedimg').hide();		
				} else {
					alertpopupdouble('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$(document).on("click","#removeimageusingcloseicon",function() {
			var imgloc = $(this).closest('div').attr('data-imgpath');
			var imagewithloc = imgloc.replace(base_url,'');
			if(imagewithloc != '') {
				$(this).closest('div').remove();
				var tagimage = $('#tagimage').val();
				tagimage = tagimage.replace(imgloc,''); 
				$('#tagimage').val(tagimage.replace(/^,|,$/g,''));
				$.ajax({
					url: base_url +"Itemtag/deleteimagefromfolder",
					data:{imagewithloc:imagewithloc},
					type: "POST",
					success: function(data) {
						if(data == 'SUCCESS') {
							alertpopup('Image was removed from System.');
						} else {
							alertpopup('Unable to remove images from system. Kindly try again later!');
						}
					}
				});
			} else {
				alertpopup('No image to be remove.');
			}
		});
		$("#taglogomulitplefileuploader").uploadFile(companysettings);		
		//file upload drop down change function
		$("#tagfileuploadfromid").click(function(){
			$(".triggeruploadtagfileupload:last").trigger('click');
			$('#savecapturedimg').hide();
		});
		$("#resetcapturedimg").click(function(){
			var imgloc = $('#tagimagedisplay').find('img').attr('src');	
			var imagewithloc;			
			$("#imgurldata").val('');				
			$("#tagimagedisplay").empty();
			$("#savecapturedimg").show();
			$('#tagimage').val('');
			if(imgloc != '') {
				imagewithloc = imgloc.replace(base_url,'');
			} else {
				imagewithloc = 0;
			}
			if(imagewithloc != '0') {
				$.ajax({
				url: base_url +"Itemtag/deleteimagefromfolder",
				data:{imagewithloc:imagewithloc},
				type: "POST",
				success: function(data) {
						alertpopup('Image was removed from System.');
						$('#alerts').fadeOut(20);
					}
				});
			} else {
				alertpopup('No image to be remove.');
				$('#alerts').fadeOut(20);
			}
		});
	}
	{//*image upload manage*//
		$("#imgcapture").click(function(){			
			$("#taglivedisplay").empty();
			$("#tagimagedisplay").empty();
			addscreencapture();
		});
		$("#imagecaptureclose").click(function(){
			$("#imagecaptureoverlay").fadeOut();
		});
		function addscreencapture() {
			// Web cam capture Add screen function			
			var sayCheese = new SayCheese('#taglivedisplay', {video: true});
			sayCheese.start();
			$('#take-snapshot').click(function(){
				var width = 0, height = 0;
				sayCheese.takeSnapshot(width, height);				
				$("#savecapturedimg").show();					
			});
			sayCheese.on('snapshot', function(snapshot){			  
				var img = document.createElement('img');
				$(img).on('load', function(){
					$("#tagimagedisplay").empty();
					$('#tagimagedisplay').prepend(img);
				});
				img.src = snapshot.toDataURL('image/png');
				$("#imgurldata").val(img.src);
			});
			$("#savecapturedimg").click(function(){
				var dataurlimg =  $("#imgurldata").val();
				setTimeout(function(){ 
				success(dataurlimg);
				$("#savecapturedimg").hide();	
				},10);	
					
			});
			$("#resetcapturedimg").click(function() {
				$("#imgurldata").val('');				
				$("#tagimagedisplay").empty();
				$("#savecapturedimg").show();
			});
		}
		{// Webcam add Function
			function success(imageData) {			
				var url = base_url+'webcamcapture.php';
				var params = {image: imageData};
				$.post(url, params, function(data)
				{				
					$("#tagimage").val(data);
				});
			}
			{//image alter function
				function successdelete(imageData) {
					var url = base_url+'webcamcapture.php';
					var params = {image: imageData};
					$.post(url, params, function(data) {
						$(".snapshotcapturebtn").removeClass('hidedisplay');
						$("#imagecaptureoverlay").fadeOut();
						$("#resultbtnset").addClass('hidedisplay');
						$("#delnewimgname").val(data);
						savecapturedproimgbtn();
					});
				}
			}
		}
	}
	// bulk sales button click
	$('#salesbulkicon').click(function(){
		var accountid = $('#accountid').val();
		if(accountid) {
			$('#bulksalesgridoverlay').fadeIn();
			cleargriddata('bulksalesgrid');
			getbulktagnumberdata();
		}else {
			alertpopup('Please select account');
		}
	});
	$('#bulksalesrgridclose').click(function(){
		$('#bulksalesgridoverlay').fadeOut();
	});
	$('#bulksalesrgridclose').click(function(){
		$('#activecount,#conflictcount').val(0);
		cleargriddata('bulksalesgrid');
		clearrfidtable();
	});
	$('#appoutrfidbtn').click(function(){
		var accountid = $('#accountid').val();
		if(accountid) {
			$('#totalsalesoverlay').fadeIn();
			getbulktagnumberdata();
		}else {
			alertpopup('Please select account');
		}
	});
	$('#tagnomultiscan').click(function(){
		var accountid = $('#accountid').val();
		if(accountid) {
			$('#multitagscan').fadeIn();
			$('#multitagscantagstextarea').focus();
		}else {
			alertpopup('Please select account');
		}
	});
	$('#multitagclose').click(function(){
		$('#multitagscan').fadeOut();
		$('#multitagscantagstextarea').val('');
	});
	$('#multiscanclose').click(function(){
		$('#multiscanconfirmoverlay').fadeOut();
	});
	$('#multitagaddsbtn').click(function(){
		getvalidatethetagdata(1);
	});
	$('#mainmultitagaddsbtn').click(function(){
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		if(salestransactiontypeid == 18 || salestransactiontypeid == 16) {	
			getvalidatethetagdata(2);
		} else {
			alertpopup('Cannot Use Main Save For sales');
		}
	});
	$('#multiscanaddsbtn').click(function(){
		multiscanmainsave(1);
	});
	$('#totalsalesclose').click(function(){
		$('#totalsalesoverlay').fadeOut();
	});
	$('#totalsalesfetchtbl').click(function(){
		$('#appoutrfidbtn').trigger('click');
	});
	$('#totalsalescleartbl').click(function(){
		$.ajax({
			url:base_url +"Sales/clearrfidtable",
			data:{},
			type: "POST",
			success: function() {
				$('#totaltags,#loadtags,#totaltagshidden,#totaltagsnohidden').val('');
				alertpopup('Data Cleared kindly Scan Again and after complete Press Fetch');
			}
		});
	});
	$('#totalsalesaddsbtn').click(function(){
		$('#processoverlay').show();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		if(approvaloutrfid == 2 || approvaloutrfid == 3){
			if(salestransactiontypeid == 18 && approvaloutcalculation == 'NO') {
				getbulkdatarows($('#totaltagshidden').val());
			}else if(salestransactiontypeid == 11 ||  salestransactiontypeid == 16 || salestransactiontypeid == 18 && approvaloutcalculation == 'YES') {
				rfidtagarray = $('#totaltagsnohidden').val();
				rfidtagarray = rfidtagarray.split(",");
				if(rfidtagarray.length > 0){
					rfidbasedtagfetch = 0;
					$('#itemtagnumber').val(rfidtagarray[0]).trigger('change');
				}
			}
		}
		$('#totalsalesoverlay').fadeOut();
		$('#processoverlay').hide();
	});
	// Main view grid - to display image i.e., salesdetail image preview
	$(document).on( "click", "#maintableimagepreview", function() {
		var salesid = $(this).closest('div').attr('id');
		if(salesid !='' || salesid != 1) {
			$.ajax({
				url:base_url+"Sales/retrievesalesdetailimage",
				data: "salesid=" +salesid,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var totalimages = JSON.parse(msg);
					if (totalimages == 'FAILED') {
						alertpopup('No Image to preview');
					} else {
						$('#salesdetailimageoverlaypreview').fadeIn();
						$('#salesdetailtagimagepreview').empty();
						var imagepreview = new Array();
						for (var i = 0; i < totalimages.length; i++) {
							imagepreview = totalimages[i].split(',');
							for (var j = 0; j < imagepreview.length; j++) {
								var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
								img.attr('src', base_url+imagepreview[j]);
								img.appendTo('#salesdetailtagimagepreview');
							}
						}
					}
				}
			});
		}
	});
	{ // Old Purchase Round Off Concept Code
		// Old Purchase Round Off Amount
		$(document).on( "click", "#oldjewelamtsummary-div", function() {
			var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
			var oldjewelamtsummary = $('#oldjewelamtsummary').val();
			if(oldjewelamtsummary  > 0 && salestransactiontypeid == 11 && billamountcalculationtrigger == 1) {
				var paymenttotalamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
				if(paymenttotalamount > 0) {
					alertpopupdouble('After Payment you cannot adjust the Old Purchase Amount. Kindly delete the payment lists and try again!');
				} else {
					$('#oldjewelscalculationoverlay').fadeIn();
					$('#oldjewelscalculationchange').val(oldjewelamtsummary);
					Materialize.updateTextFields();
				}
			}
		});
		// Old Purchase Round Off Close
		$(document).on( "click", "#oldjewelscalculationchangeclose", function() {
			$('#oldjewelscalculationoverlay').fadeOut();
		});
		// Change of Amount - Checking maximum value condition
		$(document).on( "change", "#oldjewelscalculationchange", function() {
			var valueofamount = parseFloat($(this).val()).toFixed(roundamount);
			$('#oldjewelscalculationchange').val(valueofamount);
			var oldjewelamtsummary = $('#oldjewelamtsummary').val();
			var checkval = $("#billsumarydiscountlimit").val();
			if(checkval > 0) {
				if(discountoldamtwithsign < 0) {
					var differencevalue = Math.abs(parseFloat(oldjewelamtsummary) - parseFloat(valueofamount) - parseFloat(discountoldamtwithsign)).toFixed(roundamount);
				} else {
					var differencevalue = Math.abs(parseFloat(oldjewelamtsummary) - parseFloat(valueofamount) + (discountoldamtwithsign * -1)).toFixed(roundamount);
				}
				var maxbillleveldiscamt = $("#billsumarydiscountlimit").val();
				if(parseFloat(differencevalue) > parseFloat(maxbillleveldiscamt)) {
					$('#oldjewelscalculationchange').val(oldjewelamtsummary);
					alertpopupdouble('Your max adjustment value is '+maxbillleveldiscamt+', and you have used '+Math.abs(discountoldamtwithsign)+'');
					Materialize.updateTextFields();
				}
			} else {
				alertpopupdouble('Not given authorization for giving discount.!');
			}
		});
		// Trigger Change the amount and reflects in Old Purchase section
		$(document).on("click", "#oldjewelscalculationchangesubmit", function() {
			var exactoldamountvalue = [];
			var exactoldamountcheck = 0;
			var differenceadd_grosscheck = 0;
			var calculationsuccess = 0;
			var oldjewelamtsummary = $('#oldjewelamtsummary').val();
			var manualoldjewelamtsummary = $('#oldjewelscalculationchange').val();
			var differenceadd_gross = parseFloat(parseFloat(manualoldjewelamtsummary) - parseFloat(oldjewelamtsummary)).toFixed(roundamount);
			discountoldamtwithsign = parseFloat(parseFloat(discountoldamtwithsign) + parseFloat(manualoldjewelamtsummary) - parseFloat(oldjewelamtsummary)).toFixed(roundamount);
			var numberofolditemsingrid = getsalesstocktypegridallrowids('saledetailsgrid','old');
			if(numberofolditemsingrid.length > 0) {
				differenceadd_grosscheck = differenceadd_gross;
				differenceadd_gross = parseFloat(differenceadd_gross / numberofolditemsingrid.length).toFixed(roundamount);
				for(var k=0; k<numberofolditemsingrid.length; k++) {
					exactoldamountvalue[k] = differenceadd_gross;
					exactoldamountcheck = parseFloat(parseFloat(exactoldamountcheck) + parseFloat(differenceadd_gross)).toFixed(roundamount);
				}
			}
			if(parseFloat(differenceadd_grosscheck) != parseFloat(exactoldamountcheck)) {
				var finalarrayvalue = parseFloat(parseFloat(differenceadd_grosscheck) - parseFloat(exactoldamountcheck)).toFixed(roundamount);
				exactoldamountvalue[0] = parseFloat(parseFloat(exactoldamountvalue[0]) + parseFloat(finalarrayvalue)).toFixed(roundamount);
			}
			for(var j=0; j< numberofolditemsingrid.length; j++) {
				var finalamounteachitem = 0;
				$('#saledetailsgrid div.gridcontent div.data-rows').removeClass('active');
				$('#saledetailsgrid div.gridcontent div #'+numberofolditemsingrid[j]+'').addClass('active');
				var salesdetailsgrossamount = $('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .grossamount-class').text();
				var stonechargecheck = $('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .stonecharge-class').text();
				var dustweightcheck = $('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .dustweight-class').text();
				if(stonechargecheck > 0) {
					var salesdetailsstonedetails = JSON.parse($('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .hiddenstonedata-class').text());
					if(salesdetailsstonedetails[0] != '') {
						var purchaserate = salesdetailsstonedetails[0].purchaserate;
						var editstoneamount = salesdetailsstonedetails[0].stonetotalamount;
						var manualfinalstoneamount = parseFloat(parseFloat(editstoneamount) + parseFloat(exactoldamountvalue[j])).toFixed(roundamount);
						var maunalbasicrate = parseFloat(manualfinalstoneamount / (salesdetailsstonedetails[0].caratweight)).toFixed(roundamount);
						salesdetailsstonedetails[0].stonetotalamount = manualfinalstoneamount;
						salesdetailsstonedetails[0].purchaserate = maunalbasicrate;
						$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .stonecharge-class').text(manualfinalstoneamount);
					}
					$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .hiddenstonedata-class').text(JSON.stringify(salesdetailsstonedetails));
					$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .hiddenstonedetail-class').text(stonejsonconvertdataautotrigger(JSON.stringify(salesdetailsstonedetails)));
				} else if(dustweightcheck > 0){
					var salesdetailsratepergram = $('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .ratepergram-class').text();
					var salesdetailsgrsweight = $('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .grossweight-class').text();
					var manualgrsamt = parseFloat(parseFloat(salesdetailsgrossamount) + parseFloat(exactoldamountvalue[j])).toFixed(roundamount);
					var manualnetwt = parseFloat(parseFloat(manualgrsamt) / parseFloat(salesdetailsratepergram)).toFixed(roundamount);
					var manualdustwt = parseFloat(parseFloat(salesdetailsgrsweight) - parseFloat(manualnetwt)).toFixed(roundamount);
					$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .dustweight-class').text(manualdustwt);
					$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .netweight-class').text(manualnetwt);
				}
				finalamounteachitem = parseFloat(parseFloat(salesdetailsgrossamount) + parseFloat(exactoldamountvalue[j])).toFixed(roundamount);
				$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .grossamount-class').text(finalamounteachitem);
				$('#saledetailsgrid .gridcontent div#'+numberofolditemsingrid[j]+' ul .totalamount-class').text(finalamounteachitem);
			}
			finalsummarycalc(3);
			$('#oldjewelscalculationoverlay').fadeOut();
		});
	}
	{ // Net Amount Adjustment - affected in all the items (tag, untag, p.tag, Order tag)
		// Overlay to enter final net amount
		$(document).on( "click", "#billnetamount-div", function() {
			if(salesdetailentryeditstatus == 1) {
				alertpopup('Unable to use. Because item is in edit mode');
				return false;
			}
			var checkval = $("#billsumarydiscountlimit").val();
			if(checkval > 0) {
				var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
				var billamount = $('#billnetamount').val();
				if(billamount > 0 && (salestransactiontypeid == 11) && (billamountcalculationtrigger == 1 || billamountcalculationtrigger == 2)) {
					var salesitemcount = getsalesstocktypegridallrowids('saledetailsgrid','all').length;
					if(salesitemcount == 0) {
						alertpopupdouble('There is no sale item in the grid. Kindly add some items to adjust net value.!');
					} else {
						var paymenttotalamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
						if(paymenttotalamount > 0) {
							alertpopupdouble('After Payment you cannot adjust the Bill Amount. Kindly delete the payment lists and try again!');
						} else {
							$('#billamountcalculationoverlay').fadeIn();
							$('#finalbillamountcalculationtrigger').val(billamount).trigger('change');
						}
					}
				}
			} else {
				alertpopupdouble('Not given authorization for giving discount.!');
			}
		});
		// Overlay close without trigger change
		$(document).on( "click", "#billamountcalculationtriggerclose", function() {
			$('#billamountcalculationoverlay').fadeOut();
		});
		// Final Net Amount Change - Maximum value check
		$(document).on( "change", "#finalbillamountcalculationtrigger", function() {
			var valueofamount = parseFloat($(this).val()).toFixed(roundamount);
			$('#finalbillamountcalculationtrigger').val(valueofamount);
			var billamount = $('#billnetamount').val();
			if(discountamtwithsign < 0) {
				var differencevalue = Math.abs(parseFloat(billamount - valueofamount - discountamtwithsign).toFixed(roundamount));
			} else {
				var differencevalue = Math.abs(parseFloat(billamount - valueofamount + (discountamtwithsign * -1)).toFixed(roundamount));
			}
			// To find the max discount value
			if(billamountcalculationtrigger == 1) {
				var maxbillleveldiscamt = $("#presumarydiscountamount").val();
				if(differencevalue > maxbillleveldiscamt ) {
					$('#finalbillamountcalculationtrigger').val(billamount).trigger('change');
					alertpopupdouble('Your max adjustment value is '+maxbillleveldiscamt+', and you have used '+Math.abs(discountamtwithsign)+'');
				}
			} else if(billamountcalculationtrigger == 2) {
				var adjustmentenable = $('#salestransactiontypeid').find('option:selected').data('roundoffamount');
				var billnetamountwithsign = $('#billnetamountwithsign').val();
				var adjustamt = $('#roundvalue').val();
				var taxtypeid = $("#taxtypeid").val();
				var gridsalesnetamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','83')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','89'));
				if(adjustmentenable == 'Yes') {
					if(billnetamountwithsign > 0) {
						var diffamount = parseFloat(parseFloat(valueofamount) + parseFloat(adjustamt) - parseFloat(billamount)).toFixed(roundamount);
					} else if(billnetamountwithsign < 0) {
						var diffamount = parseFloat(parseFloat(valueofamount) - parseFloat(adjustamt) - parseFloat(billamount)).toFixed(roundamount);
					}
				} else {
					var diffamount = parseFloat(parseFloat(valueofamount) - parseFloat(billamount)).toFixed(roundamount);
				}
				if((billnetamountwithsign > 0 && diffamount > 0) || (billnetamountwithsign < 0 && diffamount < 0)) {
					var discountsign = 1;
				} else if((billnetamountwithsign > 0 && diffamount < 0) || (billnetamountwithsign < 0 && diffamount > 0)) {
					var discountsign = -1;
				}
				var maxbillleveldiscamt = 0;
				var tagitemsrowids = getsalesstocktypegridallrowids('saledetailsgrid','all');
				for(var j=0; j<tagitemsrowids.length; j++) {
					var eachitemnetamt = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .totalamount-class').text();
					var eachitemtaxcatgid = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .taxcategory-class').text();
					var eachamtpercent = parseFloat(parseFloat(eachitemnetamt) / parseFloat(gridsalesnetamount) * 100).toFixed(3);
					var discamtvalue = Math.abs(parseFloat(parseFloat(diffamount) * parseFloat(eachamtpercent) / 100).toFixed(roundamount));
					if(taxtypeid == 2 && eachitemtaxcatgid > 1) {
						var taxrate = 0;
						var taxmasterdetails = taxmasterdetailsinarray();
						$.each(taxmasterdetails, function(index, value) {
							if(eachitemtaxcatgid == value[0]) {
								taxrate = value[1];
							}
						});
					} else {
						var taxrate = 0;
					}
					var eachfinaldiscamount = parseFloat((parseFloat(discamtvalue) / (100 + parseFloat(taxrate))) *100 ).toFixed(roundamount);
					var wastageamountcheck = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastage-class').text();
					var makingchargeamount = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text();
					var eachitemdiscountcheck = parseFloat(parseFloat(wastageamountcheck) + parseFloat(makingchargeamount)).toFixed(roundamount);
					maxbillleveldiscamt = parseFloat(parseFloat(maxbillleveldiscamt) + parseFloat(wastageamountcheck) + parseFloat(makingchargeamount)).toFixed(roundamount);
					if(parseFloat(eachfinaldiscamount) > parseFloat(eachitemdiscountcheck)) {
						$('#finalbillamountcalculationtrigger').val(billamount).trigger('change');
						var productname = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .productname-class').text();
						alertpopupdouble(''+productname+' has discount amount ('+eachfinaldiscamount+') and it should not greater than value '+eachitemdiscountcheck+'');
						return false;
					}
				}
			}
		});
		// Bill Amount Calculation overlay submit call
		$(document).on( "click", "#billamountcalculationtriggersubmit", function() {
			// If it is 0 then Amount reduced from discount otherwise 1 is adjustment with gross, tax and total amount
			var adjust_discount = $('#salestransactiontypeid').find('option:selected').data('adjustmentconcept');
			// If it is 0 then don't calculate the amount from stone otherwise take the value from stone
			var stoneadjustment = $('#salestransactiontypeid').find('option:selected').data('stoneadjustment');
			var taxtypeid = $("#taxtypeid").val();
			var adjustmentenable = $('#salestransactiontypeid').find('option:selected').data('roundoffamount');
			var exactbillamountvalue = [];
			var exactbillamountcheck = 0;
			var calculationchangecheck = 0;
			var billnetamount = $('#billnetamount').val();
			var adjustamt = $('#roundvalue').val();
			var adjustsalesreturnamt = $('#summarysalesreturnroundvalue').val();
			var billnetamountwithsign = $('#billnetamountwithsign').val();
			var newbillnetamount = $('#finalbillamountcalculationtrigger').val();
			if(parseFloat(newbillnetamount) == parseFloat(billnetamount)) {
				alertpopupdouble('Entered amount is same as Bill Amount. Please change the amount and try again.!');
			} else {
				$('#processoverlay').show();
				$('#billamountcalculationoverlay').fadeOut();
				var paymenttotalamount = getgridcolvalue('saledetailsgrid','','paymenttotalamount','sum');
				if(paymenttotalamount == 0) {
					var gridsalesnetamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','83')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','89'));
					if(adjustmentenable == 'Yes') {
						if(billnetamountwithsign > 0) {
							var diffamount = parseFloat(parseFloat(newbillnetamount) + parseFloat(adjustamt) - parseFloat(billnetamount)).toFixed(roundamount);
						} else if(billnetamountwithsign < 0) {
							var diffamount = parseFloat(parseFloat(newbillnetamount) - parseFloat(adjustamt) - parseFloat(billnetamount)).toFixed(roundamount);
						}
					} else {
						var diffamount = parseFloat(parseFloat(newbillnetamount) - parseFloat(billnetamount)).toFixed(roundamount);
					}
					discountamtwithsign = parseFloat(parseFloat(discountamtwithsign) + parseFloat(newbillnetamount) - parseFloat(billnetamount)).toFixed(roundamount);
					if((billnetamountwithsign > 0 && diffamount > 0) || (billnetamountwithsign < 0 && diffamount < 0)) {
						var discountsign = 1;
					} else if((billnetamountwithsign > 0 && diffamount < 0) || (billnetamountwithsign < 0 && diffamount > 0)) {
						var discountsign = -1;
					}
					var tagitemsrowids = getsalesstocktypegridallrowids('saledetailsgrid','all');
					for(var j=0; j<tagitemsrowids.length; j++) {
						var discountapplied = 0;
						var eachitemnetamt = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .totalamount-class').text();
						var eachitemtaxcatgid = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .taxcategory-class').text();
						var eachamtpercent = parseFloat(parseFloat(eachitemnetamt) / parseFloat(gridsalesnetamount) * 100).toFixed(3);
						var discamtvalue = Math.abs(parseFloat(parseFloat(diffamount) * parseFloat(eachamtpercent) / 100).toFixed(roundamount));
						if(taxtypeid == 2 && eachitemtaxcatgid > 1) {
							var taxrate = 0;
							var taxmasterdetails = taxmasterdetailsinarray();
							$.each(taxmasterdetails, function(index, value) {
								if(eachitemtaxcatgid == value[0]) {
									taxrate = value[1];
								}
							});
						} else {
							var taxrate = 0;
						}
						var eachfinaldiscamount = parseFloat((parseFloat(discamtvalue) / (100 + parseFloat(taxrate))) *100 ).toFixed(roundamount);
						var differenceadd_gross = 0;
						$('#saledetailsgrid div.gridcontent div.data-rows').removeClass('active');
						$('#saledetailsgrid div.gridcontent div #'+tagitemsrowids[j]+'').addClass('active');
						var salesdetailsgrossamount = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .grossamount-class').text();
						if(adjust_discount == 0 && discountsign == -1) { // Item level discount field
							$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .discount-class').text(eachfinaldiscamount);
							differenceadd_gross = parseFloat(parseFloat(salesdetailsgrossamount) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
						} else {
							eachfinaldiscamount = parseFloat(parseFloat(eachfinaldiscamount) * discountsign).toFixed(roundamount);
							if(billamountcalculationtrigger != 2) {
								var stonechargecheck = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .stonecharge-class').text();
							} else {
								var stonechargecheck = 0;
							}
							var wastageamountcheck = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastage-class').text();
							var makingamountcheck = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text();
							if(stonechargecheck > 0 && stoneadjustment == 1) {
								var salesdetailsstonedetails = JSON.parse($('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .hiddenstonedata-class').text());
								if(salesdetailsstonedetails[0] != '') {
									var basicrate = salesdetailsstonedetails[0].basicrate;
									var editstoneamount = salesdetailsstonedetails[0].stonetotalamount;
									var manualfinalstoneamount = parseFloat(parseFloat(editstoneamount) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
									var maunalbasicrate = parseFloat(manualfinalstoneamount / (salesdetailsstonedetails[0].caratweight)).toFixed(roundamount);
									salesdetailsstonedetails[0].stonetotalamount = manualfinalstoneamount;
									salesdetailsstonedetails[0].basicrate = maunalbasicrate;
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .stonecharge-class').text(manualfinalstoneamount);
								}
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .hiddenstonedata-class').text(JSON.stringify(salesdetailsstonedetails));
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .hiddenstonedetail-class').text(stonejsonconvertdataautotrigger(JSON.stringify(salesdetailsstonedetails)));
								discountapplied = 1;
							} else if(wastageamountcheck > 0) {
								var wstamt = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastage-class').text();
								var mkgamt = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text();
								var manualwstamt = parseFloat(parseFloat(wstamt) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
								var manualwstmkg = parseFloat(parseFloat(wstamt) + parseFloat(mkgamt) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
								if(manualwstamt > 0) {
									var wstspandata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastagespan-class').text();
									var wstspanlabeldata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastagespanlabel-class').text();
									var ratepergram = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .ratepergram-class').text();
									if(wstspanlabeldata == 'WS-PT-N') {
										var netweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .netweight-class').text();
										wstspandata = parseFloat((manualwstamt*100) / (netweight * ratepergram)).toFixed(roundamount);
									} else if(wstspanlabeldata == 'WS-PT-G') {
										var grossweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .grossweight-class').text();
										wstspandata = parseFloat((manualwstamt*100) / (grossweight * ratepergram)).toFixed(roundamount);
									} else if(wstspanlabeldata == 'WS-WT') {
										wstspandata = parseFloat(manualwstamt / ratepergram).toFixed(roundweight);
									}
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastage-class').text(manualwstamt);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastagespan-class').text(wstspandata);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastageclone-class').text(wstspandata);
									discountapplied = 1;
								} else if((parseFloat(manualwstmkg) >= parseFloat(eachfinaldiscamount)) && (parseFloat(mkgamt) > 0)) {
									// Wastage Amount Discount code
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastage-class').text(0);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastagespan-class').text(0);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .wastageclone-class').text(0);
									// Making Charge Amount Discount Code
									var makingchargespandata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespan-class').text();
									var makingchargespanlabeldata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespanlabel-class').text();
									var ratepergram = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .ratepergram-class').text();
									if(makingchargespanlabeldata == 'MC-GM-N') {
										var netweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .netweight-class').text();
										makingchargespandata = parseFloat((manualwstmkg) / (netweight)).toFixed(roundamount);
									} else if(makingchargespanlabeldata == 'MC-GM-G') {
										var grossweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .grossweight-class').text();
										makingchargespandata = parseFloat((manualwstmkg) / (grossweight)).toFixed(roundamount);
									} else if(makingchargespanlabeldata == 'MC-FT') {
										makingchargespandata = parseFloat(manualwstmkg).toFixed(roundweight);
									} else if(makingchargespanlabeldata == 'MC-PI') {
										var pieces = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .pieces-class').text();
										makingchargespandata = parseFloat(manualwstmkg / pieces).toFixed(roundweight);
									}
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text(manualwstmkg);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespan-class').text(makingchargespandata);
									$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargeclone-class').text(makingchargespandata);
									discountapplied = 1;
								}
							} else { // Repair Charges
								if(billamountcalculationtrigger == 2) {
									if(makingamountcheck > 0) {
										var makingchargeamount = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text();
										var manualmakingamount = parseFloat(parseFloat(makingchargeamount) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
										if(manualmakingamount > 0) {
											var makingchargespandata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespan-class').text();
											var makingchargespanlabeldata = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespanlabel-class').text();
											var ratepergram = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .ratepergram-class').text();
											if(makingchargespanlabeldata == 'MC-GM-N') {
												var netweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .netweight-class').text();
												makingchargespandata = parseFloat((manualmakingamount) / (netweight)).toFixed(roundamount);
											} else if(makingchargespanlabeldata == 'MC-GM-G') {
												var grossweight = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .grossweight-class').text();
												makingchargespandata = parseFloat((manualmakingamount) / (grossweight)).toFixed(roundamount);
											} else if(makingchargespanlabeldata == 'MC-FT') {
												makingchargespandata = parseFloat(manualmakingamount).toFixed(roundweight);
											} else if(makingchargespanlabeldata == 'MC-PI') {
												var pieces = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .pieces-class').text();
												makingchargespandata = parseFloat(manualmakingamount / pieces).toFixed(roundweight);
											}
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingcharge-class').text(manualmakingamount);
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargespan-class').text(makingchargespandata);
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .makingchargeclone-class').text(makingchargespandata);
											discountapplied = 1;
										}
									}
								} else {
									if(repaircharge > 0) {
										var repaircharge = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .repaircharge-class').text();
										var manualrepchargeamount = parseFloat(parseFloat(repaircharge) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
										if(manualrepchargeamount > 0) {
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .repaircharge-class').text(manualrepchargeamount);
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .prerepaircharge-class').text(manualrepchargeamount);
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .repairchargespan-class').text(manualrepchargeamount);
											$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .repairchargeclone-class').text(manualrepchargeamount);
											discountapplied = 1;
										}
									}
								}
							}
							if(discountapplied == 1) {
								differenceadd_gross = parseFloat(parseFloat(salesdetailsgrossamount) + parseFloat(eachfinaldiscamount)).toFixed(roundamount);
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .grossamount-class').text(differenceadd_gross);
							}
						}
						if(discountapplied == 1) {
							var sdtaxmasterid = $('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .taxcategory-class').text();
							if(sdtaxmasterid > 1) {
								var taxdetails = manualtaxcalculationinnergrid(differenceadd_gross,sdtaxmasterid);
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .taxgriddata-class').text(JSON.stringify(taxdetails[0]));
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .hiddentaxdata-class').text(jsonconvertdataformanualtax(differenceadd_gross,sdtaxmasterid));
								$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .taxamount-class').text(taxdetails[1]);
								var netamountvalue = parseFloat(parseFloat(differenceadd_gross) + parseFloat(taxdetails[1])).toFixed(roundamount);
							} else {
								var netamountvalue = parseFloat(differenceadd_gross).toFixed(roundamount);
							}
							$('#saledetailsgrid .gridcontent div#'+tagitemsrowids[j]+' ul .totalamount-class').text(netamountvalue);
						}
					}
					finalsummarycalc(3);
				} else {
					alertpopupdouble('Please delete all the payment list. Then do the Adjustment!');
				}
				$('#processoverlay').hide();
			}
		});
	}
	if(accountfieldcaps == 1) {
		$('#newaccountname,#newaccountshortname,#accountprimaryaddress,#primaryarea,#accountprimarypincode,#accountprimarycity').css('text-transform','uppercase');
		$('#newaccountname').change(function() {
			var name = $('#newaccountname').val();
			$('#newaccountname').val(name.toUpperCase());
		});
		$('#newaccountshortname').change(function() {
			var shortname = $('#newaccountshortname').val();
			$('#newaccountshortname').val(shortname.toUpperCase());
		});
		$('#accountprimaryaddress').change(function() {
			var address = $('#accountprimaryaddress').val();
			$('#accountprimaryaddress').val(address.toUpperCase());
		});
		$('#primaryarea').change(function() {
			var area = $('#primaryarea').val();
			$('#primaryarea').val(area.toUpperCase());
		});
		$('#accountprimarypincode').change(function() {
			var pincode = $('#accountprimarypincode').val();
			$('#accountprimarypincode').val(pincode.toUpperCase());
		});
		$('#accountprimarycity').change(function() {
			var city = $('#accountprimarycity').val();
			$('#accountprimarycity').val(city.toUpperCase());
		});
	}
});
{// all grid data
	{// Sales View Grid
		function salesgrid(page,rowcount) {
			var defrecview = $('#mainviewdefaultview').val();
			if(Operation == 1) {
				var rowcount = $('#transactionpgrowcount').val();
				var page = $('.paging').data('pagenum');
			} else {
				var rowcount = $('#transactionpgrowcount').val();
				page = typeof page == 'undefined' ? 1 : page;
				rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
			}
			var wwidth = $(window).width();
			var wheight = $(window).height();
			//col sort
			var sortcol = $("#sortcolumn").val();
			var sortord = $("#sortorder").val();
			if(sortcol){
				sortcol = sortcol;
			} else{
				var sortcol = $('.transactionheadercolsort').hasClass('datasort') ? $('.transactionheadercolsort.datasort').data('sortcolname') : '';
			}
			if(sortord) {
				sortord = sortord;
			} else {
				var sortord =  $('.transactionheadercolsort').hasClass('datasort') ? $('.transactionheadercolsort.datasort').data('sortorder') : '';
			}
			var headcolid = $('.transactionheadercolsort').hasClass('datasort') ? $('.transactionheadercolsort.datasort').attr('id') : '0';
			var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	        var viewfieldids = $('#viewfieldids').val();
	        var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
			var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
			var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	        $.ajax({
				url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=sales&primaryid=salesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					$('#salesgrid').empty();
					$('#salesgrid').append(data.content);
					$('#salesgridfooter').empty();
					$('#salesgridfooter').append(data.footer);
					//data row select event
					datarowselectevt();
					//column resize
					columnresize('salesgrid');
					{//sorting
						$('.transactionheadercolsort').click(function(){
							$("#processoverlay").show();
							$('.transactionheadercolsort').removeClass('datasort');
							$(this).addClass('datasort');
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							var sortcol = $('.transactionheadercolsort').hasClass('datasort') ? $('.transactionheadercolsort.datasort').data('sortcolname') : '';
							var sortord =  $('.transactionheadercolsort').hasClass('datasort') ? $('.transactionheadercolsort.datasort').data('sortorder') : '';
							$("#sortorder").val(sortord);
							$("#sortcolumn").val(sortcol);
							var sortpos = $('#salesgrid .gridcontent').scrollLeft();
							salesgrid(page,rowcount);
							$('#salesgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
							$("#processoverlay").hide();
						});
						sortordertypereset('transactionheadercolsort',headcolid,sortord);
					}
					{//pagination
						$('.pvpagnumclass').click(function(){
							Operation = 0;
							$("#processoverlay").show();
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							salesgrid(page,rowcount);
							$("#processoverlay").hide();
						});
						$('#transactionpgrowcount').change(function(){
							Operation = 0;
							$("#processoverlay").show();
							var rowcount = $(this).val();
							$('.pagerowcount').attr('data-rowcount',rowcount);
							$('.pagerowcount').text(rowcount);
							var page = 1;$('#prev').data('pagenum');
							salesgrid(page,rowcount);
							$("#processoverlay").hide();
						});
					}
					//Material select
					$('#transactionpgrowcount').material_select();		
				},
			});
		}
		{//refresh grid
			function salesrefreshgrid() {
				var page = $('ul#salespgnum li.active').data('pagenum');
				var rowcount = $('ul#salespgnumcnt li .page-text .active').data('rowcount');
				salesgrid(page,rowcount);
			}
		}
		
		
		function billgrid(page,rowcount) {
			if($('#savedbillstatus').val() == ''){
				var billstatus='';
			} else {
				var billstatus=$('#savedbillstatus').val();
			}
			if($('#generatesalesid').val() == ''){
				var salesid=1;
			} else {
				var salesid=$('#generatesalesid').val();
			}
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
			var wwidth = $('#billgrid').width();
			var wheight = $('#billgrid').height();
			/*col sort*/
			var sortcol = $('.billheadercolsort').hasClass('datasort') ? $('.billheadercolsort.datasort').data('sortcolname') : '';
			var sortord =  $('.billheadercolsort').hasClass('datasort') ? $('.billheadercolsort.datasort').data('sortorder') : '';
			var headcolid = $('.billheadercolsort').hasClass('datasort') ? $('.billheadercolsorts.datasort').attr('id') : '0';	
			$.ajax({
				url:base_url+"Sales/gridinformationfetchbill?maintabinfo=sales&primaryid=salesid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52'+'&filter='+billstatus+'&salesid='+salesid,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$('#billgrid').empty();
					$('#billgrid').append(data.content);
					$('#billgridfooter').empty();
					$('#billgridfooter').append(data.footer);
					//data row select event
					datarowselectevt();
					//column resize
					columnresize('billgrid');
					{//sorting
						$('.billheadercolsort').click(function(){
							$('.billheadercolsort').removeClass('datasort');
							$(this).addClass('datasort');
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							var sortpos = $('#billgrid .gridcontent').scrollLeft();
							billgrid(page,rowcount);
							$('#billgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						});
						sortordertypereset('billheadercolsort',headcolid,sortord);
					}
					{//pagination
						$('.pvpagnumclass').click(function(){
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							billgrid(page,rowcount);
						});
						$('#billpgrowcount').change(function(){
							var rowcount = $(this).val();
							$('.pagerowcount').attr('data-rowcount',rowcount);
							$('.pagerowcount').text(rowcount);
							var page = $('.pvpagnumclass').data('pagenum');
							billgrid(page,rowcount);
						});
					}
					//Material select
					$('#billpgrowcount').material_select();
				},
			});
		}
		function billrefreshgrid() {
			var page = $('ul#groupspgnum li.active').data('pagenum');
			var rowcount = $('ul#groupspgnumcnt li .page-text .active').data('rowcount');
			billgrid(page,rowcount);
		}
	}
	function accountsearchgrid(page,rowcount) {
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#accountsearchgrid').width();
		var wheight = $('#accountsearchgrid').height();
		/* col sort */
		var sortcol = $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').attr('id') : '0';
		var viewfieldids = 91;
		var userviewid = 172;
		var maintabinfo ='account';
		var primaryid = 'accountid';
		var transactionid = $('#salestransactiontypeid').find('option:selected').val();
		var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
		var filterid = $("#accfilterid").val()+'|3008';
		var conditionname = $("#accconditionname").val()+'|IN';
		if(transactionid == '21') {
			var filtervalue = $("#accfiltervalue").val()+'|16';
		} else if(transactionid == '9') { //for purchase
			var filtervalue = $("#accfiltervalue").val()+'|16';
		} else if(transactionid == '22' || transactionid == '23') { //for payment issue /receipt
			var filtervalue = $("#accfiltervalue").val()+'|16,6';
		} else {
			if(transactionmodeid == 2) {
				var filtervalue = $("#accfiltervalue").val()+'|6';
			} else if(transactionmodeid == 3) {
				var filtervalue = $("#accfiltervalue").val()+'|16';
			} else if(transactionmodeid == 4) {
				var filtervalue = $("#accfiltervalue").val()+'|16,6';
			}
		}
		
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo="+maintabinfo+"&primaryid="+primaryid+"&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#accountsearchgrid').empty();
				$('#accountsearchgrid').append(data.content);
				$('#accountsearchgridfooter').empty();
				$('#accountsearchgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('accountsearchgrid');
				{/* sorting */
					$('.accountsheadercolsort').click(function(){
						$('.accountsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						var sortpos = $('#accountsearchgrid .gridcontent').scrollLeft();
						accountsearchgrid(page,rowcount);
						$('#accountsearchgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					});
					sortordertypereset('accountsheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						accountsearchgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						accountsearchgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						accountsearchgrid(page,rowcount);
					});
					$('#accountspgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						accountsearchgrid(page,rowcount);
					});
					//Material select
				$('#accountspgrowcount').material_select();
				}
			},
		});
	}
	}
//innser grid check / Uncheck operation
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
		$("."+headerclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").prop( "checked", false );
	}
}
{//retrieve the addon charges
	function loadproductaddon(){
		sttypesales = ['11','12','13','14','15','16','74','75','17','62','63','80','81','82','20','73','85']; // all sales & purchase
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val(); //sales-transaction-type
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
		restric_addon_transactiontype = ['13','19']; // restricted salestransctions - ()
		if(sttypesales.indexOf(stocktypeid) != -1 && restric_addon_transactiontype.indexOf(salestransactiontypeid) === -1) {
			setzero(['grossweight','netweight','wastageweight','taxamount','proditemrate']);
			var productid = $('#product').find('option:selected').val();
			var categoryid = $('#category').find('option:selected').val();
			var purchasedisplay = $('#purchasedisplay').val();
			if(categoryid > 1) {} else {categoryid = '';}
			var purityid = $('#purity').find('option:selected').val();
			var lstpurityid = $('#product').find('option:selected').data('purityid');
			if(lstpurityid == 11) {
				var weight = parseFloat($('#itemcaratweight').val());
			} else {
				var weight = parseFloat($('#grossweight').val());
			}
			if($('#accountid').val() == '') {
				var accountid = '';
				var accounttype_id = '';
			} else {
				var accountid = $('#accountid').val();
				if(salestransactiontypeid == 9 || salestransactiontypeid == 27){
					var accounttype_id = 16;
				} else {
					var accounttype_id = 6;
				}
			}
			var itemtagnumber = $('#itemtagnumber').val();	
			var rfidtagnumber = $('#rfidtagnumber').val();
			var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
			if(tagtypeid == 5 && purchasedisplay == 1) {
				var conditioncheck = '1';
			} else {
				if(checkValue(weight) == true && weight > 0) {
					var conditioncheck = '1';
				} else {
					var conditioncheck = '2';
				}
			}
			if(checkValue(productid) == true && checkValue(purityid) == true && conditioncheck == 1) {
				$.ajax({
					url:base_url+"Sales/loadproductaddon",
					data:{productid:productid,categoryid:categoryid,purityid:purityid,weight:weight,accountid:accountid,salestransactiontypeid:salestransactiontypeid,accounttype_id:accounttype_id,itemtagnumber:itemtagnumber,rfidtagnumber:rfidtagnumber,stocktypeid:stocktypeid,purchasedisplay:purchasedisplay},
					dataType:'json',
					async:false,
					success: function(data) {
						calculationdone = 0;
						$("#productaddonform .chargedetailsdiv").addClass('hidedisplay');
						$('.hpiecefinaldiv,.hflatfinaldiv,.certfinaldiv').addClass('hidedisplay');
						$("#productaddonform .chargedetailsdiv .chargedata").val(0);
						$("#productaddonform .chargedetailsdiv .chargedata").attr('data-validation-engine','');
						// display charges based on product select
						if(salestransactiontypeid == 9) {
							 var chargeid=$("#product").find('option:selected').data('purchasechargeid');
							if(purchasewastage == 'Yes') {
								if(data.chargewastageweight == '' || data.chargewastageweight == 0){
								} else {
									var chargewastageweight = data.chargewastageweight;
									$('#wastageweight').val(chargewastageweight);
							 		setwastageweight();
								}
							}
						} else if(salestransactiontypeid == 18) {
						    if(accounttype_id == 6) {
								var chargeid = $("#product").find('option:selected').data('chargeid');
							} else if(accounttype_id == 16) {
								var chargeid = $("#product").find('option:selected').data('purchasechargeid');
							}
						} else {
							var chargeid = $("#product").find('option:selected').data('chargeid');
						}
						if(chargeid != '' || chargeid != 1) {
							chargeid=chargeid.toString();
							var chargeiddata = ['2','3','4','5','6','7','14'];  // wastage & making charge ids
							if (chargeid.indexOf(',') > -1) {
								var chargedetails=chargeid.split(",");
								if(chargedetails == '') {
								} else {
									$.each(chargedetails, function (index, value) {
										if(jQuery.inArray( value, chargeiddata ) == -1) {  // except wastage & making charge
											$("#productaddonform .charge"+value).removeClass('hidedisplay');
											$("#productaddonform .charge"+value+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
										}
									});
								}
							} else {
								if(jQuery.inArray( chargeid, chargeiddata ) == -1) {  // except wastage & making charge
									$("#productaddonform .charge"+chargeid).removeClass('hidedisplay');
								}
							}
						}
						var chargekey=[];
						var chargevalue=[];
						if(data.chargedetails != ""  && data.chargedetails != 'null') {
							var chargedetails1=data.chargedetails.split(",");
							$.each(chargedetails1, function (index, value) {
								if(data.chargeid == '') {
									var res=value.split(":");
									chargekey.push(res[0]);
									chargevalue.push(0);
									$("#productaddonform input[keyword="+res[0]).attr('originalvalue',0);
									$("#productaddonform input[keyword="+res[0]).val(0);
									$("#productaddonform label span[id="+res[0]).text('('+0+')');
									if(value =='MC-FT' || value == 'MC-GM-N' || value == 'MC-PI' || value == 'MC-GM-G') {
										if(value =='MC-FT') {
											$('#makingchargekeylabel').text('/flat');
										} else if(value =='MC-GM-N' || value == 'MC-GM-G') {
											$('#makingchargekeylabel').text('/gm');
										} else if(value == 'MC-PI') {
											$('#makingchargekeylabel').text('/pcs');
										}
										$("#makingchargeclone").attr('keyword',value);
										$("#makingchargespan,#makingcharge").attr('keyword',value);
										$("#makingchargeclone").val(0).trigger('change');
									} else if(value =='WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G') {
										if(value =='WS-PT-N') {
											$('#wastagekeylabel').text('%Nt');
										} else if(value =='WS-WT') {
											$('#wastagekeylabel').text('/flat');
										} else if(value =='WS-PT-G') {
											$('#wastagekeylabel').text('%Gr');
										}
										$("#wastageclone").attr('keyword',value);
										$("#wastagespan,#wastage").attr('keyword',value);
										$("#wastageclone,#paymenttouch").val(0).trigger('change');
									} else if(value =='FC' || value == 'FC-WT' ) {
										if(value =='FC') {
											$('#flatchargekeylabel').text('/AMT');
										} else if(value =='FC-WT') {
											$('#flatchargekeylabel').text('/WT');
										}
										$("#flatchargeclone").attr('keyword',value);
										$("#flatchargespan,#flatcharge").attr('keyword',value);
										$("#flatchargeclone").val(0).trigger('change');
									} else if(value =='TV') {
										$("#paymenttouch").val(0).trigger('change');
									} else {
										if(value =='HM-PI') {
											$('.hpiecefinaldiv').removeClass('hidedisplay');
											setzero(['hpiecefinal']);
										} else if(value =='HM-FT') {
											$('.hflatfinaldiv').removeClass('hidedisplay');
											setzero(['hflatfinal']);
										} else if(value =='CC-FT') {
											$('.certfinaldiv').removeClass('hidedisplay');
											setzero(['certfinal']);
										}
									}
								} else {
									var res=value.split(":");
									var rescalc = res[0].split('.');
									var chargecalccheck = '';
									if(chargecalculation == 1) { // Max based on company setting
										chargecalccheck = 'MAX';
									} else if(chargecalculation == 2) {  // Min based on company setting
										chargecalccheck = 'MIN';
									}
									if(chargecalccheck == rescalc[1]) { // check min or max charge value apply
										if(rescalc[0] =='MC-FT' || rescalc[0] == 'MC-GM-N' || rescalc[0] == 'MC-PI' || rescalc[0] == 'MC-GM-G') {
											if(rescalc[0] =='MC-FT') {
												$('#makingchargekeylabel').text('/flat');
											} else if(rescalc[0] =='MC-GM-N' || rescalc[0] == 'MC-GM-G') {
												$('#makingchargekeylabel').text('/gm');
											} else if(rescalc[0] == 'MC-PI') {
												$('#makingchargekeylabel').text('/pcs');
											}
											$("#makingcharge").attr('originalvalue',res[1]);
											$("#makingchargeclone").attr('keyword',rescalc[0]);
											$("#makingchargespan,#makingcharge").attr('keyword',rescalc[0]);
											$('#makingchargeclonelabel').text(rescalc[0]);
											$("#makingchargeclone").val(res[1]).trigger('change');
											var mcharge = 'makingchargespan';
											$("#salesaddform label span[id="+mcharge).text(res[1]);
										} else if(rescalc[0] =='WS-PT-N' || rescalc[0] == 'WS-WT' || rescalc[0] == 'WS-PT-G') {
											if(rescalc[0] =='WS-PT-N') {
												$('#wastagekeylabel').text('%Nt');
											} else if(rescalc[0] =='WS-WT') {
												$('#wastagekeylabel').text('/flat');
											} else if(rescalc[0] =='WS-PT-G') {
												$('#wastagekeylabel').text('%Gr');
											}  
											$("#wastageclone").attr('keyword',rescalc[0]);
											$("#wastagespan,#wastage").attr('keyword',rescalc[0]);
											$('#wastageclonelabel').text(rescalc[0]);
											if(wastagecalctype == 5 || wastagecalctype == 3) {
												$("#wastageclone").val(0);
												$("#paymenttouch").val(res[1]).trigger('change');
												var touch = checknan($('#paymenttouch').val());
												var melting = checknan($('#melting').val());
												var wastagepercent =  parseFloat(parseFloat(touch) - parseFloat(melting)).toFixed(3);
												$("#wastage").attr('originalvalue',wastagepercent);
											} else {
												$("#paymenttouch").val(0);
												$("#wastage").attr('originalvalue',res[1]);
												$("#wastageclone").val(res[1]).trigger('change');
												var wastagecharge = 'wastagespan';
												$("#salesaddform label span[id="+wastagecharge).text(res[1]);
											}
										} else if(rescalc[0] =='FC' || rescalc[0] == 'FC-WT') {
											if(rescalc[0] =='FC') {
												$('#flatchargekeylabel').text('/AMT');
											} else if(rescalc[0] =='FC-WT') {
												$('#flatchargekeylabel').text('/WT');
											}
											$("#flatcharge").attr('originalvalue',res[1]);
											$("#flatchargeclone").attr('keyword',rescalc[0]);
											$("#flatchargespan,#flatcharge").attr('keyword',rescalc[0]);
											$('#flatchargeclonelabel').text(rescalc[0]);
											$("#flatchargeclone").val(res[1]).trigger('change');
											var flatchargespan = 'flatchargespan';
											$("#salesaddform label span[id="+flatchargespan).text(res[1]);
										} else if(rescalc[0] =='TV') {
											   $("#paymenttouch").val(res[1]).trigger('change');
										} else {
											chargekey.push(rescalc[0]); 
											chargevalue.push(res[1]);
											if(rescalc[0] =='HM-PI') {
												$('.hpiecefinaldiv').removeClass('hidedisplay');
												setzero(['hpiecefinal']);
											} else if(rescalc[0] =='HM-FT') {
												$('.hflatfinaldiv').removeClass('hidedisplay');
												setzero(['hflatfinal']);
											} else if(rescalc[0] =='CC-FT') {
												$('.certfinaldiv').removeClass('hidedisplay');
												setzero(['certfinal']);
											}
											$("#productaddonform input[keyword="+rescalc[0]).attr('originalvalue',res[1]);
											$("#productaddonform input[keyword="+rescalc[0]).val(res[1]);
											$("#productaddonform label span[id="+rescalc[0]).text('('+res[1]+')');
										}
								    }
								}
								loadlstcharge(value); // load lst charge
							});
							$('#ckeyword').val(chargekey);
							$('#ckeywordoriginalvalue').val(chargevalue);
							$('.chargedata').trigger("change");	
							
						}
						calculationdone = 1;
					}
				});
			}
		}
	}
}
{ // tax 
	// Tax Grid Load
	function loadtaxcharges(product){
			var taxstatus = $('#taxlabel').text();
			if(taxstatus == "Applicable")
			{
				var salestransactiontypeid=$("#salestransactiontypeid").val();
				if(salestransactiontypeid==11){				
					var taxid = $('#product').find('option:selected').data('taxid');
				}else{	
					var taxid = $('#salestransactiontypeid').find('option:selected').data('taxid');
				}
				var stocktypeid = $('#stocktypeid').val();
				var stocktypetaxable = $('#stocktypeid').find('option:selected').data('stktaxable');
				var taxable = $('#product').find('option:selected').data('taxapplicable');
				taxablestocktypeid = ['11','12','13','14','16','74'];
				taxablesalestransactiontypeid = ['11'];
			}
	}
}	
{// issue/ receipt dd autosetting paymentmethodid
	function autosetissuereceiptdd(field)
	{
		var l = 0;
		var value = $('#'+field+'').val();
		if(value >= 0){
			$('#paymentmethodid').select2('val','3').trigger('change');
		} else if(value < 0){
			$('#paymentmethodid').select2('val','2').trigger('change');
		}
		else
		{
			$('#paymentmethodid').select2('val','').trigger('change');
			 select2ddenabledisable('paymentmethodid','enable');
			 l++;
		}
		if(l==0)
		{
			select2ddenabledisable('paymentmethodid','enable');
		}
	}
}		
{//account
function createnewaccount() {
	var accountype = $('#accounttypeid').val();
	var accountname = $('#newaccountname').val();
	var accountshortname = $('#newaccountshortname').val();
	var mobilenumber = $('#accountmobilenumber').val();
	var accountpannumber = $('#accountpannumber').val();
	var newaccountgroupids = $('#newaccountgroup').val();
	var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
	var accountprimarystate = $('#accountprimarystate').find('option:selected').val();
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var form = $("#accountcreateform").serialize();	
	var amp = '&';
	var datainformation = amp + form;
    $.ajax({
        url: base_url +"Sales/accountcreate",
        data: "datas=" + datainformation+amp+"accountype="+accountype+amp+"newaccountgroupids="+newaccountgroupids+"&accountpannumber="+accountpannumber,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg > 0) {
				alertpopup('New Account Added Successfully');
				$("#accountstateid").val(accountprimarystate);
				resetirfields();
				$('#closeaccount').trigger('click');
				$('#accountid').val(nmsg);
				$('#selaccounttypeid').val(accountype);
				if(mobilenumber != '') {
					newaccountname = accountname+' - '+mobilenumber;
				} else {
					newaccountname = accountname;
				}
				// reset summary data
				$('#accountopeningamountirid,#accountopeningweightirid').val(1);
				$('#spanaccopenamt,#spanacccloseamt,#spanaccopenwt,#spanaccclosewt,#accountadvamt,#accountcreditamt').text(0);
				$('#accountopeningamount,#accountopeningweight').val(0);
				setTimeout(function() {
					$('#s2id_accountid a span:first').text(newaccountname);
					$('#accountid').trigger('change');
					if((transactiontypeid == 21 && stocktypeid == 76) || (transactiontypeid == 29 && stocktypeid == 87)) { // place order
						$('#ordermodeid').select2('val',3).trigger('change');
						$('#ordermodeid-div').addClass('hidedisplay');
					}
				},250);
				select2serachaccountcontent = '';
				select2searchdataempty = 0;
			} else {
				alertpopup(submiterror);
			}
        },
    });
}
//update the account
function updateaccount() {
	var accountype = $('#accounttypeid').val();
	var newaccountid = $('#newaccountid').val();
	var accountname = $('#newaccountname').val();
	var mobilenumber = $('#accountmobilenumber').val();
	var accountpannumber = $('#accountpannumber').val();
	var newaccountgroupids = $('#newaccountgroup').val();
	var accountprimarystate = $('#accountprimarystate').find('option:selected').val();
	var form = $("#accountcreateform").serialize();
	var amp = '&';
	var datainformation = amp + form;
    $.ajax({
        url: base_url +"Sales/accountupdate",
        data: "datas=" + datainformation+amp+"accountype="+accountype+amp+"newaccountgroupids="+newaccountgroupids+"&newaccountid="+newaccountid+"&accountpannumber="+accountpannumber,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg > 0) {
				alertpopup('Account Updated Successfully');
				$("#accountstateid").val(accountprimarystate);
				$('#closeaccount').trigger('click');
				$('#accountid').val(nmsg);
				$('#selaccounttypeid').val(accountype);
				if(mobilenumber != '') {
					newaccountname = accountname+' - '+mobilenumber;
				} else {
					newaccountname = accountname;
				}
				setTimeout(function() {
					$('#s2id_accountid a span:first').text(newaccountname);
				},250);
            } else {
				alertpopup(submiterror);
			}
        },
    });
}
//load the accounts with groupings
function appendaccount(id)
{
	$('#'+id+'').empty();
	$.ajax({
		url:base_url+'Sales/loadaccount',
		dataType:'json',
		async:false,
		success: function(data) {
		if((data.fail) == 'FAILED') {
		} else {
				var newddappend="";
				var newlength=data.length;
				prev = ' ';
				for(var m=0;m < newlength;m++){
					var cur = data[m]['accounttypeid'];
					if(prev != cur ) {
						if(prev != " ") {
							newddappend += "</optgroup>";
						}
						newddappend += "<optgroup  label='"+data[m]['accounttypename']+"' id='"+data[m]['accounttypeid']+"'>";
						prev = data[m]['accounttypeid'];
					}
					newddappend += "<option data-accounttype ='"+data[m]['accounttypeid']+"'  value ='"+data[m]['accountid']+"'>"+data[m]['accountname']+'-'+data[m]['mobilenumber']+"</option>";
				}
				//after run
				$('#'+id+'').append(newddappend);
		}    
		},
	});
}

}//identify the bill balance of the accounts

{// functions
	function checknan(output){
		if(isNaN(output)){
			output = 0;
		}
		return output;
	}
	function getnumber(value){
		var result = checkValue(value);
		if(result == true){
			return value;
		} else {
			return 0;
		}
	}
	function ttbasedstypeshowhide() {
        //$("#stocktypeid").select2('val','').trigger('change');
		//show/hide stocktype options
		var stocktype_arr = $('#salestransactiontypeid').find('option:selected').data('stocktyperelation');
		var stocktype_arr = stocktype_arr.toString();		
		if(checkValue(stocktype_arr) == true) {	
			$("#stocktypeid option").addClass("ddhidedisplay");
			$("#stocktypeid optgroup").addClass("ddhidedisplay");
			$("#stocktypeid option").prop('disabled',true);
			var stocktype_array = stocktype_arr.split(",");
			for(var opi =0;opi < stocktype_array.length;opi++) {
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").closest('optgroup').removeClass("ddhidedisplay");
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").removeClass("ddhidedisplay");
				$("#stocktypeid option[value='"+stocktype_array[opi]+"']").prop('disabled',false);
			}
		} else {
			$("#stocktypeid option").removeClass("ddhidedisplay");
			$("#stocktypeid optgroup").removeClass("ddhidedisplay");
			$("#stocktypeid option").prop('disabled',false);
		}
		$("#stocktypeid option:enabled").closest('optgroup').removeClass("ddhidedisplay");	
	}
	function transactionmanageidshowhide() {
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected');
		$("#transactionmanageid option").addClass("ddhidedisplay");
		$("#transactionmanageid optgroup").addClass("ddhidedisplay");
		$("#transactionmanageid option").prop('disabled',true);
		$("#transactionmanageid option[data-salestransactiontypeid='"+salestransactiontypeid+"']").closest('optgroup').removeClass("ddhidedisplay");
		$("#transactionmanageid option[data-salestransactiontypeid='"+salestransactiontypeid+"']").removeClass("ddhidedisplay");
		$("#transactionmanageid option[data-salestransactiontypeid='"+salestransactiontypeid+"']").prop('disabled',false);
		var transactionmanageid = $('#salestransactiontypeid').find('option:selected').data('transactionmanageid');
		$('#transactionmanageid').select2('val',transactionmanageid).trigger('change');
	}
	function puritychangesetrate() {
		var metalid = $('#purity').find('option:selected').data('metalid');
		var transaction = $('#salestransactiontypeid').val();
		var stocktype = $("#stocktypeid").find('option:selected').val();
		var ratearray = $('#ratearray').val();
		if(ratearray  != '') {
			if(transaction == 11 || transaction == 16 || transaction == 20) { //sales & rough estimate & take order
				if(salesrate == 1) {		
					var purity = $('#purity').val();
				} else {
				  var metalresult = $.parseJSON(metalpurityarrayjson);
				  var purity =	metalresult[metalid];
				}
			} else if(transaction == 9 || transaction == 21 || transaction == 22 || transaction == 23 || transaction == 18) { // purchase payments appout
				if(comppurchaserate == 1) {
					var purity = $('#purity').val();
				} else {
					var metalresult = $.parseJSON(metalpurityarrayjson);
					var purity = metalresult[metalid];
				}
			} else {
				var purity = $('#purity').val();
			}
			var result = $.parseJSON(ratearray);
			var rate = result[purity];
			if(stocktype == 86 || stocktype == 88) {
				$('#ratepergram').val(0);
			} else if(retrievetagstatus == 0) {
				$('#ratepergram').val(rate);
			}
		}
	}
	function banknametoken(){
	//get bank name while load page
	 bankname = [];
	 $.ajax({
        url: base_url + "Sales/banknameload",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) 
        { 
			result = data; 
			var i=0;
			$.each(data, function(index) {
			bankname[i]=data[index];
			i++;
			});
        },
    });
 
	$("#bankname").select2({
			tags: bankname,
			tokenSeparators: [',', ' '],
			maximumSelectionSize: 1,
			tokenizer: function(input, selection, callback) {

				// no comma no need to tokenize
				if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
					return;

				var parts = input.split(/,| /);
				for (var i = 0; i < parts.length; i++) {
					var part = parts[i];
					part = part.trim();

					callback({id:part,text:part});
				}
			}
		});		
	}
	function compybasednetmccalc(){
		$('#netwtcalctype').select2('val',$("#netwtcalid").val()).trigger('change');
	}
	// reduce textbox size 
	function generateiconafterload(fieldid,clickevnid,iconclass)
	{
		if($("#hiddenstoneyesno").val()==1){	
		$("#"+fieldid+"").css('display','inline').css('width','90%');	
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="'+iconclass+'" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
		$('label[for="'+fieldid+'"]').css('width','90%');
		}
	}
	function generatecalciconafterload(fieldid,clickevnid,iconclass)
	{
		$("#"+fieldid+"").css('display','inline').css('width','90%');	
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="'+iconclass+'" id="'+clickevnid+'" style="position:absolute;top:10px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
		$('label[for="'+fieldid+'"]').css('width','90%');
	}	
	function rateupdate(){
		var purity=$('#purityrate').find('option:selected').val();
		var rate = $('#itemrate').val();
		var result = $.parseJSON($("#ratearray").val());
		result[purity] = rate;
		$("#ratearray").val(JSON.stringify(result));
		var salesdate = $('#salesdate').val();
		var amp = '&';
		$.ajax({
			url: base_url +"Sales/updateratebasedpurity",
			data: "purity="+purity+amp+"rate="+rate+amp+"salesdate="+salesdate,
			type: "POST",
			success: function(msg) {
				if(msg == 'SUCCESS') {
					$('#salesrateoverlay').fadeOut();
				} else {
					$('#salesrateoverlay').fadeOut();
					alertpopup('There is something problem, please try again.');
				}
			}
		});
	}
	function getratebasedonpurity(){
		var purity = $('#purityrate').val();
		var result = $.parseJSON($("#ratearray").val());
		$('#itemrate').val(result[purity]);
	}
	function puritysetbasedonproduct() {
		var purityid = $.trim($("#product").find('option:selected').data('purityid'));
		if(purityid == '' || purityid == 'null' || purityid == 1) {
			purity_change_status = 1;  
			$('#purity').val('').trigger('change');
		} else {
			purityid=purityid.toString();
			purityshowhide('purity',purityid);
			if (purityid.indexOf(',') > -1) { 
				var dataarray = purityid.split(",");
				$('#purity').select2('val','');
				//$('#purity').val(dataarray[0]).trigger('change');
			} else {
				if(purityid != 1){
				$('#purity').val(purityid).trigger('change');
				}
			}
		}
	}
	function paymentpuritysetbasedonproduct() {
		$('#paymentpurity-div').show();
		var purityid=$.trim($("#paymentproduct").find('option:selected').data('purityid'));
		if(purityid == '' || purityid == 'null' || purityid == 1) {
			purity_change_status = 1;  
			$('#paymentpurity').val('').trigger('change');
		}else{
			purityid=purityid.toString();
			purityshowhide('paymentpurity',purityid);
			if (purityid.indexOf(',') > -1) { 
				var dataarray = purityid.split(",");
				$('#paymentpurity').val(dataarray[0]).trigger('change');
			} else { 
				if(purityid != 1){
					$('#paymentpurity').val(purityid).trigger('change');
					//$('#paymentpurity-div').hide();
				}
			}
		}
	}
	function tagerrorresetandalert(tagerrordisp){	
		alertpopup(tagerrordisp);
		$('#itemtagnumber').val('');
		$('#stocktypeid').trigger('change');
	}
	function tagerrorresetandalertrfid(tagerrordisp){	
		alertpopup(tagerrordisp);
		$('#rfidtagnumber').val('');
		$('#stocktypeid').trigger('change');
	}
}

{//chit related works
	function chitbookdetails(chitbookno,triggerfrom) {
		var allchitbookno = $.trim(getgridcolcolumnvalue('saledetailsgrid','','chitbookno'));
		var alleditchitbookno = '';
		if(salesdetailentryeditstatus == 1) {
			var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
			var alleditchitbookno = getgridcolvalue('saledetailsgrid',selectedrow,'chitbookno','');
		}
		setzero(['grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge','chitamount','totalpaidamount']);
		$('#chitcancelaccountname').val('');
		$('#chittotalamount').val('0');
		$('#chitcancelaccountid').val('');
		$('#chitlastentrydate').val('');
		$('#amountentered').val(''); 
		if(checkValue(chitbookno) == true ) {
			$.ajax({
				url:base_url+"Sales/retrievechitbookdetails",
				data:{chitbookno:chitbookno,allchitbookno:allchitbookno,alleditchitbookno:alleditchitbookno,salesdetailentryeditstatus:salesdetailentryeditstatus,chitbooktypeid:chitbooktypeid},
				dataType:'json',
				async:false,
				success :function(data) {
					if(data.output == 'NO') {
						var chitbookerrordisp = "Chitbook Nos : "+chitbookno+" is invalid,enter correct chitbook number";
						chitbooknoerrorresetandalert(chitbookerrordisp);
						if(triggerfrom == 1) {
							$('#chitbooknocreate').hide();
						}
					} else {
						if(data.invalidchit != '') {
							if(data.validbooks == 'YES') {
								$('#totalpaidamount').focus(); 
								var a = 'Total '+data.totalbooks+' is active and data is fetched. Apart From these'+data.invalidchit;
								alertpopup(a);
							} else {
								if(triggerfrom == 1) {
									$('#chitbooknocreate').hide();
								}
								chitbooknoerrorresetandalert(data.invalidchit);
							}
						}
						if(data.validbooks == 'YES') {
							$('#totalpaidamount').focus(); 
							$('#chitbookno').val(data.chitbookno);
							var a = 'Total '+data.totalbooks+' Chitbooks is active and data is fetched.';
							alertpopup(a);
							var elementsname=['chitbookname','chitamount','totalpaidamount','chitgram'];
							var txtboxname = elementsname;
							var dropdowns = [''];
							textboxsetvaluenew(txtboxname,txtboxname,data,dropdowns);
							$("#chitamount").trigger('keyup');
							Materialize.updateTextFields();
							if(triggerfrom == 1) {
								$('#chitbooknocreate').hide();
							}
						}
					}
				},
			});
		}
	}
	function chitbooknoerrorresetandalert(chitbookerrordisp){	
		alertpopup(chitbookerrordisp);
		$('#chitbookno').val('');
		$('#chitbookno').focus();
	}
}
{// ezee tap
	function getaccountnotificationdetails(accountid)
	{
		$.ajax({
			url: base_url+"Sales/getaccountnotificationdetails",
			data:{accountid:accountid},
			dataType:'json',
			async:false,
			success: function(data) { 
				$("#ezeaccmobileno").val(data.mobilenumber);
				$("#ezeaccemail").val(data.email);
			},
		});
	}
	function checkezetaportstatus(handleData)
	{ 
		$.ajax({
			url: base_url+"Sales/checkezetaportstatus",
			data:{},
			success: function(result) { 
				handleData(result);
			},
		});
	}	
}
function getitemtagbasedstoneentry() {
	var tagnumber=$("#tagnumber").val(); 
	if($("#accountid").val() == ''){
		var accid=1;
	}else{
		var accid=$("#accountid").val();
	}
	
	if(tagnumber!=''){
		$.ajax({
				url:base_url+"sales/stoneentryretrive_stone?tagnumber="+tagnumber+"&accid="+accid, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{	
					var stonedata = data['stoneentry'];
					$("#hiddenstonedetail").val(JSON.stringify(data['stonedetail']));
					$("#hiddenstonedata").val(stonedata);
					loadinlinegriddata('stoneentrygrid',data['stonedetail'].rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('stoneentrygrid');
					var hideprodgridcol = ['stonegroupid','stoneid','totalcaratwt','totalnetwt'];
					gridfieldhide('stoneentrygrid',hideprodgridcol);
					getstonegridsummary();
					netwtcalbasedoncalctype();
					$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
				}
		});
	}
	/* 	if(tagnumber!=''){
		$.ajax({
				
				url:base_url+"sales/stoneentryretrive?tagnumber="+tagnumber+"&accid="+accid, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{	
					if(data == ''){
						
					}else{
						loadinlinegriddata('stoneentrygrid',data.rows,'json');
							/* data row select event */
							/*datarowselectevt();
							/* column resize */
							/*columnresize('stoneentrygrid');
							var hideprodgridcol = ['stonegroupid','stoneid','totalcaratwt','totalnetwt'];
							gridfieldhide('stoneentrygrid',hideprodgridcol);
							getstonegridsummary();
							netwtcalbasedoncalctype();
					}
				}
		});
		} */	
}
function netwtcalbasedoncalctype()
{ 
	var transactiontype=$("#salestransactiontypeid").find('option:selected').val();
	if(transactiontype!=13) {
		var accid=$("#accountid").val();
		var purityid=$("#purity").val();	
		if(accid!='' && purityid!='') { 	
			// Based on netwtcalctype,we will calculate the val of Net Wt
			var netwtcalctype=$("#netwtcalctype").find('option:selected').val();
			var totnetwt='';
			var totstoneamt='';
			
			var totcswt=$("#totcswt").html();
			var totpearlwt=$("#totpearlwt").html();
			var totdiawt=$("#totdiawt").html();
			if(netwtcalctype==2) //G.wt - C.S - P
			{
				totnetwt=(parseFloat(totcswt)+parseFloat(totpearlwt)).toFixed(round);
				totstoneamt=$("#totamt").html();
				//totstoneamt=$("#totcsamt").html();
			}
			else if(netwtcalctype==3)  // G.wt - D - C.S - P
			{
				totnetwt=parseFloat($("#totwt").html()).toFixed(round);
				totstoneamt=$("#totamt").html();
			}
			else if(netwtcalctype==4) // G.wt - D
			{
				totnetwt=parseFloat(totdiawt).toFixed(round);
				totstoneamt=$("#totamt").html();
			}
			else if(netwtcalctype==5) // G.wt
			{
				totnetwt=0;
				totstoneamt=$("#totamt").html();
			}
			else if(netwtcalctype==6) // G.wt - C.S => Stone amount = Total stone amount - stone amount [only reduced stone type weight] 
			{	
				totnetwt=(parseFloat($("#totwt").html()) - parseFloat($("#totcswt").html())).toFixed(round);
				totstoneamt=$("#totamt").html();
			}
			$("#stoneweight").attr('readonly', false);
			//$("#stoneweight").val(totnetwt).trigger('change');
			$("#stoneweight").val(totnetwt);
			$("#stonecharge").val(totstoneamt).trigger('change');
			$("#stoneweight").attr('readonly', true);
			$("#stoneweight").trigger('blur');
			$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
		}
	} else {
		$("#stonecharge").val('');
	}
}
function disblethelotfields() {
	var lottypeid= getgridcolcolumnvalue('salesinnergrid','','lottypeid');
	var purchasemode= getgridcolcolumnvalue('salesinnergrid','','purchasemodeid');
	if(lottypeid.length != 0){
		for(var i=0;i<lottypeid.length;i++) {
			if(lottypeid[i] == 2 || lottypeid[i] == 3) {
				$("#lottypeid").prop('readonly',true);
				$("#lottypeid").select2('val',lottypeid[i]);
			} else {
				$("#lottypeid").prop('readonly','');
				$("#lottypeid").select2('val',3);
			}
		}
		for(var i=0;i<purchasemode.length;i++) {
			if(purchasemode[i] == 2 || purchasemode[i] == 3) {
				$("#purchasemode").prop('readonly',true);
				$("#purchasemode").select2('val',purchasemode[i]);
			} else {
				$("#purchasemode").prop('readonly','');
				$("#purchasemode").select2('val',2);
			}
		}
	} else {
		$("#lottypeid,#purchasemode").prop('readonly','');
		$("#lottypeid").select2('val',3);
		$("#purchasemode").select2('val',2);
	}
}
function billdataretrive(datarowid) {
	approvalnoeditstatus = 1;
	takeorderoldstatus = 0;
	takeorderreceivestatus = 0;
	takeorderstatus = 0;
	var elementsname=['40','salesdate','employeeid','salespersonid','employeename','salestransactiontypeid','accountid','balancedescription','balanceduedate','printtemplateid','cashcounter','taxcategory','discounttypeid','discountpercent','singlediscounttotal','transactionmodeid','mainordernumberid','journalbilldate','journalbillno','billsumarydiscountlimit','presumarydiscountamount','discountempid','creditno','estimatearray','summarytaxgriddata','issuereceiptid','sumarydiscountamount','orderadvamtsummaryhidden','orderadvamtsummary','sumarytaxamount','salesdetailcount','orderadvcreditno','liveadvanceclose','orderadvadjustamthidden','creditoverlayhidden','creditoverlayvaluehidden','loadbilltype','credittotalamount','credittotalpurewt','billrefno','ratecuttypeid']; 
	//form field first focus
	firstfieldfocus();
	$.ajax({
		url:base_url+"Sales/billretrive",
        data:{primaryid:datarowid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) {
		    var txtboxname = elementsname;
			var dropdowns = ['9','salespersonid','salestransactiontypeid','printtemplateid','cashcounter','discounttypeid','transactionmodeid','loadbilltype','taxcategory','ratecuttypeid'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			Materialize.updateTextFields();
			billeditstatus = 1;
			billeditstatusclose = 1;
			discountamtwithsign = 0;
			discountoldamtwithsign = 0;
			$('#salestransactiontypeid').trigger('change');
			{// setting data
				var salestransactiontype = data.salestransactiontypeid;
				$('.oradvamtspan').text('('+data.orderadvamtsummaryhidden+')');
				$('#primarydataid').val(data.salesid);
				estimatearray.push(data.estimatearray);	
				$('#generatesalesid').val(datarowid);
				$('#generatesalesnumber').val(data.salesnumber);
				$("#ratearray").val(JSON.stringify(data.purityid)); 
				$('#transactionmodeid').select2('val',data.transactionmodeid).trigger('change');
				$('#accountid').val(data.accountid);
				$('#editaccount').show();
				billleveltaxautoremoval = data.billleveltaxautoremoval;
				adjustmentamountkeypressed = data.adjustmentamountkeypressed;
				$("#ratecuttypeid").select2('val',data.ratecuttypeid).trigger('change');
				$('#roundvalue').val(data.roundvalue);
				$('#roundwholevalue').val(data.roundwholevalue);
				
			}
			{// tax discount
				retrievealltaxdetailinformation();
				var salestaxtypeid = $("#taxtypeid").val();
				if(salestaxtypeid == '3') {
					$('#hiddentaxcategoryid').val(data.taxcategory);
					$('#taxcategory').val(data.taxcategory).trigger('change');
				}
				var sdiscounttypeid = $('#sdiscounttypeid').val();
				if(sdiscounttypeid == 3) { // bill level discount
					$('#discountpercent').val(data.discountpercent);
					$('#singlediscounttotal').val(data.singlediscounttotal);
					setTimeout(function(){
						$("#discountcalname").text('(' +parseFloat(data.presumarydiscountamount).toFixed(roundamount)+')');
					},10);
					$("#presumarydiscountamount").val('(' +parseFloat(data.presumarydiscountamount).toFixed(roundamount)+')');
					$('#billdiscamt,#billsumarydiscountlimit').val(parseFloat(data.billdiscamt).toFixed(roundamount));
					$('#sumarydiscountamount').val(parseFloat(data.sumarydiscountamount).toFixed(roundamount)); 
				}
			}
			if(billamountcalculationtrigger == 1) {
				discountamtwithsign = parseFloat(data.sumarydiscountamount).toFixed(roundamount);
				discountoldamtwithsign = parseFloat(data.summaryolddiscountamount).toFixed(roundamount);
			}
			cleargriddata('saledetailsgrid');
			billeditsalesdetails(data.salesid); // salesgrid data loading
			compybasednetmccalc();
			
			$('#accounttypeid').select2('val',data.accounttypeid);
			$('#selaccounttypeid').val(data.accounttypeid);
			
			{ // type based settings
				if(salestransactiontype == 20) { // takeorder
					takeordertypehideshow();
				} else if(salestransactiontype == 9) { //purchase
					disblethelotfields();
					if($('#weightcheck').val() == 1) {
						$('#purchasecheckgrwt').val(data.purchasecheckgrwt);
					}
				} else if(salestransactiontype == 23 || salestransactiontype == 22) {
					$('#salesdetailsubmit').hide();
				}
				if(salestransactiontype == 18 || salestransactiontype == 9 || salestransactiontype == 20) {
					$('#stocktypeid').select2('val',data.mainstocktypeid).trigger('change');
					if(salestransactiontype == 20) {
						orderstocktypehideshow(data.mainstocktypeid);
					} else if(salestransactiontype == 18) {
						$('#billrefnooverlaytbox').val(data.billrefno);
					} else if(salestransactiontype == 9) {
						$('#billrefno').val(data.billrefno);
					}
				} else if(salestransactiontype == 22 || salestransactiontype == 23) {
					$('#paymentstocktypeid').select2('val',data.mainstocktypeid).trigger('change');
					$('#paymentirtypeid').select2('val',data.paymentirtypeid);
					var manualstatus = accountmanualstatus();
					if(manualstatus == 0) {
						$('#issuereceipticon').hide();
						$('#creditno').css({'width':'100%'});
						setTimeout(function() {
						$('#creditno').val(data.creditno);
						},10);
					}
				} else if(salestransactiontype == 11) {
					$('#stocktypeid').select2('val',data.mainstocktypeid).trigger('change');
					deliveryorderstocktypehideshow(data.mainstocktypeid);
				} else if(salestransactiontype == 21) {
					$('#stocktypeid').select2('val','76').trigger('change');
				} else if(salestransactiontype == 27) {
					$('#stocktypeid').select2('val','85').trigger('change');
				} else if(salestransactiontype == 25) {
					$('#paymentstocktypeid').trigger('change');
				}
			}
			
			if(data.mobilenumber != '') {	
				$('#s2id_accountid a span:first').text(data.accountname+'-'+data.mobilenumber);
			} else {
				$('#s2id_accountid a span:first').text(data.accountname);
			}
			
			// pi/pr set summary 
		    if(salestransactiontype == 22 || salestransactiontype == 23) {
				if($('#customerwise').val() == 1 && data.accounttypeid == 6 || $('#vendorwise').val() == 1 && data.accounttypeid == 16) {
					$('#credittotalamount').val(data.credittotalamount);
					$('#credittotalpurewt').val(data.credittotalpurewt);
				}
				setcreditsummary();
				setcreditnoval();
			}
			$('#accountid').val(data.accountid);
			
			// Bill level restriction metal
			billlevelrestrictmetalid = 1;
			billlevelrestrictoldmetalid = 1;
			billlevelrestrictreturnmetalid = 1;
			if(salestransactiontype == 11 || salestransactiontype == 16) {
				if(metalarraydetails != '' && data.salesmetalid != 1) {
					billlevelrestrictmetalid = data.salesmetalid;
					metalstatus = 1;
				} else {
					billlevelrestrictmetalid = 1;
					metalstatus = 0;
				}
				if(metaloldarraydetails != '' && data.oldpurchasemetalid != 1) {
					billlevelrestrictoldmetalid = data.oldpurchasemetalid;
					metaloldstatus = 1;
				} else {
					billlevelrestrictoldmetalid = 1;
					metaloldstatus = 0;
				}
				if(metalreturnarraydetails != '' && data.salesreturnmetalid != 1) {
					billlevelrestrictreturnmetalid = data.salesreturnmetalid;
					metalreturnstatus = 1;
				} else {
					billlevelrestrictreturnmetalid = 1;
					metalreturnstatus = 0;
				}
			}
		}		
	});
}
// mainsave save
function salenumbergenerate(mainprintemplateid) {
	var salesaccount = $("#salescreateform").serialize();
	var amp = '&';
	var datainformation = amp + salesaccount;
	//salesdetail inner grid
	var criteriacnt = $('#saledetailsgrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriagriddata = JSON.stringify(griddata);
	var result = $.parseJSON($("#ratearray").val());
	var purityid = [];
	var purityrate = [];
	var summaryolddiscountamount = 0;
	$.each(result, function (index, value) {
	    purityid.push(index);
		purityrate.push(value);
	});
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	if(salestransactiontypeid == 18 || salestransactiontypeid == 9 || salestransactiontypeid == 20 || salestransactiontypeid == 11) {
		var mainstocktypeid = $('#stocktypeid').find('option:selected').val();
	} else if(salestransactiontypeid == 22 || salestransactiontypeid == 23) {
		var mainstocktypeid = $('#paymentstocktypeid').find('option:selected').val();
	} else {
		var mainstocktypeid = 1;
	}
	var printtemplateid = mainprintemplateid;
	var salespersonid = $('#salespersonid').find('option:selected').val();
	var cashcounter = $('#cashcounter').find('option:selected').val();
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	var accountid = $('#accountid').val();
	var taxtypeid = $('#salestransactiontypeid').find('option:selected').data('taxtypeid');
	if(taxtypeid == 2) {
		var sumarytaxamount = 0;
	} else if(taxtypeid == 3) {
		var sumarytaxamount = $('#sumarytaxamount').val();
	}
	if((wastagecalctype == 1 || wastagecalctype == 2 || wastagecalctype == 4) && (transactionmodeid == 2 || transactionmodeid == 4)) {
		getsalesdetailtaxcalc(); // Summary tax calculation
	}
	var sumarytaxamount = $('#sumarytaxamount').val();
	var sdiscounttypeid = $('#sdiscounttypeid').val();
	if(sdiscounttypeid == 1) {
		var sumarydiscountamount = 0;
		var discountformdata = '';
	} else {
		var sumarydiscountamount = $('#sumarydiscountamount').val();
		var discountformdata = $('#discountdata').val();
	}
	if(billamountcalculationtrigger == 1) {
		var sumarydiscountamount = discountamtwithsign;
		var summaryolddiscountamount = discountoldamtwithsign;
	}
	if(sdiscounttypeid == 2) {
		var sumarydiscountlimit = $('#sumarydiscountlimit').val();
	} else if(sdiscounttypeid == 3) {
		var sumarydiscountlimit = $('#billsumarydiscountlimit').val();
	}
	var presumarydiscountamount = $('#presumarydiscountamount').val();
	var sumarynetamount = $('#sumarynetamount').val(); 
	var taxgriddata = $('#taxgriddata').val();
	var taxcategoryid = $('#taxcategory').find('option:selected').val();
	var gsttaxmodeid = $('#taxcategory').find('option:selected').data('gsttaxmode');
	var billnetamount = $('#billnetamount').val();
	var billnetwt = $('#billnetwt').val();
	var billnetamountwithsign = $('#billnetamountwithsign').val();
	var billnetwtwithsign = $('#billnetwtwithsign').val();
	var sumarypendingamount = $('#sumarypendingamount').val();
	var estimatearray = $('#estimatearray').val();
	var paymentirtypeid=$('#paymentirtypeid').find('option:selected').val();
	var creditno = $('#creditno').val();
	var generatesalesid = $('#generatesalesid').val();
	var balancedescription = $('#balancedescription').val();
	var prepaymenttotalamount = $('#prepaymenttotalamount').val();
	var roundvalue = $('#roundvalue').val();
	var summarytaxgriddata = $('#summarytaxgriddata').val();
	var issuereceiptid = $('#issuereceiptid').val();
	var wtissuereceiptid = $('#wtissuereceiptid').val();
	var salesdate = $('#salesdate').val();
	var oldjewelamtsummary = $('#oldjewelamtsummary').val();
	var sumarypaidamount = $('#sumarypaidamount').val();
	var sumaryissueamount = $('#sumaryissueamount').val();
	var spanissuepurewthidden = $('#spanissuepurewthidden').val();
	var spanpaidpurewthidden = $('#spanpaidpurewthidden').val();
	var ordermodeid = $('#ordermodeid').find('option:selected').val();
	var mainordernumberid = $('#mainordernumberid').val();
	var orderadvanceadjustamt = $('#orderadvamtsummary').val();
	var orderadvanceamt = $('#orderadvamtsummaryhidden').val();
	var orderadvanceno = $('#orderadvcreditno').val();
	var balpurewthidden = $('#balpurewthidden').val();
	var creditoverlayhidden = $('#creditoverlayhidden').val();
	var creditoverlayvaluehidden = $('#creditoverlayvaluehidden').val();
	var credittotalamount = $('#credittotalamount').val();
	var credittotalpurewt = $('#credittotalpurewt').val();
	var billrefno = $('#billrefno').val();
	var loadbilltype = $('#loadbilltype').find('option:selected').val();
	var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
	var purchasewtcheckgrwt = $('#purchasecheckgrwt').val();
	var purchasewtcheckerrorwt = $('#purchasecheckerrorwt').val();
	var accountstateid = $('#accountstateid').val();
	var serialnumbermastername = $('#salestransactiontypeid').find('option:selected').data('serialnumbermastername');
	/** Advance Concept, Receipt Concept, Balance Concept **/
	var advanceserialno = $('#salestransactiontypeid').find('option:selected').data('advanceserialno');
	var advanceprintid = $('#salestransactiontypeid').find('option:selected').data('advanceprintid');
	var receiptserialno = $('#salestransactiontypeid').find('option:selected').data('receiptserialno');
	var receiptprintid = $('#salestransactiontypeid').find('option:selected').data('receiptprintid');
	var issueserialno = $('#salestransactiontypeid').find('option:selected').data('issueserialno');
	var issueprintid = $('#salestransactiontypeid').find('option:selected').data('issueprintid');
	var balanceserialno = $('#salestransactiontypeid').find('option:selected').data('balanceserialno');
	var balanceprintid = $('#salestransactiontypeid').find('option:selected').data('balanceprintid');
	/** Class Id **/
	var transactionmanageid = $('#salestransactiontypeid').find('option:selected').data('transactionmanageid');
	var gridcountdetailsofoldpurchase = getgridcolvaluewithcontition('saledetailsgrid','','stocktypeid','count','stocktypeid','19');
	var gridcountdetailsofsalesreturn = getgridcolvaluewithcontition('saledetailsgrid','','stocktypeid','count','stocktypeid','20');
	var gridcountdetailsofsalestrantype = getsalesstocktypegridallrowids('saledetailsgrid','all').length;
	gridcountdetailsofsalestrantype = typeof gridcountdetailsofsalestrantype == 'undefined' ? '0' : gridcountdetailsofsalestrantype;
	var summaryproditemrate = getgridcolvaluewithcontition('saledetailsgrid','','proditemrate','sum','stocktypeid','17');
	var summarylstchargecaratwt = getgridcolvaluewithcontition('saledetailsgrid','','itemcaratweight','sum','stocktypeid','12');
	var summarysalesreturnroundvalue = $('#summarysalesreturnroundvalue').val();
	var metalserialnumberdetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metalserialnumberdetails'));
	var metalprinttempids = converttoarray($('#salestransactiontypeid').find('option:selected').data('metalprinttempids'));
	var metaloldserialnumberdetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metaloldserialnumberdetails'));
	var metalreturnserialnumberdetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('metalreturnserialnumberdetails'));
	var repairserialnumberdetails = converttoarray($('#salestransactiontypeid').find('option:selected').data('repairserialnumberdetails'));
	var repairmetalprintid = converttoarray($('#salestransactiontypeid').find('option:selected').data('repairmetalprintid'));
	var defoldpurserialmaster = $('#salestransactiontypeid').find('option:selected').data('defoldpurserialmaster');
	var defsalesreturnserialmaster = $('#salestransactiontypeid').find('option:selected').data('defsalesreturnserialmaster');
	var summaryitemscount = totalitemscountinsalesdetailgrid('saledetailsgrid').length;
	var repairitemscount = getpuritybasedmetalidgridcondition('saledetailsgrid','checkrepair','count');
	$('#salesbalanceoverlay').fadeOut();
	var manualstatus = accountmanualstatus();
	var oldpurchasecredittype = 1;
	if(gridcountdetailsofoldpurchase > 0 && gridcountdetailsofsalesreturn == 0 && gridcountdetailsofsalestrantype == 0 && $('#oldpurchasecredittype').val() == 1 && (sumarypendingamount > 0 || sumarypendingamount < 0)) {
		oldpurchasecredittype = $('#oldpurchasecredittypeid').find('option:selected').val();
	}
	$("#processoverlay").show();
	$.ajax({
		url:base_url+"Sales/salenumbergenerate",
		data: "datas=" + datainformation+amp+"salesdetailgriddata="+criteriagriddata+amp+"count="+criteriacnt+amp+"purityid="+purityid+amp+"purityrate="+purityrate+amp+"taxtypeid="+taxtypeid+amp+"sumarytaxamount="+sumarytaxamount+amp+"sdiscounttypeid="+sdiscounttypeid+amp+"sumarydiscountamount="+sumarydiscountamount+amp+"discountformdata="+discountformdata+amp+"sumarydiscountlimit="+sumarydiscountlimit+amp+"presumarydiscountamount="+presumarydiscountamount+amp+"sumarynetamount="+sumarynetamount+amp+"taxgriddata="+taxgriddata+amp+"taxcategoryid="+taxcategoryid+amp+"billnetamount="+billnetamount+amp+"billnetwt="+billnetwt+amp+"gsttaxmodeid="+gsttaxmodeid+amp+"salestransactiontypeid="+salestransactiontypeid+amp+"printtemplateid="+printtemplateid+amp+"salespersonid="+salespersonid+amp+"cashcounter="+cashcounter+amp+"transactionmodeid="+transactionmodeid+amp+"sumarypendingamount="+sumarypendingamount+amp+"estimatearray="+estimatearray+amp+"paymentirtypeid="+paymentirtypeid+amp+"balancedescription="+balancedescription+amp+"generatesalesid="+generatesalesid+amp+"accountid="+accountid+amp+"prepaymenttotalamount="+prepaymenttotalamount+amp+"mainstocktypeid="+mainstocktypeid+amp+"roundvalue="+roundvalue+amp+"summarytaxgriddata="+summarytaxgriddata+amp+"issuereceiptid="+issuereceiptid+amp+"salesdate="+salesdate+amp+"oldjewelamtsummary="+oldjewelamtsummary+amp+"sumarypaidamount="+sumarypaidamount+amp+"ordermodeid="+ordermodeid+amp+"mainordernumberid="+mainordernumberid+amp+"orderadvanceadjustamt="+orderadvanceadjustamt+amp+"orderadvanceamt="+orderadvanceamt+amp+"orderadvanceno="+orderadvanceno+amp+"balpurewthidden="+balpurewthidden+amp+"wtissuereceiptid="+wtissuereceiptid+amp+"creditno="+creditno+amp+"sumaryissueamount="+sumaryissueamount+amp+"creditoverlayhidden="+creditoverlayhidden+amp+"creditoverlayvaluehidden="+creditoverlayvaluehidden+amp+"credittotalamount="+credittotalamount+amp+"credittotalpurewt="+credittotalpurewt+amp+"billleveltaxautoremoval="+billleveltaxautoremoval+amp+"adjustmentamountkeypressed="+adjustmentamountkeypressed+amp+"loadbilltype="+loadbilltype+amp+"billrefno="+billrefno+amp+"ratecuttypeid="+ratecuttypeid+amp+"purchasewtcheckgrwt="+purchasewtcheckgrwt+amp+"purchasewtcheckerrorwt="+purchasewtcheckerrorwt+amp+"manualstatus="+manualstatus+amp+"spanissuepurewthidden="+spanissuepurewthidden+amp+"spanpaidpurewthidden="+spanpaidpurewthidden+amp+"accountstateid="+accountstateid+amp+"serialnumbermastername="+serialnumbermastername+amp+"billnetamountwithsign="+billnetamountwithsign+amp+"billnetwtwithsign="+billnetwtwithsign+amp+"receiveorderconcept="+receiveorderconcept+amp+"summaryproditemrate="+summaryproditemrate+amp+"summarylstchargecaratwt="+summarylstchargecaratwt+amp+"gridcountdetailsofoldpurchase="+gridcountdetailsofoldpurchase+amp+"gridcountdetailsofsalesreturn="+gridcountdetailsofsalesreturn+amp+"gridcountdetailsofsalestrantype="+gridcountdetailsofsalestrantype+amp+"advanceserialno="+advanceserialno+amp+"advanceorbalance="+advanceorbalance+"&advanceprintid="+advanceprintid+"&receiptserialno="+receiptserialno+"&receiptprintid="+receiptprintid+"&issueserialno="+issueserialno+"&issueprintid="+issueprintid+"&balanceserialno="+balanceserialno+"&balanceprintid="+balanceprintid+"&transactionmanageid="+transactionmanageid+"&summarysalesreturnroundvalue="+summarysalesreturnroundvalue+"&billlevelrestrictmetalid="+billlevelrestrictmetalid+"&metalarraydetails="+metalarraydetails+"&metalserialnumberdetails="+metalserialnumberdetails+"&metalprinttempids="+metalprinttempids+"&metaloldarraydetails="+metaloldarraydetails+"&billlevelrestrictoldmetalid="+billlevelrestrictoldmetalid+"&metaloldserialnumberdetails="+metaloldserialnumberdetails+"&metalreturnarraydetails="+metalreturnarraydetails+"&billlevelrestrictreturnmetalid="+billlevelrestrictreturnmetalid+"&metalreturnserialnumberdetails="+metalreturnserialnumberdetails+"&defoldpurserialmaster="+defoldpurserialmaster+"&defsalesreturnserialmaster="+defsalesreturnserialmaster+"&summaryitemscount="+summaryitemscount+"&questionairegenpovendorarray="+questionairegenpovendorarray+"&questionairerecorderadminarray="+questionairerecorderadminarray+"&questionaireapplicable="+questionaireapplicable+"&repairitemscount="+repairitemscount+"&repairserialnumberdetails="+repairserialnumberdetails+"&repairmetalprintid="+repairmetalprintid+"&summaryolddiscountamount="+summaryolddiscountamount+"&oldpurchasecredittype="+oldpurchasecredittype,
        dataType:'json',
		type: "POST",
		async:false,
		beforeSend: function() {
			mainsaveclicksuccess = 1;
		},
		success: function(msg) {
		    if(msg['status'] == 'SUCCESS') {
				oldsalesreturntax = 0;
				billleveltaxautoremoval = 0;
				adjustmentamountkeypressed = 0;
				discountamtwithsign = 0;
				discountoldamtwithsign = 0;
				$("#processoverlay").hide();
				if(salestransactiontypeid != 13) { // weight sales
					if(salestransactiontypeid == 20 && mainstocktypeid == 82 && receiveordertemplateid > 1) {
						printautomate(msg['salesid'],receiveordertemplateid,msg['salesnumber']);
					} else if(salestransactiontypeid == 23 && advanceserialno != '' && advanceorbalance == '2') {
						printautomate(msg['salesid'],advanceprintid,msg['salesnumber']);
					} else if(salestransactiontypeid == 22 && receiptserialno != '' && advanceorbalance == '3') {
						printautomate(msg['salesid'],receiptprintid,msg['salesnumber']);
					} else {
						if(salestransactiontypeid == 11) {
							if(billlevelrestrictmetalid != 1 || billlevelrestrictoldmetalid != 1 || billlevelrestrictreturnmetalid != 1) {
								if(billlevelrestrictmetalid != 1) {
									if(repairitemscount > 0 && repairserialnumberdetails != '') {
										var printtempids = repairmetalprintid[metalarraydetails.indexOf(billlevelrestrictmetalid)];
									} else {
										var printtempids = metalprinttempids[metalarraydetails.indexOf(billlevelrestrictmetalid)];
									}
								} else if(billlevelrestrictoldmetalid != 1) {
									var printtempids = metalprinttempids[metaloldarraydetails.indexOf(billlevelrestrictoldmetalid)];
								} else if(billlevelrestrictreturnmetalid != 1) {
									var printtempids = metalprinttempids[metalreturnarraydetails.indexOf(billlevelrestrictreturnmetalid)];
								}
								printautomate(msg['salesid'],printtempids,msg['salesnumber']);
							} else {
								printautomate(msg['salesid'],mainprintemplateid,msg['salesnumber']);
							}
						} else {
							printautomate(msg['salesid'],mainprintemplateid,msg['salesnumber']);
						}
					}
				}
				var stocktypeid = $('#stocktypeid').val();
				mainsavestatus = 1;
				if(salestransactiontypeid == 11) { // sales
					estimatestatus = 0;
				}else{
					estimatestatus = 1;
				}
				if(salestransactiontypeid == 21) { // place order
					var accountdata = $('#s2id_accountid a span:first').text();
					var res = accountdata.split('-');
					alertpopup('place order converted successfully to '+res[0]);
				} else if(salestransactiontypeid == 20) { // take order
					if(stocktypeid == '82') { // receive order
						$("#s2id_accountid a span:first").text('');
						alertpopup('Place Order convert into Receive Order Successfully');
					}
				}
				$('#formclearicon').trigger("click");
				$('#estimatearray,#discountarray,#discountdata').val('');
				$('#presumarydiscountamount').val(0);
				$('#salestransactiontypeid').select2('val',salestransactiontypeid).trigger('change');
				advanceorbalance = '1';
				mainsaveclicksuccess = 0;
				alertpopup(savealert);
				$('#alerts').fadeOut(250);
				if(salestransactiontypeid == 13) { // weight sales
					$('#itemtagnumber').focus();
				}
				estimatemainsavetrigger = 0;
				if(salestransactiontypeid == 27) { // Generated Place order close form
					cleargriddata('saledetailsgrid');
					addslideup('salesaddformdiv','salesgriddisplay');
					salesrefreshgrid();
					$('#alertsfcloseyes').trigger('click');
				}
			} else {
				mainsaveclicksuccess = 0;
				estimatemainsavetrigger = 0;
				alertpopup('Error Has occured, so kindly delete the current bill if generated using main delete button and then continue. Kindly copy the error from next popup and save it in notepad for further reference to software provider');
				alertpopup(msg['status']);
			}
			$("#accountopeningamount,#accountopeningweight,#wastagelesspercent,#sumarygrossamount,#sumarypendingamount,#sumarynetamount,#sumarybillamount,#sumarysalesretamount,#sumaryoldjewelamount,#sumarytaxamount,#sumarydiscountamount,#finalsumarybillamount,#sumarypaidamount,#sumaryissueamount,#summarysalesreturnroundvalue").val(0);
			$("#discountpercent,#singlediscounttotal,#giftnumber,#giftremark,#giftdenomination").val('0');
			if(salestransactiontypeid == 24) {
				alertpopup('Journal Data stored successfully');
			} else if(salestransactiontypeid == 9) {
				alertpopup('Purchase Data stored successfully');
			}
			if(salestransactiontypeid == 13 || salestransactiontypeid == 24 || (salestransactiontypeid == 16 && $("#estimatedefaultaccid").val() > 1 && estimatedefaultset == 1)) {
				$('#mainheadersubmit').trigger('click');
			}
			retrievealltaxdetailinformation();
			billlevelrestrictmetalid = 1;
			billlevelrestrictoldmetalid = 1;
			billlevelrestrictreturnmetalid = 1;
			metalstatus = 0;
			metaloldstatus = 0;
			metalreturnstatus = 0;
			$("#accountid").select2('focus');
		}
	});
}
function getsalesdetails(salesdetailid){
	var elementsname=['15','stocktypeid','itemtagid','itemtagnumber','product','purity','counter','grossweight','stoneweight','netweight','pieces','ratepergram','hiddenstonedata','ckeyword','ckeywordvalue','ckeywordoriginalvalue'];
	//form field first focus
	firstfieldfocus();
	$.ajax(
	{
		url:base_url+"Sales/getsalesdetails",
         data:{primaryid:salesdetailid},		
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data)
		{ 
		   if(data.output == 'FAIL'){
			   alertpopup('Voucherno is Invalid.Please verify');
		   }else{
		    var txtboxname = elementsname;
			var dropdowns = ['4','stocktypeid','product','purity','counter'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$('#taxdetails').val(data.taxdetails);
			Materialize.updateTextFields();
			$('#generateitemtagid').val(data.itemtagid);
			$('#purity').trigger('change');
			var chargekeyworddata=data.ckeywordvaluedetails.split(",");
		 $.each(chargekeyworddata, function (index, value) {
			 var res=value.split(":");
			 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
			 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).val(res[1]);
			 });
			 var ckeywordoriginalvalue=data.ckeywordoriginalvaluedetails.split(",");
		 $.each(ckeywordoriginalvalue, function (index, value) {
			  var res=value.split(":");
			 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
			 $("#productaddonform .chargedetailsdiv span[id="+res[0]).text(res[1]);
		  });
			   var otherdetailsvalue=data.otherdetailsvalue.split(",");
		 $.each(otherdetailsvalue, function (index, value) {
           	 $("#productaddonform input .extracalc").val(value);
			   });
			    setTimeout(function(){
						retrievestatus=1;
			     $('#additionalcharge').trigger("click");
					},50); 
			    }
		}		 
	});
}
//identify the bill balance of the accounts
function checkbillbalance() {
	var amount = checknan(parseFloat($('#balanceamt').val()));
	var pureweight = checknan(parseFloat($('#balancepurewt').val()));
	if((amount) == 0 && (pureweight) == 0){
		return false;
	} else {
		return true;
	}
}
function discountadd(datarowid,value) {
	$.ajax({
			url: base_url+"Sales/discountadd",
			data:{primaryid:datarowid,discountvalue:value},
			dataType:'text',
			type: "POST",
			async:false,
			success: function(msg) {
				if(msg == 'SUCCESS'){
					alertpopup('Discount updated successfully');
					$("#salesdiscountoverlay").fadeOut();
				}
			},
		});
}
//print template default
function setdefaultprinttemplate(datarowid) {
	$.ajax({
		url:base_url+'sales/salesprinttemplate',
		data:{salesid:datarowid},
		dataType:'json',
		async:false,
		success: function(data) {
			var vid = data.id;
			if(checkValue(vid) == true) {
				$('#pdftemplateprview').select2('val',vid).trigger('change');
				$('#defaultpdftemplateid').val(vid);
			}
		},
	});
}
function printautomate(salesid,printtemplateid,snumber) {
	$('#printpdfid').val(salesid);
	$('#printsnumber').val(snumber);
	$('#pdftemplateprview').select2('val',printtemplateid).trigger('change');
	$('#defaultpdftemplateid').val(printtemplateid);
	$('#pdffilepreviewintab').trigger("click");
}
function getstoneentrydiv(datarowid,salesid) {
	$.ajax({
		url: base_url+"Sales/retrive_sales_stoneentry_div",
		data:{primaryid:datarowid,salesid:salesid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			if(msg == 'FAIL') {
				
			} else {
				loadinlinegriddata('stoneentrygrid',msg.rows,'json');
			   /* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('stoneentrygrid');
				var hideprodgridcol = ['stonegroupid','stoneid','totalcaratwt','totalnetwt'];
				gridfieldhide('stoneentrygrid',hideprodgridcol);
				getstonegridsummary();
			}
		},
	});
}
function gettaxentrydiv(datarowid,salesid) {
	$.ajax({
		url: base_url+"Sales/retrive_sales_taxentry_div",
		data:{primaryid:datarowid,salesid:salesid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			if(msg == 'FAIL') {
				
			} else {
				loadinlinegriddata('taxgrid',msg.rows,'json');
			   /* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('taxgrid');
				var griddataid = 'taxgriddata';
				var tax_category_id = 'taxcategory';
				var tax_json ={}; //declared as object
				tax_json.id = $('#'+tax_category_id+'').val();//categoryid
				tax_json.data = getgridrowsdata('taxgrid');//taxgrid
				$('#'+griddataid+'').val(JSON.stringify(tax_json));
			}
		},
	});
}
function loadapprovalnumber(stktype,accountid) {
	$('#approvalnumber').empty();
	$('#approvalnumber').append($("<option value='1'>Select</option>"));
	$('#approvalnumber').append($("<option value='all'>All</option>"));
	$.ajax({
		url: base_url+"Sales/loadapprovalnumber",
		data:{stocktype:stktype,accountid:accountid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			if(msg != '') {	
				$.each(msg, function(index) {
					$("#approvalnumber")
					.append($("<option></option>")
					.attr("value",msg[index]['data'])
					.attr('data-approvalnumberhidden',msg[index]['data'])
					.text(msg[index]['data']));
				});
			}
		},
	});
}
function setsalesdetails(salesdetailid) {
	elementsname=['19','salesdetailid','referencenumber','itemtagid','itemtagnumber','product','purity','counter','grossweight','stoneweight','netweight','pieces','ratepergram','hiddenstonedata','ckeyword','ckeywordoriginalvalue','wastage','makingcharge','takeordernumber','presalesdetailid'];
	//form field first focus
	$("#processoverlay").show();
	$.ajax({
		url:base_url+"Sales/setsalesdetails",
		data:{primaryid:salesdetailid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) { 
		    var txtboxname = elementsname;
			var dropdowns = ['3','product','purity','counter'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			loadproductaddon();
			var chargeid = $('#product').find('option:selected').data('chargeid');
			var stocktypeid = $("#stocktypeid").find('option:selected').val();
			stocktypeid = typeof stocktypeid == 'undefined' ? '1' : stocktypeid;
			extrachargehideshow(chargeid);
			flatchargehideshow(chargeid);
			makingchargehideshow(chargeid);
			lstchargehideshow(chargeid);
			wastagechargehideshow(chargeid);
			paymenttouchhideshow(chargeid);
			repairchargehideshow(stocktypeid);
			stonechargehideshow();
			sizehideshow(data.product);
			var mcharge = 'makingchargespan';
			$("#salesaddform label span[id="+mcharge).text(data.makingchargespan);
			var wastagecharge = 'wastagespan';
			$("#salesaddform label span[id="+wastagecharge).text(data.wastagespan);
			$('#wastageclone').val(data.wastagespan).trigger('change');
			$('#makingchargeclone').val(data.makingchargespan).trigger('change');
			$('.chargedata').trigger("change");
			$("#processoverlay").hide();
			Materialize.updateTextFields();
			$('#generateitemtagid').val(data.itemtagid);
			if(data.stoneweight != 0) { // without stone
			  getstoneentrydiv(salesdetailid,data.salesid);
			  $('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
			}
		}		
	});
}
function loadtakeordernumber(status) {
	$('#ordernumber').empty();
	$('#ordernumber').append($("<option value='1'>Select</option>"));
	$('#ordernumber').append($("<option value='all' data-ordernumber='all'>All</option>"));
	var accountid = $('#accountid').val();
	var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
	var stocktype = $("#stocktypeid").find('option:selected').val();
	$.ajax({
		url: base_url+"Sales/loadtakeordernumber",
		data:{status:status,accountid:accountid,povendormanagement:povendormanagement,poautomaticvendor:poautomaticvendor,receiveorderconcept:receiveorderconcept,transactiontype:transactiontype,stocktype:stocktype,placeorderid:mainviewgrid_placeorderid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			if(msg != ''){
				if(transactiontype == 21 && stocktype == 76) {
					$.each(msg, function(index) {
						$("#ordernumber")
						.append($("<option></option>")
						.attr("data-ordernumber",msg[index]['data'])
						.attr("data-ordernumberhidden",msg[index]['data'])
						.attr("data-accountid",msg[index]['accountid'])
						.attr("data-salesdetailcount",msg[index]['salesdetailcount'])
						.attr("data-accountname",msg[index]['accountname'])
						.attr("value",msg[index]['id'])
						.text(msg[index]['data']+' - '+msg[index]['accountname']));
					});
				} else {
					$.each(msg, function(index) {
						$("#ordernumber")
						.append($("<option></option>")
						.attr("data-ordernumber",msg[index]['data'])
						.attr("data-ordernumberhidden",msg[index]['data'])
						.attr("data-accountid",msg[index]['accountid'])
						.attr("data-salesdetailcount",msg[index]['salesdetailcount'])
						.attr("data-accountname",msg[index]['accountname'])
						.attr("value",msg[index]['id'])
						.text(msg[index]['data']));
					});
				}
			}
		},
	});
}
function loadordernumber(stktype,status,refno) {
	$('#ordernumber').empty();
	if(stktype != 76) {
		$('#ordernumber').append($("<option></option>"));
		$('#ordernumber').append($("<option></option>").attr("value","1").text('Stock order'));
	}
	$.ajax({
		url: base_url+"Sales/loadordernumber",
		data:{stocktype:stktype,status:status,refno:refno},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			if(msg != '') {
				$.each(msg, function(index) {
				$("#ordernumber")
				.append($("<option></option>")
				.attr("data-salesdetailid",msg[index]['id'])
				.attr("value",msg[index]['id'])
				.text(msg[index]['data']));
				});
			}
		},
	});
}
//payment purewt calculation
function paymentcalculateratecut(fieldid,type) {
	var amount = $('#paymenttotalamount').val();
	setzero(['paymentratepergram','paymenttotalamount','paymentpureweight']);
	var rate = parseFloat($('#paymentratepergram').val());
	var output = 0;
	var outputid ='vishaldump';
	var metal = $('#paymentpureweight').val();
	var metalid = 'paymentpureweight';
	if(fieldid == 'paymentratepergram'){
		if(amount > 0.01 && rate != 0) {
			var output = (parseFloat(amount)/parseFloat(rate)).toFixed(5);
			var outputid = metalid;
		} else if(metal > 0 && rate != 0) {
			var output = (parseFloat(metal)*parseFloat(rate)).toFixed(5);
			var outputid ='paymenttotalamount';
		}
	} else if(fieldid == 'paymenttotalamount') {
		if(rate > 0) {
			var output = (parseFloat(amount)/parseFloat(rate)).toFixed(5);
			var outputid = metalid;
		} else if(metal > 0) {
			var output = (parseFloat(amount)/parseFloat(metal)).toFixed(5);
			var outputid ='paymentratepergram';
		}
	} else if(fieldid == 'paymentpureweight') {
		if(rate > 0) {
			var output = (parseFloat(metal)*parseFloat(rate)).toFixed(5);
			var outputid ='paymenttotalamount';
		} else if(amount > 0) {
			var output = (parseFloat(amount)/parseFloat(metal)).toFixed(5);
			var outputid ='paymentratepergram';
		}
	}
	var output = checknan(output);
	if(outputid == 'paymenttotalamount' || outputid == 'paymentratepergram')	{
		var output = parseFloat(output).toFixed(roundamount);
	} else {
		var output = parseFloat(output).toFixed(roundweight);
	}
	$('#'+outputid+'').val(output);
	Materialize.updateTextFields();
}
{// View create success function
    function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
function bullionproductshowhide(productname,productid){
	$("#"+productname+" option").addClass("ddhidedisplay");
	$("#"+productname+" option").prop('disabled',true);
	var val = 3;
	$("#"+productname+" option[data-categoryid="+val+"]").addClass("ddhidedisplay");
	$("#"+productname+" option[data-categoryid="+val+"]").prop('disabled',true);
	if(productid == ''){
		$("#"+productname+" option").removeClass("ddhidedisplay");
		$("#"+productname+" option").prop('disabled',false);
		$("#"+productname+" option[data-categoryid="+val+"]").addClass("ddhidedisplay");
		$("#"+productname+" option[data-categoryid="+val+"]").prop('disabled',true);
	}else{
		if(productid.indexOf(',') > -1) {
			var product = productid.split(',');
			for(j=0;j<(product.length);j++) {
				$("#"+productname+" option[value="+product[j]+"]").removeClass("ddhidedisplay");
				$("#"+productname+" option[value="+product[j]+"]").prop('disabled',false);
			}
		}else{
			var product = productid;
			$("#"+productname+" option[value="+product+"]").removeClass("ddhidedisplay");
			$("#"+productname+" option[value="+product+"]").prop('disabled',false);
		}
	}
}
function accountshowhide(accountid,val,transactionmode){
	$("#"+accountid+" option").addClass("ddhidedisplay");
	$("#"+accountid+" optgroup").addClass("ddhidedisplay");
	$("#"+accountid+" option").prop('disabled',true);
	var accounttypeid = '';
	if(val == '9' || val == '19' || val == '21' || val == '24' || val == '27' || val == '29' || val == '30') {
		accounttypeid = '16';
	} else if(val =='13') {
		accounttypeid = '6';
	} else if(val =='22' || val == '23' || val == '20' || val == '26' || val == '28') {
		accounttypeid = '6,16';
	} else if(val == '11' || val == '16') {
		if(transactionmode == 2) {
			accounttypeid = '6';
		} else if(transactionmode == 3) {
			accounttypeid = '16';
		} else if(transactionmode == 4) {
			accounttypeid = '6,16';
		} else {
			accounttypeid = '6,16';
		}
	}
	if(val == '' || val == '18') {
		$("#"+accountid+" option").removeClass("ddhidedisplay");
		$("#"+accountid+" option").prop('disabled',false);
		$("#"+accountid+" option:enabled").closest('optgroup').removeClass("ddhidedisplay");
	} else {
		if(accounttypeid.indexOf(',') > -1) {
			var stocktype = accounttypeid.split(',');
			for(j=0;j<(stocktype.length);j++) {
				$("#"+accountid+" option[data-accounttype="+stocktype[j]+"]").removeClass("ddhidedisplay");
				$("#"+accountid+" option[data-accounttype="+stocktype[j]+"]").prop('disabled',false);
				$("#"+accountid+" optgroup[id='"+stocktype[j]+"']").closest('optgroup').removeClass("ddhidedisplay");
			}
		}else {
			$("#"+accountid+" option[data-accounttype="+accounttypeid+"]").removeClass("ddhidedisplay");
			$("#"+accountid+" option[data-accounttype="+accounttypeid+"]").prop('disabled',false);
			$("#"+accountid+" optgroup[id='"+accounttypeid+"']").closest('optgroup').removeClass("ddhidedisplay");
		}
	}
	if(accountid == 'accounttypeid') {
		$('#accounttypeid').select2('val',accounttypeid).trigger('change');
		//$('#selaccounttypeid').val(accounttypeid);
	}
}
function paymenttypeshowhide(stocktypeid,val,transactiontype){
    if(transactiontype == '11' || transactiontype == '9' || transactiontype == '20' || transactiontype == '24' || transactiontype == '23' || transactiontype == '22'){
		var paymentstocktype = $("#salestransactiontypeid").find('option:selected').data('stocktyperelation');
		$("#"+stocktypeid+" option").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option").prop('disabled',true);
		var typeid = '';
		if(val == '2'){
			if(transactiontype == '11'){
				typeid = '11,12,13,16,19,20,74,83';  // sales type
			}else if(transactiontype == '20'){
				typeid = '75,19,82';  // take order type
			}else if(transactiontype == '9'){
				typeid = '17,73,77,80,81';  // purchase type
			}
		}
		if(typeid.indexOf(',') > -1) {
			var stocktype = typeid.split(',');
			for(j=0;j<(stocktype.length);j++) {
				$("#"+stocktypeid+" option[value="+stocktype[j]+"]").removeClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value="+stocktype[j]+"]").prop('disabled',false);
			}
		}else {
			if(typeid != ''){
				$("#"+stocktypeid+" option[value="+typeid+"]").removeClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value="+typeid+"]").prop('disabled',false);
			}
		}
	}
	
}
//tax overlay grid
function taxgrid() {
	var wwidth = $("#taxgrid").width();
	var wheight = $("#taxgrid").height();
	$.ajax({
		url:base_url+"Sales/taxgirdheaderinformationfetch?moduleid=221&width="+wwidth+"&height="+wheight+"&modulename=salestaxgrid",
		dataType:'json',
		async:true,
		cache:false,
		success:function(data) {
			$("#taxgrid").empty();
			$("#taxgrid").append(data.content);
			$("#taxgridfooter").empty();
			$("#taxgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('taxgrid');
			/* Calculate total taxes */
			tax_data_total();
		},
	});	
}
// Bill tax overlay grid
function billtaxgrid() {
	var wwidth = $("#billtaxgrid").width();
	var wheight = $("#billtaxgrid").height();
	$.ajax({
		url:base_url+"Sales/taxgirdheaderinformationfetch?moduleid=221&width="+wwidth+"&height="+wheight+"&modulename=salestaxgrid",
		dataType:'json',
		async:true,
		cache:false,
		success:function(data) {
			$("#billtaxgrid").empty();
			$("#billtaxgrid").append(data.content);
			$("#billtaxgridfooter").empty();
			$("#billtaxgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('billtaxgrid');
			retrievealltaxdetailinformation();
		},
	});	
}
function tax_data_total() {
	var total =  parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
    $('#tax_data_total').val(total.toFixed(2));
	var salestaxtypeid = $("#taxtypeid").val();
}
function billtax_data_total() {
	var total =  parseFloat(getgridcolvalue('billtaxgrid','','amount','sum'));
    $('#billtax_data_total').val(total.toFixed(2));
	{ // Tax grid json convert
		var tax_json = {}; //declared as object
		tax_json.id = '1';//categoryid
		tax_json.data = getgridrowsdata('billtaxgrid');//taxgrid
		$('#summarytaxgriddata').val(JSON.stringify(tax_json));
	}
}
function submittaxgrid(stocktypeid) {
	{// defaults and fetch grossamount
		var salestaxtypeid = $("#taxtypeid").val();
		if(stocktypeid == '20' || stocktypeid == '19' ){salestaxtypeid = 2;}
		if(salestaxtypeid == 2) 
		{
			var griddataid = 'taxgriddata';
			var tax_amount_field = 'taxamount';
		}else{
			var griddataid = 'summarytaxgriddata';
			var tax_amount_field = 'sumarytaxamount';
		}
	}
	{ // final set amount
		taxamountcalculation(tax_amount_field,stocktypeid);
	}
	{// declare
	var tax_category_id = 'taxcategory';
	var tax_json ={}; //declared as object
	tax_json.id = $('#'+tax_category_id+'').val();//categoryid
	tax_json.data = getgridrowsdata('taxgrid');//taxgrid
	$('#'+griddataid+'').val(JSON.stringify(tax_json));
	}
	if(salestaxtypeid == 2) {//item wise
		billdiscountcalculation();
	} 
	$('#taxoverlay').fadeOut();
}

function taxamountcalculation(tax_amount_field,stocktypeid) {// calculate from tax grid data
	var grossamount =  taxamtcalcfromgrossamt(stocktypeid);
	var totalcharge = 0;
	var rowid = getgridallrowids('taxgrid');
	for(var i=0;i<rowid.length;i++) {
		var rate = getgridcolvalue('taxgrid',rowid[i],'taxrate','');
		var famount =(parseFloat(rate)/100)*parseFloat(grossamount);
		var gridtaxamount = famount.toFixed(2);
		$('#taxgrid .gridcontent div#'+rowid[i]+' ul .amount-class').text(gridtaxamount);
		totalcharge = parseFloat(gridtaxamount)+parseFloat(totalcharge);
	}
	$('#'+tax_amount_field+'').val(totalcharge.toFixed(2));
	var total =  parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
    $('#tax_data_total').val(totalcharge.toFixed(2));
}

 function taxamtcalcfromgrossamt(stocktypeid) {
	var netamount = '0';
	var salestaxtypeid = $("#taxtypeid").val();
	if(stocktypeid == '20' || stocktypeid == '19' ){salestaxtypeid = 2;}
	if(salestaxtypeid == 2) {
		var grossamount = 0;
		var discountamount = 0;
		if($.trim($('#discount').val()) != ''){
			discountamount=$.trim($('#discount').val());
		}
		grossamount=parseFloat($('#grossamount').val());
		if(taxcalc == 1) { // after discount
		  var netamount = parseFloat(grossamount)-parseFloat(discountamount);
		}else if(taxcalc == 2){ // before discount
		  var netamount = parseFloat(grossamount);	
		}
	} else {
		var sumgrossamount = parseFloat($("#billgrossamount").val());
		var sumdiscountamount=parseFloat($('#sumarydiscountamount').val());
		if(taxcalc == 1) { // after discount
		  var netamount = parseFloat(sumgrossamount)-parseFloat(sumdiscountamount);
		}else if(taxcalc == 2){ // before discount
		  var netamount = parseFloat(sumgrossamount);	
		}
	}
	return netamount;
}
function resetoverlay(gridid) {
	cleargriddata(gridid);
}
//bill number grid load
function billnumbergrid() {
	var wwidth = $("#billnumbergrid").width();
	var wheight = $("#billnumbergrid").height();
	var checkmultiple = $("#salesreturntype").val();
	$.ajax({
		url:base_url+"Sales/billnumbergridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=billnumbergrid"+"&checkmultiple="+checkmultiple,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#billnumbergrid").empty();
			$("#billnumbergrid").append(data.content);
			$("#billnumbergridfooter").empty();
			$("#billnumbergridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('billnumbergrid');
			//header check box
			$(".billnumbergrid_headchkboxclass").click(function() {
				$(".billnumbergrid_rowchkboxclass").prop('checked', false);
				if($(".billnumbergrid_headchkboxclass").prop('checked') == true) {
					$(".billnumbergrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".billnumbergrid_rowchkboxclass").prop('checked', false);
				}
				getcheckboxrowid('billnumbergrid');
			});
		},
	});	
}
//old jewel voucher grid load
function oldjewelvouchergrid() {
	var wwidth = $("#oldjewelvouchergrid").width();
	var wheight = $("#oldjewelvouchergrid").height();
	$.ajax({
		url:base_url+"Sales/oldjewelvouchergridgridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=oldjewelvouchergrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#oldjewelvouchergrid").empty();
			$("#oldjewelvouchergrid").append(data.content);
			$("#oldjewelvouchergridfooter").empty();
			$("#oldjewelvouchergridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('oldjewelvouchergrid');
			//header check box
			$(".oldjewelvouchergrid_headchkboxclass").click(function() {
				$(".oldjewelvouchergrid_rowchkboxclass").prop('checked', false);
				if($(".oldjewelvouchergrid_headchkboxclass").prop('checked') == true) {
					$(".oldjewelvouchergrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".oldjewelvouchergrid_rowchkboxclass").prop('checked', false);
				}
				oldjewelvouchergetcheckboxrowid('oldjewelvouchergrid');
			});
		},
	});	
}
//approval return grid load
function approvalreturngrid() {
	var wwidth = $("#approvalreturngrid").width();
	var wheight = $("#approvalreturngrid").height();
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/approvalreturngridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=approvalreturngrid"+"&checkmultiple="+checkmultiple,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#approvalreturngrid").empty();
			$("#approvalreturngrid").append(data.content);
			$("#approvalreturngridfooter").empty();
			$("#approvalreturngridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('approvalreturngrid');
			//header check box
			$(".approvalreturngrid_headchkboxclass").click(function() {
				$(".approvalreturngrid_rowchkboxclass").prop('checked', false);
				if($(".approvalreturngrid_headchkboxclass").prop('checked') == true) {
					$(".approvalreturngrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".approvalreturngrid_rowchkboxclass").prop('checked', false);
				}
				approvaloutreturngetcheckboxrowid('approvalreturngrid');
			});
		},
	});	
}
//issue/receipt grid load
function issuereceiptgrid() {
	var wwidth = $("#issuereceiptgrid").width();
	var wheight = $("#issuereceiptgrid").height();
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/issuereceiptgridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=issuereceiptgrid"+"&checkmultiple="+checkmultiple,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#issuereceiptgrid").empty();
			$("#issuereceiptgrid").append(data.content);
			$("#issuereceiptgridfooter").empty();
			$("#issuereceiptgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('issuereceiptgrid');
			//header check box
			$(".issuereceiptgrid_headchkboxclass").click(function() {
				$(".issuereceiptgrid_rowchkboxclass").prop('checked', false);
				if($(".issuereceiptgrid_headchkboxclass").prop('checked') == true) {
					$(".issuereceiptgrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".issuereceiptgrid_rowchkboxclass").prop('checked', false);
				}
				issuereceiptgridgetcheckboxrowid('issuereceiptgrid');
			});
			//hideprodgridcol = ['finalamtissue','finalamtreceipt','finalweightissue','finalweightreceipt'];
			hideprodgridcol = ['mainfinalamthidden','mainfinalwthidden'];
			gridfieldhide('issuereceiptgrid',hideprodgridcol);
		},
	});	
}
//issue/receipt local grid load
function issuereceiptlocalgrid() {
	var wwidth = $("#issuereceiptlocalgrid").width();
	var wheight = $("#issuereceiptlocalgrid").height();
	$.ajax({
		url:base_url+"Sales/issuereceiptlocalgridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=issuereceiptlocalgrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#issuereceiptlocalgrid").empty();
			$("#issuereceiptlocalgrid").append(data.content);
			$("#issuereceiptlocalgridfooter").empty();
			$("#issuereceiptlocalgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('issuereceiptlocalgrid');
			//header check box
			$(".issuereceiptlocalgrid_headchkboxclass").click(function() {
				$(".issuereceiptlocalgrid_rowchkboxclass").prop('checked', false);
				if($(".issuereceiptlocalgrid_headchkboxclass").prop('checked') == true) {
					$(".issuereceiptlocalgrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".issuereceiptlocalgrid_rowchkboxclass").prop('checked', false);
				}
				issuereceiptgridgetcheckboxrowid('issuereceiptlocalgrid');
			});
			hideprodgridcol = ['finalamthidden','finalwthidden'];
			gridfieldhide('issuereceiptlocalgrid',hideprodgridcol);
		},
	});	
}
//order grid load
function ordergrid() {
	var wwidth = $("#ordergrid").width();
	var wheight = $("#ordergrid").height();
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/ordergridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=ordergrid"+"&checkmultiple="+checkmultiple,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#ordergrid").empty();
			$("#ordergrid").append(data.content);
			$("#ordergridfooter").empty();
			$("#ordergridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('ordergrid');
			//header check box
			$(".ordergrid_headchkboxclass").click(function() {
				$(".ordergrid_rowchkboxclass").prop('checked', false);
				if($(".ordergrid_headchkboxclass").prop('checked') == true) {
					$(".ordergrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".ordergrid_rowchkboxclass").prop('checked', false);
				}
				ordergridgetcheckboxrowid('ordergrid');
			});
		},
	});	
}
//bulk sales grid load
function bulksalesgrid() {
	var wwidth = $("#bulksalesgrid").width();
	var wheight = $("#bulksalesgrid").height();
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/bulksalesgridgirdheaderinformationfetch?moduleid=52&width="+wwidth+"&height="+wheight+"&modulename=bulksalesgrid"+"&checkmultiple="+checkmultiple,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#bulksalesgrid").empty();
			$("#bulksalesgrid").append(data.content);
			$("#bulksalesgridfooter").empty();
			$("#bulksalesgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('bulksalesgrid');
			//header check box
			$(".bulksalesgrid_headchkboxclass").click(function() {
				$(".bulksalesgrid_rowchkboxclass").prop('checked', false);
				if($(".bulksalesgrid_headchkboxclass").prop('checked') == true) {
					$(".bulksalesgrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".bulksalesgrid_rowchkboxclass").prop('checked', false);
				}
				bulksalesgridgetcheckboxrowid('bulksalesgrid');
			});
		},
	});	
}
function bulksalesgridgetcheckboxrowid(gridid) {
	var selected = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#bulksalesrgridcloseproductid").val(selected);
}
function getcheckboxrowid(gridid) {
	var selected = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#billbasedproductid").val(selected);
}
function takeordergetcheckboxrowid(gridid) {
	var selected = [];
	var orderno = [];
	var i=0;
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
		if(i==0) {
			orderno.push(getgridcolvalue(gridid,$(this).attr('data-rowid'),'orderno',''));
		}
		i++;
	});
	$("#billbasedproductid").val(selected);
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	if(salestransactiontypeid != '21' && salestransactiontypeid != '20') {
		$("#mainordernumber").val(orderno);
	}
}
function oldjewelvouchergetcheckboxrowid(gridid) {
	var selected = [];
	var oldjewelvoucher = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
		oldjewelvoucher.push(getgridcolvalue(gridid,$(this).attr('data-rowid'),'salesnumber',''));
	});
	$("#oldjewelvoucherproductid").val(selected);
	$("#oldjewelvoucherbillno").val(oldjewelvoucher);
}
function approvaloutreturngetcheckboxrowid(gridid) {
	var selected = [];
	var approvaloutreturntagno = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
		approvaloutreturntagno.push(getgridcolvalue(gridid,$(this).attr('data-rowid'),'itemtagnumber',''));
	});
	$("#approvaloutreturnproductid").val(selected);
	$("#approvaloutreturntagno").val(approvaloutreturntagno);
}
function issuereceiptgridgetcheckboxrowid(gridid) {
	var selected = [];
	var approvaloutreturntagno = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#issuereceiptproductid").val(selected);
}
function ordergridgetcheckboxrowid(gridid) {
	var selected = [];
	var approvaloutreturntagno = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#orderproductid").val(selected);
}
function newloadgriddata(gridid,grid_data) {
	cleargriddata(gridid);
	$('#'+gridid+' .gridcontent div.data-content').empty();
	var gridlength = grid_data.length;
	var j=0;
	for(var i=0;i<gridlength;i++) {
		/*add json data to grid*/
		addinlinegriddata(gridid,j+1,grid_data[i],'json');
		j++;
	}
	/*data row select event*/
	datarowselectevt();
	/*column resize*/
	columnresize(gridid);
}
function totalgrossamountcalculationwork() {
	setzero(['grossweight','stoneweight','dustweight','wastageweight','netweight','grossamount','additionalchargeamt','stonecharge','taxamount','discount','wastageless','wastage','makingcharge','ratepergram','flatcharge','paymenttouch','lstcharge','proditemrate','repaircharge']);
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
	var transactionmode = $('#transactionmodeid').find('option:selected').val();
	var touch = parseFloat($("#paymenttouch").val());
	var rate = parseFloat($("#ratepergram").val());
	var melting = $('#melting').val();
	var pieces = $('#pieces').val();
	{// wastage calculation if wt we multiply by rate
		if(wastagecalctype == 1 ) {
			var wastage = parseFloat($("#prewastage").val());
			if(touch > 0) {
				var finaltouch =  parseFloat(parseFloat(touch)/parseFloat(purewtcalctype)).toFixed(5);
			} else {
				var finaltouch = 1;
			}
		} else if(wastagecalctype == 4 || wastagecalctype == 3) {
			var	wastage = parseFloat($("#prewastage").val());
			var finaltouch =  parseFloat(parseFloat(melting)/parseFloat(purewtcalctype)).toFixed(5);
		} else if(wastagecalctype == 5 ) {
			var	wastage = 0;
			var finaltouch =  parseFloat(parseFloat(touch)/parseFloat(purewtcalctype)).toFixed(5);
		} else if(wastagecalctype == 2 ) {
			var	wastage = parseFloat($("#prewastage").val()* rate);
			var finaltouch = 1;
		}
	}
	{// product calculation on grosswt or netwt based on wat we give in product master
		var netwt = parseFloat($("#grossweight").val());
		var productcalctype = $('#product').find('option:selected').data('calctypeid');
		if(productcalctype  == '2') {
			var netwt = parseFloat($("#netweight").val());
		}
	}
	{// all charges shortand
		var stonetotamt = parseFloat($("#stonecharge").val());
		var makingcharge = parseFloat($('#premakingcharge').val());
		var lstcharge = parseFloat($('#prelstcharge').val());
		var charable = parseFloat($("#additionalchargeamt").val());
		var flatcharge = parseFloat($("#preflatcharge").val());
		var repaircharge = parseFloat($("#prerepaircharge").val());
		var allchargesinclwastage = parseFloat((charable+stonetotamt+makingcharge+wastage+flatcharge+lstcharge+repaircharge)).toFixed(5);
		var hflatcharge = parseFloat($("#hflat").val());
		var hpiececharge = parseFloat($("#hpiece").val());
		var cflatcharge = parseFloat($("#cflat").val());
	}
	{// pregrossamount and transaction mode i.e calctype
		if(transactionmode == '' || transactionmode == '1' ) {
			transactionmode = 2;
		}
		if(transactiontype == 9) {
			if(stocktypeid == 17) { // P.amt
				var tagtypeid=$('#product').find('option:selected').data('tagtypeid');
				if(tagtypeid == 5) {
					var pieces = $("#pieces").val();
					var proditemrate = $("#proditemrate").val();
					var pregrossamount = parseInt(pieces*proditemrate);
				} else {
					var pregrossamount = (parseFloat((netwt)*(parseFloat(finaltouch)))*rate).toFixed(5);
				}
			} else {
				var pregrossamount = '0';
			}
		} else {
			var pregrossamount = (parseFloat((netwt)*(parseFloat(finaltouch)))*rate).toFixed(5);
		}
	}
	{//  calculate grosspurewt and grossamount based on calctype
		if(transactionmode == 3) { // pure weight
			if(rate == 0) {
				var grossprepureweight = 0;
			} else {
				var grossprepureweight = parseFloat((allchargesinclwastage)/rate).toFixed(5);
			}
			var grosspureweight = (parseFloat((netwt)*(parseFloat(finaltouch)))+parseFloat(grossprepureweight)).toFixed(roundweight);
			
			if(stocktypeid != 19) {
				$("#grosspureweight").val(grosspureweight).trigger('change');
			}
			$('#pregrossamount').val(pregrossamount);
			$("#grossamount").val(0);
		} else if(transactionmode == 4) { // amount pureweight
			var pureweightcalc = $('#salestransactiontypeid').find('option:selected').data('pureadditionalchargeid');
			var pureweightcalc = pureweightcalc.toString();
			var pureweightcalc = '0,'+pureweightcalc ;
			var purechargedata=pureweightcalc.split(",");
			var stonepureweight = 0;
			var wastagepureweight = 0;
			var makingchargepureweight = 0;
			var lstchargepureweight = 0;
			var flatchargepureweight = 0;
			var hflatpureweight = 0;
			var hpiecepureweight = 0;
			var cflatpureweight = 0;
			if(rate > 0) {
				$.each(purechargedata, function (index, value) { //pure weight
					var attrid = $("#salesaddform input[charge=additionalcharge"+value).attr('id');
					if(attrid == 'stonecharge'){
						stonepureweight = (parseFloat(stonetotamt/rate)).toFixed(5);
						stonetotamt = 0;
					}
					else if(attrid == 'wastage'){
						wastagepureweight = (parseFloat(wastage/rate)).toFixed(5);
						wastage = 0;
					}
					else if(attrid == 'makingcharge'){
						makingchargepureweight = (parseFloat(makingcharge/rate)).toFixed(5);
						makingcharge = 0;
					} else if(attrid == 'lstcharge'){
						lstchargepureweight = (parseFloat(lstcharge/rate)).toFixed(5);
						lstcharge = 0;
					}
					else if(attrid == 'flatcharge'){
						flatchargepureweight = (parseFloat(flatcharge/rate)).toFixed(5);
						flatcharge = 0;
					}
					else if(attrid == 'hflat'){
						hflatpureweight = (parseFloat(hflatcharge/rate)).toFixed(5);
						hflatcharge = 0;
					}
					else if(attrid == 'hpiece'){
						hpiecepureweight = (parseFloat(hpiececharge/rate)).toFixed(5);
						hpiececharge = 0;
					}
					else if(attrid == 'cflat'){
						cflatpureweight = (parseFloat(cflatcharge/rate)).toFixed(5);
						cflatcharge = 0;
					}
				});
				
			}
			var puretoamtchargeamt = parseFloat(parseFloat(stonetotamt) + parseFloat(wastage) + parseFloat(makingcharge) + parseFloat(flatcharge) +  parseFloat(hflatcharge) + parseFloat(hpiececharge) + parseFloat(cflatcharge) + parseFloat(lstcharge)).toFixed(5);
			var puretoamtchargewt = parseFloat(parseFloat(stonepureweight) + parseFloat(wastagepureweight) + parseFloat(makingchargepureweight) + parseFloat(flatchargepureweight) +  parseFloat(hflatpureweight) + parseFloat(hpiecepureweight) + parseFloat(cflatpureweight) + parseFloat(lstchargepureweight)).toFixed(5);
		
			if(stocktypeid != 19) {
				var grossamount = parseFloat(puretoamtchargeamt).toFixed(5);
				$("#grossamount").val(parseFloat(grossamount).toFixed(roundamount));
				$('#pregrossamount').val(pregrossamount);
			} else {
				$('#pregrossamount').val($('#grossamount').val());
			}
			var grosspureweight = (parseFloat((netwt)*(parseFloat(finaltouch))) + parseFloat(puretoamtchargewt)).toFixed(roundweight);
			if(stocktypeid != 19) {
				$("#grosspureweight").val(grosspureweight).trigger('change');
			}
		} else { // Amount Calculation
			var tagtypeid = $('#product').find('option:selected').data('tagtypeid');
			if(taxchargeinclude == 0){ // IF grossamt we apply tax before charges 
				var allchargesinclwastage = 0;
			}
			if(stocktypeid == 20 && tagtypeid == 5) {
				$('#pregrossamount').val($('#grossamount').val());
			} else if(stocktypeid == 17 && tagtypeid == 5) {
				var totgrossamount = parseFloat(parseFloat(pregrossamount)+parseFloat(allchargesinclwastage)).toFixed(0);
				$('#grossamount').val(totgrossamount);
			} else if(stocktypeid != 19) {
				/* if(stocktypeid == 75 || stocktypeid == 82) {
					 var grossamount = (parseFloat(netwt)*rate*parseFloat(pieces))+parseFloat(allchargesinclwastage);
					}
					else{ 
					  
				}*/
				var grossamount = ((parseFloat((netwt)*(parseFloat(finaltouch)))*rate)+parseFloat(allchargesinclwastage)).toFixed(5);
				if(transactiontype == 13) {
					$("#grossamount").val(parseFloat(grossamount).toFixed(roundamount));
				} else {
					$("#grossamount").val(parseFloat(grossamount).toFixed(roundamount)).trigger('change');
				}
				$('#pregrossamount').val(pregrossamount);
			} else if(stocktypeid == 19 && oldjewelsstonedetails == 1) {
				var grossamount = (parseFloat($('#grossamount').val())+parseFloat(stonetotamt)).toFixed(5);
				$("#grossamount").val(parseFloat(grossamount).toFixed(roundamount)).trigger('change');
			} else {
				$('#pregrossamount').val($('#grossamount').val());
			}
			$("#grosspureweight").val(0);
		}		
	}
	{// tax calcualte for itemlevel tax on item grossamount change
		var taxtypeid = $("#taxtypeid").val();
		if(stocktypeid == '20' || stocktypeid == '19' ){taxtypeid = 2;} // for old jewel and salesreturn
		if(taxtypeid == '2' && stocktypeid != 19) { // restrict old jewel as of now, since for old no tax
			var taxcategoryid = $.trim($('#taxcategory').find('option:selected').val());
			if(taxcategoryid != '') {
				var taxmasterid = taxcategoryid;
			} else {
				var taxmasterid = $('#product').find('option:selected').data('taxmasterid');
			}
			var autocalculatetax = $('#salestransactiontypeid').find('option:selected').data('autotax');
			
			if(oldjewelsumtaxapplyid == 0 && stocktypeid == '19'){autocalculatetax = 0;}
			
			if(($('#taxamount').val() > '0' &&  taxcategoryid == '' ) || oldsalesreturntax == 2 || billleveltaxautoremoval == 1) {
				
				$('#taxamount').val(0); 
				$('#taxdetails,#taxgriddata').val('');
			  
			} else if((autocalculatetax == '1' && taxmasterid != '1') || $('#taxamount').val() != 0 || taxcategoryid != '') {
				
				taxautocalculation(stocktypeid);
			}
			$('#taxoverlay').fadeOut();
		}
	}
	{
		if(transactionmode == 3) { // pure weight
			var netpureweight =  $("#grosspureweight").val();
			var netamount = 0;
		} else if(transactionmode == 4) { // pure weight amt
			var netpureweight =  $("#grosspureweight").val();
			var netamount = $('#grossamount').val();
		} else { // Amount
			var taxamount  = 0;
			if($.trim($('#taxamount').val()) != '') {
			   taxamount=$.trim($('#taxamount').val());
			}
			var discountamount = 0;
			var grossamount = $('#grossamount').val();
			if($.trim($('#discount').val()) != '') {
			   discountamount=$.trim($('#discount').val());
			}
			var netpureweight = 0;
			var netamount = parseFloat(parseFloat(grossamount) + parseFloat(taxamount) - parseFloat(discountamount)).toFixed(roundamount);
			
		}
		$('#pureweight').val(netpureweight);
		$('#totalamount').val(netamount);
		if(transactiontype == 16 && estimatequicktotal == 1 && stocktypeid == 11) { // estimatequicktotal
			$('#fasttotalfortagamt').val(netamount);
			$('#fasttotalfortagwt').val(netpureweight);
		}
	}
}

function discount_total(){
	setzero(['grossamount','discountpercent']); //sets zero on EMPTY
	var dtypeid = $('#sdiscounttypeid').val();//billing or  item discount		
	var typeid = $('#discounttypeid').val(); //discount typeid percent/diect / gift voucher
	var value = $('#discountpercent').val();	//discount valuesa
	if(dtypeid == 2) { //item wise
		if(discountcalc == 1){ // Before tax
		  var grossamount = $('#grossamount').val();
		}else if(discountcalc == 2){ // After tax
		  var grossamount = (parseFloat($('#grossamount').val()) + parseFloat($('#taxamount').val()));
		}		
	} else if(dtypeid == 3) { //bill wise
		if(discountcalc == 1){ // Before tax
		  var grossamount = (parseFloat($.trim($("#sumgross").text())));
		}else if(discountcalc == 2){ // After tax
		  var grossamount = (parseFloat($.trim($("#sumgross").text()))+ parseFloat($('#sumarytaxamount').val()));
		}
	} else { //both level
		
	}
	if(typeid != null && typeid !=''){			
		var discount_amount = parseFloat(value);
		if(typeid == 3) {//on percentage type. 
			var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
		}			
		var amt_discount =  discount_amount.toFixed(roundamount);
	} else {			
		var amt_discount =  0;
	}
	$('#singlediscounttotal').val(amt_discount);
}
function giftdiscount_total() {
	var denomination = $('#giftdenomination').val(); //discount typeid
	var unit = $('#giftunit').val();
	var discount_amount = (parseFloat(denomination) * parseFloat(unit));
	var amt_discount =  discount_amount.toFixed(roundamount);
	$('#singlediscounttotal').val(amt_discount);
}
function discountcalculation(discountdata,grossamount) {	
	if(discountdata.value != null && discountdata.value !=''){			
		var discount_amount = parseFloat(discountdata.value);
		if(discountdata.typeid == 3) {//on percentage type.
			var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
		} else if(discountdata.typeid == 4){
			var giftdenomination = parseFloat(discountdata.giftdenomination);
			var unit = parseFloat(discountdata.giftunit);
			var discount_amount = (parseFloat(giftdenomination)*parseFloat(unit));
		}	 		
		return discount_amount.toFixed(roundamount); 
	} else {			
		return 0;
	}
}
function getdetailsforoldjewel(){
	var stocktype = $("#stocktypeid").find('option:selected').val();
	var transactionid = $("#salestransactiontypeid").find('option:selected').val();
	var itemdetail = $("#olditemdetails").find('option:selected').val();
	if(itemdetail == "") {
		itemdetail = 2;
	} else {
		itemdetail = itemdetail;
	}
	var purity = $.trim($("#purity").find('option:selected').val());
	if(purity != '') {
		$.ajax({
			url:base_url+"Sales/detailsofoldjewel?stocktype="+stocktype+"&transactionid="+transactionid+"&itemdetail="+itemdetail+"&purity="+purity,
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				var gwt = data.grosswt;
				var rate = data.rateless;
				var weight = data.weight;
				$("#gwcaculation").val(gwt);
				$("#ratelesscalc").val(rate);
				$("#weightlesscalc").val(weight);
				if(weight == '' || weight == 'null' || weight === undefined){
					$("#wastageless").val(0);
				}else{
					$("#wastageless").val(weight);
				}
				Materialize.updateTextFields();
			},
		});
	}	
}
//bill number grid data load
function billnumbergriddatafetch(salesid) {
	var transaction = $('#salestransactiontypeid').find('option:selected').val();
	var tagtype = $('#stocktypeid').find('option:selected').val();
	var checkmultiple = $("#salesreturntype").val();
	var salesdetailsarray = [];
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['salesdetailid'] != '1' && griddata[i]['salesdetailid'] != ''){
		 salesdetailsarray.push(griddata[i]['salesdetailid']);
		}
	}
	var billbasedproductid = $("#billbasedproductid").val();
	$.ajax({
		url:base_url+"Sales/billnumbergriddatafetch?stocktype="+tagtype+"&transactionid="+transaction+"&billnumberid="+salesid+"&billbasedproductid="+salesdetailsarray,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			if(checkmultiple == 3) {
				loadinlinegriddata('billnumbergrid',data['estimatedata'].rows,'json','checkbox');
			} else {
				loadinlinegriddata('billnumbergrid',data['estimatedata'].rows,'json');
			}
			//row based check box
			$(".billnumbergrid_rowchkboxclass").click(function(){
				$('.billnumbergrid_headchkboxclass').removeAttr('checked');
				getcheckboxrowid('billnumbergrid');
			});
			var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid'];
			gridfieldhide('billnumbergrid',hideprodgridcol);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('billnumbergrid');
		},
	});	
}
//old jewel voucher grid data load
function oldjewelvouchergriddatafetch(accountid) {
	var transaction = $('#salestransactiontypeid').find('option:selected').val();
	var tagtype = $('#stocktypeid').find('option:selected').val();
	var salesdetailsarray = [];
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['salesdetailid'] != '1' && griddata[i]['salesdetailid'] != ''){
		 salesdetailsarray.push(griddata[i]['salesdetailid']);
		}
	}
	$.ajax({
		url:base_url+"Sales/oldjewelvouchergriddatafetch?stocktype="+tagtype+"&transactionid="+transaction+"&accountid="+accountid+"&billbasedproductid="+salesdetailsarray,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			loadinlinegriddata('oldjewelvouchergrid',data['estimatedata'].rows,'json','checkbox');
			//row based check box
			$(".oldjewelvouchergrid_rowchkboxclass").click(function(){
				$('.oldjewelvouchergrid_headchkboxclass').removeAttr('checked');
				oldjewelvouchergetcheckboxrowid('oldjewelvouchergrid');
			});
			/* data row select event */
			datarowselectevt();
			var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','paymentstocktypename','paymenttotalamount','itemtagnumber','wastage','makingcharge','stonecharge','taxamount','discount','hiddenstonedata','flatcharge','cflat','additionalchargeamt','bankname','paymentrefnumber','paymentreferencedate','taxgriddata','discountdata','issuereceiptid','paymentaccountid','paymentaccountname','cardtype','cardtypename','approvalcode','gwcaculation','ratelesscalc','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagekeylabel','makingchargekeylabel','wastagespanlabel','makingchargespanlabel','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','chitbookno','chitbookname','chitamount','totalpaidamount','chitschemetypeid','balancecounter','rfidtagnumber','taxcategory','prewastage','premakingcharge','lottypeid','lottypeidname','purchasemode','paymenttouch','pureweight','paymentproduct','paymentproductname','paymentpurity','paymentpurityname','paymentcounter','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsize','salescomment','paymentcomment','orderduedate','orderitemratefix','orderitemsizename','ordernumber','paymentemployeepersonid','paymentemployeepersonidname','vendoraccountid','issuereceipttypeidname','issuereceipttypeid','issuereceiptmergetext','ordernumbername','placeordernumber','receiveordernumber','mainsalesdetailid','category','categoryname','oldsalesreturntax','creditadjustvaluehidden','creditadjusthidden','tagimage','prelstcharge','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','flatchargekeylabel','flatchargespanlabel','preflatcharge','untagvalidatemergetext','untagvalidatecountermergetext','approvalnumber','paymentlessweight','paymentpieces','vendororderduedate','ordervendorid','itemwastagevalue','itemmakingvalue','itemflatvalue','itemlstvalue','itemrepairvalue','itemchargesamtvalue']; //LST
			gridfieldhide('oldjewelvouchergrid',hideprodgridcol);
			/* column resize */
			columnresize('oldjewelvouchergrid');
		},
	});	
}
// approval out return 
function approvalreturngriddatafetch(salesnumber,stocktypeid) {
	var salesdetailsarray = [];
	var accid=$("#accountid").val(); 
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['salesdetailid'] != '1' && griddata[i]['salesdetailid'] != ''){
		 salesdetailsarray.push(griddata[i]['salesdetailid']);
		}
	}
	$.ajax({
		url:base_url+"Sales/approvalreturngriddatafetch?salesnumber="+salesnumber+"&stocktypeid="+stocktypeid+"&accountid="+accid+"&billbasedproductid="+salesdetailsarray,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			estimate_loadinlinegriddata('approvalreturngrid',data['estimatedata'].rows,'json','checkbox');
			finalsummarycalc(0);
			//row based check box
			$(".approvalreturngrid_rowchkboxclass").click(function(){
				$('.approvalreturngrid_headchkboxclass').removeAttr('checked');
				approvaloutreturngetcheckboxrowid('approvalreturngrid');
			});
			/* data row select event */
			datarowselectevt();
			var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','gwcaculation','ratelesscalc','hiddenstonedata','taxgriddata','discountdata','issuereceiptid','paymentaccountid','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagespanlabel','makingchargespanlabel','wastagekeylabel','makingchargekeylabel','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','totalchargeamount','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','additionalchargeamt','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','paymentaccountidname','chitbookno','chitbookname','chitamount','totalpaidamount','chitschemetypeid','balancecounter','taxcategory','olditemdetailsname','prewastage','premakingcharge','prelstcharge','lottypeid','lottypeidname','purchasemode','paymenttouch','pureweight','paymentproduct','paymentproductname','paymentpurity','paymentpurityname','paymentcounter','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsize','salescomment','paymentcomment','orderduedate','orderitemratefix','orderitemsizename','ordernumber','paymentemployeepersonid','paymentemployeepersonidname','vendoraccountid','issuereceipttypeidname','issuereceipttypeid','issuereceiptmergetext','ordernumbername','placeordernumber','receiveordernumber','mainsalesdetailid','category','categoryname','oldsalesreturntax','creditadjustvaluehidden','creditadjusthidden','tagimage','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','flatchargekeylabel','flatchargespanlabel','preflatcharge','untagvalidatemergetext','untagvalidatecountermergetext','approvalnumber','paymentlessweight','paymentpieces','chitgram','wastageweight','vendororderduedate','ordervendorid','itemwastagevalue','itemmakingvalue','itemflatvalue','itemlstvalue','itemrepairvalue','itemchargesamtvalue']; // LST
			gridfieldhide('approvalreturngrid',hideprodgridcol);
			/* column resize */
			columnresize('approvalreturngrid');
		},
	});	
}
// order grid data 
function ordergriddatafetch() {
	var ratearray = $('#ratearray').val();
	if(ratearray != '') {
		var result = $.parseJSON(ratearray);
	}
	var orderid = $('#ordernumber').find('option:selected').val();
	if(orderid != '' && orderid != '1'){
		ordergrid();
		var accid=$("#accountid").val(); 
		var ordernumber = $('#ordernumber').find('option:selected').data('ordernumber');
		var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		var selectedstocktypeid = $("#stocktypeid").find('option:selected').val();
		var accountypeid = 1;
		var status = '';
		var stocktypeid = '';
		var substocktypeid = ''
		if(transactiontypeid == 21 || transactiontypeid == 29) {  // place order && Place repair
			status = 2  // after take order
		} else if(transactiontypeid == 20 || transactiontypeid == 27 || transactiontypeid == 30) { // take order (receive) & generate po by vendor
			status = 3;  // after place order
		} else if(transactiontypeid == 11 || transactiontypeid == 16) { // sales & Rough Estimate
			if(selectedstocktypeid == 83) {
				status = 5;  // after receive order
			} else if(selectedstocktypeid == 89) {
				status = 4;  // after receive repair
			}
		}
		if(status == 2) { // Place Order & Place Repair
			if(transactiontypeid == 21) {
				stocktypeid = 75;
			} else if(transactiontypeid == 29) {
				stocktypeid = 86;
			}
		} else if(status == 3) { // take order data
			if(transactiontypeid == 20) {
				stocktypeid = 76;
				substocktypeid = 1;
			} else if(transactiontypeid == 27) {
				accountypeid = $('#selaccounttypeid').val();
				substocktypeid = 85;
				stocktypeid = 76;
			} else if(transactiontypeid == 30) {
				substocktypeid = 1;
				stocktypeid = 87;
			}
		} else if(status == 5) { // receive order data
			stocktypeid = 82;
		} else if(status == 4) {
			stocktypeid = 88;
		}
		var salesdetailsarray = [];
		var griddata = getgridrowsdata('saledetailsgrid');
		var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
		for(var i=0;i< criteriacnt;i++){
			if(griddata[i]['salesdetailid'] != '1' && griddata[i]['salesdetailid'] != ''){
			 salesdetailsarray.push(griddata[i]['salesdetailid']);
			}
		}
		$.ajax({
			url:base_url+"Sales/ordergriddatafetch?ordernumber="+ordernumber+"&status="+status+"&billbasedproductid="+salesdetailsarray+"&stocktypeid="+stocktypeid+"&orderid="+orderid+"&accountid="+accid+"&substocktypeid="+substocktypeid+"&povendormanagement="+povendormanagement+"&selectedstocktypeid="+selectedstocktypeid+"&accountypeid="+accountypeid+"&ratearray="+ratearray+"&poautomaticvendor="+poautomaticvendor,
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				estimate_loadinlinegriddata('ordergrid',data['estimatedata'].rows,'json','checkbox');
				//row based check box
				$(".ordergrid_rowchkboxclass").click(function() {
					$('.ordergrid_headchkboxclass').removeAttr('checked');
					ordergridgetcheckboxrowid('ordergrid');
				});
				/* data row select event */
				datarowselectevt();
				if(selectedstocktypeid == 87 || selectedstocktypeid == 88) {
					var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','gwcaculation','ratelesscalc','hiddenstonedata','taxgriddata','discountdata','issuereceiptid','paymentaccountid','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagespanlabel','makingchargespanlabel','wastagekeylabel','makingchargekeylabel','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','totalchargeamount','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','additionalchargeamt','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','paymentaccountidname','chitbookno','chitbookname','chitamount','totalpaidamount','chitschemetypeid','balancecounter','taxcategory','olditemdetailsname','prewastage','premakingcharge','prelstcharge','itemtagnumber','rfidtagnumber','lottypeid','lottypeidname','purchasemode','paymenttouch','pureweight','paymentproduct','paymentproductname','paymentpurity','paymentpurityname','paymentcounter','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsize','paymentcomment','countername','paymentemployeepersonid','paymentemployeepersonidname','ordernumber','vendoraccountid','issuereceipttypeidname','issuereceipttypeid','issuereceiptmergetext','mainsalesdetailid','category','categoryname','oldsalesreturntax','creditadjustvaluehidden','creditadjusthidden','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','flatchargekeylabel','flatchargespanlabel','preflatcharge','untagvalidatemergetext','untagvalidatecountermergetext','approvalnumber','paymentlessweight','paymentpieces','wastageweight','chitgram','placeordernumber','receiveordernumber','lstcharge','orderitemratefix','orderitemratefix','vendororderduedate','ordervendorid','ordervendoridname','proditemrate','itemcaratweight','orderitemratefixname','repairchargekeylabel','repairchargespanlabel','prerepaircharge','repairchargespan','repairchargeclone','itemwastagevalue','itemmakingvalue','itemflatvalue','itemlstvalue','itemrepairvalue','itemchargesamtvalue']; // Place Repair
				} else {
					var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','gwcaculation','ratelesscalc','hiddenstonedata','taxgriddata','discountdata','issuereceiptid','paymentaccountid','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagespanlabel','makingchargespanlabel','wastagekeylabel','makingchargekeylabel','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','totalchargeamount','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','additionalchargeamt','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','paymentaccountidname','chitbookno','chitbookname','chitamount','totalpaidamount','chitschemetypeid','balancecounter','taxcategory','olditemdetailsname','prewastage','premakingcharge','prelstcharge','itemtagnumber','rfidtagnumber','lottypeid','lottypeidname','purchasemode','paymenttouch','pureweight','paymentproduct','paymentproductname','paymentpurity','paymentpurityname','paymentcounter','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsize','paymentcomment','countername','paymentemployeepersonid','paymentemployeepersonidname','ordernumber','vendoraccountid','issuereceipttypeidname','issuereceipttypeid','issuereceiptmergetext','mainsalesdetailid','category','categoryname','oldsalesreturntax','creditadjustvaluehidden','creditadjusthidden','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','flatchargekeylabel','flatchargespanlabel','preflatcharge','untagvalidatemergetext','untagvalidatecountermergetext','approvalnumber','paymentlessweight','paymentpieces','wastageweight','chitgram','repairchargekeylabel','repairchargespanlabel','prerepaircharge','repairchargespan','repairchargeclone','repaircharge','itemwastagevalue','itemmakingvalue','itemflatvalue','itemlstvalue','itemrepairvalue','itemchargesamtvalue'];//LST
				}
				gridfieldhide('ordergrid',hideprodgridcol);
				/* column resize */
				columnresize('ordergrid');
			},
		});
	} else {
		$("#ordergridoverlay").hide();
		alertpopup('Please select Order details.');
	}
}
//Order items grid overlay
function orderitemsgrid(page,rowcount) {
	var posalesid = $('#posalesid').val();
	var ordertransactionpage = $('#ordertransactionpage').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
	var wwidth = $('#orderitemsgridwidth').width();
	var wheight = $('#orderitemsgridwidth').height();
	/*col sort*/
	var sortcol = $("#orderitemssortcolumn").val();
	var sortord = $("#orderitemssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').attr('id') : '0';	
	var footername = 'sales';
	var filterid = posalesid;
	var conditionname = '';
	var filtervalue = '';
	$.ajax({
		url:base_url+"Sales/ordergridinformationfetch?maintabinfo=sales&primaryid="+posalesid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+'&ordertransactionpage='+ordertransactionpage,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#orderitemsgrid').empty();
			$('#orderitemsgrid').append(data.content);
			$('#orderitemsgridfooter').empty();
			$('#orderitemsgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('orderitemsgrid');
			{//sorting
				$('.orderitemheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.orderitemheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#stonesettinggridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortorder') : '';
					$("#orderitemssortorder").val(sortord);
					$("#orderitemssortcolumn").val(sortcol);
					orderitemsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('orderitemheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					orderitemsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#orderitempgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					orderitemsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$('#orderitempgrowcount').material_select();
			//header check box
			$(".orderitem_headchkboxclass").click(function() {
				checkboxclass('orderitem_headchkboxclass','orderitem_rowchkboxclass');
			});
			//row based check box
			$(".orderitem_rowchkboxclass").click(function() {
				$('.orderitem_headchkboxclass').prop( "checked", false );
			});
		},
	});
}
//innser grid check / Uncheck operation
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").prop('checked', true);
		$("."+headerclass+"").prop('checked', true);
	} else {
		$("."+rowclass+"").prop( "checked", false );
	}
}
// issue/receipt summary
function issuereceiptgriddatafetch(accountid,status) {
	if(accountid > 1 ){
	var salesdetailsarray = [];
	var griddata = getgridrowsdata('issuereceiptlocalgrid');
	var criteriacnt = $('#issuereceiptlocalgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['overlaycreditno'] != ''){
		 salesdetailsarray.push(griddata[i]['overlaycreditno']);
		}
	}
	var salesdetailid = '';
	var paymentstocktypeid = $('#paymentstocktypeid').find('option:selected').val();
	if(paymentstocktypeid == 31 && status == 0 ){salesdetailid = $('#mainsalesdetailid').val();}
	var salesdate = $('#salesdate').val();
	var loadbilltype = $('#loadbilltype').find('option:selected').val();
	var issuereceipttypeid = $('#issuereceipttypeid').find('option:selected').val();
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	var generatesalesid = $('#generatesalesid').val();
	$.ajax({
		url:base_url+"Sales/issuereceiptgriddatafetch?accountid="+accountid+"&salesdate="+salesdate+"&loadbilltype="+loadbilltype+"&transactionmodeid="+transactionmodeid+"&creditno="+salesdetailsarray+"&issuereceipttypeid="+issuereceipttypeid+"&status="+status+"&billeditstatus="+billeditstatus+"&generatesalesid="+generatesalesid+"&salesdetailid="+salesdetailid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			loadinlinegriddata('issuereceiptgrid',data['estimatedata'].rows,'json','checkbox');
			//row based check box
			$(".issuereceiptgrid_rowchkboxclass").click(function(){
				$('.issuereceiptgrid_headchkboxclass').removeAttr('checked');
				issuereceiptgridgetcheckboxrowid('issuereceiptgrid');
			});
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('issuereceiptgrid');
		},
	});	
	}else{
		$("#issuereceiptoverlay").hide();
		alertpopup('Please Select the Account');
	}
} 
function retrivesalesreturn(datarowid) {
	var salesid=$('#billnumberid').val();
	var typeid=$('#stocktypeid').val();
	var storag=$('#counter').val();
	var elementsname=['47','salesdetailid','itemtagid','itemtagnumber','product','purity','grossweight','stoneweight','dustweight','netweight','pieces','ratepergram','hiddenstonedata','ckeyword','ckeywordoriginalvalue','referencenumber','salescomment','chitbookno','totalpaidamount','interestcalc','interestamount','chitbookname','chitamount','prechitbooknumber','stonecharge','discount','totalamount','taxamount','pureweight','bankname','expiredate','paymentreferencedate','paymentrefnumber','paymentmethodid','cardtype','approvalcode','paymentmelting','paymenttouch','netwtcalctype','olditemdetails','rfidtagnumber','modeid','defaultcardid','wastage','wastagespan','makingcharge','makingchargespan','additionalchargeamt'];
	//form field first focus
	firstfieldfocus();
	$.ajax({
		url:base_url+"Sales/retrive",
        data:{primaryid:datarowid,salesid:salesid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data){ 
		    var txtboxname = elementsname;
			var dropdowns = ['3','product','purity','olditemdetails'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$('#taxdetails').val(data.taxdetails);
			$('#wastageclone').val(data.wastagespan);
			$('#makingchargeclone').val(data.makingchargespan);
			$('#totalchargeamount').val(data.additionalchargeamt);
			$('#partialgrossweight').val(data.grossweight);
			$('#generateitemtagid').val(data.itemtagid);
			retrievetagstatus=1;
			puritysetbasedonproduct();
			setTimeout(function() {
					fromcounterhideshow('counter',data.counter);
				},10);
			setTimeout(function() {
				$("#counter").select2('val',data.counter).trigger('change');
			},10);
			if(data.ckeywordvaluedetails != ''){
				var chargekeyworddata=data.ckeywordvaluedetails.split(",");
			 $.each(chargekeyworddata, function (index, value) {
				 var res=value.split(":");
				 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
				 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).val(res[1]);
				 });
			}
			if(data.ckeywordoriginalvaluedetails != ''){
				 var ckeywordoriginalvalue=data.ckeywordoriginalvaluedetails.split(",");
			 $.each(ckeywordoriginalvalue, function (index, value) {
				  var res=value.split(":");
				 $("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
				 $("#productaddonform .chargedetailsdiv span[id="+res[0]).text(res[1]);
			  });
			}
			getstoneentrydiv(datarowid,salesid);
			if(data.ckeywordvaluedetails != '' || data.ckeywordoriginalvaluedetails != ''){
				setTimeout(function() {
					$('#stonecharge').val(parseFloat(data.stonecharge).toFixed(roundamount));
				},10);
			}
			var mcharge = 'makingchargespan';
			$("#salesaddform label span[id="+mcharge).text(data.makingchargespan);
			var wastagecharge = 'wastagespan';
			$("#salesaddform label span[id="+wastagecharge).text(data.wastagespan);
			$('#taxamount').val(parseFloat(data.taxamount).toFixed(roundamount));
			$('#discount').val(parseFloat(data.discount).toFixed(roundamount));
		    $('#grossamount').val(parseFloat(data.grossamount).toFixed(roundamount));
			Materialize.updateTextFields();
		}
	});
}
function stocktypeset(val,transactiontype,stocktypeid){
     $.ajax({
		url: base_url+"Sales/stocktypeset",
		data:{modeid:val,transactiontype:transactiontype},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
		   $('#'+stocktypeid+'').select2('val',msg).trigger('change');
		},
	});
}
function modehideshow(val){
	var stocktypeid = 'modeid';
	var mode = 3;
	if(val == 9 || val == 11 || val == 20){ // sales,purchase & takeorder
		$("#"+stocktypeid+" option[value="+mode+"]").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+mode+"]").prop('disabled',false);
		$("#paymentaddform").removeClass('hidedisplay');
	}else{
		$("#"+stocktypeid+" option[value="+mode+"]").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+mode+"]").prop('disabled',true);
		$("#paymentaddform").addClass('hidedisplay');
	}
}
//get product name based on purity while partial tag
function getpendingproductname(purity) {
	$("#balaceproductid").empty();
	$('#balaceproductid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Sales/pendingproductnamefetch",
		data:{purity:purity},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
		   if(productidshow == 'YES') {
		        $.each(msg, function(index) {
					$("#balaceproductid")
					.append($("<option></option>")
					.attr("data-label",msg[index]['productname'])
					.attr("value",msg[index]['productid'])
					.text(msg[index]['productid']+'-'+msg[index]['productname']));
				});

		   }else{
				$.each(msg, function(index) {
					$("#balaceproductid")
					.append($("<option></option>")
					.attr("data-label",msg[index]['productname'])
					.attr("value",msg[index]['productid'])
					.text(msg[index]['productname']));
				});
			}
		},
	});
}
function flatchargecalc(chargevalue,keyword) {
	var wastagespan = $.trim($('#wastagespan').text());
	var rate = $('#ratepergram').val();
	var calfinalvalue=0;
		switch(keyword) {
			case 'FC-WT':  // FC Weight
				if($('#flatweightwastagespan').val() == 1) {
					var calfinalvalue=(parseFloat(chargevalue)*parseFloat(rate)) + (parseFloat(rate)*parseFloat(chargevalue)*wastagespan/100);
				} else {
					var calfinalvalue=parseFloat(rate)*parseFloat(chargevalue);
				}
				break;
			case 'FC':  // flatcharge 
				var calfinalvalue=parseFloat(chargevalue);
				break;
				default:
		}
	$('#preflatcharge').val(calfinalvalue.toFixed(5));
	calfinalvalue = calfinalvalue.toFixed(roundamount);
	$('#flatcharge,#flatchargecloneconversion').val(calfinalvalue);
	if(chargesdonttriggergrosscalc == 0)
	{totalgrossamountcalculationwork();}
}
function flatchargereversecalc(conversionvalue,keyword) {
		var wastagespan = $.trim($('#wastagespan').text());
		var rate = $('#ratepergram').val();
		var calfinalvalue=0;
			switch(keyword) {
				case 'FC-WT':  // FC Weight
					if($('#flatweightwastagespan').val() == 1) {
						var calfinalvalue = (parseFloat(conversionvalue)*parseFloat(rate)) + (parseFloat(rate)*parseFloat(conversionvalue)*wastagespan/100);
					} else {
						var calfinalvalue = parseFloat(conversionvalue) / parseFloat(rate);
					}
					break;
				case 'FC':  // flatcharge 
					var calfinalvalue = parseFloat(conversionvalue);
					break;
					default:
			  }
		$('#preflatcharge').val(conversionvalue.toFixed(5));
		calfinalvalue = calfinalvalue.toFixed(roundamount);
		$('#flatchargeclone').val(calfinalvalue);
		$('#flatchargespan').text(calfinalvalue);
		$('#flatcharge').val(conversionvalue);
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
}
function makingchargecalc(chargevalue,keyword){
		var grosswt=$("#grossweight").val();	
		var netwt=$("#netweight").val();	
		var pieces = $('#pieces').val();
		var calfinalvalue=0;
			switch(keyword) {
				case 'MC-GM-N':  // MAKING  PER GRAM NET
					var calfinalvalue=parseFloat(netwt)*parseFloat(chargevalue);
					break;
				case 'MC-FT':  // MAKING  FLAT
					var calfinalvalue=parseFloat(chargevalue);
					break;
				case 'MC-PI':  // MAKING  PER PIECE
					var calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
					break;	
				case 'MC-GM-G':  // MAKING  PER GRAM GROSS
					var calfinalvalue=parseFloat(grosswt)*parseFloat(chargevalue);
					break;
					default:
			  }
		  $('#premakingcharge').val(calfinalvalue.toFixed(5));
		  calfinalvalue = calfinalvalue.toFixed(roundamount);
		  $('#makingcharge,#makingcloneconversion').val(calfinalvalue);
		if(chargesdonttriggergrosscalc == 0)
		{totalgrossamountcalculationwork();}
}
function makingchargereversecalc(conversionvalue,keyword) {
	var grosswt=$("#grossweight").val();	
	var netwt=$("#netweight").val();	
	var pieces = $('#pieces').val();
	var calfinalvalue = 0;
		switch(keyword) {
			case 'MC-GM-N':  // MAKING  PER GRAM NET
				var calfinalvalue = parseFloat(conversionvalue) / parseFloat(netwt);
				break;
			case 'MC-FT':  // MAKING  FLAT
				var calfinalvalue = parseFloat(conversionvalue);
				break;
			case 'MC-PI':  // MAKING  PER PIECE
				var calfinalvalue = parseFloat(conversionvalue) / parseFloat(pieces);
				break;	
			case 'MC-GM-G':  // MAKING  PER GRAM GROSS
				var calfinalvalue = parseFloat(conversionvalue) / parseFloat(grosswt);
				break;
				default:
		}
	$('#premakingcharge').val(parseFloat(conversionvalue).toFixed(5));
	calfinalvalue = calfinalvalue.toFixed(roundamount);
	
	$('#makingchargeclone').val(calfinalvalue);
	$('#makingchargespan').text(calfinalvalue);
	$('#makingcharge').val(conversionvalue);
	
	if(chargesdonttriggergrosscalc == 0)
	{totalgrossamountcalculationwork();}
}
function lstchargecalc(chargevalue,keyword){
	var grosswt=$("#itemcaratweight").val();	
	var lstcaratwt=parseFloat($("#itemcaratweight").val());	
	var pieces = $('#pieces').val();
	var calfinalvalue=0;
	switch(keyword) {
		case 'LST-G':
			var calfinalvalue=parseFloat(grosswt)*parseFloat(chargevalue);
			break;
		case 'LST-P':
			var calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
			break;
		case 'LST-C':  
			var calfinalvalue=parseFloat(lstcaratwt)*parseFloat(chargevalue);
			break;	
		default:
	}
	$('#prelstcharge').val(calfinalvalue.toFixed(5));
	calfinalvalue = calfinalvalue.toFixed(roundamount);
	$('#lstcharge').val(calfinalvalue);
	if(chargesdonttriggergrosscalc == 0)
	{totalgrossamountcalculationwork();}
}
function repairchargecalc(chargevalue,keyword) {
	var calfinalvalue = 0;
	switch(keyword) {
		case 'RC':  // Repaircharge 
			var calfinalvalue = parseFloat(chargevalue);
			break;
		default:
	}
	$('#prerepaircharge').val(calfinalvalue.toFixed(5));
	calfinalvalue = calfinalvalue.toFixed(roundamount);
	$('#repaircharge').val(calfinalvalue);
	if(chargesdonttriggergrosscalc == 0)
	{totalgrossamountcalculationwork();}
}
function wastagecalctypewithtouch(chargevalue,keyword) {
	var netwt=$("#netweight").val();
	var grosswt=$("#grossweight").val();	
	var pieces = $('#pieces').val();
	var rate = $('#ratepergram').val();
	var calfinalvalue=0;
		switch(keyword) {
			case 'WS-PT-N':  // WASTAGE PERCENT NET
				var calfinalvaluemelting=(parseFloat(netwt)*(parseFloat(chargevalue)/100));
				var calfinalvalue=(parseFloat(calfinalvaluemelting) * rate);
				$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
				calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
				break;
			case 'WS-WT':  // WASTAGE FLAT WEIGHT
				var calfinalvalue=(parseFloat(chargevalue) * rate);
				$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
				calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);	
				break;
			case 'WS-PT-G':  // WASTAGE PERCENT GROSS
				var calfinalvaluemelting=(parseFloat(grosswt)*(parseFloat(chargevalue)/100));
				var calfinalvalue=(parseFloat(calfinalvaluemelting)* rate);
				$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
				calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
				break;
				default:
		  }
	$('#wastage').val(calfinalvalue).trigger('change');
}

function wastagecalctypewithmelting(chargevalue,keyword){
        var netwt=$("#netweight").val();
		var grosswt=$("#grossweight").val();	
		var pieces = $('#pieces').val();
		var rate = $('#ratepergram').val();
		var melting = checknan($('#melting').val());
		var calfinalvalue=0;
			switch(keyword) {
				case 'WS-PT-N':  // WASTAGE PERCENT NET
					//var calfinalvaluemelting=(parseFloat(netwt)*(parseFloat(melting)/100));
					var calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100) * rate);
					$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
					calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
					break;
				case 'WS-WT':  // WASTAGE FLAT WEIGHT
					var calfinalvalue=(parseFloat(chargevalue) * rate);
					$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
					calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);	
					break;
				case 'WS-PT-G':  // WASTAGE PERCENT GROSS
					//var calfinalvaluemelting=(parseFloat(grosswt)*(parseFloat(melting)/100));
					var calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)* rate);
					$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
					calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
					break;
					default:
			  }
		$('#wastage').val(calfinalvalue).trigger('change');
}
 
function wastagechargecalc(chargevalue,keyword) {
	var netwt=$("#netweight").val();
	var grosswt=$("#grossweight").val();	
	var pieces = $('#pieces').val();
	var rate = $('#ratepergram').val();
	var melting = $('#melting').val();
	var calfinalvalue=0;
	switch(keyword) {
		case 'WS-PT-N':  // WASTAGE PERCENT NET
			var calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100) * rate);
			$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
			calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
			break;
		case 'WS-WT':  // WASTAGE FLAT WEIGHT
			var calfinalvalue=(parseFloat(chargevalue) * rate);
			$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
			calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
			break;
		case 'WS-PT-G':  // WASTAGE PERCENT GROSS
			var calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)* rate);
			$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
			calfinalvalue = parseFloat(calfinalvalue).toFixed(roundamount);
			break;
			default:
	}
	$('#wastagecloneconversion').val(calfinalvalue);
	$('#wastage').val(calfinalvalue).trigger('change');
}
function wastagechargereversecalc(conversionvalue,keyword) {
	var netwt=$("#netweight").val();
	var grosswt=$("#grossweight").val();	
	var pieces = $('#pieces').val();
	var rate = $('#ratepergram').val();
	var melting = $('#melting').val();
	var wastageclonevalue = 0;
	switch(keyword) {
		case 'WS-PT-N':  // WASTAGE PERCENT NET
			var wastageclonevalue = parseFloat((parseFloat(conversionvalue)/(parseFloat(netwt * rate)))*100).toFixed(2);
			if(wastageclonevalue > 100) {
				$('#wastageclone').val(0).trigger('change');
				alertpopupdouble('Please enter percentage value below 100');
				return false;
			} else {
				$('#prewastage').val(parseFloat(conversionvalue).toFixed(5));
			}
			break;
		case 'WS-WT':  // WASTAGE FLAT WEIGHT
			var wastageclonevalue = (parseFloat(conversionvalue) / rate).toFixed(roundweight);
			$('#prewastage').val(parseFloat(conversionvalue).toFixed(5));
			break;
		case 'WS-PT-G':  // WASTAGE PERCENT GROSS
			var wastageclonevalue = parseFloat((parseFloat(conversionvalue)/(parseFloat(grosswt * rate)))*100).toFixed(2);
			if(wastageclonevalue > 100) {
				$('#wastageclone').val(0).trigger('change');
				alertpopupdouble('Please enter percentage value below 100');
				return false;
			} else {
				$('#prewastage').val(parseFloat(conversionvalue).toFixed(5));
			}
			break;
		default:
	}
	$('#wastageclone').val(wastageclonevalue);
	$('#wastagespan').text(wastageclonevalue);
	$('#wastage').val(parseFloat(conversionvalue).toFixed(roundamount));
}
//counter show hide based on the stock type
function countershowhide(ddname,hideval) {
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	if(ddname == 'counter') {
		if(stocktypeid == 19 || stocktypeid == 20 || stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81 || stocktypeid == 73) { // old jewel & sales return & purchase
			$("#"+ddname+" option[data-defparentcounterids=12]").addClass("ddhidedisplay");
			$("#"+ddname+" option[data-defparentcounterids=12]").prop('disabled',true);
		} else {
			$("#"+ddname+" option[data-defparentcounterids=12]").removeClass("ddhidedisplay");
			$("#"+ddname+" option[data-defparentcounterids=12]").prop('disabled',false);
		}
	}
	if(hideval != 0) {
		$("#"+ddname+" option[data-defparentcounterids="+hideval+"]").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-defparentcounterids="+hideval+"]").prop('disabled',false);
	} else {
		$("#"+ddname+" option[data-defcounterids="+hideval+"]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[data-defcounterids="+hideval+"]").prop('disabled',false);
	}
	if(hideval != 0) {
		var i =0;
		var id = 0;
		$("#"+ddname+" option[data-defparentcounterids="+hideval+"]").each(function() {
			id = $(this).val();
			i++; 
		});
		if(i == 1) {
			$("#"+ddname+"").select2('val',id);
			if( ddname == 'balancecounter') {
				$("#balancecounter-div").addClass('hidedisplay');
			} else {
				$("#counter-div").hide();
			}
		} else {
			if( ddname == 'balancecounter') {
				$("#balancecounter-div").removeClass('hidedisplay');
			} else {
				$("#counter-div").show();
			}
		}
	}
}
//tax overlay display
function taxoverlaydisplay(stocktypeid) {
	{// tax grid data fetch
		var taxtypeid = $("#taxtypeid").val();
		if(stocktypeid == '20' || stocktypeid == '19' ){taxtypeid = 2;}
		if(taxtypeid == 2) {
			var g_data = $.trim($('#taxgriddata').val());
		} else {
			var g_data = $.trim($('#summarytaxgriddata').val());
		}
		if(g_data == '') {
			var main_data = '';
		} else {
			var main_data = $.parseJSON(g_data);
		}
	}
	var tax_category_id= 'taxcategory';
	$('#'+tax_category_id+'').select2('focus');
	if(!main_data || main_data == null || main_data == '') {
		$('#'+tax_category_id+'').select2('val','').trigger('change');
	} else {
		var grid_data = main_data.data;
		if (grid_data.length > 0) {
			$('#'+tax_category_id+'').select2('val',main_data.id);
			newloadgriddata('taxgrid',grid_data);
		} 
	}
	$("#taxoverlay").fadeIn();
	if(taxtypeid == 2) {
		tax_data_total();
	}
}
//Bill Level Tax Overlay Display
function billtaxoverlaydisplay() {
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	if((wastagecalctype == 1 || wastagecalctype == 2 || wastagecalctype == 4) && (transactionmodeid == 2 || transactionmodeid == 4)) {
		getsalesdetailtaxcalc(); // Summary tax calculation
	}
	{ // Hide display Tax details in grid.
		$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			$('#billtaxgrid .gridcontent div#'+datarowid+'').removeClass('hidedisplay');
		});
		$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var amountcheck = $('#billtaxgrid .gridcontent div#'+datarowid+' ul .amount-class').text();
			if(amountcheck == 0) {
				$('#billtaxgrid .gridcontent div#'+datarowid+'').addClass('hidedisplay');
			}
		});
	}
	$("#billtaxoverlay").fadeIn();
}
// Bill Level Tax Grid Load
function retrievealltaxdetailinformation() {
	cleargriddata('billtaxgrid');
	var netamount = 0;
	$.ajax({
		url:base_url+'Sales/billtaxmasterload?netamount='+netamount,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				loadinlinegriddata('billtaxgrid',data.rows,'json');
				$('#hiddentaxdata').val(JSON.stringify(data));
				billtax_data_total();
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('billtaxgrid');
				$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
					datarowid = $(e).attr('id');
					billleveltaxdetailids.push($('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxid-class').text());
				});
			}
			Materialize.updateTextFields();
		},
	});
}
// Bill Level Tax Calculation of all items in inner Grid.
function getsalesdetailtaxcalc() {
	var salesstocktype = ['11','12','13','17','75','81','83','86','88','89'];
	var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
	if(transactiontype != 13) {
		for(var i=0; i< billleveltaxdetailids.length; i++) {
			var amount = 0;
			var billtaxgridids = billleveltaxdetailids[i];
			$('#saledetailsgrid .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				var gridstocktype = $('#saledetailsgrid .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
				if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
					var itemhiddentaxdetails = JSON.parse($('#saledetailsgrid .gridcontent div#'+datarowid+' ul .taxgriddata-class').text());
					if(itemhiddentaxdetails.data != '') {
						for ( var j=0; j<itemhiddentaxdetails.data.length; j++) {
							var taxid = itemhiddentaxdetails.data[j].taxid;
							var itemamount = itemhiddentaxdetails.data[j].amount;
							if(billtaxgridids == taxid) {
								amount = (parseFloat(amount) + parseFloat(itemamount)).toFixed(2);
							}
						}
					}
				}
			});
			$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				var taxids = $('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxid-class').text();
				if(billtaxgridids == taxids) {
					$('#billtaxgrid .gridcontent div#'+datarowid+' ul .amount-class').text(amount);
				}
			});
		}
		billtax_data_total();
	}
}
function accountloadcolumnname(moduleid) {
	$("#accountcondcolumn").empty();
	$("#accountcondcolumn").append($("<option value=''></option>"));
	var tabsction = "405";
	$.ajax({
		url:base_url+"Reportsview/loadcolumnname?moduleid="+moduleid+"&tabsction="+tabsction,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {			
			$.each(data, function(index){			
				//calculation field load
				$('#accountcondcolumn')
				.append($("<option></option>")
				.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
				.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
				.attr("data-uitypeid",data[index]['uitypeid'])
				.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
				.attr("data-label",data[index]['viewcreationcolumnname'])
				.attr("value",data[index]['viewcreationcolumnid'])
				.text(data[index]['viewcreationcolumnname']));							
			});			
		}
	});
}
//condition drop down load
function accconditiondropdownload(data) {
	$('#acccondition').empty();
	var accountcondcolumn = $('#accountcondcolumn').find('option:selected').val();
	$.ajax({
		url:base_url+"Base/loadconditionbyuitype?uitypeid="+data, 
		dataType:'json',
		async:false,
		success: function(data) {
			$.each(data, function(index){
				if((accountcondcolumn == '4247' ||  accountcondcolumn == '3008') && data[index]['whereclauseid'] == 17) {	
					
				}else{
					$('#acccondition')
					.append($("<option></option>")
					.attr("value",data[index]['whereclausename'])
					.text(data[index]['whereclausename']));
				}
			});	
		}
	});
}
//filter work for main view module
function accountfilterwork(gridname) {
	var columnid =$("#accountcondcolumn").val();
	var label =$("#accountcondcolumn").find('option:selected').attr('data-label');
	var condition = $("#acccondition").val();
	var value = $("#finalacccondvalue").val();
	var valuename = $("#finalacccondvaluename").val();
	{ // get filterdata for verification
		var fid = $("#accfilterid").val();
		var cname = $("#accconditionname").val();
		var fvalue = $("#accfiltervalue").val();
	}
	var count = accfiltername.length;
	var usecond = label+' is '+condition+' '+valuename;
	$("#accfilterconddisplay").append("<span class='innnerfilterresult'>"+usecond+" <i class='material-icons accfilterdocclsbtn' data-fileid='"+count+"'>close</i></span>");
	if(accfiltername.length>0){
		var filterid = $("#accfilterid").val()+'|'+columnid;
		var filtercondition = $("#accconditionname").val()+'|'+condition;
		var filtervalue = $("#accfiltervalue").val()+'|'+value;
		$("#accfilterid").val(filterid);
		$("#accconditionname").val(filtercondition);
		$("#accfiltervalue").val(filtervalue);
	} else {
		$("#accfilterid").val(columnid);
		$("#accconditionname").val(condition);
		$("#accfiltervalue").val(value);
	}
	accfiltername.push(columnid);
	gridname();
	$(".accfilterdocclsbtn").click(function() {
		var id = $(this).data("fileid");
		var count = accfiltername.length;
		for(var k=0;k<count;k++) {
			if( k == id) {
				//for condition value split
				var ncondition = $("#accconditionname").val();
				var filternewcond = ncondition.split('|');
				accfiltername = jQuery.grep(filternewcond, function(value,vid) {
					return vid != id;
				});
				var fname = filterarrayget(accfiltername);
				console.log(fname);
				$("#accconditionname").val(fname);
				//for filter id split
				var nid = $("#accfilterid").val();
				var filternewid = nid.split('|');
				accfiltercid = jQuery.grep(filternewid, function(idvalue,fid) {
					return fid != id;
				});
				var fid = filterarrayget(accfiltercid);
				console.log(fid);
				$("#accfilterid").val(fid);
				//for filter value
				var nvalue = $("#accfiltervalue").val();
				var filternewvalue = nvalue.split('|');
				accfiltervid = jQuery.grep(filternewvalue, function(fvalue,fvid) {
					return fvid != id;
				});
				var fval = filterarrayget(accfiltervid);
				console.log(fval);
				$("#accfiltervalue").val(fval);
				$(this).parent("span").remove();
				$(this).next("br").remove();
				$(this).remove();
				gridname();
			}
		}
	});
	$("#accountcondcolumn").select2('val','');//filterddcondvalue
	$("#acccondition").select2('val','');
	$("#accddcondvalue").select2('val','');
	$("#acccondvalue").val('');
	$("#finalacccondvalue").val('');
	$("#finalacccondvaluename").val('');
}
//show hide fields based on the comapny setting transaction manage
function showhidecommonfields(transactiontype,modeid) {
	$('#paymentemployeepersonid-div,#employeepersonid-div').addClass('hidedisplay');
	$('#employeepersonid').attr('data-validation-engine','');
	var salespersonid = $('#salespersonid').find('option:selected').val();
	var discounttypeid = $("#sdiscounttypeid").val();
	var employeeid =  $("#hiddenemployeeid").val();
	var discaltype =  $("#discaltype").val();
	var allowedaccounttypeset =  $("#allowedaccounttypeset").val();
	var transactionmanageid = $('#transactionmanageid').find('option:selected').val();
	$.ajax({
		url:base_url+"Sales/showhidecommonfields?transactiontype="+transactiontype+"&modeid="+modeid+"&salespersonid="+salespersonid+"&discaltype="+discaltype+"&discountmode="+discounttypeid+"&employeeid="+employeeid+"&allowedaccounttypeset="+allowedaccounttypeset+"&transactionmanageid="+transactionmanageid,
		dataType:'json',
		async:true,
		cache:false,
		success: function(data) {
			var divid = data['hidden'].toString().split(',');
			defaultvaluearray = data['default'];
			hideshowvaluearray = data['hidden'];
			cashcountervaluearray = data['cashcounter'];
			discountvaluearray = data['discountarray'];
			$('#discountarray').val(JSON.stringify(data['discount']));
			//$('#stocktypeid').select2('val',data['default']).trigger('change');
			salesfieldcommentrequired = 0;
			if(data['hidden'] == '') {
				$('#employeepersonid').attr('data-validation-engine','');
			} else {
				if(divid.length > 0){
					for(j=0;j<(divid.length);j++) {
						$("#"+divid[j]+"").removeClass("hidedisplay");
						if(divid[j] == 'employeepersonid-div') {
							$('#employeepersonid').attr('data-validation-engine','validate[required]');
							salesfieldcommentrequired = data['salesfieldcommentreq'];
						}
					}
				}
			}
			if(data['cashcounter'] != 1){
				$("#cashcounter").select2('val',data['cashcounter']).trigger('change');
			} 
			var resultdiscount = $.parseJSON($('#discountarray').val());
			var discountvaluedata = 'discountvalue';
			var discounttypedata = 'discounttypeid';
			var discountvalue = resultdiscount[discountvaluedata];
			var discounttype = resultdiscount[discounttypedata];
			if(discounttypeid == 3){
					$("#discamt,#billsumarydiscountlimit,#billdiscamt").val(discountvalue);
					$("#discountcalname").text('(0)');
			}else if(discounttypeid == 2){
					
					$("#itemdiscountcalname").text('(0)');
			}
		}
	});
}
//get cash counter based on the user
function getcashcounterdata(salespersonid) {
	$.ajax({
		url:base_url+"Sales/getcashcounterdata?salespersonid="+salespersonid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if(data != 1){
				$("#cashcounter").select2('val',data).trigger('change');
			}
		}
	});
}
//used for set the estimatation data
function setestimatedata(data) {
	var elementsname=['55','salesdetailid','stocktypeid','itemtagid','itemtagnumber','product','purity','counter','grossweight','stoneweight','dustweight','netweight','pieces','ratepergram','hiddenstonedata','ckeyword','ckeywordoriginalvalue','referencenumber','salescomment','chitbookno','totalpaidamount','interestcalc','interestamount','chitbookname','chitschemetypeid','chitamount','prechitbooknumber','chitgram','grambonus','stonecharge','discount','totalamount','taxamount','pureweight','bankname','expiredate','paymentreferencedate','paymentrefnumber','paymentmethodid','cardtype','approvalcode','paymentmelting','paymenttouch','netwtcalctype','olditemdetails','rfidtagnumber','modeid','defaultcardid','wastage','makingcharge','grossamount','wastagespan','makingchargespan','employeepersonid','balaceproductid','flatcharge','flatchargespan'];
	if(data.approvalstatus == 11){
		alertpopup('Approval status closed.So unable to edit sales bill');
		return false;
	}
	var txtboxname = elementsname;
	var dropdowns = ['6','stocktypeid','product','purity','counter','olditemdetails','employeepersonid'];
	textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
	approvalnoeditstatus = 1;
	$('#wastage').val(data.wastage);
	$('#makingcharge').val(data.makingcharge);
	$('#flatcharge').val(data.flatcharge);
	$('#grossamount').val(data.grossamount);
	$('#wastageclone').val(data.wastagespan);
	$('#makingchargeclone').val(data.makingchargespan);
	$('#flatchargeclone').val(data.flatchargespan);
	if(data.stocktypeid == 17 || data.stocktypeid == 80 || data.stocktypeid == 81){
		$('#stocktypeid').select2('val',data.stocktypeid);
		$('#modeid').select2('val',data.modeid);
	}else{
		$('#modeid').select2('val',data.modeid).trigger('change');
		$('#stocktypeid').select2('val',data.stocktypeid).trigger('change');
	}
	$('#paymentmethodid').select2('val',data.paymentmethodid).trigger('change');
	$('#cardtype').select2('val',data.cardtype).trigger('change');
	$('#netwtcalctype').select2('val',data.netwtcalctype).trigger('change');
	$('#defaultcardid').select2('val',data.defaultcardid).trigger('change');
	$('#taxdetails').val(data.taxdetails);
	$('#partialgrossweight').val(data.grossweight);
	if(data.olditemdetails) {
		$("#olditemdetails").select2('val',data.olditemdetails).trigger('change');
	}
	Materialize.updateTextFields();
	$('#generateitemtagid').val(data.itemtagid);
	retrievetagstatus=1;
	puritysetbasedonproduct();
	$('#purity').select2('val',data.purity).trigger('change');
	if(data.stocktypeid == 25 || data.stocktypeid == 77 || data.stocktypeid == 65 || data.stocktypeid == 74 || data.stocktypeid == 66 || data.stocktypeid == 16){
		if(data.referencenumber != ''){
			approvalnoeditstatus = 1;
			$('#approvalnumber').select2('val',data.referencenumber).trigger('change');
		}
	}
	if(data.stocktypeid == 76){
		if(data.referencenumber != ''){
			approvalnoeditstatus = 1;
			loadordernumber(data.stocktypeid,1,data.referencenumber);
			$('#ordernumber').select2('val',data.referencenumber);
			$('#prereferencenumber').val(data.referencenumber);
		}
	}
	if(data.ckeywordvaluedetails != ''){
		var chargekeyworddata=data.ckeywordvaluedetails.split(",");
		$.each(chargekeyworddata, function (index, value) {
			var res=value.split(":");
			$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
			$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).val(res[1]);
		});
	}
	var discount_json ={}; //declared as object
	discount_json.typeid = data.calculationtypeid;
	discount_json.limit = data.itemdiscountlimit;
	discount_json.value = data.discountpercent;
	discount_json.giftnumber = data.giftnumber;
	discount_json.giftremark = data.giftremark;
	discount_json.giftdenomination = data.giftdenomination;
	discount_json.giftunit = data.giftunit;
	$('#discountdata').val(JSON.stringify(discount_json));
	if(data.ckeywordoriginalvaluedetails != ''){
		var ckeywordoriginalvalue=data.ckeywordoriginalvaluedetails.split(",");
		$.each(ckeywordoriginalvalue, function (index, value) {
			var res=value.split(":");
			$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
			$("#productaddonform .chargedetailsdiv span[id="+res[0]).text(res[1]);
		});
	}
	if(data.otherdetailsvalue != ''){
		var otherdetailsvalue=data.otherdetailsvalue.split(",");
		$.each(otherdetailsvalue, function (index, value) {
			$("#productaddonform input .extracalc").val(value);
		});
	}
	retrievestatus=1;
	getstoneentrydiv(data.salesdetailid,data.salesid);
    if(data.ckeywordvaluedetails != '' || data.ckeywordoriginalvaluedetails != ''){
		setTimeout(function() {
		},10);
		setTimeout(function() {
			$('#stonecharge').val(parseFloat(data.stonecharge).toFixed(roundamount));
			$('#taxamount').val(parseFloat(data.taxamount).toFixed(roundamount));
			$('#discount').val(parseFloat(data.discount).toFixed(roundamount));
			$("#counter").select2('val',data.counter).trigger('change');
		},10);
	}
   setTimeout(function() {
		$('#grossweight').val(parseFloat(data.grossweight).toFixed(roundweight));
		$('#stoneweight').val(parseFloat(data.stoneweight).toFixed(roundweight));
		$('#netweight').val(parseFloat(data.netweight).toFixed(roundweight));
		$('#ratepergram').val(parseFloat(data.ratepergram).toFixed(roundamount));
		$('#grossamount').val(parseFloat(data.grossamount).toFixed(roundamount));
		$('#stonecharge').val(parseFloat(data.stonecharge).toFixed(roundamount));
		$('#employeepersonid').select2('val',data.employeepersonid); 
		$('#taxamount').val(parseFloat(data.taxamount).toFixed(roundamount));
		$('#discount').val(parseFloat(data.discount).toFixed(roundamount));
		$("#balaceproductid").select2('val',data.pendingproductid).trigger('change');
        var mcharge = 'makingchargespan';
		$("#salesaddform label span[id="+mcharge).text(data.makingchargespan);
		var wastagecharge = 'wastagespan';
		$("#salesaddform label span[id="+wastagecharge).text(data.wastagespan);
		var flatchargespan = 'flatchargespan';
		$("#salesaddform label span[id="+flatchargespan).text(data.flatchargespan);
		$('#stonetotdiaweight').text(parseFloat(getgridcolvaluewithcontition('stoneentrygrid','','caratweight','sum','','')).toFixed(2));
	},100);
	/* var tranaction = $('#salestransactiontypeid').find('option:selected').val();
	var rateaddition = $('#salestransactiontypeid').find('option:selected').data('rateaddition');
	if(tranaction == 11 || tranaction == 16){ // sales & rough estimate
		if(rateaddition == 'Yes'){
			$('#rateclone').val(data.rateclone);
			$('#rateaddition').val(data.rateaddition);
		}
	} */
}
function stonechargehideshow(){
	var stone=$('#product').find('option:selected').data('stoneapplicable');
	var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
	var stocktypeid=$('#stocktypeid').find('option:selected').val();
	if(stocktypeid == 12) { // untag
		if(untagstoneenable == '0') { 
			$("#stoneweight").attr('readonly',true);
			$('#stonecharge').val(0);
			$('#stonecharge-div').hide();
			$("#stoneoverlayicon").hide();
		}else if(untagstoneenable == '1') { 
			$("#stoneweight").attr('readonly',true);
			$('#stonecharge-div').show();
			$("#stoneoverlayicon").show();
		}
	}else{
		if(stone == 'Yes') {
			if(salestransactiontypeid == 13) {
				//$("#stoneweight").attr('readonly',true);
				$('#stoneweight-div').show();
				$('#stonecharge').val(0);
				$('#stonecharge-div').hide();
				$("#stoneoverlayicon").hide();
			}else if(salestransactiontypeid == 19) {
				//$("#stoneweight").attr('readonly',true);
				$('#stoneweight-div').show();
				$('#stonecharge').val(0);
				$('#stonecharge-div').hide();
				$("#stoneoverlayicon").hide();
			}else if(salestransactiontypeid == 9) {
				$("#stoneweight").attr('readonly',false);
				$('#stoneweight-div').show();
				$('#stonecharge').val(0);
				$('#stonecharge-div').show();
				$("#stoneoverlayicon").hide();
			}else {
				$("#stoneweight").attr('readonly',true);
				$('#stoneweight-div').show();
				if(salestransactiontypeid != 18) { // approval out
					$('#stonecharge-div').show();
				}
				/* if(stocktypeid == 20) {
					$("#stoneoverlayicon").hide();
				}else{ */
					if(salestransactiontypeid != 18 && salestransactiontypeid != 9) { // approval out
						$("#stoneoverlayicon").show();
					}
				//}
			}
		} else if(salestransactiontypeid == 9) {
			if(checkVariable('product') == true && stone == 'No') {
				$("#stoneweight").attr('readonly',false);
				$('#stoneweight-div').hide();
				$('#stonecharge').val(0);
				$('#stonecharge-div').hide();
				$("#stoneoverlayicon").hide();
			} else {
				$("#stoneweight").attr('readonly',false);
				$('#stoneweight-div').show();
				$('#stonecharge').val(0);
				$('#stonecharge-div').show();
				$("#stoneoverlayicon").hide();
			}
		} else {
			$("#stoneweight").attr('readonly',false);
			$('#stoneweight-div').hide();
			$('#stonecharge').val(0);
			$('#stonecharge-div').hide();
			$("#stoneoverlayicon").hide();
		}
	}
}
function extrachargehideshow(chargeid){
	if( typeof chargeid === 'undefined' || chargeid === null ){
		$('#cdicon-div').hide();
	}else{
		var extrachargeiddata = ['8','9','11'];
		var extrachargeid=chargeid.toString();
		var extrachargedetails=extrachargeid.split(",");
		var extrastatus = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(extrachargedetails == ''){
		} else {
			$.each(extrachargedetails, function (index, value) {
				if(jQuery.inArray( value, extrachargeiddata ) == -1) {  
				} else {
					 extrastatus = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#cdicon-div').hide();
			} else if(extrastatus == 1){
				$('#cdicon-div').show();
			} else {
				$('#cdicon-div').hide();
			}	
		}
	}
}
// Repair Charges applicable for only Take Repair Stock type
function repairchargehideshow(stocktypeid) {
	if( typeof stocktypeid === 'undefined' || stocktypeid === null ){
		$('#cdicon-div').hide();
	} else {
		var repairchargeiddata = ['20'];
		if(stocktypeid == 86 || stocktypeid == 88 || stocktypeid == 89) {
			$('#repaircharge-div').show();
			$('#repairchargekeylabel').text('RC');
			$('#repairchargespanlabel').val('RC');
		} else {
			$('#repaircharge-div').hide();
			$('#repairchargekeylabel').text('');
			$('#repairchargespanlabel').val('');
		}
	}
}
function flatchargehideshow(chargeid) {//for flat charge_json
	if( typeof chargeid === 'undefined' || chargeid === null ){
		$('#cdicon-div').hide();
	}else{
		var flatchargeiddata = ['12','19'];
		var flatchargeid=chargeid.toString();
		var flatchargedetails=flatchargeid.split(",");
		var flatstatus = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(flatchargedetails == '') {
		} else {
			$.each(flatchargedetails, function (index, value) {
				if(jQuery.inArray( value, flatchargeiddata ) == -1) {  
				} else {
					flatstatus = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#flatcharge-div').hide();
			} else if(flatstatus == 1) {
				$('#flatcharge-div').show();
			} else {
				$('#flatcharge-div').hide();
			}
		}
	}
}
function makingchargehideshow(chargeid) {	//for making charge
	if( typeof chargeid === 'undefined' || chargeid === null ){
		$('#cdicon-div').hide();
	}else{
		var makingchargeiddata = ['2','3','4','14'];
		var makingchargeid=chargeid.toString();
		var makingchargedetails=makingchargeid.split(",");
		var makingstatus = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(makingchargedetails == ''){
		} else {
			$.each(makingchargedetails, function (index, value) {
				if(jQuery.inArray( value, makingchargeiddata ) == -1) {  
				} else {
					 makingstatus = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#makingcharge-div').hide();
			} else if(makingstatus == 1){
				$('#makingcharge-div').show();
			} else {
				$('#makingcharge-div').hide();
			}	
		}
	}
}
function lstchargehideshow(chargeid) {	//for lst charge
	if( typeof chargeid === 'undefined' || chargeid === null ) {
		$('#cdicon-div').hide();
	} else {
		var lstchargeiddata = ['16','17','18'];
		var lstchargeid=chargeid.toString();
		var lstchargedetails=lstchargeid.split(",");
		var lststatus = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(lstchargedetails == '') {
		} else {
			$.each(lstchargedetails, function (index, value) {
				if(jQuery.inArray( value, lstchargeiddata ) == -1) {  
				} else {
					lststatus = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#lstcharge-div').hide();
			} else if(lststatus == 1) {
				$('#lstcharge-div').show();
			} else {
				$('#lstcharge-div').hide();
			}
		}
	}
}
function wastagechargehideshow(chargeid) {	//for wastage charge
	if( typeof chargeid === 'undefined' || chargeid === null ){
		$('#cdicon-div').hide();
	} else {
		var chargeiddata = ['5','6','7'];
		var cchargeid=chargeid.toString();
		var chargedetails=cchargeid.split(",");
		var status = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(chargedetails == '') {
		} else {
			$.each(chargedetails, function (index, value) {
				if(jQuery.inArray( value, chargeiddata ) == -1) {  
				} else {
					status = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#wastage-div').hide();
				wastagevisible = 0;
			} else if(status == 1 && wastagecalctype != 5) {
				$('#wastage-div').show();
				wastagevisible = 1;
			} else if(wastagecalctype == 3) {
				$('#wastage-div').show();
				wastagevisible = 1;
			} else {
				$('#wastage-div').hide();
				wastagevisible = 0;
			}
		}
	}
}
function paymenttouchhideshow(chargeid) { //for paymentouch charge
	if( typeof chargeid === 'undefined' || chargeid === null ){
		$('#cdicon-div').hide();
	}else{
		var chargeiddata = ['15'];
		var cchargeid=chargeid.toString();
		var chargedetails=cchargeid.split(",");
		var status = 0;
		var stocktype = $('#stocktypeid').find('option:selected').val();
		stocktype = typeof stocktype == 'undefined' ? '1' : stocktype;
		if(chargedetails == '') {
		} else {
			$.each(chargedetails, function (index, value) {
				if(jQuery.inArray( value, chargeiddata ) == -1) {  
				} else {
					status = 1;
				}
			});
			if(stocktype == 86 || stocktype == 88 || stocktype == 89) {
				$('#paymenttouch-div').hide();
			} else if(status == 1 && wastagecalctype != 5) {
				$('#paymenttouch-div').show();
			} else if(wastagecalctype == 3 || wastagecalctype == 5) {
				$('#paymenttouch-div').show();
			} else {
				$('#paymenttouch-div').hide();
			}
		}
	}
}
function setwastageweight() {
	var grosswt = $('#grossweight').val();
	var stonewt = $('#stoneweight').val();
	var wastagewt = $('#wastageweight').val();
	var netwt = (parseFloat(grosswt)-parseFloat(stonewt)+parseFloat(wastagewt)).toFixed(round);
	$('#netweight').val(netwt);
}
function loadproductfromstock(stocktype,productid) {
	$("#"+productid+" option").addClass("ddhidedisplay");
	$("#"+productid+" optgroup").addClass("ddhidedisplay");
	$("#"+productid+" option").prop('disabled',true); 
	$("#"+productid+"").select2('val','');
	$.ajax({
		url: base_url +"Sales/loadproductfromstock",
		data:{stocktype:stocktype,branchid:loggedinbranchid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) {
			if($.trim(msg.productid) != '') {
				fromcounterhideshow(productid,msg.productid);
			} else {
				$("#"+productid+"").select2('val','');
		   }
		}
	});
}
function fromcounterhideshow(stocktypeid,counterid) {
	if(counterid != '') {
		if (counterid.indexOf(',') > -1) {
			var val = counterid.split(',');
			for(j=0;j<(val.length);j++) {
				$("#"+stocktypeid+" option[value="+val[j]+"]").removeClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value="+val[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value="+val[j]+"]").prop('disabled',false);
			}
			if(stocktypeid == 'counter') {
				$('#counter').prop('disabled',false);
			}
			if(stocktypeid == 'purity') {
				$('#purity').prop('disabled',false);
			}
		} else {
			$("#"+stocktypeid+" option[value="+counterid+"]").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value="+counterid+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value="+counterid+"]").prop('disabled',false);
			$("#"+stocktypeid+"").select2('val',counterid).trigger('change');
			if(stocktypeid == 'counter') {
				$('#counter').prop('disabled',true);
			}
			if(stocktypeid == 'purity') {
				$('#purity').prop('disabled',true);
			}
		}
	} else {
		
	}
}
function oldjewelhideshow(stocktypeid,categoryid) {
	$("#"+stocktypeid+" option").addClass("ddhidedisplay");
	$("#"+stocktypeid+" optgroup").addClass("ddhidedisplay");
	$("#"+stocktypeid+" option").prop('disabled',true);
	$("#"+stocktypeid+" option[data-categoryid="+categoryid+"]").removeClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-categoryid="+categoryid+"]").closest('optgroup').removeClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-categoryid="+categoryid+"]").prop('disabled',false);
}
function oldjewelsshowallproducts(stocktypeid) {
	$("#"+stocktypeid+" option[data-categoryid='5']").addClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-categoryid='5']").closest('optgroup').addClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-categoryid!='5']").removeClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-categoryid!='5']").closest('optgroup').removeClass("ddhidedisplay");
	$("#"+stocktypeid+" option").prop('disabled',false);
}
function getpurity(stocktype,product,purityid) {
    $("#"+purityid+" option").addClass("ddhidedisplay");
	$("#"+purityid+" optgroup").addClass("ddhidedisplay");
	$("#"+purityid+" option").prop('disabled',true);
	$("#"+purityid+"").select2('val','');
	if(product > 0) {
		$.ajax({
			url: base_url +"Sales/getpurity",
			data:{stocktype:stocktype,product:product,branchid:loggedinbranchid},
			type: "POST",
			async:false,
			dataType:'json',
			success: function(msg) {   
				if($.trim(msg.purityid) != '') {
					fromcounterhideshow(purityid,msg.purityid);
				}
			}
		});
	} else {
		fromcounterhideshow(purityid,'');
	}
}
function getcounter(stocktypeid,product,purity,counterid) {
    $("#"+counterid+" option").addClass("ddhidedisplay");
	$("#"+counterid+" optgroup").addClass("ddhidedisplay");
	$("#"+counterid+" option").prop('disabled',true);
	$("#"+counterid+"").select2('val','');
	if(product > 0 && purity > 0){
		$.ajax({
			url: base_url +"Sales/getcounter",
			data:{stocktypeid:stocktypeid,product:product,purity:purity,branchid:loggedinbranchid},
			type: "POST",
			async:false,
			dataType:'json',
			success: function(msg) {   
				if($.trim(msg.counterid) != '') {
				  fromcounterhideshow(counterid,msg.counterid);
			   }
			}
		});
	} else {
		fromcounterhideshow(counterid,'');
	}
}

function taxautocalculation(stocktypeid) {
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	var salestaxtypeid = $("#taxtypeid").val();
	if(stocktypeid == '20' || stocktypeid == '19' ) {
		salestaxtypeid = 2;
	}
	if(salestaxtypeid == '2') {
		var taxcategoryid = $.trim($('#taxcategory').find('option:selected').val());
		if(taxcategoryid != '') {
			var taxdataid = taxcategoryid;
		} else {
			var taxdetails = $('#product').find('option:selected').data('taxid');
			var categorytaxid = $('#product').find('option:selected').data('categorytaxid');
			if(categorytaxid == '1') { 
				var taxdataid = $('#product').find('option:selected').data('taxmasterid');
			} else { // category - if gst tax map
				var taxdataid = categorytaxid;
			}
			$('#taxdetails').val(taxdetails);
			$('#taxcategory').select2('val',taxdataid).trigger('change');
		}
	} else {
		var taxdetails = '';
		var taxcategoryid = $.trim($('#taxcategory').find('option:selected').val());
		if(taxcategoryid != '') {
			var taxdataid = taxcategoryid;
		} else {
			$('#taxdetails').val('');
			$('#taxcategory').select2('val','').trigger('change');
		}
	}
	if(taxdataid == 1 || taxdataid == '') {
		$('#taxdetails').val('');
		$('#taxcategory').select2('val','').trigger('change');
	}
	if(stocktypeid != 20 && (wastagecalctype != 5 || wastagecalctype != 3) && (transactionmodeid != 3)) {
		submittaxgrid(stocktypeid);
	}
}
function checkdiscountpassword(password) {
	var employeeid = $('#salespersonid').val();
	$.ajax({
		url: base_url +"Sales/checkdiscountpassword",
		data:{password:password,employeeid:employeeid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) {
			if(msg['status'] == 'success') {
				$('#discountpwdoverlay').hide();
				$('#discountpassword').val('');
				//discountoverlayshow();
			} else {
				$('#discountpassword').val('');
				alertpopupdouble('Invalid Password');
			}
		}
	});
}
function getdiscountvalue(type,employeeid,salesdiscounttypeid) {
	$.ajax({
		url: base_url +"Sales/getdiscountvalue",
		data:{discounttype:type,employee:employeeid,salesdiscounttypeid:salesdiscounttypeid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) {
			if(msg['discountvalue'] == '') {
				$('#discounttypeid').select('val','');
				alertpopupdouble('Kindly set discount limit in discount authorization for this discount type');
				return false;
			} else {
				$('#discountvaluehidden').val(msg['discountvalue']);
				$('#itemdiscountlimit').val(msg['discountvalue']);
			}
		}
	});
}
function roundoff(value) {
	var roundingtype = $('#salestransactiontypeid').find('option:selected').data('roundingtype');
	var roundoffremove = $('#salestransactiontypeid').find('option:selected').data('roundoffremove');
	var finalvalue = 0;
	if(roundingtype == 1) { // normal round
       finalvalue = Math.round10(value,0);
	} else if(roundingtype == 2) { // upward round
		finalvalue = Math.ceil10(value,0);
	} else if(roundingtype == 3) { // downward round
		finalvalue = Math.floor10(value,0);
	} else {
		finalvalue = Math.round10(value,0);
	}
	if(roundoffremove == 1) { // round off zero remove after decimal
	    return finalvalue;
	} else {
		if(roundamount == '0') {
			var result =finalvalue;
		} else if(roundamount == '1') {
			var result =finalvalue+".0";
		} else if(roundamount == '2') {
			var result =finalvalue+".00";
		} else if(roundamount == '3') {
			var result =finalvalue+".000";
		} else if(roundamount == '4') {
			var result =finalvalue+".0000";
		} else if(roundamount == '5') {
			var result =finalvalue+".00000";
		}
		return result;
	}
}
function paymentaccounthideshow(ddname,stktype) {
	$("#"+ddname+"").select2('val','');
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	var hideval = '';
	if(stktype == 26) { // cash
		hideval = '6';
	} else if(stktype == 27 || stktype == 28 || stktype == 29 || stktype == 79) { //dd,card,cheque,NEFT/IMPS 
		hideval = '7,9,10';
	}
	if(hideval.indexOf(',') > -1) {
		var val = hideval.split(',');
		for(j=0;j<(val.length);j++) {
			$("#"+ddname+" option[data-accountcatalogid="+val[j]+"]").removeClass("ddhidedisplay");	
		   $("#"+ddname+" option[data-accountcatalogid="+val[j]+"]").prop('disabled',false);
		}
	} else {
		$("#"+ddname+" option[data-accountcatalogid="+hideval+"]").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-accountcatalogid="+hideval+"]").prop('disabled',false);
	}
}
function generatebillnumber(val,transactionmode) {
	$.ajax({
		url: base_url +"Sales/generatebillnumber",
		data:{type:val,transactionmode:transactionmode},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) {
			$('#salesnumber').val(msg);
		}
	});
}
function wastagecalctypewithnetweight(val,keyword) {
	$('#wastage').val(0);
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var pieces = $('#pieces').val();
	var netwt = $('#netweight').val();
	var stonewt = $('#stoneweight').val();
	var grosswt = $('#grossweight').val();
	var finalnetwt = parseFloat(netwt)+parseFloat(val);
	switch(keyword) {
		case 'WS-PT-N':  // WASTAGE PERCENT NET
			var calfinalvalue=(parseFloat(netwt)*(parseFloat(val)/100));  
			break;
		case 'WS-WT':  // WASTAGE FLAT WEIGHT
			var calfinalvalue=parseFloat(val); 
			break;
		case 'WS-PT-G':  // WASTAGE PERCENT GROSS
			var calfinalvalue=(parseFloat(grosswt)*(parseFloat(val)/100)); 
			break;
		default:
			var calfinalvalue=0;
	}
	$('#prewastage').val(parseFloat(calfinalvalue).toFixed(5));
	$('#wastage').val(parseFloat(calfinalvalue).toFixed(roundweight));
	if(chargesdonttriggergrosscalc == 0)
	{totalgrossamountcalculationwork();}
}
{/* alertpopup */
	function moduleeditoralertmsg(idname,msg,finalval) {
		$('#creditnogenerationoverlay').fadeIn();
		$('.oldpurchasecredittypecls').hide();
		if(finalval == 1) {
			$('.oldpurchasecredittypecls').show();
			$('#oldpurchasecredittypeid').select2('val',1).trigger('change');
		}
		$('.alert-message ').text(msg);
		$('.commodyescls').val('Yes');
		$('.commodyescls').attr('id',idname);
		$(".commodyescls").focus();
	}
	// Cancel Overlay not from base
	function moduleeditoralertmsgcancel(idname,msg) {
		$('#basecanceloverlayforcommodule').fadeIn();
		$('.alert-message ').text(msg);
		$('.commodyescls').val('Yes');
		$('.commodyescls').attr('id',idname);
		$(".commodyescls").focus();
	}
}
// bill cancel
function billcancel(datarowid,cancelledcomment) {
	$.ajax({
		url: base_url + "Sales/Salesbillcancel?primaryid="+datarowid+"&cancelledcomment="+cancelledcomment,
		success: function(msg)  {
			var nmsg =  $.trim(msg);
			salesrefreshgrid();
			$("#basecanceloverlayforcommodule").fadeOut();
			if (nmsg == "SUCCESS") {
				alertpopup('Cancelled successfully');
			} else  {
				alertpopup(submiterror);
			}				
		},
	});
}
//discount limit fetch
function discountlimitfetch(discountmode) {
	if(discountmode) {
		var employeeid =  $("#hiddenemployeeid").val();
		var discaltype =  $("#discaltype").val();
		$.ajax({
			url: base_url +"Sales/discountlimitfetch",
			data:{discountmode:discountmode,discaltype:discaltype,employeeid:employeeid},
			type: "POST",
			async:false,
			dataType:'json',
			success: function(msg) {
				for(j=0;j<(msg.length);j++) {
					$("#discamt").val(msg[j].discountvalue);
					if(discountmode == 3 || discountmode == 4) {
						if(discountoperation == 0) {
							$("#sumarydiscountlimit").val(msg[j].discountvalue);
						}
						if(msg[j].discounttypeid == 2) { //direct
							$("#discountcalname,#itemdiscountcalname").text('(D)');
						} else if(msg[j].discounttypeid == 3) { //percent
							$("#discountcalname,#itemdiscountcalname").text('(%)');
						}
					}
				}
			}
		});
	}
}
function billdiscountcalculation() {
	setzero(['billgrossamount','discountcalc','sumarytaxamount','presumarydiscountamount','sumarydiscountamount','discamt']);
	var dtype = $('#sdiscounttypeid').val();
	var discaltype = $("#discaltype").val();
	var discountcalc = $("#discountcalc").val();
	var finaldiscountamt = 0;
	var discaltypedesc = '';
	if(discaltype == 3) { 
	var discaltypedesc = ' %';
	}
	if(dtype == 3) { //if not itemwise discount
		var grossamt = $("#billgrossamount").val();
		var sumarytaxamount = $("#sumarytaxamount").val();
		var oldjewelamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','19'));
		var salesreturnamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','20'));
		var salesreturnroundoffamt = roundoff(salesreturnamt);
		var diff_salesreturn = salesreturnroundoffamt - salesreturnamt;
		$('#summarysalesreturnroundvalue').val(diff_salesreturn.toFixed(roundamount));
		salesreturnamt = parseFloat(salesreturnamt) + parseFloat(diff_salesreturn);
		var discamt = $("#billdiscamt").val();
			if(grossamt != '' || grossamt != 0) {
				var checkval = $("#billsumarydiscountlimit").val();
				if(parseFloat(checkval) <= parseFloat(discamt)) {
					if(discaltype == 2) { //direct
						if(discountcalc == 1) { //before tax
							var discount = parseFloat(checkval);
							var gamount = parseFloat(grossamt);
							if(discount >= gamount) {
								finaldiscountamt = gamount;
							}else{
								finaldiscountamt = discount;
							}
							$("#presumarydiscountamount").val(Math.abs(finaldiscountamt).toFixed(roundamount));
							$("#discountcalname").text('('+ Math.abs(finaldiscountamt).toFixed(roundamount)+')');
						} else { // after tax
							var discount = parseFloat(checkval);
							var gamount = parseFloat(grossamt)+parseFloat(sumarytaxamount);
							if(discount >= gamount) {
								finaldiscountamt = gamount;
							}else{
								finaldiscountamt = discount;
							}
							$("#presumarydiscountamount").val(Math.abs(finaldiscountamt).toFixed(roundamount));
							$("#discountcalname").text('('+ Math.abs(finaldiscountamt).toFixed(roundamount)+')');
						}
					} else if(discaltype == 3) { //percentage
						if(discountcalc == 1) {//before tax
							var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
							if(salestransactiontypeid == 11 ||  salestransactiontypeid == 16 || salestransactiontypeid == 20) {
								if(discountbilllevelpercent == 0) {
									var percentant = ((checkval/100)*grossamt);
								} else if(discountbilllevelpercent == 2) {
									var count = $('#saledetailsgrid .gridcontent div.data-content div.data-rows').length;
									var ratepergram = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','17')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','sum','stocktypeid','83'));
									var ratepergramcount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','ratepergram','count','stocktypeid','83'));
									var netweight = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','12')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','13')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','16')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','83'));
									var finalratenetwt =  parseFloat((ratepergram/ratepergramcount) * netweight);
									var percentant = ((checkval/100)*finalratenetwt);
								} else if(discountbilllevelpercent == 1) {
									var wastageamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','13')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','wastage','sum','stocktypeid','83'));
									var percentant = ((checkval/100)*wastageamount);
								}
								$("#presumarydiscountamount").val(Math.abs(percentant).toFixed(roundamount));
								$("#discountcalname").text('('+ Math.abs(percentant).toFixed(roundamount)+')');
							} else {
								var percentant = ((checkval/100)*grossamt);
								$("#presumarydiscountamount").val(Math.abs(percentant).toFixed(roundamount));
								$("#discountcalname").text('('+ Math.abs(percentant).toFixed(roundamount)+')');
							}
						} else { // after tax
							var gamount = parseFloat(grossamt)+parseFloat(sumarytaxamount);
							var percentant = ((checkval/100)*gamount);
							var discount = parseFloat(percentant);
							$("#presumarydiscountamount").val(Math.abs(discount).toFixed(roundamount));
							$("#discountcalname").text('('+ Math.abs(discount).toFixed(roundamount)+')');
						}
					}
					if(discountauthorization == 0) {
						$("#discountcalname").text('('+ Math.abs(grossamt).toFixed(roundamount)+')');
					}
				} else {
					$("#sumarydiscountamount").val(0);
					alertpopup('You cant exceed the discount than authorized.Please enter value less than limit '+discamt+""+discaltypedesc+"");
					return false;
				}
				
				
			} else {
				alertpopup('Please enter data in sales details');
				$("#billsumarydiscountlimit").val(parseFloat(discamt).toFixed(roundamount));
				$("#billsumarydiscountlimit").trigger('change');
			}
	} else if(dtype == 2) {  // item level discount
	    if($("#grossamount").val() == ''){
			var grossamt = 0;
		}else{
			var grossamt = $("#grossamount").val();
		}
		if($("#taxamount").val() == ''){
			var sumarytaxamount = 0;
		}else{
			var sumarytaxamount = $("#taxamount").val();
		}
		//if(discountauthorization == 1) { //if discount autorization is yes in company settings
			var discamt = $("#discamt").val();
			//if(grossamt != '' || grossamt != 0) {
				var checkval = $("#sumarydiscountlimit").val();
				if(parseFloat(checkval) <= parseFloat(discamt)) {
					if(discaltype == 2) { //direct
						if(discountcalc == 1) { //before tax
							var discount = parseFloat(checkval);
							var gamount = parseFloat(grossamt);
							if(discount >= gamount) {
								finaldiscountamt = gamount;
							}else{
								finaldiscountamt = discount;
							}
							$("#discountdata").val(parseFloat(finaldiscountamt).toFixed(roundamount));
							$("#itemdiscountcalname").text('(' + parseFloat(finaldiscountamt).toFixed(roundamount)+')');
						} else { // after tax
							var discount = parseFloat(checkval);
							var gamount = parseFloat(grossamt)+parseFloat(sumarytaxamount);
							if(discount >= gamount) {
								finaldiscountamt = gamount;
							}else{
								finaldiscountamt = discount;
							}
							$("#discountdata").val(parseFloat(finaldiscountamt).toFixed(roundamount));
							if(parseFloat($("#discount").val()) > parseFloat(finaldiscountamt)) 
							{
								$("#discount").val(0).trigger('change');
							}
							$("#itemdiscountcalname").text('(' + parseFloat(finaldiscountamt).toFixed(roundamount)+')');
						}
						
					} else if(discaltype == 3) { //percentage
						if(discountcalc == 1) {//before tax
							var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
							if(salestransactiontypeid == 11 ||  salestransactiontypeid == 16 || salestransactiontypeid == 20) {
								if(discountbilllevelpercent == 0) {
									var percentant = ((checkval/100)*grossamt);
								} else if(discountbilllevelpercent == 2) {
									var netwt = $('#netweight').val();
									var ratepergram = $('#ratepergram').val();
									var finalratenetwt =  parseFloat(ratepergram * netwt);
									var percentant = ((checkval/100)*finalratenetwt);
								} else if(discountbilllevelpercent == 1) {
									var wastageamt = $('#wastage').val();
									var percentant = ((checkval/100)*wastageamt);
								}
							} else {
								var percentant = ((checkval/100)*grossamt);
							}
							$("#discountdata").val(parseFloat(percentant).toFixed(roundamount));
							$("#itemdiscountcalname").text('('+parseFloat(percentant).toFixed(roundamount)+')');
					
						} else { // after tax
							var gamount = parseFloat(grossamt)+parseFloat(sumarytaxamount);
							var percentant = ((checkval/100)*gamount);
							var discount = percentant;
							$("#discountdata").val(parseFloat(discount).toFixed(roundamount));
							if(parseFloat($("#discount").val()) > (parseFloat(discount).toFixed(roundamount))) 
							{
								$("#discount").val(0).trigger('change');
							}
							$("#itemdiscountcalname").text('('+parseFloat(discount).toFixed(roundamount)+')');
						}
					}
				} else {
					$("#discount").val(0);
					alertpopup('You cant exceed the discount than authorized.Please enter value less than limit '+discamt+""+discaltypedesc+"");
					return false;
				}
	}
}
// size hide show based on product
function sizehideshow(productvalue) {
	var sizeapplicable = $("#product").find('option:selected').data('size');
	var stocktypeid=$('#stocktypeid').find('option:selected').val();
	if(stocktypeid == 12) { // untag
		$("#orderitemsize-div").hide();
	} else {
		if(sizeapplicable == 'Yes') {
			$("#orderitemsize-div").show();
			$("#orderitemsize option").addClass("ddhidedisplay");
			$("#orderitemsize option").prop('disabled',true);
			$("#orderitemsize option[data-productid="+productvalue+"]").removeClass("ddhidedisplay");
			$("#orderitemsize option[data-productid="+productvalue+"]").prop('disabled',false);
		} else {
			$("#orderitemsize-div").hide();
		}
	}
}
//discount auth overlay submit
function discountauthcreate() {
	var empid = $("#discountauthemployeeid").val();
	var password = $("#discountauthpassword").val();
	var limit = $("#discountauthlimit").val();
	var discaltype =  $("#discaltype").val();
	var discountmode = $("#sdiscounttypeid").val();
	$.ajax({
		url: base_url + "Sales/discountauthcreate?employeeid="+empid+"&password="+password+"&limit="+limit+"&discountmode="+discountmode+"&discaltype="+discaltype,
		success: function(msg) {				
			var nmsg =  $.trim(msg);
			
		},
	});
}
function oldjewelcalc() {
	var calctype = $('#gwcaculation').val();
	var rateless = $('#ratelesscalc').val();
	var weightless = 0;
	if($.trim($('#wastageless').val()) == '' || $.trim($('#wastageless').val()) == 0) {
		weightless = 0;
		$('#wastageless').val(0);
	} else {	
		weightless = $('#wastageless').val();
	}
	var netweight = $("#netweight").val();
	var rate = $("#ratepergram").val();
	var newrate = 0;
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	var oldpurchasetouch = $('#salestransactiontypeid').find('option:selected').data('oldpurchasetouch');
	var grosswt = 0;
	if(transactionmodeid == 2) {
		if(rate > 0) {
			if(calctype) {
				switch(calctype) {
					case '2': 
						newrate =  (parseFloat(rate)-parseFloat(rateless));
						var newntwt =  (parseFloat(netweight)*newrate);
						var grossamount = ((parseFloat(weightless)/100)*parseFloat(newntwt)).toFixed(roundamount);
						break;
					case '3': 
						newrate =  (parseFloat(rate)-parseFloat(rateless));
						var newntwt =  (parseFloat(netweight)*newrate);
						var weightlessamount = ((parseFloat(weightless)/100)*parseFloat(newntwt)).toFixed(roundamount);
						var grossamount = ((parseFloat(newntwt))-parseFloat(weightlessamount)).toFixed(roundamount);
						break;
					default:
						grossamount=0;
				}
			} else{
				grossamount=0;
			}
		}else{
			grossamount=0;
		}
		if(rateless != 0) {			
			$('#oldjewelnewrate').text('('+newrate+')');
		}
		$('#grossamount').val(grossamount).trigger('change');
		$('#totalamount').val(grossamount);
	} else {
		if(rate > 0) {
			if(calctype) {
				switch(calctype) {
					case '2': 
						//newrate =  (parseFloat(rate)-parseFloat(rateless));
						var newntwt =  parseFloat(netweight);
						grosswt = ((parseFloat(weightless)/100)*parseFloat(newntwt)).toFixed(round);
						break;
					case '3': 
						// newrate =  (parseFloat(rate)-parseFloat(rateless));
						var newntwt =  parseFloat(netweight);
						var weightlessamount = ((parseFloat(weightless)/100)*parseFloat(newntwt)).toFixed(round);
						grosswt = ((parseFloat(newntwt))-parseFloat(weightlessamount)).toFixed(round);
						break;
					default:
						grosswt=0;
				}
			} else {
				grosswt=0;
			}
		} else {
			grosswt=0;
		}
		if(oldpurchasetouch == 1 && (wastagecalctype == 5 || wastagecalctype == 3)) {
			var touch = parseFloat($("#paymenttouch").val());
			if(touch > 0) {
				grosswt = ((parseFloat(touch)/100)*parseFloat(grosswt)).toFixed(round);
			}
		}
		$('#grosspureweight').val(grosswt);
		$('#pureweight').val(grosswt).trigger('change');
	}
}
function mobilenocheck() {
	if(accountmobileunique == 1) { // Mobile No unique validation check
		var primaryid = $("#accountid").val();
		var accname = $("#accountmobilenumber").val();
		var fieldname = 'mobilenumber';
		var elementpartable = 'account';
		if( accname != "" ) {
			$.ajax({
				url:base_url+"Base/uniqueshortdynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Mobileno already exists!";
					}
				}else {
					return "Mobileno already exists!";
				}
			}
		}
	}
}
function mainsalesbilldelete(datarowid) {
	//var gridlength = $('#salesinnergrid .gridcontent div.data-content div.data-rows').length;
	$.ajax({
		url: base_url + "Sales/mainsalesdelete?primaryid="+datarowid,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'PURCHASESTOCKEXIST') {
				$("#basedeleteoverlay").fadeOut();
				alertpopup('This purchase stock created in stockentry.So unable to delete this one');
				return false;
			} else if(nmsg == 'CANCELBILL') {
				$("#basedeleteoverlay").fadeOut();
				alertpopup('You cannot delete cancelled invoice bill');
				return false;
			} else {
				salesrefreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if(status == 0) { // status = 1 close the form manually
					if (nmsg == "SUCCESS") {
						alertpopup(deletealert);
					} else {					
						alertpopup(submiterror);
					}
				}
			}				
		},
	});
}
// Discount Field Hide Show in Form Fields
function discountlimitsetuserrolewise() {
	var discounttypeid = $("#sdiscounttypeid").val();
	var discountdisplayid = $("#discountdisplayid").val();
	var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
	setzero(['sumarydiscountamount','presumarydiscountamount','sumarydiscountlimit']); //sets zero on EMPTY
	if(discounttypeid == 2) { // itemlevel and 
		if(discountauthmoduleview == 1) {
			$("#discount-div").hide();
			$("#sumarydiscountamount-div").hide();
		} else {
			if(discountdisplayid == 2) { //discount field hide based tranaction type
				$("#discount-div").hide();
				$("#sumarydiscountamount-div").hide();
			}else{
			   $("#discount-div").show();
			   $("#sumarydiscountamount-div").hide();
			}
		}
	} else if(discounttypeid == 4) { //both level
		if(discountauthmoduleview == 1) {
			$("#discount-div").hide();
			$("#sumarydiscountamount-div").hide();
		} else {
			$("#discount-div").show();
			$("#sumarydiscountamount-div").show();
			/* if(discountauthorization == 1) {
				if(transactiontype == 11 || transactiontype == 16) { // sales
					//discountlimitfetch(discounttypeid);
				}
			} */
		}
	} else {//bill level
		if(discountauthmoduleview == 1) {
			$("#discount-div").hide();
			$("#sumarydiscountamount-div").hide();
			$("#sumarydiscountamount,#presumarydiscountamount,#sumarydiscountlimit").val(0);
		} else {
			if(discountdisplayid == 2) { // discount field hide based tranaction type
				$("#discount-div").hide();
				$("#sumarydiscountamount-div").hide();
				$("#sumarydiscountamount,#presumarydiscountamount,#sumarydiscountlimit").val(0);
			} else {
				$("#discount-div").hide();
				$("#sumarydiscountamount-div").show();
				/* if(discountauthorization == 1) {
					if(transactiontype == 11 || transactiontype == 16) { // sales
					   //discountlimitfetch(discounttypeid);
					}
				} */
			}
		}
	}
}
function transactionbasedbilltemphideshow() {
	var allstocktypeid = $.trim(getgridcolcolumnvalue('saledetailsgrid','','stocktypeid'));
	var details=allstocktypeid.split(",");
	oldstatus = 0;
	 $.each(details, function (index, value) {
			if(value == '11' || value == '12' || value == '13'){
			  salesstatus = 1;
			}
			if(value == '19'){
			  oldstatus = 1;
			  onlytakeorderadvancestatus = 1;
			}
			if(value == '20'){
			  returnstatus = 1;
			}
			if(value == '82'){
			  receivestatus = 1;
			}
			if(value == '75'){
			  placeorderstatus = 1;
			}
			if(value == '82'){
			  onlytakeorderadvancestatus = 1;
			}
			if(value == '26' || value == '27' || value == '28' || value == '29' || value == '79' || value == '39'){
				onlytakeorderadvancestatus = 1;
			}
		 });
		 if(onlytakeorderadvancestatus == 1) {
			 onlytakeorderstatus = 0;
		 }else{
			 onlytakeorderstatus = 1;
		 }
		
			mainprintemplateid = $.trim($('#printtemplateid').find('option:selected').val());
		//}
}
{//date Range
	function daterangefunction(generalstartdate,generatenddate){ 
		var dateformetdata = $('#'+generalstartdate+'').attr('data-dateformater');
		var startDateTextBox = $('#'+generalstartdate+'');
		var endDateTextBox = $('#'+generatenddate+'');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#'+generalstartdate+'').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.hasDatepicker').removeClass('error');
				}
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				} else {
					startDateTextBox.val(dateText);
				}
				$('#'+generatenddate+'').focus();
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
}

function takeordertypehideshow() {
	var allstocktypeid = $.trim(getgridcolcolumnvalue('salesinnergrid','','stocktypeid'));
	var details=allstocktypeid.split(",");
	 $.each(details, function (index, value) {
			if(value == '19'){
			  takeorderoldstatus = 1;
			}
			if(value == '82'){
			  takeorderreceivestatus = 1;
			}
			if(value == '75'){
			  takeorderstatus = 1;
			}
		 });
		if(takeorderreceivestatus == 1){ // take order - receive
		   orderstocktypehideshow(82);
		}else if(takeorderstatus == 1){ // takeorder & old jewel
		   orderstocktypehideshow(75);
		}
}
//fetch account data
function fetchaccountdataset(accountid) {
	$.ajax({
		url: base_url +"Sales/fetchaccountdataset",
		data:{accountid:accountid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(data) {
			if(data['accountdata'].accounttypeid != '0') {
				$("#accounttypeid").select2('val',data['accountdata'].accounttypeid);
				$("#selaccounttypeid").val(data['accountdata'].accounttypeid);
				$("#salutationid").select2('val',data['accountdata'].salutationid);
				$("#accountprimarycountry").select2('val',data['accountdata'].countryid);
				$("#accountprimarystate").select2('val',data['accountdata'].stateid);
				$("#newaccountname").val(data['accountdata'].accountname);
				$("#newaccountshortname").val(data['accountdata'].accountshortname);
				$("#accountmobilenumber").val(data['accountdata'].mobilenumber);
				$("#accountaddtmobilenumber").val(data['accountdata'].additionalmobilenumber);
				$("#accountpannumber").val(data['accountdata'].pannumber);
				$("#primaryarea").val(data['accountdata'].primaryarea);
				$("#accountprimaryaddress").val(data['accountdata'].address);
				$("#accountprimarypincode").val(data['accountdata'].pincode);
				$("#accountprimarycity").val(data['accountdata'].city);
				$("#gstnnumber").val(data['accountdata'].gstnnumber);
				$("#newaccountid").val(accountid);
				parentaccountname = data['accountdata'].accountname+' - '+data['accountdata'].mobilenumber;
				$('#s2id_parentaccountname a span:first').text(parentaccountname);
				$("#parentaccountname").val(data['accountdata'].parentaccountid);
				Materialize.updateTextFields();
			}
		}
	});
}
function fetchaccountdatasetjournal(accountid) {
	$.ajax({
		url: base_url +"Sales/fetchaccountdataset",
		data:{accountid:accountid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(data) {
		 if(data['accountdata'].accounttypeid != '0'){
			  $("#paymentaccountid option[value="+accountid+"]").remove();
			  var newddappend = "<option value='"+data['accountdata'].accountid+"' data-accountcatalogid='"+data['accountdata'].accountcatalogid+"' data-paymentaccountidhidden='"+data['accountdata'].accountname+"' data-accounttypeid='"+data['accountdata'].accounttypeid+"'>"+data['accountdata'].accountname+"</option>";
			  $('#paymentaccountid').append(newddappend);
			  $('#paymentaccountid').select2('val',accountid).trigger('change');
			  $("#paymentaccountid option").removeClass("ddhidedisplay").attr('disabled', false);
			  journalaccountname = data['accountdata'].accountname+' - '+data['accountdata'].mobilenumber;
			  $('#s2id_journalaccountid a span:first').text(journalaccountname);
			  Materialize.updateTextFields();
			  $('#issuereceipttypeid').trigger('change');
		 }
		}
	});
}
function getpannumberbasedonaccount(accountid) {
	$.ajax({
		url: base_url +"Sales/getpannumberbasedonaccount",
		data:{accountid:accountid},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) {   
			$("#pancardnumber").val(msg);
			$("#processoverly").hide();
		}
	});
}
function checkgreaterval() // check validation below the gross weight
{
	var grossweight = parseFloat($("#grossweight").val()); 
	var stonedustweight = parseFloat($("#dustweight").val()) + parseFloat($("#stoneweight").val());
	if(grossweight <= stonedustweight)
	{return "*Gr wt > dust wt";}else{return true;}
}
function getcurrentrate(){
	$.ajax({
		url: base_url +"Sales/getrate",
		type: "POST",
		async:true,
		dataType:'json',
		success: function(msg) {  
		   $('#ratearray').val(JSON.stringify(msg)); 
		}
	});
}
function itemdetailshideshow(status) {
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	var tagtypeid = $('#product').find('option:selected').data('tagtypeid');
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	if(status == 1){
		if(stocktypeid == 20 && tagtypeid == 5) {
			$('#grossweight,#stoneweight,#netweight').attr('disabled',true);
		} else {
			$('#grossweight-div,#stoneweight-div,#netweight-div,#ratepergram-div').hide();
		}
	} else if(status == 0) {
		if(salestransactiontypeid == 13) {
			$('#grossweight-div,#stoneweight-div,#netweight-div').show();
		} else {
			if(salestransactiontypeid == 18) {
				if(approvaloutcalculation == 'YES') {
					$('#grossweight-div,#stoneweight-div,#netweight-div,#ratepergram-div').show();
					ratepergramdivlsthide();
				} else {
					$('#grossweight-div,#stoneweight-div,#netweight-div').show();
				}
			} else {
				$('#grossweight-div,#stoneweight-div,#netweight-div,#ratepergram-div').show();
				ratepergramdivlsthide();
			}
		}
	}
}
function stonefieldshideshow(status) {
	if(status == 1) {
		$('#stoneweight-div').hide();
	} else if(status == 0) {
		$('#stoneweight-div').show();
	}
}
function savealertpopuphideshow(transactionmode,sumarypendingamount,sumarypendingwt,finalval) {
	if(transactionmode == 2) {
		moduleeditoralertmsg('salessaveyes','Balance Amount : '+sumarypendingamount+'. If you click on YES button it will generate a credit balance!',finalval);
	} else if(transactionmode == 3) {
		moduleeditoralertmsg('salessaveyes','Balance Weight : '+sumarypendingwt+'. If you click on YES button it will generate a credit balance!',finalval);
	} else if(transactionmode == 4) {
		moduleeditoralertmsg('salessaveyes','Balance Amount : '+sumarypendingamount+' | Balance Weight : '+sumarypendingwt+'. If you click on YES button it will generate a credit balance!',finalval);
	} else {
		moduleeditoralertmsg('salessaveyes','Are you sure, You want to save the details.!',finalval);
	}
	if(finalval == 1) {
		$('#balanceamountcredit').val(sumarypendingamount);
	}
	$(document).on( "click", "#salessaveyes", function() {
		$("#creditnogenerationoverlay").fadeOut();
		$("#balaccountsave").trigger("click");
		$('#salessaveyes').unbind();
	});
}
function saledetailsgrid() {
	var wwidth = $("#saledetailsgrid").width();
	var wheight = $("#saledetailsgrid").height();
	$.ajax({
		url:base_url+"Sales/salesdetaillocalgirdheaderinformationfetch?width="+wwidth+"&height="+wheight+"&modulename=salesdetail",
		dataType:'json',
		async:true,
		cache:false,
		success:function(data) {
			$("#saledetailsgrid").empty();
			$("#saledetailsgrid").append(data.content);
			$("#saledetailsgridfooter").empty();
			$("#saledetailsgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('saledetailsgrid');
			hideinnergridcolumns();
	  },
	});
}
{// LIVE WORKING USED FUNCTIONS

//form to grid 
function salesdetailformtogriddata_jewel(gridname,method,position,datarowid,status) {
	method = typeof method == 'undefined' ? 'ADD' : method;
	var dataval = "";
	var align = '';
	var opndivcontent = '';
	var content = '';
	var clsdivcontent = '';	
//if(deviceinfo != 'phone'){
		var n = $('#'+gridname+' .gridcontent div.data-content div').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.data-content').empty();
		} else{
			n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
			n = parseInt(n);
		}
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<ul class="inline-list">';
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
			var txtboxid = $(e).attr('id');
			var uitype = $('#'+txtboxid).data('uitype');
			var txtboxname = $('#'+txtboxid).data('fieldname');
			var txtboxlabel = $('#'+txtboxid).data('label');
			var txtwidth = $('#'+txtboxid).data('width');
			var txtclass = $('#'+txtboxid).data('class');
			var viewtype = $('#'+txtboxid).data('viewtype');
			var modeid = $('#'+txtboxid).data('modeid');
			var defaultdata = $('#'+txtboxid).data('defaultdata');
			var viewstyle = (viewtype=='0') ? ' display:none':'';
			if(uitype == '251') {
				if(status == 1) {
					var product = $("#product").val();
					var purity = $("#purity").val();
					var counter = $("#counter").val();
					dataval = loggedinbranchid+'-'+product+'-'+purity+'-'+counter;
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				} else {
					var product = $('#paymentproduct').val();
					var purity = $('#paymentpurity').val();
					var counter = $('#paymentcounter').val();
					dataval = loggedinbranchid+'-'+product+'-'+purity+'-'+counter;
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				}
		   } else if(uitype == '250') {
				if(status == 1) {
					var product = $("#product").val();
					var purity = $("#purity").val();
					dataval = loggedinbranchid+'-'+product+'-'+purity;
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				} else {
					var product = $('#paymentproduct').val();
					var purity = $('#paymentpurity').val();
					dataval = loggedinbranchid+'-'+product+'-'+purity;
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				}
			} else if(uitype == '252') {
				if(method == 'ADD' || method == '') {
					dataval = n+1;
				} else {
				    dataval = datarowid;
				}
				txtclass ='';
			    txtwidth = '70';
				viewstyle = '';
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
		   } else if(status == 1 && modeid == 1) {
				if(uitype == '3') { // span value
					dataval = $('#'+txtboxname+'').text();
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '6') { // wastage keyword attribute value
					dataval = $('#wastage').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '7') { // making charge keyword attribute value
					dataval = $('#makingcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '10') { // lst charge keyword attribute value
					dataval = $('#lstcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '11') { // flatcharge charge keyword attribute value
					dataval = $('#flatcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
					dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				} else if(uitype == '31') {
					if(txtboxname == 'categoryname' && transactioncategoryfield == '0') {
						dataval = $('#treecategoryname').val();
						datavalue = dataval === undefined ? '' : dataval;
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
					} else if(txtboxname == 'employeepersonidname' && transactionemployeeperson == '0') {
						dataval = $('#multipleemployeename').val();
						datavalue = dataval === undefined ? '' : dataval;
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
					} else {
						var avoid ="name";
						txtboxname = txtboxname.replace(avoid,'');
						dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
						datavalue = dataval === undefined ? '' : dataval;
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
					}
				} else {
					if(txtboxname == 'tagimage') {
						var removetext = 'radio_button_checked';
						dataval = $('#'+txtboxname+'').val();
						dataval = dataval.replace(removetext,'');
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+' tagimageoverlaypreview" id="tagimageoverlaypreview" style="width:'+txtwidth+'px;'+viewstyle+'">';
						content+='<i class="material-icons">radio_button_checked</i>';
						content+='<a href="'+dataval+'" style="display: none" target="_blank">'+dataval+'</a>';
						content+='</li>';
					} else {
						if(txtboxname == 'itemwastagevalue') {
							dataval = $('#wastage').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else if(txtboxname == 'itemmakingvalue') {
							dataval = $('#makingcharge').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else if(txtboxname == 'itemflatvalue') {
							dataval = $('#flatcharge').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else if(txtboxname == 'itemlstvalue') {
							dataval = $('#lstcharge').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else if(txtboxname == 'itemrepairvalue') {
							dataval = $('#repaircharge').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else if(txtboxname == 'itemchargesamtvalue') {
							dataval = $('#additionalchargeamt').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						} else {
							dataval = $('#'+txtboxname+'').val();
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						}
					}
				}
		   } else if(status == 0 && modeid == 0) {
				if(uitype == '3') { // span value
					dataval = $('#'+txtboxname+'').text();
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '6') { // wastage keyword attribute value
					dataval = $('#wastage').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '7') { // making charge keyword attribute value
					dataval = $('#makingcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '10') { // lst charge keyword attribute value
					dataval = $('#lstcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if(uitype == '11') { // flat charge keyword attribute value
					dataval = $('#flatcharge').attr('keyword');
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				} else if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
					dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			   } else if(uitype == '31') {
					if(txtboxname == 'categoryname' && transactioncategoryfield == '0') {
						dataval = $('#treecategoryname').val();
						datavalue = dataval === undefined ? '' : dataval;
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
					} else {
						var avoid ="name";
						txtboxname = txtboxname.replace(avoid,'');
						dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
						datavalue = dataval === undefined ? '' : dataval;
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
					}
				} else if(uitype == '32') {
					var paymentstocktypeid =$('#paymentstocktypeid').find('option:selected').val();
					var issuereceipttypeid =$('#issuereceipttypeid').find('option:selected').val();
					dataval = paymentstocktypeid+'-'+issuereceipttypeid;
					datavalue = dataval === undefined ? '' : dataval;
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
				} else {
					dataval = $('#'+txtboxname+'').val();
					align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
			   }
		   } else {
				dataval = defaultdata;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
		   }
		});
		content+='</ul>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.data-content:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.data-content').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).append(content);
			}
		}
	//}
}

//grid to form
function sales_gridtoformdataset(gridname,datarowid,modestatus) {
	//if(deviceinfo != 'phone'){
		var value = "";
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
			var txtboxid = $(e).attr('id');
			var uitype = $('#'+txtboxid).data('uitype');
			var txtboxname = $('#'+txtboxid).data('fieldname');
			var txtclass = $('#'+txtboxid).data('class');
			var modeid = $('#'+txtboxid).data('modeid');
			if(modestatus == 2 && modeid == 1) // sales details edit
			{
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+txtclass).text();
				if(uitype == '3') { // span value
					$('#'+txtboxname).text(value);
				} else if(uitype == '6') { // wastage keyword value
					$('#wastage,#wastagespan,#wastageclone').attr('keyword',value);
				} else if(uitype == '7') { // makingcharge keyword value
					$('#makingcharge,#makingchargespan,#makingchargeclone').attr('keyword',value);
				} else if(uitype == '10') { // lstcharge keyword value
					$('#lstcharge,#lstchargespan,#lstchargeclone').attr('keyword',value);
				} else if(uitype == '11') { // flatcharge keyword value
					$('#flatcharge,#flatchargespan,#flatchargeclone').attr('keyword',value);
				} else if(uitype == '4') { // hidden stone data
					$('#'+txtboxname).val(value);
				} else if(uitype == '5') { // hidden stone data
					$('#'+txtboxname).text('('+getgridcolvalue('saledetailsgrid',datarowid,'discountdata','')+')');
				} else if(uitype == '8') { // keyword value set
					if(value != '') {
						var chargekeyworddata=value.split(",");
						$.each(chargekeyworddata, function (index, value) {
							var res=value.split(":");
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).val(res[1]);
						});
						$('#'+txtboxname).val(value);
					}
				} else if(uitype == '9') { //  span value set
					if(value != '') {
						var ckeywordoriginalvalue=value.split(",");
						$.each(ckeywordoriginalvalue, function (index, value) {
							var res=value.split(":");
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
							$("#productaddonform .chargedetailsdiv span[id="+res[0]).text(res[1]);
						});
						$('#'+txtboxname).val(value);
					}
				} else if( uitype != '17' || uitype != '18' || uitype != '19' || uitype != '20' || uitype != '23' || uitype != '25' || uitype != '26' || uitype != '27' || uitype != '28' || uitype != '29') {
					if(txtboxname == 'categoryname' && transactioncategoryfield == '0') {
						$('#categorynamehidden').val(value);
						$('#treecategoryname').val(value);
						$('#prodcategorylistbutton').prop('disabled',true);
					} else if( uitype != '31') {
						if(value.indexOf(',') != -1) {
							var valsplit = value.split(',');
							$('#'+txtboxname).select2("val",valsplit);
						} else {
							if($('#'+txtboxname).prop('type') == 'select-one') {
								$('#'+txtboxname).select2("val",value);
							} else {
								$('#'+txtboxname).val(value);
							}
						}
					}
				}
			} else if(modestatus == 0 && modeid == 0) // payment details edit
			{
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+txtclass).text();
				if(uitype == '3') { // span value
					$('#'+txtboxname).text(value);
				} else if(uitype == '6') { // wastage keyword value
					$('#wastage,#wastagespan,#wastageclone').attr('keyword',value);
				} else if(uitype == '7') { // makingcharge keyword value
					$('#makingcharge,#makingchargespan,#makingchargeclone').attr('keyword',value);
				} else if(uitype == '10') { // lstcharge keyword value
					$('#lstcharge,#lstchargespan,#lstchargeclone').attr('keyword',value);
				} else if(uitype == '11') { // flatcharge keyword value
					$('#flatcharge,#flatchargespan,#flatchargeclone').attr('keyword',value);
				} else if(uitype == '4') { // hidden stone data
					$('#'+txtboxname).val(value);
				} else if(uitype == '5') { // hidden stone data
					$('#'+txtboxname).text('('+getgridcolvalue('saledetailsgrid',datarowid,'discountdata','')+')');
				} else if(uitype == '8') { // keyword value set
					if(value != '') {
						var chargekeyworddata=value.split(",");
						$.each(chargekeyworddata, function (index, value) {
							var res=value.split(":");
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).val(res[1]);
						});
						$('#'+txtboxname).val(value);
					}
				} else if(uitype == '9') { //  span value set
					if(value != '') {
						var ckeywordoriginalvalue=value.split(",");
						$.each(ckeywordoriginalvalue, function (index, value) {
							var res=value.split(":");
							$("#productaddonform .chargedetailsdiv input[keyword="+res[0]).parent().removeClass('hidedisplay');
							$("#productaddonform .chargedetailsdiv span[id="+res[0]).text(res[1]);
						});
						$('#'+txtboxname).val(value);
					}
				} else if( uitype != '17' || uitype != '18' || uitype != '19' || uitype != '20' || uitype != '23' || uitype != '25' || uitype != '26' || uitype != '27' || uitype != '28' || uitype != '29') {
					if( uitype != '31') {
						if(value.indexOf(',') != -1){
							var valsplit = value.split(',');
							$('#'+txtboxname).select2("val",valsplit);
						} else {
							if($('#'+txtboxname).prop('type') == 'select-one') {
								$('#'+txtboxname).select2("val",value);
							} else {
								$('#'+txtboxname).val(value);
							}
						}
					}
				}
			}
		});
	//}
}
function formdatatransfertogrid(operationstone,status) { // form data transfer to local grid
	var gridname ='saledetailsgrid';
	var coldatas = $('#gridcolnames').val();
	var columndata = coldatas.split(',');
	var coluiatas = $('#gridcoluitype').val();
	var columnuidata = coluiatas.split(',');
	var datalength = columndata.length;
	/*Form to Grid*/
	if(operationstone == 'ADD' || operationstone == '') {
		salesdetailformtogriddata_jewel(gridname,operationstone,'last','',status);
	} else if(operationstone == 'UPDATE') {
		var selectedrow = $('#editinnerrowid').val();
		salesdetailformtogriddata_jewel(gridname,operationstone,'',selectedrow,status);
	}
	hideinnergridcolumns();
}
function pricetagnetcalc() {
	var netamount = 0;
	netamount = parseFloat($('#grossamount').val()) + parseFloat($('#taxamount').val());
	$('#totalamount').val(netamount);
}
function getestimatedata(estimationnumber) { // get estimate salesdetail data
	var deliveryoderstatus = 0;
	var transactiontype = $('#salestransactiontypeid').find('option:selected').val();
	var allstocktypeid = $.trim(getgridcolcolumnvalue('saledetailsgrid','','stocktypeid'));
	var details=allstocktypeid.split(",");
	$.each(details, function (index, value) {
		if(value == '83'){
		  deliveryoderstatus = 1;
		}
	}); 
	$.ajax({
		url:base_url+"Sales/getestimatedata",
		data: "estimationnumber=" +estimationnumber+"&deliveryoderstatus="+deliveryoderstatus,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$("#estimationnumber").val('');
			if(data['status'] == 'fail') {
				alertpopup('Enter Proper Number');
			}else if(data['status'] == 'invalid') {
				alertpopup('New item not allowed for order sales');
			}else {
				var estimatestatus = 1;
				var eststatustagdetail = ''
				if(data['tagarray'] != '') {
					var estimatefinal=data['tagarray'].toString().split(",");
					var estimatedata = $('#estimatearray').val();
					var itemtagnumber = getgridcolcolumnvalue('saledetailsgrid','','itemtagnumber');
					var chargedetails1=itemtagnumber.toString().split(",");
					$.each(estimatefinal, function (index, value) {
						$.each(chargedetails1, function (index, res) {
							if(res == value) {
								estimatestatus = 0;
								eststatustagdetail = eststatustagdetail +','+ value;
							}
						}); 
					}); 
				}
				if(estimatestatus == 1) {
					estimatearray.push(estimationnumber);
					$('#estimatearray').val(estimatearray);
					estimate_loadinlinegriddata('saledetailsgrid',data['estimatedata'].rows,'json');
					/* data row select event */
					datarowselectevt();
					hideinnergridcolumns();
					billleveltaxautoremoval = data['billleveltaxautoremoval']
					adjustmentamountkeypressed = data['adjustmentamountkeypressed'];
					var prevdiscount = $('#sumarydiscountamount').val();
					var preroundvalue = $('#roundvalue').val();
					var prevsumarytax = $('#sumarytaxamount').val();
					$('#transactionmodeid').select2('val',data['transactionmodeid']).trigger('change');
					$('#accountid').val(data['accountid']);
					if(data['mobilenumber'] != '') {
						$('#s2id_accountid a span:first').text(data['accountname']+' - '+data['mobilenumber']);
					} else {
						$('#s2id_accountid a span:first').text(data['accountname']);
					}
					$("#salescreateform :input").attr("readonly", true);
					$('#salespersonid,#salestransactiontypeid,#transactionmodeid').prop('disabled',true);
					$("#estimationnumber").attr('readonly',false);
					$('#accountid').prop('disabled', false);
					$('#selaccounttypeid').val(data['accounttypeid']);
					//getsalesdetailcalc(); // get local grid data based calcualtion
					finalsummarycalc(1); // get local grid data based calcualtion
					setmainsummary(data['amountarray']['discount'],data['amountarray']['summarytaxamount'],data['amountarray']['roundvalue'],prevdiscount,preroundvalue,prevsumarytax);
					$('.select2-input').prop('readonly', false);
					
					// Bill level restriction metal
					billlevelrestrictmetalid = 1;
					billlevelrestrictoldmetalid = 1;
					billlevelrestrictreturnmetalid = 1;
					if(transactiontype == 11) {
						if(metalarraydetails != '') {
							var retrievemetalids = retrieveuniquemetalidingrid('saledetailsgrid','all');
							if(retrievemetalids.length > 1) {
								billlevelrestrictmetalid = 1;
								metalstatus = 0;
							} else {
								billlevelrestrictmetalid = retrievemetalids.toString();
								metalstatus = 1;
							}
						} else {
							billlevelrestrictmetalid = 1;
							metalstatus = 0;
						}
						if(metaloldarraydetails != '') {
							var retrievemetalids = retrieveuniquemetalidingrid('saledetailsgrid','old');
							if(retrievemetalids.length > 1) {
								billlevelrestrictoldmetalid = 1;
								metaloldstatus = 0;
							} else {
								billlevelrestrictoldmetalid = retrievemetalids.toString();
								metaloldstatus = 1;
							}
						} else {
							billlevelrestrictoldmetalid = 1;
							metaloldstatus = 0;
						}
						if(metalreturnarraydetails != '') {
							var retrievemetalids = retrieveuniquemetalidingrid('saledetailsgrid','return');
							if(retrievemetalids.length > 1) {
								billlevelrestrictreturnmetalid = 1;
								metalreturnstatus = 0;
							} else {
								billlevelrestrictreturnmetalid = retrievemetalids.toString();
								metalreturnstatus = 1;
							}
						} else {
							billlevelrestrictreturnmetalid = 1;
							metalreturnstatus = 0;
						}
					}
				} else if(estimatestatus == 0) {
					alertpopup('Tagnumbers '+eststatustagdetail+' of this estimate is already inserted in detail list.So none of the Data of this estimate is loaded into inner grid.Kindly Generate new Estimate for Rest of data of this estimate.');
				}
				if(data['errormsg'] != '') {
					alertpopupdouble(data['errormsg']);
				}
			}
		}
	});
}

//load inner grid data
function estimate_loadinlinegriddata(gridname,datas,type,checkbox) {
	checkbox = typeof checkbox == 'undefined' ? '' : checkbox;
	var opndivcontent='';
	var content='';
	var clsdivcontent='';
	if(type=='json') {
		//if(deviceinfo != 'phone'){
			$.each(datas,function(key,value){
				var n = $('#'+gridname+' .gridcontent div.data-content div').length;
				if(n=='0' || n=='') {
					$('#'+gridname+' .gridcontent div.data-content div').empty();
				}else{
					n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
					n = parseInt(n);
				}
				opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
				content = '<ul class="inline-list">';				
				var j = 0;
				var m = 0;
				$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
					var txtboxid = $(e).attr('id');
					var uitype = $('#'+txtboxid).data('uitype');
					var txtboxname = $('#'+txtboxid).data('fieldname');
					var txtwidth = $('#'+txtboxid).data('width');
					var txtclass = $('#'+txtboxid).data('class');
					var viewtype = $('#'+txtboxid).data('viewtype');
					var viewstyle = (viewtype=='0') ? ' display:none':'';
					if(checkbox == 'checkbox' && m==0) {
						content +='<li style="width:'+txtwidth+'px;'+viewstyle+'"><input type="checkbox" id="rowchkbox'+value['id']+'" name="rowchkbox'+value['id']+'" data-rowid="'+value['id']+'" class="'+gridname+'_rowchkboxclass '+txtclass+' rowcheckbox " value="'+value['id']+'" /></li>';
						m=1;
					}else if(uitype == '252') {
					   dataval = n+1;
					   txtclass ='';
						txtwidth = '70';
						viewstyle = '';
					   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
					   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				    } else {
						if(txtboxname == 'tagimage') {
							dataval = value['cell'][j];
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+' tagimageoverlaypreview" id="tagimageoverlaypreview" style="width:'+txtwidth+'px;'+viewstyle+'">';
							content+='<i class="material-icons">radio_button_checked</i>';
							content+='<a href="'+dataval+'" style="display: none" target="_blank">'+dataval+'</a>';
							content+='</li>'
							j++;
						} else {
							dataval = value['cell'][j];
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
							j++;
						}
					}
				});
				content+='</ul>';
				clsdivcontent='</div>';
				var finalcontent = opndivcontent+content+clsdivcontent;
				$('#'+gridname+' .gridcontent div.data-content').append(finalcontent);
			});		
		//}
	}
}
function setdiscountdata() { // set discount based on metal,purity or category from discount authorization
	var categoryid = $('#product').find('option:selected').data('categoryid');
	var purityid = $('#purity').find('option:selected').val();
	var metalid = $('#purity').find('option:selected').data('metalid');
	if($.trim(discountvaluearray) != ''){
		discountvaluearray.forEach(function(entry) {
			if (entry.categoryid == categoryid && entry.purityid == purityid && entry.metalid == metalid) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == purityid && entry.metalid == metalid && entry.categoryid == 1) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == purityid && entry.metalid == 1 && entry.categoryid == 1) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == 1 && entry.metalid == metalid && entry.categoryid == 1) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == purityid && entry.metalid == 1 && entry.categoryid == categoryid) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == 1 && entry.metalid == metalid && entry.categoryid == categoryid) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.purityid == 1 && entry.metalid == 1 && entry.categoryid == categoryid) {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			} else if (entry.categoryid == '1' && entry.purityid == '1' && entry.metalid == '1') {
				$("#sumarydiscountlimit,#discamt").val(entry.discountvalue);
			}
		});
	} else {
		$("#sumarydiscountlimit,#discamt").val(0);
	}
}
//inline grid data
function salesaddinlinegriddata(gridname,id,datas,type) {
	var opndivcontent='';
	var content='';
	var clsdivcontent='';
	if(type=='json') {
		//if(deviceinfo != 'phone'){
			var n = $('#'+gridname+' .gridcontent div.data-content div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.data-content').empty();
			}else{
				n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
				n = parseInt(n);
			}
			if($('#stocktypeid').find('option:selected').val() == 82) {
				$('div.gridcontent div.data-rows').removeClass('active');
				opndivcontent = '<div class="data-rows active" id="'+(n+1)+'">';
			}else {
				opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
			}
			content = '<ul class="inline-list">';
			var j = 0;
			$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
				var dataval='';
				var txtboxid = $(e).attr('id');
				var uitype = $('#'+txtboxid).data('uitype');
				var txtboxname = $('#'+txtboxid).data('fieldname');
				var txtwidth = $('#'+txtboxid).data('width');
				var txtclass = $('#'+txtboxid).data('class');
				var viewtype = $('#'+txtboxid).data('viewtype');
				var viewstyle = (viewtype=='0') ? ' display:none':'';
				if(uitype == '252') {
				   dataval = n+1;
				   txtclass ='';
					txtwidth = '70';
					viewstyle = '';
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
			   }else{
					if(txtboxname == "tagimage") { //Image - url to new tab - Kumaresan
						var removetext = 'radio_button_checked';
						dataval = datas[txtboxname].replace(removetext,'');
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+' tagimageoverlaypreview" id="tagimageoverlaypreview" style="width:'+txtwidth+'px;'+viewstyle+'">';
						content+='<i class="material-icons">radio_button_checked</i>';
						content+='<a href="'+dataval+'" style="display: none" target="_blank">'+dataval+'</a>';
						content+='</li>'
						j++;
					} else {
						if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
							dataval = datas[txtboxname+'name'];
						} else {
							dataval = datas[txtboxname];
						}
						align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
						content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						j++;
					}
			   }
			});
			content+='</ul>';
			clsdivcontent='</div>';
			var finalcontent = opndivcontent+content+clsdivcontent;
			$('#'+gridname+' .gridcontent div.data-content').append(finalcontent);		
		/*} else{
			var n = $('#'+gridname+' .gridcontent div.wrappercontent div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.wrappercontent').empty();
			}
			opndivcontent = '<div class="data-rows" id="'+id+'">';
			content = '<div class="bottom-wrap">';
			var j = 0;
			$('#'+gridname+' div.header-caption span').map(function(i,e) {
				var dataval='';
				var txtboxid = $(e).attr('id');
				var uitype = $('#'+txtboxid).data('uitype');
				var txtboxname = $('#'+txtboxid).data('fieldname');
				var txtboxlabel = $('#'+txtboxid).data('label');
				var txtwidth = $('#'+txtboxid).data('width');
				var txtclass = $('#'+txtboxid).data('class');
				var viewtype = $('#'+txtboxid).data('viewtype');
				var viewstyle = (viewtype=='0') ? ' display:none':'';
				if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
					dataval = datas[txtboxname+'name'];
				} else {
					dataval = datas[txtboxname];
				}
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<span class="content" style="'+ viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				j++;
			});
			content+='</div>';
			clsdivcontent='</div>';
			var finalcontent = opndivcontent+content+clsdivcontent;
			$('#'+gridname+' .gridcontent div.wrappercontent').append(finalcontent);		
		} */
	}
}
// get sales details based on salesid while edit MAINEDIT
function billeditsalesdetails(salesid) {
	$.ajax({
			url:base_url+"Sales/billeditsalesdetails",
			data: "salesid=" +salesid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				estimate_loadinlinegriddata('saledetailsgrid',data['estimatedata'].rows,'json');
				/* data row select event */
				datarowselectevt();
				hideinnergridcolumns();
			}
		});
}
function hideinnergridcolumns() {
	var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
	var hideprodgridcol = '';
	var showprodgridcol = '';
	if(transactiontypeid == 16) { // Estimate
		hideprodgridcol = ['paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','lottypeidname','chitbookno','chitbookname','chitamount','totalpaidamount','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsizename','orderduedate','orderitemratefixname','paymentcomment','ordernumber','paymentemployeepersonidname','issuereceipttypeidname','categoryname','tagimage','vendororderduedate','ordervendoridname','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','totalamount','salescomment','paymenttouch','chitgram','proditemrate'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	} else if(transactiontypeid == 11) { // Sales
		hideprodgridcol = ['lottypeidname','categoryname','tagimage','ordervendoridname','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','totalamount','chitbookno','chitbookname','chitamount','totalpaidamount','salescomment','paymentcomment','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsizename','orderduedate','orderitemratefixname','ordernumbername','chitgram','vendororderduedate','proditemrate'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	} else if(transactiontypeid == 23 || transactiontypeid == 22) { // Payment receipt  &receipt
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','chitbookno','chitbookname','chitamount','totalpaidamount','paymentcomment','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','chitgram'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
		hideprodgridcol = ['stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','totalamount','rfidtagnumber','olditemdetailsname','lottypeidname','orderitemsizename','orderduedate','orderitemratefixname','salescomment','ordernumber','ordernumbername','categoryname','tagimage','vendororderduedate','ordervendoridname','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
	} else if(transactiontypeid == 25) { // Contra
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','paymentcomment'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
		hideprodgridcol = ['stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','totalamount','rfidtagnumber','olditemdetailsname','lottypeidname','orderitemsizename','orderduedate','orderitemratefixname','salescomment','ordernumber','ordernumbername','chitbookno','chitbookname','chitamount','totalpaidamount','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','categoryname','tagimage','chitgram','vendororderduedate','ordervendoridname','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
	} else if(transactiontypeid == 9) { // Purchase
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','stocktypename','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','totalamount','lottypeidname','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','salescomment','paymentcomment','categoryname','proditemrate'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
		hideprodgridcol = ['itemtagnumber','wastage','dustweight','wastageless','rfidtagnumber','olditemdetailsname','chitbookno','chitbookname','chitamount','totalpaidamount','orderitemsizename','orderduedate','orderitemratefixname','ordernumbername','tagimage','chitgram','vendororderduedate','ordervendoridname','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
	} else if(transactiontypeid == 18) { // Approval out
		hideprodgridcol = ['paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','chitbookno','chitbookname','chitamount','totalpaidamount','olditemdetailsname','paymentaccountidname','lottypeidname','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsizename','orderduedate','orderitemratefixname','paymentcomment','ordernumbername','paymentemployeepersonidname','issuereceipttypeidname','categoryname','tagimage','chitgram','vendororderduedate','ordervendoridname','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','employeepersonidname','salescomment','rfidtagnumber'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	} else if(transactiontypeid == 20) { // Take order
		hideprodgridcol = ['lottypeidname','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','rfidtagnumber','itemtagnumber','categoryname','chitgram','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','stocktypename','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','totalamount','chitbookno','chitbookname','chitamount','totalpaidamount','orderitemsizename','orderduedate','salescomment','paymentcomment','ordernumbername','tagimage','orderitemratefixname','vendororderduedate','ordervendoridname'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	} else if(transactiontypeid == 21) { // Place order
		hideprodgridcol = ['paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','chitbookno','chitbookname','chitamount','totalpaidamount','olditemdetailsname','paymentaccountidname','lottypeidname','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','paymentcomment','itemtagnumber','countername','rfidtagnumber','paymentemployeepersonidname','categoryname','chitgram','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','stocktypename','productname','purityname','grossweight','stoneweight','netweight','pieces','employeepersonidname','salescomment','orderitemsizename','ordernumbername','orderduedate','orderitemratefixname','tagimage','vendororderduedate','ordervendoridname'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	} else if(transactiontypeid == 24) { // Journals
		showprodgridcol = ['innergridrowid','paymentstocktypename','paymenttotalamount','paymentrefnumber','paymentreferencedate','paymentaccountidname','totalpaidamount','paymentcomment'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
		hideprodgridcol = ['stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','bankname','cardtype','cardtypename','approvalcode','chitbookno','chitbookname','chitamount','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','paymenttouch','newpaymenttouch','paymentpureweight','paymentratepergram','totalamount','rfidtagnumber','olditemdetailsname','lottypeidname','orderitemsizename','orderduedate','orderitemratefixname','salescomment','ordernumber','ordernumbername','categoryname','tagimage','chitgram','vendororderduedate','ordervendoridname','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
	} else if(transactiontypeid == '26') { // Metal IR
		showprodgridcol = ['innergridrowid','paymentstocktypename','issuereceipttypeidname','paymentproductname','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpieces'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
		hideprodgridcol = ['stocktypename','itemtagnumber','productname','purityname','countername','grossweight','stoneweight','netweight','pieces','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','employeepersonidname','flatcharge','cflat','dustweight','wastageless','bankname','cardtype','cardtypename','approvalcode','chitbookno','chitbookname','chitamount','pureweight','paymenttouch','paymentpureweight','paymentratepergram','totalamount','rfidtagnumber','olditemdetailsname','lottypeidname','orderitemsizename','orderduedate','orderitemratefixname','salescomment','ordernumber','ordernumbername','categoryname','tagimage','chitgram','paymenttotalamount','paymentrefnumber','paymentreferencedate','paymentaccountidname','totalpaidamount','paymentcomment','vendororderduedate','ordervendoridname','proditemrate','repaircharge'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
	} else if(transactiontypeid == '28' || transactiontypeid == '29' || transactiontypeid == '30') { // Take Repair, Place Repair
		hideprodgridcol = ['paymentstocktypename','paymenttotalamount','lottypeidname','paymenttouch','paymentproductname','pureweight','paymentpurityname','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','rfidtagnumber','itemtagnumber','categoryname','chitgram','proditemrate','countername','chitbookno','chitbookname','chitamount','orderitemratefixname','vendororderduedate','ordervendoridname','paymentcomment','ordernumbername','bankname','paymentrefnumber','paymentreferencedate','paymentaccountidname','cardtype','cardtypename','approvalcode','ratepergram','wastage','stonecharge','dustweight','wastageless','totalpaidamount','olditemdetailsname','paymentemployeepersonidname','issuereceipttypeidname','lstcharge','wastageweight','itemcaratweight','makingcharge','discount','flatcharge','cflat'];
		gridfieldhide('saledetailsgrid',hideprodgridcol);
		showprodgridcol = ['innergridrowid','stocktypename','productname','purityname','grossweight','stoneweight','netweight','pieces','taxamount','grossamount','employeepersonidname','totalamount','orderitemsizename','orderduedate','salescomment','tagimage','repaircharge'];
		salesgridfieldshow('saledetailsgrid',showprodgridcol);
	}
	hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','gwcaculation','ratelesscalc','hiddenstonedata','taxgriddata','discountdata','issuereceiptid','paymentaccountid','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagespanlabel','makingchargespanlabel','wastagekeylabel','makingchargekeylabel','cardtype','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','totalchargeamount','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','additionalchargeamt','chitschemetypeid','balancecounter','taxcategory','prewastage','premakingcharge','prelstcharge','lottypeid','purchasemode','paymentproduct','paymentpurity','paymentcounter','orderitemsize','paymentemployeepersonid','ordernumber','vendoraccountid','placeordernumber','receiveordernumber','ordernumbername','issuereceipttypeid','issuereceiptmergetext','creditadjustvaluehidden','creditadjusthidden',rfidshowhide,'oldsalesreturntax','mainsalesdetailid','category','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','flatchargekeylabel','flatchargespanlabel','preflatcharge','untagvalidatemergetext','untagvalidatecountermergetext','approvalnumber','paymentlessweight','paymentpieces','orderitemratefix','ordervendorid','repairchargekeylabel','repairchargespanlabel','prerepaircharge','repairchargespan','repairchargeclone','itemwastagevalue','itemmakingvalue','itemflatvalue','itemlstvalue','itemrepairvalue','itemchargesamtvalue'];//LST
	gridfieldhide('saledetailsgrid',hideprodgridcol);
	
}
//show the hiiden fileds in grid
function salesgridfieldshow(gridname,showarray) {
	$("#"+gridname+" .header-caption ul ."+showarray[0]+"-class").attr('style', 'display: block');
	$("#"+gridname+" .header-caption ul ."+showarray[0]+"-class").data('viewtype', '1');
	$("#"+gridname+" .header-caption ul ."+showarray[0]+"-class").attr('style', 'width: 70px;');
	$("#"+gridname+" .gridcontent ul ."+showarray[0]+"-class").attr('style', 'display: block');
	$("#"+gridname+" .gridcontent ul ."+showarray[0]+"-class").attr('style', 'width: 500px;');
	for(var i = 1; i < showarray.length; i++){
			$("#"+gridname+" .header-caption ul ."+showarray[i]+"-class").attr('style', 'display: block');
			$("#"+gridname+" .header-caption ul ."+showarray[i]+"-class").data('viewtype', '1');
			$("#"+gridname+" .header-caption ul ."+showarray[i]+"-class").attr('style', 'width: 150px;');
			$("#"+gridname+" .gridcontent ul ."+showarray[i]+"-class").attr('style', 'display: block');
			$("#"+gridname+" .gridcontent ul ."+showarray[i]+"-class").attr('style', 'width: 150px;');
	}
	var gridwidth=0;
	var j=0;
	$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   var txtboxid = $(e).attr('id');
		   var txtwidth = $('#'+txtboxid).data('width');
		   var viwtype = $('#'+txtboxid).data('viewtype');
		   if(viwtype=='1') {
			   gridwidth=parseInt(gridwidth)+parseInt(txtwidth);
			   j++;
		   }
	});
	var oldsize = $("#"+gridname).width();
	gridwidth = oldsize>gridwidth ? oldsize : gridwidth;
	if(gridwidth>0) {
		$("#"+gridname+" .header-caption").attr('style','width:'+gridwidth+'px');
		$("#"+gridname+" .gridcontent div.data-content").attr('style','width:'+gridwidth+'px');
	}
}
function showsavebutton(){
	var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
	if(salestransactiontypeid == 13){ // weight sales
		$('#salesdetailsubmit').show();
	}else{
		$('#salesdetailsubmit').hide();
	}
}
// stone weight hide/show
function stoneweighthideshow() {
	var productstone=$('#product').find('option:selected').data('stoneapplicable');
	var stocktypeid=$('#stocktypeid').find('option:selected').val();
	if(stocktypeid == 12) { // untag
		$('#stoneweight-div').hide();
	} else if(stocktypeid == 19) { // old
		$('#stoneweight-div').show();
	} else {
		if(productstone == 'No') {
			$('#stoneweight-div').hide();
		} else {
			$('#stoneweight-div').show();
		}
	}
}
//Accounticon hide/show
function accounticonhideshow() {
	var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
	var stocktypeid=$('#stocktypeid').find('option:selected').val();
	if(salestransactiontypeid == 18 || salestransactiontypeid == 19) { // approval out and purchase
		if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
			$('#approvaloutreturnproductid').val('');
			if($('#generatesalesid').val() != '')
			{
				$('#stocktypeid').prop('disabled',true);
			}else
			{
				$('#stocktypeid').prop('disabled',false);
			}
		}else {
			$('#stocktypeid').prop('disabled',true);
		}
	}
	if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
		if($('#generatesalesid').val() != ''){
			if(salestransactiontypeid == 18 && stocktypeid == 62) {
				$('#accountsearchevent,#addaccount').show();
			} else {
				$('#accountsearchevent,#addaccount').hide();
			}
		} else {
			if(salestransactiontypeid == 18 && stocktypeid == 65) {
				$('#accountsearchevent,#addaccount').hide();
				$("#accountid").prop('disabled',false);
			} else if (salestransactiontypeid == 20 || salestransactiontypeid == 21) {
				$("#accountid").prop('disabled',false);
			} else {
				// maddy
				if(salestransactiontypeid != 25) {
					$('#accountsearchevent,#addaccount').show();
				}
			}
		}
	}else{
		if(salestransactiontypeid == 18 && stocktypeid == 65) {
			$('#accountsearchevent,#addaccount').hide();
			$("#accountid").prop('disabled',true);
		} else if (salestransactiontypeid == 20 || salestransactiontypeid == 21){
			$("#accountid").prop('disabled',true);
		}
		if(salestransactiontypeid == 18 && stocktypeid == 62) {
			$('#accountsearchevent,#addaccount').show();
		} else {
			$('#accountsearchevent,#addaccount').hide();
		}
	}
}
// account hide show based on entry for issue & receipt
function iraccounthideshow() {
	var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
	var stocktypeid=$('#stocktypeid').find('option:selected').val();
	if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
		if($('#generatesalesid').val() == '') {
			$('#accountsearchevent,#addaccount').show();
		}
	}else{
		$('#accountsearchevent,#addaccount').hide();
	}
}
function finalsummarycalc(status) {
	
	setzero(['billgrossamount','billnetamount','sumarypaidamount','editpaymenttotalamount']);
	var ids = getgridallrowids('saledetailsgrid');
	var salestaxtypeid = $("#taxtypeid").val();
	var sdiscounttypeid = $("#sdiscounttypeid").val();
	var pregrossamount = 0;
	var oldjewelamt = 0;
	var salesreturnamt = 0;
	var grossamount = 0;
	var finalgrossamount = 0;
	var taxamt = 0;
	var discountamt = 0;
	var processgrossamt = 0;
	var prepaymenttotalamount = 0;
	var creditfinalamt = 0;
	var totalpaid = 0;
    var issueamt = 0;
	var totalpaidpurewt = 0;
	var balancepurewt = 0;
	var sumdiscountamt = 0;
	var transactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
	var transactionmodeid = $("#transactionmodeid").find('option:selected').val();
	var paymentstocktypeid = $("#paymentstocktypeid").find('option:selected').val();
	var stocktypeid = $("#stocktypeid").find('option:selected').val();
	var billnetamount = 0;
	var presumarydiscountamount = $('#presumarydiscountamount').val();
	var editpaymenttotalamount = $('#editpaymenttotalamount').val();
	var roundvalue = Math.abs($('#roundvalue').val());
	var roundvaluewithsign = $('#roundvalue').val();
	prepaymenttotalamount = $("#prepaymenttotalamount").val();
	var sumarypaidamount = $("#sumarypaidamount").val();
	var sumaryissueamount = $("#sumaryissueamount").val(); 
	var prediscountamount = $("#prediscountamount").val();
	var finalamt = 0;
	var sumarydiscountamount = 0;
	oldjewelstatus = 1;
	salesreturnstatus = 1;
	var pregrossweight = 0;
	var prebillforgrossweight = 0;
	var prebillfornetweight = 0;
	var prebillforpieces = 0;
	var orderadvanceamount = 0;
	var finalcloseamt = 0;
	var finalclosewt = 0;
	var oldjewelwt = 0;
	var salesreturnwt = 0;
	var finalnetwt = 0;
	var prebillnetamount = 0;
	{// order advance summary amount checking
		if($('#orderadvamtsummary').val() == '') {
			 orderadvanceamount = 0;
		} else {
			if(transactiontypeid == 11) {
				orderadvanceamount = $('#orderadvamtsummary').val();
			} else {
				orderadvanceamount = 0;
			}
		}
	}
	var creditnoadjreceipt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','31-3'));
	var creditnoadjissue = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','31-2'));
	var creditnoadjreceiptwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','31-3'));
	var creditnoadjissuewt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','31-2'));
	if(ids != '')
	{
	if(transactiontypeid == 21 || transactiontypeid == 18 || transactiontypeid == 19) {
		$('#commonotherwtsummarygwt').text(parseFloat(getgridcolvalue('saledetailsgrid','','grossweight','sum')).toFixed(round));
		$('#commonotherwtsummarynwt').text(parseFloat(getgridcolvalue('saledetailsgrid','','netweight','sum')).toFixed(round));		
		$('#commonotherwtsummarypieces').text(parseFloat(getgridcolvalue('saledetailsgrid','','pieces','sum')).toFixed(0));
		var appouttags = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','itemtagnumber','count','stocktypeid','62')).toFixed(0);
		var appouttagsreturn = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','itemtagnumber','count','stocktypeid','65')).toFixed(0);
		if(stocktypeid == 62){
			$('#commonotherwtsummarytags').text(appouttags);
		}else if(stocktypeid == 65){
			$('#commonotherwtsummarytags').text(appouttagsreturn);
		}else {
			$('#commonotherwtsummarytags').text(0);
		}
	} else if(transactiontypeid == 26) {
		$('#commonotherwtsummarygwt').text(parseFloat(getgridcolvalue('saledetailsgrid','','paymentgrossweight','sum')).toFixed(round));
		$('#commonotherwtsummarynwt').text(parseFloat(getgridcolvalue('saledetailsgrid','','paymentnetweight','sum')).toFixed(round));		
		$('#commonotherwtsummarypieces').text(parseFloat(getgridcolvalue('saledetailsgrid','','paymentpieces','sum')).toFixed(0));
		$('#commonotherwtsummarytags').text(0);
	} else if(transactiontypeid == 24) {
		{ // paid wt and  issue wt
			totalpaidpurewt = totalpaidpurewtcalc();
			totalissuepurewt = totalissuepurewtcalc();
			$('#spanpaidpurewthidden').val(totalpaidpurewt);
			$('#spanpaidpurewt').text(Math.abs(totalpaidpurewt).toFixed(round));
			$('#spanissuepurewthidden').val(totalissuepurewt);
			$('#spanissuepurewt').text(Math.abs(totalissuepurewt).toFixed(round));
			totalpaidgrstwt = totalpaidgrstwtcalc();
			totalissuegrstwt = totalissuegrstwtcalc();
			$('#spanpaidgrswthidden').val(totalpaidgrstwt);
			$('#spanpaidgrswt').text(Math.abs(totalpaidgrstwt).toFixed(round));
			$('#spanissuegrswthidden').val(totalissuegrstwt);
			$('#spanissuegrswt').text(Math.abs(totalissuegrstwt).toFixed(round));
		}
				
		{  // paid amt and  issue amt
			totalpaid = totalpaidamtcalc();
			issueamt = issueamtcalc();
			$('#sumarypaidamount').val(totalpaid);
			$('#sumpaidamt').text(Math.abs(totalpaid));
			$('#sumaryissueamount').val(issueamt);
			$('#sumissueamt').text(Math.abs(issueamt));
			
		}
	} else if(transactiontypeid == 11 || transactiontypeid == 9 || transactiontypeid == 20  || transactiontypeid == 16 || transactiontypeid == 28) { // sales ,purchase & take order & take repair
		
		{// sales return amt
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				salesreturnwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','20'));
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				salesreturnamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','20'));
				var salesreturnroundoffamt = roundoff(salesreturnamt);
				var diff_salesreturn = salesreturnroundoffamt - salesreturnamt;
				$('#summarysalesreturnroundvalue').val(diff_salesreturn.toFixed(roundamount));
				salesreturnamt = parseFloat(salesreturnamt) + parseFloat(diff_salesreturn);
			}
		}
		{// sales return amt
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				purchasereturnwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','73'));
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				purchasereturnamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','73'));
			}
		}
		{// Calculating the pregrossamount and set grossamt in summary	
		    if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				if(transactiontypeid == 9) { // for purchase we dont check bill level or itemlevel tax etc,use totalamount
					pregrossweight = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','80'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','81')));
					prebillforgrossweight = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','80'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','81')));
					prebillfornetweight = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','80'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','81')));
					prebillforpieces = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','80'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','81')));
				} else {
					pregrossweight = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','11'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','12'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','13'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','89'));
					prebillforgrossweight = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','11'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','12'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','13'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','89'));
					prebillfornetweight = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','11'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','12'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','13'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum','stocktypeid','89'));
					oldjewelwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','19'));
					prebillforpieces = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','11'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','12'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','13'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum','stocktypeid','89'));
					oldjewelwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pureweight','sum','stocktypeid','19'));
				}
				$('#billgrosswt').val(Math.abs(pregrossweight).toFixed(round));
				$('#billforgrosswt').val(Math.abs(prebillforgrossweight).toFixed(round));
				$('#billfornetwt').val(Math.abs(prebillfornetweight).toFixed(round));
				$('#billforpieces').text(Math.abs(prebillforpieces));
				var billgrosswt = $('#billgrosswt').val();
				
				
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{	
				if(transactiontypeid == 9) // for purchase we dont check bill level or itemlevel tax etc,use totalamount
				{
					if(salestaxtypeid == 2) {
						pregrossamount = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','17'))+parseFloat(	getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','81')));
					} else {
						pregrossamount = -(parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','17'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','81')));
					}
				}  
				else  if(salestaxtypeid == 3 && sdiscounttypeid == 3) // bill Level Tax & discount - using grossamount for sum 
				{
					pregrossamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','13')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','89'));
					
					 oldjewelamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','19'));
				} else if(salestaxtypeid == 2 && sdiscounttypeid == 3) // Item level tax + bill level discount
				{
					pregrossamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','13')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossamount','sum','stocktypeid','89'));
					
					oldjewelamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','19'));
				}
				else  // using totalamount for sum 
				{
					
					pregrossamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','13')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','89'));
					
					oldjewelamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','totalamount','sum','stocktypeid','19'));
				}
				// Setting the Gross Amount to final summary
				$('#billgrossamount').val(Math.abs(pregrossamount).toFixed(roundamount)); 
				var billgrossamount = $('#billgrossamount').val();
			}
			
		}
		{// discount and tax only for amount
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				var sumarytaxamount = 0;
				sumarydiscountamount = 0;
				prediscountval = 0;
				{// tax amount calcualtion  bill level tax
					var sumarytaxamount = 0;
					
					if(salestaxtypeid == 3) {
						var autocalculatetax=$('#salestransactiontypeid').find('option:selected').data('autotax');	
						var taxcategoryid = $('#taxcategory').find('option:selected').val();
						if((autocalculatetax == 1 && billleveltaxautoremoval == 0)) {
						  taxautocalculation(0);
						} else if( taxcategoryid > 1 ) {
							taxautocalculation(0);
						} else if( typeof taxcategoryid === 'undefined' || taxcategoryid === null || taxcategoryid == '' ) {
							$('#sumarytaxamount').val(0);
						}
						$('#taxoverlay').fadeOut();
						var sumarytaxamount = $('#sumarytaxamount').val();
					} else if(salestaxtypeid == 2) {
						var autocalculatetax=$('#salestransactiontypeid').find('option:selected').data('autotax');
						if((autocalculatetax == 1 && billleveltaxautoremoval == 0)) {
							taxautocalculation(0);
						}
					}
					
					sumarytaxamount = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','11')) + parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','12')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','13'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','17')) +parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','75'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','81'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','83'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','86'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','taxamount','sum','stocktypeid','89'));
					$('#sumarytaxamount').val(Math.abs(sumarytaxamount).toFixed(2));
					var sumarytaxamount = $('#sumarytaxamount').val();
					
				}
				{// discount calcualtion
					sumarydiscountamount = 0;
					prediscountval = 0;
					if(sdiscounttypeid == 3)
					{
						if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length != 0) 
						{
							billdiscountcalculation();
						}
						sumarydiscountamount = $('#sumarydiscountamount').val();
						var prediscountval = $.trim($('#sumarydiscountamount').val());
						if(prediscountval > 0 )
						{
							$('#sumarydiscountamount').val(parseFloat(prediscountval).toFixed(roundamount));
						}else
						{
							prediscountval = 0;
							$('#sumarydiscountamount').val(0);
						}
					}
					
					$('#prediscountamount').val(prediscountval);
				}
			}
		}
		{// finalamt netamt calculation
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				var paymentpureweight = $("#paymentpureweight").val();
				var mainbillnetwt = $("#billnetwt").val();
				var mainprebillnetwt = $("#prebillnetwt").val();
				if(transactiontypeid == 9) { // for purchase grossamt is negative so we add tax,round etc with negative sign
					finalwt = parseFloat(pregrossweight) + parseFloat(purchasereturnwt);
				} else {	
					finalwt = parseFloat(pregrossweight)-parseFloat(oldjewelwt)-parseFloat(salesreturnwt);
				}
				prebillnetwt = parseFloat(pregrossweight);
				$('#prepaymenttotalwt,#finalbillwt,#billnetwt').val(Math.abs(finalwt).toFixed(round));
				$('#spanbillwt').text(Math.abs(finalwt).toFixed(round));
				$('#billnetwtwithsign').val(finalwt.toFixed(round));
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				
				if(parseFloat(prediscountval) > parseFloat(presumarydiscountamount) && transactiontypeid != '9') // discount check with discspan
				{
					$('#sumarydiscountamount').val(0).trigger('change');
					$('#sumarydiscountamount').focus();
					//alert(presumarydiscountamount);
					//alert(prediscountval);
					alertpopup('Please enter the amount less or equal to Discount Value');
					return false;
				}else 
				{
				var paymenttotalamount = $("#paymenttotalamount").val();
				var mainbillnetamount = $("#billnetamount").val();
				var mainprebillnetamount = $("#prebillnetamount").val();
				
				if(transactiontypeid == 9) // for purchase grossamt is negative so we add tax,round etc with negative sign
				{
					finalamt = parseFloat(pregrossamount) - parseFloat(sumarytaxamount) + parseFloat(sumarydiscountamount) + parseFloat(purchasereturnamt);
					prebillnetamount = parseFloat(pregrossamount) - parseFloat(sumarytaxamount);
				}
				else
				{	
					finalamt = parseFloat(pregrossamount) + parseFloat(sumarytaxamount) - parseFloat(oldjewelamt)- parseFloat(salesreturnamt) - parseFloat(sumarydiscountamount) - parseFloat(orderadvanceamount) ;
					prebillnetamount = parseFloat(pregrossamount) + parseFloat(sumarytaxamount) ;
				}
				//for rounding auto or if changed its manual calculation
				if(adjustmentamountkeypressed == 1)
				{
					if(transactiontypeid == 9) // for purchase grossamt is negative so we add tax,round etc with negative sign
					{
						finalamt = parseFloat(finalamt) - parseFloat(roundvaluewithsign);
						prebillnetamount = parseFloat(finalamt) - parseFloat(roundvaluewithsign);
					}
					else
					{	
						finalamt = parseFloat(finalamt) + parseFloat(roundvaluewithsign);
						prebillnetamount = parseFloat(prebillnetamount) + parseFloat(roundvaluewithsign);
					}
				}
				else if(adjustmentamountkeypressed == 0 ){
					
					if(transactiontypeid == 9) // for purchase grossamt is negative so we add tax,round etc with negative sign
					{
						finalamt = parseFloat(finalamt) - parseFloat(roundvaluewithsign);
						prebillnetamount = parseFloat(finalamt) - parseFloat(roundvaluewithsign);
					}
					else
					{	finalamtrounded = roundoff(finalamt);
						difference = finalamtrounded - finalamt;
						$('#roundvalue').val(difference.toFixed(roundamount));
						var roundvaluewithsign = $('#roundvalue').val();
						finalamt = parseFloat(finalamt) + parseFloat(roundvaluewithsign);
						prebillnetamount = parseFloat(prebillnetamount) + parseFloat(roundvaluewithsign);
					}
					
				}
				
				$('#prepaymenttotalamount,#finalbillamount,#billnetamount').val(Math.abs(finalamt).toFixed(roundamount));
				$('#spanbillamt').text(Math.abs(finalamt).toFixed(roundamount));
				$('#billnetamountwithsign').val(finalamt.toFixed(roundamount));
			    }
			}
			
		}
		{// setting some summary data
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				$('#prebillnetwt').val(Math.abs(prebillnetwt).toFixed(round)); // hidden variable 
				$('#oldjewelwtsummary').val(Math.abs(oldjewelwt).toFixed(round));
				$('#salesreturnwtsummary').val(Math.abs(salesreturnwt).toFixed(round));
				$('#purchasereturnwtsummary').val(Math.abs(purchasereturnwt).toFixed(round));
				if(transactiontypeid == 16){ // for estimate only netamt rest zero
					$('#paymentpureweight,#balpurewthidden,#paymenttotalwthidden').val(0);
					$('#spanbillclosewt').text(0);
				}
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				$('#prebillnetamount').val(Math.abs(prebillnetamount).toFixed(roundamount)); // hidden variable 
				$('#oldjewelamtsummary').val(Math.abs(oldjewelamt).toFixed(roundamount));
				$('#salesreturnsummary').val(Math.abs(salesreturnamt).toFixed(roundamount));
				$('#purchasereturnsummary').val(Math.abs(purchasereturnamt).toFixed(roundamount));
				if(transactiontypeid == 16){ // for estimate only netamt rest zero
					$('#paymenttotalamount,#sumarypendingamount,#paymenttotalamounthidden').val(0);
					$('#sumpendingamt').text(0);
				}
			}
		}
		{// setting billnetamountspan i.e billamount in a/c summary and bill summary section
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				if(parseFloat(finalamt) > 0 ) { // set issue/receipt
					$('.billnetamountspan').text('|BAL|');
				}else if(parseFloat(finalamt) < 0 ) {
						$('.billnetamountspan').text('|ADV|');
				}
			}
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				if(parseFloat(finalwt) > 0 ) { // set issue/receipt
						$('.billwtspan ').text('|BAL|');
				}else if(parseFloat(finalwt) < 0 ) {
						$('.billwtspan').text('|ADV|');
				}
			}
		}
	
	}
	
	{//setting bill and account closing
    var payformnetamt = 0;
	var payformnetwt = 0;
	var manualstatus = accountmanualstatus();	
	var loadbilltype = $("#loadbilltype").find('option:selected').val();
	if(transactiontypeid == 11 || transactiontypeid == 9 || transactiontypeid == 20 || transactiontypeid == 22 || transactiontypeid == 23 ) {
		{// setting bill closing 
			if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				if((transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) && manualstatus == 0)
				{
					finalwt = 0;
				}else{
					finalwt = $('#billnetwtwithsign').val();
				}
				{ // paid wt and  issue wt
					totalpaidpurewt = totalpaidpurewtcalc();
					totalissuepurewt = totalissuepurewtcalc();
					$('#spanpaidpurewthidden').val(totalpaidpurewt);
					$('#spanpaidpurewt').text(Math.abs(totalpaidpurewt).toFixed(round));
					$('#spanissuepurewthidden').val(totalissuepurewt);
					$('#spanissuepurewt').text(Math.abs(totalissuepurewt).toFixed(round));
					totalpaidgrstwt = totalpaidgrstwtcalc();
					totalissuegrstwt = totalissuegrstwtcalc();
					$('#spanpaidgrswthidden').val(totalpaidgrstwt);
					$('#spanpaidgrswt').text(Math.abs(totalpaidgrstwt).toFixed(round));
					$('#spanissuegrswthidden').val(totalissuegrstwt);
					$('#spanissuegrswt').text(Math.abs(totalissuegrstwt).toFixed(round));
				}
				
				{// bill balance wt setting
						
						var wtmaualstatuscheck = 1;
						// p.receipt and advance - p.issue and bal - in these we need to calcualte balamt differently
						if((transactiontypeid == 22 && loadbilltype == 2 ) || (transactiontypeid == 23 && loadbilltype == 3 )){ 
							balancewt = parseFloat(finalwt) + (parseFloat(totalpaidpurewt)) - parseFloat(totalissuepurewt);
						}else{
							balancewt = parseFloat(finalwt) - (parseFloat(totalpaidpurewt)) + parseFloat(totalissuepurewt);
						}
						
						$('#balpurewthidden').val(balancewt.toFixed(round));
						$('#spanbillclosewt').text(Math.abs(balancewt).toFixed(round));
						
						if(paymentstocktypeid == 39 || transactiontypeid == 20 || transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) 
						{
							$('#paymentpureweight,#paymenttotalwthidden').val(0);
						}else
						{
							$('#paymentpureweight').val(Math.abs(balancewt).toFixed(round));
							$('#paymenttotalwthidden').val(balancewt.toFixed(round));
						}
						
						if((transactiontypeid == 20 || transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) && manualstatus == 0){
							$('#balpurewthidden').val(0);
						    $('#spanbillclosewt').text(0);
							wtmaualstatuscheck = 0;
						}else{
							var payformnetwt = parseFloat(Math.abs(finalwt)) - (parseFloat(totalpaidpurewt)) - parseFloat(totalissuepurewt);
							if(parseFloat(balancewt) > 0)
							{
								$('.billbalwtspan').text('|BAL|');
								$("#issuereceipttypeid").select2('val',3);
								$('#wtissuereceiptid').val(3);
							}else if(parseFloat(balancewt) < 0){
								$('.billbalwtspan').text('|ADV|');
								$("#issuereceipttypeid").select2('val',2);
								$('#wtissuereceiptid').val(2);
								payformnetwt = -parseFloat(payformnetwt);
							}else {
								wtmaualstatuscheck = 0;
							}
						}
						if(wtmaualstatuscheck == 0)
						{
							if(transactiontypeid == 11  || transactiontypeid == 20 || transactiontypeid == 23 || transactiontypeid == 25 )
							{
								$('.billbalwtspan').text('|BAL|');
								$("#issuereceipttypeid").select2('val',3);
								$('#wtissuereceiptid').val(3);
							}else if(transactiontypeid == 9  || transactiontypeid == 22 ){
								$('.billbalwtspan').text('|ADV|');
								$("#issuereceipttypeid").select2('val',2);
								$('#wtissuereceiptid').val(2);
							}
						}
				}
			}
			if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
			{
				if((transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) && manualstatus == 0)
				{
					finalamt = 0;
				}else{
					finalamt = $('#billnetamountwithsign').val();
				}
				{  // paid amt and  issue amt
					totalpaid = totalpaidamtcalc();
					issueamt = issueamtcalc();
					$('#sumarypaidamount').val(totalpaid);
					$('#sumpaidamt').text(Math.abs(totalpaid));
					$('#sumaryissueamount').val(issueamt);
					$('#sumissueamt').text(Math.abs(issueamt));
					
				}
						
				{// bill balance amount setting
					var amtmaualstatuscheck = 1;
					// p.receipt and advance - p.issue and bal - in these we need to calcualte balamt differently
					if((transactiontypeid == 22 && loadbilltype == 2 ) || (transactiontypeid == 23 && loadbilltype == 3 )){ 
						balanceamt = parseFloat(finalamt) + (parseFloat(totalpaid)) - parseFloat(issueamt);
					}else{
						balanceamt = parseFloat(finalamt) - (parseFloat(totalpaid)) + parseFloat(issueamt);
					}
					$('#sumpendingamt').text(Math.abs(balanceamt).toFixed(roundamount));
					$('#sumarypendingamount').val(balanceamt.toFixed(roundamount));
					if(paymentstocktypeid == 39 || transactiontypeid == 20 || transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) 
					{
						$('#paymenttotalamount,#paymenttotalamounthidden').val(0);
					}else
					{
						$('#paymenttotalamount').val(Math.abs(balanceamt).toFixed(roundamount));
						$('#paymenttotalamounthidden').val(balanceamt.toFixed(roundamount));
					}
					
					if((transactiontypeid == 20 || transactiontypeid == 22 || transactiontypeid == 23 || transactiontypeid == 25) && manualstatus == 0){
						$('#sumpendingamt').text(0);
					    $('#sumarypendingamount').val(0);
						amtmaualstatuscheck = 0;
					}else{
						// at a time only issue or receipt can be done so we subtract both from abs bill amount 
						var payformnetamt = parseFloat(Math.abs(finalamt)) - (parseFloat(totalpaid)) - parseFloat(issueamt);
						if(parseFloat(balanceamt) > 0)
						{
							$('.billbalamountspan').text('|BAL|');
							$("#issuereceipttypeid").select2('val',3);
							$('#issuereceiptid').val(3);
						}else if(parseFloat(balanceamt) < 0) {
							$('.billbalamountspan').text('|ADV|');
							$("#issuereceipttypeid").select2('val',2);
							$('#issuereceiptid').val(2);
							payformnetamt = -parseFloat(payformnetamt);
						}else {
							amtmaualstatuscheck = 0;
						}
					}
					if(amtmaualstatuscheck == 0)
					{
						if(transactiontypeid == 11  || transactiontypeid == 20 || transactiontypeid == 23 || transactiontypeid == 25)
						{
							$('.billbalamountspan').text('|BAL|');
							$("#issuereceipttypeid").select2('val',3);
							$('#issuereceiptid').val(3);
						}else if(transactiontypeid == 9  || transactiontypeid == 22 ){
							$('.billbalamountspan').text('|ADV|');
							$("#issuereceipttypeid").select2('val',2);
							$('#issuereceiptid').val(2);
						}
					}
				}
			}
			if(transactiontypeid == 23)
			{
				$("#issuereceipttypeid").select2('val',3);
				$('#issuereceiptid').val(3);
				$('#wtissuereceiptid').val(3);
					
			}else if(transactiontypeid == 22)
			{
				$("#issuereceipttypeid").select2('val',2);
				$('#issuereceiptid').val(2);
				$('#wtissuereceiptid').val(2);
			}
	    }
		{// closing
			if(transactiontypeid == 22 || transactiontypeid == 23 )
			{   
				// payment form netamount and wt setting - since for payment different concept. these values are calculated above
				$('#paymenttotalamount').val(Math.abs(payformnetamt).toFixed(roundamount));
				$('#paymentpureweight').val(Math.abs(payformnetwt).toFixed(round));
				$('#paymenttotalamounthidden').val(payformnetamt.toFixed(roundamount));
				$('#paymenttotalwthidden').val(payformnetwt.toFixed(round));
				// closing wt/amt calc 
				if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
				{
					var accountopeningweight = $('#accountopeningweight').val();
					if((transactiontypeid == 22 && loadbilltype == 2 ) || (transactiontypeid == 23 && loadbilltype == 3 )){
						finalclosewt = parseFloat(accountopeningweight) - parseFloat(totalpaidpurewt) + parseFloat(totalissuepurewt)+  parseFloat(creditnoadjreceiptwt) -  parseFloat(creditnoadjissuewt);
					}else{
						finalclosewt = parseFloat(accountopeningweight) - parseFloat(totalpaidpurewt) + parseFloat(totalissuepurewt)+  parseFloat(creditnoadjreceiptwt) -  parseFloat(creditnoadjissuewt);
					}
					$('#accountclosingwt').val((finalclosewt).toFixed(round)); // with actual sign
					$('#spanaccclosewt').text(Math.abs(finalclosewt).toFixed(round));
					if(parseFloat(finalclosewt) > 0){
						$('#spanaccclosewtsymbol').text('|BAL|');					
					} else {
						$('#spanaccclosewtsymbol').text('|ADV|');
					}	
				}
				if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
				{
					var accountopeningamount = $('#accountopeningamount').val();
					if((transactiontypeid == 22 && loadbilltype == 2 ) || (transactiontypeid == 23 && loadbilltype == 3 )){
						finalcloseamt = parseFloat(accountopeningamount) - parseFloat(totalpaid) + parseFloat(issueamt)+  parseFloat(creditnoadjreceipt) -  parseFloat(creditnoadjissue);
					}else{
						finalcloseamt = parseFloat(accountopeningamount) - parseFloat(totalpaid) + parseFloat(issueamt)+  parseFloat(creditnoadjreceipt) -  parseFloat(creditnoadjissue);
					}
					$('#accountclosingamount').val((finalcloseamt).toFixed(roundamount)); // with actual sign
					$('#spanacccloseamt').text(Math.abs(finalcloseamt).toFixed(roundamount));
					if(parseFloat(finalcloseamt) > 0){
						$('#acccloseamtspan').text('|BAL|');					
					} else {
						$('#acccloseamtspan').text('|ADV|');
					}
				}	
			}
			else
			{   // other type closing wt amt calc 
				if(transactionmodeid == 3 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
				{
					var accountopeningweight = $('#accountopeningweight').val();
					finalclosewt = parseFloat(accountopeningweight)  + parseFloat(balancewt) +  parseFloat(creditnoadjreceiptwt) -  parseFloat(creditnoadjissuewt);
					$('#accountclosingwt').val((finalclosewt).toFixed(round)); // with actual sign
					$('#spanaccclosewt').text(Math.abs(finalclosewt).toFixed(round));
					if(parseFloat(finalclosewt) > 0){
						$('#spanaccclosewtsymbol').text('|BAL|');					
					} else {
						$('#spanaccclosewtsymbol').text('|ADV|');
					}	
					
				}
				if(transactionmodeid == 2 || transactionmodeid == 4 || bhavcutbasedsummary == 1)
				{
					var orderadvamtsummary = $('#orderadvamtsummary').val(); 
					var accountopeningamount = $('#accountopeningamount').val();
					finalcloseamt = parseFloat(accountopeningamount) + parseFloat(balanceamt) + parseFloat(orderadvamtsummary) +  parseFloat(creditnoadjreceipt) -  parseFloat(creditnoadjissue);
					$('#accountclosingamount').val((finalcloseamt).toFixed(roundamount)); // with actual sign
					$('#spanacccloseamt').text(Math.abs(finalcloseamt).toFixed(roundamount));
					if(parseFloat(finalcloseamt) > 0){
						$('#acccloseamtspan').text('|BAL|');					
					} else {
						$('#acccloseamtspan').text('|ADV|');
					}
				}	
			}
		}
	}
	else if(transactiontypeid == 25) { // Contra
			$("#issuereceipttypeid").select2('val',2);
			// issue amt
			issueamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','27-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','28-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','29-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','79-2'));
			$('#sumaryissueamount').val(issueamt);
			$('#sumissueamt').text(Math.abs(issueamt));
			var sumaryissueamount = $('#sumaryissueamount').val();
			var accountopeningamount = $('#accountopeningamount').val();
			finalcloseamt = parseFloat(accountopeningamount) - parseFloat(sumaryissueamount);
			$('#accountclosingamount').val((finalcloseamt).toFixed(roundamount)); // with actual sign
			$('#spanacccloseamt').text(Math.abs(finalcloseamt).toFixed(roundamount));
			if(parseFloat(finalcloseamt) > 0){
				$('#acccloseamtspan').text('|BAL|');					
			} else {
				$('#acccloseamtspan').text('|ADV|');
			}
	 }
	}	
	}
	else 
	{	
		$('#accountclosingwt,#spanissuepurewthidden,#prebillnetwt,#paymenttotalwthidden,#finalbillwt,#spanbillwt,#billnetwtwithsign,#billgrossamount,#billnetamount,#prepaymenttotalamount,#paymenttotalamount,#finalbillamount,#sumarytaxamount,#roundvalue,#sumarydiscountamount,#sumarypendingamount,#oldjewelamtsummary,#salesreturnsummary,#purchasereturnsummary,#orderadvamtsummary,#orderadvamtsummaryhidden,#billweighthidden,#spanpaidpurewthidden,#prepaymentpureweight,#paymentpureweight,#balpurewthidden,#prepaymenttotalwt,#editpaymentpureweight,#oldjewelwtsummary,#purchasereturnwtsummary,#salesreturnwtsummary,#billnetwt,#billgrosswt,#sumarypaidamount,#accountclosingamount,#salesdetailcount,#liveadvanceclose,#orderadvadjustamthidden,#paymenttotalamounthidden,#prebillnetamount,#sumaryissueamount,#billnetamountwithsign,#spanpaidgrswthidden,#spanissuegrswthidden,#billforgrosswt,#billfornetwt,#summarysalesreturnroundvalue').val(0);
		$('#spanbillamt,#sumpendingamt,#spanbillwt,#spanpaidpurewt,#spanbillclosewt,#sumpaidamt,#spanacccloseamt,#spanaccopenamt,#spanaccclosewt,#spanaccopenwt,#sumissueamt,#spanissuepurewt,#spanissuegrswt,#spanpaidgrswt,#billforpieces').text(0);
		$('.commonotherwtsummaryspan').text('0');
		$('.oradvamtspan').text('');
		$('#orderadvcreditno,#issuereceiptmergetext').val('');
		loadaccountopenclose($("#accountid").val());
		if(transactiontypeid == 22 || transactiontypeid == 23 )
		{
			setcreditsummary();
		}
		$("#salescreateform :input").attr("readonly", false);
		if(billeditstatus != '1') {
			$('#salespersonid,#paymentirtypeid,#loadbilltype,#salestransactiontypeid,#transactionmodeid,#salesdate,#ratecuttypeid').prop('disabled',false);
		} else {
			$('#salespersonid,#paymentirtypeid,#loadbilltype,#salestransactiontypeid,#transactionmodeid,#salesdate,#ratecuttypeid').prop('disabled',true);
		}
		if(approvalnoeditstatus == 0) {
			$('#salestransactiontypeid,#transactionmodeid').prop('disabled',false);
		}
		if(transactiontypeid != 25) {
			$('#addaccount,#accountsearchevent,#editaccount').show();
		}
		billeditstatus = 0;
	}
	{// sales - order tag hide/show based on stocktype
		if(transactiontypeid == 11)
		{ /** this was hide bcz arvind informed to kumaresan, that we can do all kind of stocktypes with order tag. **/
			/* var stocktypeid = "stocktypeid";
			var salesstatus = 0;
			var stocktype_arr = '83';
			var allstocktypeid = $.trim(getgridcolcolumnvalue('saledetailsgrid','','stocktypeid'));
			var details=allstocktypeid.split(",");
			 $.each(details, function (index, value) {
					if(value == '11' || value == '12' || value == '13' || value == '20' || value == '74'){
						salesstatus = 1;
					 }
					
				 });
			if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
				if($('#generatesalesid').val() == '') {
					$("#"+stocktypeid+" option[value='"+stocktype_arr+"']").removeClass("ddhidedisplay");
					$("#"+stocktypeid+" option[value='"+stocktype_arr+"']").prop('disabled',false);
				}
			}else{
				if(salesstatus == 1)
				{
					$("#"+stocktypeid+" option[value='"+stocktype_arr+"']").addClass("ddhidedisplay");
					$("#"+stocktypeid+" option[value='"+stocktype_arr+"']").prop('disabled',true);
				}
			} */
		}
	}
  if(status == 3)
  {
	  {// on edit summary setting
		loadaccountopenclose($("#accountid").val());
		if(transactiontypeid == 22 || transactiontypeid == 23) 
		{
			var sumpaidamt =  $('#sumpaidamt').text();
			var sumissueamt = $('#sumissueamt').text();
			var sumpaidwt =  $('#spanpaidpurewt').text();
			var sumissuewt = $('#spanissuepurewt').text();
			var editfinalamtreceipt = $('#editfinalamtreceipt').val();
			var editfinalamtissue = $('#editfinalamtissue').val();
			var editfinalweightreceipt = $('#editfinalweightreceipt').val();
			var editfinalweightissue = $('#editfinalweightissue').val();
			var editweightissuereceiptid = $('#editweightissuereceiptid').val();
			var accledgerissuereceiptid = $('#accledgerissuereceiptid').val();
			{// a/c bill closings
				var accountopeningamount = $("#accountopeningamount").val();
				var facamount = parseFloat(accountopeningamount) + parseFloat(sumpaidamt) + parseFloat(sumissueamt)+ parseFloat(creditnoadjreceipt) - parseFloat(creditnoadjissue);
				$('#accountopeningamount').val((facamount).toFixed(roundamount)); // with actual sign
				$('#spanaccopenamt').text(Math.abs(facamount).toFixed(roundamount));
				if(parseFloat(facamount) > 0){
					$('#accopenamtspan').text('|BAL|');					
				} else if(parseFloat(facamount) == 0){
					$('#accopenamtspan').text('');
				}else{
					$('#accopenamtspan').text('|ADV|');
				}
				
				var accountopeningweight = $("#accountopeningweight").val();
				var facwtt = parseFloat(accountopeningweight) + parseFloat(sumpaidwt) - parseFloat(sumissuewt);
				$('#accountopeningweight').val((facwtt).toFixed(round)); // with actual sign
				$('#spanaccopenwt').text(Math.abs(facwtt).toFixed(round));
				if(parseFloat(facwtt) > 0){
					$('#spanaccopenwtsymbol').text('|BAL|');					
				} else if(parseFloat(facwtt) == 0){
					$('#spanaccopenwtsymbol').text('');
				}else{
					$('#spanaccopenwtsymbol').text('|ADV|');
				}
			}
		}else if(transactiontypeid == 25) 
		{
			$("#issuereceipttypeid").select2('val',2);
			// issue amt
			issueamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','27-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','28-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','29-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','79-2'));
			$('#sumaryissueamount').val(issueamt);
			$('#sumissueamt').text(Math.abs(issueamt));
			var sumaryissueamount = $('#sumaryissueamount').val();
			var accountopeningamount = parseFloat($('#accountopeningamount').val()) + parseFloat(sumaryissueamount);
			$('#accountopeningamount').val(accountopeningamount);
			$('#spanaccopenamt').text(Math.abs(accountopeningamount).toFixed(roundamount));
			if(parseFloat(accountopeningamount) > 0){
				$('#accopenamtspan').text('|BAL|');					
			} else {
				$('#accopenamtspan').text('|ADV|');
			}
			finalcloseamt = parseFloat(accountopeningamount) - parseFloat(sumaryissueamount);
			$('#accountclosingamount').val((finalcloseamt).toFixed(roundamount)); // with actual sign
			$('#spanacccloseamt').text(Math.abs(finalcloseamt).toFixed(roundamount));
			if(parseFloat(finalcloseamt) > 0){
				$('#acccloseamtspan').text('|BAL|');					
			} else {
				$('#acccloseamtspan').text('|ADV|');
			}
		}
		else
		{
			var sumpaidamt =  $('#sumpaidamt').text();
			var sumissueamt = $('#sumissueamt').text();
			var sumpaidwt =  $('#spanpaidpurewt').text();
			var sumissuewt = $('#spanissuepurewt').text();	
			var oldjewelwtsummary  = $("#oldjewelwtsummary").val(); 
			var oldjewelamtsummary  = $("#oldjewelamtsummary").val();
			var takeorderadvanceamt = parseFloat(oldjewelamtsummary) + parseFloat(sumpaidamt) - parseFloat(sumissueamt);
			var takeorderadvancewt = parseFloat(oldjewelwtsummary) + parseFloat(sumpaidwt) - parseFloat(sumissuewt);
			
			{// a/c bill closings
				var accountopeningamount = $("#accountopeningamount").val();
				var billbalanceamt = $("#sumarypendingamount").val();
				var billirid = $("#issuereceiptid").val();
				if(transactiontypeid == 20) 
				{
					var newaccopenamt = parseFloat(accountopeningamount) + parseFloat(takeorderadvanceamt);
				}
				else
				{
					var newaccopenamt = parseFloat(accountopeningamount) - parseFloat(billbalanceamt) - parseFloat(creditnoadjreceipt) + parseFloat(creditnoadjissue);
				}
				$('#accountopeningamount').val((newaccopenamt).toFixed(roundamount)); // with actual sign
				$('#spanaccopenamt').text(Math.abs(newaccopenamt).toFixed(roundamount));
				if(parseFloat(newaccopenamt) > 0){
					$('#accopenamtspan').text('|BAL|');					
				} else if(parseFloat(newaccopenamt) == 0){
					$('#accopenamtspan').text('');
				}else{
					$('#accopenamtspan').text('|ADV|');
				}
				
				var accountopeningweight = $("#accountopeningweight").val();
				var billbalancewt = $("#balpurewthidden").val();
				var billwtirid = $("#wtissuereceiptid").val();
				if(transactiontypeid == 20) 
				{
					var newaccopenwt = parseFloat(accountopeningweight) + parseFloat(takeorderadvancewt);
				}
				else
				{
					var newaccopenwt = parseFloat(accountopeningweight) - parseFloat(billbalancewt) - parseFloat(creditnoadjreceiptwt) + parseFloat(creditnoadjissuewt);
				}
				$('#accountopeningweight').val((newaccopenwt).toFixed(round)); // with actual sign
				$('#spanaccopenwt').text(Math.abs(newaccopenwt).toFixed(round));
				if(parseFloat(newaccopenwt) > 0){
					$('#spanaccopenwtsymbol').text('|BAL|');					
				} else if(parseFloat(newaccopenwt) == 0){
					$('#spanaccopenwtsymbol').text('');
				}else{
					$('#spanaccopenwtsymbol').text('|ADV|');
				}
			}
		  }
		  finalsummarycalc(0);	
		}
			
   }
}

// set main summary while doing estimate to sales estimatetosales
function setmainsummary(summarydiscount,summarytax,summaryround,prevdiscount,preroundvalue,prevsumarytax)
{
	var salestaxtypeid = $("#taxtypeid").val();
	var sdiscounttypeid = $("#sdiscounttypeid").val();
	var sdiscounttypeid = $("#sdiscounttypeid").val();
	var presumarytaxamount = 0;
	var presumarydiscountamount = 0;
	
	var finalsumarytaxamount = 0;
	var finalsumarydiscountamount = 0;
	var finalroundvalue = 0;
	if(salestaxtypeid == 3)
	{
		presumarytaxamount = prevsumarytax;
	}else{
		presumarytaxamount = 0;
	}
	if(sdiscounttypeid == 3)
	{
		presumarydiscountamount = prevdiscount;
	}else{
		presumarydiscountamount = 0;
	}
	finalsumarytaxamount = parseFloat(presumarytaxamount)+parseFloat(summarytax);
	$('#sumarytaxamount').val(finalsumarytaxamount);
	finalsumarydiscountamount = parseFloat(presumarydiscountamount)+parseFloat(summarydiscount);
	$('#sumarydiscountamount').val(finalsumarydiscountamount);
	finalroundvalue = parseFloat(preroundvalue)+parseFloat(summaryround);
	$('#roundvalue').val(finalroundvalue);
	finalsummarycalc(1);
}
{// issue receipt data get based on accoutn id
	function issuereceiptdataget(status) {
		var accountid = $('#accountid').val();
		//var accounttypeid = $("#accounttypeid").find('option:selected').val();
		var accounttypeid = $("#selaccounttypeid").val();
		var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
		$('#paymentirtypeid').prop('disabled',true);
		var manualstatus = accountmanualstatus();
		if(manualstatus == 0){
			$('#creditno').attr('readonly',false);
			if(status == 0){
				creditoverlaytriggerplace = 1;
				$('#creditoverlaystatus').val(status);
				issuereceiptlocalgrid();
				issuereceiptgrid();
				creditfieldhideshow();
				$('#processid').select2('val',0);
				$('#accountledgerid').val(1);
				$('#creditsalesdate').val($('#salesdate').val());
				var loadbilltype = $("#loadbilltype").find('option:selected').val();
				$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',loadbilltype);
			}
		} else {
			$('#creditno').attr('readonly',true);
			if(status == 0){ creditoverlaytriggerplace = 1;}else if(status == 1){creditoverlaytriggerplace = 2;}
			$('#creditoverlaystatus').val(status);
			$("#processoverlay").show();
			issuereceiptlocalgrid();
			issuereceiptgrid();
			if(salestransactiontypeid == 24){
				 var accountid = $('#paymentaccountid').find('option:selected').val();
			}
			issuereceiptgriddatafetch(accountid,status);
			$("#issuereceiptoverlay").show();
			creditfieldhideshow();
			$('#processid').select2('val',0);
			$('#accountledgerid').val(1);
			$('#creditsalesdate').val($('#salesdate').val());
			if($('#creditoverlaystatus').val() == 1) {
				var loadbilltype = $("#loadbilltype").find('option:selected').val();
			} else if($('#creditoverlaystatus').val() == 0) {
				var loadbilltype = $("#issuereceipttypeid").find('option:selected').val();
			}
			$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',loadbilltype);
			$("#processoverlay").hide();
		}
	}
}
// reset issue receipt related fields
function resetirfields() {
	$('#creditno').val('');		
}
// takeoder stocktype hide/show based on stock type entry
function orderstocktypehideshow(stocktype) {
	var stocktypeid = "stocktypeid";
	if(stocktype == '19' || stocktype == '75' || stocktype == '82') {
		$("#"+stocktypeid+" option[value='19']").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='19']").prop('disabled',false);
		$("#"+stocktypeid+" option[value='75']").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='75']").prop('disabled',false);
		$("#"+stocktypeid+" option[value='82']").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='82']").prop('disabled',false);
	}
	//var accounttypeid = $("#accounttypeid").find('option:selected').val(); 
    var accounttypeid = $("#selaccounttypeid").val();
	if(accounttypeid == 16){
		$("#"+stocktypeid+" option[value='19']").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='19']").prop('disabled',true);
		$("#"+stocktypeid+" option[value='75']").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='75']").prop('disabled',true);
		$("#"+stocktypeid+"").select2('val',82).trigger('change');
	}else  if(accounttypeid == 6){
		$("#"+stocktypeid+" option[value='82']").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value='82']").prop('disabled',true);
		$("#"+stocktypeid+"").select2('val',75).trigger('change');
	}
}
// delivery order based stock type hide/show
function deliveryorderstocktypehideshow(stocktype) {
	/* var stocktypeid = "stocktypeid";
	var deliveryorderstatus = 0;
	var stocktype_arr = '11,12,13,20,19,83,74';
	var stocktype_array = stocktype_arr.split(",");
	var allstocktypeid = $.trim(getgridcolcolumnvalue('saledetailsgrid','','stocktypeid'));
	var details=allstocktypeid.split(",");
	 $.each(details, function (index, value) {
			if(value == '83'){
			  deliveryorderstatus = 1;
			  $('#orderadvamtsummary-div').show();
			  $('#salesreturnsummary-div').hide();
			}
			
		 });
	if((stocktype == '19' || stocktype == '83') && deliveryorderstatus == 1) {
		for(var opi =0;opi < stocktype_array.length;opi++)
		{
			$("#"+stocktypeid+" option[value='"+stocktype_array[opi]+"']").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value='"+stocktype_array[opi]+"']").prop('disabled',false);
		}	
	}
	if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length == 0) {
		$('#orderadvamtsummary-div').hide();
		$('#salesreturnsummary-div').show();
		var stocktype_array_in = stocktype_arr.split(",");
		for(var opj =0;opj < stocktype_array_in.length;opj++)
		{
			$("#"+stocktypeid+" option[value='"+stocktype_array_in[opj]+"']").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value='"+stocktype_array_in[opj]+"']").prop('disabled',false);
		}
	}else 
	{
		if((stocktype == '19' || stocktype == '83') && deliveryorderstatus == 1) {
			$("#"+stocktypeid+" option[value='19']").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value='19']").prop('disabled',false);
			$("#"+stocktypeid+" option[value='83']").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value='83']").prop('disabled',false);
			var in_stocktype_arr = '11,12,13,20,74';
			var in_stocktype_array = in_stocktype_arr.split(",");
			for(var opk =0;opk < in_stocktype_array.length;opk++)
			{
				$("#"+stocktypeid+" option[value='"+in_stocktype_array[opk]+"']").addClass("ddhidedisplay");
				$("#"+stocktypeid+" option[value='"+in_stocktype_array[opk]+"']").prop('disabled',true);
			}	
		}
	} */
}
// get order advance amt
function getorderadvanceamt() {
	var mainordernumberid = $('#mainordernumberid').val();
	$.ajax({
			url:base_url+"Sales/getorderadvanceamt",
			data: "salesid=" +mainordernumberid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var finalamt = parseFloat($('#orderadvadjustamthidden').val()) + parseFloat(data['finalamtreceipt']);
				$('.oradvamtspan').text('('+finalamt+')');
				$('#orderadvamtsummaryhidden').val(finalamt);
				$('#orderadvcreditno').val(data['creditno']);
			}
		});
}
// get open/close amt & weight for purchase
function loadaccountopenclose(accountid) 
{
	var trans_type = $("#salestransactiontypeid").find('option:selected').val();
	$('#accountopeningamountirid,#accountopeningweightirid,#accountopeningamount,#accountopeningweight,#accountclosingamount,#accountclosingwt').val(0);
	$('#spanaccopenamt,#spanacccloseamt,#spanaccopenwt,#spanaccclosewt,#accountadvamt,#accountcreditamt').text(0);
	$('#accopenamtspan,#acccloseamtspan,#spanaccopenwtsymbol').text('');
	if(trans_type != 24 &&  accountid != $("#estimatedefaultaccid").val()){
	$.ajax({
			url:base_url+"Sales/loadaccountopenclose",
			data: "accountid=" +accountid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#accounttypeid').select2('val',data['accounttypeid']);
				$('#selaccounttypeid').val(data['accounttypeid']);
				$('#accountopeningamountirid').val(data['issuereceiptid']);
				$('#accountopeningweightirid').val(data['weightissuereceiptid']);
				
				
				$('#accountadvamt').text(data['advanceamt']);
				$('#accountcreditamt').text(data['creditamt']);
				if(data['advanceamt'] != 0 && trans_type == 11) {
					$('#accountadvinfo').show();
				} else {
					$('#accountadvinfo').hide();
				}
				if(data['creditamt'] != 0 && trans_type == 11) {
					$('#accountcreditinfo').show();
				} else {
					$('#accountcreditinfo').hide();
				}
				
				$('#spanaccopenamt,#spanacccloseamt').text(data['finalamt']);
				$('#spanaccopenwt,#spanaccclosewt').text(data['finalwt']);
				
				if(data['issuereceiptid'] == 3) // final amtreceipt > 0 cust advance
				{
					$('#accopenamtspan,#acccloseamtspan').text('|ADV|');
					$('#accountopeningamount,#accountclosingamount').val('-' + data['finalamt']);
				}else if(data['issuereceiptid'] == 2) // final amtissue > 0 cust bal
				{
					$('#accopenamtspan,#acccloseamtspan').text('|BAL|');
					$('#accountopeningamount,#accountclosingamount').val(data['finalamt']);
				}
				if(data['weightissuereceiptid'] == 3) {
					$('#spanaccopenwtsymbol,#spanaccclosewtsymbol').text('|ADV|');
					$('#accountopeningweight,#accountclosingwt').val('-' + data['finalwt']);
				} else if(data['weightissuereceiptid'] == 2) {
					$('#spanaccopenwtsymbol,#spanaccclosewtsymbol').text('|BAL|');
					$('#accountopeningweight,#accountclosingwt').val(data['finalwt']);
				}
			}
		});
	}
}
// load itemdetail
function loaditemdetail(id) {
	$('#itemdetail').empty();
	$('#itemdetail').append($("<option value=''></option>"));
	$.ajax({
		url:base_url+"Sales/loaditemdetail",
		data:{salesid:id,status:1},
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if(data == '')
			{
				alertpopupdouble("No Bills to Load");
			}else{
				$.each(data, function(index) {
					if(data[index]['itemtagnumber'] == ''){
						$("#itemdetail")
						.append($("<option></option>")
						.attr("data-deliverystatusid",data[index]['deliverystatusid'])
						.attr("data-deliverystatusname",data[index]['deliverystatusname'])
						.attr("value",data[index]['salesdetailid'])
						.text(data[index]['stocktypename']+' - '+data[index]['purityname']+' - '+data[index]['productname']+' - '+data[index]['grossweight']));
					}else {
						$("#itemdetail")
						.append($("<option></option>")
						.attr("data-deliverystatusid",data[index]['deliverystatusid'])
						.attr("data-deliverystatusname",data[index]['deliverystatusname'])
						.attr("value",data[index]['salesdetailid'])
						.text(data[index]['stocktypename']+' - '+data[index]['itemtagnumber']+' - '+data[index]['purityname']+' - '+data[index]['productname']+' - '+data[index]['grossweight']));
					}
				});
			}
		}
	});
}
// load order itemdetail
function loadorderitemdetail(id) {
	$('#cancelitemdetail').empty();
	$('#cancelitemdetail').append($("<option value=''></option>"));
	$.ajax({
				url:base_url+"Sales/loaditemdetail",
				data:{salesid:id,status:2},
				type: "POST",
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$.each(data, function(index) {
							$("#cancelitemdetail")
							.append($("<option></option>")
							.attr("data-orderstatusid",data[index]['orderstatusid'])
							.attr("data-orderstatusname",data[index]['orderstatusname'])
							.attr("value",data[index]['salesdetailid'])
							.text(data[index]['purityname']+' - '+data[index]['productname']+' - '+data[index]['grossweight']+' - '+data[index]['orderstatusname']));
						});
				}
	});
}
// push to pending delivery order
function pushtopendingdeliverycreate() {
	var form = $("#p2pdeliveryform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	$('#processoverlay').show();
	$.ajax( {
        url: base_url +"Sales/pushtopendingdeliverycreate",
        data: "datas=" + datainformation,
		type: "POST",
		success: function(msg) {
			if(msg == 'SUCCESS') {
				$('#processoverlay').hide();
				resetFields();
				$('#currentstatusid,#newstatusid').val('');
				$('#tocounter').select2('val','');
				var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
				loaditemdetail(datarowid);
				$('#itemdetail').select2('focus');
				alertpopupdouble(savealert);
			} else {
				resetFields();
				$('#currentstatusid,#newstatusid').val('');
				$('#tocounter').select2('val','');
				$('#itemdetail').select2('focus');
				alertpopup(submiterror);
			}
        },
    });
}
//RFID Tagnumber unique check
function rfitagnocheck() {
	var primaryid = '';
	var accname = $("#deliveryrfidtagno").val();
	var elementpartable = 'itemtag';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Itemtag/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			datatype :"text",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "RFID Tagno already exists!";
				}
			} else {
				return "RFID Tagno already exists!";
			}
		} 
	}
}
// load itemtag for delivery
function loaditemtag(value) {
	$('#pendingitemdetail').empty();
	$('#pendingitemdetail').append($("<option value=''></option>"));
	$.ajax({
				url:base_url+"Sales/loaditemtag",
				data: "statusid=" +value,
				type: "POST",
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data != '') {
						$.each(data, function(index) {
							$("#pendingitemdetail")
								.append($("<option></option>")
								.attr("value",data[index]['itemtagid'])
								.text(data[index]['itemtagnumber']+' - '+data[index]['purityname']+' - '+data[index]['productname']+' - '+data[index]['grossweight']));
						});
					}
				}
	});
}
// push to delivery update
function pushtodeliveryupdate() {
	var form = $("#p2ddeliveryform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	$('#processoverlay').show();
	$.ajax( {
        url: base_url +"Sales/pushtodeliveryupdate",
        data: "datas=" + datainformation,
		type: "POST",
		success: function(msg) {
			if(msg == 'SUCCESS') {
				$('#processoverlay').hide();
				resetFields();
				$('#pendingitemdetail').empty();
				$('#deliverystatusid').select2('val','');
				$('#process').select2('focus');
				alertpopupdouble(savealert);
			} else {
				$('#processoverlay').hide();
				resetFields();
				$('#pendingitemdetail').empty();
				$('#deliverystatusid').select2('val','');
				$('#process').select2('focus');
				alertpopup(submiterror);
			}
        },
    });
}
// Place order overlay status update
function orderstatusoverlaysave() {
	var salesdetailid = getselectedrowids('orderitemsgrid');
	var ordertransactionpage = $('#ordertransactionpage').val();
	var poordernumberid = $('#poordernumberid').val();
	var poordernumbernumber = $('#poordernumberid').find('option:selected').attr('data-ordernumber');
	var orderitemsstatusacc = $('#orderitemsstatusacc').find('option:selected').val();
	var orderitemdate = $("#orderitemdate").val();
	var orderitemsstatuscomment = $("#orderitemsstatuscomment").val();
	var adminorderitemsstatuscomment = $("#adminorderitemsstatuscomment").val();
	var formdata = $("#orderitemsform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$('#processoverlay').show();
	$.ajax( {
        url: base_url +"Sales/postatusoverlaysubmitform",
        data: "datas="+datainformation+amp+"orderitemdate="+orderitemdate+amp+"salesdetailid="+salesdetailid+amp+"ordertransactionpage="+ordertransactionpage+amp+"orderitemsstatuscomment="+orderitemsstatuscomment+amp+"adminorderitemsstatuscomment="+adminorderitemsstatuscomment+amp+"orderitemsstatusacc="+orderitemsstatusacc,
		type: "POST",
		success: function(msg) {
			var nmsg =  $.trim(msg);
			$("#orderitemsstatusacc").select2('val',0);
			$("#orderitemsstatuscomment,#adminorderitemsstatuscomment").val('');
			$("#processoverlay").hide();
			if(nmsg == 'SUCCESS'){
				if(rejectreviewordertemplateid > 1 && orderitemsstatusacc != 0) {
					var today = new Date();
					var time = today.getHours() + today.getMinutes() + today.getSeconds();
					printautomate(poordernumberid,rejectreviewordertemplateid,poordernumbernumber+time);
				}
				alertpopup(savealert);
				orderitemsgrid();
			} else {
				alertpopup(submiterror);
			}
        },
    });
}
// Load All Place Orders based vendor
function loadplaceordernumber(vendorid) {
	var ordertransactionpage = $('#ordertransactionpage').val();
	$('#poordernumberid').empty();
	$('#poordernumberid').append($("<option></option>"));
	 $.ajax({
        url: base_url + "Sales/loadplaceordernumber",
		data: "accountid="+vendorid+"&ordertransactionpage="+ordertransactionpage,
       	async:false,
		cache:false,
		type: "POST",
		dataType:'json',
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#poordernumberid')
					.append($("<option></option>")
					.attr("value",data[index]['salesid'])
					.attr("data-ordernumber",data[index]['salesnumber'])
					.attr("data-poorderstatusid",data[index]['poorderstatusid'])
					.attr("data-postatuscomment",data[index]['postatuscomment'])
					.attr("data-poadminstatuscomment",data[index]['poadminstatuscomment'])
					.text(data[index]['salesnumber']));
				});
				
			}	
        },
    });
}
// Delivery Place Order to Admin Management
function deliverypooverlaysave() {
	var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
	var form = $("#deliveryplaceordform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	$('#processoverlay').show();
	$.ajax( {
        url: base_url +"Sales/deliverypooverlaysave",
        data: "datas=" + datainformation+"&salesid="+datarowid,
		type: "POST",
		success: function(msg) {
			if(msg == 'SUCCESS') {
				$('#processoverlay').hide();
				resetFields();
				$('#deliveryplaceordoverlay').fadeOut();
				alertpopupdouble(savealert);
			} else {
				$('#processoverlay').hide();
				resetFields();
				$('#deliveryplaceordoverlay').fadeOut();
				alertpopupdouble('Something went wrong. Please try again.');
			}
        },
    });
}
// cancel order update
function cancelorderupdate() {
	var form = $("#cancelorderform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	$('#processoverlay').show();
	$.ajax( {
        url: base_url +"Sales/cancelorderupdate",
        data: "datas=" + datainformation,
		type: "POST",
		success: function(msg) {
				$('#processoverlay').hide();
				$('#currentorderstatus,#cancelcomment').val('');
				$('#cancelitemdetail').select2('val','');
				$('#cancelitemdetail').select2('focus');
			if(msg == 'SUCCESS') {
				var datarowid = $('#salesgrid div.gridcontent div.active').attr('id');
				loadorderitemdetail(datarowid);
				alertpopupdouble(savealert);
			}else {
				alertpopup(submiterror);
			}
        },
    });
}
// credit entry navigate
function creditentrynavigate() {
	var accountid = $('#accountid').val();
	issuereceiptgrid();
	var creditoverlaystatus = $('#creditoverlaystatus').val();
	if(creditoverlaystatus == 1) { // Payment issue/receipt - load bill type
		issuereceiptgriddatafetch(accountid,1);
	}else if(creditoverlaystatus == 0) {
		issuereceiptgriddatafetch(accountid,0);
	}
	datarowselectevt();
	creditsummaycalc();
	Materialize.updateTextFields();
}
// credit field hide/show 
function creditfieldhideshow() {
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	var creditoverlaystatus = $('#creditoverlaystatus').val();
	if(creditoverlaystatus == 1) { // Payment issue/receipt - load bill type
		var loadbilltype = $('#loadbilltype').find('option:selected').val();
	}else if(creditoverlaystatus == 0) {
		var loadbilltype = $('#issuereceipttypeid').find('option:selected').val();
	}
	if(transactionmodeid == 2) { // amount
		$('.creditwtdiv').hide();
		$('.creditamtdiv').show();
		$('.mainfinalwthiddendiv').hide();
		$('.mainfinalamthiddendiv').show();
		
	}else if(transactionmodeid == 3) { // purewt
		$('.creditamtdiv').hide();
		$('.creditwtdiv').show();
		$('.mainfinalwthiddendiv').show();
		$('.mainfinalamthiddendiv').hide();
		
	}else if(transactionmodeid == 4) {  // amount purewt
		$('.creditamtdiv,.creditwtdiv').show();
		$('.mainfinalwthiddendiv,.mainfinalamthiddendiv').show();
	}
}
//credit form to grid 
function creditformtogriddata_jewel(gridname,method,position,datarowid) {
	method = typeof method == 'undefined' ? 'ADD' : method;
	var dataval = "";
	var align = '';
	var opndivcontent = '';
	var content = '';
	var clsdivcontent = '';	
	//if(deviceinfo != 'phone'){
		var n = $('#'+gridname+' .gridcontent div.data-content div').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.data-content').empty();
		} else{
			n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
			n = parseInt(n);
		}
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<ul class="inline-list">';
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   var txtboxid = $(e).attr('id');
		   var uitype = $('#'+txtboxid).data('uitype');
		   var txtboxname = $('#'+txtboxid).data('fieldname');
		    var txtboxlabel = $('#'+txtboxid).data('label');
		   var txtwidth = $('#'+txtboxid).data('width');
		   var txtclass = $('#'+txtboxid).data('class');
		   var viewtype = $('#'+txtboxid).data('viewtype');
		   var viewstyle = (viewtype=='0') ? ' display:none':'';
		   if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
			  
			   dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
			   datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
		   } else if(uitype == '31'){
				   dataval = $('#creditreference').find('option:selected').data('refername');
				    datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			   }
           else if(uitype == '32'){
				   dataval = $('#processid').find('option:selected').data('refername');
				    datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			}else if(uitype == '33'){
				   dataval = $('#creditpaymentirtypeid').find('option:selected').data('refername');
				    datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			}
			else if(uitype == '34'){
				   dataval = $('#creditissuereceiptid').find('option:selected').data('refername');
				    datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			}else if(uitype == '35'){
				   dataval = $('#creditweightissuereceiptid').find('option:selected').data('refername');
				    datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			}	
			else {
			   dataval = $('#'+txtboxname+'').val();
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
		   }
		});
		content+='</ul>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.data-content:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.data-content').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).append(content);
			}
		}
	//}	
}
// credit overlay summary calc
function creditsummaycalc(){
	var finalamt = Math.abs(parseFloat(getgridcolvalue('issuereceiptlocalgrid','','mainfinalamthidden','sum')));
	var finalwt = Math.abs(parseFloat(getgridcolvalue('issuereceiptlocalgrid','','mainfinalwthidden','sum')));
	$('#credittotalamount').val(finalamt);
	$('#credittotalpurewt').val(finalwt);
}
// load bill trigger overlay
function loadbilltrigger() {
	var salestransactiontypeid = $("#salestransactiontypeid").find('option:selected').val();
	var ratecuttypeid = $("#ratecuttypeid").find('option:selected').val();
	var manualstatus = accountmanualstatus();
	$('#loadbilltype').prop('disabled',false);
	if(manualstatus == 1){
		if(salestransactiontypeid == 22) {
			$('#loadbilltype').select2('val',3).trigger('change');		
		}else if(salestransactiontypeid == 23) {
			$('#loadbilltype').select2('val',2).trigger('change');	
		}
	}else  if(manualstatus == 0){
		//var accountopeningamount = $('#accountopeningamount').val();
		//var accountopeningweight =  $('#accountopeningweight').val();
		//alert(ratecuttypeid);
		if(salestransactiontypeid == 22 && ratecuttypeid == 1) {
			$('#loadbilltype').select2('val',3).trigger('change');
		} else if(salestransactiontypeid == 22 && ratecuttypeid == 2) {
			$('#loadbilltype').select2('val',2).trigger('change');	
		} else if(salestransactiontypeid == 23 && ratecuttypeid == 1) {
			$('#loadbilltype').select2('val',2).trigger('change');	
		} else if(salestransactiontypeid == 23 && ratecuttypeid == 2) {
			$('#loadbilltype').select2('val',3).trigger('change');	
		}
		$('#loadbilltype').prop('disabled',true);
	} 
}
function jsonconvertdata(creditgridata,status) {
	$.ajax( {
        url: base_url +"Sales/jsonconvertdata",
        data: "datas=" + creditgridata,
		type: "POST",
		success: function(msg) {
				if(status == 1) {
					$('#creditoverlayhidden').val(msg);
				}else if(status == 0) {
					$('#creditadjusthidden').val(msg);
					if(salesdetailentryeditstatus == 0 ) {
						$('#paymentdetailsubmit').trigger('click');
					}else {
						$('#paymentdetailupdate').trigger('click');
					}
					$('#creditadjustvaluehidden,#creditadjusthidden').val('');
				}
        },
    });
}
function stonejsonconvertdata(stonegridata) {
	$.ajax({
        url: base_url +"Sales/stonejsonconvertdata",
        data: "datas=" + stonegridata,
		type: "POST",
		success: function(msg) {
			$('#hiddenstonedetail').val(msg);
		},
    });
}
function stonejsonconvertdataautotrigger(stonegridata) {
	var jsonreturndata = '';
	$.ajax({
        url: base_url +"Sales/stonejsonconvertdata",
        data: "datas=" + stonegridata,
		type: "POST",
		async:false,
		cache:false,
		success: function(msg) {
			jsonreturndata = msg;
		},
    });
	return jsonreturndata;
}
// set credit number value
function setcreditnoval() {
	var finalcreditgridata = $('#creditoverlayvaluehidden').val();
	var result = new Array();
	if(finalcreditgridata!= ''){
		var result = JSON.parse(finalcreditgridata);
	}
	var list = new Array();
	$.each(result, function(index) {
		list.push(result[index].overlaycreditno);
	});
	$('#creditno').val(list);
}
// set credit summary
function setcreditsummary() {
	var issuereceiptid = $("#loadbilltype").find('option:selected').val();
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	var wtissuereceiptid = issuereceiptid;
	//Amount
	if(issuereceiptid == 2) { // issue
		finalamt = $('#credittotalamount').val();
		$('.billnetamountspan,.billbalamountspan').text('|BAL|');
		$('#issuereceiptid').val(3);
	} else if(issuereceiptid == 3) { //receipt
		finalamt = -parseFloat($('#credittotalamount').val());
		$('.billnetamountspan,.billbalamountspan').text('|ADV|');
		$('#issuereceiptid').val(2);
	}
	//Weight
	if(wtissuereceiptid == 2) { // issue
		finalwt = $('#credittotalpurewt').val();
		$('.billwtspan,.billbalwtspan').text('|BAL|');
		$('#wtissuereceiptid').val(3);
	} else if(wtissuereceiptid == 3) { //receipt
		finalwt = -parseFloat($('#credittotalpurewt').val());
		$('.billwtspan,.billbalwtspan').text('|ADV|');
		$('#wtissuereceiptid').val(2);
	}
	if(salestransactiontypeid == 23) {
		$("#issuereceipttypeid").select2('val',3);
	} else if(salestransactiontypeid == 22) {
		$("#issuereceipttypeid").select2('val',2);
	}
	$('#spanbillamt,#sumpendingamt').text(Math.abs(finalamt).toFixed(roundamount));
	$('#billnetamountwithsign,#sumarypendingamount').val(parseFloat(finalamt).toFixed(roundamount));
	$('#spanbillwt,#spanbillclosewt').text(Math.abs(parseFloat(finalwt)).toFixed(round));
	$('#billnetwtwithsign,#balpurewthidden').val(parseFloat(finalwt).toFixed(round));
	if($('#saledetailsgrid .gridcontent div.data-content div.data-rows').length != 0) {	
		finalsummarycalc(0);
	}
}
// set credit overlay data
function creditoverlaydataset() {
	clearceditoverlayform();
	$('#creditoverlaystatus').val(1);  
	issuereceiptlocalgrid();
	if($('#creditoverlayhidden').val() != ''){
		var hiddentaxdata = $.parseJSON($('#creditoverlayhidden').val());
		loadinlinegriddata('issuereceiptlocalgrid',hiddentaxdata.rows,'json');
	}
	var accountid = $('#accountid').val();
	issuereceiptgrid();
	issuereceiptgriddatafetch(accountid,1);
	creditoverlaytriggerplace = 2;
	$("#issuereceiptoverlay").show();
	creditfieldhideshow();
	$('#processid').select2('val',0);
	$('#accountledgerid').val(1);
	var loadbilltype = $("#loadbilltype").find('option:selected').val();
	$('#creditissuereceiptid,#creditweightissuereceiptid').select2('val',loadbilltype);
	creditsummaycalc();
	$("#processoverlay").hide();
}
// reset credit overlay form
function clearceditoverlayform() {
	$('#overlaycreditno').val('');
	$('#creditreference,#creditpaymentirtypeid').select2('val','');
	$('#amtissue,#amtreceipt,#weightissue,#weightreceipt,#finalamtissue,#finalamtreceipt,#finalweightissue,#finalweightreceipt,#mainfinalamthidden,#mainfinalwthidden').val(0);
}
function totalpaidamtcalc()	{
	 totalpaid = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','27-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','28-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','29-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','79-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','39-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','31-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','37-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','38-2'));
	 return totalpaid;
 }
function issueamtcalc()	{
	 issueamt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','26-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','27-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','28-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','29-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','79-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','39-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','31-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','37-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymenttotalamount','sum','issuereceiptmergetext','38-3'));
	 return issueamt;
 }
function totalpaidpurewtcalc()	{
	totalpaidpurewt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','49-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','31-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','38-3'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','37-2'));
	return totalpaidpurewt;
}
function totalissuepurewtcalc()	{
	totalissuepurewt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','49-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','31-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','38-2'))+parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentpureweight','sum','issuereceiptmergetext','37-3'));
	return totalissuepurewt;
}
function totalpaidgrstwtcalc()	{
	totalissuepurewt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentgrossweight','sum','issuereceiptmergetext','49-3'));
	return totalissuepurewt;
}
function totalissuegrstwtcalc()	{
	totalissuepurewt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentgrossweight','sum','issuereceiptmergetext','49-2'));
	return totalissuepurewt;
}
function paymentamountvalidation(amount,status)	{
	/* var billamount =  Math.abs($('#billnetamountwithsign').val()); 
	var sumarypaidamount =  Math.abs($('#sumarypaidamount').val()); 
	var sumaryissueamount =  Math.abs($('#sumaryissueamount').val());
	var innerpaymenttotalamount = 0;
	if(salesdetailentryeditstatus == 1) {
		var selectedrow = $('#editinnerrowid').val();
		var innerpaymenttotalamount = Math.abs(getgridcolvalue('saledetailsgrid',selectedrow,'paymenttotalamount','')); 
	}
	var finalamount = parseFloat(billamount - sumarypaidamount - sumaryissueamount + innerpaymenttotalamount).toFixed(roundamount);
	var amount = parseFloat(amount).toFixed(roundamount);	
	//alert(finalamount+'-'+billamount+'-'+sumarypaidamount+'-'+sumaryissueamount+'-'+amount+'-'+innerpaymenttotalamount);
	if( parseFloat(amount) >  parseFloat(finalamount) )
	{
		alertpopupdouble('Kindly enter value below or equal to '+finalamount+' Rs');
		$('#paymenttotalamount').val(0);
		return 1;
	}else{
		return 0;
	} */
	return 0;
}
function paymentweightvalidation(paymentpureweight,status)	{
	
	/* var billwt =  Math.abs($('#billnetwtwithsign').val()); 
	var spanpaidpurewthidden =  Math.abs($('#spanpaidpurewthidden').val()); 
	var spanissuepurewthidden =  Math.abs($('#spanissuepurewthidden').val());
	var innerpaymenttotalwt = 0;
	if(salesdetailentryeditstatus == 1) {
		var selectedrow = $('#editinnerrowid').val();
		var innerpaymenttotalwt = Math.abs(getgridcolvalue('saledetailsgrid',selectedrow,'paymentpureweight','')); 
	}
	var finalwt = parseFloat(billwt - spanpaidpurewthidden - spanissuepurewthidden + innerpaymenttotalwt).toFixed(round);
	var paymentpureweight = parseFloat(paymentpureweight).toFixed(round);	
	if( parseFloat(paymentpureweight) >  parseFloat(finalwt) )
	{
		alertpopupdouble('Kindly enter value below or equal to '+finalwt+' Gms');
		$('#paymentpureweight').val(0);
		return 1;
	}else{
		return 0;
	} */
	return 0;
}
function bhavcutbasedsummaryshowhide(){
	bhavcutbasedsummary = 0;
	var gridrowdata = getgridallrowids('saledetailsgrid');
	var takeordersalesdetailid = gridrowdata.toString();
	var takeordersalesdetails=takeordersalesdetailid.split(",");
	$.each(takeordersalesdetails, function (index, value) {
		var takeordertypeid = getgridcolvalue('saledetailsgrid',value,'paymentstocktypeid','');
		if(takeordertypeid == '37' || takeordertypeid == '38'){
		  bhavcutbasedsummary = 1;
		}
	});
	if(bhavcutbasedsummary == 1)
	{
		$('#pureweightsummary,#amountsummary').show();
	}else{
		var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
		if(transactionmodeid == 2) {
			$('#amountsummary').show();
			$('#pureweightsummary').hide();
		}else if(transactionmodeid == 3) {
			$('#amountsummary').hide();
			$('#pureweightsummary').show();
		}
		else if(transactionmodeid == 4) {
			$('#pureweightsummary,#amountsummary').show();
		}
	}

}
function ratecuttypechange() {
	var ratecuttypeid=$('#ratecuttypeid').find('option:selected').val();
	var salestransactiontypeid=$('#salestransactiontypeid').find('option:selected').val();
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	if((salestransactiontypeid == 22 && ratecuttypeid == 2) || (salestransactiontypeid == 23 && ratecuttypeid == 2)){
		$('#pureweightsummary').show();
		$("#paymentstocktypeid").select2('val','');
		$("#paymentstocktypeid option ").addClass("ddhidedisplay");
		$("#paymentstocktypeid option ").prop('disabled',true);
		if(transactionmodeid == 2){
			$("#paymentstocktypeid option[value=37]").removeClass("ddhidedisplay");
			$("#paymentstocktypeid option[value=37]").prop('disabled',false);
			$('#paymentstocktypeid').select2('val',37).trigger('change');
		}else if(transactionmodeid == 3){
			$("#paymentstocktypeid option[value=38]").removeClass("ddhidedisplay");
			$("#paymentstocktypeid option[value=38]").prop('disabled',false);
			$('#paymentstocktypeid').select2('val',38).trigger('change');
		}
		if(ratecuttypeid == 2 && transactionmodeid == 4) {
			$('#ratecuttypeid').select2('val',1).trigger('change');
			alertpopup('Rate cut not applicable for AmtPurewt calc type');
		}
		$('#dataaddsbtn').hide();
	} else if((salestransactiontypeid == 22 && ratecuttypeid == 1) || (salestransactiontypeid == 23 && ratecuttypeid == 1)) {
		$('#pureweightsummary').hide();
		$('#dataaddsbtn').show();
		$('#transactionmodeid').trigger('change');
	}
}			
function resettransactionfields(){ 
	$("#spanaccopenamt,#spanaccopenwt,#spanbillclosewt,#spanbillcloseamt,#spanbillwt,#spanbillamt,#sumoldjewels,#sumsalesreturn,#sumbillamount,#spanaccclosewt,#spanacccloseamt,#spanpaidpurewt,#spanissuepurewt,#spanpaidgrswt,#spanissuegrswt,#accountadvamt,#accountcreditamt").text(0);
	$("#accountopeningamount,#accountopeningweight,#wastagelesspercent,#sumarygrossamount,#sumarypendingamount,#sumarynetamount,#sumarybillamount,#sumarysalesretamount,#sumaryoldjewelamount,#sumarytaxamount,#sumarydiscountamount,#finalsumarybillamount,#sumarypaidamount,#billgrossamount,#billnetamount,#oldjewelamtsummary,#prewastage,#premakingcharge,#prelstcharge,#purchasereturnsummary,#salesreturnsummary,#orderadvamtsummary,#orderadvamtsummaryhidden,#paymenttotalamounthidden,#billweighthidden,#spanpaidpurewthidden,#prepaymentpureweight,#paymentpureweight,#balpurewthidden,#purchasereturnwtsummary,#prepaymenttotalwt,#editpaymentpureweight,#oldjewelwtsummary,#salesreturnwtsummary,#billnetwt,#billgrosswt,#salesdetailcount,#sumaryissueamount,#accountopeningamountirid,#accountopeningweightirid,#finalamthidden,#finalwthidden,#purchasecheckgrwt,#spanpaidgrswthidden,#spanissuegrswthidden,#billforgrosswt,#billfornetwt,#itemwastagevalue,#itemmakingvalue,#itemflatvalue,#itemlstvalue,#itemrepairvalue,#itemchargesamtvalue").val(0);
	$("#discountpercent,#singlediscounttotal,#giftnumber,#giftremark,#giftdenomination").val('0');
	$("#sumgwt,#sumwastage,#sumgross,#sumswt,#summaking,#sumtax,#sumnwt,#sumcharge,#sumdiscount,#smpieces,#sumstone,#sumnetamount,#sumpendingamt,#sumpaidamt,#sumpurewt,#sumpurewttoamt,#sumamttopurewt,#paycash,#paycheque,#paycard,#paydd,#payadvamt,#paychit,#payvoucher,#payamtpurewt,#paypurewtamt,#paypurewt,#oldgrosswt,#oldstonewt,#olddustwt,#oldnetwt,#oldpieces,#oldwastageless,#oldgrossamt,#oldtaxamt,#oldnetamt,#oldpurewt,#srgrosswt,#srstonewt,#srnetwt,#srpieces,#srcharges,#srstoneamt,#srgrossamt,#srtaxamt,#srdiscount,#srnetamt,#srpurewt,#sumbilltax,#sumbilldiscount,#sumflatcharge,#sumissueamt,#billforpieces").text(0);
	$('#approvaloutreturnproductid,#approvaloutreturntagno,#approvalouttagnumberarray,#approvaloutrfidnumberarray,#issuereceiptmergetext,#creditoverlayhidden,#creditadjusthidden,#creditoverlayvaluehidden,#creditadjustvaluehidden,#creditoverlaystatus,#tagimage,#untagvalidatemergetext,#untagvalidatecountermergetext').val('');
	$('#tagimagedisplay').empty();
}
// get lst charge
function getlstcharge(stoneid) {
	var accid=$("#accountid").val();
	var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
	transactiontype = typeof transactiontype == 'undefined' ? '1' : transactiontype;
	$.ajax({
		url:base_url +"Sales/getratefromstonename",
		data:{stoneid:stoneid,accid:accid},
		type: "POST",
		dataType:'json',	
		success: function(data) {
			if(transactiontype == 9) {
				$("#lstchargeclone").val(data.purchaserate).trigger('change');
			} else {
				if($('#loosestonecharge').val() == 1) {  // salescharge
					$("#lstchargeclone").val(data.stonerate).trigger('change');
				}else if($('#loosestonecharge').val() == 2) { // puchasecharge
					$("#lstchargeclone").val(data.purchaserate).trigger('change');
				}
			}
		}
	});
}
// load lst charge
function loadlstcharge(value) {
	// lst charge value set
	if(value =='LST-G' || value == 'LST-P' || value == 'LST-C') {
		if(value =='LST-G'){
			$('#lstchargekeylabel').text('-G');
		}else if(value =='LST-P'){
			$('#lstchargekeylabel').text('-P');
		}else if(value =='LST-C'){
			$('#lstchargekeylabel').text('-C');
		}
		$("#lstchargeclone").attr('keyword',value);
		$("#lstchargespan,#lstcharge").attr('keyword',value);
		var stoneid=$("#product").find('option:selected').data('stoneid');
		getlstcharge(stoneid);
	}
}
// get bulk rfid data
function getbulktagnumberdata() {
	$('#processoverlay').show();
	var innergridtagidsarr = [];
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['itemtagid'] != '1' && griddata[i]['itemtagid'] != ''){
		 innergridtagidsarr.push(griddata[i]['itemtagid']);
		}
	}
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	var stocktypeid = $('#stocktypeid').find('option:selected').val();	
	var approvalnumber = $('#approvalnumber').find('option:selected').val();	 
	var rfiddeviceno = $('#rfiddeviceno').find('option:selected').val();
	rfiddeviceno = typeof rfiddeviceno == 'undefined' ? '' : rfiddeviceno;
	var accid=$("#accountid").val(); 	
	$.ajax({
			url:base_url +"Sales/getbulktagnumberdata?salestransactiontypeid="+salestransactiontypeid+"&approvalnumber="+approvalnumber+"&stocktypeid="+stocktypeid+"&accountid="+accid+"&innergridtagids="+innergridtagidsarr+"&rfiddeviceno="+rfiddeviceno,
			type: "POST",
			dataType:'json',	
			success: function(data) {
				$('#totaltags').val(data['totalcount']);
				$('#loadtags').val(data['loadedcount']);
				$('#totaltagshidden').val(data['id']);
				$('#totaltagsnohidden').val(data['tagno']);
				Materialize.updateTextFields();
			}
	});
	$('#processoverlay').hide();
}
function getbulkdatarows(id) {
	var innergridtagidsarr = [];
	var griddata = getgridrowsdata('saledetailsgrid');
	var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
	for(var i=0;i< criteriacnt;i++){
		if(griddata[i]['itemtagid'] != '1' && griddata[i]['itemtagid'] != ''){
		 innergridtagidsarr.push(griddata[i]['itemtagid']);
		}
	}
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var approvalnumber = $('#approvalnumber').find('option:selected').val();
	var accountid = $("#accountid").val();
	$.ajax({
		url:base_url+"Sales/bulksalesgriddatafetch?innergridtagids="+innergridtagidsarr+"&id="+id+"&stocktypeid="+stocktypeid+"&approvalnumber="+approvalnumber+"&accountid="+accountid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			estimate_loadinlinegriddata('saledetailsgrid',data['estimatedata'].rows,'json');
			$('#totaltagshidden').val('');
			/* //row based check box
			$(".bulksalesgrid_rowchkboxclass").click(function(){
				$('.bulksalesgrid_headchkboxclass').removeAttr('checked');
				bulksalesgridgetcheckboxrowid('bulksalesgrid');
			}); */
			/* data row select event */
			datarowselectevt();
			/* var hideprodgridcol = ['stocktypeid','product','purity','counter','employeepersonid','employeepersonidname','orderitemsizename','salescomment','orderduedate','ordernumber','placeordernumber','receiveordernumber','wastageclone','makingchargeclone','flatchargeclone','certfinal','totalchargeamount','wastagespan','makingchargespan','flatchargespan','stockgrossweight','stockpieces','partialgrossweight','modeid','paymentstocktypeid','paymentmodeid','itemtagid','salesdetailid','gwcaculation','ratelesscalc','hiddenstonedata','taxgriddata','discountdata','issuereceiptid','paymentaccountid','hiddentaxdata','itemdiscountcalname','hiddenstonedetail','balaceproductid','wastagespanlabel','makingchargespanlabel','wastagekeylabel','makingchargekeylabel','ckeyword','ckeywordvalue','ckeywordoriginalvalue','ckeywordfinalkey','ckeywordfinalvalue','totalchargeamount','sumarydiscountlimit','discamt','estimationnumber','olditemdetails','statusoldjewelvoucher','additionalchargeamt','paymentstocktypename','paymenttotalamount','bankname','paymentrefnumber','paymentreferencedate','paymentaccountname','cardtype','cardtypename','approvalcode','ratepergram','wastage','makingcharge','stonecharge','taxamount','discount','grossamount','flatcharge','cflat','dustweight','wastageless','totalamount','paymentaccountidname','chitbookno','chitbookname','chitamount','totalpaidamount','chitschemetypeid','balancecounter','taxcategory','olditemdetailsname','prewastage','premakingcharge','prelstcharge','lottypeid','lottypeidname','purchasemode','paymenttouch','pureweight','paymentproduct','paymentproductname','paymentpurity','paymentpurityname','paymentcounter','paymentcountername','paymentgrossweight','paymentnetweight','paymentmelting','newpaymenttouch','paymentpureweight','paymentratepergram','orderitemsize','paymentcomment','countername','paymentemployeepersonid','paymentemployeepersonidname','ordernumber','vendoraccountid','issuereceipttypeidname','issuereceipttypeid','issuereceiptmergetext','mainsalesdetailid','category','categoryname','oldsalesreturntax','creditadjustvaluehidden','creditadjusthidden','tagimage','lstchargeclone','lstchargespan','lstchargekeylabel','lstchargespanlabel','lstcharge','ordernumbername'];//LST
			gridfieldhide('bulksalesgrid',hideprodgridcol);
			/* column resize */
			//columnresize('bulksalesgrid'); */
			finalsummarycalc(0);
			alertpopup('Successfully converted');
		},
	});
	if(estimatemainsavetrigger == 1){
		$('#dataaddsbtn').trigger('click');
	}
}
// clear rfid table
function clearrfidtable() {
	$.ajax({
		url:base_url +"Sales/clearrfidtable",
		type: "POST",
		dataType:'json',	
		success: function(data) {
			if(data == 'SUCCESS') {
			alertpopup('RFID Table cleared successfully');
			}
		}
	});
}
function accountmanualstatus() {
	//var accounttypeid = $("#accounttypeid").find('option:selected').val();
	var accounttypeid = $("#selaccounttypeid").val();
	var manualstatus = 0;
	if(accounttypeid == '6' || accounttypeid == '18' || accounttypeid == '19') { // customer
		if($('#customerwise').val() == 1) {
			manualstatus = 1;
		} else {
			manualstatus = 0;
		}
	} else if(accounttypeid == '16') {
		if($('#vendorwise').val() == 1) {
			manualstatus = 1;
		} else  {
			manualstatus = 0;
		}
	}	
	return manualstatus;
}
function journalaccountmanualstatus() {
	var accounttypeid = $("#paymentaccountid").find('option:selected').data('accounttypeid');
	var manualstatus = 0;
	if(accounttypeid == 6 || accounttypeid == '18' || accounttypeid == '19') { // customer
		if($('#customerwise').val() == 1) {
		  manualstatus = 1;
		} else {
		  manualstatus = 0;
		}
	}else if(accounttypeid == 16) {
		if($('#vendorwise').val() == 1) {
		  manualstatus = 1;
		} else {
		  manualstatus = 0;
		}
	} else {
		manualstatus = 0;
	}
	return manualstatus;
}
function ratepergramdivlsthide() {
	var metalid = $('#purity').find('option:selected').data('metalid');
	var transactionmodeid = $('#transactionmodeid').find('option:selected').val();
	if(metalid == 4 && transactionmodeid == 2) {
		$('#ratepergram-div').hide(); 
	} else {
		$('#ratepergram-div').show(); 
	}
}
function getvalidatethetagdata(status) {
	if($('#multitagscantagstextarea').val() == '') {
		alertpopup('Kindly Enter Tag No in the Scanning Area');
	} else {
		var multitagscantagstextarea = $('#multitagscantagstextarea').val().split('\n');
		var multitagscantagstextarea = multitagscantagstextarea.toString();
		var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
		var multiscanmode = $('#multiscanmode').val();
		var stocktypeid = $('#stocktypeid').find('option:selected').val();
		var approvalnumber = $('#approvalnumber').find('option:selected').val();
		var innergridtagidsarr = [];
		var griddata = getgridrowsdata('saledetailsgrid');
		var criteriacnt = $('#saledetailsgrid div.gridcontent div.data-content div').length;
		for(var i=0;i< criteriacnt;i++) {
			if(griddata[i]['itemtagid'] != '1' && griddata[i]['itemtagid'] != '') {
				innergridtagidsarr.push(griddata[i]['itemtagid']);
			}
		}
		$.ajax({
			url:base_url+"Sales/getvalidatethetagdata?multitagscantagstextarea="+multitagscantagstextarea+"&multiscanmode="+multiscanmode+"&salestransactiontypeid="+salestransactiontypeid+"&stocktypeid="+stocktypeid+"&approvalnumber="+approvalnumber+"&innergridtagids="+innergridtagidsarr ,
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#multiscantotaltags').val(data['totalcount']);
				$('#multiscanloadtags').val(data['loadedcount']);
				$('#multiscantotaltagshidden').val(data['id']);
				$('#multiscantotaltagsnohidden').val(data['tagno']);
				if(status == 1) {
					$('#multiscanconfirmoverlay').fadeIn(); 
					$('#multiscanaddsbtn').focus();  
					Materialize.updateTextFields();
				} else if(status == 2) {
					multiscanmainsave(status);
				}
			},
		});	
	}
}
function multiscanmainsave(status) {
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	$('#processoverlay').show();
	if(multitagbarcode == 2) {
		if(salestransactiontypeid == 18 && approvaloutcalculation == 'NO') {
			if($('#multiscantotaltagshidden').val() != '') {
				if(status == 2) { estimatemainsavetrigger = 1; }
				getbulkdatarows($('#multiscantotaltagshidden').val());
			} else {
				alertpopup('Please scan or enter active/correct tags.');
			}
		} else if(salestransactiontypeid == 11 || salestransactiontypeid == 16 || salestransactiontypeid == 18 && approvaloutcalculation == 'YES') {
			rfidtagarray = $('#multiscantotaltagsnohidden').val();
			if(rfidtagarray != '') {
				rfidtagarray = rfidtagarray.split(",");
				if(rfidtagarray.length > 0){
					rfidbasedtagfetch = 0;
					$('#itemtagnumber').val(rfidtagarray[0]).trigger('change');
				}
			} else {
				alertpopup('Please scan or enter active/correct tags.');
			}
		}
	}
	$('#multitagscan').fadeOut();
	$('#multiscanconfirmoverlay').fadeOut();
	$('#processoverlay').hide();
	$('#multitagscantagstextarea').val('');
}
function getapprovaloutuntagweight() {
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	var salesdetailid = $('#salesdetailid').val();
	if(untagwtvalidate == 1 || untagwtshow == 1) {
		if(stocktypeid == '66' || stocktypeid == '25') {
			$.ajax({
				url: base_url +"Sales/appoutuntagupdate",
				data:{salestransactiontypeid:salestransactiontypeid,salesdetailid:salesdetailid},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) {   
				    $('#stocknetweight').val(parseFloat(msg.pendingnwt).toFixed(roundweight));
					$('#stockgrossweight').val(parseFloat(msg.pendinggwt).toFixed(roundweight));
					$('#stockpieces').val(parseInt(msg.pendingpieces));
					$('#netweight_validatewt').text($('#stocknetweight').val());
					$('#grossweight_validatewt').text($('#stockgrossweight').val());
					$('#pieces_validatewt').text($('#stockpieces').val());
				}
			});
		}
	} else {
		$('#stocknetweight,#stockgrossweight,#stockpieces').val(0);
		$('#netweight_validatewt,#grossweight_validatewt,#pieces_validatewt').text('');
	}
}
function getweight(stocktypeid,product,purity,counter,status) {
  if(untagwtvalidate == 1 || untagwtshow == 1){
   if(stocktypeid == '73' || stocktypeid == '12' || stocktypeid == '49' || stocktypeid == '63' ) {
	if(product > 1 && purity > 1) {
		if((counterstatus == 'YES' && counter > 1) || counterstatus == 'NO'){
			 if(counterstatus == 'YES'){
				 var valgridcolname = 'untagvalidatecountermergetext';
				 var valgridcolnamecheck = loggedinbranchid+'-'+product+'-'+purity+'-'+counter;
			 }else{
				  var valgridcolname = 'untagvalidatemergetext';
				  var valgridcolnamecheck = loggedinbranchid+'-'+product+'-'+purity;
			 }
			var datarowid = $('#generatesalesid').val();
			$.ajax(
			{
				url: base_url +"Sales/getweight",
				data:{stocktypeid:stocktypeid,product:product,purity:purity,branchid:loggedinbranchid,counter:counter,datarowid:datarowid},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) 
				{   
					if(msg == '') {
						if(status == 1){
							$('#stocknetweight,#stockgrossweight,#stockpieces').val(0);
							$('#netweight_validatewt,#grossweight_validatewt,#pieces_validatewt').text('0');
						}else if(status == 2){
							$('#paymentstocknetweight,#paymentstockgrossweight,#paymentstockpieces').val(0);
							$('#paymentnetweight_validatewt,#paymentgrossweight_validatewt,#paymentpieces_validatewt').text('0');
						} 
					} else {
					var gridnetwt  = 0;
					var gridgrosswt  = 0;
					var gridpieces  = 0;
					var innereditgridnetwt  = 0;
					var innereditgridgrosswt  = 0;
					var innereditgridpieces  = 0;
					if(status == 1){
						var gridnetwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','netweight','sum',valgridcolname,valgridcolnamecheck));
						 var gridgrosswt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum',valgridcolname,valgridcolnamecheck));
						 var gridpieces = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','pieces','sum',valgridcolname,valgridcolnamecheck));
						 if(salesdetailentryeditstatus == 1){
						 var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
						 var innereditgridnetwt = getgridcolvalue('saledetailsgrid',selectedrow,'netweight',''); 
						 var innereditgridgrosswt = getgridcolvalue('saledetailsgrid',selectedrow,'grossweight','');
						 var innereditgridpieces = getgridcolvalue('saledetailsgrid',selectedrow,'pieces','');
						}
						$('#stocknetweight').val(parseFloat(parseFloat(msg.netweight) - parseFloat(gridnetwt) + parseFloat(innereditgridnetwt)).toFixed(roundweight));
						$('#stockgrossweight').val(parseFloat(parseFloat(msg.grossweight) - parseFloat(gridgrosswt) + parseFloat(innereditgridgrosswt)).toFixed(roundweight));
						$('#stockpieces').val(parseInt(parseInt(msg.pieces) - parseInt(gridpieces) + parseInt(innereditgridpieces)));
						$('#netweight_validatewt').text($('#stocknetweight').val());
						$('#grossweight_validatewt').text($('#stockgrossweight').val());
						$('#pieces_validatewt').text($('#stockpieces').val());
				 
					}else if(status == 2){
						var gridnetwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentnetweight','sum', valgridcolname,valgridcolnamecheck));
						var gridgrosswt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','paymentgrossweight','sum',valgridcolname,valgridcolnamecheck));
						var gridpieces = 0;
						var issuereceitpid = $('#issuereceipttypeid').find('option:selected').val();
						if(salesdetailentryeditstatus == 1) {
							var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
							var innereditgridnetwt = getgridcolvalue('saledetailsgrid',selectedrow,'paymentnetweight',''); 
							var innereditgridgrosswt = getgridcolvalue('saledetailsgrid',selectedrow,'paymentgrossweight','');
							var innereditgridpieces = 0;
							issuereceitpid = getgridcolvalue('saledetailsgrid',selectedrow,'issuereceiptid','');
						}
						if(issuereceitpid == 2) {
							$('#paymentstocknetweight').val(parseFloat(parseFloat(msg.netweight) - parseFloat(gridnetwt) + parseFloat(innereditgridnetwt)).toFixed(roundweight));
							$('#paymentstockgrossweight').val(parseFloat(parseFloat(msg.grossweight) - parseFloat(gridgrosswt) + parseFloat(innereditgridgrosswt)).toFixed(roundweight));
							$('#paymentstockpieces').val(parseInt(msg.pieces) - parseInt(gridpieces) + parseInt(innereditgridpieces));
						} else if(issuereceitpid == 3) {
							$('#paymentstocknetweight').val(parseFloat(parseFloat(msg.netweight) + parseFloat(gridnetwt) + parseFloat(innereditgridnetwt)).toFixed(roundweight));
							$('#paymentstockgrossweight').val(parseFloat(parseFloat(msg.grossweight) + parseFloat(gridgrosswt) + parseFloat(innereditgridgrosswt)).toFixed(roundweight));
							$('#paymentstockpieces').val(parseInt(msg.pieces) + parseInt(gridpieces) + parseInt(innereditgridpieces));
						}
						$('#paymentgrossweight_validatewt').text($('#paymentstockgrossweight').val());
						$('#paymentnetweight_validatewt').text($('#paymentstocknetweight').val());
						$('#paymentpieces_validatewt').text($('#paymentstockpieces').val());
					}
				}
			  }
				
			});
		}else{
			 if(status == 1){
				$('#stocknetweight,#stockgrossweight,#stockpieces').val(0);
				$('#netweight_validatewt,#grossweight_validatewt,#pieces_validatewt').text('');
			 }else if(status == 2){
				$('#paymentstocknetweight,#paymentstockgrossweight,#paymentstockpieces').val(0);
				$('#paymentnetweight_validatewt,#paymentgrossweight_validatewt,#paymentpieces_validatewt').text('');
			 }
		}
	 }else{
		 if(status == 1){
			$('#stocknetweight,#stockgrossweight,#stockpieces').val(0);
			$('#netweight_validatewt,#grossweight_validatewt,#pieces_validatewt').text('');
		 }else if(status == 2){
			$('#paymentstocknetweight,#paymentstockgrossweight,#paymentstockpieces').val(0);
			$('#paymentnetweight_validatewt,#paymentgrossweight_validatewt,#paymentpieces_validatewt').text('');
		 }
	 }
	}
   }
}

function validategetweight(stocktypeid,status,fieldid) { 
	if(untagwtvalidate == 1){
		var issuereceipttypeid = $('#issuereceipttypeid').find('option:selected').val();
		if(stocktypeid == '73' || stocktypeid == '12' || stocktypeid == '63' || stocktypeid == '66' || stocktypeid == '25' || (stocktypeid == '49' && issuereceipttypeid == '2')) {
			var stocknetweight = $('#stocknetweight').val();
			var stockgrossweight = $('#stockgrossweight').val();
			var stockpieces = $('#stockpieces').val();
			var paymentstocknetweight = $('#paymentstocknetweight').val();
			var paymentstockgrossweight = $('#paymentstockgrossweight').val();
			var paymentstockpieces = $('#paymentstockpieces').val();
			if(status == 3) {	
				var stockpieces = $('#stockpieces').val();
				var pieces = $('#pieces').val();
				if(parseFloat(pieces) > parseFloat(stockpieces)) {
					$('#pieces').val('');
					alertpopup('Stock Available only '+stockpieces+' pcs. Kindly Enter equal or less than '+stockpieces+' pcs ');
					return false;
				}
			} else if(status == 1) {	
				var grosswt = $('#'+fieldid+'').val();
				if(parseFloat(grosswt) > parseFloat(stockgrossweight)) {
					$('#'+fieldid+'').val(0).trigger('change');  
					alertpopup('Stock Available only '+stockgrossweight+' gms. Kindly Enter equal or less than '+stockgrossweight+' gms ');
					return false;
				}
			} else if(status == 2) {	
				var netwt = $('#'+fieldid+'').val();
				if(parseFloat(netwt) > parseFloat(stocknetweight)) {
					$('#'+fieldid+'').val(0).trigger('change');  
					alertpopup('Stock Available only '+stocknetweight+' gms. Kindly Enter equal or less than '+stocknetweight+' gms ');
					return false;
				}
			} else if(status == 4) {	
				var netwt = $('#'+fieldid+'').val();
				if(parseFloat(netwt) > parseFloat(paymentstocknetweight)) {
					$('#'+fieldid+'').val(0).trigger('change');  
					alertpopup('Stock Available only '+paymentstocknetweight+' gms. Kindly Enter equal or less than '+paymentstocknetweight+' gms ');
					return false;
				}
			} else if(status == 5) {	
				var grosswt = $('#'+fieldid+'').val();
				if(parseFloat(grosswt) > parseFloat(paymentstockgrossweight)) {
					$('#'+fieldid+'').val(0).trigger('change');  
					alertpopup('Stock Available only '+paymentstockgrossweight+' gms. Kindly Enter equal or less than '+paymentstockgrossweight+' gms ');
					return false;
				}
			}
		}
	}
	return true;
}
function purchaseweightvalidation(status) {
	var purerrorwt = parseFloat($("#purchasecheckerrorwt").val()).toFixed(roundweight);
	var purgwt = parseFloat($("#purchasecheckgrwt").val()).toFixed(roundweight);
	var purchasereturnwt = parseFloat(getgridcolvaluewithcontition('saledetailsgrid','','grossweight','sum','stocktypeid','73'));
	if(status == 0){
		var gridgrosswt = parseFloat(getgridcolvalue('saledetailsgrid','','grossweight','sum') - purchasereturnwt).toFixed(roundweight);
		var salesdetiltotwt = parseFloat(parseFloat($("#grossweight").val()) + parseFloat(gridgrosswt)).toFixed(roundweight);
	}else if(status == 1){
		 var selectedrow = $('#saledetailsgrid div.gridcontent div.active').attr('id');
		var innereditrowgwt = parseFloat(getgridcolvalue('saledetailsgrid',selectedrow,'grossweight','')).toFixed(roundweight);
		var gridgrosswt = parseFloat(getgridcolvalue('saledetailsgrid','','grossweight','sum') - purchasereturnwt - parseFloat(innereditrowgwt)).toFixed(roundweight);
		var salesdetiltotwt = parseFloat(parseFloat($("#grossweight").val()) + parseFloat(gridgrosswt)).toFixed(roundweight);
	}else if(status == 3){
		var gridgrosswt = parseFloat(getgridcolvalue('saledetailsgrid','','grossweight','sum') - purchasereturnwt).toFixed(roundweight);
		var salesdetiltotwt = parseFloat(parseFloat(gridgrosswt)).toFixed(roundweight);
	}
	else if(status == 4){
		var gridgrosswt = parseFloat(getgridcolvalue('saledetailsgrid','','grossweight','sum') - purchasereturnwt).toFixed(roundweight);
		var salesdetiltotwt = parseFloat(gridgrosswt).toFixed(roundweight);
	}
	var purchasetotwt = parseFloat(parseFloat(purgwt)).toFixed(roundweight);
	var purchasetotpluserrorwt = parseFloat(parseFloat(purgwt) + parseFloat(purerrorwt)).toFixed(roundweight);
	var diffwt = parseFloat(parseFloat(purchasetotwt) + parseFloat(purerrorwt) - parseFloat(gridgrosswt)).toFixed(roundweight);
	var fulldiffwt = parseFloat(parseFloat(purchasetotwt) - parseFloat(salesdetiltotwt)).toFixed(roundweight);
	if(Math.abs(fulldiffwt) <= purerrorwt) {
		alertpopup('You completed entering '+salesdetiltotwt+' gms. No pending wt.');
		return true;
	}else if(parseFloat(purchasetotpluserrorwt) < parseFloat(salesdetiltotwt) && status == 4) {
		alertpopup('Kindly Enter equal or less than '+salesdetiltotwt+' gms ');
		return false;
	}else if(parseFloat(salesdetiltotwt) < parseFloat(purchasetotpluserrorwt) && status == 3) {
		alertpopup('Kindly Enter equal or less than '+purchasetotpluserrorwt+' gms ');
		return false;
	}else if(parseFloat(salesdetiltotwt) > parseFloat(purchasetotpluserrorwt) && status != 4) {
		alertpopup('Kindly Enter equal or less than '+diffwt+' gms ');
		return false;
	}
}
function loadproductbasedcounter(prodid,counterid,counterdiv,defaultcounterid) {
	var val = $("#"+prodid+"").val();
	if(checkValue(val) == true){
		var counteriddata = $('#'+prodid+'').find('option:selected').data('counterid');	
		$("#"+counterid+" option").addClass("ddhidedisplay");
		$("#"+counterid+" optgroup").addClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',true);
		if(checkValue(counterid) == true) { 
			var counterids = counteriddata.toString().split(','); 
			for(var mk = 0; mk < counterids.length; mk++) {
				$("#"+counterid+" option[value='"+counterids[mk]+"']").closest('optgroup').removeClass("ddhidedisplay");
				$("#"+counterid+" option[value='"+counterids[mk]+"']").removeClass("ddhidedisplay");	
				$("#"+counterid+" option[value='"+counterids[mk]+"']").prop('disabled',false);
				if(defaultcounterid == 1) {
					$("#"+counterid+" option[data-defparentcounterids='2']").removeClass("ddhidedisplay");
					$("#"+counterid+" option[data-defparentcounterids='2']").prop('disabled',false);
					$("#"+counterid+" option[data-defparentcounterids='4']").removeClass("ddhidedisplay");	
					$("#"+counterid+" option[data-defparentcounterids='4']").prop('disabled',false);
					$("#"+counterid+" option[data-defparentcounterids='12']").removeClass("ddhidedisplay");	
					$("#"+counterid+" option[data-defparentcounterids='12']").prop('disabled',false);
					$("#"+counterid+" option[data-defparentcounterids='21']").removeClass("ddhidedisplay");	
					$("#"+counterid+" option[data-defparentcounterids='21']").prop('disabled',false);
				}
			}
			if(counterids.length >= 1) {
				$("#"+counterid+"").select2('val',counterids[0]).trigger('change');
				$("#"+counterdiv+"").show();
			} else {
				$("#"+counterid+"").select2('val','');
				$("#"+counterdiv+"").show();
			}
		}
	} else {
		$("#"+counterid+" option").removeClass("ddhidedisplay");
		$("#"+counterid+" optgroup").removeClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',false);	
	}
	$("#"+counterid+" option:enabled").closest('optgroup').removeClass("ddhidedisplay");	
}
function checkdeliverypostatus(datarowid) {
	if(datarowid){
		$.ajax({
				url:base_url+"Sales/checkdeliverypostatus",
				data:{salesid:datarowid},
				type: "POST",
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$('#deliveryplaceordsave').hide();
					$('#deliverypodate,#deliverypomodeid#delemployeeid,#delvyorderstatusacc').prop("disabled", true);
					$('#delvendorname,#deliverypocomment,#delcouriername,#delotp').prop("readonly", true);
					$('#delvendorname,#deliverypocomment,#delcouriername,#delotp').val('');
					$('#deliverypomodeid,#delemployeeid').select2('val','');
					if(data['deliverypomodeid'] == '1') {
						getcurrentsytemdate('deliverypodate');
						$('#deliverypodate,#deliverypomodeid,#delemployeeid').prop("disabled", false);
						$('#delvendorname,#deliverypocomment,#delcouriername,#delotp').prop("readonly", false);
						$('#deliveryplaceordsave').show();
						$('.delvyorderstatusaccdiv').hide();
						$('#delvyorderstatusacc').attr('class','validate[]');
					} else {
						$('#deliverypomodeid').select2('val',data['deliverypomodeid']).trigger('change');
						$('#deliverypodate').val(data['deliverypodate']);
						$('#deliverypocomment').val(data['deliverypocomment']);
						alertpopup('Already the items has been delivered.');
						if(data['deliverypomodeid'] == '2') {
							$('#delvendorname').val(data['delvendorname']);
						} else if(data['deliverypomodeid'] == '3') {
							$('#delcouriername').val(data['delcouriername']);
						} else if(data['deliverypomodeid'] == '4') {
							$('#delemployeeid').select2('val',data['delemployeeid']);
							$('#delotp').val(data['delotp']);
						}
						if(loggedinuserrole != '3') {
							$('.delvyorderstatusaccdiv').show();
							if(data['delvstatus'] != '1') {
								if(data['delvstatus'] == '24') {
									data['delvstatus'] = 0;
								} else {
									data['delvstatus'] = 1;
								}
								$('#delvyorderstatusacc').select2('val',data['delvstatus']);
							} else {
								$('#delvyorderstatusacc').select2('val','');
								$('#delvyorderstatusacc').prop("disabled", false);
								$('#deliveryplaceordsave').show();
								$('#delvyorderstatusacc').attr('class','validate[required]');
							}
						} else {
							$('.delvyorderstatusaccdiv').hide();
							$('#delvyorderstatusacc').attr('class','validate[]');
						}
					}
				}
			});
	}else {
		alertpopup(selectrowalert);
	}
}
function checkeditsales(datarowid) {
	var vendorstatus = 1;
	if(datarowid){
		$.ajax({
			url:base_url+"Sales/checkeditsales",
			data: "salesid=" +datarowid+"&vendorstatus=" +vendorstatus,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['rows'] > 0) {
					$("#processoverlay").hide();
					if(data['id'] == 13) {
						alertpopup("Can't edit weight sales entry");
					} else if(data['id'] == 18) {
						alertpopup("Can't edit approval out entry");
					} else if(data['id'] == 11) {
						alertpopup("Can't edit this invoice.Bill Used in payment receipt/issue");
					} else if(data['id'] == 9) {
						alertpopup("Can't edit this purchase bill bcoz this data based  stock entry created");
					} else if(data['id'] == 20 && data['cancelstatus'] == 1) {
						alertpopup("Can't edit order.This order items all are cancelled");
					} else if(data['id'] == 20) {
						alertpopup("Can't edit order.This order already used in either place order or delivery order");
					} else if(data['id'] == 'acp21') {
						alertpopup("Place Order has been accepted by vendor. So can't edit Place Order.");
					} else if(data['id'] == 21) {
						alertpopup("Can't edit order.This order already used in receive order");
					} else if(data['id'] == 28 || data['id'] == 29 || data['id'] == 30) {
						alertpopup("Can't edit Repair details. This order already used in either Place Repair or delivery repair");
					}
				} else {
					$('#processoverlay').show();
					addsalesstatus = 0;
					salesdetailcreatestatus = 0; 
					retrievetagstatus = 0;
					approvalnoeditstatus = 0;
					salesdetailentryeditstatus = 0;
					addslideup('salesgriddisplay','salesaddformdiv');
					// Metal checking datas
					metalarraydetails = '';
					billlevelrestrictmetalid = 1;
					metalstatus = 0;
					metaloldarraydetails = '';
					billlevelrestrictoldmetalid = 1;
					metaloldstatus = 0;
					metalreturnarraydetails = '';
					billlevelrestrictreturnmetalid = 1;
					metalreturnstatus = 0;
					// Metal Checking data closed
					salesfieldcommentrequired = 0;
					$("#notificationbadg").text();
					$('.billnetamountspan,.oradvamtspan,.billbalamountspan,.billnetwtspan,.sumbillnetwtspan').text('');
					$('#estimationnumber').attr('readonly',false);
					$('#itemtagnumber,#rfidtagnumber').prop('readonly','');
					$("#billdate,#sperson,#account,#transtype,.iropenamountspan,.iropenwtspan").text('');
					$("#salesaddformheader :input").prop("disabled", false);
					$("#paymentform :input").prop('disabled', false);
					$("#salescreateform :input").attr("readonly", false);
					$("#lottypeid,#purchasemode").prop('readonly','');
					$('#cashcounter').prop('disabled',true);
					$("#accountid").prop('disabled',false);
					$('#issuereceipticon').show();
					$('#creditno').css('width','80%');
					$('#orderadvamtsummary-div').hide();
					$('#addaccount,#accountsearchevent,#editaccount,#salesdetailentrydelete').show();
					$('#creditentryedit,#creditentrydelete,#issuereceiptpush,#creditentryadd').show();
					$("#ordermodeid,#ordernumber,#stocktypeid,#sumarytaxamount,#sumarydiscountamount,#paymentstocktypeid,#paymentproduct,#paymentpurity,#paymentcounter,#ordernumber,#transactionmodeid,#loadbilltype,#ratecuttypeid,#category").prop('disabled',false);
					$('#sumarydiscountamount,#roundvalue').attr('readonly',false);
					if(paymentsavebutton == 1) {
						$('#paymentdetailsubmit').hide();
					} else {
						$('#paymentdetailsubmit').show();
					}
					$('#salesdetailupdate,#paymentdetailupdate,#salesdetailsubmit,#paymentdetailedview').hide();
					$("#estimatearray,#summarytaxgriddata,#issuereceiptid,#wtissuereceiptid,#approvaloutreturnproductid,#approvaloutreturntagno,#approvalouttagnumberarray,#approvaloutrfidnumberarray,#paymentcomment,#salescomment,#orderadvcreditno,#creditoverlayhidden,#creditadjusthidden,#creditoverlayvaluehidden,#creditadjustvaluehidden,#creditoverlaystatus,#tagimage,#multipleemployeeid,#multipleemployeename").val('');
					estimatearray = []; //Empty array variable (for estimate scanning)
					clearform('salescreateform');
					discountoperation = 0; // for discount in billwise
					 //set defaults 
					$("#accountid").select2('focus');
					// Normally to set with transaction type
					{
						mainviewgrid_placeorderid = datarowid;
						$("#salestransactiontypeid").select2('val',27).trigger('change');
						$('#addaccount,#accountsearchevent').hide();
						getandsetaccountidpo(datarowid);
					}
					if(approvalnoeditstatus == 0) {
						$("#salestransactiontypeid").prop('disabled',false);
					}
					$("#issuereceipttypeid").select2('val',3);
					$("#salespersonid").select2('val',$('#hiddenemployeeid').val()).trigger('change');
					$('#employeename').val($('#hiddenemployeename').val());
					$('#employeeid').val($('#hiddenemployeeid').val());
					getcurrentsytemdate("salesdate");				
					$("#sumarydiscountamount,#creditfinalamt").val(0);
					//$("#itemdiscountcalname").text('');
					$("#itemdiscountcalname").text('(0)');
					$(".addbtnclass").removeClass('hidedisplay');
					$("#formclearicon,#stonedetailsshow,#salesdetailedit,#salesdetaildelete,#paymentdelete,#paymentedit").show();
					//$("#stonedetailsshow,#salesdetailedit,#salesdetaildelete,#paymentdelete,#paymentedit").show();
					$("#employeename").val($('#hiddenemployeename').val());
					$("#employeeid").val($('#hiddenemployeeid').val());
					$("#generatesalesid,#generatesalesnumber,#mainordernumber,#orderadvanceno,#mainordernumberid,#balancedescription,#billbasedproductid,#taxgriddata,#editinnerrowid,#issuereceiptmergetext,#untagvalidatemergetext,#untagvalidatecountermergetext").val('');
					$('#tagimagedisplay').empty();
					$("#creditno").attr('readonly',false);
					$("#createnewaccount").trigger("click");
					var date = $("#salesdate").val();
					//opacity reset
					$(".itemdetailsopacity").css('opacity',1);
					$("#salesdetailcancel").hide(); // Cancel Button SHOW / HIDE
					cleargriddata('stoneentrygrid');
					cleargriddata('saledetailsgrid');
					$('#taxcategory').select2('val','').trigger('change');
					$("#spanaccopenamt,#spanaccopenwt,#spanbillclosewt,#spanbillcloseamt,#spanbillwt,#spanbillamt,#sumoldjewels,#sumsalesreturn,#sumbillamount,#spanaccclosewt,#spanacccloseamt,#spanpaidpurewt,#spanissuepurewt,#spanpaidgrswt,#spanissuegrswt,#accountadvamt,#accountcreditamt").text(0);
					$("#accountopeningamount,#accountopeningweight,#wastagelesspercent,#sumarygrossamount,#sumarypendingamount,#sumarynetamount,#sumarybillamount,#sumarysalesretamount,#sumaryoldjewelamount,#sumarytaxamount,#sumarydiscountamount,#finalsumarybillamount,#sumarypaidamount,#billgrossamount,#billnetamount,#oldjewelamtsummary,#prewastage,#premakingcharge,#preflatcharge,#prelstcharge,#purchasereturnsummary,#salesreturnsummary,#orderadvamtsummary,#orderadvamtsummaryhidden,#paymenttotalamounthidden,#billweighthidden,#spanpaidpurewthidden,#prepaymentpureweight,#paymentpureweight,#balpurewthidden,#prepaymenttotalwt,#editpaymentpureweight,#oldjewelwtsummary,#purchasereturnwtsummary,#salesreturnwtsummary,#billnetwt,#billgrosswt,#salesdetailcount,#liveadvanceclose,#orderadvadjustamthidden,#prebillnetamount,#sumaryissueamount,#accountopeningamountirid,#accountopeningweightirid,#finalamthidden,#finalwthidden,#purchasecheckgrwt,#spanpaidgrswthidden,#spanissuegrswthidden,#billforgrosswt,#billfornetwt,#prerepaircharge,#itemwastagevalue,#itemmakingvalue,#itemflatvalue,#itemlstvalue,#itemrepairvalue,#itemchargesamtvalue").val(0);
					$("#discountpercent,#singlediscounttotal,#giftnumber,#giftremark,#giftdenomination,#summarysalesreturnroundvalue").val('0');
					$("#sumgwt,#sumwastage,#sumgross,#sumswt,#summaking,#sumtax,#sumnwt,#sumcharge,#sumdiscount,#smpieces,#sumstone,#sumnetamount,#sumpendingamt,#sumpaidamt,#sumpurewt,#sumpurewttoamt,#sumamttopurewt,#paycash,#paycheque,#paycard,#paydd,#payadvamt,#paychit,#payvoucher,#payamtpurewt,#paypurewtamt,#paypurewt,#oldgrosswt,#oldstonewt,#olddustwt,#oldnetwt,#oldpieces,#oldwastageless,#oldgrossamt,#oldtaxamt,#oldnetamt,#oldpurewt,#srgrosswt,#srstonewt,#srnetwt,#srpieces,#srcharges,#srstoneamt,#srgrossamt,#srtaxamt,#srdiscount,#srnetamt,#srpurewt,#sumbilltax,#sumbilldiscount,#sumflatcharge,#sumissueamt,#billforpieces").text(0);
					retrievestatus = 0;
					purityshowhide('purity','');
					$("#s2id_accountid a span:first").text('');
					$("#salestransactiontypeid,#accountid,#salespersonid,#paymentirtypeid,#journalbillno").prop('disabled',false);
					$("#modeid").attr('readonly',false);
					$("#cashcounter").select2('val',$('#logincashcounterid').val()).trigger('change');
					$('#prodcategorylistbutton').prop('disabled',false);
					compybasednetmccalc();
					getcurrentrate();
					//For Keyboard Shortcut Variables
					viewgridview = 0;
					addformview = 1;
					Operation = 0; //for pagination
					billeditstatus = 1;
					billeditstatusclose = 0;
					mainprintemplateid = 0;
					salesandoldstatus = 0;
					salesstatus = 0;
					salesoldstatus = 0;
					oldstatus = 0;
					onlyoldstatus = 0;
					returnstatus = 0;
					setzero(['roundvalue']);
					estimatestatus = 0;
					receivestatus = 0;
					placeorderstatus = 0;
					takeorderoldstatus = 0;
					takeorderreceivestatus = 0;
					takeorderstatus = 0;
					onlytakeorderstatus = 0;
					$("#ordernumber,#ordermodeid").removeAttr("disabled");
					onlytakeorderadvancestatus = 0;
					editpaymentstatus = 0;
					$('#prepaymenttotalamount').val(0);
					wastagevisible = 0;
					touchvisible = 0;
					calculationdone = 0;
					oldjewelstatus = 1;
					salesreturnstatus = 1;
					$('.billamountdiv').show();
					Materialize.updateTextFields();
					$('#processoverlay').hide();
				}
			}
		});
	}else {
		alertpopup(selectrowalert);
	}
}
function getandsetaccountidpo(datarowid) {
	if(datarowid){
		$.ajax({
			url:base_url+"Sales/getandsetaccountidpo",
			data: "salesid=" +datarowid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['accountid'] != '1') {
					$('#s2id_accountid a span:first').text(data['accountname']);
					$('#accountid').select2('val',data['accountid']).trigger('change');
				} else {
					alertpopup('Please select proper account.');
				}
			}
		});
	}
}
function loadplacenumbersbasedvendor(datarowid) {
	if(datarowid){
		$.ajax({
			url:base_url+"Sales/getandsetaccountidpo",
			data: "salesid=" +datarowid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['accountid'] != '1') {
					$('#poaccountid').val(data['accountid']);
					$('#poaccountname').val(data['accountname']);
					$('#orderitemaccountid').append($("<option value='"+data['accountid']+"'>"+data['accountname']+"</option>"));
					$('#orderitemaccountid').select2('val',data['accountid']).trigger('change');
					$('#orderitemaccountid').prop('disabled',true);
					loadplaceordernumber(data['accountid']);
				}
			}
		});
	}
}
function loadpuritybasedoncategory(categoryid,ddname,status) { // load purity based on category selection
	if(categoryid == 1){
		$("#"+ddname+" optgroup").addClass("ddhidedisplay");
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		$("#"+ddname+"").select2('val','');
		$("#"+ddname+"").prop('disabled',false);
	}else if(categoryid == ''){
		$("#"+ddname+" optgroup").removeClass("ddhidedisplay");
		$("#"+ddname+" option").removeClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',false);
		$("#"+ddname+"").select2('val','');
		$("#"+ddname+"").prop('disabled',false);
	}else{
		$("#"+ddname+" optgroup").addClass("ddhidedisplay");
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		$("#"+ddname+"").select2('val','');
		if(categoryid.indexOf(',') > -1) {
			$("#"+ddname+"").prop('disabled',false);
			var purity = categoryid.split(',');
			for(j=0;j<(purity.length);j++) {
				$("#"+ddname+" option[value="+purity[j]+"]").removeClass("ddhidedisplay");
				$("#"+ddname+" option[value="+purity[j]+"]").prop('disabled',false);
				$("#"+ddname+" option[value="+purity[j]+"]").parent().removeClass("ddhidedisplay");
			}
		}else{
			$("#"+ddname+" option[value="+categoryid+"]").removeClass("ddhidedisplay");
			$("#"+ddname+" option[value="+categoryid+"]").prop('disabled',false);
			//$("#"+ddname+"").select2('val',categoryid);
			$("#"+ddname+"").select2('val',categoryid).trigger('change');
			/* if(status == 1){
				$("#"+ddname+"").prop('disabled',true);
			} */
			$("#"+ddname+" option[value="+categoryid+"]").parent().removeClass("ddhidedisplay");
		}
	}
}
function getpuritybasedoncategory(categoryid) {
	$.ajax({
		url: base_url+"Sales/getpuritybasedoncategory",
		data:{primaryid:categoryid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			loadpuritybasedoncategory(msg,'purity',0);
		},
	});
}
function ltrim(str, chr) {
	var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
	return str.replace(rgxtrim, '');
}
//vendor item status grid - grid will display in main view
function vendoritemstatusgrid(page,rowcount) {
	var salesid = $('#salesgrid div.gridcontent div.active').attr('id');
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#vendoritemstatusgrid").width();
	var wheight = $("#vendoritemstatusgrid").height();
	/*col sort*/
	var sortcol = $("#vendoritemstatussortcolumn").val();
	var sortord = $("#vendoritemstatussortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.vendoritemstatusheadercolsort').hasClass('datasort') ? $('.vendoritemstatusheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.vendoritemstatusheadercolsort').hasClass('datasort') ? $('.vendoritemstatusheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.vendoritemstatusheadercolsort').hasClass('datasort') ? $('.vendoritemstatusheadercolsort.datasort').attr('id') : '0';	
	var footername = 'sales';
	var filterid = salesid;
	var conditionname = '';
	var filtervalue = '';
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/vendoritemstatusgridheaderinformationfetch?maintabinfo=sales&primaryid="+salesid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#vendoritemstatusgrid").empty();
			$("#vendoritemstatusgrid").append(data.content);
			$("#vendoritemstatusgridfooter").empty();
			$("#vendoritemstatusgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('vendoritemstatusgrid');
			//header check box
			$(".ordergrid_headchkboxclass").click(function() {
				$(".ordergrid_rowchkboxclass").prop('checked', false);
				if($(".ordergrid_headchkboxclass").prop('checked') == true) {
					$(".ordergrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".ordergrid_rowchkboxclass").prop('checked', false);
				}
				ordergridgetcheckboxrowid('vendoritemstatusgrid');
			});
		},
	});
}
function retrievevendorproductdetailinformation(datarowid) {
	$("#povendoritemlistid").empty();
	$("#povendoritemlistid").select2('val','')
	if(datarowid){
		$.ajax({
			url:base_url+"Sales/retrievevendorproductdetailinformation",
			data: "salesid=" +datarowid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['fail'] == 'FAILED') {
					alertpopupdouble('Place Order Item either in Active Status / Generated PO!. So try with other Place Order Items.');
					$('#vendoritemstatusgridclose').trigger('click');
					return false;
				} else {
					$.each(data, function(index){
						$("#povendoritemlistid")
							.append($("<option></option>")
							.attr("data-salesdetailid",data[index]['salesdetailid'])
							.attr("data-ordernumber",data[index]['ordernumber'])
							.attr("data-productname",data[index]['productname'])
							.attr("data-purityname",data[index]['purityname'])
							.attr("data-grossweight",data[index]['grossweight'])
							.attr("value",data[index]['salesdetailid'])
							.text(data[index]['ordernumber']+' - '+data[index]['productname']+' - '+data[index]['purityname']+' - '+data[index]['grossweight']));
						$("#povendoritemlistid").select2('val','').trigger('change');
					});
				}
			}
		});
	}
}
function vendoritemstatusgridsubmit() {
	//Get grid details and update in saledetail table
	var criteriacnt = $('#vendoritemstatusgrid div.gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('vendoritemstatusgrid');
	var criteriagriddata = JSON.stringify(griddata);
	$('#processoverlay').show();
	$.ajax({
		url:base_url+"Sales/vendoritemstatusgridsubmitdetails",
		data: "innergriddata=" +criteriagriddata+"&criteriacnt="+criteriacnt,
		type: "POST",
		async:false,
		cache:false,
		success: function(data) {
			if(data == 'SUCCESS') {
				$('#processoverlay').hide();
				$('#vendoritemstatusgridoverlay').fadeOut();
				alertpopup('Updated Successfully');
			} else {
				$('#processoverlay').hide();
				alertpopup('There is something problem to update status information.');
			}
		}
	});
}
//get the tag type all row ids in grid
function getsalesstocktypegridallrowids(gridname,criteria) {
	if(criteria == 'all') {
		var salesstocktype = ['11','12','13','83','89'];
	} else if(criteria == 'old') {
		var salesstocktype = ['19'];
	}
	var rowids =  new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		var gridstocktype = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
		if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
			rowids.push(datarowid);
		}
	});
	return rowids;
}
function accountshortnamecheck() {
	var primaryid = $("#accountid").val();
	var accname = $("#newaccountshortname").val();
	var fieldname = 'accountshortname';
	var elementpartable = 'account';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		}
	}
}
// Retrieve Rejected Product details
function retrieverejectedproductdetails(datarowid,status) {
	$("#salesdetailproductid").empty();
	$("#salesdetailproductid").select2('val','');
	if(datarowid) {
		$.ajax({
			url:base_url+"Sales/retrieverejectedvendorproductdetailinformation",
			data: "salesid=" +datarowid+"&status="+status,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['fail'] == 'FAILED') {
					alertpopup('There is something problem. Please try again later.');
				}else {
					$.each(data, function(index) {
						$("#salesdetailproductid")
							.append($("<option></option>")
							.attr("data-salesdetailid",data[index]['salesdetailid'])
							.attr("data-productname",data[index]['productname'])
							.attr("data-purityname",data[index]['purityname'])
							.attr("data-grossweight",data[index]['grossweight'])
							.attr("data-vendororderduedate",data[index]['vendororderduedate'])
							.attr("data-ordervendorid",data[index]['ordervendorid'])
							.attr("data-status",data[index]['status'])
							.attr("value",data[index]['salesdetailid'])
							.text(data[index]['productname']+' - '+data[index]['purityname']+' - '+data[index]['grossweight']));
					});
					$("#salesdetailproductid").select2('val','');
				}
			}
		});
	}
}
// Retrieve Rejected Product details
function retrieverejectedproductimagedetails(salesdetailid) {
	$('#tagimage').val('');
	$('#tagimagedisplay').empty();
	if(salesdetailid){
		$.ajax({
			url:base_url+"Sales/retrieverejectedproductimagedetails",
			data: "salesdetailid=" +salesdetailid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['fail'] == 'FAILED') {
					alertpopup('There is something problem. Please try again later.');
				}else {
					$.each(data, function(index) {
						var image = data[index]['salesdetailimage'];
						if(checkValue(image) == true) {
							$('#tagimage').val(image);
							$('#tagimagedisplay').empty();
							var imagesplit = [];
							var removetext = 'radio_button_checked';
							image = image.replace(removetext,'');
							imagesplit = image.split(',');
							var imagecount = image.split(',').length;
							for(var i=0; i< imagecount; i++) {
								$('#tagimagedisplay').append('<div data-imgpath="'+imagesplit[i]+'"><img id="companylogodynamic" style="height:100%;width:100%" src="'+base_url+imagesplit[i]+'"><i class="material-icons documentslogodownloadclsbtn" id="removeimageusingcloseicon" data-imagepath="'+imagesplit[i]+'">close</i></div>');
							}
						} else {
							$('#tagimage').val('');
							$('#tagimagedisplay').empty();
						}
					});
				}
			}
		});
	}
}
// Update Rejected product details
function updaterejectedproductdetails() {
	var transactionpage = $('#transactiontypepage').val();
	var newvendoraccountid = $('#newvendoraccountid').find('option:selected').val();
	var tagimage = $('#tagimage').val();
	var form = $("#reassigningvendorform").serialize();
	var amp = '&';
	var datainformation = amp + form;
    $.ajax({
        url: base_url +"Sales/updaterejectedproductdetails",
        data: "datas=" + datainformation+amp+"tagimage="+tagimage+amp+"transactionpage="+transactionpage+amp+"newvendoraccountid="+newvendoraccountid,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				if(transactionpage == 1) {
					alertpopup('Product assinged to Vendor Successfully!');
				} else if(transactionpage == 2) {
					alertpopup('New Due Date has assinged to Vendor Successfully!');
				}
				$('#salesdetailproductid,#newvendoraccountid').select2('val','').trigger('change');
				$('#transactiontypepage').val('');
				$('#tagimage').val('');
				$('#tagimagedisplay').empty();
				$('#reassigningvendoroverlay').fadeOut();
			} else {
				alertpopup(submiterror);
				$('#reassigningvendoroverlay').fadeOut();
			}           
        },
    });
}
// Retrieve Purity value in form view
function retrievelastupdatepurityvalue(ratedisplaypurityvalue) {
	var purityvaluedetails = '';
    $.ajax({
        url: base_url +"Sales/retrievelastupdatepurityvalue",
        data: "ratedisplaypurityvalue=" + ratedisplaypurityvalue,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(msg) {
			if(msg != '') {
				purityvaluedetails = msg;
			}
        },
    });
	return purityvaluedetails;
}
// Order Tracking Overlay grid.
function ordertrackinggrid(page,rowcount) {
	var salesid = '1';
	var categoryid = $('#ordtrackproductcatgid').find('option:selected').val();
	var accountid = $('#ordtrackaccountid').find('option:selected').val();
	var accountshortnameid = $('#ordtrackaccountshrtid').find('option:selected').val();
	var mobilenumber = $('#ordtrackaccmobilenumber').val();
	var ordernumber = $('#ordtrackaccordernumber').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#ordertrackinggrid").width();
	var wheight = $("#ordertrackinggrid").height();
	/*col sort*/
	var sortcol = $("#orderitemssortcolumn").val();
	var sortord = $("#orderitemssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.ordertrackinggridheadercolsort').hasClass('datasort') ? $('.ordertrackinggridheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.ordertrackinggridheadercolsort').hasClass('datasort') ? $('.ordertrackinggridheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.ordertrackinggridheadercolsort').hasClass('datasort') ? $('.ordertrackinggridheadercolsort.datasort').attr('id') : '0';	
	var footername = 'sales';
	var filterid = salesid;
	var conditionname = '';
	var filtervalue = '';
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Sales/ordertrackinggridheaderinformationfetch?maintabinfo=sales&primaryid="+salesid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+'&categoryid='+categoryid+'&accountid='+accountid+'&accountshortnameid='+accountshortnameid+'&mobilenumber='+mobilenumber+'&ordernumber='+ordernumber+"&ordertrackmainviewclick="+ordertrackmainviewclick,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#ordertrackinggrid").empty();
			$("#ordertrackinggrid").append(data.content);
			$("#vendoritemstatusgridfooter").empty();
			$("#vendoritemstatusgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('ordertrackinggrid');
			//header check box
			$(".ordergrid_headchkboxclass").click(function() {
				$(".ordergrid_rowchkboxclass").prop('checked', false);
				if($(".ordergrid_headchkboxclass").prop('checked') == true) {
					$(".ordergrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".ordergrid_rowchkboxclass").prop('checked', false);
				}
				ordergridgetcheckboxrowid('ordertrackinggrid');
			});
			if(ordertrackmainviewclick == 1) {
				var datalength = $('#ordertrackinggrid .gridcontent div.data-content div.emptydata-rows').length;
				if(datalength > 0) {
					alertpopup('No records found in your condition!');
				}
			}
		},
	});
}
// Retrieve all Bill numbers to load
function retrieveallbillnumberdetails() {
	$("#billnumberid").empty();
	$("#billnumberid").select2('val','');
	var accountid = $("#accountid").val();
	var salesid = $('#generatesalesid').val();
	if(accountid) {
		$.ajax({
			url:base_url+"Sales/retrieveallbillnumberdetails",
			data: "salesid=" +salesid+"&accountid="+accountid+"&salesreturnbillcustomers="+salesreturnbillcustomers,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data['fail'] != 'FAILED') {
					$.each(data, function(index) {
						$("#billnumberid")
							.append($("<option></option>")
							.attr("data-accountid",data[index]['accountid'])
							.attr("data-label",data[index]['salesnumber'])
							.attr("value",data[index]['salesid'])
							.text(data[index]['salesnumber']));
					});
					$("#billnumberid").select2('val','');
				}
			}
		});
	}
}
// Get process value from grid - to check advance and balance due entries in grid.
function getgridprocessvalue(gridname) {
	var creditreference = 3;
	var rowids = 0;
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		var gridcreditref = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .creditreference-class').text();
		if(gridcreditref == creditreference) {
			rowids++;
		}
	});
	return rowids;
}
// Account search - Retrieve state id
function accountsearchsubmitretrievestateid(statename) {
	var stateid = 1;
    $.ajax({
        url: base_url +"Sales/accountsearchsubmitretrievestateid",
        data: "statename="+statename,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(msg) {
			if(msg != '') {
				stateid = msg;
			}
        },
    });
	return stateid;
}
// Convert an string to array
function converttoarray(stringdata) {
	var conversion = '';
	if(stringdata != '') {
		conversion = stringdata.split(',');
	}
	return conversion;
}
// Get unique purity id with metal id
function getpuritybasedmetalidgridcondition(gridname,criteria,returntypedata) {
	if(criteria == 'all') {
		var salesstocktype = ['11','12','13','75','83','89'];
	} else if(criteria == 'old') {
		var salesstocktype = ['19'];
	} else if(criteria == 'return') {
		var salesstocktype = ['20'];
	} else if(criteria == 'checksales') {
		var salesstocktype = ['11','12','13','19','20','83','75'];
	} else if(criteria == 'checkrepair') {
		var salesstocktype = ['89'];
	}
	var countdetails = 0;
	var rowids =  new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		if(returntypedata == 'count') {
			var gridstocktype = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
			if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
				countdetails++;
			}
		} else {
			var gridstocktype = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
			if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
				rowids.push(datarowid);
			}
		}
	});
	if(returntypedata == 'count') {
		return countdetails;
	} else {
		return rowids;
	}
}
// Retrieve unique metalid in grid on main edit.
function retrieveuniquemetalidingrid(gridname,criteria) {
	if(criteria == 'all') {
		var salesstocktype = ['11','12','13','83'];
	} else if(criteria == 'old') {
		var salesstocktype = ['19'];
	} else if(criteria == 'return') {
		var salesstocktype = ['20'];
	}
	var metalidsinarray = new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		var gridstocktype = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
		var purityid = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .purity-class').text();
		if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
			var metalid = metalarraydata[purityarraydata.indexOf(purityid)];
			if(jQuery.inArray(metalid, metalidsinarray) === -1) {
				metalidsinarray.push(metalid);
			}
		}
	});
	return metalidsinarray;
}
// Total Items Count in salesdetail grid. (At the time clicking of save button)
function totalitemscountinsalesdetailgrid(gridname) {
	var salesstocktype = ['11','12','13','16','17','24','25','62','63','65','66','73','74','75','76','80','81','82','83','85','86','87','88','89'];
	var itemscountinarray = new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		var gridstocktype = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .stocktypeid-class').text();
		if(jQuery.inArray(gridstocktype, salesstocktype) != -1) {
			itemscountinarray.push(gridstocktype);
		}
	});
	return itemscountinarray;
}
// Manual Tax Calculation Trigger in Inner Grid
function manualtaxcalculationinnergrid(grossamount,taxmasterid) {
	var taxgriddata = '';
	var totaltaxamount = 0;
	var result = new Array();
	$.ajax({
		url:base_url+'Sales/manualtaxmasterload?taxmasterid='+taxmasterid+"&grossamount="+grossamount,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				taxgriddata = data.resultdata;
				totaltaxamount = data.totaltaxamount;
				result.push(taxgriddata,totaltaxamount);
			}
		},
	});
	return result;
}
// JSON COnvert for Tax Dat in inner Grid
function jsonconvertdataformanualtax(grossamount,taxmasterid) {
	var taxgriddata = '';
	$.ajax({
        url:base_url+'Sales/taxgridatajsonconvertdata?taxmasterid='+taxmasterid+"&grossamount="+grossamount,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				taxgriddata = JSON.stringify(data);
			}
		},
    });
	return taxgriddata;
}
// Total Sum Value of Each Tax Category
function taxmasterdetailsinarray() {
	var taxdetails = [];
	var taxmasterids = new Array();
	var i = 0;
	$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		if(i == 0) {
			taxmasterids.push($('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxmasterid-class').text());
		} else {
			var taxmasterid = $('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxmasterid-class').text();
			if(jQuery.inArray(taxmasterid, taxmasterids) === -1) {
				taxmasterids.push(taxmasterid);
			}
		}
		i++;
	});
	for(var j=0; j < taxmasterids.length; j++) {
		var taxcategid = taxmasterids[j];
		var taxpercent = 0;
		$('#billtaxgrid .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var taxmasterid = $('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxmasterid-class').text();
			if(taxcategid == taxmasterid) {
				var taxeachpercent = $('#billtaxgrid .gridcontent div#'+datarowid+' ul .taxrate-class').text();
				taxpercent = parseFloat(parseFloat(taxpercent) + parseFloat(taxeachpercent)).toFixed(5);
			}
		});
		taxdetails.push([taxcategid,taxpercent]);
	}
	return taxdetails;
}
// Load Payment Product - Touch value from Product Charges Table
function loadproductchargeforpayment() {
	var touchreturnvalue = 0;
	var salestransactiontypeid = $('#salestransactiontypeid').find('option:selected').val();
	if($('#accountid').val() == '') {
		var accountid = '';
		var accounttype_id = '';
	} else {
		var accountid = $('#accountid').val();
		if(salestransactiontypeid == 9 || salestransactiontypeid == 27){
			var accounttype_id = 16;
		} else {
			var accounttype_id = 6;
		}
	}
	var stocktypeid = $('#stocktypeid').find('option:selected').val();
	if(stocktypeid == 19) {
		var productid = $('#product').find('option:selected').val();
		var categoryid = $('#product').find('option:selected').data('categoryid');
		var purityid = $('#product').find('option:selected').data('purityid');
		var weight = $('#grossweight').val();
	} else {
		var productid = $('#paymentproduct').find('option:selected').val();
		var categoryid = $('#paymentproduct').find('option:selected').data('categoryid');
		var purityid = $('#paymentproduct').find('option:selected').data('purityid');
		var weight = $('#paymentgrossweight').val();
	}
	var itemtagnumber = '';
	var rfidtagnumber = '';
	$.ajax({
		url:base_url+"Sales/loadproductaddon",
		data:{productid:productid,categoryid:categoryid,purityid:purityid,weight:weight,accountid:accountid,salestransactiontypeid:salestransactiontypeid,accounttype_id:accounttype_id,itemtagnumber:itemtagnumber,rfidtagnumber:rfidtagnumber,stocktypeid:stocktypeid,purchasedisplay:purchasedisplay},
		dataType:'json',
		async:false,
		success: function(data) {
			if(data.chargedetails != ""  && data.chargedetails != 'null') {
				var chargedetails1 = data.chargedetails.split(",");
				$.each(chargedetails1, function (index, value) {
					var res = value.split(":");
					var s = res[0].substr(0, 2);
					if(s == 'WS' || s == 'TV') {
						touchreturnvalue = res[1];
					}
				});
			}
		}
	});
	return touchreturnvalue;
}
// Sequence No - Make an sequence no from ascending order.
function sequenceascendingorder(gridname) {
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		$('#'+gridname+' .gridcontent div#'+datarowid+' ul li:first').text(datarowid);
	});
}
//RFID Tagnumber unique check
function rfitagnochecksales() {
	var accname = $("#rfidtagnumber").val();
	var elementpartable = 'itemtag';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Sales/rfitagnochecksales",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable,
			type:"post",
			datatype :"text",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "BLANKFIELD") {
			$("#rfidtagnumber").val('');
			$("#rfidtagnumber").focus();
			alertpopup('Please Enter value in Field!');
		} else if(nmsg == "NOTAGNO") {
			$("#rfidtagnumber").val('');
			$("#rfidtagnumber").focus();
			alertpopup(accname+" is not available in Database!");
		} else {
			$("#rfidtagnumber").val(nmsg);
		}
	}
}
// Display Account Information
function displayaccountdetails() {
	var accid = $("#accountid").val();
	if(accid != "") {
		$.ajax({
			url:base_url+"Sales/getselaccountinformation",
			data:"data=&accid="+accid,
			type:"post",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				if(data.mobileno != '') {
					$('#accountinformation').text('['+data.accname+'-'+data.mobileno+']');
				} else {
					$('#accountinformation').text('['+data.accname+']');
				}
			},
		});
	} else {
		$('#accountinformation').text('');
	}
}
}
</script>