$(document).ready(function() {
	{// Foundation Initialization
		$(document).foundation();
		PAYMETHOD='ADD';
		PRECISION=0;
		MODULEID = 0;
		UPDATEID = 0;
		$('#paymentpayupdatebutton').removeClass('updatebtnclass');
		$('#paymentpayupdatebutton').removeClass('hidedisplayfwg');
		$('#paymentpayaddbutton').removeClass('addbtnclass');
		$("#paymentpayupdatebutton").hide();
	}
    {// Main div height width change
		maindivwidth();
		var maindiv = $('.maindiv').width();
		var a = screen.width;
		reportsviewgridwidth = ((a * maindiv)/100);	 
	}
    {// Grid Calling Functions
		paymentgrid();	
		getmoduleid();
		//crud action
		crudactionenable();
	}
    {// field break
		$("<div class='row'></div>").insertAfter("#paymentstatusiddivhid");
	}
	{// Clear the form - regenerate the number 
		$('#formclearicon').click(function() {
			//cleargrids
			cleargriddata('paymentpayaddgrid1');
		});
	}
	{// Close Add Form
		var addcloseinfo = ["closeaddform", "paymentgriddisplay", "paymentaddformdiv"]
		addclose(addcloseinfo);
	}
  	{// payment view by drop down change
		$('#dynamicdddataview').change(function() {
			paymentgrid();
		});		
	}
	{// Validation for payment Add  
		$('#dataaddsbtn').click(function(e) {
			$('#dataaddsbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#moduleid,#transactionid,#paymentdate').removeClass('validate[required]');
			$('#paymenttypeid').removeClass('validate[required]');
			$('#paymentmethodid').removeClass('validate[required]');
			$('#paymentamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');	
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				paymentupdation();
				$('#dataaddsbtn').removeClass('singlesubmitonly');
			},
			onFailure: function() {
				var dropdownid =['4','accountid','paymentsourceid','employeeid','crmstatusid'];
				$('#dataaddsbtn').removeClass('singlesubmitonly');
				$('#moduleid,#transactionid,#paymentdate').addClass('validate[required]');
				$('#paymenttypeid').addClass('validate[required]');
				$('#paymentmethodid').addClass('validate[required]');
				$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	} 
	{// For touch
		fortabtouch = 0;
	}
	{// Action Events
		$("#cloneicon").click(function() {
			var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				paymentsclonedatafetchfun(datarowid);
				showhideiconsfun('editclick','paymentaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('paymentgrid');
			sectionpanelheight('paymentpayoverlay');
		});
		{//inner-form-with-grid
			$("#paymentpayingridadd1").click(function(){
				clearform('gridformclear');
				sectionpanelheight('paymentpayoverlay');
				$("#paymentpayoverlay").removeClass("closed");
				$("#paymentpayoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#paymentpaycancelbutton").click(function(){
				clearform('gridformclear');
				$("#paymentpayoverlay").removeClass("effectbox");
				$("#paymentpayoverlay").addClass("closed");
				$('#paymentpayupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#paymentpayaddbutton').show();//display the ADD button(inner-productgrid)*/	
			});
		}
		$("#detailedviewicon").click(function() {
			var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				paymentseditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','paymentaddformdiv');	
				$(".froala-element").css('pointer-events','none');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					paymentseditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','paymentaddformdiv');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','paymentaddformdiv');
			$(".froala-element").css('pointer-events','auto');
		});
		//writeoffamount
		$("#writeoffamount").focusout(function(){
			if(isNaN($("#writeoffamount").val())){
				$("#writeoffamount").val(0);
			}
			var val = parseFloat($(this).val());
			if(val > 0 || val == 0){
				calculatepaymentsummary();
			} else {
				$("#writeoffamount").val(0);
			}
		});
	}	
	{// Update payments information
		$('#dataupdatesubbtn').click(function(e) {
			$('#dataupdatesubbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#moduleid,#transactionid,#paymentdate').removeClass('validate[required]');
			$('#paymenttypeid').removeClass('validate[required]');
			$('#paymentmethodid').removeClass('validate[required]');
			$('#paymentamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');	
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
			},
			onFailure: function() {
				var dropdownid =['4','accountid','paymentsourceid','employeeid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				$('#moduleid,#transactionid,#paymentdate').addClass('validate[required]');
				$('#paymenttypeid').addClass('validate[required]');
				$('#paymentmethodid').addClass('validate[required]');
				$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				alertpopup(validationalert);
			}	
		});
	}
	//-payment-report session check
	{//redirected from corresponding modules
		var paymentmoduleid = sessionStorage.getItem("paymentmoduleid"); 
		var paymenttransactionid = sessionStorage.getItem("paymenttransactionid"); 
		if(checkValue(paymentmoduleid) == true && checkValue(paymenttransactionid) == true) {
			$('#addicon').trigger('click');
			setTimeout(function(){
				$('#moduleid').select2('val',paymentmoduleid).trigger('change');				
				$('#transactionid').select2('val',paymenttransactionid).trigger('change');				
				sessionStorage.removeItem("paymentmoduleid");
				sessionStorage.removeItem("paymenttransactionid");
			},50);	
		} else {
			sessionStorage.removeItem("paymentmoduleid");
			sessionStorage.removeItem("paymenttransactionid");
		}
	}
	{
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){
			// ?Identify where the current record 
			$('#paymentid').val(0); //new entry of payment id set to zero.(for new entry only)
			var balanceamount = parseFloat($('#balanceamount').val());
			var paymentamount = parseFloat($('#paymentamount').val());
			if(PAYMETHOD != 'UPDATE'){
				if(balanceamount <= 0){
					alertpopup('Full Payment Done');					
				}
				else if(paymentamount <= 0){
					alertpopup('Amount should be greater than Zero');					
				}
				else{
					var i = $(this).data('frmtogriddataid');
					var gridname = $(this).data('frmtogridname');
					gridformtogridvalidatefunction(gridname);
					griddataid = i;
					gridynamicname = gridname;
					$("#"+gridname+"validation").validationEngine('validate');
					PAYMETHOD = 'ADD';
				}
			}else{
				if(paymentamount <= 0){
					alertpopup('Amount should be greater than Zero');					
				}
				else{
					var i = $(this).data('frmtogriddataid');
					var gridname = $(this).data('frmtogridname');
					gridformtogridvalidatefunction(gridname);
					griddataid = i;
					gridynamicname = gridname;
					$("#"+gridname+"validation").validationEngine('validate');
					PAYMETHOD = 'ADD';
				}
			}			
			setTimeout(function()
			{
			Materialize.updateTextFields();
			},100);
		});		
	}	
	//Enables the Edit Buttons
	$('#paymentpayingrideditspan1').removeAttr('style');
	//append the module ids-only few module-future append
	var mids = [MODULEID];//,217,225
	var midnames = ['Invoice'];//,'Salesorder','Purchaseorder'
	for(var gg=0;gg < mids.length;gg++){
		$('#moduleid')
			.append($("<option></option>")
			.attr("data-moduleidhidden",midnames[gg])	
			.attr("value",mids[gg])
			.text(midnames[gg]));
	}
	//change of modules
	$("#transactionid").change(function() {
		var moduleid = $("#moduleid").val();
		var transactionid = $("#transactionid").val();
		var paymentdate = $("#paymentdate").val();
		var modulename = $("#moduleid option:selected").text();
		var transactionnumber = $("#transactionid option:selected").text();
		if(checkVariable('moduleid') == true && checkVariable('transactionid') == true){			
			$('#formclearicon').trigger('click'); //reset clears
			$('#moduleid').select2('val',moduleid);
			$('#transactionid').select2('val',transactionid);
			$('#paymentdate').val(paymentdate);
			cleargriddata('paymentpayaddgrid1');
			loadpaymentdetails(moduleid,transactionid,modulename,transactionnumber);			
			retrievepaymentsummary(moduleid,transactionid,modulename);		
			Materialize.updateTextFields();
		}
	});
	//paymenttypeid
	$("#paymenttypeid").change(function(){
		var val = $(this).val();
		if(checkValue(val) == true){				
			var amount = $('#balanceamount').val();
			if(val == 3){ //full
				$("#paymentamount").val(amount);
				Materialize.updateTextFields();
			}else{
				$("#paymentamount").val('');
			}
		}
	});
	//payment methods
	$("#paymentmethodid").change(function(){
		var val = $(this).val();
		if(checkValue(val) == true){				
			if(val == 2){ //cash
				$('#banknamedivhid,#referencedatedivhid').hide();					
			} else { // neft-rtgs-dd-cheque-ecs-c.card-d.card
				$('#banknamedivhid,#referencedatedivhid').show();
			}
		}
	});
	$("#moduleid").change(function() {
		if(checkVariable('moduleid') == true){			
			$('#transactionid').empty();
			var moduleid = $("#moduleid").val();
			var paymentdate = $("#paymentdate").val();
			$('#formclearicon').trigger('click'); //reset clears
			$('#moduleid').select2('val',moduleid);
			$('#paymentdate').val(paymentdate);
			var modulename = $("#moduleid option:selected").text();
			$('#transactionid').append($("<option></option>"));
			paymenttransaction(moduleid,modulename);
		}
	});
	//COMPANY_NAME = getcompanyname();
	// Invoice Payment Grid Edit
	$("#paymentpayingridedit1").click(function() {
		var selectedrow = $('#paymentpayaddgrid1 div.gridcontent div.active').attr('id');		
		if(selectedrow){
			sectionpanelheight('paymentpayoverlay');
			$("#paymentpayoverlay").removeClass("closed");
			$("#paymentpayoverlay").addClass("effectbox");
			gridtoformdataset('paymentpayaddgrid1',selectedrow);	
			var paymenttypeid = $("#paymenttypeid").val();
			if(paymenttypeid != ''){
				if(paymenttypeid == 2){
					$("#paymenttypeid option[value='3']").wrap('<span/>');
					$("#paymenttypeid option[value='4']").wrap('<span/>');
				}
				if(paymenttypeid == 3){
					$("#paymenttypeid option[value='2']").wrap('<span/>');
					$("#paymenttypeid option[value='4']").wrap('<span/>');
				}
				if(paymenttypeid == 4){
					$("#paymenttypeid option[value='2']").wrap('<span/>');
					$("#paymenttypeid option[value='3']").wrap('<span/>');
				}
			}
			$('#paymentpayupdatebutton').show();	//display the UPDATE button/
			$('#paymentpayaddbutton').hide();	//display the ADD button/
			PAYMETHOD = 'UPDATE';
			UPDATEID = selectedrow;
		} else {
			alertpopup('Please Select The Row To Edit');
		}
		Materialize.updateTextFields();
	});
	{
		$("#paymentpayupdatebutton").click(function() {			
			var paymenttypeid = $("#paymenttypeid").val();
			if(paymenttypeid != ''){
				if(paymenttypeid == 2){
					$("#paymenttypeid option[value='3']").unwrap();
					$("#paymenttypeid option[value='4']").unwrap();
				}
				if(paymenttypeid == 3){
					$("#paymenttypeid option[value='2']").unwrap();
					$("#paymenttypeid option[value='4']").unwrap();
				}
				if(paymenttypeid == 4){
					$("#paymenttypeid option[value='2']").unwrap();
					$("#paymenttypeid option[value='3']").unwrap();
				}
			}
			$('#paymentpayaddbutton').show();
			$('#paymentpayupdatebutton').hide();
		});
	}
	$("#paymentpayingriddel1").click(function(){
		    var datarowid = $('#paymentpayaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid){
				if(PAYMETHOD == 'UPDATE'){
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('paymentpayaddgrid1',datarowid);
					calculatepaymentsummary();
				}
			} else {
				alertpopup("Please select a row");
			}
	});
	{// Dashboard Icon Click Event	
		$('#dashboardicon').click(function(){
			var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,paymentgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
function calculatepaymentsummary() {		
	setzero(['totalpayable','writeoffamount']);
	var writeoffamount =parseFloat($("#writeoffamount").val());
	var totalpayable = parseFloat($("#totalpayable").val());
	var totalpayable = totalpayable.toFixed(PRECISION);
	var paidamount = parseFloat(getgridcolvalue('paymentpayaddgrid1','','paymentamount','sum'));
	$("#paidamount").val(paidamount.toFixed(PRECISION));
	var balanceamount = parseFloat(totalpayable)-parseFloat(paidamount)-parseFloat(writeoffamount);
	$("#balanceamount").val(balanceamount.toFixed(PRECISION));		
}
function retrievepaymentsummary(moduleid,transactionid,modulename) {
	if(moduleid == MODULEID){
		modulename = 'invoice';
	}
	$.ajax({
		url:base_url+'Payment/retrievepaymentsummary?moduleid='+moduleid+'&transactionid='+transactionid+'&modulename='+modulename,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
		  if((data.fail) == 'FAILED') {
		  } else {
			  	if(data.totalpayable == data.paidamount){
			  		alertpopup("Full Payment made for this Entry");
			  		$('#transactionid').select2('val','');
			  		cleargriddata('paymentpayaddgrid1');
			  	}else{
			  		$('#totalpayable').val(data.totalpayable);
					$('#paidamount').val(data.paidamount);
					$('#writeoffamount').val(data.writeoffamount);
					$('#balanceamount').val(data.balanceamount);
			  	}
				
		  }		
		},
	});
}
function loadpaymentdetails(moduleid,transactionid,modulename,transactionnumber) {
	if(checkValue(moduleid) == true && checkValue(transactionid) == true){
		$.ajax({
			url:base_url+'Payment/retrievepaymentdetail?moduleid='+moduleid+'&transactionid='+transactionid+'&modulename='+modulename+'&transactionnumber='+transactionnumber,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('paymentpayaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('paymentpayaddgrid1');
					var hideprodgridcol = ['paymentid'];
					gridfieldhide('paymentpayaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
function paymenttransaction(moduleid,modulename) {
	$.ajax({
		url: base_url+"Payment/gettransactionnumbers?moduleid="+moduleid+"&modulename="+modulename,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-transactionidhidden='"+data[m]['number']+"' value = '" +data[m]['id']+ "'>"+data[m]['number']+"</option>";
				}
				//after run
				$('#transactionid').append(newddappend);
			}	
			$('#transactionid').trigger('change');
		},
	});
}
{// Get and Set Date
	function getandsetdate(dateset) { 
		$('#deliveryduedate').datetimepicker('option', 'minDate', dateset);
	}
}
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// payments View Grid
	function paymentgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#paymentpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#paymentpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#paymentgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.paymentheadercolsort').hasClass('datasort') ? $('.paymentheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.paymentheadercolsort').hasClass('datasort') ? $('.paymentheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.paymentheadercolsort').hasClass('datasort') ? $('.paymentheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=payment&primaryid=paymentid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#paymentgrid').empty();
				$('#paymentgrid').append(data.content);
				$('#paymentgridfooter').empty();
				$('#paymentgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('paymentgrid');
				{//sorting
					$('.paymentheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.paymentheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#paymentpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.paymentheadercolsort').hasClass('datasort') ? $('.paymentheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.paymentheadercolsort').hasClass('datasort') ? $('.paymentheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						paymentgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('paymentheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						paymentgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#paymentpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						paymentgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var paymentsideditvalue = sessionStorage.getItem("paymentidforedit"); 
					if(paymentsideditvalue != null){
						setTimeout(function(){ 
						paymentseditdatafetchfun(paymentsideditvalue);
						sessionStorage.removeItem("paymentidforedit");},100);	
					}
				}
				//Material select
				$('#paymentpgrowcount').material_select();
			},
		});		
	}
}
{
	function getmoduleid(){
		$.ajax({
			url:base_url+'Invoice/getmoduleid',
			async:false,
			cache:false,
			success: function(data) {
				MODULEID=data;
			}
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			paymentinneraddgrid();	
			addslideup('paymentgriddisplay', 'paymentaddformdiv');
			resetFields();
			cleargriddata('paymentpayaddgrid1');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');			
			showhideiconsfun('addclick','paymentaddformdiv');				
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);		
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				paymentinneraddgrid();	
				paymentseditdatafetchfun(datarowid);
				showhideiconsfun('editclick','paymentaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#paymentgrid div.gridcontent div.active').attr('id');
			if(datarowid) {		
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#paymentpgnumcnt li .page-text .active').data('rowcount');
		paymentgrid(page,rowcount);
	}
}
{// New data add submit function
	function paymentupdation() {
		var amp = '&';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				}
			}
		}		
		var sendformadddata = JSON.stringify(addgriddata);		
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;
		var moduleid = $('#moduleid option:selected').val();
		var transactionid = $('#transactionid').val();
		var modulename = $('#moduleid option:selected').text();
		$.ajax({
			url: base_url + "Payment/newdatacreate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"moduleid="+moduleid+amp+"transactionid="+transactionid+amp+"modulename="+modulename,
			type: "POST",
			cache:false,
			success: function(msg) {
				if (msg == true) {
					$(".ftab").trigger('click');				
					resetFields();
					$('#paymentaddformdiv').hide();
					$('#paymentgriddisplay').fadeIn(1000);
					refreshgrid();
					clearformgriddata();					
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');alertpopup(savealert);
					$('#moduleid,#transactionid,#paymentdate').addClass('validate[required]');
					$('#paymenttypeid').addClass('validate[required]');
					$('#paymentmethodid').addClass('validate[required]');
					$('#paymentamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');			
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;					
				}
				else if (msg == "Disallowed Key Characters."){
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
				}
				else {					
					alertpopup("Error during update.Please close and try again");
				}
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
			},
		});
	}
}
{// Clear grid data
	function clearformgriddata()
	{
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++){
			 cleargriddata(gridnames[j]);
			}
	}
}
{// Update old information
	function updateformdata() {
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		//editor data fetch
		var editordata = '';
		var neweditordata = '';
		var editorname = $('#editornameinfo').val();
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(',');
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').editable('getHTML');
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){	
					for (var i = 0; i < datacontent.length; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				neweditordata += '&'+fname[0]+'_editorfilename='+editordata;
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
		}
		if(editordata != "") {
			$.ajax({
				url: base_url + "Payment/datainformationupdate",
				data: "datas=" + datainformation+"&editordatass="+neweditordata,
				type: "POST",
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$('#paymentaddformdiv').hide();
						$('#paymentgriddisplay').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						//For Keyboard Shortcut Variables
						addformupdate = 0;
						viewgridview = 1;
						addformview = 0;
					}  else if (nmsg == "false") {
					
					}
				},
			});
		} else {
			alertpopup('Please Enter the Values in Description Editor');
		}
	}
}
{// Delete Operation
	function recorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Payment/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "false") {
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Edit Function
	function paymentseditdatafetchfun(datarowid) {
		addslideup('paymentgriddisplay', 'paymentaddformdiv');
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();
		cleargriddata('paymentpayaddgrid1');
		if(datarowid!='') {
			$.ajax({
				url:base_url+'Payment/fetchformdataeditdetails?primarydataid='+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('paymentpayaddgrid1',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('paymentpayaddgrid1');
						var hideprodgridcol = ['paymentid'];
						gridfieldhide('paymentpayaddgrid1',hideprodgridcol);
					}
				},
			});
		}
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Payment view Grid
	function paymentinneraddgrid() 
	{
		var wwidth = $("#paymentpayaddgrid1").width();
		var wheight = $("#paymentpayaddgrid1").height();
		$.ajax({
			url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=136&moduleid=28&width="+wwidth+"&height="+wheight+"&modulename=paymentgrid",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#paymentpayaddgrid1").empty();
				$("#paymentpayaddgrid1").append(data.content);
				$("#paymentpayaddgrid1footer").empty();
				$("#paymentpayaddgrid1footer").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('paymentpayaddgrid1');
				var hideprodgridcol = ['paymentid'];
				gridfieldhide('paymentpayaddgrid1',hideprodgridcol);
			},
		});
	}
}
/*
*get the company names
*/
function getcompanyname(){		
	var n='';
	$.ajax({
		url: base_url+"Base/getcompanyname",		
		async:false,
		cache:false,
		success: function(data) {				
			n=data;
		},
	});
	return n;
}
// Form to grid base function //Used on the new add's/edit's
function gridformtogridvalidatefunction(gridnamevalue){
	//Function call for validate		
	$("#"+gridnamevalue+"validation").validationEngine({
		onSuccess: function() {
			var moduleid=$('#moduleid').val();
			var moduleno=$('#transactionid').val();
			var paymentdate=$('#paymentdate').val();
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(PAYMETHOD == 'ADD' || PAYMETHOD == ''){
				formtogriddata(gridname,PAYMETHOD,'last','');
			} else if(PAYMETHOD == 'UPDATE') {
				formtogriddata(gridname,PAYMETHOD,'',UPDATEID);
			}
			/* Hide columns */
			var hideprodgridcol = ['paymentid'];
			gridfieldhide(gridname,hideprodgridcol);
			/* Data row select event */
			datarowselectevt();
			setTimeout(function(){
				calculatepaymentsummary();
				$('#moduleid').select2('val',moduleid);
				$('#transactionid').select2('val',moduleno);	
				$('#paymentdate').val(paymentdate);
			},100);	
			$('#paymentpayupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#paymentpayaddbutton').show();//display the ADD button(inner-productgrid)			
			clearform('gridformclear');
			//close product section
			$('#paymentpaycancelbutton').trigger('click');
			griddataid = 0;
			gridynamicname = '';					
			PAYMETHOD = 'ADD'; //reset
			},
			onFailure: function() {	
				var dropdownid =['1','productid'];
				dropdownfailureerror(dropdownid);
			}
		});
	}