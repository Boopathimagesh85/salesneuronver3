$(document).ready(function() {	
	{//Hidedisplay Buttons
		$('#uomconversiondataupdatesubbtn').hide();
		$('#uomconversionsavebutton').show();
	}
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		uomconversionaddgrid();
		firstfieldfocus();
		crudactionenable();
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			uomconversionaddgrid();
		});
	}
	{//validation for Unit Of Mesure Add  -uomconversionsavebutton
		$('#uomconversionsavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#uomconversionformaddwizard").validationEngine('validate');	
			}		
		});
		jQuery("#uomconversionformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#uomconversionsavebutton').attr('disabled','disabled'); 
				uomconversionaddformdata();
			},
			onFailure: function() {
				var dropdownid =['1','uomtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//validation for Unit Of Mesure Edit  
		$( window ).resize(function() {
			innergridresizeheightset('uomconversionaddgrid');
			sectionpanelheight('uomconversionsectionoverlay');
		});
		//update company information
		$('#uomconversiondataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#uomconversionformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#uomconversionformeditwizard").validationEngine({
			onSuccess: function() {
				uomconversionupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','uomtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{
		$(".addsectionclose").click(function(){
			$("#uomconversionsectionoverlay").removeClass("effectbox");
			$("#uomconversionsectionoverlay").addClass("closed");
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{
		$("#uomtoid").change(function() {
			var fromuom = $("#uomid").val();
			var touom = $("#uomtoid").val();
			if(fromuom == touom){
				alertpopup('From UOM and To UOM should be different');
				 $("#uomtoid").select2('val','')
			}
		});
		
		$("#uomid").change(function() {
			var fromuom = $("#uomid").val();
			var touom = $("#uomtoid").val();
			if(fromuom == touom){
				alertpopup('From UOM and To UOM should be different');
				 $("#uomid").select2('val','')
			}
		});
	}	
	{//filter work
		//for toggle
		$('#uomconversionviewtoggle').click(function() {
			if ($(".uomconversionfilterslide").is(":visible")) {
				$('div.uomconversionfilterslide').addClass("filterview-moveleft");
			} else {
				$('div.uomconversionfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#uomconversionclosefiltertoggle').click(function(){
			$('div.uomconversionfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#uomconversionfilterddcondvaluedivhid").hide();
		$("#uomconversionfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#uomconversionfiltercondvalue").focusout(function(){
				var value = $("#uomconversionfiltercondvalue").val();
				$("#uomconversionfinalfiltercondvalue").val(value);
				$("#uomconversionfilterfinalviewconid").val(value);
			});
			$("#uomconversionfilterddcondvalue").change(function() {
				var value = $("#uomconversionfilterddcondvalue").val();
				if( $('#s2id_uomconversionfilterddcondvalue').hasClass('select2-container-multi') ) {
					uomconversionmainfiltervalue=[];
					$('#unitofmeasurefilterddcondvalue option:selected').each(function(){
					    var $uomconvvalue =$(this).attr('data-ddid');
					    uomconversionmainfiltervalue.push($uomconvvalue);
						$("#uomconversionfinalfiltercondvalue").val(uomconversionmainfiltervalue);
					});
					$("#uomconversionfilterfinalviewconid").val(value);
				} else {
					$("#uomconversionfinalfiltercondvalue").val(value);
					$("#uomconversionfilterfinalviewconid").val(value);
				}
			});
		}
		uomconversionfiltername = [];
		$("#uomconversionfilteraddcondsubbtn").click(function() {
			$("#uomconversionfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#uomconversionfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(uomconversionaddgrid,'uomconversion');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Documents Add Grid
	function uomconversionaddgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = '1232';
		var wheight = $("#uomconversionaddgrid").height();
		//col sort
		var sortcol = $("#uomconversionsortcolumn").val();
		var sortord = $("#uomconversionsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.uomconversionheadercolsort').hasClass('datasort') ? $('.uomconversionheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.uomconversionheadercolsort').hasClass('datasort') ? $('.uomconversionheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.uomconversionheadercolsort').hasClass('datasort') ? $('.uomconversionheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#uomconversionviewfieldids').val();
		if(userviewid != '') {
			userviewid= '55';
		}
		var filterid = $("#uomconversionfilterid").val();
		var conditionname = $("#uomconversionconditionname").val();
		var filtervalue = $("#uomconversionfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=uomconversion&primaryid=uomconversionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#uomconversionaddgrid').empty();
				$('#uomconversionaddgrid').append(data.content);
				$('#uomconversionaddgridfooter').empty();
				$('#uomconversionaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('uomconversionaddgrid');
				{//sorting
					$('.uomconversionheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.uomconversionheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#uomconversionpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.uomconversionheadercolsort').hasClass('datasort') ? $('.uomconversionheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.uomconversionheadercolsort').hasClass('datasort') ? $('.uomconversionheadercolsort.datasort').data('sortorder') : '';
						$("#uomconversionsortorder").val(sortord);
						$("#uomconversionaddgrid").val(sortcol);
						var sortpos = $('#uomconversionaddgrid .gridcontent').scrollLeft();
						uomconversionaddgrid(page,rowcount);
						$('#uomconversionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('uomconversionheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						uomconversionaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});//uomconversionpgrowcount
					$('#uomconversionpgrowcount').change(function(){ //unitofmeasureaddgridfooter
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						uomconversionaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$(".gridcontent").click(function(){
					var datarowid = $('#uomconversionaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#uomconversionpgrowcount').material_select();
			},
		});
	}
}
function crudactionenable() {
	{//add icon click
		$('#uomconversionaddicon').click(function(e){
			sectionpanelheight('uomconversionsectionoverlay');
			$("#uomconversionsectionoverlay").removeClass("closed");
			$("#uomconversionsectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#uomconversiondataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#uomconversionsavebutton').show();
		});
	}
	//edit icon	
	$("#uomconversionediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#uomconversionaddgrid div.gridcontent div.active').attr('id');
		if (datarowid){
			resetFields();
			$('#uomconversiondataupdatesubbtn').show();
			$('#uomconversionsavebutton').hide();
			uomconversiongetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else{
			alertpopup("Please select a row");
		}
	});
	//delete icon
	$("#uomconversiondeleteicon").click(function(){
		var datarowid = $('#uomconversionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#uomconversionprimarydataid").val(datarowid);
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('uomconversiondeleteyes');
			$("#uomconversiondeleteyes").click(function(){
				var datarowid = $("#uomconversionprimarydataid").val();
				uomconversionrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#uomconversionpgnum li.active').data('pagenum');
		var rowcount = $('ul#uomconversionpgnumcnt li .page-text .active').data('rowcount');
		uomconversionaddgrid(page,rowcount);
	}
}
{// New data add submit function
	function uomconversionaddformdata() {
		var formdata = $("#uomconversionaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax( {
			url: base_url + "Uomconversion/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					alertpopup(savealert);
					resetFields();
					$(".addsectionclose").trigger("click");
					$("#uomconversionsavebutton").attr('disabled',false); 
					alertpopup(savealert);				
				} 
			},
		});
	}
}
{// Old information show in form
	function uomconversiongetformdata(datarowid) {
		var uomconversionelementsname = $('#uomconversionelementsname').val();
		var uomconversionelementstable = $('#uomconversionelementstable').val();
		var uomconversionelementscolmn = $('#uomconversionelementscolmn').val();
		var uomconversionelementpartable = $('#uomconversionelementspartabname').val();
		$.ajax( {
			url:base_url+"Uomconversion/fetchformdataeditdetails?uomconversionprimarydataid="+datarowid+"&uomconversionelementsname="+uomconversionelementsname+"&uomconversionelementstable="+uomconversionelementstable+"&uomconversionelementscolmn="+uomconversionelementscolmn+"&uomconversionelementpartable="+uomconversionelementpartable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					clearinlinesrchandrgrid('uomconversionaddgrid');
					//For Touch
					smsmasterfortouch = 0;
				} else {
					sectionpanelheight('uomconversionsectionoverlay');
					$("#uomconversionsectionoverlay").removeClass("closed");
					$("#uomconversionsectionoverlay").addClass("effectbox");
					var txtboxname = uomconversionelementsname + ',uomconversionprimarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(','); 
					var dataname = uomconversionelementsname + ',primarydataid';
					var datasname = {};
					datasname = dataname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				}			
			}
		});
		$('#uomconversiondataupdatesubbtn').show().removeClass('hidedisplayfwg');
		$('#uomconversionsavebutton').hide();
	}
}
{// Update old information
	function uomconversionupdateformdata() {
		var formdata = $("#uomconversionaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Uomconversion/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if(nmsg == 'TRUE'){
					$('#uomconversiondataupdatesubbtn').hide();
					$('#uomconversionsavebutton').show();
					$('.singlecheckid').click();
					$(".addsectionclose").trigger("click");
					resetFields();
					refreshgrid();
					alertpopup(savealert);
					//for touch
					masterfortouch = 0;
				}           
			},
		});	
	}
}
{// Delete Information
	function uomconversionrecorddelete(datarowid) {
		var uomconversionelementstable = $('#uomconversionelementstable').val();
		var elementspartable = $('#uomconversionelementspartabname').val();
		$.ajax({
			url: base_url + "Uomconversion/deleteinformationdata?uomconversionprimarydataid="+datarowid+"&uomconversionelementstable="+uomconversionelementstable+"&uomconversionparenttable="+elementspartable,
			cache:false,
			success: function(msg) {				
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "false")  {
					refreshgrid();
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup('Permission denied');
				}			
			},
		});
	}
}