<script>
$(document).ready(function() 
{ 
	{//Foundation Initialization
		$(document).foundation();
	}	
   	{//grid Settings call
		stonegrid();
	}	
	{//first field focus
		firstfieldfocus();
	}
	STONECATEGORYDELETE = 0;
	retrievestatus = 0;
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	//form submission preventing on enter key from fields
	$("form").bind('keypress', function(event){
		if(event.keyCode == 13){ 
		    event.preventDefault();
		    return false;
		}
	});
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	$('#groupcloseaddform').hide();
	$('#stonecategoryviewtoggle').hide();
	// category tree
	{
		$('#dl-promenucategory').dlmenu({
		animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
	}
	// parent category select
	$('#prodcategorylistuldata li').click(function(){ 
		var name=$(this).attr('data-listname');
		var listid=$(this).attr('data-listid'); 
		var level = parseInt($(this).attr('data-level'));
		var maxlevel = parseInt($('#maxcategorylevel').val());
		$('#prodparentcategoryname').val(name);
		$('#proparentcategoryid').val(listid);
		//reload grid
		if(level >= maxlevel){
			alertpopup('Maximum category level reached');
		}
		var lststone = $(this).attr('data-advfield'); 
		if(lststone == 'Yes') {
			$("#loosestonecboxid").prop('checked',true).attr('disabled',true);
			$("#loosestonecbox").val('Yes');
		} else {
			$("#loosestonecboxid").prop('checked',false).attr('disabled',false);
			$("#loosestonecbox").val('No');
		}
		var editid = $('#stonecategoryprimaryid').val();					  
		if(listid != editid) {
			refreshgrid();
		} else {
			$('#prodparentcategoryname').val('');
			$('#proparentcategoryid').val(1);
			alertpopup('You cant choose the same category as parentcategory');
		}
	});
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{ // stonecategory crud opertaions
		// stonecategory add
		$('#stonecreate').click(function(){  
		     $("#stoneaddwizard").validationEngine('validate');
					$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#stoneaddwizard").validationEngine({
					onSuccess: function()
					{
						stoneadd();
					},
					onFailure: function()
					{
						var dropdownid =['1','stonetype'];
						dropdownfailureerror(dropdownid);
					}
		});
		//Stonecategory add
		$("#addicon").click(function(){	
			$("#stonecategorysectionoverlay").removeClass("closed");
			$("#stonecategorysectionoverlay").addClass("effectbox");
			resetFields();
			retrievestatus = 0;
			$("#stonecategoryprimaryid").val('');
			$("#setdefaultcboxid,#loosestonecboxid").prop('checked',false);
			$("#setdefault,#loosestonecbox").val('No');
			Materialize.updateTextFields();
			$('#stonetype,#prodcategorylistbutton,#loosestonecboxid').attr('disabled',false);
			firstfieldfocus();
		});
		 //stonecategory edit
		$("#editicon").click(function(){			
			var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var btnid = ['stonecreate','stoneupdate'];
				var btnsh = ['hide','show'];
				stoneaddupdatebtnsh(btnid,btnsh);
				retrievestatus = 1;
				$('#stonedelete,#stoneedit').hide();
				$('#stonetype').attr('disabled',true);
				$("#stonecategorysectionoverlay").removeClass("closed");
				$("#stonecategorysectionoverlay").addClass("effectbox");
				stonecategoryretrive();
			} else {
				alertpopup(selectrowalert);
			}
		});
		$('.addsectionclose').click(function(){
			var btnid = ['stoneupdate','stonecreate'];
			var btnsh = ['hide','show'];
			stoneaddupdatebtnsh(btnid,btnsh);
			$('#stonedelete,#stoneedit').show();
			$("#stonecategorysectionoverlay").addClass("closed");
			$("#stonecategorysectionoverlay").removeClass("effectbox");
			resetFields();
			$("#setdefaultcboxid,#loosestonecboxid").prop('checked',false);
			$("#setdefault,#loosestonecbox").val('No');
		});
		//update stonecategory information
		$('#stoneupdate').click(function()
		{
			$("#stoneeditwizard").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
	    jQuery("#stoneeditwizard").validationEngine({
			onSuccess: function()
			{	
				stoneupdate();
			},
			onFailure: function()
			{
				var dropdownid =['1','stonetype'];
				dropdownfailureerror(dropdownid);
			}	
		});	
	  //delete stonecategory detail
		$("#deleteicon").click(function(){			
			var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
			var parentstonecategorynameid = getgridcolvalue('stonegrid',datarowid,'parentstonecategoryname','');
			var parentcatval=$(parentstonecategorynameid).text().length;
			if(datarowid){
			$("#stonecategoryprimaryid").val(datarowid);
				$.ajax({
					url: base_url + "Base/multilevelmapping?level="+2+"&datarowid="+datarowid+"&table=stonecategory&fieldid=parentstonecategoryid&table1=stone&fieldid1=stonecategoryid&table2=''&fieldid2=''",
					success: function(msg) {
						if(msg > 0){
							alertpopup("This stonecategory is already in usage. You cannot delete this stonecategory");						
						}else{
							if(parentcatval>1){
								combainedmoduledeletealert('stonedeleteyes','Are you sure,you want to delete this stonecategory ?');
							} else {
								combainedmoduledeletealert('stonedeleteyes','Are you sure,you want to delete this stonecategory ?');		
							}
							if(STONECATEGORYDELETE == 0) {
								$("#stonedeleteyes").click(function(){	var datarowid = $("#stonecategoryprimaryid").val();	
									stonecategorydelete(datarowid);
								});
							}
							STONECATEGORYDELETE = 1;
						}
					},
				});
			}
			else{
				alertpopup(selectrowalert);
			}
		});
		//Stone Reload
		$("#stonerefresh").click(function(){
			resetFields();
			refreshgrid();
			$('#stoneupdate').hide();
			$('#stonecreate,#stonedelete,#stoneedit').show();
			$('#stonetype').attr('disabled',false);
			setTimeout(function(){
				$('#stonetype').focus();
	        },100);
		});	
	}
	// display category based on stone type change
	$('#stonetype').change(function(){
		var stonetype=$(this).val();
		$('#proparentcategoryid').val('');
		stonecategorytreereloadfun(stonetype);
	});
	//filter
	$("#stonecategoryfilterddcondvaluedivhid").hide();
	$("#stonecategoryfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#stonecategoryfiltercondvalue").focusout(function(){
			var value = $("#stonecategoryfiltercondvalue").val();
			$("#stonecategoryfinalfiltercondvalue").val(value);
			$("#stonecategoryfilterfinalviewconid").val(value);
		});
		$("#stonecategoryfilterddcondvalue").change(function(){
			var value = $("#stonecategoryfilterddcondvalue").val();
			stonecategorymainfiltervalue=[];
			$('#stonecategoryfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    stonecategorymainfiltervalue.push($mvalue);
				$("#stonecategoryfinalfiltercondvalue").val(stonecategorymainfiltervalue);
			});
			$("#stonecategoryfilterfinalviewconid").val(value);
		});
	}
	stonecategoryfiltername = [];
	$("#stonecategoryfilteraddcondsubbtn").click(function() {
		$("#stonecategoryfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#stonecategoryfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(stonegrid,'stonecategory');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});	
});
function stoneadd(){
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	var formdata = $("#stoneform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	 $.ajax(
		{
			url: base_url +"Stonecategory/stonecategorycreate",
			data: "datas=" + datainformation,
			type: "POST",
			success: function(msg)
			{
				var nmsg =  $.trim(msg);
				refreshgrid();
				 var parentcatid = $('#proparentcategoryid').val();
				 var parentcatname = $('#prodparentcategoryname').val();	
				if (nmsg == 'SUCCESS'){
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					$(".addsectionclose").trigger("click");
					alertpopup(savealert);
							resetFields();
							$('#prodcategorylistuldata li').empty();
					}
				else if(nmsg == 'LEVEL') {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup('Maximum level of sub category reached !!! cannot create category further.');
				}
				else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
			},
		});
}
//stonecategory  treeload
function categorytreereloadfun(param1,param2)
{	
	$.ajax({
		url: base_url + "Stonecategory/categorytreedatafetchfun",     
		type: "POST",
		async:false,
        success: function(data) {
			$('#dl-promenucategory').empty();
			$('#dl-promenucategory').append(data);
			$('#prodcategorylistuldata li').click(function()
			{
				var name=$(this).attr('data-listname');
				var listid=$(this).attr('data-listid');
				var level = parseInt($(this).attr('data-level'));
				var maxlevel = parseInt($('#maxcategorylevel').val());
				$('#prodparentcategoryname').val(name);
				$('#proparentcategoryid').val(listid);
				var lststone = $(this).attr('data-advfield'); 
				if(lststone == 'Yes') {
					$("#loosestonecboxid").prop('checked',true).attr('disabled',true);
					$("#loosestonecbox").val('Yes');
				} else {
					$("#loosestonecboxid").prop('checked',false).attr('disabled',false);
					$("#loosestonecbox").val('No');
				}
				//reload grid
				if(level >= maxlevel){
					alertpopup('Maximum category level reached');
				}
				var editid = $('#stonecategoryprimaryid').val();					  
				if(listid != editid) {
					refreshgrid();
				} else {
					$('#prodparentcategoryname').val('');
					$('#proparentcategoryid').val(1);
					alertpopup('You cant choose the same category as parentcategory');
				}
			});
			$(function() {
				$( '#dl-promenucategory' ).dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});
        },
    });
	if(checkValue(param1) == true){		
		$('#proparentcategoryid').val(param1);		
		$('#prodparentcategoryname').val(param2);		
	}
}
//stone type based category  treeload
function stonecategorytreereloadfun(stonetype)
{
	var primarydataid = 0;
	var mandfield = $('#treevalidationcheck').val();
	var fieldlab = $('#treefiledlabelcheck').val();
	var primarydataid = $("#stonecategoryprimaryid").val();
	$.ajax({
		url: base_url + "Stonecategory/stonecategorytreedatafetchfun",
		data:"stonetype="+stonetype+"&primaryid="+primarydataid+"&mandval="+mandfield+"&fieldlabl="+fieldlab,     
		type: "POST",
		async:false,
        success: function(data) {
			$('.categorytreediv').empty();
			$('.categorytreediv').append(data);
			$('#prodcategorylistuldata li').click(function()
			{
				var name=$(this).attr('data-listname');
				var listid=$(this).attr('data-listid');
				var level = parseInt($(this).attr('data-level'));
				var maxlevel = parseInt($('#maxcategorylevel').val());
				$('#prodparentcategoryname').val(name);
				$('#proparentcategoryid').val(listid);
				var lststone = $(this).attr('data-advfield'); 
				if(lststone == 'Yes') {
					$("#loosestonecboxid").prop('checked',true).attr('disabled',true);
					$("#loosestonecbox").val('Yes');
				} else {
					$("#loosestonecboxid").prop('checked',false).attr('disabled',false);
					$("#loosestonecbox").val('No');
				}
				//reload grid
				if(level >= maxlevel){
					alertpopup('Maximum category level reached');
					$('#proparentcategoryid').val('');
					$('#prodparentcategoryname').val('');
					return false;
				}
				var editid = $('#stonecategoryprimaryid').val();					  
				if(listid != editid) {
					refreshgrid();
				} else {
					$('#prodparentcategoryname').val('');
					$('#proparentcategoryid').val(1);
					alertpopup('You cant choose the same category as parentcategory');
				}
			});
			$(function() {
				$( '#dl-promenucategory' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});
			if(retrievestatus == 1) {			
				gettheparentcounter(primarydataid);
			}
        },
    });
}
//load the parent id
function gettheparentcounter(datarowid){
	$.ajax({               
		url:base_url+"Stonecategory/getcounterparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			$('#proparentcategoryid').val(data.parentcategoryid);
			$('#prodparentcategoryname').val(data.categoryname);
		}
	});
}
{//Stone View Grid
	function stonegrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#stonegrid').width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $("#stonesortcolumn").val();
		var sortord = $("#stonesortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.stonecategoryheadercolsort').hasClass('datasort') ? $('.stonecategoryheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.stonecategoryheadercolsort').hasClass('datasort') ? $('.stonecategoryheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.stonecategoryheadercolsort').hasClass('datasort') ? $('.stonecategoryheadercolsort.datasort').attr('id') : '0';
		var filterid = '';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = 45;
		userviewid = userviewid == '' ? '117' : userviewid;
		var filterid = $("#stonecategoryfilterid").val();
		var conditionname = $("#stonecategoryconditionname").val();
		var filtervalue = $("#stonecategoryfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stonecategory&primaryid=stonecategoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#stonegrid').empty();
				$('#stonegrid').append(data.content);
				$('#stonegridfooter').empty();
				$('#stonegridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('stonegrid');
				{//sorting
					$('.stonecategoryheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.stonecategoryheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.stonecategoryheadercolsort').hasClass('datasort') ? $('.stonecategoryheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.stonecategoryheadercolsort').hasClass('datasort') ? $('.stonecategoryheadercolsort.datasort').data('sortorder') : '';
						$("#stonesortorder").val(sortord);
						$("#stonesortcolumn").val(sortcol);
						stonegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('stonecategoryheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						stonegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stonecategorypgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						stonegrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#stonecategorypgrowcount').material_select();
			},
		});
	}
	{//refresh grid
		function refreshgrid() {
			var page = $('ul#stonecategorypgnum li.active').data('pagenum');
			var rowcount = $('ul#stonecategorypgnumcnt li .page-text .active').data('rowcount');
			stonegrid(page,rowcount);
		}
	}
}
//stone add/update button show or hide
function stoneaddupdatebtnsh(btnid,btnsh)
{
	for(var m=0;m < btnid.length;m++)
	{
		if(btnsh[m] == 'show')
		{
			$('#'+btnid[m]+'').show();
			$('#'+btnid[m]+'').attr('data-shstatus','show');
		}
		else if(btnsh[m] == 'hide')
		{
			$('#'+btnid[m]+'').hide();
			$('#'+btnid[m]+'').attr('data-shstatus','hide');
		}
		
	}
}
function stonecategoryretrive()
{
	var elementsname=['9','stonecategoryprimaryid','stonecategoryprimaryname','stonecategoryshortprimaryname','stonetype','proparentcategoryid','prodparentcategoryname','stonecategoryname','shortname','description'];
	var datarowid = $('#stonegrid div.gridcontent div.active').attr('id');
	$.ajax(
	{
		url:base_url+"Stonecategory/stonecategoryretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			var txtboxname = elementsname;
			var dropdowns = ['1','stonetype'];
			setTimeout(function(){
				$('#stonetype').select2('val',data.stonetype).trigger('change');
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				Materialize.updateTextFields();
			},25);
			if(data.setdefault == 'No') {
				$('#setdefaultcboxid').prop('checked',false);
				$('#setdefault').val(data.setdefault);
			} else {
				$('#setdefaultcboxid').prop('checked',true);
				$('#setdefault').val(data.setdefault);
			}
			setTimeout(function(){
				if(data.checkproduct == 0 && data.loosestone == 'No') {
					$('#prodcategorylistbutton').prop('disabled',false);
				}else{
					$('#prodcategorylistbutton').prop('disabled',true);
				}
			},25);
			if(data.loosestone == 'No') {
				$('#loosestonecboxid').prop('checked',false);
				$('#loosestonecbox').val(data.loosestone);
			} else {
				$('#loosestonecboxid').prop('checked',true);
				$('#loosestonecbox').val(data.loosestone);
			}
			$("#loosestonecboxid").attr('disabled',true);
			$('#stonecategoryname').attr('data-primaryid',datarowid);
		}
	});
}
//stonecategory detail update
function stoneupdate()
{
	var proparentcategoryid=$("#proparentcategoryid").val();
	var editedcategoryid=$("#stonecategoryname").attr('data-primaryid');
	if(proparentcategoryid!=editedcategoryid){
			$('#stonecategoryname').attr('data-primaryid','');
			var formdata = $("#stoneform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			var datarowid = $('#stonecategoryprimaryid').val();
			setTimeout(function(){
				$('#processoverlay').show();
			},25); 
			var stonetype = $('#stonetype').val();
			 $.ajax({
					url: base_url + "Stonecategory/stonecategoryupdate",
					data: "datas=" + datainformation+"&primaryid="+ datarowid+amp+"stonetype="+stonetype,
					type: "POST",
					async:false,
					success: function(msg) 
					{
						var nmsg =  $.trim(msg);
						resetFields();
						refreshgrid();
						$("#stonegrid").trigger("reloadGrid");
						$('#stoneupdate').hide();
						$('#stonecreate,#stonedelete,#stoneedit').show();
						$('#stonetype').attr('disabled',false);
						if (nmsg == 'SUCCESS'){
							setTimeout(function(){
								$('#processoverlay').hide();
								$(".addsectionclose").trigger("click");
								alertpopup(updatealert);
							},25);
							$('#prodcategorylistuldata li').empty();
						} else if(nmsg == 'FAILURE'){
							setTimeout(function(){
									$('#processoverlay').hide();
								},25);
							alertpopup("Check your Parent Stone  Category mapping. ");
							}
						 else {
							setTimeout(function(){
								$('#processoverlay').hide();
							},25);
							alertpopup(submiterror);
						}            
					},
				});
		}
		else{
			alertpopup("Don't map with same stonecategory");
		}
}
//delete scheme information
function stonecategorydelete(datarowid)
{	
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Stonecategory/stonecategorydelete",
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			refreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
			setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
		},
    });
}
//stonecategorycheck
function stonecategorycheck() {
	var primaryid = $("#stonecategoryprimaryid").val();
	var accname = $("#stonecategoryname").val();
	var elementpartable = 'stonecategory';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Stone Category name already exists!";
				}
			}else {
				return "Stone Category name already exists!";
			}
		} 
	}
}
//stoneshortnamecheck
function stoneshortnamecheck() {
	var primaryid = $("#stonecategoryprimaryid").val();
	var accname = $("#shortname").val();
	var fieldname = 'shortname';
	var elementpartable = 'stonecategory';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Stone Category short name already exists!";
				}
			}else {
				return "Stone Category short name already exists!";
			}
		} 
	}
}
</script>