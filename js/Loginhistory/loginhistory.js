$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid calling
		loginhistorygrid();
	}
	//module list reload
	$('#reloadicon').click(function(){
		resetFields();
		refreshgrid();
	});
	//overlay close
	$('#historyoverlayclose').click(function(){
		$("#loginhistoryoverlay").fadeOut();
	});
	//export history
	$('#exporticon').click(function(){
		var ExportPath = base_url+"Loginhistory/exceldataexport";
		var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="excelexport" id="excelexport" method="POST" target="_blank"><input type="hidden" name="moduleid" id="moduleid" value="245" /><input type="hidden" name="modulename" id="modulename" value="Login History" /></form>');
		$(form).appendTo('body');
		form.submit();
	});
	$(window).resize(function(){
		maingridresizeheightset('loginhistorygrid');
	});
	$(".filterspan").addClass('hidedisplay');
	$('#closeaddform').click(function(){
		window.location =base_url+'Employee';
	});
	//filter
	$("#loginhistoryfilterddcondvaluedivhid").hide();
	$("#loginhistoryfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#loginhistoryfiltercondvalue").focusout(function(){
			var value = $("#loginhistoryfiltercondvalue").val();
			$("#loginhistoryfinalfiltercondvalue").val(value);
			$("#loginhistoryfilterfinalviewconid").val(value);
		});
		$("#loginhistoryfilterddcondvalue").change(function(){
			var value = $("#loginhistoryilterddcondvalue").val();
			loginhistorymainfiltervalue=[];
			$('#loginhistoryfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    loginhistorymainfiltervalue.push($mvalue);
				$("#loginhistoryfinalfiltercondvalue").val(loginhistorymainfiltervalue);
			});
			$("#loginhistoryfilterfinalviewconid").val(value);
		});
	}
	loginhistoryfiltername = [];
	$("#loginhistoryfilteraddcondsubbtn").click(function() {
		$("#loginhistoryfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#loginhistoryfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(loginhistorygrid,'loginhistory');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
//Login history list grid
function loginhistorygrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $(window).width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.loginhistoryheadercolsort').hasClass('datasort') ? $('.loginhistoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.loginhistoryheadercolsort').hasClass('datasort') ? $('.loginhistoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.loginhistoryheadercolsort').hasClass('datasort') ? $('.loginhistoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = '110';
	var viewfieldids = '245';
	var filterid = $("#loginhistoryfilterid").val();
	var conditionname = $("#loginhistoryconditionname").val();
	var filtervalue = $("#loginhistoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=loginhistory&primaryid=loginhistoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#loginhistorygrid').empty();
			$('#loginhistorygrid').append(data.content);
			$('#loginhistorygridfooter').empty();
			$('#loginhistorygridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('loginhistorygrid');
			{//sorting
				$('.loginhistoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.loginhistoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#loginhistorypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.loginhistoryheadercolsort').hasClass('datasort') ? $('.loginhistoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.loginhistoryheadercolsort').hasClass('datasort') ? $('.loginhistoryheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#loginhistorygrid .gridcontent').scrollLeft();
					loginhistorygrid(page,rowcount);
					$('#loginhistorygrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('loginhistoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					loginhistorygrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#loginhistorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					loginhistorygrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			{//history information
				$('div.gridcontent div.data-rows').dblclick(function(){
					var datarowid = $('#loginhistorygrid div.gridcontent div.active').attr('id');
				    $('#ovusername').text(getgridcolvalue('loginhistorygrid',datarowid,'employeename',''));
				    $('#emailid').text(getgridcolvalue('loginhistorygrid',datarowid,'emailid',''));
				    $('#ipaddress').text(getgridcolvalue('loginhistorygrid',datarowid,'ipaddress',''));
				    $('#signintime').text(getgridcolvalue('loginhistorygrid',datarowid,'signintime',''));
				    $('#signouttime').text(getgridcolvalue('loginhistorygrid',datarowid,'signouttime',''));
				    $('#status').text(getgridcolvalue('loginhistorygrid',datarowid,'loginstatus',''));
				    $('#browsername').text(getgridcolvalue('loginhistorygrid',datarowid,'browsername',''));
				    $('#platformname').text(getgridcolvalue('loginhistorygrid',datarowid,'platformname',''));
					$("#loginhistoryoverlay").fadeIn();
				});
			}
			//Material select
			$('#loginhistorypgrowcount').material_select();
		},
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#loginhistorypgnum li.active').data('pagenum');
		var rowcount = $('ul#loginhistorypgnumcnt li .page-text .active').data('rowcount');
		loginhistorygrid(page,rowcount);
	}
}