$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		solutionscategoryaddgrid();
		maingridresizeheightset('solutionscategoryaddgrid');
		firstfieldfocus();
		crudactionenable()
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			//leadactivitiesgrid();
			solutionscategoryaddgrid();
		});
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	//hidedisplay
	$('#solutionscategorydataupdatesubbtn').hide();
	$('#solutionscategorysavebutton').show();
	$( window ).resize(function() {
		maingridresizeheightset('solutionscategoryaddgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	{//validation for Company Add  
		$('#solutionscategorysavebutton').click(function() {
			$("#solutionscategoryformaddwizard").validationEngine('validate');
			//for touch
			masterfortouch = 0;	
		});
		jQuery("#solutionscategoryformaddwizard").validationEngine( {
			onSuccess: function() {
				addformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update company information
		$('#solutionscategorydataupdatesubbtn').click(function() {
			$("#solutionscategoryformeditwizard").validationEngine('validate');
		});
		jQuery("#solutionscategoryformeditwizard").validationEngine({
			onSuccess: function() {
				$('#solutionscategorydataupdatesubbtn').attr('disabled','disabled'); 
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
   {//counter hierarchy
		$('#solutionscategorylistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var primaryid = $("#solutionscategoryprimarydataid").val();
			if(primaryid == listid) {
				alertpopup('You cant choose the same category as parent category');
			} else {
				$('#parentknowledgebasecategory').val(name);
				$('#parentknowledgebasecategoryid').val(listid);
				solutionscategoryaddgrid();
			}
			
		});	
	}
   {//category action
		$("#cloneicon").click(function() {
			var datarowid = $('#solutionscategoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {	
				solutioncatclonedatafetchfun(datarowid);
				Materialize.updateTextFields();
			} else { 
				alertpopup("Please select a row");
			}
		});		
		$("#reloadicon").click(function() {
			var treevalidation = $('#treevalidationcheck').val();
			var treelabel = $('#treefiledlabelcheck').val();
			resetFields();
			$('#treevalidationcheck').val(treevalidation);
			$('#treefiledlabelcheck').val(treelabel);
			$('#solcategoryimageattachdisplay').empty();
			$('#imagename').val('');
			$('#parentknowledgebasecategoryid').val('');
			refreshgrid();			
			$('#solutionscategorydataupdatesubbtn').hide();
			$('#solutionscategorysavebutton').show();
			$("#solutionscategorydeleteicon").show();
		});
	}
   {// From Computer
		fileuploadmoname = 'solutioncategoryfileupload';
		var categorysettings = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			dataType:'json',
			allowedTypes: "png,jpg,jpeg,bmp,gif",
			async:false,
			onSuccess:function(files,data,xhr) {
				if(data != 'Size') {
					$("#processoverlay").show();
					setTimeout(function() {
						var arr = data.split(',');
						var name = $('.dyimageupload').data('imgattr');
						$('#imagename').val(arr[3]);
						$('#imagename').val(arr[3]);
						$('#solcategoryimageattachdisplay').empty();
						var img = $('<img id="imagenamedynamic" style="height:100%;width:100%">');
						img.attr('src', base_url+arr[3]);
						img.appendTo('#solcategoryimageattachdisplay');
						$('#solcategoryimageattachdisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#knowledgefileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function() {
							$(this).remove();
							$('#solcategoryimageattachdisplay').empty();
							$('#imagename').val('');
						});
						$("#processoverlay").hide();
						alertpopup('File uploaded successfully');
					},100);
				} else {
					$("#processoverlay").hide();
					alertpopup('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#imagenamemulitplefileuploader").uploadFile(categorysettings);
		//file upload drop down change function
		$("#knowledgefileuploadfromid").change(function() {
			var valoption = $("#knowledgefileuploadfromid").val();
			$("#fileuploadfromid").val(valoption);
			if(valoption == '2') {				
				$(".triggeruploadsolutioncategoryfileupload:last").trigger('click');
				$("#knowledgefileuploadfromid").select2("val","");
			} else {
				$(".dropin-btn-status").trigger('click');
				$("#knowledgefileuploadfromid").select2("val","");
			} 
		});	
	}
	{//from drop box
		$("#imagenamedb-chooser").on('DbxChooserSuccess',function(event){ 
			var a = event.originalEvent.files;
			var datalength = event.originalEvent.files.length;
			var n = jQuery("#fileuploadgrid").jqGrid('getGridParam', 'records');
			for(var i=0; i < datalength; i++) {
				var file = a[i].name;
				var extension = file.substr( (file.lastIndexOf('.') +1) );
				if(extension == 'png' || extension == 'jpg' || extension == 'jpeg') {
					var name = $('.dyimageupload').data('imgattr');
					$('#imagename').val(a[i].link);
					$('#solcategoryimageattachdisplay').empty();
					var img = $('<img id="imagenamedynamic" style="height:100%;width:100%">');
					img.attr('src', a[i].link);
					img.appendTo('#solcategoryimageattachdisplay');
					alertpopup('Your file is uploaded successfully.');
					$("#knowledgefileuploadfromid").select2("val","");
				} else {
					alertpopup('Only jpg,png,jpeg Image accept');
				}
			}
		});
	}
	{//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#solutionscategoryviewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#solutionscategoryviewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#solutionscategoryviewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
	}	
	{//filter work
		//for toggle
		$('#solutionscategoryviewtoggle').click(function() {
			if ($(".solutionscategoryfilterslide").is(":visible")) {
				$('div.solutionscategoryfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.solutionscategoryfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#solutionscategoryclosefiltertoggle').click(function(){
			$('div.solutionscategoryfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#solutionscategoryfilterddcondvaluedivhid").hide();
		$("#solutionscategoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#solutionscategoryfiltercondvalue").focusout(function(){
				var value = $("#solutionscategoryfiltercondvalue").val();
				$("#solutionscategoryfinalfiltercondvalue").val(value);
			});
			$("#solutionscategoryfilterddcondvalue").change(function(){
				var value = $("#solutionscategoryfilterddcondvalue").val();
				if( $('#s2id_solutionscategoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					solutionscategorymainfiltervalue=[];
					$('#categoryfilterddcondvalue option:selected').each(function(){
					    var $value =$(this).attr('data-ddid');
					    solutionscategorymainfiltervalue.push($value);
						$("#solutionscategoryfinalfiltercondvalue").val(solutionscategorymainfiltervalue);
					});
					$("#solutionscategoryfilterfinalviewconid").val(value);
				} else {
					$("#solutionscategoryfinalfiltercondvalue").val(value);
					$("#solutionscategoryfilterfinalviewconid").val(value);
				}
			});
		}
		solutionscategoryfiltername = [];
		$("#solutionscategoryfilteraddcondsubbtn").click(function() {
			$("#solutionscategoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#solutionscategoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(solutionscategoryaddgrid,'solutionscategory');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$(".solutionviewaddiconclass").click(function(){
		var moduleid = $("#solutionscategoryviewfieldids").val();
		$.ajax({
			url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
			cache:false,
			success: function(data) {  
				$("#viewcreationformdiv").empty();
				$("#viewcreationformdiv").append(data);
				$('.gridviewdivforsh').hide();
				$('#viewcreationformdiv').fadeIn(900);
				$("#viewreloadiconbtn").show();
				var selectid='';
				viewcondvaluereset();
				$("#viewwoconsubmitbtn").show();
				$("#vieweditconsubmitbtn").hide();
				clearform('viewcleardataform');
				$("#s2id_viewcolname").find('ul').removeClass('error');
				$("#viewname").focus();
				setTimeout(function() {
					$('#viewcolname').select2('val',selectid).trigger('change');
					$('#defviewfiledids').val(selectid);
					$('#viewactiontype').val('0');
					$('#viewdefaultopt,#viewpublicopt').val('No');
					$("#viewdefault,#viewfavourite").prop('checked',false);
				},400);
				viewcreateconditiongrid();
				setTimeout(function(){
					andorcondvalidationreset();
				},50);
				var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
				if(condcnt==0) {
					$("#viewandorconddivhid").hide();
				} else{
					$("#viewandorconddivhid").show();
				}
				formheight();
			},
		});
	});
	//view edit icon
	$(".solutionviewediticonclass").click(function(){
		viewcondvaluereset();
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		$.ajax({
			url:base_url+"Base/viewcreateuseridget",
			data: "userviewid="+userviewid,
			type: "POST",
			dataType :'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var moduleid = $("#viewfieldids").val();
					$.ajax({
						url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
						cache:false,
						success: function(viewdata) {  
							$("#viewcreationformdiv").empty();
							$("#viewcreationformdiv").append(viewdata);
							$("#viewreloadiconbtn").hide();
							$('.gridviewdivforsh').hide();
							$('#viewcreationformdiv').fadeIn(900);
							$("#viewwoconsubmitbtn").hide();
							$("#vieweditconsubmitbtn").show();
							$('#viewactiontype').val('1');
							$("#viewname").focus();
							$(".ftab").trigger('click');
							viewcreateconditiongrid();
							setTimeout(function(){
								andorcondvalidationreset();
							},50);
							var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
							if(condcnt==0) {
								$("#viewandorconddivhid").hide();
							} else{
								$("#viewandorconddivhid").show();
							}
							formheight()
							if(userviewid!="") {
								viewcreationeditshowinfo(userviewid);
							}
						},
					});
				} else {
					alertpopup("It is default view of this application.it can't be edited by any user");
				}
			}
		});
	});
	{//view delete
		$('.solutionviewdeleteiconclass').click(function(){	
			$.ajax({
				url: base_url + "Base/viewdeleteformfetch",
				success: function(data) {
					$("#viewdeleteoverlayconform").empty();
					$("#viewdeleteoverlayconform").append(data);
					$("#viewdeleteoverlayconform").show();
					$("#viewdeleteconyes").focus();
					$('#viewdeleteconyes').click(function(){
						var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
						$.ajax({
							url:base_url+"Base/gridinformationdelete?viewid="+userviewid,
							cache:false,
							success :function(msg) {
								var nmsg = $.trim(msg);
								if(nmsg == "Success") {
									viewcreatesuccfun("dontset");
									$("#viewdeleteoverlayconform").hide();
									$("#viewddoverlay").hide();
									alertpopup('View is deleted successfully.');
								} else if(nmsg == "Fail") {
									$("#viewdeleteoverlayconform").hide();
									$("#viewddoverlay").hide();
									alertpopup("It is default view of this application.it can't be deleted by any user");
								}
							},
						});
					});
					$('#viewdeleteconno').click(function(){	
						$("#viewdeleteoverlayconform").fadeOut();
					});
				},
			});
			
		});
	}
	{//view clone
		$('.solutionviewcloneiconclass').click(function() {
			var moduleid = $("#viewfieldids").val();
			$.ajax({
				url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
				cache:false,
				success: function(viewdata) {  
					$("#viewcreationformdiv").empty();
					$("#viewcreationformdiv").append(viewdata);
					$("#viewreloadiconbtn").hide();
					$('.gridviewdivforsh').hide();
					$('#viewcreationformdiv').fadeIn(900);
					viewcondvaluereset();
					$("#viewwoconsubmitbtn").show();
					$("#vieweditconsubmitbtn").hide();
					$('#viewactiontype').val('1');
					$("#viewname").focus();
					$(".ftab").trigger('click');
					viewcreateconditiongrid();
					setTimeout(function(){
						andorcondvalidationreset();
					},50);
					var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#viewandorconddivhid").hide();
					} else{
						$("#viewandorconddivhid").show();
					}
					formheight()
					var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
					if(userviewid!="") {
						viewcreationeditshowinfo(userviewid);
					}
				},				
			});
		});
	}
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Solutions';
	});
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function solutionscategoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#solutionscategoryaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#solutionscategorysortcolumn").val();
	var sortord = $("#solutionscategorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.solutionscategoryheadercolsort').hasClass('datasort') ? $('.solutionscategoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.solutionscategoryheadercolsort').hasClass('datasort') ? $('.solutionscategoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.solutionscategoryheadercolsort').hasClass('datasort') ? $('.solutionscategoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#solutionscategoryviewfieldids').val();
	var filterid = $("#solutionscategoryfilterid").val();
	var conditionname = $("#solutionscategoryconditionname").val();
	var filtervalue = $("#solutionscategoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=knowledgebasecategory&primaryid=knowledgebasecategoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#solutionscategoryaddgrid').empty();
			$('#solutionscategoryaddgrid').append(data.content);
			$('#solutionscategoryaddgridfooter').empty();
			$('#solutionscategoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('solutionscategoryaddgrid');
			{//sorting
				$('.solutionscategoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.solutionscategoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#solutionscategorypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.solutionscategoryheadercolsort').hasClass('datasort') ? $('.solutionscategoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.solutionscategoryheadercolsort').hasClass('datasort') ? $('.solutionscategoryheadercolsort.datasort').data('sortorder') : '';
					$("#solutionscategorysortorder").val(sortord);
					$("#solutionscategorysortcolumn").val(sortcol);
					var sortpos = $('#solutionscategoryaddgrid .gridcontent').scrollLeft();
					solutionscategoryaddgrid(page,rowcount);
					$('#solutionscategoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('solutionscategoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					solutionscategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#solutionscategorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					solutionscategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#solutionscategoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#solutionscategorypgrowcount').material_select();
		},
	});
}
function crudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#solutionscategorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#solutionscategorysavebutton').show();
			$("#solutionscategoryprimarydataid").val('');
		});
	}
	$("#editicon").click(function() {
		var datarowid = $('#solutionscategoryaddgrid div.gridcontent div.active').attr('id');
		if (datarowid){
			var treevalidation = $('#treevalidationcheck').val();
			var treelabel = $('#treefiledlabelcheck').val();
			resetFields();
			$('#treevalidationcheck').val(treevalidation);
			$('#treefiledlabelcheck').val(treelabel);
			solutioncateditdatafetchfun(datarowid);
			Materialize.updateTextFields();
		} else {
			alertpopup("Please select a row");
		}
		//for touch
		masterfortouch = 1;	
		//for Keyboard	
		viewgridview = 4;
	});
	$("#deleteicon").click(function(){
		var datarowid = $('#solutionscategoryaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			$("#basedeleteoverlay").fadeIn();
			$("#solutionscategoryprimarydataid").val(datarowid);
			$("#basedeleteyes").focus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#basedeleteyes").click(function() {
		var datarowid = $("#solutionscategoryprimarydataid").val(); 
		recorddelete(datarowid);
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#solutionscategorypgnum li.active').data('pagenum');
		var rowcount = $('ul#solutionscategorypgnumcnt li .page-text .active').data('rowcount');
		solutionscategoryaddgrid(page,rowcount);
	}
}
//create counter
function addformdata() {
    var formdata = $("#solutionscategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Solutionscategory/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg){
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				var treevalidation = $('#treevalidationcheck').val();
				var treelabel = $('#treefiledlabelcheck').val();
				resetFields();
				$(".addsectionclose").trigger("click");
				$('#treevalidationcheck').val(treevalidation);
				$('#treefiledlabelcheck').val(treelabel);
				$('#imagename').val('');
				$('#solcategoryimageattachdisplay').empty();
				alertpopup(savealert);
				$('#parentknowledgebasecategoryid').val('');
				refreshgrid()
				setTimeout(function(){treereloadfun();},500); 
		    } 
        },
    });
}
//tree reload
function treereloadfun() {
	var tablename = "knowledgebasecategory";
	var mandfield = $('#treevalidationcheck').val();
	var fieldlab = $('#treefiledlabelcheck').val();
	var primarydataid = $("#solutionscategoryprimarydataid").val(); 
	$.ajax({
		url: base_url + "Solutionscategory/treedatafetchfun",
        data: "tabname="+tablename+'&mandval='+mandfield+'&fieldlabl='+fieldlab+"&primaryid="+primarydataid,
		type: "POST",
		cache:false,
        success: function(data) {
			$('.'+tablename+'treediv').empty();
			$('.'+tablename+'treediv').append(data);
			$('#solutionscategorylistuldata li').click(function() {
				var name=$(this).attr('data-listname'); 
				var listid=$(this).attr('data-listid');
				$('#parentknowledgebasecategory').val(name);
				$('#parentknowledgebasecategoryid').val(listid);
				refreshgrid();
			});
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});		
        },
    });
}
//retrive counter data on edit
function getformdata(datarowid) {
	var solutionscategoryelementsname = $('#solutionscategoryelementsname').val();
	var solutionscategoryelementstable = $('#solutionscategoryelementstable').val();
	var solutionscategoryelementscolmn = $('#solutionscategoryelementscolmn').val();
	var solutionscategoryelementspartabname = $('#solutionscategoryelementspartabname').val();
	$.ajax(	{
		url:base_url+"Solutionscategory/fetchformdataeditdetails?solutionscategoryprimarydataid="+datarowid+"&solutionscategoryelementsname="+solutionscategoryelementsname+"&solutionscategoryelementstable="+solutionscategoryelementstable+"&solutionscategoryelementscolmn="+solutionscategoryelementscolmn+"&solutionscategoryelementspartabname="+solutionscategoryelementspartabname, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)	{
			if((data.fail) == 'Denied') {
				$("#solutionscategorydeleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				clearinlinesrchandrgrid('solutionscategoryaddgrid');
				//For Touch
				smsmasterfortouch = 0;
			} else {
				var txtboxname = solutionscategoryelementsname + ',solutionscategoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = solutionscategoryelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var attachfldname = $('#solutionscategoryattachfieldnames').val();
				var attachcolname = $('#solutionscategoryattachcolnames').val();
				var attachuitype = $('#solutionscategoryattachuitypeids').val();
				var attachfieldid = $('#solutionscategoryattachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				treereloadfun();
				setTimeout(function() {
					gettheparentcounter(datarowid);
				},500); 
				
			}
		}
	});
	
}
//update counter
function updateformdata() {
	var formdata = $("#solutionscategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Solutionscategory/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#solutionscategorydataupdatesubbtn').hide();
				$('#solutionscategorysavebutton').show();
				refreshgrid();
				$(".addsectionclose").trigger("click");
				var treevalidation = $('#treevalidationcheck').val();
				var treelabel = $('#treefiledlabelcheck').val();
				resetFields();
				$('#treevalidationcheck').val(treevalidation);
				$('#treefiledlabelcheck').val(treelabel);
				$('#imagename').val('');
				$('#solcategoryimageattachdisplay').empty();
				$("#solutionscategorydataupdatesubbtn").attr('disabled',false); 				
				alertpopup(savealert);
				setTimeout(function(){treereloadfun();},500); 
				$("#solutionscategorydeleteicon").show();		
            }  
			//for Touch
			masterfortouch = 0;	
			viewgridview = 3;
        },
    });
}
//delete counter
function recorddelete(datarowid) {
	var solutionscategoryelementstable = $('#solutionscategoryelementstable').val();
	var solutionscategoryelementspartabname = $('#solutionscategoryelementspartabname').val();
    $.ajax({
        url: base_url + "Solutionscategory/deletetreeinformationdata?solutionscategoryprimarydataid="+datarowid+"&solutionscategoryelementstable="+solutionscategoryelementstable+"&solutionscategoryelementspartabname="+solutionscategoryelementspartabname,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid()
				$("#basedeleteoverlay").fadeOut();
				alertpopup("Deleted successfully");
				setTimeout(function(){treereloadfun();},500); 
            } else if (nmsg == "Denied")  {
            	refreshgrid()
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
function reloadthecounterlistelement() {
	$.ajax({               
		url:base_url+"Category/reloadcounterlist",
		dataType:'json',
		async:false,
		cache:false,
		success:function (data) {
			console.log(data);
		}
	});
}
//load the parent id
function gettheparentcounter(datarowid) {
	$.ajax({               
		url:base_url+"Solutionscategory/getcounterparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data) {
			$('#parentknowledgebasecategoryid').val(data.parentcategoryid);
			$('#parentknowledgebasecategory').val(data.categoryname);
		}
	});
}
{// Edit Function
	function solutioncateditdatafetchfun(datarowid) {
		$('#solutionscategorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
		$('#solutionscategorysavebutton').hide();
		$("#solutionscategorydeleteicon").hide();
		sectionpanelheight('groupsectionoverlay');
		$("#groupsectionoverlay").removeClass("closed");
		$("#groupsectionoverlay").addClass("effectbox");
		getformdata(datarowid);
		firstfieldfocus();
	}
}
{// Clone Function
	function solutioncatclonedatafetchfun(datarowid) {
		$("#solutionscategorysavebutton").show();
		$("#solutionscategorydataupdatesubbtn").hide();
		$("#groupsectionoverlay").removeClass("closed");
		$("#groupsectionoverlay").addClass("effectbox");
		getformdata(datarowid);
		firstfieldfocus();
	}
}