<script>
$(document).ready(function() {
	stonewtcheck = $('#stonewtcheck').val();
	{//Foundation Initialization
		$(document).foundation();
	}
	pdfpreviewtemplateload(50);
	round = parseFloat($('#weight_round').val());
	dia_weight_round = parseFloat($('#dia_weight_round').val());
	roundamount = parseFloat($('#amount_round').val());
	rate_round = parseFloat($('#rate_round').val());
	autoweight = $('#autoweight').val();
	j = 0;
	addchargeoperation = 'ADD';
	temp_categorylevel = 0;
	stonedetails_status = 0;
	additional_details = 0;
	detailedstatus =0;
	producteditstatus =0;
	itemeditstatus = 0;
	randomnumber = '';
	stonedetailsicon = 0;
	stoneweighticon = 0;
	melting_round=$('#melting_round').val();
	lotstatus = $('#lotstatus').val();
	rfidstatus = $('#rfidstatus').val();
	stocknotify = $('#stocknotify').val();
	vacchargeshow = $('#vacchargeshow').val();
	chargecalculation = $('#chargecalculation').val();
	lotpieces = $('#lotpieces').val();
	purchasewastage ='No';
	imagestatus = $('#imagestatus').val();
	storagequantity = $('#storagequantity').val();
	counterusestatus = $('#counteryesno').val();
	storagedisplay = $('#storagedisplay').val();
	wastagecalctype = 1;
	grossweighttrigger = $('#grossweighttrigger').val();
	vendordisable = $('#vendordisable').val();
	chargerequired = $('#chargerequired').val();
	userroleid = $('#userroleid').val(); //get userroleid for login person
	itemtagauthrole = $('#itemtagauthrole').val(); //Authorization based field level access - Need to implement in companysettings (front end)
	itemtagauthrole = itemtagauthrole.split(",");
	chargeapply = $('#chargeapply').val(); //Default charges set from company settings - Need to implement in companysettings (front end)
	rfidnoofdigits = $('#rfidnoofdigits').val(); //RFID no of digits validation from company settings - Need to implement in companysettings (front end)
	tolerance_lot = $('#lottolerancepercent').val(); //Lot Percentage concept. If yes, it will be 1 otherwise zero (0)
	hidproductaddoninfo = $('#hidproductaddoninfo').val();
	itemtagsavefocus = $('#itemtagsavefocus').val();
	salesreturnautomatic = $('#salesreturnautomatic').val(); // Return item automatic
	stonepiecescalc = $('#stonepiecescalc').val();
	itemtagvendorvalidate = $('#itemtagvendorvalidate').val();
	jewelmoduleid = '50';
	returnstatus = 0;
	{ // Stone Details Automatic
		$("#s2id_stoneid").css('display','inline-block').css('width','90%');
		var icontabindex = $("#s2id_stoneid .select2-focusser").attr('tabindex');
		$("#stoneid").after('<span class="innerfrmicon" id="automaticstonedetails" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
	}
	tagautoimage();
	if(lotstatus == 'YES') {
		$('#loticon').show();
   	} else {
		$('#loticon').hide();
   	}
	{// Grid Calling Function
		itemtaggrid();
		stoneentrygrid();
	}
	//stone entry section add
	$("#stoneentryaddicon").click(function() {
		sectionpanelheight('stoneentrysectionoverlay');
		$("#stoneentrysectionoverlay").removeClass("closed");
		$("#stoneentrysectionoverlay").addClass("effectbox");
		Materialize.updateTextFields();
		firstfieldfocus();
	});
	$("#stoneentrysectionclose").click(function() {
		$("#stoneentrysectionoverlay").removeClass("effectbox");
		$("#stoneentrysectionoverlay").addClass("closed");
	});
    {//close
		var addcloseinfo = ["closeaddform", "itemtaggriddisplay", "itemtagaddformdiv"]
		addclose(addcloseinfo);
	}
	$("#closeaddform").click(function() {	
		var addcloseinfo = ["closeaddform", "itemtaggriddisplay", "itemtagaddformdiv"]
		addclose(addcloseinfo);
		cleargriddata('stoneentrygrid');
		itemtagrefreshgrid();
	});
	$("#alertsfcloseyes").click(function() {
		$("#reloadicon").trigger('click');
	});
	generateiconafterload('stoneweight','stoneweighticon');
	{ //Loaing Stoneentry Overlay on Click
		$("#stoneweighticon").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined) {
				var pendinglotstonewt = $('#pendinglotstonewt').val();
				var lotid = $.trim($('#lotid').find('option:selected').val());
				var lotstoneapplicable = $.trim($('#lotid').find('option:selected').data('lotstoneapplicable'));
				var lot_lottypeid = $.trim($('#lotid').find('option:selected').data('lottypeid'));
				if(lotstoneapplicable == 'No' && lotid != 1 && lotid != '') {
					alertpopup('Unable to create stone details. Created lot has No Stone Applicable.');
				} else {
					if(lot_lottypeid == 2) {
						$("#s2id_stoneid").css('display','inline-block').css('width','90%');
						$('#automaticstonedetails').show();
						$('#automaticstonedetails').trigger('click');
						// Do not make manual entries
						$('#stoneid,#stoneentrycalctypeid').prop('disabled',true);
						$('#tagstoneentryedit').hide();
					} else {
						$("#s2id_stoneid").css('display','inline-block').css('width','100%');
						$('#automaticstonedetails').hide();
						// Do only manual entries
						$('#stoneid,#stoneentrycalctypeid').prop('disabled',false);
						$('#tagstoneentryedit').show();
					}
					$('.stonedetailsform,.stoneiconlisthide').removeClass('hidedisplay');
					$('.tagadditionalchargeclear_stone > div').removeClass('hidedisplay');
					$('.taghide,.totacarat').addClass('hidedisplay');
					stoneweighticon = 1;
					$('#savestoneentrydata').show();
					addslideup('itemtagaddformdiv','stonedetailsform');
					$('#s2id_stoneid').select2('focus');
					clearform('stoneoverlay');
					clearform('tagadditionalchargeclear_stone');
					if($("#hiddenstonedata").val()!='' && $("#hiddenstonedata").val() !=0){
						getstonegridsummary();
					}
					//setTimeout(function(){
						$('.ftabstoneoverlay').addClass('active');
					//},100);	
					Materialize.updateTextFields();
				}
			}
		});
		$("#automaticstonedetails").on('click keypress', function(e) {
			var stoneid = $('#stoneid').find('option:selected').val();
			var stoneentrycalctypeid = $('#stoneentrycalctypeid').find('option:selected').val();
			if(stoneid > 1 || stoneentrycalctypeid > 1) {
				alertpopupdouble("Automatic stone details in form field. Kindly Submit and check for another stone.!");
				return false;
			} else {
				if($('#tagaddstone').css('display') != 'none') {
					$('#automaticstonedetailoverlay').fadeIn();
					cleargriddata('autostonegrid');
					automaticpurchasestonedetails();
				} else if($('#tageditstone').css('display') != 'none') {
					alertpopupdouble("Stone Details is in Edit Mode. Kindly Update and check again!");
					return false;
				}
			}
		});
	}
	//TAG auto image change event
	$('#mtagautoimage').change(function() {
		if($('#mtagautoimage').val() == 0) {
			$('#tagautoimage').val($('#mtagautoimage').val());
		}
	});
	//Stone Entry => Stone name change => Load Rate amount
	$('#stoneid').change(function() {
		var gridname = 'stoneentrygrid';
		var colname = 'stoneid';
		var list = new Array();
		$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			list.push($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
		});
		var reorderval = $(this).find('option:selected').val();
		var isInList = false;
		for(var i = 0; i < list.length; i++){
			if(list[i] === reorderval) {
				isInList = true;
				break;
			}
		}
		if(isInList){
			alertpopupdouble("stone already Exist. Kindly choose another Stone");
			$(this).select2('val','');
			return false;
		}
		var stoneid=$(this).val(); 
		$.ajax({
			url: base_url +"Itemtag/getratefromstonename",
			data:{stoneid:stoneid},
			type: "POST",
			dataType:'json',	
			success: function(data) {
				$("#basicrate").val(parseFloat(data.stonerate));
				$("#purchaserate").val(parseFloat(data.purchaserate));
				$("#stoneentrycalctypeid").select2('val',data.stoneentrycalctypeid).trigger('change');	
				stoneentrycal();
				Materialize.updateTextFields();
			}
		});
	});
	// Stone Cara , Total weight and Total Amount Calculation
	$('#basicrate,#stonepieces,#stonegram').blur(function() {
		stoneentrycal();
	});
	$('#stonepieces').change(function() {
		var lotvalue = $(this).val();
		var lot_lottypeid = $.trim($('#lotid').find('option:selected').data('lottypeid'));
		if(lot_lottypeid == 2) {
			var autolotpieces = $('#autolotpieces').val();
			if(parseInt(lotvalue) > parseInt(autolotpieces)) {
				alertpopupdouble("Can't able to give value greater than created Purchase Pieces!");
				$(this).val(autolotpieces);
				return false;
			}
		}
		stoneentrycal();
	});
	$('#stonegram').change(function() {
		var gramvalue = $(this).val();
		var lot_lottypeid = $.trim($('#lotid').find('option:selected').data('lottypeid'));
		if(lot_lottypeid == 2) {
			var autolotgramwt = $('#autolotgramwt').val();
			if(parseFloat(gramvalue) > parseFloat(autolotgramwt)) {
				alertpopupdouble("Can't able to give value greater than created Purchase Gram Weight!");
				$(this).val(autolotgramwt);
				return false;
			}
		}
		var caratval = gramvalue * 5;
		$('#caratweight').val(parseFloat(caratval).toFixed(round));
		stoneentrycal();
	});
	$('#caratweight').blur(function() {
		var caratvalue = $(this).val();
		var lot_lottypeid = $.trim($('#lotid').find('option:selected').data('lottypeid'));
		if(lot_lottypeid == 2) {
			var autolotcaratwt = $('#autolotcaratwt').val();
			var lottoldiactvalue = $('#lottolerancediactvalue').val();
			var lotdiaweightlimit = $('#lotdiaweightlimit').val();
			if(tolerance_lot == 1) {
				var finalcaratvalue = parseFloat(((parseFloat(autolotcaratwt)*parseFloat(lottoldiactvalue)/100)) + parseFloat(autolotcaratwt)).toFixed(dia_weight_round);
			} else {
				var finalcaratvalue = parseFloat(parseFloat(autolotcaratwt) + parseFloat(lotdiaweightlimit)).toFixed(dia_weight_round);
			}
			if(parseFloat(caratvalue) > parseFloat(finalcaratvalue)) {
				alertpopupdouble("Can't able to give value greater than created Purchase Carat Weight!");
				$(this).val(autolotcaratwt);
				return false;
			}
		}
		var stoneentrycalctype = $('#stoneentrycalctypeid').find('option:selected').val();
		if(stoneentrycalctype == 3) {   // pieces wise
			$('#stonegram').val('');
		}
		stoneentrycal();
	});
	$('#stoneentrycalctypeid').blur(function() {
		stoneentrycal();
	});
	//Stone-Entry refresh-form
	$("#tagstoneentryrefresh").click(function() {
		addchargeoperation = 'ADD';
		$('#tageditc').hide();
		$('#tagadd').show();
		clearform('tagadditionalchargeclear');
		Materialize.updateTextFields();
	});
	//close popup of the Stone Entry
	$(".groupcloseaddformclass").click(function() {
		if(stonedetailsicon == 1) {
			addslideup('stonedetailsform','itemtaggriddisplay');
			stonedetailsicon = 0;
		} else if(stoneweighticon == 1) {
			addslideup('stonedetailsform','itemtagaddformdiv');
			stoneweighticon = 0;
		}
	});
	//Save the Stone-Entry Form
	$("#savestoneentrydata").click(function() {
		if($('#tageditstone').css('display') != 'none') {
			alertpopupdouble("Stone Details is in Edit Mode. Kindly Update and check again!");
			return false;
		} else {
			var netwtcalctype = $("#netwtcalid").val(); // Based on company setting value,we will calculate the val of Net Wt
			var totnetwt = '';
			if(netwtcalctype == 2) {
				totnetwt=(parseFloat($("#totcswt").html()) + parseFloat($("#totpearlwt").html())).toFixed(round);
			} else if(netwtcalctype == 3) {
				totnetwt=(parseFloat($("#totwt").html())).toFixed(round);
			} else if(netwtcalctype == 4) { // G.wt - D
				totnetwt=(parseFloat($("#totdiawt").html())).toFixed(round);
			} else if(netwtcalctype == 5) { // G.wt
				totnetwt=0;
			} else if(netwtcalctype == 6) { // G.wt - C.S => Stone weight = Total stone weight - stone weight [only reduced stone type weight] 
				totnetwt=(parseFloat($("#totwt").html()) - parseFloat($("#totcswt").html())).toFixed(round);
			}
			addslideup('stonedetailsform','itemtagaddformdiv');
			var charge_json = {}; //declared as object			
			charge_json = getgridrowsdata("stoneentrygrid");	
			if(charge_json == '') {
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").val(0).trigger('change');
				$('#hiddenstonedata').val('');
			} else {
				$('#hiddenstonedata').val(JSON.stringify(charge_json));
				$("#stoneweight").val(totnetwt).trigger('change');
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").trigger('blur');
			}
			clearform('tagadditionalchargeclear_stone');
		}
	});
	{ // Double click to open itemtag image
		$('body').on('dblclick', 'div.gridcontent div.data-rows', function() {
			var ids = $('#itemtaggriddisplay div.gridcontent div.active').attr('id');
			if(ids) {
				$.ajax({
					url:base_url+"Itemtag/retrievetagimage",
					data: "itemtagid=" +ids,
					type: "POST",
					async:false,
					cache:false,
					success: function(msg) {
						var nmsg =  $.trim(msg);
						if (nmsg != "") {
							$('#tagimagepreview').fadeIn();
							$('#itemtagimagepreview').empty();
							var img = $('<img id="companylogodynamic" style="height:100%;width:100%">');
							img.attr('src', base_url+nmsg);
							img.appendTo('#itemtagimagepreview');
						} else {
							alertpopup('No Image to preview');
						}
					}
				});	
			} else {
				alertpopup("Please select a row");
			}
		});
		// Tag image preview close click
		$("#tagimagepreviewclose").click(function(){
			$('#tagimagepreview').fadeOut();
		});
	}
	{//**change events of the tag form**//
		//Tag Type Change Function//
		$("#tagtypeid").change(function(){
			//reset the forms
			var tag_type = $(this).find('option:selected').val();
			var tag_entrytype = $('#tagentrytypeid').val();
			$("#pieces").val(1);
			var lotid = $('#lotid').find('option:selected').val();
			/* if($("#lotid").find('option:selected').val() == "" || $("#lotid").find('option:selected').val() == "1"){
				$("#purityid").select2('val','');
			} */
			cleargriddata('stoneentrygrid');
			getstonegridsummary();
			if($("#lotid").val() != "" && $("#lotid").val() != "1" && $("#productid").val() != ""){
				var template = $("#productid").find('option:selected').data('tagtemplateid');
			} else {
				var template = $('#defaultprinttemplate').val();
			}
			tagautoimage();
			$('#chargefrom').prop('disabled',false);
			if($(this).is(':disabled')) {
				//ie edit
			} else { //ie add
				if(itemeditstatus == 0) {
					var lasttagno = $("#lasttagno").val();
					var lastrfidno = $("#lastrfidno").val();
					clearform('tagthirdsection');
					setTimeout(function() {
						$('#chargefrom').select2('val',chargeapply).trigger('change');
						tagautoimage();
						$("#lasttagno").val(lasttagno);
						$("#lastrfidno").val(lastrfidno);
						Materialize.updateTextFields();
					},50);
					getcurrentsytemdate('date');					
					if($("#lotid").val() != "" && $("#productid").val() != "") {
					} else {
						clearform('tagsecondsection');
						$("#lotid,#purityid,#accountid,#fromcounterid").select2('val','');			
						$("#totallotgrosswt,#totallotnetwt,#totallotpieces,#completedlotgrosswt,#completedlotnetwt,#completedlotpieces,#pendinglotgrosswt,#pendinglotnetwt,#pendinglotpieces,#melting,#touch,#rate,#pendinglotstonewt").val('');
					}
				}
			}
			if(detailedstatus == 0) {
				if($.trim($("#lotid").val()) == "" || $.trim($("#lotid").val()) == 1) {
					var purityid=$('#purityid').find('option:selected').val();
					loadproductbasedonpurity(purityid,0);
					producthideshowbasedonpurity(tag_entrytype,purityid);	
				}
				$('.weightcalculate').val(0);
			}
			$('#productaddoninfo-div').addClass('hidedisplay');
			var value = parseInt($(this).val()); 
			switch(value) {
				case 2:	//tag
					var mtype = 'tag';
					select2ddenabledisable('lotid','enable');
					select2ddenabledisable('tagtemplateid','enable');
					$("#stoneweighticon").css("opacity","0");
					$("#stoneweighticon").show();
					stoneiconset();	
					$("#tagtemplateid").select2('val',template);
					$("#stoneweighticon").css("opacity","1");
					$("#pieces").attr('readonly',false);
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
						$('#productaddoninfo-div').removeClass('hidedisplay');
					}
				break;
				case 3:	//untag				
					var mtype = 'untag';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").hide();
					stoneiconreset();
					$("#tagtemplateid").select2('val','1');
					select2ddenabledisable('tagtemplateid','disable');
					$("#pieces").attr('readonly',false);
					$('#chargefrom').select2('val',0).trigger('change');
					$('#chargefrom').prop('disabled',true);
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
						$('#productaddoninfo-div').addClass('hidedisplay');
					}
				break;
				case 4:	//bulktag
					var mtype = 'bulktag';
					select2ddenabledisable('lotid','enable');
					select2ddenabledisable('tagtemplateid','enable');
					$("#stoneweighticon").show();
					$("#tagtemplateid").select2('val',template);
					stoneiconset();	
					$("#pieces").attr('readonly',false);
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
						$('#productaddoninfo-div').removeClass('hidedisplay');
					}
				break;
				case 5:	//pricetag
					var mtype = 'pricetag';
					select2ddenabledisable('lotid','enable');
					select2ddenabledisable('tagtemplateid','enable');
					$("#tagtemplateid").select2('val',template);
					$("#stoneweighticon").hide();
					stoneiconreset();
					$("#pieces").attr('readonly',false);
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
						$('#productaddoninfo-div').addClass('hidedisplay');
					}
				break;
				case 6:	//untagtotag
					var mtype = 'untagtotag';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").show();
					stoneiconset();	
					$("#pieces").attr('readonly',true);
				break;
				case 10:	//salesreturn
					var mtype = 'salesreturn';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").hide();
					stoneiconreset();
					select2ddenabledisable('tagtemplateid','enable');
					$("#tagtemplateid").select2('val','1');
					select2ddenabledisable('tagtemplateid','disable');
					$("#pieces").attr('readonly',true);
				break;
				case 11:	//oldpurchase
					var mtype = 'oldpurchase';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").hide();
					stoneiconreset();
					select2ddenabledisable('tagtemplateid','enable');
					$("#tagtemplateid").select2('val','1');
					select2ddenabledisable('tagtemplateid','disable');
					$("#pieces").attr('readonly',true);
				break;
				case 12:	//partialtag
					var mtype = 'partialtag';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").show();
					stoneiconset();
					select2ddenabledisable('tagtemplateid','enable');
					$("#tagtemplateid").select2('val','1');
					select2ddenabledisable('tagtemplateid','disable');
					$("#pieces").attr('readonly',true);
				break;
				case 13:	//purchasereturn
					var mtype = 'purchasereturn';
					select2ddenabledisable('lotid','enable');
					$("#stoneweighticon").hide();
					stoneiconreset();
					select2ddenabledisable('tagtemplateid','enable');
					$("#tagtemplateid").select2('val','1');
					select2ddenabledisable('tagtemplateid','disable');
					$("#pieces").attr('readonly',true);
				break;
			}
			if(checkValue(mtype) == true) {
				fieldruleset('validation_property',''+mtype+'_0');
			}
			var counteryesno = $('#counteryesno').val();
			if($("#lotid").find('option:selected').val() != "1" && $("#productid").find('option:selected').val() != "") {
				var stoneapplicable = $('#productid').find('option:selected').data('stone');
					stonedivhideshow(stoneapplicable);
					var productid = $('#productid').find('option:selected').val();
					var isrfid = $("#productid").find('option:selected').data('rfid');
					if(tag_type == '3') {
						$('#size').select2('val','');
						$('#size').attr('data-validation-engine','');
						$("#size-div,#rfidtagno-div").hide();
					} else {
						sizehideshow(productid);
						rfidhideshow(isrfid);
					}
					var vendorid = $.trim($('#lotid').find('option:selected').data('accountid'));
					if(vendorid != '' && vendorid != '1') {
						select2ddenabledisable('accountid','disable');
					} else {
						select2ddenabledisable('accountid','enable');
					}
			} else if($("#lotid").find('option:selected').val() != "1") {
				var stoneapplicable = $('#productid').find('option:selected').data('stone');
					stonedivhideshow(stoneapplicable);
					var productid = $('#productid').find('option:selected').val();
					var isrfid = $("#productid").find('option:selected').data('rfid');
					if(tag_type == '3') {
						$('#size').select2('val','');
						$('#size').attr('data-validation-engine','');
						$("#size-div,#rfidtagno-div").hide();
					} else {
						sizehideshow(productid);
						rfidhideshow(isrfid);
					}
					var vendorid = $.trim($('#lotid').find('option:selected').data('accountid'));
					if(vendorid != '' && vendorid != '1') {
						select2ddenabledisable('accountid','disable');
					}else{
						select2ddenabledisable('accountid','enable');
					}
			} else {
				/* if(tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12){
					var branchid = $('#branchid').find('option:selected').val();
					getfromstorage(tag_entrytype,branchid);
				} */
				select2ddenabledisable('purityid','enable');
				select2ddenabledisable('productid','enable');
				select2ddenabledisable('accountid','enable');
			}
			if(tag_entrytype == 3 || tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12 || tag_entrytype == 13) {
					//$("#purityid option").addClass("ddhidedisplay");
					//$("#purityid optgroup").addClass("ddhidedisplay");
					//$("#purityid option").prop('disabled',true);
					if(lotid == 0 || lotid == 1 || lotid == ''){
						$("#purityid").select2('val','');
					}else {
						var purity_id=$('#purityid').find('option:selected').val();
						producthideshowbasedonpurity(tag_entrytype,purity_id)
					}
					$('.fromcounterhide').show();
					$('#fromcounterid').attr('disabled',false);
					$('#fromcounterid').attr('data-validation-engine','validate[required]');
					//$('#lotid-div').hide();
			} else {
				$('.fromcounterhide').hide();
				$('#fromcounterid').attr('disabled',true);
				$('#fromcounterid').attr('data-validation-engine','');
				$("#purityid option").removeClass("ddhidedisplay");
				$("#purityid optgroup").removeClass("ddhidedisplay");
				$("#purityid option").prop('disabled',false);
				if(lotid == 0 || lotid == 1 || lotid == ''){
					$("#purityid").select2('val','');
				}
				//$('#lotid-div').show();
			}
			if(detailedstatus == 0) {
				$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
			}
			/* if($.trim($("#lotid").val()) != "1" && $.trim($("#lotid").val()) != "" && tag_type == 2) {
				$("#pieces").attr('readonly',true);
			}else if($.trim($("#lotid").val()) != "1" && $.trim($("#lotid").val()) != "" && tag_type == 3){
				$("#pieces").attr('readonly',true);
			} */
			if($.trim($("#lotid").find('option:selected').val()) != "1" && $.trim($("#lotid").find('option:selected').val()) != "") {
				$("#purityid").prop('disabled',true);
			} else {
				$("#purityid").prop('disabled',false);
			}
			// untag
			if(tag_type == 3) {
				$('.lasttagnodiv,.lastrfidnodiv').hide();
				$('#untagno-div').show();
			} else if(tag_type == 2 || tag_type == 4 || tag_type == 5) // tag,pricetag & bulktag
			{
				$('.lasttagnodiv,.lastrfidnodiv').show();
				$('#untagno-div').hide();
			}
			Materialize.updateTextFields();
		});
		// lot changes //
		$("#lotid").change(function() {
			$("#productid,#tagtemplateid,#tagtypeid,#purityid,#counterid").select2('val','');
			$("#purityid option").removeClass("ddhidedisplay");
			$("#purityid optgroup").removeClass("ddhidedisplay");
			$("#purityid option").prop('disabled',false);
			$('#pendinglotstonewt').val(0);
			var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
			var val = $(this).val();
			$('.lotweighthide').removeClass('hidedisplay');
			$('.lotctweighthide').addClass('hidedisplay');
			//clear the previous entries next to lot ?
			if(checkValue(val) == true) {
				if(val != 0 && val != 1 && val != '') {
					var purityid = $(this).find('option:selected').data('lotpurity');
					var productid = $(this).find('option:selected').data('productid');
					var accountid = $(this).find('option:selected').data('accountid');
					var productcount = $(this).find('option:selected').data('productcount');
					if(productcount == 0) { // don't have product for selected purity
						alertpopup('Do not have product for selected lot purity');
						$(this).select2('val',1).trigger('change');
						return false;
					}
					if(accountid != 1) {
						$('#accountid').select2('val',accountid);
						if(vendordisable == 1) { // vendor disable/enable based on companysetting
						select2ddenabledisable('accountid','disable');
						}
					} else {
						$('#accountid').select2('val','');
					}
					if(purityid != '1') {
						$('#purityid').select2('val',purityid);
						select2ddenabledisable('purityid','disable');
					}
					if(purityid == 11) {
						$('.lotweighthide').addClass('hidedisplay');
						$('.lotctweighthide').removeClass('hidedisplay');
					} else {
						$('.lotweighthide').removeClass('hidedisplay');
						$('.lotctweighthide').addClass('hidedisplay');
					}
					if(productid != '1' && productid != '3') {
						$("#productid option").addClass("ddhidedisplay");
						$("#productid optgroup").addClass("ddhidedisplay");
						$("#productid option").prop('disabled',true);
						var fieldid ='productid';
						$("#"+fieldid+" option[value="+productid+"]").removeClass("ddhidedisplay");
						$("#"+fieldid+" option[value="+productid+"]").closest('optgroup').removeClass("ddhidedisplay");
						$("#"+fieldid+" option[value="+productid+"]").prop('disabled',false);
						$('#productid').select2('val',productid);
						setproducttriggerlot(productid);
						var isrfid = $("#productid").find('option:selected').data('rfid');
						rfidhideshow(isrfid);
						select2ddenabledisable('productid','disable');
						select2ddenabledisable('purityid','disable');
					} else {
						select2ddenabledisable('productid','enable');
						setpuritytriggerlot(purityid);
						select2ddenabledisable('tagtypeid','enable');
					}
					retrivecurrentlotpending(val); //live current values
					resetwtps();
					var lasttagno = $("#lasttagno").val();
					var lastrfidno = $("#lastrfidno").val();
					clearform('tagthirdsection');
					$('#chargefrom').select2('val',chargeapply).trigger('change');
					tagautoimage();
					$("#lasttagno").val(lasttagno);
					$("#lastrfidno").val(lastrfidno);
					clearform('tagadditionalchargeclear');
					var tagtypeid = $.trim($('#tagtypeid').find('option:selected').val());
					var lstpurityid = $('#productid').find('option:selected').data('purityid');
					if(tagtypeid == 2 ) {
						$("#pieces").attr('readonly',false);
					} else {
						$('#itemcaratweight-div').hide();
						$('#itemcaratweight').attr('data-validation-engine','');
						$('#grossweight-div,#netweight-div').show();
						if(productid != '1' && tagtypeid == 5) {
							var isrfidstatus = $("#productid").find('option:selected').data('rfid');
							var rfidoption = $("#rfidoption").val(); 
							if(isrfidstatus == 'Yes' && rfidoption == '3') {
								$("#pieces").attr('readonly',true);
							}else {
								$("#pieces").attr('readonly',false);
							}
						} else if(tagtypeid == 3 && lstpurityid == 11) {
							$('#productid').select2('val',productid).trigger('change');
						} else {
							$("#pieces").attr('readonly',false);
						}
					}
					// Process product hide/show based on From type
					if(tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12 || tag_entrytype == 13) {
						processproducthideshow(tag_entrytype);
						producthideshowbasedonpurity(tag_entrytype,purityid);
					}
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
					}
					Materialize.updateTextFields();
				} else {
					$('#purityid,#accountid,#productid,#counterid,#size').select2('val','');
					$('#melting,#rfidtagno').val('');
					if(detailedstatus == 0) {
					  select2ddenabledisable('purityid','enable');
					  select2ddenabledisable('accountid','enable');
					  select2ddenabledisable('productid','enable');
					  select2ddenabledisable('tagtypeid','enable');
					}
					$("#productid option").addClass("ddhidedisplay");
					$("#productid option").prop('disabled',true);
					$('#totallotgrosswt,#totallotnetwt,#totallotpieces,#completedlotgrosswt,#completedlotnetwt,#completedlotpieces,#pendinglotgrosswt,#pendinglotnetwt,#pendinglotpieces,#grossweight,#stoneweight,#netweight,#pendinglotstonewt').val('');
					resetwtps();
					if(tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12 || tag_entrytype == 13) {
						$('#processproductid').select2('val','');
					}
					if(hidproductaddoninfo == 1) {
						$("#productaddoninfoid").select2('val',1).trigger('change');
					}
					Materialize.updateTextFields();
				}
			}
		});
		$("#purityid").change(function() {
			resetwtps();
			$('#productid,#counterid,#processproductid').select2('val','');
			if(additional_details == 0) {
				var lasttagno = $("#lasttagno").val();
				var lastrfidno = $("#lastrfidno").val();
				clearform('tagthirdsection');
				$('#chargefrom').select2('val',chargeapply).trigger('change');
				tagautoimage();
				$("#lasttagno").val(lasttagno);
				$("#lastrfidno").val(lastrfidno);
			}
			var purityid=$(this).val();
			var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
			if(tag_entrytype == 4 || tag_entrytype == 10 ||  tag_entrytype == 11 || tag_entrytype == 12  || tag_entrytype == 13) {
				loadproductbasedonpurity(purityid,0);
			} else if(tag_entrytype == 3 || tag_entrytype == 10  || tag_entrytype == 12) {
				//$("#productid option").removeClass("ddhidedisplay");
				//$("#productid optgroup").removeClass("ddhidedisplay");
				//$("#productid option").prop('disabled',false); 
				var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
				var counterid = $('#fromcounterid').find('option:selected').val();
				var branchid = $('#branchid').find('option:selected').val();
				//loadproductfromstock(tag_entrytype,counterid,branchid,purityid,'productid');
			}
			if(tag_entrytype == 11 ) {
				//$("#processproductid option").removeClass("ddhidedisplay");
				//$("#processproductid optgroup").removeClass("ddhidedisplay");
				//$("#processproductid option").prop('disabled',false); 
				var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
				var counterid = $('#fromcounterid').find('option:selected').val();
				var branchid = $('#branchid').find('option:selected').val();
				//loadproductfromstock(tag_entrytype,counterid,branchid,purityid,'processproductid');
			}
			if(tag_entrytype == 10 ||  tag_entrytype == 11 || tag_entrytype == 12  || tag_entrytype == 13) {
				producthideshowbasedonpurity(tag_entrytype,purityid);
			}
			if(purityid != '') {
				//get melting and set.
					var melting = $("#purityid").find('option:selected').data('melting');
					var melting_value=parseFloat(melting).toFixed(melting_round);
					$('#melting').val(melting_value);
			} else {
				$('#melting').val('');
			}
			$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
			if(hidproductaddoninfo == 1) {
				$("#productaddoninfoid").select2('val',1).trigger('change');
			}
			Materialize.updateTextFields();
		});
		$("#nontagpurity").change(function() {
			var tag_entrytype = 3;
			var counterid = $('#nontagfromcounterid').find('option:selected').val();
			var purityid = $(this).val();
			$('#purity').val(purityid);
			//loadproductfromstock(tag_entrytype,counterid,loggedinbranchid,purityid,'nontagproductid');
			//loadproductfromstock(tag_entrytype,counterid,loggedinbranchid,purityid,'nontagprocessproductid');
			$('#nontagtocounter').select2('val','').trigger('change');	
			nontagloadproductbasedonpurity(purityid);
			//get melting and set.
			setTimeout(function(){ 
				var melting = $("#nontagpurity").find('option:selected').data('melting');
				$('#uttmelting').val(melting);
				Materialize.updateTextFields();
			},10);
		});
		//product changes//
		$("#productid").change(function() {
			var productidval = $(this).val();
			var isrfid = $(this).find('option:selected').data('rfid');
			var tagweight = $(this).find('option:selected').data('tagweight');
			var packingweight = $(this).find('option:selected').data('packingweight');
			var packwtless = $(this).find('option:selected').data('packwtless');
			resetwtps();
			var productpieces = $(this).find('option:selected').data('productpieces');
			if(productpieces != '') {
				$('#pieces').val(productpieces);
			} else {
				$('#pieces').val(1);
			}
			$('#grossweight,#stoneweight,#netweight,#itemcaratweight').val(0);
			// clear the previous entries next to lot && display only product-related counters
			$('#counterid').select2('val','');
			if(checkValue(productidval) == true) {
				loadproductbasedcounteritem('productid','counterid');
			}
			var tagtypeid = $('#productid').find('option:selected').data('tagtypeid');
			var oldval = $("#tagtypeid").val();
			if(oldval != tagtypeid) {
				if(tagtypeid != 17) {
					$("#tagtypeid").select2('val',tagtypeid);
					settagtypevaluelottagtypeid(tagtypeid);
					if($.trim($("#lotid").find('option:selected').val()) != '1' && $.trim($("#lotid").find('option:selected').val()) != '') {
						select2ddenabledisable('purityid','disable');
						select2ddenabledisable('tagtypeid','disable');
					} else {
						select2ddenabledisable('purityid','enable');
						select2ddenabledisable('tagtypeid','enable');
					}
				} else {
					select2ddenabledisable('tagtypeid','enable');
				}
			}
			
			var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
			var tagtype = $('#tagtypeid').find('option:selected').val();
			var fromcounterid = $('#fromcounterid').find('option:selected').val();
			var branchid = $('#branchid').find('option:selected').val();
			var purityid = $('#purityid').find('option:selected').val();
			
			/* if(tag_entrytype == 3 || tag_entrytype == 10 || tag_entrytype == 12){
			    getweight(tag_entrytype,fromcounterid,purityid,branchid,productidval);
			}
			else if(tag_entrytype == 11)
			{
				getweight(tag_entrytype,fromcounterid,purityid,branchid,2);
			} */
			
			producteditstatus = 0;
			var stoneapplicable = $(this).find('option:selected').data('stone');
			stonedivhideshow(stoneapplicable);
			$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
			var tagtemplateid = $(this).find('option:selected').data('tagtemplateid');
			if(tagtemplateid != '' && tagtemplateid != '1') {
				$('#tagtemplateid').select2('val',tagtemplateid);
			}
			if(tagtype == '3') {
				var lstpurityid = $('#productid').find('option:selected').data('purityid');
				$('#size').select2('val','');
				$('#size').attr('data-validation-engine','');
				$("#size-div,#rfidtagno-div").hide();
				$('#itemcaratweight-div').hide();
				$('#itemcaratweight').attr('data-validation-engine','');
				$('#grossweight-div,#netweight-div').show();
				if(lstpurityid == 11) {
					$('#itemcaratweight-div').show();
					$('#grossweight-div,#netweight-div').hide();
					$('#itemcaratweight').attr('data-validation-engine','validate[required,min[0.01],custom[number],decval['+dia_weight_round+']]');
				}
			} else {
				sizehideshow(productidval);
				rfidhideshow(isrfid);
			}
			if(tagtype == 5) { // Price tag
				var rfidoption = $("#rfidoption").val();
				if(isrfid == 'Yes' && rfidoption == '3') {
					$("#pieces").attr('readonly',true);
				} else {
					$("#pieces").attr('readonly',false);
				}
            } else if(tagtype == 2) {
				$("#pieces").attr('readonly',false);
			} else {
				$("#pieces").attr('readonly',false);
			}
			// weight details set
			$("#tagweight").val(tagweight);
			$("#packingweight").val(packingweight);
			$('#packwtless').select2('val',packwtless);
			$('#packwtlessgrosswt').val(0);
			tagautoimage();
			if($('#tagautoimage').val() == 1) {	
				$('#imgcapture').trigger('click');
			}
			if(hidproductaddoninfo == 1) {
				$("#productaddoninfoid").select2('val',1).trigger('change');
			}
 			Materialize.updateTextFields();
		});
		// product changes //
		$("#itemcaratweight").change(function() {
			$("#grossweight").val(1);
			$("#netweight").val(1);
		});
		//description key tab events
		$("#description").keydown(function (e) {
			if(e.which == 9) { //description key
				var tagtypeid = $('#tagtypeid').val();
				if(checkValue(tagtypeid) == true && tagtypeid != 3) {
					$('#additionalchargetab').trigger('click');
				}
			}	
		});
		$("#s2id_additionalchargeid :input[role=button]").keydown(function (e) {
			if(e.shiftKey && e.keyCode == 9) { //description key
				var tagtypeid = $('#tagtypeid').val();
				if(checkValue(tagtypeid) == true && tagtypeid != 3) { 
					$('#basictab').trigger('click');
					$('#description').focus();	
				}
			}	
		});
	}
	{
		//auto calculate ->grosswt/netweight/stoneweight
		$(".weightcalculate").change(function(event) {		
			var round=$("#weight_round").val(); // To get the value of weight round.
			setzero(['grossweight','stoneweight','netweight']);			
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			var regxg =/^(\d*\.?\d*)$/;
			var stocknetweight = $('#stocknetweight').val();
			var stockgrossweight = $('#stockgrossweight').val();
			var lotweightlimit = $('#lotweightlimit').val();
			var pendinglotnetwt = $('#pendinglotnetwt').val();
			var finallotlimit = parseFloat(lotweightlimit) + parseFloat(pendinglotnetwt);
			var stockpieces = $('#stockpieces').val();
			var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
			var lotid = $.trim($('#lotid').find('option:selected').val());
			var stone = $("#productid").find('option:selected').data('stone');
			var stonevalcetype = $("#productid").find('option:selected').data('stonevalcetype');
			var metalid = $("#purityid").find('option:selected').data('metalid');
			if(type == 'GWT') {
				var grosswt = $('#'+fieldid+'').val();
				grosswt = grosswt.replace(/[^0-9\.]/g, '');
				parseFloat($('#'+fieldid+'').val(grosswt)).toFixed(round);
				/* if(tag_entrytype == 4) { // check validation while use lot end weight
				   if(lotstatus == 'YES' && lotid != 1 && lotid != ''){
					 var finallotweightresult = checklotweightlimit();
					 if(finallotweightresult == 0)
					 {
					   $('#grossweight').val(0);
					   alertpopup('You should not exceed lot maximum closing weight');
						return false;
					 } 
				   }
				}
				if(tag_entrytype == 3 ||  tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12){
					 if(parseFloat(grosswt) > parseFloat(stockgrossweight))
					 {
						alertpopup('Stock Available only '+stockgrossweight+' gms. Kindly Enter equal or less than '+stockgrossweight+' gms ');
						$(".weightcalculate").val('');
						return false;
					 }
				 }  */
				if(lotstatus == 'YES' && lotid != 1 && lotid != '') {
					var finallotweightresult = checklotweightlimit();
					if(finallotweightresult == 0) {
						$('#grossweight').val(0);
						alertpopup('You should not exceed lot maximum closing weight');
						return false;
					}
				}
				if (!regxg.test(grosswt)) {
				} else {
					$("#packwtlessgrosswt").val(grosswt);
					// pack wt less from gross wt
					var packwtless = $.trim($('#packwtless').find('option:selected').val());
					var packingweight = $.trim($('#packingweight').val());
					if(packwtless == 'Yes') {
						grosswt = parseFloat(grosswt) - parseFloat(packingweight);
						$("#grossweight").val(grosswt);
					}						  
					  /* if (grosswt.toString().indexOf('.') != -1) {
							var substr = grosswt.split('.');
							var roundlength=substr[1].length; 
						}else{
							var roundlength=0;
						}
						if(round < roundlength)
						{
							return false;
						} */
					var stonewt = $('#stoneweight').val();
					if(stone == 'Yes') { //if stone applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var netwt = (parseFloat(grosswt)).toFixed(round);
						} else { // reduce the stone
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						}
						var thisval=parseFloat($("#grossweight").val()).toFixed(round);
							$("#grossweight").val(thisval);
							$('#stoneweight').val(parseFloat(stonewt).toFixed(round));
							$('#netweight').val(netwt);
					} else { //if stone not applicable for this product  do the following
						var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						var thisval=parseFloat($("#grossweight").val()).toFixed(round);
						$("#grossweight").val(thisval);
						$('#stoneweight').val(parseFloat(stonewt).toFixed(round));
						$('#netweight').val(netwt);
					}
					Materialize.updateTextFields();
				}
			} else if(type == 'CTWT') {
				var caratwt = $('#'+fieldid+'').val();
				caratwt = caratwt.replace(/[^0-9\.]/g, '');
				parseFloat($('#'+fieldid+'').val(caratwt)).toFixed(dia_weight_round);
				if(lotstatus == 'YES' && lotid != 1 && lotid != '') {
					var finallotweightresult = checklotweightlimit();
					if(finallotweightresult == 0) {
						$('#itemcaratweight').val(0);
						alertpopup('You should not exceed lot maximum closing weight');
						return false;
					}
				}
			} else if(type == 'SWT') {
				var grosswt = $('#grossweight').val();
				var stonewt = $('#'+fieldid+'').val();
				if (!regxg.test(stonewt)) {
				} else {
					if (stonewt.toString().indexOf('.') != -1) {
						var substr = stonewt.split('.');
						var roundlength=substr[1].length; 
					} else {
						var roundlength=0;
					}
					if(round < roundlength) {
						return false;
					}
					/* if(tag_entrytype == 4) { // check validation while use lot end weight
					   if(lotstatus == 'YES' && lotid != 1 && lotid != ''){
						   var finallotweightresult = checklotstoneweightlimit();
							if(finallotweightresult == 0)
						 {
							 $('#stoneweight').val(0).trigger('change');
							 cleargriddata('stoneentrygrid');
							getstonegridsummary();
						   alertpopup('You should not exceed lot maximum closing weight');
							return false;
						 } 
					   }
					} */
					if(lotstatus == 'YES' && lotid != 1 && lotid != '') {
						var finallotweightresult = checklotstoneweightlimit();
						if(finallotweightresult == 0) {
							$('#stoneweight').val(0).trigger('change');
							cleargriddata('stoneentrygrid');
							getstonegridsummary();
							alertpopup('You should not exceed lot maximum closing weight');
							return false;
						}
					}
					if(stone == 'Yes') { //if stone applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var netwt = (parseFloat(grosswt)).toFixed(round);
						} else { // reduce the stone
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						}
						var thisval = parseFloat($("#stoneweight").val()).toFixed(round);
						$('#stoneweight').val(thisval);
						$('#netweight').val(netwt);
					} else { //if stone not applicable for this product  do the following
						var thisval = parseFloat($("#stoneweight").val()).toFixed(round);
						var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						$('#stoneweight').val(thisval);
						$('#netweight').val(netwt);
					}
					Materialize.updateTextFields();
				}
			} else if(type == 'NWT') {
				var grosswt = $('#grossweight').val();
				var netwt = $('#netweight').val();
				if(lotstatus == 'YES' && lotid != 1 && lotid != '') {
					var finallotweightresult = checklotnetweightlimit();
					if(finallotweightresult == 0) {
						$('#grossweight').val(0);
						$('#netweight').val(0);
						alertpopup('You should not exceed lot maximum closing weight');
						return false;
					}
				}
				/* if(tag_entrytype == 3 ||  tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12){
					 if(parseFloat(netwt) > parseFloat(stocknetweight))
					 {
						alertpopup('Stock Available only '+stocknetweight+' gms. Kindly Enter equal or less than '+stocknetweight+' gms ');
						$(".weightcalculate").val('');
						return false;
					 }
				} */
				if (!regxg.test(netwt)) {
				} else {
					if (netwt.toString().indexOf('.') != -1) {
						var substr = netwt.split('.');
						var roundlength=substr[1].length; 
					} else {
						var roundlength=0;
					}
					if(round < roundlength) {
						return false;
					}
					if(stone == 'Yes') { //if stone not applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var stonewt = (parseFloat(grosswt)).toFixed(round);
						} else { //reduce the stone
							var stonewt = (parseFloat(grosswt)-parseFloat(netwt)).toFixed(round);
						}
						var thisval = parseFloat($("#netweight").val()).toFixed(round);
						$('#stoneweight').val(stonewt);
						$('#netweight').val(thisval);
					} else { //if stone not applicable for this product  do the following
						var thisval = parseFloat($("#netweight").val()).toFixed(round);
						var stonewt = (parseFloat(grosswt)-parseFloat(netwt)).toFixed(round);
						$('#stoneweight').val(stonewt);
						$('#netweight').val(thisval); 
					}
					Materialize.updateTextFields();
				}
			}
			var mode = $('#operationmode').val();
			var tagtypeid = $('#tagtypeid').find('option:selected').val();
			if(mode == 'add'){
				if(tagtypeid != 3) { //dont load auto-productcharge for untag(nontag)
					var data = [];
					data['productid'] = $('#productid').val();
					data['purityid'] = $('#purityid').val();
					data['weight'] = $('#netweight').val();
					data['moduleid'] = 	12;				
					Materialize.updateTextFields();
				}
			}
			setTimeout(function() {
				if(vacchargeshow == 'YES') {
					var netwt = $('#netweight').val();
					var chargeaccountgroup = $('#chargeaccountgroup').val();
					var productid = $('#productid').find('option:selected').val();
					var purityid = $('#purityid').find('option:selected').val();
					if(tagtypeid != 3) { //dont load auto-productcharge for untag(nontag)
						loadproductcharge(purityid,productid,chargeaccountgroup,netwt);
					}
				}
			},50);
		});
	}
	{// Tool Bar click function
		//Add
		$("#addicon").click(function() {
			$('#operationmode').val('add');
			resetFields();
			$('#primarydataid').val('');
			addslideup('itemtaggriddisplay', 'itemtagaddformdiv');
			$(".ftab").trigger('click');
			showhideiconsfun('addclick','itemtagaddformdiv');
			$(".updatebtnclass,.editformsummmarybtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formclearicon").removeClass('hidedisplay');
			$("#dataupdatesubbtn").attr('data-cuopactive','inactive');
			$("#dataaddsbtn").attr('data-cuopactive','active');
			$('#stoneweighticon').show();
			$('#dataupdatesubbtn').addClass('hidedisplay');;
			$('#imagediv,#take-snapshot,#savecapturedimg,#resetcapturedimg').removeClass('hidedisplay');
			$(".documentslogodownloadclsbtn").show();
			getcurrentsytemdate('date');
			loadcurrentpurchaselot('');
			$('#lotid,#tagtypeid,#pieces,#purityid,#rate,#melting,#touch,#description,#branchid,#fromcounterid,#processproductid').prop('disabled',false);
			if(jQuery.inArray( userroleid, itemtagauthrole )!= -1) {   //Userrole who has having permission to edit
				enabledisableuserformfieds();
				$("#date").prop('disabled',true);
			} else {
				$("#date,#tagweight,#packingweight,#packwtless,#chargefrom").prop('disabled',true);
			}
			tagautoimage();
			var template = $('#defaultprinttemplate').val();
			if(template == '' || template == 'null') {
				$('#tagtemplateid').select2('val',1).trigger('change');
			} else {
				$('#tagtemplateid').select2('val',template).trigger('change');
			}
			select2ddenabledisable('tagtypeid','enable');
			select2ddenabledisable('tagentrytypeid','enable');
			$('#tagentrytypeid').select2('val',4).trigger('change');
			tagtype_value=$('#tagtype').val(); 
			$('#tagtypeid').select2('val',tagtype_value).trigger('change');
			if(lotstatus == 'YES') {
				$('#lotid').select2('focus');
			} else {
				$('#purityid').select2('focus');
			}
			$('#tagimage,#itemrate,#itemratewithgst').val('');
			$('#pendinglotstonewt,#packwtlessgrosswt').val(0);
			$('#tagimagedisplay').empty();
			//setTimeout(function(){
				cleargriddata('stoneentrygrid');
				getstonegridsummary();
				$('#scaleoption').select2('val',autoweight);
				$("#branchid").select2('val',loggedinbranchid);
				Materialize.updateTextFields();
			//},100);
			stonehideshow();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
			itemeditstatus = 0;
			$('.branchiddiv').addClass('hidedisplay');
			$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
			$("#itemtagform .chargedetailsdiv .chargedata").val(0);
			purityshowhide('purityid','');
			imagetabhideshow();
			setTimeout(function() {
				getlatitemtagno('add');
			},100);
			$('#itemrate,#itemratewithgst').prop('disabled',false);
			$('#stoneweighticon').show();
			$('#grossweight,#stoneweight,#netweight').prop('disabled',false);
			select2ddenabledisable('productid','enable');
			select2ddenabledisable('purityid','enable');
			select2ddenabledisable('lotid','enable');
			select2ddenabledisable('accountid','enable');
			returnstatus = 0;
			if(hidproductaddoninfo == 1) {
				$("#productaddoninfoid").select2('val',1).trigger('change');
				$('#productaddoninfoid').prop('disabled',false);
			}
			if(itemtagvendorvalidate == 0) {
				$('#accountid').attr('data-validation-engine','');
			}
		});
		//detailed view
		$("#detailedviewicon").click(function() {
			resetFields();			
			var datarowid =  $('#itemtaggrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#pieces').prop('disabled',true);
				$('#stoneweighticon').hide();
				$('#dataupdatesubbtn').addClass('hidedisplay');
				allfieldsdisable();
				$('#imagediv,#take-snapshot,#savecapturedimg,#resetcapturedimg').addClass('hidedisplay');
				//Function Call For Edit
				detailedstatus = 1;
				edititemtag();
				showhideiconsfun('summryclick','itemtagaddformdiv');
				setTimeout(function() {
					$('#productid').attr('disabled',true);
				},100);
				imagetabhideshow();
				if(itemtagvendorvalidate == 0) {
					$('#accountid').attr('data-validation-engine','');
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Edit
		$("#editicon").click(function() {
			var id = $('#itemtaggrid div.gridcontent div.active').attr('id');
			$.ajax({
				url:base_url+"Itemtag/checkordertag",
				data: "itemtagid="+id,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == "SUCCESS") {
						resetFields();
						stonehideshow();
						$('#imagediv,#take-snapshot,#savecapturedimg,#resetcapturedimg').removeClass('hidedisplay');
						$(".documentslogodownloadclsbtn").show();
						showhideiconsfun('editclick','itemtagform');
						allfieldsdisable();
						itemeditstatus = 1;
						edititemtag();
						imagetabhideshow();
						if(itemtagvendorvalidate == 0) {
							$('#accountid').attr('data-validation-engine','');
						}
						Operation = 1; //for pagination
					} else {
						alertpopup(nmsg);
					}
				}
			});
		});
		//Reload
		$("#reloadicon").click(function() {
			itemtagrefreshgrid();
		});
		// Clear the form 
		$('#formclearicon').click(function() {
			resetFields();
			setTimeout(function() {
				getcurrentsytemdate('date');
			},100);
		});
		//Edit Detailed
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','itemtagaddformdiv');
			select2ddenabledisable('lotid','disable');
			select2ddenabledisable('tagtypeid','disable');
			select2ddenabledisable('purityid','disable');
			select2ddenabledisable('tagentrytypeid','disable');
			$('#pieces').prop('disabled',true);
			var tagtype = $('#tagtypeid').val();
			$('#tagtypeid').select2('val',tagtype).trigger('change');
			allfieldsdisable();
			$('#dataupdatesubbtn').removeClass('hidedisplay');
			$('#imagediv,#take-snapshot,#savecapturedimg,#resetcapturedimg').removeClass('hidedisplay');
		    $(".documentslogodownloadclsbtn").show();
		});
		//Delete
		$("#deleteicon").click(function() {
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$.ajax({
					url:base_url+"Itemtag/checkordertag",
					data: "itemtagid=" +datarowid,
					type: "POST",
					async:false,
					cache:false,
					success: function(msg) {
						var nmsg =  $.trim(msg);
						if (nmsg == "SUCCESS") {
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						} else {
							alertpopup(nmsg);
						}
					}
				});
			} else {
				alertpopup(selectrowalert);
			}	
		});
		//order transfers
		$("#ordericon").click(function() {
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var result = orderitem(datarowid);
				if(result == 'Yes') {
					$('#orderstatus,#ordernumber').select2('val','');
					loadordernumber(datarowid);
					$('#tagorderoverlay').fadeIn();
					$('#s2id_orderstatus').select2('focus');
				} else {
					alertpopup('Only tag type can be ordered.');
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		//order submit event		
		 $("#tagordersubmit").click(function() {
			var orderstatus =  $('#orderstatus').val();
			if(orderstatus == 'Yes') {
				if(checkVariable('ordernumber') == true) {
					tagordersubmit();
				} else {
					alertpopup('Please select order number');
				}
			} else if(orderstatus == 'No') {
					tagordersubmit();
			} else {
				alertpopup('Please select order status');
			}			
		});
		function tagordersubmit() {
			var orderstatus =  $('#orderstatus').val();				
			var ordernumber =  $('#ordernumber').val();				
			var salesdetailid = $('#ordernumber').find('option:selected').data('salesdetailid');
			var salesid = $('#ordernumber').find('option:selected').data('salesid');
			var tagid = $('#itemtaggrid div.gridcontent div.active').attr('id');				
			//update the tag/order records
			$.ajax({
				url: base_url+"sales/updatetagorder",
				data:{ordernumber:ordernumber,orderstatus:orderstatus,salesdetailid:salesdetailid,salesid:salesid,tagid:tagid},
				async:false,
				success: function(msg) {
					$('#tagorderoverlay').fadeOut();
					alertpopup('Done');
				},
			});				
		} 
		//order fadeout close
		 $("#tagorderclose").click(function() {
			$('#tagorderoverlay').fadeOut();
		}); 
		//itemtag yes delete
		$("#basedeleteyes").click(function(){
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id'); 
			$.ajax({
				url: base_url + "Itemtag/itemtagdelete?primaryid="+datarowid,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					itemtagrefreshgrid();
					$("#basedeleteoverlay").fadeOut();
					if (nmsg == "SUCCESS") {				
						alertpopup(deletealert);
					} else {
						alertpopup(msg);
					}				
				},
			});
		});
		//itemtag reprint
		$("#printicon").click(function() {
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id'); 
			if(datarowid) {
				$.ajax({
					url: base_url + "Itemtag/itemtagreprint?primaryid="+datarowid,
					success: function(msg)  {		
						var nmsg =  $.trim(msg);
						if (nmsg == 'DBERROR') {
							alertpopup('Error on template query!!!Please choose other template!!!');
						} else if(nmsg == 'SUCCESS') {
							alertpopup('Processed');
						} else {
							alertpopup(msg);
						}
					},
				});
			} else {
				alertpopup(selectrowalert);
			}
		});
		if(grossweighttrigger == '1') {
			//key-press ,add events
			/* $('#rfidtagno').keypress(function(e) {
				if (e.which == 13) {
					var updatebtn = $("#dataupdatesubbtn").attr('data-cuopactive');
					var addbtn = $("#dataaddsbtn").attr('data-cuopactive');
					if(addbtn == 'active') {
						setTimeout(function() {
							$('#dataaddsbtn').trigger('click');
						},50);
					} else if(updatebtn == 'active') {
						setTimeout(function() {
							$('#dataupdatesubbtn').trigger('click');
						},50);
					} else {
						alertpopup('Click Save button on top');
					}
				}
			}); */
			$('#pieces').keypress(function(e) {
				if (e.which == 13) {
					var updatebtn = $("#dataupdatesubbtn").attr('data-cuopactive');
					var addbtn = $("#dataaddsbtn").attr('data-cuopactive');
					if(addbtn == 'active') {
						setTimeout(function() {
							$('#dataaddsbtn').trigger('click');
						},50);
					} else if(updatebtn == 'active') {
						setTimeout(function(){
							$('#dataupdatesubbtn').trigger('click');
						},50);
					} else {
						alertpopup('Click Save button on top');
					}
				}
			});
		}
		$('#pieces').change(function() {
			var lotid = $.trim($('#lotid').find('option:selected').val());
			if(lotstatus == 'YES' && lotid != 1 && lotid != ''){
				var limitresult = checklotpiecelimit();
				if(limitresult == 0) {
					$('#pieces').val(0);
					alertpopup('You should not exceed lot maximum pieces');
					return false;
				}
			}
		});
		//additional-charge refresh-form
		$("#tagadditionalchargerefresh").click(function(){
			addchargeoperation = 'ADD';
			$('#tageditcharge').hide();
			$('#tagaddcharge').show();
			clearform('tagadditionalchargeclear');
			Materialize.updateTextFields();
		});
	}
	{//**Validation ADD/Edit**//
		$('#dataaddsbtn').click(function() {
			$("#addtagvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		//add	
		jQuery("#addtagvalidate").validationEngine({
			onSuccess: function() {
				if($('#tagautoimage').val() == 1) {  // auto tag image
					$('#take-snapshot').trigger('click');
					additemtag();
				} else {
					additemtag();
				}
			},
			onFailure: function() {
				var counterval=$('#counterval').val();
				if(counterval == 'YES') {
					var dropdownid =['4','tagtypeid','purityid','productid','counterid'];
					dropdownfailureerror(dropdownid);
				} else {
					var dropdownid =['3','tagtypeid','purityid','productid'];
					dropdownfailureerror(dropdownid);
				}
				alertpopup(validationalert);
				$('#basictab').trigger('click');
			}
		});
		$('#dataupdatesubbtn').click(function() 
		{
			$("#edittagvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		//edit	
		jQuery("#edittagvalidate").validationEngine({
			onSuccess: function() {
				updateitemtag();
			},
			onFailure: function() {
				alertpopup(validationalert);
			}
		});
	}
	{//date
		//for date picker
		$('#date').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate: null,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#date').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
	{// File upload				
		fileuploadmoname = 'tagfileupload';
		$('#tagimagedisplay').empty();
		var companysettings = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "png,jpg,jpeg,bmp,gif",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) {
				if(data != 'Size') {
					var snarr =$.parseJSON(data);
					var arr = data.split(',');
					var name = $('.dyimageupload').data('imgattr');
					if($('#tagimage').val() == '') {
						$('#tagimage').val(snarr.path);
					} else {
						$('#tagimage').val(snarr.path+','+$('#tagimage').val());
					}
					$('#tagimagedisplay').append('<div data-imgpath="'+snarr.path+'"><img id="taglogodynamic" style="height:100%;width:100%" src="'+base_url+snarr.path+'"><i class="material-icons documentslogodownloadclsbtn" id="removeimageusingcloseicon" data-imagepath="'+snarr.path+'">close</i></div>');
					alertpopupdouble('Your file is uploaded successfully.');	
					$('#savecapturedimg').hide();		
				} else {
					alertpopupdouble('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$(document).on("click","#removeimageusingcloseicon",function() {
			var imgloc = $(this).closest('div').attr('data-imgpath');
			var imagewithloc = imgloc.replace(base_url,'');
			if(imagewithloc != '') {
				$(this).closest('div').remove();
				var tagimage = $('#tagimage').val();
				tagimage = tagimage.replace(imgloc,''); 
				$('#tagimage').val(tagimage.replace(/^,|,$/g,''));
				$.ajax({
					url: base_url +"Itemtag/deleteimagefromfolder",
					data:{imagewithloc:imagewithloc},
					type: "POST",
					success: function(data) {
						if(data == 'SUCCESS') {
							alertpopup('Image was removed from System.');
						} else {
							alertpopup('Unable to remove images from system. Kindly try again later!');
						}
					}
				});
			} else {
				alertpopup('No image to be remove.');
			}
		});
		$("#taglogomulitplefileuploader").uploadFile(companysettings);
		
		//file upload drop down change function
		$("#tagfileuploadfromid").click(function() {
			$(".triggeruploadtagfileupload:last").trigger('click');
			$('#savecapturedimg').hide();
		});
		
		$("#resetcapturedimg").click(function() {
			//var imgloc = $('#tagimagedisplay').find('img').attr('src');
			var imgloc = $('#tagimage').val();
			var imagewithloc = '';			
			$("#imgurldata").val('');				
			$("#tagimagedisplay").empty();
			$("#savecapturedimg").show();
			$('#tagimage').val('');
			if(imgloc != '') {
				imagewithloc = imgloc.replace(base_url,'');
			} else {
				imagewithloc = '';
			}
			if(imagewithloc != '') {
				$.ajax({
					url: base_url +"Itemtag/deleteimagefromfolder",
					data:{imagewithloc:imagewithloc},
					type: "POST",
					success: function(data) {
						if(data == 'SUCCESS') {
							alertpopup('Image was removed from System.');
						} else {
							alertpopup('Unable to remove images from system. Kindly try again later!');
						}
					}
				});
			} else {
				alertpopup('No image to be remove.');
			}
		});
	}
	{//*image upload manage*//
		$("#imgcapture").click(function() {
			$("#taglivedisplay").empty();
			$("#tagimagedisplay").empty();
			$('#savecapturedimg').show();
			addscreencapture();
		});
		
		$("#imagecaptureclose").click(function(){
			$("#imagecaptureoverlay").fadeOut();
		});
		function addscreencapture() {
			// Web cam capture Add screen function			
			var sayCheese = new SayCheese('#taglivedisplay', {video: true});
			sayCheese.start();
			$('#take-snapshot').click(function() {
				var width = 0, height = 0;
				sayCheese.takeSnapshot(width, height);				
				$("#savecapturedimg").show();					
			});
			sayCheese.on('snapshot', function(snapshot) {			  
				var img = document.createElement('img');
				$(img).on('load', function(){
					$("#tagimagedisplay").empty();
					$('#tagimagedisplay').prepend(img);
				});
				img.src = snapshot.toDataURL('image/png');
				$("#imgurldata").val(img.src);
			});
			$("#savecapturedimg").click(function() {
				var dataurlimg =  $("#imgurldata").val();
				setTimeout(function(){ 
					success(dataurlimg);
					$("#savecapturedimg").hide();	
				},10);
			});
			$("#resetcapturedimg").click(function() {			
				$("#imgurldata").val('');				
				$("#tagimagedisplay").empty();
				$("#savecapturedimg").show();				
			});
		}
		{// Webcam add Function
			function success(imageData) {
				var url = base_url+'webcamcapture.php';
				var params = {image: imageData};
				$.post(url, params, function(data) {				
					$("#tagimage").val(data);
				});
			}
			{//image alter function
				function successdelete(imageData) {
					var url = base_url+'webcamcapture.php';
					var params = {image: imageData};
					$.post(url, params, function(data) {
						$(".snapshotcapturebtn").removeClass('hidedisplay');
						$("#imagecaptureoverlay").fadeOut();
						$("#resultbtnset").addClass('hidedisplay');
						$("#delnewimgname").val(data);
						savecapturedproimgbtn();
					});
				}
			}
		}
	}
	{// View by drop down change
        $('#dynamicdddataview').change(function(){
			itemtaggrid();
        });
    }
	{//redirection from(reports/auditlog/widgets) 
		var uniquelreportsession = sessionStorage.getItem("reportunique12");		
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() {
				itemtagdataretrieve(uniquelreportsession);
				showhideiconsfun('summryclick','itemtagform');	
				sessionStorage.removeItem("reportunique12");
			},1000);
		}
	}
	// tag transfer - madasamy
	{
		//for date picker
		var dateformetdata = $('#transferdate').attr('data-dateformater');
		$('#transferdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: 0,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#transferdate').focus();
			}
		});
		/* $('#transferdate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#transferdate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		}); */
		$('#transfericon').click(function() {
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var tagtype = getgridcolvalue('itemtaggrid',datarowid,'tagtypename','');
				if(tagtype == 'Tag' || tagtype == 'Bulk Tag' || tagtype == 'Price Tag') {
					$('#tagtransferoverlay').fadeIn();
					$('#s2id_transferto').select2('focus');
					$('#tagnumber').val('');
					getcurrentsytemdate('transferdate');
					setTimeout(function() {
						$('#transferto').select2('val',14).trigger('change');
						var tagnumber = getgridcolvalue('itemtaggrid',datarowid,'itemtagnumber','');
						$('#tagnumber').val(tagnumber).trigger('change');
						Materialize.updateTextFields();
					},100);
				} else {
					alertpopup('Please select tag type only');
					return false;
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		$('#tagnumber').change(function() {
			gettagdata();
		});
		$('#closetagtransferoverlay').click(function() {
			clearform('tagtransfer');
			$('#tagtransferoverlay').fadeOut();
			$("#tocounter").select2('val','');
			$("#tocounter").prop("disabled", false);
			itemtagrefreshgrid();
		});
		//tag transfer submit button
		$('#addtranfertagbtn').click(function() {
			$("#tagtransfervalidate").validationEngine('validate');
		});
		jQuery("#tagtransfervalidate").validationEngine({
			onSuccess: function() {
				itemtagtotransfer();
			},
			onFailure: function() {
			}
		});
		$("#tagtransferrefreshicon").click(function(){
			resetFields();
			$("#tocounter").select2('val','');
			$("#tocounter").prop("disabled", false);
			getcurrentsytemdate('transferdate');
        });
	}
	// non tag transfer - madasamy
	{
		$('#nontagtransfericon').click(function() {
			$('#nontagtransferoverlay').fadeIn();
			$('#s2id_nontagtransferto').select2('focus');
			getcurrentsytemdate('nontagtransferdate');
			$('#nontagtransferdate').prop('readonly',true);
			$("#tagentrytype").select2('val',3).trigger('change');
		});
		$('#closenontagtransferoverlay').click(function() {
			$('#nontagtransferoverlay').fadeOut();
			itemtagrefreshgrid();
		});
		$('#nontagtransferrefreshicon').click(function() {
			resetFields();
			select2ddenabledisable('nontagtocounter','enable');
			getcurrentsytemdate('nontagtransferdate');
			$('#nontagproductid_req').removeClass('hidedisplay');
			$('#nontagproductid').attr('data-validation-engine','validate[required]');
		});
		// product changes //
		$("#nontagproductid").change(function() {
			var tag_entrytype = 3;
			var fromcounterid = $('#nontagfromcounterid').find('option:selected').val();
			var branchid = loggedinbranchid;
			var purityid = $('#nontagpurity').find('option:selected').val();
			var productidval = $(this).val();
			var tocounterid = $.trim($("#nontagproductid").find('option:selected').data('counterid'));
			if(tocounterid != '') {
				tocounterhideshow(fromcounterid,tocounterid,'nontagtocounter');
			}
			//getweight(tag_entrytype,fromcounterid,purityid,branchid,productidval);
		});
		//auto calculate nontag ->grosswt/netweight/stoneweight
		$(".nontagweightcalculate").change(function(event) {
			setzero(['nontaggrossweight','nontagnetweight']);
			var round=$("#weight_round").val();			
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			var regxg =/^(\d*\.?\d*)$/;
			var stocknetweight = $('#stocknetweight').val();
			var stockgrossweight = $('#stockgrossweight').val();
			var stockpieces = $('#stockpieces').val();
			if(type == 'GWT') {
				var grosswt = $('#'+fieldid+'').val();
				 /* if(parseFloat(grosswt) > parseFloat(stockgrossweight))
					 {
						alertpopup('Stock Available only '+stockgrossweight+' gms. Kindly Enter equal or less than '+stockgrossweight+' gms ');
						$(".nontagweightcalculate").val('');
						return false;
					 } */
				if (!regxg.test(grosswt)) {
				} else {
					if (grosswt.toString().indexOf('.') != -1) {
						var substr = grosswt.split('.');
						var roundlength=substr[1].length; 
					} else {
						var roundlength=0;
					}
					if(round < roundlength) {
						return false;
					}
					var netwt = (parseFloat(grosswt)).toFixed(round);
					var thisval=parseFloat($("#nontaggrossweight").val()).toFixed(round);
					$("#nontaggrossweight").val(thisval);
					$('#nontagnetweight').val(netwt);
					Materialize.updateTextFields();
				}
			} else if(type == 'NWT') {
				var grosswt = $('#nontaggrossweight').val();
				var netwt = $('#nontagnetweight').val
				/* if(parseFloat(netwt) > parseFloat(stocknetweight)) {
					alertpopup('Stock Available only '+stocknetweight+' gms. Kindly Enter equal or less than '+stocknetweight+' gms ');
					$(".nontagweightcalculate").val('');
					return false;
				} */
				if (!regxg.test(netwt)) {
				} else {
					if (netwt.toString().indexOf('.') != -1) {
						var substr = netwt.split('.');
						var roundlength=substr[1].length; 
					} else {
						var roundlength=0;
					}
					if(round < roundlength) {
						return false;
					}
					var thisval=parseFloat($("#nontagnetweight").val()).toFixed(round);
					var stonewt = parseFloat(grosswt)-parseFloat(netwt);
					$('#nontagnetweight').val(thisval);
					Materialize.updateTextFields();
				}
			}
		});
		$("#tagentrytype").change(function() {
			clearform('untagtransferclearone');
			clearform('untagtransferclear');
			var value = $(this).val();
			$("#nontagtransferto option[label='Other Counter']").removeClass("ddhidedisplay");
			if(checkValue(value) == true) {
				if(value == 3) {
					$("#nontagtransferto option[label='Other Counter']").removeClass("ddhidedisplay");
					$('#nontagproductid_req').removeClass('hidedisplay');
					$('#nontagproductid').attr('data-validation-engine','validate[required]');
					select2ddenabledisable('nontagfromcounterid','enable'); 
				} else {
					$("#nontagtransferto option[label='Other Counter']").addClass("ddhidedisplay");
					$('#nontagproductid_req').addClass('hidedisplay');
					$('#nontagproductid').attr('data-validation-engine','');
					select2ddenabledisable('nontagfromcounterid','disable'); 
				}
			}
		});
		//non-tag transfer submit button
		$('#addtranfernontagbtn').click(function() {
			$("#nontagtransfervalidate").validationEngine('validate');	
		});
		jQuery("#nontagtransfervalidate").validationEngine({
			onSuccess: function() {
				nontagtotransfer();
			},
			onFailure: function() {
				//alertpopup(validationalert);
			}
		});
		$('.nontagtransferdatehide').hide(); //Sapna - arvind changes
		//for date picker
		$('#nontagtransferdate').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate: 0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#nontagtransferdate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
	{//Missing items overlay
		//for date picker
		var dateformetdata = $('#missingitemdate').attr('data-dateformater');
		$('#missingitemdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != '') {
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#missingitemdate').focus();
			}
		});
		$('#missingicon').click(function() {
			var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var tagtype = getgridcolvalue('itemtaggrid',datarowid,'tagtypename','');
				if(tagtype == 'Tag' || tagtype == 'Bulk Tag' || tagtype == 'Price Tag') {
					checkitemtagstatus(datarowid);
				} else {
					alertpopup('Please select tag type only');
					return false;
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Missing items overlay submit button
		$('#addmissingitemform').click(function() {
			$("#missingitemvalidate").validationEngine('validate');	
		});
		jQuery("#missingitemvalidate").validationEngine({
			onSuccess: function() {
				missingitemaddform();
			},
			onFailure: function() {
				//alertpopup(validationalert);
			}
		});
		$('#closemissingitemoverlay').click(function(){
			$('#missingitemoverlay').fadeOut();
		});
	}
	// account group change
	$('#accountgroupid').change(function() {
           var groupname=$.trim($(this).find(':selected').data('name'));
           $('#accountgroupname').val(groupname);
	});
	$('#tagentrytypeid').change(function() {
		tagtypeshowhide();
		$('#fromcounterid,#purityid,#processproductid,#productid,#counterid').select2('val','');
		$('.weightcalculate').val(0);
		$('#lotid').select2('val',1).trigger('change');
		var tag_entrytype = $(this).val();
		if(tag_entrytype == 3 || tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12 || tag_entrytype == 13) {
			$('.fromcounterhide').show();
			$('#fromcounterid').attr('disabled',false);
			$('#fromcounterid').attr('data-validation-engine','validate[required]');
			$('.lotsummary,#lotid-div').addClass('hidedisplay');
			fromstoragehideshow(tag_entrytype);
		} else {
			$('.fromcounterhide').hide();
			$('#fromcounterid').attr('disabled',true);
			$('#fromcounterid').attr('data-validation-engine','');
			if(lotstatus == 'YES') {
				$('.lotsummary,#lotid-div').removeClass('hidedisplay');
			} else {
				$('.lotsummary,#lotid-div').addClass('hidedisplay');
			}
		}
		/* $("#fromcounterid option").removeClass("ddhidedisplay");
		$("#fromcounterid optgroup").removeClass("ddhidedisplay");
		$("#fromcounterid option").prop('disabled',false); */
		if(tag_entrytype == 3) {
			storagehideshow();
		}
		$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
		if(tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12 || tag_entrytype == 13) {
			$('#processproductid-div').removeClass('hidedisplay');
			$('#processproductid').attr('data-validation-engine','validate[required]');
			processproducthideshow(tag_entrytype);
			returnstatus = 0;
			if(tag_entrytype == 10 && salesreturnautomatic == 1) {
				returnstatus = 1;
				returnitemsdetailgrid();
				$("#returnitemsdetailoverlay").fadeIn();
			}
		} else {
			returnstatus = 0;
			$('#processproductid-div').addClass('hidedisplay');
			$('#processproductid').attr('data-validation-engine','');
		}
		if(hidproductaddoninfo == 1) {
			$("#productaddoninfoid").select2('val',1).trigger('change');
		}
	});
	$('#stonedetailsicon').click(function() {
		var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var tagtype = getgridcolvalue('itemtaggrid',datarowid,'tagtypename','');
			if(tagtype == 'Tag' || tagtype == 'Bulk Tag') {
				var tagnumber = getgridcolvalue('itemtaggrid',datarowid,'itemtagnumber','');
				$('#stonetagnumber').val(tagnumber).trigger('change');
				//For Keyboard Shortcut Variables
				viewgridview = 2;
				addformview = 2;
			} else {
				alertpopup('Please select tag type only');
				return false;
			}
		} else{
			alertpopup(selectrowalert);
		}
	});
	$('#stonetagnumber').change(function() {
		var tagnumber = $(this).val();
		if(checkValue(tagnumber) == true ) {
			getstonedetails(tagnumber);
		} else {
			alertpopup('please enter proper tag number');
		}
	});
	// Lot
	$("#loticon").click(function() {
		window.location =base_url+'Lot';
	});
	// Tag template
	$('#tagtemplateicon').click(function() {
		window.location = base_url+'Tagtemplate';
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,itemtaggrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			itemtaggrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	$('#touch').change(function() {
		var val = $('#touch').val();
		var meltingval = $('#melting').val();
		if(parseFloat(val) < parseFloat(meltingval)) {
			alertpopup('Please enter value greater than or equal to melting');
			$('#touch').val('');
		}
	});
	$('#melting').change(function() {
		var val = $('#touch').val();
		var meltingval = $('#melting').val();
		if(val != ''){
			if(parseFloat(val) < parseFloat(meltingval)){ 
				alertpopup('Please enter value less than or equal to touch');
				$('#melting').val('');
			}
		}
	});
	$('#fromcounterid').change(function() {
		var val = $(this).val();
		var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
		/* if(tag_entrytype == 3 || tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12){  // untag & Old jewels,Partial Tag & Sales Return
			var branchid = $('#branchid').find('option:selected').val();
			getpurity(tag_entrytype,val,branchid,'purityid');
		} */
	});
	{ // RFID TAG READ
		$('#startbtn').click(function() {
			randomnumber = Math.random();
			window.location.href = "startrfid:start-reading-tag/"+randomnumber;
		});
		$('#stopbtn').click(function() {
			window.location.href = "stoprfid:stop-reading-tag/"+randomnumber;
		});
	}
	$(".inlinespanedit").click(function() {
		$('#chargeiconoverlay').show();
		$('.chargecloneclass').val($.trim($(this).text()));
		$('.chargecloneclass').data('keyword',$(this).attr('id'));
		Materialize.updateTextFields();
	});
	$("#chargeiconclose").click(function() {
		$('#chargeiconoverlay').hide();
	});
	$(".inlinespanedit").keyup(function() {
		var pieces = $('#pieces').val();
		var netwt = $('#netweight').val();
		var grosswt = $('#grossweight').val();
		var chargevalue = $.trim($(this).text());
		var calfinalvalue = 0;
		if(chargevalue != "") {
			chargevalue = chargevalue;
		} else {
			chargevalue = 0;
		}
		var keyword = $(this).attr('id');
		switch(keyword) {
			case 'HM-FT-MIN':  // HALL MARK FLAT MIN
				calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-FT-MAX':  // HALL MARK FLAT MAX
				calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-PI-MIN':  // HALL MARK PER PIECE MIN
				calfinalvalue=(parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-PI-MAX':  // HALL MARK PER PIECE MAX
				calfinalvalue=(parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
				break;	
			case 'CC-FT-MIN':  // CERTIFICATION  FLAT MIN
				calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'CC-FT-MAX':  // CERTIFICATION  FLAT MAX
				calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
				break;	
			case 'WS-PT-N-MIN':  // WASTAGE PERCENT NET MIN
				calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				break;
			case 'WS-PT-N-MAX':  // WASTAGE PERCENT NET MAX
				calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				break;	
			case 'WS-WT-MIN':  // WASTAGE  FLAT WEIGHT MIN
				calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
				break; 
			case 'WS-WT-MAX':  // WASTAGE  FLAT WEIGHT MAX
				 calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
				break;	
			case 'WS-PT-G-MIN':  // WASTAGE PERCENT GROSS MIN
				calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				break;
			case 'WS-PT-G-MAX':  // WASTAGE PERCENT GROSS MAX
				calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				break;	
			case 'MC-GM-N-MIN':  // MAKING  PER GRAM NET MIN
				calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-GM-N-MAX':  // MAKING  PER GRAM NET MAX
				calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-FT-MIN':  // MAKING  FLAT MIN
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'MC-FT-MAX':  // MAKING  FLAT MAX
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'FC-MIN':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'FC-MAX':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'FC-WT-MIN':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
			break;	
			case 'FC-WT-MAX':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
				break;	
			case 'MC-PI-MIN':  // MAKING  PER PIECE MIN
				calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
				break;
			case 'MC-PI-MAX':  // MAKING  PER PIECE MAX
				calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
				break;				
			case 'MC-GM-G-MIN':  // MAKING  PER GRAM GROSS MIN
				calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-GM-G-MAX':  // MAKING  PER GRAM GROSS MAX
				calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;	
				default:
		}
		if(keyword != '') {
			$("#itemtagform input[keywordcalc="+keyword).val(calfinalvalue);
		}
		Materialize.updateTextFields();
	});
	// charge decimal value  & special character restrict
	$(".chargecloneclass").keypress(function(e) {
		var keyCode = e.which;
		var start = e.target.selectionStart;
		var end = e.target.selectionEnd;
		if($(this).val().substring(start, end) == '') {
			// Not allow special 
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
				var newValue = this.value;
				if(keyCode != 8 && keyCode != 0) {
					if (hasDecimalPlace(newValue, round)) {
						e.preventDefault();
					}
				}
			} else {
				e.preventDefault();
			}
		} else {
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
			} else {
				e.preventDefault();
			}
		}
	});
	$(".chargecloneclass").change(function() {
		var pieces = $('#pieces').val();
		var netwt = $('#netweight').val();
		var grosswt = $('#grossweight').val();
		var chargevalue = $(this).val();
		var calfinalvalue = 0;
		var spanvalue = 0;
		if(chargevalue != "") {
			chargevalue = chargevalue;
		} else {
			chargevalue = 0;
		}
		var keyword = $(this).data('keyword');
		if(keyword == 'WS-PT-N-MIN' || keyword =='WS-PT-N-MAX' || keyword == 'WS-PT-G-MIN' || keyword =='WS-PT-G-MAX') {
			if(chargevalue > 100) {
				alertpopupdouble('Please enter percentage value below 100');
				return false;
			}
		}
		switch(keyword) {
			case 'HM-FT-MIN':  // HALL MARK FLAT MIN
				calfinalvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-FT-MAX':  // HALL MARK FLAT MAX
				calfinalvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-PI-MIN':  // HALL MARK PER PIECE MIN
				calfinalvalue = (parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'HM-PI-MAX':  // HALL MARK PER PIECE MAX
				calfinalvalue = (parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;	
			case 'CC-FT-MIN':  // CERTIFICATION  FLAT MIN
				calfinalvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'CC-FT-MAX':  // CERTIFICATION  FLAT MAX
				calfinalvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;	
			case 'WS-PT-N-MIN':  // WASTAGE PERCENT NET MIN
				calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break;
			case 'WS-PT-N-MAX':  // WASTAGE PERCENT NET MAX
				calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round);
				spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break;	
			case 'WS-WT-MIN':  // WASTAGE  FLAT WEIGHT MIN
				calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
				spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break; 
			case 'WS-WT-MAX':  // WASTAGE  FLAT WEIGHT MAX
				 calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
				 spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break;	
			case 'WS-PT-G-MIN':  // WASTAGE PERCENT GROSS MIN
				calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break;
			case 'WS-PT-G-MAX':  // WASTAGE PERCENT GROSS MAX
				calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
				spanvalue = (parseFloat(chargevalue)).toFixed(round);
				break;	
			case 'MC-GM-N-MIN':  // MAKING  PER GRAM NET MIN
				calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-GM-N-MAX':  // MAKING  PER GRAM NET MAX
				calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-FT-MIN':  // MAKING  FLAT MIN
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;
			case 'MC-FT-MAX':  // MAKING  FLAT MAX
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;
			case 'FC-MIN':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;
			case 'FC-MAX':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;
			case 'FC-WT-MIN':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;	
			case 'FC-WT-MAX':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;	
			case 'MC-PI-MIN':  // MAKING  PER PIECE MIN
				calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;
			case 'MC-PI-MAX':  // MAKING  PER PIECE MAX
				calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
				spanvalue = parseFloat(chargevalue);
				break;				
			case 'MC-GM-G-MIN':  // MAKING  PER GRAM GROSS MIN
				calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-GM-G-MAX':  // MAKING  PER GRAM GROSS MAX
				calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
				spanvalue = (parseFloat(chargevalue)).toFixed(roundamount);
				break;	
				default:
		}
		$("#itemtagform input[keywordcalc="+keyword).val(calfinalvalue);
		$("#itemtagform .chargedetailsdiv span#"+keyword).text(spanvalue);
		$(this).val(spanvalue);
		M.updateTextFields();
	});
	$('.addonchargeiconchange').click(function() {
		var spancharge = $(this).attr('keywordcalc');
		var chargearray = {};
		chargearray = $(this).attr('keywordarray');
		chargearray=chargearray.split(',');
		var spanindex = '';
		var spanmanipulate = '';
		var spanchargevalue = '';
		var spanchargeindex = '';	
		$.each(chargearray, function(key, value) {
			var spankey = value.split(":");
			if(spancharge == spankey[0]) {
				spanindex =key;
			}
		});
		if(spanindex < chargearray.length-1) {
			spanmanipulate = spanindex+1;
			spanchargevalue = chargearray[spanmanipulate].split(":");
			spanchargeindex = spanchargevalue[2].split(".");
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('keywordrange',''+spanchargevalue[2]+'');
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('chargeid',''+spanchargevalue[3]+'');
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv label i[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv span[spankeyword="+spancharge).text(''+spanchargevalue[1]+'');
			$("#itemtagform .chargedetailsdiv span[spankeyword="+spancharge).attr('spankeyword',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv span[id="+spancharge).attr('id',''+spanchargevalue[0]+'');
		} else {
			spanchargevalue = chargearray[0].split(":");
			spanchargeindex = spanchargevalue[2].split(".");
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('keywordrange',''+spanchargevalue[2]+'');
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('chargeid',''+spanchargevalue[3]+'');
			$("#itemtagform .chargedetailsdiv input[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv label i[keywordcalc="+spancharge).attr('keywordcalc',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv span[spankeyword="+spancharge).text(''+spanchargevalue[1]+'');
			$("#itemtagform .chargedetailsdiv span[spankeyword="+spancharge).attr('spankeyword',''+spanchargevalue[0]+'');
			$("#itemtagform .chargedetailsdiv span[id="+spancharge).attr('id',''+spanchargevalue[0]+'');
		}
		var chargevalue = $('#itemtagform .chargedetailsdiv span#'+spanchargevalue[0]).text(); 
		chargecalc(spanchargevalue[2],chargevalue,spanchargevalue[0]);
	});
	$('#nontagfromcounterid').change(function() {
		var val = $(this).val();
		var tag_entrytype = 3;
		//getpurity(tag_entrytype,val,loggedinbranchid,'nontagpurity');
		//nontagtocounterhide(val);
	});
	/* $('#nontagpieces').change(function(){
		var stockpieces = $('#stockpieces').val();
		var nontagpieces = $('#nontagpieces').val();
		if(parseFloat(nontagpieces) > parseFloat(stockpieces))
		 {
			alertpopup('Stock Available only '+stockpieces+' pcs. Kindly Enter equal or less than '+stockpieces+' pcs ');
			$("#nontagpieces").val('');
			return false;
		 }
	}); */
	/* $('#pieces').change(function(){
		var stockpieces = $('#stockpieces').val();
		var pieces = $('#pieces').val();
		var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
		if(tag_entrytype == 3 ||  tag_entrytype == 10 || tag_entrytype == 11 || tag_entrytype == 12){
			if(parseFloat(pieces) > parseFloat(stockpieces))
			 {
				alertpopup('Stock Available only '+stockpieces+' pcs. Kindly Enter equal or less than '+stockpieces+' pcs ');
				$("#pieces").val('');
				return false;
			 }
		}
	}); */
	$('#nontagtocounter').change(function() {
		var defultid = $(this).find('option:selected').data('counterdefaultid');
		if(defultid == 2) {
			$('#nontagprocessproductid-div').removeClass('hidedisplay');
			$('#nontagprocessproductid').attr('data-validation-engine','validate[required]');
			tagtransprocessproducthideshow(defultid,"nontagprocessproductid");
		} else {
			$('#nontagprocessproductid').attr('data-validation-engine','');
			$('#nontagprocessproductid-div').addClass('hidedisplay');
		}
	});
	// tag transfer - to storage change
	$('#tocounter').change(function() {
		var defultid = $(this).find('option:selected').data('counterdefaultid');
		if(defultid == 2 ) {
			$('.tagtranprocessproductdiv').show();
			$('#tagtransprocessproductid').attr('class','validate[required]');
			tagtransprocessproducthideshow(defultid,"tagtransprocessproductid");
		} else {
			$('#tagtransprocessproductid').attr('class','');
			$('.tagtranprocessproductdiv').hide();
		}
	});
	$('#stoneentrycalctypeid').change(function() {
		var typeval = $(this).val();
		$('#stonegram,#caratweight,#totalweight,#stonetotalamount').val(0);
		$('#stonegram,#caratweight,#stonepieces').prop('readonly',false);
		$("span","#stonepiecesdiv label").remove();
		$("span","#stonegramdiv label").remove();
		$("span","#caratweightdiv label").remove();
		$('#stonepieces').val(1);
		setTimeout(function() {
			if(typeval == 4) { // gram wise
				$('#stonegram').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.01]]');
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#caratweight').attr('data-validation-engine','');
				$('#caratweight').removeClass('error');
				$('#caratweight').prop('readonly',true);
				$("span","#caratweightdiv label").remove();
				$('#stonegramdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#caratweightdiv").hide();
				$("#stonegramdiv").show();
				Materialize.updateTextFields();
			} else if(typeval == 2) {  // carat wise
				$('#caratweight').attr('data-validation-engine','validate[required,decval['+dia_weight_round+'],custom[number],min[0.01]]');
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#stonegram').attr('data-validation-engine','');
				$('#stonegram').removeClass('error');
				$('#stonegram').prop('readonly',true);
				$("span","#stonegramdiv label").remove();
				$('#caratweightdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#stonegramdiv").hide();
				$("#caratweightdiv").show();
				Materialize.updateTextFields();
			} else if(typeval == 3) {  // piece wise
				$('#caratweight,#stonegram').attr('data-validation-engine','');
				$('#caratweight,#stonegram').removeClass('error');
				$("span","#caratweightdiv label").remove();
				$('#caratweight').prop('readonly',true);
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#stonepiecesdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#caratweightdiv").hide();
				$("#stonepiecesdiv").show();
				if(stonepiecescalc == '0') {
					$("#stonegramdiv").hide();
					$("span","#stonegramdiv label").remove();
					$('#stonegram').prop('readonly',true);
				} else if(stonepiecescalc == '1') {
					$('#stonegram').attr('data-validation-engine','validate[decval['+round+'],custom[number],min[0]]');
					$('#stonegram').prop('readonly',false);
					$("#stonegramdiv").show();
				}
			}
		},100);
	});
	$('#counterid').change(function() {
		var counterid = $(this).find("option:selected").val();
		if(counterusestatus == 'yes') {
			if(storagequantity == 1) {
				if(counterid == '') {
				} else {
					if(itemeditstatus == 0) {
						checkmaxquantity(counterid);
					} else {
						var editcounterid = $('#editcounterid').val();
						if(counterid != editcounterid) {
							checkmaxquantity(counterid);
						}
					}
				}
			}
		}
	});
	$(".select2-input").keyup(function() {
		if($("#select2-drop.select2-drop-active ul.select2-results li:first").attr('class') == 'select2-no-results') {
			$(".select2-input").val('');
			$("#select2-drop").attr('style','display:none');
		}
	});
	// Process product change
	$('#processproductid').change(function() {
		var processproductid = $(this).find("option:selected").val();
		var tagentrytypeid = $.trim($("#tagentrytypeid").find('option:selected').val());
		var lotid = $('#lotid').find('option:selected').val();
		if(tagentrytypeid == 10 || tagentrytypeid == 12) {
			if(lotid != 0 && lotid != 1 && lotid != '') {
			} else {
				$('#productid').select2('val',processproductid).trigger('change');
			}
		}
	});
	//Kumaresan - Purchase rate and Sales rate round value
	{
		$('#purchaserate').change(function() {
			$("#basicrate").val(0);
		});
		$('#basicrate').change(function() {
			if(parseFloat($('#purchaserate').val()) <= parseFloat($('#basicrate').val())) {
				return true;
			} else {
				$("#basicrate").val('');
				alertpopup('Sales rate should be greater than/equal to Purchase rate');
				return false; 
			}
		});
	}
	/* $("#grossweight").keypress(function(e){
        alert(e.which);
    }); */ 
	{//auto weight popups
		$("#grossweight").focus(function() {
			/* $('#grossweight').trigger(jQuery.Event('keypress', { keycode: 145 }));
			alert(e.keycode); */			
			//$('#grossweight').trigger(jQuery.Event('keypress', { keycode: 145 }));
			var autoweightransfer = $('#scaleoption').find('option:selected').val();
			var roundweight = parseFloat($('#weight_round').val());
			var weightscale = $('#weightscale').val();
			if(weightscale == 'Yes') {			
				$.ajax({
					url:base_url+"weight/weightdata.php?transfer=true",
					async: false,
					success: function(msg) {
						if(msg != '------' || msg != '') {
							var grosswt = msg.replace(/[^0-9\.]/g, '');
							$("#grossweight,#netweight").val(parseFloat(grosswt).toFixed(roundweight));
							//$("#grossweight,#netweight").val(grosswt).toFixed(round);
							//$("#netweight").val(msg.toFixed(roundweight));
							$("#stoneweight").val(0);
							//setTimeout(function(){
								if(vacchargeshow == 'YES') {
									var netwt = $('#netweight').val();
									var chargeaccountgroup = $('#chargeaccountgroup').val();
									var productid = $('#productid').find('option:selected').val();
									var purityid = $('#purityid').find('option:selected').val();
									if(itemeditstatus == 0) {
										loadproductcharge(purityid,productid,chargeaccountgroup,netwt);
									} else {
										$('.inlinespanedit').trigger('keyup');
									}
								}
								//$("#grossweight").prop('disabled',true);
							//},50);
						} else {
							$("#netweight,#stoneweight").val(0);							
						}
					},
				});
			}/* else{
				$("#netweight,#stoneweight").val(0);							
			}  */
			//$('#pieces').focus();
		});
	}
	$("#itemratewithgst").keyup(function() {
		setzero(['itemratewithgst']);
		var withgst = $("#itemratewithgst").val();
		var taxrate = $('#productid').find('option:selected').data('taxratesum');
		var taxrate = parseFloat(taxrate)  + parseFloat(100) ;
		var taxvalue = (parseFloat(withgst)/parseFloat(taxrate));
		var withoutgst = (parseFloat(taxvalue)*100);
		$("#itemrate").val(parseFloat(withoutgst).toFixed());
		Materialize.updateTextFields();
	});
	// package wt less & package wt change
	$("#packwtless,#packingweight").change(function() {
		var round=$("#weight_round").val(); // To get the value of weight round.
		var packwtless = $.trim($('#packwtless').find('option:selected').val());
		var packingweight = $.trim($('#packingweight').val());
		if(packingweight == '') {
			packingweight = 0;
			$(this).val(0);
		}
		var grosswt = $.trim($('#packwtlessgrosswt').val());
		var netwt = 0;
		var stonevalcetype = $("#productid").find('option:selected').data('stonevalcetype');
		if($.trim($('#stoneweight').val()) == '') {
			var stonewt = 0;
		} else {
			var stonewt = $('#stoneweight').val();
		}
		if(packwtless == 'Yes') {
			grosswt = parseFloat(grosswt) - parseFloat(packingweight);
			$("#grossweight").val(parseFloat(grosswt).toFixed(round));
			if(stonevalcetype== 3) {
				netwt = grosswt;
			} else {
				netwt = parseFloat(grosswt) - parseFloat(stonewt);
			}
			$("#netweight").val(parseFloat(netwt).toFixed(round));
		} else if(packwtless == 'No') {
			grosswt = parseFloat(grosswt);
			$("#grossweight").val(parseFloat(grosswt).toFixed(round));
			if(stonevalcetype== 3) {
				netwt = grosswt;
			} else {
				netwt = parseFloat(grosswt) - parseFloat(stonewt);
			}
			$("#netweight").val(parseFloat(netwt).toFixed(round));
		}
		Materialize.updateTextFields();	
	});
	$("#tagweight").change(function() {
		var tagweight = $.trim($('#tagweight').val());
		if(tagweight == '') {
			$(this).val(0);
		}
	});
	{ // Return Item Grid Functionality
		// Return Items Grid Submit
		$("#returnitemsgridsubmit").click(function() {
			var productid = getselectedrowids('returnitemsdetailgrid');
			if(productid != '') {
				var checkedlength = $('#returnitemsdetailgrid .gridcontent div.data-content div input:checked').length;
				if(checkedlength > 1) {
					alertpopupdouble('Please choose the single product details');
					return false;
				} else {
					var stocktype = '';
					var salesdetailid = '';
					$('#returnitemsdetailgrid .gridcontent input:checked').each(function() {
						salesdetailid = $(this).attr('data-rowid');
						stocktype = $('#returnitemsdetailgrid .gridcontent div#'+salesdetailid+' ul .stocktypename-class').text();
					});
					if(stocktype != '' && salesdetailid != '') {
						$('#returnsalesdetailid').val(salesdetailid);
						$("#returnitemsdetailoverlay").fadeOut();
						retrievegriddetailstofrom(salesdetailid,stocktype);
					} else {
						alertpopupdouble('There is something problem. Please try again!');
						return false;
					}
				}
			} else {
				alertpopupdouble('Please select the Items(s)');
			}
		});
		// Return Items grid Close
		$("#returnitemsgridclose").click(function() {
			$("#returnitemsdetailoverlay").fadeOut();
			$("#tagentrytypeid").select2('val',4).trigger('change');
		});
	}
	{ // Automatic Stone Item to Grid Functionality
		// Return Items Grid Submit
		$("#autostonegridsubmit").click(function() {
			var stoneentryid = getselectedrowids('autostonegrid');
			if(stoneentryid != '') {
				var checkedlength = $('#autostonegrid .gridcontent div.data-content div input:checked').length;
				if(checkedlength > 1) {
					alertpopupdouble('Please choose the single product details');
					return false;
				} else {
					$('#autostonegrid .gridcontent input:checked').each(function() {
						salesdetailid = $(this).attr('data-rowid');
						$("#stoneid").select2('val',$('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stoneid-class').text());
						$("#stoneentrycalctypeid").select2('val',$('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stoneentrycalctypeid-class').text()).trigger('change');
						$("#purchaserate").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .purchaserate-class').text());
						$("#basicrate").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .basicrate-class').text());
						$("#stonegram").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonegram-class').text());
						$("#stonepieces").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonepieces-class').text());
						$("#caratweight").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .caratweight-class').text());
						$('#autolotpieces').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonepieces-class').text());
						$('#autolotgramwt').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonegram-class').text());
						$('#autolotcaratwt').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .caratweight-class').text());
					});
					stoneentrycal();
					Materialize.updateTextFields();
					$('#automaticstonedetailoverlay').fadeOut();
				}
			} else {
				alertpopupdouble('Please select the Items(s)');
			}
		});
		// Return Items grid Close
		$("#autostonegridclose").click(function() {
			$('#automaticstonedetailoverlay').fadeOut();
		});
	}
	// RFID Tag No - Change
	$("#rfidtagno").change(function() {
		var rfidtagno = $(this).val();
		rfidtagno = rfidtagno.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '');
		$(this).val(rfidtagno);
		rfitagnocheck();
	});
	// Print Preview
	$('#printpreviewicon').click(function() {
		var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
		var snumber = $('#itemtaggrid div.gridcontent div.active ul li.itemtagnumber-class').text();
		if (datarowid) {
			$('#printpdfid').val(datarowid);
			$('#printsnumber').val(snumber);
			$("#templatepdfoverlay").fadeIn();
			setdefaultprinttemplate(datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
});
{// Item tag View Grid
	function itemtaggrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		if(Operation == 1){
			var rowcount = $('#stockentrypgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#stockentrypgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		}
		var wwidth = $(window).width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.stockentryheadercolsort').hasClass('datasort') ? $('.stockentryheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.stockentryheadercolsort').hasClass('datasort') ? $('.stockentryheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.stockentryheadercolsort').hasClass('datasort') ? $('.stockentryheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = 50;
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=itemtag&primaryid=itemtagid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#itemtaggrid').empty();
				$('#itemtaggrid').append(data.content);
				$('#itemtaggridfooter').empty();
				$('#itemtaggridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('itemtaggrid');
				{//sorting
					$('.stockentryheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.stockentryheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.stockentryheadercolsort').hasClass('datasort') ? $('.stockentryheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.stockentryheadercolsort').hasClass('datasort') ? $('.stockentryheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#itemtaggrid .gridcontent').scrollLeft();
						itemtaggrid(page,rowcount);
						$('#itemtaggrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('stockentryheadercolsort',headcolid,sortord);
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						itemtaggrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stockentrypgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						itemtaggrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
					$('#stockentrypgrowcount').material_select();	
			},
		});
	}
}
{//**CRUD**//
	//Create
	function additemtag() {
		var counterid = $('#counterid').val();
		var fromcounterid = $('#fromcounterid').val();
		var totamt = $.trim($('#totamt').text());
		var tagtypeid = $('#tagtypeid').find('option:selected').val();
		var lotid = $.trim($('#lotid').find('option:selected').val());
		var lstpurityid = $('#productid').find('option:selected').data('purityid');
		if(counterid == fromcounterid) {
			alertpopup('Do not map same storage for From and To storage');
			return false;
		}
		if(lotstatus == 'YES' && lotid != 1 && lotid != '' && lstpurityid != 11) {
			var limitresult = checklotpiecelimit();
			if(limitresult == 0) {
				alertpopup('You should not exceed lot maximum pieces');
				return false;
			}
		}
		var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
		if(lotstatus == 'YES' && lotid != 1 && lotid != '' && lstpurityid != 11 && tagtypeid != 5) {
			var finallotweightresult = checklotweightlimit();
			if(finallotweightresult == 0) {
				alertpopup('You should not exceed lot maximum closing Gross weight');
				return false;
			}
			var finallotweightresult = checklotstoneweightlimit();
			if(finallotweightresult == 0) {
				$('#stoneweight').val(0).trigger('change');
				cleargriddata('stoneentrygrid');
				getstonegridsummary();
				alertpopup('You should not exceed lot maximum closing Stone weight');
				return false;
			}
			var finallotweightresult = checklotnetweightlimit();
			if(finallotweightresult == 0) {
				$('#grossweight').val(0);
				$('#netweight').val(0);
				$('#grossweight').focus();
				alertpopup('You should not exceed lot maximum closing Net weight');
				return false;	 
			}
		}
		// stone product
		var productstatus = $('#productid').find('option:selected').data('stone');
		if(productstatus == 'Yes' && stonewtcheck == 'Yes') {
			if(totamt == 0 && tagtypeid != 3 && tagtypeid != 5){ // untag & price tag
				alertpopup('You should give stone weight for this item');
				return false;
			}
		}
		var pieces = parseFloat($('#pieces').val());
		var form = $("#itemtagform").serialize();
		$('select[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		$('input[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		var chargedetails = new Array();
		var chargeids = new Array();
		var chargevalues = new Array();
		var chargespan = new Array();
		var chargespanvalue = new Array();
		var cond = '';
		var chargestatus = 1;
		var chargeapply = 0;
		$('.chargedetailsdiv:not(.hidedisplay) > .chargedata').each(function() {
			if($.trim($(this).val()) == '') {
				chargestatus = 0;
				return false;
			} else {
				var keyword = $(this).attr('keywordrange');
				var res=keyword.split(".");
				if(chargecalculation == 2) {
					cond = 'MIN';
				} else {
					cond = 'MAX';
				}
				if(res[1] == cond) {
					chargevalues.push($(this).val());
					if($(this).val() != 0 && $(this).val() != '') {
						chargeapply = 1;
					}
					var charagetbid = $(this).attr('id');
					if($(this).val() == 0 && charagetbid != 'fcmin' && charagetbid != 'fcmax') {
						chargestatus = 0;
					}
				}
				chargedetails.push($(this).attr('keywordrange')+':'+$(this).val());
				var spanval = $(this).attr('keywordcalc');
				chargespan.push($(this).attr('keywordrange')+':'+$("#itemtagform .chargedetailsdiv span#"+spanval).text());
				chargespanvalue.push($("#itemtagform .chargedetailsdiv span#"+spanval).text());
				chargeids.push($(this).attr('chargeid'));
			}
		});
		if(chargerequired == 1) { // check validation based on company setting
			if(chargestatus == 0) {
				alertpopup('Please give productcharge for this product');
				return false;
			}
		}
		var res =chargeids.toString()
		var arr = $.unique(res.split(','));
		var chargeiddata = arr.join(",");
		var lotnumber = $('#lotid').find('option:selected').val();
		if(lotnumber != '' || lotnumber != 1 ||  lotnumber != 'undefined') {
			var receiveorderstatus = $('#lotid').find('option:selected').attr('data-receiveorder');
			if(receiveorderstatus == 'Yes') {
				var salesdetailidv = $('#lotid').find('option:selected').attr('data-lotsalesdetailid');
				var salesdetailordernumberv = $('#lotid').find('option:selected').attr('data-lotsalesdetailordernumber');
			} else {
				receiveorderstatus = 'No';
				var salesdetailidv = 1;
				var $salesdetailordernumberv = '';
			}
		} else {
			var receiveorderstatus = 'No';
			var salesdetailidv = 1;
			var salesdetailordernumberv = '';
		}
		var previouscharge = $('#previouscharge').val();
		var amp = '&'; 
		var datainformation = amp + form;
		var sizeid = 1;
		if(tagtypeid == 3) {
			sizeid = 1;
		}else {
			if($.trim($('#size').find('option:selected').val()) == '') {
				sizeid = 1;
			} else {
				sizeid = $('#size').find('option:selected').val();
			}
		}
		$('#processoverlay').show();
		var chargeapply = $('#chargefrom').find('option:selected').val();
		if($('#tagautoimage').val() == 1) {
			if($('#mtagautoimage').val() == 1) {
				var tagautoimage = $("#tagautoimage").val();
			}
		} else {
			var tagautoimage = 0;
		}
		var imglocation = '';
		if(hidproductaddoninfo == 1) {
			var productaddoninfoid = $("#productaddoninfoid").val();
			if(productaddoninfoid != '' || productaddoninfoid != null) {
				productaddoninfoid = productaddoninfoid;
			} else {
				productaddoninfoid = 1;
			}
		} else {
			var productaddoninfoid = 1;
		}
		var categoryid = $('#productid').find('option:selected').data('categoryid');
		categoryid = typeof categoryid == 'undefined' ? 1 : categoryid;
		var returnsalesdetailid = $('#returnsalesdetailid').val();
		returnsalesdetailid = typeof returnsalesdetailid == 'undefined' ? 1 : returnsalesdetailid;
		$.ajax({
			url: base_url +"Itemtag/itemtagcreate",
			data: "datas=" + datainformation+amp+"chargedata="+chargedetails+amp+"chargeiddata="+chargeiddata+amp+"chargevalues="+chargevalues+amp+"chargespan="+chargespan+amp+"previouscharge="+previouscharge+amp+"chargespanvalue="+chargespanvalue+amp+"sizeid="+sizeid+amp+"chargeapply="+chargeapply+amp+"tagautoimage="+tagautoimage+amp+"receiveorderstatus="+receiveorderstatus+amp+"salesdetailidv="+salesdetailidv+amp+"salesdetailordernumberv="+salesdetailordernumberv+amp+"productaddoninfoid="+productaddoninfoid+amp+"categoryid="+categoryid+"&returnsalesdetailid="+returnsalesdetailid,
			type: "POST",
			dataType:'json',
			success: function(msg) {
				var nmsg =  $.trim(msg);
				var netweight =  parseFloat($('#netweight').val());
				var grossweight = parseFloat($('#grossweight').val());
				var stoneweight = parseFloat($('#stoneweight').val());
				var caratweight = parseFloat($('#itemcaratweight').val());
				var tagtype = $('#tagtypeid').val();
				$('.tagaddclear').val('');
				$('#description,#itemrate,#itemratewithgst,#hallmarkno,#hallmarkcenter,#certificationno').val('');
				$('#packwtlessgrosswt').val(0);	
				clearform('tagadditionalchargeclear');
				$('#hiddenstonedata').val('');
				getstonegridsummary();
				if(checkVariable('lotid') == true) {
					if(checkValue(lotnumber) == true && lotnumber != 0 && lotnumber != 1) {
						var pendinglotnetwt = parseFloat($('#pendinglotnetwt').val());
						var pendinglotcaratwt = parseFloat($('#pendinglotcaratwt').val());
						var pendinglotgrosswt = parseFloat($('#pendinglotgrosswt').val());
						var pendinglotpieces = parseFloat($('#pendinglotpieces').val());
						var completedlotgrosswt = parseFloat($('#completedlotgrosswt').val());
						var completedlotnetwt = parseFloat($('#completedlotnetwt').val());
						var completedlotcaratwt = parseFloat($('#completedlotcaratwt').val());
						var completedlotpieces = parseFloat($('#completedlotpieces').val());
						var pendinglotstonewt = parseFloat($('#pendinglotstonewt').val());
						
						if(netweight > 0 && grossweight > 0 && lstpurityid != 11) {
							//calculate pending weight
							if(tagtype == 4) { //bulk tag methods
								$('#pendinglotnetwt').val((pendinglotnetwt-(netweight*pieces)).toFixed(round));
								$('#pendinglotgrosswt').val((pendinglotgrosswt-(grossweight*pieces)).toFixed(round));
								$('#pendinglotpieces').val(pendinglotpieces-pieces);
								$('#completedlotgrosswt').val((completedlotgrosswt+(grossweight*pieces)).toFixed(round)); 
								$('#completedlotnetwt').val((completedlotnetwt+(netweight*pieces)).toFixed(round));
								$('#completedlotpieces').val(completedlotpieces+pieces);
							} else {
								$('#pendinglotnetwt').val((pendinglotnetwt-netweight).toFixed(round));
								$('#pendinglotgrosswt').val((pendinglotgrosswt-grossweight).toFixed(round));
								$('#pendinglotpieces').val(pendinglotpieces-pieces);
								$('#completedlotgrosswt').val((completedlotgrosswt+grossweight).toFixed(round)); 
								$('#completedlotnetwt').val((completedlotnetwt+netweight).toFixed(round));
								$('#completedlotpieces').val(completedlotpieces+pieces);
							}
							$('#pendinglotstonewt').val(parseFloat(pendinglotstonewt-stoneweight).toFixed(round));
							$('.documentslogodownloadclsbtn').trigger('click');			
						} else {
							if(tagtype == 5 || lstpurityid == 11) { //price tag
								if(lstpurityid == 11) {
									$('#pendinglotcaratwt').val((pendinglotcaratwt-caratweight).toFixed(dia_weight_round));
									$('#completedlotcaratwt').val((completedlotcaratwt+caratweight).toFixed(dia_weight_round));
								}
								$('#pendinglotpieces').val(pendinglotpieces-pieces);
								$('#completedlotpieces').val(completedlotpieces+pieces);
							}
							if(lstpurityid == 11) {
								$('#productid').trigger('change');
							}
						}
						if(lotnumber != '' && lotnumber != '1') {
							setTimeout(function(){
								checklotstatus(lotnumber);
							},10);
						}
					}
				}
				if (msg['msg'] == 'DBERROR') {	
					alertpopup('Error on template query!!!Please choose other template!!!');
				} else if(msg['msg'] == 'SUCCESS') {
					if(stocknotify == 'YES') {
						if(msg['tagnumber'] == ''){
							alertpopup(" Untag Stock is added to the stock");
						} else {
							alertpopup("Tag Number: "+msg['tagnumber']+" is added to the stock");
						}
					}
					$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
					/*
					var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
					var fromcounterid = $('#fromcounterid').find('option:selected').val();
					var branchid = $('#branchid').find('option:selected').val();
					var purityid = $('#purityid').find('option:selected').val();
					var productid = $('#productid').find('option:selected').val();
					 if(tag_entrytype == 3 || tag_entrytype == 10 || tag_entrytype == 12){
						getweight(tag_entrytype,fromcounterid,purityid,branchid,productid);
					}
					else if(tag_entrytype == 11)
					{
						getweight(tag_entrytype,fromcounterid,purityid,branchid,2);
					} */
					imglocation = msg['dataurlimg'];
					if($('#tagautoimage').val() == 1) {  // auto tag image
						var dataurlimg =  $("#imgurldata").val();
						var url = base_url+'webcamcapture.php';
						var params = {image: dataurlimg,imgurl: msg['dataurlimg']};
						$.post(url, params, function(data) {				
							$('#processoverlay').hide();
						});	
					} else {
						$('#processoverlay').hide();
					}
					$("#lasttagno").val(msg['itemtagno']);
					$("#lastrfidno").val(msg['rfidno']);
					Materialize.updateTextFields();
					//getlatitemtagno('add');
				} else {
					//getlatitemtagno('add');
					imglocation = msg['dataurlimg'];
					$("#lasttagno").val(msg['itemtagno']);
					$("#lastrfidno").val(msg['rfidno']);
					Materialize.updateTextFields();
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
				$('.ftab').trigger('click');
				var tagtypeid = $("#tagtypeid").val();
				if(tagtypeid == 5) {
					var prizetaggst = $("#prizetaggst").val();
					if(prizetaggst == 0) {
						$('#itemrate').focus();
					} else if(prizetaggst == 1) {
						$('#itemratewithgst').focus();
					}
				} else {
					if(itemtagsavefocus == 0) {
						$('#grossweight').focus();
					} else {
						$('#productid').select2('focus');
					}
				}
				if($("#productid").val() != '') {
					var productpieces = $("#productid").find('option:selected').data('productpieces');
					if(productpieces != '') {
						$('#pieces').val(productpieces);
					} else {
						resetwtps();
					}
				} else {
					resetwtps();
				}
				if(counterusestatus == 'yes') {
					if(storagequantity == 1) {
						$('#counterid').trigger('change');  
					}
				}
				$("#display").hide();
				$('#tagimage,#returnsalesdetailid').val('');
				$('#tagimagedisplay').empty();
				cleargriddata('stoneentrygrid');
				if(returnstatus == 1) {
					returnitemsdetailgrid();
					$("#returnitemsdetailoverlay").fadeIn();
					$('#size,#accountid,#counterid,#fromcounterid').select2('val','').trigger('change');
				}
				Materialize.updateTextFields();
			},
		});
	}
	function updateitemtag() {
		var datarowid = $('#itemtagid').val();
		var form = $("#itemtagform").serialize();
		$('select[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		$('input[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		var amp = '&';
		var datainformation = amp + form; 
		var changelotid=$('#changelotid').val(); 
		var changegrosswt=$('#changegrosswt').val(); 
		var changenetwt=$('#changenetwt').val();
		var chargestatus = 1;
		var chargeapply = 0;
		var chargedetails = new Array();
		var chargeids = new Array();
		var chargevalues = new Array();
		var chargespan = new Array();
		var chargespanvalue = new Array();
		var cond = '';
		$('.chargedetailsdiv:not(.hidedisplay) > .chargedata').each(function() {
			if($.trim($(this).val()) == '') {
				alertpopup('Please give productcharge for this product');
				return false;
			} else {
				var keyword = $(this).attr('keywordrange');
				var res=keyword.split(".");
				if(chargecalculation == 2){
					cond = 'MIN';
				} else {
					cond = 'MAX';
				}
				if(res[1] == cond) {
					chargevalues.push($(this).val());
					if($(this).val() != 0 && $(this).val() != '') {
						chargeapply = 1;
					}
					if($(this).val() == 0) {
						chargestatus = 0;
					}
					var charagetbid = $(this).attr('id');
					if($(this).val() == 0 && charagetbid != 'fcmin' && charagetbid != 'fcmax') {
						chargestatus = 0;
					}
				}
				chargedetails.push($(this).attr('keywordrange')+':'+$(this).val());
				var spanval = $(this).attr('keywordcalc');
				chargespan.push($(this).attr('keywordrange')+':'+$("#itemtagform .chargedetailsdiv span#"+spanval).text());
				chargespanvalue.push($("#itemtagform .chargedetailsdiv span#"+spanval).text());
				chargeids.push($(this).attr('chargeid'));
			}
		});
		var res = chargeids.toString();
		var arr = $.unique(res.split(','));
		var chargeiddata = arr.join(",");
		var previouscharge = $('#previouscharge').val();
		var sizeid = 1;
		var tagtypeid = $('#tagtypeid').find('option:selected').val();
		if(chargerequired == 1) { // check validation based on company setting
			if(chargestatus == 0) {
				alertpopup('Please give productcharge for this product');
				return false;
			}
		}
		if(tagtypeid == 3) {
			sizeid = 1;
		} else {
			if($.trim($('#size').find('option:selected').val()) == '') {
				sizeid = 1;
			} else {
				sizeid = $('#size').find('option:selected').val();
			}
		}
		setTimeout(function() {
			$('#processoverlay').show();
		},25);
		var chargeapply = $('#chargefrom').val(); 
		if(hidproductaddoninfo == 1) {
			var productaddoninfoid = $("#productaddoninfoid").val();
			if(productaddoninfoid != '' || productaddoninfoid != null || productaddoninfoid != undefined) {
				productaddoninfoid = productaddoninfoid;
			}
		}
		if(productaddoninfoid == '' || productaddoninfoid == null ) {
			var productaddoninfoid = 1;
		}
		var categoryid = $('#productid').find('option:selected').data('categoryid');
		categoryid = typeof categoryid == 'undefined' ? 1 : categoryid;
		$.ajax({
			url: base_url +"Itemtag/itemtagupdate",
			data: "datas=" + datainformation+amp+"primaryid="+datarowid+amp+"changelotid="+changelotid+amp+"changegrosswt="+changegrosswt+amp+"changenetwt="+changenetwt+amp+"chargedata="+chargedetails+amp+"chargeiddata="+chargeiddata+amp+"chargevalues="+chargevalues+amp+"chargespan="+chargespan+amp+"previouscharge="+previouscharge+amp+"chargespanvalue="+chargespanvalue+amp+"sizeid="+sizeid+amp+"chargeapply="+chargeapply+amp+"productaddoninfoid="+productaddoninfoid+amp+"categoryid="+categoryid,
			type: "POST",
			success: function(msg) {
				var nmsg =  $.trim(msg);
				$(".ftab").trigger('click');
				resetFields();			
				$('#itemtagaddformdiv').hide();
				$('#itemtaggriddisplay').fadeIn(50);
				itemtagrefreshgrid();	
				$('#hiddenstonedata').val('');
				getstonegridsummary();	
				if(nmsg == 'SUCCESS') {
					$('#processoverlay').hide();
					alertpopup(updatealert);
				} else {				
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
				$("#display").hide();
				cleargriddata('stoneentrygrid');
				$('#returnsalesdetailid').val('');
			},
		});
	}
	function itemtagdataretrieve(datarowid) {
		var elementsname = ['33','itemtagid','date','purityid','accountid','fromcounterid','productid','grossweight','stoneweight','netweight','itemcaratweight','pieces','size','counterid','description','tagimage','primaryitemtagid','tagtemplateid','melting','touch','rate','tagentrytypeid','lotid','rfidtagno','itemrate','branchid','certificationno','hallmarkno','hallmarkcenter','collectionid','guaranteecard','scaleoption','itemratewithgst','tagweight','packingweight','packwtless','chargeapply'];
		$.ajax({
			url:base_url+"Itemtag/itemtagretrive?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			success: function(data) {
				if(data['tagstatusid'] == 1) {
					addslideup('itemtaggriddisplay', 'itemtagaddformdiv');
					$('#primarydataid').val(data.itemtagid);
					$(".addbtnclass").addClass('hidedisplay');
					$("#dataupdatesubbtn").attr('data-cuopactive','active');
					$("#dataaddsbtn").attr('data-cuopactive','inactive');
					if(data.lotid != 0) {
						loadcurrentpurchaselot(data.lotid);
					}
					$('#operationmode').val('edit');
					$('#tagentrytypeid').select2('val',data.tagentrytypeid).trigger('change');
					if(detailedstatus == 1) {
						$('#tagtypeid').select2('val',data.tagtypeid);
					} else {
						$(".updatebtnclass").removeClass('hidedisplay');
						$('#tagtypeid').select2('val',data.tagtypeid).trigger('change');	//pre-set
					}	
					if(itemeditstatus == 1) {
						$('#tagtypeid').select2('val',data.tagtypeid).trigger('change');
					}
					$('#chargefrom').select2('val',data.chargeapply).trigger('change');
					additional_details=1;
					if(data.lotid != '0' && data.lotid != '1' && data.lotid != '') {
						$('#lotid').select2('val',data.lotid).trigger('change');	//pre-set
					}
					var txtboxname = elementsname;
					var dropdowns = ['13','purityid','accountid','fromcounterid','productid','counterid','tagtemplateid','tagentrytypeid','branchid','collectionid','guaranteecard','scaleoption','size','packwtless'];
					textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
					Materialize.updateTextFields(); 
					//Stone Entry grid load
					if(data['stoneentry'] != '') {
						var stonedata = data['stoneentry'];
						$("#hiddenstonedata").val(stonedata);
						getstoneentrydiv(data.itemtagid);
					}
					//Image
					var image = data['tagimage'];
					if(checkValue(image) == true) {
						$('#tagimagedisplay').empty();
						var img = $('<img id="companylogodynamic" style="height:100%;width:100%">');
						img.attr('src', base_url+image);
						img.appendTo('#tagimagedisplay');
						$('#tagimagedisplay').append('<i class="material-icons documentslogodownloadclsbtn">close</i>');
						if(detailedstatus == 1) {
							$(".documentslogodownloadclsbtn").hide();
						}
						$(".documentslogodownloadclsbtn").click(function() {
							$(this).remove();
							$('#tagimagedisplay').empty();
							$('#tagimage').val('');
						});
					} else {
						$('#tagimage').val('');
						$('#tagimagedisplay').empty();
					}
					$('#changelotid').val(data.lotid);
					$('#changegrosswt').val(data.grossweight);
					$('#changenetwt').val(data.netweight);
					if(hidproductaddoninfo == 1) {
						var productaddoninfoid=data.productaddoninfoid.split(",");
						$("#productaddoninfoid").select2('val',productaddoninfoid).trigger('change');
						//$('#productaddoninfoid').prop('disabled',true);
					}
					//For Keyboard Shortcut Variables
					addformupdate = 1;
					viewgridview = 0;
					addformview = 1;
					if(vacchargeshow == 'YES') {
						$('.addonchargeiconchange').removeClass('hidedisplay');
						if(data.chargedetails != '') {
							var chargecategoryidstring =data['keyword']['chargekeyword'].toString();
							var makingkey = [];
							var wastagekey = [];
							var hallmarkkey = [];
							var flatkey = [];
							var range = '';
							var firstindexwastage = 0;
							var firstindexwastagecount = 0;
							var firstwastagecharge = '';
							var firstwastagechargename = '';
							var firstwastagechargeid = '';
							var firstwastagechargekey = '';
							var firstindexmaking = 0;
							var firstindexmakingcount = 0;
							var firstmakingcharge ='';
							var firstmakingchargename ='';
							var firstmakingchargeid ='';
							var firstmakingchargekey ='';
							var firstindexflat = 0;
							var firstindexflatcount = 0;
							var firstflatcharge ='';
							var firstflatchargename ='';
							var firstflatchargeid ='';
							var firstflatchargekey ='';
							var firstindexhallmark = 0;
							var firstindexhallmarkcount = 0;
							var firsthallmarkcharge ='';
							var firsthallmarkchargename ='';
							var firsthallmarkchargeid ='';
							var firsthallmarkchargekey ='';
							if (chargecategoryidstring.indexOf(',') > -1) {
								var chargedetails=chargecategoryidstring.split(",");
								if(chargedetails == '') {
								} else {
									$.each(chargedetails, function (index, value) {
										var splitdatakey = value.split(":");
										$("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
										$("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
										if(chargecalculation == 2) {
											range = 'MIN';
										} else if(chargecalculation == 1) {
											range = 'MAX';
										}
										if(splitdatakey[0] == '2') {  // making
											firstindexmakingcount++;
											var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
											makingkey.push(makingfinal);
											$('#makingiconchange').attr('keywordarray',''+makingkey+'');
											if(firstindexmakingcount == 1){
												$('#makingiconchange').addClass('hidedisplay');
											} else {
												$('#makingiconchange').removeClass('hidedisplay');
											}
										} else if(splitdatakey[0] == '3') { // wastage
											firstindexwastagecount++;
											var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
											wastagekey.push(wastagefinal);
											$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
											if(firstindexwastagecount == 1) {
												$('#wastageiconchange').addClass('hidedisplay');
											} else {
												$('#wastageiconchange').removeClass('hidedisplay');
											}
										} else if(splitdatakey[0] == '4') { // hallmark
											firstindexhallmarkcount++;
											var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
											hallmarkkey.push(hallmarkfinal);
											$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
											if(firstindexhallmarkcount == 1){
												$('#hallmarkiconchange').addClass('hidedisplay');
											} else {
												$('#hallmarkiconchange').removeClass('hidedisplay');
											}
										} else if(splitdatakey[0] == '6') {  // flat
											firstindexflatcount++;
											var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
											flatkey.push(flatfinal);
											$('#flaticonchange').attr('keywordarray',''+flatkey+'');
											if(firstindexflatcount == 1){
											$('#flaticonchange').addClass('hidedisplay');
											} else {
												$('#flaticonchange').removeClass('hidedisplay');
											}
										}
									});
								}
							} else {
								$('.addonchargeiconchange').addClass('hidedisplay');
								var splitdatakey = chargecategoryidstring.split(":");
								$("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
								$("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
								if(chargecalculation == 2) {
									range = 'MIN';
								} else if(chargecalculation == 1) {
									range = 'MAX';
								}
								if(splitdatakey[0] == '2') {  // making
									firstindexmakingcount++;
									var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
									makingkey.push(makingfinal);
									$('#makingiconchange').attr('keywordarray',''+makingkey+'');
									if(firstindexmakingcount == 1) {
										$('#makingiconchange').addClass('hidedisplay');
									} else {
										$('#makingiconchange').removeClass('hidedisplay');
									}
								} else if(splitdatakey[0] == '3') { // wastage
									firstindexwastagecount++;
									var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
									wastagekey.push(wastagefinal);
									$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
									if(firstindexwastagecount == 1){
										$('#wastageiconchange').addClass('hidedisplay');
									} else {
										$('#wastageiconchange').removeClass('hidedisplay');
									}
								} else if(splitdatakey[0] == '4') { // hallmark
									firstindexhallmarkcount++;
									var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
									hallmarkkey.push(hallmarkfinal);
									$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
									if(firstindexhallmarkcount == 1) {
										$('#hallmarkiconchange').addClass('hidedisplay');
									} else {
										$('#hallmarkiconchange').removeClass('hidedisplay');
									}
								} else if(splitdatakey[0] == '6') {  // flat
									firstindexflatcount++;
									var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
									flatkey.push(flatfinal);
									$('#flaticonchange').attr('keywordarray',''+flatkey+'');
									if(firstindexflatcount == 1) {
										$('#flaticonchange').addClass('hidedisplay');
									} else {
										$('#flaticonchange').removeClass('hidedisplay');
									}
								}
							}
							var chargekeyworddata=data.chargedetails.split(",");
							$.each(chargekeyworddata, function (index, value) {
								if (value.indexOf(":") >= 0) {
									var res=value.split(":");
									var finalres = res[0].split(".");
									var result = finalres[0]+'-'+finalres[1];
									if(finalres[0] == 'WS-PT-N' || finalres[0] == 'WS-WT' || finalres[0] == 'WS-PT-G') {
										$('#wastageiconchange').attr('keywordcalc',''+result+'');
										$('#wastage').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
										$('#wastage').attr('keywordrange',''+res[0]+'');
										$('#wastagespan').attr('spankeyword',''+result+'');
										if(finalres[0] == 'WS-PT-N') {
											$("#wastagespan").text('Wastage Percent Net');
											$('#wastage').attr('chargeid',5);
										} else if(finalres[0] == 'WS-WT') {
											$("#wastagespan").text('Wastage Flat Weight');
											$('#wastage').attr('chargeid',6);
										} else if(finalres[0] == 'WS-PT-G') {
											$("#wastagespan").text('Wastage % Gross');
											$('#wastage').attr('chargeid',7);
										}
									} else if(finalres[0] == 'MC-GM-N' || finalres[0] == 'MC-FT' || finalres[0] == 'MC-PI' || finalres[0] == 'MC-GM-G') {
										$('#makingiconchange').attr('keywordcalc',''+result+'');
										$('#making').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
										$('#making').attr('keywordrange',''+res[0]+'');
										$('#makingspan').attr('spankeyword',''+result+'');
										if(finalres[0] == 'MC-GM-N') {
											$("#makingspan").text('Making Per Gram Net');
											$('#making').attr('chargeid',2);
										} else if(finalres[0] == 'MC-FT') {
											$("#makingspan").text('Making Flat');
											$('#making').attr('chargeid',3);
										} else if(finalres[0] == 'MC-PI') {
											$("#makingspan").text('Making Per Piece');
											$('#making').attr('chargeid',4);
										} else if(finalres[0] == 'MC-GM-G') {
											$("#makingspan").text('Making PerGram Gross');
											$('#making').attr('chargeid',14);
										}
									} else if(finalres[0] == 'FC' || finalres[0] == 'FC-WT') {
										$('#flaticonchange').attr('keywordcalc',''+result+'');
										$('#flat').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
										$('#flat').attr('keywordrange',''+res[0]+'');
										$('#flatspan').attr('spankeyword',''+result+'');
										if(finalres[0] == 'FC') {
											$("#flatspan").text('Flat Amount');
											$('#flat').attr('chargeid',12);
										} else if(finalres[0] == 'FC-WT') {
											$("#flatspan").text('Flat Weight');
											$('#flat').attr('chargeid',19);
										}
									} else if(finalres[0] == 'HM-FT' || finalres[0] == 'HM-PI') {
										$('#hallmarkiconchange').attr('keywordcalc',''+result+'');
										$('#hallmark').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
										$('#hallmark').attr('keywordrange',''+res[0]+'');
										$('#hallmarkspan').attr('spankeyword',''+result+'');
										if(finalres[0] == 'HM-FT') {
											$("#hallmarkspan").text('Hallmark Flat');
											$('#hallmark').attr('chargeid',8);
										} else if(finalres[0] == 'HM-PI') {
											$("#hallmarkspan").text('Hallmark Per Piece');
											$('#hallmark').attr('chargeid',9);
										}
									}
									$("#itemtagform .chargedetailsdiv input[keywordcalc="+result).val(res[1]);
								} else {
									if(chargecalculation == 2){
										var cond = 'MIN';
									} else {
										var cond = 'MAX';
									}
									var result = value+'-'+cond;
									var keyword = value+'.'+cond;
									if(value == 'WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G') {
										$('#wastageiconchange').attr('keywordcalc',''+result+'');
										$('#wastage').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
										$('#wastage').attr('keywordrange',''+keyword+'');
										$('#wastagespan').attr('spankeyword',''+result+'');
										if(value == 'WS-PT-N') {
											$("#wastagespan").text('Wastage Percent Net');
											$('#wastage').attr('chargeid',5);
										} else if(value == 'WS-WT') {
											$("#wastagespan").text('Wastage Flat Weight');
											$('#wastage').attr('chargeid',6);
										} else if(value == 'WS-PT-G') {
											$("#wastagespan").text('Wastage % Gross');
											$('#wastage').attr('chargeid',7);
										}
									} else if(value == 'MC-GM-N' || value == 'MC-FT' || value == 'MC-PI' || value == 'MC-GM-G') {
										$('#makingiconchange').attr('keywordcalc',''+result+'');
										$('#making').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
										$('#making').attr('keywordrange',''+keyword+'');
										$('#makingspan').attr('spankeyword',''+result+'');
										if(value == 'MC-GM-N') {
											$("#makingspan").text('Making Per Gram Net');
											$('#making').attr('chargeid',2);
										} else if(value == 'MC-FT') {
											$("#makingspan").text('Making Flat');
											$('#making').attr('chargeid',3);
										} else if(value == 'MC-PI') {
											$("#makingspan").text('Making Per Piece');
											$('#making').attr('chargeid',4);
										} else if(value == 'MC-GM-G') {
											$("#makingspan").text('Making PerGram Gross');
											$('#making').attr('chargeid',14);
										}
									} else if(value == 'FC' || value == 'FC-WT') {
										$('#flaticonchange').attr('keywordcalc',''+result+'');
										$('#flat').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
										$('#flat').attr('keywordrange',''+keyword+'');
										$('#flatspan').attr('spankeyword',''+result+'');
										if(value == 'FC') {
											$("#flatspan").text('Flat Amount');
											$('#flat').attr('chargeid',12);
										} else if(value == 'FC-WT') {
											$("#flatspan").text('Flat Weight');
											$('#flat').attr('chargeid',19);
										}
									} else if(value == 'HM-FT' || value == 'HM-PI') {
										$('#hallmarkiconchange').attr('keywordcalc',''+result+'');
										$('#hallmark').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
										$('#hallmark').attr('keywordrange',''+keyword+'');
										$('#hallmarkspan').attr('spankeyword',''+result+'');
										if(value == 'HM-FT') {
											$("#hallmarkspan").text('Hallmark Flat');
											$('#hallmark').attr('chargeid',8);
										} else if(value == 'HM-PI') {
											$("#hallmarkspan").text('Hallmark Per Piece');
											$('#hallmark').attr('chargeid',9);
										}
									}
									$("#itemtagform .chargedetailsdiv span[id="+result).text(0);
									chargecalc(keyword,0,result);		
								}
							});
							$('#previouscharge').val(data.chargedetails);
						} else {
							$(".chargedata").attr('data-validation-engine','');
						}
						if(data.chargespandetails != '') {
							var chargespandata=data.chargespandetails.split(",");
							$.each(chargespandata, function (index, value) {
								var res=value.split(":");
								var finalres = res[0].split(".");
								var result = finalres[0]+'-'+finalres[1];
								$("#itemtagform .chargedetailsdiv span#"+result).text(res[1]);
							});
						}
						if(data.previousdata == '1') {
							$('.inlinespanedit').trigger('keyup');
						}
						Materialize.updateTextFields(); 
					}
				} else {
					alertpopup('Cannot Edit This Tag NO : '+data['itemtagnumber']+',it is '+data['tagstatusname']);
				}
				setTimeout(function() {
					$('#rate').val(data.rate);
					$('#touch').val(data.touch);
					$('#description').val(data.description);
					var purityid = $.trim($("#productid").find('option:selected').data('purityid'));
					if(purityid == '' || purityid == 'null' || purityid == 1) {
					} else {
						purityid=purityid.toString();
						purityshowhide('purityid',purityid);
					}
					$('#purityid').select2('val',data.purityid);
					loadproductbasedonpurity(data.purityid,0)
					$('#productid').select2('val',data.productid);
					var isrfid = $("#productid").find('option:selected').data('rfid');
					rfidhideshow(isrfid);
					var categoryid = $('#productid').find('option:selected').data('categoryid');
					if(categoryid == 5) {
						$("#grossweight").val(1);
						$("#netweight").val(1);
						$('#grossweight-div,#netweight-div').hide();
					}
				},10);
				if(counterusestatus == 'yes' && storagequantity == 1) {
					$('#editcounterid').val(data['counterid']);
				}
				setTimeout(function(){
					loadproductbasedcounteritem('productid','counterid');
					$('#counterid').select2('val',data['counterid']);
					$('#tagtemplateid').select2('val',data.tagtemplateid);
				},5);
				if(data.tagtypeid == '3') {
					$('#size').select2('val','');
					$('#size').attr('data-validation-engine','');
					$("#size-div").hide();
				} else {
				   sizehideshow(data.productid);
				   $('#size').select2('val',data.size);
				}
				$('#grossweight,#stoneweight,#netweight,#itemcaratweight,#tagweight,#packingweight,#packwtless').prop('disabled',true);
				select2ddenabledisable('productid','disable');
				select2ddenabledisable('purityid','disable');
				select2ddenabledisable('lotid','disable');
				$('#stoneweighticon').hide();
				$('#date,#itemrate,#itemratewithgst').prop('disabled',true);
				setTimeout(function(){
					$('#tagtypeid').select2('val',data.tagtypeid);
					var stoneapplicable = $("#productid").find('option:selected').data('stone');
					stonedivhideshow(stoneapplicable);
				},10);
				/* if(data.tagtypeid == 3) {
					$('#pieces').prop('readonly',false);
				}else{ */
					$('#pieces').prop('readonly',true);
				//}
				$('#tagtypeid').prop('disabled',true);
				setTimeout(function() {
					if(data.tagentrytypeid == 10 || data.tagentrytypeid == 11 || data.tagentrytypeid == 12 || 
					data.tagentrytypeid ==13) {
						producthideshowbasedonpurity(data.tagentrytypeid,data.purityid);
						$('#processproductid').select2('val',data.processproductid);
						$('#fromcounterid,#processproductid').prop('disabled',true);
					}
				},10);
			}
		});
	}
	function loadcurrentpurchaselot(lotid) {
		if(lotid != '') {
			lotid=lotid;
		} else {
			lotid=0;
		}
		$('#lotid').empty();
		$('#lotid').append($("<option value='1'>Select</option>"));
		$.ajax({
			url: base_url+"Itemtag/current_lot",
			data:{lotid:lotid},
			method: "POST",
			dataType:'json',
			async:false,
			success: function(data) {				
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var categoryname="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						if(lotpieces == 'Yes') {
							if(data[m]['categoryid'] == '1') {
								categoryname = '';
							} else {
								categoryname = " | "+data[m]['categoryname'];
							}
							if(data[m]['loosestone'] == 0) {
								newddappend += "<option data-categoryid ='" +data[m]['categoryid']+ "' data-lotpurity ='" +data[m]['lotpurity']+ "' data-productid ='" +data[m]['productid']+ "'  data-productcount ='" +data[m]['productcount']+ "' data-accountid ='" +data[m]['accountid']+ "' data-lotgrosswt ='" +data[m]['grossweight']+ "' data-lotnetwt ='" +data[m]['netweight']+"' data-currentlotgrosswt ='" +data[m]['pendinggrossweight']+ "' data-currentlotnetwt ='" +data[m]['pendingnetweight']+ "' data-currentpieces ='" +data[m]['pendingpieces']+ "' data-receiveorder='"+data[m]['receiveorder']+"' data-lotsalesdetailid='"+data[m]['salesdetailid']+"' data-lotsalesdetailordernumber='"+data[m]['billrefno']+"' data-lotstoneapplicable='"+data[m]['stoneapplicable']+"' data-lottypeid='"+data[m]['lottypeid']+"' data-lstapplicable='"+data[m]['loosestone']+"' value = '" +data[m]['lotid']+ "'>"+data[m]['lotnumber']+" | "+data[m]['purityname']+categoryname+" | Gr "+data[m]['grossweight']+" | Pcs "+data[m]['pieces']+" | Nt "+data[m]['netweight']+"</option>";
							} else {
								newddappend += "<option data-categoryid ='" +data[m]['categoryid']+ "' data-lotpurity ='" +data[m]['lotpurity']+ "' data-productid ='" +data[m]['productid']+ "'  data-productcount ='" +data[m]['productcount']+ "' data-accountid ='" +data[m]['accountid']+ "' data-lotgrosswt ='" +data[m]['grossweight']+ "' data-lotnetwt ='" +data[m]['netweight']+"' data-currentlotgrosswt ='" +data[m]['pendinggrossweight']+ "' data-currentlotnetwt ='" +data[m]['pendingnetweight']+ "' data-currentpieces ='" +data[m]['pendingpieces']+ "' data-receiveorder='"+data[m]['receiveorder']+"' data-lotsalesdetailid='"+data[m]['salesdetailid']+"' data-lotsalesdetailordernumber='"+data[m]['billrefno']+"' data-lotstoneapplicable='"+data[m]['stoneapplicable']+"' data-lottypeid='"+data[m]['lottypeid']+"' data-lstapplicable='"+data[m]['loosestone']+"' data-lotcaratwt ='" +data[m]['caratweight']+ "' value = '" +data[m]['lotid']+ "'>"+data[m]['lotnumber']+" | "+data[m]['purityname']+categoryname+" | Ct.Wt. "+data[m]['caratweight']+" | Pcs "+data[m]['pieces']+"</option>";
							}
						} else {
							if(data[m]['categoryid'] == '1') {
								categoryname = '';
							} else {
								categoryname = " | "+data[m]['categoryname'];
							}
							if(data[m]['loosestone'] == 0) {
								newddappend += "<option data-categoryid ='" +data[m]['categoryid']+ "' data-lotpurity ='" +data[m]['lotpurity']+ "'  data-productid ='" +data[m]['productid']+ "' data-productcount ='" +data[m]['productcount']+ "' data-accountid ='" +data[m]['accountid']+ "' data-lotgrosswt ='" +data[m]['grossweight']+ "' data-lotnetwt ='" +data[m]['netweight']+ "' data-currentlotgrosswt ='" +data[m]['pendinggrossweight']+ "' data-currentlotnetwt ='" +data[m]['pendingnetweight']+ "' data-receiveorder='"+data[m]['receiveorder']+"' data-lotsalesdetailid='"+data[m]['salesdetailid']+"' data-lotsalesdetailordernumber='"+data[m]['billrefno']+"' data-lotstoneapplicable='"+data[m]['stoneapplicable']+"' data-lottypeid='"+data[m]['lottypeid']+"' data-lstapplicable='"+data[m]['loosestone']+"' value = '" +data[m]['lotid']+ "'>"+data[m]['lotnumber']+" | "+data[m]['purityname']+categoryname+" | Gr "+data[m]['grossweight']+" | Nt "+data[m]['netweight']+"</option>";
							} else {
								newddappend += "<option data-categoryid ='" +data[m]['categoryid']+ "' data-lotpurity ='" +data[m]['lotpurity']+ "'  data-productid ='" +data[m]['productid']+ "' data-productcount ='" +data[m]['productcount']+ "' data-accountid ='" +data[m]['accountid']+ "' data-lotgrosswt ='" +data[m]['grossweight']+ "' data-lotnetwt ='" +data[m]['netweight']+ "' data-currentlotgrosswt ='" +data[m]['pendinggrossweight']+ "' data-currentlotnetwt ='" +data[m]['pendingnetweight']+ "' data-receiveorder='"+data[m]['receiveorder']+"' data-lotsalesdetailid='"+data[m]['salesdetailid']+"' data-lotsalesdetailordernumber='"+data[m]['billrefno']+"' data-lotstoneapplicable='"+data[m]['stoneapplicable']+"' data-lottypeid='"+data[m]['lottypeid']+"' data-lstapplicable='"+data[m]['loosestone']+"' data-lotcaratwt ='" +data[m]['caratweight']+ "' value = '" +data[m]['lotid']+ "'>"+data[m]['lotnumber']+" | "+data[m]['purityname']+categoryname+" | Ct.Wt. "+data[m]['caratweight']+"</option>";
							}
						}
					}
					//after run
					$('#lotid').append(newddappend);
				}	
			},
		});
	}

}
function deletescreencapture(){
// webcam delete and recapture
	var SayCheesed = new SayCheese('#container-element-delete', {video: true}); // for delete
	SayCheesed.start();
	$('#take-snapshot-delete').click(function() {
		var width = 1000, height = 800;
		SayCheesed.takeSnapshot(width, height);
		$("#viewproaddcapture").addClass('hidedisplay');
		$("#saveandcncelbtn").removeClass('hidedisplay');
		$("#container-element-delete").addClass('hidedisplay');
		$("#say-cheese-snapshots-delete").removeClass('hidedisplay');
	  });
	   SayCheesed.on('snapshot', function(snapshot) {
	  var img = document.createElement('img');

	  $(img).on('load', function() {
		$('#say-cheese-snapshots-delete').empty();
		$('#say-cheese-snapshots-delete').prepend(img);
	  });
	  img.src = snapshot.toDataURL('image/png');
	  $("#imgurldatadelete").val(img.src);
	});
	$('#deletecapturedproimgbtn').click(function() {
		$("#say-cheese-snapshots-delete").empty();
		$("#say-cheese-snapshots-delete").addClass('hidedisplay');
		$("#saveandcncelbtn").addClass('hidedisplay');
		$("#container-element-delete").removeClass('hidedisplay');
		$("#viewproaddcapture").removeClass('hidedisplay');
	  });
	  $("#savecapturedproimgbtn").click(function(){
		var dataurlimgdelete =  $("#imgurldatadelete").val();
		var itemunbindwebcam = successdelete(dataurlimgdelete);
		$("#savecapturedproimgbtn").unbind( "click", itemunbindwebcam );
	});
	$('#rfidalertsclose').click(function() {
		$('#rfidalerts').fadeOut();
		$('#rfidtagno').focus();
	});
}
{// View create success function
    function viewcreatesuccfun(viewname)
	{
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//retrieve current pending weight
function retrivecurrentlotpending(lotid)
{
	$.ajax({
			url:base_url+"Itemtag/retrivecurrentlotpending", 
			data:{lotid:lotid},
			dataType:'json',
			async:false,
			success: function(data)
			{
				$('#totallotgrosswt').val(data.totalgrossweight);
				$('#totallotnetwt').val(data.totalnetweight);
				$('#totallotcaratwt').val(data.totalcaratweight);
				$('#totallotpieces').val(data.totalpieces);
				$('#completedlotgrosswt').val(data.completedgrossweight);
				$('#completedlotnetwt').val(data.completednetweight);
				$('#completedlotcaratwt').val(data.completedcaratweight);
				$('#completedlotpieces').val(data.completedpieces);
				$('#pendinglotgrosswt').val(data.pendinggrossweight);
				$('#pendinglotnetwt').val(data.pendingnetweight);
				$('#pendinglotcaratwt').val(data.pendingcaratweight);
				$('#pendinglotpieces').val(data.pendingpieces);
				$('#pendinglotstonewt').val(data.pendingstoneweight);
			}
		});	
}
function resetwtps() {
	$('#pieces').val(1);
	$('#hiddenstonedata').val('');
	$("#stoneweight").attr('readonly', true);
	cleargriddata('stoneentrygrid');
	getstonegridsummary();
}
function orderitem(id)
{
	var nmsg = '';
	$.ajax({
			url:base_url+"Itemtag/orderitem", 
			data:{id:id},			
			async:false,
			success: function(msg)
			{
				nmsg =  $.trim(msg);				
			}
	});
	return nmsg;
}
// tag data retrieve - Madasamy
function gettagdata() {
	var tagnumber = $('#tagnumber').val();
	var tagnumber = $.trim(tagnumber);
	if(checkValue(tagnumber) == true ){
	$.ajax({
			url:base_url+"Itemtag/retrievetagdata",
			data:{tagnumber:tagnumber},
			dataType:'json',
			async:false,
			success :function(data)
			{ 
				msg = data.output;
				if(data.output == 'UNSOLD') {
					$('#transferdate').prop('readonly',true);
					var elementsname=['10','itemtagid','tagtypetransferid','purity','product','grossweighttransfer','stoneweighttransfer','netweighttransfer','transferpieces','transfermelting','transferfromcounterid'];
					var txtboxname = elementsname;
					var dropdowns = ['1','tocounter'];
					textboxsetvalue(txtboxname,txtboxname,data,dropdowns);	
					tocounterhideshow(data['transferfromcounterid'],data['transfertocounterid'],'tocounter');
					$('#hiddentagtypeid').val(data['tagtypetransferid']);
				} 
				//validate messages
				if(msg == 'UNSOLD'){
				} 
				else if(msg == 'SOLD'){
					var tagerrordisp = "Tag No : "+tagnumber+" is sold,enter a active tag";
					tagerrorresetandalert(tagerrordisp);
				} 
				else {
					var tagerrordisp = "Tag No : "+tagnumber+" is invalid,enter correct tag number";
					tagerrorresetandalert(tagerrordisp);
				}
			}
		});
	}
}
function tagerrorresetandalert(tagerrordisp)
{	
	alertpopup(tagerrordisp);
	$('#tagnumber').val('');
	$('#transferto').trigger('change');
	$('#tagnumber').focus();
}
function itemtagtotransfer() {
	var form = $("#tagtransferform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	var counterdefault = $('#tocounter').find('option:selected').data('counterdefault');
	var counterdefaultid = $('#tocounter').find('option:selected').data('counterdefaultid');
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
        url: base_url +"Itemtag/itemtagtransfercreate",
        data: "datas=" + datainformation+amp+"counterdefault="+counterdefault+amp+"counterdefaultid="+counterdefaultid,
		type: "POST",
		//async:false,
        success: function(msg) {
			if(msg == 'SUCCESS'){
				itemtagrefreshgrid();
				$('#processoverlay').hide();
				$("#tagnumber").val('');
				$('#tocounter,#tagtransprocessproductid').select2('val','');
				stocknotify = $('#stocknotify').val();
				if(stocknotify == 'YES'){
				  alertpopup('Transfered successfully');	 
				}
				$("#tagnumber").focus();
			} else {
				alertpopup(submiterror);
			}
        },
    });
}
function nontagtotransfer() {
	var purityid = $('#nontagpurity').val();
	var form = $("#nontagtransferform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	var counterdefault = $('#nontagtocounter').find('option:selected').data('counterdefault');
	var counterdefaultid = $('#nontagtocounter').find('option:selected').data('counterdefaultid');
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
	$.ajax( {
        url: base_url +"Itemtag/nontagtransfercreate",
        data: "datas=" + datainformation+"&purityid="+purityid+amp+"counterdefault="+counterdefault+amp+"counterdefaultid="+counterdefaultid,
		type: "POST",
		success: function(msg) {
			if(msg == 'SUCCESS') {
				$('#processoverlay').hide();
				purityshowhide('nontagpurity','');
				clearform('untagtransferclear');
				itemtagrefreshgrid();
				stocknotify = $('#stocknotify').val();
				if(stocknotify == 'YES'){
				  alertpopup('Transfered successfully');	 
				}
				$('#nontagfromcounterid').select2('focus');
			} else {
				$('#nontagfromcounterid').select2('focus');
				alertpopup(submiterror);
			}
        },
    });
}
function missingitemaddform() {
	var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
	var missingitemstatus = $('#missingitemstatus').val();
	var form = $("#missingitemform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	$('#processoverlay').show();
	$.ajax({
		url: base_url +"Itemtag/missingitemaddform",
        data: "datas=" + datainformation+"&missingitemstatus="+missingitemstatus+'&datarowid='+datarowid,
		type: "POST",
		success: function(msg) {
			if(msg == 'SUCCESS') {
				$('#processoverlay').hide();
				alertpopup('Data stored successfully.');
				$('#missingtagnumber,missingrfidno,#missingdescription').val('');
				$('#missingitemoverlay').fadeOut();
			} else {
				$('#processoverlay').hide();
				alertpopup('There is something problem. Please try again.');
				$('#missingtagnumber,missingrfidno,#missingdescription').val('');
				$('#missingitemoverlay').fadeOut();
			}
		}
	});
}
function checklotstatus(lotid) {
	$.ajax({
        url: base_url + "Itemtag/checklotstatus?lotid="+lotid,
       	async:false,
		cache:false,
		dataType:'text',
		success: function(data) { 
			if(data == 'closed') {
				alertpopup('Lot has been completed');
				$("#lotid option[value="+lotid+"]").addClass("ddhidedisplay");
				$("#lotid option[value="+lotid+"]").prop('disabled',true);
				$("#lotid option[value=1]").removeClass("ddhidedisplay");
				$("#lotid option[value=1]").prop('disabled',false);
				$('#lotid').select2('val',1).trigger('change');
				select2ddenabledisable('tagtypeid','enable');
			}
        },
    });
}
// reduce textbox size 
	function generateiconafterload(fieldid,clickevnid){
		if($("#hiddenstoneyesno").val()==1){
			$("#"+fieldid+"").css('display','inline').css('width','75%');	
			$("#"+fieldid+"").after('<span class="material-icons innerfrmicon stoneweighticoncls" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="246"><i class="material-icons">group_work</i></span>');
			$('label[for="'+fieldid+'"]').css('width','65%');
		}
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
	$("#s2id_"+fieldid+"").css('display','inline-block').css('width','90%');
	$("#"+fieldid+"").after('<span title="Product Search"  class="fa fa-search" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer"></span>');
	}
	function loadproductbasedonpurity(purityid,status){
		var tagtype = $.trim($("#tagtypeid").find('option:selected').val());
		if(status == 0){			
			$("#productid").select2('val','');
		}
		var purity = 'a'+purityid+'a'; // comma purity hide/show
		$("#productid option").addClass("ddhidedisplay");
		$("#productid option").prop('disabled',true);
		var categoryid = $.trim($("#lotid").find('option:selected').data('categoryid'));
		var lotid = $.trim($("#lotid").find('option:selected').val());
		var prodid = $.trim($("#lotid").find('option:selected').data('productid'));
		if(categoryid !=1 && lotid != 1 && prodid != 1 && categoryid !='' && lotid != '' && prodid != '') { //category,product based
			$("#productid option[data-purityidhidden*='"+purity+"'][data-categoryid="+categoryid+"][value="+prodid+"]").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"'][data-categoryid="+categoryid+"][value="+prodid+"]").prop('disabled',false);
		}else if(categoryid !=1 && lotid != 1 && categoryid !='' && lotid != '') { //category based
			$("#productid option[data-purityidhidden*='"+purity+"'][data-categoryid="+categoryid+"]").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"'][data-categoryid="+categoryid+"]").prop('disabled',false);
		}else if(lotid != '' && lotid != 1 && purityid != '1') {
			$("#productid option[data-purityidhidden*='"+purity+"']").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"']").prop('disabled',false);
		}else if(lotid == '' || lotid == '1' && purityid != '1' && tagtype != '') {
			$("#productid option[data-purityidhidden*='"+purity+"'][data-tagtypeid="+tagtype+"]").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"'][data-tagtypeid="+tagtype+"]").prop('disabled',false);
			$("#productid option[data-purityidhidden*='"+purity+"'][data-tagtypeid=17]").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"'][data-tagtypeid=17]").prop('disabled',false);
		}else if(lotid == '' || lotid == '1' && purityid != '1' && tagtype == '') {
			$("#productid option[data-purityidhidden*='"+purity+"']").removeClass("ddhidedisplay");
			$("#productid option[data-purityidhidden*='"+purity+"']").prop('disabled',false);
		}
	}
	function tagtypeshowhide(){
		$('#tagtypeid').select2('val','');
		$("#tagtypeid option").addClass("ddhidedisplay");
		$("#tagtypeid option").prop('disabled',true);
		var tagentrytype=$('#tagentrytypeid').find('option:selected').data('tagtypeshow'); 
		var tagentry = tagentrytype.split(",");
		var tagentrylength=tagentry.length;
		for(i=0; i<tagentrylength; i++){
			$("#tagtypeid option[value="+tagentry[i]+"]").removeClass("ddhidedisplay");
			$("#tagtypeid option[value="+tagentry[i]+"]").prop('disabled',false);
		}
	}
	function getstonedetails(tagnumber){
		$.ajax({
        url: base_url + "Itemtag/getstonedetails?tagnumber="+tagnumber,
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) 
        { 
		if(data == ''){
			alertpopup('Please choose stone details tag');
			$('#stonetagnumber').val('');
			cleargriddata('stoneentrygrid');
			getstonegridsummary();
		}else{
			    cleargriddata('stoneentrygrid');
				stonedetailsicon=1;
				addslideup('itemtaggriddisplay','stonedetailsform');
				$('.stonedetailsform,.stoneiconlisthide').addClass('hidedisplay');
				$('.tagadditionalchargeclear_stone > div').addClass('hidedisplay');
				$('#savestoneentrydata').hide();
				$('.taghide').removeClass('hidedisplay');
				loadinlinegriddata('stoneentrygrid',data.rows,'json');
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('stoneentrygrid');
				var hideprodgridcol = ['stonegroupid','stoneid','totalcaratwt','totalnetwt'];
				gridfieldhide('stoneentrygrid',hideprodgridcol);
				getstonegridsummary();
		}
       },
    });
	}
	{//refresh grid
		function itemtagrefreshgrid() {
			var page = $('ul#itemtagpgnum li.active').data('pagenum');
			var rowcount = $('ul#itemtagpgnumcnt li .page-text .active').data('rowcount');
			itemtaggrid(page,rowcount);
		}
	}
	function autocompletedata(val){
		$( "#size" ).autocomplete({
		source:base_url+"Itemtag/loadsizedata?productid="+val
	   });
	}
	function getstoneentrydiv(datarowid){
	$.ajax({
			url: base_url+"Itemtag/retrive_stoneentry_div",
			data:{primaryid:datarowid},
			dataType:'json',
			type: "POST",
			async:false,
			success: function(msg) {
				if(msg == 'FAIL'){
					
				}else{
					loadinlinegriddata('stoneentrygrid',msg.rows,'json');
				   /* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('stoneentrygrid');
						var hideprodgridcol = ['stonegroupid','stoneid','totalcaratwt','totalnetwt'];
						gridfieldhide('stoneentrygrid',hideprodgridcol);
				}
			},
		});
}
//loads takeorder ordernumber with ids
function loadordernumber(datarowid){
	$('#ordernumber').empty();
	$('#ordernumber').append($("<option></option>"));
	$.ajax({
		url: base_url+"sales/gettakeorder?tagid="+datarowid,
		dataType:'json',
		async:false,
		success: function(data) {				
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += 
					"<option data-salesdetailid ='" +data[m]['salesdetailid']+ "' data-salesid ='" +data[m]['salesid']+ "' value = '" +data[m]['salesnumber']+ "'>"+data[m]['salesnumber']+"-"+data[m]['salesdetailid']+"</option>";
				}
				//after run
				$('#ordernumber').append(newddappend);
			}	
		},
	});
}
function stonehideshow(){
	// stone hide/show base on company setting
    var stonestatus = $('#stonestatus').val();
    var fieldid='stoneweight';
    if(stonestatus == 'YES'){
    	$("#stoneweighticon").show();
    	$("#"+fieldid+"").css('display','inline').css('width','75%');
    	$('label[for="'+fieldid+'"]').css('width','65%');
    }else{
    	$("#stoneweighticon").hide();
    	$("#"+fieldid+"").css('display','inline').css('width','100%');
    	$('label[for="'+fieldid+'"]').css('width','100%');
    }
}
function stoneiconset(){
	var fieldid='stoneweight';
	$("#"+fieldid+"").css('display','inline-block').css('width','75%');
	$('label[for="'+fieldid+'"]').css('width','65%');
}
function stoneiconreset(){
	var fieldid='stoneweight';
	$("#"+fieldid+"").css('display','inline-bock').css('width','100%');
	$('label[for="'+fieldid+'"]').css('width','100%');
}
function edititemtag(){
	var datarowid = $('#itemtaggrid div.gridcontent div.active').attr('id');
	if (datarowid) {
		//Function Call For Edit
		itemtagdataretrieve(datarowid);
		if(jQuery.inArray( userroleid, itemtagauthrole )!= -1) {   //Userrole who has having permission to edit
			enabledisableuserformfieds();
			$("#date").prop('disabled',true);
		} else {
			$("#date,#tagweight,#packingweight,#packwtless,#chargefrom").prop('disabled',true);
		}
		$("#formclearicon").addClass('hidedisplay');
		$('.branchiddiv').addClass('hidedisplay');
		getlatitemtagno(datarowid);
	} else {
		alertpopup(selectrowalert);
	}
}
function getrfidtagnumber(randomnumber){
	$.ajax(
			{
				url: base_url +"Itemtag/getrfidtagnumber",
			    data:{sessionid:randomnumber},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) 
				{
					if(msg != ''){
						$('#rfidtagno').val(msg);
					}
					Materialize.updateTextFields();
				}
			});
}
function checkrfiddevice(generatenumber){
	$.ajax(
			{
				url: base_url +"Itemtag/checkrfiddevice",
			    data:{sessionid:generatenumber},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) 
				{
					if(msg != ''){
						if(msg == 1){
							$('#statusbtn').addClass('material-icons visibility').attr('title','ON');
						}else if(msg ==0){
							$('#statusbtn').addClass('material-icons visibility_off').attr('title','OFF');
						} 
				    }
			   }
			});
}
function fromstoragehideshow(tag_entrytype){
     var stocktypeid = 'fromcounterid';
	 if(tag_entrytype == 3){
			$("#"+stocktypeid+" option").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" optgroup").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option").prop('disabled',false);
			var tagentrytypeid = '2,3,4,5';
			if (tagentrytypeid.indexOf(',') > -1) {
				var val = tagentrytypeid.split(',');
				for(j=0;j<(val.length);j++) {
					$("#fromcounterid option[value="+val[j]+"]").addClass("ddhidedisplay");
					$("#fromcounterid option[value="+val[j]+"]").prop('disabled',true);
					$("#fromcounterid option[data-parentcounterid="+val[j]+"]").addClass("ddhidedisplay");
					$("#fromcounterid option[data-parentcounterid="+val[j]+"]").prop('disabled',true);
				}
			}
	 }else{
			var val = '';
			$("#"+stocktypeid+" option").addClass("ddhidedisplay");
			$("#"+stocktypeid+" optgroup").addClass("ddhidedisplay");
			$("#"+stocktypeid+" option").prop('disabled',true);
			if(tag_entrytype == 11){  // Old Jewels
			   val = 2;
			}else if(tag_entrytype == 12){   // Partial Tag
			   val = 5;
			}else if(tag_entrytype == 10){   // Sales Return
			   val = 3;
			}else if(tag_entrytype == 13){   // Purchase Return
			   val = 4;
			}
			$("#"+stocktypeid+" option[data-counterdefaultid="+val+"]").removeClass("ddhidedisplay");	
			$("#"+stocktypeid+" option[data-counterdefaultid="+val+"]").prop('disabled',false);
	}
}
function getfromstorage(tag_entrytype,branchid){
		$('#fromcounterid').attr('disabled',false);
		$('#fromcounterid').attr('data-validation-engine','validate[required]');
	    $("#fromcounterid option").addClass("ddhidedisplay");
		$("#fromcounterid optgroup").addClass("ddhidedisplay");
		$("#fromcounterid option").prop('disabled',true);
		$("#fromcounterid").select2('val','');
		$.ajax(
			{
				url: base_url +"Itemtag/getfromstorage",
			    data:{tag_entrytype:tag_entrytype,branchid:branchid},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) 
				{   
					if($.trim(msg.counterid) == ''){
					}else{
					   fromcounterhideshow('fromcounterid',msg.counterid);
					}
			   }
			});
}
function fromcounterhideshow(stocktypeid,counterid){
  if (counterid.indexOf(',') > -1) {
		var val = counterid.split(',');
		for(j=0;j<(val.length);j++) {
			$("#"+stocktypeid+" option[value="+val[j]+"]").removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value="+val[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#"+stocktypeid+" option[value="+val[j]+"]").prop('disabled',false);
			if(stocktypeid == 'fromcounterid'){
			 $('.fromcounterhide').show();
			}
		}
	}else{
		$("#"+stocktypeid+" option[value="+counterid+"]").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+counterid+"]").closest('optgroup').removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+counterid+"]").prop('disabled',false);
		if(stocktypeid != 'productid'){
			$("#"+stocktypeid+"").select2('val',counterid).trigger('change');
		}
		if(stocktypeid == 'fromcounterid'){
			 $('.fromcounterhide').hide();
		}
		if(stocktypeid == 'processproductid'){
			$('#processproductid-div').hide();
		}
		if(stocktypeid == 'nontagprocessproductid'){
			$('#nontagprocessproductid-div').hide();
		}
	}
}
function getpurity(tagentrytype,val,branchid,purityid){
    $("#"+purityid+" option").addClass("ddhidedisplay");
	$("#"+purityid+" optgroup").addClass("ddhidedisplay");
	$("#"+purityid+" option").prop('disabled',true);
	$("#"+purityid+"").select2('val','');
	$.ajax(
			{
				url: base_url +"Itemtag/getpurity",
			    data:{tag_entrytype:tagentrytype,storage:val,branchid:branchid},
				type: "POST",
				async:false,
				dataType:'json',
				success: function(msg) 
				{   
					if($.trim(msg.purityid) != ''){
				      fromcounterhideshow(purityid,msg.purityid);
				   }
				}
			});
}
function loadproductfromstock(tag_entrytype,counterid,branchid,val,productid){
	$("#"+productid+" option").addClass("ddhidedisplay");
	$("#"+productid+" optgroup").addClass("ddhidedisplay");
	$("#"+productid+" option").prop('disabled',true); 
	$("#"+productid+"").select2('val','');
	$.ajax(
		{

			url: base_url +"Itemtag/loadproductfromstock",
			data:{tag_entrytype:tag_entrytype,storage:counterid,purity:val,branchid:branchid},
			type: "POST",
			async:false,
			dataType:'json',
			success: function(msg) 
			{
			   if($.trim(msg.productid) != ''){	
				 fromcounterhideshow(productid,msg.productid);
			   }else{
				  $("#"+productid+"").select2('val','');
			   }
			}
	});
}
function getweight(tag_entrytype,fromcounterid,purityid,branchid,val){
    $.ajax({
		url: base_url +"Itemtag/getweight",
		data:{tag_entrytype:tag_entrytype,storage:fromcounterid,purity:purityid,branchid:branchid,productid:val},
		type: "POST",
		async:false,
		dataType:'json',
		success: function(msg) 
		{   
		   $('#stocknetweight').val(msg.netweight);
		   $('#stockgrossweight').val(msg.grossweight);
		   $('#stockpieces').val(msg.pieces);
		}
	});
}
function storagehideshow() {
	var val = 1;
	$("#fromcounterid option[data-counterdefault="+val+"]").addClass("ddhidedisplay");
	$("#fromcounterid option[data-counterdefault="+val+"]").prop('disabled',true);
	var counteryesno = $('#counteryesno').val();
	if(counteryesno == 'no') {
		var valid = 11;
		$("#fromcounterid option[data-counterdefaultid="+valid+"]").removeClass("ddhidedisplay");
		$("#fromcounterid option[data-counterdefaultid="+valid+"]").prop('disabled',false);
	}
}
//RFID Tagnumber unique check
function rfitagnocheck() {
	var primaryid = $.trim($("#primarydataid").val());
	var accname = $("#rfidtagno").val();
	$("#rfidtagno").val(accname); //Sapna- Arvind changes
	var elementpartable = 'itemtag';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Itemtag/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			datatype :"text",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "BLANKFIELD") {
			$("#rfidtagno").val('');
			rfidalertpopup('Please Enter value in Field!');
			return "Please Enter value in Field!";
		} else if(nmsg == "NOTAGNO") {
			$("#rfidtagno").val('');
			rfidalertpopup(accname+" is not available in Database!");
			return accname+"is not available in Database!";
		} else {
			if(nmsg.indexOf("DUPLICATE") != -1) {
				nmsg = nmsg.replace('DUPLICATE','');
				if(primaryid != nmsg) {
					$("#rfidtagno").val('');
					rfidalertpopup(accname+' is already available in stock.');
					return "RFID Tagno already exists!";
					$("#rfidtagno").focus();
				}
			} else {
				$("#rfidtagno").val(nmsg);
				$("#pieces").focus();
			}
		} 
	}
}
function loadproductcharge(purityid,productid,chargeaccountgroup,netwt){
    $.ajax({
			url:base_url+"Itemtag/loadproductcharge",
			data:{purityid:purityid,productid:productid,chargeaccountgroup:chargeaccountgroup,netwt:netwt},
			type:"post",
			dataType:'json',
			async:true,
			cache:false,
			success :function(msg) {
				$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
                 if(msg['chargeid'] == ''){
					 alertpopup('Not available charge');
				 }else{					 
						var chargecategoryidstring =msg['keyword']['chargekeyword'].toString();
						var makingkey=[];
						var wastagekey=[];
						var hallmarkkey=[];
						var flatkey=[];
						var range = '';
						 var firstindexwastage = 0;
						 var firstindexwastagecount = 0;
						 var firstwastagecharge = '';
						 var firstwastagechargename = '';
						 var firstwastagechargeid = '';
						 var firstwastagechargekey = '';
						 var firstindexmaking = 0;
						 var firstindexmakingcount = 0;
						 var firstmakingcharge ='';
						 var firstmakingchargename ='';
						 var firstmakingchargeid ='';
						 var firstmakingchargekey ='';
						 var firstindexflat = 0;
						 var firstindexflatcount = 0;
						 var firstflatcharge ='';
						 var firstflatchargename ='';
						 var firstflatchargeid ='';
						 var firstflatchargekey ='';
						 var firstindexhallmark = 0;
						 var firstindexhallmarkcount = 0;
						 var firsthallmarkcharge ='';
						 var firsthallmarkchargename ='';
						 var firsthallmarkchargeid ='';
						 var firsthallmarkchargekey ='';
						 $('.addonchargeiconchange').removeClass('hidedisplay');
						 if (chargecategoryidstring.indexOf(',') > -1) { 
						 var chargedetails=chargecategoryidstring.split(",");
						 if(chargedetails == ''){
							}else{
								$.each(chargedetails, function (index, value) {
								   var splitdatakey = value.split(":");
								  $("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
								  $("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
									 if(chargecalculation == 2){
										  range = 'MIN';
									  }else if(chargecalculation == 1){
										  range = 'MAX';
									  }
									if(splitdatakey[0] == '2'){  // making
										if(firstindexmaking == 0){
										}
										firstindexmakingcount++;
										var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										makingkey.push(makingfinal);
										$('#makingiconchange').attr('keywordarray',''+makingkey+'');
										if(firstindexmakingcount == 1){
											$('#makingiconchange').addClass('hidedisplay');
										}else{
											$('#makingiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '3'){ // wastage
										if(firstindexwastage == 0){
										}
										firstindexwastagecount++;
										var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										wastagekey.push(wastagefinal);
										$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
										if(firstindexwastagecount == 1){
											$('#wastageiconchange').addClass('hidedisplay');
										}else{
											$('#wastageiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '4'){ // hallmark
										if(firstindexhallmark == 0){
										}
										firstindexhallmarkcount++;
										var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										hallmarkkey.push(hallmarkfinal);
										$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
										if(firstindexhallmarkcount == 1){
											$('#hallmarkiconchange').addClass('hidedisplay');
										}else{
											$('#hallmarkiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '6'){  // flat
										if(firstindexflat == 0){
										}
										firstindexflatcount++;
										var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										flatkey.push(flatfinal);
										$('#flaticonchange').attr('keywordarray',''+flatkey+'');
										if(firstindexflatcount == 1){
											$('#flaticonchange').addClass('hidedisplay');
										}else{
											$('#flaticonchange').removeClass('hidedisplay');
										}
									}
								 });
							}
				    }else{
						$('.addonchargeiconchange').addClass('hidedisplay');
						 var splitdatakey = chargecategoryidstring.split(":");
								  $("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
								  $("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
									 if(chargecalculation == 2){
										  range = 'MIN';
									  }else if(chargecalculation == 1){
										  range = 'MAX';
									  }
									if(splitdatakey[0] == '2'){  // making
										if(firstindexmaking == 0){
										}
										firstindexmakingcount++;
										var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										makingkey.push(makingfinal);
										$('#makingiconchange').attr('keywordarray',''+makingkey+'');
										if(firstindexmakingcount == 1){
											$('#makingiconchange').addClass('hidedisplay');
										}else{
											$('#makingiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '3'){ // wastage
										if(firstindexwastage == 0){
										}
										firstindexwastagecount++;
										var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										wastagekey.push(wastagefinal);
										$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
										if(firstindexwastagecount == 1){
											$('#wastageiconchange').addClass('hidedisplay');
										}else{
											$('#wastageiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '4'){ // hallmark
										if(firstindexhallmark == 0){
         								}
										firstindexhallmarkcount++;
										var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										hallmarkkey.push(hallmarkfinal);
										$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
										if(firstindexhallmarkcount == 1){
											$('#hallmarkiconchange').addClass('hidedisplay');
										}else{
											$('#hallmarkiconchange').removeClass('hidedisplay');
										}
									}else if(splitdatakey[0] == '6'){  // flat
										if(firstindexflat == 0){
										}
										firstindexflatcount++;
										var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										flatkey.push(flatfinal);
										$('#flaticonchange').attr('keywordarray',''+flatkey+'');
										if(firstindexflatcount == 1){
											$('#flaticonchange').addClass('hidedisplay');
										}else{
											$('#flaticonchange').removeClass('hidedisplay');
										}
									}
					}
					 var chargedetails=msg['chargedetails'].split(",");
					 if(chargedetails == ''){
						}else{
							$.each(chargedetails, function (index, value) {
                                if (value.indexOf(":") >= 0){
									var res=value.split(":");
									 var rescalc=res[0].split(".");
									 var result = rescalc[0]+'-'+rescalc[1];
									 if(rescalc[0] == 'WS-PT-N' || rescalc[0] == 'WS-WT' || rescalc[0] == 'WS-PT-G'){
										 $('#wastageiconchange').attr('keywordcalc',''+result+'');
										$('#wastage').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
										$('#wastage').attr('keywordrange',''+res[0]+'');
										$('#wastagespan').attr('spankeyword',''+result+'');
										if(rescalc[0] == 'WS-PT-N'){
											 $("#wastagespan").text('Wastage Percent Net');
											 $('#wastage').attr('chargeid',5);
										}else if(rescalc[0] == 'WS-WT'){
											 $("#wastagespan").text('Wastage Flat Weight');
											 $('#wastage').attr('chargeid',6);
										 }else if(rescalc[0] == 'WS-PT-G'){
											 $("#wastagespan").text('Wastage % Gross');
											 $('#wastage').attr('chargeid',7);
										 }
									 }else if(rescalc[0] == 'MC-GM-N' || rescalc[0] == 'MC-FT' || rescalc[0] == 'MC-PI' || rescalc[0] == 'MC-GM-G'){
										 $('#makingiconchange').attr('keywordcalc',''+result+'');
										$('#making').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
										$('#making').attr('keywordrange',''+res[0]+'');
										$('#makingspan').attr('spankeyword',''+result+'');
										if(rescalc[0] == 'MC-GM-N'){
											 $("#makingspan").text('Making Per Gram Net');
											 $('#making').attr('chargeid',2);
										 }else if(rescalc[0] == 'MC-FT'){
											 $("#makingspan").text('Making Flat');
											 $('#making').attr('chargeid',3);
										 }else if(rescalc[0] == 'MC-PI'){
											 $("#makingspan").text('Making Per Piece');
											 $('#making').attr('chargeid',4);
										 }else if(rescalc[0] == 'MC-GM-G'){
											 $("#makingspan").text('Making PerGram Gross');
											 $('#making').attr('chargeid',14);
										 }
                                  }else if(rescalc[0] == 'FC-WT' || rescalc[0] == 'FC'){
											 $('#flaticonchange').attr('keywordcalc',''+result+'');
											$('#flat').attr('keywordcalc',''+result+'');
											$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
											$('#flat').attr('keywordrange',''+res[0]+'');
											$('#flatspan').attr('spankeyword',''+result+'');
											if(rescalc[0] == 'MC-GM-N'){
												 $("#flatspan").text('flat Per Gram Net');
												 $('#flat').attr('chargeid',2);
											 }else if(rescalc[0] == 'MC-FT'){
												 $("#flatspan").text('flat Flat');
												 $('#flat').attr('chargeid',3);
											 }else if(rescalc[0] == 'MC-PI'){
												 $("#flatspan").text('flat Per Piece');
												 $('#flat').attr('chargeid',4);
											 }else if(rescalc[0] == 'MC-GM-G'){
												 $("#flatspan").text('flat PerGram Gross');
												 $('#flat').attr('chargeid',14);
											 }
									}else if(rescalc[0] == 'HM-FT' || rescalc[0] == 'HM-PI'){
									  $('#hallmarkiconchange').attr('keywordcalc',''+result+'');
										$('#hallmark').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
										$('#hallmark').attr('keywordrange',''+res[0]+'');
										$('#hallmarkspan').attr('spankeyword',''+result+'');
                                     if(rescalc[0] == 'HM-FT'){
										 $("#hallmarkspan").text('Hallmark Flat');
										 $('#hallmark').attr('chargeid',8);
									 }else if(rescalc[0] == 'HM-PI'){
										 $("#hallmarkspan").text('Hallmark Per Piece');
										 $('#hallmark').attr('chargeid',9);
									 }
								  }
									$("#itemtagform .chargedetailsdiv span[id="+result).text(res[1]);
									chargecalc(res[0],res[1],result);
								}else{
									if(chargecalculation == 2){
										 var cond = 'MIN';
									 }else{
										 var cond = 'MAX';
									 }
									 var result = value+'-'+cond;
									 var keyword = value+'.'+cond;
									  if(value == 'WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G'){
										$('#wastageiconchange').attr('keywordcalc',''+result+'');
										$('#wastage').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
										$('#wastage').attr('keywordrange',''+keyword+'');
										$('#wastagespan').attr('spankeyword',''+result+'');
										if(value == 'WS-PT-N'){
											 $("#wastagespan").text('Wastage Percent Net');
											 $('#wastage').attr('chargeid',5);
										}else if(value == 'WS-WT'){
											 $("#wastagespan").text('Wastage Flat Weight');
											 $('#wastage').attr('chargeid',6);
										 }else if(value == 'WS-PT-G'){
											 $("#wastagespan").text('Wastage % Gross');
											 $('#wastage').attr('chargeid',7);
										 }
									 }else if(value == 'MC-GM-N' || value == 'MC-FT' || value == 'MC-PI' || value == 'MC-GM-G'){
										 $('#makingiconchange').attr('keywordcalc',''+result+'');
										$('#making').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
										$('#making').attr('keywordrange',''+keyword+'');
										$('#makingspan').attr('spankeyword',''+result+'');
										if(value == 'MC-GM-N'){
											 $("#makingspan").text('Making Per Gram Net');
											 $('#making').attr('chargeid',2);
										 }else if(value == 'MC-FT'){
											 $("#makingspan").text('Making Flat');
											 $('#making').attr('chargeid',3);
										 }else if(value == 'MC-PI'){
											 $("#makingspan").text('Making Per Piece');
											 $('#making').attr('chargeid',4);
										 }else if(value == 'MC-GM-G'){
											 $("#makingspan").text('Making PerGram Gross');
											 $('#making').attr('chargeid',14);
										 }
                                  }else if(value == 'FC' || value == 'FC-WT'){
										$('#flaticonchange').attr('keywordcalc',''+result+'');
										$('#flat').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
										$('#flat').attr('keywordrange',''+keyword+'');
										$('#flatspan').attr('spankeyword',''+result+'');
										if(value == 'FC'){
												 $("#flatspan").text('Flat Amount');
												 $('#flat').attr('chargeid',12);
										 }else if(value == 'FC-WT'){
											 $("#flatspan").text('Flat Weight');
											 $('#flat').attr('chargeid',19);
										 }
                                  }else if(value == 'HM-FT' || value == 'HM-PI'){
									  $('#hallmarkiconchange').attr('keywordcalc',''+result+'');
										$('#hallmark').attr('keywordcalc',''+result+'');
										$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
										$('#hallmark').attr('keywordrange',''+keyword+'');
										$('#hallmarkspan').attr('spankeyword',''+result+'');
                                     if(value == 'HM-FT'){
										 $("#hallmarkspan").text('Hallmark Flat');
										 $('#hallmark').attr('chargeid',8);
									 }else if(value == 'HM-PI'){
										 $("#hallmarkspan").text('Hallmark Per Piece');
										 $('#hallmark').attr('chargeid',9);
									 }
								  }
									 $("#itemtagform .chargedetailsdiv span[id="+result).text(0);
									chargecalc(keyword,0,result);
								}									
							});
							$('#previouscharge').val(msg['chargedetails']);
							//$('.chargedetailsdiv').addClass('hidedisplay');
					}
			  }				
			},
		});
}
function chargecalc(keyword,chargevalue,result){
	var pieces = $('#pieces').val();
	var netwt = $('#netweight').val();
	var grosswt = $('#grossweight').val();
    switch(keyword) {
			case 'HM-FT.MIN':  // HALL MARK FLAT MIN
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
                break;
			case 'HM-FT.MAX':  // HALL MARK FLAT MAX
			    var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
			    break;
			case 'HM-PI.MIN':  // HALL MARK PER PIECE MIN
				var calfinalvalue=(parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
                break;
			case 'HM-PI.MAX':  // HALL MARK PER PIECE MAX
				var calfinalvalue=(parseFloat(pieces)*parseFloat(chargevalue)).toFixed(roundamount);
                break;	
			case 'CC-FT.MIN':  // CERTIFICATION  FLAT MIN
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
                break;
			case 'CC-FT.MAX':  // CERTIFICATION  FLAT MAX
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(roundamount);
                break;	
			case 'WS-PT-N.MIN':  // WASTAGE PERCENT NET MIN
				var calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round); 
                break;
			case 'WS-PT-N.MAX':  // WASTAGE PERCENT NET MAX
				var calfinalvalue=(parseFloat(netwt)*(parseFloat(chargevalue)/100)).toFixed(round); 
                break;	
			case 'WS-WT.MIN':  // WASTAGE  FLAT WEIGHT MIN
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
                break; 
			case 'WS-WT.MAX':  // WASTAGE  FLAT WEIGHT MAX
				var calfinalvalue=(parseFloat(chargevalue)).toFixed(round); 
                break;	
			case 'WS-PT-G.MIN':  // WASTAGE PERCENT GROSS MIN
				var calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
                break;
			case 'WS-PT-G.MAX':  // WASTAGE PERCENT GROSS MAX
				var calfinalvalue=(parseFloat(grosswt)*(parseFloat(chargevalue)/100)).toFixed(round); 
                break;	
			case 'MC-GM-N.MIN':  // MAKING  PER GRAM NET MIN
				var calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-GM-N.MAX':  // MAKING  PER GRAM NET MAX
				var calfinalvalue=(parseFloat(netwt)*parseFloat(chargevalue)).toFixed(roundamount);
				break;
			case 'MC-FT.MIN':  // MAKING  FLAT MIN
				var calfinalvalue=parseFloat(chargevalue);
                break;
			case 'MC-FT.MAX':  // MAKING  FLAT MAX
				var calfinalvalue=parseFloat(chargevalue);
                break;
			case 'FC.MIN':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'FC.MAX':  // FLAT CHARGES MIN
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'FC-WT.MIN':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
			break;	
			case 'FC-WT.MAX':  // FLAT CHARGES MAX
				calfinalvalue=parseFloat(chargevalue);
				break;
			case 'MC-PI.MIN':  // MAKING  PER PIECE MIN
				var calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
                break;
            case 'MC-PI.MAX':  // MAKING  PER PIECE MAX
				var calfinalvalue=parseFloat(pieces)*parseFloat(chargevalue);
                break;				
			case 'MC-GM-G.MIN':  // MAKING  PER GRAM GROSS MIN
				var calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
                break;
			case 'MC-GM-G.MAX':  // MAKING  PER GRAM GROSS MAX
				var calfinalvalue=(parseFloat(grosswt)*parseFloat(chargevalue)).toFixed(roundamount);
                break;	
				default:
				
		  }
		  $("#itemtagform input[keywordcalc="+result).val(calfinalvalue);
		  Materialize.updateTextFields();
}
function stonedivhideshow(stoneapplicable){
	var tagtypeid = $('#tagtypeid').find('option:selected').val();
			if(stoneapplicable == 'Yes'){  // stone hide/show based on product
	            if(tagtypeid == 5 || tagtypeid == 3) {  //price tag
				 $('#stoneweight-div').hide();
				}else{
				 $('#stoneweight-div').show();
				}
			}else{
				$('#stoneweight-div').hide();
			}
}
function imagetabhideshow(){
	if(imagestatus == 'YES'){
		$('#tab2').show();
	}else{
		$('#tab2').hide();
	}
}
function nontagtocounterhide(val){
	    var stocktypeid = 'nontagtocounter';
		$("#"+stocktypeid+" option").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" optgroup").removeClass("ddhidedisplay");
		$("#"+stocktypeid+" option").prop('disabled',false);
		$("#"+stocktypeid+" option[value="+val+"]").addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+val+"]").closest('optgroup').addClass("ddhidedisplay");
		$("#"+stocktypeid+" option[value="+val+"]").prop('disabled',true);
}
function checkmaxquantity(counterid){
	$.ajax({
			url:base_url+"Itemtag/checkmaxquantity",
            data:{counterid:counterid},
			type:"post",
			datatype :"json",
			async:false,
			cache:false,
			success :function(msg) {
				var maxquantity = $('#counterid').find('option:selected').data('maxquantity');
				if($.trim(msg) >= maxquantity){
					var countername = $('#counterid').find('option:selected').text();
					$('#counterid').select2('val','').trigger('change');
					alertpopup('Counter Name : '+countername+' has already '+maxquantity+' nos quantity and that is the max qty it can have');
				    return false;
				}
			},
		});
}
function loadproductbasedcounteritem(prodid,counterid) { 
	var val = $("#"+prodid+"").find('option:selected').val();
	if(checkValue(val) == true) {
		var counteriddata = $('#'+prodid+'').find('option:selected').data('counterid');	
		$("#"+counterid+" option").addClass("ddhidedisplay");
		$("#"+counterid+" optgroup").addClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',true);
		if(checkValue(counterid) == true) {
			var counterids = counteriddata.toString().split(','); 
			for(var mk = 0; mk < counterids.length; mk++) {
				$("#"+counterid+" option[value='"+counterids[mk]+"']").closest('optgroup').removeClass("ddhidedisplay");
				$("#"+counterid+" option[value='"+counterids[mk]+"']").removeClass("ddhidedisplay");	
				$("#"+counterid+" option[value='"+counterids[mk]+"']").prop('disabled',false);
				if(storagedisplay != '1') {
					var storagevalue = '';
				 	if(storagedisplay == '2'){
						storagevalue = '3';
					} else if(storagedisplay == '3') {
						storagevalue = '2';
					}
					$("#"+counterid+" option[data-countertypeid='"+storagevalue+"']").addClass("ddhidedisplay");	
					$("#"+counterid+" option[data-countertypeid='"+storagevalue+"']").prop('disabled',true);
				}			
				if(counterids.length == 1) {
					if(counterusestatus == 'yes') {
						$("#"+counterid+"").prop('disabled',true);
						$("#"+counterid+"").select2('val',counterids[mk]).trigger('change');
					}
				} else if(counterids.length > 1) {
					if(counterusestatus == 'yes') {
						$("#"+counterid+"").prop('disabled',false);
					}
				} else {
					$("#"+counterid+"").select2('val','');
				}
			}
		}
	} else {
		$("#"+counterid+" option").removeClass("ddhidedisplay");
		$("#"+counterid+" optgroup").removeClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',false);	
	}
	$("#"+counterid+" option:enabled").closest('optgroup').removeClass("ddhidedisplay");	
}
function tocounterhideshow(fromcounterid,tocounterid,counterid){
	$("#"+counterid+" option").addClass("ddhidedisplay");
	$("#"+counterid+" option").prop('disabled',true);
	var counterids = tocounterid.toString().split(','); 
	for(var mk = 0; mk < counterids.length; mk++) {
		$("#"+counterid+" option[value='"+counterids[mk]+"']").removeClass("ddhidedisplay");	
		$("#"+counterid+" option[value='"+counterids[mk]+"']").prop('disabled',false);
	}
	$("#"+counterid+" option[value="+fromcounterid+"]").addClass("ddhidedisplay");
	$("#"+counterid+" option[value="+fromcounterid+"]").prop('disabled',true);
	$("#"+counterid+" option[data-counterdefaultid='2']").removeClass("ddhidedisplay");	
	$("#"+counterid+" option[data-counterdefaultid='2']").prop('disabled',false);
	$("#"+counterid+" option[data-counterdefaultid='4']").removeClass("ddhidedisplay");	
	$("#"+counterid+" option[data-counterdefaultid='4']").prop('disabled',false);
	$("#"+counterid+" option[data-counterdefaultid='12']").removeClass("ddhidedisplay");	
	$("#"+counterid+" option[data-counterdefaultid='12']").prop('disabled',false);
	$("#"+counterid+" option[data-counterdefaultid='21']").removeClass("ddhidedisplay");	
	$("#"+counterid+" option[data-counterdefaultid='21']").prop('disabled',false);
}
//get last item tag number
function getlatitemtagno(action) {
	$.ajax({
		url:base_url+"Itemtag/getlastitemtagno",
		data:{action:action},
		method: "POST",
		datatype :"json",
		async:false,
		success :function(msg) {  
		var result = $.parseJSON(msg);
		$("#lasttagno").val(result['itemtagno']);
		$("#lastrfidno").val(result['rfidno']);
		Materialize.updateTextFields();
		},
	});
}
// size hide show based on product
function sizehideshow(productvalue) {
	if(productvalue != 1 || productvalue !='') {
		var sizeapplicable = $("#productid").find('option:selected').data('size');
		if(detailedstatus == 0) {
			$('#size').select2('val','');
		}
		if(sizeapplicable == 'Yes') {
			$("#size-div").show();
			$('#size').attr('data-validation-engine','validate[required]');
			$("#size option").addClass("ddhidedisplay");
			$("#size option").prop('disabled',true);
			$("#size option[data-productid="+productvalue+"]").removeClass("ddhidedisplay");
			$("#size option[data-productid="+productvalue+"]").prop('disabled',false);
			var length = $("#size option[data-productid="+productvalue+"]").length;
			if(length == 1) {
				var sizevalue = $("#size option[data-productid="+productvalue+"]").val();
				$('#size').select2('val',sizevalue).trigger('change');
			} else {
				$('#size').select2('val','').trigger('change');
			}
		} else {
			$('#size').attr('data-validation-engine','');
			$("#size-div").hide();
		}
		$("#size option:enabled").closest('optgroup').removeClass("ddhidedisplay");	//sapna changes	
	}
}
// Rfid hide show based on product
function rfidhideshow(value) {
	var tagtype = $('#tagtypeid').find('option:selected').val();
	if(value == 'Yes') {
		$("#rfidtagno-div").show();
		if(tagtype == 4) {
			$('#rfidtagno').attr('data-validation-engine','');
			$("#rfidtagno-div").hide();
		} else {
			//$('#rfidtagno').attr('data-validation-engine','validate[required,funcCall[rfitagnocheck],custom[onlyLetterNumber],maxSize['+rfidnoofdigits+']]');
			$('#rfidtagno').attr('data-validation-engine','validate[required,custom[onlyLetterNumber],maxSize['+rfidnoofdigits+']]');
			$('#rfidtagno').attr('readonly',false);
		}
	} else {
		$('#rfidtagno').attr('data-validation-engine','');
		$("#rfidtagno-div").hide();
	}
}
// set product trigger - based on lot choose
function setproducttriggerlot(productid) {
	loadproductbasedcounteritem('productid','counterid');
	var tagtypeid = $('#productid').find('option:selected').data('tagtypeid');
	var tagtype = $('#tagtypeid').find('option:selected').val();
	var oldval = $("#tagtypeid").val();
	if(oldval != tagtypeid) {
		if(tagtypeid != 17) {
			$("#tagtypeid").select2('val',tagtypeid);
			settagtypevaluelottagtypeid(tagtypeid);
			select2ddenabledisable('tagtypeid','disable');
		} else {
			select2ddenabledisable('tagtypeid','enable');
		}	
	}
	var stoneapplicable = $('#productid').find('option:selected').data('stone');
	stonedivhideshow(stoneapplicable);
	$("#itemtagform .chargedetailsdiv").addClass('hidedisplay');
	var tagtemplateid = $('#productid').find('option:selected').data('tagtemplateid');
	if(tagtemplateid != '' && tagtemplateid != '1'){
		$('#tagtemplateid').select2('val',tagtemplateid);
	}
	if(tagtype == '3') {
		var lstpurityid = $('#productid').find('option:selected').data('purityid');
		$('#size').select2('val','');
		$('#size').attr('data-validation-engine','');
		$("#size-div").hide();
		$('#itemcaratweight-div').hide();
		$('#itemcaratweight').attr('data-validation-engine','');
		$('#grossweight-div,#netweight-div').show();
		if(lstpurityid == 11) {
			$('#grossweight-div,#netweight-div').hide();
			$('#itemcaratweight-div').show();
			$('#itemcaratweight').attr('data-validation-engine','validate[required,min[0.01],custom[number],decval['+dia_weight_round+']]');
		}
	} else {
		sizehideshow(productid);
	}
}
// set purity trigger - based on lot
function setpuritytriggerlot(purityid) {
	var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
	if(tag_entrytype == 4  || tag_entrytype == 11 || tag_entrytype == 10  || tag_entrytype == 12 || tag_entrytype == 13){
		loadproductbasedonpurity(purityid,1);
	}
	/* else if(tag_entrytype == 3 || tag_entrytype == 10  || tag_entrytype == 12){
		$("#productid option").removeClass("ddhidedisplay");
		$("#productid optgroup").removeClass("ddhidedisplay");
		$("#productid option").prop('disabled',false); 
		var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
		var counterid = $('#fromcounterid').find('option:selected').val();
		var branchid = $('#branchid').find('option:selected').val();
		loadproductfromstock(tag_entrytype,counterid,branchid,purityid,'productid');
	}
	if(tag_entrytype == 11 ){
		$("#processproductid option").removeClass("ddhidedisplay");
		$("#processproductid optgroup").removeClass("ddhidedisplay");
		$("#processproductid option").prop('disabled',false); 
		var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
		var counterid = $('#fromcounterid').find('option:selected').val();
		var branchid = $('#branchid').find('option:selected').val();
		loadproductfromstock(tag_entrytype,counterid,branchid,purityid,'processproductid');
	} */
}
function settagtypevaluelottagtypeid(value) {
	var template = $('#tagtemplateid').find('option:selected').val();
	switch(value) {
		case 2:	//tag
			var mtype = 'tag';
			select2ddenabledisable('lotid','enable');
			select2ddenabledisable('tagtemplateid','enable');
			$("#stoneweighticon").css("opacity","0");
			$("#stoneweighticon").show();
			stoneiconset();	
			$("#tagtemplateid").select2('val',template);
			$("#stoneweighticon").css("opacity","1");
			$("#pieces").attr('readonly',true);
		break;
		case 3:	//untag				
			var mtype = 'untag';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").hide();
			stoneiconreset();
			$("#tagtemplateid").select2('val','1');
			select2ddenabledisable('tagtemplateid','disable');
			$("#pieces").attr('readonly',false);
		break;
		case 4:	//bulktag
			var mtype = 'bulktag';
			select2ddenabledisable('lotid','enable');
			select2ddenabledisable('tagtemplateid','enable');
			$("#stoneweighticon").show();
			$("#tagtemplateid").select2('val',template);
			stoneiconset();
			$("#pieces").attr('readonly',true);	
		break;
		case 5:	//pricetag
			var mtype = 'pricetag';
			select2ddenabledisable('lotid','enable');
			select2ddenabledisable('tagtemplateid','enable');
			$("#tagtemplateid").select2('val',template);
			$("#stoneweighticon").hide();
			stoneiconreset();	
			$("#pieces").attr('readonly',true);
		break;
		case 6:	//untagtotag
			var mtype = 'untagtotag';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").show();
			stoneiconset();	
			$("#pieces").attr('readonly',true);
		break;
		case 10:	//salesreturn				
			var mtype = 'salesreturn';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").hide();
			stoneiconreset();
			select2ddenabledisable('tagtemplateid','enable');
			$("#tagtemplateid").select2('val','1');
			select2ddenabledisable('tagtemplateid','disable');
			$("#pieces").attr('readonly',true);
		break;
		case 11:	//oldpurchase				
			var mtype = 'oldpurchase';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").hide();
			stoneiconreset();
			select2ddenabledisable('tagtemplateid','enable');
			$("#tagtemplateid").select2('val','1');
			select2ddenabledisable('tagtemplateid','disable');
			$("#pieces").attr('readonly',true);
		break;
		case 12:	//partialtag				
			var mtype = 'partialtag';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").show();
			stoneiconset();
			select2ddenabledisable('tagtemplateid','enable');
			$("#tagtemplateid").select2('val','1');
			select2ddenabledisable('tagtemplateid','disable');
			$("#pieces").attr('readonly',true);
		break;
		case 13:	//purchasereturn				
			var mtype = 'purchasereturn';
			select2ddenabledisable('lotid','enable');
			$("#stoneweighticon").hide();
			stoneiconreset();
			select2ddenabledisable('tagtemplateid','enable');
			$("#tagtemplateid").select2('val','1');
			select2ddenabledisable('tagtemplateid','disable');
			$("#pieces").attr('readonly',true);
		break;
	}
	if(checkValue(mtype) == true) {
		fieldruleset('validation_property',''+mtype+'_0');
	}
}
function checklotpiecelimit() {
	var pendinglotpieces = $('#pendinglotpieces').val();
	var pieces = $('#pieces').val();
	if(parseFloat(pieces) > parseFloat(pendinglotpieces)) {
		return 0;
	} else {
		return 1;
	}
}
function checklotweightlimit() {
	var grossweight = $('#grossweight').val();
	var caratweight = $('#itemcaratweight').val();
	var pieces = $('#pieces').val();
	var tagtype = $('#tagtypeid').find('option:selected').val();
	var purityid = $('#purityid').find('option:selected').val();
	if(tagtype == 4) { //bulk tag
		var grossweight = parseFloat(parseFloat(grossweight) * parseFloat(pieces));
	}
	if(purityid == 11) {
		var lotdiaweightlimit = $('#lotdiaweightlimit').val();
		var pendinglotcaratwt = $('#pendinglotcaratwt').val();
		var lotcaratweight = $('#lotid').find('option:selected').attr('data-lotcaratwt');
		if(tolerance_lot == 1) {
			var finallotlimit = ((parseFloat(lotcaratweight)*parseFloat($('#lottolerancediactvalue').val())/100)) + parseFloat(pendinglotcaratwt);
		} else {
			var finallotlimit = parseFloat(lotdiaweightlimit) + parseFloat(pendinglotcaratwt);
		}
		if(parseFloat(caratweight) > finallotlimit) {
		return 0;
		} else {
			return 1;
		}
	} else {
		var lotweightlimit = $('#lotweightlimit').val();
		var pendinglotgrosswt = $('#pendinglotgrosswt').val();
		var lotweight = $('#lotid').find('option:selected').attr('data-lotgrosswt');
		if(tolerance_lot == 1) {
			var finallotlimit = ((parseFloat(lotweight)*parseFloat($('#lottolerancevalue').val())/100)) + parseFloat(pendinglotgrosswt);
		} else {
			var finallotlimit = parseFloat(lotweightlimit) + parseFloat(pendinglotgrosswt);
		}
		if(parseFloat(grossweight) > finallotlimit) {
		return 0;
		} else {
			return 1;
		}
	}
}
function checklotnetweightlimit() {
	var pieces = $('#pieces').val();
	var tagtype = $('#tagtypeid').find('option:selected').val();
	var netweight = $('#netweight').val();
	if(tagtype == 4) { //bulk tag
		var netweight = parseFloat(parseFloat(netweight) * parseFloat(pieces));
	}
	var lotweightlimit = $('#lotweightlimit').val();
	var pendinglotnetwt = $('#pendinglotnetwt').val();
	var lotnetweight = $('#lotid').find('option:selected').attr('data-lotnetwt');
	if(tolerance_lot == 1) {
		var finallotlimit = ((parseFloat(lotnetweight)*parseFloat($('#lottolerancevalue').val())/100)) + parseFloat(pendinglotnetwt);
	} else {
		var finallotlimit = parseFloat(lotweightlimit) + parseFloat(pendinglotnetwt);
	} 
	if(parseFloat(netweight) > finallotlimit) {
		return 0;
	} else {
		return 1;
	}
}
function checklotstoneweightlimit() {
	var stoneweight = $('#stoneweight').val();
	var pendinglotstonewt = $('#pendinglotstonewt').val();
	if(parseFloat(stoneweight) > pendinglotstonewt) {
		return 0;
	} else {
		return 1;
	}
}
function allfieldsdisable() {
	$('#tagentrytypeid,#tagtypeid,#totallotgrosswt,#totallotnetwt,#totallotpieces,#completedlotgrosswt,#completedlotnetwt,#completedlotpieces,#pendinglotgrosswt,#pendinglotnetwt,#pendinglotpieces,#branchid,#date,#itemrate,#itemratewithgst,#grossweight,#stoneweight,#netweight,#productid,#purityid,#lotid').prop('disabled',true);
	$('#stoneweighticon').hide();
	var tagtypeid = $('#tagtypeid').find('option:selected').val();
	if(tagtypeid == 3) {
		$('#pieces').prop('readonly',false);
	} else {
		$('#pieces').prop('readonly',true);
	}
}
// restrit decimal value based on weight round from company settings
function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}
// Process product option hide/show based on from type
function processproducthideshow(tag_entrytype) {
	var ddname = "processproductid";
	var hideval = '';
	if(tag_entrytype == 11 || tag_entrytype == 13) {
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		if(tag_entrytype == 11) { // old jewels
			hideval = 3;
		}else if(tag_entrytype == 13) { // Purchase return
			hideval = 4;
		}
		$("#"+ddname+" option[data-categoryid="+hideval+"]").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid="+hideval+"]").prop('disabled',false);
	} else if(tag_entrytype == 10 || tag_entrytype == 12) { // Partial tag  & sales return
		$("#"+ddname+" option").removeClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',false);
		$("#"+ddname+" option[data-categoryid=3]").addClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid=3]").prop('disabled',true);
		$("#"+ddname+" option[data-categoryid=4]").addClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid=4]").prop('disabled',true);
	}
}
// Process product filter based on purity id
function producthideshowbasedonpurity(tagentrytypeid,purityid) {
	var ddname = "processproductid";
	var hideval = '';
	var tagtype = $.trim($("#tagtypeid").find('option:selected').val());
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	$("#processproductid").select2('val','');
	if(tagentrytypeid == 11 || tagentrytypeid == 13) {
		if(tagentrytypeid == 11) { // old jewels
			hideval = 3;
		} else if(tagentrytypeid == 13) { // Purchase return
			hideval = 4;
		}
		if(tagtype != '') {
			$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"'][data-tagtypeid="+tagtype+"]").removeClass("ddhidedisplay");	
			$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"'][data-tagtypeid="+tagtype+"]").prop('disabled',false);
		}
		$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"'][data-tagtypeid=17]").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"'][data-tagtypeid=17]").prop('disabled',false);
	} else if(tagentrytypeid == 10 || tagentrytypeid == 12) { // Partial tag  & sales return
		if(tagtype != '') {
			$("#"+ddname+" option[data-purityid*='"+purityid+"'][data-tagtypeid="+tagtype+"]").removeClass("ddhidedisplay");	
			$("#"+ddname+" option[data-purityid*='"+purityid+"'][data-tagtypeid="+tagtype+"]").prop('disabled',false);
		}
		$("#"+ddname+" option[data-purityid*='"+purityid+"'][data-tagtypeid=17]").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-purityid*='"+purityid+"'][data-tagtypeid=17]").prop('disabled',false);
		$("#"+ddname+" option[data-categoryid=3]").addClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid=3]").prop('disabled',true);
		$("#"+ddname+" option[data-categoryid=4]").addClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid=4]").prop('disabled',true);
	}
}
// Process product option hide/show based on to storage in tag & nontag transfer overlay
function tagtransprocessproducthideshow(tag_entrytype,ddname) {
	var hideval = '';
	var purityid = $('#purity').val();
	if(tag_entrytype == 2 || tag_entrytype == 4) {
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		if(tag_entrytype == 2) { // old jewels
			hideval = 3;
		} else if(tag_entrytype == 4) { // Purchase return
			hideval = 4;
		}
		$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"']").removeClass("ddhidedisplay");	
		$("#"+ddname+" option[data-categoryid="+hideval+"][data-purityid*='"+purityid+"']").prop('disabled',false);
	}
}
// non tag product hide/show based on purity
function nontagloadproductbasedonpurity(purityid) {
	$("#nontagproductid").select2('val','');
	$("#nontagproductid option").addClass("ddhidedisplay");
	$("#nontagproductid option").prop('disabled',true);
	$("#nontagproductid option[data-purityid*='"+purityid+"']").removeClass("ddhidedisplay");
	$("#nontagproductid option[data-purityid*='"+purityid+"']").prop('disabled',false);
}
//Enable Disable form fields userbased using Roles
function enabledisableuserformfieds(purityid) {
	$("#date,#tagweight,#packingweight,#packwtless,#chargefrom").prop('disabled',false);
}
//Tag Auto image - Enable/Disable field
function tagautoimage() {
	$('#mtagautoimage').select2('val',$('#tagautoimage').val()).trigger('change');
	if($('#tagautoimage').val() == 1) {
		$('#mtagautoimage').prop('disabled',false);
	} else {
		$('#mtagautoimage').prop('disabled',true);
	}
}
//Check available status for itemtag
function checkitemtagstatus(datarowid) {
	if(datarowid > '1') {
		$.ajax({
			url: base_url +"Itemtag/checkitemtagstatus",
			data: "datarowid=" + datarowid,
			dataType:'json',
			type: "POST",
			success: function(data) {
				$('#missingitemoverlay').fadeIn();
				if(data['missingdescription'] != '') {
					$('#missingitemdate').val(data['missingdate']);
					$('#missingitemstatus').select2('val',26).prop('disabled',true);
					$('#missingtagnumber').val(data['itemtagnumber']).trigger('change');
					if(data['itemtagnumber'] != '') {
						$('#missingrfidno').val(data['itemtagnumber']).trigger('change');
					}
					$('#missingdescription').val(data['missingdescription']).trigger('change');
					$('#missingitemdate').prop('disabled',true);
					$("#missingtagnumber,#missingrfidno,#missingdescription").attr('readonly', true);
					$('#addmissingitemform').hide();
				} else {
					$('#missingitemdate').prop('disabled',false);
					$('#missingdescription').attr('readonly',false);
					getcurrentsytemdate('missingitemdate');
					$('#missingitemstatus').select2('val',26).prop('disabled',true);
					$('#missingtagnumber,#missingrfidno,#missingdescription').val('');
					setTimeout(function(){
						var tagnumber = getgridcolvalue('itemtaggrid',datarowid,'itemtagnumber','');
						var rfidtagno = getgridcolvalue('itemtaggrid',datarowid,'rfidtagno','');
						$('#missingtagnumber').val(tagnumber).trigger('change');
						if(rfidtagno != '') {
							$('#missingrfidno').val(rfidtagno).trigger('change');
						}
						$("#missingtagnumber,#missingrfidno").attr('readonly', true);
						$('#addmissingitemform').show();
						Materialize.updateTextFields();
					},100);
				}
			}
		});
	}
}
// Retrieve Grid Details to Form Data Set
function retrievegriddetailstofrom(salesdetailid,stocktype) {
	$.ajax({
		url: base_url +"Itemtag/retrievegriddetailstofrom",
        data: "salesdetailid=" + salesdetailid+"&stocktype="+stocktype,
		dataType:'json',
		type: "POST",
		success: function(data) {
			if(stocktype == 'Tag' && data != '') {
				$('#tagtypeid').select2('val',data['tagtypeid']).trigger('change');
				$('#fromcounterid').select2('val',7).trigger('change');
				$('#tagtemplateid').select2('val',data['tagtemplateid']).trigger('change');
				$('#purityid').select2('val',data['purityid']).trigger('change');
				$('#processproductid,#productid').select2('val',data['productid']).trigger('change');
				$('#accountid').select2('val',data['accountid']).trigger('change');
				$('#counterid').select2('val',data['counterid']).trigger('change');
				$('#grossweight').val(data['grossweight']);
				$('#stoneweight').val(data['stoneweight']);
				$('#netweight').val(data['netweight']);
				$('#pieces').val(data['pieces']);
				$('#size').select2('val',data['size']).trigger('change');
				var productaddoninfoid = data['productaddoninfoid'].split(',');
				if(productaddoninfoid != ''){
					$('#productaddoninfoid').select2('val',productaddoninfoid).trigger('change');
				}
				$('#certificationno').val(data['certificationno']);
				//Stone Entry grid load
				if(data['stoneentry'] != ''){
					var stonedata = data['stoneentry'];
					$("#hiddenstonedata").val(stonedata);
					getstoneentrydiv(data['itemtagid']);
				}
				{ // Set Product Charges details to field
					var chargecategoryidstring = data['keyword']['chargekeyword'].toString();
					var makingkey = [];
					var wastagekey = [];
					var hallmarkkey = [];
					var flatkey = [];
					var range = '';
					var firstindexwastage = 0;
					var firstindexwastagecount = 0;
					var firstwastagecharge = '';
					var firstwastagechargename = '';
					var firstwastagechargeid = '';
					var firstwastagechargekey = '';
					var firstindexmaking = 0;
					var firstindexmakingcount = 0;
					var firstmakingcharge ='';
					var firstmakingchargename ='';
					var firstmakingchargeid ='';
					var firstmakingchargekey ='';
					var firstindexflat = 0;
					var firstindexflatcount = 0;
					var firstflatcharge ='';
					var firstflatchargename ='';
					var firstflatchargeid ='';
					var firstflatchargekey ='';
					var firstindexhallmark = 0;
					var firstindexhallmarkcount = 0;
					var firsthallmarkcharge ='';
					var firsthallmarkchargename ='';
					var firsthallmarkchargeid ='';
					var firsthallmarkchargekey ='';
					$('.addonchargeiconchange').removeClass('hidedisplay');
					if (chargecategoryidstring.indexOf(',') > -1) {
						var chargedetails = chargecategoryidstring.split(",");
						if(chargedetails == '') {
						} else {
							$.each(chargedetails, function (index, value) {
								var splitdatakey = value.split(":");
								$("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
								$("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
									if(chargecalculation == 2){
										range = 'MIN';
									} else if(chargecalculation == 1) {
										range = 'MAX';
									}
									if(splitdatakey[0] == '2') {  // making
										if(firstindexmaking == 0) {
										}
										firstindexmakingcount++;
										var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										makingkey.push(makingfinal);
										$('#makingiconchange').attr('keywordarray',''+makingkey+'');
										if(firstindexmakingcount == 1){
											$('#makingiconchange').addClass('hidedisplay');
										} else {
											$('#makingiconchange').removeClass('hidedisplay');
										}
									} else if(splitdatakey[0] == '3') { // wastage
										if(firstindexwastage == 0) {
										}
										firstindexwastagecount++;
										var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										wastagekey.push(wastagefinal);
										$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
										if(firstindexwastagecount == 1){
											$('#wastageiconchange').addClass('hidedisplay');
										} else {
											$('#wastageiconchange').removeClass('hidedisplay');
										}
									} else if(splitdatakey[0] == '4') { // hallmark
										if(firstindexhallmark == 0){
										}
										firstindexhallmarkcount++;
										var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										hallmarkkey.push(hallmarkfinal);
										$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
										if(firstindexhallmarkcount == 1) {
											$('#hallmarkiconchange').addClass('hidedisplay');
										} else {
											$('#hallmarkiconchange').removeClass('hidedisplay');
										}
									} else if(splitdatakey[0] == '6') {  // flat
										if(firstindexflat == 0) {
										}
										firstindexflatcount++;
										var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
										flatkey.push(flatfinal);
										$('#flaticonchange').attr('keywordarray',''+flatkey+'');
										if(firstindexflatcount == 1){
											$('#flaticonchange').addClass('hidedisplay');
										} else {
											$('#flaticonchange').removeClass('hidedisplay');
										}
									}
							});
						}
					} else {
						$('.addonchargeiconchange').addClass('hidedisplay');
						var splitdatakey = chargecategoryidstring.split(":");
						$("#itemtagform .charge"+splitdatakey[0]).removeClass('hidedisplay');
						$("#itemtagform .charge"+splitdatakey[0]+" .chargedata").attr('data-validation-engine','validate[required,custom[number]]');
						if(chargecalculation == 2) {
							range = 'MIN';
						} else if(chargecalculation == 1) {
							range = 'MAX';
						}
						if(splitdatakey[0] == '2') {  // making
							if(firstindexmaking == 0) {
							}
							firstindexmakingcount++;
							var makingfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
							makingkey.push(makingfinal);
							$('#makingiconchange').attr('keywordarray',''+makingkey+'');
							if(firstindexmakingcount == 1){
								$('#makingiconchange').addClass('hidedisplay');
							}else{
								$('#makingiconchange').removeClass('hidedisplay');
							}
						} else if(splitdatakey[0] == '3') { // wastage
							if(firstindexwastage == 0) {
							}
							firstindexwastagecount++;
							var wastagefinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
							wastagekey.push(wastagefinal);
							$('#wastageiconchange').attr('keywordarray',''+wastagekey+'');
							if(firstindexwastagecount == 1) {
								$('#wastageiconchange').addClass('hidedisplay');
							} else {
								$('#wastageiconchange').removeClass('hidedisplay');
							}
						} else if(splitdatakey[0] == '4') { // hallmark
							if(firstindexhallmark == 0) {
							}
							firstindexhallmarkcount++;
							var hallmarkfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
							hallmarkkey.push(hallmarkfinal);
							$('#hallmarkiconchange').attr('keywordarray',''+hallmarkkey+'');
							if(firstindexhallmarkcount == 1) {
								$('#hallmarkiconchange').addClass('hidedisplay');
							} else {
								$('#hallmarkiconchange').removeClass('hidedisplay');
							}
						} else if(splitdatakey[0] == '6') {  // flat
							if(firstindexflat == 0) {
							}
							firstindexflatcount++;
							var flatfinal = splitdatakey[2]+'-'+range+':'+splitdatakey[1]+':'+splitdatakey[2]+'.'+range+':'+splitdatakey[3];
							flatkey.push(flatfinal);
							$('#flaticonchange').attr('keywordarray',''+flatkey+'');
							if(firstindexflatcount == 1) {
								$('#flaticonchange').addClass('hidedisplay');
							} else {
								$('#flaticonchange').removeClass('hidedisplay');
							}
						}
					}
					var chargedetails = data['chargespandetails'].split(",");
					if(chargedetails == '') {
					} else {
						$.each(chargedetails, function (index, value) {
							if (value.indexOf(":") >= 0) {
								var res=value.split(":");
								var rescalc=res[0].split(".");
								var result = rescalc[0]+'-'+rescalc[1];
								if(rescalc[0] == 'WS-PT-N' || rescalc[0] == 'WS-WT' || rescalc[0] == 'WS-PT-G') {
									$('#wastageiconchange').attr('keywordcalc',''+result+'');
									$('#wastage').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
									$('#wastage').attr('keywordrange',''+res[0]+'');
									$('#wastagespan').attr('spankeyword',''+result+'');
									if(rescalc[0] == 'WS-PT-N') {
										$("#wastagespan").text('Wastage Percent Net');
										$('#wastage').attr('chargeid',5);
									} else if(rescalc[0] == 'WS-WT') {
										$("#wastagespan").text('Wastage Flat Weight');
										$('#wastage').attr('chargeid',6);
									} else if(rescalc[0] == 'WS-PT-G') {
										$("#wastagespan").text('Wastage % Gross');
										$('#wastage').attr('chargeid',7);
									}
								} else if(rescalc[0] == 'MC-GM-N' || rescalc[0] == 'MC-FT' || rescalc[0] == 'MC-PI' || rescalc[0] == 'MC-GM-G') {
									$('#makingiconchange').attr('keywordcalc',''+result+'');
									$('#making').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
									$('#making').attr('keywordrange',''+res[0]+'');
									$('#makingspan').attr('spankeyword',''+result+'');
									if(rescalc[0] == 'MC-GM-N') {
										$("#makingspan").text('Making Per Gram Net');
										$('#making').attr('chargeid',2);
									} else if(rescalc[0] == 'MC-FT') {
										$("#makingspan").text('Making Flat');
										$('#making').attr('chargeid',3);
									} else if(rescalc[0] == 'MC-PI') {
										$("#makingspan").text('Making Per Piece');
										$('#making').attr('chargeid',4);
									} else if(rescalc[0] == 'MC-GM-G') {
										$("#makingspan").text('Making PerGram Gross');
										$('#making').attr('chargeid',14);
									}
								} else if(rescalc[0] == 'FC-WT' || rescalc[0] == 'FC') {
									$('#flaticonchange').attr('keywordcalc',''+result+'');
									$('#flat').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
									$('#flat').attr('keywordrange',''+res[0]+'');
									$('#flatspan').attr('spankeyword',''+result+'');
									if(rescalc[0] == 'MC-GM-N') {
										$("#flatspan").text('flat Per Gram Net');
										$('#flat').attr('chargeid',2);
									} else if(rescalc[0] == 'MC-FT') {
										$("#flatspan").text('flat Flat');
										$('#flat').attr('chargeid',3);
									} else if(rescalc[0] == 'MC-PI') {
										$("#flatspan").text('flat Per Piece');
										$('#flat').attr('chargeid',4);
									} else if(rescalc[0] == 'MC-GM-G') {
										$("#flatspan").text('flat PerGram Gross');
										$('#flat').attr('chargeid',14);
									}
								} else if(rescalc[0] == 'HM-FT' || rescalc[0] == 'HM-PI') {
									$('#hallmarkiconchange').attr('keywordcalc',''+result+'');
									$('#hallmark').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
									$('#hallmark').attr('keywordrange',''+res[0]+'');
									$('#hallmarkspan').attr('spankeyword',''+result+'');
									if(rescalc[0] == 'HM-FT') {
										$("#hallmarkspan").text('Hallmark Flat');
										$('#hallmark').attr('chargeid',8);
									} else if(rescalc[0] == 'HM-PI') {
										$("#hallmarkspan").text('Hallmark Per Piece');
										$('#hallmark').attr('chargeid',9);
									}
								}
								$("#itemtagform .chargedetailsdiv span[id="+result).text(res[1]);
								chargecalc(res[0],res[1],result);
							} else {
								if(chargecalculation == 2) {
									var cond = 'MIN';
								} else {
									var cond = 'MAX';
								}
								var result = value+'-'+cond;
								var keyword = value+'.'+cond;
								if(value == 'WS-PT-N' || value == 'WS-WT' || value == 'WS-PT-G') {
									$('#wastageiconchange').attr('keywordcalc',''+result+'');
									$('#wastage').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=wastagespankey").attr('id',''+result+'');
									$('#wastage').attr('keywordrange',''+keyword+'');
									$('#wastagespan').attr('spankeyword',''+result+'');
									if(value == 'WS-PT-N') {
										$("#wastagespan").text('Wastage Percent Net');
										$('#wastage').attr('chargeid',5);
									} else if(value == 'WS-WT') {
										$("#wastagespan").text('Wastage Flat Weight');
										$('#wastage').attr('chargeid',6);
									} else if(value == 'WS-PT-G') {
										$("#wastagespan").text('Wastage % Gross');
										$('#wastage').attr('chargeid',7);
									}
								} else if(value == 'MC-GM-N' || value == 'MC-FT' || value == 'MC-PI' || value == 'MC-GM-G') {
									$('#makingiconchange').attr('keywordcalc',''+result+'');
									$('#making').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=makingspankey").attr('id',''+result+'');
									$('#making').attr('keywordrange',''+keyword+'');
									$('#makingspan').attr('spankeyword',''+result+'');
									if(value == 'MC-GM-N') {
										$("#makingspan").text('Making Per Gram Net');
										$('#making').attr('chargeid',2);
									} else if(value == 'MC-FT') {
										$("#makingspan").text('Making Flat');
										$('#making').attr('chargeid',3);
									} else if(value == 'MC-PI') {
										$("#makingspan").text('Making Per Piece');
										$('#making').attr('chargeid',4);
									} else if(value == 'MC-GM-G') {
										$("#makingspan").text('Making PerGram Gross');
										$('#making').attr('chargeid',14);
									}
								} else if(value == 'FC' || value == 'FC-WT') {
									$('#flaticonchange').attr('keywordcalc',''+result+'');
									$('#flat').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=flatspankey").attr('id',''+result+'');
									$('#flat').attr('keywordrange',''+keyword+'');
									$('#flatspan').attr('spankeyword',''+result+'');
									if(value == 'FC') {
										$("#flatspan").text('Flat Amount');
										$('#flat').attr('chargeid',12);
									} else if(value == 'FC-WT') {
										$("#flatspan").text('Flat Weight');
										$('#flat').attr('chargeid',19);
									}
								} else if(value == 'HM-FT' || value == 'HM-PI') {
									$('#hallmarkiconchange').attr('keywordcalc',''+result+'');
									$('#hallmark').attr('keywordcalc',''+result+'');
									$("#itemtagform .chargedetailsdiv span[keyname=hallmarkspankey").attr('id',''+result+'');
									$('#hallmark').attr('keywordrange',''+keyword+'');
									$('#hallmarkspan').attr('spankeyword',''+result+'');
									if(value == 'HM-FT') {
										$("#hallmarkspan").text('Hallmark Flat');
										$('#hallmark').attr('chargeid',8);
									} else if(value == 'HM-PI') {
										$("#hallmarkspan").text('Hallmark Per Piece');
										$('#hallmark').attr('chargeid',9);
									}
								}
								$("#itemtagform .chargedetailsdiv span[id="+result).text(0);
								chargecalc(keyword,0,result);
							}
						});
						$('#previouscharge').val(msg['chargedetails']);
					}
				}
			} else {
				
			}
		}
	});
}
// Return Items Overlay Grid
function returnitemsdetailgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#returnitemsdetailgrid").width();
	var wheight = $("#returnitemsdetailgrid").height();
	/*col sort*/
	var sortcol = $("#returnitemssortcolumn").val();
	var sortord = $("#returnitemssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').attr('id') : '0';	
	var footername = 'sales';
	var filterid = '1';
	var conditionname = '';
	var filtervalue = '';
	var checkmultiple = 'true';
	$.ajax({
		url:base_url+"Itemtag/returnitemsgridheaderinformationfetch?maintabinfo=sales&primaryid=1"+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#returnitemsdetailgrid").empty();
			$("#returnitemsdetailgrid").append(data.content);
			$("#returnitemsdetailgridfooter").empty();
			$("#returnitemsdetailgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('returnitemsdetailgrid');
		},
	});
}
// Retrieve Live Stone details
// Order Tracking Overlay grid.
function automaticpurchasestonedetails(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#autostonegrid").width();
	var wheight = $("#autostonegrid").height();
	/*col sort*/
	var sortcol = $("#returnitemssortcolumn").val();
	var sortord = $("#returnitemssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').attr('id') : '0';	
	var footername = 'itemtag';
	var filterid = '1';
	var conditionname = '';
	var filtervalue = '';
	var checkmultiple = 'true';
	var lotid = $.trim($('#lotid').find('option:selected').val());
	var salesdetailid = $.trim($('#lotid').find('option:selected').data('lotsalesdetailid'));
	var gridstoneids = getgridcolcolumnvalue('stoneentrygrid','','stoneid');
	$.ajax({
		url:base_url+"Itemtag/autostonegridheaderinformationfetch?maintabinfo=itemtag&primaryid="+lotid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=50&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+"&salesdetailid="+salesdetailid+"&gridstoneids="+gridstoneids,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#autostonegrid").empty();
			$("#autostonegrid").append(data.content);
			$("#autostonegridfooter").empty();
			$("#autostonegridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('autostonegrid');
			var hideprodgridcol = ['stoneid','stonetypeid','stoneentrycalctypeid'];
			gridfieldhide('autostonegrid',hideprodgridcol);
		},
	});
}
function rfidalertpopup(txtmsg)	{
	$(".rfidalertinputstyle").text(txtmsg);
	$('#rfidalerts').fadeIn('fast');
	$("#rfidalertsclose").focus();
}
</script>