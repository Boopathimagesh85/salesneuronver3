$(document).ready(function() 
{ 
	{//Foundation Initialization
		$(document).foundation();
	}
  viewcreationcolumngrid();
  whereclausegrid();
  prefix=$('#prefix').val();
  suffix=$('#suffix').val();
 {// View by drop down change
    $('#dynamicdddataview').change(function(){
    	viewcreationcolumngrid();
    });
}
{// crud operations
	//Mergetext add icon
	$('#addicon').click(function(){
		resetFields();
		addslideup('mergetextcreationview','mergetextformadd');
		$('.updatebtnclass').addClass('hidedisplay');
		$("#mergetextadd").show();
		$('.addbtnclass').removeClass('hidedisplay');
		$('#roundtype').select2('val',6).trigger('change');
		firstfieldfocus();
		Materialize.updateTextFields();
	});
	$("#dataaddsbtn").click(function(){
		 $("#viewcreationcolumnaddwizard").validationEngine('validate');
	});
	$("#viewcreationcolumnaddwizard").validationEngine(
	{
		onSuccess: function(){
			addform();
		},
		onFailure: function() {				
			var dropdownid =[''];
			dropdownfailureerror(dropdownid);				
		}
	});
	//Edit
	$("#editicon").click(function(){
		var datarowid = $('#viewcreationcolumngrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$('.editformsummmarybtnclass').addClass('hidedisplay');
			setTimeout(function(){
				addslideup('mergetextcreationview','mergetextformadd');
				$("#mergetextclearicon").hide();
			},10);
			$('.addbtnclass,.clonebtnclass').addClass('hidedisplay');
			$('.updatebtnclass').removeClass('hidedisplay');
				retrive(datarowid);
		} else {
			alertpopup(selectrowalert);
		}
	});
	$("#dataupdatesubbtn").click(function(){
		$("#viewcreationcolumneditwizard").validationEngine('validate');
	});
	$("#viewcreationcolumneditwizard").validationEngine(
			{
				onSuccess: function()
				{
					updateform();
				},
				onFailure: function()
				{		
					var dropdownid =['4','moduleid','uitypeid','tabsectionid','roundtype'];
					dropdownfailureerror(dropdownid);	
					alertpopup(validationalert);
				}
			});
	$("#deleteicon").click(function(){
		var datarowid = $('#viewcreationcolumngrid div.gridcontent div.active').attr('id'); 
		if(datarowid){
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
				
		} else {
			alertpopup("Please select a row");
		}	
	});
	$( window ).resize(function() {
		maingridresizeheightset('viewcreationcolumngrid');
	});
	//stone charge yes delete
	$("#basedeleteyes").click(function() { 
		var datarowid = $('#viewcreationcolumngrid div.gridcontent div.active').attr('id'); 
		$.ajax({
			url: base_url + "Viewcreationcolumn/delete?primaryid="+datarowid,
			success: function(msg) {				
				var nmsg =  $.trim(msg);
				refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == "SUCCESS") {
					alertpopup(deletealert);
				} else  {					
					alertpopup(submiterror);
				}				
			},
		});
	});
	//mergetext close icon
	var addcloseinfo =["closeaddform","mergetextcreationview","mergetextformadd",""];
	addclose(addcloseinfo);
}
{ // change event
	$('#moduleid').change(function() 
	{
		$('#relatedmoduleid').empty();
		$("#relatedmoduleid").append($("<option></option>"));
		var moduleid = $(this).val();
		loadtabsection(moduleid);
		if(checkValue(moduleid) == true){
			$.ajax({
				url: base_url + "Base/relatedmodulelistfetchmodel",			
				data: "moduleid="+moduleid,
				dataType:'json',
				type: "POST",
				async:false,
				cache:false,
				success: function(data) {					
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++){
						if(moduleid != data[m]['datasid']){
						newddappend += "<option value ='"+data[m]['datasid']+"' data-mergemoduleidhidden='"+data[m]['datasid']+"' data-mergemoduletype='"+data[m]['datatype']+"'>"+data[m]['dataname']+ " </option>";
						}
					}
					//after run
					$('#relatedmoduleid').append(newddappend);
				},
			});
		} 
		//loadcolumnname(moduleid);
		Materialize.updateTextFields();
	});
	$('#relatedmoduleid').change(function() {
		var moduleid = $('#moduleid').val();
		$('#relatedmodule').val('');
		var data1 = $(this).val();
		$('#relatedmodule').val(data1);
		loadcolumnname(moduleid);
	});
	$('#whereclauseicon').click(function(){
		$('#mergetextquery').val('');
		$('#whereclausesearchoverlay').fadeIn();
		clearinlinesrchandrgrid('whereclausegrid');
	});
	$("#whereclausesearchclose").click(function(){
		$("#whereclausesearchoverlay").fadeOut();
	});
	$('#whereclauseokicon').click(function(){
		var uniqueid=$.trim($('#querybox').val());
		var uniqueidsplit=uniqueid.split(' ');
		var uniqueidarray=[];
		var uniqueroundarray=[];
		for(var i=0; i<uniqueidsplit.length; i++) {
			if (uniqueidsplit[i].match('^'+prefix)&& uniqueidsplit[i].match(suffix+'$')) {
				uniqueidarray.push(uniqueidsplit[i]);
				uniqueroundarray.push('');
			}
			if (uniqueidsplit[i].match('^round')|| uniqueidsplit[i].match('round$')) {
			    uniqueidarray.push('');
				uniqueroundarray.push(uniqueidsplit[i]);
			}
	   }
		loadquery(uniqueidarray,uniqueroundarray);
		Materialize.updateTextFields();
	});
		
 }
//mention plugin
$.getJSON(base_url+'Viewcreationcolumn/loadautocompletedata', function(data) {
 $("#querybox").mention({
	users: data,
	after : " "
});
});
$('#linkableoption').change(function(){
	var val = $(this).val();
	if(val == 'Yes'){
		$('.relatedmodulediv').removeClass('hidedisplay');
	}else{
		$('.relatedmodulediv').addClass('hidedisplay');
		$('#relatedmoduleid').select2('val','');
	}
});
});
// main view
function viewcreationcolumngrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#viewcreationcolumngrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.viewcreationcolumnheadercolsort').hasClass('datasort') ? $('.viewcreationcolumnheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.viewcreationcolumnheadercolsort').hasClass('datasort') ? $('.viewcreationcolumnheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.viewcreationcolumnheadercolsort').hasClass('datasort') ? $('.viewcreationcolumnheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = '';
	$.ajax({
		url:base_url+"Viewcreationcolumn/gridinformationfetch?viewid="+userviewid+"&maintabinfo=viewcreationcolumns&primaryid=viewcreationcolumnid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=52'+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#viewcreationcolumngrid').empty();
			$('#viewcreationcolumngrid').append(data.content);
			$('#viewcreationcolumngridfooter').empty();
			$('#viewcreationcolumngridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('viewcreationcolumngrid');
			{//sorting
				$('.viewcreationcolumnheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.viewcreationcolumnheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#viewcreationcolumnpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.viewcreationcolumnheadercolsort').hasClass('datasort') ? $('.viewcreationcolumnheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.viewcreationcolumnheadercolsort').hasClass('datasort') ? $('.viewcreationcolumnheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					viewcreationcolumngrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('viewcreationcolumnheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#viewcreationcolumnpgnumcnt li .page-text .active').data('rowcount');
					viewcreationcolumngrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#viewcreationcolumnpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					viewcreationcolumngrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#viewcreationcolumnpgnumcnt li .page-text .active').data('rowcount');
					viewcreationcolumngrid(page,rowcount);
				});
				$('#viewcreationcolumnpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#viewcreationcolumnpgnum li.active').data('pagenum');
					viewcreationcolumngrid(page,rowcount);
				});
			}
		},
	});
}
function whereclausegrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#whereclausegrid').width();
	var wheight = $('#whereclausegrid').height();
	/*col sort*/
	var sortcol = $('.whereclausecreationheadercolsort').hasClass('datasort') ? $('.whereclausecreationheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.whereclausecreationheadercolsort').hasClass('datasort') ? $('.whereclausecreationheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.whereclausecreationheadercolsort').hasClass('datasort') ? $('.whereclausecreationheadercolsort.datasort').attr('id') : '0';
	var filterid = '';
	var userviewid =126;
	var viewfieldids =55;
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=whereclausecreation&primaryid=whereclausecreationid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#whereclausegrid').empty();
			$('#whereclausegrid').append(data.content);
			$('.whereclausegridfootercontainer').empty();
			$('.whereclausegridfootercontainer').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('whereclausegrid');
			{//sorting
				$('.whereclausecreationheadercolsort').click(function(){
					$('.whereclausecreationheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#whereclausecreationpgnumcnt li .page-text .active').data('rowcount');
					whereclausegrid(page,rowcount);
				});
				sortordertypereset('whereclausecreationheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#whereclausecreationpgnumcnt li .page-text .active').data('rowcount');
					whereclausegrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#whereclausecreationpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					whereclausegrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#whereclausecreationpgnumcnt li .page-text .active').data('rowcount');
					whereclausegrid(page,rowcount);
				});
				$('#ratepgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#whereclausecreationpgnum li.active').data('pagenum');
					whereclausegrid(page,rowcount);
				});
			}
		},
	});
}
//load viewcreation column name
function loadcolumnname(moduleid) {
	var data1=$('#relatedmodule').val();
	$.ajax({
			url:base_url+"Viewcreationcolumn/loadcolumnname?moduleid="+moduleid,
			data:{moduleid:moduleid,relatedmoduleid:data1},
			dataType:'json',
			type: "POST",
			async:false,
			cache:false,
			success: function(data) {			
				$.each(data, function(index){					
					$('#viewcreationcolumnid')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])								
							.text(data[index]['viewcreationcolumnname']));											
				});	
			}
	});		
}
{ // CRUD operations
	function addform() {
		var formdata = $("#viewcreationcolumnform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		setTimeout(function() {
			$("#processoverlay").show();
		},25);
		$.ajax({
				url:base_url+"Viewcreationcolumn/viewcreationcolumncreate",
				data: "datas=" + datainformation,
				type:'POST',
				async:false,
				cache:false,
				success :function(msg) {
					resetFields();
					refreshgrid();
					setTimeout(function() {
						$("#processoverlay").hide();
							},25);
					if(msg == 'SUCCESS'){
							alertpopup(savealert);
						addslideup('mergetextformadd','mergetextcreationview');
					}else{
						alertpopup(submiterror);
				}
				}
			});
     }
	function retrive(datarowid){
		resetFields();
		firstfieldfocus();
		var elementsname=['15','viewcreationcolumnformprimaryid','viewcreationcolumnformprimaryname','moduleid','uitypeid','tabsectionid','fieldname','tablename','viewcreationcolumnname','roundtype','querybox','linkableoption','relatedmoduleid','parenttable','joincolumnname'];
		$.ajax(
				{
					url:base_url+"Viewcreationcolumn/retrieve?primaryid="+datarowid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data)
					{ 
						$('#moduleid').select2('val',data.moduleid).trigger('change');
						var txtboxname = elementsname;
						var dropdowns = ['6','moduleid','uitypeid','tabsectionid','roundtype','linkableoption','relatedmoduleid'];
						textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
						$('#tabsectionid').select2('val',data.tabsectionid).trigger('change');
						$('#linkableoption').select2('val',data.linkableoption).trigger('change');
						$('#viewcreationcolumnname').attr('data-primaryid',datarowid);
						$('#whereclauseokicon').trigger('click');
						Materialize.updateTextFields();										
					}
				}); 
	
    }
	function updateform()
	{
		var formdata = $("#viewcreationcolumnform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid =$('#viewcreationcolumnformprimaryid').val();
		setTimeout(function(){
			$('#processoverlay').show();
		},10);
	    $.ajax({
	        url: base_url + "Viewcreationcolumn/update",
	        data: "datas=" + datainformation+amp+"primaryid="+datarowid,
	        type: "POST",
			async:false,
			cache:false,
	        success: function(msg) 
	        {
				var nmsg =  $.trim(msg);
				addslideup('mergetextformadd','mergetextcreationview');
				resetFields();
				refreshgrid();
	            if (nmsg == 'SUCCESS'){				
				setTimeout(function(){
						$('#processoverlay').hide();
					},10);
					alertpopup(updatealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},10);
					alertpopup(submiterror);
	            }
	        },
	    });
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#viewcreationcolumnpgnum li.active').data('pagenum');
		var rowcount = $('ul#viewcreationcolumnpgnumcnt li .page-text .active').data('rowcount');
		viewcreationcolumngrid(page,rowcount);
	}
}
function loadquery(querybox,uniqueroundarray){
	if(checkValue(querybox) == true || checkValue(uniqueroundarray) == true){
		$.ajax({
			url:base_url+"Viewcreationcolumn/retrievequery",
			data:{querybox:querybox,uniqueroundarray:uniqueroundarray},
			dataType:'json',
			type: "POST",
			async:false,
			success :function(data)
			{ 
				var uniqueid=$.trim($('#querybox').val());
				var uniqueidsplit=uniqueid.split(' ');
				var uniqueidarray=[];
				var res=[];
				for(var i=0; i<uniqueidsplit.length; i++) {
					if (uniqueidsplit[i].match('^'+prefix)&& uniqueidsplit[i].match(suffix+'$')) {
						$.each( data, function( key, value ){
							if(uniqueidsplit[i] == key) {
								res+=' '+value;
						    }
						}); 
					}else if(uniqueidsplit[i].match('^round') || uniqueidsplit[i].match('round$')) {
						$.each( data, function( key, value ){
							if(uniqueidsplit[i] == key) {
								res+=' '+value;
						    }
						}); 
					}
					else{
						res+=' '+uniqueidsplit[i];
					}
			   }
				$('#sqlquery').val(res);
			}
		});	
	}
}
function loadtabsection(moduleid){
	$('#tabsectionid').empty();
	$('#tabsectionid').select2('val','').trigger('change');
	$.ajax({
			url:base_url+"Viewcreationcolumn/loadtabsection",
			data:{moduleid:moduleid},
			dataType:'json',
			type: "POST",
			async:false,
			cache:false,
			success: function(data) {			
				$.each(data, function(index){					
					$('#tabsectionid')
							.append($("<option></option>")
							.attr("value",data[index]['moduletabsectionid'])								
							.text(data[index]['moduletabsectionname']));											
				});	
			}
	});	
}
{//checks the unique name of the giveninputs
	//return error if value exists already-*validation done with spaces
	function viewcheckuniquename(field){	
		var table = field.attr('data-table');
		var fieldname = field.attr('data-validatefield');
		var value = field.attr('value');	
		var editid = field.attr('data-primaryid');	
		value=$.trim(value);
		var primarydataname = $('#'+table+'primaryname').val();	
		if(primarydataname != value){
			if(value.length > 0){
				var check = '';
				$.ajax({
					url: base_url + "Viewcreationcolumn/checkuniquename",
					data:{table:table,fieldname:fieldname,value:value,editid:editid},
					async:false,
					success: function(msg) {
						check =  $.trim(msg);
					},
				});
				if (check == 'YES') {
					return "*value already exists";
				}
			}
		} 
	}
}