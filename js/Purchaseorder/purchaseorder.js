$(document).ready(function() {	
		$('#purchaseorderproupdatebutton,#purchaseorderpayupdatebutton').removeClass('updatebtnclass');
		$('#purchaseorderproupdatebutton,#purchaseorderpayupdatebutton').removeClass('hidedisplayfwg');
		$('#purchaseorderproaddbutton,#purchaseorderpayaddbutton').removeClass('addbtnclass');
		$("#purchaseorderproupdatebutton,#purchaseorderpayupdatebutton").hide();
		PAYMETHOD ='ADD';
		METHOD ='ADD';
		PRECISION = 0;
		PRODUCTTAXABLE ='Yes';
		QUANTITY_PRECISION = 0;
		PRICBOOKCONV_VAL = 0;
		COMPANY_NAME = '';
		MODULEID = 0;
		UPDATEID = 0;
    {	
		// Foundation Initialization
        $(document).foundation();
		// Maindiv height width change
        maindivwidth();
    }
	{//module showhides
		$("#moduleid").select2('val','').trigger('change');
		$("#moduleid option").addClass("ddhidedisplay").addClass("ddclose");
		if(softwareindustryid == 1){
			$("#moduleid option[value=216]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=217]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=226]").removeClass("ddhidedisplay").removeClass("ddclose");
		}else if(softwareindustryid == 2){
			$("#moduleid option[value=94]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=96]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=95]").removeClass("ddhidedisplay").removeClass("ddclose");
		}else if(softwareindustryid == 4){
			$("#moduleid option[value=85]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=87]").removeClass("ddhidedisplay").removeClass("ddclose");
			$("#moduleid option[value=86]").removeClass("ddhidedisplay").removeClass("ddclose");
		}	
		$("#moduleid option[value=249]").removeClass("ddhidedisplay").removeClass("ddclose");
		$("#crmstatusid option[value='71']").remove();
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	$('#currencyconverticon').removeClass("hidedisplay")
	{// field break
		$("<div class='row'></div>").insertAfter("#paymentstatusiddivhid");
	}
	{//Enables the Edit Buttons
		$('#purchaseorderproingrideditspan1').removeAttr('style');
		$('#purchaseorderpayingrideditspan2').removeAttr('style');
	}
	{// Shipping billing address details fetching
		$("#billingaddresstype,#shippingaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
			Materialize.updateTextFields();
		});
		//shipping /billing hiding
		$("#billingaddresstype option[value='6']").remove();
		$("#shippingaddresstype option[value='5']").remove();
	}
	//buttoon hide
	$("#purchaseorderproupdatebutton").hide();
	$("#purchaseorderpayupdatebutton").hide();
	{// Grid Calling
        purchaseordergrid();
        getmoduleid();
		//crud action
		crudactionenable();
    }
	$(".dropdownchange").change(function(){
		var dataattrname = ['dataidhidden'];
		var dropdownid =['employeeidddid'];
		var textboxvalue = ['employeetypeid'];
		var selectid = $(this).attr('id');
		var index = dropdownid.indexOf(selectid);
		var selected=$('#'+selectid+'').find('option:selected');
		var datavalue=selected.data(''+dataattrname[index]+'');
		$('#'+textboxvalue[index]+'').val(datavalue);
		if(selectid == 'employeeidddid') {
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data('ddid');
			$('#employeeid').val(datavalue);
		}
	});
	{// Formclear resetting 
		$("#formclearicon").click(function(){
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setdefaultproperty();
			$('#payeename').val(COMPANY_NAME);			
			//cleargrid data
			cleargriddata('purchaseorderproaddgrid1');
			cleargriddata('purchaseorderpayaddgrid2');
			froalaset(froalaarray);
			METHOD = 'ADD';
			PAYMETHOD ='ADD';
			//empty option of depedency dropdowns
			$('#opportunityid,#contactid').empty();
		});
	}	
	{
		if(softwareindustryid == 4){
			var ordertype = $('#producthide').val();
			if(ordertype){
				$.ajax({
					url:base_url+"Base/getorderbasedproduct?ordertypeid="+ordertype+"&producthide=Yes",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$('#productid').empty();
						prev = ' ';
						var optclass = [];
						$.each(data, function(index) {
							var cur = data[index]['PId'];
							if(prev != cur ) {
								if(prev != " ") {
									$('#productid').append($("</optgroup>"));
								}
								if(jQuery.inArray(data[index]['optclass'], optclass) == -1) {
									optclass.push(data[index]['optclass']);
									$('#productid').append($("<optgroup  label='"+data[index]['optclass']+"' class='"+data[index]['optclass']+"'>")); 
								} 
								$("#productid optgroup[label='"+data[index]['optclass']+"']")
    							.append($("<option></option>")
    							.attr("class",data[index]['optionclass'])
    							.attr("value",data[index]['value'])
    							.attr("data-productidhidden",data[index]['data-productidhidden'])
    							.text(data[index]['data-productidhidden']));
								prev = data[index]['PId'];
							}
							
						}); 
						$('#productid').select2('val','').trigger("change");
					}
				});
			}
		}
	}
	{//module id changes events
		$("#moduleid").change(function(){
			var val =$(this).val();
			if(val > 0){
				//Reset the purchaseorder elements
				var purchaseorderdate = $('#purchaseorderdate').val();
				var purchaseordernumber = $('#purchaseordernumber').val();
				var purchaseordertype = $('#ordertypeid').val();				
				var modulenumber = $('#modulenumber').val();
				if(checkValue(modulenumber) == true){ //reset the elements ony if any numbers are set
					$("#formclearicon").trigger('click');
					$('#moduleid').select2('val',val);
					$('#purchaseorderdate').val(purchaseorderdate);
					$('#purchaseordernumber').val(purchaseordernumber);
					$('#ordertypeid').select2('val',purchaseordertype);
				} else {
					if(checkValue(val) == true){ //reset the elements 
					$("#formclearicon").trigger('click');
					$('#moduleid').select2('val',val);
					$('#purchaseorderdate').val(purchaseorderdate);
					$('#purchaseordernumber').val(purchaseordernumber);
					$('#ordertypeid').select2('val',purchaseordertype);
					}
				}
			} else {
				cleargriddata('modulenumbersearchgrid');
			}			
		});
		//mnsearchsubmit
		$("#mnsearchsubmit").click(function(){			
			var datarowid = $('#modulenumbersearchgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var sourcemodule = $('#moduleid').val();
				var prevmodulenumber = $('#modulenumber').val();
				if($.inArray(sourcemodule, ["216","85","94"]) != -1) {
				var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'quotenumber','');
				}
				else if($.inArray(sourcemodule, ["217","87","96"]) != -1) {
				var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'salesordernumber','');
				}
				else if($.inArray(sourcemodule, ["226","86","95"]) != -1){
				var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'invoicenumber','');
				}
				else if(sourcemodule == 249){
				var number =  getgridcolvalue('modulenumbersearchgrid',datarowid,'materialrequisitionnumber','');
				}
				$('#modulenumber').val(number);
				$('#mnsearchoverlay').fadeOut('slow');				
				var destinatemodule =MODULEID;
				//data clear
				//Reset the purchaseorder elements				
				var purchaseorderdate = $('#purchaseorderdate').val();
				var purchaseordernumber = $('#purchaseordernumber').val();
				var purchaseordertype = $('#ordertypeid').val();
					if((prevmodulenumber != number) ||(checkValue(prevmodulenumber) == false)){ //reset the elements ony if any numbers are set
						$("#formclearicon").trigger('click');
						$('#moduleid').select2('val',sourcemodule);
						$('#purchaseorderdate').val(purchaseorderdate);
						$('#purchaseordernumber').val(purchaseordernumber);
						$('#modulenumber').val(number);
						$('#ordertypeid').select2('val',purchaseordertype);
						//dataretrievals
						conversiondatamapping(datarowid,sourcemodule,destinatemodule);	
						if(softwareindustryid != 2){
							$('#accountid,#contactid').attr('readonly','readonly');	
						}						
						Materialize.updateTextFields();
					}	
				cleargriddata('modulenumbersearchgrid');
			} else {
				alertpopup('Please select row');
			}
		});
	}	
	{// Account change	
		$("#accountid").change(function() {
			var accountid=$(this).val();
			getaddress(accountid,'account','Billing');		
			getaddress(accountid,'account','Shipping');
			$('#billingaddresstype,#shippingaddresstype').select2('val','2');
			//get dependent values
			var id=$(this).val();
			if(id != ''){			
				//opportunity
				$('#opportunityid').empty();
				var table = 'opportunity';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
						newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
				  }
				  $('#opportunityid').append(newoptions);
				  $('#opportunityid').select2('val','').trigger('change');
				},
				});
				//opportunity
				$('#contactid').empty();
				var table = 'contact';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
					    newoptions="";
						$.each(data, function(index) {
							newoptions += "<option value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";
						});
				  }
				  $('#contactid').append(newoptions);				  
				},
				});	
			}
			Materialize.updateTextFields();
		});		
	}
	$("#territoryid").change(function(){
		var id = $(this).val();
		if(id){
			$("#pricebookid").empty();
			$.ajax({
				url: base_url+"Base/getpricebookdetail?territoryid="+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					newoptions="<option></option>";
					$.each(data, function(index) {
						newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";								
					});
					$('#pricebookid').append(newoptions);
					$('#pricebookid').select2('val','').trigger('change');
				},
			});
		}else{
			$("#pricebookid").empty();
			$.ajax({
				url: base_url+"Base/getpricebookdetail?territoryid="+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$("#pricebookid").empty();
					newoptions="<option></option>";
					$.each(data, function(index) {
						newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
					});
					$('#pricebookid').append(newoptions);
					$('#pricebookid').select2('val','').trigger('change');
				},
			});
		}
	});
	{//pricebooks		
		$("#pricebookid").change(function()
		{			
			var val= $(this).val();
			var currency = $('#currencyid').val();
			if(checkValue(val) == true){
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
				$.ajax({
				url:base_url+'Quote/pricebookcurrency?pricebook='+val+'&currency='+currency,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.status != 'FAILED'){
						if(currency == data.currencyid){
						} else  {	
							var conversionrate = data.rate; //to get conversion rate
							$('#currentcurrency').select2('val',currency).trigger('change').attr("disabled","disabled");
							$('#pricebookcurrency').select2('val',val).trigger('change').attr("disabled","disabled");	
							$('#pricebook_currencyconv').val(conversionrate);							
							$('#pricebook_currencyconv').focus();							
							$("#pricebookcurrencyconvoverlay").fadeIn();
							Materialize.updateTextFields();
						}
					}
				},
				});
				$("#pricebookclickevent").hide();
			}else{
				$("#pricebookclickevent").show();
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
			}
		});	
	}
	{// On lead contact change 
		$("#leadcontacttype").change(function(){
			var val=$(this).val();	
			appendleadcontact(val,'contactid');
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Toolbar actions
		$("#cloneicon").click(function(){
			QUANTITY_PRECISION =0;
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				purchaseorderproaddgrid1();
				purchaseorderclonedatafetchfun(datarowid);				
				$('#payeename').val(COMPANY_NAME);
				Materialize.updateTextFields();
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				showhideiconsfun('editclick','purchaseordercreationformadd');
				$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").show();
				$(".fr-element").attr("contenteditable", 'true');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else {
				alertpopup("Please select a row");			
			}
		});
		//cancel data
		$("#cancelno").click(function(){
			$("#basecanceloverlay").fadeOut();
		});
		$("#cancelyes").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			var cancelresult=checkcancelstatus(MODULEID,'purchaseorder',datarowid);
			if(cancelresult == true)
			{
			$.ajax({
			url: base_url + "Purchaseorder/canceldata?primarydataid="+datarowid+"&purchaseordermodule="+MODULEID,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == true) {
					$("#basecanceloverlay").fadeOut();
					refreshgrid();					
				} 
				else if (nmsg == "false") {
				}
			},
		  });
		  }
		  else{
			alertpopup("This record cannot be Cancelled");
		  }
		});
		$("#cancelicon").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage = purchaseorderstage(datarowid);
			    if(stage == 48){
					alertpopup("purchaseorder Booked!!!Cannot Change To Cancel");
			    }	
				else if(stage == 49){				
					$('#cancelmessage').text("Record Already Cancelled.Do u want revert to Draft?");
					$("#basecanceloverlay").fadeIn();				
					$("#cancelyes").focus();
			    } 				
				else {
					$('#cancelmessage').text("Do You Want To Cancel This Record?");
					$("#basecanceloverlay").fadeIn();				
					$("#cancelyes").focus();
			    }				
			}
			else {
				alertpopup("Please select a row");
			}		
		});
		$("#bookedicon").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage = purchaseorderstage(datarowid);
			    if(stage == 48){
					$('#bookedmessage').text("purchaseorder Already Booked.Do u want revert to Draft?");
					$("#bookedoverlay").fadeIn();
					$("#bookedyes").focus();
			    } else  if(stage == 47){
					$('#bookedmessage').text("Book purchaseorder?");
					$("#bookedoverlay").fadeIn();
					$("#bookedyes").focus();
			    } 
			    else {
					alertpopup("purchaseorder Can't Change to Booked Stage");
				}				
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$("#bookedyes").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');		
			$.ajax({
				url: base_url + "Purchaseorder/bookpurchaseorder?primarydataid="+datarowid+"&purchaseordermodule="+MODULEID,
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == true) {
						$("#bookedoverlay").fadeOut();
						refreshgrid();					
					} 					
				},
			});		
		});	
		$("#reloadicon").click(function(){
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('purchaseordergrid');
			sectionpanelheight('purchaseorderprooverlay');
		});
		{//inner-form-with-grid
			$("#purchaseorderproingridadd1").click(function(){
				sectionpanelheight('purchaseorderprooverlay');
				$("#purchaseorderprooverlay").removeClass("closed");
				$("#purchaseorderprooverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#purchaseorderprocancelbutton").click(function(){
				clearform('gridformclear');
				$("#purchaseorderprooverlay").removeClass("effectbox");
				$("#purchaseorderprooverlay").addClass("closed");
				$('#purchaseorderproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#purchaseorderproaddbutton').show();//display the ADD button(inner-productgrid)*/	
			});
		}
		$("#detailedviewicon").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				froalaset(froalaarray);
				purchaseordereditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','purchaseordercreationformadd');
				$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").hide();
				$("#purchaseorderproingridedit1,#purchaseorderpayingridedit2").hide();
				$(".froala-element").css('pointer-events','none');
				$(".fr-element").attr("contenteditable", 'false');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					//Function Call For Edit
					froalaset(froalaarray);
					purchaseordereditdatafetchfun(rdatarowid);
					Materialize.updateTextFields();
					showhideiconsfun('summryclick','purchaseordercreationformadd');
					$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").hide();
					$("#purchaseorderproingridedit1,#purchaseorderpayingridedit2").hide();
					$(".froala-element").css('pointer-events','none');
					$(".fr-element").attr("contenteditable", 'false');
					sessionStorage.removeItem("datarowid");
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					 },50);
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var stage=purchaseorderstage(datarowid);
				if(stage == 48){ //Booked
					alertpopup("This purchaseorder is Booked !!!");
				} else if(stage == 49){ //cancel
					alertpopup("This purchaseorder is Cancelled");
				}  else if(stage == 47){ //draft
					showhideiconsfun('editfromsummryclick','purchaseordercreationformadd');
					$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").show();
					$("#purchaseorderproingridedit1,#purchaseorderpayingridedit2").show();
					$(".fr-element").attr("contenteditable", 'true');
					$(".froala-element").css('pointer-events','auto');
				}
			}			
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			var accountid = getaccountid(datarowid,'purchaseorder','accountid');
			if (datarowid) 
			{
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','purchaseorder',datarowid);
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",emailtosend.subject);
					sessionStorage.setItem("moduleid",MODULEID);
					sessionStorage.setItem("recordid",datarowid);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				}
				else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#purchaseorderproingriddel1").click(function(){
			var datarowid = $('#purchaseorderproaddgrid1 div.gridcontent div.active').attr('id');			
			if(datarowid)
			{		
				if(METHOD == 'UPDATE'){
					alertpopup("A Record Under Edit Form");
				}
				else{
					/*delete grid data*/
					deletegriddatarow('purchaseorderproaddgrid1',datarowid);
					var productrecords = $('#purchaseorderproaddgrid1 .gridcontent div.data-content div').length;					
					var currentmode = $('#currentmode').val();
					if(productrecords == 0 )//&& currentmode == 1)
					{	
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').select2('val','');
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').select2("readonly", false);	
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').trigger('change');
						//Empty the Tax/Charge data-because its dependent
						$('#grouptaxgriddata,#groupchargegriddata').val('');
						PRICBOOKCONV_VAL = 0;
						if(softwareindustryid == 4){
							$("#productid").empty();
						}						
						$("#pricebookclickevent").show();
					}
					gridsummeryvaluefetch('','','');
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}	
    {// Close Add Screen
        var addcloseacccreation =["closeaddform","purchaseordercreationview","purchaseordercreationformadd"]
        addclose(addcloseacccreation);
		var addcloseviewcreation =["viewcloseformiconid","purchaseordercreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
    }
	{// View by drop down change
        $('#dynamicdddataview').change(function(){
            purchaseordergrid();
        });
    }
	{// Validation for purchaseorder Add  
		$('#dataaddsbtn').click(function(e){
			$('#dataaddsbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#conversionrate').removeClass('validate[required,maxSize[100]]');
			$('#conversionquantity').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#uomid').removeClass('validate[required]');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone'){
					var getrownumber = $('#purchaseorderproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#purchaseorderproaddgrid1 .gridcontent div.wrappercontent div').length;
				}				
				if(getrownumber != 0){
					var status = $("#crmstatusid").val();
					if(status == null || status == undefined || status == ''){
						$("#crmstatusid").select2('val',47).trigger("change");
					}
					purchaseordercreate();
					$('#dataaddsbtn').removeClass('singlesubmitonly');
				}
				else{
					alertpopup('Enter The Product!!');
					$('#dataaddsbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
					$('#conversionrate').addClass('validate[required,maxSize[100]]');
					$('#conversionquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#uomid').addClass('validate[required]');
				}					
			},
			onFailure: function() {
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);				
				alertpopup(validationalert);	
				$('#dataaddsbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
				$('#conversionrate').addClass('validate[required,maxSize[100]]');
				$('#conversionquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#uomid').addClass('validate[required]');
			}
		});
	}
	{// Update purchaseorder information
		$('#dataupdatesubbtn').click(function(e) {
			$('#dataupdatesubbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');			
			$('#conversionrate').removeClass('validate[required,maxSize[100]]');
			$('#conversionquantity').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#uomid').removeClass('validate[required]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function()
			{
				if(deviceinfo != 'phone'){
					var getrownumber = $('#purchaseorderproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#purchaseorderproaddgrid1 .gridcontent div.wrappercontent div').length;
				}				
				if(getrownumber != 0){
					purchaseorderupdate();
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				}else{
					alertpopup('Enter The Product...');
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
					$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#uomid').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#conversionrate').addClass('validate[required,maxSize[100]]');
					$('#conversionquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#uomid').addClass('validate[required]');
				}				
			},
			onFailure: function()
			{
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');					
				$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#uomid').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#conversionrate').addClass('validate[required,maxSize[100]]');
				$('#conversionquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#uomid').addClass('validate[required]');
				alertpopup(validationalert);
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{// For Form to Grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){	
			var clickgridname = $(this).data('frmtogridname');
			if(clickgridname =='purchaseorderproaddgrid1'){  
				$('#purchaseorderdetailid').val(0);
				//reset individual array
				$('.resetindividualoverlay').find('input[type=text],select,input[type=hidden]').val('');
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);
				setTimeout(function(){
					QUANTITY_PRECISION = 0;
					$('#touomid').select2('val','');
					$('#touomid').empty();
				},100);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
			}
		});
	}
	{// Terms and condition value fetch
		$("#termsandconditionid").change(function()
		{
			var id = $("#termsandconditionid").val();
			tandcdatafrtch(id);
		});
	}
	{// Product change events 
		$("#productid").change(function(){
			var val=$(this).val();
			if(val){
				var productval = $(this).find('option:selected').data('productidhidden');
				var ul = $('#purchaseorderproaddgrid1 div.gridcontent .inline-list');
				var lis = ul.children('li');
				var isInList = false;
				for(var i = 0; i < lis.length; i++){
				    if(lis[i].innerHTML === productval) {
				        isInList = true;
				        break;
				    }
				}
				if(isInList){
					 alert("Product already Exist. Kindly choose another Product");
					 $('#productid').select2('val','');
					 $('#uomid').select2('val','');
					 $('#touomid').empty();
					 $('#instock,#conversionrate,#conversionquantity,#quantity,#unitprice,#pretaxtotal,#sellingprice,#grossamount,#discountamount,#requireddate,#netamount').val(" ");
				}else{
					$("#quantity").removeClass();
					$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#discountoverlay,#additionaloverlay,#taxoverlay').find('input:text').val('');
					$('#chargeamount,#discountamount,#taxamount,#netamount').val('');
					$('#additionalcategory').select2('val','').trigger('change');			
					getproductdetails(val);				
					$('.error').removeClass('error');
					$(".btmerrmsg").remove();
					$('.formError').remove();
					var inputs = $(this).closest('form').find(':input');
					inputs.eq( inputs.index(this)+ 1 ).focus();		
				}				
			}				
		});
	}
	//uom change
	$("#touomid").change(function() {
		var touomid = $("#touomid").val();
		if(touomid != null){
			QUANTITY_PRECISION = 0;
			var symbol = $('option:selected', this).attr('data-symbol');
			var conrate = $('option:selected', this).attr('data-conrate');
			var precision = $('option:selected', this).attr('data-precision');		
			QUANTITY_PRECISION = precision;
			var uomurl=base_url+'Uommaster';
			$("#instockdivhid label").html('Stock Quantity  <a href="'+uomurl+'" style="color:#575757;">('+symbol+')</a>'); //set the specified product's uom symbol with link
			QUANTITY_PRECISION = precision; //set's the specified product quantity rounding.
			$('#conversionrate').val(conrate).trigger('focusout');
			Materialize.updateTextFields();
		}else{
			$('#conversionrate').val("");
			$('#conversionquantity').val("");
		}			
	});		
	{// Calculation type
		$(".caltypedd").change(function()
		{var addctype = $(this).attr('id');
			var elementposition = $(this).data('val');
			var addvalue='addval_'+elementposition;
			var addamts='addamount_'+elementposition;
			setzero([addvalue,'grossamount']);
			var calctype=$("#"+addctype+"").val();
			var value=$('#'+addvalue+'').val();
			var price=$('#grossamount').val();
			var addamt=0;
			switch (calctype)
			{ 
				case '2': 
					var addamt=value;
				break;
				case '3':
					var addamt=(parseFloat(value/100)*parseFloat(price));
				break;
			}
			var faddamt=parseFloat(addamt);
			$('#'+addamts+'').val(faddamt.toFixed(2));
		});
	}
	{// Addamount overlay submit
		$("#addamtsubmit").click(function()	{
			var addamount = $("#totaladdselling").val();
			$("#additionalamount").val(addamount);
			additionalamountvalueadd();
			$("#additionaloverlay").fadeOut();
		});
		$('#addamtsubmit').click(function(e){
			$("#additionaloverlayvalidation").validationEngine('validate');			
		});
		jQuery("#additionaloverlayvalidation").validationEngine({
				onSuccess: function() {
					$("#additionaloverlay").fadeOut();
				},
				onFailure: function() {	
				}
		});
	}	
	{// Pricebook submit
		$("#pricebooksubmit").click(function()
		{
			var selectedrow = $('#pricebookgrid div.gridcontent div.active').attr('id');
			if(selectedrow) 
			{
				var sellingprice = getgridcolvalue('pricebookgrid',selectedrow,'sellingprice','');
				var currencyid = getgridcolvalue('pricebookgrid',selectedrow,'currencyid','');
				var current_currencyid=$("#currencyid").val();
				if(current_currencyid == currencyid){
					$('#sellingprice').val(sellingprice).trigger('focusout'); //sets the value and triggerchange calc
					$("#pricebookoverlay").fadeOut();	
				} else if(current_currencyid != currencyid) {	
					var conversionrate = getcurrencyconversionrate(currencyid,current_currencyid); //to get conversion rate
					$('#currentcurrency').select2('val',current_currencyid).trigger('change').attr("disabled","disabled");
					$('#pricebookcurrency').select2('val',currencyid).trigger('change').attr("disabled","disabled");					
					$("#pricebook_sellingprice").val(sellingprice);					
					$('#pricebook_currencyconv').val(conversionrate);
					$("#pricebookcurrencyconvoverlay").fadeIn();
					Materialize.updateTextFields();
				}			
			}
			else
			{
				alertpopup('Please select a row');
			} 
		});
	}
	{
		//currency overlay
		$("#currencyconverticon").click(function(){
			var productrecords = $('#purchaseorderproaddgrid1 .gridcontent div.data-content div').length;
			if(productrecords == 0){
				$("#currencyconvoverlay").fadeIn();
				$.ajax({
					url:base_url+"Base/getdefaultcurrency", 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {						
						$('#defaultcurrency').select2('val',data.currency).trigger('change').attr("readonly","readonly");
					}
				});
			}					
		});
		$("#currencyconvclose").click(function() {
			$("#currencyconvoverlay").fadeOut();
		});
		//currency 
		$("#convcurrency").change(function() {
			var defaultcurrency = $("#currencyid").val();
			var currencyid = $(this).val();
			var quotedate = $("#purchaseorderdate").val();
			if(currencyid > 0) {
					var selected=$('#currencyid').find('option:selected');
					var datavalue=selected.data('precision');
					PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null) {
					PRECISION = 0;
				}
				$.ajax({
					url:base_url+"Quote/getconversionrate?fromcurrency="+defaultcurrency+"&tocurrency="+currencyid+"&quotedate="+quotedate, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data.status == true){
							$('#currencyconv').val(data.rate);
							Materialize.updateTextFields();
						}else{
							$('#currencyconv').val(0);
							Materialize.updateTextFields();
						}
					}
				});
			} else {
				PRECISION = 0;
				$('#currencyconv').val(0);
			}
		});
		$('#currencyconv_submit').click(function(e){
			$("#currencyconv_validate").validationEngine('validate');			
		});
		jQuery("#currencyconv_validate").validationEngine({
			onSuccess: function() { 
				var convcurrency=$("#convcurrency").val();	 //convert currency
				var currencyconv=$("#currencyconv").val();	 //convert rate
				$("#currencyid").val(convcurrency).trigger("change");
				$("#currencyconvresionrate").val(currencyconv).trigger("change");
				$("#productid").select2('val','').trigger("change");
				$("#pricebookid").select2('val','').trigger("change");
				$("#currencyconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});		
		$("#currencyid").change(function(){			
			var currencyid = $(this).val();
			if(currencyid > 0){
				var selected=$('#currencyid').find('option:selected');
				var datavalue=selected.data('precision');
				PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null){
					PRECISION = 0;
				}	
				
			} else {
				PRECISION = 0;
			}
		});
	}
	{
		// Redirected form Home		
		add_fromredirect("purchaseorderaddsrc",MODULEID);
	}
	{// purchaseorder Product Grid Edit
		$("#purchaseorderproingridedit1").click(function()
		{
			var selectedrow = $('#purchaseorderproaddgrid1 div.gridcontent div.active').attr('id');
			if(selectedrow){	
				sectionpanelheight('purchaseorderprooverlay');
				$("#purchaseorderprooverlay").removeClass("closed");
				$("#purchaseorderprooverlay").addClass("effectbox");
				$("#touomid").select2("val", "");
				$("#touomid").empty();
				var value = $('#purchaseorderproaddgrid1 .gridcontent div#'+selectedrow+' ul .productid-class').text();
				var uomdata = $('#purchaseorderproaddgrid1 .gridcontent div#'+selectedrow+' ul .uomdata-class').text();
				if (uomdata) {
					var uomdetail = $.parseJSON(uomdata);
				} else {
					var uomdetail = [];
				}
				$.ajax({
					url:base_url+'Base/getproductdetails?id='+value,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) 
					{
						$uomdata = data.touomarr;
						$uomlength = $uomdata.length;
						for (var i = 0; i < $uomlength; i++) {
							$('#touomid')
							.append($("<option></option>")
			                .attr("value",$uomdata[i]['uomtoid'])
			                .attr("data-touomidhidden",$uomdata[i]['uomname'])
			                .attr("data-conrate",$uomdata[i]['conversionrate'])
			                .attr("data-symbol",$uomdata[i]['symbol'])
			                .attr("data-precision",$uomdata[i]['uomprecision'])
			                .text($uomdata[i]['uomname']));
						}
						$('#uomid').select2('val',uomdetail['uomid']);
						$("#uomid").attr('readonly','readonly');
						$('#touomid').select2('val',uomdetail['touomid']).trigger('change');
						$('#conversionrate').val(uomdetail['conversionrate']);
						$('#conversionquantity').val(uomdetail['conversionquantity']);
						QUANTITY_PRECISION = data.uomprecision;
						$("#quantity").removeClass();
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
					}
				});
				gridtoformdataset('purchaseorderproaddgrid1',selectedrow);				
				$('#purchaseorderproupdatebutton').show();	//display the UPDATE button(inner - productgrid)
				$('#purchaseorderproaddbutton').hide();	//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
	{// Get salesorder data
		$("#mname").change(function()
		{
			var moduleid=217;
			var modulenumber=$("#mname").find('option:selected').data('moduleuniqueid');			
		});
	}
	//check dataLoad from 
	var temppurchaseorderid = sessionStorage.getItem("convertpurchaseorderfromso");
	if(temppurchaseorderid != null){
		setTimeout(function(){
			$("#addicon").trigger('click');
			$('#mname').select2('val',temppurchaseorderid).trigger('change');
			sessionStorage.removeItem("convertpurchaseorderfromso");
		},100);				
	}
	//salesorder-reportsession check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique225"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				purchaseordereditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','purchaseordercreationformadd');
				$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").hide();
				sessionStorage.removeItem("reportunique225");
			},50);	
		}
	}
	{
		//print icon
		$('#printicon').click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});				
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Date REstriction
		var dateformetinv  = 0;
		dateformetinv = $('#purchaseorderdate').attr('data-dateformater');
		$('#purchaseorderdate').datetimepicker({
			dateFormat: dateformetinv,
			showTimepicker :false,
			minDate: 0,
			onSelect: function () {
				getandsetdate($('#purchaseorderdate').val());
			},
			onClose: function () {
				$('#purchaseorderdate').focus();
			}
		});
	}
	{//validity data extension 
		$("#validdate,#purchaseorderdate").change(function()
		{
			$("#validatetypeid").select2('val','').trigger('change');
		});
		$("#validatetypeid").change(function()
		{
			var vl=parseInt($(this).val());			
			if(!isNaN(vl)){
				var vg=$("#validatetypeid option:selected").text();
				var val = vg.split("-");
				var date = $('#purchaseorderdate').datepicker('getDate');
		        var tempStartDate = new Date(date);
		        var res = tempStartDate.setTime(tempStartDate.getTime() + (val * 24 * 60 * 60 * 1000));
		        new_date = new Date(res);
			 	var dateformat = $('#validdate').attr('data-dateformater');
			 	result = $.datepicker.formatDate(dateformat, new_date);
				$('#validdate').val(result);				
			}
		});
	}	
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}	
	{ //auto trigger
		generatemoduleiconafterload('modulenumber','modulenumberevent');
		generateiconafterload('sellingprice','pricebookclickevent','sellingpricedivhid');
		productgenerateiconafterload('productid','productsearchevent');		
		generateuomiconafterload('quantity','uomclickevent','quantitydivhid');
	}		
	{ //
		$("#uomclickevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
			if(checkVariable('productid') == true) {
				$("#uomconvoverlay").fadeIn();
				$("#uomconvclose").focus();
			} else {
				alertpopup("Kindly Choose Product to Load UOM");
			}
			}
		});
		
		$("#pricebookclickevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				if(checkVariable('productid') == true) {
					$("#pricebookoverlay").fadeIn();
					$('#pricebookclose').focus();
					pricebookgrid();
				} else {
					cleargriddata('pricebookgrid');
				}
			}
		});
		$("#productsearchevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				$("#productsearchoverlay").fadeIn();
				$("#productsearchclose").focus();
				productsearchgrid();
			}
		});
		$("#pricebookclose").click(function()
		{
			$("#pricebookoverlay").fadeOut();
			$('#grossamount').focus();
		});
		$("#pricebookcurrencyconvclose").click(function()
		{
			$("#pricebookcurrencyconvoverlay").fadeOut();
		});
		$("#productsearchclose").click(function()
		{
			$("#productsearchoverlay").fadeOut();
			$('#quantity').focus();
		});
		$('#uomconv_submit').click(function(e){
			$("#uomconv_validate").validationEngine('validate');			
		});
		jQuery("#uomconv_validate").validationEngine({
			onSuccess: function() {
				var uom_json ={}; //declared as object
				uom_json.uomid = $('#uomid').val(); 
				uom_json.touomid = $('#touomid').val();	
				uom_json.conversionrate = $('#conversionrate').val();
				uom_json.conversionquantity = $('#conversionquantity').val();
				$('#uomdata').val(JSON.stringify(uom_json));
				$("#uomconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
		$("#uomconvclose").click(function() {
			$("#uomconvoverlay").fadeOut();
		});		
	}	
	{ // Grid overlay operations
		$("#discountamount").click(function(){			
			var product=checkVariable('productid');
			var discount_data = $('#discountdata').val();
			var parse_discount = $.parseJSON(discount_data);
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#discounttypeid').select2('val','').trigger('change');
				$('#discountpercent').val('');				
			} else {								
				$('#discounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#discountpercent').val(parse_discount.value);
			}			
			$("#discountoverlay").fadeIn();
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
				$("#discountclose").focus();
			},100);
		});		
		$("#discounttypeid").change(function(){
			$('#discountpercent').val(0);
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#discountpercent").focusout(function(){			
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//discountrefresh-individual
		$("#discountrefresh").click(function(){
			$('#discounttypeid').select2('val','2').trigger('change');
			$('#discountpercent').val('');
			$('#singlediscounttotal').val('');			
		});
		//*group discount click*//
		$("#groupdiscountamount").click(function()
		{			
			var discount_data = $('#groupdiscountdata').val();
			var parse_discount = $.parseJSON(discount_data);				
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#groupdiscounttypeid').select2('val','').trigger('change');
				$('#groupdiscountpercent').val('');				
			} else {							
				$('#groupdiscounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#groupdiscountpercent').val(parse_discount.value);
			}
			$("#summarydiscountoverlay").fadeIn();				
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);			
		});
		//*group discount close*//		
		$("#summarydiscountclose").click(function()
		{
			$("#summarydiscountoverlay").fadeOut();
			var value=parseFloat($('#groupdiscountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#groupdiscounttypeid').select2('val','').trigger('change');
			}
		});
		//*Summary Discount validation*//
		$('#summarydiscountadd').click(function(e){
			$("#summarydiscountvalidation").validationEngine('validate');			
		});
		jQuery("#summarydiscountvalidation").validationEngine({
			onSuccess: function() {											
				var discount_json ={}; //declared as object
				discount_json.typeid = $('#groupdiscounttypeid').val(); //discount typeid
				discount_json.value = $('#groupdiscountpercent').val();	//discount values		
				$('#groupdiscountdata').val(JSON.stringify(discount_json)); //saves discountdata in hiddenfield(JSON)
				var netamount = parseFloat(getgridcolvalue('purchaseorderproaddgrid1','','netamount','sum'));
				var discount_amount=discountcalculation(discount_json,netamount); //to calculate discount
				$('#groupdiscountamount').val(discount_amount);					
				$("#summarydiscountoverlay").fadeOut();
				//?set a trigger point here
				gridsummeryvaluefetch('','','');
			},
			onFailure: function() {	
			}
		});
		//discountrefresh-group
		$("#groupdiscountrefresh").click(function(){
			$('#groupdiscounttypeid').select2('val','2').trigger('change');
			$('#groupdiscountpercent').val('0');
			$('#groupdiscounttotal').val('');
			Materialize.updateTextFields();
		});
		$("#groupdiscounttypeid").change(function(){
			$('#groupdiscountpercent').val(0);
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#groupdiscountpercent").focusout(function(){			
			setTimeout(function()
			{
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//tax
		$("#taxamount").click(function(){ 
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			$("#taxcategory").focus();
			if(type == 2 && checkVariable('productid') ==true && PRODUCTTAXABLE == 'Yes'){	//individual
				var g_data = $('#taxgriddata').val();
				var tax_category_id= 'taxcategory';
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$("#taxoverlay").fadeIn();
					$('#'+tax_category_id+'').select2('val','').trigger('change');
					taxgrid();
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0){	
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val',main_data.id);
						loadgriddata('taxgrid',grid_data);
					} else {
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val','').trigger('change');
					} 
				}				
				setTimeout(function()
				{
					tax_data_total();
				},200);
			}
		});
		$("#grouptaxamount").click(function(){			
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 3){	//group
				var g_data = $('#grouptaxgriddata').val();
				var tax_category_id= 'taxcategory';				 
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){						
					$("#taxoverlay").fadeIn();
					taxgrid();
					$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0){					
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val',main_data.id);
						loadgriddata('taxgrid',grid_data);
					} else {
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
					} 
				}
			}
			setTimeout(function()
			{
				tax_data_total();
			},200);
			});
		//edit/delete on overlay
		$("#taxcategory").change(function(){
			var id = $(this).val();
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			var territory = $('#territoryid').val(); //region
			var totalnetamount = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());//group
			cleargriddata('taxgrid');
			if(id != '') {
				if(type == 2) {
					var grossamount=parseFloat($('#grossamount').val());
					var discountamount=parseFloat($('#discountamount').val());
					var finalamount=parseFloat(grossamount)-parseFloat(discountamount);
				} else {
					var finalamount= 0;
				}
				if(id != ''){
					var transmethod = MODULEID;
					$.ajax({
						url:base_url+'Quote/taxmasterload?taxmasterid='+id
						+'&finalamount='+finalamount+'&territory='+territory+'&type='+type+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if((data.fail) == 'FAILED') {
							} else {
								loadinlinegriddata('taxgrid',data.rows,'json');
								setTimeout(function() {
									tax_data_total();
								},200);
								/* data row select event */
								datarowselectevt();
								/* column resize */
								columnresize('taxgrid');
							}
						},
					});
					}
			}
		});		
		$("#taxdeleteicon").click(function(){
			    var id = $('#taxgrid div.gridcontent div.active').attr('id');
				if(id){
					/*delete grid data*/
					deletegriddatarow('taxgrid',id);
					setTimeout(function()
					{
						tax_data_total();
					},100);
					if(deviceinfo != 'phone'){
						var records = $('#taxgrid .gridcontent div.data-content div').length;
					}else{
						var records = $('#taxgrid .gridcontent div.wrappercontent div').length;
					}
					
					if(records == 0){
						$('#taxcategory').select2('val','').trigger('change');
					}
				} else {
					alertpopup('Select Tax Record');
				}
		});
		$("#taxclearicon").click(function(){
			cleargriddata('taxgrid');
			$('#taxcategory').select2('val','').trigger('change');
			setTimeout(function()
			{
				tax_data_total();
			},200);
		});		
		$("#chargeclearicon").click(function(){
			cleargriddata('chargesgrid');
			$('#chargecategory').select2('val','').trigger('change');
			setTimeout(function()
			{
				charge_data_total();
			},200);
		});
		$("#chargedeleteicon").click(function(){
				var id = $('#chargesgrid div.gridcontent div.active').attr('id');
				if(id){
					/*delete grid data*/
					deletegriddatarow('chargesgrid',id);
					setTimeout(function()
					{
						charge_data_total();
					},200);
					if(deviceinfo != 'phone'){
						if($('#chargesgrid .gridcontent div.data-content div').length == 0) {
						$('#chargecategory').select2('val','').trigger('change');
						}
					}else{
						if($('#chargesgrid .gridcontent div.wrappercontent div').length == 0) {
							$('#chargecategory').select2('val','').trigger('change');
						}
					}					
				} else {
					alertpopupdouble('Select Charge Record');
				}
		});
		$("#chargeamount").click(function(){
			//identify the type(group/individual);
			var type = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 2 && checkVariable('productid') ==true){	//individual
				var g_data = $('#chargegriddata').val();
				var charge_category_id= 'chargecategory';			
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$("#additionaloverlay").fadeIn();
					chargesgrid();
					$('#'+charge_category_id+'').select2('val','').trigger('change');
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0) {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						setTimeout(function(){
							$('#'+charge_category_id+'').select2('val',main_data.id);
							loadgriddata('chargesgrid',grid_data);
						},200);
					} else {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#'+charge_category_id+'').select2('val','').trigger('change');
					} 
				}
				setTimeout(function()
				{
					charge_data_total();
					$("#s2id_chargecategory").select2('focus');
				},200);
			}  
		});
		{//group special function autoset the tax and charges in the summary
			$("#taxmasterid").change(function() {	
				taxgrid();
				if(checkVariable('taxmasterid') == true){
					//identify the type(group/individual);
					var type = '';
					var taxmasterid = $('#taxmasterid').val();
					if(taxmasterid){
						type = 3;
					}else{
						type = 2;
					}
					if(type == 3){
						$('#grouptaxgriddata').val('');
						setTimeout(function() {
							$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
						},200);
						setTimeout(function() {
							var griddataid = 'grouptaxgriddata';
							var tax_category_id = 'taxmasterid';
							var tax_amount_field = 'grouptaxamount';
							var tax_json ={}; //declared as object
							tax_json.id = $('#'+tax_category_id+'').val(); //categoryid
							tax_json.data = getgridrowsdata("taxgrid");//taxgrid				
							$('#'+griddataid+'').val(JSON.stringify(tax_json));
							var totalcharge = parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
							$('#'+tax_amount_field+'').val(totalcharge.toFixed(PRECISION));
							calculatesummarydetail();
						},1000);
					}				
				}
			});
			$("#additionalchargecategoryid").change(function() {
				chargesgrid();
				if(checkVariable('additionalchargecategoryid') == true){				
					$('#groupchargegriddata').val('');
					setTimeout(function() {
						$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
					},200);
					setTimeout(function() {
						var griddataid = 'groupchargegriddata';
						var charge_category_id = 'additionalchargecategoryid';
						var charge_amount_field = 'groupchargeamount';	
						var charge_json ={}; //declared as object
						charge_json.id = $('#'+charge_category_id+'').val(); //categoryid
						charge_json.data = getgridrowsdata('chargesgrid');	//chargesgrid			
						$('#'+griddataid+'').val(JSON.stringify(charge_json));
						var totalcharge = parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
						$('#'+charge_amount_field+'').val(totalcharge.toFixed(PRECISION));
						calculatesummarydetail();
					},1000);
				}else{
					$('#groupchargeamount').val('');
					setTimeout(function(){
						calculatesummarydetail();
					},5);
				}
			});
		}
		//summary additional amount overlay
		$("#groupchargeamount").click(function()
		{
			//identify the type(group/individual);
			var type = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 3){	//group
				var g_data = $('#groupchargegriddata').val();
				var charge_category_id= 'chargecategory';		
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
					$("#additionaloverlay").fadeIn();
					chargesgrid();
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0){	
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#'+charge_category_id+'').select2('val',main_data.id);
						loadgriddata('chargesgrid',grid_data);						
					} else {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');						
					} 
				}
				setTimeout(function()
				{
					charge_data_total();
				},200);
				$("#chargecategory").focus();
			} 
		});
		//invite overlay fade out
		$("#discountclose").click(function()
		{
			$("#discountoverlay").fadeOut();
			var value=parseFloat($('#discountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#discounttypeid').select2('val','').trigger('change');
			}
			$('#pretaxtotal').focus();
		});
		//recurrence overlay fadeOut
		$("#taxclose").click(function()
		{
			productnetcalculate();	
			var taxid = $("#taxcatid").val();
			var product = $("#productid").val();		
			$("#taxoverlay").fadeOut();
		});
		$("#additionalclose").click(function(){
			productnetcalculate();	
			var id = $("#addcatid").val();
			var product=checkVariable('productid');	
			$("#additionaloverlay").fadeOut();
		});
		$('#discountsubmit').click(function(e){
			
			$("#individualdiscountvalidation").validationEngine('validate');			
		});
		$("#individualdiscountvalidation").validationEngine({
			onSuccess: function() {									
				setTimeout(function()
				{
					setzero(['grossamount']); //sets zero on EMPTY					
					var discount_json ={}; //declared as object
					discount_json.typeid = $('#discounttypeid').val(); //discount typeid
					discount_json.value = $('#discountpercent').val();	//discount values		
					$('#discountdata').val(JSON.stringify(discount_json)); //saves discount data in hiddenfield(JSON object)
					var grossamount = $('#grossamount').val();
					var discount_amount=discountcalculation(discount_json,grossamount); //to calculate discount
					$('#discountamount').val(discount_amount);
					$('#quantity').trigger('focusout');		//after succesful discount trigger other events	
				},10);
				$("#discountoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});	
		//charge change calculations
		$("#chargecategory").change(function()
		{
			var id = $(this).val();
			//identify the type(group/individual);
			var typeid = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				typeid = 3;
			}else{
				typeid = 2;
			}
			var territory = $('#territoryid').val(); //region
			var conversionrate = $('#currencyconvresionrate').val(); //conversion rate
			var currency = $('#currencyid').val(); //conversion rate
			cleargriddata('chargesgrid');
			var grossamount = $('#grossamount').val();
			var totalnetamount = $('#totalnetamount').val();
			if(id != ''){
				var transmethod = MODULEID;
				$.ajax({
					url:base_url+'Quote/chargecategoryload?chargecategoryid='+id+'&grossamount='+grossamount+'&territory='+territory+'&conversionrate='+conversionrate+'&currency='+currency+'&typeid='+typeid+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							loadinlinegriddata('chargesgrid',data.rows,'json');
							setTimeout(function() {
								charge_data_total();
							},200);
							/* data row select event */
							datarowselectevt();
							/* column resize */
							columnresize('chargesgrid');
						}
					},
				});
			}			
		});
		//pricebook_currencyconv_submit
		{
			$('#pricebook_currencyconv_submit').click(function(e){
				$("#pricebookcurrencyconv_validate").validationEngine('validate');			
			});
			jQuery("#pricebookcurrencyconv_validate").validationEngine({
					onSuccess: function() { 
						var pb_sellingprice=$("#pricebook_sellingprice").val();	 //pricebook sellingprice
						var pb_currentcurrency = $("#currentcurrency").val();
						var pb_pricebookcurrency = $("#pricebookcurrency").val();
						var pb_conversionrate=$("#pricebook_currencyconv").val();	 //pricebook sellingprice
						$("#currentcurrencyid").val(pb_currentcurrency);
						$("#pricebookcurrencyid").val(pb_pricebookcurrency);
						$("#pricebook_currencyconvrate").val(pb_conversionrate);
						PRICBOOKCONV_VAL = pb_conversionrate;
						var productid = $("#productid").val();
						if(productid){
							var finalvalue = listprice(pb_sellingprice,pb_conversionrate);
							var conversionrate = $('#currencyconvresionrate').val();
							finalvalue = listprice(finalvalue,conversionrate);
							$('#sellingprice').val(finalvalue).trigger('focusout'); //sets the value and triggerchange calc
						}	
						$("#pricebookcurrencyconvoverlay,#pricebookoverlay").fadeOut();
					},
					onFailure: function() {	
					}
			});
		}
		{//adjustment overlay
			$("#adjustmentamount").click(function()
			{
				var adjustment_data = $('#groupadjustmentdata').val();
				var parse_adjustment = $.parseJSON(adjustment_data);				
				if(parse_adjustment == null){ //if no previous data exits then refresh the adjusttment fields
					$('#adjustmenttypeid').select2('val','2').trigger('change');
					$('#adjustmentvalue').val(0);				
				} else {							
					$('#adjustmenttypeid').select2('val',parse_adjustment.typeid).trigger('change');
					$('#adjustmentvalue').val(parse_adjustment.value);
				}				
				$("#adjustmentoverlay").fadeIn();
				Materialize.updateTextFields();
			});
			$("#adjustmentclose").click(function()
			{
				clearform('clearadjustmentform');
				$("#adjustmentoverlay").fadeOut();
			});
			$("#adjustmentadd").click(function()
			{
				$('#adjustmenttypeid').trigger('change'); //to liveup adjustmentdd
				setzero(['adjustmentvalue']);				
				var adjustment_json ={}; //declared as object
				adjustment_json.typeid = $('#adjustmenttypeid').val(); //adjustment type
				adjustment_json.value = $('#adjustmentvalue').val(); // adjustment value			
				$('#groupadjustmentdata').val(JSON.stringify(adjustment_json));				
				var adjustment_value=parseFloat($('#adjustmentvalue').val());				
				$("#adjustmentamount").val(adjustment_value.toFixed(PRECISION));
				$("#adjustmentoverlay").fadeOut();
				//?trigger point for calculation
				gridsummeryvaluefetch('','','');
			});
		}
	}
	{//productsearchsubmit
		
		$("#sellingprice").focusout(function()
		{
			var vl=$(this).val();
			if(checkVariable('sellingprice') == false || isNaN(vl)){	
				$("#sellingprice").val(0);
			}
			$("#quantity").trigger('focusout');
		});
		$("#quantity").focusout(function()
		{			
			var temo_qty=$('#quantity').val();
			if(isNaN(temo_qty)){
				$('#quantity').val(1);
			}else{
				var stock = parseInt($('#instock').val());
				var qty = parseInt($('#quantity').val());
				if(qty > stock){
					alertpopup("Stock should be greater than quantity");
					$("#quantity").val(1);
					$("#touomid").val('');
					$('#conversionrate').val('');
					$('#conversionquantity').val('');
				}else{
					if(checkVariable('quantity') == true ){
						$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
						var round_qty = parseFloat(this.value);
						$(this).val(round_qty.toFixed(QUANTITY_PRECISION));				
						var quan = $("#quantity").val();
						var sellprice = $("#sellingprice").val();
						var gross = parseFloat(quan) * parseFloat(sellprice);
						$("#grossamount").val(gross.toFixed(PRECISION));
						setTimeout(function(){
							var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
							var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
							$('#discountamount').val(discount_amount);
							//pretax-total
							var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
							$("#pretaxtotal").val(pretax.toFixed(PRECISION));
						},10);	
						update_taxgriddata();						
						update_chargegriddata();
						productnetcalculate();
					}
				}
			}			
		});
		$("#productsearchsubmit").click(function()
		{
			var selectedrow = $('#productsearchgrid div.gridcontent div.active').attr('id');
			if(selectedrow){
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
			}			
		});		
		$("#modulenumberevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				var val = $('#moduleid').val();
				if(val > 0){
					$('#mnsearchoverlay').fadeIn();	
					$('#mnsearchclose').focus();	
					if($.inArray(val, ["216","85","94"])!= -1) {
						modulenumbersearchgrid(val);
					} else if($.inArray(val, ["217","87","96"]) != -1) {
						modulenumbersearchgrid(val);
					} else if($.inArray(val, ["226","86","95"]) != -1) {
						modulenumbersearchgrid(val);
					}else if(val == 249) {
						modulenumbersearchgrid(249);
					}
				}	
			}
		});
		$("#mnsearchclose").click(function()
		{
			$('#mnsearchoverlay').fadeOut();
			$("#s2id_accountid").select2('focus');
		});
	}	
	{//redirected from quote to purchaseorder.
		var quotetopurchaseorder = sessionStorage.getItem("convertquotetopurchaseorder"); 
		if(quotetopurchaseorder != '' && quotetopurchaseorder != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertquotetopurchaseorder");
				$("#addicon").trigger('click');
				if(softwareindustryid == 1){
					$('#moduleid').select2('val','216').trigger('change');
					var sourcemodule = 216; //quote
				}else if(softwareindustryid == 2){
					$('#moduleid').select2('val','94').trigger('change');
					var sourcemodule = 94; //quote
				}else if(softwareindustryid == 4){
					$('#moduleid').select2('val','85').trigger('change');
					var sourcemodule = 85; //quote
				}
				var destinatemodule = MODULEID; //purchaseorder
				//modulenumber
				var no = getmodulenumber('quote',quotetopurchaseorder);
				$('#modulenumber').val(no);	
				//dataretrievals
				conversiondatamapping(quotetopurchaseorder,sourcemodule,destinatemodule);
				Materialize.updateTextFields();
			},100);	
		}
	}
	{//redirected from salesorder to purchaseorder.
		var sotopurchaseorder = sessionStorage.getItem("convertsalesordertopurchaseorder"); 
		if(sotopurchaseorder != '' && sotopurchaseorder != null) {
			setTimeout(function() {
				sessionStorage.removeItem("convertsalesordertopurchaseorder");
				$("#addicon").trigger('click');
				if(softwareindustryid == 1){
					$('#moduleid').select2('val','217').trigger('change');
					var sourcemodule = 217; //so
				}else if(softwareindustryid == 2){
					$('#moduleid').select2('val','96').trigger('change');
					var sourcemodule = 96; //so
				}else if(softwareindustryid == 4){
					$('#moduleid').select2('val','87').trigger('change');
					var sourcemodule = 87; //so
				}
				var destinatemodule = MODULEID; //purchaseorder
				//modulenumber
				var no = getmodulenumber('salesorder',sotopurchaseorder);
				$('#modulenumber').val(no);	
				//dataretrievals
				conversiondatamapping(sotopurchaseorder,sourcemodule,destinatemodule);
				Materialize.updateTextFields();
			},100);	
		}
	}
	{ //Click to call and sms icon code - gowtham
		$("#mobilenumberoverlayclose").click(function() {
			$("#mobilenumbershow").hide();
		});	
		//sms icon click
		$("#smsicon").click(function() {
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#mobilenumbershow").show();
								$("#smsmoduledivhid").hide();
								$("#smsrecordid").val(datarowid);
								$("#smsmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'smsmobilenumid');
							} else if(mobilenumber.length == 1) {
								sessionStorage.setItem("mobilenumber",mobilenumber);
								sessionStorage.setItem("viewfieldids",viewfieldids);
								sessionStorage.setItem("recordis",datarowid);
								window.location = base_url+'Sms';
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").show();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'smsmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});	
		//sms module change
		$("#smsmodule").change(function() {
			var moduleid =	$("#smsmoduleid").val(); //main module
			var linkmoduleid =	$("#smsmodule").val(); // link module
			var recordid = $("#smsrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
			}
		});
		//call module change
		$("#callmodule").change(function() {
			var moduleid =	$("#callmoduleid").val(); //main module
			var linkmoduleid =	$("#callmodule").val(); // link module
			var recordid = $("#callrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
			}
		});
		//sms mobile number change
		$("#smsmobilenumid").change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#smsmobilenum").val(selectid);
		});
		//sms mobile overlay submit
		$("#mobilenumbersubmit").click(function() {
			var datarowid = $("#smsrecordid").val();
			var viewfieldids =$("#smsmoduleid").val();
			var mobilenumber =$("#smsmobilenum").val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem("mobilenumber",mobilenumber);
				sessionStorage.setItem("viewfieldids",viewfieldids);
				sessionStorage.setItem("recordis",datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}	
		});
		//click to call submit
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").show();
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'callmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		//click to call submit
		$("#callnumbersubmit").click(function()  {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
	}	
	$("#conversionrate,#conversionquantity").focusout(function() {
		var crate = $("#conversionrate").val();
		if(isNaN(crate)) {
			$('#conversionrate').val(1);
		}
		var cquan = $("#conversionquantity").val();
		if(isNaN(cquan)) {
			$('#conversionquantity').val(1);
		}
		if(checkVariable('conversionrate') == true && checkVariable('conversionquantity') == true){
			var crate = $("#conversionrate").val();
			var cquan = $("#conversionquantity").val();
			var quantity = parseFloat(crate)*parseFloat(cquan);
			$("#quantity").val(quantity);
			$("#quantity").trigger('focusout');
		}
		
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,purchaseordergrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,purchaseordergrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
	{// Get and Set date
		function getandsetdate(dateset){ 
			$('#paymentduedate').datetimepicker('option', 'minDate', dateset);
		}
	}

{// view create success function
    function viewcreatesuccfun(viewname)
	{
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main view Grid
	function purchaseordergrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#purchaseorderpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#purchaseorderpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#purchaseordergrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.purchaseorderheadercolsort').hasClass('datasort') ? $('.purchaseorderheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.purchaseorderheadercolsort').hasClass('datasort') ? $('.purchaseorderheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.purchaseorderheadercolsort').hasClass('datasort') ? $('.purchaseorderheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=purchaseorder&primaryid=purchaseorderid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#purchaseordergrid').empty();
				$('#purchaseordergrid').append(data.content);
				$('#purchaseordergridfooter').empty();
				$('#purchaseordergridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('purchaseordergrid');
				{//sorting
					$('.purchaseorderheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.purchaseorderheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#purchaseorderpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.purchaseorderheadercolsort').hasClass('datasort') ? $('.purchaseorderheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.purchaseorderheadercolsort').hasClass('datasort') ? $('.purchaseorderheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#purchaseordergrid .gridcontent').scrollLeft();
						purchaseordergrid(page,rowcount);
						$('#purchaseordergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('purchaseorderheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						purchaseordergrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#purchaseorderpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						purchaseordergrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{//Redirected form Home
					var purchaseorderideditvalue = sessionStorage.getItem("purchaseorderidforedit"); 
					if(purchaseorderideditvalue != null){
						setTimeout(function(){ 
							purchaseordereditdatafetchfun(purchaseorderideditvalue);
							sessionStorage.removeItem("purchaseorderidforedit");
						},100);	
					}
				}
				//Material select
				$('#purchaseorderpgrowcount').material_select();
			},
		});		
	}
}
{
	function getmoduleid(){
		$.ajax({
			url:base_url+'Purchaseorder/getmoduleid',
			async:false,
			cache:false,
			success: function(data) {
				MODULEID=data;
			}
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			purchaseorderproaddgrid1();
			COMPANY_NAME = getcompanyname();
			// Append The Currency data
			appendcurrency('currencyid');
			e.preventDefault();
			addslideup('purchaseordercreationview','purchaseordercreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','purchaseordercreationformadd');
			$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").show();
			//form field first focus
			firstfieldfocus();
			//for autonumber
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setTimeout(function() {
				$("#termsandconditionid").trigger('change');
			},10);
			froalaset(froalaarray);
			$('#payeename').val(COMPANY_NAME);
			clearformgriddata();
			$('#defaultcurrency').select2('val','');
			$('#convcurrency').select2('val','');
			$('#currencyconv').val('');
			$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');		
			$(".fr-element").attr("contenteditable", 'true');
			setdefaultproperty();
			$('#additionalchargecategoryid,#taxmasterid,#pricebookid,#accountid,#contactid,#opportunityid,#mname').select2("val",'');
			//disable fields
			$('#pricebookid,#accountid,#contactid,#opportunityid,#mname,#taxmasterid,#additionalchargecategoryid').select2("readonly", false);
			//form field first focus			
			$('#currentmode').val(1);
			var invdate = $("#purchaseorderdate").val();
			getandsetdate(invdate);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			METHOD = 'ADD';
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			QUANTITY_PRECISION =0;
			if (datarowid) {
				var stage=purchaseorderstage(datarowid);
				if(stage == 48){ //Booked
					alertpopup("This purchaseorder is Booked !!!");
				} else if(stage == 49){ //cancel
					alertpopup("This purchaseorder is Cancelled");
				} else {
					froalaset(froalaarray);
					purchaseorderproaddgrid1();
					purchaseordereditdatafetchfun(datarowid);
					Materialize.updateTextFields();
					$('#payeename').val(COMPANY_NAME);
					showhideiconsfun('editclick','purchaseordercreationformadd');
					$("#purchaseorderproingriddel1,#purchaseorderpayingriddel2").show();
					$(".fr-element").attr("contenteditable", 'true');
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					fortabtouch = 1;
					METHOD = 'ADD';
					Operation = 1; //for pagination
				}
			} else {
				alertpopup("Please select a row");			
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#purchaseordergrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				var stage=purchaseorderstage(datarowid);
				if(stage == 48){ //Booked
					alertpopup("purchaseorder Booked!!!Cannot Delete!!");
				} else if(stage == 49){ //Cancel
					alertpopup("This purchaseorder is Cancelled");
				} 
				else {
					$("#basedeleteoverlay").fadeIn();
					$('#primarydataid').val(datarowid)
					$("#basedeleteyes").focus();
				}
			}
			else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			purchaseordermaindelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#purchaseorderpgnumcnt li .page-text .active').data('rowcount');
		purchaseordergrid(page,rowcount);
	}
}
{// purchaseorder product grid
	function purchaseorderproaddgrid1() {
		var wwidth = $("#purchaseorderproaddgrid1").width();
		var wheight = $("#purchaseorderproaddgrid1").height();
		var tabgroupid = 0;
		if(MODULEID == 225){
			tabgroupid = 61;
		}else if(MODULEID == 99){
			tabgroupid = 268;
		}else if(MODULEID == 88){
			tabgroupid = 222;
		}
		$.ajax({
			url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=purchaseorderproduct",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#purchaseorderproaddgrid1").empty();
				$("#purchaseorderproaddgrid1").append(data.content);
				$("#purchaseorderproaddgrid1footer").empty();
				$("#purchaseorderproaddgrid1footer").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('purchaseorderproaddgrid1');
				var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','purchaseorderdetailid','uomdata'];
				gridfieldhide('purchaseorderproaddgrid1',hideprodgridcol);
			},
		});
	}
}
{// New data add submit function
	function purchaseordercreate() {
		var amp = '&';				
		var gridname = $('#gridnameinfo').val();
		if(gridname != '') {
			var gridnames  = [];
			gridnames = gridname.split(',');
			var datalength = gridnames.length;
			var noofrows=0;
			var addgriddata='';
			if(deviceinfo != 'phone'){
				for(var j=0;j<datalength;j++) {
					if(j!=0) {
						addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
						noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
					} else {
						addgriddata = getgridrowsdata(gridnames[j]);
						noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
					}
				}
			}else{
				for(var j=0;j<datalength;j++) {
					if(j!=0) {
						addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
						noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
					} else {
						addgriddata = getgridrowsdata(gridnames[j]);
						noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
					}
				}
			}			
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var tandcdata = 0;
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementspartabname = $('#elementspartabname').val();
		var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
		var resctable = $('#resctable').val();			
		//editor data
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Purchaseorder/purchaseordercreate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"elementsname="+elementsname+amp+"elementstable="+elementstable+amp+"elementscolmn="+elementscolmn+amp+"elementspartabname="+elementspartabname+amp+"resctable="+resctable+amp+"griddatapartabnameinfo="+griddatapartabnameinfo+amp+"tandcdata="+tandcdata+amp+"purchaseordermodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$(".ftab").trigger('click');				
					resetFields();
					$('#purchaseordercreationformadd').hide();
					$('#purchaseordercreationview').fadeIn(1000);
					refreshgrid();		
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
					$('#additionalcategory,#discountcalculationtype').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
				} else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
				} else {
					alertpopup("Error during create.Please refresh and try again");
				}
				$('#dataaddsbtn').removeClass('singlesubmitonly');
			},
		});
	}
}
{// Old information show in form
	function retrievepurchaseorderdata(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Purchaseorder/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable+"&purchaseordermodule="+MODULEID, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {				
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#purchaseordercreationformadd').hide();
					$('#purchaseordercreationview').fadeIn(1000);
					refreshgrid();
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
				} else {
					addslideup('purchaseordercreationview','purchaseordercreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					$('#pricebookcurrency').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconv').val(data['pricebook_currencyconvrate']);
					$('#currentcurrency').val(data['currentcurrencyid']);
					$('#currencyconv').val(data['currencyconvresionrate']);
					$('#convcurrency').val(data['currencyid']);
					PRICBOOKCONV_VAL = data['pricebook_currencyconvrate'];
					$('#pricebookcurrencyid').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconvrate').val(data['pricebook_currencyconvrate']);
					$('#currentcurrencyid').val(data['currentcurrencyid']);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					var ext_data = data.summary;
					//summary data settings//
					{
						$('#grouptaxgriddata').val(ext_data['grouptax']);
						$('#groupchargegriddata').val(ext_data['groupaddcharge']);
						$('#groupadjustmentdata').val(ext_data['groupadjustment']);
						$('#groupdiscountdata').val(ext_data['groupdiscount']);
						$('#paidamount').val(ext_data['paidamount']);
						$('#writeoffamount').val(ext_data['writeoffamount']);
						$('#balanceamount').val(ext_data['balanceamount']);
						$('#totalpayable').val(ext_data['totalpayable']);
					}					
					var pid = data['parentpurchaseorderid'];
					purchaseordergriddetail(datarowid);				
					primaryaddvalfetch(datarowid);
					editordatafetch(froalaarray,data);
				}
			}
		});	
	}
}
{// Salesorder product and payment detail
	function purchaseordergriddetail(pid) {	
		//salesorder product detail		
		if(pid!='') {
			$.ajax({
				url:base_url+'Purchaseorder/purchaseorderproductdetailfetch?primarydataid='+pid+"&purchaseordermodule="+MODULEID,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('purchaseorderproaddgrid1',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('purchaseorderproaddgrid1');
						var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','purchaseorderdetailid','uomdata'];
						gridfieldhide('purchaseorderproaddgrid1',hideprodgridcol);
					}
				},
			});
		}
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid) {
		$.ajax({
			url:base_url+"Purchaseorder/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
	}
}
{// Update old information
	function purchaseorderupdate() {
		var amp = '&';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				}
			}
		}		
		var sendformadddata = JSON.stringify(addgriddata); 
		var noofrows = noofrows;
		//editor data
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var resctable = $('#resctable').val();	
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;		
		$.ajax({
			url: base_url + "Purchaseorder/datainformationupdate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"purchaseordermodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg){
				if (msg == true){
					$(".ftab").trigger('click');				
					resetFields();
					$('#purchaseordercreationformadd').hide();
					$('#purchaseordercreationview').fadeIn(1000);
					refreshgrid();
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');	
					$('#additionalcategory,#discountcalculationtype,#taxmasterid,#additionalchargecategoryid,#summarydiscountcalculationtype').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
				} else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
				} else {
					alertpopup("Error during update.Please close and try again");
				}
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');			
			},
		});
	}
}
{// Record delete function
	function purchaseordermaindelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Purchaseorder/purchaseorderdelete?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable+"&purchaseordermodule="+MODULEID,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == "TRUE") { 					
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {					
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Quote number generation
	
	function appendleadcontact(val,id) {
	   if(val == 2){
		   var table='contact';
	   }  else{
		   var table='lead';
	   }
	   $('#'+id+'').empty();
	   $('#'+id+'').append($("<option></option>"));
	   $.ajax({
			url:base_url+'Base/getdropdownoptions?table='+table,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
			  if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+id+'')
						.append($("<option></option>")
						.attr("value",data[index]['id'])
						.text(data[index]['name']));
					});
				}    
			   $('#'+id+'').trigger('change');
			},
	   });
	}
}
{// Setaddress
	function setaddress(id){
		var value=$('#'+id+'').val();
		if(id == 'billingaddresstype'){
			var type='Billing';
		} else{
			var type='Shipping';
		}
		//account address
		if(value == 2){
			var accountid=$('#accountid').val();
			getaddress(accountid,'account',type);
		} else if(value == 3){   
			var addressid =$('#contactid').val();
			getaddress(addressid,'contact',type);
		}
		//swap the billing to shipping
		else if(value == 5){
			$('#billingaddress').val($('#shippingaddress').val());
			$('#billingpincode').val($('#shippingpincode').val());
			$('#billingcity').val($('#shippingcity').val());
			$('#billingstate').val($('#shippingstate').val());
			$('#billingcountry').val($('#shippingcountry').val());	
		}
		//swap the shipping to billing
		else if(value == 6){
			$('#shippingaddress').val($('#billingaddress').val());
			$('#shippingpincode').val($('#billingpincode').val());
			$('#shippingcity').val($('#billingcity').val());
			$('#shippingstate').val($('#billingstate').val());
			$('#shippingcountry').val($('#billingcountry').val());	
		}
	}
}
{// Get Address
	function getaddress(id,table,type) {
		if(id != null && id != '' && table != null && table !='') {
			var append=type.toLowerCase();
			$.ajax({
				url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.result == true) {
						var datanames = ['5','address','pincode','city','state','country'];
						var textboxname = ['5',''+append+'address',''+append+'pincode',''+append+'city',''+append+'state',''+append+'country'];
						textboxsetvalue(textboxname,datanames,data,[]);
					}			
				},
			});
		}
	}
}
{// Terms and condition data fetch
	function tandcdatafrtch(id) {
		$.ajax({
			url:base_url+"Purchaseorder/termsandcontdatafetch?id="+id,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data)
			{
				filenamevaluefetch(data);
			},
		});
	}
}
{// Editor value fetch
	function filenamevaluefetch(filename) {
		//checks whether the Terms & condition is empty
		if(filename == '' || filename == null ) {
			froalaset(froalaarray);
		} else {
			$.ajax({
				url: base_url + "Purchaseorder/editervaluefetch?filename="+filename,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					froalaedit(data,'purchaseordertermsandcondition_editor');
				},
			});
		}	
	}
}
{// Get product details - quote
	function getproductdetails(val) {
		if(checkValue(val) == true){
		var pricebook = $('#pricebookid').val();
		$("#touomid").empty();		
		$.ajax({
			url:base_url+'Base/getproductdetails?id='+val+'&pricebook='+pricebook,
			dataType:'json',
			async:false,
			cache : false,
			success: function(data) {
				var datanames = ['1','description'];
				var textboxname = ['1','descriptiondetail'];
				$uomdata = data.touomarr;
				$uomlength = $uomdata.length;
				for (var i = 0; i < $uomlength; i++) {
					$('#touomid')
					.append($("<option></option>")
	                .attr("value",$uomdata[i]['uomtoid'])
	                .attr("data-touomidhidden",$uomdata[i]['uomname'])
	                .attr("data-conrate",$uomdata[i]['conversionrate'])
	                .attr("data-symbol",$uomdata[i]['symbol'])
	                .attr("data-precision",$uomdata[i]['uomprecision'])
	                .text($uomdata[i]['uomname']));
				}	
				$('#touomid').select2('val','').trigger('change');
				$('#conversionrate').val($('#currencyconvresionrate').val());
				textboxsetvalue(textboxname,datanames,data,[]);
				productstoragefetch(val);				
				PRODUCTTAXABLE = data.taxable;
				var uomid = data.uomid;
				$('#productuomid').val(uomid);
				$("#uomid").select2('val',uomid).trigger('change');
				$("#uomid").attr('readonly','readonly');
				setTimeout(function(){
					$('#conversionquantity').val(1);
					var quantity = parseFloat($('#conversionquantity').val())*parseFloat($('#conversionrate').val());	
					$('#quantity').val(quantity).trigger('focusout');
					var unitprice = data.unitprice;
					if(unitprice == ''){
						var unitprice = 0;
					}						
					if(data.prodinprice == 0){
						$("#pricebookclickevent").show();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						var list_price = listprice(unitprice,currencyconvresionrate);
						$('#unitprice').val(list_price);					
						//calculate selling price
						var list_price = listprice(data.sellingprice,currencyconvresionrate);
						$('#sellingprice').val(list_price);
					} else {
						$("#pricebookclickevent").hide();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						var sellcurrencyconvresionrate =PRICBOOKCONV_VAL;						
						var list_price = listprice(unitprice,currencyconvresionrate);
						$('#unitprice').val(list_price);					
						//calculate selling price
						var conversell = listprice(data.sellingprice,currencyconvresionrate);
						var list_price = listprice(conversell,sellcurrencyconvresionrate);
						$('#sellingprice').val(list_price);
					}		
					$('#quantity').val('1');
					//gross amount
					$("#grossamount").val($('#sellingprice').val());
					//pretax-total
					setzero(['grossamount','discountamount']);
					var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
					$("#pretaxtotal").val(pretax.toFixed(PRECISION));
					productnetcalculate();
					Materialize.updateTextFields();
				},100);						
			},
		});
		}
	}
}
{// Discount calculation
	function itemcalculation() {
		setzero(['sellingprice','discountamount','taxamount','additionalamount','grossamount']);
		var sp=parseFloat($('#grossamount').val());
		var discount=parseFloat($('#discountamount').val());
		var tax=parseFloat($('#taxamount').val());
		if(isNaN(tax) || tax == "") {
			tax = 0;
		}
		var addamt=parseFloat($('#additionalamount').val());
		if(isNaN(addamt) || addamt=="") {
			addamt = 0;
		}
		var output=parseFloat(sp+tax+addamt-discount);
		$('#netamount').val(output.toFixed(2));	
		$("#discountoverlay").fadeOut();
	}
	
}
{// Tax overlay selling value assign
	function sellingvalueset(taxvalue,i) {
		var selprice = $("#sellingprice").val();
		var value = (parseFloat(taxvalue) / 100);
		var taxtot = (parseFloat(value) * parseFloat(selprice));
		$("#sellingamt"+i).val(taxtot);
		var selamt = $("#sellingamt"+i).val();
		quoteseiprice.push(selamt);
		quotetaxvalue.push(taxvalue);
	}
}
{// Value assign function
	function valueasignfunction() {
		var selvalue = (parseFloat($("#sellingamt0").val())+parseFloat($("#sellingamt1").val()));
		$("#totalselling").val(selvalue);
		$("#taxamount").val(selvalue);
	}
}

{// Additional amount add
	function valueassignforadditional(value,cval) {
		var addamount = $("#addamount"+cval+"").val();
		var sellingamount = $("#sellingprice").val();
		if(value ==2) {
			var addval = parseFloat(addamount)+parseFloat(sellingamount);
			$("#addsellingamt"+cval+"").val(addval);
		} else if(value == 3) {
			var sellingamount = $("#sellingprice").val();
			var addvalue = (parseFloat(addamount) / 100);
			var addamttot = parseFloat(addvalue) * parseFloat(sellingamount);
			$("#addsellingamt"+cval+"").val(addamttot.toFixed());
		}
	}
}
{// Product Net Calculation	
	function productnetcalculate()
	{
		setzero(['grossamount','discountamount','taxamount','chargeamount']);
		var grossamount=parseFloat($('#grossamount').val());
		var discount=parseFloat($('#discountamount').val());	
		var tax=parseFloat($('#taxamount').val());
		var charge=parseFloat($('#chargeamount').val());
		if(softwareindustryid == 2){
			var output=(grossamount-discount+tax).toFixed(PRECISION);
		}else{
			var output=(grossamount-discount+tax+charge).toFixed(PRECISION);
		}		
		$('#netamount').val(output);	
	}
}
{// Grid summery value fetch
	function gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid)
	{
		var taxmode = '';
		var taxmasterid = $("#taxmasterid").val();
		if(taxmasterid){
			taxmode = 3;
		}else{
			taxmode = 2;
		}
		var additionalchargecategoryid = $("#additionalchargecategoryid").val();
		var chargemode = '';
		if(additionalchargecategoryid){
			chargemode = 3;
		}else{
			chargemode = 2;
		}
		var totalnetamount = parseFloat(getgridcolvalue('purchaseorderproaddgrid1','','netamount','sum'));
		$('#totalnetamount').val(totalnetamount.toFixed(PRECISION));
		//pretax-total
		setzero(['totalnetamount','groupdiscountamount']);
		var pretax = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());
		$("#grouppretaxtotal").val(pretax);
		//update group discount.
		var gross = totalnetamount;
		var discount_json = $.parseJSON($('#groupdiscountdata').val()); //individual discount data					
		var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
		$('#groupdiscountamount').val(discount_amount);
		if(taxmode == 3){
			//?update the tax overlaydata
			update_group_taxgriddata();
		}
		if(chargemode == 3){
			//?update the charge overlaydata
			update_group_chargegriddata();
		}		
		setTimeout(function(){
			calculatesummarydetail();
		},5); 
	}	
}

{// Summary value calculation
	function calculatesummarydetail()
	{		
		setzero(['totalnetamount','groupdiscountamount','grouptaxamount','groupchargeamount','adjustmentamount','writeoffamount']);
		var totalnetamount = parseFloat($("#totalnetamount").val());
		var discount = parseFloat($("#groupdiscountamount").val());
		var tax = parseFloat($("#grouptaxamount").val());
		var charge = parseFloat($("#groupchargeamount").val());
		var adjustment =parseFloat($("#adjustmentamount").val());
		var writeoffamount =parseFloat($("#writeoffamount").val());
		if(softwareindustryid == 2){
			var pre_adjustment_total = totalnetamount - discount + tax	;
		}else{
			var pre_adjustment_total = totalnetamount - discount + tax + charge	;
		}		
		if(adjustment > 0){
			var adjustment_data=$('#groupadjustmentdata').val();			
			var parse_adjustment = $.parseJSON(adjustment_data);
			if(parse_adjustment != null){
				if(parse_adjustment.typeid == 3){
					var grandtotal = pre_adjustment_total - adjustment ;
				} else {
					var grandtotal = pre_adjustment_total + adjustment;
				}
			}
		} else {
			var grandtotal=pre_adjustment_total;
		}
		$("#grandtotal,#totalpayable").val(parseFloat(grandtotal).toFixed(PRECISION));	
		Materialize.updateTextFields();
	}	
}
{// Summary Detail Edit
	function setsummarydataedit(data)
	{
		var textboxname =['11','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','adjustmentvalue','summarynetamount','writeoffamount','balanceamount','mname'];
		var datanames =['11','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','adjustmentvalue','summarynetamount','writeoffamount','balanceamount','mname'];
		var dropdowns =['7','discountmodeid','taxmasterid','additionalchargecategoryid','adjustmenttype','mname'];
		var modedata=data.mode;		
		textboxsetvalue(textboxname,datanames,data.mode,dropdowns);	
		var mode=data.mode;	
	}
}
{// Validate percentage
	function validatepercentage(field)
	{
		var type =$('#discountcalculationtype').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";			
		}
		else{
		}
	}
}
{// Summary Validate Percentage
	function summaryvalidatepercentage(field)
	{
		var type =$('#summarydiscountcalculationtype').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		}
		else{
		}
	}
}
{// Form to Grid validation
	function gridformtogridvalidatefunction(gridnamevalue)
	{//Function call for validate 
		$("#"+gridnamevalue+"validation").validationEngine({
			onSuccess: function() {
				var i = griddataid;
				var gridname = gridynamicname;
				var coldatas = $('#gridcolnames'+i+'').val();
				var columndata = coldatas.split(',');
				var coluiatas = $('#gridcoluitype'+i+'').val();
				var columnuidata = coluiatas.split(',');
				var datalength = columndata.length;
				if(gridname =='purchaseorderproaddgrid1'){
					//disable the type
					$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
					if(METHOD == 'ADD' || METHOD == ''){ //ie add operation	
						formtogriddata(gridname,METHOD,'last','');
					}
					if(METHOD == 'UPDATE'){  //ie edit operation
						formtogriddata(gridname,METHOD,'',UPDATEID);
					}
					/* Hide columns */
					var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','purchaseorderdetailid','uomdata'];
					gridfieldhide('purchaseorderproaddgrid1',hideprodgridcol);
					/* Data row select event */
					datarowselectevt();	
					$('#taxgriddata,#chargegriddata,#discountdata').val(''); //reset the data
					$('#taxcategory,#chargecategory').select2('val',''); //reset the category
					cleargriddata('taxgrid');
					cleargriddata('chargesgrid');					
					METHOD = 'ADD'; //reset
				}
				else if(gridname =='purchaseorderpayaddgrid2'){	
					if(PAYMETHOD == 'ADD' || PAYMETHOD == ''){ //ie add operation	
						formtogriddata(gridname,PAYMETHOD,'last','');
					}
					if(PAYMETHOD == 'UPDATE'){  //ie edit operation
						var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
						formtogriddata(gridname,PAYMETHOD,'',selectedrow);
					}
					/* Data row select event */
					datarowselectevt();
					PAYMETHOD = 'ADD'; //reset				
				}
				//set the new payment number
				var moduleid=217;
				setTimeout(function(){			
					$('#payeename').val(COMPANY_NAME);				
				},150);
				$('#purchaseorderproupdatebutton').hide();	//display the UPDATE button(inner - productgrid)
				$('#purchaseorderproaddbutton').show();	//display the ADD button(inner-productgrid) */
				clearform('gridformclear');
				griddataid = 0;
				gridynamicname = '';
				//close product section
				$('#purchaseorderprocancelbutton').trigger('click');
				//summary calculate
				var gridsumcolname = ['grossamount','discountamount','taxamount','additionalamount','netamount','paymentamount'];
				var sumfieldid = ['summarygrossamount','summarydiscountamount','summarytaxamount','summaryadditionalchargeamount','summarynetamount','paidamount'];
				var gridname = ['purchaseorderproaddgrid1','purchaseorderproaddgrid1','purchaseorderproaddgrid1','purchaseorderproaddgrid1','purchaseorderproaddgrid1','purchaseorderpayaddgrid2'];
				gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid);	
				setTimeout(function(){			
					Materialize.updateTextFields();				
				},100);
				},
				onFailure: function() {
					var dropdownid =['1','productid'];
					dropdownfailureerror(dropdownid);
				}
		});
	}
	
}
{// Edit Function
	function purchaseordereditdatafetchfun(datarowid){
		clearformgriddata();
		froalaset(froalaarray);
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');		
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		retrievepurchaseorderdata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();
		//disable fields
		$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
		$('#accountid,#contactid,#opportunityid,#mname').select2("readonly",false);
		$('#currentmode').val(2);
		var invdate = $("#purchaseorderdate").val();
		getandsetdate(invdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function purchaseorderclonedatafetchfun(datarowid){
		clearformgriddata();
		froalaset(froalaarray);
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');		
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		retrievepurchaseorderdata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();
		//disable fields
		$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
		$('#accountid,#contactid,#opportunityid,#mname').select2("readonly",false);
		$('#currentmode').val(2);
		var invdate = $("#purchaseorderdate").val();
		getandsetdate(invdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Retrieves the salesorder
	function getmoduledata(datarowid)
	{
		var elementsname ="accountid,opportunityid,contactid,crmstatusid,employeetypeid,employeeid,currencyid,pricebookid,taxmasterid,additionalchargecategoryid,carriertypeid,shippingdetail";
		var elementstable = 'salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder,salesorder';//tablemae
		var elementscolmn = "accountid,opportunityid,contactid,crmstatusid,employeetypeid,employeeid,currencyid,pricebookid,taxmasterid,additionalchargecategoryid,carriertypeid,shippingdetail";//tablefieldname
		var elementpartable = 'salesorder';
		$.ajax({
			url:base_url+"Purchaseorder/getmoduledata?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
				var txtboxname = elementsname ;
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);		
			}
		});
		$.ajax({
			url:base_url+"Purchaseorder/getsoaddressdetail?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
		//productdetailarray
		$.ajax({
			url:base_url+'Purchaseorder/getsoproductdetail?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('purchaseorderproaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('purchaseorderproaddgrid1');
					var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','purchaseorderdetailid','uomdata'];
					gridfieldhide('purchaseorderproaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
{	
	function generateuomiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline-block').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">network_cell</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');	
	}
	// Generate icon after loads data - Workaround
	function generatemoduleiconafterload(fieldid,clickevnid){
		$("#"+fieldid+"").css('display','inline').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").next('label').css('display','inline').css('width','75%');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>'); 		
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	function generateiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','block').css('width','90%');
		var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
	}
	{// Clear grid data
		function clearformgriddata() {
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			for(var j=0;j<datalength;j++) {
				cleargriddata(gridnames[j]);
			}
		}
	}
}
	/*
	* purchaseorder stage-data retrival for Lost-Cancel-Draft-Convert
	*/
	function purchaseorderstage(id){
		var purchaseorderstage =0;
		if(checkValue(id) == true ){
			$.ajax({
			url:base_url+"index.php/Purchaseorder/purchaseordercurrentstage",
			data:{purchaseorderid:id},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
					purchaseorderstage = data.purchaseorderstage;
				}		
			});
			return purchaseorderstage;
		} 
	}
	function conversiondatamapping(rowid,sourcemodule,destinatemodule){
		if($.inArray(sourcemodule, ["216","85","94"]) != -1) {
			frommodule = 'quote';
		} else if($.inArray(sourcemodule, ["217","87","96"]) != -1) {
			frommodule = 'salesorder';
		} else if($.inArray(sourcemodule, ["226","86","95"]) != -1){
			frommodule = 'invoice';
		} else if(sourcemodule == 249){
			frommodule = 'materialrequisition';
		}
		$.ajax({
			url:base_url+"Base/getconversiondata", 
			type: "POST",
			data:{moduleid:sourcemodule,primaryid:rowid,tomoduleid:destinatemodule,frommodule:frommodule},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {			
			resetconversiondata(data.main);
			//address data set
			var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
			var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
			var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
			textboxsetvalue(textboxname,datanames,data.address,dropdowns);
			var price_data = data.pricedetails;
			$('#pricebookcurrency').val(price_data.pricebookcurrencyid);
			$('#pricebook_currencyconv').val(price_data.pricebook_currencyconvrate);
			$('#currentcurrency').val(price_data.currentcurrencyid);
			$('#currencyconv').val(price_data.currencyconvresionrate);
			$('#convcurrency').val(price_data.currencyid);
			$('#pricebookcurrencyid').val(price_data.pricebookcurrencyid);
			$('#pricebook_currencyconvrate').val(price_data.pricebook_currencyconvrate);
			$('#currentcurrencyid').val(price_data.currentcurrencyid);
			PRICBOOKCONV_VAL = price_data.pricebook_currencyconvrate;
			var myArr = $.parseJSON(data.linedetail);	
			cleargriddata('purchaseorderproaddgrid1');				
			var gridlength = myArr.length;
			var j=0;
			for(var i=0;i< gridlength;i++){
				addinlinegriddata('purchaseorderproaddgrid1',j+1,myArr[i],'json');
				j++;
			}
			$("#accountid").trigger("change");
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('purchaseorderproaddgrid1');
			if(data.paymentdetail != null){
				var myArr1 = $.parseJSON(data.paymentdetail);			
				var gridlength1 = myArr1.length;
				var j1=0;
				for(var i1=0;i1< gridlength1;i1++){
					addinlinegriddata('purchaseorderpayaddgrid2',j1+1,myArr1[i1],'json');
					j1++;
				}
				/*data row select event*/
				datarowselectevt();
				/*column resize*/
				columnresize('purchaseorderpayaddgrid2');
			}
			{	//summary data settings//
				var ext_data = data.summary;				
				$('#grouptaxgriddata').val(ext_data.grouptax);
				$('#groupchargegriddata').val(ext_data.groupaddcharge);
				$('#groupadjustmentdata').val(ext_data.groupadjustment);
				$('#groupdiscountdata').val(ext_data.groupdiscount);
			}
			setTimeout(function(){
				calculatesummarydetail();
			},80);
			var tandc=data.main;
			var filename=tandc.purchaseordertermsandconditions_editorfilename;
			$("#termsandconditionid").select2('val','');
				if(checkValue(filename) == true){
					filenamevaluefetch(filename); //sets the source terms and condition
				}
			$('#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
			}			
		});
	}
/*
*get the quotenumber/so number/purchaseorder number for given id
*/
function getmodulenumber(table,id){		
	var n='';
	$.ajax({
		url: base_url+"index.php/Quote/getmodulenumber",
		data:{table:table,id:id},
		async:false,
		cache:false,
		success: function(data) {				
			n=data;
		},
	});
	return n;
}
/*
*get the company names
*/
function getcompanyname(){		
	var n='';
	$.ajax({
		url: base_url+"Base/getcompanyname",		
		async:false,
		cache:false,
		success: function(data) {				
			n=data;
		},
	});
	return n;
}
// conversion based calculation
function conversionbasedproductcalculation() {
	var temo_qty=$('#quantity').val();
	if(isNaN(temo_qty)){
		$('#quantity').val(1);
		$('#productquantity').val(1);
	}
	if(checkVariable('quantity') == true ){
		var round_qty = parseFloat(this.value);
		$(this).val(round_qty.toFixed(QUANTITY_PRECISION));				
		var quan = $("#quantity").val();
		var sellprice = $("#sellingprice").val();
		var gross = parseFloat(quan) * parseFloat(sellprice);
		$("#grossamount").val(gross.toFixed(PRECISION));
		setTimeout(function(){
			var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
			var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
			$('#discountamount').val(discount_amount);
			//pretax-total
			var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
			$("#pretaxtotal").val(pretax.toFixed(PRECISION));
		},10);	
		update_taxgriddata();						
		update_chargegriddata();
		productnetcalculate();
	}
}
//product storage get - in stock 
function productstoragefetch(productid) {
	$.ajax({
		url:base_url+"Purchaseorder/productstoragefetchfun?productid="+productid,
		dataType : 'json',
		async:false,
		cache:false,
		success :function(data) {
			nmsg = $.trim(data);
			$("#instock").val(nmsg)
		},
	});
}