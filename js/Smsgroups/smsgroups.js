$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	type = 'SMS';
	{// Grid Calling Function
		smsgroupsaddgrid();
		//crud action
		crudactionenable();
	}
	{// For touch
		fortabtouch = 0;
	}
	{//for keyboard global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$('#groupcloseaddform').hide();
	{//enable support icon
		$(".supportoverlay").css("display","inline-block");
	}
	{//hide show from template type
		$('#templatetypeid').change(function() {
			var ttid = $("#templatetypeid").val();
			fieldchangeonload(ttid);
		});
	}
	{//validation for menu category
		$('#smsgroupssavebutton').click(function(e) {
			$("#smsgroupsformaddwizard").validationEngine('validate');
		});
		$("#smsgroupsformaddwizard").validationEngine({
			onSuccess: function() {
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','smsgrptypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//validation for menu category edit
		$('#smsgroupsdataupdatesubbtn').click(function(e) {
			$("#smsgroupsformeditwizard").validationEngine('validate');
		});
		$("#smsgroupsformeditwizard").validationEngine({
			onSuccess: function() {
				smsgrpupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','smsgrptypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//reload
		$("#reloadicon").click(function() {
			$("#smsgrpsubmit,#deleteicon").show();
			$("#smsgrpupdate").hide();
			refreshgrid();
			resetFields();
		});
		$( window ).resize(function() {
			maingridresizeheightset('smsgroupsaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}	
	{//filter work
		//for toggle
		$('#groupsviewtoggle').click(function() {
			if ($(".smsgroupsfilterslide").is(":visible")) {
				$('div.smsgroupsfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.smsgroupsfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#smsgroupsclosefiltertoggle').click(function(){
			$('div.smsgroupsfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#smsgroupsfilterddcondvaluedivhid").hide();
		$("#smsgroupsfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#smsgroupsfiltercondvalue").focusout(function(){
				var value = $("#smsgroupsfiltercondvalue").val();
				$("#smsgroupsfinalfiltercondvalue").val(value);
			});
			$("#smsgroupsfilterddcondvalue").change(function(){
				var value = $("#smsgroupsfilterddcondvalue").val();
				if( $('#s2id_smsgroupsfilterddcondvalue').hasClass('select2-container-multi') ) {
					smsgroupsmainfiltervalue=[];
					$('#smsgroupsfilterddcondvalue option:selected').each(function(){
					    var $value =$(this).attr('data-ddid');
					    smsgroupsmainfiltervalue.push($value);
						$("#smsgroupsfinalfiltercondvalue").val(smsgroupsmainfiltervalue);
					});
					$("#smsgroupsfilterfinalviewconid").val(value);
				} else {
					$("#smsgroupsfinalfiltercondvalue").val(value);
					$("#smsgroupsfilterfinalviewconid").val(value);
				}
			});
		}
		smsgroupsfiltername = [];
		$("#smsgroupsfilteraddcondsubbtn").click(function() {
			$("#smsgroupsfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#smsgroupsfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(smsgroupsaddgrid,'smsgroups');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Close icon from SMS
		var smsmoduletype = sessionStorage.getItem("smsmoduletype"); 
		if(smsmoduletype != null) {
			setTimeout(function(){
				type = smsmoduletype;
				sessionStorage.removeItem("smsmoduletype");
			},100);				
		}
	}
	{//Close icon from Email
		var emailmoduletype = sessionStorage.getItem("emailmoduletype"); 
		if(emailmoduletype != null) {
			setTimeout(function(){
				type = emailmoduletype;
				sessionStorage.removeItem("emailmoduletype");
			},100);				
		}
	}
	{//Close icon from Call
		var callmoduletype = sessionStorage.getItem("callmoduletype"); 
		if(callmoduletype != null) {
			setTimeout(function(){
				type = callmoduletype;
				sessionStorage.removeItem("callmoduletype");
			},100);				
		}
	}
});
//View Grid
function smsgroupsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smsgroupsaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#smsgroupssortcolumn").val();
	var sortord = $("#smsgroupssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.smsgroupsheadercolsort').hasClass('datasort') ? $('.smsgroupsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.smsgroupsheadercolsort').hasClass('datasort') ? $('.smsgroupsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.smsgroupsheadercolsort').hasClass('datasort') ? $('.smsgroupsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#smsgroupsviewfieldids').val();
	var filterid = $("#smsgroupsfilterid").val();
	var conditionname = $("#smsgroupsconditionname").val();
	var filtervalue = $("#smsgroupsfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=campaigngroups&primaryid=campaigngroupsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#smsgroupsaddgrid').empty();
			$('#smsgroupsaddgrid').append(data.content);
			$('#smsgroupsaddgridfooter').empty();
			$('#smsgroupsaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('smsgroupsaddgrid');
			maingridresizeheightset('smsgroupsaddgrid');
			{//sorting
				$('.smsgroupsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.smsgroupsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#smsgroupspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.smsgroupsheadercolsort').hasClass('datasort') ? $('.smsgroupsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.smsgroupsheadercolsort').hasClass('datasort') ? $('.smsgroupsheadercolsort.datasort').data('sortorder') : '';
					$("#smsgroupssortorder").val(sortord);
					$("#smsgroupssortcolumn").val(sortcol);
					var sortpos = $('#smsgroupsaddgrid .gridcontent').scrollLeft();
					smsgroupsaddgrid(page,rowcount);
					$('#smsgroupsaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('smsgroupsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smsgroupsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#smsgroupspgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					smsgroupsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#smsgroupsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			maingridresizeheightset('smsgroupsaddgrid');
			//Material select
			$('#smsgroupspgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable(e) {
		//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$("#smsgroupssavebutton,#deleteicon").show();
			$("#smsgroupsdataupdatesubbtn").addClass('hidedisplayfwg');
			resetFields();
			firstfieldfocus();
			$("#templatetypeid").select2('val', '2').trigger('change');
			$('#smsgroupname, #templatetypeid').select2('readonly',false);
			groupsenderidget("250");
			Materialize.updateTextFields();
		});
		$("#editicon").click(function(e){
			e.preventDefault();
			var datarowid = $('#smsgroupsaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				groupsenderidget("250");
				smsgroupeditdatafetch(datarowid);
				$("#smsgroupssavebutton").hide();
				$("#smsgroupsdataupdatesubbtn").removeClass('hidedisplayfwg');
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#smsgroupsaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$("#basedeleteoverlay").fadeIn();
				$('#smsgroupsprimarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#smsgroupsprimarydataid').val();
			smsgrpdelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smsgroupspgnum li.active').data('pagenum');
		var rowcount = $('ul#smsgroupspgnumcnt li .page-text .active').data('rowcount');
		smsgroupsaddgrid(page,rowcount);
	}
}
//new data add
function newdataaddfun() {
	var amp = '&';
	var formdata = $("#smsgroupsaddform").serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Smsgroups/newdatacreate",
		data: "datas=" + datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				refreshgrid();
				alertpopup(savealert);
				resetFields();
				$(".addsectionclose").trigger("click");
			} 
		},
	});
}
// data fetch
function smsgroupeditdatafetch(id) {
	$.ajax({
		url: base_url + "Smsgroups/datafetchineditform?primarydataid="+id,
		dataType: 'json',
		async:false,
		cache:false,
		success: function(data)  {
			var name = data['smsgroupsname'];
			var typeid = data['typeid'];
			var msg = data['personalmessage'];
			var description = data['description'];
			var senderid = data['senderid'];
			var smstypeid = data['smstypeid'];
			//var smsgrptypeid = data['smsgrptypeid'];
			var leadtemplateid = data['leadtemplateid'];
			var signatureid = data['signatureid'];
			var smsgroupstypeids = data['smsgrptypeid'];
			var emailid = data['emailid'];
			var notificationtypeid = data['notificationtypeid'];
			var fromname = data['fromname'];
			var subject = data['subject'];
			$("#emailid").val(emailid);
			$("#notificationtypeid").select2('val',notificationtypeid);
			$("#fromname").val(fromname);
			$("#subject").val(subject);
			$("#smsgroupsprimarydataid").val(id);
			$("#templatetypeid").select2('val',typeid);
			$("#smsgroupname").val(name);
			$("#smsgrpdescription").val(description);
			$("#smsgrppersonalmsg").val(msg);
			$("#smsgrpsenderid").select2('val',senderid);
			$("#grpsmstypeid").select2('val',smstypeid);
			fieldchangeonload(typeid);
			setTimeout(function() {
				$("#smsgrptypeid").select2('val',smsgroupstypeids);
				$("#smsgrptemplateid").select2('val',leadtemplateid);
				$("#smsgrpsignatureid").select2('val',signatureid);
				$('#smsgroupname, #templatetypeid').select2('readonly',true);
				$('#smsgroupname').focus();
			},100);
		},
	});
}
//update
function smsgrpupdateformdata() {
	var id = $("#smsgroupsprimarydataid").val();
	var formdata = $("#smsgroupsaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Smsgroups/smsgroupupdate",
		data: "datas=" + datainformation+amp+"id="+id,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				refreshgrid();
				$(".addsectionclose").trigger("click");
				alertpopup(savealert);
				resetFields();
				//Touch work
				fortabtouch == 0;
				$("#smsgroupssavebutton,#deleteicon").show();
				$("#smsgroupsdataupdatesubbtn").addClass('hidedisplayfwg');
			}
		},
	});
}
//delete
function smsgrpdelete(datarowid) {
	$.ajax(	{
		url: base_url + "Smsgroups/smsgroupsdelete",
		data: "datarowid=" + datarowid,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				$("#basedeleteoverlay").fadeOut();
				refreshgrid();
				alertpopup('Deleted successfully');
			} 
		},
	});
}
//unique name check
function smsgroupnamecheck() {
	var primaryid = $("#primaryid").val();
	var accname = $("#smsgroupname").val();
	var elementpartable = 'campaigngroups';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "SMS group name already exists!";
				}
			} else {
				return "SMS group name already exists!";
			}
		}
	}
}

function fieldchangeonload(ttid){
	$("#emailid").removeClass('validate[required,custom[multimail],minSize[0],maxSize[100]]');
	$("#emailid").addClass('validate[required,custom[multimail],min[0],max[100]]');
	$("#fromname").removeClass('validate[required,maxSize[100]]');
	if(ttid == 3) { // Calls
		$("#smsgrpsenderiddivhid, #grpsmstypeiddivhid, #smsgrpsignatureid, #smsgrpsignatureiddivhid").hide();
		$("#smsgrptypeid,#smsgrppersonalmsg").removeClass('validate[required]');
		$("#smsgrppersonalmsg").removeClass('validate[required,maxSize[100]]');
		$("#emailid").removeClass('validate[required,custom[multimail],min[0],max[100]]');
		$("#smsgrptypeiddivhid, #emailiddivhid, #fromnamedivhid, #subjectdivhid, #smsgrppersonalmsgdivhid").hide();
	} else if(ttid == 2){ //SMS
		$("#smsgrpsenderiddivhid, #grpsmstypeiddivhid, #smsgrptypeiddivhid, #smsgrpsignatureiddivhid, #smsgrppersonalmsgdivhid").show();
		$("#smsgrptypeid").select2('val','');
		$("#smsgrptypeid").addClass('validate[required]');
		$("#smsgrppersonalmsg").addClass('validate[required,maxSize[100]]');
		$("#emailid").removeClass('validate[required,custom[multimail],min[0],max[100]]');
		$("#emailiddivhid").hide();
		$("#fromnamedivhid").hide();
		$("#subjectdivhid").hide();
	} else if(ttid == 5){ //Email
		$("#smsgrpsenderiddivhid, #grpsmstypeiddivhid, #smsgrpsignatureid, #smsgrpsignatureiddivhid").hide();
		$("#smsgrptypeiddivhid").show();
		$("#smsgrptypeid").select2('val','');
		$("#smsgrptypeid").addClass('validate[required]');
		$("#emailiddivhid, #fromnamedivhid, #subjectdivhid, #smsgrppersonalmsgdivhid").show();
		$("#smsgrppersonalmsg").addClass('validate[required]');
		$("#emailid").addClass('validate[required,custom[multimail],min[0],max[100]]');
		$("#fromname").addClass('validate[required,maxSize[100]]');
	}
}

function groupsenderidget(id) {
	$('#smsgrpsenderid').empty();
	$('#smsgrpsenderid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Smsgroups/sendergroupdd',
		data:'moduleid='+id,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#smsgrpsenderid')
					.append($("<option></option>")
					.attr("data-apikey",data[index]['apikey'])
					.attr("data-typename",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#smsgrpsenderid').trigger('change');
		},
	});
}
//Unique Group Name check - Kumaresan
function smsgroupnamecheck() {
	var templatetypeid = $('#templatetypeid').val();
	var primaryid = $('#smsgroupsprimarydataid').val();
	var smsgroupname = $('#smsgroupname').val();
	var nmsg = "";
	if( smsgroupname != "" ) {
		$.ajax({
			url:base_url+"Smsgroups/smsgroupnameunique",
			data:"data=&smsgroupname="+smsgroupname+"&templatetypeid="+templatetypeid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Group Name already exists!";
				}
			} else {
				return "Group Name already exists!";
			}
		} 
	}
}