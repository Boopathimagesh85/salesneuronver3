$(document).ready(function() {
	$(document).foundation();
	POUOM = 'ADD';
	POROL = 'ADD';
	POSTORAGE = 'ADD';
	POATTR = 'ADD';
	MODULEID = 0;
	UPDATEID = 0;
	productaddonedit = 0;
	PRODUCTADDONDELETE = 0;
	// Maindiv height width change
	maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);
	{//initial check
		$("#taxmasterid").prop("disabled",true);
	}
	if(softwareindustryid == 3) {
		counterdatastatus = $('#counterdataid').val();
		iconhideshow('storageicon',counterdatastatus);
		businesschargedata = $('#businesschargeid').val();
		businesspurchasechargedata = $('#businesspurchasechargeid').val();
		defaultcategoryid = $('#defaultcategoryid').val();
		defaultcategoryname = $('#defaultcategoryname').val();
		stonestatus = $('#stonestatus').val();
		sizestatus = $('#sizestatus').val();
		barcodeoption = $('#barcodeoption').val();
		productroltypeid = $('#productroltypeid').val();
		$("#sizemasteriddivhid,#fromweightdivhid,#toweightdivhid,#vendoriddivhid").hide();
		var prorolid = productroltypeid.split(",");
		for(var mn=0;mn<prorolid.length;mn++){
			if(prorolid[mn]==1) {
				$("#sizemasteriddivhid").show()
			} else if(prorolid[mn]==2){
				$("#fromweightdivhid,#toweightdivhid").show()
			} else if(prorolid[mn]==3){
				$("#vendoriddivhid").show()
			}
		}
	}
	{// Grid Calling Functions
		productgrid();
		getmoduleid();
		//crud action
		crudactionenable();
		{//Date Range
			daterangefunction();
		}
	}
   // get module id
	moduleid=$("#viewfieldids").val();	   
	$('#productrolingrideditspan2').removeAttr('style');
	$('#productattingridserspan4').removeAttr('style');
	// Toolbar Icons Change Functions    
	var addcloseinfo = ["closeaddform", "productaddformview", "productform"]
	addclose(addcloseinfo);
	var addcloseviewcreation =["viewcloseformiconid","productaddformview","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	{//Product addon close
		var prodaddoncloseinfo =['productaddoncloseicon','productaddformview','productaddonformdiv'];
		addclose(prodaddoncloseinfo);
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// View creation Function
		//branch view by dropdown change
		$('#dynamicdddataview').change(function() {
			productgrid();
		});
		//form close
		$('#companycloseadd').click(function() {
			productgrid();
		});
	}
	{
		$('#productlistuldata li').click(function() {
			var categoryid = $(this).attr('data-listid');
			if(categoryid == 3) {
				$('#counterid').select2('val',6).trigger('change');
			} else if(categoryid == 4) {
				$('#counterid').select2('val',8).trigger('change');
			} else {
				$('#counterid').select2('val',1).trigger('change');
			}
		});
	}
	$(".chzn-selectcomp").select2().select2('val', 'Mobile');
	{//validation for product ADD
		$('#dataaddsbtn').click(function(e) {
			{//inner-formvalidation remove
				//UOM
				$('#uomfromid').removeClass('validate[required]');
				$('#uomtoid').removeClass('validate[required]');
				$('#conversionrate').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
				//ROL
				$('#branchid,#reordertypeid').removeClass('validate[required]');				
				$('#minimumquantity,#maximumquantity,#reorderquantity').removeClass('validate[required,maxSize[100]]');
				//STORAGE
				$('#storagebranchid,#storagecategoryid,#storageid').removeClass('validate[required]');
			}
			$("#formaddwizard").validationEngine('validate');
		});
		$("#formaddwizard").validationEngine({
			onSuccess: function() {
				var taxablestatus =$('#taxable').val();
				if(taxablestatus == 'Yes'){
					var taxmasterid = $.trim($('#taxmasterid').find('option:selected').val());
					if(taxmasterid == ''){
						alertpopup('Please select tax category');
					  return false;
					}
				}
				$("#processoverlay").show();
				addformdata();
			},
			onFailure: function() {
				var dropdownid =['1','categoryid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				{//inner-formvalidation add
				//**UOM**//
					$('#uomfromid').addClass('validate[required]');
					$('#uomtoid').addClass('validate[required]');
					$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					//**ROL**//
					$('#branchid,#reordertypeid').addClass('validate[required]');				
					$('#minimumquantity,#maximumquantity,#reorderquantity').addClass('validate[required,maxSize[100]]');
					//**STORAGE**//
					$('#storagebranchid,#storagecategoryid,#storageid').addClass('validate[required]');
				}
			}
		});
	}
	//update Product information
	$('#dataupdatesubbtn').click(function(e) {
		{//inner-formvalidation remove
				//*UOM*//
				$('#uomfromid').removeClass('validate[required]');
				$('#uomtoid,#uomid').removeClass('validate[required]');
				$('#endoflife').removeClass('validate[required]');
				$('#conversionrate').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
				//**ROL**//
				$('#branchid,#reordertypeid').removeClass('validate[required]');				
				$('#minimumquantity,#maximumquantity,#reorderquantity').removeClass('validate[required,maxSize[100]]');
				//**STORAGE**//
				$('#storagebranchid,#storagecategoryid,#storageid').removeClass('validate[required]');
			}
		$("#formeditwizard").validationEngine('validate');
	});
	$("#formeditwizard").validationEngine({
		onSuccess: function() {
			 var taxablestatus =$('#taxable').val();
				if(taxablestatus == 'Yes'){
					var taxmasterid = $.trim($('#taxmasterid').find('option:selected').val());
					if(taxmasterid == ''){
						alertpopup('Please select tax category');
					  return false;
					}
				}
			updateformdata();
		},
		onFailure: function() {
			var dropdownid =['1','categoryid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
			if(softwareindustryid != 3){
				{//inner-formvalidation add
					//*UOM*//
					$('#uomfromid').addClass('validate[required]');
					$('#uomtoid').addClass('validate[required]');
					$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					//**ROL**//
					$('#branchid,#reordertypeid').addClass('validate[required]');				
					$('#minimumquantity,#maximumquantity,#reorderquantity').addClass('validate[required,maxSize[100]]');
					//**STORAGE**//
					$('#storagebranchid,#storagecategoryid,#storageid').addClass('validate[required]');
				}
			}
		}	
	});
	$("#productstonecalctypeiddivhid").addClass('hidedisplay');
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
			if(name == 'gstapplicable') {
				gstdatahideshow($('#'+name+'').val());
			}else { 
				productcheckboxaction(name);
			}
			if(name == 'stone') {
				if($('#'+name+'').val() == 'Yes'){
					$("#productstonecalctypeiddivhid").removeClass('hidedisplay');
				} else {
					$("#productstonecalctypeiddivhid").addClass('hidedisplay');
				}
			}			
		});
	}
	{
		$("#maximumquantity").change(function(){
			var qty = parseInt($(this).val());
			if(qty){
				var reorderqty = parseInt($("#reorderquantity").val());
				var minqty = parseInt($("#minimumquantity").val());
				if(minqty > qty){
					alertpopup("Maximum Quantity should be greater than Minimum Quantity");
					$(this).val(" ");
				}else if(reorderqty > qty){
					alertpopup("Maximum Quantity should be greater than or Equal to Reorder Quantity");
					$(this).val(" ");
				}					
			}
		});
		
	}
	{
		//Pricebooks
		$("#pricebooksicon").click(function(){
			window.location =base_url+'Pricebooksmaster';
		});
		//Unit of Measure
		$("#unitofmeasureicon").click(function(){
			window.location =base_url+'Uommaster';
		});
		//Product Master
		$("#productmastericon").click(function(){
			window.location =base_url+'Productmaster';
		});
		//Batch
		$("#batchicon").click(function(){
			window.location =base_url+'Batch';
		});
		//Storage
		$("#storageicon").click(function(){
			window.location =base_url+'Storage';
		});
	}
	{// For touch
		fortabtouch = 0;
	}
   {//action events
	   //attribute grid change action
	   $("#attributesetassignid").change(function(){
		   productattributegrid();
		   $("#tab6").trigger("click");
	   });
	   //General Form Clear 
		$("#formclearicon").click(function() {
			resetFields();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			if(softwareindustryid == 3){
				cleargriddata('productroladdgrid1');
			}else{
				cleargriddata('productproaddgrid1');
				cleargriddata('productroladdgrid2');
				cleargriddata('productstoaddgrid3');
				cleargriddata('productattaddgrid4');
			}
			//reset the disable settings
			$("#taxmasterid").prop("disabled",true);
			$("#unitprice").val('').prop("readonly",false);
		});
		//Clone icon click
		$("#cloneicon").click(function() {
			var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				productuomgrid();
				productrolgrid();
				productstoragegrid();
				productattributegrid();
				if(softwareindustryid == 3){
					cleargriddata('productroladdgrid1');
				}else{
					cleargriddata('productproaddgrid1');
					cleargriddata('productroladdgrid2');
					cleargriddata('productstoaddgrid3');
					cleargriddata('productattaddgrid4');
				}
				productgridetail(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".editformsummmarybtnclass").addClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');
				$("#productstoupdatebutton").addClass('hidedisplay');
				$("#formclearicon").hide();
				showhideiconsfun('editclick','productform');
				getformdata(datarowid);
				parentproductnamefetch(datarowid);
				var getrowlength = $('#productproaddgrid1 .gridcontent div.wrappercontent div').length;
				if(getrowlength != 0){
					$("#uomid").attr('readonly','readonly');
				}
				$("#processoverlay").hide();
				Materialize.updateTextFields();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				$("#primarydataid").val('');
				firstfieldfocus();
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#detailedviewicon").click(function(){
			var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				productuomgrid();
				productrolgrid();
				productstoragegrid();
				productattributegrid();
				//Function Call For Edit	
				if(softwareindustryid == 3){
					cleargriddata('productroladdgrid1');
					countershowhide();
				}else{
					cleargriddata('productproaddgrid1');
					cleargriddata('productroladdgrid2');
					cleargriddata('productstoaddgrid3');
					cleargriddata('productattaddgrid4');
				}
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$("#formclearicon").hide();
				getformdata(datarowid);
				$("#salescboxid").prop('checked',true);
				$("#sales").val("Yes");
				if(softwareindustryid == 3){
					stonehideshow();
					$("#purchasecboxid").prop('checked',true);
					$("#purchase").val("Yes");
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').addClass('hidedisplay');
				}else{
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').removeClass('hidedisplay');
					productgridetail(datarowid);
				}
				$("#productproupdatebutton,#productrolupdatebutton,#productstoupdatebutton,#productproingriddel1,#productrolingriddel2,#productstoingriddel3,#productrolingridedit2,#productstoingriddelspan1,#productproingridadd1,#productrolingridadd2,#productstoingridadd3,#productattingridadd4").hide(); 
				showhideiconsfun('summryclick','productform');				
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				parentproductnamefetch(datarowid);
				$("#productimagedivhid").addClass('hidedisplay');
				$(".documentslogodownloadclsbtn").hide();
				var getrowlength = $('#productproaddgrid1 .gridcontent div.wrappercontent div').length;
				if(getrowlength != 0){
					$("#uomid").attr('readonly','readonly');
				}
				$("#processoverlay").hide();
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					$("#processoverlay").show();
					froalaset(froalaarray);
					productuomgrid();
					productrolgrid();
					productstoragegrid();
					productattributegrid();
					//Function Call For Edit	
					if(softwareindustryid == 3){
						cleargriddata('productroladdgrid1');
						countershowhide();
					}else{
						cleargriddata('productproaddgrid1');
						cleargriddata('productroladdgrid2');
						cleargriddata('productstoaddgrid3');
						cleargriddata('productattaddgrid4');
						$(".addbtnclass").addClass('hidedisplay');
						$(".updatebtnclass").removeClass('hidedisplay');
					}
					$("#formclearicon").hide();
					getformdata(rdatarowid);
					$("#salescboxid").prop('checked',true);
					$("#sales").val("Yes");
					if(softwareindustryid == 3){
						stonehideshow();
						$("#purchasecboxid").prop('checked',true);
						$("#purchase").val("Yes");
						$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').addClass('hidedisplay');
					}else{
						$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').removeClass('hidedisplay');
						productgridetail(datarowid);
					}
					$("#productproupdatebutton,#productrolupdatebutton,#productstoupdatebutton,#productproingriddel1,#productrolingriddel2,#productstoingriddel3,#productrolingridedit2,#productstoingriddelspan1,#productproingridadd1,#productrolingridadd2,#productstoingridadd3,#productattingridadd4").hide(); 				
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					 },50);
					parentproductnamefetch(datarowid);
					showhideiconsfun('summryclick','productform');
					$("#productimagedivhid").addClass('hidedisplay');
					$(".documentslogodownloadclsbtn").hide();
					var getrowlength = $('#productproaddgrid1 .gridcontent div.wrappercontent div').length;
					if(getrowlength != 0){
						$("#uomid").attr('readonly','readonly');
					}
					$("#processoverlay").hide();
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function(){
			if(softwareindustryid == 3){
				countershowhide();
			}
			showhideiconsfun('editfromsummryclick','productform');
			$(".documentslogodownloadclsbtn").show();
			$("#productimagedivhid").removeClass('hidedisplay');
			$("#productproupdatebutton").removeClass('hidedisplayfwg');
			$("#productrolupdatebutton").removeClass('hidedisplayfwg');
			$("#productstoupdatebutton").removeClass('hidedisplayfwg');
			$("#productproupdatebutton,#productrolupdatebutton,#productstoupdatebutton,#productproingriddel1,#productrolingriddel2,#productstoingriddel3,#productrolingridedit2,#productstoingriddelspan1,#productproingridadd1,#productrolingridadd2,#productstoingridadd3,#productattingridadd4").show();
			$("#salescboxid").prop('checked',true);
			$("#sales").val("Yes");
				if(softwareindustryid == 3){
					stonehideshow();
					$("#purchasecboxid").prop('checked',true);
					$("#purchase").val("Yes");
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').addClass('hidedisplay');
				}else{
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').removeClass('hidedisplay');
				}	
		});
		$( window ).resize(function() {
			maingridresizeheightset('productgrid');
			sectionpanelheight('productprooverlay');
			sectionpanelheight('productattoverlay');
			sectionpanelheight('productroloverlay');
		});
		{//inner-form-with-grid
			$("#productproingridadd1").click(function(){
				$('#conversionrate').val('');
				$('#uomtoid').select2('val','');
				sectionpanelheight('productprooverlay');
				$("#productprooverlay").removeClass("closed");
				$("#productprooverlay").addClass("effectbox");
				$('#uomtoid').select2('focus');
				Materialize.updateTextFields();
				
			});
			$("#productprocancelbutton").click(function(){
				$("#productprooverlay").removeClass("effectbox");
				$("#productprooverlay").addClass("closed");
			});
			$("#productattingridadd4").click(function(){
				sectionpanelheight('productattoverlay');
				$("#productattoverlay").removeClass("closed");
				$("#productattoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#productattcancelbutton").click(function(){
				$("#productattoverlay").removeClass("effectbox");
				$("#productattoverlay").addClass("closed");
			});
			$("#productrolingridadd1,#productrolingridadd2").click(function(){
				$('#minimumquantity,#reorderquantity,#maximumquantity').val('');
				sectionpanelheight('productroloverlay');
				$("#productroloverlay").removeClass("closed");
				$("#productroloverlay").addClass("effectbox");
				$('#branchid').select2('val',loggedinbranchid).trigger('change');
				$('#reordertypeid').select2('focus');
				Materialize.updateTextFields();
			});
			$("#productstoingridadd3").click(function(){
				sectionpanelheight('productstooverlay');
				$("#productstooverlay").removeClass("closed");
				$("#productstooverlay").addClass("effectbox");
				$('#storagebranchid').select2('val',loggedinbranchid).trigger('change');
				$('#storageid').select2('val','');
				$('#storageid').select2('focus');
				Materialize.updateTextFields();
			});
			$("#productrolcancelbutton").click(function(){
				$("#productroloverlay").removeClass("effectbox");
				$("#productroloverlay").addClass("closed");
			});
			$("#productstocancelbutton").click(function(){
				$("#productstooverlay").removeClass("effectbox");
				$("#productstooverlay").addClass("closed");
			});
		}
	}
	{// Redirected form Home
		add_fromredirect("productaddsrc",MODULEID);
	}	
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#uomtoid').change(function() {
			var uomfromid = $("#uomfromid").val();
			var uomtoid = $("#uomtoid").val();
			if(uomfromid == uomtoid){
				alertpopup("Please Select Different UOM. UOM Should not be Same UOM");
				$("#uomtoid").select2('val','');
			}
		});
		$('#reordertypeid').change(function() {
			if(softwareindustryid == 3){
				var reorderval = $(this).find('option:selected').data('reordertypeidhidden');
				var ul = $('#productroladdgrid1 div.gridcontent .inline-list');
				var lis = ul.children('li');
				var isInList = false;
				for(var i = 0; i < lis.length; i++){
					if(lis[i].innerHTML === reorderval) {
						isInList = true;
						break;
					}
				}
			}else{
				var reorderval = $(this).find('option:selected').data('reordertypeidhidden');
				var ul = $('#productroladdgrid2 div.gridcontent .inline-list');
				var lis = ul.children('li');
				var isInList = false;
				for(var i = 0; i < lis.length; i++){
					if(lis[i].innerHTML === reorderval) {
						isInList = true;
						break;
					}
				}
				if(isInList){
					 alertpopup("Reordertype already Exist. Kindly choose another Type");
					 $('#reordertypeid').select2('val','');
				}
			}						
		});
		$('#uomtoid').change(function() {
			var touomval = $(this).find('option:selected').data('uomtoidhidden');
			var ul = $('#productproaddgrid1 div.gridcontent .inline-list');
			var lis = ul.children('li');
			var isInList = false;
			for(var i = 0; i < lis.length; i++){
				if(lis[i].innerHTML === touomval) {
					isInList = true;
					break;
				}
			}
			if(isInList){
				 alertpopup("UOM already Exist. Kindly choose another UOM");
				 $('#uomtoid').select2('val','');
			}				   
		});
		$('#storageid').change(function() {
			var storageval = $(this).find('option:selected').data('storageidhidden');
			 var ul = $('#productstoaddgrid3 div.gridcontent .inline-list');
			 var lis = ul.children('li');
			var isInList = false;
			for(var i = 0; i < lis.length; i++){
				if(lis[i].innerHTML === storageval) {
					isInList = true;
					break;
				}
			}
			if(isInList){
				 alertpopup("Storage already Exist. Kindly choose another storage");
				 $('#storageid').select2('val','');
			}				   
		});
	}
	{
		if(softwareindustryid == 4){				
			$("#ordertypeid").change(function(){
				var ordertypeid = $(this).val();
				if(ordertypeid){
					if(ordertypeid == 5){
						$("#hasbatch,#variant,#stockable").val("No");
						$("#hasbatchcboxid,#variantcboxid,#stockablecboxid").prop('checked', false);
						$("#instock,#endoflife,#batchnameid,#durationid").val("");
						$("#hasbatchdivhid,#variantdivhid,#stockabledivhid,#instockdivhid,#endoflifedivhid,#tab4,#tab5,#tab6").addClass('hidedisplay');
						$("#instock").removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
						$("#durationiddivhid,#batchnameiddivhid").removeClass('hidedisplay');
						$('#unitpricedivhid').removeClass("input-field large-12 medium-12 small-12 columns");
						$('#unitpricedivhid').addClass("input-field large-6 medium-6 small-6 columns");
						$('#endoflife').removeClass('validate[required]');
					}else{							
						$("#hasbatchdivhid,#variantdivhid,#stockabledivhid,#endoflifedivhid,#tab6").removeClass('hidedisplay');
						$("#batchnameid,#durationid").select2("val"," ");
						$("#durationiddivhid,#batchnameiddivhid").addClass('hidedisplay');
						$('#unitpricedivhid').removeClass("input-field large-6 medium-6 small-6 columns");
						$('#unitpricedivhid').addClass("input-field large-12 medium-12 small-12 columns");
						$('#endoflife').addClass('validate[required]');
					}
				}else{			
					$("#hasbatch,#variant,#stockable").val("No");
					$("#hasbatchcboxid,#variantcboxid,#stockablecboxid").prop('checked', false);
					$("#instock,#endoflife,#batchnameid,#durationid").val("");
					$("#hasbatchdivhid,#variantdivhid,#stockabledivhid,#endoflifedivhid,#tab6").removeClass('hidedisplay');
					$("#instock").removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$("#durationiddivhid,#instockdivhid,#batchnameiddivhid").addClass('hidedisplay');
					$('#unitpricedivhid').removeClass("input-field large-6 medium-6 small-6 columns");
					$('#unitpricedivhid').addClass("input-field large-12 medium-12 small-12 columns");
					$('#endoflife').addClass('validate[required]');
				}
				
			});
		}
	}
	{// Dashboard Icon Click Event	
		$('#dashboardicon').click(function(){
			var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();			
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$('.frmtogridbutton').click(function(){
		var clickgridname = $(this).data('frmtogridname');
		if(softwareindustryid == 3){
			if(clickgridname =='productroladdgrid1') {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');				
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				POROL = 'ADD';
			} 
		}else{
			if(clickgridname =='productproaddgrid1') {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);					
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				POUOM = 'ADD';
			} else if(clickgridname =='productroladdgrid2') {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');				
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');					
			} else if(clickgridname =='productstoaddgrid3') {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');				
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				POSTORAGE = 'ADD';
			} else if(clickgridname =='productattaddgrid4') {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');				
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				POATTR = 'ADD';
			}
			var uomval=$("#uomid").val();
			setTimeout(function() {
				$("#uomfromid").select2('val',uomval).trigger('change');
			}, 100);
		}
	});
	$("#productproingriddel1").click(function(){
		var datarowid = $('#productproaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {
			deletegriddatarow('productproaddgrid1',datarowid);
			var uomval=$("#uomid").val();
			$('#uomfromid').select2('val',uomval).trigger('change');
		} else {
			alertpopup("Please select a row");
		}
	});
	if(softwareindustryid == 3){
		$("#productrolingriddel1").click(function(){
			var datarowid = $('#productroladdgrid1 div.gridcontent div.active').attr('id');
				if(datarowid) {	
					deletegriddatarow('productroladdgrid1',datarowid);
				} else {
					alertpopup("Please select a row");
				}
		});
	}else{
		$("#productrolingriddel2").click(function(){
				var datarowid = $('#productroladdgrid2 div.gridcontent div.active').attr('id');
				if(datarowid) {	
					deletegriddatarow('productroladdgrid2',datarowid);
				} else {
					alertpopup("Please select a row");
				}
		});
	}
	$("#productstoingriddel3").click(function(){
		var datarowid = $('#productstoaddgrid3 div.gridcontent div.active').attr('id');
		if(datarowid) {	
			deletegriddatarow('productstoaddgrid3',datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#productstoingriddel2").click(function(){
		var datarowid = $('#productstoaddgrid2 div.gridcontent div.active').attr('id');
		if(datarowid) {	
			deletegriddatarow('productstoaddgrid2',datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#productname").change(function(){
		var name = $(this).val();
		if(name.indexOf('&') > -1){
			alertpopup("& symbol is restricted in product name");
			$(this).val("");
		}
	});
	//stock changes
	$("#stockablecboxid").click(function(){
		if($(this).is(':checked')) {				
			$("#instock").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$("#tab4,#tab5,#instockdivhid").removeClass('hidedisplay');
			if(softwareindustryid == 4){
				$('#unitpricedivhid').removeClass("input-field large-12 medium-12 small-12 columns");
				$('#unitpricedivhid').addClass("input-field large-6 medium-6 small-6 columns");
			}				
		}else{
			$("#instock").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$("#tab4,#tab5,#instockdivhid").addClass('hidedisplay');	
			if(softwareindustryid == 4){
				$('#unitpricedivhid').removeClass("input-field large-6 medium-6 small-6 columns");
				$('#unitpricedivhid').addClass("input-field large-12 medium-12 small-12 columns");
			}				
		}
	});
	//uom change events
	$("#uomid").change(function(){
		var uomval=$(this).val();
		setTimeout(function(){
			$('#uomfromid').select2('val',uomval).trigger('change');
		},100);
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function(){
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,productgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			productgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	// purity select single group
	$("#purityid").change(function() {
		if($.trim($(this).val())==null || $.trim($(this).val())== '') {
			$("#purityid option").attr("disabled", false); 
		} else {
			var label=$('#purityid :selected').parent().attr('label');
			$("#purityid optgroup > option[label!="+label+"]").attr('disabled','disabled');
			$("#purityid optgroup[label="+label+"]").children().removeAttr('disabled');
		}
	});
	/* $("#chargeid").change(function() {
		$("#chargeid optgroup > option[label!='']").removeAttr('disabled');
		$("#chargeid optgroup[label!='']").children().removeAttr('disabled');
		if($.trim($(this).val())== null || $.trim($(this).val()) == '') {
			$("#chargeid option").attr("disabled", false); 
		} else {
			var chargeid = $('#chargeid').val() + '';
			var chargedata = chargeid.split(',');
			for(var i=0 ; i < chargedata.length; i++) {
				var label = $('#chargeid option[value="'+chargedata[i]+'"]').parent().attr('label');
				$("#chargeid optgroup > option[label="+label+"]").attr('disabled','disabled');
				$("#chargeid optgroup[label="+label+"]").children().attr('disabled','disabled');
			}
		}
	}); */
   $('.addsectionclose').click(function(){
		$("#productaddonsectionoverlay").addClass("closed");
		$("#productaddonsectionoverlay").removeClass("effectbox");
	});
	{// Counter hierarchy
		$('#productlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var primaryid = $("#primarydataid").val();
			$('#parentproduct').val(name).trigger('focus');
			$('#categoryid').val(listid);
		});	
	}
	if($("#gstapplicableid").val() != 1) { // if gst is not enabled in company setting
			$("#tab2").hide();
	} else {// if gst is  enabled  in company setting
			$("#tab2").show();
	}
	$('#tagtypeid').change(function(){
		var val = $(this).find('option:selected').val();
		stonesizehideshow(val);
	});
	//$("#productlistuldata li[data-listname='Loose Stone']").remove();
});

//product checkbox
	function productcheckboxaction(name) {
		switch(name){				
			case 'taxable':			
				if($('#'+name+'cboxid').is(':checked')){
					$("#taxmasterid").prop("disabled",false);
				} else {					
					$('#taxmasterid').select2('val','').trigger('change');
					$("#taxmasterid").prop("disabled",true);
				}
			break;
		}
	}
	//default product settings
	function defaultproductsetting() {
		$.ajax({
			url:base_url+"Product/defaultsettings", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				setTimeout(function(){
					$('#uomid').select2('val',data.defaultuom).trigger('change');
					$('#brandid').select2('val',data.defaultbrand).trigger('change');					
				},25);
			}
		});
	}
	//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
	// Product View Grid
	function productgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		if(Operation == 1){
			var rowcount = $('#productpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#productpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		}
		var wwidth = $('#productgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.productheadercolsort').hasClass('datasort') ? $('.productheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.productheadercolsort').hasClass('datasort') ? $('.productheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.productheadercolsort').hasClass('datasort') ? $('.productheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=product&primaryid=productid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#productgrid').empty();
				$('#productgrid').append(data.content);
				$('#productgridfooter').empty();
				$('#productgridfooter').append(data.footer);
				//getaddinfo();
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('productgrid');
				{//sorting
					$('.productheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.productheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.productheadercolsort').hasClass('datasort') ? $('.productheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.productheadercolsort').hasClass('datasort') ? $('.productheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#productgrid .gridcontent').scrollLeft();
						productgrid(page,rowcount);
						$('#productgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('productheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						productgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#productpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						productgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var productidforeditvalue = sessionStorage.getItem("productidforedit");
					if(productidforeditvalue != null) {
						setTimeout(function() {
							productseditdatafetchfun(productidforeditvalue);
							sessionStorage.removeItem("productidforedit");
						},100);
					}
				}
				//Material select
				$('#productpgrowcount').material_select();
			},
		});
	}
	function productgridetail(datarowid){
		if(softwareindustryid == 3){
		   
		}else{
			/*Product UOM*/
			$.ajax({
				url:base_url+'Product/productuom?productid='+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data == "") {
					} else {
						loadinlinegriddata('productproaddgrid1',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productproaddgrid1');
						var hideprodgridcol = ['productuomconversionid'];
						gridfieldhide('productproaddgrid1',hideprodgridcol);
					}
				},
			});
			/*Product ROL*/
			$.ajax({
				url:base_url+'Product/productrol?productid='+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data == "") {
					} else {
						loadinlinegriddata('productroladdgrid2',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productroladdgrid2');
						var hideprodgridcol = ['productrolid'];
						gridfieldhide('productroladdgrid2',hideprodgridcol);
					}
				},
			});
			/*Product Storage*/
			$.ajax({
				url:base_url+'Product/productstorage?productid='+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data == "") {
					} else {
						loadinlinegriddata('productstoaddgrid3',data.rows,'json');
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productstoaddgrid3');
						var hideprodgridcol = ['productstorageid'];
						gridfieldhide('productstoaddgrid3',hideprodgridcol);
					}
				},
			});	
		}
	}
	{//crud operation
		function crudactionenable() {
			$("#addicon").click(function(e) {
				e.preventDefault();
				$("#processoverlay").show();
				$('.ftab').trigger('click');
				addslideup('productaddformview', 'productform');
				resetFields();
                if(softwareindustryid == 3){
					$("#chargeid option").siblings("option").prop('disabled', false);
					$("#purityid option").prop("disabled", false);
					$('#purityid,#tagtypeid,#chargeid,#treebutton,#sizecboxid').prop('disabled',false);
					$('#productstonecalctypeiddivhid').addClass('hidedisplay');
					$('#stonedivhid,#sizedivhid').show();
					cleargriddata('productroladdgrid1');
					productrolgrid();
				} else {
					productuomgrid();
			     	productattributegrid();
					productstoragegrid();
			  		cleargriddata('productproaddgrid1');
					cleargriddata('productroladdgrid2');
					cleargriddata('productstoaddgrid3');
			  		cleargriddata('productattaddgrid4');
					productrolgrid();
				}
				$('#supplytypeiddivhid,#gstcalculationtypeiddivhid,#taxmasteriddivhid,#gsthsncodedivhid,#gsthsndescriptiondivhid').addClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');
				$("#productproaddbutton,#productroladdbutton").removeClass('hidedisplayfwg');
				$(".editformsummmarybtnclass").addClass('hidedisplay');
				$(".addbtnclass").removeClass('hidedisplay');
				var elementname = $('#elementsname').val();
				elementdefvalueset(elementname);
				if(softwareindustryid != 3) {
					defaultproductsetting();
				}
				if(softwareindustryid == 4){
					$("#ordertypeid").select2('val',5).trigger("change");
				}				
				$("#salescboxid").prop('checked',true);
				$("#sales").val("Yes");
				if(softwareindustryid == 3){
					$('#gsthsncode').val('7113');
					stonehideshow();
					$("#purchasecboxid").prop('checked',true);
					$("#purchase").val("Yes");
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').addClass('hidedisplay');
					$('#productpieces').addClass('validate[custom[number],min[1]]');
					var taxcompanyvalue = $('#taxmasterhideshow').val();
					if(taxcompanyvalue != '' && taxcompanyvalue != 1){
						$('#taxmasterid').select2('val',taxcompanyvalue).trigger('change');
						$("#taxablecboxid").prop('checked',true);
						$("#taxable").val("Yes");
					}else{				
					  $('#taxmasterid').select2('val','').trigger('change');
					  $("#taxmasterid").prop("disabled",true);
					}
				}else{
					$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').removeClass('hidedisplay');
					defaultoptionset();
				
				}
				defaultproductownerset();
				if(softwareindustryid != 3){
					$("#tab4,#tab5").addClass('hidedisplay');
				}else{
					$("#tab5").addClass('hidedisplay');
				}
				$('#instock').removeClass('validate[custom[number],decval[2],maxSize[100]]');
				$('#instockdivhid').addClass('hidedisplay');
				//For tablet and mobile Tab section reset						
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 0;
				$("#uomid").removeAttr('readonly');
				if(softwareindustryid == 3) {
					stonesizehideshow($('#tagtypeid').val());
					$('#uomid').select2('val',9).trigger('change');
					saleschargehideshow(businesschargedata,'chargeid');  // sales charge hide/show based on company setting
					saleschargehideshow(businesspurchasechargedata,'purchasechargeid'); // purchase charge hide/show based on company setting
					var chargeid = $('#chargedataid').val();
					var chargedata = chargeid.split(",");
					if(chargedata != '') {
						$('#chargeid').select2('val',chargedata).trigger('change');
					}
					var purchasechargedataid = $('#purchasechargedataid').val();
					var purchasedata = purchasechargedataid.split(",");
					if(purchasedata != ''){
						$('#purchasechargeid').select2('val',purchasedata).trigger('change');
					}
					countershowhide();
				}
				showhideiconsfun('addclick','productform');
				if(softwareindustryid == 2) {
					$('#unitpricedivhid').removeClass("input-field large-6 medium-6 small-6 columns");
					$('#unitpricedivhid').addClass("input-field large-12 medium-12 small-12 columns");
				}
				$("#processoverlay").hide();
				$('#storagebranchid,#branchid,#productbranchid').select2('val',loggedinbranchid).trigger('change');
				if(softwareindustryid == 3) {
					$('#categoryid').val(defaultcategoryid);
					if(defaultcategoryname == 1) {
						defaultcategoryname = '';
					}
					$('#parentproduct').val(defaultcategoryname).trigger('focus');
				}
				froalaset(froalaarray);
				$("#primarydataid").val('');
				Materialize.updateTextFields();
				//For Keyboard Shortcut Variables
				viewgridview = 0;
				addformview = 1;
				Operation = 0; //for pagination
			});
			$("#editicon").click(function(e) {
				e.preventDefault();
				var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
				if (datarowid) {
					$("#processoverlay").show();
					$.ajax({
						url:base_url+"Product/checkeditlooseproduct",
						data: "productid=" +datarowid,
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['rows'] > 0){
								$("#processoverlay").hide();
								alertpopup("Can't edit loose stone product");
							}else { 
								if(softwareindustryid == 3){
									countershowhide();
									//checkstockentry(datarowid);
									checksize(datarowid);
								}else{
									cleargriddata('productproaddgrid1');
									cleargriddata('productroladdgrid2');
									cleargriddata('productstoaddgrid3');
									cleargriddata('productattaddgrid4');
									productuomgrid();
									productattributegrid();
									productrolgrid();
									productstoragegrid();
									productstoragefetch(datarowid); //product in-stock fetch
									productorderanddemand(datarowid); // product based order and demand fetch
								}
								productgridetail(datarowid);
								$(".addbtnclass").addClass('hidedisplay');
								$(".editformsummmarybtnclass").addClass('hidedisplay');
								$(".updatebtnclass").removeClass('hidedisplay');
								$("#productproupdatebutton").removeClass('hidedisplayfwg');
								$("#productstoupdatebutton").removeClass('hidedisplayfwg');
								$("#productproaddbutton").addClass('hidedisplayfwg');
								$('#productroladdbutton').removeClass('hidedisplay');
								$("#productproingriddel1,#productrolingriddel2,#productstoingriddel3").show();
								$("#formclearicon").hide(); 
								froalaset(froalaarray);
								getformdata(datarowid);
								parentproductnamefetch(datarowid);
								$("#uomid").trigger('change');//uom dd trigger		
								var getrowlength = $('#productproaddgrid1 .gridcontent div.wrappercontent div').length;
								if(getrowlength != 0){
									$("#uomid").attr('readonly','readonly');
								}
								$('.ftab').trigger('click');
								firstfieldfocus();
								//For tablet and mobile Tab section reset
								$('#tabgropdropdown').select2('val','1');
								fortabtouch = 1;
								//For Keyboard Shortcut Variables
								addformupdate = 1;
								viewgridview = 0;
								addformview = 1;
								$("#processoverlay").hide();
								$("#salescboxid").prop('checked',true);
								$("#sales").val("Yes");
								if(softwareindustryid == 3){
									stonehideshow();
									$("#purchasecboxid").prop('checked',true);
									$("#purchase").val("Yes");
									$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').addClass('hidedisplay');
									$('#productpieces').addClass('validate[custom[number],min[1]]');
								}else{
									$('#purchasedivhid,#salesdivhid,#uomiddivhid,#employeeiddivhid').removeClass('hidedisplay');
								}
								Materialize.updateTextFields();
								Operation = 1; //for pagination
							}
						}
					});	
				} else {
					alertpopup("Please select a row");		
				}
			});
			$("#deleteicon").click(function(e) {
				e.preventDefault();
				var datarowid = $('#productgrid div.gridcontent div.active').attr('id');
				if(datarowid) {
                   	$.ajax({
						url:base_url+"Product/checkeditlooseproduct",
						data: "productid=" +datarowid,
						type: "POST",
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if(data['rows'] > 0){
								alertpopup("Can't delete loose stone product");
							}else { 				
								$.ajax({ // checking whether this value is already in usage.
									url: base_url + "Base/multilevelmapping?level="+3+"&datarowid="+datarowid+"&table=itemtag&fieldid=productid&table1=sizemaster&fieldid1=productid&table2=productcharge&fieldid2=productid",
									success: function(msg) { 
										if(msg > 0){
											alertpopup("This product already mapped either in stock entry or sizemaster or product addon.So unable to delete this one");						
										}else{
											$('#primarydataid').val(datarowid);
											$("#basedeleteoverlay").fadeIn();
											$("#basedeleteyes").focus();
										}
									},
								});
							}
						}
					});
				} else {
					alertpopup("Please select a row");
				}
			});
			$("#basedeleteyes").click(function() {
				var datarowid = $('#primarydataid').val();
				recorddelete(datarowid);
			});
			$('#alertsdoublecloseproductaddon').click(function() { 
			 $('#productaddonalertsdouble').fadeOut();
			});
		}
	}
	$('#tab2').click(function() {
		if(softwareindustryid == 3){
			if($("#gstapplicableid").val() == 1) {
				gstdatahideshow('Yes');
				$("#gstapplicablecboxid").prop('disabled',true);
			 }
		}
	});
	{// Parent account name value fetch
		function parentproductnamefetch(pid) {
			$.ajax({
				url:base_url+"Product/parentproductnamefetch?id="+pid,
				dataType:'json',
				async:false,
				cache:false,
				success:function (data)	{
					$('#categoryid').val(data.parentcategoryid);
					$('#parentproduct').val(data.categoryname).trigger('focus');
				}
			});
		}
	}
	{// Edit Function 
		function productseditdatafetchfun(datarowid) {
			if(softwareindustryid == 3){
				cleargriddata('productroladdgrid1');
			}else{
				cleargriddata('productproaddgrid1');
				cleargriddata('productroladdgrid2');
				cleargriddata('productstoaddgrid3');
				cleargriddata('productattaddgrid4');
				productorderanddemand(datarowid); // product based order and demand fetch
				productgridetail(datarowid);
			}
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			$("#productproupdatebutton").removeClass('hidedisplayfwg');
			$("#productrolupdatebutton").removeClass('hidedisplayfwg');
			$("#productstoupdatebutton").removeClass('hidedisplayfwg');
			$("#productproingriddel1,#productrolingriddel2,#productstoingriddel3").show();
			$("#formclearicon").hide(); 
			getformdata(datarowid);
			productstoragefetch(datarowid); //product in-stock fetch
			$("#uomid").trigger('change');//uom dd trigger
			firstfieldfocus();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 1;
			//For Keyboard Shortcut Variables
			addformupdate = 1;
			viewgridview = 0;
			addformview = 1;
		}
	}
	{//refresh grid
		function refreshgrid() {
			var page = $(this).data('pagenum');
			var rowcount = $('ul#productpgnumcnt li .page-text .active').data('rowcount');
			productgrid(page,rowcount);
		}
	}
	//new data add submit function
	function addformdata() {
		if(softwareindustryid == 4){
			if($("#ordertypeid").val()== 5){
				var batchname=$('#batchnameid').val();
				batchname = batchname.toString();
				batchname = batchname.replace(/(^,)|(,$)/g, "");
				batchname = batchname.split(',');
				$('#batchnameid').select2('val',batchname);
			}			
		}
		var gridname = $('#gridnameinfo').val();
		if(gridname == ''){
		  var gridnames = gridname;
		}else{
		 var gridnames = gridname.split(',');
		}
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata=new Array();
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata.push(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			} else {
				addgriddata.push(getgridrowsdata(gridnames[j]));
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);	
		var noofrows = JSON.stringify(noofrows);
		//attribute set part	
		var attributerowid = getselectedrowids('productattaddgrid4');
		if (typeof attributerowid !== 'undefined') {
			if(attributerowid.length > 0) {
				attributerowid = attributerowid;
			} else {
				attributerowid= '';
			}
		} else {
			attributerowid= '';
		}
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
	        url: base_url + "Product/newdatacreate",
	        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&attributevalueid="+attributerowid,
	       	type: "POST",
			cache:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
	            if (nmsg == 'TRUE') {
					$(".ftab").trigger('click');
					resetFields();
					$(".singleuncheckid").removeClass('hidedisplay');
					$(".singlecheckid").addClass('hidedisplay');
					$('#productform').hide();
					$('#productaddformview').fadeIn(1000);
					refreshgrid();
					{//inner-formvalidation add
						//*UOM**//
						$('#uomfromid').addClass('validate[required]');
						$('#uomtoid').addClass('validate[required]');
						$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
						//**ROL**//
						$('#branchid,#reordertypeid').addClass('validate[required]');				
						$('#minimumquantity,#maximumquantity,#reorderquantity').addClass('validate[required,maxSize[100]]');
						//**STORAGE**//
						$('#storagebranchid,#storagecategoryid,#storageid').addClass('validate[required]');
					}
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
	            } else if (nmsg == "false") {
	            }
	        },
	    });
		$('.singlecheckid').click();
	}
	//old information show in form
	function getformdata(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax( {
			url:base_url+"Product/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {                			
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					$('#productform').hide();
					$('#productaddformview').fadeIn(1000);
					refreshgrid();
					//For Touch
					smsmasterfortouch = 0;
				} else if((data.fail) == 'sndefault'){
					resetFields();
					$('#productform').hide();
					alertpopup('Cannot Edit/Delete default records');
				} else {
					addslideup('productaddformview', 'productform');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					editordatafetch(froalaarray,data);
					$('#attributesetassignid').select2('val',data.attributesetassignid).trigger('change');
					$("#tab1").trigger("click");
					if(data.stockable == 'No'){
						$("#instock").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#tab4,#tab5,#instockdivhid").addClass('hidedisplay');
					}else{
						$("#instock").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#tab4,#tab5,#instockdivhid").removeClass('hidedisplay');
					}		
					if(softwareindustryid == 4){
						if(data.ordertypeid == 5){
							$("#hasbatchdivhid,#variantdivhid,#stockabledivhid,#instockdivhid,#endoflifedivhid,#tab4,#tab5,#tab6").addClass('hidedisplay');
							$("#durationiddivhid,#batchnameiddivhid").removeClass('hidedisplay');
						}else{							
							$("#hasbatchdivhid,#variantdivhid,#stockabledivhid,#endoflifedivhid,#tab6").removeClass('hidedisplay');
							$("#durationiddivhid,#batchnameiddivhid").addClass('hidedisplay');
						}
					}
					if(softwareindustryid == 3){
						checkstockentry(data.primarydataid,data.tagtypeid);
						$('#chargeid,#purityid').trigger('change');
						if(data.gstapplicable == 'Yes'){
							$('#gstapplicable').trigger('click');
							$('#supplytypeiddivhid,#gstcalculationtypeiddivhid,#taxmasteriddivhid,#gsthsncodedivhid,#gsthsndescriptiondivhid').removeClass('hidedisplay');
						}else{
							$('#supplytypeiddivhid,#gstcalculationtypeiddivhid,#taxmasteriddivhid,#gsthsncodedivhid,#gsthsndescriptiondivhid').addClass('hidedisplay');
						}
						if(data.stone == 'Yes'){
							$("#productstonecalctypeiddivhid").removeClass('hidedisplay');
						} else {
							$("#productstonecalctypeiddivhid").addClass('hidedisplay');
						}
						stonesizehideshow(data.tagtypeid);
					}
					Materialize.updateTextFields();	
				}
			}
		});
		setthecheckedvalue();
		setTimeout(function() {
			var vg=$('#attributesetassignid').val();
			if( vg > 0){
				$.ajax({
					url:base_url+"Product/getproductattributevalue?productid="+datarowid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						var nn=data.attributevalues;
						gridcheckboxsetselection('productattaddgrid4',nn);
					}
				});
			}
		},100);
	}
	function setthecheckedvalue() {
		var checkarray=['taxable','stockable','sales','variant'];
		for(var j=0;j<5;j++) {
			var value=$('#'+checkarray[j]+'').val();
			if (value == 'Yes') {
				$("#"+checkarray[j]+"checked").removeClass('hidedisplay');
				$("#"+checkarray[j]+"unchecked").addClass('hidedisplay');	
			} else if (value == 'No') {
				$("#"+checkarray[j]+"unchecked").removeClass('hidedisplay');
				$("#"+checkarray[j]+"checked").addClass('hidedisplay');
			}
			productcheckboxaction(checkarray[j]);
		}
	}
	//udate old information
	function updateformdata() {
		var gridname = $('#gridnameinfo').val();
		if(gridname == ''){
		  var gridnames = gridname;
		}else{
		  var gridnames = gridname.split(',');
		}
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata=new Array();
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata.push(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			} else {
				addgriddata.push(getgridrowsdata(gridnames[j]));
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);	
		var noofrows = JSON.stringify(noofrows);
		//attribute set part	
		var attributerowid = getselectedrowids('productattaddgrid4');
		if (typeof attributerowid !== 'undefined') {
			if(attributerowid.length > 0) {
				attributerowid = attributerowid;
			} else {
				attributerowid= '';
			}
		} else {
			attributerowid= '';
		}
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditordataget(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		 $.ajax({
	        url: base_url + "Product/datainformationupdate",
	        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&attributevalueid="+attributerowid,
			type: "POST",
			cache:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
	            if (nmsg == 'TRUE') {
					$(".singleuncheckid").removeClass('hidedisplay');
					$(".singlecheckid").addClass('hidedisplay');
					$('#productform').hide();
					$('#productaddformview').fadeIn(1000);
					resetFields();
					refreshgrid();
					alertpopup(savealert);
					{//inner-formvalidation add
						//*UOM**//
						$('#uomfromid').addClass('validate[required]');
						$('#uomtoid').addClass('validate[required]');
						$('#conversionrate').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
						//**ROL**//
						$('#branchid,#reordertypeid').addClass('validate[required]');
						$('#minimumquantity,#maximumquantity,#reorderquantity').addClass('validate[required,maxSize[100]]');
						//**STORAGE**//
						$('#storagebranchid,#storagecategoryid,#storageid').addClass('validate[required]');
					}
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
				} else if (nmsg == "false") {
	            }
	        },
	    });
		$('.singlecheckid').click();
	}
	//udate old information
	function recorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
	    $.ajax({
	        url: base_url + "Product/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
	            if (nmsg == 'TRUE') {
	            	refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
	            } else if (nmsg == "false") {
	            	refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
	            } else if(nmsg == "sndefault") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Cannot Edit/Delete default records');
					$("#processoverlay").hide();
				}
	        },
	    });
	}
	//Product name check
	function productnamecheck() {
		var primaryid = $("#primarydataid").val();
		var accname = $("#productname").val();
		var elementpartable = $('#elementspartabname').val();
		var nmsg = "";
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Product name already exists!";
					}
				} else {
					return "Product name already exists!";
				}
			} 
		} 
	}
	function productshortnamecheck() {
		var primaryid = $("#primarydataid").val();
		var accname = $("#shortname").val();
		var fieldname = 'shortname';
		var elementpartable = $('#elementspartabname').val();
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniqueshortdynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Short name already exists!";
					}
				}else {
					return "Short name already exists!";
				}
			}
		}
}
	{// Product view Grid
		function productuomgrid() {
			//GRA to restrict grid reload and take width of hidden grid
			$('#productform').css('visibility','hidden').show();
			$('#productform #subformspan2').css('display', 'block !important').show();
			var wwidth = $('#productform #productproaddgrid1').width();
			$('#productform #subformspan2').css('display', 'block !important').hide();
			$('#productform').css('visibility','visible').hide();
			var wheight = $("#productproaddgrid1").height();			
			if(MODULEID == 9){
				tabgroupid = 111;
			}else if(MODULEID == 89){
				tabgroupid = 225;
			}else if(MODULEID == 90){
				tabgroupid = 231;
			}else if(MODULEID == 98){
				tabgroupid = 260;
			}
			$.ajax({
				url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=productuom",
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#productproaddgrid1").empty();
					$("#productproaddgrid1").append(data.content);
					$("#productproaddgrid1footer").empty();
					$("#productproaddgrid1footer").append(data.footer);
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('productproaddgrid1');
					var hideprodgridcol = ['productuomconversionid'];
					gridfieldhide('productproaddgrid1',hideprodgridcol);
				},
			});
		}
		/*Product ROL grid*/
		function productrolgrid() {
			if(softwareindustryid == 3){
				var wwidth = $("#productroladdgrid1").width();
				var wheight = $("#productroladdgrid1").height();
				$.ajax({
					url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=233&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=productrol",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#productroladdgrid1").empty();
						$("#productroladdgrid1").append(data.content);
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productroladdgrid1');
						var hideprodgridcol = ['productrolid'];
						gridfieldhide('productroladdgrid1',hideprodgridcol);
					},
				});
			}else{
				var wwidth = $("#productroladdgrid2").width();
				var wheight = $("#productroladdgrid2").height();
				if(MODULEID == 9){
					tabgroupid = 114;
				}else if(MODULEID == 89){
					tabgroupid = 227;
				}else if(MODULEID == 90){
					tabgroupid = 233;
				}else if(MODULEID == 98){
					tabgroupid = 262;
				}
				$.ajax({
					url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=productrol",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#productroladdgrid2").empty();
						$("#productroladdgrid2").append(data.content);
						$("#productroladdgrid2footer").empty();
						$("#productroladdgrid2footer").append(data.footer);
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productroladdgrid2');
						var hideprodgridcol = ['productrolid'];
						gridfieldhide('productroladdgrid2',hideprodgridcol);
					},
				});
		  }
		}
		{// Invoice Payment Grid Edit
			$("#productrolingridedit2").click(function()
			{
				var selectedrow = $('#productroladdgrid2 div.gridcontent div.active').attr('id');
				if(selectedrow){
					gridtoformdataset('productroladdgrid2',selectedrow);
					sectionpanelheight('productroloverlay');
					$("#productroloverlay").removeClass("closed");
					$("#productroloverlay").addClass("effectbox");
					$("#productrolupdatebutton").removeClass('hidedisplayfwg');
					$('#productrolupdatebutton').show();	//display the UPDATE button/
					$('#productroladdbutton').hide();	//display the ADD button/
					POROL = 'UPDATE';
					UPDATEID = selectedrow;
					Materialize.updateTextFields();
				} else {
					alertpopup('Please Select The Row To Edit');
				}
			});
			$("#productrolupdatebutton").click(function() {	
				$('#productroladdbutton').show();
				$('#productrolupdatebutton').hide();
				$("#productrolupdatebutton").addClass('hidedisplayfwg');
			});
		}
		/*Product storage grid*/
		function productstoragegrid() {
			if(softwareindustryid == 3){
				var wwidth = $("#productstoaddgrid2").width();
				var wheight = $("#productstoaddgrid2").height();
				$.ajax({
					url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=234&moduleid="+moduleid+"&width="+wwidth+"&height="+wheight+"&modulename=productstorage",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#productstoaddgrid2").empty();
						$("#productstoaddgrid2").append(data.content);
						$("#productstoaddgrid2footer").empty();
						$("#productstoaddgrid2footer").append(data.footer);
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productstoaddgrid2');
						var hideprodgridcol = ['productstorageid'];
						gridfieldhide('productstoaddgrid2',hideprodgridcol);
					},
				});
			}else{
				var wwidth = $("#productstoaddgrid3").width();
				var wheight = $("#productstoaddgrid3").height();
				if(MODULEID == 9){
					tabgroupid = 115;
				}else if(MODULEID == 89){
					tabgroupid = 228;
				}else if(MODULEID == 90){
					tabgroupid = 234;
				}else if(MODULEID == 98){
					tabgroupid = 263;
				}
				$.ajax({
					url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=productstorage",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#productstoaddgrid3").empty();
						$("#productstoaddgrid3").append(data.content);
						$("#productstoaddgrid3footer").empty();
						$("#productstoaddgrid3footer").append(data.footer);
						/* data row select event */
						datarowselectevt();
						/* column resize */
						columnresize('productstoaddgrid3');
						var hideprodgridcol = ['productstorageid'];
						gridfieldhide('productstoaddgrid3',hideprodgridcol);
					},
				});
			}
		}
		/*Product attribute grid*/
		function productattributegrid()	{
			//GRA to restrict grid reload and take width of hidden grid
			$('#productform').css('visibility','hidden').show();
			$('#productform #subformspan6').css('display', 'block !important').show();
			var wwidth = $('#productform #productattaddgrid4').width();
			$('#productform #subformspan6').css('display', 'block !important').hide();
			$('#productform').css('visibility','visible');
			var wheight = $("#productattaddgrid4").height();
			$('#productattingriddel4').hide();
			$('#productattaddbutton').hide();
			$("#productattingridserspan4").hide();
			$(".searchiconclass").removeClass('fa fa-search');
			var attributesetassignid=$('#attributesetassignid').val();
			$.ajax({
				url:base_url+"Product/productattributecolumn?attributesetassignid="+attributesetassignid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=productattr&checkbox=true",
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#productattaddgrid4").empty();
					$("#productattaddgrid4").append(data.content);
					$("#productattaddgrid4footer").empty();
					$("#productattaddgrid4footer").append(data.footer);
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('productattaddgrid4');
					attributevaluesetdataload(attributesetassignid);
				},
			});
		}
	}
	/*Attribute set data load*/
	function attributevaluesetdataload(attributesetassignid) {
		if(attributesetassignid != "") {
			$.ajax({
				url:base_url+"Product/attributevaluecombination?attributesetassignid="+attributesetassignid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						loadinlinegriddata('productattaddgrid4',data.rows,'json','checkbox');
						/*data row select event*/ 
						datarowselectevt();
						/*column resize */
						columnresize('productattaddgrid4');
						/*check box select events*/
						gridcheckboxselectevents('productattaddgrid4');
						{ /*check box events*/
							current_attrib_val = getselectedrowids('productattaddgrid4');
							/*select row checkbox*/
							$('#productattaddgrid4 div.gridcontent .rowcheckbox').click(function(){
								var variant=$('#variant').val();
								var selectedRow = getselectedrowids('productattaddgrid4');
								if(selectedRow.length > 1 && variant != 'Yes') {
									alertpopup('Choose Only One Combination On General Mode');
									cleargriddata('productattaddgrid4');
									setTimeout(function(){
										var attributesetassignid=$('#attributesetassignid').val();
										attributevaluesetdataload(attributesetassignid);
									},100);
								}
							});
							/*select all checkbox*/
							$('#productattaddgrid4 div .headercheckbox').click(function(){
								var variant=$('#variant').val();
								var selectedRow = getselectedrowids('productattaddgrid4');
								if(selectedRow.length > 1 && variant != 'Yes') {
									alertpopup('Choose Only One Combination On General Mode');
									cleargriddata('productattaddgrid4');
									setTimeout(function(){
										var attributesetassignid=$('#attributesetassignid').val();
										attributevaluesetdataload(attributesetassignid);
										$('#productattaddgrid4 div .headercheckbox').prop('checked',false);
									},100);
								}
							});
						}
					}
				},
			});
		}
	}
	{// Form to grid base function 
		function gridformtogridvalidatefunction(gridnamevalue) {
			{//Function call for validate 
				$("#"+gridnamevalue+"validation").validationEngine({
					onSuccess: function() {
						var i = griddataid;
						var gridname = gridynamicname;
						var coldatas = $('#gridcolnames'+i+'').val();
						var columndata = coldatas.split(',');
						var coluiatas = $('#gridcoluitype'+i+'').val();
						var columnuidata = coluiatas.split(',');
						var datalength = columndata.length;
						if(softwareindustryid == 3){
							if(gridname =='productroladdgrid1'){	
								/*Form to Grid*/
								if(POROL == 'ADD' || POROL == '') {
									formtogriddata(gridname,POROL,'last','');
								} else if(POROL == 'UPDATE') {
									var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
									formtogriddata(gridname,POROL,'',selectedrow);
								}	
								var hideprodgridcol = ['productrolid'];
								gridfieldhide('productroladdgrid1',hideprodgridcol);
								/* Data row select event */
								datarowselectevt();
								POROL = 'ADD'
							}
							else if(gridname =='productstoaddgrid2'){	
								/*Form to Grid*/
								if(POSTORAGE == 'ADD' || POSTORAGE == '') {
									formtogriddata(gridname,POSTORAGE,'last','');
								} else if(POSTORAGE == 'UPDATE') {
									var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
									formtogriddata(gridname,POSTORAGE,'',selectedrow);
								}		
								var hideprodgridcol = ['productstorageid'];
								gridfieldhide('productstoaddgrid1',hideprodgridcol);
								/* Data row select event */
								datarowselectevt();
								POSTORAGE = 'ADD'
							}
						}else{
							if(gridname =='productproaddgrid1'){	
								/*Form to Grid*/
								if(POUOM == 'ADD' || POUOM == '') {
									formtogriddata(gridname,POUOM,'last','');
								} else if(POUOM == 'UPDATE') {
									var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
									formtogriddata(gridname,POUOM,'',selectedrow);
								}			
								/* Hide columns */
								var hideprodgridcol = ['productuomconversionid'];
								gridfieldhide('productproaddgrid1',hideprodgridcol);
								/* Data row select event */
								datarowselectevt();
								POUOM = 'ADD'
								$("#productprooverlay").removeClass("effectbox");
								$("#productprooverlay").addClass("closed");
							}
							else if(gridname =='productroladdgrid2'){	
								/*Form to Grid*/
								if(POROL == 'ADD' || POROL == '') {
									formtogriddata(gridname,POROL,'last','');
								} else if(POROL == 'UPDATE') {
									formtogriddata(gridname,POROL,'',UPDATEID);
								}	
								var hideprodgridcol = ['productrolid'];
								gridfieldhide('productroladdgrid2',hideprodgridcol);
								/* Data row select event */
								datarowselectevt();
								POROL = 'ADD'
								$("#productroloverlay").removeClass("effectbox");
								$("#productroloverlay").addClass("closed");
							}
							else if(gridname =='productstoaddgrid3'){	
								/*Form to Grid*/
								if(POSTORAGE == 'ADD' || POSTORAGE == '') {
									formtogriddata(gridname,POSTORAGE,'last','');
								} else if(POSTORAGE == 'UPDATE') {
									var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
									formtogriddata(gridname,POSTORAGE,'',selectedrow);
								}		
								var hideprodgridcol = ['productstorageid'];
								gridfieldhide('productstoaddgrid3',hideprodgridcol);
								/* Data row select event */
								datarowselectevt();
								POSTORAGE = 'ADD'
								$("#productstooverlay").removeClass("effectbox");
								$("#productstooverlay").addClass("closed");
							}
							else if(gridname =='productattaddgrid4'){	
								/*Form to Grid*/
								if(POATTR == 'ADD' || POATTR == '') {
									formtogriddata(gridname,POATTR,'last','');
								} else if(POATTR == 'UPDATE') {
									var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
									formtogriddata(gridname,POATTR,'',selectedrow);
								}				
								/* Data row select event */
								datarowselectevt();
								POATTR = 'ADD'
							}
						}
						clearform('gridformclear');
						griddataid = 0;
						gridynamicname = '';
						//Materilize Update
						setTimeout(function(){
							Materialize.updateTextFields();
						},100);
						
					},
					onFailure: function() {
						var dropdownid =['1','productid'];
						dropdownfailureerror(dropdownid);
					}
				});
			}
		}
	}
	//checks the option if only one option then sets itas the default value
	function defaultoptionset() {
		var branchlength=document.getElementById("storagebranchid").length-1;
		if(branchlength == 1){
			setTimeout(function() { 
			$("#storagebranchid").select2('val',loggedinbranchid).trigger('change');},1);	
		}
		if(softwareindustryid != 3){
			var branchlength=document.getElementById("branchid").length-1;
			if(branchlength == 1){
				var value=$("#branchid option:eq(1)").val();
				setTimeout(function() { 
				$("#branchid").select2('val',loggedinbranchid).trigger('change');},1);	
			}
	 }
	}
	{//date Range
		function daterangefunction() { 
			$('#salesstartdate').datetimepicker("destroy");
			$('#salesenddate').datetimepicker("destroy");		
			var startdateformater = $('#salesstartdate').attr('data-dateformater');
			var startDateTextBox = $('#salesstartdate');
			var endDateTextBox = $('#salesenddate');
			var minimumdate = $("#supportstartdate");
			var maximumdate = $("#supportenddate");
			startDateTextBox.datetimepicker({ 
				timeFormat: 'HH:mm z',
				dateFormat: startdateformater,
				changeMonth: true,
				changeYear: true,
				minDate:0,
				onClose: function(dateText, inst) {
					if (endDateTextBox.val() != '') {
						var testStartDate = startDateTextBox.datetimepicker('getDate');
						var testEndDate = endDateTextBox.datetimepicker('getDate');
						if (testStartDate > testEndDate)
							endDateTextBox.datetimepicker('setDate', testStartDate);
					}
					else {
						endDateTextBox.val(dateText);
					}
					$('#salesstartdate').focus();
				},
				onSelect: function (selectedDateTime){
					endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
					minimumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
					maximumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
						$('.salesenddateformError').remove();
						$('#salesenddate').removeClass('error');
					}
				}
			});
			endDateTextBox.datetimepicker({ 
				timeFormat: 'HH:mm z',
				dateFormat: startdateformater,
				changeMonth: true,
				changeYear: true,
				minDate:0,
				onClose: function(dateText, inst) {
					if (startDateTextBox.val() != '') {
						var testStartDate = startDateTextBox.datetimepicker('getDate');
						var testEndDate = endDateTextBox.datetimepicker('getDate');
						if (testStartDate > testEndDate)
							startDateTextBox.datetimepicker('setDate', testEndDate);
					}
					else {
						startDateTextBox.val(dateText);
					}
					$('#salesenddate').focus();
				},
				onSelect: function (selectedDateTime){
					startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
					minimumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
					maximumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				}
			});
			Materialize.updateTextFields();
		}
	}
	//default product owner set
	function defaultproductownerset() {
		$.ajax({
			url:base_url+"Product/productownerset",
			dataType : 'json',
			async:false,
			cache:false,
			success :function(data) {
				nmsg = $.trim(data);
				$("#employeeid").select2('val',nmsg)
			},
		});
	}
	//product storage get - in stock 
	function productstoragefetch(productid) {
		$.ajax({
			url:base_url+"Product/productstoragefetchfun?productid="+productid,
			dataType : 'json',
			async:false,
			cache:false,
			success :function(data) {
				nmsg = $.trim(data);
				$("#instock").val(nmsg);
			},
		});
	}
	//product based order and demand fetch
	function productorderanddemand(productid) {
		$.ajax({
			url:base_url+"Product/productorderanddemandfetchfun?productid="+productid,
			dataType : 'json',
			async:false,
			cache:false,
			success :function(data) {
				$("#orderedstock").val(data.quaninorder);
				$("#demandstock").val(data.quanindemand);
			},
		});
	}
{
	function getmoduleid(){
		$.ajax({
			url:base_url+'Product/getmoduleid',
			async:false,
			cache:false,
			success: function(data) {
				MODULEID=data;
			}
		});
	}
}
function accountgroupshowhide(){
	var accounttypeid = $('#accounttypeid').val();
	$("#chargeaccountgroup option").addClass("ddhidedisplay");
	$("#chargeaccountgroup optgroup").addClass("ddhidedisplay");
	$("#chargeaccountgroup option").prop('disabled',true);
	$("#chargeaccountgroup option[data-accountype="+1+"]").removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+1+"]").closest('optgroup').removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+1+"]").prop('disabled',false);
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").closest('optgroup').removeClass("ddhidedisplay");
	$("#chargeaccountgroup option[data-accountype="+accounttypeid+"]").prop('disabled',false);
}
function purityshowhide(purityid){
	$("#addon_purityid option").addClass("ddhidedisplay");
	$("#addon_purityid optgroup").addClass("ddhidedisplay");
	$("#addon_purityid option").prop('disabled',true);
	if (purityid.indexOf(',') > -1) {
		var purity = purityid.split(',');
		for(j=0;j<(purity.length);j++) {
			$("#addon_purityid option[value="+purity[j]+"]").removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity[j]+"]").prop('disabled',false);
		}
	}else{
			var purity = purityid;
			$("#addon_purityid option[value="+purity+"]").removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#addon_purityid option[value="+purity+"]").prop('disabled',false);
		
	}
}
function countershowhide(){
	var counterdataid=$('#counterdataid').val();
	   if(counterdataid == 'NO'){
			$('#counteriddivhid').addClass('hidedisplay');
			$('#counterid').removeClass('validate[required]');
	   }else{
			$('#counteriddivhid').removeClass('hidedisplay');
			$('#counterid').addClass('validate[required]');
	   }
}
function saleschargehideshow(businesschargedata,ddname){
	$("#"+ddname+" option").addClass("ddhidedisplay");
	$("#"+ddname+" option").prop('disabled',true);
	$("#"+ddname+" optgroup").addClass("ddhidedisplay");
	if(businesschargedata !='' && businesschargedata != 'null'){ 
		if(businesschargedata.indexOf(',') > -1) {
			var charge = businesschargedata.split(',');
				for(j=0;j<(charge.length);j++) {
					$("#"+ddname+" option[value="+charge[j]+"]").removeClass("ddhidedisplay");
					$("#"+ddname+" option[value="+charge[j]+"]").prop('disabled',false);
					$("#"+ddname+" option[value="+charge[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
				}
		}else{
			$("#"+ddname+" option[value="+businesschargedata+"]").removeClass("ddhidedisplay");
			$("#"+ddname+" option[value="+businesschargedata+"]").prop('disabled',false);
			$("#"+ddname+" option[value="+businesschargedata+"]").closest('optgroup').removeClass("ddhidedisplay");
		}
	}
}
function stonehideshow(){
	if(stonestatus == 'YES'){
		$('#stonedivhid').removeClass('hidedisplay');
	}else{
		$('#stonedivhid').addClass('hidedisplay');
	}
	if(sizestatus == '1'){
		$('#sizedivhid').removeClass('hidedisplay');
	}else{
		$('#sizedivhid').addClass('hidedisplay');
	}
	if(barcodeoption == '1'){
		$('#rfiddivhid').addClass('hidedisplay');
	}else{
		$('#rfiddivhid').removeClass('hidedisplay');
	}
}
function gstdatahideshow(hidedata) { 
	if(hidedata == 'Yes'){
		$('#supplytypeiddivhid,#gstcalculationtypeiddivhid,#taxmasteriddivhid,#gsthsncodedivhid,#gsthsndescriptiondivhid').removeClass('hidedisplay');
	}else{
		$('#supplytypeiddivhid,#gstcalculationtypeiddivhid,#taxmasteriddivhid,#gsthsncodedivhid,#gsthsndescriptiondivhid').addClass('hidedisplay');
	}
}
// check the paricular product available in stock entry or not
function checkstockentry(productid,tagtypeid) {
	$.ajax({
			url:base_url+'Product/checkitem?productid='+productid+'&moduleid=50',
			async:false,
			cache:false,
			success: function(data) {
				if(data == 0) {
					$('#purityid,#tagtypeid,#chargeid,#treebutton').prop('disabled',false);
				}else {
					if(tagtypeid == 17) {
						$('#tagtypeid').prop('disabled',true);
					} else {
						$('#tagtypeid').prop('disabled',true);
						//$('#tagtypeid option[value!="17"] option[value!="'+tagtypeid+'"] option:not(:eq(17))').remove();
						//$('option', this).not(':eq(17), :[option!=17 && option!='+tagtypeid+']').remove();
					}
					$('#purityid,#chargeid,#treebutton').prop('disabled',true);
				}
			}
		});
}
// check the paricular product available in size or not
function checksize(productid) {
	$.ajax({
			url:base_url+'Product/checkitem?productid='+productid+'&moduleid=134',
			async:false,
			cache:false,
			success: function(data) {
				if(data == 0) {
					$('#sizecboxid').prop('disabled',false);
				}else {
					$('#sizecboxid').prop('disabled',true);
				}
			}
		});
}
// stone,size hide show based on stock type
function stonesizehideshow(val) {
	if(val == 3) { // untag
		$('#stonedivhid,#sizedivhid').hide();
		$('#productstonecalctypeiddivhid').addClass('hidedisplay');
		$("#rfiddivhid").hide();
		$('#rfid,#stone,#size').val('No');
	}
	if(val == 5) { // pricetag
		if(sizestatus == '1') {
			$('#sizedivhid').show();
		} else {
			$('#sizedivhid').hide();
		}
		$('#stonedivhid').hide();
		$('#productstonecalctypeiddivhid').addClass('hidedisplay');
		$('#stone').val('No');
		$("#rfiddivhid").show();
	}
	if(val == 2 || val == 17 || val == 4) { //stock type - tag, bulk tag, all (hide show from company settings)
		if(stonestatus == 'YES' && sizestatus == '1') {
			$('#stonedivhid').show();
			$('#sizedivhid').show();
			if($('#stone').val() == 'Yes') {
				$('#productstonecalctypeiddivhid').removeClass('hidedisplay');
			}
		}else if(stonestatus == 'YES' && sizestatus == '0') {
			$('#stonedivhid').show();
			$('#sizedivhid').hide();
			$('#size').val('No');
			if($('#stone').val() == 'Yes') {
				$('#productstonecalctypeiddivhid').removeClass('hidedisplay');
			}
		}else if(stonestatus == 'NO' && sizestatus == '1') {
			$('#sizedivhid').show();
			$('#stone').val('No');
			$('#stonedivhid').hide();
			$('#productstonecalctypeiddivhid').addClass('hidedisplay');
		}
		$("#rfiddivhid").show();
	}
}