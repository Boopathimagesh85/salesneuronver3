jQuery(document).ready(function() {
	forinc = 0;
	{ // Foundation Initialization
		$(document).foundation();
	}
	{//enable support icon
		$(".supportoverlay").css("display","inline-block")
	}
	{//globalsetvariable-for filter loading
		quote_filter_status = so_filter_status = po_filter_status =inv_filter_status = lead_filter_status = 0;
		account_filter_status = campaign_filter_status = contact_filter_status = contract_filter_status = 0;
		document_filter_status = opp_filter_status = solution_filter_status= task_filter_status =act_filter_status=0;
		ticket_filter_status=solutioncat_filter_status=product_filter_status=pricebook_filter_status=materialreq_filter_status=0;
	}
	{//for scroll-dashboard
		var newheightaddform = $( window ).height();
		var addheight = parseInt(newheightaddform) - parseInt(70);
		$("#dashboardcontainer").css('height',''+addheight+'px');
		$(".homedashboardcontainer").css('height',''+addheight+'px');
		$( window ).resize(function() {
			var newheightaddform = $( window ).height();
			var addheight = parseInt(newheightaddform) - parseInt(40);
			$("#dashboardcontainer").css('height',''+addheight+'px');
			$(".homedashboardcontainer").css('height',''+addheight+'px');
		});
	}
	//conversation @-tag container close
	$("#dashboardcontainer" ).scroll(function() {
		$(".sm_bar" ).css("display","none");
	});	
	{//Notification toolbar icon declaration		
		 notificationaction=['added','updated','deleted','active','cancelled','lost','convert','stopped','import','duplicate','export','massdelete','massupdate','conversation','invite','replied','mail'];
		 notificationicons=['icon24 icon24-add','icon24 icon24-mode_edit','icon24 icon24-delete','icon24 icon24-assignment_turned_in','icon24 icon24-close','icon24 icon24-thumb_down','icon icon-point-right','icon icon-stop','icon icon-box-add','icon icon-binoculars','icon icon-upload2','icon icon-database','icon icon-database','icon icon-chat75','icon icon-tree','icon icon-bubbles','icon icon-envelop'];
		 notificationlink=['1','1','2','1','5','5','6','5','7','0','7','0','0','3','4','3','8'];
	}	
	{
		$("#closedashboardform").click(function(){
			$("#dashboardcontainer").addClass('hidedisplay');
			$('.actionmenucontainer').removeClass('hidedisplay');
			$('.fullgridview').removeClass('hidedisplay');
			$(".forgetinggridname").removeClass('hidedisplay');
			$('.footercontainer').removeClass('hidedisplay');
			tabletheadertap = 0;
		});	 
	}
	
	{// Dashboard Select and Close Overlay Function
		$("#dashboardselecticon").click(function(){
			$("#dashboardselectoverlay").show();
		});	
		//dashboard previous record		
		$("#dashboardpreviousicon").click(function(){
			var moduleid = $('#anfieldnamemodinfo').val();
			var parenttable = $('#elementspartabname').val();
			var recordid = $("#dashboardrecordid").val();	
			$("#processoverlay").show();
			dashboardpreviousnextfun(moduleid,parenttable,recordid,'previous');
		});	
		//dashboard next record		
		$("#dashboardnexticon").click(function(){
			var moduleid = $('#anfieldnamemodinfo').val();
			var parenttable = $('#elementspartabname').val();
			var recordid = $("#dashboardrecordid").val();	
			$("#processoverlay").show();
			dashboardpreviousnextfun(moduleid,parenttable,recordid,'next');
		});
		//dashboard close
		$("#dashboardselectioncloseid").click(function(){
			$("#dashboardselectoverlay").hide();
		});	
	}
	//home dashboard create function
	 $('#dashboardviewid').change(function(){
		var val=$('#conversationdivid').attr('data-conversationdivid');
		var moduleval=$('#conversationdivid').attr('data-moduleid');
		var rowid=$('#wholerowid').val();
		if(rowid == '' || rowid == null){
			rowid=1;
		}			
		dynamicchartwidgetcreate(rowid);
	});
	{// Close Alert
		$("#dbdelalertsfcloseno").click(function(){
			$("#dbdeletealertscloseyn").hide();
		});	
	}	
$('#notificationdatatype').val('my');
{//support page overlay
	$('.supportoverlay').click(function(){
		$.ajax({
			url:base_url+"Home/supportoverlayfetch",
			cache:false,
			success :function(data) {
				$("#supportoverlayappend").empty();
				$("#supportoverlayappend").append(data);
				$("#supportpageoverlay").show();
				$("#supportmobileoverlay").parent().addClass("move-right");
				//support overlay close
				$('.supportpageoverlayclose').click(function(){
					$('#supportpageoverlay').hide();
					$("#supportmobileoverlay").parent().removeClass("move-right");
				});
				{// support Overlay Top Bar Code
					$(".tabliicons").click(function() {
						$(".tablititle").removeClass('tabactive');
						$(this).addClass('tabactive');
						$('.supportoverlayul li i').removeClass('icon24');
						$('.supportoverlayul li i').addClass('icon24-w');
						$('.supportoverlayul li.tabactive i').removeClass('icon24-w');
						$('.supportoverlayul li.tabactive i').addClass('icon24');
						var supportsubformval = $(this).data('supportsubform');
						$(".hiddensupportsubform").hide();
						if(supportsubformval == 4) {
							$(".support-updatepan").empty;
							$("#supportsubformpan"+supportsubformval+"").show();
							fetchupdatedata();
						} else {
							$("#supportsubformpan"+supportsubformval+"").show();
						}
					});	
				}
			}
		});
	});
}
});
{//dynamic dashboard chart and widget creation
	function dynamicchartwidgetcreate(rowid) {
		var dashboardid = $('#dashboardviewid').val();
		$("#dashboardrecordid").val(rowid);	
		$.ajax({
			url:base_url+"Base/dashboardcreatedatafetch",
			data:{data:dashboardid,rowid:rowid},
			type:"POST",
			async:false,
			cache:false,
			success :function(msg) {
				$('#widgetdata').empty();
				$('#widgetdata').append(msg);
			}
		});
	}
}
{//dynamic dashboard chart and widget creation - mini
	function dynamicchartwidgetcreatemini(rowid,notestatus) {
		var dashboardid = $('#dashboardviewid').val();
		var minidashboard = 1;
		$("#dashboardrecordid").val(rowid);	
		$.ajax({
			url:base_url+"Base/dashboardcreatedatafetch",
			data:{data:dashboardid,rowid:rowid,minidashboard: minidashboard,notestatus :notestatus },
			type:"POST",
			async:false,
			cache:false,
			success :function(msg) {
				if(notestatus == 0){
					$('#dashboardwidget').empty();
					$('#dashboardwidget').append(msg);
				}else if(notestatus == 1){
				    $('#noteswidget').empty();
					$('#noteswidget').append(msg);
				}
			}
		});
	}
}
{// chart color Pallete
	
		var materialtheme700 ={
	    "root":{
	        "background-color":"white",
			"gui":{
	            "context-menu":{
	                "button":{"visible":false},
	                "gear":{"visible":false}
	            }
	        }
		},
			"palette" : {
				"mixed" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"vbar" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"vbar3d" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"hbar" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"hbar3d" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"pie" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"pie3d" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"nestedpie" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"area" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"area3d" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"gauge" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"line" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"line3d" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"piano" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"vbullet" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"hbullet" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"vfunnel" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"hfunnel" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"scatter" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"radar" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"venn" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"chord" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"treemap" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"wordcloud" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"stock" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				],
				"bubble" : [
					["#ffffff", "#D32F2F", "#D32F2F", "#D32F2F"],
					["#ffffff", "#1976D2", "#1976D2", "#1976D2"], 
					["#ffffff", "#689F38", "#689F38", "#689F38"],
					["#ffffff", "#E64A19", "#E64A19", "#E64A19"],
					["#ffffff", "#C2185B", "#C2185B", "#C2185B"],
					
					["#ffffff", "#0288D1", "#0288D1", "#0288D1"],
					["#ffffff", "#AFB42B", "#AFB42B", "#AFB42B"],
					["#ffffff", "#7B1FA2", "#7B1FA2", "#7B1FA2"],
					["#ffffff", "#0097A7", "#0097A7", "#0097A7"],
					["#ffffff", "#FBC02D", "#FBC02D", "#FBC02D"],
					
					["#ffffff", "#512DA8", "#512DA8", "#512DA8"],
					["#ffffff", "#00796B", "#00796B", "#00796B"],
					["#ffffff", "#FFA000", "#FFA000", "#FFA000"],
					["#ffffff", "#303F9F", "#303F9F", "#303F9F"],
					["#ffffff", "#388E3C", "#388E3C", "#388E3C"]
				]
			}
		};
}
{//Pie Chart
	function piechartfun(piechartval) 
	{
		console.log(piechartval);
		var type = 'pie';
		if(piechartval[3] == 'false'){
			type = "pie";
		}
		else{
			type = "pie3d";
		}	
		var angle = 270;
		if(piechartval[4] != ''){
			angle = piechartval[4]
		}else{
			angle = 270
		}
		
		var data = {
				"type": "pie", 
			 	"backgroundColor": "#fff",
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
			         "background-color":"#fff",	
			    	 "type":type,
			    	 "title":{
			    		    "text":piechartval[2],
			    		    "background-color":"transparent",
			    		    "color":"#222222",
			    		    "font-family":"roboto",
			    		    "font-weight":"normal",
			    		    "text-align":"left",
			    		    "padding-top":"10px",
			    		    "padding-left":"20px",
			    		    "font-size":"16px"
			    		},	
		    		"subtitle": {
		    			  "text": "My Subtitle",
		    			  "color":"#cdcdcd",
		    			  "text-align":"left",
		    			  "padding-left":"20px",
		    			},
			         "scale-r":{
			             "ref-angle":angle, 
			             "layout":"auto"
			         },
			    	 "plot":{
			    	        "offset-r":"1%", //To apply globally.
			    	        "shadow":1,
		    	            "shadow-alpha":0.16,
		    	            "shadow-distance":"3px",
		    	            "hover-state":{
		    	                "shadow":true,
		    	                "shadow-distance":"3px",
		    	                "shadow-alpha":0.16
		    	              },
			    	        "value-box":{
					             "visible":true, //Specify your visibility: true or false.
				            	 "placement":"in", //Specify your placement: "out", "in", or "center".
				            	 "text":"%t(%v)", //Specify your value box text.    
				            	 "font-size":12,
					             "font-weight":"normal",
					           },
	    	            	"tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "font-family":"roboto",
			    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
			    	            "font-color":"#000000",
			    	            "font-size":14,
			    	            "text-align":"left",
			    	            "text-alpha":1,
			    	            "background-color":"#ffffff",
			    	            "alpha":1,
			    	            "border-radius":"2%",
			    	            "padding":"10%",
			    	            "callout": false,
			    	            "shadow":1,
			    	            "shadow-alpha":0.16,
			    	            "shadow-distance":"4px",
			    	            },
			    	    },	
			    	 "series": piechartval[1]
			        }]
	    }
		if(piechartval[5] == 'true'){			
		    data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
					"behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
			         "background-color":"#fff",	
			    	 "type":type,
			    	 "title":{
			    		    "text":piechartval[2],
			    		    "background-color":"transparent",
			    		    "color":"#222222",
			    		    "font-family":"roboto",
			    		    "font-weight":"normal",
			    		    "text-align":"left",
			    		    "padding-top":"10px",
			    		    "padding-left":"20px",
			    		    "font-size":"16px"
			    		},	
		    		"subtitle": {
		    			  "text": "My Subtitle",
		    			  "color":"#cdcdcd",
		    			  "text-align":"left",
		    			  "padding-left":"20px",
		    			},
			         "scale-r":{
			             "ref-angle":angle, 
			             "layout":"auto"
			         },
			    	 "plot":{
			    	        "offset-r":0, //To apply globally..
			    	        "border-width":0,
			    	        "font-family":"roboto",
			    	        "shadow":1,
		    	            "shadow-alpha":0.16,
		    	            "shadow-distance":"3px",
		    	            "hover-state":{
		    	                "shadow":true,
		    	                "shadow-distance":"3px",
		    	                "shadow-alpha":0.16
		    	              },
		    	            "tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "font-family":"roboto",
			    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
			    	            "font-color":"#000000",
			    	            "font-size":14,
			    	            "text-align":"left",
			    	            "text-alpha":1,
			    	            "background-color":"#ffffff",
			    	            "alpha":1,
			    	            "border-radius":"2%",
			    	            "padding":"10%",
			    	            "callout": false,
			    	            "shadow":1,
			    	            "shadow-alpha":0.16,
			    	            "shadow-distance":"4px",
			    	            },
					         "value-box":{
					             "visible":true, //Specify your visibility: true or false.
				            	 "placement":"in", //Specify your placement: "out", "in", or "center".
				            	 "text":"%v%", //Specify your value box text.    
				            	 "font-size":12,
					             "font-weight":"normal",
					           },
					      	 "animation":{
					      	      "effect": 2, 
					      	      "method": 5,
					      	      "speed": 500,
					      	      "sequence": 1
					      	    }
			    	    },	
			    	 "legend":{
			    		 "y":"10%",
			    		 "marker":{
			    	          "type":piechartval[6],
			    	          "border-width":0,
			    	          
			    		 },
			    	    "border-width":0,
			    	    "shadow":0,  
			    	    "alpha":1,  
			    	    "item":{  
			    	        "color":"#000",
			    	        "font-family":"roboto",
			    	        "font-size":13,
			    	    },
			    	    
			    	 },
			    	 "series": piechartval[1],
			    	 }]
	    }
	}
		zingchart.render({
		    id:piechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: '100%',
		    height: 'auto',
		    defaults:materialtheme700,
		   
		});
	}
		
}
{//Donut Chart
	function donutchartfun(donutchartval) 
	{ 
		var type = 'ring';
		if(donutchartval[3] == 'false'){
			type = "ring";
		}
		else{
			type = "ring3d";
		}
		var angle = 300;
		if(donutchartval[4] != ''){
			angle = donutchartval[4]
		}else{
			angle = 300
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [ 
			              {
			            	 "background-color":"#fff",	
					    	 "type":type,
					    	 "title":{
					    		    "background-color":"transparent",
					    		    "color":"#222222",
					    		    "font-family":"roboto",
					    		    "font-weight":"normal",
					    		    "text-align":"left",
					    		    "padding-top":"10px",
					    		    "padding-left":"20px",
					    		    "font-size":"16px"
					    		},	
				    		 "subtitle": {
				    			  "text": "My Subtitle",
				    			  "color":"#cdcdcd",
				    			  "text-align":"left",
				    			  "padding-left":"20px",
				    			},
					         "scale-r":{
					             "ref-angle":angle, 
					             "layout":"auto"
					             },
					    	 "plot":{
					    	        "offset-r":"0", //To apply globally.
					    	        "slice":"35%" ,
					    	        "shadow":1,
				    	            "shadow-alpha":0.16,
				    	            "shadow-distance":"3px",
				    	            "hover-state":{
				    	                "shadow":true,
				    	                "shadow-distance":"3px",
				    	                "shadow-alpha":0.16
				    	              },
				    	        	"value-box":{
				    	                "visible":true, //Specify your visibility: true or false.
				    	                "placement":"in", //Specify your placement: "out", "in", or "center".
				    	                "text":"%t(%v)", //Specify your value box text.
				    	            	},
			    	            	"tooltip":{
					    	            "visible":true, //Specify your visibility: true or false.
					    	            "font-family":"roboto",
					    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
					    	            "font-color":"#000000",
					    	            "font-size":14,
					    	            "text-align":"left",
					    	            "text-alpha":1,
					    	            "background-color":"#ffffff",
					    	            "alpha":1,
					    	            "border-radius":"2%",
					    	            "padding":"10%",
					    	            "callout": false,
					    	            "shadow":1,
					    	            "shadow-alpha":0.16,
					    	            "shadow-distance":"4px",
					    	            },
					    	    },
					    	 "series": donutchartval[1]
			              		}
				    	    ]
		    }
		if(donutchartval[5] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					    ]
					},
				 "graphset": [ 
				              {
				            	 "background-color":"#fff",	
						    	 "type":type,
						    	 "title":{
						    		    "text":donutchartval[2],
						    		    "background-color":"transparent",
						    		    "color":"#222222",
						    		    "font-family":"roboto",
						    		    "font-weight":"normal",
						    		    "text-align":"left",
						    		    "padding-top":"10px",
						    		    "padding-left":"20px",
						    		    "font-size":"16px"
						    		},	
					    		 "subtitle": {
					    			  "text": "My Subtitle",
					    			  "color":"#cdcdcd",
					    			  "text-align":"left",
					    			  "padding-left":"20px",
					    			},
						         "scale-r":{
						             "ref-angle":angle, 
						             "layout":"auto"
						             },
					             "plot":{
						    	        "offset-r":"0%", //To apply globally.
						    	        "border-width":0,
						    	        "slice":"35%" ,	
						    	        "tooltip":{
						    	            "visible":true, //Specify your visibility: true or false.
						    	            "font-family":"roboto",
						    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
						    	            "font-color":"#000000",
						    	            "font-size":14,
						    	            "text-align":"left",
						    	            "text-alpha":1,
						    	            "background-color":"#ffffff",
						    	            "alpha":1,
						    	            "border-radius":"2%",
						    	            "padding":"10%",
						    	            "callout": false,
						    	            "shadow":1,
						    	            "shadow-alpha":0.16,
						    	            "shadow-distance":"4px",
						    	            },
					    	            "value-box":{
								             "visible":true, //Specify your visibility: true or false.
							            	 "placement":"in", //Specify your placement: "out", "in", or "center".
							            	 "text":"%v%", //Specify your value box text.    
							            	 "font-size":12,
								             "font-weight":"normal",
								           },
							           "animation":{
								      	      "effect": 3, 
								      	      "method": 5,
								      	      "speed": 500,
								      	      "sequence": 1
								      	    }
						    	    },
						    	  "legend":{
						    		     "y":"10%",
							    		 "marker":{
							    	          "type":donutchartval[6],
							    	          "border-width":0,
							    		 },
							    	    "border-width":0,  
							    	    "shadow":0,  
							    	    "alpha":1,  
							    	    "item":{  
							    	        "color":"#000",
							    	        "font-family":"roboto",
							    	        "font-size":13,
							    	    },
							    	    "y":"10%",
							    	 },   
						    	 "series": donutchartval[1]
				              		}
					    	    ]
			    }
		}
			zingchart.render({
			    id:donutchartval[0] ,
			    theme :'classic',
			    data: data,
			    width: "100%",
			    height: "auto",
			    defaults:materialtheme700,
			});
	}
		
}
{//Funnel Chart
	function funnelchartfun(funnelchartval)
	{
		var array = funnelchartval[1];		
		var text = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}
		var type = 'funnel';
		if(funnelchartval[3] == 'vertical'){
			type = "funnel";
		}
		else{
			type = "hfunnel";
		}
		var data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [ 
			              {
			            	"background-color":"#fff",	
							"type":type,	
					    	"title":{
					    		 "text":funnelchartval[2],
					    		 "background-color":"transparent",
					    		    "color":"#222222",
					    		    "font-family":"roboto",
					    		    "font-weight":"normal",
					    		    "text-align":"left",
					    		    "padding-top":"10px",
					    		    "padding-left":"20px",
					    		    "font-size":"16px"
					    		},	
				    		 "subtitle": {
				    			  "text": "My Subtitle",
				    			  "color":"#cdcdcd",
				    			  "text-align":"left",
				    			  "padding-left":"20px",
				    			},
					    	 "scale-x": {	
					    	 	},
					    	 "scale-y": {
					                "values": text
					                 },
					    	 "plot": {
					    		 	"tooltip":{
					    	            "visible":true, //Specify your visibility: true or false.
					    	            "font-family":"source-sans-pro",
					    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
					    	            "font-color":"#000000",
					    	            "font-size":14,
					    	            "text-align":"left",
					    	            "text-alpha":1,
					    	            "background-color":"#ffffff",
					    	            "alpha":1,
					    	            "border-radius":"2%",
					    	            "padding":"10%",
					    	            "callout": false,
					    	            "shadow":1,
					    	            "shadow-alpha":0.16,
					    	            "shadow-distance":"4px",
					    	            },
					                 "scales": "scale-x,scale-y",		
					             },
					         "series": funnelchartval[1]
					         }
			              ]
		}
		if(funnelchartval[4] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [ 
			              {
			 			     "background-color":"#ffffff",
					    	 "type":type,	
					    	 "title":{
					    		 "text":funnelchartval[2],
					    		 "background-color":"transparent",
					    		    "color":"#222222",
					    		    "font-family":"roboto",
					    		    "font-weight":"normal",
					    		    "text-align":"left",
					    		    "padding-top":"10px",
					    		    "padding-left":"20px",
					    		    "font-size":"16px"
					    		},	
				    		 "subtitle": {
				    			  "text": "My Subtitle",
				    			  "color":"#cdcdcd",
				    			  "text-align":"left",
				    			  "padding-left":"20px",
				    			},
					    	 "plot": {
					    		 "offset-r":"0%", //To apply globally.
					    	     "border-width":0,
					    		 "tooltip":{
					    	            "visible":true, //Specify your visibility: true or false.
					    	            "font-family":"source-sans-pro",
					    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
					    	            "font-color":"#000000",
					    	            "font-size":14,
					    	            "text-align":"left",
					    	            "text-alpha":1,
					    	            "background-color":"#ffffff",
					    	            "alpha":1,
					    	            "border-radius":"2%",
					    	            "padding":"10%",
					    	            "callout": false,
					    	            "shadow":1,
					    	            "shadow-alpha":0.16,
					    	            "shadow-distance":"4px",
					    	            },
			    	          },
					    	 "legend":{
					    		 "y":"10%",
					             "marker":{
					                 "type":funnelchartval[5],
					                 "border-width":0,
					                 "borderColor":"tranparent",
					                 "shadow":false,  
					                 "shadow-alpha":0
					             },	
					             "border-width":0,  
					    	     "shadow":0,  
					    	     "alpha":1,  
					    	     "item":{  
					    	        "color":"#000",
					    	        "font-family":"source-sans-pro",
					    	        "font-size":13,
					    	     }
					         },			    	  			 
					    	 "series": funnelchartval[1]
						   }
			            ]
			}
		}
		zingchart.render({
		    id:funnelchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		    defaults:materialtheme700,
		});

	}
}
{//Pyramid Chart
	function pyramidchartfun(pyramidchartval) {		
		var array = pyramidchartval[1];		
		var text = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}
		var type = 'vertical';
		if(pyramidchartval[3] == 'vertical'){
			type = "funnel";
		}
		else{
			type = "hfunnel";
		}
		var data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
					    ]
					},
				    "graphset": [ 
				                 {
				                 "background-color":"#fff",
						    	 "type":type,
						    	 "title":{
						    		 "text":pyramidchartval[2],
						    		 "background-color":"transparent",
						    		    "color":"#222222",
						    		    "font-family":"roboto",
						    		    "font-weight":"normal",
						    		    "text-align":"left",
						    		    "padding-top":"10px",
						    		    "padding-left":"20px",
						    		    "font-size":"16px"
						    		},	
					    		 "subtitle": {
					    			  "text": "My Subtitle",
					    			  "color":"#cdcdcd",
					    			  "text-align":"left",
					    			  "padding-left":"20px",
					    			},
						    	 "scale-x": {
						              
							    	 	},
							     "scale-y": {
						        	 mirrored: true,
						        	 "values": text
						         	}, 
						    	 "plot": {	
						    		 "offset-r":"0%", //To apply globally.
							    	 "border-width":0,
						    		 "tooltip":{
						    	            "visible":true, //Specify your visibility: true or false.
						    	            "font-family":"source-sans-pro",
						    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
						    	            "font-color":"#000000",
						    	            "font-size":14,
						    	            "text-align":"left",
						    	            "text-alpha":1,
						    	            "background-color":"#ffffff",
						    	            "alpha":1,
						    	            "border-radius":"2%",
						    	            "padding":"10%",
						    	            "callout": false,
						    	            "shadow":1,
						    	            "shadow-alpha":0.16,
						    	            "shadow-distance":"4px",
						    	            },
						                 "scales": "scale-x,scale-y",			                
						             },
						    	 "series": pyramidchartval[1]
				                 }
				                ]
		    }
		if(pyramidchartval[4] == 'true'){
			data = {
					"gui":{
						   "context-menu":{
						        "button":{
						            "visible":0
						        }
						    },
						   "behaviors":[
								        {
								            "id":"DownloadPDF",
								            "enabled":"none"
								        },	
								        {
								            "id":"DownloadSVG",
								            "enabled":"none"
								        },	
								        {
								            "id":"Reload",
								            "enabled":"none"
								        },	
								        {
								            "id":"FullScreen",
								            "enabled":"none"
								        },	
								        {
								            "id":"Print",
								            "enabled":"none"
								        },	
								        {
								            "id":"SaveAsImage",
								            "enabled":"none"
								        },	
								        {
								            "id":"ViewSource",
								            "enabled":"none"
								        },	
								        {
								        	"id":"LogScale",
								        	"enabled":"none"
								        }
								       ]
							},
							"graphset": [ 
							              {
							             "background-color":"#fff",
								    	 "type":type,
								    	 "title":{
								    		 "text":pyramidchartval[2],
								    		 "background-color":"transparent",
								    		    "color":"#222222",
								    		    "font-family":"roboto",
								    		    "font-weight":"normal",
								    		    "text-align":"left",
								    		    "padding-top":"10px",
								    		    "padding-left":"20px",
								    		    "font-size":"16px"
								    		},	
							    		 "subtitle": {
							    			  "text": "My Subtitle",
							    			  "color":"#cdcdcd",
							    			  "text-align":"left",
							    			  "padding-left":"20px",
							    			},
								    	 "scale-x": {
								    	 	},
								    	 "scale-y": {
								    		 	mirrored: true,
								                 },
								    	 "plot": {
								    		 "offset-r":"0%", //To apply globally.
									    	 "border-width":0,
								    		 "tooltip":{
								    	            "visible":true, //Specify your visibility: true or false.
								    	            "font-family":"source-sans-pro",
								    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
								    	            "font-color":"#000000",
								    	            "font-size":14,
								    	            "text-align":"left",
								    	            "text-alpha":1,
								    	            "background-color":"#ffffff",
								    	            "alpha":1,
								    	            "border-radius":"2%",
								    	            "padding":"10%",
								    	            "callout": false,
								    	            "shadow":1,
								    	            "shadow-alpha":0.16,
								    	            "shadow-distance":"4px",
								    	            },
								                 "scales":"scale-x,scale-y",		                
								             },
								         "legend":{
								        	 	 "y":"10%",
									             "marker":{
									                 "type":pyramidchartval[5],
									                 "border-width":0,
									             },
									             "border-width":0,  
									    	     "shadow":0,  
									    	     "alpha":1,  
									    	     "item":{  
									    	        "color":"#000",
									    	        "font-family":"source-sans-pro",
									    	        "font-size":13,
									    	     }
									         },			    	 
								    	 "series": pyramidchartval[1]
							              }
					             		]
					}
			}
		zingchart.render({
		    id:pyramidchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		    defaults:materialtheme700,
		});
	}
}
{//column
	function columnchartfun(columnchartval) 
	{	
		var array = columnchartval[1];		
		var text = [];
		var value = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'values'){
					value.push(parseInt(array[i][prop].join()));
					}
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}		
		var type = 'bar';
		if(columnchartval[4] == 'vertical'){
			type = "bar";
		}
		else{
			type = "hbar";
		}		
		if(columnchartval[3] == 'true'){
			if(columnchartval[4] == 'vertical'){
				type = "bar3d";
			}
			else{
				type = "hbar3d";
			}		
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
			         "background-color":"#ffffff",
				     "type":type,
				     "3d-aspect":{
				         "true3d":false,
				     },
				     "title":{
				    	 "text":columnchartval[2],
				    	 "background-color":"transparent",
			    		    "color":"#222222",
			    		    "font-family":"roboto",
			    		    "font-weight":"normal",
			    		    "text-align":"left",
			    		    "padding-top":"10px",
			    		    "padding-left":"20px",
			    		    "font-size":"16px"
			    		},	
		    		 "subtitle": {
		    			  "text": "My Subtitle",
		    			  "color":"#cdcdcd",
		    			  "text-align":"left",
		    			  "padding-left":"20px",
		    			},		
			    	 "plotarea": {
			    		    "adjust-layout":true, /* For automatic margin adjustment. */
							"margin-top":"40px"
			    		  },
			    	"plot":{
			    			"border-width":0,
			    			"tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "font-family":"source-sans-pro",
			    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
			    	            "font-color":"#000000",
			    	            "font-size":14,
			    	            "text-align":"left",
			    	            "text-alpha":1,
			    	            "background-color":"#ffffff",
			    	            "alpha":1,
			    	            "border-radius":"2%",
			    	            "padding":"10%",
			    	            "callout": false,
			    	            "shadow":1,
			    	            "shadow-alpha":0.16,
			    	            "shadow-distance":"4px",
			    	            },
		    			    "aspect":columnchartval[5],
			    		    "stacked":false 
		    			  },
			    	 "scale-x": {		    		
			    		 "labels":text, /* Scale Labels */	
			    	 	},	    	 
			    	 "series": [
			    	            {"values":value}
			    	           ]
			        }
			    ]
		    }
		if(columnchartval[6] == 'true'){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
					    ]
					},
				 "graphset": [
				        {
					     "background-color":"#ffffff",
					     "type":type,
					     "3d-aspect":{
					         "true3d":false,
					     },
					     "title":{
					    	 "text":columnchartval[2],
					    	 "background-color":"transparent",
				    		    "color":"#222222",
				    		    "font-family":"roboto",
				    		    "font-weight":"normal",
				    		    "text-align":"left",
				    		    "padding-top":"10px",
				    		    "padding-left":"20px",
				    		    "font-size":"16px"
				    		},	
			    		 "subtitle": {
			    			  "text": "My Subtitle",
			    			  "color":"#cdcdcd",
			    			  "text-align":"left",
			    			  "padding-left":"20px",
			    			},	    		
				    	 "plotarea": {
				    		    "adjust-layout":true, /* For automatic margin adjustment. */
								"margin-top":"40px"
				    		  },
				    	"plot":{
				    			"border-width":0,
				    			"tooltip":{
				    	            "visible":true, //Specify your visibility: true or false.
				    	            "font-family":"source-sans-pro",
				    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
				    	            "font-color":"#000000",
				    	            "font-size":14,
				    	            "text-align":"left",
				    	            "text-alpha":1,
				    	            "background-color":"#ffffff",
				    	            "alpha":1,
				    	            "border-radius":"2%",
				    	            "padding":"10%",
				    	            "callout": false,
				    	            "shadow":1,
				    	            "shadow-alpha":0.16,
				    	            "shadow-distance":"4px",
				    	            },
			    			    "aspect":columnchartval[5],
			    			    "stacked":false 
			    			  },
			    		 "legend":{	
			    			 "y":"10%",
			    			 "marker":{
			    	          "type":columnchartval[7],
			    	          "border-width":0,
			    			 },
			    			 "border-width":0,  
				    	     "shadow":0,  
				    	     "alpha":1,  
				    	     "item":{  
				    	        "color":"#000",
				    	        "font-family":"source-sans-pro",
				    	        "font-size":13,
				    	     }
			    		 },
				    	 "series": columnchartval[1]
				        }
				      ]
			    }
		}
		zingchart.render({
		    id:columnchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		    defaults:materialtheme700,
		});
	}
}
{// Wordcloud
	function wordcloudfun(wordcloudval){		
		zingchart.render({
		    id:wordcloudval[0] ,
		    theme :'classic',
		    width: "100%",
		    height: "auto",
		    data: {
		    		"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
				    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
					    ]
				},
			 "graphset": [
			        {
			    	"type" : "wordcloud",
			    	"title":{
				    	 "text":"Title",
				    	 "background-color":"transparent",
				    	 "color":"#000",
			    		 "font-family":"source-sans-pro"
			    		},
		    		"options":{ // Object
	    			    "text":"Home,Accounts,Leads,Contacts,Content,Documents,Collabaration,SMS,E-Mail,Sales,Leads,Purchase,Quotation,Invoice,Sales Order,Calls,Tasks,Calendar,Modules,Users,Profile,Activity,Notification,Templates,Payments,Support,Ticket,Solutions,Contract,Product,Price", // String
	    			    "max-font-size":50,
	    			    "aspect":"flow-center",
	    			  }
			        }
			       ]
		    	},
		 	defaults:materialtheme700,
		    });
	} 
}
{// Area Chart function
	function areachartfun(areachartval)
	{
		var array = areachartval[1];		
		var text = [];
		var value = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'values'){
					value.push(parseInt(array[i][prop]));
					}
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}
		var type = 'area';
		if(areachartval[4] == 'horizontal'){
			type = "area";
		}
		else{
			type = "varea";
		}		
		if(areachartval[3] == 'true'){
			if(areachartval[4] == 'horizontal'){
				type = "area3d";
			}
		}
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {		
				     "background-color":"#ffffff",
			    	 "type":type,
			    	 "3d-aspect":{
				         "true3d":false,
				     },
			    	 "title":{
			    		    "text":areachartval[2],
			    		    "background-color":"transparent",
			    		    "color":"#222222",
			    		    "font-family":"roboto",
			    		    "font-weight":"normal",
			    		    "text-align":"left",
			    		    "padding-top":"10px",
			    		    "padding-left":"20px",
			    		    "font-size":"16px"
			    		},	
		    		 "subtitle": {
		    			  "text": "My Subtitle",
		    			  "color":"#cdcdcd",
		    			  "text-align":"left",
		    			  "padding-left":"20px",
		    			},
			    	 "plot":{ 
			    		    "aspect":areachartval[5], 
				    		"border-width":0,
			    			"tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "font-family":"source-sans-pro",
			    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
			    	            "font-color":"#000000",
			    	            "font-size":14,
			    	            "text-align":"left",
			    	            "text-alpha":1,
			    	            "background-color":"#ffffff",
			    	            "alpha":1,
			    	            "border-radius":"2%",
			    	            "padding":"10%",
			    	            "callout": false,
			    	            "shadow":1,
			    	            "shadow-alpha":0.16,
			    	            "shadow-distance":"4px",
			    	            },
			    		  }, 	  
			    	 "plotarea": {
			    			    "margin":"70px"
			    			  },
			    	 "scale-x": {  
			    		 "labels":text
			    	 },
			    	 "series":[ 
			    	           {"values":value}, 
			    	           
			    	         ] 
			        }
			    ]
		    	 
		    }
		zingchart.render({
		    id:areachartval[0] ,
		    theme :'classic',
		    data: data,
		    width: '100%',
		    height: 'auto',
		    defaults:materialtheme700,
		});
	}
}
{// Line Chart function
	function linechartfun(linechartval)
	{
		var array = linechartval[1];		
		var text = [];
		var value = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'values'){
					value.push(parseInt(array[i][prop]));
					}
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}
		var type = 'line';
		if(linechartval[4] == 'horizontal'){
			type = "line";
		}
		else{
			type = "vline";
		}		
		if(linechartval[3] == 'true'){
			if(linechartval[4] == 'horizontal'){
				type = "line3d";
			}
		}
		zingchart.render({
		    id:linechartval[0] ,
		    theme :'classic',
		    width: "100%",
		    height: 'auto',
		    data: {
		    	"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				        {
				        	"id":"LogScale",
				        	"enabled":"none"
				        }
				    ]
				},
			 "graphset": [
			        {
					     "background-color":"#ffffff",
				    	 "type":type,
				    	 "3d-aspect":{
					         "true3d":false,
					     },
				    	 "title":{
				    		    "text":linechartval[2],
				    		    "background-color":"transparent",
				    		    "color":"#222222",
				    		    "font-family":"roboto",
				    		    "font-weight":"normal",
				    		    "text-align":"left",
				    		    "padding-top":"10px",
				    		    "padding-left":"20px",
				    		    "font-size":"16px"
				    		},	
			    		 "subtitle": {
			    			  "text": "My Subtitle",
			    			  "color":"#cdcdcd",
			    			  "text-align":"left",
			    			  "padding-left":"20px",
			    			},
		    			"plot":{ 
				    		    "aspect":linechartval[5], 
					    		"border-width":0,
				    			"tooltip":{
				    	            "visible":true, //Specify your visibility: true or false.
				    	            "font-family":"source-sans-pro",
				    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
				    	            "font-color":"#000000",
				    	            "font-size":14,
				    	            "text-align":"left",
				    	            "text-alpha":1,
				    	            "background-color":"#ffffff",
				    	            "alpha":1,
				    	            "border-radius":"2%",
				    	            "padding":"10%",
				    	            "callout": false,
				    	            "shadow":1,
				    	            "shadow-alpha":0.16,
				    	            "shadow-distance":"4px",
				    	            },
					    		  },
		    		    "plotarea": {
								"margin":"70px",
							},
				    	 "scale-x": {  
				    		 "labels":text
				    	 },
				    	 "series":[ 
				    	           {"values":value}, 				    	           
				    	         ] 
			        }
			    ]
		    },
		    defaults:materialtheme700,
		});
	}
}
{//stacked
	function stackchartfun(stackedchartval) {
		var array = stackedchartval[1];		
		var text = [];
		for (var i = 0; i < array.length; i++) {
			for (var prop in array[i]) {
				if(prop == 'text'){
					text.push(array[i][prop]);
					}
				}
			}
		var type = 'bar';
		if(stackedchartval[3] == 'vertical'){
			type = 'bar';
		}
		else {
			type = 'hbar';
		}
		var data =  {
					"gui":{
						"context-menu":{
								"button":{
									"visible":0
								}
							},
				    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					        {
					        	"id":"LogScale",
					        	"enabled":"none"
					        }
				        ]
					},
				    "graphset": [
						        {
							     "background-color":"#ffffff",
							     "type":type,
							     "title":{
						    		 "text":stackedchartval[2],
						    		 "background-color":"transparent",
						    		    "color":"#222222",
						    		    "font-family":"roboto",
						    		    "font-weight":"normal",
						    		    "text-align":"left",
						    		    "padding-top":"10px",
						    		    "padding-left":"20px",
						    		    "font-size":"16px"
						    		},	
					    		 "subtitle": {
					    			  "text": "My Subtitle",
					    			  "color":"#cdcdcd",
					    			  "text-align":"left",
					    			  "padding-left":"20px",
					    			},
						    	 "plot":{
						    		    "stacked":true,
						    		    "stack-type":stackedchartval[4], /* Optional specification */
						    		    "border-width":0,
						    			"tooltip":{
						    	            "visible":true, //Specify your visibility: true or false.
						    	            "font-family":"source-sans-pro",
						    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
						    	            "font-color":"#000000",
						    	            "font-size":14,
						    	            "text-align":"left",
						    	            "text-alpha":1,
						    	            "background-color":"#ffffff",
						    	            "alpha":1,
						    	            "border-radius":"2%",
						    	            "padding":"10%",
						    	            "callout": false,
						    	            "shadow":1,
						    	            "shadow-alpha":0.16,
						    	            "shadow-distance":"4px",
						    	            },
						    		  },	
					    		 "scale-x": {  
							    		 "labels":text
							    	 },
						    	 "series":stackedchartval[1]
						        }
						        ]
		    }		
		zingchart.render({
		    id:stackedchartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		    defaults:materialtheme700,
		});
	}
}
{//Angular Gauge Chart
	function angulargaguechartfun(angulargaugechartval)
	{
		var data = {
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
				    "behaviors":[
				        {
				            "id":"DownloadPDF",
				            "enabled":"none"
				        },	
				        {
				            "id":"DownloadSVG",
				            "enabled":"none"
				        },	
				        {
				            "id":"Reload",
				            "enabled":"none"
				        },	
				        {
				            "id":"FullScreen",
				            "enabled":"none"
				        },	
				        {
				            "id":"Print",
				            "enabled":"none"
				        },	
				        {
				            "id":"SaveAsImage",
				            "enabled":"none"
				        },	
				        {
				            "id":"ViewSource",
				            "enabled":"none"
				        },	
				    ]
				},
			 "graphset": [
			        {
			     "background-color":"#ffffff",
		    	 "type":"gauge",
		    	 "title":{
		    		    "text":angulargaugechartval[2],
		    		    "background-color":"transparent",
		    		    "color":"#222222",
		    		    "font-family":"roboto",
		    		    "font-weight":"normal",
		    		    "text-align":"left",
		    		    "padding-top":"10px",
		    		    "padding-left":"20px",
		    		    "font-size":"16px"
		    		},	
	    		 "subtitle": {
	    			  "text": "My Subtitle",
	    			  "color":"#cdcdcd",
	    			  "text-align":"left",
	    			  "padding-left":"20px",
	    			},
		    	 "plot":{
		    		    "csize":"10px",
		    		    "border-width":0,
		    		    "tooltip":{
		    	            "visible":true, //Specify your visibility: true or false.
		    	            "font-family":"source-sans-pro",
		    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
		    	            "font-color":"#000000",
		    	            "font-size":14,
		    	            "text-align":"left",
		    	            "text-alpha":1,
		    	            "background-color":"#ffffff",
		    	            "alpha":1,
		    	            "border-radius":"2%",
		    	            "padding":"10%",
		    	            "callout": false,
		    	            "shadow":1,
		    	            "shadow-alpha":0.16,
		    	            "shadow-distance":"4px",
		    	          },
		    		  },
		    	 "scale-r":{
		    			    "center":{
		    			      "size":5,
		    			    },
		    			    "aperture":"240"
		    		},		    		
		    	"series":angulargaugechartval[1]
		    	}
			]
		}
		if(angulargaugechartval[3] == 'true' ){
			data = {
					"gui":{
					    "context-menu":{
					        "button":{
					            "visible":0
					        }
					    },
					    "behaviors":[
					        {
					            "id":"DownloadPDF",
					            "enabled":"none"
					        },	
					        {
					            "id":"DownloadSVG",
					            "enabled":"none"
					        },	
					        {
					            "id":"Reload",
					            "enabled":"none"
					        },	
					        {
					            "id":"FullScreen",
					            "enabled":"none"
					        },	
					        {
					            "id":"Print",
					            "enabled":"none"
					        },	
					        {
					            "id":"SaveAsImage",
					            "enabled":"none"
					        },	
					        {
					            "id":"ViewSource",
					            "enabled":"none"
					        },	
					    ]
					},
				 "graphset": [
				        {
				     "background-color":"#ffffff",
			    	 "type":"gauge",
			    	 "title":{
			    		    "text":angulargaugechartval[2],
			    		    "background-color":"transparent",
			    		    "color":"#222222",
			    		    "font-family":"roboto",
			    		    "font-weight":"normal",
			    		    "text-align":"left",
			    		    "padding-top":"10px",
			    		    "padding-left":"20px",
			    		    "font-size":"16px"
			    		},	
		    		 "subtitle": {
		    			  "text": "My Subtitle",
		    			  "color":"#cdcdcd",
		    			  "text-align":"left",
		    			  "padding-left":"20px",
		    			},
			    	 "plot":{
			    		    "csize":"10px",
			    		    "border-width":0,
			    		    "tooltip":{
			    	            "visible":true, //Specify your visibility: true or false.
			    	            "font-family":"source-sans-pro",
			    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
			    	            "font-color":"#000000",
			    	            "font-size":14,
			    	            "text-align":"left",
			    	            "text-alpha":1,
			    	            "background-color":"#ffffff",
			    	            "alpha":1,
			    	            "border-radius":"2%",
			    	            "padding":"10%",
			    	            "callout": false,
			    	            "shadow":1,
			    	            "shadow-alpha":0.16,
			    	            "shadow-distance":"4px",
			    	          },
			    		  },
			    	 "scale-r":{
			    			    "center":{
			    			      "size":5,
			    			    },
			    			   "aperture":"240"
			    		},	
			    	"legend":{
			    			 "y":"10%",
				    		 "marker":{
				    	          "type":angulargaugechartval[4],
				    	          "border-width":0,
				    		 },
				    		 "border-width":0,  
				    	     "shadow":0,  
				    	     "alpha":1,  
				    	     "item":{  
				    	        "color":"#000",
				    	        "font-family":"source-sans-pro",
				    	        "font-size":13,
				    	     }
				    	 }, 	
			    	"series":angulargaugechartval[1]
			    	}
			]
		}
	}
		zingchart.render({
		    id:angulargaugechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",	
		    defaults:materialtheme700,
		 });
		
	}
}
{//Bubble
	function bubblechartfun(bubblechartval){
		var type = "bubble"
	    if(bubblechartval[3] == 'vertical'){
	    	type = "bubble"
	    }
	    else{
	    	type = "hbubble"
	    }
		var data ={
				"gui":{
				    "context-menu":{
				        "button":{
				            "visible":0
				        }
				    },
			    "behaviors":[
			        {
			            "id":"DownloadPDF",
			            "enabled":"none"
			        },	
			        {
			            "id":"DownloadSVG",
			            "enabled":"none"
			        },	
			        {
			            "id":"Reload",
			            "enabled":"none"
			        },	
			        {
			            "id":"FullScreen",
			            "enabled":"none"
			        },	
			        {
			            "id":"Print",
			            "enabled":"none"
			        },	
			        {
			            "id":"SaveAsImage",
			            "enabled":"none"
			        },	
			        {
			            "id":"ViewSource",
			            "enabled":"none"
			        },	
			        {
			        	"id":"LogScale",
			        	"enabled":"none"
			        }
			    ]
			},
		 "graphset": [
				        {
				         "background-color":"#ffffff",
				    	 "type":type,
				    	 "title":{
				    		    "text":bubblechartval[2],
				    		    "background-color":"transparent",
				    		    "color":"#222222",
				    		    "font-family":"roboto",
				    		    "font-weight":"normal",
				    		    "text-align":"left",
				    		    "padding-top":"10px",
				    		    "padding-left":"20px",
				    		    "font-size":"16px"
				    		},	
			    		 "subtitle": {
			    			  "text": "My Subtitle",
			    			  "color":"#cdcdcd",
			    			  "text-align":"left",
			    			  "padding-left":"20px",
			    			},
				    	"plot":{
					    		"border-width":0,
				    		    "tooltip":{
				    	            "visible":true, //Specify your visibility: true or false.
				    	            "font-family":"source-sans-pro",
				    	            "text":"<span style='color:#000;'>%t</span><br><span style='font-weight:bold'>%v</span>",
				    	            "font-color":"#000000",
				    	            "font-size":14,
				    	            "text-align":"left",
				    	            "text-alpha":1,
				    	            "background-color":"#ffffff",
				    	            "alpha":1,
				    	            "border-radius":"2%",
				    	            "padding":"10%",
				    	            "callout": false,
				    	            "shadow":1,
				    	            "shadow-alpha":0.16,
				    	            "shadow-distance":"4px",
				    	          },
					    		 "marker":{
					                 "type":bubblechartval[4],
				    				 "border-width":0,
					             },				    			
				    		  },
				    	"series":bubblechartval[1]
				        }
		        ]
	    	
		}
		zingchart.render({
		    id:bubblechartval[0] ,
		    theme :'classic',
		    data: data,
		    width: "100%",
		    height: "auto",
		    defaults:materialtheme700,
		    });		
	}
}
{// Widget Creation Conversation -select
 function conversationwidgetcreation(widgettitle,appendid,appendclass,data) {
	var appendid=$('#conversationdivid').val();
 	$('#'+appendid+'').empty();
	$('#'+appendclass+'').empty();
	$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span id="conversationaddicon" title="Add" class="widgetredirecticonconversation"><i class="material-icons">add</i></span></div></div><div class="large-12 columns scrollbarclass c_scroll" style="height: 16rem;overflow-y:scroll"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><span class="large-12 columns mblnopadding" id="textenterdiv"><div class="large-12 columns paddingzero"><div class="large-12 columns paddingzero" style="text-align:left;cursor:pointer"><input type="hidden" id="unique_rowid"/></span></div> <div class="large-12 columns hidedisplay titleinputspan">&nbsp;</div> <div class="large-12 columns paddingzero">   <div class="atsearchdiv hidedisplay"></div><div style="background:#e0e0e0;" class="large-12 columns paddingzero"><div class="large-12 columns conversbar paddingzero"><span id="convmulitplefileuploader"></span><span class="uploadattachments attachbtnstyle" title="Attachment" style="cursor:pointer;"><i class="material-icons">attachment</i></span><textarea class="mention messagetoenterstyle" rows="2" placeholder="Enter Your Message Here..." id="homedashboardconvid" name="homedashboardconvid"></textarea><span class="sendbtnstyle" id="postsubmit" style=""><i class="material-icons">send</i></span><span class="sendbtnstyle hidedisplay" id="editpostsubmit" style=""><i class="material-icons">send</i></span><span class="convfileupload hidedisplay" style="position:relative;top:0.1rem;"> <!--<span id="c_fileid" class="attachmentname" data-filepath="">--></span><input type="hidden" id="cc_filename"/><input type="hidden" id="cc_filepath"/></span></div><div class="large-6 medium-6  small-6 columns" style="text-align:right;"><!--<span><select class="chzn-select" style="width:35%" id="c_privacy" "><option value="1">Public</option><option value="2">OnlyMe</option></select></span><span class="postbtnstyle" id="postsubmit">Post</span><span class="postbtnstyle hidedisplay" id="editpostsubmit">Post</span><span class="postbtnstyle hidedisplay" id="postcancel">Cancel</span>--></div></div><div class="large-12 columns">&nbsp;</div></div></div></span></div></div></div>');
	var employeeurl=base_url+'Employee';
	var conversationappend="";
	for(var i=0; i<data.length;i++) {
		var file = "";
		var filefolder ="";
		var editablefield='';
		if(data[i]['profile'] !== null){	
			var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
		} else {
			var imgurl = '<div class="convers-user-img"><span class=""><i class="material-icons">account_circle</i></span></div>';
		}
		if(data[i]['profile'] != '' || data[i]['profile'] !== null || data[i]['profile'] !== 'undefined'){
			if(data[i]['profile']) {
				if(data[i]['fromid'] == '2' ) {
					var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+data[i]['profile']+'">';
				} else {
					var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
				}
			}
		}
		if(checkValue(data[i]['filepath']) == true){			
			var file=data[i]['file'];
			var filefolder=base_url+data[i]['filepath'];
		}
		if (data[i]['editable'] == "yes"){
		var editablefield='<span class="convopicons c_edit" data-rowid="'+data[i]['c_id']+'" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons c_delete" data-rowid="'+data[i]['c_id']+'" title="Delete"><i class="material-icons">delete</i></span>'
		}
		if(filefolder){
			var attachment = '<span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span>';
		} else {
			var attachment = '';
		}
		conversationappend +='<div class="large-12 columns mblnopadding"><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv"><div class="large-1 medium-1 small-1  column paddingzero">'+imgurl+'</div><div class="large-11 medium-11 small-11 columns talk-bubble tri-right left-top z-depth-1"><div class="large-12 columns paddingzero"><div class="large-6 column paddingzero"><div style="color:#df6c6e" class="large-12 columns conversationnamestyle"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+data[i]['userid']+'>'+data[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle">'+data[i]['c_title']+'</div></div><div class="large-6 columns paddingzero iconsetconv" style="text-align: right; opacity: 0;"><span data-id="replycount12" style="font-size: 1rem; vertical-align: top; line-height: 24px;"></span>&nbsp;<span class="vcancel hidedisplay"><span class="viewcancelaction convopicons" data-rowid="'+data[i]['c_id']+'" data-eq="'+i+'"title="Close Reply"><i class="material-icons">visibility_off</i></span></span><span id="viewaction'+data[i]['c_id']+'" class="viewaction convopicons" data-eq="'+i+'" data-rowid="'+data[i]['c_id']+'" title="View Reply"><i class="material-icons">visibility</i></span><span class="replyaction convopicons c_reply" data-actionid="viewaction'+data[i]['c_id']+'" data-eq="'+i+'"data-rowid="'+data[i]['c_id']+'" title="Reply"><i class="material-icons">reply</i></span>'+editablefield+'</div><div class="large-12 columns messagecontentstyle paddingzero">'+data[i]['c_name']+'</div></div><div class="large-12  columns paddingzero"><span class="conversationtime" title="">'+data[i]['time']+'</span></div></div></div></div><div class="large-12 columns">&nbsp</div>';
	}
	$(".ajax-upload-dragdrop").addClass('hidedisplay');
	$("#"+appendclass+"").append(conversationappend);
	CONVERSATIONPOST=1;//show
	$("#conversationaddicon").click(function(){
		$('#textenterdiv').slideToggle();
		if(CONVERSATIONPOST == 1){
			CONVERSATIONPOST = 0; //hide
		} else {
			CONVERSATIONPOST = 1;
		}
	});
	
	$(function() {
		setTimeout(function(){
			//download-clicks file//pending verfiy file exists in that path-else alert not available
			$(".c_filedownload").click(function(){
				var file=$(this).attr('data-file');
				if(checkValue(file) == true){
					var ExportPath = base_url+'Home/download';
					var filepath=base_url+file;					
					var path=file;
					var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+file+'" /><input type="hidden" name="filename" id="filename" value="'+path+'" /></form>');
					$(form).appendTo('body');
					form.submit();
				}
			});
			$(".replyaction").click(function(){
				$('.replyactioncontainer').remove();
				var c_reply_rowid=$(this).attr("data-rowid");		
				var i=$(this).attr("data-eq");		
				$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyactioncontainer"><div class="large-12 columns">&nbsp;</div><div class="large-12 columns conversbar paddingzero"><textarea id="replytextarea" class="mention convmessagetoenterstyle" placeholder="Enter your reply here..."></textarea><input type="hidden" id="replyrowid"/><div class="atsearchdiv hidedisplay"></div><span class="sendbtnstyle replyactionsave"><i class="material-icons">send</i></span><span class="sendbtnstyle replyactioncancel"><i class="material-icons">cancel</i></span></div><div class="large-12 columns">&nbsp;</div></div>'); 
				$('#replyrowid').val(c_reply_rowid);
				//removes the on reply area
				$('.replyactioncancel').click(function(){
					$(this).parent('div').parent('div').remove();
				});
				//on reply submit
				$('.replyactionsave').click(function(){
					//get the reply data
					var reply_rowid=$('#replyrowid').val();
					var reply_message=$('#replytextarea').val();
					if(checkValue(reply_rowid) == true  && checkValue(reply_message) == true){
						var status=conversationreplyinsert(reply_rowid,reply_message);
						if(status == true){
						$(".totalviewcontainer").remove();
						$(".viewcancelaction:eq(" + i + ")").trigger('click');
						$(this).parent('div').parent('div').remove();
							//append recent-comments
							var cc='commentbox'+reply_rowid;					
						}
					} else {
						alertpopup("No Empty Reply");
					}
				});
				$('.mention').smention(""+base_url+""+"Home/getuser",{
					after : " "
				});	
			});
			//hide the view box
			$(".viewcancelaction").click(function(){
				var i=$(this).attr("data-eq");
				var rowid=$(this).attr("data-rowid");
				$(".totalviewcontainer"+rowid+"").remove();
				$(".viewaction:eq("+i+")").show();
				$(".vcancel:eq("+i+")").addClass('hidedisplay');
			});
			//on click its shows the reply's to that post
			$(".viewaction").click(function(){
				var actionid=$(this).attr("id");
				$('.replyactioncontainer').remove();
				var c_comment_rowid=$(this).attr("data-rowid");
				var i=$(this).attr("data-eq");
				var commentdata=loadcommentdata(c_comment_rowid);		
				if(checkValue(c_comment_rowid) == true && commentdata.length > 0){

				//vshow,vcancel.
				$(".viewaction:eq("+i+")").hide();
				$(".vcancel:eq("+i+")").removeClass('hidedisplay');
				var employeeurl=base_url+'Employee';
				var rcount = commentdata[i]['count'];
				for(var i=0; i<commentdata.length;i++) 
				{
					var replyeditablefield='';
					if(commentdata[i]['profile'] !== null){	
						var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
					}else{
						var imgurl = '<div class="convers-user-img"><span class=""><i class="material-icons">account_circle</i></span></div>';
					}
					if( rcount != 0){
						var replycount = commentdata[i]['count']+' reply';
					} else {
						var replycount = '';
					}
					if(commentdata[i]['profile'] != '' || commentdata[i]['profile'] !== null || commentdata[i]['profile'] !== 'undefined'){
						if(commentdata[i]['profile']){
							if(commentdata[i]['fromid'] == '2' ) {
								var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+commentdata[i]['profile'];+'">';
							} else {
								var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
							}
						}
					}
					if (commentdata[i]['editable'] == "yes"){
						var replyeditablefield='<span class="material-icons delete convopicons creply_delete" data-rowid="'+commentdata[i]['c_id']+'" title="Delete" data-eq="'+i+'"></span>';
					}
					var commentbox='commentbox'+c_comment_rowid;
					$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns mblnopadding"><div class="large-12 columns paddingzero rtotalviewcontainer totalviewcontainer'+c_comment_rowid+'"><div class="large-12 columns mblnopadding"> &nbsp;</div><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv '+commentbox+'"><div class="large-1 small-1 column paddingzero">'+imgurl+'</div><div class="large-11 small-11 columns talk-bubble tri-right left-top z-depth-1"><div class="large-12 columns paddingzero"><div class="large-6 columns paddingzero"><div class="large-6 column paddingzero"><div class="large-6 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+commentdata[i]['userid']+'>'+commentdata[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle"></div></div></div><div style="text-align: right; opacity: 0;" class="large-6 columns paddingzero iconsetconv"><span style="font-size: 1rem; vertical-align: top; line-height: 24px;" data-id="replycount'+commentdata[i]['c_id']+'">'+replycount+'</span>&nbsp;'+replyeditablefield+'</div><div class="large-12 columns messagecontentstyle paddingzero">'+commentdata[i]['c_name']+'</div></div><div class="large-12 columns paddingzero"><span class="conversationtime" title="'+commentdata[i]['c_date']+'">'+commentdata[i]['time']+'</span></div></div></div></div></div>');
				}
				//reply hovers
				$( ".totalviewcontainer" ).hover(function(){
					$( this ).find('.iconsetconv').css('opacity','1');}, function(){
					$( this ).find('.iconsetconv').css('opacity','0');
				});
				////conversation delete
				$('.creply_delete').click(function(){
					var appendid=$('#conversationdivid').val();
					var c_delete_rowid=$(this).attr("data-rowid");
					var rclass = $(".rtotalviewcontainer");
					var i=$(this).attr("data-eq");
					if(c_delete_rowid !='' ||c_delete_rowid != null){
						$.ajax({
							url:base_url+"Home/conversationdelete",
							data:{rowid:c_delete_rowid},
							type:"POST",
							async:false,
							cache:false,
							success :function(msg) {
								rclass.remove();
								$("#"+actionid+"").trigger('click');
								//remove the specific reply
								$(".totalviewcontainer:eq("+i+")").remove();
							}
						});
					}
				});
				}
			});		
			//conversation create
			var moduleid=$('#conversationdivid').attr('data-moduleid');
			var rowid=$('#conversationdivid').attr('data-rowid');
			var appendid=$('#conversationdivid').val();
			$('#postsubmit').click(function(){
				var conversationmsg = $('#homedashboardconvid').val();
				var conversationtt = $('#homedashboardtitle').val();
				var file=$('#cc_filepath').val();
				var filename=$('#cc_filename').val();
				var privacy=$('#c_privacy').val();		
				if(conversationmsg != "") {
					$.ajax({
						url:base_url+'Home/conversationcreate',				
						data:{convmsg:conversationmsg,convtitle:conversationtt,moduleid:moduleid,rowid:rowid,privacy:privacy,file:file,fname:filename},
						type:"POST",
						async:false,
						cache:false,
						success :function(msg) {
							var nmsg = $.trim(msg);
							if(nmsg == true) {
								loadconversationlog(appendid);					
							}
						}
					});
				}
			});
			//conversation editdata
			$('.c_edit').click(function(){
				$('#postsubmit').addClass('hidedisplay');
				$("#editpostsubmit").removeClass('hidedisplay');
				$('#postcancel').removeClass('hidedisplay');
				var c_edit_rowid=$(this).attr("data-rowid");
				if(c_edit_rowid !='' ||c_edit_rowid != null){
					$.ajax({
						url:base_url+"Home/getconversationeditdata",
						data:{rowid:c_edit_rowid},
						type:"POST",
						async:false,
						cache:false,
						dataType:'json',
						success :function(data) {
							//set the data
							 setTimeout(function(){
							$('#homedashboardtitle').val(data.c_title);
							$('#homedashboardconvid').val(data.c_message);
							$('#unique_rowid').val(data.c_rowid);
							if(checkValue(data.c_file) == true){					
							  $('#cc_filepath').val(data.c_file);
							  $('#cc_filename').val(data.c_title);
							  $('#c_fileid').text(data.c_file);
							}
							$('#homedashboardconvid').val(data.c_message);
							},10);
							$('.addtitlestyle').trigger('click');
							
							if(CONVERSATIONPOST == 0){
								 $('#textenterdiv').slideToggle();//slides down the comment overlay
							}
						}
					});
				}
				//scrollbarclass
				$('.c_scroll').animate({scrollTop:0},'slow');	
			});
			//conversation update
			$('#editpostsubmit').click(function(){
			 var appendid=$('#conversationdivid').val();
				var rowid=$('#unique_rowid').val();
				var moduleid=$('#conversationdivid').attr('data-moduleid');
				var conversationmsg = $('#homedashboardconvid').val();
				var conversationtt = $('#homedashboardtitle').val();
				var file=$('#cc_filepath').val();
				var privacy=$('#c_privacy').val();		
				if(rowid !='' ||rowid != null){
					$.ajax({
						url:base_url+"Home/updateconversationdata",
						data:{rowid:rowid,moduleid:moduleid,c_message:conversationmsg,convtitle:conversationtt,privacy:privacy,file:file},
						type:"POST",
						async:false,
						cache:false,						
						success :function(msg) {
							//reload the div
							loadconversationlog(appendid);
						}
					});
				}
			});
			//conversation delete
			$('.c_delete').click(function(){
				var appendid=$('#conversationdivid').val();
				var c_delete_rowid=$(this).attr("data-rowid");
				if(c_delete_rowid !='' ||c_delete_rowid != null){
					$.ajax({
						url:base_url+"Home/conversationdelete",
						data:{rowid:c_delete_rowid},
						type:"POST",
						async:false,
						cache:false,
						success :function(msg) {
							loadconversationlog(appendid);
						}
					});
				}
			});
			
			//conversation cancel-on edit mode
			$('#postcancel').click(function(){	
				//reset the conversation
				$('#removefile').trigger('click');
				$('#homedashboardtitle,#homedashboardconvid').val('');		
				$("#editpostsubmit").addClass('hidedisplay');
				$('#postcancel').addClass('hidedisplay');
				$('#postsubmit').removeClass('hidedisplay');
			});
			$(".addtitlestyle").click(function(){
				$(this).addClass('hidedisplay');
				$('.titleinputspan').removeClass('hidedisplay');
			});
			
			$( ".totalcotainerconv" ).hover(function(){
				$( this ).find('.iconsetconv').css('opacity','1');}, function(){
				$( this ).find('.iconsetconv').css('opacity','0');
			});	
			// 	
			{	//load -data-onscrolls
				$("#conversationwidgetclass").scroll( function() {
					  if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight()) {							  
						loadconversationend('conversationwidgetclass');
					  }
				}); 
			}	
			{// file upload
				$("#convmulitplefileuploader").hide();
				setTimeout(function(){
					$(".ajax-file-upload-container").hide(); 
					$(".ajax-file-upload-filename").hide(); 
				}, 100);
				fileuploadmoname = 'convfileupload';
				var conversationsettings = {
				sequential:true,
				sequentialCount:1,
				showStatusAfterSuccess: false,
				url: base_url+"upload.php",
				method: "POST",
				fileName: "myfile",
				allowedTypes: "*",
				dataType:'json',
				async:false,
				onSuccess:function(files,data,xhr){
					if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
						var arr = jQuery.parseJSON(data);
						var c_filepath=arr.path;
						var c_filename=arr.fname;//convfileupload
						$('.convfileupload').removeClass('hidedisplay');
						$('#cc_filepath').val(c_filepath);
						$('#cc_filename').val(c_filename);
						$('#c_fileid').text(files);
					}else if(data == "Size"){
						alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
					} else if(data == "MaxSize"){
						alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
					} else if(data == "MaxFile"){
						alertpopupdouble("Maximum number fo files exceeded.");
					}else{
						alertpopupdouble("Upload Failed");
					}
				},
				onError: function(files,status,errMsg) {
				}}
				$("#convmulitplefileuploader").uploadFile(conversationsettings);
				$(".uploadattachments").click(function(e){
					e.preventDefault(); 
					$(".ajax-file-upload>form>input:first").trigger('click');
					e.stopPropagation();
					return false;
				});
				{//file upload removal-
					$('#removefile').click(function(){	
						$('#c_fileid').text('');
						$('.convfileupload').addClass('hidedisplay');
						$('#cc_filepath').val('');
					});
				}
			}
			
			$('.mention').smention(""+base_url+""+"Home/getuser",{
				after : " "
			});			
		},100); 
	});	
 }
 // mini conversation
 function conversationwidgetcreationmini(widgettitle,appendid,appendclass,data) {
		var appendid='chartgenid0';
		$('#'+appendclass+'').empty();
		var employeeurl=base_url+'Employee';
		var conversationappend="";
		for(var i=0; i<data.length;i++) {
			var file = "";
			var filefolder ="";
			var editablefield='';
			if(data[i]['profile'] !== null){	
				var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
			} else {
				var imgurl = '<div class="convers-user-img"><span class=""><i class="material-icons">account_circle</i></span></div>';
			}
			if(data[i]['profile'] != '' || data[i]['profile'] !== null || data[i]['profile'] !== 'undefined'){
				if(data[i]['profile']) {
					if(data[i]['fromid'] == '2' ) {
						var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+data[i]['profile']+'">';
					} else {
						var imgurl = '<img class="convers-user-img" id="" title="" src="'+data[i]['profile']+'">';
					}
				}
			}
			if(checkValue(data[i]['filepath']) == true){			
				var file=data[i]['file'];
				var filefolder=base_url+data[i]['filepath'];
			}
			if (data[i]['editable'] == "yes"){
			var editablefield='<span class="convopicons c_edit" data-rowid="'+data[i]['c_id']+'" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons c_delete" data-rowid="'+data[i]['c_id']+'" title="Delete"><i class="material-icons">delete</i></span>'
			}
			if(filefolder){
				var attachment = '<span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span>';
			} else {
				var attachment = '';
			}
			conversationappend +='<div class="large-12 columns mblnopadding"><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv"><div class="large-1 medium-1 small-1  column paddingzero">'+imgurl+'</div><div class="large-11 medium-11 small-11 columns talk-bubble tri-right left-top z-depth-1"><div class="large-12 columns paddingzero"><div class="large-6 column paddingzero"><div style="color:#df6c6e" class="large-12 columns conversationnamestyle"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+data[i]['userid']+'>'+data[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle">'+data[i]['c_title']+'</div></div><div class="large-6 columns paddingzero iconsetconv" style="text-align: right; opacity: 0;"><span data-id="replycount12" style="font-size: 1rem; vertical-align: top; line-height: 24px;"></span>&nbsp;<span class="vcancel hidedisplay"><span class="viewcancelaction convopicons" data-rowid="'+data[i]['c_id']+'" data-eq="'+i+'"title="Close Reply"><i class="material-icons">visibility_off</i></span></span><span id="viewaction'+data[i]['c_id']+'" class="viewaction convopicons" data-eq="'+i+'" data-rowid="'+data[i]['c_id']+'" title="View Reply"><i class="material-icons">visibility</i></span><span class="material-icons replyaction convopicons c_reply" data-actionid="viewaction'+data[i]['c_id']+'" data-eq="'+i+'"data-rowid="'+data[i]['c_id']+'" title="Reply">reply</span>'+editablefield+'</div><div class="large-12 columns messagecontentstyle paddingzero">'+data[i]['c_name']+'</div></div><div class="large-12  columns paddingzero"><span class="conversationtime" title="">'+data[i]['time']+'</span></div></div></div></div><div class="large-12 columns">&nbsp</div>';
		}
		$(".ajax-upload-dragdrop").addClass('hidedisplay');
		$("#"+appendclass+"").append(conversationappend);
		CONVERSATIONPOST=1;//show
		$("#conversationaddicon").click(function(){
			$('#textenterdiv').slideToggle();
			if(CONVERSATIONPOST == 1){
				CONVERSATIONPOST = 0; //hide
			} else {
				CONVERSATIONPOST = 1;
			}
		});
		
		$(function() {
			setTimeout(function(){
				//download-clicks file//pending verfiy file exists in that path-else alert not available
				$(".c_filedownload").click(function(){
					var file=$(this).attr('data-file');
					if(checkValue(file) == true){
						var ExportPath = base_url+'Home/download';
						var filepath=base_url+file;					
						var path=file;
						var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+file+'" /><input type="hidden" name="filename" id="filename" value="'+path+'" /></form>');
						$(form).appendTo('body');
						form.submit();
					}
				});
				$(".replyaction").click(function(){
					$('.replyactioncontainer').remove();
					var c_reply_rowid=$(this).attr("data-rowid");		
					var i=$(this).attr("data-eq");		
					$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyactioncontainer"><div class="large-12 columns">&nbsp;</div><div class="large-12 columns conversbar paddingzero"><textarea id="replytextarea" class="mention convmessagetoenterstyle" placeholder="Enter your reply here..."></textarea><input type="hidden" id="replyrowid"/><div class="atsearchdiv hidedisplay"></div><span class="sendbtnstyle replyactionsave"><i class="material-icons">send</i></span><span class="sendbtnstyle replyactioncancel"><i class="material-icons">cancel</i></span></div><div class="large-12 columns">&nbsp;</div></div>'); 
					$('#replyrowid').val(c_reply_rowid);
					//removes the on reply area
					$('.replyactioncancel').click(function(){
						$(this).parent('div').parent('div').remove();
					});
					//on reply submit
					$('.replyactionsave').click(function(){
						//get the reply data
						var reply_rowid=$('#replyrowid').val();
						var reply_message=$('#replytextarea').val();
						if(checkValue(reply_rowid) == true  && checkValue(reply_message) == true){
							var status=conversationreplyinsert(reply_rowid,reply_message);
							if(status == true){
							$(".totalviewcontainer").remove();
							$(".viewcancelaction:eq(" + i + ")").trigger('click');
							$(this).parent('div').parent('div').remove();
								//append recent-comments
								var cc='commentbox'+reply_rowid;					
							}
						} else {
							alertpopup("No Empty Reply");
						}
					});
					$('.mention').smention(""+base_url+""+"Home/getuser",{
						after : " "
					});	
				});
				//hide the view box
				$(".viewcancelaction").click(function(){
					var i=$(this).attr("data-eq");
					var rowid=$(this).attr("data-rowid");
					$(".totalviewcontainer"+rowid+"").remove();
					$(".viewaction:eq("+i+")").show();
					$(".vcancel:eq("+i+")").addClass('hidedisplay');
				});
				//on click its shows the reply's to that post
				$(".viewaction").click(function(){
					var actionid=$(this).attr("id");
					$('.replyactioncontainer').remove();
					var c_comment_rowid=$(this).attr("data-rowid");
					var i=$(this).attr("data-eq");
					var commentdata=loadcommentdata(c_comment_rowid);		
					if(checkValue(c_comment_rowid) == true && commentdata.length > 0){

					//vshow,vcancel.
					$(".viewaction:eq("+i+")").hide();
					$(".vcancel:eq("+i+")").removeClass('hidedisplay');
					var employeeurl=base_url+'Employee';
					var rcount = commentdata[i]['count'];
					for(var i=0; i<commentdata.length;i++) 
					{
						var replyeditablefield='';
						if(commentdata[i]['profile'] !== null){	
							var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
						}else{
							var imgurl = '<div class="convers-user-img"><span class=""><i class="material-icons">account_circle</i></span></div>';
						}
						if( rcount != 0){
							var replycount = commentdata[i]['count']+' reply';
						} else {
							var replycount = '';
						}
						if(commentdata[i]['profile'] != '' || commentdata[i]['profile'] !== null || commentdata[i]['profile'] !== 'undefined'){
							if(commentdata[i]['profile']){
								if(commentdata[i]['fromid'] == '2' ) {
									var imgurl='<img class="convers-user-img" id="" title="" src="'+base_url+commentdata[i]['profile'];+'">';
								} else {
									var imgurl = '<img class="convers-user-img" id="" title="" src="'+commentdata[i]['profile']+'">';
								}
							}
						}
						if (commentdata[i]['editable'] == "yes"){
							var replyeditablefield='<span class="convopicons creply_delete" data-rowid="'+commentdata[i]['c_id']+'" title="Delete" data-eq="'+i+'"><i class="material-icons">delete</i></span>';
						}
						var commentbox='commentbox'+c_comment_rowid;
						$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns mblnopadding"><div class="large-12 columns paddingzero rtotalviewcontainer totalviewcontainer'+c_comment_rowid+'"><div class="large-12 columns mblnopadding"> &nbsp;</div><div class="large-12 medium-12 small-12 columns mblnopadding totalcotainerconv '+commentbox+'"><div class="large-1 small-1 column paddingzero">'+imgurl+'</div><div class="large-11 small-11 columns talk-bubble tri-right left-top z-depth-1"><div class="large-12 columns paddingzero"><div class="large-6 columns paddingzero"><div class="large-6 column paddingzero"><div class="large-6 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+commentdata[i]['userid']+'>'+commentdata[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle"></div></div></div><div style="text-align: right; opacity: 0;" class="large-6 columns paddingzero iconsetconv"><span style="font-size: 1rem; vertical-align: top; line-height: 24px;" data-id="replycount'+commentdata[i]['c_id']+'">'+replycount+'</span>&nbsp;'+replyeditablefield+'</div><div class="large-12 columns messagecontentstyle paddingzero">'+commentdata[i]['c_name']+'</div></div><div class="large-12 columns paddingzero"><span class="conversationtime" title="'+commentdata[i]['c_date']+'">'+commentdata[i]['time']+'</span></div></div></div></div></div>');
					}
					//reply hovers
					$( ".totalviewcontainer" ).hover(function(){
						$( this ).find('.iconsetconv').css('opacity','1');}, function(){
						$( this ).find('.iconsetconv').css('opacity','0');
					});
					////conversation delete
					$('.creply_delete').click(function(){
						var appendid=$('#conversationdivid').val();
						var c_delete_rowid=$(this).attr("data-rowid");
						var rclass = $(".rtotalviewcontainer");
						var i=$(this).attr("data-eq");
						if(c_delete_rowid !='' ||c_delete_rowid != null){
							$.ajax({
								url:base_url+"Home/conversationdelete",
								data:{rowid:c_delete_rowid},
								type:"POST",
								async:false,
								cache:false,
								success :function(msg) {
									rclass.remove();
									$("#"+actionid+"").trigger('click');
									//remove the specific reply
									$(".totalviewcontainer:eq("+i+")").remove();
								}
							});
						}
					});
					}
				});		
				//conversation create
				var moduleid=$('#conversationdivid').attr('data-moduleid');
				var rowid=$('#conversationdivid').attr('data-rowid');
				var appendid=$('#conversationdivid').val();
				$('#postsubmit').click(function(){
					var conversationmsg = $('#homedashboardconvid').val();
					var conversationtt = $('#homedashboardtitle').val();
					var file=$('#cc_filepath').val();
					var filename=$('#cc_filename').val();
					var privacy=$('#c_privacy').val();		
					if(conversationmsg != "") {
						$.ajax({
							url:base_url+'Home/conversationcreate',				
							data:{convmsg:conversationmsg,convtitle:conversationtt,moduleid:moduleid,rowid:rowid,privacy:privacy,file:file,fname:filename},
							type:"POST",
							async:false,
							cache:false,
							success :function(msg) {
								var nmsg = $.trim(msg);
								if(nmsg == true) {
									loadconversationlog(appendid);					
								}
							}
						});
					}
				});
				//conversation editdata
				$('.c_edit').click(function(){
					$('#postsubmit').addClass('hidedisplay');
					$("#editpostsubmit").removeClass('hidedisplay');
					$('#postcancel').removeClass('hidedisplay');
					var c_edit_rowid=$(this).attr("data-rowid");
					if(c_edit_rowid !='' ||c_edit_rowid != null){
						$.ajax({
							url:base_url+"Home/getconversationeditdata",
							data:{rowid:c_edit_rowid},
							type:"POST",
							async:false,
							cache:false,
							dataType:'json',
							success :function(data) {
								//set the data
								 setTimeout(function(){
								$('#homedashboardtitle').val(data.c_title);
								$('#homedashboardconvid').val(data.c_message);
								$('#unique_rowid').val(data.c_rowid);
								if(checkValue(data.c_file) == true){					
								  $('#cc_filepath').val(data.c_file);
								  $('#cc_filename').val(data.c_title);
								  $('#c_fileid').text(data.c_file);
								}
								$('#homedashboardconvid').val(data.c_message);
								},10);
								$('.addtitlestyle').trigger('click');
								
								if(CONVERSATIONPOST == 0){
									 $('#textenterdiv').slideToggle();//slides down the comment overlay
								}
							}
						});
					}
					//scrollbarclass
					$('.c_scroll').animate({scrollTop:0},'slow');	
				});
				//conversation update
				$('#editpostsubmit').click(function(){
				 var appendid=$('#conversationdivid').val();
					var rowid=$('#unique_rowid').val();
					var moduleid=$('#conversationdivid').attr('data-moduleid');
					var conversationmsg = $('#homedashboardconvid').val();
					var conversationtt = $('#homedashboardtitle').val();
					var file=$('#cc_filepath').val();
					var privacy=$('#c_privacy').val();		
					if(rowid !='' ||rowid != null){
						$.ajax({
							url:base_url+"Home/updateconversationdata",
							data:{rowid:rowid,moduleid:moduleid,c_message:conversationmsg,convtitle:conversationtt,privacy:privacy,file:file},
							type:"POST",
							async:false,
							cache:false,						
							success :function(msg) {
								//reload the div
								loadconversationlog(appendid);
							}
						});
					}
				});
				//conversation delete
				$('.c_delete').click(function(){
					var appendid=$('#conversationdivid').val();
					var c_delete_rowid=$(this).attr("data-rowid");
					if(c_delete_rowid !='' ||c_delete_rowid != null){
						$.ajax({
							url:base_url+"Home/conversationdelete",
							data:{rowid:c_delete_rowid},
							type:"POST",
							async:false,
							cache:false,
							success :function(msg) {
								loadconversationlog(appendid);
							}
						});
					}
				});
				
				//conversation cancel-on edit mode
				$('#postcancel').click(function(){	
					//reset the conversation
					$('#removefile').trigger('click');
					$('#homedashboardtitle,#homedashboardconvid').val('');		
					$("#editpostsubmit").addClass('hidedisplay');
					$('#postcancel').addClass('hidedisplay');
					$('#postsubmit').removeClass('hidedisplay');
				});
				$(".addtitlestyle").click(function(){
					$(this).addClass('hidedisplay');
					$('.titleinputspan').removeClass('hidedisplay');
				});
				
				$( ".totalcotainerconv" ).hover(function(){
					$( this ).find('.iconsetconv').css('opacity','1');}, function(){
					$( this ).find('.iconsetconv').css('opacity','0');
				});	
				// 	
				{	//load -data-onscrolls
					$("#conversationwidgetclass").scroll( function() {
						  if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight()) {							  
							loadconversationend('conversationwidgetclass');
						  }
					}); 
				}	
				{// file upload
					$("#convmulitplefileuploader").hide();
					setTimeout(function(){
						$(".ajax-file-upload-container").hide(); 
						$(".ajax-file-upload-filename").hide(); 
					}, 100);
					fileuploadmoname = 'convfileupload';
					var conversationsettings = {
					sequential:true,
					sequentialCount:1,
					showStatusAfterSuccess: false,
					url: base_url+"upload.php",
					method: "POST",
					fileName: "myfile",
					allowedTypes: "*",
					dataType:'json',
					async:false,
					onSuccess:function(files,data,xhr){
						if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
							var arr = jQuery.parseJSON(data);
							var c_filepath=arr.path;
							var c_filename=arr.fname;//convfileupload
							$('.convfileupload').removeClass('hidedisplay');
							$('#cc_filepath').val(c_filepath);
							$('#cc_filename').val(c_filename);
							$('#c_fileid').text(files);
						}else if(data == "Size"){
							alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
						} else if(data == "MaxSize"){
							alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
						} else if(data == "MaxFile"){
							alertpopupdouble("Maximum number fo files exceeded.");
						}else{
							alertpopupdouble("Upload Failed");
						}
					},
					onError: function(files,status,errMsg) {
					}}
					$("#convmulitplefileuploader").uploadFile(conversationsettings);
					$(".uploadattachments").click(function(e){
						e.preventDefault(); 
						$(".ajax-file-upload>form>input:first").trigger('click');
						e.stopPropagation();
						return false;
					});
					{//file upload removal-
						$('#removefile').click(function(){	
							$('#c_fileid').text('');
							$('.convfileupload').addClass('hidedisplay');
							$('#cc_filepath').val('');
						});
					}
				}
				
				$('.mention').smention(""+base_url+""+"Home/getuser",{
					after : " "
				});			
			},100); 
		});	
	 }	

}
{// Widget Creation Announcement
	function announcementwidgetcreation(widgettitle,appendid,appendclass,data) { 
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Add" class="widgetredirecticonannouncement"><i class="material-icons">add</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 20rem"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div></div>');		
		for(var i=0; i<data;i++) {
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns announcementnamestyle ">Announcement given from admin about the sales increse '+i+'</div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">12-10-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
	}
}
{// Widget Creation Notes Editor
	function noteswidgetcreation(widgettitle,appendid,appendclass,data) { 
		$("#"+appendid+"").append('<div class="large-12 columns" style="border:1px solid #cccccc;background:#ffffff"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right"><span id="notesaddicon" title="Add" class="widgetredirecticonnotes"><i class="material-icons">add</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><span class="large-12 columns hidedisplay paddingzero" id="notesenterdiv"><div class="large-12 columns"> &nbsp;</div><div class="large-12 columns" style="padding-right:0"><div class="large-12"><label>Title</label><input id="" class="" type="text" name=""></div><div class="large-12 columns">&nbsp;</div><div class="large-12 column paddingzero"><textarea class="messagetoenterstyle"> Enter Your Message Here...</textarea></div></div></span></div></div></div>');
		for(var i=0; i<data;i++) {
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-10 medium-10 small-10 columns paddingzero"><div class="large-12 columns tasknamestyle ">Notes Title : My Title '+i+'</div><div class="large-12 columns taskassignnamestyle">My notes for the lead'+i+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">2014-12-1'+i+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		}
		$("#notesaddicon").click(function(){
			$('#notesenterdiv').slideToggle();
		});

	}
}
{// Widget Creation Task
	function taskwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,tasktoolbar,statusdata,assignempdata) {
		var datacheck="";
		var datalist = "";
		task_filter_status = 0;
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="todolist-card"><div class="todolist-header"><span class="todolist-title">Task List</span><span class="todolist-button">ADD</span></div><div class="todolist-content" id="append'+appendclass+'"><div></div></div></div><input type="hidden" id="lasttaskid"></input><div class="large-12 paddingzero columns todolist-footer"><span style="display:inline-block"><span style="display:inline-block;background-color: #929292;color:#fff;position:relative;padding:5px;top:10px;left:10px;font-size:0.9rem;margin-right:5px;border-radius: 2%;">'+statusdata['notstrated']+' Left</span><span style="display:inline-block;background-color:#546e7a;color:#fff;position:relative;padding:5px;top:10px;left:10px;font-size:0.9rem;border-radius: 2%;">Completed('+statusdata['completed']+')</span></span></div><div class="large-12 columns text-boxcontainer card-reveal"><div class="card-reveal-header">Create Task</div><div id="taskcreatewidget" class="validationEngineContainer"><div class="input-field large-12 medium-6  small-6 columns"><input name="todotextmessage" id="todotextmessage" placeholder="" class="validate[required,maxSize[100]]" tabindex="-1" type="text"><label for="todotextmessage" class="">Task Name<span class="mandatoryfildclass">*</span></label></div><div class="input-field large-6 medium-6  small-6 columns"><input calss="validate[required]" type="text" data-dateformater="dd-mm-yy" id="taskdate" name="taskdate"><label for="taskdate" class="">Due Date<span class="mandatoryfildclass">*</span></label></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Priority<span class="mandatoryfildclass">*</span></label><select id="taskpriority" class="chzn-select validate[required]" name="taskpriority" tabindex="-1"><option value="select">Select</option><option value="5">High</option><option value="6">Highest</option><option value="7">Low</option><option value="8">Lowest</option><option selected=selected value="9">Normal</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Status<span class="mandatoryfildclass">*</span></label><select id="taskstatus" class="chzn-select validate[required]" name="taskstatus" tabindex="-1"><option value="select">Select</option><option selected=selected value="64" data-crmstatusidhidden="Not Started">Not Started</option><option value="65" data-crmstatusidhidden="Deferred">Deferred</option><option value="66" data-crmstatusidhidden="In Progress">In Progress</option><option value="67" data-crmstatusidhidden="Completed">Completed</option><option value="68" data-crmstatusidhidden="Waiting on Someone else">Waiting on Someone else</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Assignto<span class="mandatoryfildclass">*</span></label><select id="taskassignto" class="chzn-select validate[required]" name="taskassignto" tabindex="-1"><option value="select">Select</option></select><input type="hidden" name="taskemployeeid" id="taskemployeeid" value=""><input type="hidden" name="taskemployeetypeid" id="taskemployeetypeid" value=""></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Lead/Contact Type</label><select id="taskleadcontacttypeid" class="chzn-select" name="taskleadcontacttypeid" tabindex="-1"><option value="select">Select</option><option value="2">Contact</option><option value="3">Lead</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Name</label><select id="taskcomminid" class="chzn-select" name="taskcomminid" tabindex="-1"><option value="select">Select</option></select></div><input type="hidden" name="taskeditid" id="taskeditid" value="" /></div><span id="tasksubmit" class="todolist-submit">Submit</span><span id="taskupdate" class="hidedisplay todolist-submit">Submit</span><span class="card-reveal-close"><i class="material-icons" title="close">close</i></span></div></div>');
		//initial data loading call----
		var fieldlength = data.length;		
		if(fieldlength > 0){
			taskwidgetcontent('append'+appendclass+'',data,tasktoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#taskmore').empty();
		}
		//For Icon Hover 
		$( ".icondisplay" ).hover(function() {
				$(this).find('.iconset').removeClass('hidedisplay');
			}, function() {
				$(this).find('.iconset').addClass('hidedisplay');
		});
		empgroupdrodownset('taskassignto',assignempdata);
		/*Load the events after loadings*/
		$(function() {
			//For Dropdown
			$(".chzn-select").select2({containerCss: {display: "block"}});
			$('.todolist-button').click(function(){
				$('.text-boxcontainer').css('transform', 'translateY(-100%)');
				$("#taskassignto").select('val',1+':'+loggedinuser);
				var taskeditid = $("#taskeditid").val();
				if(taskeditid !='') {
					$("#tasksubmit").removeClass('hidedisplay');
					$("#taskupdate").addClass('hidedisplay');
				} else{
					$("#tasksubmit").addClass('hidedisplay');
					$("#taskupdate").removeClass('hidedisplay');
				}
			});
			$('.card-reveal-close').click(function(){
				$('.text-boxcontainer').css('transform', 'translateY(0%)');
			});
			$(".checkboxcls").click(function(){
				var checktaskid=$(this).val();
				var taskstatus = '';
				if ($(this).is(':checked')) {
					var taskstatus = 'Yes';
					$(this).next('label').css( "text-decoration", "line-through" );
				} else {
					var taskstatus = 'No';
					$(this).next('label').removeAttr('style');
				}
				changestatusbasedoncheckbox(checktaskid,taskstatus,'crmtask');
			});
			//On lead contact change
			$("#taskleadcontacttypeid").change(function(){
				var val = $(this).val();
				if(val == 2){
					appendleadcontact(val,'taskcomminid');
				}else if(val == 3){
					appendleadcontact(val,'taskcomminid');
				}
			});
			$("#taskdateicon").click(function(){
				$("#taskdate").trigger('click');
			});
			var dateformetdata = $("#taskdateicon").attr("data-dateformater");
			$("#taskdate").datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					var checkdate = $(this).val();
					$("#taskdate").val(checkdate);
				},
				onClose: function () {
					$("#taskdate").focus();
				}
			}).click(function() {
				$(this).datetimepicker("show");
			});
			//task submit 
			$("#tasksubmit").click(function(){
				$("#taskcreatewidget").validationEngine("validate");
			});
			$("#taskcreatewidget").validationEngine({
				onSuccess: function() {
					var taskname = $("#todotextmessage").val();
					var taskdate = $("#taskdate").val();
					var priority = $("#taskpriority").val();
					var status = $("#taskstatus").val();
					var assignto = $("#taskassignto").val();
					var leadcontacttypeid = $("#taskleadcontacttypeid").val();
					var taskcomminid = $("#taskcomminid").val();
					var taskeditid = $("#taskeditid").val();
					quickcreatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,'crmtask');
					var rdata=getmoreoutput('crmtask','','','',dashboardwidgetid,dashboardrowid,0,'');
					if(rdata.length > 0){
						taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
					}
					$('.card-reveal-close').trigger('click');
				},
				onFailure: function() {
					var dropdownid =['1','taskpriority'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			$("#taskupdate").click(function(){
				$("#taskcreatewidget").validationEngine("validate");
			});
			$("#taskcreatewidget").validationEngine({
				onSuccess: function() {
					var taskname = $("#todotextmessage").val();
					var taskdate = $("#taskdate").val();
					var priority = $("#taskpriority").val();
					var status = $("#taskstatus").val();
					var assignto = $("#taskassignto").val();
					var leadcontacttypeid = $("#taskleadcontacttypeid").val();
					var taskcomminid = $("#taskcomminid").val();
					var taskeditid = $("#taskeditid").val();
					quickupdatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,taskeditid,'crmtask');
					var rdata=getmoreoutput('crmtask','','','',dashboardwidgetid,dashboardrowid,0,'');
					if(rdata.length > 0){
						taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
					}
					$('.card-reveal-close').trigger('click');
				},
				onFailure: function() {
					var dropdownid =['1','taskpriority'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			$('#taskpriority').dropdown({
			      inDuration: 300,
			      outDuration: 225,
			      constrain_width: false,
			      hover: false,
			      gutter: 0,
			      belowOrigin: false,
			      alignment: 'left'
			    }
			  );
			setTimeout(function(){
				//Load Options on uitype
				$("#taskfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('taskfields',206,'tasktextdiv','taskdddiv','taskdatediv','taskfilterselect');
					}		
				});	
				//more append activity data-
				$("#taskmore").click(function(){
					var task_id=$('#lasttaskid').val();
					if(task_id > 0){
						var uitype=$("#taskfields").find('option:selected').data('uitype');					
						var columnfield=$("#taskfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#taskfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#taskfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#taskfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#taskfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('crmtask',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,task_id,uitype);
						if(rdata.length > 0){
							taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('taskfields') == true ){
						var uitype=$("#taskfields").find('option:selected').data('uitype');					
						var columnfield=$("#taskfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#taskfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#taskfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#taskfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#taskfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('crmtask',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
						} else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#taskfilterbtn"+id+"").trigger('click');
						$("#taskfiltertext,#taskfilterdate").val('');
						$("#taskfields,#taskfilterselect").select2('val','').trigger('change');
						$("#taskfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});
				//For Filter 
				$( "#taskfilterbtn"+id+"" ).click(function(){
					$( "#taskfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(task_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load task fields
							loadfilterfield('taskfields',206,'crmtask');
							task_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				//For Add
				$(".widgetredirecticontask").click(function(){
					{// For Redirection Task Add
						sessionStorage.setItem("taskaddsrc",'formhome');
						sessionStorage.setItem("widgetid",10);
						sessionStorage.setItem("moduleid",moduleid);
						sessionStorage.setItem("rowid",dashboardrowid);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Reload
				$("#taskreloadbtn"+id+"").click(function(){
					$('#'+reloadid+'').trigger('click');
				});
				
				//For View 
				$( ".widgetviewredirecticon" ).click(function(){
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});				
				// Date validation
				var dateformetdata = $('#taskfilterdate').attr('data-dateformater');
				$('#taskfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval);
		});
		/*End of Events*/		
	}
	// task - mini
	function taskwidgetcreationmini(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,tasktoolbar,statusdata,assignempdata) {
		var datacheck="";
		var datalist = "";
		task_filter_status = 0;
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="todolist-card"><div class="todolist-header"><span class="todolist-title">Task List</span><span class="todolist-button">ADD</span></div><div class="todolist-content" id="append'+appendclass+'"><div></div></div></div><input type="hidden" id="lasttaskid"></input><div class="large-12 paddingzero columns todolist-footer"><span style="display:inline-block"><span style="display:inline-block;background-color: #929292;color:#fff;position:relative;padding:5px;top:10px;left:10px;font-size:0.9rem;margin-right:5px;border-radius: 2%;">'+statusdata['notstrated']+' Left</span><span style="display:inline-block;background-color:#546e7a;color:#fff;position:relative;padding:5px;top:10px;left:10px;font-size:0.9rem;border-radius: 2%;">Completed('+statusdata['completed']+')</span></span></div><div class="large-12 columns text-boxcontainer card-reveal"><div class="card-reveal-header">Create Task</div><div id="taskcreatewidget" class="validationEngineContainer"><div class="input-field large-12 medium-6  small-6 columns"><input name="todotextmessage" id="todotextmessage" placeholder="" class="validate[required,maxSize[100]]" tabindex="-1" type="text"><label for="todotextmessage" class="">Task Name<span class="mandatoryfildclass">*</span></label></div><div class="input-field large-6 medium-6  small-6 columns"><input calss="validate[required]" type="text" data-dateformater="dd-mm-yy" id="taskdate" name="taskdate"><label for="taskdate" class="">Due Date<span class="mandatoryfildclass">*</span></label></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Priority<span class="mandatoryfildclass">*</span></label><select id="taskpriority" class="chzn-select validate[required]" name="taskpriority" tabindex="-1"><option value="select">Select</option><option value="5">High</option><option value="6">Highest</option><option value="7">Low</option><option value="8">Lowest</option><option selected=selected value="9">Normal</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Status<span class="mandatoryfildclass">*</span></label><select id="taskstatus" class="chzn-select validate[required]" name="taskstatus" tabindex="-1"><option value="select">Select</option><option selected=selected value="64" data-crmstatusidhidden="Not Started">Not Started</option><option value="65" data-crmstatusidhidden="Deferred">Deferred</option><option value="66" data-crmstatusidhidden="In Progress">In Progress</option><option value="67" data-crmstatusidhidden="Completed">Completed</option><option value="68" data-crmstatusidhidden="Waiting on Someone else">Waiting on Someone else</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Assignto<span class="mandatoryfildclass">*</span></label><select id="taskassignto" class="chzn-select validate[required]" name="taskassignto" tabindex="-1"><option value="select">Select</option></select><input type="hidden" name="taskemployeeid" id="taskemployeeid" value=""><input type="hidden" name="taskemployeetypeid" id="taskemployeetypeid" value=""></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Lead/Contact Type</label><select id="taskleadcontacttypeid" class="chzn-select" name="taskleadcontacttypeid" tabindex="-1"><option value="select">Select</option><option value="2">Contact</option><option value="3">Lead</option></select></div><div class="static-field  large-6 medium-6 small-6 columns"><label>Name</label><select id="taskcomminid" class="chzn-select" name="taskcomminid" tabindex="-1"><option value="select">Select</option></select></div><input type="hidden" name="taskeditid" id="taskeditid" value="" /></div><span id="tasksubmit" class="todolist-submit">Submit</span><span id="taskupdate" class="hidedisplay todolist-submit">Submit</span><span class="card-reveal-close"><i class="material-icons" title="close">close</i></span></div></div>');
		//initial data loading call----
		var fieldlength = data.length;		
		if(fieldlength > 0){
			taskwidgetcontent('append'+appendclass+'',data,tasktoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#taskmore').empty();
		}
		//For Icon Hover 
		$( ".icondisplay" ).hover(function() {
				$(this).find('.iconset').removeClass('hidedisplay');
			}, function() {
				$(this).find('.iconset').addClass('hidedisplay');
		});
		empgroupdrodownset('taskassignto',assignempdata);
		/*Load the events after loadings*/
		$(function() {
			//For Dropdown
			$(".chzn-select").select2({containerCss: {display: "block"}});
			$('.todolist-button').click(function(){
				$('.text-boxcontainer').css('transform', 'translateY(-100%)');
				$("#taskassignto").select('val',1+':'+loggedinuser);
				var taskeditid = $("#taskeditid").val();
				if(taskeditid !='') {
					$("#tasksubmit").removeClass('hidedisplay');
					$("#taskupdate").addClass('hidedisplay');
				} else{
					$("#tasksubmit").addClass('hidedisplay');
					$("#taskupdate").removeClass('hidedisplay');
				}
			});
			$('.card-reveal-close').click(function(){
				$('.text-boxcontainer').css('transform', 'translateY(0%)');
			});
			$(".checkboxcls").click(function(){
				var checktaskid=$(this).val();
				var taskstatus = '';
				if ($(this).is(':checked')) {
					var taskstatus = 'Yes';
					$(this).next('label').css( "text-decoration", "line-through" );
				} else {
					var taskstatus = 'No';
					$(this).next('label').removeAttr('style');
				}
				changestatusbasedoncheckbox(checktaskid,taskstatus,'crmtask');
			});
			//On lead contact change
			$("#taskleadcontacttypeid").change(function(){
				var val = $(this).val();
				if(val == 2){
					appendleadcontact(val,'taskcomminid');
				}else if(val == 3){
					appendleadcontact(val,'taskcomminid');
				}
			});
			$("#taskdateicon").click(function(){
				$("#taskdate").trigger('click');
			});
			var dateformetdata = $("#taskdateicon").attr("data-dateformater");
			$("#taskdate").datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					var checkdate = $(this).val();
					$("#taskdate").val(checkdate);
				},
				onClose: function () {
					$("#taskdate").focus();
				}
			}).click(function() {
				$(this).datetimepicker("show");
			});
			//task submit 
			$("#tasksubmit").click(function(){
				$("#taskcreatewidget").validationEngine("validate");
			});
			$("#taskcreatewidget").validationEngine({
				onSuccess: function() {
					var taskname = $("#todotextmessage").val();
					var taskdate = $("#taskdate").val();
					var priority = $("#taskpriority").val();
					var status = $("#taskstatus").val();
					var assignto = $("#taskassignto").val();
					var leadcontacttypeid = $("#taskleadcontacttypeid").val();
					var taskcomminid = $("#taskcomminid").val();
					var taskeditid = $("#taskeditid").val();
					quickcreatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,'crmtask');
					var rdata=getmoreoutput('crmtask','','','',dashboardwidgetid,dashboardrowid,0,'');
					if(rdata.length > 0){
						taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
					}
					$('.card-reveal-close').trigger('click');
				},
				onFailure: function() {
					var dropdownid =['1','taskpriority'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			$("#taskupdate").click(function(){
				$("#taskcreatewidget").validationEngine("validate");
			});
			$("#taskcreatewidget").validationEngine({
				onSuccess: function() {
					var taskname = $("#todotextmessage").val();
					var taskdate = $("#taskdate").val();
					var priority = $("#taskpriority").val();
					var status = $("#taskstatus").val();
					var assignto = $("#taskassignto").val();
					var leadcontacttypeid = $("#taskleadcontacttypeid").val();
					var taskcomminid = $("#taskcomminid").val();
					var taskeditid = $("#taskeditid").val();
					quickupdatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,taskeditid,'crmtask');
					var rdata=getmoreoutput('crmtask','','','',dashboardwidgetid,dashboardrowid,0,'');
					if(rdata.length > 0){
						taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
					}
					$('.card-reveal-close').trigger('click');
				},
				onFailure: function() {
					var dropdownid =['1','taskpriority'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			$('#taskpriority').dropdown({
			      inDuration: 300,
			      outDuration: 225,
			      constrain_width: false,
			      hover: false,
			      gutter: 0,
			      belowOrigin: false,
			      alignment: 'left'
			    }
			  );
			setTimeout(function(){
				//Load Options on uitype
				$("#taskfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('taskfields',206,'tasktextdiv','taskdddiv','taskdatediv','taskfilterselect');
					}		
				});	
				//more append activity data-
				$("#taskmore").click(function(){
					var task_id=$('#lasttaskid').val();
					if(task_id > 0){
						var uitype=$("#taskfields").find('option:selected').data('uitype');					
						var columnfield=$("#taskfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#taskfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#taskfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#taskfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#taskfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('crmtask',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,task_id,uitype);
						if(rdata.length > 0){
							taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('taskfields') == true ){
						var uitype=$("#taskfields").find('option:selected').data('uitype');					
						var columnfield=$("#taskfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#taskfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#taskfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#taskfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#taskfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('crmtask',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							taskwidgetcontent('append'+appendclass+'',rdata,tasktoolbar,reloadid);					
						} else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#taskfilterbtn"+id+"").trigger('click');
						$("#taskfiltertext,#taskfilterdate").val('');
						$("#taskfields,#taskfilterselect").select2('val','').trigger('change');
						$("#taskfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});
				//For Filter 
				$( "#taskfilterbtn"+id+"" ).click(function(){
					$( "#taskfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(task_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load task fields
							loadfilterfield('taskfields',206,'crmtask');
							task_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				//For Add
				$(".widgetredirecticontask").click(function(){
					{// For Redirection Task Add
						sessionStorage.setItem("taskaddsrc",'formhome');
						sessionStorage.setItem("widgetid",10);
						sessionStorage.setItem("moduleid",moduleid);
						sessionStorage.setItem("rowid",dashboardrowid);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Reload
				$("#taskreloadbtn"+id+"").click(function(){
					$('#'+reloadid+'').trigger('click');
				});
				
				//For View 
				$( ".widgetviewredirecticon" ).click(function(){
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});				
				// Date validation
				var dateformetdata = $('#taskfilterdate').attr('data-dateformater');
				$('#taskfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval);
		});
		/*End of Events*/		
	}	
}
{// Widget Creation Documents
	function documentswidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,doctoolbar) {
		var datacheck="";
		var datalist = "";
		document_filter_status =0;
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Document">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+doctoolbar[2]+'"><span title="Add" class="widgetredirecticondocument widgetviewicon" data-redirecturl="Document"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="documentsfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="documentsfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="documentsfields" class="chzn-select"></select></div><div class="large-6 columns" id="documentstextdiv"><label>Filter Value</label><input type="text" id="documentsfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="documentsdddiv"><label>Filter Value</label><select id="documentsfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="documentsdatediv"><label>Filter Value</label><input type="text" id="documentsfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="documentmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastdocumentid"></input></span></div>');		
		//load Documents fields	
		//initial data loading call documentsreloadbtn
		var fieldlength = data.length;		
		if(fieldlength > 0){
			docmentswidgetcontent('append'+appendclass+'',data,doctoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#documentmore').empty();
		}		
		/*Load the events after loadings*/
		$(function() {
			setTimeout(function(){
				$("#documentmore").click(function(){
				//$('.chzn-select').select2();
				var document_id=$('#lastdocumentid').val();
				if(document_id > 0){
					var uitype=$("#documentsfields").find('option:selected').data('uitype');					
					var columnfield=$("#documentsfields").find('option:selected').data('dbcolumn');
					var columnfieldtwo=$("#documentsfields").find('option:selected').data('viewcreationjoincolmodelname');
					var clause='';				
					if(uitype == 8){ //date	
						var filtervalue=$('#documentsfilterdate').val();
						var col_field=columnfield;
						var clause='like';
					}
					else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
						var filtervalue=$('#documentsfilterselect').val();	
						var col_field=columnfieldtwo;
					}
					else { //text box	
						var filtervalue=$('#documentsfiltertext').val();
						var col_field=columnfield;
						var clause='like';
					}				
					//retrieve filter data
					var rdata=getmoreoutput('crmfileinfo',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,document_id,uitype);
					if(rdata.length > 0){
						docmentswidgetcontent('append'+appendclass+'',rdata,doctoolbar,reloadid);					
					}
				}
			});
			//For Reload
				$("#documentsreloadbtn"+id+"").click(function(){
					$('#'+reloadid+'').trigger('click');
				});
			//Filter submit results
		$("#filtersubmit"+id+"").click(function(){
			if(checkVariable('documentsfields') == true ){
				var uitype=$("#documentsfields").find('option:selected').data('uitype');					
				var columnfield=$("#documentsfields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#documentsfields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#documentsfilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#documentsfilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#documentsfiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getfilteroutput('crmfileinfo',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
				$('#append'+appendclass+'').empty();
				if(rdata.length > 0){
					docmentswidgetcontent('append'+appendclass+'',rdata,doctoolbar,reloadid);					
				} else {
					//append empty set
					$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
				}
				$("#documentsfilterbtn"+id+"").trigger('click');
				$("#documentsfiltertext,#documentsfilterdate").val('');
				$("#documentsfields,#documentsfilterselect").select2('val','').trigger('change');
				$("#documentsfilterselect").empty();
			}
			else{
				alertpopup('Please Give All Filter Values');
			}
		});
			//For Filter 
			$( "#documentsfilterbtn"+id+"" ).click(function(){
				$( "#documentsfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
				{//load filter fields
					if(document_filter_status == 0){
						//For Dropdown
						$(".chzn-select").select2({containerCss: {display: "block"}});
						loadfilterfield('documentsfields',207,'crmfileinfo');
						document_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
					}
				}
			});
			//Load Options on uitype
			$("#documentsfields").change(function(){
				var value=$(this).val();
				if(checkValue(value) == true){			
					filtervalueload('documentsfields',207,'documentstextdiv','documentsdddiv','documentsdatediv','documentsfilterselect');
				}		
			});
			// Date validation
			var dateformetdata = $('#documentsfilterdate').attr('data-dateformater');
			$('#documentsfilterdate').datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					$(this).removeClass('error');
				}
			});
			},interval);				
		});
		//For Add 
		$(".widgetredirecticondocument").click(function(){
			{// For Redirection Sales order Add
				sessionStorage.setItem("documentaddsrc",'formhome');
				sessionStorage.setItem("widgetid",11);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
	}
	// mini document
	function documentswidgetcreationmini(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,doctoolbar) {
		var datacheck="";
		var datalist = "";
		document_filter_status =0;
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Document">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+doctoolbar[2]+'"><span title="Add" class="widgetredirecticondocument widgetviewicon" data-redirecturl="Document"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="documentsfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div id="'+appendclass+'" class="large-12 columns scrollbarclass widgetwrapper" style="height: 16rem;overflow-x:hidden"><div id="documentsfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="documentsfields" class="chzn-select"></select></div><div class="large-6 columns" id="documentstextdiv"><label>Filter Value</label><input type="text" id="documentsfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="documentsdddiv"><label>Filter Value</label><select id="documentsfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="documentsdatediv"><label>Filter Value</label><input type="text" id="documentsfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="hai"></ul></div><span id="documentmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastdocumentid"></input></span></div>');		
		//load Documents fields	
		//initial data loading call documentsreloadbtn
		var fieldlength = data.length;		
		if(fieldlength > 0){
			docmentswidgetcontent('append'+appendclass+'',data,doctoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#documentmore').empty();
		}		
		/*Load the events after loadings*/
		$(function() {
			setTimeout(function(){
				$("#documentmore").click(function(){
				//$('.chzn-select').select2();
				var document_id=$('#lastdocumentid').val();
				if(document_id > 0){
					var uitype=$("#documentsfields").find('option:selected').data('uitype');					
					var columnfield=$("#documentsfields").find('option:selected').data('dbcolumn');
					var columnfieldtwo=$("#documentsfields").find('option:selected').data('viewcreationjoincolmodelname');
					var clause='';				
					if(uitype == 8){ //date	
						var filtervalue=$('#documentsfilterdate').val();
						var col_field=columnfield;
						var clause='like';
					}
					else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
						var filtervalue=$('#documentsfilterselect').val();	
						var col_field=columnfieldtwo;
					}
					else { //text box	
						var filtervalue=$('#documentsfiltertext').val();
						var col_field=columnfield;
						var clause='like';
					}				
					//retrieve filter data
					var rdata=getmoreoutput('crmfileinfo',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,document_id,uitype);
					if(rdata.length > 0){
						docmentswidgetcontent('append'+appendclass+'',rdata,doctoolbar,reloadid);					
					}
				}
			});
			//For Reload
				$("#documentsreloadbtn"+id+"").click(function(){
					$('#'+reloadid+'').trigger('click');
				});
			//Filter submit results
		$("#filtersubmit"+id+"").click(function(){
			if(checkVariable('documentsfields') == true ){
				var uitype=$("#documentsfields").find('option:selected').data('uitype');					
				var columnfield=$("#documentsfields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#documentsfields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#documentsfilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#documentsfilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#documentsfiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getfilteroutput('crmfileinfo',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
				$('#append'+appendclass+'').empty();
				if(rdata.length > 0){
					docmentswidgetcontent('append'+appendclass+'',rdata,doctoolbar,reloadid);					
				} else {
					//append empty set
					$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
				}
				$("#documentsfilterbtn"+id+"").trigger('click');
				$("#documentsfiltertext,#documentsfilterdate").val('');
				$("#documentsfields,#documentsfilterselect").select2('val','').trigger('change');
				$("#documentsfilterselect").empty();
			}
			else{
				alertpopup('Please Give All Filter Values');
			}
		});
			//For Filter 
			$( "#documentsfilterbtn"+id+"" ).click(function(){
				$( "#documentsfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
				{//load filter fields
					if(document_filter_status == 0){
						//For Dropdown
						$(".chzn-select").select2({containerCss: {display: "block"}});
						loadfilterfield('documentsfields',207,'crmfileinfo');
						document_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
					}
				}
			});
			//Load Options on uitype
			$("#documentsfields").change(function(){
				var value=$(this).val();
				if(checkValue(value) == true){			
					filtervalueload('documentsfields',207,'documentstextdiv','documentsdddiv','documentsdatediv','documentsfilterselect');
				}		
			});
			// Date validation
			var dateformetdata = $('#documentsfilterdate').attr('data-dateformater');
			$('#documentsfilterdate').datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					$(this).removeClass('error');
				}
			});
			},interval);				
		});
		//For Add 
		$(".widgetredirecticondocument").click(function(){
			{// For Redirection Sales order Add
				sessionStorage.setItem("documentaddsrc",'formhome');
				sessionStorage.setItem("widgetid",11);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
	}	
}
{// Widget Creation Activities
	function activitieswidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,activitytoolbar) {
		var datacheck="";
		var datalist = "";
		act_filter_status=0;
		$("#"+appendid+"").html('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Activity">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+activitytoolbar[2]+'"><span title="Add" class="widgetredirecticonactivities widgetviewicon" data-redirecturl="Activity"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="activityfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="activityfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="activityfields" class="chzn-select"></select></div><div class="large-6 columns" id="activitytextdiv"><label>Filter Value</label><input type="text" id="activityfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="activitydddiv"><label>Filter Value</label><select id="activityfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="activitydatediv"><label>Filter Value</label><input type="text" id="activityfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="activitymore" class="widgetmorestyle" style="">More<input type="hidden" id="lastactivityid"></input></span>');	
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			activitywidgetcontent('append'+appendclass+'',data,activitytoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#activitymore').empty();
		}
		//For Add 
		$(".widgetredirecticonactivities").click(function(){
			{// For Redirection Activity Add
				sessionStorage.setItem("activityaddsrc",'formhome');
				sessionStorage.setItem("widgetid",12);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#activityreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		/*Load the events after loadings*/
		$(function() {
			setTimeout(function(){
				//more append activity data-
				$("#activitymore").click(function(){
					var activity_id=$('#lastactivityid').val();
					if(activity_id > 0){
						//getmoreoutput
						var uitype=$("#activityfields").find('option:selected').data('uitype');					
						var columnfield=$("#activityfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#activityfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#activityfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#activityfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#activityfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('crmactivity',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,activity_id,uitype);
						if(rdata.length > 0){
							activitywidgetcontent('append'+appendclass+'',rdata,activitytoolbar,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#activityfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){	filtervalueload('activityfields',205,'activitytextdiv','activitydddiv','activitydatediv','activityfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('activityfields') == true ){
						var uitype=$("#activityfields").find('option:selected').data('uitype');					
						var columnfield=$("#activityfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#activityfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#activityfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#activityfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#activityfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('crmactivity',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							activitywidgetcontent('append'+appendclass+'',rdata,activitytoolbar,reloadid);					
						}else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#activityfilterbtn"+id+"").trigger('click');
						$("#activityfiltertext,#activityfilterdate").val('');
						$("#activityfields,#activityfilterselect").select2('val','').trigger('change');
						$("#activityfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					} 
				});
				//For Filter 
				$( "#activityfilterbtn"+id+"" ).click(function(){
					$( "#activityfiltercontainer" ).slideToggle( "slow", function() { });
					{//load filter fields
						if(act_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load Activity fields
							loadfilterfield('activityfields',205,'crmactivity');
							act_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#accountfilterdate').attr('data-dateformater');
				$('#activityfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval);
		}); 
	}
}
{
	function customwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,customeditfields,toolbaraction) {
		var canedit = toolbaraction['3'];
		$("#"+appendid+"").html('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns"  style="text-align:right;"></div></div><div class="large-12 columns paddingzero validationEngineContainer scrollbarclass"  style="height: 16rem;overflow-y:scroll"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div>');		
		var appendcustomdata="";
		var ccustuomeditfields = "";
		var inlineedit = '';
		var k=0;
        $.each(data[0], function( index, value ) {
           if(checkValue(value)==false){
              value=''
           }
           var nvalue = data[1][index].split(',');
           var require = '';
           if(nvalue[3] == 2) {
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,maxSize[100]]';
               } else {
            	   var require = 'validate[maxSize[100]]';
               }
           } else if(nvalue[3] == 4){
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,custom[integer]]';
               } else {
            	   var require = 'validate[custom[integer]]';
               }
           } else if(nvalue[3] == 5){
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,custom[number],decval[3],maxSize[100]]';
               } else {
            	   var require = 'validate[custom[number],decval[3],maxSize[100]]';
               }
           } else if(nvalue[3] == 10){
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,custom[multimail]]';
               } else {
            	   var require = 'validate[custom[multimail]]';
               }
           } else if(nvalue[3] == 11){
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,custom[phone]]';
               } else {
            	   var require = 'validate[custom[phone]]';
               }
           } else if(nvalue[3] == 12){
        	   if(nvalue[7] == 'Yes') {
            	   var require = 'validate[required,custom[csturl],maxSize[100]]';
               } else {
            	   var require = 'validate[custom[csturl],maxSize[100]]';
               }
           }
           //custom widget inline edit operation 
           if(nvalue[3] == 8 ) { //date
        	   var cclass = 'custominlineedit';
        	   if(canedit == 'hidedisplay') {
        		   inlineedit = "</span>";
        	   }else{
        		   inlineedit = '<span data-id="dbinline'+nvalue[1]+'" data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" data-dataval="'+value+'" style="font-size:0.7rem;margin-left:5px;display:inline-block;position:absolute;opacity:0;cursor:pointer;" class="'+cclass+' customedit '+canedit+'">Edit</span></span>';
        	   }
        	   ccustuomeditfields = '<div class="hidedisplay custominlineedit validationEngineContainer" data-nname='+nvalue[4]+' id="custominlineedit'+nvalue[4]+'"><input type="text" name="dbinline'+nvalue[4]+'" id="dbinline'+nvalue[4]+'" data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-id="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" value="'+value+'" class="fordatepicicon" data-dateformater="dd-mm-yy"/><span class="inlineeditsubmit" style="color:#546e7a;cursor:pointer;padding-right:15px;font-size:0.8rem;" data-uitype="'+nvalue[3]+'" data-nname='+nvalue[4]+' id="inlineeditsubmit'+nvalue[4]+'" data-id="dbinline'+nvalue[4]+'">Submit</span><span class="inlineeditcancel" style="color:#546e7a;cursor:pointer;font-size:0.8rem;" data-uitype="'+nvalue[3]+'" id="inlineeditcancel'+nvalue[4]+'" data-id="dbinline'+nvalue[4]+'" data-id1="'+nvalue[4]+'">Cancel</span></div>';
           } else if(nvalue[3] == 17 || nvalue[3] == 20 ||nvalue[3] == 19 || nvalue[3] == 18 || nvalue[3] == 21 || nvalue[3] == 25 || nvalue[3] == 26 || nvalue[3] == 28 || nvalue[3] == 29) { //drop down
        	   var cclass = 'custominlineedit';
        	   if(canedit == 'hidedisplay') {
        		   inlineedit = "</span>";
        	   }else{
        		   inlineedit = '<span data-id="dbinline'+nvalue[1]+'" data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" data-dataval="'+value+'" style="font-size:0.7rem;margin-left:5px;display:inline-block;position:absolute;opacity:0;cursor:pointer;" class="'+cclass+' customedit">Edit</span></span>';
        	   }
        	   ccustuomeditfields = '<div class="hidedisplay custominlineedit validationEngineContainer" data-nname='+nvalue[4]+' id="custominlineedit'+nvalue[4]+'"><select name="dbinline'+nvalue[4]+'" id="dbinline'+nvalue[4]+'" style="height:1.5rem;padding:0px;"data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-id="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" value="'+value+'" class="chzn-select '+require+'"></select><span class="inlineeditsubmit" style="color:#546e7a;cursor:pointer;padding-right:15px;font-size:0.8rem;" data-nname='+nvalue[4]+' id="inlineeditsubmit'+nvalue[4]+'" data-uitype="'+nvalue[3]+'" data-id="dbinline'+nvalue[4]+'">Submit</span><span class="inlineeditcancel" style="color:#546e7a;cursor:pointer;font-size:0.8rem;"data-uitype="'+nvalue[3]+'" id="inlineeditcancel'+nvalue[4]+'" data-id="dbinline'+nvalue[4]+'" data-id1="'+nvalue[4]+'">Cancel</span></div>';
           } else if(nvalue[3] == 2 || nvalue[3] == 3 || nvalue[3] == 4 || nvalue[3] == 5 || nvalue[3] == 6 || nvalue[3] == 7 || nvalue[3] == 10 || nvalue[3] == 11 || nvalue[3] == 12 || nvalue[3] == 30){ //text box
        	   var cclass = 'custominlineedit';
        	   if(canedit == 'hidedisplay') {
        		   inlineedit = "</span>";
        	   }else{
        		   inlineedit = '<span data-id="dbinline'+nvalue[1]+'" data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" data-dataval="'+value+'" style="font-size:0.7rem;margin-left:5px;display:inline-block;position:absolute;opacity:0;cursor:pointer;" class="'+cclass+' customedit '+canedit+'">Edit</span></span>';
        	   }
        	   ccustuomeditfields = '<div class="hidedisplay custominlineedit validationEngineContainer" data-nname='+nvalue[4]+' id="custominlineedit'+nvalue[4]+'"><input class="'+require+'" style="color:#222222; font-size:0.8rem;word-wrap: break-word; width:100%;" type="teaxtbox" name="dbinline'+nvalue[4]+'" id="dbinline'+nvalue[4]+'" data-uitype="'+nvalue[3]+'" data-columnname="'+nvalue[1]+'" data-tablename="'+nvalue[2]+'" data-fieldname="'+nvalue[4]+'" data-id="'+nvalue[4]+'" data-fieldlabel="'+nvalue[5]+'" data-parenttable="'+nvalue[6]+'" data-moduleid="'+nvalue[0]+'" value="'+value+'"/><span class="inlineeditsubmit" data-nname='+nvalue[4]+' style="color:#546e7a;cursor:pointer;padding-right:15px;font-size:0.8rem;" data-uitype="'+nvalue[3]+'" id="inlineeditsubmit'+nvalue[4]+'" data-id="dbinline'+nvalue[4]+'">Submit</span><span class="inlineeditcancel" style="color:#546e7a;cursor:pointer;font-size:0.8rem;" data-uitype="'+nvalue[3]+'" id="inlineeditcancel'+nvalue[4]+'" data-id="dbinline'+nvalue[4]+'" data-id1="'+nvalue[4]+'">Cancel</span></div>';
           }else if(nvalue[3] == 14){ //autonumber
        	   var cclass = '';
        	   inlineedit ='';
        	   ccustuomeditfields = '';
           } else { //unwanted uitype
        	   var cclass = '';
        	   inlineedit ='';
        	   ccustuomeditfields = '';
           }    

          appendcustomdata +='<div class="large-4 medium-6 small-12 columns end customwidget"><div class="large-12 columns"style="line-height:1.4rem;height:4rem;"><div class="" style="color:#546e7a; font-size:0.8rem;word-wrap: break-word">'+index+'</div><span class="customhover"><span class="" id="dbinline'+nvalue[4]+nvalue[3]+'" style="font-size:0.9rem;  display:inline-block; overflow:hidden; width:7em;text-overflow:ellipsis; white-space:nowrap;cursor:pointer;">'+value+'</span>'+inlineedit+ccustuomeditfields+'</div><div class="large-12">&nbsp;</div></div></div>';
         k++
		});
		$("#"+appendclass+"").append(appendcustomdata);//appends the row data in widgets
		//custom hover 
		$(".customhover").hover(function() {
			$( this ).find('.customedit').css('opacity','1');}, function(){
			$( this ).find('.customedit').css('opacity','0');
		})
		$('.chzn-select').select2();
		//Custom widget inline edit operation
		$(".custominlineedit").click(function(){
			var uitype=$(this).attr('data-uitype');
			var fieldid=$(this).attr('data-id');
			var fieldname=$(this).attr('data-fieldname');
			var moduleid=$(this).attr('data-moduleid');
			var columnname=$(this).attr('data-columnname');
			if(uitype == 17 || uitype == 20 || uitype == 19 || uitype == 18) {
				var value=$(this).attr('data-dataval');
				customwidgetfieldvalues(moduleid,fieldname,fieldid,uitype,value);
			}
			$("#custominlineedit"+fieldname+"").removeClass('hidedisplay');
			$("#dbinline"+fieldname+""+uitype+"").css('display','none');
		});
		//inline edit cancel
		$(".inlineeditcancel").click(function(){
			$(this).removeClass('error');
			var id=$(this).attr('data-id');
			var id1=$(this).attr('data-id1');
			var uitype=$(this).attr('data-uitype');
			$("#custominlineedit"+id1+"").addClass('hidedisplay');
			$("#"+id+""+uitype+"").css('display','inline-block');
		});
		//inline edit submit 
		$(".inlineeditsubmit").click(function(){
			var nname = $(this).attr('data-nname');
			var id=$("#custominlineedit"+nname+" span.inlineeditsubmit").attr('data-id');
			var uitype=$("#custominlineedit"+nname+" span.inlineeditsubmit").attr('data-uitype');
			var inlinefieldfiled=$("#"+id+"").attr('data-id');
			if(uitype == 17 || uitype == 20 || uitype == 19 || uitype == 18){
				var value = $("#"+id+"").find(':selected').data('productid');
				var relatedtab=$("#"+id+"").attr('data-parenttable');
				var tablename=$("#"+id+"").attr('data-tablename');
				var columnname=$("#"+id+"").attr('data-columnname');
				var fieldname=$("#"+id+"").attr('data-fieldname');
				//productname
				var productname = $("#"+id+"").find('option:selected').data('productname');
				$("#"+id+uitype+"").text(productname);
				$("#"+id+uitype+"").attr('data-dataval', productname);
			} else {
				var value = $("#"+id+"").val();
				var relatedtab=$("#"+id+"").attr('data-parenttable');
				var tablename=$("#"+id+"").attr('data-tablename');
				var columnname=$("#"+id+"").attr('data-columnname');
				var fieldname=$("#"+id+"").attr('data-fieldname');
				//span value assign
				$("#"+id+uitype+"").text(value);
			}
			var parenttable = $('#elementspartabname').val();
			var recordid = $("#dashboardrecordid").val();
			customeditfieldsubmit(value,parenttable,relatedtab,tablename,columnname,recordid,fieldname);
			$("#"+id+""+uitype+"").css('display','block');
			$("#custominlineedit"+inlinefieldfiled+"").addClass('hidedisplay');
		});
	}
}
{// Widget Creation Mail Log
	function mailnotificationwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid) {
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Reload" class=" widgetreloadicon" id="mailnotificationreloadbtn'+id+'"><i class="material-icons">refresh</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div><span id="mailnotilogmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastmailnotlogid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;	
		if(fieldlength > 0) {
			maillogwidgetcontent(appendclass,data);
		} else {
			//append empty set
			$("#"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#mailnotilogmore').empty();
		}
		$(function() {
		    setTimeout(function() {
				//more append SMS log
				$("#mailnotilogmore").click(function() {
					var lastlogid=$('#lastmailnotlogid').val();
					if(lastlogid > 0){
						maillogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass);
					} else { 
					}
				});
			},interval); 
		});
	}
}
function maillogwidgetcontent(appendclass,data) {
	// For View Icon and redirect icon		
	for(var i=0; i<data.length;i++) {
		$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">'+data[i]['message']+'<span class="redcolor">   </span></div><div class="large-12 columns taskassignnamestyle">Regards Customer List</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['duration']+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		$('#lastmailnotlogid').val(data[i]['logid']);
	}
}
function maillogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass) {
	$.ajax({
		url:base_url+"Home/getsmslogmoredata",
		data: {dashboardrowid:dashboardrowid,lastnode:lastlogid,moduleid:moduleid,modname:'Mail'},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != null &&  data != ''){
				var llength=data.length;			
				if(llength > 0){
					maillogwidgetcontent(appendclass,data);
				}
			}
		}
	});
}
{// Widget Creation sms Log
	function smsnotificationwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid) {
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Reload" class="widgetreloadicon" id="smsnotificationreloadbtn'+id+'"><i class="material-icons">refresh</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div><span id="smsnotilogmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastsmsnotlogid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;	
		if(fieldlength > 0){
			smslogwidgetcontent(appendclass,data);
		} else {
			//append empty set
			$("#"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#smsnotilogmore').empty();
		}
		$(function() {
		    setTimeout(function(){
				//more append sms log
				$("#smsnotilogmore").click(function(){
					var lastlogid=$('#lastsmsnotlogid').val();
					if(lastlogid > 0){
						smslogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass);
					} else { 
					}
				});
			},interval); 
		});
	}
}
function smslogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass) {
	$.ajax({
		url:base_url+"Home/getsmslogmoredata",
		data: {dashboardrowid:dashboardrowid,lastnode:lastlogid,moduleid:moduleid,modname:'SMS'},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != null &&  data != ''){
				var llength=data.length;			
				if(llength > 0){
					smslogwidgetcontent(appendclass,data);
				}
			}
		}
	});
}
function smslogwidgetcontent(appendclass,data){ 
	for(var i=0; i<data.length;i++) {
		$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">'+data[i]['message']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['duration']+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		$('#lastsmsnotlogid').val(data[i]['logid']);
	} 
}
{// Widget Creation Receive SMS Log
	function receivesmsnotificationwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid) {
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Reload" class=" widgetreloadicon" id="receivesmsnotificationreloadbtn'+id+'"><i class="material-icons">refresh</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div><span id="receivesmsnotilogmore" class="widgetmorestyle" style="">More<input type="hidden" id="receivelastsmsnotlogid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;	
		if(fieldlength > 0){
			receivesmslogwidgetcontent(appendclass,data);
		} else {
			//append empty set
			$("#"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#receivesmsnotilogmore').empty();
		}
		$(function() {
		    setTimeout(function(){
				//more append sms log
				$("#receivesmsnotilogmore").click(function(){
					var lastlogid=$('#receivelastsmsnotlogid').val();
					if(lastlogid > 0){
						receivesmslogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass);
					} else { 
					}
				});
			},interval); 
		});
	}
}
function receivesmslogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass) {
	$.ajax({
		url:base_url+"Home/getsmslogmoredata",
		data: {dashboardrowid:dashboardrowid,lastnode:lastlogid,moduleid:moduleid,modname:'SMS'},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != null &&  data != ''){
				var llength=data.length;			
				if(llength > 0){
					receivesmslogwidgetcontent(appendclass,data);
				}
			}
		}
	});
}
function receivesmslogwidgetcontent(appendclass,data){ 
	for(var i=0; i<data.length;i++) {
		$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">'+data[i]['message']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['duration']+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		$('#receivelastsmsnotlogid').val(data[i]['logid']);
	} 
}
{// Widget Creation send sms Log sender
//"Quick SMS","chartgenid'.$id.'","chartreloadid'.$id.'",'.$id.','.$dashboardfieldid.','.$dashboardrowid.','.$interval.'
	function sendsmslogwidgetcreation(widgettitle,appendid,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid){
		$("#"+appendid+"").append('<div class="large-12 columns end" ><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns" ><span class="dsbtaskcaption">Quick SMS</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;" ><span class="smswidget" id="quickadvancesettingicon" title="Advance Setting"><i class="material-icons">settings</i></span><span id="quickpreviewicon" class="smswidget" title="Preview"><i class="material-icons">visibility_off</i></span></div></div><form id="quicksendsmsform" class="cleardataform" action="" name="dataaddform" method="POST"><span id="quicksmsvalidatewizard" class="validationEngineContainer"><div class="large-12 columns scrollbarclass" style="height:16rem; overflow-y:scroll"><div class="widgetwrapper large-12 columns"><div id="quickpreviewsms" class="large-12 columns hidedisplay"><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-12 columns" ><span class="dsbtaskcaption">Preview SMS</span></div></div><div class="large-12 columns"><div class="large-12 columns"><textarea id="quicksmspreview" data-prompt-position="bottomLeft" rows="3" tabindex="" name="quicksmspreview"></textarea></div></div><div class="large-12 columns"> &nbsp </div><div class="large-12 columns"> &nbsp </div></div><div id="advancedsettings" class="large-12 columns hidedisplay "><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns" ><span class="dsbtaskcaption">Advanced settings</span></div></div><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle "> Signature </div><div class="large-12 columns">	<select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft" id="quicksignature" name="quicksignature" ><option value="">select</option></select></div></div><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle "> Record Id </div><div class="large-12 columns"><select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft" id="quickrecordid" name="quickrecordid" ><option value="">select</option></select></div></div></div><div class="large-12 columns paddingzero"><div class="large-12 columns"> &nbsp </div><div class="large-12 columns"> &nbsp </div><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle "> Schedule Date </div><div class="large-12 columns"><input id="quickscheduledate" class="fordatepicicon" type="text" data-dateformater="dd-mm-yy" data-prompt-position="bottomLeft" tabindex="" name="quickscheduledate"></div></div><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle "> Schedule Time </div><div class="large-12 columns"><input id="quickscheduletime" class=" fortimepicicon " type="text" data-prompt-position="bottomLeft" readonly="readonly" tabindex="" name="quickscheduletime"></div></div><div class="large-12 columns"> &nbsp </div><div class="large-12 columns"> &nbsp </div></div></div><div id="quicksms" class="large-12 columns"><div class="large-12 columns paddingzero"><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Transaction </div><div class="large-12 columns"><select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft" id="smssendtype" name="smssendtype" ><option value="">select</option></select></div></div><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Sender</div><div class="large-12 columns"><select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft" id="senderid" name="senderid" ><option value="">select</option></select></div></div><div class="large-4 medium-4 small-12 columns"><div class="large-12 columns tasknamestyle "> Type </div><div class="large-12 columns"><select class="chzn-select  validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft" id="smstype" name="smstype" ><option value="">select</option></select></div></div></div><div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle ">Template </div><div class="large-12 columns"><select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft" id="smstemplateid" name="smstemplateid" ><option value="">select</option></select></div></div><div class="large-6 medium-6 small-12 columns"><div class="large-12 columns tasknamestyle "> Mobile Number </div><div class="large-12 columns"><input id="mobilenumber" class="validate[required] custom[phone]" type="text" data-prompt-position="bottomLeft" tabindex="" name="mobilenumber"></div></div></div><div class="large-12 columns"><div class="large-12 columns tasknamestyle">Message</div><div id="smsmessagedivhid" class="large-12 columns"><textarea id="smsmessage" data-prompt-position="bottomLeft" rows="2" class = "validate[required]" tabindex="" name="smsmessage"></textarea></div><div class="large-12 columns"> &nbsp;</div><div class="large-12 columns"><div class="large-12 columns righttext"><input id="quicksendsmsbtn" class="dbfiltersubmit btn" type="button" value="Submit" name="quicksendsmsbtn"></div></div></div></div></div></div></span></form></div>');
		/*Load the events after loadings*/
		$(function() {
			setTimeout(function(){
				$('.chzn-select').select2();
				$("#quickrecordid").attr('readonly',false);
				if(moduleid != '1'){
					mobilenumberfetch(moduleid,dashboardrowid);
					recordidddvaluefetch(moduleid,dashboardrowid);
					$("#quickrecordid").attr('readonly',true);
				}
				widgetfieldvalues(210,'smstypename','smstype');
				widgetfieldvalues(210,'smssendtypename','smssendtype');
				smssenderidvalueget('senderid');
				smssignaturevalueget('quicksignature');
				{//sms send type change
					$("#smssendtype").change(function() {
						var typeid = $("#smssendtype").val();
						if(typeid == '2'){
							$('#senderid').removeClass('validate[required]');
							$('#senderid').addClass('validate[required]');
						} else {
							$('#senderid').removeClass('validate[required]');
						}
						smstemplatevalueget('smstemplateid');
					});
				}
				//template name change
				$('#smstemplateid').change(function() {
					var tempid = $('#smstemplateid').val();
					$.ajax( {
						url:base_url+"Sms/fetchtemplatedetails?templid="+tempid, 
						dataType:'json',
						async:false,
						success: function(data) {                
							var msgfilename = data.msg;
							templateeditervalueget(msgfilename);
							//For record drop down value load
							if(data.moduleid != '1'){
								var mid = data.moduleid;
								recordidddvaluefetch(mid,dashboardrowid);
							} else{
								$("#quickrecordid").empty();
								$("#quickrecordid").select2('val','');
							}
						}
					});
					$( "#smsmessage" ).focus();
				});
				//hide show
				$("#quickadvancesettingicon").click(function() {
					$("#advancedsettings").slideToggle( "slow", function() {/* Animation complete. */ });
					$("#advancedsettings").removeClass('hidedisplay');
					$("#quickpreviewsms").addClass('hidedisplay');
					$("#advancedsettings").show();
					$("#quickpreviewsms").hide();
				});
				$("#quickpreviewicon").click(function() {
					$("#quickpreviewsms").slideToggle( "slow", function() {/* Animation complete. */ });
					$("#quickpreviewsms").removeClass('hidedisplay');
					$("#advancedsettings").addClass('hidedisplay');
					$("#quickpreviewsms").show();
					$("#advancedsettings").hide();
				});
				{//message focus out
					$("#smsmessage").focusout(function() {
						var recordid = $('#quickrecordid').val();
						if(recordid != '') { 
							recordid = recordid;
						}else {
							recordid = '';
						}
						var content = $('#smsmessage').val();
						var regExp = /\{([^}]+)\}/g;
						var datacontent = content.match(regExp);
						var moduleid = $("#smstemplateid").find('option:selected').data('moduleid');
						var parenttablename = $('#quickrecordid').find('option:selected').data('modulename');
						if(datacontent != null){
							var newcontent = "";
							if(recordid != null) {
								$.ajax({
									url:base_url+'Sms/smsmergcontinformationfetch',
									data:'content='+datacontent+"&recordid="+recordid+"&moduleid="+moduleid+"&parenttablename="+parenttablename,
									type:'POST',
									dataType:'json',
									async:false,
									cache:false,
									success: function(data) {
										var newcontent = $('#smsmessage').val();
										for (var i = 0; i < datacontent.length; i++) {
											newcontent = newcontent.replace(''+datacontent[i]+'',''+data[i]+'');
										}
										$('#quicksmspreview').val(newcontent);
									},
								});
							}
						} else {
							$('#quicksmspreview').val(content);
						}
					});
				}
				{//signature file value get
					$("#quicksignature").change(function() {
						var sigid = $("#quicksignature").val();
						if(sigid != ''){
							var smssignature = $(this).find('option:selected').data('smssig');
							var oldcontent = $("#smsmessage").val();
							var message = oldcontent+''+smssignature;
							$('#quicksmspreview').val(message);
						}
					});
				}
				$('#quickrecordid').change(function() {
					var recordid = $(this).val();
					var content = $('#smsmessage').val();
					if( $(this).val()!= "" ) {
					var regExp = /\{([^}]+)\}/g;
					var datacontent = content.match(regExp);
					var moduleid = $("#smstemplateid").find('option:selected').data('moduleid');
					var parenttablename = $('#quickrecordid').find('option:selected').data('modulename');
					if(datacontent != null) {
							var newcontent = "";
							if(recordid != null){
								$.ajax({
									url:base_url+'Sms/smsmergcontinformationfetch',
									data:'content='+datacontent+"&recordid="+recordid+"&moduleid="+moduleid+"&parenttablename="+parenttablename,
									type:'POST',
									dataType:'json',
									async:false,
									cache:false,
									success: function(data) {
										for(var j=0;j < data.length; j++) {
											var ncontent = $('#smsmessage').val();
											for (var i = 0; i < datacontent.length; i++) {
												ncontent = ncontent.replace(''+datacontent[i]+'',''+data[j][i]+'');
											}
										}
										$('#quicksmspreview').val(ncontent);
									},
								});
							}
						} else {
							$('#quicksmspreview').val(content);
						}
					}
				});
				{//mobile number validation
					$("#mobilenumber").focusout(function() {
						var mobilenum = $("#mobilenumber").val();
						var regExp = /^([\+][0-9]{1,3}[\ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9\ \.\-\/]{3,20})((x|ext|extension)[\ ]?[0-9]{1,4})?$/;
						var data = mobilenum.match(regExp);
						if(data == null){return 'Invalid Mobile Number';}
					});
				}
				{// Date REstriction
					var date = new Date();
					$('#quickscheduledate').datetimepicker({
						dateFormat: 'yy-mm-dd',
						showTimepicker :false,
						minDate: date,
						onSelect: function () {
							$(this).removeClass('error');
						},
						onClose: function () {
							$('#quickscheduledate').focus();
						}
					});
				}
				$('#quickscheduletime').timepicker({
					showTimepicker: true,
					timeOnly:true,
					showSecond: true,
					timeFormat: 'HH:mm:ss',
					 onSelect: function () {
						var checkdate = $(this).val();
					},
					onClose: function () {
						$('#quickscheduletime').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
				$("#quicksendsmsbtn").click(function() {
					$("#quicksmsvalidatewizard").validationEngine('validate');
				});
				jQuery("#quicksmsvalidatewizard").validationEngine({
					onSuccess: function() {
						quicksendsms();
					},
					onFailure: function() {
						var dropdownid =['2','smstype','smssendtype'];
						dropdownfailureerror(dropdownid);
						alertpopup(validationalert);
					}	
				});
			},interval);
		});
	}
}
{// Widget Creation Click to call
	function clicktocallwidgetcreation(widgettitle,appendid,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid){
		$("#"+appendid+"").append('<div class="large-12 columns end"> <div class="large-12 columns paddingzero"> <div class="large-12 medium-12 small-12 columns"><span class="dsbtaskcaption">Quick Call</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div class="widgetwrapper large-12 columns"><div id="" class="large-8 medium-8 columns small-12 large-centered medium-centered"> <div class="large-12 columns tasknamestyle ">Module <span class="mandatoryfildclass">*</span></div><select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="c2cmoduleid" name="c2cmoduleid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div id="relatedmoddiv" class="large-8 medium-8 columns small-12 large-centered medium-centered"> <div class="large-12 columns tasknamestyle">Related Module <span class="mandatoryfildclass">*</span></div><select class="chzn-select" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="c2crelmoduleid" name="c2crelmoduleid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="large-8 medium-8 columns small-12 large-centered medium-centered"> <div class="large-12 columns tasknamestyle ">Record <span class="mandatoryfildclass">*</span></div><select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="c2crecordid" name="c2crecordid" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="large-8 medium-8 columns small-12 large-centered medium-centered"> <div class="large-12 columns tasknamestyle ">Mobile Number <span class="mandatoryfildclass">*</span></div><select class="chzn-select validate[required]" data-placeholder="Select" data-prompt-position="bottomLeft:14,36" id="c2cmobilenum" name="c2cmobilenum" tabindex="103"> <option value="">Select</option> </select> </div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="row">&nbsp;</div><div class="large-12 "> <div class="medium-4 large-4 columns">&nbsp;</div><div class="small-4 small-centered medium-4 large-4 columns"> <input type="button" id="c2csubmit" name="c2csubmit" value="Submit" class="formbuttonsalert"> </div><div class="medium-4 large-4 columns">&nbsp;</div><div class="large-12 columns">&nbsp;</div></div></div></div></div>');
		/*Load the events after loadings*/
		$(function() {
			$('.chzn-select').select2();
			{//module dd function
				moduleddload('c2cmoduleid');
				$("#c2cmoduleid").change(function() {
					var mid = $("#c2cmoduleid").val();
					if(mid == 201 || mid == 4 || mid == 202 || mid == 203) {
						$("#c2crelmoduleid").attr('readonly',true);
						recordddval(mid,'c2crecordid');
					} else {
						$("#c2crelmoduleid").attr('readonly',false);
						relatedmodulevaluefetch(mid);
					}
				});
				$("#c2crelmoduleid").change(function() {
					var rmid = $("#c2crelmoduleid").val();
					recordddval(rmid,'c2crecordid');
				});
			}
			{//record dd change
				$("#c2crecordid").change(function() {
					var mid = $("#c2cmoduleid").val();
					var rmid = $("#c2crelmoduleid").val();
					var recordid = $("#c2crecordid").val();
					if(rmid == ''){
						mobilenumberddload(mid,recordid);
					}else {
						relatedmobilenumberddload(mid,rmid,recordid);
					}
					
				});
			}
			//submit button
			$("#c2csubmit").click(function() {
				var mnum = $("#c2cmobilenum").val();
				if(mnum == '' || mnum == null) {
					alertpopup("Please Select the mobile number to call");
				} else {
					homeclicktocallfunction(mnum);
				}
			});
		});		
	}
}
{// Widget Creation Receive SMS Log
	function inboundcalllogwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid) {
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Reload" class="widgetreloadicon" id="inboundcallnotificationreloadbtn'+id+'"><i class="material-icons">refresh</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div><span id="inboundcallnotilogmore" class="widgetmorestyle" style="">More<input type="hidden" id="inboundcalllastnotlogid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;	  
		if(fieldlength > 0){
			inboundcalllogwidgetcontent(appendclass,data);
		} else {
			//append empty set
			$("#"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#inboundcallnotilogmore').empty();
		}
		$(function() {
		    setTimeout(function(){
				//more append sms log
				$("#inboundcallnotilogmore").click(function(){
					var lastlogid=$('#inboundcalllastnotlogid').val();
					if(lastlogid > 0){
						inboundcalllogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass);
					} else { 
					}
				});
			},interval); 
		});
		//For Reload 
		$("#inboundcallnotificationreloadbtn"+id+"").click(function(){
			inboundcalllogmoredatafetch(moduleid,dashboardrowid,0,appendclass);
		});
	}
}
function inboundcalllogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass) {
	$.ajax({
		url:base_url+"Home/getinboundcalllogmoredata",
		data: {dashboardrowid:dashboardrowid,lastnode:lastlogid,moduleid:moduleid,modname:'Calls'},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != null &&  data != ''){
				var llength=data.length;			
				if(llength > 0){
					inboundcalllogwidgetcontent(appendclass,data);
				}
			}
		}
	});
}
function inboundcalllogwidgetcontent(appendclass,data){ 
	for(var i=0; i<data.length;i++) {
		$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">'+data[i]['message']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['duration']+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		$('#inboundcalllastnotlogid').val(data[i]['logid']);
	} 
}
{// Widget Creation Receive SMS Log
	function outboundcalllogwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid) {
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Reload" class="widgetreloadicon" id="outboundcallnotificationreloadbtn'+id+'"><i class="material-icons">refresh</i></span></div></div><div class="large-12 columns scrollbarclass"  style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"></div></div><span id="outboundcallnotilogmore" class="widgetmorestyle" style="">More<input type="hidden" id="outboundcalllastnotlogid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;	
		if(fieldlength > 0){
			outboundcalllogwidgetcontent(appendclass,data);
		} else {
			//append empty set
			$("#"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#outboundcallnotilogmore').empty();
		}
		$(function() {
		    setTimeout(function(){
				//more append sms log
				$("#outboundcallnotilogmore").click(function(){
					var lastlogid=$('#outboundcalllastnotlogid').val();
					if(lastlogid > 0){
						outboundcalllogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass);
					} else { 
					}
				});
			},interval); 
		});
		//For Reload 
		$("#outboundcallnotificationreloadbtn"+id+"").click(function(){
			outboundcalllogmoredatafetch(moduleid,dashboardrowid,0,appendclass);
		});
	}
}
function outboundcalllogmoredatafetch(moduleid,dashboardrowid,lastlogid,appendclass) {
	$.ajax({
		url:base_url+"Home/getinboundcalllogmoredata",
		data: {dashboardrowid:dashboardrowid,lastnode:lastlogid,moduleid:moduleid,modname:'Calls'},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if(data != null &&  data != ''){
				var llength=data.length;			
				if(llength > 0){
					outboundcalllogwidgetcontent(appendclass,data);
				}
			}
		}
	});
}
function outboundcalllogwidgetcontent(appendclass,data){ 
	for(var i=0; i<data.length;i++) {
		$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock tkmediumpr paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">'+data[i]['message']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['duration']+'</span></div></div><div class="large-12 columns"> &nbsp;</div>');
		$('#outboundcalllastnotlogid').val(data[i]['logid']);
	} 
}
{// Widget Creation Opportunity
	function opportunitywidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,opptoolbar) {
		var datacheck="";
		var datalist = "";
		opp_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Opportunity">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+opptoolbar[2]+'"><span title="Add" class="widgetredirecticonopportunity widgetviewicon" data-redirecturl="Opportunity"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="opportunityfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="opportunityfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="opportunityfields" class="chzn-select"></select></div><div class="large-6 columns" id="opportunitytextdiv"><label>Filter Value</label><input type="text" id="opportunityfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="opportunitydddiv"><label>Filter Value</label><select id="opportunityfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="opportunitydatediv"><label>Filter Value</label><input type="text" id="opportunityfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="opportunitymore" class="widgetmorestyle" style="">More<input type="hidden" id="lastopportunityid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			opportunityidgetcontent('append'+appendclass+'',data,opptoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<li style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</li>');
			$('#opportunitymore').empty();
		}
		//For Add 
		$(".widgetredirecticonopportunity").click(function(){
			{// For Redirection Opportunity Add
				sessionStorage.setItem("opportunityaddsrc",'formhome');
				sessionStorage.setItem("widgetid",18);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#opportunityreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
					
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//more append activity data-
				$("#opportunitymore").click(function(){
					var opportunity_id=$('#lastopportunityid').val();
					if(opportunity_id > 0){
						var uitype=$("#opportunityfields").find('option:selected').data('uitype');					
						var columnfield=$("#opportunityfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#opportunityfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#opportunityfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#opportunityfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#opportunityfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('opportunity',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,opportunity_id,uitype);
						if(rdata.length > 0){
							opportunityidgetcontent('append'+appendclass+'',rdata,opptoolbar,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#opportunityfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('opportunityfields',204,'opportunitytextdiv','opportunitydddiv','opportunitydatediv','opportunityfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('opportunityfields') == true ){
						var uitype=$("#opportunityfields").find('option:selected').data('uitype');					
						var columnfield=$("#opportunityfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#opportunityfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#opportunityfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#opportunityfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#opportunityfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('opportunity',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							opportunityidgetcontent('append'+appendclass+'',rdata,opptoolbar,reloadid);					
						} else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#opportunityfilterbtn"+id+"").trigger('click');
						$("#opportunityfiltertext,#opportunityfilterdate").val('');
						$("#opportunityfields,#opportunityfilterselect").select2('val','').trigger('change');
						$("#opportunityfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});		
				//For Filter 
				$( "#opportunityfilterbtn"+id+"" ).click(function(){
					$( "#opportunityfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(opp_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load opportunity fields
							loadfilterfield('opportunityfields',204,'opportunity');
							opp_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#opportunityfilterdate').attr('data-dateformater');
				$('#opportunityfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Contacts
	function contactswidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,conttoolbar){
		var datacheck="";
		var datalist = "";
		contact_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Contact">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+conttoolbar[2]+'"><span title="Add" class="widgetredirecticoncontact widgetviewicon" data-redirecturl="Contact"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="contactsfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="contactsfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="contactsfields" class="chzn-select"></select></div><div class="large-6 columns" id="contactstextdiv"><label>Filter Value</label><input type="text" id="contactsfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="contactsdddiv"><label>Filter Value</label><select id="contactsfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="contactsdatediv"><label>Filter Value</label><input type="text" id="contactsfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="contactmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastcontactid"></input></span>');
		//load lead fields				
		//intial dataloading call 10
		var fieldlength = data.length;		
		if(fieldlength > 0){
			contactwidgetcontent('append'+appendclass+'',data,conttoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#contactmore').empty();
		}
		//For Add 
		$(".widgetredirecticoncontact").click(function(){
			{// For Redirection Contact Add
				sessionStorage.setItem("contactaddsrc",'formhome');
				sessionStorage.setItem("widgetid",19);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#contactsreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#contactsfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('contactsfields',203,'contactstextdiv','contactsdddiv','contactsdatediv','contactsfilterselect');
					}		
				});
				//more append activity data-
				$("#contactmore").click(function(){
					var contact_id=$('#lastcontactid').val();
					if(contact_id > 0){
						var uitype=$("#contactsfields").find('option:selected').data('uitype');					
						var columnfield=$("#contactsfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#contactsfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#contactsfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#contactsfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#contactsfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('contact',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,contact_id,uitype);
						if(rdata.length > 0){
							contactwidgetcontent('append'+appendclass+'',rdata,conttoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('contactsfields') == true ){
						var uitype=$("#contactsfields").find('option:selected').data('uitype');					
						var columnfield=$("#contactsfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#contactsfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#contactsfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#contactsfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#contactsfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('contact',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							contactwidgetcontent('append'+appendclass+'',rdata,conttoolbar,reloadid);					
						} else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#contactsfilterbtn"+id+"").trigger('click');
						$("#contactsfiltertext,#contactsfilterdate").val('');
						$("#contactsfields,#contactsfilterselect").select2('val','').trigger('change');
						$("#contactsfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#contactsfilterbtn"+id+"" ).click(function(){
					$( "#contactsfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(contact_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load opportunity fields
							loadfilterfield('contactsfields',203,'contact');
							contact_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});				
				// Date validation
				var dateformetdata = $('#contactsfilterdate').attr('data-dateformater');
				$('#contactsfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});
			},interval); 
		});			
	}
}
{// Widget Creation Invoice
	function invoicewidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,invoicetoolbar) {
		var datacheck="";
		var datalist = "";
		inv_filter_status=0;
		$("#"+appendid+"").html('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Invoice">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+invoicetoolbar[2]+'"><span title="Add" class="widgetredirecticoninvoice widgettextbtn" data-redirecturl="Invoice">ADD</span></span><span title="Filter" class="widgettextbtn" id="invoicefilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="invoicefiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="invoicefields" class="chzn-select"></select></div><div class="large-6 columns" id="invoicetextdiv"><label>Filter Value</label><input type="text" id="invoicefiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="invoicedddiv"><label>Filter Value</label><select id="invoicefilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="invoicedatediv"><label>Filter Value</label><input type="text" id="invoicefilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="invoicemore" class="widgetmorestyle" style="">More<input type="hidden" id="lastinvoiceid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			invoicewidgetcontent('append'+appendclass+'',data,invoicetoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#invoicemore').empty();
		}
		//For Add 
		$(".widgetredirecticoninvoice").click(function(){
			{// For Redirection Invoice Add
				sessionStorage.setItem("invoiceaddsrc",'formhome');
				sessionStorage.setItem("widgetid",20);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#invoicereloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//more append activity data-
				$("#invoicemore").click(function(){
					var invoice_id=$('#lastinvoiceid').val();
					if(invoice_id > 0){
						var uitype=$("#invoicefields").find('option:selected').data('uitype');					
						var columnfield=$("#invoicefields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#invoicefields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#invoicefilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#invoicefilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#invoicefiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('invoice',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,invoice_id,uitype);
						if(rdata.length > 0){
							invoicewidgetcontent('append'+appendclass+'',rdata,invoicetoolbar,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#invoicefields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('invoicefields',226,'invoicetextdiv','invoicedddiv','invoicedatediv','invoicefilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('invoicefields') == true ){
						var uitype=$("#invoicefields").find('option:selected').data('uitype');					
						var columnfield=$("#invoicefields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#invoicefields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#invoicefilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#invoicefilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#invoicefiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('invoice',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							invoicewidgetcontent('append'+appendclass+'',rdata,invoicetoolbar,reloadid);					
						} else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#invoicefilterbtn"+id+"").trigger('click');
						$("#invoicefiltertext,#invoicefilterdate").val('');
						$("#invoicefields,#invoicefilterselect").select2('val','').trigger('change');
						$("#invoicefilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#invoicefilterbtn"+id+"" ).click(function(){
					$( "#invoicefiltercontainer" ).slideToggle( "slow", function() { });
					{//load filter fields
						if(inv_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load invoice fields
							loadfilterfield('invoicefields',226,'invoice');
							inv_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				// Date validation
				var dateformetdata = $('#invoicefilterdate').attr('data-dateformater');
				$('#invoicefilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});
		
	}
}
{// Widget Creation Quote
	function quotewidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,quotetoolbar) {	
		var datacheck="";
		var datalist = "";
		quote_filter_status=0;
		$("#"+appendid+"").html('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Quote">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+quotetoolbar[2]+'"><span title="Add" class="widgetredirecticonquote widgettextbtn" data-redirecturl="Quote">ADD</span></span><span title="Filter" class="widgettextbtn" id="quotefilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="quotefiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="quotefields" class="chzn-select"></select></div><div class="large-6 columns" id="quotetextdiv"><label>Filter Value</label><input type="text" id="quotefiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="quotedddiv"><label>Filter Value</label><select id="quotefilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="quotedatediv"><label>Filter Value</label><input type="text" id="quotefilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="quotemore" class="widgetmorestyle" style="">More<input type="hidden" id="lastquoteid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			quotewidgetcontent('append'+appendclass+'',data,quotetoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
		}
		//For Add 
		$(".widgetredirecticonquote").click(function(){
		{// For Redirection Quote Add
			sessionStorage.setItem("quoteaddsrc",'formhome');
			sessionStorage.setItem("widgetid",21);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
		var fullurl = $(this).data('redirecturl');
		window.location = base_url+fullurl;
		});
		//For Reload 
		$("#quotereloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
		  setTimeout(function(){
		  //Handler for .ready() called.
		  $("#quotemore").click(function(){
			var quote_id=$('#lastquoteid').val();
			if(quote_id > 0){
				var uitype=$("#quotefields").find('option:selected').data('uitype');					
				var columnfield=$("#quotefields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#quotefields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#quotefilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#quotefilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#quotefiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getmoreoutput('quote',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,quote_id,uitype);
				if(rdata.length > 0){
					quotewidgetcontent('append'+appendclass+'',rdata,quotetoolbar,reloadid);					
				}
			}
			});
			//Load Options on uitype
		  $("#quotefields").change(function(){
				var value=$(this).val();
				if(checkValue(value) == true){			
					filtervalueload('quotefields',216,'quotetextdiv','quotedddiv','quotedatediv','quotefilterselect');
				}		
			});
			//For Filter 
			$( "#quotefilterbtn"+id+"" ).click(function(){
			$( "#quotefiltercontainer" ).slideToggle( "slow", function() { });
			{//load filter fields
				if(quote_filter_status == 0){
					//For Dropdown
					$(".chzn-select").select2({containerCss: {display: "block"}});
					//load quote fields
					loadfilterfield('quotefields',216,'quote');
					quote_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
				}
			}
		});		
		// Date validation
			var dateformetdata = $('#quotefilterdate').attr('data-dateformater');
			$('#quotefilterdate').datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					$(this).removeClass('error');
				}
			});
			//Filter submit results
			$("#filtersubmit"+id+"").click(function(){
			if(checkVariable('quotefields') == true ){
				var uitype=$("#quotefields").find('option:selected').data('uitype');					
				var columnfield=$("#quotefields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#quotefields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#quotefilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#quotefilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#quotefiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getfilteroutput('quote',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
				$('#append'+appendclass+'').empty();
				if(rdata.length > 0){
					quotewidgetcontent('append'+appendclass+'',rdata,quotetoolbar,reloadid);					
				} else {
						//append empty set
						$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
				}
				$("#quotefilterbtn"+id+"").trigger('click');
				$("#quotefiltertext,#quotefilterdate").val('');
				$("#quotefields,#quotefilterselect").select2('val','').trigger('change');
				$("#quotefilterselect").empty();
			}
			else{
				alertpopup('Please Give All Filter Values');
			}
			});
		  },interval);		  
	});	
	}
}
{// Widget Creation Status
	function leadstatuswidgetcreation(widgettitle,appendid,appendclass,data) {		
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Lead">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Add" class="widgetredirecticonleadstatus widgetviewicon" data-redirecturl="Lead"><i class="material-icons">add</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden ;overflow-y: scroll;"><div class="clearfix"></div><ul id="'+appendclass+'" class="member-list widgetwrapper large-12 columns"></ul></div>');
		for(var i=0; i<data.length;i++) {
			if(data[i]['status']){
				var status = data[i]['status'];
			} else{
				var status = 'Without Status';
			}
			$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span class="">'+status+'</span><span class="">'+data[i]['count']+'</span><div class="large-12 columns"> &nbsp;</div></div><div class="member-image">'+status.substr(0, 1)+' <span class=""></span></div></li>');
		}
		//For View 
		$(".widgetviewredirecticon").click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For add 
		$(".widgetredirecticonleadstatus").click(function(){
			{// For Redirection Lead Add
				sessionStorage.setItem("leadaddsrc",'formhome');
				sessionStorage.setItem("widgetid",22);
				sessionStorage.setItem("moduleid",1);
				sessionStorage.setItem("rowid",1);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
	}
}
{
	function timeanddatewidget(appendid){
		$("#"+appendid+"").append('<div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:center"><span style="font-family:latosbt;color:#575757;font-size:1.2rem">Date & Time</span></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" id="datewidget" style="background:#ffffff;border:1px solid #cccccc"><div class="large-12 columns">&nbsp;</div><div id="Date"></div><ul><li id="hours"> </li><li id="point">:</li><li id="min"> </li><li id="point">:</li><li id="sec"> </li></ul><div class="large-12 columns">&nbsp;</div></div>');
		// Create two variable with the names of the months and days in an array
							var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
							var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

							// Create a newDate() object
							var newDate = new Date();
							// Extract the current date from Date object
							newDate.setDate(newDate.getDate());
							// Output the day, date, month and year    
							$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

							setInterval( function() {
								// Create a newDate() object and extract the seconds of the current time on the visitor's
								var seconds = new Date().getSeconds();
								// Add a leading zero to seconds value
								$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
								},1000);

							setInterval( function() {
								// Create a newDate() object and extract the minutes of the current time on the visitor's
								var minutes = new Date().getMinutes();
								// Add a leading zero to the minutes value
								$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
								},1000);

							setInterval( function() {
								// Create a newDate() object and extract the hours of the current time on the visitor's
								var hours = new Date().getHours();
								// Add a leading zero to the hours value
								$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
								}, 1000);
	}
}
{// Widget Creation Accounts
	function accountswidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,accounttoolbar){
		account_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Account">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+accounttoolbar[59]+'"></span><span class="'+accounttoolbar[2]+'"><span title="Add" class="widgetredirecticonaccount widgetviewicon" data-redirecturl="Account"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="accountfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="accountfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="accountfields" class="chzn-select"></select></div><div class="large-6 columns" id="accounttextdiv"><label>Filter Value</label><input type="text" id="accountfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="accountdddiv"><label>Filter Value</label><select id="accountfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="accountdatediv"><label>Filter Value</label><input type="text" id="accountfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="accountsmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastaccountid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0) {
			accountwidgetcontent('append'+appendclass+'',data,accounttoolbar,reloadid); 
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#accountsmore').empty();
		}
		//For Add 
		$(".widgetredirecticonaccount").click(function(){
			{// For Redirection Accounts Add
				sessionStorage.setItem("accountaddsrc",'formhome');
				sessionStorage.setItem("widgetid",23);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#accountsreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
					//Load Options on uitype
		$("#accountfields").change(function(){
			var value=$(this).val();
			if(checkValue(value) == true){			
				filtervalueload('accountfields',202,'accounttextdiv','accountdddiv','accountdatediv','accountfilterselect');
			}		
		});
		//more append activity data-
		$("#accountsmore").click(function(){
			var account_id=$('#lastaccountid').val();
			if(account_id > 0){
				var uitype=$("#accountfields").find('option:selected').data('uitype');					
				var columnfield=$("#accountfields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#accountfields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#accountfilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#accountfilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#accountfiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getmoreoutput('account',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,account_id,uitype);
				if(rdata.length > 0){
					accountwidgetcontent('append'+appendclass+'',rdata,accounttoolbar,reloadid);					
				}
			}
		});
		//Filter submit results
		$("#filtersubmit"+id+"").click(function(){
			if(checkVariable('accountfields') == true ){
				var uitype=$("#accountfields").find('option:selected').data('uitype');					
				var columnfield=$("#accountfields").find('option:selected').data('dbcolumn');
				var columnfieldtwo=$("#accountfields").find('option:selected').data('viewcreationjoincolmodelname');
				var clause='';				
				if(uitype == 8){ //date	
					var filtervalue=$('#accountfilterdate').val();
					var col_field=columnfield;
					var clause='like';
				}
				else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
					var filtervalue=$('#accountfilterselect').val();	
					var col_field=columnfieldtwo;
				}
				else { //text box	
					var filtervalue=$('#accountfiltertext').val();
					var col_field=columnfield;
					var clause='like';
				}				
				//retrieve filter data
				var rdata=getfilteroutput('account',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
				$('#append'+appendclass+'').empty();
				if(rdata.length > 0){
					accountwidgetcontent('append'+appendclass+'',rdata,accounttoolbar,reloadid);					
				} else {
						//append empty set
						$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
				}
				$("#accountfilterbtn"+id+"").trigger('click');
				$("#accountfiltertext,#accountfilterdate").val('');
				$("#accountfields,#accountfilterselect").select2('val','').trigger('change');
				$("#accountfilterselect").empty();
			}
			else{
				alertpopup('Please Give All Filter Values');
			}
		});
		//For Filter 
		$( "#accountfilterbtn"+id+"" ).click(function(){
			$( "#accountfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
			{//load filter fields
				if(account_filter_status == 0){
					//For Dropdown
					$(".chzn-select").select2({containerCss: {display: "block"}});
					//load account fields
					loadfilterfield('accountfields',202,'account');
					account_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
				}
			}
		});
		// Date validation
		var dateformetdata = $('#accountfilterdate').attr('data-dateformater');
		$('#accountfilterdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: 0,
			onSelect: function () {
				$(this).removeClass('error');
			}
		});	
			},interval); 
		});		
	}
}
{// Widget Creation Tickets
	function ticketswidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,tickettoolbar) {
		var datacheck="";
		var datalist = "";
		ticket_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Ticket">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+tickettoolbar[2]+'"><span title="Add" class=" widgetredirecticonticket widgettextbtn" data-redirecturl="Ticket">ADD</span></span><span title="Filter" class="widgettextbtn" id="ticketfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="ticketsfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="ticketsfields" class="chzn-select"></select></div><div class="large-6 columns" id="ticketstextdiv"><label>Filter Value</label><input type="text" id="ticketsfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="ticketsdddiv"><label>Filter Value</label><select id="ticketsfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="ticketsdatediv"><label>Filter Value</label><input type="text" id="ticketsfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="ticketsmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastticketid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0) {
			ticketswidgetcontent('append'+appendclass+'',data,tickettoolbar,reloadid);  
		}else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#ticketsmore').empty();
		}
		//For Add 
		$(".widgetredirecticonticket").click(function(){
		{// For Redirection Tickets Add
			sessionStorage.setItem("ticketsaddsrc",'formhome');
			sessionStorage.setItem("widgetid",24);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#ticketreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});	
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#ticketsfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('ticketsfields',227,'ticketstextdiv','ticketsdddiv','ticketsdatediv','ticketsfilterselect');
					}		
				});
				//more append activity data-
				$("#ticketsmore").click(function(){
					var ticket_id=$('#lastticketid').val();
					if(ticket_id > 0){
						var uitype=$("#ticketsfields").find('option:selected').data('uitype');					
						var columnfield=$("#ticketsfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#ticketsfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#ticketsfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#ticketsfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#ticketsfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('ticket',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,ticket_id,uitype);
						if(rdata.length > 0){
							ticketswidgetcontent('append'+appendclass+'',rdata,tickettoolbar,reloadid);					
						}
					}
				});			
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('ticketsfields') == true ){
						var uitype=$("#ticketsfields").find('option:selected').data('uitype');					
						var columnfield=$("#ticketsfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#ticketsfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#ticketsfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#ticketsfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#ticketsfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('ticket',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							ticketswidgetcontent('append'+appendclass+'',rdata,tickettoolbar,reloadid);					
						} else {
						//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#ticketfilterbtn"+id+"").trigger('click');
						$("#ticketsfiltertext,#ticketsfilterdate").val('');
						$("#ticketsfields,#ticketsfilterselect").select2('val','').trigger('change');
						$("#ticketsfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});
				//For Filter 
				$( "#ticketfilterbtn"+id+"" ).click(function(){
					$( "#ticketsfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(ticket_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load lead fields
							loadfilterfield('ticketsfields',227,'ticket');
							ticket_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});				
				// Date validation
				var dateformetdata = $('#ticketsfilterdate').attr('data-dateformater');
				$('#ticketsfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Campaign
	function campaignwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,campaigntoolbar) {
		var datacheck="";
		var datalist = "";
		campaign_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Campaign">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+campaigntoolbar[2]+'"><span title="Add" class="widgetredirecticoncampaign widgetviewicon" data-redirecturl="Campaign"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="campaignfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="campaignfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="campaignfields" class="chzn-select"></select></div><div class="large-6 columns" id="campaigntextdiv"><label>Filter Value</label><input type="text" id="campaignfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="campaigndddiv"><label>Filter Value</label><select id="campaignfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="campaigndatediv"><label>Filter Value</label><input type="text" id="campaignfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="campaignmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastcampaignid"></input></span>');	
		//intial dataloading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			campaignwidgetcontent('append'+appendclass+'',data,campaigntoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#campaignmore').empty();
		}
		//For Add 
		$(".widgetredirecticoncampaign").click(function(){
		{// For Redirection Campaign Add
			sessionStorage.setItem("campaignaddsrc",'formhome');
			sessionStorage.setItem("widgetid",25);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#campaignreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#campaignfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('campaignfields',213,'campaigntextdiv','campaigndddiv','campaigndatediv','campaignfilterselect');
					}		
				});	
				//more append activity data-
				$("#campaignmore").click(function(){
					var campaign_id=$('#lastcampaignid').val();
					if(campaign_id > 0){
						var uitype=$("#campaignfields").find('option:selected').data('uitype');					
						var columnfield=$("#campaignfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#campaignfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#campaignfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#campaignfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#campaignfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('campaign',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,campaign_id,uitype);
						if(rdata.length > 0){
							campaignwidgetcontent('append'+appendclass+'',rdata,campaigntoolbar,reloadid);					
						}
					}
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('campaignfields') == true ){
						var uitype=$("#campaignfields").find('option:selected').data('uitype');					
						var columnfield=$("#campaignfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#campaignfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#campaignfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#campaignfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#campaignfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('campaign',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							campaignwidgetcontent('append'+appendclass+'',rdata,campaigntoolbar,reloadid);					
						} else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#campaignfilterbtn"+id+"").trigger('click');
						$("#campaignfiltertext,#campaignfilterdate").val('');
						$("#campaignfields,#campaignfilterselect").select2('val','').trigger('change');
						$("#campaignfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});
				//For Filter 
				$( "#campaignfilterbtn"+id+"" ).click(function(){
					$( "#campaignfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(campaign_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load campaign fields
							loadfilterfield('campaignfields',213,'campaign');
							campaign_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				// Date validation
				var dateformetdata = $('#campaignfilterdate').attr('data-dateformater');
				$('#campaignfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Sales Order
	function salesordergetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,sotoolbar) {
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Salesorder">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+sotoolbar[2]+'"><span title="Add" class=" widgetredirecticonso widgettextbtn" data-redirecturl="Salesorder">ADD</span></span><span title="Filter" class="widgettextbtn" id="salesorderfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="salesorderfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="salesorderfields" class="chzn-select"></select></div><div class="large-6 columns" id="salesordertextdiv"><label>Filter Value</label><input type="text" id="salesorderfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="salesorderdddiv"><label>Filter Value</label><select id="salesorderfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="salesorderdatediv"><label>Filter Value</label><input type="text" id="salesorderfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="salesordermore" class="widgetmorestyle" style="">More<input type="hidden" id="lastsalesorderid"></input></span>');
		//intial dataloading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			salesorderwidgetcontent('append'+appendclass+'',data,sotoolbar,reloadid);
		}
		//For Add 
		$(".widgetredirecticonso").click(function(){
		{// For Redirection Sales order Add
			sessionStorage.setItem("salesorderaddsrc",'formhome');
			sessionStorage.setItem("widgetid",26);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#salesorderreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#salesorderfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('salesorderfields',217,'salesordertextdiv','salesorderdddiv','salesorderdatediv','salesorderfilterselect');
					}		
				});
				//more append activity data-
				$("#salesordermore").click(function(){
					var salesorder_id=$('#lastsalesorderid').val();
					if(salesorder_id > 0){
						var uitype=$("#salesorderfields").find('option:selected').data('uitype');					
						var columnfield=$("#salesorderfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#salesorderfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#salesorderfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#salesorderfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#salesorderfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('salesorder',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,salesorder_id,uitype);
						if(rdata.length > 0){
							salesorderwidgetcontent('append'+appendclass+'',rdata,sotoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('salesorderfields') == true ){
						var uitype=$("#salesorderfields").find('option:selected').data('uitype');					
						var columnfield=$("#salesorderfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#salesorderfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#salesorderfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#salesorderfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#salesorderfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('salesorder',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							salesorderwidgetcontent('append'+appendclass+'',rdata,sotoolbar,reloadid);					
						} else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#salesorderfilterbtn"+id+"").trigger('click');
						$("#salesorderfiltertext,#salesorderfilterdate").val('');
						$("#salesorderfields,#salesorderfilterselect").select2('val','').trigger('change');
						$("#salesorderfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});		
				//For Filter 
				$( "#salesorderfilterbtn"+id+"" ).click(function(){
					$( "#salesorderfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(so_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load sales order fields
							loadfilterfield('salesorderfields',217,'salesorder');
							so_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#salesorderfilterdate').attr('data-dateformater');
				$('#salesorderfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});
			},interval); 
		});			
	}
}
{// Widget Creation Purchase Order
	function purchaseordergetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,potoolbar) {
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Purchaseorder">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+potoolbar[2]+'"><span title="Add" class=" widgetredirecticonpo widgettextbtn" data-redirecturl="Purchaseorder">ADD</span></span><span title="Filter" class="widgettextbtn" id="purchaseorderfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="purchaseorderfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="purchaseorderfields" class="chzn-select"></select></div><div class="large-6 columns" id="purchaseordertextdiv"><label>Filter Value</label><input type="text" id="purchaseorderfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="purchaseorderdddiv"><label>Filter Value</label><select id="purchaseorderfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="purchaseorderdatediv"><label>Filter Value</label><input type="text" id="purchaseorderfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="purchaseordermore" class="widgetmorestyle" style="">More<input type="hidden" id="lastpurchaseorderid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			purchaseorderwidgetcontent('append'+appendclass+'',data,potoolbar,reloadid);
		}
		//For Add 
		$(".widgetredirecticonpo").click(function(){
			{// For Redirection Purchase order Add
				sessionStorage.setItem("purchaseorderaddsrc",'formhome');
				sessionStorage.setItem("widgetid",27);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#purchaseorderreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#purchaseorderfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('purchaseorderfields',225,'purchaseordertextdiv','purchaseorderdddiv','purchaseorderdatediv','purchaseorderfilterselect');
					}		
				});
				//more append activity data-
				$("#purchaseordermore").click(function(){
					var purchaseorder_id=$('#lastpurchaseorderid').val();
					if(purchaseorder_id > 0){
						var uitype=$("#purchaseorderfields").find('option:selected').data('uitype');					
						var columnfield=$("#purchaseorderfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#purchaseorderfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#purchaseorderfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#purchaseorderfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#purchaseorderfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('purchaseorder',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,purchaseorder_id,uitype);
						if(rdata.length > 0){
							purchaseorderwidgetcontent('append'+appendclass+'',rdata,potoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('purchaseorderfields') == true ){
						var uitype=$("#purchaseorderfields").find('option:selected').data('uitype');					
						var columnfield=$("#purchaseorderfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#purchaseorderfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#purchaseorderfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#purchaseorderfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#purchaseorderfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('purchaseorder',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							purchaseorderwidgetcontent('append'+appendclass+'',rdata,potoolbar,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#purchaseorderfilterbtn"+id+"").trigger('click');
						$("#purchaseorderfiltertext,#purchaseorderfilterdate").val('');
						$("#purchaseorderfields,#purchaseorderfiltertext").select2('val','').trigger('change');
						$("#purchaseorderfiltertext").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#purchaseorderfilterbtn"+id+"" ).click(function(){
					$( "#purchaseorderfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(po_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load purchase order fields
							loadfilterfield('purchaseorderfields',225,'purchaseorder');
							po_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#purchaseorderfilterdate').attr('data-dateformater');
				$('#purchaseorderfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Payment
	function paymentwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,paymenttoolbar) {
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Payment">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+paymenttoolbar[2]+'"><span title="Add" class=" widgetredirecticonpo widgettextbtn" data-redirecturl="Payment">ADD</span></span><span title="Filter" class="widgettextbtn" id="paymentfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="paymentfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="paymentfields" class="chzn-select"></select></div><div class="large-6 columns" id="paymenttextdiv"><label>Filter Value</label><input type="text" id="paymentfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="paymentdiv"><label>Filter Value</label><select id="paymentfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="paymentdatediv"><label>Filter Value</label><input type="text" id="paymentfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="paymentmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastpaymentid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			paymentwidgetcontent('append'+appendclass+'',data,paymenttoolbar,reloadid,moduleid);
		}
		//For Add 
		$(".widgetredirecticonpo").click(function(){
			{// For Redirection Purchase order Add
				sessionStorage.setItem("paymentaddsrc",'formhome');
				sessionStorage.setItem("widgetid",27);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#paymentreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#paymentfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('paymentfields',28,'paymenttextdiv','paymentdiv','paymentdatediv','paymentfilterselect');
					}		
				});
				//more append activity data-
				$("#paymentmore").click(function(){
					var purchaseorder_id=$('#lastpaymentid').val();
					if(purchaseorder_id > 0){
						var uitype=$("#paymentfields").find('option:selected').data('uitype');					
						var columnfield=$("#paymentfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#paymentfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#paymentfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#paymentfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#paymentfilterselect').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('payment',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,purchaseorder_id,uitype);
						if(rdata.length > 0){
							paymentwidgetcontent('append'+appendclass+'',rdata,paymenttoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('paymentfields') == true ){
						var uitype=$("#paymentfields").find('option:selected').data('uitype');					
						var columnfield=$("#paymentfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#paymentfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#paymentfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#paymentfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#paymentfilterselect').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('payment',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							paymentwidgetcontent('append'+appendclass+'',rdata,paymenttoolbar,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#paymentfilterbtn"+id+"").trigger('click');
						$("#paymentfiltertext,#purchaseorderfilterdate").val('');
						$("#paymentfields,#paymentfiltertext").select2('val','').trigger('change');
						$("#paymentfiltertext").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#paymentfilterbtn"+id+"" ).click(function(){
					$( "#paymentfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(po_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load purchase order fields
							loadfilterfield('paymentfields',28,'payment');
							po_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#paymentfilterdate').attr('data-dateformater');
				$('#paymentfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Material Requisition
	function materialrequisitiongetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,mrtoolbar) {
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Materialrequisition">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+mrtoolbar[2]+'"><span title="Add" class=" widgetredirecticonmr widgettextbtn" data-redirecturl="Materialrequisition">ADD</span></span><span title="Filter" class="widgettextbtn" id="materialrequisitionfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="materialrequisitionfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="materialrequisitionfields" class="chzn-select"></select></div><div class="large-6 columns" id="materialrequisitiontextdiv"><label>Filter Value</label><input type="text" id="materialrequisitionfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="materialrequisitiondddiv"><label>Filter Value</label><select id="materialrequisitionfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="materialrequisitiondatediv"><label>Filter Value</label><input type="text" id="materialrequisitionfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="materialrequisitionmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastmaterialrequisitionid"></input></span>');
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			materialrequisitionwidgetcontent('append'+appendclass+'',data,mrtoolbar,reloadid);
		}
		//For Add 
		$(".widgetredirecticonmr").click(function(){
		{// For Redirection Purchase order Add
			sessionStorage.setItem("materialrequisitionaddsrc",'formhome');
			sessionStorage.setItem("widgetid",34);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#materialrequisitionreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		//For Icon Hover 
		$( ".dsbtaskblock" ).hover(function() {
				$(this).find('.iconset').removeClass('hidedisplay');
			}, function() {
				$(this).find('.iconset').addClass('hidedisplay');
		});
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#materialrequisitionfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('materialrequisitionfields',249,'materialrequisitiontextdiv','materialrequisitiondddiv','materialrequisitiondatediv','materialrequisitionfilterselect');
					}		
				});
				//more append activity data-
				$("#materialrequisitionmore").click(function(){
					var mr_id=$('#lastmaterialrequisitionid').val();
					if(mr_id > 0){
						var uitype=$("#materialrequisitionfields").find('option:selected').data('uitype');					
						var columnfield=$("#materialrequisitionfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#materialrequisitionfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#materialrequisitionfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#materialrequisitionfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#materialrequisitionfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('materialrequisition',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,mr_id,uitype);
						if(rdata.length > 0){
							materialrequisitionwidgetcontent('append'+appendclass+'',rdata,mrtoolbar,reloadid);					
						}
					}
				});
				//Filter submit results materialrequisition
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('materialrequisitionfields') == true ){
						var uitype=$("#materialrequisitionfields").find('option:selected').data('uitype');					
						var columnfield=$("#materialrequisitionfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#materialrequisitionfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#materialrequisitionfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#materialrequisitionfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#materialrequisitionfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('materialrequisition',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							materialrequisitionwidgetcontent('append'+appendclass+'',rdata,mrtoolbar,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#materialrequisitionfilterbtn"+id+"").trigger('click');
						$("#materialrequisitionfiltertext,#materialrequisitionfilterdate").val('');
						$("#materialrequisitionfields,#materialrequisitionfiltertext").select2('val','').trigger('change');
						$("#materialrequisitionfiltertext").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#materialrequisitionfilterbtn"+id+"" ).click(function(){
					$( "#materialrequisitionfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields-
						if(materialreq_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load purchase order fields
							loadfilterfield('materialrequisitionfields',249,'materialrequisition');
							materialreq_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				// Date validation
				var dateformetdata = $('#materialrequisitionfilterdate').attr('data-dateformater');
				$('#materialrequisitionfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Solution
	function solutionwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,soltoolbar) {
		var datacheck="";
		var datalist = "";
		solution_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Solutions">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+soltoolbar[2]+'"><span title="Add" class=" widgetredirecticonsolution widgettextbtn" data-redirecturl="Solutions">ADD</span></span><span title="Filter" class="widgettextbtn" id="solutionfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="solutionfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="solutionfields" class="chzn-select"></select></div><div class="large-6 columns" id="solutiontextdiv"><label>Filter Value</label><input type="text" id="solutionfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="solutiondddiv"><label>Filter Value</label><select id="solutionfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="solutiondatediv"><label>Filter Value</label><input type="text" id="solutionfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="solutionsmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastsolutionsid"></input></span>');			
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			solutionwidgetcontent('append'+appendclass+'',data,soltoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#solutionsmore').empty();
		}
		//For Add 
		$(".widgetredirecticonsolution").click(function(){
		{// For Redirection Solution Add
			sessionStorage.setItem("solutionsaddsrc",'formhome');
			sessionStorage.setItem("widgetid",28);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#solutionreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#solutionfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('solutionfields',229,'solutiontextdiv','solutiondddiv','solutiondatediv','solutionfilterselect');
					}		
				});
				//more append activity data-
				$("#solutionsmore").click(function(){
					var solution_id=$('#lastsolutionsid').val();
					if(solution_id > 0){
						var uitype=$("#solutionfields").find('option:selected').data('uitype');					
						var columnfield=$("#solutionfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#solutionfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#solutionfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#solutionfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#solutionfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('knowledgebase',col_field,filtervalue,clause,dashboardwidgetid,solution_id,uitype);
						if(rdata.length > 0){
							solutionwidgetcontent('append'+appendclass+'',rdata,soltoolbar,reloadid);					
						}
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('solutionfields') == true ){
						var uitype=$("#solutionfields").find('option:selected').data('uitype');					
						var columnfield=$("#solutionfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#solutionfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#solutionfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#solutionfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#solutionfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('knowledgebase',col_field,filtervalue,clause,dashboardwidgetid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							solutionwidgetcontent('append'+appendclass+'',rdata,soltoolbar,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#solutionfilterbtn"+id+"").trigger('click');
						$("#solutionfiltertext,#solutionfilterdate").val('');
						$("#solutionfields,#solutionfilterselect").select2('val','').trigger('change');
						$("#solutionfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#solutionfilterbtn"+id+"" ).click(function(){
					$( "#solutionfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(solution_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load solution fields
							loadfilterfield('solutionfields',229,'knowledgebase');
							solution_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});				
				// Date validation
				var dateformetdata = $('#solutionfilterdate').attr('data-dateformater');
				$('#solutionfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Solution Category
	function solutioncategorywidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,socatltoolbar) {
		var datacheck="";
		var datalist = "";
		solutioncat_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Solutionscategory">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+socatltoolbar[2]+'"><span title="Add" class=" widgetredirecticonsolutioncat widgettextbtn" data-redirecturl="Solutionscategory">ADD</span></span><span title="Filter" class="widgettextbtn" id="solutioncatfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass"  style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="solutioncatfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="solutioncatfields" class="chzn-select"></select></div><div class="large-6 columns" id="solutioncattextdiv"><label>Filter Value</label><input type="text" id="solutioncatfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="solutioncatdddiv"><label>Filter Value</label><select id="solutioncatfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="solutioncatdatediv"><label>Filter Value</label><input type="text" id="solutioncatfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="solutioncatmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastsolutioncatid"></input></span>');		
		//intial dataloading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			solutioncatwidgetcontent('append'+appendclass+'',data,socatltoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#solutioncatmore').empty();
		}
		//For Add 
		$(".widgetredirecticonsolutioncat").click(function(){
			{// For Redirection Solution Add
				sessionStorage.setItem("solutionscataddsrc",'formhome');
				sessionStorage.setItem("widgetid",29);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#solutioncatreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});	
		$(function() {
			setTimeout(function(){
				//more append activity data-
				$("#solutioncatmore").click(function(){
					var solution_id=$('#lastsolutioncatid').val();
					if(solution_id > 0){
						var uitype=$("#solutioncatfields").find('option:selected').data('uitype');					
						var columnfield=$("#solutioncatfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#solutioncatfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#solutioncatfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#solutioncatfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#solutioncatfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}
						//retrieve filter data
						var rdata=getmoreoutput('knowledgebasecategory',col_field,filtervalue,clause,dashboardwidgetid,solution_id,uitype);
						if(rdata.length > 0){
							solutioncatwidgetcontent('append'+appendclass+'',rdata,socatltoolbar,reloadid);					
						}						
					}
				});
				//Load Options on uitype
				$("#solutioncatfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('solutioncatfields',228,'solutioncattextdiv','solutioncatdddiv','solutioncatdatediv','solutioncatfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('solutioncatfields') == true ){
						var uitype=$("#solutioncatfields").find('option:selected').data('uitype');					
						var columnfield=$("#solutioncatfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#solutioncatfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#solutioncatfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#solutioncatfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#solutioncatfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('knowledgebasecategory',col_field,filtervalue,clause,dashboardwidgetid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							solutioncatwidgetcontent('append'+appendclass+'',rdata,socatltoolbar,reloadid);					
						}  else {
							//append empty set
							$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#solutioncatfilterbtn"+id+"").trigger('click');
						$("#solutioncatfiltertext,#solutioncatfilterdate").val('');
						$("#solutioncatfields,#solutioncatfilterselect").select2('val','').trigger('change');
						$("#solutioncatfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});
				//For Filter 
				$( "#solutioncatfilterbtn"+id+"" ).click(function(){
					$( "#solutioncatfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(solutioncat_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load Solution Category fields
							loadfilterfield('solutioncatfields',228,'knowledgebasecategory');
							solutioncat_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				
				// Date validation
				var dateformetdata = $('#solutioncatfilterdate').attr('data-dateformater');
				$('#solutioncatfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});	
	}		
}

{// Widget Creation Contract
	function contractwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,contracttoolbar){
		var datacheck="";
		var datalist = "";
		contract_filter_status=0;
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Contract">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+contracttoolbar[2]+'"><span title="Add" class=" widgetredirecticoncontract widgettextbtn" data-redirecturl="Contract">ADD</span></span><span title="Filter" class="widgettextbtn" id="contractfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="contractfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="contractfields" class="chzn-select"></select></div><div class="large-6 columns" id="contracttextdiv"><label>Filter Value</label><input type="text" id="contractfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="contractdddiv"><label>Filter Value</label><select id="contractfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="contractdatediv"><label>Filter Value</label><input type="text" id="contractfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div><div><span id="contractsmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastcontractid"></input></span>');		
		//initial data loading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			contractwidgetcontent('append'+appendclass+'',data,contracttoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#contractsmore').empty();
		}
		//For Add 
		$(".widgetredirecticoncontract").click(function(){
			{// For Redirection Contract Add
				sessionStorage.setItem("contractaddsrc",'formhome');
				sessionStorage.setItem("widgetid",30);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#contractreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//Load Options on uitype
				$("#contractfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('contractfields',230,'contracttextdiv','contractdddiv','contractdatediv','contractfilterselect');
					}		
				});
				//more append activity data-
				$("#contractsmore").click(function(){
					var contract_id=$('#lastcontractid').val();
					if(contract_id > 0){
						var uitype=$("#contractfields").find('option:selected').data('uitype');					
						var columnfield=$("#contractfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#contractfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#contractfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#contractfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#contractfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}	
						//retrieve filter data
						var rdata=getmoreoutput('contract',col_field,filtervalue,clause,dashboardwidgetid,contract_id,uitype);
						if(rdata.length > 0){
							contractwidgetcontent('append'+appendclass+'',rdata,contracttoolbar,reloadid);					
						}							
					}
				});
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('contractfields') == true ){
						var uitype=$("#contractfields").find('option:selected').data('uitype');					
						var columnfield=$("#contractfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#contractfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#contractfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#contractfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#contractfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('contract',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							contractwidgetcontent('append'+appendclass+'',rdata,contracttoolbar,reloadid);					
						}
						$("#contractfilterbtn"+id+"").trigger('click');
						$("#contractfiltertext,#contractfilterdate").val('');
						$("#contractfields,#contractfilterselect").select2('val','').trigger('change');
						$("#contractfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});	
				//For Filter 
				$( "#contractfilterbtn"+id+"" ).click(function(){
					$( "#contractfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(contract_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load contract fields
							loadfilterfield('contractfields',230,'contract');
							contract_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});		
				// Date validation
				var dateformetdata = $('#contractfilterdate').attr('data-dateformater');
				$('#contractfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{// Widget Creation Lead
	function leadwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat,leadtoolbar){
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Lead">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span class="'+leadtoolbar[2]+'"><span title="Add" class="widgetredirecticonlead widgetviewicon" data-redirecturl="Lead"><i class="material-icons">add</i></span></span><span title="Filter" class="widgetfiltericon" id="leadfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="leadfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="leadfields" class="chzn-select"></select></div><div class="large-6 columns" id="leadtextdiv"><label>Filter Value</label><input type="text" id="leadfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="leaddddiv"><label>Filter Value</label><select id="leadfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="leaddatediv"><label>Filter Value</label><input type="text" id="leadfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div class="clearfix"></div><ul id="append'+appendclass+'" class="member-list"></ul></div></div><span id="leadmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastleadid"></input></span>');		
		//intial dataloading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			leadwidgetcontent('append'+appendclass+'',data,leadtoolbar,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<li style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</li>');
			$('#leadmore').empty();
		}
		//For Add 
		$(".widgetredirecticonlead").click(function(){
		{// For Redirection Lead Add
			sessionStorage.setItem("leadaddsrc",'formhome');
			sessionStorage.setItem("widgetid",31);
			sessionStorage.setItem("moduleid",moduleid);
			sessionStorage.setItem("rowid",dashboardrowid);
		}
		var fullurl = $(this).data('redirecturl');
		window.location = base_url+fullurl;
		});
		//For Reload 
		$("#leadreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});		
		$(function() {
			setTimeout(function(){
				//more append activity data-
				$("#leadmore").click(function(){
					var lead_id=$('#lastleadid').val();
					if(lead_id > 0){
						var uitype=$("#leadfields").find('option:selected').data('uitype');					
						var columnfield=$("#leadfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#leadfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#leadfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#leadfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#leadfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('lead',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,lead_id,uitype);
						if(rdata.length > 0){
							leadwidgetcontent('append'+appendclass+'',rdata,leadtoolbar,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#leadfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('leadfields',201,'leadtextdiv','leaddddiv','leaddatediv','leadfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('leadfields') == true ){
						var uitype=$("#leadfields").find('option:selected').data('uitype');					
						var columnfield=$("#leadfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#leadfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#leadfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#leadfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#leadfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('lead',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							leadwidgetcontent('append'+appendclass+'',rdata,leadtoolbar,reloadid);					
						} else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#leadfilterbtn"+id+"").trigger('click');
						$("#leadfiltertext,#leadfilterdate").val('');
						$("#leadfields,#leadfilterselect").select2('val','').trigger('change');
						$("#leadfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});		
				//For Filter 
				$( "#leadfilterbtn"+id+"" ).click(function(){
					$( "#leadfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(lead_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load lead fields
							loadfilterfield('leadfields',201,'lead');
							lead_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});				
				// Date validation
				var dateformetdata = $('#leadfilterdate').attr('data-dateformater');
				$('#leadfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});
			},interval); 
		});
	}
}
{// Widget Creation Product
	function productwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat){
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticon" data-redirecturl="Product">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Add" class="widgetredirecticonproduct widgetviewicon" data-redirecturl="Product"><i class="material-icons">add</i></span><span title="Filter" class="widgetfiltericon" id="productfilterbtn'+id+'"><i class="material-icons">filter_list</i></span></div></div><div class="large-12 columns scrollbarclass" style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="productfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="productfields" class="chzn-select"></select></div><div class="large-6 columns" id="producttextdiv"><label>Filter Value</label><input type="text" id="productfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="productdddiv"><label>Filter Value</label><select id="productfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="productdatediv"><label>Filter Value</label><input type="text" id="productfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="productmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastproductid"></input></span>');
		//intial dataloading call
		var fieldlength = data.length;
		if(fieldlength > 0){
			productwidgetcontent('append'+appendclass+'',data,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<li style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</li>');
			$('#productmore').empty();
		}
		//For Add
		$(".widgetredirecticonproduct").click(function(){
			{// For Redirection Lead Add
				sessionStorage.setItem("productaddsrc",'formhome');
				sessionStorage.setItem("widgetid",35);
				sessionStorage.setItem("moduleid",moduleid);
				sessionStorage.setItem("rowid",dashboardrowid);
			}
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		//For Reload 
		$("#productreloadbtn"+id+"").click(function(){
			$('#'+reloadid+'').trigger('click');
		});
		
		//For View 
		$( ".widgetviewredirecticon" ).click(function(){
			var fullurl = $(this).data('redirecturl');
			window.location = base_url+fullurl;
		});
		$(function() {
		    setTimeout(function(){
				//more append activity data-product
				$("#productmore").click(function(){
					var lead_id=$('#lastproductid').val();
					if(lead_id > 0){
						var uitype=$("#productfields").find('option:selected').data('uitype');					
						var columnfield=$("#productfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#productfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#productfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#productfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#productfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('product',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,lead_id,uitype);
						if(rdata.length > 0){
							productwidgetcontent('append'+appendclass+'',rdata,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#productfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('productfields',9,'producttextdiv','productdddiv','productdatediv','productfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('productfields') == true ){
						var uitype=$("#productfields").find('option:selected').data('uitype');					
						var columnfield=$("#productfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#productfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#productfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#productfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#productfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('product',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							productwidgetcontent('append'+appendclass+'',rdata,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#productfilterbtn"+id+"").trigger('click');
						$("#productfiltertext,#productfilterdate").val('');
						$("#productfields,#productfilterselect").select2('val','').trigger('change');
						$("#productfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});		
				//For Filter 
				$( "#productfilterbtn"+id+"" ).click(function(){
					$( "#productfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(product_filter_status == 0){	
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load lead fields
							loadfilterfield('productfields',9,'product');
							product_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});
				
				// Date validation
				var dateformetdata = $('#productfilterdate').attr('data-dateformater');
				$('#productfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});
			},interval); 
		});
	}
}
{// Widget Creation Pricebook
	function pricebookwidgetcreation(widgettitle,appendid,appendclass,data,reloadid,id,dashboardwidgetid,dashboardrowid,interval,moduleid,dateformat){
		var datacheck="";
		var datalist = "";
		$("#"+appendid+"").append('<div class="large-12 columns paddingzero"><div class="large-6 medium-6 small-6 columns"><span class="dsbtaskcaption widgetviewredirecticonpb" data-redirecturl="Pricebooksmaster">'+widgettitle+'</span></div><div class="large-6 medium-6 small-6 columns" style="text-align:right;line-height:2.3rem;"><span title="Add" class="widgetredirecticonpb widgettextbtn" data-redirecturl="Pricebooksmaster">ADD</span><span title="Filter" class="widgettextbtn" id="pricebookfilterbtn'+id+'">FILTER</span></div></div><div class="large-12 columns scrollbarclass"  style="height: 16rem;overflow-x:hidden"><div id="'+appendclass+'" class="widgetwrapper large-12 columns"><div id="pricebookfiltercontainer" class="hidedisplay"><div class="large-6 columns"><label>Filter By</label><select id="pricebookfields" class="chzn-select"></select></div><div class="large-6 columns" id="pricebooktextdiv"><label>Filter Value</label><input type="text" id="pricebookfiltertext" placeholder="Filter Data"></div><div class="large-6 columns hidedisplay" id="pricebookdddiv"><label>Filter Value</label><select id="pricebookfilterselect" class="chzn-select"></select></div><div class="large-6 columns hidedisplay" id="pricebookdatediv"><label>Filter Value</label><input type="text" id="pricebookfilterdate" class="fordatepicicon" data-dateformater="'+dateformat+'"></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns" style="text-align:right"><input type="button" class="dbfiltersubmit btn" value="Submit" id="filtersubmit'+id+'"></div><div class="large-12 columns">&nbsp;</div></div><div id="append'+appendclass+'"></div></div></div><span id="pricebookmore" class="widgetmorestyle" style="">More<input type="hidden" id="lastpricebookid"></input></span>');		
		//intial dataloading call
		var fieldlength = data.length;		
		if(fieldlength > 0){
			pricebookwidgetcontent('append'+appendclass+'',data,reloadid);
		} else {
			//append empty set
			$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
			$('#pricebookmore').empty();
		}
		$(function() {
		    setTimeout(function(){
				//more append activity data-pricebook
				$("#pricebookmore").click(function(){
					var lead_id=$('#lastpricebookid').val();
					if(lead_id > 0){
						var uitype=$("#pricebookfields").find('option:selected').data('uitype');					
						var columnfield=$("#pricebookfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#pricebookfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#pricebookfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#pricebookfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#pricebookfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getmoreoutput('pricebook',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,lead_id,uitype);
						if(rdata.length > 0){
							pricebookwidgetcontent('append'+appendclass+'',rdata,reloadid);					
						}
					}
				});
				//Load Options on uitype
				$("#pricebookfields").change(function(){
					var value=$(this).val();
					if(checkValue(value) == true){			
						filtervalueload('pricebookfields',218,'pricebooktextdiv','pricebookdddiv','pricebookdatediv','pricebookfilterselect');
					}		
				});	
				//Filter submit results
				$("#filtersubmit"+id+"").click(function(){
					if(checkVariable('pricebookfields') == true ){
						var uitype=$("#pricebookfields").find('option:selected').data('uitype');					
						var columnfield=$("#pricebookfields").find('option:selected').data('dbcolumn');
						var columnfieldtwo=$("#pricebookfields").find('option:selected').data('viewcreationjoincolmodelname');
						var clause='';				
						if(uitype == 8){ //date	
							var filtervalue=$('#pricebookfilterdate').val();
							var col_field=columnfield;
							var clause='like';
						}
						else if(uitype == 17 || uitype == 20 ||uitype == 19 ||uitype == 18){ //uitype				
							var filtervalue=$('#pricebookfilterselect').val();	
							var col_field=columnfieldtwo;
						}
						else { //text box	
							var filtervalue=$('#pricebookfiltertext').val();
							var col_field=columnfield;
							var clause='like';
						}				
						//retrieve filter data
						var rdata=getfilteroutput('pricebook',col_field,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype);
						$('#append'+appendclass+'').empty();
						if(rdata.length > 0){
							pricebookwidgetcontent('append'+appendclass+'',rdata,reloadid);					
						}  else {
								//append empty set
								$("#append"+appendclass+"").append('<div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div>');
						}
						$("#pricebookfilterbtn"+id+"").trigger('click');
						$("#pricebookfiltertext,#pricebookfilterdate").val('');
						$("#pricebookfields,#pricebookfilterselect").select2('val','').trigger('change');
						$("#pricebookfilterselect").empty();
					}
					else{
						alertpopup('Please Give All Filter Values');
					}
				});		
				//For Add 
				$(".widgetredirecticonpb").click(function(){
				{// For Redirection Lead Add
					sessionStorage.setItem("pricebookaddsrc",'formhome');
					sessionStorage.setItem("widgetid",33);
					sessionStorage.setItem("moduleid",moduleid);
					sessionStorage.setItem("rowid",dashboardrowid);
				}
				var fullurl = $(this).data('redirecturl');
				window.location = base_url+fullurl;
				});
				//For Reload 
				$("#pricebookreloadbtn"+id+"").click(function(){
					$('#'+reloadid+'').trigger('click');
				});
				
				//For View 
				$( ".widgetviewredirecticonpb" ).click(function(){
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Filter 
				$( "#pricebookfilterbtn"+id+"" ).click(function(){
					$( "#pricebookfiltercontainer" ).slideToggle( "slow", function() {/* Animation complete. */ });
					{//load filter fields
						if(pricebook_filter_status == 0){
							//For Dropdown
							$(".chzn-select").select2({containerCss: {display: "block"}});
							//load lead fields
							loadfilterfield('pricebookfields',218,'pricebook');
							pricebook_filter_status = 1;//set to 1 after first loading of options/to avoid reloading
						}
					}
				});				
				// Date validation
				var dateformetdata = $('#pricebookfilterdate').attr('data-dateformater');
				$('#pricebookfilterdate').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						$(this).removeClass('error');
					}
				});	
			},interval); 
		});		
	}
}
{//in-line data delete
	function inlinedashboarddatadelete(tblname,tblfiledid,tblfieldval) {
		if(tblname != "" && tblfieldval != "") {
			$.ajax({
				url:base_url+"Home/inlinedatadelete",
				data:"data=&tablename="+tblname+"&tablefield="+tblfiledid+"&tablevalue="+tblfieldval,
				type:"POST",
				async:false,
				cache:false,
				success :function(msg) {
					var nmsg = $.trim(msg);
					if(nmsg == 'TRUE') {
						$("#dbdeletealertscloseyn").hide();
						alertpopup('Record deleted successfully!');
					}
				}
			});
		}
	}
}
{//conversation create
	function conversationcreatefun(databoxid,title) {
		
	}
}
{//widget reload without condition
	function widegetrelodfun(funcname,widgettitle,widgetid,widgetclass,withreloadid,id,tablename,fieldname,jointblname,joinfieldids,rowcount,dashboarddata,dashboardwidgetid,rowid,interval,moduleid,dateformat,toolbar) {		
		var dashboardboardname=dashboarddata.charttype;
		var dashboardmoduleid=dashboarddata.moduleid;
		$.ajax({
			url:base_url+"Base/basicinformationvalfetch",			
			data:{tabname:tablename,fieldname:fieldname,jointabname:jointblname,joinfiledids:joinfieldids,datacount:rowcount,dashboardboardname:dashboardboardname,rowid:rowid,dashboardmoduleid:dashboardmoduleid},
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(generaldata) {
				window[funcname](widgettitle,widgetid,widgetclass,generaldata,withreloadid,id,dashboardwidgetid,rowid,interval,moduleid,dateformat,toolbar);
			}
		});
	}
}
{/* alertpopup */
	function dbwidgetdeletealert(idname,spanid,reloadid)
	{ 
		$("#dbdeletealertscloseyn").show();
		$('.dynamicid').attr('id',idname);
		$(".dynamicid").focus();
		var tblname = $('#'+spanid+'').data('tablename');
		var tblfiledid = $('#'+spanid+'').data('tablefieldid');
		var tblfieldval = $('#'+spanid+'').data('tablefiledvalue');
		inlinedashboarddatadelete(tblname,tblfiledid,tblfieldval);
		$('#'+spanid+'').parent('.iconset').parent('.dsbtaskblock').prev('div').remove();
		$('#'+spanid+'').parent('.iconset').parent('.dsbtaskblock ').remove();
		$('#'+reloadid+'').trigger('click');
		$("#"+idname+"").unbind();
	}
}
{//Widget Creation Notifications-modulename
	function notificationswidgetcreation(widgettitle,datalen,appendid,appendclass,closeid,data,datatype) {
		for(var i=0; i<datalen;i++) {
			if(data[i]['device'] == 'phone') {
				var height = '14';
			} else {
				var height = '16';
			}
		}
		$("#"+datatype+"").addClass('active');
		var appendid=$('#notificationdivid').val();
		$("#"+appendid+"").empty();
		$("#"+appendclass+"").empty();
		var employeeurl=base_url+'Employee';
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="notification-card"><div class="notification-header"><div class="notification-title">Notifications</div><div class="notification-status"><span id="showmylist">My</span><span id="showalllist" data-activates="notificationlist" class="dropdown-button notificationstatusactive">All</span><ul id="notificationlist" class="dropdown-content"><li id="alllist" class=""><a>All</a></li><li id="showtoday" class=""><a>Today</a></li><li id="showyesterday" class=""><a>Yesterday</a></li><li id="showthisweek" class=""><a>Last Week</a></li><li id="showthismonth" class=""><a>Current Month</a></li><li id="showlastmonth" class=""><a>Last Month</a></li></ul></div></div><ul id="'+appendclass+'" class="notification-content"></ul></div><input type="hidden" name="notificationdatatype" id="notificationdatatype" value=""/></div>');
		for(var i=0; i<datalen;i++) {
			var displaydata= data[i]['message'];
			$("#"+appendclass+"").append('<li><div>'+data[i]['employeetwoname']+'</div><i class="notification-date">'+data[i]['duration']+'</i><p>'+displaydata+'<div class="large-12 columns notificationchildnode" data-notificationlogid='+data[i]['logid']+'></div></p><!--<div class="nt-status">PENDING</div>--><img class="notification-image" alt="" src="'+data[i]['imagepath']+'"></li>');
		}
		if(datalen == 0){
			$("#append"+appendclass+"").append('<li><div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div></li>');
		}
		{//notification list click events
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrain_width: false, // Does not change width of dropdown to that of the activator
				hover: false, // Activate on hover
				gutter: 0, // Spacing from edge
				belowOrigin: true, // Displays dropdown below the button
				alignment: 'right' // Displays dropdown with edge aligned to the left of button
			});
			$("#alllist").click(function() {
				$(this).addClass('notificationstatusactive');
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive');
				$('#notificationdatatype').val('all');
				$("#showalllist").text('All');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});	
			$("#showmylist").click(function() {
				$("#showmylist").addClass('notificationstatusactive');
				$("#showalllist").removeClass('notificationstatusactive');
				$('#notificationdatatype').val('my');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});	 
			$("#showtoday").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('today');
				$("#showalllist").text('Today');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showyesterday").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('yesterday');
				$("#showalllist").text('Yesteday');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showthisweek").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('thisweek');
				$("#showalllist").text('Last week');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showlastmonth").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('lastmonth');
				$("#showalllist").text('Last Month');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showthismonth").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('thismonth');
				$("#showalllist").text('Current Month');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
		}
		$(".notiredirecturl").click(function(){
			var redirecturl = $(this).attr('data-redirecturl');
			var datarowid = $(this).attr('data-datarowid');
			sessionStorage.setItem("datarowid",datarowid);
			window.location = redirecturl;	
		});
		{//load -data-onscrolls
			$("#notificationwidgetclass").scroll( function() {
				 if($('#notificationdatatype').val() != 'today'){
					 if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight()) {
						var ID=$(".notificationchildnode:last").attr("data-notificationlogid"); //its the last div node child
						loadnotificationend(appendclass,ID);
					  }
				 }
			}); 
		}
		{//Notification widget active tab
			$('.mynotificationcls').click(function() {
				$('.mynotificationcls').removeClass('active');
				$(this).addClass('active');
			});
		}
	}
	function notificationswidgetcreationmini(widgettitle,datalen,appendid,appendclass,closeid,data,datatype) { // mini notification
		for(var i=0; i<datalen;i++) {
			if(data[i]['device'] == 'phone') {
				var height = '14';
			} else {
				var height = '16';
			}
		}
		$("#"+datatype+"").addClass('active');
		var appendid=$('#notificationdivid').val();
		$("#"+appendid+"").empty();
		$("#"+appendclass+"").empty();
		var employeeurl=base_url+'Employee';
		$("#"+appendid+"").append('<div class="large-12 columns end profilewidgetheight"><div class="notification-card"><div class="notification-header"><div class="notification-title">Notifications</div><div class="notification-status"><span id="showmylist">My</span><span id="showalllist" data-activates="notificationlist" class="dropdown-button notificationstatusactive">All</span><ul id="notificationlist" class="dropdown-content"><li id="alllist" class=""><a>All</a></li><li id="showtoday" class=""><a>Today</a></li><li id="showyesterday" class=""><a>Yesterday</a></li><li id="showthisweek" class=""><a>Last Week</a></li><li id="showthismonth" class=""><a>Current Month</a></li><li id="showlastmonth" class=""><a>Last Month</a></li></ul></div></div><ul id="'+appendclass+'" class="notification-content"></ul></div><input type="hidden" name="notificationdatatype" id="notificationdatatype" value=""/></div>');
		for(var i=0; i<datalen;i++) {
			var displaydata= data[i]['message'];
			$("#"+appendclass+"").append('<li><div>'+data[i]['employeetwoname']+'</div><div class="notification-date">'+data[i]['duration']+'</div><p>'+displaydata+'<div class="large-12 columns notificationchildnode" data-notificationlogid='+data[i]['logid']+'></div></p><!--<div class="nt-status">PENDING</div>--><img class="notification-image" alt="" src="'+data[i]['imagepath']+'"></li>');
		}
		if(datalen == 0){
			$("#append"+appendclass+"").append('<li><div style="text-align:center" class="large-12 columns dsbtaskblock tasknamestyle paddingzero">No Data Available</div></li>');
		}
		{//notification list click events
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrain_width: false, // Does not change width of dropdown to that of the activator
				hover: false, // Activate on hover
				gutter: 0, // Spacing from edge
				belowOrigin: true, // Displays dropdown below the button
				alignment: 'right' // Displays dropdown with edge aligned to the left of button
			});
			$("#alllist").click(function() {
				$(this).addClass('notificationstatusactive');
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive');
				$('#notificationdatatype').val('all');
				$("#showalllist").text('All');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});	
			$("#showmylist").click(function() {
				$("#showmylist").addClass('notificationstatusactive');
				$("#showalllist").removeClass('notificationstatusactive');
				$('#notificationdatatype').val('my');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});	 
			$("#showtoday").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('today');
				$("#showalllist").text('Today');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showyesterday").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('yesterday');
				$("#showalllist").text('Yesteday');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showthisweek").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('thisweek');
				$("#showalllist").text('Last week');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showlastmonth").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('lastmonth');
				$("#showalllist").text('Last Month');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
			$("#showthismonth").click(function() {
				$("#showmylist").removeClass('notificationstatusactive');
				$("#showalllist").addClass('notificationstatusactive')
				$('#notificationdatatype').val('thismonth');
				$("#showalllist").text('Current Month');
				var ID = 0;
				loadnotificationend(appendclass,ID);
			});
		}
		$(".notiredirecturl").click(function(){
			var redirecturl = $(this).attr('data-redirecturl');
			var datarowid = $(this).attr('data-datarowid');
			sessionStorage.setItem("datarowid",datarowid);
			window.location = redirecturl;	
		});
		{//load -data-onscrolls
			$("#notificationwidgetclass").scroll( function() {
				 if($('#notificationdatatype').val() != 'today'){
					 if($(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight()) {
						var ID=$(".notificationchildnode:last").attr("data-notificationlogid"); //its the last div node child
						loadnotificationend(appendclass,ID);
					  }
				 }
			}); 
		}
		{//Notification widget active tab
			$('.mynotificationcls').click(function() {
				$('.mynotificationcls').removeClass('active');
				$(this).addClass('active');
			});
		}
	}
	function loadnotificationend(appendclass,ID) {
		var loop=0;
		var datatype = $('#notificationdatatype').val();
		var moduleid=$('#notificationdivid').attr('data-moduleid');
		var rowid=$('#notificationdivid').attr('data-rowid');
		var employeeurl=base_url+'Employee';	
		//Load the data nodes
		$.ajax({
			url:base_url+"Home/getnotificationdata",
			data: {datatype:datatype,lastnode:ID,moduleid:moduleid,rowid:rowid},			
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				var loop=data.length;
				$("#"+appendclass+"").empty();
				for(var i=0; i<loop;i++ ) {
					var displaydata= data[i]['message'];
					$("#"+appendclass+"").append('<li><div>'+data[i]['employeetwoname']+'</div><div class="notification-date">'+data[i]['duration']+'</div><p>'+displaydata+'</p><img class="notification-image" alt="" src="'+data[i]['imagepath']+'"></li>');
				}
				$(".notiredirecturl").click(function(){
					var redirecturl = $(this).attr('data-redirecturl');
					var datarowid = $(this).attr('data-datarowid');
					sessionStorage.setItem("datarowid",datarowid);
					window.location = redirecturl;	
				});
			}
		});
	}
}
{    //ajax call to load data-notification data 
	function loadnotificationlog(datatype,divid,ministatus){
		var moduleid=$('#notificationdivid').attr('data-moduleid');
		var rowid=$('#notificationdivid').attr('data-rowid');
		$.ajax({
			url:base_url+"Home/getnotificationdata",
			data: {datatype:datatype,lastnode:0,moduleid:moduleid,rowid:rowid,status:0},//status zero used in query for status not in 2	
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				var loop=data.length;
				if(ministatus == 0){
					notificationswidgetcreation('Notification',loop,divid,'notificationwidgetclass','closenotificationwidget',data,datatype);
				}else{
					notificationswidgetcreationmini('Notification',loop,divid,'notificationwidgetclass','closenotificationwidget',data,datatype);
				}
			}
		});
	}
	//insert reply to conversation data
	function conversationreplyinsert(reply_rowid,reply_message){
		var status='';		
			$.ajax({
				url:base_url+"Home/createreply",
				data: {lastnode:0,message:reply_message,rowid:reply_rowid},
				async:false,			
				type:"POST",
				cache:false,
				success :function(msg) {							
						status=msg;	
				}
			});
		return status;		
	}
	//load the comment data for a conversation
	function loadcommentdata(c_comment_rowid){
	var mdata=[];
		$.ajax({
			url:base_url+"Home/getconversationcomment",
			data: {rowid:c_comment_rowid,lastnode:0},			
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {	
				mdata=data;
			}
		});
	return mdata;
	}
}
{//ajax call to load 
	function loadconversationlog(divid)
	{
		var moduleid=$('#conversationdivid').attr('data-moduleid');
		var rowid=$('#conversationdivid').attr('data-rowid');
		$.ajax({
			url:base_url+"Home/getconversationdata",
			data: {lastnode:0,moduleid:moduleid,rowid:rowid},			
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {							
				var loop=data.length;			
				conversationwidgetcreation('Conversation',divid,'conversationwidgetclass',data);			
			}
		});
	}
	function loadconversationend(appendclass)
	{		
		var loop=0;
		var ID=$(".conversationchildnode:last").val();	 //its the last div node child'
		var moduleid=$('#conversationdivid').attr('data-moduleid');
	    var rowid=$('#conversationdivid').attr('data-rowid');
		var employeeurl=base_url+'Employee';	
		//Load the data nodes
		$.ajax({
			url:base_url+"Home/getconversationdata",
			data: {lastnode:ID,moduleid:moduleid,rowid:rowid},		
			type:"POST",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				var loop=data.length;
				if(loop == 0){
					$("#"+appendclass+"").append('');
				}
				for(var i=0; i<loop;i++ )
				{
					var filefolder="";
					var file = "";
					var editablefield='';
					if(data[i]['profile'] !== null){	
						var imgurl = '<img class="conversationpropic" id="" title="" src="'+data[i]['profile']+'">';
					}else{
						var imgurl = '<div class="conversationpropic" ><span class=""><i class="material-icons">account_circle</i></span></div>';
					}
					if(data[i]['profile'] != '' || data[i]['profile'] !== null || data[i]['profile'] !== 'undefined'){
						if(data[i]['profile']) {
							if(data[i]['fromid'] == '2' ) {
								var imgurl=base_url+data[i]['profile'];
							} else {
								var imgurl = data[i]['profile'];
							}
						}
					}
					if(checkValue(data[i]['file']) == true){
						var file=data[i]['file'];
						var filefolder=base_url+data[i]['file'];
					}
					if (data[i]['editable'] == "yes"){
					var editablefield='<span class="convopicons c_edit'+forinc+'" data-rowid="'+data[i]['c_id']+'" title="Edit"><i class="material-icons">edit</i></span><span class="convopicons c_delete'+forinc+'" data-rowid="'+data[i]['c_id']+'" title="Delete"><i class="material-icons">delete</i></span>'
					}
					$("#"+appendclass+"").append('<div class="large-12 columns"><div class="large-12 medium-12 small-12 columns totalcotainerconv" style="border:1px solid #cccccc"><div class="large-9 mesium-9 small-9  columns paddingzero"><div class="large-2 column"><img id="" title="" class="conversationpropic" src="'+imgurl+'"></div><div class="large-10 column paddingzero"> <div class="large-12 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+data[i]['userid']+'>'+data[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle">'+data[i]['c_title']+'</div></div><div class="large-12 columns messagecontentstyle">'+data[i]['c_name']+'</div></div><div class="large-3 medium-3 small-3 columns paddingzero" style="text-align:right"><span title="'+data[i]['c_date']+'" class="conversationtime">'+data[i]['time']+'</span><br><span class=""><img id="" title="" class="conversationpropic" src="'+imgurl+'"></span></div><div class="large-12 columns paddingzero iconsetconv" data-rowid="'+data[i]['c_id']+'" style="text-align:right;opacity:0"><div class="large-6 columns" style="text-align:left"><span class="convfiledownload">Attached File : <a class="c_filedownload'+forinc+'" data-file="'+filefolder+'">"'+file+'"</a></span></div><div class="large-6 columns"><span class="vcancel'+forinc+' hidedisplay"><span class="viewcancelaction'+forinc+' convopicons" data-rowid="'+data[i]['c_id']+'" data-eq="'+i+'"title="Close Comment"><i class="material-icons">visibility_off</i></span></span><span class="viewaction'+forinc+' convopicons" data-eq="'+i+'" data-rowid="'+data[i]['c_id']+'" title="View Comment"><i class="material-icons">visibility</i></span><span class="replyaction'+forinc+' convopicons c_reply" data-eq="'+i+'"data-rowid="'+data[i]['c_id']+'" title="Reply"><i class="material-icons>reply</i></span>'+editablefield+'</div></div></div></div><div class="large-12 columns  data-conversationlogid='+data[i]['c_id']+'"> &nbsp;</div><input type="hidden" class="conversationchildnode" value="'+data[i]['c_id']+'" />'); 
				}
			}
		});
	//download-clicks file//pending verfiy file exists in that path-else alert not available
	$(".c_filedownload"+forinc+"").click(function(){
		var file=$(this).attr('data-file');
		if(checkValue(file) == true){
			var ExportPath = base_url+'Home/download';
			var filepath=base_url+file;
			var path=file;
			var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+filepath+'" /><input type="hidden" name="filename" id="filename" value="'+path+'" /></form>');
			$(form).appendTo('body');
			form.submit();
		}
	});
	$(".replyaction"+forinc+"").click(function(){
		$('.replyactioncontainer').remove();
		var c_reply_rowid=$(this).attr("data-rowid");		
		var i=$(this).attr("data-eq");		
		$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero replyactioncontainer"><div class="large-12 columns">&nbsp;</div><div class="large-12 columns"><textarea id="replytextarea" class="mention" placeholder="Enter your reply here..."></textarea><input type="hidden" id="replyrowid"/></div><div class="large-12 columns">&nbsp;</div><div class="large-12 columns"><span class="postbtnstyle replyactionsave'+forinc+'">Save</span><span class="postbtnstyle replyactioncancel">Cancel</span></div><div class="large-12 columns">&nbsp;</div></div>'); 
		$('#replyrowid').val(c_reply_rowid);
		//removes the on reply area
		$('.replyactioncancel').click(function(){
			$(this).parent('div').parent('div').remove();
		});
		//on reply submit
		$('.replyactionsave'+forinc+'').click(function(){
			//get the reply data
			var reply_rowid=$('#replyrowid').val();
			var reply_message=$('#replytextarea').val();
			if(checkValue(reply_rowid) == true  && checkValue(reply_message) == true){
				var status=conversationreplyinsert(reply_rowid,reply_message);
				if(status == true){
				$(".totalviewcontainer").remove();
				$(".viewcancelaction"+forinc+":eq(" + i + ")").trigger('click');
				$(this).parent('div').parent('div').remove();
					//append recent-comments
					var cc='commentbox'+reply_rowid;					
				}
			}
			else{
				alertpopup("No Empty Reply");
			}
		});
		$('.mention textarea').smention(""+base_url+""+"Home/getuser",{
		avatar:true
		});	
	});
	//hide the view box
	$(".viewcancelaction"+forinc+"").click(function(){
		var i=$(this).attr("data-eq");
		$(".totalviewcontainer").remove();
		$(".viewaction:eq("+i+")").show();
		$(".vcancel"+forinc+":eq("+i+")").addClass('hidedisplay');
	});
	//on click its shows the reply's to that post
	$(".viewaction"+forinc+"").click(function(){
		var c_comment_rowid=$(this).attr("data-rowid");
		var i=$(this).attr("data-eq");
		var commentdata=loadcommentdata(c_comment_rowid);		
		if(checkValue(c_comment_rowid) == true && commentdata.length > 0){

		//vshow,vcancel.
		$(".viewaction"+forinc+":eq("+i+")").hide();
		$(".vcancel"+forinc+":eq("+i+")").removeClass('hidedisplay');
		var employeeurl=base_url+'Employee';		
		for(var i=0; i<commentdata.length;i++) 
		{
			if(commentdata[i]['profile'] !== null){	
				var imgurl = '<img class="conversationpropic" id="" title="" src="'+commentdata[i]['profile']+'">';
			}else{
				var imgurl = '<div class="conversationpropic" ><span class=""><i class="material-icons">account_circle</i></span></div>';
			}
			var replyeditablefield='';
			if(commentdata[i]['profile'] != ''  || commentdata[i]['profile'] !== null || commentdata[i]['profile'] !== 'undefined'){
				if(commentdata[i]['profile']){
					if(commentdata[i]['fromid'] == '2' ) {
						var imgurl=base_url+commentdata[i]['profile'];
					} else {
						var imgurl = commentdata[i]['profile'];
					}
				}
			}
			if (commentdata[i]['editable'] == "yes"){
			var replyeditablefield='<span class="convopicons creply_delete'+forinc+'" data-rowid="'+commentdata[i]['c_id']+'" title="Delete" data-eq="'+i+'"><i class="material-icons">delete</i></span>';
			}
			var commentbox='commentbox'+c_comment_rowid;
			$(this).parent('div').parent('div').parent('div').parent('div').append('<div class="large-12 columns paddingzero totalviewcontainer "><div class="large-12 columns"> &nbsp;</div><div class="large-12 columns"><div class="large-12 medium-12 small-12 columns totalcotainerconv '+commentbox+'" style="border:1px solid #cccccc"><div class="large-9 mesium-9 small-9  columns paddingzero"><div class="large-2 column"><img id="" title="" class="conversationpropic" src="'+imgurl+'"></div><div class="large-10 column paddingzero"> <div class="large-12 columns conversationnamestyle" style="color:#df6c6e"><a href='+employeeurl+' data-moduleid="4" target="_blank" data-rowid='+commentdata[i]['userid']+'>'+commentdata[i]['username']+'</a></div><div class="large-12 columns conversationtitlestyle">'+commentdata[i]['c_title']+'</div></div><div class="large-12 columns messagecontentstyle">'+commentdata[i]['c_name']+'</div></div><div class="large-3 medium-3 small-3 columns paddingzero" style="text-align:right"><span title="'+commentdata[i]['c_date']+'" class="conversationtime">'+commentdata[i]['time']+'</span><br><span class="">'+imgurl+'</span></div><div class="large-12 columns paddingzero iconsetconv" style="text-align:right;opacity:0">'+replyeditablefield+'</div></div></div><div class="large-12 columns"> &nbsp;</div></div>');
		}
		//reply hovers
		$( ".totalviewcontainer" ).hover(function(){
		$( this ).find('.iconsetconv').css('opacity','1');}, function(){
		$( this ).find('.iconsetconv').css('opacity','0');
		});
		//conversation delete
		$('.creply_delete'+forinc+'').click(function(){
			var appendid=$('#conversationdivid').val();
			var c_delete_rowid=$(this).attr("data-rowid");
			var i=$(this).attr("data-eq");
			if(c_delete_rowid !='' ||c_delete_rowid != null){
				$.ajax({
					url:base_url+"Home/conversationdelete",
					data:{rowid:c_delete_rowid},
					type:"POST",
					async:false,
					cache:false,
					success :function(msg) {
						loadconversationlog(appendid);
						//remove the specific reply
						$(".totalviewcontainer:eq("+i+")").remove();
					}
				});
			}
		});
		}
	});	
	//conversation delete
	$('.c_delete'+forinc+'').click(function(){
		var appendid=$('#conversationdivid').val();
		var c_delete_rowid=$(this).attr("data-rowid");
		if(c_delete_rowid !='' ||c_delete_rowid != null){
			$.ajax({
				url:base_url+"Home/conversationdelete",
				data:{rowid:c_delete_rowid},
				type:"POST",
				async:false,
				cache:false,
				success :function(msg) {
					loadconversationlog(appendid);
				}
			});
		}
	});
	//conversation editdata
	$('.c_edit'+forinc+'').click(function(){
		$('#postsubmit').addClass('hidedisplay');
		$("#editpostsubmit").removeClass('hidedisplay');
		$('#postcancel').removeClass('hidedisplay');
		var c_edit_rowid=$(this).attr("data-rowid");
		if(c_edit_rowid !='' ||c_edit_rowid != null){
			$.ajax({
				url:base_url+"Home/getconversationeditdata",
				data:{rowid:c_edit_rowid},
				type:"POST",
				async:false,
				dataType:'json',
				cache:false,
				success :function(data) {
					//set the data
					 setTimeout(function(){
					$('#homedashboardtitle').val(data.c_title);
					$('#homedashboardconvid').val(data.c_message);
					$('#unique_rowid').val(data.c_rowid);
					if(checkValue(data.c_file) == true){					
					  $('#cc_filepath').val(data.c_file);
					  $('#c_fileid').text(data.c_file);
					}
					$('#homedashboardconvid').val(data.c_message);
					},10);
					$('.addtitlestyle').trigger('click');
					
							if(CONVERSATIONPOST == 0){
								 $('#textenterdiv').slideToggle();//slides down the comment overlay
							}
				}
			});
		}
		//scrollbarclass
		$('.c_scroll').animate({scrollTop:0},'slow');	
		
	});	
	$( ".totalcotainerconv" ).hover(function(){
		$( this ).find('.iconsetconv').css('opacity','1');}, function(){
		$( this ).find('.iconsetconv').css('opacity','0');
	});	
	$('.mention').smention(""+base_url+""+"Home/getuser",{
		after : " "
	});			
		forinc++;
	}
	//Load Lead widget data's
	function leadwidgetcontent(appendclass,data,leadtoolbar,reloadid){
		for(var i=0; i<data.length;i++) {
			if(data[i]['employeename'] == '' || data[i]['employeename'] == null) {
				var assign = '';
			} else {
				var assign = 'Assign To : '+data[i]['employeename']+'';
			}
			if(data[i]['leadnumber']) {
				var leadnumber = data[i]['leadnumber'];
			} else {
				var leadnumber = '';
			}
			$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['leadname']+' -  '+leadnumber+'</span><span>'+assign+'</span></div><div class="member-image">'+data[i]['leadname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+leadtoolbar[3]+'"><span id="leadediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Lead" data-leadid="'+data[i]['leadid']+'"><i class="material-icons">edit</i></span></span><span class="'+leadtoolbar[4]+'"><span id="leaddeleteicon'+i+'" data-tablefieldid="leadid" data-tablename="lead"  data-tablefiledvalue="'+data[i]['leadid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
			//For Delete 
			$("#leaddeleteicon"+i+"").click(function(){
				var spanid = $(this).attr('id');
				dbwidgetdeletealert('leaddeleteconfirm'+i+'',spanid,reloadid);
			});
			//For Edit 
			$("#leadediticon"+i+"").click(function(){
				{// For Redirection Lead Edit
					var leadidedit = $(this).data('leadid');
					sessionStorage.setItem("leadidforedit", leadidedit);
				}
				var fullurl = $(this).data('redirecturl');
				window.location = base_url+fullurl;
			});
			//For Icon hover 
			$( ".member-data" ).hover(function() {
					$(this).find('.iconset').removeClass('hidedisplay');
				}, function() {
					$(this).find('.iconset').addClass('hidedisplay');
			});
			$('#lastleadid').val(data[i]['leadid']);
		 }	
	}
	//Load Product widget data's
	function productwidgetcontent(appendclass,data,reloadid){
		for(var i=0; i<data.length;i++) {
			$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Product Name : '+data[i]['productname']+'</div><div class="large-12 columns taskassignnamestyle">Category : '+data[i]['categoryname']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class=""></span></div><span class="iconset hidedisplay"><span id="productediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Product" data-productid="'+data[i]['productid']+'"><i class="material-icons">edit</i></span><span id="productdeleteicon'+i+'" data-tablefieldid="productid" data-tablename="product"  data-tablefiledvalue="'+data[i]['productid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></div><div class="large-12 columns"> &nbsp;</div>');
			//For Delete 
			$("#productdeleteicon"+i+"").click(function(){
				var spanid = $(this).attr('id');
				dbwidgetdeletealert('productdeleteconfirm'+i+'',spanid,reloadid);
			});
			//For Edit 
			$("#productediticon"+i+"").click(function(){
				{// For Redirection Lead Edit
					var productidedit = $(this).data('productid');
					sessionStorage.setItem("productidforedit", productidedit);
				}
				var fullurl = $(this).data('redirecturl');
				window.location = base_url+fullurl;
			});
			//For Icon hover 
			$( ".dsbtaskblock" ).hover(function() {
					$(this).find('.iconset').removeClass('hidedisplay');
				}, function() {
					$(this).find('.iconset').addClass('hidedisplay');
			});
			$('#lastproductid').val(data[i]['productid']);
		 }	
	}
	//Load Pricebook widget data
	function pricebookwidgetcontent(appendclass,data,reloadid){
		for(var i=0; i<data.length;i++) {		
			$("#"+appendclass+"").append('<div class="large-12 ranway columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Pricebook Name : '+data[i]['pricebookname']+'</div><div class="large-12 columns taskassignnamestyle">Currency : '+data[i]['currencyname']+'</div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class=""></span></div><span class="iconset hidedisplay"><span id="pricebookediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Pricebooksmaster" data-pricebookid="'+data[i]['pricebookid']+'"><i class="material-icons">edit</i></span><span id="pricebookdeleteicon'+i+'" data-tablefieldid="pricebookid" data-tablename="pricebook"  data-tablefiledvalue="'+data[i]['pricebookid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></div><div class="large-12 columns"> &nbsp;</div>');
			//For Delete 
			$("#pricebookdeleteicon"+i+"").click(function(){
				var spanid = $(this).attr('id');
				dbwidgetdeletealert('pricebookdeleteconfirm'+i+'',spanid,reloadid);
			});
			//For Edit 
			$("#pricebookediticon"+i+"").click(function(){
				{// For Redirection Lead Edit
					var leadidedit = $(this).data('pricebookid');
					sessionStorage.setItem("pricebookidforedit", leadidedit);
				}
				var fullurl = $(this).data('redirecturl');
				window.location = base_url+fullurl;
			});
			//For Icon hover 
			$( ".dsbtaskblock" ).hover(function() {
					$(this).find('.iconset').removeClass('hidedisplay');
				}, function() {
					$(this).find('.iconset').addClass('hidedisplay');
			});
			$('#lastpricebookid').val(data[i]['pricebookid']);			
		 }	
	}
	{//Load Account Widget Values
		function accountwidgetcontent(appendclass,data,accounttoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : '+data[i]['employeename']+'</div>';
				}
				if(data[i]['sourcename'] != ''  || data[i]['sourcename'] == null) {
					var source = '<span class="taskdatestyle mblheight">'+data[i]['sourcename']+'</span>';
				} else {
					var source = '';
				}
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['accountname']+'</span><span>'+source+'</span></div><div class="member-image">'+data[i]['accountname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+accounttoolbar[3]+'"><span id="accountediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Account" data-accountid="'+data[i]['accountid']+'"><i class="material-icons">edit</i></span></span><span class="'+accounttoolbar[4]+'"><span id="accountdeleteicon'+i+'" data-tablefieldid="accountid" data-tablename="account"  data-tablefiledvalue="'+data[i]['accountid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
				//For Delete 
				$("#accountdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('accountsdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#accountediticon"+i+"").click(function(){
					{// For Redirection Accounts Edit
						var accountsidedit = $(this).data('accountid');
						sessionStorage.setItem("accountidforedit", accountsidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastaccountid').val(data[i]['accountid']);
			}
		}
	}
	{//Load Contact Widget Values
		function contactwidgetcontent(appendclass,data,conttoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['contactname'] ==  '' || data[i]['contactname'] == null){
					var contact = '';
				} else {
					var contact = '<div class="large-12 columns tasknamestyle">'+data[i]['contactname']+'     '+'</div';
					if(data[i]['designationtypename'] != ''){
						var contact = '<div class="large-12 columns tasknamestyle">'+data[i]['contactname']+'  -  '+data[i]['designationtypename']+'</div>';
					}
				}
				if(data[i]['emailid'] != '' || data[i]['emailid'] == null) {
					var mail = data[i]['emailid'];
				} else{
					var mail = '';
				}
				if(data[i]['mobilenumber'] != '' || data[i]['mobilenumber'] == null) {
					var mobile = data[i]['mobilenumber'];
				} else{
					var mobile = '';
				}
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['contactname']+'</span><span>'+mail+'</span></div><div class="member-image">'+data[i]['contactname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+conttoolbar[3]+'"><span id="contactediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Contact" data-contactid="'+data[i]['contactid']+'"><i class="material-icons">edit</i></span></span><span class="'+conttoolbar[4]+'"><span id="contactdeleteicon'+i+'" data-tablefieldid="contactid" data-tablename="contact"  data-tablefiledvalue="'+data[i]['contactid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
				//For Delete
				$("#contactdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('contactsdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#contactediticon"+i+"").click(function(){
					{// For Redirection Contact Edit
						var contactidedit = $(this).data('contactid');
						sessionStorage.setItem("contactidforedit", contactidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".member-data" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastcontactid').val(data[i]['contactid']);
			}
		}
	}
	{//Load Campaign Widget Values
		function campaignwidgetcontent(appendclass,data,campaigntoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = 'Assign To : '+data[i]['employeename'];
				}
				if(data[i]['startdate'] ==  '' || data[i]['startdate'] == null){
					var sdate = '';
				} else {
					var sdate = data[i]['startdate'];
				}
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['campaignname']+'</span><span>'+sdate+'</span></div><div class="member-image">'+data[i]['campaignname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+campaigntoolbar[3]+'"><span id="campaignediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Campaign" data-campaignid="'+data[i]['campaignid']+'"><i class="material-icons">edit</i></span></span><span class="'+campaigntoolbar[4]+'"><span id="campaigndeleteicon'+i+'" data-tablefieldid="campaignid" data-tablename="campaign"  data-tablefiledvalue="'+data[i]['campaignid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
				//For Delete 
				$("#campaigndeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('campaigndeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#campaignediticon"+i+"").click(function(){
					{// For Redirection Campaign Edit
						var campaignidedit = $(this).data('campaignid');
						sessionStorage.setItem("campaignidforedit", campaignidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".member-data" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastcampaignid').val(data[i]['campaignid']);
			}
		}
	}
	{//Load Documents Widget Values
		function docmentswidgetcontent(appendclass,data,doctoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['filename']+'</span><span></span></div><div class="member-image"><span class="">'+data[i]['filename'].substr(0, 1)+'</span></div><span class="iconset hidedisplay" style=""><span><span class="widgetediticon c_edit" data-rowid="131" title="Edit"><i class="material-icons">edit</i></span></span><span><span class="delete widgetdeleteicon c_delete" data-rowid="131" title="Delete"><i class="material-icons">edit</i></span></span></span></li>');
				//For Delete 
				$("#documentsdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('documentsdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Icon Hover
				$( ".member-data" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastdocumentid').val(data[i]['crmfileinfoid']);
			}
		}
	}
	{//Load Quote Widget Values
		function quotewidgetcontent(appendclass,data,quotetoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> '+data[i]['employeename']+' </span></div>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-8 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Quote.No :</span> '+data[i]['quotenumber']+'</div>'+assign+'</div><div class="large-3 medium-3 small-4 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['quotedate']+'</span></div><span class="iconset hidedisplay"><span class="'+quotetoolbar[3]+'"><span id="quoteediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Quote" data-quoteid="'+data[i]['quoteid']+'"><i class="material-icons">edit</i></span></span><span class="'+quotetoolbar[4]+'"><span id="quotedeleteicon'+i+'" data-tablefieldid="quoteid" data-tablename="quote"  data-tablefiledvalue="'+data[i]['quoteid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#quotedeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('quotedeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#quoteediticon"+i+"").click(function(){
					{// For Redirection Quote Edit
						var quoteidedit = $(this).data('quoteid');
						sessionStorage.setItem("quoteidforedit", quoteidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastquoteid').val(data[i]['quoteid']);
			}
		}
	}
	{//Load Invoice Widget Values
		function invoicewidgetcontent(appendclass,data,invoicetoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> '+data[i]['employeename']+' </span></div>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-8 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Inv.No :</span> '+data[i]['invoicenumber']+'</div>'+assign+'</div><div class="large-3 medium-3 small-4 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['invoicedate']+'</span></div><span class="iconset hidedisplay"><span class="'+invoicetoolbar[3]+'"><span id="invoiceediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Invoice" data-invoiceid="'+data[i]['invoiceid']+'"><i class="material-icons">edit</i></span></span><span class="'+invoicetoolbar[4]+'"><span id="invoicedeleteicon'+i+'" data-tablefieldid="invoiceid" data-tablename="invoice" data-tablefiledvalue="'+data[i]['invoiceid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#invoicedeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('invoicedeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#invoiceediticon"+i+"").click(function(){
					{// For Redirection Invoice Edit
						var invoiceidedit = $(this).data('invoiceid');
						sessionStorage.setItem("invoiceidforedit", invoiceidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastinvoiceid').val(data[i]['invoiceid']);
			}
		}
	}
	{//Load SO Widget Values
		function salesorderwidgetcontent(appendclass,data,sotoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> '+data[i]['employeename']+' </span></div>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Sales.Or.No :</span> '+data[i]['salesordernumber']+'</div>'+assign+'</div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['salesorderdate']+'</span></div><span class="iconset hidedisplay"><span class="'+sotoolbar[3]+'"><span id="salesorderediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Salesorder" data-salesorderid="'+data[i]['salesorderid']+'"><i class="material-icons">edit</i></span></span><span class="'+sotoolbar[4]+'"><span id="salesorderdeleteicon'+i+'" data-tablefieldid="salesorderid" data-tablename="salesorder"  data-tablefiledvalue="'+data[i]['salesorderid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#salesorderdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('salesorderdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#salesorderediticon"+i+"").click(function(){
					{// For Redirection Sales order Edit
						var salesorderidedit = $(this).data('salesorderid');
						sessionStorage.setItem("salesorderidforedit", salesorderidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastsalesorderid').val(data[i]['salesorderid']);
			}
		}
	}
	{//Load PO Widget Values
		function purchaseorderwidgetcontent(appendclass,data,potoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : <span class="tealcolor"> '+data[i]['employeename']+' </span></div>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Purchase.Or.No :</span> '+data[i]['purchaseordernumber']+'</div>'+assign+'</div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['purchaseorderdate']+'</span></div><span class="iconset hidedisplay"><span class="'+potoolbar[3]+'"><span id="purchaseorderediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Purchaseorder" data-purchaseorderid="'+data[i]['purchaseorderid']+'"><i class="material-icons">edit</i></span></span><span class="'+potoolbar[4]+'"><span id="purchaseorderdeleteicon'+i+'" data-tablefieldid="purchaseorderid" data-tablename="purchaseorder"  data-tablefiledvalue="'+data[i]['purchaseorderid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#purchaseorderdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('purchaseorderdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#purchaseorderediticon"+i+"").click(function(){
					{// For Redirection Purchase order Edit
						var salesorderidedit = $(this).data('purchaseorderid');
						sessionStorage.setItem("purchaseorderidforedit", salesorderidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastpurchaseorderid').val(data[i]['purchaseorderid']);
			}
		}
	}
	{//Load Payment Widget Values
		function paymentwidgetcontent(appendclass,data,potoolbar,reloadid,moduleid) {
			for(var i=0; i<data.length;i++) {				
				var assign = '<div class="large-12 columns taskassignnamestyle"><span class="redcolor"> Mode :</span> <span class="tealcolor">'+ data[i]['paymentmethodname']+'</span></div>';
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">Amount : </span>'+data[i]['paymentamount']+'<span class="redcolor">Type :</span>'+data[i]['paymenttypename']+'</div>'+assign+'</div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['paymentdate']+'</span></div><span class="iconset hidedisplay"><span class="'+potoolbar[3]+'"><span id="paymentediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Payment" data-paymentid="'+data[i]['paymentid']+'"><i class="material-icons">edit</i></span></span><span class="'+potoolbar[4]+'"><span id="paymentdeleteicon'+i+'" data-tablefieldid="paymentid" data-tablename="payment"  data-tablefiledvalue="'+data[i]['paymentid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#paymentdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('paymentdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#paymentediticon"+i+"").click(function(){
					{// For Redirection Purchase order Edit
						var salesorderidedit = $(this).data('paymentid');
						sessionStorage.setItem("paymentidforedit", salesorderidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastpaymentid').val(data[i]['paymentid']);
			}
		}
	}
	{//Load MR Widget Values
		function materialrequisitionwidgetcontent(appendclass,data,mrtoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 columns tasknamestyle"><span class="redcolor">MaterialReq.No :</span> '+data[i]['materialrequisitionnumber']+'</div><div class="large-12 columns taskassignnamestyle"><span class="tealcolor"></span></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['requisitiondate']+'</span></div><span class="iconset hidedisplay"><span class="'+mrtoolbar[3]+'"><span id="materialrequisitionediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Materialrequisition" data-materialrequisitionid="'+data[i]['materialrequisitionid']+'"><i class="material-icons">edit</i></span></span><span class="'+mrtoolbar[4]+'"><span id="materialrequisitiondeleteicon'+i+'" data-tablefieldid="materialrequisitionid" data-tablename="materialrequisition"  data-tablefiledvalue="'+data[i]['materialrequisitionid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#materialrequisitiondeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('materialrequisitiondeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#materialrequisitionediticon"+i+"").click(function(){
					{// For Redirection Purchase order Edit
						var salesorderidedit = $(this).data('materialrequisitionid');
						sessionStorage.setItem("materialrequisitionidforedit", salesorderidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				$('#lastmaterialrequisitionid').val(data[i]['materialrequisitionid']);
			}
		}
	}
	{//Load Opportunity Widget Values
		function opportunityidgetcontent(appendclass,data,opptoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['amount'] ==  '' || data[i]['amount'] == null){
					var amount = '';
				} else {
					var amount = 'Amount : '+data[i]['amount']+'';
				}
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['opportunityname']+'</span><span>'+amount+'</span></div><div class="member-image">'+data[i]['opportunityname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+opptoolbar[3]+'"><span id="opportunityediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Opportunity" data-opportunityid="'+data[i]['opportunityid']+'"><i class="material-icons">edit</i></span></span><span class="'+opptoolbar[4]+'"><span id="opportunitydeleteicon'+i+'" data-tablefieldid="opportunityid" data-tablename="opportunity"  data-tablefiledvalue="'+data[i]['opportunityid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
				//For Delete 
				$("#opportunitydeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('opportunitydeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#opportunityediticon"+i+"").click(function(){
					{// For Redirection Opportunity Edit
						var opportunityidedit = $(this).data('opportunityid');
						sessionStorage.setItem("opportunityidforedit", opportunityidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});	
				$('#lastopportunityid').val(data[i]['opportunityid']);
			}
		}
	}
	{//Load Solution Widget Values
	function solutionwidgetcontent(appendclass,data,soltoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['amount'] ==  '' || data[i]['amount'] == null){
					var serial = '';
				} else {
					var serial = '<div class="large-12 columns taskassignnamestyle">Status : '+data[i]['crmstatusname']+'</div>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-8 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Serial No : '+data[i]['knowledgebasenumber']+'</div>'+serial+'</div></div><div class="large-3 medium-3 small-4 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['knowledgebasedate']+'</span></div><span class="iconset hidedisplay"><span class="'+soltoolbar[3]+'"><span id="solutionediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Solutions" data-solutionid="'+data[i]['knowledgebaseid']+'"><i class="material-icons">edit</i></span></span><span class="'+soltoolbar[4]+'"><span id="solutiondeleteicon'+i+'" data-tablefieldid="knowledgebaseid" data-tablename="knowledgebase"  data-tablefiledvalue="'+data[i]['knowledgebaseid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#solutiondeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('solutiondeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#solutionediticon"+i+"").click(function(){
					{// For Redirection Solution Edit
						var solutionidedit = $(this).data('solutionid');
						sessionStorage.setItem("solutionidforedit", solutionidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});				
				//For Icon hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastsolutionsid').val(data[i]['knowledgebaseid']);
			}
		}
	}
	{//Load Solution Category Widget Values
		function solutioncatwidgetcontent(appendclass,data,socatltoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['knowledgebasecategorysuffix'] ==  ''){
					var suffix = '';
				} else {
					var suffix = '<span class="taskdatestyle mblheight">'+data[i]['knowledgebasecategorysuffix']+'</span>';
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-9 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Solution Name : '+data[i]['knowledgebasecategoryname']+'</div><div class="large-12 columns taskassignnamestyle">Parent : '+data[i]['knowledgebasecategoryname']+' </div></div></div><div class="large-3 medium-3 small-3 columns paddingzero"><div class="large-12 columns"> &nbsp;</div>'+suffix+'</div><span class="iconset hidedisplay"><span class="'+socatltoolbar[3]+'"><span id="solutioncatediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Solutionscategory" data-solutioncatid="'+data[i]['knowledgebasecategoryid']+'"><i class="material-icons">edit</i></span></span><span class="'+socatltoolbar[4]+'"><span id="solutioncatdeleteicon'+i+'" data-tablefieldid="knowledgebasecategoryid" data-tablename="knowledgebasecategory"  data-tablefiledvalue="'+data[i]['knowledgebasecategoryid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#solutioncatdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('solutioncatdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#solutioncatediticon"+i+"").click(function(){
					{// For Redirection Solution Cat Edit
						var solutioncatidedit = $(this).data('solutioncatid');
						sessionStorage.setItem("solutioncatidforedit", solutioncatidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastsolutioncatid').val(data[i]['knowledgebasecategoryid']);
			}
		}
	}
	{//Load Task Widget Values
		function taskwidgetcontent(appendclass,data,tasktoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">'+data[i]['employeename']+'</div>';
				}
				if(data[i]['crmstatusid'] == '67') {
					var checktype = 'checked=checked';
					var stripe = 'text-decoration: line-through;';
				} else {
					var checktype = '';
					var stripe = '';
				}
				$("#"+appendclass+"").append('<div style="" class="icondisplay todolist-checkbox"><input name="taskcheckbox" id="taskid'+data[i]['crmtaskid']+'" class="checkboxcls radiochecksize filled-in"  value="'+data[i]['crmtaskid']+'" '+checktype+' type="checkbox"><label style="'+stripe+'" for="taskid'+data[i]['crmtaskid']+'">'+data[i]['crmtaskname']+'</label><span id="taskshowdate" class="todolist-warning">3 weeks</span><span id="taskpriid" class="todolist-today">'+data[i]['priorityname']+'</span><span class="iconset hidedisplay"><span class="'+tasktoolbar[3]+'"><span id="taskediticon'+i+'" class="widgetediticon" title="Edit" data-crmtaskid="'+data[i]['crmtaskid']+'"><i class="material-icons">edit</i></span></span><span class="'+tasktoolbar[4]+'"><span id="taskdeleteicon'+i+'" data-tablefieldid="crmtaskid" data-tablename="crmtask"  data-tablefiledvalue="'+data[i]['crmtaskid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></div>');
				//For Delete taskediticon
				$("#taskdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('taskdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit
				$("#taskediticon"+i+"").click(function(){
					var taskidedit = $(this).data('crmtaskid');
					$("#taskeditid").val(taskidedit);
					taskdatafetch(taskidedit);
					$('.todolist-button').trigger('click');
				});
				//For Icon Hover
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lasttaskid').val(data[i]['crmtaskid']);
			}
		}
	}
	{//Load Task Widget Values dsbtaskblock
		function activitywidgetcontent(appendclass,data,activitytoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = ''+data[i]['employeename']+' ';
					if(data[i]['location'] != '' || data[i]['location'] == null){
						var assign = 'Assign To : '+data[i]['employeename']+' @ '+data[i]['location']+' ';
					} 						
				}
				$("#"+appendclass+"").append('<li class="member-data"><div class="member-detail"><span>'+data[i]['crmactivityname']+'</span><span>'+assign+'</span></div><div class="member-image">'+data[i]['crmactivityname'].substr(0, 1)+' <span class=""></span></div><span class="iconset hidedisplay"><span class="'+activitytoolbar[3]+'"><span id="activityediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Activity" data-activityid="'+data[i]['crmactivityid']+'"><i class="material-icons">edit</i></span></span><span class="'+activitytoolbar[4]+'"><span id="activitydeleteicon'+i+'" data-tablefieldid="crmactivityid" data-tablename="crmactivity"  data-tablefiledvalue="'+data[i]['crmactivityid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></li>');
				//For Delete 
				$("#activitydeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('activitydeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#activityediticon"+i+"").click(function(){
					{// For Redirection Activity Edit
						var activityidedit = $(this).data('activityid');
						sessionStorage.setItem("activityidforedit", activityidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				$('#lastactivityid').val(data[i]['crmactivityid']);
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');				
				});
			}
				
		}
	}
	{//Load Ticket Widget Values 
		function ticketswidgetcontent(appendclass,data,tickettoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : '+data[i]['employeename']+'</div>';						
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-8 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Ticket No : '+data[i]['ticketnumber']+'</div>'+assign+'</div></div><div class="large-3 medium-3 small-4 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['ticketdate']+'</span></div><span class="iconset hidedisplay"><span class="'+tickettoolbar[3]+'"><span id="ticketsediticon'+i+'" class="widgetediticon" title="Edit" data-redirecturl="Ticket" data-ticketid="'+data[i]['ticketid']+'"><i class="material-icons">edit</i></span></span><span class="'+tickettoolbar[4]+'"><span id="ticketsdeleteicon'+i+'" data-tablefieldid="ticketid" data-tablename="ticket"  data-tablefiledvalue="'+data[i]['ticketid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#ticketsdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('ticketsdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#ticketsediticon"+i+"").click(function(){
					{// For Redirection Tickets Edit
						var ticketidedit = $(this).data('ticketid');
						sessionStorage.setItem("ticketidforedit", ticketidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastticketid').val(data[i]['ticketid']);
			}
		}
	}
	{//Load Contract Widget Values 
		function contractwidgetcontent(appendclass,data,contracttoolbar,reloadid) {
			for(var i=0; i<data.length;i++) {
				if(data[i]['employeename'] ==  '' || data[i]['employeename'] == null){
					var assign = '';
				} else {
					var assign = '<div class="large-12 columns taskassignnamestyle">Assign To : '+data[i]['employeename']+'</div>';						
				}
				$("#"+appendclass+"").append('<div class="large-12 columns dsbtaskblock dbbasecolor paddingzero"><div class="large-9 medium-9 small-8 columns paddingzero"><div class="large-12 medium-12 small-12 columns paddingzero"><div class="large-12 columns tasknamestyle">Contract No : '+data[i]['contractnumber']+'</div>'+assign+'</div></div><div class="large-3 medium-3 small-4 columns paddingzero"><div class="large-12 columns"> &nbsp;</div><span class="taskdatestyle">'+data[i]['contractdate']+'</span></div><span class="iconset hidedisplay"><span class="'+contracttoolbar[3]+'"><span id="contractediticon'+i+'" class="widgetediticon" title="Edit"  data-redirecturl="Contract" data-contractid="'+data[i]['contractid']+'"><i class="material-icons">edit</i></span></span><span class="'+contracttoolbar[4]+'"><span id="contractdeleteicon'+i+'" data-tablefieldid="contractid" data-tablename="contract"  data-tablefiledvalue="'+data[i]['contractid']+'" class="widgetdeleteicon" title="Delete"><i class="material-icons">delete</i></span></span></span></div><div class="large-12 columns"> &nbsp;</div>');
				//For Delete 
				$("#contractdeleteicon"+i+"").click(function(){
					var spanid = $(this).attr('id');
					dbwidgetdeletealert('contractdeleteconfirm'+i+'',spanid,reloadid);
				});
				//For Edit 
				$("#contractediticon"+i+"").click(function(){
					{// For Redirection Contract Edit
						var contractidedit = $(this).data('contractid');
						sessionStorage.setItem("contractidforedit", contractidedit);
					}
					var fullurl = $(this).data('redirecturl');
					window.location = base_url+fullurl;
				});
				//For Icon Hover 
				$( ".dsbtaskblock" ).hover(function() {
						$(this).find('.iconset').removeClass('hidedisplay');
					}, function() {
						$(this).find('.iconset').addClass('hidedisplay');
				});
				$('#lastcontractid').val(data[i]['contractid']);
			}
		}
	}
	//Load Filter by fields
	function loadfilterfield(field,moduleid,table)
	{
		$('#'+field+'').empty();		
		$('#'+field+'')
				.append($("<option value=''></option>")
				.attr("value",'')
				.text(''));		
		$.ajax({
			url:base_url+"Home/loadfilterfield",
			data:{moduleid:moduleid,table:table},
			type:"GET",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++){
					newddappend += "<option value ='"+data[m]['viewcreationcolumnname']+"' data-uitype='"+data[m]['uitypeid']+"' data-dbcolumn='"+data[m]['viewcreationcolmodelindexname']+"' data-viewcolumnid='"+data[m]['viewcreationcolumnid']+"' data-viewcreationjoincolmodelname='"+data[m]['viewcreationjoincolmodelname']+"' >"+data[m]['viewcreationcolumnname']+ " </option>";
				}
				//after run
				$('#'+field+'').append(newddappend);
			}
		});
		
	}
	//Retrieve field values and Filter by based change
	function filtervalueload(field,moduleid,textdiv,selectdiv,datediv,ddid)
	{	
		var uitype=$("#"+field+"").find('option:selected').data('uitype');
		$('#'+textdiv+',#'+selectdiv+',#'+datediv+'').addClass('hidedisplay');
		if(uitype == 8){ //date
			$('#'+datediv+'').removeClass('hidedisplay');	
		}
		else if(uitype == 17 ||uitype == 20 ||uitype == 19 ||uitype == 18||uitype == 25||uitype == 29||uitype == 21){ //uitype
			var fieldname=$("#"+field+"").find('option:selected').data('dbcolumn');				
			widgetfieldvalues(moduleid,fieldname,ddid,uitype)
			$('#'+selectdiv+'').removeClass('hidedisplay');
		}
		else { //text box
			$('#'+textdiv+'').removeClass('hidedisplay');
		}
	}
	//Retrieve widget field values
	function widgetfieldvalues(moduleid,fieldname,dropdownid,uitype){	
		$.ajax({
			url: base_url + "Base/fieldnamebesdpicklistddvalue?fieldid="+fieldname+"&moduleid="+moduleid+"&uitype="+uitype,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#'+dropdownid+'').empty();
				$('#'+dropdownid+'').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++){
						newddappend += "<option value = '" +data[m]['datasid']+ " '>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+dropdownid+'').append(newddappend);
				}
				$('#'+dropdownid+'').trigger('change');			
			},
		});
	}
	function customwidgetfieldvalues(moduleid,fieldname,dropdownid,uitype,value) {
		alert(dropdownid);
		$.ajax({
			url: base_url + "Home/customfieldnamebesdpicklistddvalue?fieldid="+fieldname+"&moduleid="+moduleid+"&uitype="+uitype,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#'+dropdownid+'').empty();
				$('#'+dropdownid+'').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++){
						newddappend += "<option data-productname='"+data[m]['dataname']+"' data-productid='"+data[m]['datasid']+"' value = '" +data[m]['dataname']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+dropdownid+'').append(newddappend);
				}			
			},
		});
	}
	//Date retrival after filter
	function getfilteroutput(table,columnfield,filtervalue,clause,dashboardwidgetid,dashboardrowid,uitype)
	{
		var mdata='';
		$.ajax({
			url:base_url+"Home/loadfilterdata",
			data:{table:table,columnfield:columnfield,filtervalue:filtervalue,clause:clause,dashboardwidgetid:dashboardwidgetid,dashboardrowid:dashboardrowid,uitype:uitype},
			type:"GET",
			dataType:'json',
			async:false,
			cache:false,
			success :function(gdata) {
				mdata=	gdata;
			}
		});
		return mdata;
	}
	//Load scroll data-moreclick
	function getmoreoutput(table,columnfield,filtervalue,clause,dashboardwidgetid,dashboardrowid,limit,uitype)
	{
		var mdata='';
		$.ajax({
			url:base_url+"Home/loadfilterdata",
			data:{table:table,columnfield:columnfield,filtervalue:filtervalue,clause:clause,dashboardwidgetid:dashboardwidgetid,dashboardrowid:dashboardrowid,limit:limit,uitype:uitype},
			type:"GET",
			dataType:'json',
			async:false,
			cache:false,
			success :function(gdata) {
				mdata=	gdata;
			}
		});
		return mdata;
	}
	function activitieswidgetcreations(){}
	function quotewidgetcreations(){}
	function invoicewidgetcreations(){}
}
//For SMS Send Widget
{//Sender Id drop down value get
	function smssenderidvalueget(ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sms/smssenderddvalfetch",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-apikey ='" +data[m]['apikey']+ "' data-sendername = '"+data[m]['dataname']+"' value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//Sender Id drop down value get
	function smstemplatevalueget(ddname) {
		var smssendtype = $("#smssendtype").val(); 
		var senderid = $("#senderid").val();
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		if((senderid != '' && smssendtype ==2) || smssendtype == 3 || smssendtype == 4) {
			$.ajax({
				url: base_url+"Home/Home/smstemplateddvalfetch?smssendtype="+smssendtype+"&senderid="+senderid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						var newddappend="";
						var newlength=data.length;
						for(var m=0;m < newlength;m++){
							newddappend += "<option data-moduleid="+data[m]['moduleid']+" data-typeid="+data[m]['typeid']+" value = '" +data[m]['datasid']+ " '>"+data[m]['dataname']+ " </option>";
						}
						//after run
						$('#'+ddname+'').append(newddappend);
					}	
				},
			});
		} else {
			$('#smssendtype').select2('val','');
			alertpopup('Please Select the Sender Id');
		}	
	}
}
//Message Value fetch
function templateeditervalueget(filename) {
	if(filename != '') {
		$.ajax({
			url: base_url+"Sms/editervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {
					var lengthofchar = data.length;
					$("#smsmessage").val(data);
				} else {
					$("#smsmessage").val('');
				}
				$("#quicksignature").trigger('change');
			},
		});
	}
}

{//SMS Signature value get
	function smssignaturevalueget(ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sms/smssignatureddvalfetch",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-smssig ='" +data[m]['smssig']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
// Signature file get
function signaturefileget(filename) {
	var oldcontent = $("#smsmessage").val();
	if(filename != '') {
		$.ajax({
			url: base_url+"Sms/editervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var sigtext = jQuery(data).text();
				var message = oldcontent+''+sigtext;
				if(data != 'Fail') {
					var lengthofchar = message.length;
					$("#quicksmspreview").val(message);
				} else {
					$("#quicksmspreview").val('');
				}
			},
		});
	}
}
{//Record Drop down value fetch
	function recordidddvaluefetch(mid,recordid){ 
		$('#quickrecordid').empty();
		$('#quickrecordid').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sms/smsrecordddvalfetch?mid="+mid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-modulename ='" +data[m]['modulename']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#quickrecordid').append(newddappend);
				}	
			},
		});
		$('#quickrecordid').select2('val',recordid).trigger('change');
	}
}
//related module data fetch
function relatedmodulevaluefetch(mid) {
	var ddname = 'c2crelmoduleid';
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax( {
		url: base_url+"Home/Home/relatedmoduledatafetch?moduleid="+mid,
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['moduleid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#'+ddname+'').append(newddappend);
			}	
		},
	});
}
//mobile number fetch
function mobilenumberfetch(moduleid,dashboardrowid) {
	$.ajax({
		url: base_url+"Home/Home/mobilenumfetch?moduleid="+moduleid+"&recordid="+dashboardrowid,
		cache:false,
		success: function(data) {
			$("#mobilenumber").val(data);
		},
	});
}
{// Get and Set Date
	function getandsetdate(dateset) { 
		$('#quickscheduledate').datetimepicker('option', 'minDate', dateset);
	}
}
//quick send submit
function quicksendsms() {
	var apikey = $("#senderid").find('option:selected').data('apikey');
	var sendername = $("#senderid").find('option:selected').data('sendername');
	var content = $('#smsmessage').val();
	var regExp = /\{([^}]+)\}/g;
	var datacontent = content.match(regExp);
	var formdata=$("#quicksendsmsform").serialize();
	var amp = '&';
	var smsdatas = amp + formdata ;
	$.ajax({                       
		url:base_url+"Home/Home/quickesmssend",  
		data: "data="+smsdatas+"&apikey="+apikey+"&sendername="+sendername+"&datacontent="+datacontent,
		dataType:'json',
		async:false,
		cache:false,
		success: function(msg) {
			if(msg == 'TRUE') {
				clearform('cleardataform');
				$("#quickpreviewsms").trigger('click');
				$("#advancedsettings").trigger('click');
			} else if(msg == 'Credits') {
				resetFields();
				$("#quickpreviewsms").trigger('click');
				$("#advancedsettings").trigger('click');
				alertpopup('Your SMS credits is too low.Please update your Addons SMS Credits');
			} else if(msg == 'Your IP Address is not valid') {
				clearform('cleardataform');
				$("#quickpreviewsms").trigger('click');
				$("#advancedsettings").trigger('click');
				resetFields();
				alertpopup(msg);
			}
		},                
	});
}
//outgoing call module dd load
function moduleddload(ddname) {
	$("#"+ddname+"").empty();
	$("#"+ddname+"").append($("<option></option>"));
	$.ajax({
		url: base_url+"Home/moduleddvalfetch",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-modulename ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$("#"+ddname+"").append(newddappend);
			}	
		},
	});
}
//outgoing record dd value fetch based on the module dd 
function recordddval(mid,ddname) {
	$("#"+ddname+"").empty();
	$("#"+ddname+"").append($("<option></option>"));
	$.ajax({
		url: base_url+"Home/recordvaluefetch?mid="+mid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$("#"+ddname+"").append(newddappend);
			}	
		},
	});
}
//mobile number dd load
function mobilenumberddload(mid,rid) {
	$.ajax({
		url:base_url+"Base/mobilenumberinformationfetch?viewid="+mid+"&recordid="+rid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			mobilenumber = [];
			if(data != 'No mobile number') {
				for(var i=0;i<data.length;i++){
					if(data[i] != ''){
						mobilenumber.push(data[i]);
					}
				}
				if(mobilenumber.length > 1) {
					newmobilenumberload(mobilenumber,'c2cmobilenum');
				} else if(mobilenumber.length == 1){
					clicktocallfunction(mobilenumber);
				}
			} else {
			}
		},
	});
}
// related mobile number load
function relatedmobilenumberddload(mid,rmid,recordid) {
	$.ajax({
		url:base_url+"Base/mobilenumberfetchbasedonmodule?moduleid="+mid+"&linkmoduleid="+rmid+"&recordid="+recordid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			mobilenumber = [];
			if(data != 'No mobile number') {
				for(var i=0;i<data.length;i++){
					if(data[i] != ''){
						mobilenumber.push(data[i]);
					}
				}
				if(mobilenumber.length > 1) {
					newmobilenumberload(mobilenumber,'c2cmobilenum');
				} else if(mobilenumber.length == 1){
					clicktocallfunction(mobilenumber);
				}
			} else {
			}
		},
	});
}
//sms and click to call mobile number overlay generation
function newmobilenumberload(mobilenumber,ddname) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax( {
		url:base_url+"Base/mobilenumberddload", 
		data: "mobilenumber=" + mobilenumber,
		type: "POST",
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['dataname']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#'+ddname+'').append(newddappend);
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
//outgoing call function
function homeclicktocallfunction(mobilenum) {
	$.ajax( {
		url:base_url+"Base/clicktocallfunction", 
		data: "mobilenumber=" + mobilenumber,
		type: "POST",
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
				$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").select2('val','');
				$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").val('');
				$("#c2crecordid,#c2cmobilenum").empty();
			} else {
				if(data == 'OK') {
					$("#c2cmobileoverlay").hide();
					$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").select2('val','');
					$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").val('');
					$("#c2crecordid,#c2cmobilenum").empty();
					alertpopup('Call Submitted Successfully and will get u soon..');
				} else {
					$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").select2('val','');
					$("#c2cmoduleid,#c2crecordid,#c2cmobilenum").val('');
					$("#c2crecordid,#c2cmobilenum").empty();
					alertpopup('Error with click to call process. try again later.');
				}
			}	
		},
	});
}
//dashboard previous and next data fetch
function dashboardpreviousnextfun(moduleid,parenttable,recordid,action) {
	$.ajax({
		url:base_url+"Home/dashboardpreviousnextget?moduleid="+moduleid+"&parenttable="+parenttable+"&recordid="+recordid+"&action="+action,
		dataType:'json',
		async:false,
		success:function(data){
			if(data != 0) {
				dynamicchartwidgetcreate(data);
				$("#processoverlay").hide();
			} else {
				$("#processoverlay").hide();
				alertpopup('There is no record available');
			}
		}
	}); 
}
//custom widget edit operation
function customeditfieldsubmit(value,parenttable,relatedtab,tablename,columnname,recordid,fieldname) {
	$.ajax({
		url:base_url+"Home/customeditfieldsubmit?value="+value+"&parenttable="+parenttable+"&recordid="+recordid+"&tablename="+tablename+"&columnname="+columnname+"&relatedtab="+relatedtab+"&fieldname="+fieldname,
		dataType:'json',
		async:false,
		success:function(data){
			if(data == 'Success'){
			} else {
			}
		}
	}); 
}
//checkbox based status change
function changestatusbasedoncheckbox(rowid,checkboxstatus,tablename) {
	$.ajax({
		url:base_url+"Home/changestatusbasedoncheckbox",
		dataType:'json',
		data: "rowid="+rowid+"&checkboxstatus="+checkboxstatus+"&tablename="+tablename,
		type: "POST",
		async:false,
		success:function(data){
			if(data == 'Success'){
			} else {
				
			}
		}
	});
}
//quick task create
function quickcreatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,tablename) {
	$.ajax({
		url:base_url+"Home/quickcreatefunction",
		dataType:'json',
		data: "name="+taskname+"&date="+taskdate+"&priority="+priority+"&tablename="+tablename+"&status="+status+"&assignto="+assignto+"&leadcontacttypeid="+leadcontacttypeid+"&taskcomminid="+taskcomminid,
		type: "POST",
		async:false,
		success:function(data){
			if(data == 'Success'){
				$("#todotextmessage").val('');
				$("#taskdate").val('');
				$("#taskpriority").select2('val','');
				$("#taskstatus").select2('val','');
				$("#taskassignto").select2('val','');
				$("#taskleadcontacttypeid").select2('val','');
				$("#taskcommonid").select2('val','');
				alertpopup('Quick task created successfully');
			} else {
			}
		}
	});
}
function appendleadcontact(val,id) {
   if(val == 2) {
	var table='contact';
   }  else {
	var table='lead';
   }
   $('#'+id+'').empty();
   $('#'+id+'').append($("<option></option>"));
   $.ajax({
		url:base_url+'Home/getdropdownoptions?table='+table,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
		  if((data.fail) == 'FAILED') {
		  } else {
				$.each(data, function(index) {
					$('#'+id+'')
					.append($("<option></option>")
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
				});
			} 
		   $('#'+id+'').trigger('change');
		},
   });
}
//assign to dropdown load
function empgroupdrodownset(dropdownid,data) {
	if(data['status']!='fail') {
		$('#'+dropdownid+'').empty();
		prev = ' ';
		$.each(data, function(index) {
			var cur = data[index]['PId'];
			if(prev != cur ) {
				if(prev != " ") {
					$('#'+dropdownid+'').append($("</optgroup>"));
				}
				$('#'+dropdownid+'').append($("<optgroup  label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
				prev = data[index]['PId'];
			}
			$("#"+dropdownid+" optgroup[label='"+data[index]['PName']+"']")
			.append($("<option></option>")
			.attr("class",data[index]['CId']+'pclass')
			.attr("value",data[index]['PId']+':'+data[index]['CId'])
			.attr("data-ddid",data[index]['CId'])
			.attr("data-dataidhidden",data[index]['PId'])
			.text(data[index]['CName']));
		});
	}
}
function taskdatafetch(taskidedit) {
	$.ajax({
		url:base_url+'Home/fetchtaskdata?primaryid='+taskidedit,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$("#todotextmessage").val(data.taskname);
				$("#taskdate").val(data.taskstartdate);
				$("#taskpriority").select2('val',data.priorityid);
				$("#taskstatus").select2('val',data.crmstatusid);
				$("#taskassignto").select2('val',data.employeetypeid+':'+data.employeeid);
				$("#taskleadcontacttypeid").select2('val',data.leadcontacttypeid).trigger('change');
				setTimeout(function(){
					if(data.leadcontacttypeid == 2){
						$("#taskcomminid").select2('val',data.contacttaskid);
					} else {
						$("#taskcomminid").select2('val',data.leadtaskid);
					}
				},100);
				Materialize.updateTextFields();
				$("#taskupdate").removeClass('todolist-submit hidedisplay');
				$("#tasksubmit").addClass('todolist-submit hidedisplay');
			} 
		},
   });
}
//quick task create
function quickupdatefunction(taskname,taskdate,priority,status,assignto,leadcontacttypeid,taskcomminid,recordid,tablename) {
	$.ajax({
		url:base_url+"Home/quickupdatefunction",
		dataType:'json',
		data: "name="+taskname+"&date="+taskdate+"&priority="+priority+"&tablename="+tablename+"&status="+status+"&assignto="+assignto+"&leadcontacttypeid="+leadcontacttypeid+"&taskcomminid="+taskcomminid+"&recordid="+recordid,
		type: "POST",
		async:false,
		success:function(data){
			if(data == 'Success'){
				$("#todotextmessage").val('');
				$("#taskeditid").val('');
				$("#taskdate").val('');
				$("#taskpriority").select2('val','');
				$("#taskstatus").select2('val','');
				$("#taskassignto").select2('val','');
				$("#taskleadcontacttypeid").select2('val','');
				$("#taskcommonid").select2('val','');
				alertpopup('Quick task updated successfully');
			} else {
			}
		}
	});
}