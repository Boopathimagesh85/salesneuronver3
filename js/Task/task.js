$(document).ready(function() {		
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}	
	{// Drop Down Change Function 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{//inner-form-with-grid
		$(".sectionalertbuttonarea").addClass('hidedisplay');
		$("#tasksinvingridadd1").click(function(){
			sectionpanelheight('tasksinvoverlay');
			$("#tasksinvoverlay").removeClass("closed");
			$("#tasksinvoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$(".addsectionclose").click(function(){
			$("#tasksinvoverlay").removeClass("effectbox");
			$("#tasksinvoverlay").addClass("closed");
		});
	}
	{//Date Range
		daterangefunction();
	}
	{//Grid Calling
		taskaddgrid();		
		//crud action
		crudactionenable();
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	$("#taskrecurrenceontheiddivhid").hide();
	$("#taskrecurrencedayiddivhid").hide();
	$("#afteroccurencesdivhid,#taskrecurrenceuntildivhid").hide();//
	$('#taskdataupdatesubbtn').hide();
	$('#tasksavebutton').show();
	//invite user hide
	$("#tasksinvaddbutton,#tasksinvingriddel1").hide();
	$('#tasksendsmsnotid').val('No');
	$('#tasksendmailnotid').val('No');
	{//Close Add Screen
		var addcloseactivitycreation =["closeaddform","taskcreationview","taskcreationaddform"]
		addclose(addcloseactivitycreation);
		var addcloseviewcreation =["viewcloseformiconid","taskcreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// For touch
		fortabtouch = 0;
	}
	{
		$("input").on("change keyup paste", function(){
			if($("form#dataaddform").attr( "formtype" ) === 'clone'){
				     $("form#dataaddform").attr("changed", 'changed');
			}
		});
	}
	{
		//Calendar
		$("#calendaricon").click(function(){
			window.location =base_url+'Calendar';
		});
	}
	{//restrict text input in time picker 
		$('#taskstarttime').timepicker({'disableTextInput' : 'False'});
		$('#taskendtime').timepicker({'disableTextInput' : 'False'});

		$('#taskstarttime').timepicker({
			'step':15,
			'timeFormat': 'H:i',
			'scrollDefaultNow':true
		});
		$('#taskstarttime').on('changeTime', function() {
		    $('#taskstarttime').focus();
		});
		$('#taskendtime').timepicker({
			'step':15,
			'timeFormat': 'H:i',
			'scrollDefaultNow':true
		});
	}
	$("#taskendtime").change(function(){
		var startdate = $('#taskstartdate').datepicker('getDate');
		var enddate = $('#taskenddate').datepicker('getDate');
		var starttime = $('#taskstarttime').val();
		var endtime = $('#taskendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	    	diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			if(splitst[0] >= splitet[0]) {
				if(splitst[1] >= splitet[1]) {
					$('#taskendtime').val('');
					alertpopup('Please select the end time based on the start time');
				}
			}
		}
	});
	{//tool bar icon change
		$("#cloneicon").click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {	
				$("#processoverlay").show();
				taskclonedatafetchfun(datarowid);
				showhideiconsfun('editclick','taskcreationaddform');
				//For tablet and mobile Tab section reset
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				settemplatesid(datarowid);
				//remove read only for recurrence
				$("#recurrencecboxid").prop('checked',false);
				$("#taskrecurrencefreqid").attr('readonly',false);
				$("#taskrecurrenceeveryday").attr('readonly',false);
				$("#taskrecurrenceendsid").attr('readonly',false);
				$("#afteroccurences").attr('readonly',false);
				$("#taskrecurrenceuntil").attr('readonly',false);
				$("#taskrecurrenceontheid").attr('readonly',false);
				$("#taskrecurrencedayid").attr('readonly',false);
				$("#reminderleadtemplateid").attr('readonly',true);
				$("#reminderemailtemplatesid").attr('readonly',true);
				$("#leadtemplateid").attr('readonly',true);
				$("#emailtemplatesid").attr('readonly',true);
				$('#tabgropdropdown').select2('val','1');
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
				$( "form#dataaddform" ).attr( "formtype", "clone");
				$("#leadcontacttype").trigger('change');
				Materialize.updateTextFields();
				Operation = 0; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
			removevalidaterequired();
			endsonddvalidateremove();
			resetFields();
		});
		$( window ).resize(function() {
			maingridresizeheightset('taskaddgrid');
			sectionpanelheight('tasksinvoverlay');
		});
		$('#formclearicon').click(function() {
			//invite user grid
			cleargriddata('tasksinvaddgrid1');
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			removevalidaterequired();
			endsonddvalidateremove();
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
			//Reset Date Picker
			daterangefunction();
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				inviteusermoduledropdownload();
				froalaset(froalaarray);
				taskeditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','taskcreationaddform');	
				$(".froala-element").css('pointer-events','none');
				//For tablet and mobile Tab section reset
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				settemplatesid(datarowid);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					 $("#processoverlay").show();
					inviteusermoduledropdownload();
					froalaset(froalaarray);
					taskeditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','taskcreationaddform');	
					$(".froala-element").css('pointer-events','none');
					//For tablet and mobile Tab section reset
					templatedatafetch('leadtemplateid','templates','templatesid','templatesname','206','sms');
					templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
					templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','206','sms');
					templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
					settemplatesid(datarowid);
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					 },50);
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','taskcreationaddform');
			$(".froala-element").css('pointer-events','auto');
			Materialize.updateTextFields();
		});
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			taskaddgrid();
		});
	}
	{//field id assign
		$("#taskcommonid").change(function() {
			var commonid = $("#taskcommonid").val();
			$("#commonid").val(commonid);
		});
	}
	{
		//validation for TASK Add  
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			var change = $("form#dataaddform").attr('changed');
			if($("form#dataaddform").attr('formtype') === 'clone'){
				if (change == 'changed') {
					$("#formaddwizard").validationEngine('validate');
					Materialize.updateTextFields();
					$("form#dataaddform").removeAttr('formtype');
					$("form#dataaddform").removeAttr('changed');
				} else {
					alertpopupdouble("You have made no changes.");
				}
			}else{
				var recurrenceid = $("#recurrence").val();
				if(recurrenceid == 'Yes'){
					var recurrenceid = $("#taskrecurrencefreqid").val();
					if(recurrenceid){
						$("#formaddwizard").validationEngine('validate');
						Materialize.updateTextFields();
					} else {
						alertpopup('Please select the recurrence frequency');
					}
				} else {
					$("#formaddwizard").validationEngine('validate');
					Materialize.updateTextFields();
				}
			}
		});
		jQuery("#formaddwizard").validationEngine(
		{
			onSuccess: function() {
				var name = $("#crmtaskname").val();
				var date = $("#taskstartdate").val();
				var time = $("#taskstarttime").val();
				$.ajax({
					url:base_url+"Task/taskvalidation",
					data: "name=" + name+"&date="+date+"&time="+time,
					type: "POST",
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
							tasknewdataaddfun();
						} else {   
							$("#taskvalidateovrelay").fadeIn();
							$("#valactivityname").val(name);
							$("#valactivitytime").val(time);
						}	
					},
				});
			},
			onFailure: function() {
				var dropdownid =['1','employeeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//task validation form	
		$("#taskcreatebtn").click(function() {
			tasknewdataaddfun();
		});
		$("#taskcancelbtn").click(function() {
			$("#taskvalidateovrelay").fadeOut();
		});
	}
	{//activity validation form	
		$("#taskmobileemailcreate").click(function(){
			taskdataaddfun();
		});
		$("#taskmobileemailupdate").click(function(){
			taskdataupdateinformationfun();
		});
		$("#taskmobileemailcancel,#taskmobemailclose").click(function(){
			$("#taskmobemailvalidateovrelay").fadeOut();
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{
		//update company information
		$('#dataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function()
			{
				tasknewdataupdatefun();
			},
			onFailure: function()
			{
				var dropdownid =['1','employeeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//module name based on user name
	$("#taskmoduleid").change(function() {
		var value = $("#taskmoduleid").val();
		$.ajax({
			url:base_url+"Task/dropdownvaluefecth?moduleid="+value,
			dataType:'json',
			async :false,
			cache:false,
			success: function(data) 
			{
				var fieldname = data.filedname;
				var tabname = data.tablename;
				usernamefetch(fieldname,tabname);//data is a table name
			},
		});
	}); 
	//Frequency based Recurrence Fields Hide and Show
	$("#taskrecurrencefreqid").change(function() {
		removevalidaterequired();
		var id = $("#taskrecurrencefreqid").val();
		if(id == '8' || id == '11') {
			$("#taskrecurrenceontheiddivhid,#taskrecurrencedayiddivhid,#taskrecurrenceuntildivhid,#afteroccurencesdivhid").hide();
			$("#taskrecurrenceendsid").select2('val','');
			$("#afteroccurences,#taskrecurrenceuntil").val('');
			$("#taskrecurrenceendsiddivhid").show();
			$("#taskrecurrenceeveryday,#taskrecurrenceendsid").addClass('validate[required]');
		} else if(id == '9') {
			$("#taskrecurrenceendsid").select2('val','');
			$("#taskrecurrencedayiddivhid,#taskrecurrenceendsiddivhid").show();
			$("#afteroccurencesdivhid,#taskrecurrenceontheiddivhid,#taskrecurrenceuntildivhid").hide();
			$("#taskrecurrenceeveryday,#taskrecurrenceendsid,#taskrecurrencedayid").addClass('validate[required]');
		} else if(id == '10') {
			$("#taskrecurrenceendsid").select2('val','');
			$("#taskrecurrenceontheiddivhid,#taskrecurrenceendsiddivhid").show();
			$("#taskrecurrenceuntildivhid,#afteroccurencesdivhid,#taskrecurrencedayiddivhid").hide();
			$("#taskrecurrenceeveryday,#taskrecurrenceendsid,#taskrecurrenceontheid").addClass('validate[required]');
		}
	});
	{//recurrences
		$("#taskrecurrenceendsid").change(function() {
			endsonddvalidateremove();
			var value = $("#taskrecurrenceendsid").val();
			if(value == 5) {
				$("#afteroccurences").val('');
				$("#activityrecurrenceuntil").val('');
				$("#afteroccurencesdivhid").hide();
				$("#taskrecurrenceuntildivhid").hide();
			} else if(value == 6) {
				$("#activityrecurrenceuntil").val('');
				$("#afteroccurencesdivhid").show();
				$("#taskrecurrenceuntildivhid").hide();
				$("#afteroccurences").addClass('validate[required]');
			}else if(value == 7) {
				$("#taskrecurrenceuntildivhid").show();
				$("#afteroccurences").val('');
				$("#afteroccurencesdivhid").hide();
				$("#taskrecurrenceuntil").addClass('validate[required]');
			}
		});
	}
	//repeat on - nulti select
	$("#taskrecurrencedayid").change(function() {
		var data = $('#taskrecurrencedayid').select2('data');
		var finalResult = [];
		for( item in $('#taskrecurrencedayid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#repeatonmultipleid").val(selectid);
	});
	{// Redirected form Home
		add_fromredirect("taskaddsrc",10);
	}
	//Task-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique206");
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				taskeditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','taskcreationaddform');
				sessionStorage.removeItem("reportunique206");
				Materialize.updateTextFields();
			},50);	
		}
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Untill Date Picker
		var dateformetdata = $('#taskrecurrenceuntil').attr('data-dateformater');
		$('#taskrecurrenceuntil').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			maxDate:0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#taskrecurrenceuntil').focus();
			}
			
		}); 
	}
	{	//reminder check
		$("#taskreminderintervalid").change(function() {
			var data = $('#taskreminderintervalid').select2('data');
			var finalResult = [];
			for( item in $('#taskreminderintervalid').select2('data') ) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#remindbeforeid").val(selectid);
			var interval = $("#remindbeforeid").val();
			var statrdate = $("#taskstartdate").val();	
			var starttime = $("#taskstarttime").val();	
			var remindervalue = $("#taskreminderintervalid").find('option:selected').data('taskreminderintervalidhidden');
			if(statrdate != '' && starttime != '') {
				$.ajax({
					url:base_url+"Task/reminderintervalcheck?reminderval="+interval+"&sdate="+statrdate+"&stime="+starttime+"&remindervalue="+remindervalue,
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						if(data != 'True') {
							alertpopup('You cant create the Reminder for the selected date '+statrdate+' time ' +starttime);
							$("#taskreminderintervalid").select2('val','');
						} else {
							
						}
					},
				});
			} else {
				$("#taskreminderintervalid").select2('val','');
				alertpopup("Please select the Start date and Start Time");
			}
		});
	}	
	//module based view drop down load 
	$("#taskinvitemoduleid").change(function() {
		var invitemoduleid = $("#taskinvitemoduleid").val();
		if(invitemoduleid == '247' || invitemoduleid== '246' || invitemoduleid == '1'){
			$('#inviteviewname').empty();
			$('#inviteviewname').select2('val','');
			$("#inviteviewname").attr('readonly',true);
			tasksinvaddgrid1();
		} else {
			$("#inviteviewname").attr('readonly',false);
			viewdropdownload(invitemoduleid);
		}
		if(invitemoduleid == 4){
			$("#taskremindermodeid option[value='4']").removeAttr('disabled');
		}
		if(invitemoduleid != 4 && invitemoduleid != undefined){
			$("#taskremindermodeid option[value='4']").prop('disabled', 'disabled');
		}
	});
	//view drop down change
	$("#inviteviewname").change(function() {
		tasksinvaddgrid1();
	});
	//invite type change
	$("#taskremindermodeid").change(function() {
		var data = $('#taskremindermodeid').select2('data');
		var finalResult = [];
		for( item in $('#taskremindermodeid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#invitetypeid").val(selectid);
		var reminder = $("#invitetypeid").val();
		//templates readonly mode set
		var invitereminder = $("#taskremindermodeid").val();
		if(invitereminder || invitereminder != ''){
			$("#leadtemplateid").attr('readonly',false);
			$("#emailtemplatesid").attr('readonly',false);
		} else {
			$("#leadtemplateid").attr('readonly',true);
			$("#emailtemplatesid").attr('readonly',true);
		}
		//template required set
		if(finalResult.length == 0){
			$("#emailtemplatesid").removeClass('validate[required]');
			$("#leadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('2', finalResult) > -1) {
			$("#leadtemplateid").addClass('validate[required]');
		} else {
			$("#leadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('3', finalResult) > -1) {
			$("#emailtemplatesid").addClass('validate[required]');
		} else {
			$("#emailtemplatesid").removeClass('validate[required]');
		}
	});
	{//sms template assign - reminder
		$("#reminderleadtemplateid").change(function() {
			var smstempid = $("#reminderleadtemplateid").val();
		});
	}
	{//mail template assign - reminder
		$("#reminderemailtemplatesid").change(function() {
			var mailtempid = $("#reminderemailtemplatesid").val();
		});
	}
	{//sms invite template
		$("#leadtemplateid").change(function() {
			var smstempid = $("#leadtemplateid").val();
		});
	}
	{//email template assign
		$("#emailtemplatesid").change(function() {
			var mailtempid = $("#emailtemplatesid").val();
		});
	}
	$("#mailicon").click(function() {
		var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {	
			sessionStorage.setItem("forcomposemailid",'');
			sessionStorage.setItem("forsubject",'');
			var fullurl = 'erpmail/?_task=mail&_action=compose';
			window.open(''+base_url+''+fullurl+'');
		} else {
			alertpopup("Please select a row");
		}	
	}); 
	{ //sms and c2c icon work -gowtham
		$("#mobilenumberoverlayclose").click(function() {
			$("#mobilenumbershow").hide();
		});	
		//sms icon click
		$("#smsicon").click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#mobilenumbershow").show();
								$("#smsmoduledivhid").hide();
								$("#smsrecordid").val(datarowid);
								$("#smsmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'smsmobilenumid');
							} else if(mobilenumber.length == 1) {
								sessionStorage.setItem("mobilenumber",mobilenumber);
								sessionStorage.setItem("viewfieldids",viewfieldids);
								sessionStorage.setItem("recordis",datarowid);
								window.location = base_url+'Sms';
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").show();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'smsmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});	
		//sms module dd change
		$("#smsmodule").change(function() {
			var moduleid =	$("#smsmoduleid").val(); //main module
			var linkmoduleid =	$("#smsmodule").val(); // link module
			var recordid = $("#smsrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
			}
		});
		//call module dd chnge
		$("#callmodule").change(function() {
			var moduleid =	$("#callmoduleid").val(); //main module
			var linkmoduleid =	$("#callmodule").val(); // link module
			var recordid = $("#callrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
			}
		});
		//sms mobile number select
		$("#smsmobilenumid").change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#smsmobilenum").val(selectid);
		});
		//sms overlay mobile number submit 
		$("#mobilenumbersubmit").click(function() {
			var datarowid = $("#smsrecordid").val();
			var viewfieldids =$("#smsmoduleid").val();
			var mobilenumber =$("#smsmobilenum").val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem("mobilenumber",mobilenumber);
				sessionStorage.setItem("viewfieldids",viewfieldids);
				sessionStorage.setItem("recordis",datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}	
		});
		//c2c icon click
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").show();
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'callmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		//c2c overlay close
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		//c2c mobile number submit.
		$("#callnumbersubmit").click(function()  {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
		//On lead contact change
		$("#leadcontacttype").change(function(){
			var val = $(this).val();
			if(val == 2){
				appendleadcontact(val,'contacttaskid');
				$('#leadtaskiddivhid').hide();
				$('#contacttaskiddivhid').show();
			}else if(val == 3){
				$('#contacttaskiddivhid').hide();
				$('#leadtaskiddivhid').show();
				appendleadcontact(val,'leadtaskid');
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,taskaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			taskaddgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	//recurrence hide
	var recurrence = $("#recurrence").val();
	if(recurrence == 'No') {
		$('#recurrencestartdatedivhid,#recurrenceenddatedivhid').show();
		$("#taskrecurrencefreqiddivhid").hide();
		$("#taskrecurrenceeverydaydivhid").hide();
		$("#taskrecurrenceendsiddivhid").hide();
	}
	$("#recurrencecboxid").change(function(){
		var recurrence = $("#recurrence").val();
		if(recurrence == 'Yes') {
			$('#recurrencestartdatedivhid,#recurrenceenddatedivhid').show();
			$("#taskrecurrencefreqiddivhid").show();
			$("#taskrecurrenceeverydaydivhid").show();
			$("#taskrecurrenceendsiddivhid").show();
			$('#activityrecurrenceeveryday').removeClass('validate[custom[integer],maxSize[3]]');
			$('#activityrecurrenceeveryday').addClass('validate[required,custom[integer],maxSize[3],min[1]]');
		} else if(recurrence == 'No') {
			$('#activityrecurrenceeveryday').removeClass('validate[required,custom[integer],maxSize[3],min[1]]');
			$('#recurrencestartdatedivhid,#recurrenceenddatedivhid').hide();
			$("#taskrecurrencefreqiddivhid").hide();
			$("#taskrecurrenceeverydaydivhid").hide();
			$("#taskrecurrenceendsiddivhid").hide();
		}
	});
	$("#remindermodeid").change(function(){
		var reminder = $("#remindermodeid").val();
		if(reminder){
			$("#reminderleadtemplateid").attr('readonly',false);
			$("#reminderemailtemplatesid").attr('readonly',false);
		} else {
			$("#reminderleadtemplateid").attr('readonly',true);
			$("#reminderemailtemplatesid").attr('readonly',true);
		}
		var data = $('#remindermodeid').select2('data');
		var finalResult = [];
		for( item in $('#remindermodeid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		//template required set
		if(finalResult.length == 0){
			$("#reminderemailtemplatesid").removeClass('validate[required]');
			$("#reminderleadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('2', finalResult) > -1) {
			$("#reminderleadtemplateid").addClass('validate[required]');
		} else {
			$("#reminderleadtemplateid").removeClass('validate[required]');
		}
		if($.inArray('3', finalResult) > -1) {
			$("#reminderemailtemplatesid").addClass('validate[required]');
		} else {
			$("#reminderemailtemplatesid").removeClass('validate[required]');
		}
	});	
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Task Add Grid
function taskaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#taskspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#taskspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	}
	var wwidth = $('#taskaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.tasksheadercolsort').hasClass('datasort') ? $('.tasksheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.tasksheadercolsort').hasClass('datasort') ? $('.tasksheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.tasksheadercolsort').hasClass('datasort') ? $('.tasksheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=crmtask&primaryid=crmtaskid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType: "application/json; charset=utf-8",
		dataType:'json',
		async:false,
		success :function(data) {
			$('#taskaddgrid').empty();
			$('#taskaddgrid').append(data.content);
			$('#taskaddgridfooter').empty();
			$('#taskaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('taskaddgrid');
			{//sorting
				$('.tasksheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.tasksheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#taskspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.tasksheadercolsort').hasClass('datasort') ? $('.tasksheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.tasksheadercolsort').hasClass('datasort') ? $('.tasksheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#taskaddgrid .gridcontent').scrollLeft();
					taskaddgrid(page,rowcount);
					$('#taskaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('tasksheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					taskaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#taskspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					taskaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				{// Redirected form Home
					var taskideditvalue = sessionStorage.getItem("taskidforedit");
					if(taskideditvalue != null){
						setTimeout(function(){
						taskeditdatafetchfun(taskideditvalue);
						sessionStorage.removeItem("taskidforedit");},10);
						Materialize.updateTextFields();
					}
				}
				{// Redirected form calendar
					var uniquelcalsession = sessionStorage.getItem("taskidforclone");
					if(uniquelcalsession != '' && uniquelcalsession != null) {
						setTimeout(function() { 
							taskclonedatafetchfun(uniquelcalsession);
							showhideiconsfun('editclick','taskcreationaddform');
							sessionStorage.removeItem("taskidforclone");
							Materialize.updateTextFields();
						},50);	
					}
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#taskspgrowcount').material_select();
			}
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(){
			tasksinvaddgrid1();
			addslideup('taskcreationview','taskcreationaddform');
			resetFields();
			$("#taskcommonid").empty();
			$("#recurrencedivhid").show();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');			
			//form field first focus
			firstfieldfocus();
			removevalidaterequired();
			endsonddvalidateremove();
			recurrenceondaydataget();
			showhideiconsfun('addclick','taskcreationaddform');	
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			cleargriddata('tasksinvaddgrid1');
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			//show hide recurrence fields
			$("#contacttaskiddivhid").show();
			$("#recurrencestartdatedivhid,#recurrenceenddatedivhid").hide();
			$("#taskrecurrencefreqiddivhid,#taskrecurrenceeverydaydivhid,#taskrecurrenceendsiddivhid").hide();
			$("#afteroccurencesdivhid,#taskrecurrenceuntildivhid,#taskrecurrencedayiddivhid,#taskrecurrenceontheiddivhid").hide();
			$("#leadtaskiddivhid").hide();
			inviteusermoduledropdownload();
			//For tablet and mobile Tab section reset
			templatedatafetch('leadtemplateid','templates','templatesid','templatesname','206','sms');
			templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
			templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','206','sms');
			templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
			//remove read only for recurrence
			$("#recurrencecboxid").prop('checked',false);
			$("#taskrecurrencefreqid").attr('readonly',false);
			$("#taskrecurrenceeveryday").attr('readonly',false);
			$("#taskrecurrenceendsid").attr('readonly',false);
			$("#afteroccurences").attr('readonly',false);
			$("#taskrecurrenceuntil").attr('readonly',false);
			$("#taskrecurrenceontheid").attr('readonly',false);
			$("#taskrecurrencedayid").attr('readonly',false);
			$("#reminderleadtemplateid").attr('readonly',true);
			$("#reminderemailtemplatesid").attr('readonly',true);
			$("#leadtemplateid").attr('readonly',true);
			$("#emailtemplatesid").attr('readonly',true);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			appendleadcontact('2','contacttaskid');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			$("#filtercolumn,#filtercondition,#filtercondvalue,#filterddcondvalue,#filterassignddcondvalue,#filterdatecondvalue").removeClass('validate[required]');
			$("#recurrencestartdate").removeClass('validate[required]');
			$("#recurrenceenddate,#employeeidddid").removeClass('validate[required]');
			$("form#dataaddform").removeAttr('formtype');
			$("form#dataaddform").removeAttr('changed');
			$("#emailtemplatesid").removeClass('validate[required]');
			$("#leadtemplateid").removeClass('validate[required]');
			Operation = 0; //for pagination
		});
		//edit icon click
		$("#editicon").click(function(){
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				$("#emailtemplatesid").removeClass('validate[required]');
				$("#leadtemplateid").removeClass('validate[required]');
				cleargriddata('tasksinvaddgrid1');
				inviteusermoduledropdownload();
				froalaset(froalaarray);
				taskeditdatafetchfun(datarowid);
				removevalidaterequired();
				endsonddvalidateremove();
				showhideiconsfun('editclick','taskcreationaddform');
				//For tablet and mobile Tab section reset
				templatedatafetch('leadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('emailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				templatedatafetch('reminderleadtemplateid','templates','templatesid','templatesname','206','sms');
				templatedatafetch('reminderemailtemplatesid','emailtemplates','emailtemplatesid','emailtemplatesname','206','email');
				settemplatesid(datarowid);
				//show hide recurrence fields
				$("#filtercolumn,#filtercondition,#filtercondvalue,#filterddcondvalue,#filterassignddcondvalue,#filterdatecondvalue").removeClass('validate[required]');
				$("#recurrencestartdate").removeClass('validate[required]');
				$("#recurrenceenddate,#employeeidddid").removeClass('validate[required]');
				$("#recurrencestartdatedivhid,#recurrenceenddatedivhid").hide();
				$("#recurrencedivhid,#taskrecurrencefreqiddivhid,#taskrecurrenceeverydaydivhid,#taskrecurrenceendsiddivhid").hide();
				$("#afteroccurencesdivhid,#taskrecurrenceuntildivhid,#taskrecurrencedayiddivhid,#taskrecurrenceontheiddivhid").hide();
				//add read only for recurrence
				$("#taskrecurrencefreqid").attr('readonly',true);
				$("#taskrecurrenceeveryday").attr('readonly',true);
				$("#taskrecurrenceendsid").attr('readonly',true);
				$("#afteroccurences").attr('readonly',true);
				$("#taskrecurrenceuntil").attr('readonly',true);
				$("#taskrecurrenceontheid").attr('readonly',true);	
				$("#taskrecurrencedayid").attr('readonly',true);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				$("#leadcontacttype").trigger('change');
				fortabtouch = 1;
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#taskaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			taskrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#taskspgnum li.active').data('pagenum');
		var rowcount = $('ul#taskspgnumcnt li .page-text .active').data('rowcount');
		taskaddgrid(page,rowcount);
	}
}
//Task invite user grid
function tasksinvaddgrid1(page,rowcount) {
	var maintabinfo='';
	var parentid='';
	var invitemoduleid = $("#taskinvitemoduleid").val();
	var viewid = $("#inviteviewname").val();
	if(viewid == null || viewid == '1' || viewid == '') { 
		viewid = '3'; 
	}
	if(invitemoduleid === null || invitemoduleid === '' || invitemoduleid === undefined) { 
		invitemoduleid = '201'; 
	}
	$.ajax({
		url:base_url+"Task/parenttableget?moduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(pdata) {
			if(pdata != "") {
				maintabinfo = pdata;
				parentid = maintabinfo+"id";
			} else {
				maintabinfo = '';
				parentid = '';
			}
		},
	});
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#tasksinvaddgrid1').width();
	var wheight = $('#tasksinvaddgrid1').height();
	//col sort
	var sortcol = $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.inviteuserlistheadercolsort').hasClass('datasort') ? $('.inviteuserlistheadercolsort.datasort').attr('id') : '0';
	var footername = 'invitefooter';
	$.ajax({
		url:base_url+"Task/inviteusergrigdatafetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+invitemoduleid+"&viewid="+viewid+"&checkbox=true"+'&footername='+footername,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#tasksinvaddgrid1').empty();
			$('#tasksinvaddgrid1').append(data.content);
			$('#tasksinvaddgrid1footer').empty();
			$('#tasksinvaddgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('tasksinvaddgrid1');
			{//sorting
				$('.inviteuserlistheadercolsort').click(function(){
					$('.inviteuserlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#inviteuserlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#inviteuserlistpgnumcnt li .page-text .active').data('rowcount');
					tasksinvaddgrid1(page,rowcount);
				});
				sortordertypereset('inviteuserlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					tasksinvaddgrid1(page,rowcount);
				});
				$('#invitefooterpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					tasksinvaddgrid1(page,rowcount);
				});
			}
			//Material select
			$('#invitefooterpgrowcount').material_select();
			//header check box
			$(".invite_headchkboxclass").click(function() {
				checkboxclass('invite_headchkboxclass','invite_rowchkboxclass');
				getcheckboxrowid(invitemoduleid);
			});
			//row based check box
			$(".invite_rowchkboxclass").click(function(){
				$('.invite_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(invitemoduleid);
			});
			checktheselectedinvitevalues(invitemoduleid);
		}
	});
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid(invitemoduleid) {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	inviteselectuserfetch(invitemoduleid,selected);
}
//new data add submit function
function tasknewdataaddfun() {
    $("#taskvalidateovrelay").fadeOut();
	var smscheckid = $('#invitetypeid').val();
	var inviteviewid = $('#inviteviewname').val();
	var invitemoduleid = $('#taskinvitemoduleid').val();
	if(smscheckid != '') {
		var leadid = $('#leadinviteusetid').val();
		var contactid = $('#contactinviteuserid').val();
		var userid = $('#userinviteuserid').val();
		var groupid = $('#groupinviteuserid').val();
		var rolesid = $('#rolesinviteuserid').val();
		var randsid = $('#randsinviteuserid').val();
		if((invitemoduleid == 201 && leadid.length > 0) || (invitemoduleid==203 && contactid.length > 0) || (invitemoduleid==4 && userid.length > 0)) {
			mobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid);
		} else {
			alertpopup('Please select the invite users');
		}
	} else {
		$("#taskvalidateovrelay").fadeOut();
		taskdataaddfun();
	}
}
//mobile and email validation
function mobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid) {
	$.ajax({
		url: base_url+"Task/mobileandmailvalidation",
		data:"leadid="+leadid+"&contactid="+contactid+"&userid="+userid+"&groupid="+groupid+"&rolesid="+rolesid+"&randsid="+randsid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) 
		{
			var emailid = data.emailid;
			$("#inviteemailid").val(jQuery.unique(emailid));
			var withoutemailid = data.withoutmail;
			var mobilenum = data.mobilenum;
			$("#invitemobilenumber").val(jQuery.unique(mobilenum));
			var withoutmobilenum = data.withoutmoble;
			var emailvalue=jQuery.trim(withoutemailid.replace(/,/g, " "));
			var mobilevalue=jQuery.trim(withoutmobilenum.replace(/,/g, " "));
			if(emailvalue != '' || mobilevalue != '') {
				$("#taskmobemailvalidateovrelay").fadeIn();
				$("#taskmobileemailcreate").show();
				$("#taskmobileemailupdate").hide();
				if(mobilevalue != '') {
					$("#taskmobileshow").show();
					$("#withoutmobilenum").val(withoutmobilenum);
				} else {
					$("#taskmobileshow").hide();
				}
				if(emailvalue != '') {
					$("#taskmailshow").show();
					$("#withoutmailid").val(withoutemailid);
				} else {
					$("#taskmailshow").hide();
				}
			} else {
				$("#taskmobemailvalidateovrelay").fadeOut();
				taskdataaddfun();
			}
		},
	});
}
//new data add - after validation
function taskdataaddfun() {	
	$("#taskmobemailvalidateovrelay").fadeOut();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	$("#processoverlay").show();
	var amp = '&';
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Task/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == "TRUE") {
            	    $("#processoverlay").hide();
    				$('#taskcreationaddform').hide();
    				$('#viewcreationformdiv').hide();
    				$('#taskcreationview').fadeIn(1000);
    				$(".ftab").trigger('click');
    				$("#taskcommonid").empty();
    				$("#taskmobemailvalidateovrelay").fadeOut();
    				cleargriddata('tasksinvaddgrid1');
    				refreshgrid();
    				alertpopup(savealert);
    				//For Keyboard Shortcut Variables
    				viewgridview = 1;
    				addformview = 0;
        	} else {
				$("#processoverlay").hide();
				alertpopup('Error during create.Please refresh and try again');
			}
        },
    });
}
//old information show in form
function taskeditformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax( {
		url:base_url+"Task/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$(".ftab").trigger('click');
				$('#taskcreationaddform').hide();
				$('#taskcreationview').fadeIn(1000);
				refreshgrid();
				$("#processoverlay").hide();
			} else {
				addslideup('taskcreationview','taskcreationaddform');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				leadcontacttypefunction(datarowid);
				editordatafetch(froalaarray,data);
				$("#taskmoduleid").trigger('change');
				setTimeout(function(){
					//show hide recurrence fields
					$("#filtercolumn,#filtercondition,#filtercondvalue,#filterddcondvalue,#filterassignddcondvalue,#filterdatecondvalue").removeClass('validate[required]');
					$("#recurrencestartdate").removeClass('validate[required]');
					$("#recurrenceenddate,#employeeidddid").removeClass('validate[required]');
					$("#recurrencestartdatedivhid,#recurrenceenddatedivhid").hide();
					$("#recurrencedivhid,#taskrecurrencefreqiddivhid,#taskrecurrenceeverydaydivhid,#taskrecurrenceendsiddivhid").hide();
					$("#afteroccurencesdivhid,#taskrecurrenceuntildivhid,#taskrecurrencedayiddivhid,#taskrecurrenceontheiddivhid").hide();
					var commonid = data.commonid;
					$("#taskcommonid").select2('val',commonid);
					$("#commonid").val(commonid);
				},100);
				$("#processoverlay").hide();
				Materialize.updateTextFields();
			}
		}
	});
}
function leadcontacttypefunction(datarowid) {
	var leadcontacttype = $('#leadcontacttype').val();
	var contacttaskid = $('#contacttaskid').val();
	var leadtaskid = $('#leadtaskid').val();
	$.ajax({
	    url: base_url+"Task/leadcontacttypefunction?primarydataid="+datarowid+"&contacttaskid="+contacttaskid+"&leadtaskid="+leadtaskid,
		cache:false,
		dataType:'json',
	    success: function(msg)  {
	        if (msg.failed != 'Failure') {
        		setTimeout(function(){
        			if(leadcontacttype == 2){
	        			$("#contacttaskid").select2('val',msg.contacttaskid);
		        		$("#contacttaskid").val(msg.contacttaskid);
			        } else {
			        	$("#leadtaskid").select2('val',msg.leadtaskid);
			        	$("#leadtaskid").val(msg.leadtaskid);
			        }
        		},100);
			}
	    },
	});
}
function tasknewdataupdatefun() {
	var smscheckid = $('#invitetypeid').val();
	if(smscheckid != '') {
		var leadid = $('#leadinviteusetid').val();
		var contactid = $('#contactinviteuserid').val();
		var userid = $('#userinviteuserid').val();
		var groupid = $('#groupinviteuserid').val();
		var rolesid = $('#rolesinviteuserid').val();
		var randsid = $('#randsinviteuserid').val();
		updatemobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid);
	} else {
		taskdataupdateinformationfun();
	}
}
//mobile and email validation
function updatemobilemailcheckvalidation(leadid,contactid,userid,groupid,rolesid,randsid) {
	$.ajax({
		url: base_url+"Task/mobileandmailvalidation",
		data:"leadid="+leadid+"&contactid="+contactid+"&userid="+userid+"&groupid="+groupid+"&rolesid="+rolesid+"&randsid="+randsid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var emailid = data.emailid;
			$("#inviteemailid").val(jQuery.unique(emailid));
			var withoutemailid = data.withoutmail;
			var mobilenum = data.mobilenum;
			$("#invitemobilenumber").val(jQuery.unique(mobilenum));
			var withoutmobilenum = data.withoutmoble;
			var emailvalue=jQuery.trim(withoutemailid.replace(/,/g, " "));
			var mobilevalue=jQuery.trim(withoutmobilenum.replace(/,/g, " "));
			if(emailvalue != '' || mobilevalue != '') {
				$("#taskmobemailvalidateovrelay").fadeIn();
				$("#taskmobileemailcreate").hide();
				$("#taskmobileemailupdate").show();
				if(mobilevalue != '') {
					$("#taskmobileshow").show();
					$("#withoutmobilenum").val(withoutmobilenum);
				} else {
					$("#taskmobileshow").hide();
				}
				if(emailvalue != '') {
					$("#taskmailshow").show();
					$("#withoutmailid").val(withoutemailid);
				} else {
					$("#taskmailshow").hide();
				}
			} else {
				$("#taskmobemailvalidateovrelay").fadeOut();
				taskdataupdateinformationfun();
			}
		},
	});
}
//update old information
function taskdataupdateinformationfun() {
	$("#taskmobemailvalidateovrelay").fadeOut();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	$("#processoverlay").show();
	var amp = '&';
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Task/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
        	var nmsg =  $.trim(msg);
            if (nmsg = "TRUE") {
				$("#processoverlay").hide();
				resetFields();
				$(".ftab").trigger('click');
				$('#taskaddform').hide();
				$('#viewcreationformdiv').hide();
				$('#taskcreationaddform').hide();
				$('#taskcreationview').fadeIn(1000);
				refreshgrid();
				$("#commonid").empty();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            } else {
				$("#processoverlay").hide();
				alertpopup('Error during update.Please refresh and try again');
			}
        },
    });	
}
//record delete function
function taskrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Task/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg)  {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            }  else if (nmsg.result == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
{//date Range
	function daterangefunction() { 
		$('#recurrencestartdate').datetimepicker("destroy");
		$('#recurrenceenddate').datetimepicker("destroy");
		var startdateformater = $('#recurrencestartdate').attr('data-dateformater');
		var startDateTextBox = $('#recurrencestartdate');
		var endDateTextBox = $('#recurrenceenddate');
		var minimumdate = $("#taskrecurrencestartdate");
		var maximumdate = $("#taskrecurrenceuntil");
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			changeMonth: true,
			changeYear: true,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
					Materialize.updateTextFields();
				}
				else {
					endDateTextBox.val(dateText);
					Materialize.updateTextFields();
				}
				$('#recurrencestartdate').focus();
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				minimumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.taskenddateformError').remove();
					$('#recurrenceenddate').removeClass('error');
					Materialize.updateTextFields();
				}
			},
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			changeMonth: true,
			changeYear: true,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						Materialize.updateTextFields();
						}
				else {
					Materialize.updateTextFields();
				}
				$('#recurrenceenddate').focus();
			},
			onSelect: function (selectedDateTime){
				minimumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				maximumdate.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				Materialize.updateTextFields();
			},
		});
	}
}
{// Edit Function
	function taskeditdatafetchfun(datarowid) {		
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		$("#taskcommonid").empty();
		taskeditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function
	function taskclonedatafetchfun(datarowid) {		
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		taskeditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{//invite user module drop down load
	function inviteusermoduledropdownload() {
		$('#taskinvitemoduleid').empty();
		$('#taskinvitemoduleid').append($("<option></option>"));
		$.ajax({
			url: base_url+"Task/invitemoduledropdownloadfun",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#taskinvitemoduleid').append(newddappend);
				}	
			},
		});
	}
	//view drop down load
	function viewdropdownload(invitemoduleid) {
		$('#inviteviewname').empty();
		$.ajax({
			url: base_url+"Task/viewdropdownloadfun?invitemoduleid="+invitemoduleid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#inviteviewname').append(newddappend);
				}	
				$('#inviteviewname').trigger('change');
			},
		});
	}
	//selected row data get - invite grid
	function inviteselectuserfetch(invitemoduleid,datarowid) {
		var	inviteuser =  invitemoduleid+'|'+datarowid;
		if(datarowid.length > 0){
			inviteuser=inviteuser;
		} else{
			inviteuser='';
		}
		if(invitemoduleid == '201') {
			$("#leadinviteusetid").val(inviteuser);
		} else if(invitemoduleid == '203') {
			$("#contactinviteuserid").val(inviteuser);
		} else if(invitemoduleid == '4') {
			$("#userinviteuserid").val(inviteuser);
		} else if(invitemoduleid == '246') {
			$("#groupinviteuserid").val(inviteuser);
		} else if(invitemoduleid == '247') {
			$("#rolesinviteuserid").val(inviteuser);
		} else if(invitemoduleid == '1') {
			$("#randsinviteuserid").val(inviteuser);
		}
	}
	//check the selected invite user 
	function checktheselectedinvitevalues(invitemoduleid) {
		var leadid = $("#leadinviteusetid").val();
		var contactid = $("#contactinviteuserid").val();
		var userid = $("#userinviteuserid").val();
		var groupid = $("#groupinviteuserid").val();
		var rolesid = $("#rolesinviteuserid").val();
		var randsid = $("#randsinviteuserid").val();
		if(invitemoduleid == '201') {
			if(leadid != '') {
				var lead = leadid.split('|');
				var lid = lead[1].split(',');
				checkboxcheck(invitemoduleid,lid);
			}
		} else if(invitemoduleid == '203') {
			if(contactid != '') {
				var contact = contactid.split('|');
				var cid = contact[1].split(',');
				checkboxcheck(invitemoduleid,cid);
			}
		} else if(invitemoduleid == '4') {
			if(userid != '') {
				var user = userid.split('|');
				var uid = user[1].split(',');
				checkboxcheck(invitemoduleid,uid);
			}
		} else if(invitemoduleid == '246') {
			if(groupid != '') {
				var group = groupid.split('|');
				var gid = group[1].split(',');
				checkboxcheck(invitemoduleid,gid);
			}
		} else if(invitemoduleid == '247') {
			if(rolesid != '') {
				var roles = rolesid.split('|');
				var rid = roles[1].split(',');
				checkboxcheck(invitemoduleid,rid);
			}
		} else if(invitemoduleid == '1') {
			if(randsid != '') {
				var rands = randsid.split('|');
				var rsid = rands[1].split(',');
				checkboxcheck(invitemoduleid,rsid);
			}
		}
	}
	function checkboxcheck(invitemoduleid,checkid) {
		for(var i=0;i <checkid.length;i++) {
			if(invitemoduleid == 201){
				$('#leads_rowchkbox'+checkid[i]).attr('checked',true);
			} else if(invitemoduleid == 203) {
				$('#contacts_rowchkbox'+checkid[i]).attr('checked',true);
			} else if(invitemoduleid == 4) {
				$('#employee_rowchkbox'+checkid[i]).attr('checked',true);
			}
		}
	}
}
//activity recurrence on day  
function recurrenceondaydataget() {
	$.ajax({
		url: base_url + "Task/recurrecneonthe",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#taskrecurrenceontheid').empty();
			$('#taskrecurrenceontheid').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#taskrecurrenceontheid')
					.append($("<option></option>")
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
					
				});
			}
		},
	});
}
//record data fetch
function usernamefetch(fieldname,tabname) {
	$('#taskcommonid').empty();
	$('#taskcommonid').select2('val','');
	$.ajax({
		url:base_url+"Task/recordnamefetch?fieldname="+fieldname+"&tabname="+tabname,
		dataType:'json',
		async :false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {   
				$.each(data, function(index) {
					$('#taskcommonid')
					.append($("<option></option>")
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
				});
			}	
			$('#taskcommonid').trigger('change');
		},
	});
}
{//SMS template and MAIL TEMPLATE value get
	function templatedatafetch(ddname,tablename,fieldid,fieldname,moduleid,type) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Task/templateddvalfetch?tablename="+tablename+"&fieldid="+fieldid+"&fieldname="+fieldname+"&moduleid="+moduleid+"&type="+type,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//required class removed from reminder concept
	function removevalidaterequired() {
		$("#taskrecurrenceeveryday").removeClass('validate[required]');
		$("#taskrecurrenceendsid").removeClass('validate[required]');
		$("#taskrecurrencedayid").removeClass('validate[required]');
		$("#taskrecurrenceontheid").removeClass('validate[required]');
	}
	function endsonddvalidateremove() {
		$("#afteroccurences").removeClass('validate[required]');
		$("#taskrecurrenceuntil").removeClass('validate[required]');
	}
}
{//template name fetch
	function templatenamefetch(datarowid){
		$.ajax({
			url: base_url+"Task/templatenamefetch?datarowid="+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var emailtemplate = data.emailtemplatesid;
					var leadtemplate = data.leadtemplateid;
					$("#reminderemailtemplatesid").select2('val',emailtemplate);
					$("#reminderleadtemplateid").select2('val',leadtemplate);
				} 	
			},
		});
	}
}
{//template file name fetch
	function remindertemplatefilenameget(smstempid) {
		$.ajax({
			url: base_url+"Task/smsfilenamefetch?smstempid="+smstempid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var smstemplate = data.templatename;
					smsfilenamevaluefetch(smstemplate);
				} 	
			},
		});
	}
	//editor value fetch
	function smsfilenamevaluefetch(filename) {
		$.ajax({
			url: base_url + "Task/editervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {
					var regExp = /a10s/g;
					var datacontent = data.match(regExp);
					if(datacontent !== null){	
						for (var i = 0; i < datacontent.length; i++) {
							data = data.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#remindersmstemplatevalue').val(data);
				}
			},
		});
	}
}
{//template file name fetch
	function remindermailfilenameget(mailtempid) {
		$.ajax({
			url: base_url+"Task/mailfilenamefetch?mailtempid="+mailtempid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var mailtemplate = data.templatename;
					mailfilenamevaluefetch(mailtemplate);
				} 	
			},
		});
	}
	//editor value fetch
	function mailfilenamevaluefetch(filename) {
		$.ajax({
			url: base_url + "Task/maileditervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {
					var regExp = /a10s/g;
					var datacontent = data.match(regExp);
					if(datacontent !== null) {	
						for (var i = 0; i < datacontent.length; i++) {
							data = data.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#remindermailtemplatevalue').val(data);
				}
			},
		});
	}
}
{//template file name fetch
	function invitetemplatefilenameget(smstempid) {
		$.ajax({
			url: base_url+"Activity/smsfilenamefetch?smstempid="+smstempid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var smstemplate = data.templatename;
					invitesmsfilenamevaluefetch(smstemplate);
				} 	
			},
		});
	}
	//editer value fetch
	function invitesmsfilenamevaluefetch(filename) {
		$.ajax({
			url: base_url + "Activity/editervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail'){
					var regExp = /a10s/g;
					var datacontent = data.match(regExp);
					if(datacontent !== null){	
						for (var i = 0; i < datacontent.length; i++) {
							data = data.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#invitesmstemplatevalue').val(data);
				}
			},
		});
	}
}
{//template file name fetch
	function invitemailfilenameget(mailtempid) {
		$.ajax({
			url: base_url+"Activity/mailfilenamefetch?mailtempid="+mailtempid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var mailtemplate = data.templatename;
					invitemailfilenamevaluefetch(mailtemplate);
				} 	
			},
		});
	}
	//editor value fetch
	function invitemailfilenamevaluefetch(filename) {
		$.ajax({
			url: base_url + "Activity/maileditervaluefetch?filename="+filename,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {
					var regExp = /a10s/g;
					var datacontent = data.match(regExp);
					if(datacontent !== null) {	
						for (var i = 0; i < datacontent.length; i++) {
							data = data.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#invitemailtemplatevalue').val(data);
				}
			},
		});
	}
	//set templates
	function settemplatesid(id) {
		$.ajax({
			url: base_url+"Task/gettemplatesid?taskid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.status) != 'FAILED') {
					if(data.smsid && data.smsid != 1){
						$('#reminderleadtemplateid').select2('val',data.smsid);
					}
					if(data.emailid && data.emailid != 1){
						$('#reminderemailtemplatesid').select2('val',data.emailid);
					}
					if(data.invmodeid && data.invmodeid != 1){
						var modid = data.invmodeid;
						var arraymode = modid.split(',');
						$('#taskremindermodeid').select2('val',arraymode);
					}
					if(data.invsmsid && data.invsmsid != 1){
						$('#leadtemplateid').select2('val',data.invsmsid);
					}
					if(data.invemailid && data.invemailid != 1){
						$('#emailtemplatesid').select2('val',data.invemailid);
					}
					if(data.moduleid && data.moduleid != 1){
						$('#taskinvitemoduleid').select2('val',data.moduleid);
					}
					if(data.viewid && data.viewid != 1){
						$('#inviteviewname').select2('val',data.viewid);
					}
				}
			},
		});
	}	
}
{
	function appendleadcontact(val,id) {
	   if(val == 2) {
		var table='contact';
	   }  else {
		var table='lead';
	   }
	   $('#'+id+'').empty();
	   $('#'+id+'').append($("<option></option>"));
	   $.ajax({
			url:base_url+'Task/getdropdownoptions?table='+table,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
			  if((data.fail) == 'FAILED') {
			  } else {
					$.each(data, function(index) {
						$('#'+id+'')
						.append($("<option></option>")
						.attr("value",data[index]['id'])
						.text(data[index]['name']));
					});
				} 
			   $('#'+id+'').trigger('change');
			},
	   });
	}
}