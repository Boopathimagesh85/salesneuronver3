$(document).ready(function()
{	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		customaddgrid();
		generaladdgrid();
		gridsettings();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$('#groupcloseaddform').hide();
	$("#tab1").click(function() {
		//To View the Content Area 
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
		$('#gview_generaladdgrid').css('opacity','0');
		setTimeout(function() {
			generaladdgrid();
		}, 20);
	});
	//user custom tab
	$("#tab2").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
		// To View the Content Area 
		$('#gview_customaddgrid').css('opacity','0');
		setTimeout(function() {
			customaddgrid();
		}, 20);
	});
	{ //Addicon for overlay
		$("#ruleaddicon").click(function(){
			$("#customrulesectionoverlay").removeClass("closed");
			$("#customrulesectionoverlay").addClass("effectbox");
			$('#dataupdate').hide();
			$('#datasubmit').show();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{ //cancel for overlay
		$("#rulecancelbutton").click(function(){
			$("#customrulesectionoverlay").addClass("closed");
			$("#customrulesectionoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	//role based module submit
	$("#gensharerulesubbtn").click(function() {
		//module grid details
		var gridData = jQuery("#generaladdgrid").jqGrid('getRowData');
		var allmoduleid = $('#generaladdgrid').jqGrid('getDataIDs');
		var content = {};
		$.each(allmoduleid,function(index,value) {
			content[index] = {};
			content[index]['moduleid']=value;
			content[index]['private']=$('#private_'+value+'').val();
			content[index]['pubread']=$('#pubread_'+value+'').val();
			content[index]['pubreadcrdedit']=$('#pubreadcrdedit_'+value+'').val();
			content[index]['pubreadcrdedtdel']=$('#pubreadcrdedtdel_'+value+'').val();
		});
		var shareruledata = JSON.stringify(content);
		var amp = '&';
		var datainfo = "shareruledata="+shareruledata;
		if(allmoduleid != '') {
			$.ajax({
				url:base_url+"Sharingrules/datasharerulecreateupdate",
				data:'datainformation='+amp+datainfo,
				type:'POST',
				cache:false,
				success :function(msg) {
					setTimeout(function() {
						generaladdgrid();
					},100);
					alertpopup('Data(s) stored successfully');
				}
			});
		} else {
			alertpopup('Please select the role');
		}
	});
	{//custom rule actions
		//reload
		$('#rulereloadicon').click(function(){
			clearform('cleardataform');
			clearinlinesrchandrgrid('customaddgrid');
			setTimeout(function(){
				var elementname = 'datasharetype,datasuperiorallow';
				elementdefvalueset(elementname);
			},200);
		});
		//edit
		$("#ruleediticon").click(function(){
			var datarowid = $('#customaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				resetFields();
				$("#datasubmit").hide();
				$("#dataupdate").show();
				customruleeditdatafetchfun(datarowid);
				$("#customrulesectionoverlay").removeClass("closed");
				$("#customrulesectionoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			} else {
				alertpopup("Please select a row");
			}
		});
		//delete
		$("#ruledeleteicon").click(function(){
			var datarowid = $('#customaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
			var datarowid = $('#customaddgrid').jqGrid('getGridParam','selrow'); 
			customruledatadeletefun(datarowid);
		});
	}
	//create custom rule
	$('#datasubmit').on('mousedown click',function(e){
		if(e.which==1 || e.which === undefined) {
			$("#customdatashareruleaddvalidation").validationEngine('validate');
		}
	});
	$("#customdatashareruleaddvalidation").validationEngine({
		onSuccess: function() {
			customruledataaddfun();
		},
		onFailure: function() {
			var dropdownid =['3','sharemoduleid','datasharedfromdd','datasharedtodd','datasharetype'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//update custom rule
	$('#dataupdate').on('mousedown click',function(e){
		if(e.which==1 || e.which === undefined) {
			$("#customdatashareruleupdatevalidation").validationEngine('validate');
		}
	});
	$("#customdatashareruleupdatevalidation").validationEngine({
		onSuccess: function() {
			customruledataupdatefun();
		},
		onFailure: function() {
			var dropdownid =['3','sharemoduleid','datasharedfromdd','datasharedtodd','datasharetype'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{//share from/to change events
		$('#datasharedfromdd').change(function(){ //from
			var typeid = $('#datasharedfromdd').find('option:selected').data('sharefromtypeid');
			var fromid = $('#datasharedfromdd').find('option:selected').data('sharefromid');
			var totypeid = $('#datasharedtodd').find('option:selected').data('sharetotypeid');
			var toid = $('#datasharedtodd').find('option:selected').data('sharetoid');
			if(!((typeid==totypeid) && (fromid==toid))){
				$('#datasharedfromtype').val(typeid);
				$('#datasharedfrom').val(fromid);
			} else {
				$("#datasharedfromdd").select2('val','');
				alertpopup('Cant choose the same value in shared from');
			}
		});
		$('#datasharedtodd').change(function(){ //to
			var fromtypeid = $('#datasharedfromdd').find('option:selected').data('sharefromtypeid');
			var fromid = $('#datasharedfromdd').find('option:selected').data('sharefromid');
			var typeid = $('#datasharedtodd').find('option:selected').data('sharetotypeid');
			var toid = $('#datasharedtodd').find('option:selected').data('sharetoid');
			if(!((typeid==fromtypeid) && (fromid==toid))){
				$('#datasharedtotype').val(typeid);
				$('#datasharedto').val(toid);
			} else {
				$("#datasharedtodd").select2('val','');
				alertpopup('Cant choose the same value in shared to');
			}
		});
	}
});
//General Add Grid
function generaladdgrid() {
	$('#generaladdgrid').jqGrid('GridUnload');
	$("#generaladdgrid").jqGrid({
		url:base_url+'Sharingrules/datasharingmodulelist',
		datatype: "xml",
		colNames:['Module Id','Category Name','Module Name','Private','Public: Readonly','Public: Read,Create/Edit','Public: Read,Create/Edit,Delete'],
   	    colModel:
        [
			{name:'moduleid',index:'module.moduleid',hidden:true, sortable: false},
			{name:'menucategoryname',index:'menucategoryname', sortable: false},
			{name:'menuname',index:'menuname', sortable: false},
			{name:'private',index:'', editable: true, search: false, align:"left", sortable: false, edittype: 'custom', editoptions: { value: "Yes:No" },formatter:radio, formatoptions: {disabled : false} },
			{name:'pubread',index:'', editable: true, search: false, align:"left", sortable: false, editoptions: { value: "Yes:No" },formatter:radio, formatoptions: {disabled : false} },
			{name:'pubreadcrdedit',index:'', editable: true, search: false, align:"left", sortable: false, editoptions: { value: "Yes:No" },formatter:radio, formatoptions: {disabled : false} },
			{name:'pubreadcrdedtdel',index:'', editable: true, search: false, align:"left", sortable: false, editoptions: { value: "Yes:No" },formatter:radio, formatoptions: {disabled : false}, width:400 },
		],
		pager:'#generaladdgridnav',
		sortname:'menucategory.sortorder',
		//scroll:1,
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function() {
			var allmodulevalueid = $('#generaladdgrid').jqGrid('getDataIDs');
			$.each(allmodulevalueid,function(index,value) {
				//private
				var pv = $('#private_'+value+'').val();
				( (pv=='Yes') ? $('#private_'+value+'').prop("checked", true) : $('#private_'+value+'').prop("checked", false) );
				//events
				$('#private_'+value+'').click(function(){
					var rowid = $(this).data('radbtnrowid');
					radiobuttonvalueset(rowid);
				});
				//read only
				var pr = $('#pubread_'+value+'').val();
				( (pr=='Yes') ? $('#pubread_'+value+'').prop("checked", true) : $('#pubread_'+value+'').prop("checked", false) );
				//events
				$('#pubread_'+value+'').click(function(){
					var rowid = $(this).data('radbtnrowid');
					radiobuttonvalueset(rowid);
				});
				//read,create,edit
				var pcre = $('#pubreadcrdedit_'+value+'').val();
				( (pcre=='Yes') ? $('#pubreadcrdedit_'+value+'').prop("checked", true) : $('#pubreadcrdedit_'+value+'').prop("checked", false) );
				//events
				$('#pubreadcrdedit_'+value+'').click(function(){
					var rowid = $(this).data('radbtnrowid');
					radiobuttonvalueset(rowid);
				});
				//read,create,edit,delete
				var pcred = $('#pubreadcrdedtdel_'+value+'').val();
				( (pcred=='Yes') ? $('#pubreadcrdedtdel_'+value+'').prop("checked", true) : $('#pubreadcrdedtdel_'+value+'').prop("checked", false) );
				//events
				$('#pubreadcrdedtdel_'+value+'').click(function(){
					var rowid = $(this).data('radbtnrowid');
					radiobuttonvalueset(rowid);
				});
			});
		}
	});
	//pagination remove
	$("#generaladdgridnav_center .ui-pg-table").addClass('hidedisplay');	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('generaladdgrid');
	formwithgridclass("generaladdgrid");
	gridsettings();
}
function radiobuttonvalueset(rowid) {
	var pvals = ( ($('#private_'+rowid+'').prop("checked")) ? 'Yes':'No' );
	$('#private_'+rowid+'').val(pvals);
	var prvals = ( ($('#pubread_'+rowid+'').prop("checked")) ? 'Yes':'No' );
	$('#pubread_'+rowid+'').val(prvals);
	var pcvals = ( ($('#pubreadcrdedit_'+rowid+'').prop("checked")) ? 'Yes':'No' );
	$('#pubreadcrdedit_'+rowid+'').val(pcvals);
	var pcdvals = ( ($('#pubreadcrdedtdel_'+rowid+'').prop("checked")) ? 'Yes':'No' );
	$('#pubreadcrdedtdel_'+rowid+'').val(pcdvals);
}
function radio(value, options, rawObject) {
	var id = options['colModel']['name'];
	var radioHtml = '<input type="radio" value='+value+' data-radbtnrowid="'+options['rowId']+'" name="ruleradio_'+options['rowId']+'" id="'+id+'_'+options['rowId']+'" />';
	return radioHtml;
}
//Custom Add Grid
function customaddgrid() {
	$('#customaddgrid').jqGrid('GridUnload');
	$("#customaddgrid").jqGrid({
		url:base_url + "Sharingrules/customsharerulegriddatafetch",
		datatype: "xml",
		colNames:['Module Name','Records Shared From','Records Shared TO','Access Type','Superior Allowed','Status'],
   	    colModel:
        [
			{name:'modulename',index:'module.menuname'}, 
			{name:'recordsharedfrom',index:'recordsharedfrom'},
			{name:'recordsharedto',index:'recordsharedto'},
			{name:'accesstype',index:'accesstype'},
			{name:'superiorallow',index:'superiorallow'},
			{name:'statusname',index:'status.statusname'}
		],
		pager:'#customaddgridnav',
		sortname:'datasharecustomruleid',
		sortorder:'desc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false,"top"],
		height: '100%',
		width: 'auto',	
	});
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('customaddgrid');
	formwithgridclass("customaddgrid");
	$(".ui-pg-input").attr('readonly',true);
}
//checks to remove the restricted tool bar
function hidegridcell(rowId, cellValue, rawObject, cm, rdata,cellpoint) {	
	if(rdata[cellpoint] == 'empty') {	
		return result = ' class="cbox_ronly" style="opacity:0"';
	}
}
// Grid Height Width Settings
function gridsettings()
{
	formwithgridclass("generaladdgrid");
	formwithgridclass("customaddgrid");
	var rolesistb = ["2","generaladdgrid","customaddgrid"];
	createistb(rolesistb);
}
{// New custom rule create function
	function customruledataaddfun() {
		var formdata = $("#customrulecreateionform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Sharingrules/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'True') {
					resetFields();
					$("#customaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('customaddgrid');
					var elementname = 'datasharetype,datasuperiorallow';
					elementdefvalueset(elementname);
					$("#customrulesectionoverlay").addClass("closed");
					$("#customrulesectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
				}
			},
		});
	}
	//custom rule edit data fetch
	function customruleeditdatafetchfun(datarowid) {
		$.ajax({
			url:base_url+"Sharingrules/fetchformdataeditdetails?dataprimaryid="+datarowid, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var modeid = data['moduleid'];
				var accesstype = data['accesstype'];
				var supallow = data['superiorallow'];
				var ftypeid = data['fromtypeid'];
				var totypeid = data['totypeid'];
				var sharefrom = data['sharedfrom'];
				var shareto = data['sharedto'];
				$("#sharemoduleid").select2('val',modeid).trigger('change');
				$("#datasharetype").select2('val',accesstype).trigger('change');
				$("#datasharedfromdd").select2('val',ftypeid+':'+sharefrom).trigger('change');
				$("#datasharedtodd").select2('val',totypeid+':'+shareto).trigger('change');
				//chkbox
				$('#datasuperiorallow').val(supallow);
				if(supallow == 'Yes') {
					$('#datasuperiorallowcboxid').prop('checked',true);
				} else {
					$('#datasuperiorallowcboxid').prop('checked',false);
				}
				$("#primaryid").val(datarowid);
			}
		});	
	}
	// custom rule data update function
	function customruledataupdatefun() {
		var formdata = $("#customrulecreateionform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Sharingrules/customruledataupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'True') {
					resetFields();
					$("#customaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('customaddgrid');
					var elementname = 'datasharetype,datasuperiorallow';
					elementdefvalueset(elementname);
					$("#rulereloadicon,#datasubmit").show();
					$("#dataupdate").hide();
					$("#primaryid").val('');
					$("#customrulesectionoverlay").addClass("closed");
					$("#customrulesectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
				}
			},
		});
	}
	//custom rule delete function
	function customruledatadeletefun(datarowid) {
		$.ajax({
			url: base_url + "Sharingrules/customruledeletefunction?primaryid="+datarowid,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlay").fadeOut();
					$("#customaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('customaddgrid');
					alertpopup('Rule is deleted successfully.');
				} else {
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Sorry rule not deleted');
				}
			},
		});	
	}
}