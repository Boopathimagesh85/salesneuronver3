$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling	
		firstfieldfocus();
		stocktakingview();
	}
	// tab group hide
	$('.tabgroupstyle').addClass('hidedisplay');
	datechange =0;
	daterangefunction('startdate','enddate');
	Operation = 0; //for pagination
	timer_interval = null;
	{// form height
		var saleswindow = $(window).height();
		var saleformheight =  saleswindow - 92;
		$(".salesmdaddformcontainer").css("height",saleformheight);
	}
	
	pdfpreviewtemplateload(103);
	rfidbarcode = '1';
	rfidstockmaxentry = 0;
	rfidstoragemaxentry = 0;
	rfct = 0;
	expectedarray = new Array();
	expectedarraygrosswt = new Array();
	expectedarraynetwt = new Array();
	scannedarray = new Array();
	scannedarraygrosswt = new Array();
	scannedarraynetwt = new Array();
	newscannedarray = new Array();
	missedarray = new Array();
	missedarraygrosswt = new Array();
	missedarraynetwt = new Array();
	conflictarray = new Array();
	conflictarraygrosswt = new Array();
	conflictarraynetwt = new Array();
	barcodearray = new Array();
	scannednosarray = new Array();
	expectedgrosswt = 0;
	expectednetwt = 0;
	finalscannedgrosswt = 0;
	finalscannednetwt = 0;
	missedgrosswt = 0;
	missednetwt = 0;
	conflictgrosswt = 0;
	conflictnetwt = 0;
	weight_round = $('#weight_round').val();
	imgcurrentid = '';
	{// toggle start /stop
		$('#stopprocess').click(function() {
			var modeid = $('#brmode').val();
			var deviceno = $('#deviceno').val();
			var connectioncheck = $('#stocktakingcode').find('option:selected').data('connectioncheck');
			var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
			var loopexecute = 0;
			if(modeid == 2) {
				if(connectioncheck == 'Yes') {
					var connectstatus = $('#connectstatus').val();
					if(connectstatus == 'Not Connected' || connectstatus == 'NotConnected') {
						loopexecute = 1;
						alertpopup('Device is not connected! Please check and try again!');
					} else if(connectstatus == 'Connected') {
						loopexecute = 0;
					} else {
						loopexecute = 1;
						alertpopup('Kindly start app and connect the device');
					}
				} else {
					loopexecute = 0;
				}
			}
			if(loopexecute == 0 ) {
				$('#stopprocess-div').addClass('hidedisplay');
				$('#startprocess-div').removeClass('hidedisplay');
				$('#itemtagnumber').attr('disabled',true);
				if($.trim($('#stocktakingsessioncreationid').val()) != '') {
					$('#clearprocess-div').removeClass('hidedisplay');
				}else {
					stopprocessbase();
				}
				if(startstoptrigger == 'Yes') {
					$.ajax({
						url:base_url+"Stocktaking/updaterfidsummary",
						data:{deviceno:deviceno,message:'stop'},
						method: "POST",
						dataType:'json',
						async:false,
						success :function(data) {
							
						},
					});
				}
				$('#stocktakeformstatus').val('stop'); 
			}
		});
	}
	{// For Stock Taking Form Details
		$("#addicon").click(function(){
			addslideup('stocktakingview','stocktakingform');
			resetFields();
			$('#reloadicon').trigger('click');
			$('#dataupdatesubbtn').hide();
			$('#dataaddsbtn').show();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			$('#stocktakingheaderform :input').attr('disabled', false);
			$('#itemtagnumber').attr('disabled', true);
			$('#prevstocktakingsessionid,#storagedatahidden,#conflictdatahidden,#stocktakingsessioncreationid').val('');
			getcurrentsytemdate('startdate');
			getcurrentsytemdate('enddate');
			$('#clearprocess-div').addClass('hidedisplay');
			$('#employeeid').select2('val',$('#hiddenemployeeid').val());
			$('#rfidstatusinfo').addClass('hidedisplay');
			loadstockcode();
		});
	}
	{// Close Add Screen
		$('#closeaddform').click(function(){
			if($('#startprocess').is(':visible') == false) {
				alertpopup('Stop the scan process and then close');
			}else {
				if( globalclosetrigger == "1") {
				  $('#stocktakingform').hide();
				  $('#stocktakingview').fadeIn(1500);
				} else { 
					alertpopupformclose("Do you wish to close form ?");
					$("#alertsfcloseyes").focus();
					$("#alertsfcloseyes").click(function(){
						var clsform = $("#alertsfcloseyes").val();                 
						if(clsform == "Yes") {
							$("#alertscloseyn").fadeOut("fast");
							$('#stocktakingform').hide();
							$('#stocktakingview').fadeIn(1000);
							resetFields();
							var deviceno = $('#stocktakingcode').find('option:selected').data('deviceno');
							setTimeout(function() {
								$.ajax({
									url: base_url +"Stocktaking/closebuttonresetsession",
									data:{deviceno:deviceno},
									type:"post",
									success: function(data) {
										
								   },
								});
							},5000);
							stocktakingrefreshgrid();
							$('.btmerrmsg').remove();
							$(".ftab").trigger('click');
							//For Keyboard Shortcut Variables
							addformupdate = 0;
							viewgridview = 1;
							addformview = 0;
							$("#slide-out").removeClass('hidedisplay');
							$(".off-canvas-wrap").css({'padding-left':'65px'});
						}
					});
				}
			}
        });
	}
	{//date
		//for date picker
		$('#date').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate: null,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#date').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});
	}
	{//stock takin session date
		//for date picker
		$('#stocktakinsessiondate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate: null,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#stocktakinsessiondate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		});
	}
	$('#stocktakingcode').change(function() {
		cleardata();
		productremovehideclass('productid',1);
		$('#prodparentcategoryname,#proparentcategoryid,#prodparentstoragename,#proparentstorageid').val('');
		$('#storagemodeid').select2('val',1).trigger('change');
		$('#productid').select2('val','');
		$('.loadprocessdiv').removeClass('hidedisplay');
		$('#rfidstatusinfo').addClass('hidedisplay');
		var branchid = $(this).find('option:selected').data('branchid');
		var modeid = $(this).find('option:selected').data('stocksessionmodeid');
		var typeid = $(this).find('option:selected').data('stocktakingtypeid');
		var deviceno = $(this).find('option:selected').data('deviceno');
		var stocksessionmethodid = $(this).find('option:selected').data('stocksessionmethodid');
		var connectioncheck = $(this).find('option:selected').data('connectioncheck');
		var startstoptrigger = $(this).find('option:selected').data('startstoptrigger');
		rfidbarcode = $(this).find('option:selected').data('itemtagdatatypeid');
		
		if (typeof refreshIntervalRFID !== 'undefined') {
			clearInterval(refreshIntervalRFID);
		}
		if(modeid == 2) {
			$("#startdatehdivid,#enddatehdivid").hide();
			$("#stocktakingpurityid").attr('data-validation-engine','validate[required]');
			var purity = $(this).find('option:selected').data('purityid');
			var purityid=purity.toString();
			if (purityid.indexOf(',') > -1) {	  
				var puritydata = purityid.split(",");
				$('#stocktakingpurityid').select2('val',puritydata).trigger('change');
			}else{
				$('#stocktakingpurityid').select2('val',purity).trigger('change');
			}
			if(stocksessionmethodid == 2) {
				$('#barcodeprocess,#rfidtagmachine').addClass('hidedisplay');
				$('#brmode').val('2');
				$('#rfidprocess').removeClass('hidedisplay');
				$('#itemtagnumber-div,#scanprocess-div').addClass('hidedisplay');
				$('#deviceno').val(deviceno);
				if(stocksessionmethodid == 2 && deviceno != '' && (connectioncheck == "Yes" || startstoptrigger == "Yes")) {
					$('#rfidstatusinfo').removeClass('hidedisplay');
					$("#rfidstatusinfobtn").prop('value', 'Waiting...');
					$("#rfidstatusinfobtn").css("background-color", "red");
					refreshIntervalRFID = setInterval(rfiddevicetable, 5000);
				}
				Materialize.updateTextFields();
			} else if(stocksessionmethodid == 4) {
				$('#barcodeprocess,#rfidprocess').addClass('hidedisplay');
				$('#brmode').val('2');
				$('#rfidtagmachine').removeClass('hidedisplay');
				$('#itemtagnumber-div,#scanprocess-div').addClass('hidedisplay');
				$('#deviceno').val('');
				Materialize.updateTextFields();
			} else {
				$('#rfidprocess,#rfidtagmachine').addClass('hidedisplay');
				$('#brmode').val('1');
				$('#barcodeprocess').removeClass('hidedisplay');
				$('#itemtagnumber-div,#scanprocess-div').removeClass('hidedisplay');
				$('#deviceno').val('');
			}
			fieldhideshow(typeid);
		} else {
			$("#startdatehdivid,#enddatehdivid").show();
			$("#stocktakingpurityid").attr('data-validation-engine','');
		}
		userhideshow(branchid);	// branch based user display
		$('#stocktakingpurityid').attr('readonly',true);
	});
	//RFID Device Add
	$("#rfiddeviceaddicon").click(function() {
		sectionpanelheight('rfiddevicesectionoverlay');
		$("#rfiddevicesectionoverlay").removeClass("closed");
		$("#rfiddevicesectionoverlay").addClass("effectbox");
		M.updateTextFields();
		firstfieldfocus();
	});
	$("#rfiddeviceclosebtn").click(function(){
		$("#rfiddevicesectionoverlay").removeClass("effectbox");
		$("#rfiddevicesectionoverlay").addClass("closed");
	});
	// category tree
	{
		$('#dl-promenucategory').dlmenu({
		animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
	}
	// RFID Tag Machine Overlay
	{	
		$('#rfidtagmachine').click(function() {
			var stocktakingcode = $('#stocktakingcode').find('option:selected').data('stocksessionmethodid');
			var lotnumberid = $('#lotnumberid').val();
			if(stocktakingcode == '4') {
				$('#totalsalesoverlay').fadeIn();
			} else {
				alertpopup('Please select Lot Details to proceed');
			}
		});
	}
	// parent category select
	$('#prodcategorylistuldata li').click(function() { 
		var name=$(this).attr('data-listname');
		var listid=$(this).attr('data-listid'); 
		$('#prodparentcategoryname').val(name);
		$('#proparentcategoryid').val(listid);
		emptyexpectedmissed();
		if(listid != '') {
			categorybasedproducthideshow(listid);
		}else {
			$("#productid option").removeClass("ddhidedisplay");
			$("#productid option").prop('disabled',false);
		}
	});
	// Storage tree
	{
		$('#dl-promenustorage').dlmenu({
			animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
	}
	// parent storage select
	$('#prodstoragelistuldata li').click(function() { 
		var name=$(this).attr('data-listname');
		var listid=$(this).attr('data-listid'); 
		$('#prodparentstoragename').val(name);
		$('#proparentstorageid').val(listid);
		emptyexpectedmissed();
		$("#productid").select2('val','');
		$("#productid option").removeClass("ddhidedisplay");
		$("#productid option").prop('disabled',false);
	});
	$('#startprocess').click(function() {
		datechange =1;
		$("#adddataimportfiletabvalidate").validationEngine('validate');
	});
	jQuery("#adddataimportfiletabvalidate").validationEngine({
		onSuccess: function() {
			$('#productid,#prodparentcategoryname,#prodparentstoragename').attr('disabled', true);
			if(expectedarray.length == 0) {
				alertpopup('Unable to scan the item.Because Expected item not available');
			} else {
				var modeid = $('#brmode').val();
				var deviceno = $('#deviceno').val();
				var connectioncheck = $('#stocktakingcode').find('option:selected').data('connectioncheck');
				var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
				var loopexecute = 0;
				if(modeid == 2) {
					if(connectioncheck == 'Yes') {
						var connectstatus = $('#connectstatus').val();
						if(connectstatus == 'Not Connected' || connectstatus == 'NotConnected') {
							loopexecute = 1;
							alertpopup('Device is not connected! Please check and try again!');
						} else if(connectstatus == 'Connected') {
							loopexecute = 0;
						} else {
							loopexecute = 1;
							alertpopup('Kindly start app and connect the device');
						}	
					} else {
						loopexecute = 0;
					}
					if(loopexecute == 0 && $('#stocktakingsessioncreationid').val() == ''){
						if(startstoptrigger == 'Yes') {
							var startstopstatus = 0;
							if($('#stocktakingsessioncreationid').val() == '') {
								$.ajax({
									url:base_url+"Stocktaking/checkduplicatedeviceisconnected",
									data:{deviceno:deviceno},
									method: "POST",
									dataType:'json',
									async:false,
									success :function(data) {
										startstopstatus = data['connectstatus'];
									},
								});
							}
							if(startstopstatus == 1){
								loopexecute = 1;
								alertpopup('Device Already in use');
							}
						}
					}
				}
				$('#processoverlay').show();
				if(loopexecute == 0) {
					$('#startprocess-div,.loadprocessdiv').addClass('hidedisplay');
					$('#stopprocess-div').removeClass('hidedisplay');
					$('#stopprocess').css('background-color', '#ff0707');
					$('#itemtagnumber').attr('disabled',false);
					$("#prodstoragelistbutton,#prodcategorylistbutton,#storagemodeid,#counterid").attr('disabled', true);
					$("#employeeid,#stocktakingcode,#startdate,#enddate").attr('disabled', true);
					$('#itemtagnumber').focus();
					$('#stocktakeformstatus').val('start'); 
					if($('#stocktakingsessioncreationid').val() == '') {
						createstocksession();
					}
					if(modeid == 2) {
						clearexistdetails(deviceno);
						refreshIntervalId = setInterval(getrfidvishal, 5000);
						/* setTimeout(function(){
							if(startstoptrigger == 'Yes') {
								$.ajax({
									url:base_url+"Stocktaking/updaterfidsummary",
									data:{deviceno:deviceno,message:'clear'},
									method: "POST",
									dataType:'json',
									async:true,
									success :function(data) {
										
									},
								});
							}
						},1000); */
						setTimeout(function(){
							if(startstoptrigger == 'Yes') {
								$.ajax({
									url:base_url+"Stocktaking/updaterfidsummary",
									data:{deviceno:deviceno,message:'start'},
									method: "POST",
									dataType:'json',
									async:true,
									success :function(data) {
										
									},
								});
							}
						},500);
					 }
				}
				$('#processoverlay').hide();
			}
		},
		onFailure: function() {
			var dropdownid =['3','branchid','employeeid','modeid'];
			dropdownfailureerror(dropdownid);
		}
	});
	// for mohan jewellery alone this code
	$('#itemtagnumber').change(function() {
		if($(this).val() != '') {
			if(rfidbarcode == 1) {
				s = $(this).val().substr(-4);
				s = ''+1+s;
				s = parseInt(s);
			} else {
				s = $(this).val();
			}
			scanneddetails($.trim(s));
		}
	});
	$('#reloadicon').click(function() {
		if($('#startprocess').is(':visible') == false) {
			alertpopup('Stop the scan process and then click reload');
		} else {
			resetFields();
			getcurrentsytemdate('date');
			$('#startprocess-div').removeClass('hidedisplay');
			$('#stopprocess-div').addClass('hidedisplay');
			cleardata();
		}
	});
	$('#productid').change(function() {
		emptyexpectedmissed();
	});
	$('#dataaddsbtn').click(function() {
		if(scannedarray.length == 0) {
			alertpopup('Unable to save the item.Because scanned item not available');
		} else if($('#startprocess').is(':visible') == false) {
			alertpopup('Stop the scan process and then save');
		} else{
			addform();
		}
	});
	//Tag template print action
	$("#printicon").click(function() {
		tagtemplatesload(103); //Tagtemplate function from Base JS or local function
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		var templateid = 0;
		if(datarowid) {
			$("#tagtemplateoverlay").fadeIn();
		} else {
			alertpopup(selectrowalert);
		}
	});
	//Tag template print overlay close
	$("#tagtempprintcancel").click(function() {
		$("#tagtemplateoverlay").fadeOut();
	});
	//TagTemplate Overlay Submit
	$("#tagtempprintinprinter").click(function() {
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		var templateid = $("#tagtemplateprview").find('option:selected').val();
		if(datarowid) {
			$.ajax({
				url: base_url + "Stocktaking/stktagtemplatereprint?primaryid="+datarowid+"&templateid="+templateid,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'DBERROR') {	
						alertpopup('Error on template query!!!Please choose other template!!!');
					} else if(nmsg == 'SUCCESS') {
						$("#tagtemplateoverlay").fadeOut();
						alertpopup('Processed');
					} else {
						alertpopup(msg);
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	//Print template action click
	$('#printpreviewicon').click(function() {
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		var snumber = $('#stocktakingviewgrid div.gridcontent div.active ul li.stocktakingnumber-class').text();
		if (datarowid) {
			$('#printpdfid').val(datarowid);
			$('#printsnumber').val(snumber);
			$("#templatepdfoverlay").fadeIn();
			$('#pdftemplateprview').select2('val','');
		} else {
			alertpopup(selectrowalert);
		}
	});
	//Print template print overlay close
	$("#pdfpreviewcancel").click(function() {
		$("#templatepdfoverlay").fadeOut();
	});
	//PrintTemplate Overlay Submit
	$("#pdffilepreviewintab").click(function() {
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		var templateid = $("#pdftemplateprview").find('option:selected').val();
		var snumber = $('#stocktakingviewgrid div.gridcontent div.active ul li.stocktakingnumber-class').text();
		snumber = 'STK'+snumber;
		if(datarowid){
			//htmlprintbase(datarowid,templateid,stocktakingnumber);
			printautomate(datarowid,templateid,snumber);
		} else {
			alertpopup(selectrowalert);
		}
	});
	//Delete
	$("#deleteicon").click(function() {
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$('#deletemode,#sessiondataid').select2('val','');
			$('.sessiondataiddiv').addClass('hidedisplay');
			$("#stocktakingdeleteoverlay").fadeIn();
		} else {
			alertpopup(selectrowalert);
		}	
	});
	// delete overlay close
	$("#closedeleteoverlay").click(function() {
		$("#stocktakingdeleteoverlay").fadeOut();
	});
	// delete mode change
	$("#deletemode").change(function() {
		var deletemode = $(this).find('option:selected').val();
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		if(deletemode == 2) {
			$('.sessiondataiddiv').addClass('hidedisplay');
			$("#sessiondataid").attr('data-validation-engine','');
		}else if(deletemode == 3) {
			$('.sessiondataiddiv').removeClass('hidedisplay');
			$("#sessiondataid").attr('data-validation-engine','validate[required]');
			loadsessiondata(datarowid);
		} 
	});
	//stock taking yes delete
	$("#takingdelete").click(function() {
		$("#stocktakingdeletevalidate").validationEngine('validate');
	});
	jQuery("#stocktakingdeletevalidate").validationEngine({
		onSuccess: function() {
			var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
			var deletemode = $('#deletemode').find('option:selected').val();
			if(deletemode == 2) {
				var sessiondataid = 0;
			} else if(deletemode == 3) {					
				var sessiondataid = $('#sessiondataid').find('option:selected').val();
			}				
			$.ajax({
				url: base_url + "Stocktaking/stocktakingdelete",
				data:{primaryid:datarowid,stocktakingsessionid:sessiondataid,deletemode:deletemode},
				type:"post",
				success: function(msg) {				
					var nmsg =  $.trim(msg);
					stocktakingrefreshgrid();
					$("#stocktakingdeleteoverlay").fadeOut();
					if (nmsg == "SUCCESS") { 					
						alertpopup(deletealert);
					} else {					
						alertpopup(msg);
					}			
				},
			}); 
		},
		onFailure: function() {
			var dropdownid =['1','sessiondataid'];
			dropdownfailureerror(dropdownid);
		}
	});
	// Device Clear in Stock Summary table
	$("#devicestatusicon").click(function() {
		$("#deviceclearoverlay").fadeIn();
		$('#deviceclearsubmit').show();
		$('#devicemacaddress').select2('val','').trigger('change');
		$('#devmacaddress,#devbluetoothname,#devconnectstatus,#devconnectrequestdatetime').val('');
	});
	// Device Clear in Stock Summary table
	$("#rfidstatusinfo").click(function() {
		$("#deviceclearoverlay").fadeIn();
		$('#deviceclearsubmit').hide();
		$('#devicemacaddress').select2('val','').trigger('change');
		$('#devmacaddress,#devbluetoothname,#devconnectstatus,#devconnectrequestdatetime').val('');
	});
	// Device ID change
	$("#devicemacaddress").change(function() {
		var macaddress = $(this).find('option:selected').data('macaddress');
		var bluetoothname = $(this).find('option:selected').data('bluetoothname');
		var connectstatus = $(this).find('option:selected').data('connectstatus');
		var connectrequestdatetime = $(this).find('option:selected').data('connectdatetime');
		$('#devmacaddress').val(macaddress);
		$('#devbluetoothname').val(bluetoothname);
		$('#devconnectstatus').val(connectstatus);
		$('#devconnectrequestdatetime').val(connectrequestdatetime);
		Materialize.updateTextFields();
	});
	$("#closedeviceclearverlay").click(function() {
		$("#deviceclearoverlay").fadeOut();
	});
	$("#deviceclearsubmit").click(function() {
		$("#deviceclearvalidate").validationEngine('validate');
	});
	jQuery("#deviceclearvalidate").validationEngine({
		onSuccess: function() {
			var macaddress = $('#devicemacaddress').find('option:selected').data('macaddress');
			$.ajax({
				url: base_url + "Stocktaking/alldeviceclearoverlay",
				data:{macaddress:macaddress},
				type:"post",
				success: function(msg) {
					var nmsg = $.trim(msg);
					stocktakingrefreshgrid();
					$("#deviceclearoverlay").fadeOut();
					if(nmsg == "SUCCESS") {
						alertpopup('Device is Cleared!');
					} else {					
						alertpopup('Unable to clear the device!');
					}			
				},
			}); 
		},
		onFailure: function() {
			var dropdownid =['1','sessiondataid'];
			dropdownfailureerror(dropdownid);
		}
	});
	$('#stocktakingsessionicon').click(function() {  // session creation
		addslideup('stocktakingview','productaddonformdiv');
		sessionformgrid();
	});
	$('#sessionidclose').click(function() {  // session close
		addslideup('productaddonformdiv','stocktakingview');
	});
	$('#rfidsettingclose').click(function() {  // session close
		addslideup('stockrfidsettingview','stocktakingview');
	});
	$("#sessionaddicon").click(function() {
		resetFields();
		sectionpanelheight('stocktakingsessionoverlay');
		$("#stocktakingsessionoverlay").removeClass("closed");
		$("#stocktakingsessionoverlay").addClass("effectbox");
		getcurrentsytemdate('stocktakinsessiondate');
		serialnumbergenerate();
		$("#branchid").select2('val',loggedinbranchid).trigger('change');
		$("#conflicttrackid").select2('val',2);
		$("#stocksessionmethodid").select2('val',3).trigger('change');
		$("#stocksessionmodeid").select2('val',2).trigger('change');
		firstfieldfocus();
		Materialize.updateTextFields();
	});
	$( window ).resize(function() {
		sectionpanelheight('stocktakingsessionoverlay');
	});
	$('.addsectionclose').click(function(){
		$("#stocktakingsessionoverlay").addClass("closed");
		$("#stocktakingsessionoverlay").removeClass("effectbox");
	});
	// Stock Session Create
	$('#stocktakingsessioncreate').click(function() {
		$("#stocktakingsessionvalidate").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#stocktakingsessionvalidate").validationEngine({
		onSuccess: function() {
			addsessionform();
		},
		onFailure: function() {
			var dropdownid =['3','branchid','purityid','stocktakingsessiontypeid'];
			dropdownfailureerror(dropdownid);
		}
	});
	$('#scannedcloseyes').click(function() {
		var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
		var storagemodeid = $('#storagemodeid').find('option:selected').val();
		if(storagemodeid == 2 || storagemodeid == 4) {
			var storageid = $('#storagedatahidden').val();
		}else if(storagemodeid == 3) {
			var storageid = $('#counterid').find('option:selected').val();
		}else if(storagemodeid == 5) {
			var storageid = $('#proparentstorageid').val();
		}
		var categoryid = $('#proparentcategoryid').val();
		var productid = $('#productid').find('option:selected').val();
		$('#scannedalert').fadeOut();
		var modeid =  '1';
		getscanneddata(stocktakingid,storageid,categoryid,productid,modeid);
	});
	$('#startprocessrfid').click(function(){
		$('#startprocessrfid-div').addClass('hidedisplay');
		$('#stopprocessrfid-div').removeClass('hidedisplay');
	});
	$('#stopprocessrfid').click(function(){
		$('#startprocessrfid-div').removeClass('hidedisplay');
		$('#stopprocessrfid-div').addClass('hidedisplay');
	});
	$('div.detailedlistoverlay').click(function(){
		var attrname = $(this).attr('id');
		var data = '';
		if(attrname == 'stock-expected-count-overlay') {
			data = expectedarray;
			$('#stockentrydetailoverlay .sectionheaderformcaptionstyle').text('Expected Stock Taking Details');
		} else if(attrname == 'stock-scanned-count-overlay'){
			data = scannedarray;
			$('#stockentrydetailoverlay .sectionheaderformcaptionstyle').text('Scanned Stock Taking Details');
		}else if(attrname == 'stock-missing-count-overlay') {
			data = missedarray;
			$('#stockentrydetailoverlay .sectionheaderformcaptionstyle').text('Missed Stock Taking Details');
		}else if(attrname == 'stock-conflict-count-overlay') {
			data = conflictarray;
			$('#stockentrydetailoverlay .sectionheaderformcaptionstyle').text('Conflict Stock Taking Details');
		}
		if(data == '') {
			alertpopup('Data Not Available');
		} else {
			$('#stockentrydetailoverlay').fadeIn();
			stocktakingdetailsgrid();
			var modeid = $('#brmode').val();
			var stocksessionmodeid = $("#stocktakingcode").find('option:selected').data('stocksessionmodeid');
			stocktakingdetailsdata(data,modeid,stocksessionmodeid);
			sumofstocktakingdetailsdata(data,modeid,stocksessionmodeid);
		}
	});
	$("#stocktakingdetailgridclose").click(function() {
		$("#stockentrydetailoverlay").fadeOut();
	});
	//for filter
	filtername = [];
	$("#filteraddcondsubbtn").click(function() {
		$("#filterformconditionvalidation").validationEngine("validate");	
	});
	$("#filterformconditionvalidation").validationEngine({
		onSuccess: function() {
			mainviewfilterwork(stocktakingview);
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$(".ddfilerchange").change(function() {
		dropdownfilterchange(stocktakingview);
	});
	//mode change
	$("#stocksessionmodeid").change(function() {
		var modeid = $("#stocksessionmodeid").val();
		if(modeid == 2) {
			$("#purityidhdivid").removeClass('hidedisplay');
			$("#purityid").attr('data-validation-engine','validate[required]');
		} else {
			$("#purityidhdivid").addClass('hidedisplay');
			$("#purityid").attr('data-validation-engine','');
		}
	});
	// method change
	$("#stocksessionmethodid").change(function() {
		var methodid = $(this).find('option:selected').val();
		if(methodid == 2) { // RFID
			$("#stocksessiondeviceno_div").removeClass('hidedisplay');
			$("#stocksessiondeviceno").attr('data-validation-engine','validate[required]');
			$('#itemtagdatatypeid').select2('val','1').trigger('change');
			$('#itemtagdatatypeid').prop('disabled', true);
		} else {
			$('#itemtagdatatypeid').prop('disabled',false);
			if(methodid == 4) { // RFID Tag Machine (Rasi Jewellery)
				$('#itemtagdatatypeid').select2('val','1').trigger('change');
				$('#itemtagdatatypeid').prop('disabled',true);
			}
			$("#stocksessiondeviceno").select2('val','');
			$("#stocksessiondeviceno_div").addClass('hidedisplay');
			$("#stocksessiondeviceno").attr('data-validation-engine','');
		}
	});
	// type change
	$("#stocktakingsessiontypeid").on('change', function(){
		var selected = $(this).val();
		if(selected != null)
		{
		  if(selected.indexOf('5')>=0){
			  $(this).val('5').select2();
		  }
		  if(selected.indexOf('6')>=0){
			  $(this).val('6').select2();
		  }
		}
	});
	// storage mode change
	$("#storagemodeid").change(function() {
		var modeid = $(this).find('option:selected').val();
		$('#counterid').select2('val','');
		$('#proparentstorageid,#prodparentstoragename,#storagecount,#storagedatahidden,#storagename').val('');
		$('.counteridhide,.storagetreediv,.loadstoragediv').addClass('hidedisplay');
		if(modeid == 3) { // Single Storage
			$('.counteridhide,.loadprocessdiv').removeClass('hidedisplay');
			$('.storagetreediv').addClass('hidedisplay');
		}else if(modeid == 5) { // Parent Storage
			$('.counteridhide').addClass('hidedisplay');
			$('.storagetreediv,.loadprocessdiv').removeClass('hidedisplay');
		}else if(modeid == 2 || modeid == 4) { // RFID Storage & Multiple Storage
			$('.storagenamediv,.storageappenddiv,.storagestartdiv').hide();
			$('.loadstoragediv').removeClass('hidedisplay');
			$('.loadprocessdiv').addClass('hidedisplay');
			sectionpanelheight('storagedataoverlay');
			$("#storagedataoverlay").removeClass("closed");
			$("#storagedataoverlay").addClass("effectbox");
			storagedatagrid();
			if(modeid == 2){
				$('.storagestartdiv').show();
			}else if(modeid == 4){
				$('.storagenamediv,.storageappenddiv').show();
			}
			Materialize.updateTextFields();
		}
		//emptysummaryarray();
		cleardata();
	});
	// load storage click
	$("#loadstorage").click(function() {
		var modeid = $('#storagemodeid').find('option:selected').val();
		sectionpanelheight('storagedataoverlay');
		$("#storagedataoverlay").removeClass("closed");
		$("#storagedataoverlay").addClass("effectbox");
		//storagedatagrid();
		if(modeid == 2){
			$('.storagestartdiv').show();
		}else if(modeid == 4){
			$('.storagenamediv,.storageappenddiv').show();
		}
	});
	// storage add click
	$("#storageappend").click(function() {
		var storagename = $.trim($('#storagename').val());
		if(storagename != '') {
			checkproductstorage(storagename);
			cleardata();
		}else {
			alertpopupdouble('Please give the storage name');
		}
	});
	// storage name change
	$("#storagename" ).on( "keypress", function(e) {
		if (e.which == 13) {
			var storagename = $.trim($('#storagename').val());
			if(storagename != '') {
				cleardata();
				checkproductstorage(storagename);
			}
		}
	});
	// storage data delete
	$("#storagedatadeleteicon").click(function() {
		var id = $('#storagedatagrid div.gridcontent div.active').attr('id');
		if(id) {
			deletegriddatarow('storagedatagrid',id);
			var gridlength = $('#storagedatagrid .gridcontent div.data-content div.data-rows').length;
			$('#storagecount').val(gridlength);
			cleardata();
		} else {
			alertpopupdouble('Please select the record');
		}
	});
	// storage data submit
	$("#storagedatacreate").click(function() {
		if($('#storagedatagrid .gridcontent div.data-content div.data-rows').length == 0) {
			alertpopupdouble('Please add the data otherwise close the form');
		} else {
			cleardata();
			$('#storagedatahidden').val(getgridcolcolumnvalue('storagedatagrid','','counterid'));
			$("#storagedataoverlay").addClass("closed");
			$("#storagedataoverlay").removeClass("effectbox");
			//$("#loadprocess").trigger('click');
			if(getgridcolcolumnvalue('storagedatagrid','','counterid') != '') {
				if($("#startprocess").is(':visible') == true) {
					var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
					var checkstatus = checkformvalue();
					if(checkstatus == 1) {
						checksessiontable(stocktakingid);
					} else {
						alertpopup('Please fill the relevant form');
					}
				} else {
					alertpopup('Already start process is going on');
				}
			}
		}
	});
	// storage data start
	$("#storagestart").click(function() {
		$('.storagestartdiv').addClass('hidedisplay');
		$('.storagestopdiv').removeClass('hidedisplay');
		var deviceno = $('#deviceno').val();
		clearexistdetails(deviceno);
		refreshIntervalIdstorage = setInterval(getrfidstorage,5000);
		 
	});
	// storage data stop
	$("#storagestop").click(function() {
		$('.storagestartdiv').removeClass('hidedisplay');
		$('.storagestopdiv').addClass('hidedisplay');
		 clearInterval(refreshIntervalIdstorage);
	});
	$('#counterid').click(function(){
		cleardata();
	});
	// load click
	$("#loadprocess").click(function() {
		if($("#startprocess").is(':visible') == true) {
			//emptysummaryarray();
			cleardata();
			var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
			var checkstatus = checkformvalue();
			if(checkstatus == 1) {
				checksessiontable(stocktakingid);
			}else {
				alertpopup('Please fill the relevant form');
			}
		}else {
			alertpopup('Already start process is going on');
		}
	});
	// storage data close
	$("#storagedataclose").click(function() {
		$("#storagedataoverlay").addClass("closed");
		$("#storagedataoverlay").removeClass("effectbox");
	});
	// conflict data yes
	$("#conflictcloseyes").click(function() {
		var id = $('#conflictdatahidden').val();
		var modeid = $('#brmode').val();
		conflictdata(id,modeid);
		$('#conflictalert').fadeOut();
	});
	// conflict data no
	$("#conflictcloseno").click(function() {
		$('#conflictalert').fadeOut();
	});
	// clear sesssion
	$("#clearprocess").click(function() {
		$('#clearsessionalert').fadeIn();
	});
	// clear sesssion yes
	$("#clearsessioncloseyes").click(function() {
		var currentsessionid = $('#stocktakingsessioncreationid').val();
		var modeid = $('#brmode').val();
		var deviceno = $('#deviceno').val();
		var connectioncheck = $('#stocktakingcode').find('option:selected').data('connectioncheck');
		var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
		$('#stocktakeformstatus').val('clear');
		if(startstoptrigger == 'Yes' && modeid == 2) {
			$.ajax({
				url:base_url+"Stocktaking/updaterfidsummary",
				data:{deviceno:deviceno,message:'clear'},
				method: "POST",
				dataType:'json',
				async:false,
				success :function(data) {
					
				},
			});
			setTimeout(function(){
				
				$.ajax({
					url:base_url+"Stocktaking/updaterfidsummary",
					data:{deviceno:deviceno,message:'delete'},
					method: "POST",
					dataType:'json',
					async:true,
					success :function(data) {
						
					},
				});
				
			},3000);
		}
		resetsession(currentsessionid);
		stopprocessbase();
		$('#clearprocess-div').addClass('hidedisplay');
		$('#clearsessionalert').fadeOut();
	});
	// clear sesssion no
	$("#clearsessioncloseno").click(function() {
		$('#clearsessionalert').fadeOut();
	});
	 //Print
	$("#printicon_old").click(function(){
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$('#stocksessiondetailoverlay').fadeIn();
			stocksessiondetailsgrid();
			stocksessiondetailsdata(datarowid);
		} else {
			alertpopup(selectrowalert);
		}	
	});
	// print close
	$("#stocksessiondetailgridclose").click(function(){
		$('#stocksessiondetailoverlay').fadeOut();	
	});
	//prints
	$('#stocksessiondetailprint').click(function() {
		var stocksessiondetailhidden = $('#stocksessiondetailhidden').val();
		var datarowid = $('#stocktakingviewgrid div.gridcontent div.active').attr('id');
		//var snumber = $('#salesgrid div.gridcontent div.active ul li.salesnumber-class').text();
		if (stocksessiondetailhidden) {
			$('#printpdfid').val(datarowid);
			$('#stocksessionid').val(stocksessiondetailhidden);
			$("#templatepdfoverlay").fadeIn();
			$('.printtempoverlay').css('z-index', 9999);
			var viewfieldids = 103; //moduleid.			
			pdfpreviewtemplateload(viewfieldids);
			//setdefaultprinttemplate(datarowid);
		} else {
			alertpopupdouble("Please select a row");
		}
	});
	// RFID Device Overlay Grid
	$('#rfiddeviceicon').click(function() {
		$('.tabgroupstyle').removeClass('hidedisplay');
		addslideup('stocktakingview','stockrfidsettingview');
		$('.ftabstockrfidsettingview,#tab1').addClass('active');
		resetFields();
		$('#macaddress,#bluetoothname,#devicename').val('');
		$('#connectioncheck,#startstoptrigger').select2('val','No').trigger('change');
		rfiddeviceoverlaystatus = 1;
		rfiddevicegrid();
	});
	// RFID Device Information Overlay Close
	$('#rfiddeviceoverlayclose').click(function() {
		$('.tabgroupstyle').addClass('hidedisplay');
		resetFields();
		if(rfiddeviceoverlaystatus == 1) {
			addslideup('stockrfidsettingview','stocktakingview');
			$('#addrfiddevicebtn,#editrfiddevicebtn,#rfiddevicedeleteicon').show();
			$('#editrfiddevicebtn').hide();
			rfiddeviceoverlaystatus = 0;
		}
	});
	// Bluetooth Name change auto set in Device Name
	$('#bluetoothname').focusout(function() {
		var value = $(this).val();
		$('#devicename').val(value);
	});
	// Create New RFID Device Information
	$("#addrfiddevicebtn").click(function() {
		$("#addrfiddeviceformvalidate").validationEngine("validate");	
	});
	$("#addrfiddeviceformvalidate").validationEngine({
		onSuccess: function() {
			addrfiddevicedetails();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	// Edit and set RFID Device Information in form fields
	$("#rfiddeviceediticon").click(function() {
		var datarowid = $('#rfiddevicegrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var btnid = ['addrfiddevicebtn','editrfiddevicebtn'];
			var btnsh = ['hide','show'];
			rfiddeviceaddupdatebtnsh(btnid,btnsh);
			$('#rfiddevicedeleteicon,#rfiddeviceediticon').hide();
			rfiddevicedetailsretrive(datarowid);
		} else {
			alertpopup(selectrowalert);
		}
	});
	// Update RFID Device Information
	$('#editrfiddevicebtn').click(function() {
		$("#editrfiddeviceformvalidate").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
    jQuery("#editrfiddeviceformvalidate").validationEngine({
		onSuccess: function() {			
			rfiddeviceupdate();
		},
		onFailure: function() {
			var dropdownid =['','',''];
			dropdownfailureerror(dropdownid);
		}	
	});
	// Delete RFID Device Information
	$("#rfiddevicedeleteicon").click(function() {	 	
		var datarowid = $('#rfiddevicegrid div.gridcontent div.active').attr('id');
		if(datarowid) {																	
			rfiddeviceinformationdelete(datarowid);
		} else {
			alertpopup(selectrowalert);
		}
	});
	{ // Image Display Code starts here
		$(document).on("click",".innergridimagedisplay",function() {
			imgcurrentid = '';
			var ids = imgcurrentid = $(this).closest('div').attr('id');
			var imgbarcodevalue = '';
			$('.prevnexthideshow').removeClass('hidedisplay');
			itemtagimagedisplay(imgbarcodevalue,ids);
		});
		// Image Preview
		$(document).on("click",".tagimageoverlaypreview",function() {
			var imgbarcodevalue =  $(this).closest('div').attr('id');
			var itemtagid = '';
			var removetext = 'product-list';
			imgbarcodevalue = $.trim(imgbarcodevalue.replace(removetext,''));
			$('.prevnexthideshow').addClass('hidedisplay');
			itemtagimagedisplay(imgbarcodevalue,itemtagid);
		});
		// Itemtagid - Previous Click
		$(document).on("click",".baseitemtagprevid",function() {
			if(imgcurrentid != '') {
				var ids = $('#stocktakingdetailsgrid .gridcontent div.data-content #'+imgcurrentid+'').prev().attr('id');
				ids = typeof ids == 'undefined' ? '' : ids;
				if(ids != '') {
					imgcurrentid = ids;
					var imgbarcodevalue = '';
					$('.prevnexthideshow').removeClass('hidedisplay');
					itemtagimagedisplay(imgbarcodevalue,ids);
				} else {
					alertpopupdouble('No More items to display');
				}
			}
		});
		// Itemtagid - Next Click
		$(document).on("click",".baseitemtagnextid",function() {
			if(imgcurrentid != '') {
				var ids = $('#stocktakingdetailsgrid .gridcontent div.data-content #'+imgcurrentid+'').next().attr('id');
				ids = typeof ids == 'undefined' ? '' : ids;
				if(ids != '') {
					imgcurrentid = ids;
					var imgbarcodevalue = '';
					$('.prevnexthideshow').removeClass('hidedisplay');
					itemtagimagedisplay(imgbarcodevalue,ids);
				} else {
					alertpopupdouble('No More items to display');
				}
			}
		});
		// Tag image preview close click
		$("#baseitemtagimgprevclose").click(function() {
			imgcurrentid = '';
			$('#baseitemtagimgpreview').fadeOut();
		});
	}
	// Export Overlay Details
	$('#stocktakingexportexcel').click(function() {
		var stockitemids = new Array();
		var headerdetails = $('#stockentrydetailoverlay .sectionheaderformcaptionstyle').text();
		$('#stockentrydetailoverlay .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			stockitemids.push(datarowid);
		});
		var summaryinfoname = headerdetails.replace(' Stock Taking Details', '');
		if(summaryinfoname == 'Conflict') {
			var griddata = getgridrowsdata('stockentrydetailoverlay');
			var criteriagriddata = JSON.stringify(griddata);
			var criteriacnt = $('#stockentrydetailoverlay .gridcontent div.data-content div').length;
		} else {
			var criteriagriddata = '';
			var criteriacnt = '';
		}
		var stocktakingvalue = $('#stocktakingcode').find('option:selected').val();
		var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
		if (stockitemids != '' || criteriacnt != '') {
			var pdfgeneratename = summaryinfoname;
			var functionurl = 'Stocktaking/generateexcelexport';
			var target = '_blank';
			var form = $("<form action="+functionurl+" class='hidedisplay' name='excelexport' id='excelexport' method='POST' target='"+target+"'><input type='hidden' name='pdfgeneratename' id='pdfgeneratename' value='"+pdfgeneratename+"' /><input type='hidden' name='stocktakingid' id='stocktakingid' value='"+stocktakingid+"' /><input type='hidden' name='stocktakingvalue' id='stocktakingvalue' value='"+stocktakingvalue+"' /><input type='hidden' name='stockitemids' id='stockitemids' value='"+stockitemids+"' /><input type='hidden' name='criteriagriddata' id='criteriagriddata' value='"+criteriagriddata+"' /><input type='hidden' name='criteriacnt' id='criteriacnt' value='"+criteriacnt+"' />");
			$(form).appendTo('body');
			form.submit();
		} else {
			alertpopup(selectrowalert);
		}
	});
});
// Add RFID Device Details
function addrfiddevicedetails() {
	var formdata = $("#rfiddeviceform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
		url: base_url +"Stocktaking/addrfiddevicedetails",
		data: "datas=" + datainformation,
		type: "POST",
		async:false,
		cache:false,
		success: function(msg) { 
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				rfiddevicegrid();
				setTimeout(function(){
				$('#processoverlay').hide();
				},25);
				resetFields();
				alertpopup(savealert);
			} else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
		},
	});
}
// RFID device - Hide show button for create and update
function rfiddeviceaddupdatebtnsh(btnid,btnsh) {
	for(var m=0;m < btnid.length;m++) {
		if(btnsh[m] == 'show') {
			$('#'+btnid[m]+'').show();
			$('#'+btnid[m]+'').attr('data-shstatus','show');
		} else if(btnsh[m] == 'hide') {
			$('#'+btnid[m]+'').hide();
			$('#'+btnid[m]+'').attr('data-shstatus','hide');
		}		
	}
}
// Edit and set RFID Device Information in form fields
function rfiddevicedetailsretrive(datarowid) {
	$.ajax({
		url:base_url+"Stocktaking/rfiddevicedetailsretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#rfiddeviceid').val(data.rfiddeviceid);
			$('#macaddress').val(data.macaddress);
			$('#bluetoothname').val(data.bluetoothname);
			$('#devicename').val(data.devicename);
			$('#connectioncheck').select2('val',data.connectioncheck);
			$('#startstoptrigger').select2('val',data.startstoptrigger);
			Materialize.updateTextFields();
		}
	});
}
// RFID Device Information update
function rfiddeviceupdate() {
	var formdata = $("#rfiddeviceform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid= $('#rfiddeviceid').val();
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax({
		url: base_url + "Stocktaking/rfiddevicedetailsupdate",
		data: "datas=" + datainformation+"&primaryid="+datarowid,
		type: "POST",
		async:false,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			resetFields();
			$('#editrfiddevicebtn').hide();
			$('#addrfiddevicebtn,#rfiddeviceediticon,#rfiddevicedeleteicon').show();
			$('#purity').attr('disabled',false);
			if (nmsg == 'SUCCESS') {
				rfiddevicegrid();
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(updatealert);
				Materialize.updateTextFields();
			} else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
				Materialize.updateTextFields();
			}            
		},
	});
}
// RFID Device Information Delete
function rfiddeviceinformationdelete(datarowid) {
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
	$.ajax({
		url: base_url + "Stocktaking/rfiddeviceinformationdelete",
		data:{primaryid:datarowid},
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				rfiddevicegrid();
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
			} else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
			}
		},
	});
}
// RFID Device Grid
function rfiddevicegrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $('#rfiddevicegrid').width();
	var wheight = $('#rfiddevicegrid').height();
	/*col sort*/
	var sortcol = $("#rfiddevicesortcolumn").val();
	var sortord = $("#rfiddevicesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.rfiddeviceheadercolsort').hasClass('datasort') ? $('.rfiddeviceheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.rfiddeviceheadercolsort').hasClass('datasort') ? $('.rfiddeviceheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.rfiddeviceheadercolsort').hasClass('datasort') ? $('.rfiddeviceheadercolsort.datasort').attr('id') : '0';	
	var footername = '';
	var filterid = '';
	var conditionname = '';
	var filtervalue = '';
	$.ajax({
		url:base_url+"Stocktaking/rfiddevicegridinformationfetch?maintabinfo=rfiddevice&primaryid=rfiddeviceid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=1&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#rfiddevicegrid').empty();
			$('#rfiddevicegrid').append(data.content);
			$('#rfiddevicegridfooter').empty();
			$('#rfiddevicegridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('rfiddevicegrid');
			{//sorting
				$('.rfiddeviceheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.rfiddeviceheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#stonesettinggridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.rfiddeviceheadercolsort').hasClass('datasort') ? $('.rfiddeviceheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.rfiddeviceheadercolsort').hasClass('datasort') ? $('.rfiddeviceheadercolsort.datasort').data('sortorder') : '';
					$("#stonesettingsortorder").val(sortord);
					$("#stonesettingsortcolumn").val(sortcol);
					rfiddevicegrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('rfiddeviceheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					rfiddevicegrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#stonesettingpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					rfiddevicegrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$('#stonesettingpgrowcount').material_select();
		},
	});
}
function userhideshow(val) {
	var stocktypeid = "employeeid";
	$("#"+stocktypeid+" option").addClass("ddhidedisplay");
	$("#"+stocktypeid+" option").prop('disabled',true);
	$("#"+stocktypeid+" option[data-branchid="+val+"]").removeClass("ddhidedisplay");
	$("#"+stocktypeid+" option[data-branchid="+val+"]").prop('disabled',false);
	var employeeoption = $("#"+stocktypeid+" option[data-branchid="+val+"]").length;
	if(employeeoption == 1){
		setTimeout(function(){
		 var value = $("#"+stocktypeid+" option[data-branchid="+val+"]").attr('value');
		 $("#"+stocktypeid+"").select2('val',value).trigger('change');
		},100);
	}
}
function expecteddetails(storageid,branchid,purityid,modeid,sessionmodeid,startdate,enddate,categoryid,productid,lotnumberids) {
	var itemlist = '';
	emptyexpectedmissed();
	$.ajax({
		url:base_url+"Stocktaking/expecteddetails",
		type:"post",
		data:{storageid:storageid,branchid:branchid,purityid:purityid,modeid:modeid,sessionmodeid:sessionmodeid,startdate:startdate,enddate:enddate,categoryid:categoryid,productid:productid,rfidbarcode:rfidbarcode,lotnumberids:lotnumberids},
		dataType:'json',
		async:false,
		cache:false,
		success :function(msg) {
			
			var sessionid = $('#stocktakingcode').find('option:selected').val();
			var tagsymbol = 'B';
			
			if(msg.length > 0) {
				for(var i=0; i<msg.length;i++) {
					itemlist += divappenddata(msg[i],sessionid);
					expectedarray[i] = msg[i]['barcodeid'];
					expectedarraygrosswt[i] = msg[i]['grosswt'];
					expectedarraynetwt[i] = msg[i]['netwt'];
					
					missedarray[i] = msg[i]['barcodeid'];
					missedarraygrosswt[i] = msg[i]['grosswt'];
					missedarraynetwt[i] = msg[i]['netwt'];
					
					expectedgrosswt = parseFloat(expectedgrosswt) + parseFloat(msg[i]['grosswt']);
					expectednetwt = parseFloat(expectednetwt) + parseFloat(msg[i]['netwt']);
					missedgrosswt = parseFloat(expectedgrosswt);
					missednetwt = parseFloat(expectednetwt);
				}
				$('#expecteddata').append(itemlist);
				$('#misseddata').append(itemlist);
				$('#stock-expected-count').text(expectedarray.length);
				$('#stock-missing-count').text(missedarray.length);
				
				$('#stock-expected-grosswt').text(parseFloat(expectedgrosswt).toFixed(weight_round));
				$('#stock-expected-netwt').text(parseFloat(expectednetwt).toFixed(weight_round));
				$('#stock-missing-grosswt').text(parseFloat(expectedgrosswt).toFixed(weight_round));
				$('#stock-missing-netwt').text(parseFloat(expectednetwt).toFixed(weight_round));
				/* $('#stock-expected-grosswt').text(expectedarraygrosswt.reduce(getSum));
				$('#stock-expected-netwt').text(expectedarraynetwt.reduce(getSum));
				$('#stock-missing-grosswt').text(missedarraygrosswt.reduce(getSum));
				$('#stock-missing-netwt').text(missedarraynetwt.reduce(getSum)); */
			} else {
				itemlist += '<div class="product-list textcenter">No Items found</div>';
				alertpopup('Data not available for this combination');
			}
		},
	});
}
// get summary
function getSum(total, num) {
    return (parseFloat(total) + parseFloat(num)).toFixed(weight_round);
}
function scanneddetails(id) {
	$('#itemtagnumber').val('');
	var modeid = $('#brmode').val();
	var itemlist = '';
	var scannednoslist = '';
	var sessionid = $('#stocktakingcode').find('option:selected').val();
	var scannedgrosswt = 0;
	var scannednetwt = 0;
	// scanned nos section
	scannednoslist += '<div class="product-list" id="product-list'+id+'" sessionid="'+sessionid+'">';
	scannednoslist += id;
	scannednoslist += '</div>';
	$('#scannednos').prepend(scannednoslist);
	scannednosarray[scannednosarray.length] = id;
	//alert(jQuery.inArray( id, expectedarray ));
	if(jQuery.inArray( id, expectedarray ) != -1) {
		if(jQuery.inArray( id, scannedarray ) == -1) {
			var res = $("#expecteddata #product-list"+id+"").html();
			itemlist += '<div class="product-list" id="product-list'+id+'" sessionid="'+sessionid+'">';
			itemlist += res;
			itemlist += '</div>';
			$('#scanneddata').append(itemlist);
			$("#expecteddata #product-list"+id).remove();
			$("#misseddata #product-list"+id).remove();
			var index = missedarray.indexOf(id);
			if (index > -1) {
				scannedgrosswt = missedarraygrosswt[index];
				scannednetwt = missedarraynetwt[index];
				missedarray.splice(index, 1);
				missedarraygrosswt.splice(index, 1);
				missedarraynetwt.splice(index, 1);
			}
			$('#stock-missing-count').text(missedarray.length);
			if(missedarraygrosswt.length > 0) {
				/* $('#stock-missing-grosswt').text(missedarraygrosswt.reduce(getSum));
				$('#stock-missing-netwt').text(missedarraynetwt.reduce(getSum)); */
				missedgrosswt = parseFloat(parseFloat(missedgrosswt) - parseFloat(scannedgrosswt)).toFixed(weight_round);
				missednetwt = parseFloat(parseFloat(missednetwt) - parseFloat(scannednetwt)).toFixed(weight_round);
				$('#stock-missing-grosswt').text(parseFloat(missedgrosswt).toFixed(weight_round));
				$('#stock-missing-netwt').text(parseFloat(missednetwt).toFixed(weight_round));
			} else {
				$('#stock-missing-grosswt').text(0);
				$('#stock-missing-netwt').text(0);
			}
			if($('#prevstocktakingsessionid').val() == '') {
				
			} else {
				newscannedarray[newscannedarray.length] = id;
			}
			scannedarray[scannedarray.length] = id;
			scannedarraygrosswt[scannedarray.length] = scannedgrosswt;
			scannedarraynetwt[scannedarray.length] = scannednetwt;
			$('#stock-scanned-count').text(scannedarray.length);
			if(scannedarraygrosswt.length > 0) {
				/* $('#stock-scanned-grosswt').text(scannedarraygrosswt.reduce(getSum));
				$('#stock-scanned-netwt').text(scannedarraynetwt.reduce(getSum)); */
				finalscannedgrosswt = parseFloat(parseFloat(finalscannedgrosswt) + parseFloat(scannedgrosswt)).toFixed(weight_round);
				finalscannednetwt = parseFloat(parseFloat(finalscannednetwt) + parseFloat(scannednetwt)).toFixed(weight_round);
				$('#stock-scanned-grosswt').text(parseFloat(finalscannedgrosswt).toFixed(weight_round));
				$('#stock-scanned-netwt').text(parseFloat(finalscannednetwt).toFixed(weight_round));
			} else {
				$('#stock-scanned-grosswt').text(0);
				$('#stock-scanned-netwt').text(0);
			}
		} else {
			if(modeid == 1) {
				alertpopup('This item already scanned');
			}
		}
	} else {
		if(jQuery.inArray( id, scannedarray ) == -1) {
			var conflicttrackid = $('#stocktakingcode').find('option:selected').data('conflicttrackid'); // conflict yes
			var stocksessionmethodid = $('#stocktakingcode').find('option:selected').data('stocksessionmethodid');  // barcode mode
			if(conflicttrackid == 2 && stocksessionmethodid == 3) {
				$('#conflictdatahidden').val(id);
				//$('#conflictalert').fadeIn();
				$('#conflictcloseyes').trigger('click');
			} else if(stocksessionmethodid == 2) {
				conflictarray[conflictarray.length] = id;
				$('#stock-conflict-count').text(conflictarray.length);
				$('div #scannednos #product-list'+id).addClass('conflictdbdata');
			}
		} else {
			if(modeid == 1) {
				alertpopup('This item already scanned');
			}
		} 
	}
	$('#itemtagnumber').focus();
}
{// Auto number generation 
	function serialnumbergenerate() {
		$.ajax({
			url: base_url + "Stocktaking/serialnumbergenerate",
			async:false,
			type: "POST",
			cache:false,
			success: function(data) {
				$('#stocktakingsessionid').val(data);
				Materialize.updateTextFields();
			},
		});
	}
}
function conflictdata(id,modeid) {
	var itemlist = '';
	if(jQuery.inArray( id, conflictarray ) == -1) {
		$.ajax({
				url:base_url+"Stocktaking/conflictdata",
				type:"post",
				data:{barcodeid:id,modeid:modeid,rfidbarcode:rfidbarcode},
				dataType:'json',
				async:false,
				cache:false,
				success :function(msg) {
					if(msg != '') {
						var tagsymbol = 'B';
						var sessionid = $('#stocktakingcode').find('option:selected').val();
							itemlist += '<div class="product-list" id="product-list'+msg['barcodeid']+'" productid="'+msg['productid']+'" categoryid="'+msg['categoryid']+'" counterid="'+msg['counterid']+'" sessionid="'+sessionid+'">';
							itemlist += '<span class="pdt-name">'+msg['productname']+'</span>';
							itemlist += '<span class="pdt-name ">'+tagsymbol+' :'+msg['barcodeid']+'</span>';
							itemlist += '<span class="pdt-value pdt-left">G : '+msg['grosswt']+'</span>';
							itemlist += '<span class="pdt-value">N : '+msg['netwt']+'</span>';
							itemlist += '</div>';
							conflictarray[conflictarray.length] = msg['barcodeid'];
							conflictarraygrosswt[conflictarray.length] = msg['grosswt'];
							conflictarraynetwt[conflictarray.length] = msg['netwt'];
							// set css for conflict scanned no 
							if(msg['status'] == 1) {
								$('div #scannednos #product-list'+id).addClass('conflictdbdata');
							}else if(msg['status'] == 0) {
								$('div #scannednos #product-list'+id).addClass('conflictdummaydata');
							}
					} else {
						
					}
					$('#conflictdata').append(itemlist);
					$('#stock-conflict-count').text(conflictarray.length);
				},
			});
	} else {
		if(modeid == 1) {
			alertpopup('This item already conflicted');
		} 
	}		
}
function addform(){
	var form = $("#stocktakingheaderform").serialize();
	var amp = '&'; 
	var datainformation = amp + form;
	var expectedid = expectedarray;
	var scannedid = scannedarray;
	var newscannedid = newscannedarray;
	var conflictid = conflictarray;
	var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
	var conflicttrackid = $('#stocktakingcode').find('option:selected').data('conflicttrackid');
	var stocktakingsessionid = $('#stocktakingsessioncreationid').val();
	var prevstocktakingsessionid = $('#prevstocktakingsessionid').val();
	var stocktakingcode = $('#stocktakingcode').val();
	var employeeid = $('#employeeid').val();
	var startdate = $('#startdate').val();
	var enddate = $('#enddate').val();
	$('#processoverlay').show();
	$.ajax({
        url: base_url +"Stocktaking/stocktakingcreate",
        data: "datas=" + datainformation+amp+"expectedid="+expectedid+amp+"scannedid="+scannedid+amp+"conflictid="+conflictid+amp+"stocktakingid="+stocktakingid+amp+"stocktakingsessionid="+stocktakingsessionid+amp+"prevstocktakingsessionid="+prevstocktakingsessionid+amp+"newscannedid="+newscannedid+amp+"stocktakingcode="+stocktakingcode+amp+"employeeid="+employeeid+amp+"startdate="+startdate+amp+"enddate="+enddate+amp+"conflicttrackid="+conflicttrackid,
		type: "POST",
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'SUCCESS'){
				$('#stocktakingcode').select2('val',stocktakingcode).trigger('change');
				$('#productid,#prodparentcategoryname,#prodparentstoragename,#storagemodeid,#counterid').attr('disabled', false);
				$('#itemtagnumber').attr('disabled',true);
				$("#prodstoragelistbutton,#prodcategorylistbutton").attr('disabled', false);
				$("#employeeid,#stocktakingcode,#startdate,#enddate").attr('disabled', false);
				$('#prevstocktakingsessionid,#storagedatahidden,#conflictdatahidden,#stocktakingsessioncreationid').val('');
				$('#clearprocess-div').addClass('hidedisplay');
				stocktakingrefreshgrid();
				alertpopup(savealert);
				$('#processoverlay').hide();
			}
       },
    });
}
{// Stocktaking View Grid
	function stocktakingview(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		if(Operation == 1){
			var rowcount = $('#stocktakingpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#stocktakingpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		}
		var wwidth = $(window).width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.stocktakingheadercolsort').hasClass('datasort') ? $('.stocktakingheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.stocktakingheadercolsort').hasClass('datasort') ? $('.stocktakingheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.stocktakingheadercolsort').hasClass('datasort') ? $('.stocktakingheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = 103;
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stocktaking&primaryid=stocktakingid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#stocktakingviewgrid').empty();
				$('#stocktakingviewgrid').append(data.content);
				$('#stocktakingviewgridfooter').empty();
				$('#stocktakingviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('stocktakingviewgrid');
				{//sorting
					$('.stocktakingheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.stocktakingheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.stocktakingheadercolsort').hasClass('datasort') ? $('.stocktakingheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.stocktakingheadercolsort').hasClass('datasort') ? $('.stocktakingheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						stocktakingview(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('stocktakingheadercolsort',headcolid,sortord);
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#stocktakingview div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						stocktakingview(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stocktakingpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						stocktakingview(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
					$('#stocktakingpgrowcount').material_select();	
			},
		});
	}
}
{//refresh grid
	function stocktakingrefreshgrid() {
		var page = $('ul#stockentrypgnum li.active').data('pagenum');
		var rowcount = $('ul#stockentrypgnumcnt li .page-text .active').data('rowcount');
		stocktakingview(page,rowcount);
	}
}
function addsessionform() {
	
	var form = $("#stocktakingsessionform").serialize();
	var amp = '&'; 
	var datainformation = amp + form;
	var puirtyid = $('#purityid').val();
	var stocktakingsessiontypeid = $('#stocktakingsessiontypeid').val();
	var itemtagdatatypeid = $('#itemtagdatatypeid').val()
	var employeeid = $('#employeeid').find('option:selected').val();
	var deviceno = $('#stocksessiondeviceno').find('option:selected').val();
	var stocksessionmethodid = $('#stocksessionmethodid').find('option:selected').val();
	if(stocksessionmethodid == 2 && deviceno != '') {
		var macaddress = $('#stocksessiondeviceno').find('option:selected').data('macaddress');
		var rfiddeviceid = $('#stocksessiondeviceno').find('option:selected').data('rfiddeviceid');
		var connectioncheck = $('#stocksessiondeviceno').find('option:selected').data('connectioncheck');
		var startstoptrigger = $('#stocksessiondeviceno').find('option:selected').data('startstoptrigger');
	} else {
		var macaddress = '';
		var rfiddeviceid = '1';
		var connectioncheck = 'No';
		var startstoptrigger = 'No';
	}
	$('#processoverlay').show();
	$('#stocktakeformstatus').val('');
	$.ajax({
        url: base_url +"Stocktaking/stocktakingsessioncreate",
        data: "datas=" + datainformation+amp+"purity="+puirtyid+amp+"sessiontypeid="+stocktakingsessiontypeid+amp+"itemtagdatatypeid="+itemtagdatatypeid+amp+"employeeid="+employeeid+"&macaddress="+macaddress+"&rfiddeviceid="+rfiddeviceid+"&connectioncheck="+connectioncheck+"&startstoptrigger="+startstoptrigger,
		type: "POST",
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'SUCCESS') {
				clearform('stocktakingsessionform');
				$(".addsectionclose").trigger("click");
				sessionformrefreshgrid();
				alertpopup(savealert);
				$('#processoverlay').hide();
			}
       },
    });
}
{//session creation form grid
	function sessionformgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $('#sessionformgrid').width();
		var wheight = $('#sessionformgrid').height();
		//col sort
		var sortcol = $("#chitbooksortcolumn").val();
		var sortord = $("#chitbooksortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.sessionformgridheadercolsort').hasClass('datasort') ? $('.sessionformgridheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.sessionformgridheadercolsort').hasClass('datasort') ? $('.sessionformgridheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.sessionformgridheadercolsort').hasClass('datasort') ? $('.sessionformgridheadercolsort.datasort').attr('id') : '0';
		var filterid ='';
		var conditionname = '';
		var filtervalue = '';
		var userviewid =185;
		var viewfieldids =103;
		var footername = 'sessionformgridfooter';
		var cuscondfieldsname='stocktaking.stocktakingid';
		var cuscondvalues='';
		var conditionoperator='IN';
		var cuscondtablenames="";
		var cusjointableid='';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stocktaking&primaryid=stocktakingid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+'&footername='+footername+"&cuscondfieldsname="+cuscondfieldsname+"&cuscondvalues="+cuscondvalues+"&cuscondtablenames="+cuscondtablenames+"&cusjointableid="+cusjointableid+"&conditionoperator="+conditionoperator,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#sessionformgrid').empty();
				$('#sessionformgrid').append(data.content);
				$('#sessionformgridfooter').empty();
				$('#sessionformgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('sessionformgrid');
				{//sorting
					$('.sessionformgridheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.sessionformgridheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#stocktakingpgnumcnt li .page-text .active').data('rowcount');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.sessionformgridheadercolsort').hasClass('datasort') ? $('.sessionformgridheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.sessionformgridheadercolsort').hasClass('datasort') ? $('.sessionformgridheadercolsort.datasort').data('sortorder') : '';
						$("#chitbooksortorder").val(sortord);
						$("#chitbooksortcolumn").val(sortcol);
						sessionformgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('sessionformgridheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						sessionformgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#sessionformgridfooterfooterpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						sessionformgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#sessionformgridfooterpgrowcount').material_select();
			},
		});
	 }
	 {//refresh grid
		function sessionformrefreshgrid() {
			var page = $('ul#stocktakingpgnum li.active').data('pagenum');
			var rowcount = $('ul#stocktakingpgnumcnt li .page-text .active').data('rowcount');
			sessionformgrid(page,rowcount);
		}
	}
	function loadstockcode() {
		$('#stocktakingcode').empty();	
		$('#stocktakingcode').append($("<option></option>"));
		$.ajax({
			url: base_url +"Stocktaking/loadstockcode",
			dataType:'json',
			success: function(data) {
				if(data != ''){
					$.each(data, function(index) {
					 $('#stocktakingcode')
						.append($("<option></option>")
						.attr("data-stocktakingtypeid",data[index]['stocktakingtypeid'])
						.attr("data-branchid",data[index]['branchid'])
						.attr("data-purityid",data[index]['purityid'])
						.attr("data-stocktakingid",data[index]['stocktakingid'])
						.attr("data-itemtagdatatypeid",data[index]['itemtagdatatypeid'])
						.attr("data-stocksessionmodeid",data[index]['stocksessionmodeid'])
						.attr("data-deviceno",data[index]['deviceno'])
						.attr("data-stocksessionmethodid",data[index]['stocksessionmethodid'])
						.attr("data-conflicttrackid",data[index]['conflicttrackid'])
						.attr("data-rfiddeviceid",data[index]['rfiddeviceid'])
						.attr("data-connectioncheck",data[index]['connectioncheck'])
						.attr("data-startstoptrigger",data[index]['startstoptrigger'])
						.attr("value",data[index]['stocktakingnumber'])
						.text(data[index]['stocktakingid']+'-'+data[index]['stocktakingnumber'])); 
					});
				}
		   },
		});
	}
	// Check Session Table is already exists
	function checksessiontable(stocktakingid) {
		var storagemodeid = $('#storagemodeid').find('option:selected').val();
		var deviceno = $('#deviceno').val();				  
		if(storagemodeid == 2 || storagemodeid == 4) {
			var storageid = $('#storagedatahidden').val();
		} else if(storagemodeid == 3) {
			var storageid = $('#counterid').find('option:selected').val();
		} else if(storagemodeid == 5) {
			var storageid = $('#proparentstorageid').val();
		} else {
			var storageid = $('#proparentstorageid').val();
		}
		var categoryid = $('#proparentcategoryid').val();
		var productid = $('#productid').find('option:selected').val();
		var lotnumberids = $.trim($('#lotnumberid').val());
		lotnumberids = ltrim(lotnumberids,",");
		if(lotnumberids == 1) { lotnumberids = ''; } else { lotnumberids = lotnumberids; }
		$.ajax({
			url:base_url+"Stocktaking/checksessiontable",
			type:"post",
			data:{storageid:storageid,categoryid:categoryid,productid:productid,stocktakingid:stocktakingid,lotnumberids:lotnumberids,deviceno:deviceno},
			dataType:'json',
			async:false,
			cache:false,
			success :function(msg) {
				if(msg['active'] > 0){
					$('#prevstocktakingsessionid').val('');
					$('#alertsclose').trigger('click');
					emptyexpectedmissed();
					alertpopup('Currently stocktaking for selected combination is in process, started by '+msg['empname']+' on '+msg['empcreatedate']+'');
					
				}else if(msg['scanned'] > 0){
					$('#scannedalert').fadeIn();
					emptyexpectedmissed();
					
				}else if(msg['inactive'] > 0 || msg['inactive'] == 0){
					var branchid = $('#stocktakingcode').find('option:selected').data('branchid');
					var purityid = $('#stocktakingcode').find('option:selected').data('purityid');
					var sessionmodeid = $('#stocktakingcode').find('option:selected').data('stocksessionmodeid');
					var modeid = $('#brmode').val();
					var startdate = $('#startdate').val();
					var enddate = $('#enddate').val();
					expecteddetails(storageid,branchid,purityid,modeid,sessionmodeid,startdate,enddate,categoryid,productid,lotnumberids);
					$('#prevstocktakingsessionid').val('');
				}
				$("#processoverlay").hide();
			},
		});
	}
	function getscanneddata(stocktakingid,storageid,categoryid,productid,modeid){
		var itemlist = '';
		var missitemlist = ''; 
		var scanneditemlist = '';
		var mergetext = '';
		emptyexpectedmissed();
		$.ajax({
			url:base_url+"Stocktaking/getscanneddata",
			type:"post",
			data:{stocktakingid:stocktakingid,storageid:storageid,categoryid:categoryid,productid:productid,modeid:modeid,rfidbarcode:rfidbarcode},
			dataType:'json',
			async:false,
			cache:false,
			success :function(msg) {
				var tagsymbol = 'B';
				var sessionid = $('#stocktakingcode').find('option:selected').val();
				if(msg.length >0){
					for(var i=0; i<msg.length;i++) {
						$('#prevstocktakingsessionid,#stocktakingsessioncreationid').val(msg[i]['stocktakingsessionid']);	
						itemlist += divappenddata(msg[i],sessionid);
						expectedarray[i] = msg[i]['barcodeid'];
						expectedarraygrosswt[i] = msg[i]['grosswt'];
						expectedarraynetwt[i] = msg[i]['netwt'];
						
						expectedgrosswt = parseFloat(expectedgrosswt) + parseFloat(msg[i]['grosswt']);
						expectednetwt = parseFloat(expectednetwt) + parseFloat(msg[i]['netwt']);
						
						if(msg[i]['status'] == 2){
							scanneditemlist += divappenddata(msg[i],sessionid);
							scannedarray[i] = msg[i]['barcodeid'];
							scannedarraygrosswt[i] = msg[i]['grosswt'];
							scannedarraynetwt[i] = msg[i]['netwt'];
							$('#prevstocktakingsessionid').val(msg[i]['stocktakingsessionid']);
							finalscannedgrosswt = parseFloat(finalscannedgrosswt) + parseFloat(msg[i]['grosswt']);
							finalscannednetwt = parseFloat(finalscannednetwt) + parseFloat(msg[i]['netwt']);
						
						}
						else
						{
							missitemlist += divappenddata(msg[i],sessionid);
							missedarray[i] = msg[i]['barcodeid'];
							missedarraygrosswt[i] = msg[i]['grosswt'];
							missedarraynetwt[i] = msg[i]['netwt'];
							missedgrosswt = parseFloat(missedgrosswt) + parseFloat(msg[i]['grosswt']);
							missednetwt = parseFloat(missednetwt) + parseFloat(msg[i]['netwt']);
						
						}							
					}
					missedarray = missedarray.filter(function(v){return v!==''});
					scannedarray = scannedarray.filter(function(v){return v!==''});
					$('#expecteddata').append(itemlist);
					$('#stock-expected-count').text(expectedarray.length);
					$('#misseddata').append(missitemlist);
					$('#stock-missing-count').text(missedarray.length);
					$('#scanneddata').append(scanneditemlist);
					$('#stock-scanned-count').text(scannedarray.length);
					$('#stock-expected-grosswt').text(parseFloat(expectedgrosswt).toFixed(weight_round));
					$('#stock-expected-netwt').text(parseFloat(expectednetwt).toFixed(weight_round));
					$('#stock-missing-grosswt').text(parseFloat(missedgrosswt).toFixed(weight_round));
					$('#stock-missing-netwt').text(parseFloat(missednetwt).toFixed(weight_round));
					$('#stock-scanned-grosswt').text(parseFloat(finalscannedgrosswt).toFixed(weight_round));
					$('#stock-scanned-netwt').text(parseFloat(finalscannednetwt).toFixed(weight_round));
					
				}else{
					itemlist += '<div class="product-list textcenter">No Items found</div>';
				}
			}
		});
	}
	function createstocksession() {
		var form = $("#stocktakingheaderform").serialize();
		var amp = '&'; 
		var datainformation = amp + form;
		var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
		var stocktakingtypeid = $('#stocktakingcode').find('option:selected').data('stocktakingtypeid');
		var counterid = '' ;
		var productid = '' ;
		var categoryid = '' ;
		var lotid = '' ;
		var startdate = '' ;
		var enddate = '' ;
		var storagemodeid = $('#storagemodeid').find('option:selected').val();
		if(storagemodeid == 2 || storagemodeid == 4) {
			counterid = $('#storagedatahidden').val();
		} else if(storagemodeid == 3) {
			counterid = $('#counterid').find('option:selected').val();
		} else if(storagemodeid == 5) {
			counterid = $('#proparentstorageid').val();
		} else {
			counterid = $('#proparentstorageid').val();
		}
		categoryid = $('#proparentcategoryid').val();
		lotid = $.trim($('#lotnumberid').val());
		lotid = ltrim(lotid,",");
		productid = $.trim($('#productid').find('option:selected').val());
		var employeeid = $('#employeeid').find('option:selected').val();
		var modeid = $('#brmode').val();
		var connectioncheck = $('#stocktakingcode').find('option:selected').data('connectioncheck');
		var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
		var deviceno = $('#stocktakingcode').find('option:selected').data('deviceno');
		$.ajax({
			url: base_url +"Stocktaking/createstocksession",
			data: "datas=" + datainformation+amp+"stocktakingid="+stocktakingid+amp+"counterid="+counterid+amp+"categoryid="+categoryid+amp+"productid="+productid+amp+"lotid="+lotid+amp+"employeeid="+employeeid+amp+"startdate="+startdate+amp+"enddate="+enddate+"&modeid="+modeid+"&connectioncheck="+connectioncheck+"&startstoptrigger="+startstoptrigger+"&deviceno="+deviceno,
			type: "POST",
			dataType:'text',
			success: function(msg) {
				$('#stocktakingsessioncreationid').val($.trim(msg));
			},
		});
	}
}
function getbarcodedata(data,barcodedata) {
		$.ajax({
			url:base_url+"Stocktaking/getbarcodedata",
			type:"post",
			data:{data:data,barcodedata:barcodedata},
			dataType:'json',
			async:false,
			cache:false,
			success :function(msg) {
				$('#barcodearray').val(msg['barcodedataarray']);
				var res = msg['barcodedata'].split(",");	
				for(var i=0; i<res.length; i++) {
					scanneddetails(res[i]);
				}
			}
		 });
}
//stock taking detail grid load
function stocktakingdetailsgrid() {
	var page = typeof page == 'undefined' ? 1 : page;
	var rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
	var wwidth = $("#stocktakingdetailsgrid").width();
	var wheight = $("#stocktakingdetailsgrid").height();
	var primarytable = 'stocktaking';
	var primaryid = '1';
	$.ajax({
		url:base_url+"Stocktaking/Stocktakingdetailsgridheaderinformationfetch?moduleid=103&width="+wwidth+"&height="+wheight+"&page="+page+"&records="+rowcount+"&modulename=stocktakingdetailgrid"+"&primarytable="+primarytable+"&primaryid="+primaryid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#stocktakingdetailsgrid").empty();
			$("#stocktakingdetailsgrid").append(data.content);
			$("#stocktakingdetailsgridfooter").empty();
			$("#stocktakingdetailsgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('stocktakingdetailsgrid');
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					stocktakingdetailsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#chitbookfooterpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					stocktakingdetailsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
		},
	});	
}
//stock session detail grid load
function stocksessiondetailsgrid() {
	var wwidth = $("#stocksessiondetailsgrid").width();
	var wheight = $("#stocksessiondetailsgrid").height();
	$.ajax({
		url:base_url+"Stocktaking/Stocksessiondetailsgridheaderinformationfetch?moduleid=103&width="+wwidth+"&height="+wheight+"&modulename=stocksessiondetailsgrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#stocksessiondetailsgrid").empty();
			$("#stocksessiondetailsgrid").append(data.content);
			$("#stocksessiondetailsgridfooter").empty();
			$("#stocksessiondetailsgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('stocksessiondetailsgrid');
			//header check box
			$(".stocksessiondetailsgrid_headchkboxclass").click(function() {
				$(".stocksessiondetailsgrid_rowchkboxclass").prop('checked', false);
				if($(".stocksessiondetailsgrid_headchkboxclass").prop('checked') == true) {
					$(".stocksessiondetailsgrid_rowchkboxclass").prop('checked', true);
				} else {
					$(".stocksessiondetailsgrid_rowchkboxclass").prop('checked', false);
				}
				issuereceiptgridgetcheckboxrowid('stocksessiondetailsgrid');
			});
		},
	});	
}
 function issuereceiptgridgetcheckboxrowid(gridid) {
	var selected = [];
	var approvaloutreturntagno = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#stocksessiondetailhidden").val(selected);
}
function stocktakingdetailsdata(ids,mode,stocksessionmodeid) {
	$.ajax({
		url:base_url+"Stocktaking/stocktakingdetailsdatafetch",
		dataType:'json',
		data:{ids:ids,mode:mode,stocksessionmodeid:stocksessionmodeid,rfidbarcode:rfidbarcode},
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			if(data != '') {
				loadinlinegriddata('stocktakingdetailsgrid',data.rows,'json');
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('stocktakingdetailsgrid');
			}
		},
	});
}
function sumofstocktakingdetailsdata(ids,mode,stocksessionmodeid) {
	$.ajax({
		url:base_url+"Stocktaking/sumofstocktakingdetailsdatafetch",
		dataType:'json',
		data:{ids:ids,mode:mode,stocksessionmodeid:stocksessionmodeid,rfidbarcode:rfidbarcode},
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			if(data != ''){
				$('#stockentrydetailoverlay .sumgrossweight').text('Total G.WT: '+data.sumgrossweight+'');
				$('#stockentrydetailoverlay .sumstoneweight').text('Total S.WT: '+data.sumstoneweight+'');
				$('#stockentrydetailoverlay .sumnetweight').text('Total N.WT: '+data.sumnetweight+'');
				$('#stockentrydetailoverlay .sumpieces').text('Total Pieces: '+data.sumpieces+'');
			}
		},
	});
}
function stocksessiondetailsdata(stocktakingid){
	$.ajax({
		url:base_url+"Stocktaking/stocksessiondetailsdata",
		dataType:'json',
		data:{stocktakingid:stocktakingid},
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			if(data != ''){
				loadinlinegriddata('stocksessiondetailsgrid',data.rows,'json','checkbox');
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('stocksessiondetailsgrid');
				//row based check box
				$(".stocksessiondetailsgrid_rowchkboxclass").click(function(){
					$('.stocksessiondetailsgrid_headchkboxclass').removeAttr('checked');
					issuereceiptgridgetcheckboxrowid('stocksessiondetailsgrid');
				});
			}else{
			}
		},
	});	
}
function getrfidvishal() {
	var a = 0;
	 rfct = 0;
	 $("#rfidcountdb").val(0);
	var deviceno = $("#deviceno").val();
	var scannednoslist = '';
	var sessionid = $('#stocktakingcode').find('option:selected').val();
	$.ajax({
		url:base_url+"Stocktaking/getrfidvishal",
		data:{maxrfid:rfidstockmaxentry,deviceno:deviceno},
		method: "POST",
		dataType:'json',
		async:false,
		success :function(data) {
			
			rfidstockmaxentry = data['orginal']['maxrf'];
			rfct = data['orginal']['rfcount'];
			var res = data['orginal']['tagno'].split(",");	
			for(var i=0; i<res.length; i++) {
				if(res[i] != '') {
					scanneddetails($.trim(res[i]));
				} 
			}
			// conflict data
			$.each(data['diffarray'], function (index, value) {
				conflictarray[conflictarray.length] = data['diffarray'][index]['tagno'];
				// scannednos array push
				// scanned nos section
				scannednoslist += '<div class="product-list" id="product-list'+data['diffarray'][index]['tagno']+'" sessionid="'+sessionid+'">';
				scannednoslist += data['diffarray'][index]['tagno'];
				scannednoslist += '</div>';
				scannednosarray[scannednosarray.length] = data['diffarray'][index]['tagno'];
			});
			$('#scannednos').prepend(scannednoslist);
			$.each(data['diffarray'], function (index, value) {
				// set css for conflict scanned no
				$('div #scannednos #product-list'+data['diffarray'][index]['tagno']).addClass('conflictdummaydata');
			});	
			$('#stock-conflict-count').text(conflictarray.length);
		},
	});
	rfidstockmaxentry = rfidstockmaxentry;
	$("#rfidcountdb").val(rfct);
}
function clearexistdetails(deviceno) {
	$.ajax({
		url:base_url+"Stocktaking/clearexistdetails",
		data:{deviceno:deviceno},
		method: "POST",
		dataType:'json',
		async:false,
		success :function(data) {
						
		},
	});
}
function emptyexpectedmissed() {
	$('#expecteddata').empty();
	expectedarray = [];
	$('#misseddata').empty();
	missedarray = [];
	$('#stock-conflict-count,#stock-scanned-count,#stock-missing-count,#stock-expected-count,#stock-expected-grosswt,#stock-expected-netwt,#stock-scanned-grosswt,#stock-scanned-netwt,#stock-missing-grosswt,#stock-missing-netwt').text(0);
}
function emptysummaryarray() {
	$('#expecteddata,#misseddata,#scannednos').empty();
	expectedarray = [];
	missedarray = [];
	scannednosarray = [];
	$('#stock-conflict-count,#stock-scanned-count,#stock-missing-count,#stock-expected-count,#stock-expected-grosswt,#stock-expected-netwt,#stock-scanned-grosswt,#stock-scanned-netwt,#stock-missing-grosswt,#stock-missing-netwt').text(0);
}
{//date Range
	function daterangefunction(generalstartdate,generatenddate){
		var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
		var listid =1;
		var dateformetdata = $('#'+generalstartdate+'').attr('data-dateformater');
		var startDateTextBox = $('#'+generalstartdate+'');
		var endDateTextBox = $('#'+generatenddate+'');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
					var listid =1;
					$("#processoverlay").show();
					if(stocktakingid != '') {
						checksessiontable(stocktakingid,listid);
					}
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#'+generalstartdate+'').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.hasDatepicker').removeClass('error');
				}
			}
		}).click(function() {
			if(datechange == 0) {
				$(this).datetimepicker('show');
			}
		});
		endDateTextBox.datetimepicker({
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					var stocktakingid = $('#stocktakingcode').find('option:selected').data('stocktakingid');
					var listid =1;
					$("#processoverlay").show();
					if(stocktakingid != '') {
						checksessiontable(stocktakingid,listid);	
					}
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				} else {
					startDateTextBox.val(dateText);
					
				}
				$('#'+generatenddate+'').focus();
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			if(datechange == 0) {
				$(this).datetimepicker('show');
			}
		});
	}
}
function loadproductbasedoncategory(categoryid,ddname) { // load product based on category tree selection
	if(categoryid == '')  {
		$("#"+ddname+"").select2('val','');
		$("#"+ddname+" option").removeClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',false);
	}else {
		$("#"+ddname+"").select2('val','');
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		var categorydefaultid = 1;
		$("#"+ddname+" option[data-categoryid="+categorydefaultid+"]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[data-categoryid="+categorydefaultid+"]").prop('disabled',false);
		$("#"+ddname+" option[data-categoryid="+categoryid+"]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[data-categoryid="+categoryid+"]").prop('disabled',false);
	}
}
// field hide/show based on session id	
function fieldhideshow(typeid) {
	$('.storagetreediv,.categorytreediv,.productiddiv,.storagemodeiddiv,.lotnumberdiv').addClass('hidedisplay');
	$('#storagemodeid').select2('val',1).trigger('change');
	var typearray=typeid.toString().split(",");
	var storagestatus = 1;
	$.each(typearray, function (index, value) {
		if(value != '6') { //Lotnumber DD
			if(value != '5') { // All
				if(value == '2'){ // storage
					$('.storagetreediv').removeClass('hidedisplay');
				}else if(value == '3'){ // Category
					$('.categorytreediv').removeClass('hidedisplay');
				}else if(value == '4'){ // Product
					$('.productiddiv').removeClass('hidedisplay');
				}
			}
			$('#lotnumberid').select2('val',0).trigger('change');
		} else {
			$('.lotnumberdiv').removeClass('hidedisplay');
		}
		if(value != '2') {
			storagestatus = 0;
		}
	});
	if(storagestatus == 1) {
		$('.storagemodeiddiv').removeClass('hidedisplay');
		$('#storagemodeid').select2('val',5).trigger('change');
	}
}
// check product storage
function checkproductstorage(storagename) {
	$('#storagename').val('');
	$('#processoverlay').show();
	$.ajax({
		url:base_url+"Stocktaking/checkproductstorage",
		data:{storagename:storagename},
		method: "POST",
		dataType:'json',
		async:false,
		success :function(data) {
			$('#processoverlay').hide();
			if(data.statusid[0]['id'] == 0){
				alertpopupdouble('Please give Product Storage Name');
			}else if(data.statusid[0]['id'] == 1){
				var counterid = getgridcolcolumnvalue('storagedatagrid','','counterid');
				if (jQuery.inArray(data.rows[0]['id'], counterid) == '-1') {
					loadinlinegriddata('storagedatagrid',data.rows,'json');	
					datarowselectevt();
					/* column resize */
					columnresize('storagedatagrid');
				}else {
					alertpopupdouble('Already you added this storage.Please add new one');
				}
				var gridlength = $('#storagedatagrid .gridcontent div.data-content div.data-rows').length;
				$('#storagecount').val(gridlength);
				Materialize.updateTextFields();	
			}
		},
	});
}
//storage data grid
function storagedatagrid() {
	var wwidth = $("#storagedatagrid").width();
	var wheight = $("#storagedatagrid").height();
	$.ajax({
		url:base_url+"Stocktaking/storagedataheaderinformationfetch?moduleid=103&width="+wwidth+"&height="+wheight+"&modulename=stocktakingstoragedatagrid",
		dataType:'json',
		async:true,
		cache:false,
		success:function(data) {
			$("#storagedatagrid").empty();
			$("#storagedatagrid").append(data.content);
			$("#storagedatagridfooter").empty();
			$("#storagedatagridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('storagedatagrid');
		},
	});	
}
function getrfidstorage() {
	var deviceno = $("#deviceno").val();
	$.ajax({
		url:base_url+"Stocktaking/getrfidstorage",
		data:{maxrfid:rfidstoragemaxentry,deviceno:deviceno},
		method: "POST",
		dataType:'json',
		async:false,
		success :function(data) {
			rfidstoragemaxentry = data.maxrfid[0]['id'];
			loadinlinegriddata('storagedatagrid',data.rows,'json');	
			datarowselectevt();
			/* column resize */
			columnresize('storagedatagrid');
			var gridlength = $('#storagedatagrid .gridcontent div.data-content div.data-rows').length;
			$('#storagecount').val(gridlength);
			Materialize.updateTextFields();		
		},
	});
	rfidstoragemaxentry = rfidstoragemaxentry;
}
// get summary gross wt
function getsumgrosswt(summaryarray) {
	sum = summaryarray.reduce(function (s, a) {
		return parseFloat(s) + parseFloat(a.gwt);
	}, 0);
	return sum.toFixed(weight_round);
}
// get summary net wt
function getsumnetwt(summaryarray) {
	sum = summaryarray.reduce(function (s, a) {
		return parseFloat(s) + parseFloat(a.nwt);
	}, 0);
	return sum.toFixed(weight_round);
}
// clear data
function cleardata() {
	  $('#scanneddata,#misseddata,#conflictdata,#expecteddata,#scannednos').empty();
	  $('#stock-conflict-count,#stock-scanned-count,#stock-missing-count,#stock-expected-count,#stock-expected-grosswt,#stock-expected-netwt,#stock-scanned-grosswt,#stock-scanned-netwt,#stock-missing-grosswt,#stock-missing-netwt').text(0);
	  $('#prevstocktakingsessionid,#storagedatahidden').val('');
	  scannedarray = [];
	  scannedarraygrosswt = [];
	  scannedarraynetwt = [];
	  missedarray = [];
	  missedarraygrosswt = [];
	  missedarraynetwt = [];
	  conflictarray = [];
	  conflictarraygrosswt = [];
	  conflictarraynetwt = [];
	  expectedarray = [];		
	  expectedarraygrosswt = [];		
	  expectedarraynetwt = [];
	  scannednosarray = [];	
	  expectedgrosswt = 0;
	  expectednetwt = 0;
	  finalscannedgrosswt = 0;
	  finalscannednetwt = 0;
	  missedgrosswt = 0;
	  missednetwt = 0;
	  conflictgrosswt = 0;
	  conflictnetwt = 0;
	  $('#stocktakeformstatus').val('');
}
// load session data
function loadsessiondata(datarowid) {
		$('#sessiondataid').empty();	
		$('#sessiondataid').append($("<option></option>"));
		var mergedata = '';
		$.ajax(
		{
			url: base_url +"Stocktaking/loadsessiondata",
			data:{stocktakingid:datarowid},
			dataType:'json',
			type:"post",
			success: function(data) 
			{
				if(data != '') {	
				 $.each(data, function(index) {
							if(data[index]['countername'] != '') {
								mergedata += ' - S: '+data[index]['countername'];
							}
							if(data[index]['categoryname'] != '') {
								mergedata += ' - C: '+data[index]['categoryname'];
							}
							if(data[index]['productname'] != '') {
								mergedata += ' - P: '+data[index]['productname'];
							}
								 $('#sessiondataid')
								.append($("<option></option>")
								.attr("value",data[index]['stocktakingsessionid'])
								.text(data[index]['stocktakingnumber']+mergedata)); 
							
						});
				}					
		   },
		});
}
// category based product hide/show
function categorybasedproducthideshow(listid) {
	var ddname = "productid";
	$('#productid').select2('val','');
	productremovehideclass(ddname,0);
	$("#"+ddname+" option[data-categoryid="+listid+"]").removeClass("ddhidedisplay");	
	$("#"+ddname+" option[data-categoryid="+listid+"]").prop('disabled',false);
}
function productremovehideclass(ddname,statusid) {
	if(statusid == 0) {
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
	}else if(statusid == 1) {
		$("#"+ddname+" option").removeClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',false);
	}
}
// check form value
function checkformvalue() {
	var formstatus = 0;
	var typeid = $('#stocktakingcode').find('option:selected').data('stocktakingtypeid');
	if($.trim($('#stocktakingcode').find('option:selected').val()) != '') {
		formstatus = 1;
	}
	if(typeid == 5) {
		
	} else if(typeid == 6) {
		if($(".lotnumberdiv").is(':visible') == true) {
			var lotnumberids = $.trim($("#lotnumberid").val());
			lotnumberids = ltrim(lotnumberids,",");
			if(lotnumberids == 1) { lotnumberids = ''; } else { lotnumberids = lotnumberids; }
			if(lotnumberids == '') {
				formstatus = 0;
			}
		}
	} else {
		if($(".storagemodeiddiv").is(':visible') == true) {
			var storagemodeid = $('#storagemodeid').find('option:selected').val();
			if(storagemodeid == '3') { // single storage
				if($.trim($('#counterid').find('option:selected').val()) == '') {
					formstatus = 0;
				}
			} else if(storagemodeid == '2' || storagemodeid == '4') {
				if($('#storagedatahidden').val() == '') {
					formstatus = 0;
				}
			} else if(storagemodeid == '5') {
				if($('#proparentstorageid').val() == '') {
					formstatus = 0;
				}
			}
		} else {
			if($(".storagetreediv").is(':visible') == true) {
				if($('#proparentstorageid').val() == '') {
					formstatus = 0;
				}
			}
			if($(".categorytreediv").is(':visible') == true) {
				if($('#proparentcategoryid').val() == '') {
					formstatus = 0;
				}
			}
			if($(".productiddiv").is(':visible') == true) {
				if($.trim($('#productid').find('option:selected').val()) == '') {
					formstatus = 0;
				}
			}
			
		}
	}
	return formstatus;
}
// Append div data
function divappenddata(msg,sessionid) {
	var itemlist = '';
	var picturedata = '';
	var mergetext = '';
	if(rfidbarcode == 1) {
		if(msg['itemtagnumber'] != '' ) { 
			mergetext = ' - '+msg['itemtagnumber'];
		} else {
			mergetext = '';
		}
	} else {
		if(msg['rfidtagno'] != '' ) { 
			mergetext = ' - '+msg['rfidtagno'];
		} else {
			mergetext = '';
		}	
	}
	itemlist += '<div class="product-list" id="product-list'+msg['barcodeid']+'" productid="'+msg['productid']+'" categoryid="'+msg['categoryid']+'" counterid="'+msg['counterid']+'" grosswt="'+msg['grosswt']+'" netwt="'+msg['netwt']+'" sessionid="'+sessionid+'">';
	{ //Picture information to view
		picturedata += '<span class="textleft material-icons tagimageoverlaypreview" id="tagimageoverlaypreview" style="width:20px; vertical-align:middle;" >radio_button_checked<a href="" style="display: none" target="_blank"></a></span>';
	}
	itemlist += '<span class="pdt-name"> C: '+msg['categoryname']+' - P: '+msg['productname']+' - '+picturedata+' - S: '+msg['countername']+'</span>';
	itemlist += '<span class="pdt-name"> P: '+msg['purityname']+' - '+msg['grosswt']+' - '+msg['netwt']+' - '+msg['barcodeid']+mergetext+'</span>';
	itemlist += '</div>';
	return itemlist;
}
// stop button process clear
function stopprocessbase() {
	var modeid = $('#brmode').val();
	if(modeid == 2){
		clearInterval(refreshIntervalId);
	}
	$('.loadprocessdiv').removeClass('hidedisplay');
	$('#productid,#prodparentcategoryname,#prodparentstoragename,#storagemodeid,#counterid').attr('disabled', false);
	$("#prodstoragelistbutton,#prodcategorylistbutton").attr('disabled', false);
	$("#employeeid,#stocktakingcode,#startdate,#enddate").attr('disabled', false);
	$('#stocktakingsessioncreationid').val('');
	cleardata();
}
// reset session id
function resetsession(currentsessionid) {
	var viewscanstatus = $('#prevstocktakingsessionid').val();
	var deviceno = $(this).find('option:selected').data('deviceno');
	var modeid = $('#brmode').val();
	var connectioncheck = $('#stocktakingcode').find('option:selected').data('connectioncheck');
	var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
	if(startstoptrigger == 'Yes' && modeid == 2) {
		$.ajax({
			url:base_url+"Stocktaking/updaterfidsummary",
			data:{deviceno:deviceno,message:'clear'},
			method: "POST",
			dataType:'json',
			async:false,
			success :function(data) {
				
			},
		});
	setTimeout(function(){
		$.ajax(
		{
			url: base_url +"Stocktaking/resetsession",
			data:{currentsessionid:currentsessionid,viewscanstatus:viewscanstatus,deviceno:deviceno},
			type:"post",
			success: function(data) 
			{
				if(data == 'SUCCESS') {
					if(viewscanstatus == '') {
						alertpopup('Session cleared successfully');
					}else {
						alertpopup('Session reset successfully');
					}
				}					
		   },
		});					
	},5000);
	
}
}
//Load Tagtemplate details in DD.
function tagtemplatesload(viewfieldids) {
	$('#tagtemplateprview,#printtemplatepreview').empty();
	$('#tagtemplateprview,#printtemplatepreview').append($("<option value=''></option>"));
	$.ajax({
		url:base_url+'Base/tagtemplatesloadddval?dataname=tagtemplatename&dataid=tagtemplateid&datatab=tagtemplate&viewfieldids='+viewfieldids+"&column=printername",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#tagtemplateprview,#printtemplatepreview')
					.append($("<option></option>")
					.attr("data-foldernameidhidden",data[index]['dataname'])
					.attr("data-printername",data[index]['printername'])
					.attr("data-moduleid",viewfieldids)
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#tagtemplateprview,#printtemplatepreview').trigger('change');
		},
	});
}
//retrieve Stocksession value.
function retrievestocktakingnumber(datarowid) {
	$.ajax({
		url:base_url+'Stocktaking/retrievestocktakingnumber?datarowid='+datarowid,
		type:"post",
		success: function(data) {
			datarowid(data);
		},
	});
}
function printautomate(salesid,printtemplateid,snumber){
	$('#printpdfid').val(salesid);
	$('#printsnumber').val(snumber);		
	$('#pdftemplateprview').select2('val',printtemplateid).trigger('change');
	//$('#pdffilepreviewintab').trigger("click");
}
function ltrim(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
}
function rfiddevicetable() {
	var deviceno = $('#stocktakingcode').find('option:selected').data('deviceno');
	var startstoptrigger = $('#stocktakingcode').find('option:selected').data('startstoptrigger');
	$.ajax({
		url:base_url+"Stocktaking/getrfiddevicetable",
		data:{deviceno:deviceno},
		method: "POST",
		dataType:'json',
		//async:false,
		success :function(data) {
			$('#appmessage').val(data['message']);
			$('#connectstatus').val(data['connectstatus']);
			if(data['connectstatus'] == 'Connected') {
				$("#rfidstatusinfobtn").prop('value', 'Con');
				$("#rfidstatusinfobtn").css("background-color", "Green");
			} else {
				$("#rfidstatusinfobtn").prop('value', 'Dis-Con');
				$("#rfidstatusinfobtn").css("background-color", "Red");
			}
			if(startstoptrigger == 'Yes') {
				var stocktakeformstatus = $('#stocktakeformstatus').val();
				if(stocktakeformstatus != data['message'])
				{
					if(data['message'] == 'stop')
					{
						$('#stocktakeformstatus').val('stop');
						$('#stopprocess').trigger('click');
					}else if(data['message'] == 'start')
					{
						$('#stocktakeformstatus').val('start');
						$('#startprocess').trigger('click');
					}
				}
			}
			
		},
	});
}
function checkdeviceisconnected(deviceno) {
	var connectstatus = '';
	if(deviceno != '') {
		$.ajax({
			url:base_url+"Stocktaking/checkdeviceisconnected",
			data:{deviceno:deviceno},
			method: "POST",
			dataType:'json',
			async:false,
			success :function(data) {
				connectstatus = data['connectstatus'];
			},
		});
	}
	return connectstatus;
}
function itemtagimagedisplay(imgbarcodevalue,itemtagid) {
	$('#imgpreview_itemtagno,#imgpreview_rfidtagno,#imgpreview_category,#imgpreview_product,#imgpreview_purity,#imgpreview_storage,#imgpreview_grossweight,#imgpreview_stoneweight,#imgpreview_netweight').text('');
	if (imgbarcodevalue != "" || imgbarcodevalue != null) {
		$.ajax({
			url: base_url+"Base/retrieveitemtagdetails",
			data: "barcodeno=" +imgbarcodevalue+"&itemtagid="+itemtagid,
			type: "POST",
			async:false,
			cache:false,
			dataType:'json',
			success: function(msg) {
				if(msg['fail'] == 'FAILED') {
					alertpopup('No details to display.');
				} else {
					$('#baseitemtagimgpreview').fadeIn();
					$('#baseitemtagimagepreview').empty();
					if(msg['tagimage'] != '') {
						var imagearray = msg['tagimage'].split(",");
						for (i=0;i<imagearray.length;i++) {
							var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
							img.attr('src', base_url+imagearray[i]);
							img.appendTo('#baseitemtagimagepreview');
						}
					} else {
						var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
							img.attr('src', base_url+'uploads/default/noimage.png');
							img.appendTo('#baseitemtagimagepreview');
					}
					$('#imgpreview_itemtagno').text(msg['itemtagnumber']);
					$('#imgpreview_rfidtagno').text(msg['rfidtagno']);
					$('#imgpreview_category').text(msg['categoryname']);
					$('#imgpreview_product').text(msg['productname']);
					$('#imgpreview_purity').text(msg['purityname']);
					$('#imgpreview_storage').text(msg['countername']);
					$('#imgpreview_grossweight').text(msg['grossweight']);
					$('#imgpreview_stoneweight').text(msg['stoneweight']);
					$('#imgpreview_netweight').text(msg['netweight']);
				}
			}
		});
	} else {
		alertpopupdouble('No Image to preview');
	}
}