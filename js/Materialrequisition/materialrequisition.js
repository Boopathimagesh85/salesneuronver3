$(document).ready(function(){
	$('body').fadeIn(1500);
	METHOD ='ADD';
	PRECISION = 0;
	QUANTITY_PRECISION = 0;
	PRICBOOKCONV_VAL = 0;
	PRODUCTTAXABLE = 'Yes';
	UPDATEID = 0;
	$(document).foundation();
	maindivwidth();
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{
		materialrequisitionaddgrid();
		//crud action
		crudactionenable();
	}
	{
		//module number overlay
		generatemoduleiconafterload('modulenumber','modulenumberevent');
		productgenerateiconafterload('productid','productsearchevent');
		generateuomiconafterload('quantity','uomclickevent','quantitydivhid');
		generateiconafterload('sellingprice','pricebookclickevent','sellingpricedivhid');
		$('#currencyconverticon').removeClass("hidedisplay")
		$("#uomclickevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				if(checkVariable('productid') == true) {
					$("#uomconvoverlay").fadeIn();
					$("#uomconvclose").focus();
				} else {
					alertpopup("Kindly Choose Product to Load UOM");
				}
			}
		});
		$("#pricebookclickevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				if(checkVariable('productid') == true) {
					$("#pricebookoverlay").fadeIn();
					$('#pricebookclose').focus();
					pricebookgrid();
				} else {
					cleargriddata('pricebookgrid');
				}
			}
		});
	}
	{	//Enables the Edit Buttons
		$('#materialrequisitionproingrideditspan1').removeAttr('style');		
	}
	{
		var addclosematerialrequisitioncreation =['closeaddform','materialrequisitioncreationview','materialrequisitioncreationformadd'];
		addclose(addclosematerialrequisitioncreation);
		var addcloseviewcreation =["viewcloseformiconid","materialrequisitioncreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			materialrequisitionaddgrid();
		});
	}
	//hide button
	$("#materialrequisitionproupdatebutton").hide();
	$("#materialrequisitionproingridexport1").hide();
	{
		if(softwareindustryid == 4){
			var ordertype = $('#producthide').val();
			if(ordertype){
				$.ajax({
					url:base_url+"Base/getorderbasedproduct?ordertypeid="+ordertype+"&producthide=Yes",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$('#productid').empty();
						prev = ' ';
						var optclass = [];
						$.each(data, function(index) {
							var cur = data[index]['PId'];
							if(prev != cur ) {
								if(prev != " ") {
									$('#productid').append($("</optgroup>"));
								}
								if(jQuery.inArray(data[index]['optclass'], optclass) == -1) {
									optclass.push(data[index]['optclass']);
									$('#productid').append($("<optgroup  label='"+data[index]['optclass']+"' class='"+data[index]['optclass']+"'>")); 
								} 
								$("#productid optgroup[label='"+data[index]['optclass']+"']")
    							.append($("<option></option>")
    							.attr("class",data[index]['optionclass'])
    							.attr("value",data[index]['value'])
    							.attr("data-productidhidden",data[index]['data-productidhidden'])
    							.text(data[index]['data-productidhidden']));
								prev = data[index]['PId'];
							}
							
						}); 
						$('#productid').select2('val','').trigger("change");
					}
				});
			}
		}
	}
	{
	//operation events click/submit/validation/ddchange
	$("#formclearicon").click(function(){
		var elementname = $('#elementsname').val();
        elementdefvalueset(elementname);
        serialnumber();
		setdefaultproperty();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		//cleargrid data		
		cleargriddata('materialrequisitionproaddgrid1');
		$('#discountmodeid').removeAttr('readonly');	
		$('#discountmodeid').select2('val','2').trigger('change');
		froalaset(froalaarray);
		//
		setTimeout(function(){
			$("#accountid,#discountmodeid,#modulenumber").select2("readonly", false);},100); 
		$('#currentmode').val(1);
	});
	{// For touch
		fortabtouch = 0;
	}
		$('#cloneicon').click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid){
				materialrequisitionproaddgrid1();
				clearformgriddata();
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$('#adjustmenttypeid').select2('val','').trigger('change');
				froalaset(froalaarray);
				loadgroupdropdown();
				$('.updatebtnclass').addClass('hidedisplay');
				$('.addbtnclass').removeClass('hidedisplay');
				$('#dataaddsbtn').removeClass('hidedisplayfwg');
				showhideiconsfun('editclick','materialrequisitioncreationformadd');
				$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").show();
				$(".fr-element").attr("contenteditable", 'true');
				retrievematerialrequisitiondata(datarowid);
				serialnumber();
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				firstfieldfocus();
				$("#pricebookclickevent").show();
				setTimeout(function(){
				$("#accountid,#discountmodeid,#modulenumber").select2("readonly",false);},100); 
				$('#currentmode').val(2);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} 
			else{
				alertpopup('Please select a row');
			}
		});
		//cancel data
		$("#cancelno").click(function(){
			$("#basecanceloverlay").fadeOut();
		});
		$("#cancelyes").click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			var cancelresult=checkcancelstatus('249','materialrequisition',datarowid);
			if(cancelresult == true)
			{
			$.ajax({
			url: base_url + "Materialrequisition/canceldata?primarydataid="+datarowid,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == true) {
					$("#basecanceloverlay").fadeOut();
					refreshgrid();				
				} 
				else if (nmsg == "false") {
				}
			},
			});
			}
			else{
				alertpopup("This record cannot be Cancelled");
			}
		});
		$("#cancelicon").click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$("#basecanceloverlay").fadeIn();				
				$("#cancelyes").focus();
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$('#stopicon').click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basestopoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#basestopyes').click(function()
		{
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			$.ajax({
			url: base_url + "Materialrequisition/stopdata?primarydataid="+datarowid,
			cache:false,
			success: function(msg){
				var nmsg =  $.trim(msg);
				if (nmsg == true){
					refreshgrid();
					$('#basestopoverlay').fadeOut();
				} 
				else if (nmsg == "false"){
				}
			},
			});
		});	
		$('#reloadicon').click(function(){
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('materialrequisitionaddgrid');
			sectionpanelheight('materialrequisitionprooverlay');
		});
		{//inner-form-with-grid
			$("#materialrequisitionproingridadd1").click(function(){
				sectionpanelheight('materialrequisitionprooverlay');
				$("#materialrequisitionprooverlay").removeClass("closed");
				$("#materialrequisitionprooverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
			});
			$("#materialrequisitionprocancelbutton").click(function(){
				clearform('gridformclear');
				$("#materialrequisitionprooverlay").removeClass("effectbox");
				$("#materialrequisitionprooverlay").addClass("closed");
				$('#materialrequisitionproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#materialrequisitionproaddbutton').show();//display the ADD button(inner-productgrid)*/	
			});
		}
		$("#detailedviewicon").click(function() {
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				materialrequisitionproaddgrid1();
				//Function Call For Edit
				clearformgriddata();
				froalaset(froalaarray);
				retrievematerialrequisitiondata(datarowid);
				showhideiconsfun('summryclick','materialrequisitioncreationformadd');
				$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").hide();
				$(".froala-element").css('pointer-events','none');
				$('.addbtnclass').addClass('hidedisplay');
				$("#pricebookclickevent,#productsearchevent,#uomclickevent").hide();
				$(".fr-element").attr("contenteditable", 'false');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					materialrequisitionproaddgrid1();
					//Function Call For Edit
					clearformgriddata();
					froalaset(froalaarray);
					retrievematerialrequisitiondata(rdatarowid);
					showhideiconsfun('summryclick','materialrequisitioncreationformadd');
					$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").hide();
					$(".froala-element").css('pointer-events','none');
					$('.addbtnclass').addClass('hidedisplay');
					$("#pricebookclickevent,#productsearchevent,#uomclickevent").hide();
					$(".fr-element").attr("contenteditable", 'false');
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function()
		{
			showhideiconsfun('editfromsummryclick','materialrequisitioncreationformadd');
			$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").show();
			$(".fr-element").attr("contenteditable", 'true');
			$(".froala-element").css('pointer-events','auto');
			if(softwareindustryid == 2){
				$("#productsearchevent").show();
			}else{
				$("#pricebookclickevent,#productsearchevent,#uomclickevent").show();
			}		
		});
		$('#materialrequisitionproingriddel1').click(function() {
			var datarowid = $('#materialrequisitionproaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {		
				if(METHOD == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('materialrequisitionproaddgrid1',datarowid);
					var gridsumcolname = ['grossamount','discountamount','taxamount','additionalamount','netamount'];
					var sumfieldid = ['summarygrossamount','summarydiscountamount','summarytaxamount','summaryadditionalchargeamount','summarynetamount'];
					var gridname = ['materialrequisitionproaddgrid1','materialrequisitionproaddgrid1','materialrequisitionproaddgrid1','materialrequisitionproaddgrid1','materialrequisitionproaddgrid1'];		
					var productrecords = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length; 						
					var currentmode = $('#currentmode').val();
					if(productrecords == 0 && currentmode == 1) {
						$('#discountmodeid').removeAttr('readonly');	
						$('#discountmodeid').select2('val','2').trigger('change');	
						$('#pricebookid').select2('val','');
						$('#pricebookid').select2("readonly", false);	
						$('#pricebookid').trigger("change");
						PRICBOOKCONV_VAL = 0;
						$("#pricebookclickevent").show();
					}
					gridsummeryvaluefetch('','','');
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			var accountid=getaccountid(datarowid,'materialrequisition','accountid');
			if (datarowid) {
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','materialrequisition',datarowid);
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",emailtosend.subject);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				} else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{
		$('#dataaddsbtn').click(function() {
			$('#dataaddsbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[integer],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#requireddate').removeClass('validate[required]');
			$('#uomid').removeClass('validate[required]');
			$('#conversionquantity').removeClass('validate[required,maxSize[100]]');
			$('#grossamount').removeClass('validate[required,maxSize[100]]');
			$('#materialrequisitionformaddwizard').validationEngine('validate');
			
		});
		$('#materialrequisitionformaddwizard').validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone' ){
					var getrownumber = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#materialrequisitionproaddgrid1 .gridcontent div.wrappercontent div').length;
				}				
				if (getrownumber != 0){
					materialrequisitioncreate();
					$('#dataaddsbtn').removeClass('singlesubmitonly');
				}else{
					alertpopup('Enter The Product...');
					$('#dataaddsbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#uomid').addClass('validate[required]');
					$('#requireddate').addClass('validate[required]');
					$('#conversionquantity').addClass('validate[required,maxSize[100]]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
				}	
			},
			onFailure: function() {
				var dropdownid =['4','requisitiontypeid','requisitionstatusid','accountid','currencyid'];
				dropdownfailureerror(dropdownid);
				$('#dataaddsbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#uomid').addClass('validate[required]');
				$('#requireddate').addClass('validate[required]');
				$('#conversionquantity').addClass('validate[required,maxSize[100]]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('#dataupdatesubbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[integer],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#uomid').removeClass('validate[required]');
			$('#requireddate').removeClass('validate[required]');
			$('#conversionquantity').removeClass('validate[required,maxSize[100]]');
			$('#grossamount').removeClass('validate[required,maxSize[100]]');
			$('#materialrequisitionformeditwizard').validationEngine('validate');
		});
		$('#materialrequisitionformeditwizard').validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone'){
					var getrownumber = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length;	
				}else{
					var getrownumber = $('#materialrequisitionproaddgrid1 .gridcontent div.wrappercontent div').length;	
				}							
				if(getrownumber != 0){
					materialrequisitionupdate();
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				}
				else{
					alertpopup('Enter The Product...');
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#uomid').addClass('validate[required]');
					$('#requireddate').addClass('validate[required]');
					$('#conversionquantity').addClass('validate[required,maxSize[100]]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
				}	
			},
			onFailure: function() {
				var dropdownid =['4','requisitiontypeid','requisitionstatusid','accountid','currencyid'];
				dropdownfailureerror(dropdownid);
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#uomid').addClass('validate[required]');
				$('#requireddate').addClass('validate[required]');
				$('#conversionquantity').addClass('validate[required,maxSize[100]]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				alertpopup(validationalert);
			}
		});
		//summery discount submit		
		$('#summaryindiscountadd').click(function(e){
			$("#summarydiscountvalidation").validationEngine('validate');			
		});
		jQuery("#summarydiscountvalidation").validationEngine({
			onSuccess: function() {
				summarydiscountcalculation();
				$("#summarydiscountoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
	}
	{
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){
			//set newquotedetail
			$('#materialrequisitionproductdetailsid').val(0);
			//array manipulate to set the array details
			//reset individual array
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');
			gridformtogridvalidatefunction(gridname);
			setTimeout(function(){
				$('#touomid').select2('val','');
				$('#touomid').empty();
			},100);
			griddataid = i;
			gridynamicname = gridname;
			$('#'+gridname+'validation').validationEngine('validate');
			//materialize update
			setTimeout(function(){					
				Materialize.updateTextFields();
			},150);
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{		
		{//product change events  - 
			$("#productid").change(function(){
				var val=$(this).val();
				if(val){
					var productval = $(this).find('option:selected').data('productidhidden');
					var ul = $('#materialrequisitionproaddgrid1 div.gridcontent .inline-list');
					var lis = ul.children('li');
					var isInList = false;
					for(var i = 0; i < lis.length; i++){
					    if(lis[i].innerHTML === productval) {
					        isInList = true;
					        break;
					    }
					}
					if(isInList){
						 alert("Product already Exist. Kindly choose another Product");
						 $('#productid').select2('val','');
						 $('#uomid').select2('val','');
						 $('#touomid').empty();
						 $('#instock,#conversionrate,#conversionquantity,#quantity,#unitprice,#sellingprice,#grossamount,#discountamount,#requireddate,#netamount').val(" ");
					}else{
						var proname = $("#productid").find('option:selected').data('productidhidden');
						$("#productidname").val(proname);
						$('#discountoverlay').find('input:text').val('');
						$('#discountamount,#netamount').val('');
						$('#discounttypeid').select2('val','').trigger('change');					
						getproductdetails(val);
						$('.error').removeClass('error');
						$(".btmerrmsg").remove();
						$('.formError').remove();
						var inputs = $(this).closest('form').find(':input');
						inputs.eq( inputs.index(this)+ 1 ).focus();
					}					
				}else{
					$('#uomid').select2('val','');
					$('#touomid').empty();
					$('#instock,#conversionrate,#conversionquantity,#quantity,#unitprice,#sellingprice,#grossamount,#discountamount,#requireddate,#netamount').val(" ");
				}				
			});
		}
	}
	{
		//currency overlay
		$("#currencyconverticon").click(function(){
			var productrecords = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length;
			if(productrecords == 0){
				$("#currencyconvoverlay").fadeIn();
				$.ajax({
					url:base_url+"Base/getdefaultcurrency", 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {						
						$('#defaultcurrency').select2('val',data.currency).trigger('change').attr("readonly","readonly");
					}
				});
			}					
		});
		$("#currencyconvclose").click(function() {
			$("#currencyconvoverlay").fadeOut();
		});
		//currency 
		$("#convcurrency").change(function() {
			var defaultcurrency = $("#currencyid").val();
			var currencyid = $(this).val();
			var quotedate = $("#requisitiondate").val();
			if(currencyid > 0) {
					var selected=$('#currencyid').find('option:selected');
					var datavalue=selected.data('precision');
					PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null) {
					PRECISION = 0;
				}
				$.ajax({
					url:base_url+"Quote/getconversionrate?fromcurrency="+defaultcurrency+"&tocurrency="+currencyid+"&quotedate="+quotedate, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data.status == true){
							$('#currencyconv').val(data.rate);
							Materialize.updateTextFields();
						}else{
							$('#currencyconv').val(0);
							Materialize.updateTextFields();
						}
					}
				});
			} else {
				PRECISION = 0;
				$('#currencyconv').val(0);
			}
			
		});
		$('#currencyconv_submit').click(function(e){
			$("#currencyconv_validate").validationEngine('validate');			
		});
		jQuery("#currencyconv_validate").validationEngine({
			onSuccess: function() { 
				var convcurrency=$("#convcurrency").val();	 //convert currency
				var currencyconv=$("#currencyconv").val();	 //convert rate
				$("#currencyid").val(convcurrency).trigger("change");
				$("#currencyconvresionrate").val(currencyconv).trigger("change");
				$("#productid").select2('val','').trigger("change");
				$("#currencyconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});		
		$("#currencyid").change(function(){			
			var currencyid = $(this).val();
			if(currencyid > 0){
				var selected=$('#currencyid').find('option:selected');
				var datavalue=selected.data('precision');
				PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null){
					PRECISION = 0;
				}	
				
			} else {
				PRECISION = 0;
			}
		});
		
		// Price book submit
		$("#pricebooksubmit").click(function() {
			var selectedrow = $('#pricebookgrid div.gridcontent div.active').attr('id');
			if(selectedrow) {
				var sellingprice = getgridcolvalue('pricebookgrid',selectedrow,'sellingprice','');
				var currencyid = getgridcolvalue('pricebookgrid',selectedrow,'currencyid','');
				var current_currencyid=$("#currencyid").val();
				if(current_currencyid == currencyid) {
					$('#sellingprice').val(sellingprice).trigger('focusout'); //sets the value and triggerchange calc
					$("#pricebookoverlay").fadeOut();
				} else if(current_currencyid != currencyid) {
					var quotedate = $('#quotedate').val();
					var conversionrate = getcurrencyconversionrate(currencyid,current_currencyid,quotedate); //to get conversion rate
					$('#currentcurrency').select2('val',current_currencyid).trigger('change').attr("disabled","disabled");
					$('#pricebookcurrency').select2('val',currencyid).trigger('change').attr("disabled","disabled");				
					$("#pricebook_sellingprice").val(sellingprice);					
					$('#pricebook_currencyconv').val(conversionrate);
					$("#pricebookcurrencyconvoverlay").fadeIn();
					Materialize.updateTextFields();
				}								
			} else {
				alertpopup('Please select a row');
			} 
		});	
		
		$('#uomconv_submit').click(function(e){
			$("#uomconv_validate").validationEngine('validate');			
		});
		jQuery("#uomconv_validate").validationEngine({
			onSuccess: function() {
				var uom_json ={}; //declared as object
				uom_json.uomid = $('#uomid').val(); 
				uom_json.touomid = $('#touomid').val();	
				uom_json.conversionrate = $('#conversionrate').val();
				uom_json.conversionquantity = $('#conversionquantity').val();	
				$('#uomdata').val(JSON.stringify(uom_json));
				$("#uomconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
		$("#uomconvclose").click(function() {
			$("#uomconvoverlay").fadeOut();
			$("#sellingprice").focus();
		});		
		$("#territoryid").change(function(){
			var id = $(this).val();
			if(id){
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";								
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}else{
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						$("#pricebookid").empty();
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}
		});
		$("#pricebookid").change(function() {	
			var val= $(this).val();
			var currency = $('#currencyid').val();
			if(checkValue(val) == true){
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
				$.ajax({
				url:base_url+'Quote/pricebookcurrency?pricebook='+val+'&currency='+currency,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.status != 'FAILED'){
						if(currency == data.currencyid){
						} else  {	
							var conversionrate = data.rate; //to get conversion rate
							$('#currentcurrency').select2('val',currency).trigger('change').attr("disabled","disabled");
							$('#pricebookcurrency').select2('val',data.currencyid).trigger('change').attr("disabled","disabled");					
							$('#pricebook_currencyconv').val(conversionrate);							
							$('#pricebook_currencyconv').focus();							
							$("#pricebookcurrencyconvoverlay").fadeIn();
							Materialize.updateTextFields();
						}
					}
				},
				});
				$("#pricebookclickevent").hide();
			}else{
				$("#pricebookclickevent").show();
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
			}
		});
		
		$('#pricebook_currencyconv_submit').click(function(e){
			$("#pricebookcurrencyconv_validate").validationEngine('validate');			
		});
		jQuery("#pricebookcurrencyconv_validate").validationEngine({
			onSuccess: function() { 
				var pb_sellingprice=$("#pricebook_sellingprice").val();	 //pricebook sellingprice
				var pb_currentcurrency = $("#currentcurrency").val();
				var pb_pricebookcurrency = $("#pricebookcurrency").val();
				var pb_conversionrate=$("#pricebook_currencyconv").val();	 //pricebook sellingprice
				$("#currentcurrencyid").val(pb_currentcurrency);
				$("#pricebookcurrencyid").val(pb_pricebookcurrency);
				$("#pricebook_currencyconvrate").val(pb_conversionrate);
				PRICBOOKCONV_VAL = pb_conversionrate;
				var productid = $("#productid").val();
				if(productid){
					var finalvalue = listprice(pb_sellingprice,pb_conversionrate);
					var conversionrate = $('#currencyconvresionrate').val();
					finalvalue = listprice(finalvalue,conversionrate);
					$('#sellingprice').val(finalvalue).trigger('focusout'); //sets the value and triggerchange calc
				}	
				$("#pricebookcurrencyconvoverlay,#pricebookoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
		
		$("#pricebookclose").click(function() {
			$("#pricebookoverlay").fadeOut();
			$('#grossamount').focus();
		});
		$("#pricebookcurrencyconvclose").click(function() {
			$("#pricebookcurrencyconvoverlay").fadeOut();
		});
	}
	{//overlay opertaions
		$("#discountamount").click(function() {
			var product=checkVariable('productid');
			var discount_data = $('#discountdata').val();
			var parse_discount = $.parseJSON(discount_data);
			if(parse_discount == null) { //if no previous data exits then refresh the discount fields
				clearform('resetoverlay');
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$('#discounttypeid').select2('val','').trigger('change');
				$('#discountpercent').val('');				
			} else {								
				$('#discounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#discountpercent').val(parse_discount.value);
				Materialize.updateTextFields();
			} 
			$("#discountoverlay").fadeIn(); 
			$("#discountclose").focus();
		});
		$("#discountclose").click(function()
		{
			$("#discountoverlay").fadeOut();
			var value=parseFloat($('#discountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#discounttypeid').select2('val','').trigger('change');
			}
		});
		$('#discountsubmit').click(function(e){
			$("#individualdiscountvalidation").validationEngine('validate');			
		});
		jQuery("#individualdiscountvalidation").validationEngine({
			onSuccess: function() {
				var discount_json ={}; //declared as object
				discount_json.typeid = $('#discounttypeid').val(); //discount typeid
				discount_json.value = $('#discountpercent').val();	//discount values		
				$('#discountdata').val(JSON.stringify(discount_json));
				discountcalculation();
				clearform('resetoverlay');
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$("#discountoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
		//Price book overlay
		$("#sellingprice").dblclick(function()
		{
			$("#pricebookoverlay").fadeIn();
			var pid = $("#productid").val();
			var pricebookid=$("#pricebookid").val();
			pricebookgrid();
		});
		{// Quantity calculation
		$("#sellingprice").focusout(function()
		{
			if(checkVariable('sellingprice') == false){	
				$("#sellingprice").val(0);
			}
			$("#quantity").trigger('focusout');
		});
		$("#quantity").focusout(function()
		{
			var quan = $("#quantity").val();
			if(isNaN(quan)){
				$('#quantity').val(1);
			}else{
				var stock = parseInt($('#instock').val());
				var qty = parseInt($('#quantity').val());
				if(qty > stock){
					alertpopup("Stock should be greater than quantity");
					$("#quantity").val(1);
					$("#touomid").val('');
					$('#conversionrate').val('');
					$('#conversionquantity').val('');
				}else{
					if(checkVariable('quantity') == true ){
						var sellprice = $("#sellingprice").val();
						var gross = parseFloat(quan) * parseFloat(sellprice);
						$("#grossamount").val(gross.toFixed(2));
						setTimeout(function(){
							discountcalculation();
						},1); 			
						productnetcalculate();
					}
				}
			}			
		});
	}	
	//price book overlay close
	$("#pricebookclose").click(function()
	{
		$("#pricebookoverlay").fadeOut();
		$('#grossamount').focus();
	});	
	//summary discount calculation 
		$("#summarydiscountamount").click(function() {
			var productlist = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length;
			if(productlist > 0) {
				var discount_data = $('#groupdiscountdata').val();
				var parse_discount = $.parseJSON(discount_data);				
				if(parse_discount == null){ //if no previous data exits then refresh the discount fields
					$('#groupdiscounttypeid').select2('val','').trigger('change');
					$('#groupdiscountpercent').val('');				
				} else {							
					$('#groupdiscounttypeid').select2('val',parse_discount.typeid).trigger('change');
					$('#groupdiscountpercent').val(parse_discount.value);
				}
				$("#summarydiscountoverlay").fadeIn();
				Materialize.updateTextFields();
			} else{
				alertpopup('Please enter the product details');
			}			
		});
		$("#summarydiscountclose").click(function() {
			clearform('summarydiscountclearform');
			$("#summarydiscountoverlay").fadeOut();
			var value=parseFloat($('#groupdiscountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#groupdiscounttypeid').select2('val','').trigger('change');
			}
		});
		//adjustment overlay
		$("#summaryadjustmentamount").click(function() {
			var productlist = $('#materialrequisitionproaddgrid1 .gridcontent div.data-content div').length;
			if(productlist >0) {
				var adjustment_data = $('#groupadjustmentdata').val();
				var parse_adjustment = $.parseJSON(adjustment_data);				
				if(parse_adjustment == null){ //if no previous data exits then refresh the adjusttment fields
					$('#adjustmenttypeid').select2('val','2').trigger('change');
					$('#adjustmentvalue').val(0);				
				} else {							
					$('#adjustmenttypeid').select2('val',parse_adjustment.typeid).trigger('change');
					$('#adjustmentvalue').val(parse_adjustment.value);
				}	
				$("#adjustmentoverlay").fadeIn();
				Materialize.updateTextFields();
			}
		});
		$("#adjustmentclose").click(function()
		{
			clearform('clearadjustmentform');
			$("#adjustmentoverlay").fadeOut();
		});
		$("#adjustmentadd").click(function()
		{
			$('#adjustmenttypeid').trigger('change'); //to liveup adjustmentdd
			setzero(['adjustmentvalue']);	
			var adjustment_json ={}; //declared as object
			adjustment_json.typeid = $('#adjustmenttypeid').val(); //adjustment type
			adjustment_json.value = $('#adjustmentvalue').val(); // adjustment value			
			$('#groupadjustmentdata').val(JSON.stringify(adjustment_json));				
			var adjustment_value=parseFloat($('#adjustmentvalue').val());				
			$("#summaryadjustmentamount").val(adjustment_value.toFixed(PRECISION));
			$("#adjustmentoverlay").fadeOut();
			//?trigger point for calculation
			summarynetamountcalc();
		});
	}	
	//module change events
	$('#modulenameid').change(function(){
		var id = $(this).val();
		smsdropdownmodulevalset('formfieldsid','formfieldsidhidden','modfiledcolumn','modfiledtable','fieldlabel','modulefieldid','columnname','tablename','modulefield','moduletabid,uitypeid',''+id+',0');
	});
	//module fields change events
	$('#formfieldsid').change(function(){
		if( $(this).val()!= "" ){
			var table = $(this).find('option:selected').data('modfiledtable');
			var tabcolumn = $(this).find('option:selected').data('modfiledcolumn');
			var mtext = '{'+table+'.'+tabcolumn+'}';
			var oldmtext = $('#mergetext').val();
			var ftext = oldmtext +' '+ mtext;
			$('#mergetext').val(mtext);
		}
	});
	//terms and condition value fetch
	$("#termsandconditionid").change(function()
	{
		var id = $("#termsandconditionid").val();
		tandcdatafrtch(id);
	});
	$("#materialrequisitionproingridedit1").click(function() {
		var selectedrow = $('#materialrequisitionproaddgrid1 div.gridcontent div.active').attr('id');
		if(selectedrow) {
			sectionpanelheight('materialrequisitionprooverlay');
			$("#materialrequisitionprooverlay").removeClass("closed");
			$("#materialrequisitionprooverlay").addClass("effectbox");
			$("#touomid").select2("val", "");
			$("#touomid").empty();
			var value = $('#materialrequisitionproaddgrid1 .gridcontent div#'+selectedrow+' ul .productid-class').text();
			var pricebook = $('#pricebookid').val();
			var uomdata = $('#materialrequisitionproaddgrid1 .gridcontent div#'+selectedrow+' ul .uomdata-class').text();
			if(uomdata) {
				var uomdetail = $.parseJSON(uomdata);
			} else {
				var uomdetail = [];
			}
			$.ajax({
				url:base_url+'Base/getproductdetails?id='+value+'&pricebook='+pricebook,
				dataType:'json',
				async:false,cache:false,
				cache:false,
				success: function(data) 
				{
					$uomdata = data.touomarr;
					$uomlength = $uomdata.length;
					for (var i = 0; i < $uomlength; i++) {
						$('#touomid')
						.append($("<option></option>")
		                .attr("value",$uomdata[i]['uomtoid'])
		                .attr("data-touomidhidden",$uomdata[i]['uomname'])
		                .attr("data-conrate",$uomdata[i]['conversionrate'])
		                .attr("data-symbol",$uomdata[i]['symbol'])
		                .attr("data-precision",$uomdata[i]['uomprecision'])
		                .text($uomdata[i]['uomname']));
					}
					$('#uomid').select2('val',uomdetail['uomid']);
					$("#uomid").attr('readonly','readonly');
					$('#touomid').select2('val',uomdetail['touomid']).trigger('change');
					$('#conversionrate').val(uomdetail['conversionrate']);
					$('#conversionquantity').val(uomdetail['conversionquantity']);
				}
			});
			gridtoformdataset('materialrequisitionproaddgrid1',selectedrow);
			$('#materialrequisitionproupdatebutton').show();	//display the UPDATE button(inner - productgrid)
			$('#materialrequisitionproaddbutton').hide();	//display the ADD button(inner-productgrid) */
			$("#materialrequisitionproupdatebutton").removeClass('hidedisplayfwg');
			METHOD = 'UPDATE';
			UPDATEID = selectedrow;
			Materialize.updateTextFields();
		} else {
			alertpopup('Please Select The Row To Edit');
		}
	});
	 //load salesorder detail on change
	$("#modulenumber").change(function()
	{
		var moduleid=217;
		var moduleuniqueid=$("#modulenumber").find('option:selected').data('moduleuniqueid');
	});
	{//convert to purchaseorder method
		$("#convertpoicon").click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if(datarowid)
			{
				var ckstatus=checkvalidconvertpo(datarowid);
				if(ckstatus == true){
				sessionStorage.setItem("convertpofrommr",datarowid);
				window.location = base_url+'Purchaseorder';
				}
				else {
					alertpopup("Cannot Convert To Purchaseorder");
				}
			}			
		});
	}
	//check dataLoad from 
	var salesorderid = sessionStorage.getItem("convertmrfromso"); 
	if(salesorderid != null){
		setTimeout(function(){
			$(".addicon").trigger('click');
			$('#modulenumber').select2('val',salesorderid).trigger('change');
			sessionStorage.removeItem("convertmrfromso");
		},100);				
	}
	{
		$('#printicon').click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});		
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// For Inner Grid Width Issue
		$("#tab2").click(function() {
			refreshgrid();
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}	
	{// module number overlay	
		$("#modulenumberevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
			$('#mnsearchoverlay').fadeIn();	
			$('#mnsearchclose').focus();
			modulenumbersearchgrid(217);
			}
		});
		$("#mnsearchclose").click(function() {
			$('#mnsearchoverlay').fadeOut();	
			$("#s2id_branchid").select2('focus');
		});
		$("#mnsearchsubmit").click(function(){
			var datarowid = $('#modulenumbersearchgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var prevmodulenumber = $('#modulenumber').val();
				var number = getgridcolvalue('modulenumbersearchgrid',datarowid,'salesordernumber','');								
				$('#modulenumber').val(number);
				$('#mnsearchoverlay').fadeOut('slow');
				//data clear
				//Reset the invoice elements				
				var mrdate = $('#requisitiondate').val();
				var mrnumber = $('#materialrequisitionnumber').val();
				if((prevmodulenumber != number) ||(checkValue(prevmodulenumber) == false)){ //reset the elements ony if any numbers are set
					$("#formclearicon").trigger('click');
					$('#invoicedate').val(mrdate);
					$('#invoicenumber').val(mrnumber);
					$('#modulenumber').val(number);
					conversiondatamapping(datarowid,217,249);	
					Materialize.updateTextFields();
				}		
				cleargriddata('modulenumbersearchgrid');
			} else {
				alertpopup('Please select row');
			}
		});
	} 
	{ // product search overlay
		$("#productsearchevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
			$("#productsearchoverlay").fadeIn();
			$("#productsearchclose").focus();
			productsearchgrid();
			}
		});
		$("#productsearchclose").click(function() {
			$("#productsearchoverlay").fadeOut();
			$('#quantity').focus();
		});
		$("#productsearchsubmit").click(function() {
			var selectedrow = $('#productsearchgrid div.gridcontent div.active').attr('id');
			if(selectedrow){
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
			}			
		});
	}	
	$("#discounttypeid").change(function(){
		$('#discountpercent').val(0);
		setTimeout(function()
		{
			discount_total();
			Materialize.updateTextFields();
		},100);
	});
	$("#discountpercent").focusout(function(){			
		setTimeout(function()
		{
			discount_total();
			Materialize.updateTextFields();
		},100);
	});
	//discountrefresh-individual
	$("#discountrefresh").click(function(){
		$('#discounttypeid').select2('val','2').trigger('change');
		$('#discountpercent').val('');
		$('#singlediscounttotal').val('');			
	});
	//discountrefresh-group
	$("#groupdiscountrefresh").click(function(){
		$('#groupdiscounttypeid').select2('val','2').trigger('change');
		$('#groupdiscountpercent').val('0');
		$('#groupdiscounttotal').val('');
		Materialize.updateTextFields();
	});
	$("#groupdiscounttypeid").change(function(){
		$('#groupdiscountpercent').val(0);
		setTimeout(function()
		{
			group_discount_total();
			Materialize.updateTextFields();
		},100);
	});
	$("#groupdiscountpercent").focusout(function(){			
		setTimeout(function()
		{
			group_discount_total();
			Materialize.updateTextFields();
		},100);
	});	
	//uom change
	$("#touomid").change(function() {
		var touomid = $("#touomid").val();
		if(touomid != null){
			var symbol = $('option:selected', this).attr('data-symbol');
			var conrate = $('option:selected', this).attr('data-conrate');
			var precision = $('option:selected', this).attr('data-precision');		
			QUANTITY_PRECISION = precision;
			$('#conversionrate').val(conrate).trigger('focusout');
		}else{
			$('#conversionrate').val("");
			$('#conversionquantity').val("");
		}		
	});
	//conversion rate
	$("#conversionrate,#conversionquantity").focusout(function(){
		var crate = $("#conversionrate").val();
		if(isNaN(crate)) {
			$('#conversionrate').val(1);
		}
		var cquan = $("#conversionquantity").val();
		if(isNaN(cquan)) {
			$('#conversionquantity').val(1);
		}
		if(checkVariable('conversionrate') == true && checkVariable('conversionquantity') == true){
			var crate = $("#conversionrate").val();
			var cquan = $("#conversionquantity").val();
			var quantity = parseFloat(cquan) * parseFloat(crate);	
			$('#quantity').val(quantity);
			$("#quantity").trigger('focusout');
		}
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,materialrequisitionaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	//Function call for validate
	$("#"+gridnamevalue+"validation").validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				formtogriddata(gridname,METHOD,'',UPDATEID);
			}
			/* Hide columns */
			var hideprodgridcol = ['discountdata','materialrequisitionproductdetailsid','uomdata'];
			gridfieldhide('materialrequisitionproaddgrid1',hideprodgridcol);
			/* Data row select event */
			datarowselectevt();
			$('#pricebookid').attr("readonly", true);
			$('#discountdata').val('');
			METHOD = 'ADD'
			//summary calculate
			$('#materialrequisitionproupdatebutton').show();	//display the UPDATE button(inner - productgrid)
			$('#materialrequisitionproaddbutton').hide();	//display the ADD button(inner-productgrid) */
			clearform('gridformclear');
			//close product section
			$('#materialrequisitionprocancelbutton').trigger('click');
			griddataid = 0;
			gridynamicname = '';
			var gridsumcolname = ['grossamount','discountamount','netamount'];
			var sumfieldid = ['summarygrossamount','summarydiscountamount','summarynetamount'];
			gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid);
		},
		onFailure: function() {
			var dropdownid =['1','productid'];
			dropdownfailureerror(dropdownid);
		}
	});
}
{// Price book Overlay grid
	function pricebookgrid(page,rowcount) {
		var pid = $("#productid").val()!='' ? $("#productid").val() : 1;
		var territory = $("#territoryid").val() ? $("#territoryid").val() : 1;
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#pricebookgrid').width();
		var wheight = $('#pricebookgrid').height();
		/* col sort */
		var sortcol = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/pricebookview?maintabinfo=pricebookdetail&primaryid=pricebookdetailid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=219&productid='+pid+'&territory='+territory,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#pricebookgrid').empty();
				$('#pricebookgrid').append(data.content);
				$('#pricebookgridfooter').empty();
				$('#pricebookgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('pricebookgrid');
				/* hide grid column */
				var hideprbokcol = ['currencyid'];
				gridfieldhide('pricebookgrid',hideprbokcol);
				{/* sorting */
					$('.pricebooklistheadercolsort').click(function(){
						$('.pricebooklistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					sortordertypereset('pricebooklistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('#pricebooklistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						pricebookgrid(page,rowcount);
					});
				}
			},
		});
	}
}
function materialrequisitionaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#materialrequisitionpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#materialrequisitionpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#materialrequisitionaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.materialrequisitionheadercolsort').hasClass('datasort') ? $('.materialrequisitionheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.materialrequisitionheadercolsort').hasClass('datasort') ? $('.materialrequisitionheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.materialrequisitionheadercolsort').hasClass('datasort') ? $('.materialrequisitionheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=materialrequisition&primaryid=materialrequisitionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#materialrequisitionaddgrid').empty();
			$('#materialrequisitionaddgrid').append(data.content);
			$('#materialrequisitionaddgridfooter').empty();
			$('#materialrequisitionaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('materialrequisitionaddgrid');
			{//sorting
				$('.materialrequisitionheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.materialrequisitionheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#materialrequisitionpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.materialrequisitionheadercolsort').hasClass('datasort') ? $('.materialrequisitionheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.materialrequisitionheadercolsort').hasClass('datasort') ? $('.materialrequisitionheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#materialrequisitionaddgrid .gridcontent').scrollLeft();
					materialrequisitionaddgrid(page,rowcount);
					$('#materialrequisitionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('materialrequisitionheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					materialrequisitionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#materialrequisitionpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					materialrequisitionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#materialrequisitionpgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			materialrequisitionproaddgrid1();
			appendcurrency('currencyid');
			e.preventDefault();
			addslideup('materialrequisitioncreationview','materialrequisitioncreationformadd');
			resetFields();
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			serialnumber();
			setdefaultproperty();
			froalaset(froalaarray);
			$('#materialrequisitionproaddbutton').removeClass('hidedisplay');
			$('.updatebtnclass').addClass('hidedisplay');
			loadgroupdropdown();
			clearformgriddata();
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			$("#pricebookclickevent,#productsearchevent,#uomclickevent").show();
			$('#defaultcurrency').select2('val','');
			$('#convcurrency').select2('val','');
			$('#currencyconv').val('');
			$('#adjustmenttypeid').select2('val','').trigger('change');			
			showhideiconsfun('addclick','materialrequisitioncreationformadd');
			$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").show();			
			firstfieldfocus();
			$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
			$(".fr-element").attr("contenteditable", 'true');
			$('#discountmodeid').removeAttr('readonly');
			$("#termsandconditionid").trigger('change');
			//disable fields
			setTimeout(function(){
			$("#accountid,#discountmodeid,#modulenumber,#pricebookid").select2("readonly", false);},100); 
			$('#currentmode').val(1);
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid){
				materialrequisitionproaddgrid1();
				clearformgriddata();
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$('#adjustmenttypeid').select2('val','').trigger('change');
				if(softwareindustryid != 2){
					froalaset(froalaarray);
				}
				loadgroupdropdown();
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#materialrequisitionproupdatebutton').removeClass('hidedisplayfwg');
				$('#materialrequisitionproupdatebutton').show();
				$('.addbtnclass').addClass('hidedisplayfwg');
				showhideiconsfun('editclick','materialrequisitioncreationformadd');
				$("#materialrequisitionproingridedit1,#materialrequisitionproingriddel1").show();
				$(".fr-element").attr("contenteditable", 'true');
				froalaset(froalaarray);
				retrievematerialrequisitiondata(datarowid);
				firstfieldfocus();
				setTimeout(function(){
				$("#discountmodeid,#modulenumber").select2("readonly", true);},100); //#accountid removed
				$('#currentmode').val(2);
				$("#pricebookclickevent").show();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				Operation = 1; //for pagination
			} else{
				alertpopup('Please select a row');
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#materialrequisitionaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup('Please select a row');
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			materialrequisitionrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#materialrequisitionpgnumcnt li .page-text .active').data('rowcount');
		materialrequisitionaddgrid(page,rowcount);
	}
}
function materialrequisitionproaddgrid1() {
	var wwidth = $("#materialrequisitionproaddgrid1").width();
	var wheight = $("#materialrequisitionproaddgrid1").height();
	$.ajax({
		url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=96&moduleid=249&width="+wwidth+"&height="+wheight+"&modulename=mrproduct",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#materialrequisitionproaddgrid1").empty();
			$("#materialrequisitionproaddgrid1").append(data.content);
			$("#materialrequisitionproaddgrid1footer").empty();
			$("#materialrequisitionproaddgrid1footer").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('materialrequisitionproaddgrid1');
			var hideprodgridcol = ['discountdata','materialrequisitionproductdetailsid','uomdata'];
			gridfieldhide('materialrequisitionproaddgrid1',hideprodgridcol);
		},
	});
}

function materialrequisitioncreate() {
	var amp = '&';
	var snmodulenumber=$('#modulenumber option:selected').text();
	if(checkVariable('modulenumber') == false){
		var snmodulenumber='';
	}
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	var resctable = $('#resctable').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata+'&'+getgridrowsdata(gridnames[j]);
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata+'&'+getgridrowsdata(gridnames[j]);
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;;
				}
			}
		}
		
	}
	var sendformadddata = JSON.stringify(addgriddata);	
	var summerydiscountformdata = $("#summarydiscountform").serialize();
	var summerydiscountdata = amp + summerydiscountformdata;
	var adjustment = $("#adjustmentform").serialize();
	var adjustmentdata = amp + adjustment;
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $('#materialrequisitiondataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Materialrequisition/newdatacreate',
		data:'datas='+datainformation+'&griddatas='+sendformadddata+'&numofrows='+noofrows+"&summerydiscountdata="+summerydiscountdata+"&adjustmentdata="+adjustmentdata+"&snmodulenumber="+snmodulenumber,
		type:'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#materialrequisitioncreationformadd').hide();
				$('#materialrequisitioncreationview').fadeIn(1000);
				refreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
				$("#materialrequisitioneditor").text('');
				clearformgriddata();
				$('#discountmodeid').removeAttr('readonly');
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$('#discounttypeid').select2('val','').trigger('change');
				alertpopup(savealert);
				$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#uomid').addClass('validate[required]');
				$('#requireddate').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
			} else if (nmsg == 'false') {
			}
		},
	});
}
function retrievematerialrequisitiondata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Materialrequisition/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#materialrequisitioncreationformadd').hide();
					$('#materialrequisitioncreationview').fadeIn(1000);
					refreshgrid();
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
				} else {
					addslideup('materialrequisitioncreationview','materialrequisitioncreationformadd');					
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					var modulenumber = $('#modulenumber').val();
					$('#pricebookcurrency').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconv').val(data['pricebook_currencyconvrate']);
					$('#currentcurrency').val(data['currentcurrencyid']);
					$('#currencyconv').val(data['currencyconvresionrate']);
					$('#convcurrency').val(data['currencyid']);
					$('#pricebookcurrencyid').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconvrate').val(data['pricebook_currencyconvrate']);
					$('#currentcurrencyid').val(data['currentcurrencyid']);
					$('#pricebookid').attr("readonly", true);
					PRICBOOKCONV_VAL = data['pricebook_currencyconvrate'];
					if(modulenumber == 1){
						$("#modulenumber").val('');
					}						
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					salesordernumbersload();
					//group discount adjustment data fetch
					groupadanddiscountdatafetch(datarowid);
					materialrequisitionproductdetail(datarowid);
					editordatafetch(froalaarray,data);
					Materialize.updateTextFields();
				}	
		}
	});
}
//
function groupadanddiscountdatafetch(datarowid) {
	$.ajax({
		url:base_url+'Materialrequisition/groupdiscountandadjustdetailsfetch?dataprimaryid='+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$("#groupdiscountdata").val(data.discount);
			$("#groupadjustmentdata").val(data.addcharge);
		}
	});
} 
function materialrequisitionproductdetail(pid) 
{	
	if(pid!='') {
		$.ajax({
			url:base_url+'Materialrequisition/materialrequisitionproductdetailfetch?primarydataid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('materialrequisitionproaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('materialrequisitionproaddgrid1');
					var hideprodgridcol = ['discountdata','materialrequisitionproductdetailsid','uomdata'];
					gridfieldhide('materialrequisitionproaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
function materialrequisitionupdate() {
	var amp = '&';
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]) );
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]) );
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	var summerydiscountformdata = $("#summarydiscountform").serialize();
	var summerydiscountdata = amp + summerydiscountformdata;
	var adjustment = $("#adjustmentform").serialize();
	var adjustmentdata = amp + adjustment;
	var formdata = $('#materialrequisitiondataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Materialrequisition/datainformationupdate',
		data: 'datas='+datainformation+"&adjustmentdata="+adjustmentdata+"&summerydiscountdata="+summerydiscountdata+"&numofrows="+noofrows+"&griddatas="+sendformadddata,
		type: 'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == true) {
				$('.ftab').trigger('click');
				resetFields();
				$('#materialrequisitioncreationformadd').hide();
				$('#materialrequisitioncreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
				$('#discountmodeid').removeAttr('readonly');
				$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
				$('#discounttypeid,#groupdiscounttypeid').select2('val','').trigger('change');
				alertpopup(savealert);
				$('#quantity').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#requireddate').addClass('validate[required]');
				$('#uomid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
			} else if (nmsg == 'false') {
			}
		},
	});
}
function materialrequisitionrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Materialrequisition/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		cache:false,
		success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == "TRUE") { 
					refreshgrid();
					$('#basedeleteoverlay').fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {
					refreshgrid();
					$('#basedeleteoverlay').fadeOut();
					alertpopup('Permission denied');
				}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewfieldids').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');		
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
///////////////
//serialnumber/
///////////////
function serialnumber()
{
	var moduleid=249;
	$.ajax({
		url:base_url+"Base/generatenumber?moduleid="+moduleid+"&table=materialrequisition", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
		$('#materialrequisitionnumber').val(data.number);		
		}
	});
	//set current user and branch
	$.ajax({
		url:base_url+"Base/userbranch", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#requestor').val(data.employeename);		
			$('#branchid').select2('val',data.branchid).trigger('change');		
		}
	});
}
////
//retrives salesorder numbers
function salesordernumbersload()
{
	$('#modulenumber').empty();
	$.ajax({
        url:base_url+'Materialrequisition/getsalesordernumber',
        dataType:'json',
        async:false,
		cache:false,
        success: function(data) 
		{
		  if((data.fail) == 'FAILED') {
            } else {
				  $('#modulenumber')
                    .append($("<option></option>"));
                $.each(data, function(index) {
				     $('#modulenumber')
                    .append($("<option></option>")
					.attr("data-moduleuniqueid",data[index]['id'])
                    .attr("value",data[index]['id'])
                    .text(data[index]['name']));
                });
            } 
        },
   });
}
//load group dowm
function loadgroupdropdown()
{
var table='singlegrouptype';
$('#discountmodeid').empty();
  $('#discountmodeid').append($("<option></option>"));
   $.ajax({
        url:base_url+'Base/getdropdownoptions?table='+table,
        dataType:'json',
        async:false,
		cache:false,
        success: function(data) 
		{
		  if((data.fail) == 'FAILED') {
            } else {
                $.each(data, function(index) {
                    $('#discountmodeid')
                    .append($("<option></option>")
                    .attr("value",data[index]['id'])
                    .text(data[index]['name']));
                });
            }    
           $('#discountmodeid').select2('val','2').trigger('change');
        },
   });
}
//retrieves product details
//get product details
function getproductdetails(val)
{
	if(checkValue(val) == true){
		var socurrency = $('#currencyid').val();
	}
	var pricebook = $('#pricebookid').val();
	$("#touomid").empty();
	$.ajax({
		url:base_url+'Base/getproductdetails?id='+val+'&pricebook='+pricebook,
		dataType:'json',
		async:false,cache:false,
		cache:false,
		success: function(data) 
		{
			//calculate selling price
			productstoragefetch(val);			
			$uomdata = data.touomarr;
			$uomlength = $uomdata.length;
			for (var i = 0; i < $uomlength; i++) {
				$('#touomid')
				.append($("<option></option>")
                .attr("value",$uomdata[i]['uomtoid'])
                .attr("data-touomidhidden",$uomdata[i]['uomname'])
                .attr("data-conrate",$uomdata[i]['conversionrate'])
                .attr("data-symbol",$uomdata[i]['symbol'])
                .attr("data-precision",$uomdata[i]['uomprecision'])
                .text($uomdata[i]['uomname']));
			}	
			$('#touomid').select2('val','').trigger('change');
			PRODUCTTAXABLE = data.taxable;
			$("#uomid").select2('val',data.uomid).trigger('change');
			$("#uomid").attr('readonly','readonly');
			setTimeout(function(){ 
				$('#conversionquantity').val('1');
				$('#conversionrate').val('1');
				var cquan = $('#conversionquantity').val();
				var crate = $('#conversionrate').val();
				var quantity = parseFloat(cquan) * parseFloat(crate);	
				$('#quantity').val(quantity);
				var unitprice = data.unitprice;
				if(unitprice == ''){
					var unitprice = 0;
				}	
				//list price set
				if(data.prodinprice == 0){
					$("#pricebookclickevent").show();
					var currencyconvresionrate =$('#currencyconvresionrate').val();
					if(softwareindustryid == 2){
						var list_price = listprice(unitprice,currencyconvresionrate);
						$('#unitprice').val(list_price);
						if(data.sellingprice != ''){
							var list_price = listprice(data.sellingprice,currencyconvresionrate);
							$('#sellingprice').val(list_price);
						}else if(data.unitprice != ''){
							var list_price = listprice(data.unitprice,currencyconvresionrate);
							$('#sellingprice').val(list_price);
						} else{
							$('#sellingprice').val(0);
						}
					}else{
						var list_price = listprice(unitprice,currencyconvresionrate);
						$('#unitprice').val(list_price);					
						//calculate selling price
						var list_price = listprice(data.sellingprice,currencyconvresionrate);
						$('#sellingprice').val(list_price);
					}
				}else{
					$("#pricebookclickevent").hide();
					var currencyconvresionrate =$('#currencyconvresionrate').val();
					var sellcurrencyconvresionrate =PRICBOOKCONV_VAL;
					if(softwareindustryid == 2){
						if(data.sellingprice != ''){
							var conversell = listprice(data.sellingprice,currencyconvresionrate);
							var list_price = listprice(conversell,sellcurrencyconvresionrate);
							$('#sellingprice').val(list_price);
						}else if(data.unitprice != ''){
							var list_price = listprice(data.unitprice,sellcurrencyconvresionrate);
							$('#sellingprice').val(list_price);
						} else{
							$('#sellingprice').val(0);
						}
					}else{
						var list_price = listprice(unitprice,currencyconvresionrate);
						$('#unitprice').val(list_price);					
						//calculate selling price
						var conversell = listprice(data.sellingprice,currencyconvresionrate);
						var list_price = listprice(conversell,sellcurrencyconvresionrate);
						$('#sellingprice').val(list_price);
					}
				}
				//gross amount
				$("#grossamount").val($('#sellingprice').val());
				//Net amount
				productnetcalculate();
				Materialize.updateTextFields();
			},100);
		},
	});
}
////calculates the netamount
function productnetcalculate()
{
	setzero(['grossamount','discountamount']);
	var sp=parseFloat($('#grossamount').val());
	var discount=parseFloat($('#discountamount').val());	
	var output=(sp-discount).toFixed(2);
	$('#netamount').val(output);	
}
function setzero(arrvalue)
{
	var a=arrvalue.length;
	for(var m=0;m < a;m++)
	{
		var fvalue= $('#'+arrvalue[m]+'').val();
		if(fvalue == '' || fvalue == null || fvalue == " "){
			$('#'+arrvalue[m]+'').val(0);
		}
	}
}
//discount calculation
function discountcalculation()
{
	var type=$('#discounttypeid').val();
	setzero(['discountpercent','grossamount']);
	var val=parseFloat($('#discountpercent').val());
	var saleprice=parseFloat($('#grossamount').val());
	discount=val;
	if(type == 3) {
		discount=(val/100)*saleprice;
	}
	$('#discountamount').val(discount.toFixed(PRECISION));		
	//itemcalculation
	setzero(['sellingprice','discountamount','grossamount']);
	var sp=parseFloat($('#grossamount').val());
	var discount=parseFloat($('#discountamount').val());
	var output=parseFloat(sp-discount);
	$('#netamount').val(output.toFixed(PRECISION));	
}
//validate percentage
function validatepercentage(field)
{
	var type =$('#discounttypeid').val();
	var value=field.attr('id');
	var num=parseFloat($('#'+value+'').val());
	if(type == 3){
		if(num < 0 || num > 100)
		return "percent value 0%-100%";			
	}
	else{
	}
}
function summaryvalidatepercentage(field)
{
	var type =$('#groupdiscounttypeid').val();
	var value=field.attr('id');
	var num=parseFloat($('#'+value+'').val());
	if(type == 3){
		if(num < 0 || num > 100)
		return "percent value 0%-100%";		
	}
	else{
	}
}
//product detail array
function productdetailarray()
{
	var discounttype=$('#discountmodeid').val();
	if(discounttype == 2){
		var discountarray=[];
		var data = {};		
		data.type=$('#discounttypeid').val();
		data.value=$('#discountpercent').val();
		discountarray.push(data);
	}
	else{
		var discountarray=[];
		var data = {};
		data.type=false;
		discountarray.push(data);
	}
	var discountjs=JSON.stringify(discountarray);
	$('#discountdata').val(discountjs);
}
//grid summery value fetch
function gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid)
{
		var summeryamount = parseFloat(getgridcolvalue('materialrequisitionproaddgrid1','','netamount','sum'));
		$('#summarygrossamount').val(summeryamount.toFixed(2));
		setzero(['summarygrossamount','groupdiscountamount']);
		//update group discount.
		var gross = summeryamount;
		var discount_json = $.parseJSON($('#groupdiscountdata').val()); //individual discount data					
		var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
		$('#groupdiscountamount').val(discount_amount);
		setTimeout(function(){
			calculatesummarydetail();			
		},10); 	
}
//summery discount calculation
function summarydiscountcalculation()
{
	var type=$('#groupdiscounttypeid').val();
	setzero(['groupdiscountpercent','summarygrossamount']);
	var val=parseFloat($('#groupdiscountpercent').val());
	var saleprice=parseFloat($('#summarygrossamount').val());
	discount=val;
	if(type == 3)
	{
		discount=(val/100)*saleprice;
	}
	tdiscount=parseFloat(discount);
	$('#summarydiscountamount').val(tdiscount.toFixed(2));	
	//total net amount value get
	summarynetamountcalc();
}
//calculates summary details
function calculatesummarydetail()
{
	summarynetamountcalc();
}
{//summary value calculation
	function summarynetamountcalc()
	{
		setzero(['summarygrossamount','summaryadjustmentamount','summarydiscountamount']);
		var typeid = $("#adjustmenttypeid").val();
		var adjustment = parseFloat($("#summaryadjustmentamount").val());
		var gross = parseFloat($("#summarygrossamount").val());
		var discount = parseFloat($("#summarydiscountamount").val());
		var pre_adjustment_total = gross - discount;
		if(adjustment > 0){
			var adjustment_data=$('#groupadjustmentdata').val();			
			var parse_adjustment = $.parseJSON(adjustment_data);
			if(parse_adjustment != null){
				if(parse_adjustment.typeid == 3){
					var grandtotal = pre_adjustment_total - adjustment ;
				} else {
					var grandtotal = pre_adjustment_total + adjustment;
				}
			}
		} else {
			var grandtotal=pre_adjustment_total;
		}
		$("#summarynetamount").val(grandtotal.toFixed(PRECISION));
		Materialize.updateTextFields();
	}	
	//summery discount calculation
	function summarydiscountcalculation()
	{
		var type=$('#groupdiscounttypeid').val();
		setzero(['groupdiscountpercent','summarygrossamount']);
		var val=parseFloat($('#groupdiscountpercent').val());
		var saleprice=parseFloat($('#summarygrossamount').val());
		discount=val;
		if(type == 3)
		{
			discount=(val/100)*saleprice;
		}
		discount=parseFloat(discount);
		$('#summarydiscountamount').val(discount.toFixed(2));	
		//total net amount value get
		summarynetamountcalc();
	}
	//clear grid data
	function clearformgriddata()
	{
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++)
		{
			cleargriddata(gridnames[j]);
		}
	}
	
}
//terms and condition data fetch
function tandcdatafrtch(id)
{
	$.ajax({
			url:base_url+"Materialrequisition/termsandcontdatafetch?id="+id,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data)
			{
				filenamevaluefetch(data);
			},
		});
}
//editor value fetch
function filenamevaluefetch(filename)
{
	//checks whether the Terms & condition is empty
	if(filename == '' || filename == null )
	{
		if(softwareindustryid != 2){
			froalaset(froalaarray);
		}
	}
	else{
	$.ajax({
		url: base_url + "Materialrequisition/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			froalaedit(data,'materialrequisitiontermsandconditions_editor');
		},
	});
	}
}
	//drop down values set for view
function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Materialrequisition/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});	
}
//retrieves salesorder detail based on so number elementspartabname
function getmoduledata(datarowid) 
{
	var elementsname = "accountid,currencyid,pricebookid";
	var elementstable = 'salesorder,salesorder,salesorder';//tablemae
	var elementscolmn = "accountid,currencyid,pricebookid";//tablefieldname
	var elementpartable = 'salesorder';
	$.ajax({
		url:base_url+"Materialrequisition/getsomoduledata?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) 
		{
			var txtboxname = elementsname ;
			var textboxname = {};
			textboxname = txtboxname.split(',');
			var dropdowns = [];
			textboxsetvaluenew(textboxname,textboxname,data,dropdowns);		
		}
	});
	//productdetailarray
	$.ajax({
		url:base_url+'Materialrequisition/getsoproductdetail?primarydataid='+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				loadinlinegriddata('materialrequisitionproaddgrid1',data.rows,'json');
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('materialrequisitionproaddgrid1');
				var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','materialrequisitiondetailid','uomdata'];
				gridfieldhide('materialrequisitionproaddgrid1',hideprodgridcol);
			}
		}
	});
	//summary calculate
	getsosummarydata(datarowid);	
}
//check valid connert
function checkvalidconvertpo(datarowid)
{
	var status='';
	$.ajax({
			url: base_url + "Materialrequisition/checkconvertpo?primarydataid="+datarowid,
			async:false,
			cache:false,
			success: function(msg)  {
			status =msg;
			},
			});
	return status;
}
function getsosummarydata(pid)
{
	$.ajax({
		url:base_url+"Materialrequisition/getsosummarydata?dataprimaryid="+pid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {		
			var textboxname =['6','summarygrossamount','summarydiscountamount','adjustmenttypeid','adjustmentvalue','summaryadjustmentamount','','summarydiscountamount'];
			var datanames =['6','summarygrossamount','summarydiscountamount','adjustmenttypeid','adjustmentvalue','adjustmentvalue','','summarydiscountamount'];
			var dropdowns=['1','adjustmenttypeid'];
			textboxsetvalue(textboxname,datanames,data,dropdowns);			
		}
	});
	summarynetamountcalc();
}
{// Generate icon after loads data - Workaround
	function generateuomiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline-block').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">network_cell</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');	
	}
	function generatemoduleiconafterload(fieldid,clickevnid){
		$("#"+fieldid+"").css('display','inline').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:relative;left:5px;top:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>'); 		
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	//Pricebook icon appending
	function generateiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline-block').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">library_books</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','block').css('width','90%');
		var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
	}
	//this is for the uom search icon-dynamic append
	function uomgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','inline-block').css('width','90%');
		var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
		$("#"+fieldid+"").after('<span class="" id="'+clickevnid+'" style="position:relative;left:5px;top:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
	}
}
	function modulenumbersearchgrid(moduleid,page,rowcount){
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#modulenumbersearchgrid').width();
		var wheight = $('#modulenumbersearchgrid').height();
		//col sort
		var sortcol = $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/getmoduledatanumber?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&moduleid="+moduleid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#modulenumbersearchgrid').empty();
				$('#modulenumbersearchgrid').append(data.content);
				$('#modulenumbersearchgridfooter').empty();
				$('#modulenumbersearchgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('modulenumbersearchgrid');
				{//sorting
					$('.modulenumberheadercolsort').click(function(){
						$('.modulenumberheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					sortordertypereset('modulenumberheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('#modulenumberpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
				}
			}
		});
	}
	function conversiondatamapping(datarowid,sourcemodule,destinatemodule){		
		if(sourcemodule == 217){
			frommodule = 'salesorder';
		}		
		$.ajax({
			url:base_url+"Base/getconversiondata", 
			type: "POST",
			data:{moduleid:sourcemodule,primaryid:datarowid,tomoduleid:destinatemodule,frommodule:frommodule},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {			
				resetconversiondata(data.main);
				var myArr = $.parseJSON(data.linedetail);				
				cleargriddata('materialrequisitionproaddgrid1');
				var gridlength = myArr.length;
				var j=0;
				for(var i=0;i< gridlength;i++){				
					addinlinegriddata('materialrequisitionproaddgrid1',j+1,myArr[i],'json');
					j++;
				}				
				setTimeout(function(){
					calculatesummarydetail();
				},80);	
				var tandc=data.main;
				var filename=tandc.invoicestermsandconditions_editorfilename;
				$("#termsandconditionid").select2('val','');
					if(checkValue(filename) == true){
						filenamevaluefetch(filename); //sets the source terms and condition
					}
				$('#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
				}							
		});
	}
	function productsearchgrid(page,rowcount) {
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#productsearchgrid').width();
		var wheight = $('#productsearchgrid').height();
		/* col sort */
		var sortcol = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').attr('id') : '0';
		var moduleid = 9;
		if(softwareindustryid == 1){
			moduleid = 9;
		}else if (softwareindustryid == 2){
			moduleid = 98;
		}else if (softwareindustryid == 3){
			moduleid = 90;
		}else if (softwareindustryid == 4){
			moduleid = 89;
		}
		var ordertype = 0;
		$.ajax({
			url:base_url+"Base/getproductsearch?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&ordertype="+ordertype+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+moduleid+'&industryid='+softwareindustryid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#productsearchgrid').empty();
				$('#productsearchgrid').append(data.content);
				$('#productsearchgridfooter').empty();
				$('#productsearchgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('productsearchgrid');
				{/* sorting */
					$('.productlistheadercolsort').click(function(){
						$('.productlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					sortordertypereset('productlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('#productlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						productsearchgrid(page,rowcount);
					});
				}
			},
		});
	}
	// Price book Overlay grid
	function pricebookgrid(page,rowcount) {
		var pid = $("#productid").val()!='' ? $("#productid").val() : 1;
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#pricebookgrid').width();
		var wheight = $('#pricebookgrid').height();
		/* col sort */
		var sortcol = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/pricebookview?maintabinfo=pricebookdetail&primaryid=pricebookdetailid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=249&productid='+pid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#pricebookgrid').empty();
				$('#pricebookgrid').append(data.content);
				$('#pricebookgridfooter').empty();
				$('#pricebookgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('pricebookgrid');
				/* hide grid column */
				var hideprbokcol = ['currencyid','pricebookdetailid'];
				gridfieldhide('pricebookgrid',hideprbokcol);
				{/* sorting */
					$('.pricebooklistheadercolsort').click(function(){
						$('.pricebooklistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					sortordertypereset('pricebooklistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('#pricebooklistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						pricebookgrid(page,rowcount);
					});
				}
			},
		});
	}
	/*
	*Calculates the discount live-single.
	*/
	function discount_total(){	
		setzero(['grossamount','discountpercent']); //sets zero on EMPTY					
		var typeid = $('#discounttypeid').val(); //discount typeid
		var value = $('#discountpercent').val();	//discount values		
		var grossamount = $('#grossamount').val();		
		//
		if(typeid != null && typeid !=''){			
			var discount_amount = parseFloat(value);
			if(typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
			
		} else {			
			var amt_discount =  0;
		}
		$('#singlediscounttotal').val(amt_discount);
	}
	/*
	*Calculates the group discount live-.
	*/
	function group_discount_total(){	
		setzero(['groupdiscountamount','summarygrossamount']); //sets zero on EMPTY					
		var typeid = $('#groupdiscounttypeid').val(); //discount typeid
		var value = $('#groupdiscountpercent').val();	//discount values		
		var grossamount = $('#summarygrossamount').val();		
		//
		if(typeid != null && typeid !=''){			
			var discount_amount = parseFloat(value);
			if(typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
			
		} else {			
			var amt_discount =  0;
		}
		$('#groupdiscounttotal').val(amt_discount);
	}
	function resetconversiondata(data){
		$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
		$.each( data, function( key, vale ) {
			if($('#'+key+'').hasClass('empdd')) {
				var ddval = '2:'+vale;
				$('#'+key+'ddid').select2("val",ddval);
				$('#'+key+'').val(vale);
			} else {
				var value = vale;				
				var type = $('#'+key+'').getType();
				if(type == "select") {
					if( $('#s2id_'+key+'').hasClass('select2-container-multi') ) {
						var mulval = value.split(',');
						$('#'+key+'').select2("val",mulval);
					} else {
						$('#'+key+'').select2("val",value);
					}
				} else if(type == "text") {
					$('#'+key+'').val(value);
				} else if(type == "hidden") {
					$('#'+key+'').val(value);
					if(value == 'Yes') {
						$('#'+key+'cboxid').prop('checked',true);
					} else {
						$('#'+key+'cboxid').prop('checked',false);
					}
				} else if(type == "textarea") {
					$('#'+key+'').val(value);
				}
			}
		});
	}
//currency value get
function appendcurrency(id){		
	$('#'+id+'').empty();
	$('#'+id+'').append($("<option></option>"));
	$.ajax({
		url: base_url+"index.php/Materialrequisition/specialcurrency",
		dataType:'json',
		async:false,
		success: function(data) {				
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-precision ='" +data[m]['decimal']+ "'  value = '" +data[m]['currencyid']+ "'>"+data[m]['currencyname']+"</option>";
				}
				$('#'+id+'').append(newddappend);
			}	
		},
	});
}
//product storage get - in stock 
function productstoragefetch(productid) {
	$.ajax({
		url:base_url+"Materialrequisition/productstoragefetchfun?productid="+productid,
		dataType : 'json',
		async:false,
		cache:false,
		success :function(data) {
			nmsg = $.trim(data);
			$("#instock").val(nmsg)
		},
	});
}
//currency rate set
function listprice(unitprice,rate){		
	var value = (parseFloat(unitprice)/(1/parseFloat(rate))).toFixed(PRECISION);		
	return value;
}