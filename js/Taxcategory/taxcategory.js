$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		taxcategoryaddgrid();
		firstfieldfocus();
		taxcategorycrudactionenable();
	}
	//hidedisplay
	$('#taxcategorydataupdatesubbtn,#gsttaxmodeiddivhid').hide();
	$('#taxcategorysavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			taxcategoryaddgrid();
		});
	}
	{
		//validation for Company Add  
		$('#taxcategorysavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#taxcategoryformaddwizard").validationEngine('validate');
				masterfortouch = 0;	
			}
		});
		jQuery("#taxcategoryformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#taxcategorysavebutton').attr('disabled','disabled'); 
				taxmasteraddformdata();
			},
			onFailure: function() {
				var dropdownid =['2','branchid','taxapplytypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$("#taxcategoryreloadicon").click(function(){
			clearform('cleardataform');
			taxcategoryrefreshgrid();
			$('#taxcategorydataupdatesubbtn').hide();
			$('#taxcategorysavebutton').show();
		});
		$( window ).resize(function() {
			innergridresizeheightset('taxcategoryaddgrid');
			sectionpanelheight('taxcategorysectionoverlay');
		});
	}
	{
		//update company information
		$('#taxcategorydataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {	
				$("#taxcategoryformeditwizard").validationEngine('validate');
			}
		});
		jQuery("#taxcategoryformeditwizard").validationEngine({
			onSuccess: function() {
				taxmasterupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','branchid','taxapplytypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				if(name == 'gsttax') {
					$('#gsttaxmodeiddivhid').show();
					$('#gsttaxmodeid').addClass('validate[required]');
				}
			}else{
				$('#'+name+'').val('No');
				if(name == 'gsttax') {
					$('#gsttaxmodeiddivhid').hide();
					$('#gsttaxmodeid').removeClass('validate[required]');
					$('#gsttaxmodeid').select2('val','');
				}
			}		
		});
	}	
	{//filter work
		//for toggle
		$('#taxcategoryviewtoggle').click(function() {
			if ($(".taxcategoryfilterslide").is(":visible")) {
				$('div.taxcategoryfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.taxcategoryfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#taxcategoryclosefiltertoggle').click(function(){
			$('div.taxcategoryfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#taxcategoryfilterddcondvaluedivhid").hide();
		$("#taxcategoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#taxcategoryfiltercondvalue").focusout(function(){
				var value = $("#taxcategoryfiltercondvalue").val();
				$("#taxcategoryfinalfiltercondvalue").val(value);
				$("#taxcategoryfilterfinalviewconid").val(value);
			});
			$("#taxcategoryfilterddcondvalue").change(function(){
				var value = $("#taxcategoryfilterddcondvalue").val();
				if( $('#s2id_taxcategoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					taxcategorymainfiltervalue=[];
					$('#taxcategoryfilterddcondvalue option:selected').each(function(){
					    var $tvalue =$(this).attr('data-ddid');
					    taxcategorymainfiltervalue.push($tvalue);
						$("#taxcategoryfinalfiltercondvalue").val(taxcategorymainfiltervalue);
					});
					$("#taxcategoryfilterfinalviewconid").val(value);
				} else {
					$("#taxcategoryfinalfiltercondvalue").val(value);
					$("#taxcategoryfilterfinalviewconid").val(value);
				}
			});
		}
		taxcategoryfiltername = [];
		$("#taxcategoryfilteraddcondsubbtn").click(function() {
			$("#taxcategoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#taxcategoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(taxcategoryaddgrid,'taxcategory');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//gst tax work
		if($("#gstapplicable").val() != 1) { // if gst not enabled
			$("#gsttaxdivhid").hide();
		} else { // if gst  enabled
			$("#gsttaxdivhid").show();
		}
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function taxcategoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $("#taxcategoryaddgrid").width();
	var wheight = $("#taxcategoryaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#taxcategorysortcolumn").val();
	var sortord = $("#taxcategorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.taxcategoryheadercolsort').hasClass('datasort') ? $('.taxcategoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.taxcategoryheadercolsort').hasClass('datasort') ? $('.taxcategoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.taxcategoryheadercolsort').hasClass('datasort') ? $('.taxcategoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#taxcategoryviewfieldids').val();
	if(userviewid != '') {
		userviewid= '74';
	}
	var filterid = $("#taxcategoryfilterid").val();
	var conditionname = $("#taxcategoryconditionname").val();
	var filtervalue = $("#taxcategoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=taxmaster&primaryid=taxmasterid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#taxcategoryaddgrid').empty();
			$('#taxcategoryaddgrid').append(data.content);
			$('#taxcategoryaddgridfooter').empty();
			$('#taxcategoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('taxcategoryaddgrid');
			innergridresizeheightset('taxcategoryaddgrid');
			{//sorting
				$('.taxcategoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.taxcategoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#taxcategorypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.taxcategoryheadercolsort').hasClass('datasort') ? $('.taxcategoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.taxcategoryheadercolsort').hasClass('datasort') ? $('.taxcategoryheadercolsort.datasort').data('sortorder') : '';
					$("#taxcategorysortorder").val(sortord);
					$("#taxcategorysortcolumn").val(sortcol);
					var sortpos = $('#taxcategoryaddgrid .gridcontent').scrollLeft();
					taxcategoryaddgrid(page,rowcount);
					$('#taxcategoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('taxcategoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					taxcategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#taxcategorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					taxcategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#taxcategoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#taxcategorypgrowcount').material_select();
		},
	});
}
function taxcategorycrudactionenable() {
	{//add icon click
		$('#taxcategoryaddicon').click(function(e){
			sectionpanelheight('taxcategorysectionoverlay');
			$("#taxcategorysectionoverlay").removeClass("closed");
			$("#taxcategorysectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			$("#branchid").prop('disabled',false);
			$("#gsttaxcboxid").prop('checked',false);
			$("#gsttax").val('No');
			$('#gsttaxmodeiddivhid').hide();
			$('#gsttaxmodeid').removeClass('validate[required]');
			$('#gsttaxmodeid').select2('val','');
			$("#setdefaultcboxid").prop('checked',false);
			$("#taxcategoryprimarydataid").val('');
			$("#setdefault").val('No');
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#taxcategorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#taxcategorysavebutton').show();
		});
	}
	$("#taxcategoryediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxcategoryaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			taxmastergetformdata(datarowid);
			$("#branchid").prop('disabled',true);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
			//Keyboard short cut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#taxcategorydeleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxcategoryaddgrid div.gridcontent div.active').attr('id');
		var parenttaxnamenameid = getgridcolvalue('taxcategoryaddgrid',datarowid,'taxmasterid','');
		var parentcatval=$(parenttaxnamenameid).text().length;
		if(datarowid){		
		$("#taxcategoryprimarydataid").val(datarowid);
			
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=tax&fieldid=taxmasterid&table1=''&fieldid1=''&table2=''&fieldid2=''",
					success: function(msg) { 
						if(msg > 0){
							alertpopup("This tax category already mapped into tax name.So unable to delete this one");						
						}
							else{
				combainedmoduledeletealert('taxcategorydeleteyes');
							
								$("#taxcategorydeleteyes").click(function(){
									var datarowid = $("#taxcategoryprimarydataid").val();
									taxmasterrecorddelete(datarowid);
									$(this).unbind();
								});
					   }
					},
				});
		}
		else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function taxcategoryrefreshgrid() {
		var page = $('ul#taxcategorypgnum li.active').data('pagenum');
		var rowcount = $('ul#taxcategorypgnumcnt li .page-text .active').data('rowcount');
		taxcategoryaddgrid(page,rowcount);
	}
}
//new data add submit function
function taxmasteraddformdata(){
    var formdata = $("#taxcategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax(    {
        url: base_url + "Taxcategory/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				var def = $("#setdefault").val();
				if(def == 'Yes') {
					$('.singlecheckid').trigger('click');
				}
				resetFields();
				taxcategoryrefreshgrid();
				$("#taxcategorysavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#taxcategorysectionoverlay").removeClass("effectbox");
		$("#taxcategorysectionoverlay").addClass("closed");
	});
}
//old information show in form
function taxmastergetformdata(datarowid){
	var taxmasterelementsname = $('#taxcategoryelementsname').val();
	var taxmasterelementstable = $('#taxcategoryelementstable').val();
	var taxmasterelementscolmn = $('#taxcategoryelementscolmn').val();
	var taxmasterelementpartable = $('#taxcategoryelementspartabname').val();
	$.ajax( {
		url:base_url+"Taxcategory/fetchformdataeditdetails?taxmasterprimarydataid="+datarowid+"&taxmasterelementsname="+taxmasterelementsname+"&taxmasterelementstable="+taxmasterelementstable+"&taxmasterelementscolmn="+taxmasterelementscolmn+"&taxmasterelementpartable="+taxmasterelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				clearinlinesrchandrgrid('taxcategoryaddgrid');
			} else {
				sectionpanelheight('taxcategorysectionoverlay');
				$("#taxcategorysectionoverlay").removeClass("closed");
				$("#taxcategorysectionoverlay").addClass("effectbox");
				var txtboxname = taxmasterelementsname + ',taxcategoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = taxmasterelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var checkarray=data['setdefault'];
				setthecheckedvalue(checkarray);
				var id=data['primarydataid'];
				$("#taxcategoryprimarydataid").val(id);
				var description=data['description'];
				$("#description").text(description);
				if(data['gsttax'] == 'Yes') {
					$('#gsttaxmodeiddivhid').show();
					$('#gsttaxmodeid').addClass('validate[required]');
				}else{
					$('#gsttaxmodeiddivhid').hide();
					$('#gsttaxmodeid').removeClass('validate[required]');
				}		
			}
		}
	});
	$('#taxcategorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#taxcategorysavebutton').hide();	
}
//update old information
function taxmasterupdateformdata() {
	var formdata = $("#taxcategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Taxcategory/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE')  {
            	$(".addsectionclose").trigger("click");
				var def = $("#setdefault").val();
				if(def == '1') {
					$('.singlecheckid').trigger('click');
				}
				$('#taxcategorydataupdatesubbtn').hide();
				$('#taxcategorysavebutton').show();
				resetFields();
				taxcategoryrefreshgrid();
				alertpopup(savealert);
				//For Touch work  to change to add state
				masterfortouch = 0;
				//Keyboard shortcut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function taxmasterrecorddelete(datarowid){
	var taxmasterelementstable = $('#taxcategoryelementstable').val();
	var taxmasterelementspartable = $('#taxcategoryelementspartabname').val();
    $.ajax({
        url: base_url + "Taxcategory/deleteinformationdata?taxmasterprimarydataid="+datarowid+"&taxmasterelementstable="+taxmasterelementstable+"&taxmasterelementspartable="+taxmasterelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	taxcategoryrefreshgrid();
				$("#taxmasterid").val("");
            	$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            }  else if (nmsg == "false")  {
            	taxcategoryrefreshgrid();
            }
        },
    });
}
//tax  category name 
function taxcatnamecheck(){
	var primaryid = $("#taxcategoryprimarydataid").val();
	var accname = $("#taxmastername").val();
	var elementpartable = 'taxmaster';
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Tax category name already exists!";
				}
			}else {
				return "Tax category name already exists!";
			}
		} 
	}
}
//tax category data reload
function taxcategorydatareload() {
	$('#taxmasterid').empty();
	$('#taxmasterid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Taxcategory/fetchdddataviewddval?dataname=taxmastername&dataid=taxmasterid&datatab=taxmaster',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#taxmasterid')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
				$.each(data, function(index) {
					if(data[index]['setdefault'] == 'Yes'){
						$('#taxmasterid').select2('val',data[index]['datasid']).trigger('change');
					}
				});
			}
		},
	});
}