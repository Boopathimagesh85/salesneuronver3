$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		invoicelistgrid();
		addonslistgrid();
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 3;
	}
	$(".subscriptionmanagerform").css("opacity",1);
	$('#groupcloseaddform').addClass('hidedisplay');
	$('#billedit').removeClass('hidedisplay');
	$('#planupgrade').removeClass('hidedisplay');
	$('#cancelaccount').removeClass('hidedisplay');
	$('#usersubmitbtn').show();
	$('#userupdatebtn').hide();
	//tab click events
	$("#tab1").click(function()	{
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
	});
	$("#tab2").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
		invoicelistgrid();
		Materialize.updateTextFields();
	});
	$("#tab3").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
		addonslistgrid();
		Materialize.updateTextFields();
	});
	$(window).resize(function() {
		innergridresizeheightset('invoicelistgrid');
		innergridresizeheightset('addonslistgrid');
	});
	setTimeout(function(){
		var planamount = $('#plancost').val();
		var demodatas = $('#demodata').val();
		if(planamount!=0 && planamount!='' && demodatas=='No') {
			$('#planediticon').show();
		}
	},100);
	//plan upgrade
	$('#planupgrade').click(function(){
		var chk = $('#check').val();
		var url = $('#url').val();
		var info = $('#customerinfo').val();
		if(chk!='') {
			window.location=url+'?sets='+info;
		}
	});
	//addon info
	$('#addonbuyicon').click(function(){
		var chk = $('#check').val();
		var url = $('#url').val();
		var info = $('#editcustomerinfo').val();
		if(chk!='') {
			window.location=url+'?sets='+info;
		}
	});
	//reload invoice details
	$('#reloadicon').click(function(){
		refreshinvoicegrid();
	});
	//reload invoice details
	$('#addonreloadicon').click(function(){
		refreshaddonsgrid();
	});
	//disable forms
	$('#subscriptioninformationform :input').attr('disabled', true);
	$('#subscripbillinfo :input').attr('disabled', true);
	//edit bill details info click event
	$('#billedit').click(function(){
		$('#tab1').trigger('click');
		$('#subscripbillinfo :input').attr('disabled', false);
		$('.billdetailmand').removeClass('hidedisplay');
		$('#billdetupdatediv').show();
		$('#billpersonname').focus();
	});
	//bill details edit cancel
	$('#billdetailsupdatecancel').click(function(){
		getbillingdetails();
		Materialize.updateTextFields();
		$('#subscripbillinfo :input').attr('disabled',true);
		setTimeout(function(){
			$('.cleardataform').validationEngine('hideAll');
			$('.cleardataform .select2-choice').removeClass('error');
			$('.cleardataform :input').removeClass('error');
		},50);
		$('.billdetailmand').addClass('hidedisplay');
		$('#billdetupdatediv').hide();
	});
	//update billing details
	$('#billdetailsupdate').on('click',function(e){
		if(e.which == 1 || e.which === undefined) {
			$('#subscripbillinfovalidate').validationEngine('validate');
		}
	});
	$('#subscripbillinfovalidate').validationEngine({
		onSuccess: function() {
			billdetailsdataupdatefun();
			Materialize.updateTextFields();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//cancel account
	$('#cancelaccount').click(function(){
		$('#cancelaccconfirmoverlay').fadeIn(200);
	});
	//close cancel account confirmation
	$('#cancelaccno').click(function(){
		$('#cancelaccconfirmoverlay').fadeOut();
	});
	//cancel account confirmation
	$('#cancelaccyes').click(function(){
		clearform('accountcancelclearform');
		$('#cancelaccconfirmoverlay').fadeOut();
		$('#subscriptioncanceloverlay').fadeIn(200);
	});
	//account cancel overlay close
	$('#subscripoverlayclose').click(function(){
		$('#subscriptioncanceloverlay').fadeOut();
	});
	//cancel account
	$('#cancelaccbtn').click(function(e){
		if(e.which == 1 || e.which === undefined) {
			$('#accountcanceldatasetsformvalidate').validationEngine('validate');
		}
	});
	$('#accountcanceldatasetsformvalidate').validationEngine({
		onSuccess: function() {
			cancelaccount();
		},
		onFailure: function() {
			var dropdownid =[1,'cancelaccreason'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//cancel account reset btn
	$('#cancelaccnobtn').click(function(){
		clearform('accountcancelclearform');
	});
	//plan edit events
	$('#planediticon').click(function(){
		var duration = $('#editplanduration').val();
		if(duration==12) {
			$('#editplanduration').attr('readonly',true);
		} else {
			$('#editplanduration').attr('readonly',true);
		}
		editplantotalamountcalculation();
		$('#planeditoverlay').fadeIn(200);
	});
	//Plan duration change events
	$('#editplanduration').change(function(){
		var duration = $('#editplanduration').val();
		$('#plandurations').val(duration);
		editplantotalamountcalculation();
	});
	//users,cost change events
	$('#editplanusers,#plancosts').change(function(){
		editplantotalamountcalculation();
	});
	//plan edit overlay close
	$('#planeditoverlayclose').click(function(){
		$('#planeditoverlay').fadeOut();
	});
	//plan edit cancel
	$('#cancelplanupdate').click(function(){
		$('#planeditoverlay').fadeOut();
	});
	//edit plan update
	$('#planupdatebtn').click(function(e){
		if(e.which == 1 || e.which === undefined) {
			$('#editplanoverlayvalidate').validationEngine('validate');
		}
	});
	$('#editplanoverlayvalidate').validationEngine({
		onSuccess: function() {
			var curid = $('#currencyid').val();
			if(curid=='2') {
				$('#instsubmit').trigger('click');
			} else {
				$('#submit').trigger('click');
			}
		},
		onFailure: function() {
			var dropdownid =[1,'cancelaccreason'];
			dropdownfailureerror(dropdownid);
		}
	});
	//edit plan instamojo validate
	$('#instplanupdatebtn').click(function(e){
		if(e.which == 1 || e.which === undefined) {
			$('#editplanoverlayvalidate').validationEngine('validate');
		}
	});
	$('#editplanoverlayvalidate').validationEngine({
		onSuccess: function() {
		},
		onFailure: function() {
			var dropdownid =[1,'cancelaccreason'];
			dropdownfailureerror(dropdownid);
		}
	});
	{
		//allow only numbers in user text box
		$('#editplanusers').keypress(function (e) {
			var len = $(this).val().length;
			var specialKeys = new Array();
			specialKeys.push(8); //Backspace
			specialKeys.push(9); //Tab
			specialKeys.push(46); //Delete
			specialKeys.push(36); //Home
			specialKeys.push(35); //End
			specialKeys.push(37); //Left
			specialKeys.push(39);
			var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
			if(len==0) {
				var ret = ((keyCode >= 49 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			} else {
				var ret = ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			}
			if (ret) {
				return true;
			}
			e.preventDefault();
			return false;
		});
	}
	{//download invoice click icon..
		$('#invoicedownicon').click(function(){
			var rowid = $('#invoicelistgrid div.gridcontent div.active').attr('id');
			if(rowid) {
				var form = $('<form action="'+base_url+'datamail.php" class="hidedisplay" name="invoicedownload" id="invoicedownload" method="POST" target="_blank"><input type="text" name="invoiceid" id="invoiceid" value="'+rowid+'"/><input type="text" name="mailtype" id="mailtype" value="fdownload"/><input type="text" name="filetype" id="filetype" value="application/pdf"/></form>');
				$(form).appendTo('body');
				form.submit();
			} else {
				alertpopup('Please select a row');
			}
		});
	}
	{//clear demo data
		$("#clearicon").click(function(){
			combinedmodalertdynamictxt('cleardatabtn','Are you sure to clean demo data?');
			$("#cleardatabtn").click(function(){
				$('#basedeleteoverlayforcommodule').hide();
				$("#processoverlay").show();
				$.ajax({
					url:base_url+"Subscriptionmanager/cleardemodata",
					type:'POST',
					async:false,
					cache:false,
					success:function(data) {
						var nmsg =  $.trim(data);
						if(nmsg == 'success') {
							$('#basedeleteoverlayforcommodule').fadeOut();
							alertpopup('Your demo data is cleared successfully.');
						} else {
							$('#basedeleteoverlayforcommodule').fadeOut();
							alertpopup('Your demo data is not cleared.');
						}
					}
				});
				$("#processoverlay").hide();
			});
		});
	}
});
//Invoice List Grid
function invoicelistgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
	var wwidth = $('#invoicelistgrid').width();
	var wheight = $('#invoicelistgrid').height();
	/*col sort*/
	var sortcol = $('.invoiceheadercolsort').hasClass('datasort') ? $('.invoiceheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.invoiceheadercolsort').hasClass('datasort') ? $('.invoiceheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.invoiceheadercolsort').hasClass('datasort') ? $('.invoiceheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Subscriptionmanager/paymentinvoicedetails?maintabinfo=salesneuronpaymenthistory&primaryid=salesneuronpaymenthistoryid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=5',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#invoicelistgrid').empty();
			$('#invoicelistgrid').append(data.content);
			$('#invoicelistgridfooter').empty();
			$('#invoicelistgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('invoicelistgrid');
			{//sorting
				$('.invoiceheadercolsort').click(function(){
					$('.invoiceheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#invoicepgnumcnt li .page-text .active').data('rowcount');
					invoicelistgrid(page,rowcount);
				});
				sortordertypereset('invoiceheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					invoicelistgrid(page,rowcount);
				});
				$('#invoicepgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					productgrid(page,rowcount);
				});
			}
			//Material select
			$('.pagedropdown').material_select();
		},
	});
}
{//refresh grid
	function refreshinvoicegrid() {
		var page = $('ul#invoicepgnum li.active').data('pagenum');
		var rowcount = $('ul#invoicepgnumcnt li .page-text .active').data('rowcount');
		invoicelistgrid(page,rowcount);
	}
}
//Addons list & buy Grid
function addonslistgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#addonslistgrid').width();
	var wheight = $('#addonslistgrid').height();
	/*col sort*/
	var sortcol = $('.addonslistheadercolsort').hasClass('datasort') ? $('.addonslistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.addonslistheadercolsort').hasClass('datasort') ? $('.addonslistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.addonslistheadercolsort').hasClass('datasort') ? $('.addonslistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Subscriptionmanager/addonsinvoicedetails?maintabinfo=salesneuronpaymentdetails&primaryid=salesneuronpaymentdetailsid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=5',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#addonslistgrid').empty();
			$('#addonslistgrid').append(data.content);
			$('#addonslistgridfooter').empty();
			$('#addonslistgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('addonslistgrid');
			{//sorting
				$('.addonslistheadercolsort').click(function(){
					$('.addonslistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#addonslistpgnumcnt li .page-text .active').data('rowcount');
					addonslistgrid(page,rowcount);
				});
				sortordertypereset('addonslistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#addonslistpgnumcnt li .page-text .active').data('rowcount');
					addonslistgrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#addonslistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					addonslistgrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#addonslistpgnumcnt li .page-text .active').data('rowcount');
					addonslistgrid(page,rowcount);
				});
				$('#invoicepgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#addonslistpgnum li.active').data('pagenum');
					addonslistgrid(page,rowcount);
				});
				//Material select
				$('.pagedropdown').material_select();
			}
		},
	});
}
{//refresh grid
	function refreshaddonsgrid() {
		var page = $('ul#addonslistpgnum li.active').data('pagenum');
		var rowcount = $('ul#addonslistpgnumcnt li .page-text .active').data('rowcount');
		addonslistgrid(page,rowcount);
	}
}
//get billing details
function getbillingdetails() {
	$.ajax({
		url:base_url+'Subscriptionmanager/getbilldetails',
		type:'POST',
		dataType:'json',
		cache:false,
		async:false,
		success: function(data) {
			var textboxname = ['billpersonname','billmobilenum','billaddr','billcity','billzip','billstate','billcountry'];
			var dropdowns = [];
			textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
		},
	});
}
//update billing information
function billdetailsdataupdatefun() {
	var amp = '&';
	var formdata = $('#subscripbillinfo').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Subscriptionmanager/billdetailsupdate',
		data:'datas='+datainformation,
		type:'POST',
		cache:false,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				alertpopup(savealert);
				$('#subscripbillinfo :input').attr('disabled',true);
				$('.billdetailmand').addClass('hidedisplay');
				$('#billdetupdatediv').hide();
			} else if (nmsg == 'false') {
				$('#subscripbillinfo :input').attr('disabled',true);
				$('.billdetailmand').addClass('hidedisplay');
				$('#billdetupdatediv').hide();
			}
		},
	});
}
//cancel account
function cancelaccount() {
	var amp = '&';
	var formdata = $('#accountcanceldatasetsform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Subscriptionmanager/cancelaccount',
		data:'datas='+datainformation,
		type:'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				console.clear();
				clearform('accountcancelclearform');
				$('#subscriptioncanceloverlay').fadeOut();
				alertpopup('Your account cancelled successfully.');
				setTimeout(function(){
					window.location=base_url+'Login/logout';
				},1000);
			} else {
				console.clear();
				clearform('accountcancelclearform');
				$('#subscriptioncanceloverlay').fadeOut();
				alertpopup(msg);
			}
		},
	});
}
//validate users in edit plan
function userplanvalidate() {
	var users = parseInt($('#planusercount').val());
	var editusers = parseInt($('#editplanusers').val());
	if( users >= editusers ) {
		$('#instplanupdatebtn').show();
		$('#instspan').html('');
		return "You can buy more than "+users+" users only.";
	}
}
//calculate total amount
function editplantotalamountcalculation() {
	var user = (($('#editplanusers').val()!='')?$('#editplanusers').val():0);
	var cost = (($('#plancosts').val()!='')?$('#plancosts').val():0);
	var duration = (($('#editplanduration').val()!='')?$('#editplanduration').val():1);
	//pro rata calculation.
	var actdate = $('#editplanactdate').val();
	var expdate = $('#editplanexpdate').val();
	var adate = actdate.split(' ');
	var edate = expdate.split(' ');
	var startDay = new Date(adate[0].split('-').join(','));
	var endDay = new Date(edate[0].split('-').join(','));
	var millisecondsPerDay = 1000 * 60 * 60 * 24;
	var millisBetween = endDay.getTime() - startDay.getTime();
	var activationdays = millisBetween / millisecondsPerDay;
	var daysleft = $('#editplandaysleft').val();
	var useddays = activationdays-daysleft;
	var actduration = $('#editplandur').val();
	var extusercount = $('#planusercount').val();
	//set information to payment form
	$("#paymentduration,#instpaymentduration").val(actduration);
	//cost calculation
	var daycost = parseFloat((cost*actduration)/activationdays).toFixed(2);
	var newusercount = parseInt(user)-parseInt(extusercount);
	var newusercost = parseFloat(daycost)*parseInt(daysleft);
	//set information to payment form
	$("#li_0_quantity,#instli_0_quantity").val(newusercount);
	$("#li_0_price,#instli_0_price").val(newusercost.toFixed(2));
	//Total amount calculation
	var totalamount = parseFloat(parseFloat(newusercost)*parseInt(newusercount));
	$('#totaleditplancost').text(totalamount.toFixed(2));
	$('#instsubscripetotalamount').val(totalamount.toFixed(2));
	if($('#instsubscripetotalamount').val()>'0.00') {
		updateinstpaymentlink();
	}
}
//update instamojo pay link
function updateinstpaymentlink() {
	$('#instplanupdatebtn').hide();
	var payurl = $('#instamojourl').val();
	var paydatasets = $("#instamojodataform").serialize();
	paydatasets = decodeURIComponent(paydatasets);
	var hreflink = payurl+'?'+paydatasets;
	var instlinks = '<a href="'+hreflink+'" rel="im-checkout" data-behavior="remote" data-style="light" data-text="Update Plan" data-token="9a4f982b5a7cf96cfe5563f2219fd296" id="instaurl"></a>';
	$('#instspan').html(instlinks);
	$.getScript('https://d2xwmjc4uy2hr5.cloudfront.net/im-embed/im-embed.min.js', function(){ });
}