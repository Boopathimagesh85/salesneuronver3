$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		currencyaddgrid();
		firstfieldfocus();
		currencycrudactionenable();
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			currencyaddgrid();
		});
	}
	//hidedisplay
	$('#currencydataupdatesubbtn').hide();
	$('#currencysavebutton').show();
	{//keyboard shortcut reset global variable
		viewgridview=0;
		addformview=0;
	}
	{//validation for Company Add  
		$('#currencysavebutton').click(function() {
			masterfortouch = 0;		
			$("#currencyformaddwizard").validationEngine('validate');
		});
		jQuery("#currencyformaddwizard").validationEngine( {
			onSuccess: function() { 
				currencyaddformdata();
			},
			onFailure: function() {
				var dropdownid =['1','currencyformattypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$("#currencyreloadicon").click(function(){
			clearform('cleardataform');
			currencyrefreshgrid();
			$('#currencydataupdatesubbtn').hide();
			$('#currencysavebutton').show();
			$("#currencydeleteicon").show();
		});
		$( window ).resize(function() {
			innergridresizeheightset('currencyaddgrid');
			sectionpanelheight('currencysectionoverlay');
		});
	}
	{//update company information
		$('#currencydataupdatesubbtn').click(function() {
			$("#currencyformeditwizard").validationEngine('validate');
		});
		jQuery("#currencyformeditwizard").validationEngine({
			onSuccess: function()
			{
				currencyupdateformdata();
			},
			onFailure: function()
			{
				var dropdownid =['1','currencyformattypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	{//filter work
		//for toggle
		$('#currencyviewtoggle').click(function() {
			if ($(".currencyfilterslide").is(":visible")) {
				$('div.currencyfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.currencyfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#currencyclosefiltertoggle').click(function(){
			$('div.currencyfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#currencyfilterddcondvaluedivhid").hide();
		$("#currencyfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#currencyfiltercondvalue").focusout(function(){
				var value = $("#currencyfiltercondvalue").val();
				$("#currencyfinalfiltercondvalue").val(value);
			});
			$("#currencyfilterddcondvalue").change(function(){
				var value = $("#currencyfilterddcondvalue").val();
				if( $('#s2id_currencyfilterddcondvalue').hasClass('select2-container-multi') ) {
					currencymainfiltervalue=[];
					$('#currencyfilterddcondvalue option:selected').each(function(){
					    var $currencyvalue =$(this).attr('data-ddid');
					    currencymainfiltervalue.push($currencyvalue);
						$("#currencyfinalfiltercondvalue").val(currencymainfiltervalue);
					});
					$("#currencyfilterfinalviewconid").val(value);
				} else {
					$("#currencyfinalfiltercondvalue").val(value);
					$("#currencyfilterfinalviewconid").val(value);
				}
			});
		}
		currencyfiltername = [];
		$("#currencyfilteraddcondsubbtn").click(function() {
			$("#currencyfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#currencyfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(currencyaddgrid,'currency');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function currencyaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#currencyaddgrid").width();
	var wheight = $("#currencyaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#currencysortcolumn").val();
	var sortord = $("#currencysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.currencyheadercolsort').hasClass('datasort') ? $('.currencyheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.currencyheadercolsort').hasClass('datasort') ? $('.currencyheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.currencyheadercolsort').hasClass('datasort') ? $('.currencyheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#currencyviewfieldids').val();
	if(userviewid != '') {
		userviewid= '78';
	}
	var filterid = $("#currencyfilterid").val();
	var conditionname = $("#currencyconditionname").val();
	var filtervalue = $("#currencyfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=currency&primaryid=currencyid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#currencyaddgrid').empty();
			$('#currencyaddgrid').append(data.content);
			$('#currencyaddgridfooter').empty();
			$('#currencyaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('currencyaddgrid');
			{//sorting
				$('.currencyheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.currencyheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#currencypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.currencyheadercolsort').hasClass('datasort') ? $('.currencyheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.currencyheadercolsort').hasClass('datasort') ? $('.currencyheadercolsort.datasort').data('sortorder') : '';
					$("#currencysortorder").val(sortord);
					$("#currencysortcolumn").val(sortcol);
					var sortpos = $('#currencyaddgrid .gridcontent').scrollLeft();
					currencyaddgrid(page,rowcount);
					$('#currencyaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('currencyheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					currencyaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});		
				$('#currencypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					currencyaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#currencyaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#currencypgrowcount').material_select();
		},
	});
}
function currencycrudactionenable() {
	{//add icon click
		$('#currencyaddicon').click(function(e){
			sectionpanelheight('currencysectionoverlay');
			$("#currencysectionoverlay").removeClass("closed");
			$("#currencysectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#currencydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#currencysavebutton').show();
		});
	}
	$("#currencyediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#currencyaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$("#currencydeleteicon").hide();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			currencygetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;		
			//Keyboard shortcut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#currencydeleteicon").click(function(e){
		var datarowid = $('#currencyaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			clearform('cleardataform');
			$("#currencyprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('currencydeleteyes');
			$("#currencydeleteyes").click(function(){
				var datarowid = $("#currencyprimarydataid").val();
				currencyrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function currencyrefreshgrid() {
		var page = $('ul#currencypgnum li.active').data('pagenum');
		var rowcount = $('ul#currencypgnumcnt li .page-text .active').data('rowcount');
		currencyaddgrid(page,rowcount);
	}
}
//new data add submit function
function currencyaddformdata() {
    var formdata = $("#currencyaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Currency/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$(".addsectionclose").trigger("click");
				currencyrefreshgrid();
				$("#currencysavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#currencysectionoverlay").removeClass("effectbox");
		$("#currencysectionoverlay").addClass("closed");
	});
}
//old information show in form
function currencygetformdata(datarowid) {
	var currencyelementsname = $('#currencyelementsname').val();
	var currencyelementstable = $('#currencyelementstable').val();
	var currencyelementscolmn = $('#currencyelementscolmn').val();
	var currencyelementpartable = $('#currencyelementspartabname').val();
	$.ajax( {
		url:base_url+"Currency/fetchformdataeditdetails?currencyprimarydataid="+datarowid+"&currencyelementsname="+currencyelementsname+"&currencyelementstable="+currencyelementstable+"&currencyelementscolmn="+currencyelementscolmn+"&currencyelementpartable="+currencyelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$("#currencydeleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				clearinlinesrchandrgrid('currencyaddgrid');
				//For Touch
				smsmasterfortouch = 0;
			} else {
				$("#currencycountry,#currencyname,#currencycode,#symbol").attr('readonly',true)
				sectionpanelheight('currencysectionoverlay');
				$("#currencysectionoverlay").removeClass("closed");
				$("#currencysectionoverlay").addClass("effectbox");
				var txtboxname = currencyelementsname + ',currencyprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = currencyelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
		}
	});
	$('#currencydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#currencysavebutton').hide();
	
}
//update old information
function currencyupdateformdata() {
	var formdata = $("#currencyaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Currency/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
				$('#currencydataupdatesubbtn').hide();
				$('#currencysavebutton').show();
				$("#currencydeleteicon").show();
				resetFields();
				$(".addsectionclose").trigger("click");
				currencyrefreshgrid();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;	
				//Keyboard shortcut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function currencyrecorddelete(datarowid) {
	var attributeelementstable = $('#currencyelementstable').val();
	var attributeelementspartable = $('#currencyelementspartabname').val();
    $.ajax({
        url: base_url + "Currency/deleteinformationdata?attributeprimarydataid="+datarowid+"&attributeelementstable="+attributeelementstable+"&attributeelementspartable="+attributeelementspartable,
		cache:false,
        success: function(msg)  {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	currencyrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "false") {
            	currencyrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//Currency name check
function currencycodecheck() {
	var primaryid = $("#currencyprimarydataid").val();
	var accname = $("#currencycode").val();
	var elementpartable = $('#currencyelementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Currency Code already exists!";
				}
			}else {
				return "Currency Code already exists!";
			}
		} 
	}
}