$(document).ready(function() {	
	{// Foundation Initialization
        $(document).foundation();
		// Maindiv height width change
        maindivwidth();
		round =3;
    }
	{// Grid Calling
        stockmanagementaddgrid();
        //crud action
    	crudactionenable();
	}	
	generatemoduleiconafterload('purchaseorderid','purchaseordernumberevent');
	productgenerateiconafterload('productid','productsearchevent');	
	generateuomiconafterload('quantity','uomclickevent','quantitydivhid');
	$("#purchaseorderdate").datepicker("disable");
	{// Formclear resetting 
		$("#formclearicon").click(function(){
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		});
	}
	{
		//Tag Templates
		$("#tagtemplateicon").click(function(){
			window.location =base_url+'Tagtemplate';
		});
	}
	{// Toolbar actions
		//reload icon
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('stockmanagementaddgrid');
		});
		//detail view icon
		$("#detailedviewicon").click(function() {
			var datarowid = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$("#formclearicon").hide(); 
				$("#quantity").attr('readonly',true);
				addslideup('stockmanagementcreationview','stockmanagementcreationformadd');
				stockeditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','stockmanagementcreationformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					$(".addbtnclass").addClass('hidedisplay');
					$(".updatebtnclass").removeClass('hidedisplay');
					$("#formclearicon").hide(); 
					$("#quantity").attr('readonly',true);
					addslideup('stockmanagementcreationview','stockmanagementcreationformadd');
					stockeditdatafetchfun(rdatarowid);
					Materialize.updateTextFields();
					showhideiconsfun('summryclick','stockmanagementcreationformadd');
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					},50);
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		//edit summery icon click
		$("#editfromsummayicon").click(function() {			
			var datarowid = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				showhideiconsfun('editfromsummryclick','stockmanagementcreationformadd');
				$(".froala-element").css('pointer-events','auto');
			}
		});
	}	
    {// Close Add Screen
        var addcloseacccreation =["closeaddform","stockmanagementcreationview","stockmanagementcreationformadd"]
        addclose(addcloseacccreation);
		var addcloseviewcreation =["viewcloseformiconid","purchaseordercreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
    }
	{// View by drop down change
        $('#dynamicdddataview').change(function() {
            stockmanagementaddgrid();
        });
    }
	{
		if(softwareindustryid == 4){
			var ordertype = $('#producthide').val();
			if(ordertype){
				$.ajax({
					url:base_url+"Base/getorderbasedproduct?ordertypeid="+ordertype+"&producthide=Yes",
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$('#productid').empty();
						prev = ' ';
						var optclass = [];
						$.each(data, function(index) {
							var cur = data[index]['PId'];
							if(prev != cur ) {
								if(prev != " ") {
									$('#productid').append($("</optgroup>"));
								}
								if(jQuery.inArray(data[index]['optclass'], optclass) == -1) {
									optclass.push(data[index]['optclass']);
									$('#productid').append($("<optgroup  label='"+data[index]['optclass']+"' class='"+data[index]['optclass']+"'>")); 
								} 
								$("#productid optgroup[label='"+data[index]['optclass']+"']")
    							.append($("<option></option>")
    							.attr("class",data[index]['optionclass'])
    							.attr("value",data[index]['value'])
    							.attr("data-productidhidden",data[index]['data-productidhidden'])
    							.text(data[index]['data-productidhidden']));
								prev = data[index]['PId'];
							}
							
						}); 
						$('#productid').select2('val','').trigger("change");
					}
				});
			}
		}
	}
	{// Validation for salesorder Add  
		$('#dataaddsbtn').click(function(e){
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				stockmanagementcreate();						
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Update salesorder information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				stockmanagementupdate();				
			},
			onFailure: function() {
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{	//po dd change
		$("#purchaseorderid").change(function() {
			var poid = $("#purchaseorderid").val();
			purchaseorderdatafetch(poid);
			pobasedproduct(poid);
		});
		// PO grid overlay	show
		$("#purchaseordernumberevent").click(function() {
			$('#posearchoverlay').fadeIn();
			var poid = 225;
			if(softwareindustryid == 1){
				poid = 225;
			}else if(softwareindustryid == 2){
				poid = 99;
			}else if(softwareindustryid == 4){
				poid = 88;
			}else{
				poid = 225;
			}
			ponumbersearchgrid(poid);
				
		});
		//po overlay close
		$("#posearchclose").click(function() {
			$('#posearchoverlay').fadeOut();			
		});
		//po overlay submit
		$("#posearchsubmit").click(function() {
			var datarowid = $('#ponumbersearchgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var number = getgridcolvalue('ponumbersearchgrid',datarowid,'purchaseordernumber','');
				$('#purchaseorderid').select2('val',datarowid);
				$("#purchaseorderhideidid").val(datarowid);
				$('#posearchoverlay').fadeOut('slow');
				purchaseorderdatafetch(datarowid);
				cleargriddata('ponumbersearchgrid');
			} else {
				alertpopup('Please select row');
			}
		});
	} 
	{ // product search overlay
		$("#productsearchevent").click(function() {	
			$("#productsearchoverlay").fadeIn();
			sproductsearchgrid();
		});
		//product overlay close
		$("#productsearchclose").click(function() {
			$("#productsearchoverlay").fadeOut();
		});
		//product overlay submit
		$("#productsearchsubmit").click(function() {
			var selectedrow = $('#sproductsearchgrid div.gridcontent div.active').attr('id');
			if(selectedrow){
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
				productbaseddatafetch(selectedrow);	
			}			
		});
		$("#uomclickevent").click(function() {
			if(checkVariable('productid') == true) {
				$("#uomconvoverlay").fadeIn();
			} else {
				alertpopup("Kindly Choose Product to Load UOM");
			}
		});		
		$('#uomconv_submit').click(function(e){
			$("#uomconv_validate").validationEngine('validate');			
		});
		jQuery("#uomconv_validate").validationEngine({
			onSuccess: function() {	
				$("#uomconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
		$("#uomconvclose").click(function() {
			$("#uomconvoverlay").fadeOut();
		});	
	}
	//product dd change
	$("#productid").change(function() {
		var pid = $("#productid").val();
		var poid = $("#purchaseorderid").val();
		if(pid){
			productbaseddatafetch(pid,poid);
		}else{
			$('#batchid').empty();
			$('#quantity').val('');
			$('#purchaseprice').val('');
		}		
		Materialize.updateTextFields();
	});	
	//uom conversion rate fetch
	$("#touomid").change(function() {
		var touomid = $("#touomid").val();
		if(touomid != null){			
			var conrate = $('option:selected', this).attr('data-conrate');	
			$("#conversionquantity").val(1);
			$("#quantity").val(1);
			$('#uomconversionrate').val(conrate).trigger('focusout');
			Materialize.updateTextFields();
		}	
	});	
	//weight and price focus out 
	$("#weight,#purchaseprice").focusout(function() {
		var weight = $("#weight").val();
		if(weight){
			$("#weight").val(parseFloat(weight).toFixed(round));
		}		
		var price = $("#purchaseprice").val();
		if(price){
			$("#purchaseprice").val(parseFloat(price).toFixed(round));
		}		
	});
	// conversion rate focus out
	$("#uomconversionrate,#conversionquantity").focusout(function() {
		var crate = $("#uomconversionrate").val();
		if(isNaN(crate)) {
			$('#uomconversionrate').val(1);
		}
		var cquan = $("#conversionquantity").val();
		if(isNaN(cquan)) {
			$('#conversionquantity').val(1);
		}
		if(checkVariable('uomconversionrate') == true && checkVariable('conversionquantity') == true){
			var crate = $("#uomconversionrate").val();
			var cquan = $("#conversionquantity").val();
			var quantity = parseFloat(crate)*parseFloat(cquan);
			$("#quantity").val(quantity);
		}	
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,stockmanagementaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
{// view create success function
    function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main view Grid
	function stockmanagementaddgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#stockmanagementpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#stockmanagementpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#stockmanagementaddgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.stockmanagementheadercolsort').hasClass('datasort') ? $('.stockmanagementheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.stockmanagementheadercolsort').hasClass('datasort') ? $('.stockmanagementheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.stockmanagementheadercolsort').hasClass('datasort') ? $('.stockmanagementheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stockmanagement&primaryid=stockmanagementid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#stockmanagementaddgrid').empty();
				$('#stockmanagementaddgrid').append(data.content);
				$('#stockmanagementaddgridfooter').empty();
				$('#stockmanagementaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('stockmanagementaddgrid');
				{//sorting
					$('.stockmanagementheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.stockmanagementheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#stockmanagementpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.stockmanagementheadercolsort').hasClass('datasort') ? $('.stockmanagementheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.stockmanagementheadercolsort').hasClass('datasort') ? $('.stockmanagementheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#stockmanagementaddgrid .gridcontent').scrollLeft();
						stockmanagementaddgrid(page,rowcount);
						$('#stockmanagementaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('stockmanagementheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						stockmanagementaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stockmanagementpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						stockmanagementaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{//Dash board
					$('div.gridcontent div.data-rows').dblclick(function(){
						var ids = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
						$("#processoverlay").show();
						dynamicchartwidgetcreate(ids);
						$("#dashboardcontainer").removeClass('hidedisplay');
						$('.actionmenucontainer').addClass('hidedisplay');
                        $('.footercontainer').addClass('hidedisplay');
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#stockmanagementpgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('stockmanagementcreationview','stockmanagementcreationformadd');
			purchaseorderddvlfetch();
			resetFields();			
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#quantity").attr('readonly',false);
			showhideiconsfun('addclick','stockmanagementcreationformadd');
			$("#batchid").removeClass('validate[required]');
			//form field first focus
			firstfieldfocus();
			//for autonumber
            var elementname = $('#elementsname').val();
		    elementdefvalueset(elementname);
			userbasedbrach();
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$("#formclearicon").hide(); 
				$("#quantity").attr('readonly',true);
				$("#batchid").removeClass('validate[required]');
				addslideup('stockmanagementcreationview','stockmanagementcreationformadd');
				showhideiconsfun('editclick','stockmanagementcreationformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				purchaseorderddvlfetch();
				setTimeout(function(){
					stockeditdatafetchfun(datarowid);
				},100);
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");			
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#stockmanagementaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {		
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			stockmanagementdelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#stockmanagementpgnum li.active').data('pagenum');
		var rowcount = $('ul#stockmanagementpgnumcnt li .page-text .active').data('rowcount');
		stockmanagementaddgrid(page,rowcount);
	}
}
{// product grid
	function sproductsearchgrid(page,rowcount) {
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#sproductsearchgrid').width();
		var wheight = $('#sproductsearchgrid').height();
		var poid = $("#purchaseorderid").val();
		/* col sort */
		var sortcol = $('.sproductlistheadercolsort').hasClass('datasort') ? $('.sproductlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.sproductlistheadercolsort').hasClass('datasort') ? $('.sproductlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.sproductlistheadercolsort').hasClass('datasort') ? $('.sproductlistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Stockmanagement/producrgriddisplay?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=9'+'&poid='+poid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#sproductsearchgrid').empty();
				$('#sproductsearchgrid').append(data.content);
				$('#sproductsearchgridfooter').empty();
				$('#sproductsearchgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('sproductsearchgrid');
				{/* sorting */
					$('.sproductlistheadercolsort').click(function(){
						$('.sproductlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#sproductlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#sproductlistpgnumcnt li .page-text .active').data('rowcount');
						sproductsearchgrid(page,rowcount);
					});
					sortordertypereset('sproductlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#sproductlistpgnumcnt li .page-text .active').data('rowcount');
						sproductsearchgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#sproductlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						sproductsearchgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#sproductlistpgnumcnt li .page-text .active').data('rowcount');
						sproductsearchgrid(page,rowcount);
					});
					$('#sproductlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#sproductlistpgnum li.active').data('pagenum');
						sproductsearchgrid(page,rowcount);
					});
				}
			},
		});
	}
}
{// purchase order overlay grid
	function ponumbersearchgrid(moduleid,page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#ponumbersearchgrid').width();
		var wheight = $('#ponumbersearchgrid').height();
		//col sort
		var sortcol = $('.ponumberheadercolsort').hasClass('datasort') ? $('.ponumberheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.ponumberheadercolsort').hasClass('datasort') ? $('.ponumberheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.ponumberheadercolsort').hasClass('datasort') ? $('.ponumberheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/getmoduledatanumber?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&moduleid="+moduleid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#ponumbersearchgrid').empty();
				$('#ponumbersearchgrid').append(data.content);
				$('#ponumbersearchgridfooter').empty();
				$('#ponumbersearchgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('ponumbersearchgrid');
				{//sorting
					$('.ponumberheadercolsort').click(function(){
						$('.ponumberheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#ponumberpgnum li.active').data('pagenum');
						var rowcount = $('ul#ponumberpgnumcnt li .page-text .active').data('rowcount');
						ponumbersearchgrid(moduleid,page,rowcount);
					});
					sortordertypereset('ponumberheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#ponumberpgnumcnt li .page-text .active').data('rowcount');
						ponumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#ponumberpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						ponumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#ponumberpgnumcnt li .page-text .active').data('rowcount');
						ponumbersearchgrid(moduleid,page,rowcount);
					});
					$('#ponumberpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#ponumberpgnum li.active').data('pagenum');
						ponumbersearchgrid(moduleid,page,rowcount);
					});
				}
			}
		});
	}
	function generatemoduleiconafterload(fieldid,clickevnid) {
		$("#s2id_"+fieldid+"").css('display','inline-block').css('width','90%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer"><i class="material-icons">search</i></span>');
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','inline-block').css('width','90%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer"><i class="material-icons">search</i></span>');
	}
	function generateuomiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline-block').css('width','75%');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer"><i class="material-icons">network_cell</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');	
	}
}
//purchase order overlay data fetch
function purchaseorderdatafetch(datarowid) {
	$.ajax( {
		url : base_url+'Stockmanagement/purchaseorderdatafetch?poid='+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			refreshgrid();
			var podate = data.date;
			var poto = data.poto;
			var postatus = data.postatus;
			$("#purchaseorderdate").val(podate);
			$("#accountid").select2('val',poto);
			$("#crmstatusid").select2('val',postatus);
			$("#productid").select2('val','');
			$("#quantity").val('');
			$("#productuomid").val('');
			$("#weight").val('');
			$("#purchaseprice").val('');
			$("#uomid").select2('val','');
			$("#uomconversionrate").val('');
			$("#conversionquantity").val('');
			$("#quantity").val('');
			$("#counterid").select2('val','');
			$("#batchid").select('val','');
			Materialize.updateTextFields();
		},
	});
}
//product drop down value fetch
function pobasedproduct(poid) {
	$("#productid").empty();
	$("#productid").append($("<option></option>"));
	$.ajax( {
		url : base_url+'Stockmanagement/productddvalfetch?poid='+poid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
				refreshgrid();
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-productidhidden ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$("#productid").append(newddappend);
			}
		},
	});
}
//product based data fetch
function productbaseddatafetch(pid,poid) {
	if(poid == ''){
		$("#touomid").select2("val", "");
		$("#touomid").empty();
		$.ajax({
			url:base_url+'Base/getproductdetails?id='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
				$("#uomid").select2('val','');
				$("#uomconversionrate").val('');
				$("#conversionquantity").val('');
				$("#quantity").val(1);
				$("#purchaseprice").val(data.unitprice);
				$("#uomid").select2('val',data.uomid);
				$("#uomid").attr('readonly','readonly');
				$uomdata = data.touomarr;
				$uomlength = $uomdata.length;
				for (var i = 0; i < $uomlength; i++) {
					$('#touomid')
					.append($("<option></option>")
	                .attr("value",$uomdata[i]['uomtoid'])
	                .attr("data-touomidhidden",$uomdata[i]['uomname'])
	                .attr("data-conrate",$uomdata[i]['conversionrate'])
	                .attr("data-symbol",$uomdata[i]['symbol'])
	                .attr("data-precision",$uomdata[i]['uomprecision'])
	                .text($uomdata[i]['uomname']));
				}		
				$('#touomid').select2('val','').trigger('change');
				$('#touomid').attr("readonly", false);
				$("#counterid").select2('val',data.counterid);
				if(data.hasbatch != 'No') {
					batchdatafetch(pid);
				} else {
					$("#batchid").select('val','').trigger('change');
					$("#batchid").empty();					
				}
			}
		});
	}else{
		$.ajax( {
			url : base_url+'Stockmanagement/productbaseddatafetch?productid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$("#quantity").val('');
					$("#productuomid").val('');
					$("#purchaseprice").val('');
					$("#uomid").select2('val','');
					$("#uomconversionrate").val('');
					$("#conversionquantity").val('');
					$("#quantity").val('');
					$("#counterid").select2('val','');
					$("#batchid").select('val','');
					$("#touomid").select2('val','');
					$("#touomid").empty();
					var uomid = data.uomid;
					var crate = data.conversionrate;
					var cquan = data.conversionquantity;
					var quantity = parseInt(data.quantity);
					var unitprice = data.unitprice;
					var counterid = data.counterid;
					var hasbatch = data.hasbatch;
					$("#quantity").val(quantity);
					$("#productuomid").val(uomid);
					$("#purchaseprice").val(unitprice);
					$("#uomid").select2('val',uomid);
					$("#uomid").attr('readonly','readonly');
					$('#touomid')
					.append($("<option></option>")
	                .attr("value",data.touomid)               
	                .text(data.uomname));
					$('#touomid').select2('val',data.touomid).trigger('change');
					$('#touomid').attr("readonly", true);
					$("#uomconversionrate").val(crate);
					$("#conversionquantity").val(cquan);
					$("#quantity").val(quantity);
					$("#counterid").select2('val',counterid);
					if(hasbatch != 'No') {
						batchdatafetch(pid);
					} else {
						$("#batchid").select('val','').trigger('change');
						$("#batchid").empty();
					}
				}
			},
		});
	}	
}
//product based batch datafetch
function batchdatafetch(productid) {	
	$("#batchid").empty();
	$("#batchid").append($("<option></option>"));
	$.ajax( {
		url : base_url+'Stockmanagement/batchdatafetch?productid='+productid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-batchidhidden ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$("#batchid").append(newddappend);
				$("#batchid").addClass('validate[required]');
				$("#batchid").select('val','').trigger('change');
			}
		},
	});
}
//stock management create
function stockmanagementcreate() {
	var formdata = $("#dataaddform").serialize();
	var uomid = $('#uomid').val();
	var touomid = $('#touomid').val();
	var uomconversionrate = $('#uomconversionrate').val();
	var conversionquantity = $('#conversionquantity').val();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Stockmanagement/stockmanagementcreate",
		data: "datas=" + datainformation+amp+'uomid='+uomid+amp+'touomid='+touomid+amp+'uomconversionrate='+uomconversionrate+amp+'conversionquantity='+conversionquantity,
		type: "POST",
		cache:false,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#stockmanagementcreationformadd').hide();
				$('#stockmanagementcreationview').fadeIn(1000);
				alertpopup(savealert);
				refreshgrid();
			}
		},
	});
}
//stock management delete
function stockmanagementdelete(datarowid) {
	$.ajax( {
		url: base_url+"Stockmanagement/stockmanagementdelete?smid="+datarowid,
		cache:false,
		success: function(msg) {
			$("#basedeleteoverlay").fadeOut();
			refreshgrid();
			var nmsg =  $.trim(msg);
			if (nmsg == "TRUE") { 					
				alertpopup('Deleted successfully');
			} else if (nmsg == "Denied") {					
				alertpopup('Permission denied');
			}
		},
	});
}
//stock edit data fetch function
function stockeditdatafetchfun(datarowid) {
	$.ajax({
		url: base_url+"Stockmanagement/stockmanagementeditdatafetch?smid="+datarowid,
		cache:false,
		dataType : 'json',
		async : false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$("#primarydataid").val(datarowid);
				$('#stockmanagementdate').val(data.date);
				$('#purchaseorderdate').val(data.podate);
				$('#quantity').val(data.quantity);
				$('#weight').val(data.weight);
				$('#purchaseprice').val(data.price);
				$('#receivedby').val(data.receivedby);
				$('#referrencenumber').val(data.referrencenumber);
				$('#comments').val(data.comments);
				$('#tagtypeid').select2('val',data.tagtype);
				$('#crmstatusid').select2('val',data.postatus);
				$('#branchid').select2('val',data.branch);
				$('#productid').select2('val',data.product);
				$('#uomid').select2('val',data.uom);
				$("#uomid").attr('readonly','readonly');
				$('#batchid').select2('val',data.batch);
				$('#counterid').select2('val',data.counterid);
				$('#purchaseorderid').select2('val',data.ponumber);
				$('#tagtemplateid').select2('val',data.tagtemplate);
				$touomid = data.touomid;
				$touomname = data.touomname;
				$('#touomid').empty();
				$('#touomid')
				.append($("<option></option>")
                .attr("value",$touomid)               
                .text($touomname));
				$('#touomid').select2('val',$touomid).trigger('change');
				$('#uomconversionrate').val(data.convertrate);
				$('#conversionquantity').val(data.conversionquantity);
				Materialize.updateTextFields();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		},
	});
}
//stock management update
function stockmanagementupdate() {
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var uomid = $('#uomid').val();
	var touomid = $('#touomid').val();
	var uomconversionrate = $('#uomconversionrate').val();
	var conversionquantity = $('#conversionquantity').val();
	$.ajax({
		url: base_url + "Stockmanagement/stockmanagementupdate",
		data: "datas=" + datainformation+amp+'uomid='+uomid+amp+'touomid='+touomid+amp+'uomconversionrate='+uomconversionrate+amp+'conversionquantity='+conversionquantity,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#stockmanagementcreationformadd').hide();
				$('#stockmanagementcreationview').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
			}
		},
	});
}
//user based branch fetch
function userbasedbrach() {
	$.ajax({
		url : base_url+"Stockmanagement/userbasedbranchfetch",
		cache:false,
		dataType : 'json',
		async : false,
		success : function(data) {
			var nmsg =  $.trim(data);
			$("#branchid").select2("val",nmsg);
		},
	});
}
//purchase order dd value fetch
function purchaseorderddvlfetch() {
	$("#purchaseorderid").empty();
	$("#purchaseorderid").append($("<option></option>"));
	$.ajax({
		url : base_url+"Stockmanagement/purchaseorderddvlfetch",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-purchaseorderidhidden ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$("#purchaseorderid").append(newddappend);
			}
		},
	});
}