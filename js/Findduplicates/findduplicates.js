$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		findduplicatecriteriaaddgrid();
		findduplicaterecordaddgrid();
		findduplicatemergeaddgrid();
		gridsettings();
		firstfieldfocus();
		cntaddcount=0;
	}
	$("#closeaddform").removeClass('hidedisplay');
	{
		var wheight = $(window).height();
		var frmheight = wheight-130;
		$('.stepperformcontainer').css('height',frmheight+'px');
	}
	{ //keyboard shortcut reset global variable
		viewgridview =0;
		innergridview = 1;
	}
	{
		$("#closeaddform").click(function(){
			window.location =base_url+'Dataimport';
		});
	}
	{
		$("#criteriaaddicon").click(function(){
			$("#criteriasectionoverlay").removeClass("closed");
			$("#criteriasectionoverlay").addClass("effectbox");
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{
		$("#criteriacancelbutton").click(function(){
			$("#criteriasectionoverlay").addClass("closed");
			$("#criteriasectionoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{//Tab Selection
		$('#tab1 span').removeAttr('style');
		$('#tab1').click(function(){
			$('#tab2,#tab3').removeClass('activetab');
			$(this).addClass('active-step activetab');
			//$(this).find('span').removeAttr('style');
			$("#subformspan1").show();
			$("#subformspan2,#subformspan3").hide();
			findduplicatecriteriaaddgrid();
			//for touch
			mastertabid=1;
		});
		$('#tab2').click(function(){
			var checktab = $("#moduleid").val();
			if(checktab == '') {
				alertpopup('Please select module name.');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			} else {
				$("#tab1").addClass('editable-step');
				$(this).addClass('active-step activetab');
				$('#tab1,#tab3').removeClass('activetab');
				$("#subformspan2").show();
				$("#subformspan1,#subformspan3").hide();
				findduplicaterecordaddgrid();
			}
			//for touch
			mastertabid=2;
		});
		$('#tab3').click(function(){
			if($(this).hasClass('active')){
			}else{
				alertpopup('Please select records and click merge icon.');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			}
			//for touch
			mastertabid=3;
		});
	}
	//module drop down change function
	$('#moduleid').change(function(){//modname
		var modname = $("#moduleid").find('option:selected').data('modname');
		$("#moduleidname").val(modname);
		var id = $(this).val();
		conditionfieldnamelist(id);
		$("#findduplicatecriteriaaddgrid").jqGrid('clearGridData');
		if(id != "") {
			findduplicaterecordaddgrid();
		} else {
			clearform('cleardataform');
			$("#findduplicatecriteriaaddgrid").jqGrid('clearGridData');
		}
		var condcnt = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
		if(condcnt==0) {
			$("#massandorcondiddivhid").hide();
			$('.condmandclass').text('');
			$('#massandorcondid').removeClass('validate[required]');
		} else {
			$("#massandorcondiddivhid").show();
			$('.condmandclass').text('*');
			$('#massandorcondid').addClass('validate[required]');
		}
	});
	 //formfields  hidden value assign
	$("#formfields").change(function() {
		var fieldlable = $("#formfields").val();
		var ffname = $("#formfields").find('option:selected').data('ffname');
		$("#formfieldsname").val(ffname);
		var jointable = $("#formfields").find('option:selected').data('jointable');
		var indexname = $("#formfields").find('option:selected').data('indexname');
		var id = $("#formfields").find('option:selected').data('id');
		$("#jointable").val(jointable);
		$("#indexname").val(indexname);
		$("#fieldid").val(id);
		var uitypeid = $("#formfields").find('option:selected').data('uitype');
		if(uitypeid == '2' || uitypeid == '3' || uitypeid == '4'|| uitypeid == '5' || uitypeid == '6' || uitypeid == '7' || uitypeid == '10' || uitypeid == '11' || uitypeid == '12' || uitypeid == '14') {
			showhideintextbox('criteriaddfieldvalue','value');
			$('#value').val('');
			$('#value').datetimepicker("destroy");
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$("#value").prop("disabled", false);
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#value").removeClass('validate[required,maxSize[100]]');
			$("#value").addClass('validate[required,maxSize[100]]');
			$("#timevalue").removeClass('validate[required]');
		} else if(uitypeid == '17') {
			showhideintextbox('value','criteriaddfieldvalue');
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#timevalue").removeClass('validate[required]');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").datepicker("disable");
			fieldviewnamebespicklistdddvalue(indexname);
		} else if(uitypeid == '18' || uitypeid == '19'|| uitypeid == '20' || uitypeid == '21'|| uitypeid == '25' || uitypeid == '26' || uitypeid == '27' || uitypeid == '28'|| uitypeid == '29') {
			showhideintextbox('value','criteriaddfieldvalue');
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#timevalue").removeClass('validate[required]');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").datepicker("disable");
			fieldviewnamebesdddvalue(indexname);
		} else if(uitypeid == '13') {
			showhideintextbox('value','criteriaddfieldvalue');
			$("#timevalue").removeClass('validate[required]');
			$("#criteriaddfieldvalue").addClass('validate[required]');
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#value").datepicker("disable");
			checkboxvalueget('criteriaddfieldvalue');
		} else if(uitypeid == '8') {
			$("#timevalue").removeClass('validate[required]');
			showhideintextbox('criteriaddfieldvalue','value');
			$('#value').val('');
			$('#value').datetimepicker("destroy");
			$("#criteriaddfieldvalue").removeClass('validate[required]')
			if(fieldlable == 'Date of Birth'){
				var dateformetdata = $('#value').attr('data-dateformater');
				$('#value').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : '1947:c+100',
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$("#critreiavalue").val(checkdate);
						}
					},
					onClose: function () {
						$('#value').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
			} else {
				$('#value').timepicker('remove');
				var dateformetdata = $('#value').attr('data-dateformater');
				$('#value').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					onSelect: function () {
						var checkdate = $(this).val();
						$("#critreiavalue").val(checkdate)
					},
					onClose: function () {
						$(this).focus();$(this).focusout();$(this).focus();
					}
				}).click(function() {
						$(this).datetimepicker('show');
					});
			}
		} else if(uitypeid == '9') {
			$("#timevaluedivhid").show();
			$("#value").removeClass('validate[required] validate[required,maxSize[100]]');
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$("#timevalue").removeClass('validate[required]');
			$("#timevalue").addClass('validate[required]');
			$("#criteriaddfieldvaluedivhid,#valuedivhid").hide();
			$('#timevalue').val('');
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$('#timevalue').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true,
			});
			var checkdate = $("#timevalue").val();
			if(checkdate != ''){
				$("#critreiavalue").val(checkdate);
			}
		} else {
		}
	});
	{
		//focus out in value
		$("#value").focusout(function(){
			var value = $("#value").val();
			$("#critreiavalue").val(value);
		});
		//dropdown change value
		$("#criteriaddfieldvalue").change(function(){
			var value = $("#criteriaddfieldvalue").val();
			$("#critreiavalue").val(value);
		});
		//
		$("#massconditionid").change(function(){
			var condid = $("#massconditionid").find('option:selected').data('condid');
			$("#massconditionidname").val(condid);
		});
		$("#massandorcondid").change(function(){
			var andorid = $("#massandorcondid").find('option:selected').data('andorid');
			$("#massandorcondidname").val(andorid);
		});
	} 
	var viewid ="";
	var maintabinfo = '';
	var	parentid = '';

	{//dynamic form to grid
		$('#findduplicatecriaddbutton').click(function(){
			var viewcount = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
			if(viewcount == 0){
				$('#massandorcondid').removeClass('validate[required]');
			} else {
				$('#massandorcondid').addClass('validate[required]');
			}
			$("#findduplicatecriteriaaddgridvalidation").validationEngine('validate');
		});
		$("#findduplicatecriteriaaddgridvalidation").validationEngine({
			onSuccess: function() {
				var modid = $("#moduleid").val();
				var ffield = $("#formfields").val();
				var condid = $("#massconditionid").val();
				var value = $("#critreiavalue").val();
				cntaddcount++;
				$("#criteriasectionoverlay").addClass("closed");
				$("#criteriasectionoverlay").removeClass("effectbox");
				jQuery("#findduplicatecriteriaaddgrid").jqGrid('FormToGrid', cntaddcount, "#findduplicateaddform",'add', 'last');
				clearformsol();
				setTimeout(function() {
					//$('#moduleid').val(modid);
					var condcnt = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
					if(condcnt==0) {
						$("#massandorcondiddivhid").hide();
						$('.condmandclass').text('');
						$('#massandorcondid').removeClass('validate[required]');
					} else {
						$("#massandorcondiddivhid").show();
						$('.condmandclass').text('*');
						$('#massandorcondid').addClass('validate[required]');
					}
				},100); 
				if(cntaddcount == 1) {
					//$('#moduleid').attr('readonly',true);
				}
				
			},
			onFailure: function() {
				var dropdownid =['5','moduleid','formfields','massconditionid','criteriaddfieldvalue','massandorcondid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//All record check box
	$('#finddupselecticon').click(function(){
		if($(this).hasClass('unchecked')) {
			$(this).removeClass('fa-square-o');
			$(this).removeClass('unchecked');
			$(this).addClass('fa-check-square-o');
			$(this).addClass('checked');
			$('#allrecorddelete').val('Yes');
			setTimeout(function(){
				if( $('#cb_findduplicaterecordaddgrid').prop('checked') == true ) {
					$('#cb_findduplicaterecordaddgrid').trigger('click');
				}
				$('#findduplicaterecordaddgrid').jqGrid('resetSelection');
			},100);
		} else if($(this).hasClass('checked')) {
			$(this).addClass('fa-square-o');
			$(this).addClass('unchecked');
			$(this).removeClass('fa-check-square-o');
			$(this).removeClass('checked');
			$('#allrecorddelete').val('No');
		}
	});
	//duplicate delete
	$("#finddupdeleteicon").click(function(){
		var ids = $('#findduplicaterecordaddgrid').jqGrid('getGridParam', 'selarrrow');
		if(ids!='') {
			combainedmoduledeletealert('duplicatedeleteyes');
			$("#duplicatedeleteyes").click(function(){
				var ids = $("#findduplicaterecordaddgrid").jqGrid('getGridParam','selarrrow');
				var parenttbl = $("#parenttable").val();
				var moduleid = $('#moduleid').val();
				duplicaterecdelte(parenttbl,ids,moduleid);
				$(this).unbind();
			});
		} else {
			alertpopup('Please Select The Record(s) For Delete');
		}
	});
	//condition data delete
	$("#findduplicateeletebtn").click(function(){
		var datarowid = $('#findduplicatecriteriaaddgrid').jqGrid('getGridParam','selrow');
		if(datarowid) {
			$('#findduplicatecriteriaaddgrid').jqGrid('delRowData',datarowid);
			$('#findduplicatecriteriaaddgrid .gridcontent div.wrappercontent div ul:first li.massandorcondidname-class').text('');
			var count = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
			if(count == 0) {
				$('#moduleid').attr('readonly',false);
				$('.condmandclass').text('');
				$("#massandorcondiddivhid").hide();
				$('#massandorcondid').removeClass('validate[required]');
			}
			findduplicaterecordaddgrid();
		} else {
			alertpopup("Please select a row");
		}
	});
	//data merge icon click
	$('#finddupmergeicon').click(function(){
		var rowids = $("#findduplicaterecordaddgrid").jqGrid('getGridParam','selarrrow');
		var moduleid = $('#moduleid').val();
		if(rowids != '') {
			if(rowids.length < '2') {
				alertpopup('Please Select Minimum 2 Records For Merge');
			} else if(rowids.length > '3') {
				alertpopup('You Are Allowed To Select Maximum 3 Records For Merge');
			} else {
				// To View the Content Area 
				$('.ui-jqgrid-view').css('opacity','0');
				showmergecolumndetails(rowids,moduleid);
				{// Tab focus
					$("#tab2").addClass('editable-step');
					$("#tab3").addClass('active-step');
					$("#subformspan3").show();
					$("#subformspan2,#subformspan1").hide();
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','3');
				}
			}
		} else {
			alertpopup('Please Select The Record(s) For Merge');
		}
	});

	//merge selected row
	$('#savemergerecordicon').click(function(){
		var mergerowids = $('#findduplicatemergeaddgrid').jqGrid('getDataIDs');
		var mergedata = [];
		var rowid = $('#mergedatarowid').val();
		var megrowid = rowid.split(',');
		$.each(mergerowids, function(index,value) {
			var keyindex = $('#findduplicatemergeaddgrid').jqGrid('getCell',value,'internalcolumnname');
			for(var h=1; h<=(megrowid.length); h++) {
				if($('#record'+h+'_'+value+'').attr('checked')) {
					var data = $('#record'+h+'_'+value+'').val();
					mergedata.push(data);
				}
			}
		});
		var criteriagriddata = JSON.stringify(mergedata);
		$('#mergedatarow').val(criteriagriddata);
		setTimeout(function(){
			selecteddatamerge();
		},200);
	});
	{//Find Duplicates Conversion From All modules
		var finddubid = sessionStorage.getItem("finddubid"); 
		if(finddubid != null){
			setTimeout(function(){
				$('#moduleid').select2('val',finddubid).trigger('change');
				sessionStorage.removeItem("finddubid");
			},100);				
		}
	}
	//AND/OR condiron hide show 
	var condcnt =  jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
	if(condcnt==0) {
		$("#massandorcondiddivhid").hide();
	} else {
		$("#massandorcondiddivhid").show();
	}
});
function clearformsol() {
	$("#formfields").select2('val',"");
	$("#criteriaddfieldvalue").select2('val',"");
	$("#massconditionid").select2('val',"");
	$("#massandorcondid").select2('val',"");
	$("#value").val("");
	$("#timevalue").val('');
	$("#jointable").val('');
	$("#indexname").val('');
	$("#critreiavalue").val('');
	$("#fieldid").val('');
}
//merge selected data
function selecteddatamerge() {
	$('#processoverlay').show();
	var parenttbl = $("#parenttable").val();
	var parenttblid = $("#parenttableid").val();
	var moduleid = $('#moduleid').val();
	var dataset = $('#mergedatarow').val();
	var regExp = /&/g;
	var datacontent = dataset.match(regExp);
	if(datacontent !== null){	
		for (var i = 0; i < datacontent.length; i++) {
			dataset = dataset.replace(''+datacontent[i]+'','A110S');
		}
	}
	var ids = $('#mergedatarowid').val();
	var amp = '&';
	$.ajax({
		url: base_url + "Findduplicates/mergeduplicaterecords",
		data:"datas="+amp+"sets="+dataset+amp+"parenttable="+parenttbl+amp+"ids="+ids+amp+"moduleid="+moduleid+amp+'parenttab='+parenttblid,
		type:"POST",
		aync:false,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				alertpopup('Record Merged Successfully');
				$('#tab2').trigger('click');
				$("#findduplicatemergeaddgrid").jqGrid('clearGridData');
				$('#processoverlay').hide();
				//for touch
				mastertabid=2;
			} else {
				$('#processoverlay').hide();
			}
		},
	});
}
//get merge data information
function showmergecolumndetails(rowids,moduleid) {
	$('#mergedatarowid').val(rowids);
	var showhidecols = ['record3'];
	if(rowids.length == '2') {
		$("#findduplicatemergeaddgrid").jqGrid("hideCol", showhidecols);
	} else {
		$("#findduplicatemergeaddgrid").jqGrid("showCol", showhidecols);
	}
	var parenttab = $('#parenttable').val();
	var parenttabid = $('#parenttableid').val();
	jQuery("#findduplicatemergeaddgrid").jqGrid('setGridParam', {datatype: "xml",
	url:base_url + "Findduplicates/mergedatainformationfetch?rowids="+rowids+'&moduleid='+moduleid+'&ptabinfo='+parenttab+'&ptabpriminfo='+parenttabid,page: 1}).trigger('reloadGrid');
}

//duplicate record delete
function duplicaterecdelte(parenttable,ids,moduleid) {
	$.ajax({
		url: base_url + "Findduplicates/duplicaterecdelete?parenttable="+parenttable+"&ids="+ids+"&moduleid="+moduleid,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
				$("#findduplicaterecordaddgrid").trigger('reloadGrid');
			} else { }
		},
	});
}
//field name show
function conditionfieldnamelist(ids) {
	$('#formfields').empty();
	$('#formfields').val('');
	$.ajax({
		url: base_url + "Massupdatedelete/viewdropdownload?ids="+ids+"&autonum=No",
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			$('#formfields').empty();
			$('#formfields').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#formfields')
					.append($("<option></option>")
					.attr("data-jointable",data[index]['jointable'])
					.attr("data-indexname",data[index]['indexname'])
					.attr("data-uitype",data[index]['uitype'])
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//Find Duplicate Criteria Grid
function findduplicatecriteriaaddgrid() {
	$("#findduplicatecriteriaaddgrid").jqGrid({
		url:base_url+"",
		datatype: "local",
		colNames:["And/Or","Module Id", "Module Name", "Field Name", "Field Name", "Condition", "Condition", "Value", "Field Value", "Field Value", "Value",  "And/Or", "Join Table Name", "Index Name", "Hidden Field Id"],
   	    colModel:
        [
			{name:"massandorcondid", index:"massandorcondid",width:100},
			{name:"moduleid", index:"moduleid", hidden:true,width:1},
			{name:"moduleidname", index:"moduleidname",width:200},
			{name:"formfieldsname", index:"formfieldsname", hidden:true,width:1},
			{name:"formfields", index:"formfields", width:100},
			{name:"massconditionidname", index:"massconditionidname", hidden:true,width:1},
			{name:"massconditionid", index:"massconditionid", width:100},
			{name:"value", index:"value", hidden:true,width:1},
			{name:"criteriaddfieldvaluename", index:"criteriaddfieldvaluename", hidden:true,width:1},
			{name:"criteriaddfieldvalue", index:"criteriaddfieldvalue", hidden:true,width:1},
			{name:"critreiavalue", index:"critreiavalue", width:100},
			{name:"massandorcondidname", index:"massandorcondidname", hidden:true,width:1},
			{name:"jointable", index:"jointable", hidden:true,width:1},
			{name:"indexname", index:"indexname", hidden:true,width:1},
			{name:"fieldid", index:"fieldid", hidden:true,width:1}
		],
		pager:'#findduplicatecriteriaaddgridnav',
		sortname:'',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth: false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		sortable: false,		
	});
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('findduplicatecriteriaaddgrid');
	$("#findduplicatecriteriaaddgridnav_center .ui-pg-table").addClass('hidedisplay');
}
//Find Duplicate Record Grid
function findduplicaterecordaddgrid() {
$("#findduplicaterecordaddgrid").jqGrid('GridUnload');
	var id = $("#moduleid").val();
	if(id != "") {
		$.ajax({
			url:base_url+"Findduplicates/defaultviewfetch?modid="+id,
			async:false,
			cache:false,
			success:function(data) {
				if(data != "") {
					viewid = data;
					$("#defaultviewcreationid").val(viewid);
				} else {
					viewid = '84';
					$("#defaultviewcreationid").val(viewid);
				}
			},
		});
		$.ajax({
			url:base_url+"Findduplicates/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					$("#parenttable").val(pdata);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
				} else {
					maintabinfo = 'company';
					parentid = 'companyid';
				}
			},
		});
	} else {
		viewid = '84';
		maintabinfo = 'company';
		parentid = 'companyid';
		$("#defaultviewcreationid").val(viewid);
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	//criteria grid details
	var cricount = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getGridParam', 'records');
	var gridData = jQuery("#findduplicatecriteriaaddgrid").jqGrid('getRowData');
	var criteriagriddata = JSON.stringify(gridData);
	$.ajax({
		url:base_url+"Base/gridheaderinformationfetch?viewid="+viewid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			var colsname = data.colname;
			var colData = '';
			colData = columnsData(data.colmodelname,data.colmodelindex,data.colmodeluitype,data.colmodelviewtype);
			colData = eval('{' + colData + '}');
			$("#findduplicaterecordaddgrid").jqGrid({
				url:base_url+"Findduplicates/gridvalinformationfetch?viewid="+viewid+"&maintabinfo="+maintabinfo+"&parentid="+parentid+"&viewfieldids="+id+"&griddata="+criteriagriddata+"&cricount="+cricount,
				datatype: "xml",
				colNames:colsname,
				colModel:colData,
				pager:'#findduplicaterecordaddgridnav',
				sortname:parentid,
				sortorder:'desc',
				viewrecords:false,
				loadonce:false,
				autowidth: false,
				multiselect: true,
				toolbar: [false, "top"],
				height: 300,
				width: 'auto',
				loadComplete: function() {
					$('#findduplicaterecordaddgrid .cbox').click(function(){
						setTimeout(function(){
							if($('#finddupselecticon').hasClass('checked')) {
								$('#finddupselecticon').addClass('fa-square-o');
								$('#finddupselecticon').addClass('unchecked');
								$('#finddupselecticon').removeClass('fa-check-square-o');
								$('#finddupselecticon').removeClass('checked');
								$('#allrecorddelete').val('No');
							}
						},20);
					});
				},
			});
		},
	});
	//Grid Height and Width Set Based On Screen Size
	innergridwidthsetbasedonscreen('findduplicaterecordaddgrid');
	formwithgridclass("findduplicaterecordaddgrid");
	//select current page record (check box) unchecked all record checked option
	$('#cb_findduplicaterecordaddgrid').click(function(){
		setTimeout(function(){
			if( $('#cb_findduplicaterecordaddgrid').prop('checked') == true ) {
				if($('#finddupselecticon').hasClass('checked')) {
					$('#finddupselecticon').addClass('fa-square-o');
					$('#finddupselecticon').addClass('unchecked');
					$('#finddupselecticon').removeClass('fa-check-square-o');
					$('#finddupselecticon').removeClass('checked');
					$('#allrecorddelete').val('No');
				}
			}	
		},20);
	});
}
//Find Duplicate merge Grid
function findduplicatemergeaddgrid() {
	$("#findduplicatemergeaddgrid").jqGrid({
		url:base_url+"",
		datatype: "local",
		colNames:['Fields','Column Name','Table Name','Ui TypeId','Parent Table','Record 1','Record 2','Record 3'],
   	    colModel:
        [
			{name:'internalcolumnname',index:'internalcolumnname', width:200,resizable:false},
			{name:'columnname',index:'columnname', width:200,resizable:false,hidden:true},
			{name:'tablename',index:'tablename', width:200,resizable:false,hidden:true},
			{name:'uitypeid',index:'uitypeid', width:200,resizable:false,hidden:true},
			{name:'parenttable',index:'parenttable', width:200,resizable:false,hidden:true},
			{name:'record1',index:'record1', width:200,resizable:false,formatter:myformatter},
			{name:'record2',index:'record2', width:200,resizable:false,formatter:myformatter},
			{name:'record3',index:'record3', width:200,resizable:false,formatter:myformatter,hidden:true}
		],
		pager:'#findduplicatemergeaddgridnav',
		sortname:'sortorder',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		toolbar: [false,"top"],
		rowNum:1000,
		height: '100%',
		width: 'auto',	
		autowidth: false,
		shrinkToFit: false,	
	});
	$("#findduplicatemergeaddgridnav_center .ui-pg-table").addClass('hidedisplay');
	function myformatter(cellValue, option) {
		var chkattr = "";
		if(option.colModel['name'] == 'record1') {
			chkattr='checked="checked"';
		}
		var dataval = cellValue;
		if( cellValue === undefined ) {dataval="";}
		return '<input type="radio" id="'+option.colModel['name']+'_'+option.rowId+'" class="'+option.colModel['name']+'" name="radio_'+ option.rowId+'" '+chkattr+' value="'+dataval+'" /> '+dataval+'';
	}
	var $grid = $("#findduplicatemergeaddgrid");
	$grid.closest("div.ui-jqgrid-view").find("div.ui-jqgrid-hdiv table.ui-jqgrid-htable tr.ui-jqgrid-labels > th.ui-th-column")
	.each(function () {
		$('<input type="radio" id="" name="headerradiobtn">').css({height: "0px", position:"relative", top:'-1rem'}).appendTo(this).click(function (e) {
			var getname = $(this).prev('div').attr('id'); 
			var getcolnamesplit = getname.split("_");
			var getcolname = getcolnamesplit[2];
			$('.'+getcolname+'').attr('checked', true);
		});
	});
	$("#findduplicatemergeaddgrid_rn input[type='radio']").hide();
	var firstcolhide = $("#findduplicatemergeaddgrid_rn").next('th').attr('id');
	$('#'+firstcolhide+' input[type="radio"]').css('opacity','0');
	$('#'+firstcolhide+' .ui-jqgrid-sortable').css('left','-2px');
	//Grid Height and Width Set Based On Screen Size
	innergridwidthsetbasedonscreen('findduplicatemergeaddgrid');
}
// Grid Height Width Settings
function gridsettings() 
{
	formwithgridclass("findduplicatecriteriaaddgrid");
	formwithgridclass("findduplicaterecordaddgrid");
	formwithgridclass("findduplicatemergeaddgrid");
	var menucatistb = ["3","findduplicatecriteriaaddgrid","findduplicaterecordaddgrid","findduplicatemergeaddgrid"];
	createistb(menucatistb);
}
function showhideintextbox(hideid,showid) {
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
//field name based drop down value
function fieldviewnamebespicklistdddvalue(indexname){
	var moduleid = $("#moduleid").val();
	$.ajax({
		url: base_url + "Findduplicates/fieldviewnamebespicklistdddvalue?fieldname="+indexname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#criteriaddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
//field name based drop down value
function fieldviewnamebesdddvalue(indexname){
	$.ajax({
		url: base_url + "Findduplicates/viewfieldnamebesdddvalue?fieldname="+indexname,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#criteriaddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
//check box value get
function checkboxvalueget(dropdownname) {
	$.ajax({
		url: base_url + "Findduplicates/checkboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16')
					{
						$("#"+dropdownname+"")
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					}
				});
			}
			$("#"+dropdownname+"").trigger('change');			
		},
	});
}