$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		salescommissionaddgrid();
		firstfieldfocus();
		salescommissioncrudactionenable();
	}
	ACDELETE = 0;
	counterstatus = $('#counterstatus').val();
	weight_round = $('#weight_round').val();
	//hidedisplay
	$('#salescommissiondataupdatesubbtn,#groupcloseaddform').hide();
	$('#salescommissionsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			salescommissionaddgrid();
		});
	}
	 {// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{//validation for Add  
		$('#salescommissionsavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#salescommissionformaddwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#salescommissionformaddwizard").validationEngine( {
			onSuccess: function() {
				salescommissionaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#salescommissiondataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#salescommissionformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#salescommissionformeditwizard").validationEngine({
			onSuccess: function() {
				salescommissionupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$("#salescommissionreloadicon").click(function() {
			clearform('cleardataform');
			$('#salescommissionaddform').find('input, textarea, button, select').attr('disabled',true);
			$('#salescommissionaddform').find("select").select2('disable');
			storagerefreshgrid();
			$('#salescommissiondataupdatesubbtn').hide();
			$('#salescommissionsavebutton').show();
			$("#deleteicon").show();
		});
		$( window ).resize(function() {
			maingridresizeheightset('salescommissionaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	{//filter work
		//for toggle
		$('#salescommissionviewtoggle').click(function() {
			if ($(".salescommissionfilterslide").is(":visible")) {
				$('div.salescommissionfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.salescommissionfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#salescommissionclosefiltertoggle').click(function(){
			$('div.salescommissionfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#salescommissionfilterddcondvaluedivhid").hide();
		$("#salescommissionfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#salescommissionfiltercondvalue").focusout(function(){
				var value = $("#salescommissionfiltercondvalue").val();
				$("#salescommissionfinalfiltercondvalue").val(value);
				$("#salescommissionfilterfinalviewconid").val(value);
			});
			$("#salescommissionfilterddcondvalue").change(function(){
				var value = $("#salescommissionfilterddcondvalue").val();
				var fvalue = $("#salescommissionfilterddcondvalue option:selected").attr('data-ddid');
				salescommissionmainfiltervalue=[];
				if( $('#s2id_salescommissionfilterddcondvalue').hasClass('select2-container-multi') ) {
					salescommissionmainfiltervalue=[];
					$('#salescommissionfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    salescommissionmainfiltervalue.push($cvalue);
						$("#salescommissionfinalfiltercondvalue").val(salescommissionmainfiltervalue);
					});
					$("#salescommissionfilterfinalviewconid").val(value);
				} else {
					$("#salescommissionfinalfiltercondvalue").val(fvalue);
					$("#salescommissionfilterfinalviewconid").val(value);
				}
			});
		}
		salescommissionfiltername = [];
		$("#salescommissionfilteraddcondsubbtn").click(function() {
			$("#salescommissionfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#salescommissionfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(salescommissionaddgrid,'salescommission');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
			$("#employeeid").prop('disabled',false);
		});
	}
	productgenerateiconafterload('productid','productsearchevent');
	$("#productsearchevent").on('click keypress',function(e){
		if(e.keyCode == 13 || e.keyCode === undefined){
			/* var categoryid = $("#categoryid").val();
			if(categoryid != ''){
				$("#productsearchoverlay").fadeIn();
				$("#productsearchclose").focus();
				productsearchgrid();
			} else {
				alertpopup('Please choose the Product Category');
			} */
			// Removed product category field so this s the code
			$("#productsearchoverlay").fadeIn();
			$("#productsearchclose").focus();
			productsearchgrid();
			/* if(counterstatus == 'NO'){
				$("#productsearchoverlay").fadeIn();
				$("#productsearchclose").focus();
				productsearchgrid();
			}else{
				var counterid = $("#counterid").val();
				if(counterid){
					$("#productsearchoverlay").fadeIn();
					$("#productsearchclose").focus();
					productsearchgrid();
				} else {
					alertpopup('Please choose the storage');
				}
			} */
		}	
	});	
	$("#productsearchclose").click(function() {
		$("#productsearchoverlay").fadeOut();
	});
	{//productsearchsubmit
		$("#productsearchsubmit").click(function() {
			var selectedrow = $('#productsearchgrid div.gridcontent div.active').attr('id');
			if(selectedrow) {
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
			}
		});
	}
	{//storage hierarchy
		$('#salescommissionlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			$('#parentsalescommission').val(name);
			$('#counterid').val(listid);
			$('#productid').select2('val','');
		});	
	}
	//from/to validation
	$("#salescommissionto").change(function() {
		var tovalue = $("#salescommissionto").val();
		var fromvalue = $("#salescommissionfrom").val();
		if(parseFloat(tovalue) <= parseFloat(fromvalue)) {
			 $("#salescommissionto").val('');
			alertpopup('To value should be greater than From value');
		}
	});
	$("#salescommissionfrom").change(function() {
		var tovalue = $("#salescommissionto").val();
		var fromvalue = $("#salescommissionfrom").val();
		if(parseFloat(tovalue) <= parseFloat(fromvalue)) {
			 $("#salescommissionfrom").val('');
			 $("#salescommissionto").val('');
			alertpopup('From value should be less than To value');
		}
	});
	//commission rate change
	$("#commissionrate").change(function() {
		var rate = $("#commissionrate").val();
		var type = $("#commissiontypeid").val();
		if(type == 4) {
			 if(rate > 100){
				 $("#commissionrate").val('');
				 alertpopup('rate should be lessthan or equal to 100');
			 }
		}
	});
	//type chage 
	$("#commissiontypeid").change(function() {
		var rate = $("#commissionrate").val();
		var type = $("#commissiontypeid").val();
		if(type == 4) {
			 if(rate > 100){
				 $("#commissionrate").val('');
				 alertpopup('rate should be lessthan or equal to 100');
			 }
		}
	});
	$('#employeeid').change(function(){
		var val = $(this).find('option:selected').val();
		counterassignuservalid(val,'employeeid','salescommission');
	});
	$('#commissionbasedonid').change(function(){
		var val = $(this).find('option:selected').val();
		fromtovalidate(val);
	});
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function salescommissionaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#salescommissionaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#salescommissionsortcolumn").val();
	var sortord = $("#salescommissionsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.salescommissionheadercolsort').hasClass('datasort') ? $('.salescommissionheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.salescommissionheadercolsort').hasClass('datasort') ? $('.salescommissionheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.salescommissionheadercolsort').hasClass('datasort') ? $('.salescommissionheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = '108';
	if(userviewid != '') {
		userviewid= '190';
	}
	var filterid = $("#salescommissionfilterid").val();
	var conditionname = $("#salescommissionconditionname").val();
	var filtervalue = $("#salescommissionfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=salescommission&primaryid=salescommissionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#salescommissionaddgrid').empty();
			$('#salescommissionaddgrid').append(data.content);
			$('#salescommissionaddgridfooter').empty();
			$('#salescommissionaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('salescommissionaddgrid');
			{//sorting
				$('.salescommissionheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.salescommissionheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.salescommissionheadercolsort').hasClass('datasort') ? $('.salescommissionheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.salescommissionheadercolsort').hasClass('datasort') ? $('.salescommissionheadercolsort.datasort').data('sortorder') : '';
					$("#salescommissionsortorder").val(sortord);
					$("#salescommissionsortcolumn").val(sortcol);
					var sortpos = $('#salescommissionaddgrid .gridcontent').scrollLeft();
					salescommissionaddgrid(page,rowcount);
					$('#salescommissionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('salescommissionheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					salescommissionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#salescommissionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					salescommissionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#salescommissionaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#salescommissionpgrowcount').material_select();
		},
	});
}
function salescommissioncrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#salescommissiondataupdatesubbtn').hide();
			$('#salescommissionsavebutton').show();
			$("#employeeid").prop('disabled',false);
			$("#counterid").val('');
			counterhideshow();
			loadusername(1);
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#salescommissionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#salescommissiondataupdatesubbtn').show();
			$('#salescommissionsavebutton').hide();
			loadusername(0);
			salescommissioneditformdatainformationshow(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#salescommissionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			$("#salescommissionprimarydataid").val(datarowid);
			combainedmoduledeletealert('salescommissiondeleteyes');
			if(ACDELETE == 0) {
				$("#salescommissiondeleteyes").click(function(){
					var datarowid = $("#salescommissionprimarydataid").val();
					salescommissiondelete(datarowid);
				});
			}
			ACDELETE =1;
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function salescommissionrefreshgrid() {
		var page = $('ul#salescommissionpgnum li.active').data('pagenum');
		var rowcount = $('ul#salescommissionpgnumcnt li .page-text .active').data('rowcount');
		salescommissionaddgrid(page,rowcount);
	}
}
//new data add submit function
function salescommissionaddformdata() {
	var formdata = $("#salescommissionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Salescommission/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				salescommissionrefreshgrid();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level of account reached !!! cannot create category further.');
			}
        },
    });
}
//old information show in form
function salescommissioneditformdatainformationshow(datarowid) {
	var salescommissionelementsname = $('#salescommissionelementsname').val();
	var salescommissionelementstable = $('#salescommissionelementstable').val();
	var salescommissionelementscolmn = $('#salescommissionelementscolmn').val();
	var salescommissionelementpartable = $('#salescommissionelementspartabname').val();
	$.ajax(	{
		url:base_url+"Salescommission/fetchformdataeditdetails?salescommissionprimarydataid="+datarowid+"&salescommissionelementsname="+salescommissionelementsname+"&salescommissionelementstable="+salescommissionelementstable+"&salescommissionelementscolmn="+salescommissionelementscolmn+"&salescommissionelementpartable="+salescommissionelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				salescommissionrefreshgrid();
			} else {
				var txtboxname = salescommissionelementsname + ',salescommissionprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = salescommissionelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				setTimeout(function(){
					parentstoragenamefetch(datarowid);
				},200);
				$("#employeeid").prop('disabled',true);
				counterhideshow();
				fromtovalidate(data.commissionbasedonid);
				Materialize.updateTextFields();
			}
		}
	});
	$('#salescommissiondataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#salescommissionsavebutton').hide();
}
//udate old information
function salescommissionupdateformdata() {
	var formdata = $("#salescommissionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Salescommission/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#salescommissiondataupdatesubbtn').hide();
				$('#salescommissionsavebutton').show();
				salescommissionrefreshgrid();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				resetFields();
				alertpopup(savealert);			
            }else if(nmsg == 'FAILURE'){
				alertpopup("Check your Parent name mapping. ");
			}else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level reached !!! cannot create category further.');
					}
		        },
		    });
}
//udate old information
function salescommissiondelete(datarowid) {
	var salescommissionelementstable = $('#salescommissionelementstable').val();
	var salescommissionelementspartable = $('#salescommissionelementspartabname').val();
    $.ajax({
        url: base_url + "Salescommission/deleteinformationdata?salescommissionprimarydataid="+datarowid+"&salescommissionelementstable="+salescommissionelementstable+"&salescommissionparenttable="+salescommissionelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	salescommissionrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	salescommissionrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//this is for the product search/attribute icon-dynamic append
function productgenerateiconafterload(fieldid,clickevnid){
	$("#s2id_"+fieldid+"").css('display','block').css('width','90%');
	var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
	$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
	
}
function productsearchgrid(page,rowcount) {
	/*pagination*/
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 200 : rowcount;
	var wwidth = $('#productsearchgrid').width();
	var wheight = $('#productsearchgrid').height();
	/* col sort */
	var sortcol = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').attr('id') : '0';
	/* if(counterstatus == 'NO'){
		var ordertype = '';
	}else{
		var ordertype = $('#counterid').val();
	} */
	var ordertype = $.trim($("#categoryid").val());
	ordertype = ltrim(ordertype,",");
	var moduleid = 90;
	$.ajax({
		url:base_url+"Salescommission/getproductsearch?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&ordertype="+ordertype+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+moduleid+'&industryid='+softwareindustryid, 
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#productsearchgrid').empty();
			$('#productsearchgrid').append(data.content);
			$('#productsearchgridfooter').empty();
			$('#productsearchgridfooter').append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('productsearchgrid');
			{/* sorting */
				$('.productlistheadercolsort').click(function(){
					$('.productlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#productlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
					productsearchgrid(page,rowcount);
				});
				sortordertypereset('productlistheadercolsort',headcolid,sortord);
			}
			{/* pagination */
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
					productsearchgrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#productlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					productsearchgrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
					productsearchgrid(page,rowcount);
				});
				$('#productlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#productlistpgnum li.active').data('pagenum');
					productsearchgrid(page,rowcount);
				});
			}
		},
	});
}
{// Parent storage name value fetch
	function parentstoragenamefetch(pid) {
		$.ajax({
			url:base_url+"Salescommission/parentstoragenamefetch?id="+pid,
			dataType:'json',
			async:false,
			cache:false,
			success:function (data)	{
				$('#counterid').val(data.parentcounterid);
				$('#parentsalescommission').val(data.countername);
			}
		});
	}
}
function ltrim(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
}
function counterhideshow(){
	if(counterstatus == 'YES'){
		$('#counteriddivhid').removeClass('hidedisplay');
		$('#parentsalescommission').addClass('validate[required]');
	}else{
		$('#counteriddivhid').addClass('hidedisplay');
		$('#parentsalescommission').removeClass('validate[required]');
	}
}
function loadusername(datastatus){ // //load username except cashcountter assigned
	$('#employeeid').empty();	
    $('#employeeid').append($("<option></option>"));
	var tablename = 'salescommission';
	$.ajax({
        url: base_url + "Base/loadusername",
		data:{datastatus:datastatus,table:tablename},
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg) 
        { 
			for(var i = 0; i < msg.length; i++) {
				 $('#employeeid')
				.append($("<option></option>")
				.attr('data-employeeidhidden',msg[i]['employeename'])
				.attr("value",msg[i]['employeeid'])
				.text(msg[i]['employeename']));
			} 
        },
    });
}	
// from to field validation based on commission type
function fromtovalidate(val) {
	if(val == 3){
		$('#salescommissionfrom,#salescommissionto').removeClass('validate[required,custom[number],decval[5],maxSize[15]]');
		$('#salescommissionfrom,#salescommissionto').removeClass('validate[required,custom[number],decval['+weight_round+'],maxSize[15]]');
		$('#salescommissionfrom,#salescommissionto').addClass('validate[required,custom[number],decval[0],maxSize[15]]');
	}else{
		$('#salescommissionfrom,#salescommissionto').removeClass('validate[required,custom[number],decval[0],maxSize[15]]');
		$('#salescommissionfrom,#salescommissionto').removeClass('validate[required,custom[number],decval[5],maxSize[15]]');
		$('#salescommissionfrom,#salescommissionto').addClass('validate[required,custom[number],decval['+weight_round+'],maxSize[15]]');
	}
}
	