$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{// Grid Calling
		priviledgegrid();
		//crud action
		crudactionenable();
	}
	{// Clear the form - regenerate the number 
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");			
		});
	}	
	{// Close Add Screen
		var addcloseoppocreation =["closeaddform","previledgecreationview","priviledgeformadd"]
		addclose(addcloseoppocreation);	
	}
	{// Close Add Screen
		var addcloseviewcreation =["viewcloseformiconid","opportunitycreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// For touch
		fortabtouch = 0;
	}
	productgenerateiconafterload('productid','productsearchevent');
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{// Tool bar action icons
		$("#cloneicon").click(function() {
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				opportunityclonedatafetchfun(datarowid);
				showhideiconsfun('editclick','opportunitycreationformadd');	
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				$("#primarydataid").val('');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('priviledgegrid');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				$("#processoverlay").show();
				priviledgeeditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','opportunitycreationformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','opportunitycreationformadd');
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function(){
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			var accountid=getaccountid(datarowid,'opportunity','accountid');
			if (datarowid) {
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','','');
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",'');
					sessionStorage.setItem("moduleid",'204');
					sessionStorage.setItem("recordid",datarowid);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				}
				else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//Expected amount
		$("#probability").focusout(function() {
			var amount = $("#amount").val();
			var probability = $("#probability").val();
			if(amount != ''){
				var expectedamount = parseFloat(amount)*parseFloat(probability/100);
				$("#expectedamount").val(expectedamount);
			} else {
				$("#amount").val(amount);
				
			}
		});
	}
	{// View by drop down change
		$('#dynamicdddataview').change(function() {
			priviledgegrid();
		});
	}
	{// Validation for Opportunity Add  
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function(){
				$("#processoverlay").show();
				priviledgenewdataaddfun();
			},
			onFailure: function(){
				var dropdownid =['2','accountid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Update Opportunity information
		$('#dataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function(){
				$("#processoverlay").show();
				priviledgedataupdateinformationfun();
			},
			onFailure: function(){
				var dropdownid =['4','accountid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				
				alertpopup(validationalert);
			}	
		});
	}
	{// Redirected form Home		
		add_fromredirect("opportunityaddsrc",204);
	}
	//Opportunity-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique204");		
		if(uniquelreportsession != '' && uniquelreportsession != null) {	
			setTimeout(function() {
				//check the valid
				sessionStorage.removeItem("reportunique204"); 
			},50);	
		} 
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").hide();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						} else if(mobilenumber.length == 1) {
							sessionStorage.setItem("mobilenumber",mobilenumber);
							sessionStorage.setItem("viewfieldids",viewfieldids);
							sessionStorage.setItem("recordis",datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").show();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'smsmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmodule").change(function() {
		var moduleid =	$("#smsmoduleid").val(); //main module
		var linkmoduleid =	$("#smsmodule").val(); // link module
		var recordid = $("#smsrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
		}
	});
	$("#callmodule").change(function() {
		var moduleid =	$("#callmoduleid").val(); //main module
		var linkmoduleid =	$("#callmodule").val(); // link module
		var recordid = $("#callrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
		}
	});
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data')) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++){
							if(data[i] != ''){
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1){
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#c2cmobileoverlay").show();
						$("#callmoduledivhid").show();
						$("#callrecordid").val(datarowid);
						$("#callmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'callmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function()  {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,priviledgegrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	$("#productsearchevent").on('click keypress',function(e){
		if(e.keyCode == 13 || e.keyCode === undefined){
					$("#productsearchoverlay").fadeIn();
					productsearchgrid();
					chargeproductgriddatafetch($('#categoryid').val());
		}	
	});
	//from/to validation
	$("#from").change(function() {
		var tovalue = $("#to").val();
		var fromvalue = $("#from").val();
		if(parseFloat(tovalue) < parseFloat(fromvalue)) {
			 $("#from").val('');
			alertpopup('From value should be less than To value');
		}
	});
	$("#to").change(function() {
		var tovalue = $("#to").val();
		var fromvalue = $("#from").val();
		if(parseFloat(tovalue) < parseFloat(fromvalue)) {
			 $("#to").val('');
			alertpopup('To value should be greater than From value');
		}
	});
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function priviledgegrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#priviledgepgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#priviledgepgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#priviledgegrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.priviledgeheadercolsort').hasClass('datasort') ? $('.priviledgeheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.priviledgeheadercolsort').hasClass('datasort') ? $('.priviledgeheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.priviledgeheadercolsort').hasClass('datasort') ? $('.priviledgeheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=priviledge&primaryid=priviledgeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#priviledgegrid').empty();
				$('#priviledgegrid').append(data.content);
				$('#priviledgegridfooter').empty();
				$('#priviledgegridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('priviledgegrid');
				{//sorting
					$('.priviledgeheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.priviledgeheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#opportunityspgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.priviledgeheadercolsort').hasClass('datasort') ? $('.priviledgeheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.priviledgeheadercolsort').hasClass('datasort') ? $('.priviledgeheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#priviledgegrid .gridcontent').scrollLeft();
						priviledgegrid(page,rowcount);
						$('#priviledgegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('priviledgeheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						priviledgegrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#priviledgepgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						priviledgegrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$(".gridcontent").click(function(){
					var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#priviledgepgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('previledgecreationview','priviledgeformadd'); 
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','acccreationformadd');
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			$('#tabgropdropdown').select2('val','1');
			$("#primarydataid").val('');
			fortabtouch = 0;
			setTimeout(function(){
			Materialize.updateTextFields();
				//form field first focus
				firstfieldfocus();
			},100);
			$('#branchid').select2('val',$('#branchidval').val()).trigger('change');
			$('#companyid').select2('val',$('#companyidval').val()).trigger('change');
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				priviledgeeditdatafetchfun(datarowid);
				showhideiconsfun('editclick','priviledgeformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#priviledgegrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			priviledgerecorddelete(datarowid);
		});
	}
	$("#productsearchclose").click(function() {
		$("#productsearchoverlay").fadeOut();
	});
	{//productsearchsubmit
		$("#productsearchsubmit").click(function() {
			var product = $('#searchbasedproductid').val();
			product = product.split(',');
			$('#productid').select2('val',product);
			$("#productsearchoverlay").fadeOut();
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#priviledgespgnum li.active').data('pagenum');
		var rowcount = $('ul#priviledgespgnumcnt li .page-text .active').data('rowcount');
		priviledgegrid(page,rowcount);
	}
}
{// New data add submit function
	function priviledgenewdataaddfun() {
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Priviledge/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#priviledgeformadd').hide();
					$('#previledgecreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Old information show in form
	function editformdatainformationshow(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Priviledge/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,  
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#priviledgeformadd').hide();
					$('#previledgecreationview').fadeIn(1000);
					refreshgrid();
					$("#processoverlay").hide();
				} else {
					addslideup('previledgecreationview','priviledgeformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					$("#processoverlay").hide();
				}
			}
		});
	}
}
{// Update old information
	function priviledgedataupdateinformationfun() {
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		 $.ajax({
			url: base_url + "Priviledge/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$(".ftab").trigger('click');
					$("#opportunitytags").select2('val','');
					$('#priviledgeformadd').hide();
					$('#previledgecreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Delete data information
	function priviledgerecorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Priviledge/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup("Deleted successfully");
				} else if (nmsg == "Denied") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Edit Function
	function priviledgeeditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields();
		editformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function
	function opportunityclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields();
		editformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
//this is for the product search/attribute icon-dynamic append
function productgenerateiconafterload(fieldid,clickevnid){
	$("#s2id_"+fieldid+"").css('display','block').css('width','90%');
	var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
	$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
	
}
function productsearchgrid() {
	var wwidth = $("#productsearchgrid").width();
	var wheight = $("#productsearchgrid").height();
	$.ajax({
		url:base_url+"Priviledge/productsearchgridheaderinformationfetch?moduleid=109&width="+wwidth+"&height="+wheight+"&modulename=productsearchgrid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#productsearchgrid").empty();
			$("#productsearchgrid").append(data.content);
			$("#productsearchgridfooter").empty();
			$("#productsearchgridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('productsearchgrid');
			//header check box
			$(".productsearchgrid_headchkboxclass").click(function() {
				if($(".productsearchgrid_headchkboxclass").prop('checked') == true) {
					$(".productsearchgrid_rowchkboxclass").attr('checked', true);
				} else {
					$('.productsearchgrid_rowchkboxclass').removeAttr('checked');
				}
				getcheckboxrowid('productsearchgrid');
			});
		},
	});	
}
function getcheckboxrowid(gridid) {
	var selected = [];
	$('#'+gridid+' .gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	$("#searchbasedproductid").val(selected);
}
function chargeproductgriddatafetch(categoryid){
	$.ajax({
		url:base_url+"Priviledge/productgriddatafetch",
		dataType:'json',
		data:{categoryid:categoryid},
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			if(data != ''){
				loadinlinegriddata('productsearchgrid',data.rows,'json','checkbox');
				//row based check box
				$(".productsearchgrid_rowchkboxclass").click(function(){
					$('.productsearchgrid_headchkboxclass').removeAttr('checked');
					getcheckboxrowid('productsearchgrid');
				});
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('productsearchgrid');
			}else{
				alertpopupdouble('Product Not Available');
			}
		},
	});	
}