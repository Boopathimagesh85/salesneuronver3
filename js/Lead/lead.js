$(document).ready(function() {
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Main div height width change
		maindivwidth();
	}
	{// Grid Calling Function
		leadcreationviewgrid();
		//crud action
		crudactionenable();
	}
	{// For touch
		fortabtouch = 0;
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{// Click Function of Mail,Reload,Clone,Conversion,Summary Icon
		$("#mailicon").click(function() {
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							var validemail=validateemailanddonotmail(datarowid,'lead','emailid','donotmail');
							var datamail = validemail.split(',');
							if(datamail[0] == true) {
								if(datamail[1] == 'No') {
									var emailtosend = getvalidemailid(datarowid,'lead','emailid','','');
									// For Redirection
									sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
									sessionStorage.setItem("moduleid",'201');
									sessionStorage.setItem("forsubject",'');
									sessionStorage.setItem("recordid",datarowid);
									var fullurl = 'erpmail/?_task=mail&_action=compose';
									window.open(''+base_url+''+fullurl+'');
								} else {
									alertpopup("This contact has choosed the Do Not Mail option. You cant send a mail to this contact");
								}
							} else {
								alertpopup("Invalid email address");
							}
						} else {
							alertpopup("You cant sent a mail for the converted Lead");
						}
					}
				});
			} else {
				alertpopup("Please select a row");
			}	
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		
		//Conversion Settings
		$("#conversionsettingsicon").click(function(){
			window.location =base_url+'Leadconversion';
		});
		
		$( window ).resize(function() {
			maingridresizeheightset('leadcreationviewgrid');
		});
		{//Form Clear
			$("#formclearicon").click(function(){
				var elementname = $('#elementsname').val();
				elementdefvalueset(elementname);
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			});
		}
		$("#cloneicon").click(function() {
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid,  
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							//Function Call For Edit
							$("#processoverlay").show();
							leadclonetdatafetchfun(datarowid);
							//For tablet and mobile Tab section reset
							$('#tabgropdropdown').select2('val','1');
							//for autonumber
							randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
							Operation = 0; //for pagination
						} else {
							alertpopup('Converted records cant be clone');
						}
					}
				});
			} else { 
				alertpopup("Please select a row");
			}
		});
		{//lead convert process
			$("#converticon").click(function() {
				var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
				if (datarowid)  {
					$.ajax({
						url:base_url+"Lead/ledconversiondenied?dataprimaryid="+datarowid, 
						cache:false,
						success: function(data) {
							var nmsg =  $.trim(data);
							if(nmsg != 'Denied'){
								$.ajax({
									url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid,  
									dataType:'json',
									async:false,
									cache:false,
									success: function(data) {
										if((data.fail) != 'FAILED') {
											leadconvertformdatainformationshow(datarowid);
											$('#convertleadovrelay').show();
											if(softwareindustryid == 2){	
												$leadtype = data.leadtype;
												if($leadtype == 2){
													$('#convertto').empty();
													$("#convertaccount").hide();
													$("#convertcontact").show();
													$('input[name=contcheck]').trigger('click').prop('checked', true).val('yes');
													$("#contcheck").attr('disabled', 'disabled');													
													$("#acccheck").val('no');
													$('#opportunitycheckbox').prop('checked', false);
													$("#opportunitydetailsdiv").hide();
													newoptions="";
													newoptions += "<option value =''></option>";
													newoptions += "<option value ='2'>Patient</option>";
													newoptions += "<option value ='3'>Opportunity</option>";
													$('#convertto').append(newoptions);
													$("#convertto").select2('val',2).trigger('change');
												}
												if($leadtype == 3){
													$('#convertto').empty();
													$("#convertcontact").hide();
													$("#convertaccount").show();
													$('input[name=acccheck]').trigger('click').prop('checked', true).val('yes');
													$("#contcheck").val('no');
													$("#acccheck").attr('disabled', 'disabled');
													$('#opportunitycheckbox').attr('checked', false);
													$("#opportunitydetailsdiv").hide();
													newoptions="";
													newoptions += "<option value =''></option>";
													newoptions += "<option value ='1'>Account</option>";
													newoptions += "<option value ='3'>Opportunity</option>";
													$('#convertto').append(newoptions);
													$("#convertto").select2('val',1).trigger('change');
												}
												$.ajax({
													url:base_url+"Lead/leaddatafetch?leadid="+datarowid+"&industryid="+softwareindustryid,  
													dataType:'json',
													async:false,
													cache:false,
													success: function(data) {
														var lname = data['lanme'];
														var accname = data['accname'];
														var convertid = data['convertid'];
														if($leadtype == 2){
															$("#contname").val(lname);
															$("#oppname").val(accname);
															$("#convertid").val(convertid);
															$("#contname").attr('readonly', true);
														}
														if($leadtype == 3){
															$("#accname").val(accname);
															$("#oppname").val(accname);
															$("#convertid").val(convertid);
															$("#accname").attr('readonly', true);
														}	
													}
												});
											}else{
												if(softwareindustryid == 4){
													$('#convertto').empty();
													$("#convertaccount").hide();
													$("#acccheck").val('no');
													$('input[name=contcheck]').trigger('click').prop('checked', true).val('yes');												
													$('#opportunitycheckbox').prop('checked', false);												
													$("#opportunitydetailsdiv").hide();
													newoptions="";
													newoptions += "<option value =''></option>";
													newoptions += "<option value ='2'>Member</option>";
													newoptions += "<option value ='3'>Opportunity</option>";
													$('#convertto').append(newoptions);
													$("#convertto").select2('val',2).trigger('change');
													$.ajax({
														url:base_url+"Lead/leaddatafetch?leadid="+datarowid+"&industryid="+softwareindustryid, 
														dataType:'json',
														async:false,
														cache:false,
														success: function(data) {
															var lname = data['lanme'];
															var convertid = data['convertid'];
															$("#contname").val(lname);
															$("#oppname").val(lname);
															$("#convertid").val(convertid);
														}
													}); 
												}else if(softwareindustryid == 5){
													$('#convertto').empty();
													$('input[name=acccheck]').trigger('click').prop('checked', true).val('yes');
													$('input[name=contcheck]').trigger('click').prop('checked', true).val('yes');												
													$('#opportunitycheckbox').prop('checked', false);												
													$("#opportunitydetailsdiv").hide();
													newoptions="";
													newoptions += "<option value =''></option>";
													newoptions += "<option value ='1'>Company</option>";
													newoptions += "<option value ='2'>People</option>";
													newoptions += "<option value ='3'>Deals</option>";
													$('#convertto').append(newoptions);
													$("#convertto").select2('val',"").trigger('change');
													$.ajax({
														url:base_url+"Lead/leaddatafetch?leadid="+datarowid+"&industryid="+softwareindustryid, 
														dataType:'json',
														async:false,
														cache:false,
														success: function(data) {
															var lname = data['lanme'];
															var accname = data['accname'];
															var convertid = data['convertid'];
															$("#accname").val(accname);
															$("#contname").val(lname);
															$("#oppname").val(accname);
															$("#convertid").val(convertid);
														}
													}); 
												}else{
													$('#convertto').empty();
													$('input[name=acccheck]').trigger('click').prop('checked', true).val('yes');
													$('input[name=contcheck]').trigger('click').prop('checked', true).val('yes');												
													$('#opportunitycheckbox').prop('checked', false);												
													$("#opportunitydetailsdiv").hide();
													newoptions="";
													newoptions += "<option value =''></option>";
													newoptions += "<option value ='1'>Account</option>";
													newoptions += "<option value ='2'>Contact</option>";
													newoptions += "<option value ='3'>Opportunity</option>";
													$('#convertto').append(newoptions);
													$("#convertto").select2('val',"").trigger('change');
													$.ajax({
														url:base_url+"Lead/leaddatafetch?leadid="+datarowid+"&industryid="+softwareindustryid, 
														dataType:'json',
														async:false,
														cache:false,
														success: function(data) {
															var lname = data['lanme'];
															var accname = data['accname'];
															var convertid = data['convertid'];
															if(accname != '') {
																$("#accname").val(accname);
															} else {
																$("#accname").val(lname);
															}
															$("#contname").val(lname);
															$("#oppname").val(accname);
															$("#convertid").val(convertid);
														}
													}); 
												}												
											}
											Materialize.updateTextFields();
										} else {
											alertpopup('Cant convert the lead. Because its already converted');
										}
									}
								});
							} else {
								refreshgrid()
								$('#convertleadovrelay').hide();
								alertpopup('Cant convert the lead. Because its already converted');
							}
						}
					});
				} else {
					alertpopup("Please select a row");
				}
			});
		}
		$("#detailedviewicon").click(function() {
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid)  {
				$.ajax({
					url:base_url+"Lead/ledconversiondenied?dataprimaryid="+datarowid, 
					cache:false,
					success: function(data) {
						var nmsg =  $.trim(data);
						if(nmsg != 'Denied'){
							//Function Call For detail view
							$("#processoverlay").show();
							froalaset(froalaarray);
							leaddetailviewdatafetchfun(datarowid);
							showhideiconsfun('summryclick','leadcreationformadd');
							//For tablet and mobile Tab section reset
							$('#tabgropdropdown').select2('val','1');
							setTimeout(function() {
								 $('#tabgropdropdown').select2('enable');
							 },50);
						} else {
							refreshgrid()
							$('#convertleadovrelay').hide();
							alertpopup('Permission denied');
						}
					}
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null) {
				 setTimeout(function() {
					$("#processoverlay").show();
					froalaset(froalaarray);
					leaddetailviewdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','leadcreationformadd');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							showhideiconsfun('editfromsummryclick','leadcreationformadd');
						} else {
							alertpopup('Converted Lead cant be edit');
						}
					}
				});
			} else{
				alertpopup('Please select the row');
			}
		});
	}
	{// Dashboard Icon Click Event	
		$("#dashboardicon").click(function() {
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							$("#processoverlay").show();
							$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
							$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
							$.getScript('js/Home/home.js',function(){
								dynamicchartwidgetcreate(datarowid);
							});
							$("#dashboardcontainer").removeClass('hidedisplay');
							$('.actionmenucontainer').addClass('hidedisplay');
							$('.fullgridview').addClass('hidedisplay');
							$('.footercontainer').addClass('hidedisplay');
							$(".forgetinggridname").addClass('hidedisplay');
							$("#processoverlay").hide();
						} else {
							alertpopup('You cant view the converted lead dashboard');
						}
					}
				});
			} else {
				alertpopup("Please select a row");
			}
		});	
	}
	{// Close Add Screen
		var addcloseleadcreation =["closeaddform","leadcreationview","leadcreationformadd",""];
		addclose(addcloseleadcreation);
		var addcloseviewcreation =["viewcloseformiconid","leadcreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// View by drop down change
		$('#dynamicdddataview').change(function(){
			refreshgrid();
		});
	}
	{// Validation for lead Add  
		$('#dataaddsbtn').click(function(e){
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		$("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$("#processoverlay").show();
				leadnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{// Validation for lead Update  
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		$("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				leaddataupdateinformationfun();
			},
			onFailure: function() {			
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{// For lead convert
		$("#convertto").change(function() {
			var convert = $("#convertto").val();
			if(convert == '3') {
				var check = $("#opportunitycheckbox").prop("checked");
				if(check) {
				} else {
					$("#opportunitycheckbox").trigger('click');
				}
			} else if(convert == '1') {
				var check = $("#acccheck").prop("checked");
				if(check) {
				} else {
					$("#acccheck").trigger('click');
				}
			} else if(convert == '2') {
				var check = $("#contcheck").prop("checked");
				if(check) {
				} else {
					$("#contcheck").trigger('click');
				}
			}
		});
	}
	{// Account check box click
		$("#acccheck").click(function() {
			check = $("#acccheck").prop("checked");
			if(check) {
				$("#accname").addClass('validate[required,funcCall[accountnamecheck]]');
				$("#accnamemanfield").find('.mandatoryfildclass').removeClass('hidedisplay');
				$("#acccheck").val('yes'); 
			} else {
				$("#accname").removeClass('validate[required,funcCall[accountnamecheck]]');
				$("#accnamemanfield").find('.mandatoryfildclass').addClass('hidedisplay');
				$("#acccheck").val('no');
				var val = $("#convertto").val();
				if(val == '1'){
					$("#convertto").select2('val','');
					$("#convertto").val('');
				}
			}
		});
		$("#accname").focusout(function(){
			var acccheck = $("#acccheck").val();
			if(acccheck == 'yes'){
				var accnameclass = $("#accname").hasClass("validate[required,funcCall[accountnamecheck]]");
				if(accnameclass == false){
					$("#accname").addClass('validate[required,funcCall[accountnamecheck]]');
				}
			}			
		});
	}
	{// Contact check box click
		$("#contcheck").click(function() {
			check = $("#contcheck").prop("checked");
			if(check) {
				$("#contname").addClass('validate[required]');
				$("#connamemanfield").find('.mandatoryfildclass').removeClass('hidedisplay');
				$("#contcheck").val('yes'); 
			} else {
				$("#contname").removeClass('validate[required]');
				$("#connamemanfield").find('.mandatoryfildclass').addClass('hidedisplay');
				$("#contcheck").val('no');
				var val = $("#convertto").val();
				if(val == '2'){
					$("#convertto").select2('val','');
					$("#convertto").val('');
				}
			}
		});
		$("#contname").focusout(function(){
			var contcheck = $("#contcheck").val();
			if(contcheck == 'yes'){
				var contnameclass = $("#contname").hasClass("validate[required]");
				if(contnameclass == false){
					$("#contname").addClass('validate[required]');
				}
			}			
		});
	}
	{// Validation for lead convert overlay submit
		$('#convertsbtbtn').click(function() {
			$("#convertoverlayvalidation").validationEngine('validate');
		});
		$("#convertoverlayvalidation").validationEngine({
			validateNonVisibleFields:false,
			onSuccess: function() {
				var acccheck = $("#acccheck").val();
				var contcheck = $("#contcheck").val();
				var oppcheck = $("#opportunitycheckbox").val();
				if(softwareindustryid == 2){
					if((acccheck == 'no' || contcheck == 'yes') || (acccheck == 'yes' || contcheck == 'no')){
						leadconvertfunction();
					}else{
						alertpopupdouble('Please Select the Module');
					}
				}else{
					if((acccheck == 'no' && contcheck == 'yes') || (acccheck == 'yes' && contcheck == 'no') || (acccheck == 'yes' && contcheck == 'yes')) {
						leadconvertfunction();
					} else {
						alertpopupdouble('Please Select Account or Contact Module');
					}
				}				
			},
			onFailure: function() {
				var dropdownid =['2','stage','convertto'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{// Lead Convert Close Overlay Button
		$("#leadconvertoverlayclose").click(function(){
			$('#convertleadovrelay').hide();
			resetFields();
			 $("#acccheck").removeAttr('disabled');
			 $("#contcheck").removeAttr('disabled');
			 $("#accname").removeAttr('readonly');
			 $("#contname").removeAttr('readonly');
			$("#convertto").select2('val','');
		});
	}
	{// Opportunity Check Box - Convert Overlay
		$('#opportunitycheckbox').click(function() {
			if($(this).is(':checked')) {
				$("#opportunitydetailsdiv").show();
				$("#oppname,#closedate,#stage").removeClass('validate[required]');
				$("#oppname").removeClass('validate[required,funcCall[opportunitynamecheck]]');
				$("#closedate,#stage").addClass('validate[required]');
				$("#oppname").addClass('validate[required],funcCall[opportunitynamecheck]');
				$("#amount").addClass('validate[required,custom[number]]');
				$("#opportunitycheckbox").val('yes');
			} else {
				$("#opportunitydetailsdiv").hide();
				$("#closedate,#stage").removeClass('validate[required]');
				$("#oppname").removeClass('validate[required],funcCall[opportunitynamecheck]');
				$("#amount").removeClass('validate[required,custom[number]]');
				$("#opportunitycheckbox").val('no');
				var val = $("#convertto").val();
				if(val == '3'){
					$("#convertto").select2('val','');
					$("#convertto").val('');
				}
			}
		});
	}
	{// Date Function In Convert Overlay
		var dateformetdata = $('#closedate').attr('data-dateformater');
		$('#closedate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: 0,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#closedate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
	{// Redirected form Home
		add_fromredirect("leadaddsrc",201);
	} 
	{// Lead-report session check
		// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique201"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() {
				leadeditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','leadcreationformadd');	
				sessionStorage.removeItem("reportunique201");
			},50);	
		}
	}
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					for(var i=0;i<data.length;i++) {
						if(data[i] != '') {
							mobilenumber.push(data[i]);
						}
					}
					if(mobilenumber.length > 1) {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").hide();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						mobilenumberload(mobilenumber,'smsmobilenumid');
					} else if(mobilenumber.length == 1) {
						sessionStorage.setItem("mobilenumber",mobilenumber);
						sessionStorage.setItem("viewfieldids",viewfieldids);
						sessionStorage.setItem("recordis",datarowid);
						window.location = base_url+'Sms';
					} else {
						alertpopup("Invalid mobile number");
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var validemail=validatecall(datarowid,'lead','donotcall');
			if(validemail == 'No') {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++){
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1) {
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					},
				});
			} else {
				alertpopup("This Contact Choosed the Do Not Call Option. So You can't Call to This Contact");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function() {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopupdouble("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{ //address type change
		$("#primaryaddresstype option[value='7']").remove();
		$("#secondaryaddresstype option[value='8']").remove();
		$("#primaryaddresstype,#secondaryaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
			Materialize.updateTextFields();
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,leadcreationviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			leadcreationviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Lead Creation View Grid Function
	function leadcreationviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#leadspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#leadspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#leadcreationviewgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.leadsheadercolsort').hasClass('datasort') ? $('.leadsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.leadsheadercolsort').hasClass('datasort') ? $('.leadsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.leadsheadercolsort').hasClass('datasort') ? $('.leadsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=lead&primaryid=leadid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#leadcreationviewgrid').empty();
				$('#leadcreationviewgrid').append(data.content);
				$('#leadcreationviewgridfooter').empty();
				$('#leadcreationviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('leadcreationviewgrid');
				{//sorting
					$('.leadsheadercolsort').click(function() {
						$("#processoverlay").show();
						$('.leadsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#leadspgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.leadsheadercolsort').hasClass('datasort') ? $('.leadsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.leadsheadercolsort').hasClass('datasort') ? $('.leadsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#leadcreationviewgrid .gridcontent').scrollLeft();
						leadcreationviewgrid(page,rowcount);
						$('#leadcreationviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('leadsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						leadcreationviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#leadspgrowcount').change(function() {
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						leadcreationviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{//Redirected form Home
					var leadideditvalue = sessionStorage.getItem("leadidforedit"); 
					if(leadideditvalue != null){
						setTimeout(function(){
							leadeditdatafetchfun(leadideditvalue);
							sessionStorage.removeItem("leadidforedit");
						},100);	
					}
				}
				//Material select
				$('#leadspgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('leadcreationview','leadcreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','leadcreationformadd');
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			//For Left Menu Confirmation
			discardmsg = 1;
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							//Function Call For Edit
							$("#processoverlay").show();
							froalaset(froalaarray);
							leadeditdatafetchfun(datarowid);
							showhideiconsfun('editclick','leadcreationformadd');
							//For tablet and mobile Tab section reset
							$('#tabgropdropdown').select2('val','1');
							fortabtouch = 1;
							//For Left Menu Confirmation
							discardmsg = 1;
							Operation = 1; //for pagination
						} else {
							alertpopup('Converted records cant be edit');
						}
					}
				});
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#leadcreationviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$.ajax({
					url:base_url+"Lead/leaddatastatusget?leadid="+datarowid+"&industryid="+softwareindustryid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) != 'FAILED') {
							$('#primarydataid').val(datarowid);
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						} else {
							alertpopup('Converted records cant be delete');
						}
					}
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			leadrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#leadspgnum li.active').data('pagenum');
		var rowcount = $('ul#leadspgnumcnt li .page-text .active').data('rowcount');
		leadcreationviewgrid(page,rowcount);
	}
}
{// New data add submit function
	function leadnewdataaddfun() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Lead/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					var dynview = $("#dynamicdddataview").val();
					$(".ftab").trigger('click');
					resetFields();
					$('#leadcreationformadd').hide();
					$('#leadcreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					$("#dynamicdddataview").select2('val',dynview);
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
					$("#processoverlay").hide();
				} 
				else if (nmsg == "false") { }
			},
		});
	}
}
{// Old information show in form
	function leadeditformdatainformationshow(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Lead/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#leadcreationformadd').hide();
					$('#leadcreationview').fadeIn(1000);
					refreshgrid();
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
					$("#processoverlay").hide();
				} else {
					addslideup('leadcreationview','leadcreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					primaryaddvalfetch(datarowid);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					editordatafetch(froalaarray,data);
					$("#processoverlay").hide();
				}
			}
		});
	}
}
{// Old information show in form
	function leadconvertformdatainformationshow(datarowid) {
		var elementsname = $('#elementsname').val();$('#elementsconvertname').val(elementsname);
		var elementstable = $('#elementstable').val();$('#elementsconverttable').val(elementstable);
		var elementscolmn = $('#elementscolmn').val();$('#elementsconvertcolmn').val(elementscolmn);
		var elementpartable = $('#elementspartabname').val();$('#elementsconvertpartabname').val(elementpartable);
		var resctable = $('#resctable').val();
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid) {
		$.ajax({
			url:base_url+"Lead/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
				var textboxname = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
				var dropdowns = ['2','primaryaddresstype','secondaryaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
	}
}
{// Update old information
	function leaddataupdateinformationfun() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditordataget(editorname);
		var formdata = $("#dataaddform").serialize({ checkboxesAsBools: true });
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Lead/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == "TRUE") {
					resetFields();
					$(".ftab").trigger('click');
					$('#leadcreationformadd').hide();
					$('#leadcreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Delete data information
	function leadrecorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url+"Lead/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == "TRUE") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Lead convert process
	function leadconvertfunction() {
		$('#processoverlay').show();
		var convertid = $("#convertid").val();
		var accountcheck = $("#acccheck").val();
		var contactcheck = $("#contcheck").val();
		var opportunitycheck = $("#opportunitycheckbox").val();
		if(accountcheck == ''){accountcheck = 'no'; }
		if(contactcheck == ''){contactcheck = 'no'; }
		if(opportunitycheck == ''){opportunitycheck = 'no'; }
		var convertto = $("#convertto").val();
		//form serialize
		var convertformdata = $("#leadconvertoverlayform").serialize();
		var amp = '&' ;
		var convertdata = amp + convertformdata;
		if((convertto == '3' && opportunitycheck != 'no') || (convertto == '2' && contactcheck != 'no') || (convertto == '1' && accountcheck != 'no')) {
			$.ajax( {
				url:base_url+"Lead/leadconvertfunction", 
				data: "datas=" + convertdata+"&convertid="+convertid+"&accountcheck="+accountcheck+"&contactcheck="+contactcheck+"&opportunitycheck="+opportunitycheck+"&convertto="+convertto,
				type: "POST",
				async:false,
				cache:false,
				success: function(data) {
					var nmsg =  $.trim(data);
					if (nmsg == "TRUE") {
						$("#convertto").select2('val','');
						refreshgrid();
						$("#convertleadovrelay").fadeOut();
						$('#processoverlay').hide();
						alertpopup('Record Converted Successfully');
						resetFields();
					} else {
						$("#convertleadovrelay").fadeOut();
						$('#processoverlay').hide();
						alertpopup("Cant convert the Row");
					}
				}
			});	
		} else {
			alertpopupdouble('Please Select the Module Choosed in Convert To Drop Down');
		}
	}
}
{// Edit Function 
	function leadeditdatafetchfun(datarowid) {	
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields(); 
		firstfieldfocus();
		leadeditformdatainformationshow(datarowid);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Edit Function 
	function leaddetailviewdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields(); 
		firstfieldfocus();
		leadeditformdatainformationshow(datarowid);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function 
	function leadclonetdatafetchfun(datarowid) {
		showhideiconsfun('editclick','leadcreationformadd');		
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields(); 
		firstfieldfocus();
		leadeditformdatainformationshow(datarowid);
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		//For Left Menu Confirmation
		discardmsg = 1;
		Materialize.updateTextFields();
	}
}
//set address
function setaddress(id) {
	var value=$('#'+id+'').val();
	if(id == 'primaryaddresstype') {
		var type='primary';
	} else {
		var type='secondary';
	}
	//swap the billing to shipping
	if(value == 8){
		$('#primaryaddress').val($('#secondaryaddress').val());
		$('#primarypincode').val($('#secondarypincode').val());
		$('#primarycity').val($('#secondarycity').val());
		$('#primarystate').val($('#secondarystate').val());
		$('#primarycountry').val($('#secondarycountry').val());	
	}
	//swap the shipping to billing
	else if(value == 7){
		$('#secondaryaddress').val($('#primaryaddress').val());
		$('#secondarypincode').val($('#primarypincode').val());
		$('#secondarycity').val($('#primarycity').val());
		$('#secondarystate').val($('#primarystate').val());
		$('#secondarycountry').val($('#primarycountry').val());	
	}
}
{// Validate the module email and do not call
	function validateemailanddonotmail(id,table,field,donotmail) {
		var status='';
		$.ajax({
			url: base_url + "Base/checkvalidemailanddonotmail?id="+id+"&table="+table+"&field="+field+"&donotmail="+donotmail,
			async:false,
			cache:false,
			success: function(msg) {
				status =msg;
			},
		});
		return status;
	}
	//validate do not call
	function validatecall(id,table,field) {
		var status='';
		$.ajax({
			url: base_url + "Base/validatecall?id="+id+"&table="+table+"&field="+field,
			async:false,
			cache:false,
			success: function(msg)  {
				status =msg;
			},
		});
		return status;
	}
}
//account unique name check
function accountnamecheck() {
	var primaryid = '';
	var accname = $("#accname").val();
	var elementpartable = 'account';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data: {accname: accname, elementspartabname: elementpartable, primaryid: primaryid},
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Account name already exists!";
				}
			} else {
				return "Account name already exists!";
			}
		} 
	}
}
//opportunity unique name check
function opportunitynamecheck() {
	var primaryid = '';
	var accname = $("#oppname").val();
	var elementpartable = 'opportunity';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Opportunity name already exists!";
				}
			} else {
				return "Opportunity name already exists!";
			}
		} 
	}
}