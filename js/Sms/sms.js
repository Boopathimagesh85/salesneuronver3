$(document).ready(function() {	
	$(document).foundation();
	{// Grid Calling
		smscreategrid();
		//crud action
		crudactionenable();
	}
	$("#dataupdatesubbtn").hide();
	//Overlay Side Bar Code
	$(".sidebaricons").click(function()	{
		var subfomval = $(this).data('subform');
		$(".hiddensubform").hide();
		$("#subformspan"+subfomval+"").fadeIn('slow');
	});	
	$("#signatureiddivhid").change(function()	{
		Materialize.updateTextFields();
	});
	$('#reloadicon').click(function() {
		refreshgrid();
	});
	//SMS Settings
	$("#smssettingsicon").click(function(){
		window.location =base_url+'Smssettings';
	});
	maingridresizeheightset('smscreategrid');
	dndnumbers= [];
	nondndnumbers= [];
	globalsenderid = 0;
	{// Close Add Screen
		var addcloseoppocreation =["closeaddform","smscreationview","smscreationformadd"]
		addclose(addcloseoppocreation);	
		var addcloseviewcreation =["viewcloseformiconid","smscreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);	
	}
	// Maindiv height width change
	maindivwidth();
	//onclose Move home page 
	$('#closesmsoverlayform,#closemailoverlayform').click(function(){
		window.location =base_url+'Home';
	});
	{	//new template
		$('#addnewtemplate').click(function(){
			$('#templatetype').val('1'); //1 for new template
		});
		//old template
		$('#gooldtemplate').click(function(){
			$('#templatetype').val('0'); //0 for old template
		});
		//sms overlay close
		$('#closesmsoverlayform').click(function(){
			clearform('clearsmsvalues');
		});
		//template name change
		$('#leadtemplateid').change(function() {
			var tempid = $('#leadtemplateid').val();
			if(tempid != '') {
				var smssendtype = $("#smssendtypeid").val();
				if(smssendtype) {
					$("#formfields").empty();
					var typeid = $("#leadtemplateid").find('option:selected').data('typeid');
					if(typeid == 4){
						$("#formfields").attr('readonly', true);
					}else{
						$("#formfields").attr('readonly', false);
					}				
					$.ajax( {
						url:base_url+"Sms/fetchtemplatedetails?templid="+tempid, 
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {                
							var msgfilename = data.msg;
							templateeditervalueget(msgfilename);
							//For record drop down value load
							if(typeid == '3') {
								var mid = data.moduleid;
								reordidddvaluefetch(mid);
							}
						}
					});
					$("#signatureid").trigger('change');
				} else {
					$('#leadtemplateid').select2('val','');
					alertpopup('Please Select the Sms Send Type');
				}
			}
		});
	}
	//send sms
	$('#dataaddsbtn').click(function() {
		$("#formaddwizard").validationEngine('validate');
	});
	jQuery("#formaddwizard").validationEngine({
		onSuccess: function() {
			var apikey = $("#senderid").find('option:selected').data('apikey');
			var sendername = $("#senderid").find('option:selected').data('sendername');
			var smscount= $("#smscount").val();
			var content = $('#leadtemplatecontent').val();
			var regExp = /\{([^}]+)\}/g;
			var datacontent = content.match(regExp);
			var formdata= $("#dataaddform").serialize();
			var amp = '&';
			var smsdatas = amp + formdata ;
			$("#processoverlay").show();
			$.ajax({                       
				url:base_url+"Sms/leadsmssend",  
				data: "data="+smsdatas+"&apikey="+apikey+"&sendername="+sendername+"&smscount="+smscount+"&datacontent="+datacontent, 
				dataType:'json',
				type:'POST',
				async:false,
				cache:false,
				success: function(msg) {
					if(msg == 'success') {
						resetFields();
						$('#smscreationformadd').hide();
						$('#smscreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						refreshgrid();
						$("#processoverlay").hide();
						alertpopup('SMS sent Successfully...');
					} else if(msg == 'Credits') {
						resetFields();
						$('#smscreationformadd').hide();
						$('#smscreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						refreshgrid();
						$("#processoverlay").hide();
						alertpopup('Your SMS credits is too low.Please update your Addons SMS Credits');
					} else if(msg == 'Your IP Address is not valid') {
						resetFields();
						$('#smscreationformadd').hide();
						$('#smscreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						refreshgrid();
						$("#processoverlay").hide();
						alertpopup(msg);
					}
				},                
			}); 
		},
		onFailure: function() {
			var dropdownid =['2','smstypeid','senderid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//schedule outer sms
	$('#closetimesmsoverlayform').click(function() {
		$("#schedulesmsoverlay").fadeOut();
	});
	$('#opentimesmsoverlayform').click(function() {
		$("#leadschsmscontainervalidate").validationEngine('validate');
	});
	jQuery("#leadschsmscontainervalidate").validationEngine({
		onSuccess: function(){
			$("#schedulesmsoverlay").fadeIn();
		},
		onFailure: function(){	
		}
	});
	//schedule inner sms (date selection)
	$('#schdulesmsbtn').click(function() {
		$("#leadschedulesmscontainervalidate").validationEngine('validate');
	});
	jQuery("#leadschedulesmscontainervalidate").validationEngine({
		onSuccess: function() {
			var formdata= $("#sendsmsform").serialize();
			var amp = '&';
			var smsdatas = amp + formdata ;
			$.ajax({                       
				url:base_url+"Sms/smssend",  
				data: "data="+ smsdatas, 
				cache:false,
				success: function(msg) {                
					if(msg =='success') {
						clearform('clearsmsvalues');
						$("#schedulesmsoverlay").fadeOut();
					} else if(msg) {
						alertpopup(msg);
					}
				},                
			});
		},
		onFailure: function() {	}
	});
	{//signature file value get
		$("#signatureid").change(function() {
			var sigid = $("#signatureid").val();
			if(sigid != '') {
				var smssignature = $(this).find('option:selected').data('smssig');
				var oldcontent = $("#leadtemplatecontent").val();
				var message = oldcontent+''+smssignature;
				var lengthofchar = message.length;
				$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
				smscountdatafetch(message,lengthofchar);
				$("#leadtemplatecontent").val(message);
				$('#description').val(message);
			}
		});
	}
	{//message focus out
		$("#leadtemplatecontent").focusout(function() {
			var recordid = $('#formfields').val();
			if(recordid != '') { 
				recordid = recordid;
			} else {
				recordid = '';
			}
			var content = $('#leadtemplatecontent').val();
			var regExp = /\{([^}]+)\}/g;
			var datacontent = content.match(regExp);
			var moduleid = $("#leadtemplateid").find('option:selected').data('moduleid');
			var parenttablename = $('#formfields').find('option:selected').data('modulename');
			var tempid = $('#leadtemplateid').val();
			if(datacontent != null) {
				var newcontent = "";
				if(isNaN(recordid)) {
					$.ajax({
						url:base_url+'Sms/smsmergcontinformationfetch',
						data:'content='+datacontent+"&recordid="+recordid+"&moduleid="+moduleid+"&parenttablename="+parenttablename+"&tempid="+tempid,
						type:'POST',
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							var fcontent = '';
							for(var j=0;j < data.length; j++) {
								if(fcontent != '') {
									fcontent = fcontent+'|||'+data[j];
								} else {
									fcontent = data[j];
								}
							}
							$("#description").val(fcontent);
							$("#defaultsubseditorval").val(fcontent);
							setTimeout(function(){
								Materialize.updateTextFields();
							},100);
						},
					});
				}
			} else {
				$('#description').val(content);
				$("#defaultsubseditorval").val(content);
				setTimeout(function(){
					Materialize.updateTextFields();
				},100);
			}
		});
	}
	//module fields change events
	$('#formfields').change(function() {
		var recordid = $(this).val();
		var data = $('#formfields').select2('data');
		var finalResult = [];
		for( item in $('#formfields').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#recordsid").val(selectid);
		if(!selectid.length){
			recordid = '1';
		}
		if(recordid != '' && recordid != null) { 
			recordid = recordid;
		} else {
			recordid = '';
		}
		var ncontent = "";
		var fcontent = "";
		var tempid = $("#leadtemplateid").val();
		var moduleid = $("#leadtemplateid").find('option:selected').data('moduleid');
		var parenttablename = $('#formfields').find('option:selected').data('modulename');
		if(recordid != '1') {
			if( $(this).val() != "" || $(this).val() != null ) {
				var content = $('#leadtemplatecontent').val();
				var regExp = /\{([^}]+)\}/g;
				var datacontent = content.match(regExp);
				if(datacontent != null){
					var newcontent = "";
					$.ajax({
						url:base_url+'Sms/smsmergcontinformationfetch',
						data:'content='+datacontent+"&recordid="+recordid+"&moduleid="+moduleid+"&parenttablename="+parenttablename+"&tempid="+tempid,
						type:'POST',
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							for(var j=0;j < data.length; j++) {
								var ncontent = data[j];
								if(fcontent != '') {
									fcontent = fcontent+'|||'+data[j];
								} else {
									fcontent = data[j];
								}
							}
							$("#description").val(fcontent);
							$("#defaultsubseditorval").val(fcontent);
						},
					});
				} else {
					$('#description').val(ncontent);
					$('#defaultsubseditorval').val(content);
				}
			}
		} else {
			var content = $('#leadtemplatecontent').val();
			$('#description').val(content);
			$('#defaultsubseditorval').val(content);
		}
		mobilenumberget(selectid);
	});
	{//unique mobile number get
		$("#communicationtodivhid").append('<span id="uniqueid" class="icon icon-filter" style="color:#546E7A;font-size:1rem;cursor:pointer;position:relative;float:right; padding-top:5px;top:3px;" title="Unique"></span><span id="dndnumber" class="icon icon-cancel-circle" title="DND" style="color:#546E7A;font-size:1rem;cursor:pointer;position:relative;float:right; padding-top:5px;top:3px;margin-right:15px;"></span>'); 
		$("#uniqueid").click(function() {
			var mobilenum = $("#communicationto").val();
			if(mobilenum != '') {
				setTimeout(function(){
					var splitvalues = mobilenum.split(',');
					var count = splitvalues.length;
					var uniqumobilenum = [];
					$.each(splitvalues, function(i, value){
						if($.inArray(value, uniqumobilenum) === -1) uniqumobilenum.push(value);
					});
					$("#communicationto").val('');
					$("#communicationto").val(jQuery.unique( uniqumobilenum ));
				},100);
			} else {
				alertpopup('Please enter the mobile number for filter');
			}
		});
		$("#dndnumber").click(function() {
			var mobilenum = $("#communicationto").val();
			var senderid = $("#senderid").val();
			$("#totalmobilenubers").val('');
			$("#totalnondndnumbers").val('');
			$("#totaldndnumbers").val('');
			$("#nondndnumbers").val('');
			$("#dndnumbers").val('');
			dndnumbers= [];
			nondndnumbers= [];
			if(mobilenum != '' ) {
				if(senderid != '') {
					var apikey = $("#senderid").find('option:selected').data('apikey');
					if(mobilenum != '') {
						$.ajax({
							url:base_url+"Sms/dndmobilenumbercheck?mobilenum="+mobilenum+"&apikey="+apikey, 
							dataType:'json',
							async:false,
							cache:false,
							success: function(creditdata) {    
								$('#mobiledndovrelay').fadeIn();					
								var len = creditdata.data.length;
								for(var i=0;i<len;i++) {
									if(creditdata.data[i]['status'] == 'YES IN DND') {
										var dndnum = creditdata.data[i]['number'];
										dndnumbers.push(dndnum);								
									} else if(creditdata.data[i]['status'] == 'NOT IN DND'){
										var nondndnum = creditdata.data[i]['number'];
										nondndnumbers.push(nondndnum);		
									}
								}
								var splitvalues = mobilenum.split(',');
								var count = splitvalues.length;
								var uniqumobilenum = [];
								$.each(splitvalues, function(i, value){
									if($.inArray(value, uniqumobilenum) === -1) uniqumobilenum.push(value);
								});
								$("#totalmobilenubers").val(uniqumobilenum.length);
								$("#totalnondndnumbers").val(nondndnumbers.length);
								$("#totaldndnumbers").val(dndnumbers.length);
								$("#nondndnumbers").val(nondndnumbers);
								$("#dndnumbers").val(dndnumbers);
							}
						});
					}
				} else {
					alertpopup('Please select the sender id');
				}
			} else {
				alertpopup('Please enter the mobile number for check dnd');
			}
		});
	}
	{//dnd number overlay close
		$("#dndmobileoverlayclose").click(function() {
			$("#totalmobilenubers").val('');
			$("#totalnondndnumbers").val('');
			$("#totaldndnumbers").val('');
			$("#nondndnumbers").val('');
			$("#dndnumbers").val('');
			dndnumbers= [];
			nondndnumbers= [];
			$('#mobiledndovrelay').fadeOut();
		});
		$("#dndyesbtn").click(function() {
			$('#mobiledndovrelay').fadeOut();
			dndnumbers= [];
			nondndnumbers= [];
		});
		$("#dndnobtn").click(function() {
			var nondndnum = $("#nondndnumbers").val();
			$("#communicationto").val(nondndnum);
			$('#mobiledndovrelay').fadeOut();
		});
	}
	{//mobile number validation
		$("#communicationto").focusout(function() {
			var regExp = /^[-,0-9]+$/;
			var testvalue = $('#communicationto').val();
		});
	}
	{//Template Count
		$('#leadtemplatecontent').keydown(function(e) {
			$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
			var $this = $(this);
			setTimeout(function() {
				var text = $this.val();
				var lengthofchar = text.length;
				smscountdatafetch(text,lengthofchar);
				if(lengthofchar > 0) {
				} else { 
					$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
				}
			}, 0);
		});
	}
	{//Time format
		$('#communicationtime').timepicker({
			'step':15,
			'timeFormat': 'H:i',
			'scrollDefaultNow': true,
		});
	}
	{//time select
		$("#communicationtime").click(function() {
			if($("#communicationdate").val() != '') {	
				var sdate = $("#communicationdate").val();
				var stime= $("#communicationtime").val();
				$.ajax({
					url: base_url+"Sms/scheduledatetimevalidation?sdate="+sdate+"&stime="+stime,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						
					},
				});		
			} else {
				alertpopup('Please Select the Schedule Date');
			}
		});	
	}
	{//form clear
		$("#formclearicon").click(function() {
			$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
			$("#formfields").empty();
			$("#formfields").val('');
			$("#formfields").select2('val','');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			//Default Sender ID Drop down Value Fetch
			$("#smstypeid").select2('val','2');
			defaultsenderidvalueget();
		});
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			smscreategrid();
		});
	}
	//schedule edit overlay submit
	$("#scheduleeditsubmit").click(function() {
		$("#validatescheduleeditoverlay").validationEngine('validate');
	});
	jQuery("#validatescheduleeditoverlay").validationEngine({
		onSuccess: function() {
			var formdata= $("#scheduleeditoverlayform").serialize();
			var amp = '&';
			var scheduleeditdatas = amp + formdata ;
			$.ajax({
				url: base_url + "Sms/scheduleedit?datas="+scheduleeditdatas,
				dataType:'json',
				async:false,
				cache:false,
				success: function(creditdata) {
					if(creditdata != '') {
						if(creditdata == 'empty') {
							$("#scheduleeditoverlay").hide();
							alertpopup('Sorry error occured. reload it and try later.');
						} else {
							$("#scheduleeditoverlay").hide();
							var status = creditdata.status;
							var message = creditdata.message;
							if(status == 'OK') {
								alertpopup(message);
							} else if(status == 'A406') {
								alertpopup(message);
							} else if(status == 'A431') {
								alertpopup(message);
							} 
						}
					} else {
						$("#scheduleeditoverlay").hide();
						alertpopup('Sorry error occured. reload it and try later.');
					}
				},
			});
		},
		onFailure: function() {
		}
	});
	//schedule edt overlay close
	$("#closescheduleoverlay").click(function() {
		clearform('resetoverlay');
		$("#scheduleeditoverlay").fadeOut();
	});
	$('#scheduleedittime').timepicker({
		showTimepicker: true,
		timeOnly:true,
		showSecond: false,
		step:15,
		timeFormat: 'H:i',
		step:15,
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			}
		},
		onClose: function () {
			$('#scheduleedittime').focus();
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	{//resend sms
		$("#resendicon").click(function() {
			var datarowid = $('#smscreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('smscreationview','smscreationformadd');
				resetFields();
				$(".updatebtnclass").addClass('hidedisplay');
				$(".addbtnclass").removeClass('hidedisplay');
				showhideiconsfun('addclick','smscreationformadd');	
				$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
				//signature drop down load
				smssignaturevalueget('signatureid');
				//form field first focus
				firstfieldfocus();
				var elementname = $('#elementsname').val();
				elementdefvalueset(elementname);
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				//Default Sender ID Drop down Value Fetch
				resendsmsdatafetch(datarowid);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				//for touch
				fortabtouch=0;
				//For Keyboard Shortcut Variables
				viewgridview = 0;
				addformview = 1;
			} else { 
				alertpopup("Please select a row");
			}
		});
	}
	{//sms send type change
		$("#smssendtypeid").change(function() {
			var typeid = $("#smssendtypeid").val();
			if(typeid == '2'){
				$('#senderid').removeClass('validate[required]');
				$('#senderid').addClass('validate[required]');
			} else {
				$('#senderid').removeClass('validate[required]');
			}
			smssenderidvalueget('senderid');
		});
	}
	{//senderid  change
		$("#senderid").change(function() {
			var sendsmstype = $("#smssendtypeid").val();
			if(sendsmstype) {
				smstemplatevalueget('leadtemplateid');
			} else {
			}
		});
	}
	{//Mass update/ Mass Delete /Export Conversion From All modules
		var mobilenumber = sessionStorage.getItem("mobilenumber"); 
		var moduleid = sessionStorage.getItem("viewfieldids"); 
		var recordis = sessionStorage.getItem("recordis"); 
		if(moduleid != null){
			setTimeout(function(){
				$("#addicon").trigger('click');
				$('#communicationto').val(mobilenumber);
				$('#smssendtypeid').select2('val','2').trigger('change');
				templatevaluefetch('2',moduleid);
				Materialize.updateTextFields();
				sessionStorage.removeItem("mobilenumber");
				sessionStorage.removeItem("viewfieldids");
				sessionStorage.removeItem("recordis");
			},100);				
		}
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,smscreategrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			smscreategrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{// Main Grid Function
	function smscreategrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#smspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#smspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#smscreategrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=crmcommunicationlog&primaryid=crmcommunicationlogid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#smscreategrid').empty();
				$('#smscreategrid').append(data.content);
				$('#smscreategridfooter').empty();
				$('#smscreategridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('smscreategrid');
				{//sorting
					$('.smsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.smsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#smspgsmscreategridnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#smscreategrid .gridcontent').scrollLeft();
						smscreategrid(page,rowcount);
						$('#smscreategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('smsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						smscreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#smspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						smscreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#smscreategrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#smspgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(){
			//signature drop down load
			smssignaturevalueget('signatureid');
			//sender id drop down load
			addslideup('smscreationview','smscreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','smscreationformadd');	
			$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			//Default Sender ID Drop down Value Fetch
			defaultsenderidvalueget();
			$("#smstypeid").select2('val','2');
			$('#senderid').removeClass('validate[required]');
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			//for touch
			fortabtouch=0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
	}
	{//schedule delete
		$("#deleteicon").click(function() {
			var datarowid = $('#smscreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$.ajax({
					url: base_url + "Sms/scheduledelete?rid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success: function(creditdata) {
						if(creditdata == 'SENT SMS') {
							alertpopup('You cant Delete the sent sms');
						} else {
							var status = creditdata.status;
							var message = creditdata.message;
							if(status == 'OK') {
								alertpopup(message);
							} else {
								alertpopup(message);
							}
						}
					},
				});
			} else { 
				alertpopup("Please select a row");
			}
		});
	}
	$("#editicon").click(function() {
		var datarowid = $('#smscreategrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			$.ajax({
				url: base_url + "Sms/scheduleeditvaluecheck?rid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data) == 'SCHEDULE SMS') {
						$("#scheduleeditdate,#scheduleedittime").val('');
						$("#scheduleeditoverlay").show();
						$("#primaryid").val(datarowid);
					} else {
						alertpopup('You cant reschedule the sent (or) received SMS');
					}
				},
			}); 
			Operation = 1; //for pagination
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smspgnum li.active').data('pagenum');
		var rowcount = $('ul#smspgnumcnt li .page-text .active').data('rowcount');
		smscreategrid(page,rowcount);
	}
}
{//general mail opn
	function dashboardmailsend() {
		var nicE = new nicEditors.findEditor('ldmailcontentarea');
		var bodycontent = nicE.getContent();
		$("#content").val(bodycontent);
		var path = base_url+'datamail.php'; 
		$.ajax( {
			type:'POST',
			url: path,
			data:$('#sendmailform').serialize(),
			cache:false,
			success:function(response) {
				if(response == "success") {
					var nicE = new nicEditors.findEditor('ldmailcontentarea');
					nicE.setContent('', {format : 'raw'});
					alertpopup('Mail Sent Successfully..');
					clearform('clearmailtemplate');
				} else {
					alertpopup("Mail Sending failed");
				}
			}
		});
	}
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{//Template value fetch
	function templateeditervalueget(filename) {
		if(filename != '') {
			$.ajax({
				url: base_url+"Sms/editervaluefetch?filename="+filename,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data != 'Fail') {
						var lengthofchar = data.length;
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						smscountdatafetch(data,lengthofchar);
						$("#leadtemplatecontent").val(data);
						$('#description').val(data);
						$("#defaultsubseditorval").val(data);
					} else {
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						$("#leadtemplatecontent").val('');
						$('#description').val('');
						$("#defaultsubseditorval").val('');
					}
				},
			});
		}
	}
	// Signature file get
	function signaturefileget(filename) {
		var oldcontent = $("#leadtemplatecontent").val();
		if(filename != '') {
			$.ajax({
				url: base_url+"Sms/editervaluefetch?filename="+filename,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					var sigtext = jQuery(data).text();
					var message = oldcontent+''+sigtext;
					if(data != 'Fail') {
						var lengthofchar = message.length;
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						smscountdatafetch(message,lengthofchar);
						$("#leadtemplatecontent").val(message);
						$('#description').val(message);
					} else {
						$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
						$("#leadtemplatecontent").val('');
					}
				},
			});
		}
	}
}
{//SMS Signature value get
	function smssignaturevalueget(ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sms/smssignatureddvalfetch",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-smssig",data[index]['smssig'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//Record Drop down value fetch
	function reordidddvaluefetch(mid){ 
		if(mid != '1') {
			$('#formfields').empty();
			$('#formfields').append($("<option></option>"));
			$.ajax({
				url: base_url+"Sms/smsrecordddvalfetch?mid="+mid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$('#formfields')
							.append($("<option></option>")
							.attr("data-modulename",data[index]['modulename'])
							.attr("value",data[index]['datasid'])
							.text(data[index]['dataname']));
						});
					}	
				},
			});
		}
	}
}
{//Sender Id drop down value get
	function smssenderidvalueget(ddname) {
		var typeid = $("#smssendtypeid").val();
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sms/smssenderddvalfetch?typeid="+typeid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-apikey",data[index]['apikey'])
						.attr("data-sendername",data[index]['dataname'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
						globalsenderid = data[index]['datasid'];
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//default sender id Value fetch
	function defaultsenderidvalueget() {
		$.ajax({
			url: base_url + "Sms/defaultsenderidget",
			dataType:'json',
			async: false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
				setTimeout(function(){
					var senderid = data.senderid;
					$("#senderid").select2('val',senderid);
					globalsenderid = senderid;
				},0);
				} else {
					
				}
			},
		});
	}
}
//mobile number get
function mobilenumberget(selectid) {
	var modulename = $('#formfields').find('option:selected').data('modulename');
	if((selectid != '' && modulename != '') || (selectid != null && modulename != undefined)) {
		$.ajax({
			url: base_url + "Sms/mobilenumberbasedonrecordid?module="+modulename+"&recordid="+selectid,
			dataType:'json',
			async: false,
			cache:false,
			success: function(data) {
				if(data != '') {
					$("#communicationto").val(data);
				}
			},
		});
	}
}
//sms template value fetch
function smstemplatevalueget(ddname) {
	var smssendtype = $("#smssendtypeid").val();
	var senderid = $("#senderid").val();
	if(senderid) {
		senderid = senderid;
	} else {
		senderid = globalsenderid;
	}
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	if((senderid != null && smssendtype =='2') || smssendtype == '3' || smssendtype == '4') {
		$.ajax({
			url: base_url+ "Sms/smstemplateddvalfetch?smssendtype="+smssendtype+"&senderid="+senderid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++){
						newddappend += "<option data-moduleid="+data[m]['moduleid']+" data-typeid="+data[m]['typeid']+" value = '"+data[m]['datasid']+"'>"+data[m]['dataname']+" </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	} else {
		$('#smssendtypeid').select2('val','');
		alertpopup('Please Select the Sender Id');
	}
}
//sms count get
function smscountdatafetch(text,lengthofchar) {
	var typeid = $("#smstypeid").val();
	if( typeid == '' ) { 
		typeid == '2';
	}
	var smscount = 0;
	if(typeid == '2' || typeid == '3') {
		var maxcount = 1000;
		if(lengthofchar <= '160') {
			smscount = 1;
		} else if((lengthofchar > '160') && (lengthofchar <= '306')) {
			smscount = 2;	
		} else if((lengthofchar > '306') && (lengthofchar <= '459')) {
			smscount = 3;	
		} else if((lengthofchar > '459') && (lengthofchar <= '612')) {
			smscount = 4;	
		} else if((lengthofchar > '612') && (lengthofchar <= '765')) {
			smscount = 5;	
		} else if((lengthofchar > '765')&& (lengthofchar <= '922')) {
			smscount = 6;	
		} else if((lengthofchar > '922')) {
			smscount = 7;	
		}
	} else if(typeid == '4') {
		var maxcount = 500;
		if(lengthofchar <= '70') {
			smscount = 1;
		}else if((lengthofchar > '70') && (lengthofchar <= '134')) {
			smscount = 2;	
		} else if((lengthofchar > '134') && (lengthofchar <= '201')) {
			smscount = 3;	
		} else if((lengthofchar > '201') && (lengthofchar <= '268')) {
			smscount = 4;	
		} else if((lengthofchar > '268') && (lengthofchar <= '335')) {
			smscount = 5;	
		} else if((lengthofchar > '335') && (lengthofchar <= '402')) {
			smscount = 6;	
		} else if((lengthofchar > '402') && (lengthofchar <= '469')) {
			smscount = 7;	
		} else if((lengthofchar > '469')) {
			smscount = 8;	
		}
	}
	$("#smscount").val(smscount);
	$("#leadtemplatecontentdivhid").find('p').remove();
	$("#leadtemplatecontentdivhid").append('<p align="right" style="color:#546E7A;font-size:0.8rem; margin:0;opacity:1;"><span style="color:#546E7A;font-size:0.7rem; padding-left:30px;">'+lengthofchar+' characters(s) <span style="padding-left:30px;">'+smscount+' SMS</span><span style="padding-left:30px;"> Max '+maxcount+' Chars</span></span></p>');
	if(lengthofchar > 0) {
	} else { 
		$("#leadtemplatecontentdivhid").find('p').css('opacity','0');
	}
}
//resend data fetch
function resendsmsdatafetch(datarowid) {
	$.ajax({
		url: base_url + "Sms/resendsmsdatavalget?recordid="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var respstatus = data['fail'];
			if(respstatus != 'FAILED')
			{
				var datanames =['3','mobnum','type','senderid'];
				var textboxname =['3','communicationto','smstypeid','senderid'];
				var dropdowns = ['2','smstypeid','senderid'];
				textboxsetvalue(textboxname,datanames,data,dropdowns);
				var smssendtypeid = data.smssendtypeid;
				$("#smssendtypeid").select2('val',smssendtypeid).trigger('change');
				$('#senderid').select2('val',data.senderid).trigger('change');
				var template = data.templatesid;
				if($('#senderid').val() != '') {
					$("#leadtemplateid").select2('val',template);
					$("#leadtemplateid").trigger('change');
				}
				if($("#leadtemplateid").val() != '') {
					var signature = data.signatureid;
					if(signature > 1) {
						$("#signatureid").select2('val',signature);
						$("#signatureid").trigger('change');
					}
				}
				var recordid = data.recordid;
				$("#formfields").select2('val',recordid);
				$("#formfields").trigger('change');
			}
		},
	});
}
{//date Range
	function daterangefunction(generalstartdate,generatenddate){ 
		var dateformetdata = $('#'+generalstartdate+'').attr('data-dateformater');
		var startDateTextBox = $('#'+generalstartdate+'');
		var endDateTextBox = $('#'+generatenddate+'');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			minDate:0,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#'+generalstartdate+'').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.activityenddateformError,.taskenddateformError').remove();
					$('.hasDatepicker').removeClass('error');
				}
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
				$('#'+generatenddate+'').focus();
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
}
function templatevaluefetch(smssendtype,moduleid) {
	//var senderid = '2';
	var senderid = $("#senderid").find('option:selected').val();
	var ddname = 'leadtemplateid';
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	if(senderid) {
		senderid = senderid;
	} else {
		senderid = globalsenderid;
	}
	if((senderid != null && smssendtype =='2') || smssendtype == '3' || smssendtype == '4') {
		$.ajax({
			url: base_url+ "Sms/smstemplateddget?smssendtype="+smssendtype+"&senderid="+senderid+"&moduleid="+moduleid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++){
						newddappend += "<option data-moduleid="+data[m]['moduleid']+" data-typeid="+data[m]['typeid']+" value = '"+data[m]['datasid']+"'>"+data[m]['dataname']+" </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}