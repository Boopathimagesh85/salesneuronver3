$(document).ready(function()
{	
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		picklistdependencyaddgrid('','');
		firstfieldfocus();
		picklistdepcrudactionenable();
	}
	$("#picklistdependencyviewtoggle").hide();
	{
	//keyboard shortcut reset global variable
		viewgridview=0;
		addformview=0;
	}
	$( window ).resize(function() {
		innergridresizeheightset('picklistdependencyaddgrid');
	});
	//submit button show hide
	$('#picklistdependencysavebutton').show();
	$('#picklistdependencydataupdatesubbtn').hide();
	//module change events
	$('#picklistddmoduleid').trigger('change');
	$('#picklistddmoduleid').change(function(){
		var id = $(this).val();
		picklistdepvalueclear();
		dropdownmodulevalset('sourcefield','sourcefieldnameidhidden','modfiledcolumn','fieldlabel','modulefieldid','columnname','modulefield','moduletabid,uitypeid',''+id+',17|18');
		dropdownmodulevalset('targetfield','targetfieldnameidhidden','modfiledcolumn','fieldlabel','modulefieldid','columnname','modulefield','moduletabid,uitypeid',''+id+',17|18');
	});
	$("#sourcefield").change(function(){
		var moduleid  = $("#picklistddmoduleid").val();
		var data = $("#sourcefield").find('option:selected').data('modfiledcolumn');
		$("#sourcefieldid").val(data);
		var sid = $("#sourcefield").val();
		var tid = $("#targetfield").val();
		if(sid == tid) {
			$("#sourcefield").select2('val','');
			alertpopup('Source Field and Target Field should not be same');
		} else {
			sourcetablevalfetch(data,moduleid);
		}
	});
	{//pick list name drop down change
		$('#targetfield').change(function(){
			var moduleid  = $("#picklistddmoduleid").val();
			var data = $("#targetfield").find('option:selected').data('modfiledcolumn');
			var sid = $("#sourcefield").val();
			var tid = $("#targetfield").val();
			if(sid){
				if(sid != tid && tid != '') {
					targettablevalfetch(data,moduleid);
					picklistdepgridreload();
				} else {
					$("#targetfield").select2('val','');
					alertpopup('Source Field and Target Field should not be same');
				}
			} else {
				$("#targetfield").select2('val','');
				alertpopup('Please Select The Source Field');
			}
		});
	}
	{//validation for pick list data add 
		$('#picklistdependencysavebutton').mousedown(function(e){
			if(e.which == 1 || e.which === undefined) {
				$("#picklistdependencyformaddwizard").validationEngine('validate');
				// For Touch
				masterfortouch = 0;
			}
		});
		jQuery("#picklistdependencyformaddwizard").validationEngine({
			onSuccess: function() {
				$('#picklistdependencysavebutton').attr('disabled','disabled');
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['4','picklistddmoduleid','sourcefield','targetfield','sourcevalue'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//tool bar click function
		//picklist dependency reload
		$("#picklistdependencyreloadicon").click(function(){
			cleargriddata('picklistdependencyaddgrid');
			$("#picklistdependencydeleteicon,#picklistdependencysavebutton,#picklistdependencyaddicon").show();
			$("#picklistdependencydataupdatesubbtn").hide();
			resetFields();
			$('#picklistdependencyaddgrid').empty();
			$('#picklistdependencyaddgridfooter').empty();
		});
	}
	{//picklist dependency updation
		$('#picklistdependencydataupdatesubbtn').mousedown(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#picklistdependencyformeditwizard").validationEngine('validate');
			}
		});
		jQuery("#picklistdependencyformeditwizard").validationEngine({
			onSuccess: function() {
				picklistdependencydataupdatefunction();
			},
			onFailure: function() {
				var dropdownid =['4','picklistddmoduleid','sourcefield','targetfield','sourcevalue'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$('#tab2').click(function() {
		setTimeout(function(){
			$("input:text:visible:first").focus();
		},100);
		cleargriddata('picklistdependencyaddgrid');
	});
	setTimeout(function(){
		$("input:text:visible:first").focus();
	},100);	
	$('#picklistdependencyaddform').find('input, textarea, button, select').attr('disabled',true);
	{//add icon click
		$('#picklistdependencyaddicon').click(function(){
			$('#picklistdependencyaddform').find('input, textarea, button, select').attr('disabled',false);
		});
	} 
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//new data add submit function
function newdataaddfun() {
	var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
	var sourceid = $('#sourcefield').val(); 
	var targetfield = $('#targetfield').val(); 
	var sourcetableid = $("#sourcefieldid").val();
	var formdata = $("#picklistdependencyaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	if(sourceid != targetfield) {
		$.ajax({
			url: base_url + "Picklistdependency/newdatacreate",
			data: "datas=" + datainformation+"&tabnameid="+tabnameid+"&sourceid="+sourceid+"&sourcetableid="+sourcetableid,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$(".addsectionclose").trigger("click");
					alertpopup('Data Stored Successfully');
					$('#targetvalue').val('');
					$("#picklistdependencysavebutton").attr('disabled',false);
					picklistdepgridreload();
				} else {
					$("#picklistdependencysavebutton").attr('disabled',false);
				}	
			},
		});
	} else {
		alertpopup('Source Field and Target Field Should not be the same');
		$("#picklistdependencysavebutton").attr('disabled',false);
	}
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#picklistdependencysectionoverlay").removeClass("effectbox");
		$("#picklistdependencysectionoverlay").addClass("closed");
	});
}
//pick list grid reload
function picklistdepgridreload() {
	var srctabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
	var destabnameid = $('#sourcefield').find('option:selected').data('modfiledcolumn');
	var picklistddmoduleid = $('#picklistddmoduleid').val();
	picklistdependencyaddgrid(srctabnameid,destabnameid,picklistddmoduleid)
}
//Documents Add Grid
function picklistdependencyaddgrid(srctabnameid,destabnameid,picklistddmoduleid,page,rowcount) { 
	if(srctabnameid != '' && destabnameid !== '') {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#picklistdependencyaddgridwidth').width();
		var wheight = $('#picklistdependencyaddgridwidth').height();
		//col sort
		var sortcol = $("#picklistdependencysortcolumn").val();
		var sortord = $("#picklistdependencysortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.picklistdependencyvalueheadercolsort').hasClass('datasort') ? $('.picklistdependencyvalueheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.picklistdependencyvalueheadercolsort').hasClass('datasort') ? $('.picklistdependencyvalueheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.picklistdependencyvalueheadercolsort').hasClass('datasort') ? $('.picklistdependencyvalueheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Picklistdependency/picklistdepgridinfo?maintabinfo="+srctabnameid+"&primaryid="+srctabnameid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+picklistddmoduleid+"&checkbox=true&srctabname="+destabnameid+"&tartabname="+srctabnameid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#picklistdependencyaddgrid').empty();
				$('#picklistdependencyaddgrid').append(data.content);
				$('#picklistdependencyaddgridfooter').empty();
				$('#picklistdependencyaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('picklistdependencyaddgrid');
				{//sorting
					$('.picklistdependencyvalueheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.picklistdependencyvalueheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#picklistdependencyvaluepgnum li.active').data('pagenum');
						var rowcount = $('ul#picklistdependencyvaluepgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.picklistdependencyvalueheadercolsort').hasClass('datasort') ? $('.picklistdependencyvalueheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.picklistdependencyvalueheadercolsort').hasClass('datasort') ? $('.picklistdependencyvalueheadercolsort.datasort').data('sortorder') : '';
						$("#picklistdependencysortorder").val(sortord);
						$("#picklistdependencysortcolumn").val(sortcol);
						picklistdependencyaddgrid(srctabnameid,destabnameid,picklistddmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('picklistaddgrid',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						picklistdependencyaddgrid(srctabnameid,destabnameid,picklistddmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
					$('#picklistdependencyvaluepgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						picklistdependencyaddgrid(srctabnameid,destabnameid,picklistddmoduleid,page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$('#picklistdependencyvaluepgrowcount').material_select();
				//header check box
				$(".picklistdependencyvalue_headchkboxclass").click(function() {
					checkboxclass('picklistdependencyvalue_headchkboxclass','picklistdependencyvalue_rowchkboxclass');
					getcheckboxrowid();
				});
				//row based check box
				$(".picklistdependencyvalue_rowchkboxclass").click(function(){
					$('.picklistdependencyvalue_headchkboxclass').removeAttr('checked');
					getcheckboxrowid();
				});
				if(deviceinfo != 'phone') {
					{//Grid data row sorting
						var tablnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
						var len = tablnameid.length;
						var tablname = tablnameid.substring(0,(len-2));
						var sortable = document.getElementById("picklistdependencyvalue-sort");
						Sortable.create(sortable,{
							group: 'picklistdependencyelements',
							onEnd: function (evt) {
								sortorderupdate('picklistdependencyaddgrid',tablname,tablnameid,'sortorder');
						    },
						});
					}
				}
				var srctabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
				var hideprodgridcol = [srctabnameid];
				gridfieldhide('picklistdependencyaddgrid',hideprodgridcol);
			}
		});
	}
}
//sort order data update
function sortorderupdate(gridname,tablename,tableid,sortfieldname) {
	var datarowid = getgridallrowids(gridname);
	$.ajax({
		url:base_url + "Picklistdependency/datarowsorting",
		data:"datas=" + "&rowids="+datarowid+'&tablename='+tablename+'&primaryid='+tableid+'&sortfield='+sortfieldname,
		type:"POST",
		aync:false,
		cache:false,
		success: function(msg) {
		},
	});
}
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid() {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
}
function picklistdepcrudactionenable() {
	{//pick list add icon click
		$('#picklistdependencyaddicon').click(function(){
			$("#picklistdependencysectionoverlay").removeClass("closed");
			$("#picklistdependencysectionoverlay").addClass("effectbox");
			$('#picklistdependencydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#picklistdependencysavebutton').show();
		});
	}
	$("#picklistdependencyediticon").click(function(e) {
		e.preventDefault();
		var selrow = getselectedrowids('picklistdependencyaddgrid');
		if(selrow.length == 1) {
			var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
			var tabname = tabnameid.substring(0,tabnameid.length-2);
			var id =  $('#picklistdependencyaddgrid div.gridcontent div.active').attr('id');
			var name = getgridcolvalue('picklistdependencyaddgrid',selrow,'sourceid','');
			$("#picklistdependencyprimarydataid").val(id);
			$('#picklistdependencysavebutton,#picklistdependencydeleteicon,#picklistdependencyaddicon').hide();
			$('#picklistdependencydataupdatesubbtn').show().removeClass('hidedisplayfwg');
			picklistdepvalget(id);
			firstfieldfocus();
			$("#targetvalue").select2('val',name);
			// For Touch
			masterfortouch = 1;	 
			//Keyboard short cut
			saveformview = 1;
		} else {
			alertpopup("Please select single row");
		}
	});
	//delete icon
	$("#picklistdependencydeleteicon").click(function(e) {
		e.preventDefault();
		var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
		var datarowid = getselectedrowids('picklistdependencyaddgrid');
		if(datarowid.length>=1){
			combainedmoduledeletealert('picklistdependencydeleteyes');
			$("#picklistdependencydeleteyes").click(function(){
				$.ajax({
					url:base_url+"Picklistdependency/deletepicklistdepvalue",
					type:'POST',
					data:"id="+datarowid+"&tabnameid="+tabnameid,
					cache:false,
					success:function(data) {
						var nmsg =  $.trim(data);
						if(nmsg == 'TRUE') {
							picklistdepgridreload();
							$("#basedeleteoverlayforcommodule").fadeOut();
							$("#basedeleteoverlayforcommodule").hide();
							alertpopup('Record deleted successfully');
						} else {
							alertpopup("Delete Operation failed");
							$("#basedeleteoverlayforcommodule").fadeOut();
							$("#basedeleteoverlayforcommodule").hide();
						}
					}
				});
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}	
	});
	//base delete
	$("#basedeleteyes").click(function(){
		var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
		var datarowid = [];
		$('#picklistdependencyaddgrid div.gridcontent input:checked').each(function() {
			datarowid.push($(this).attr('data-rowid'));
		});
		$.ajax({
			url:base_url+"Picklistdependency/deletepicklistdepvalue",
			type:'POST',
			data:"id="+id+"&tabnameid="+tabnameid,
			cache:false,
			success:function(data) {
				if(data == true) {
					$("#picklistdependencyaddgrid").trigger('reloadGrid');
					$("#basedeleteoverlay").fadeOut();
				} else {
					alertpopup(" Cant delete the row");
				}
			}
		});
	});
}
{//refresh grid
	function picklistdependencyrefreshgrid() {
		var page = $('ul#picklistdependencyvaluepgnum li.active').data('pagenum');
		var rowcount = $('ul#picklistdependencyvaluepgnumcnt li .page-text .active').data('rowcount');
		picklistdependencyaddgrid('','','',page,rowcount);
	}
}
//pick list dependency value clear
function picklistdepvalueclear() {
	cleargriddata('picklistdependencyaddgrid');
	$("#sourcefield").select2('val',' ');
	$("#targetfield").select2('val',' ');
	$("#sourcevalue").select2('val',' ');
	$("#targetvalue").val('');
	$('#sourcevalue').empty();
	$('#sourcevalue').append($("<option></option>"));
	$('#sourcevalue').trigger('change');
}
//drop down values set for view
function dropdownmodulevalset(ddname,dataattrname,otherdataattrname,dataname,dataid,otherdataname,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Base/fetchdddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername='+otherdataname,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname,data[index]['otherdataattrname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		},
	});
}
//source filed value fetch
function sourcetablevalfetch(data,moduleid){
	$('#sourcevalue').empty();
	$('#sourcevalue').append($("<option></option>"));
	$.ajax({
		url:base_url+'Picklistdependency/sourcetablevaluefetch?data='+data+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#sourcevalue')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#sourcevalue').trigger('change');
		},
	});
}
//target dd val fetch
function targettablevalfetch(data,moduleid){
	$('#targetvalue').empty();
	$('#targetvalue').append($("<option></option>"));
	$.ajax({
		url:base_url+'Picklistdependency/sourcetablevaluefetch?data='+data+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#targetvalue')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#targetvalue').trigger('change');
		},
	});
}
//pick list updation
function picklistdependencydataupdatefunction()
{
	var picklistddmoduleid = $("#picklistddmoduleid").val();
	var sourcevalue = $("#sourcevalue").val();
	var sourcefieldid = $("#sourcefieldid").val();
	var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
	var targetid = $('#targetfield').val();
	var formdata = $("#picklistdependencyaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Picklistdependency/picklistdepdataupdatefun",
        data: "datas=" + datainformation+"&tabnameid="+tabnameid+"&sourcevalue="+sourcevalue+"&sourcefieldid="+sourcefieldid,
		type: "POST",
		cache:false,
        success: function(msg) {
        	$(".addsectionclose").trigger("click");
			alertpopup('Data Updated Successfully');
			$('#targetvalue').val('');
			$('#targetfield').val(targetid);
			$('#picklistddmoduleid').val(picklistddmoduleid);
			$("#sourcevalue").select2('val','');
			$("#targetvalue").select2('val','');
			//$('#picklistdependencyaddgrid').trigger('reloadGrid');
			picklistdepgridreload();
			$("#picklistdependencydataupdatesubbtn").hide();
			$("#picklistdependencysavebutton,#picklistdependencydeleteicon,#picklistdependencyaddicon").show();
			// For Touch
			masterfortouch = 0;	 
			//Keyboard short cut
			saveformview = 0;
		},
    });
}
//picklist dependency value get
function picklistdepvalget(id)
{
	var tabnameid = $('#targetfield').find('option:selected').data('modfiledcolumn');
	var sourceid = $("#sourcefieldid").val();
	var sourcename = $("#sourcefield").val();
	$.ajax({
        url: base_url + "Picklistdependency/picklistdepvaluegetfun",
        data: "&tabnameid="+tabnameid+"&id="+id+"&sourceid="+sourceid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
        	$("#picklistdependencysectionoverlay").removeClass("closed");
			$("#picklistdependencysectionoverlay").addClass("effectbox");
			jQuery('#sourcevalue').select2('val',data).trigger('change');
		},
    });		
}
//pickddnamecheck
function pickddnamecheck()
{
	var primaryid = $("#picklistdependencyprimarydataid").val();
	var accname = $("#targetvalue").val();
	var elementpartable = $('#targetfield').find('option:selected').data('modfiledcolumn');
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Picklistdependency/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Target field name already exists!";
				}
			} else {
				return "Target field name already exists!";
			}
		} 
	}
} 