$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//tag file global variables
		addchargeoperation = 'ADD'; //identify whether the add.charge is edit/add mode.
		MODE = 'ADD'; //for validation purpose
	}
	{// Grid Calling Function
		chitgenerationgrid();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	maingridresizeheightset('chitgenerationgrid');
	weight_round = $('#weight_round').val();
	amount_round = $('#amount_round').val();
	chitinteresttype = $('#chitinteresttype').val();
	addclickchitbookno = 1;
	mainprintemplateid = $('#mainprintemplateid').val();
	{
		$("#s2id_accountid").css('display','inline-block').css('width','95%');
		var icontabindex = $("#s2id_accountid .select2-focusser").attr('tabindex');
		$("#accountid").after('<span class="innerfrmicon" id="accountimagedisplay" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">broken_image</i></span>');
	}
	{ // action icons
		//add icon
		$("#addicon").click(function() {
			resetFields();
			addslideup('chitgenerationcreationview','chitgenerationformadd');
			$("#resetfilter").trigger('click');
			$('#scan_chitbookid,#chitschemeid').val('');
			getcurrentsytemdate("chitgenerationdate");
			$("#salespersonid").select2('val',$('#hiddenemployeeid').val()).trigger('change');
			addclickchitbookno = 1;
			chitgenerationinnergrid();
			$('#tab1').addClass('active');
			$('#chitbookno').focus();
			Materialize.updateTextFields();
		});
		//Delete icon
		$("#deleteicon").click(function() {
			var datarowid = $('#chitgenerationgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
			} else {
				alertpopup(selectrowalert);
			}
		});
		$("#basedeleteyes").click(function() {
			chitgenerationdelete();
		});
		// Cancel Icon
		$("#cancelicon").click(function() {
			var datarowid = $('#chitgenerationgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basecanceloverlayforcommodule").fadeIn();
			} else {
				alertpopup(selectrowalert);
			}
		});
		$("#basecancelyes").click(function() {
			chitgenerationcancel();
		});
		$("#basecancelno").click(function() {
			$("#basecanceloverlayforcommodule").fadeOut();
		});
		//prints
		$('#printicon').click(function() {
			var datarowid = $('#chitgenerationgrid div.gridcontent div.active').attr('id');
			var snumber = $('#chitgenerationgrid div.gridcontent div.active ul li.chitgenerationnumber-class').text();
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#printsnumber').val(snumber);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = 273;
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Close Add Screen
			$("#chitgenerationclose").click(function() {	
				var addcloseleadcreation =["chitgenerationclose","chitgenerationcreationview","chitgenerationformadd",""];
				addclose(addcloseleadcreation);
				cleargriddata('chitgenerationinnergrid');
				refreshgrid();
			});
		}
		//close icon
		{// Close Add Screen
			
		}
		$("#chitgenerationclose").click(function() {
			$("#alertsfcloseyes").click(function() {
				resetFields();
				$("#resetfilter").trigger('click');
				$('#scan_chitbookid,#chitschemeid').val('');
                var clsform = $("#alertsfcloseyes").val();
                if(clsform == "Yes") {
					location.reload();
                }
            });
		});
	}
	{//on change trigger grid
		//for date picker
		$('#chitgenerationdate').datetimepicker({
			dateFormat: 'dd-mm-yy',
			showTimepicker :false,
			minDate: 0,
			maxDate:0,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
				}
			},
			onClose: function () {
				$('#chitgenerationdate').focus();
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		$("#searchchitbookno").click(function() {
			addclickchitbookno = 0;
			var scanvalue = $('#chitbookno').val();
			$('#scanchitbookno').val(scanvalue);
			checkchitbookstatus(scanvalue);
		});
		$('.amountcalculate').change(function() {
			totalamtgramclosure();
		});
	}
	{//**sizemaster form validation**//
		$('#submitchitgenerationadd,#chitgenerationsubmit').click(function() {
			$("#addchitgenerationvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#addchitgenerationvalidate").validationEngine({
			onSuccess: function() {
				chitgenerationslipadd();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{//filter
		$("#sizemasterfilterddcondvaluedivhid").hide();
		$("#sizemasterfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#sizemasterfiltercondvalue").focusout(function(){
				var value = $("#sizemasterfiltercondvalue").val();
				$("#sizemasterfinalfiltercondvalue").val(value);
				$("#sizemasterfilterfinalviewconid").val(value);
			});
			$("#sizemasterfilterddcondvalue").change(function(){
				var value = $("#sizemasterfilterddcondvalue").val();
				sizemastermainfiltervalue=[];
				$('#sizemasterfilterddcondvalue option:selected').each(function(){
					var $mvalue =$(this).attr('data-ddid');
					sizemastermainfiltervalue.push($mvalue);
					$("#sizemasterfinalfiltercondvalue").val(sizemastermainfiltervalue);
				});
				$("#sizemasterfilterfinalviewconid").val(value);
			});
		}
		sizemasterfiltername = [];
		$("#sizemasterfilteraddcondsubbtn").click(function() {
			$("#sizemasterfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#sizemasterfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(sizemastergrid,'sizemaster');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$("#resetfilter").click(function() {
		$("#accountid,#accountstate").select2('val','').trigger('change');
		$("#accountaddress,#accountarea,#accountpostalcode,#accountcity,#accountcomments").val('');
		$("#totalpaidamt,#totalpaidgrms,#schemeinterestamt,#schemegiftamt,#totalamtclosure,#totalgrmclosure").val(0);
		$("#scanchitbookno").val('');
	});
	{//refresh grid
		function refreshgrid() {
			var page = $('ul.pagerowcount li.active').data('pagenum');
			var rowcount = $('.pagerowcount').data('rowcount')
			chitgenerationgrid(page,rowcount);
		}
	}
	{ // To display image in overlay after searched chitbook information
		$(document).on("click","#accountimagedisplay",function() {
			var chitbookno = $('#chitbookno').val();
			var accountid = $('#accountid').find('option:selected').val();
			if(chitbookno != '' && (accountid != '' || accountid != '1')) {
				$.ajax({
					url:base_url+'Chitgeneration/retrieveaccountimagedetails',
					dataType:'json',
					data:"accountid="+ accountid,
					method: "POST",
					async:false,
					success: function(data) {
						if(data['fail'] == 'FAILED') {
							alertpopupdouble('There is something problem! Please try again!');
						} else {
							if(data['imgdetails'] != '') {
								$('#salesdetailimageoverlaypreview').fadeIn();
								$('#salesdetailtagimagepreview').empty();
								var imagearray = data['imgdetails'].split(",");
								for (i=0;i<imagearray.length;i++) {
									var img = $('<img id="companylogodynamic" style="height:100%;width:100%;">');
									img.attr('src', base_url+imagearray[i]);
									img.appendTo('#salesdetailtagimagepreview');
								}
							} else {
								alertpopupdouble('No image to preview!');
							}
						}
					},
				});
			} else {
				alertpopupdouble('Kindly use correct Chitbook No!');
			}
		});
		//Image preview close icon
		$(document).on("click","#salesdetailimagepreviewclose",function() {
			$('#salesdetailtagimagepreview').empty();
			$('#salesdetailimageoverlaypreview').fadeOut();
		});
	}
	{// final main
		$("#chitbookno").change(function() {
			var chitbookno = $('#chitbookno').val();
			if(chitbookno != '') {
				$('#searchchitbookno').trigger('click');
			}
		});
	}
	{ // Chit Closure Change
		$("#chitclosureid").change(function() {
			var chitclosureid = $(this).val();
			$('.schemeinterestamt-div,.refundrateamt-div').addClass('hidedisplay');
			if(chitclosureid != 1 || chitclosureid != '') {
				$('#schemeinterestamt,#refundrateamt').val(0);
				var totalpaidamount = $('#totalpaidamt').val();
				if(chitclosureid == 2) {
					$('.schemeinterestamt-div').removeClass('hidedisplay');
					var hiddeninterestrate =  $('#hiddeninterestrate').val();
					if(chitinteresttype == 0) {
						if(hiddeninterestrate > 0) {
							var percentagecalc = parseFloat(totalpaidamount * (hiddeninterestrate/100)).toFixed(amount_round);
							$('#schemeinterestamt').val(percentagecalc);
						} else {
							$('#schemeinterestamt').val(0);
						}
						$("label[for='schemeinterestamt']").html("Interest Amt("+hiddeninterestrate+"%)");
					} else if(chitinteresttype == 1) {
						$('#schemeinterestamt').val(hiddeninterestrate);
						$("label[for='schemeinterestamt']").html("Interest Flat Amt("+hiddeninterestrate+")");
					}
				} else if(chitclosureid == 3) {
					$('.refundrateamt-div').removeClass('hidedisplay');
					var hiddenrefundrate =  $('#hiddenrefundrate').val();
					if(chitinteresttype == 0) {
						if(hiddenrefundrate > 0) {
							var percentagecalc = parseFloat(totalpaidamount * (hiddenrefundrate/100)).toFixed(amount_round);
							$('#refundrateamt').val(percentagecalc);
						} else {
							$('#refundrateamt').val(0);
						}
						$("label[for='refundrateamt']").html("Refund Rate("+hiddenrefundrate+"%)");
					} else if(chitinteresttype == 1) {
						$('#refundrateamt').val(hiddenrefundrate);
						$("label[for='refundrateamt']").html("Refund Flat Amt("+hiddenrefundrate+")");
					}
				}
			} else {
				$('#schemeinterestamt,#refundrateamt').attr('data-validation-engine','');
				$('.schemeinterestamt-div,.refundrateamt-div').addClass('hidedisplay');
				$('#schemeinterestamt,#refundrateamt,#hiddeninterestrate,#hiddenrefundrate').val(0);
			}
			totalamtgramclosure();
		});
	}
});
{//chitgeneration grid view
	function chitgenerationgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#chitgenerationgrid').width();
		var wheight =  $(window).height();
		/*col sort*/
		var sortcol = $("#chitgenerationslipsortcolumn").val();
		var sortord = $("#chitgenerationslipsortorder").val();
		if(sortcol) {
			sortcol = sortcol;
		} else{
			var sortcol = $('.chitgenerationslipheadercolsort ').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').attr('id') : '0';
		var filterid = $("#chitgenerationfilterid").val();
		var conditionname = $("#chitgenerationconditionname").val();
		var filtervalue = $("#chitgenerationfiltervalue").val();
		var userviewid = 209;
		var viewfieldids = 273;
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitgeneration&primaryid=chitgenerationid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#chitgenerationgrid').empty();
				$('#chitgenerationgrid').append(data.content);
				$('#chitgenerationgridfooter').empty();
				$('#chitgenerationgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('chitgenerationgrid');
				{//sorting
					$('.chitgenerationslipheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.chitgenerationslipheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount =  $('.pagerowcount').data('rowcount');
						var sortcol = $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortorder') : '';
						$("#chitgenerationslipsortorder").val(sortord);
						$("#chitgenerationslipsortcolumn").val(sortcol);
						var sortpos = $('#chitgenerationgrid .gridcontent').scrollLeft();
						chitgenerationgrid(page,rowcount);
						$('#chitgenerationgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('chitgenerationslipheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						chitgenerationgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#chitgenerationslippgrowcount').change(function() {
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						chitgenerationgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				// Material select
				$('#chitgenerationslippgrowcount').material_select();
			},
		});	
	}
}
//chit generation slip innmer grid
function chitgenerationinnergrid() {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 1000 : rowcount;
	var wwidth = $("#chitgenerationinnergrid").width();
	var wheight = $("#chitgenerationinnergrid").height();
	/*col sort*/
	var sortcol = $("#sizemastersortcolumn").val();
	var sortord = $("#sizemastersortcolumn").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').attr('id') : '0';
	if(addclickchitbookno == 1) {
		var chitbookno = 1;
	} else {
		var chitbookno = $('#scanchitbookno').val();
	}
	$.ajax({
		url:base_url+"Chitgeneration/gridinformationfetch?maintabinfo=chitentry&primaryid=chitentryid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=51"+"&chitbookno="+chitbookno,		
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#chitgenerationinnergrid').empty();
			$('#chitgenerationinnergrid').append(data.content);
			$('#chitgenerationinnergridfooter').empty();
			$('#chitgenerationinnergridfooter').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('chitgenerationinnergrid');
			{//sorting
				$('.chitgenerationslipheadercolsort').click(function(){
					$('.chitgenerationslipheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#sizemastergridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.chitgenerationslipheadercolsort').hasClass('datasort') ? $('.chitgenerationslipheadercolsort.datasort').data('sortorder') : '';
					$("#sizemastersortorder").val(sortord);
					$("#sizemastersortcolumn").val(sortcol);
					var sortpos = $('#chitgenerationinnergrid .gridcontent').scrollLeft();
					chitgenerationinnergrid(page,rowcount);
					$('#chitgenerationinnergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('chitgenerationslipheadercolsort',headcolid,sortord);
			}
			var hideprodgridcol = ['chitschemeid'];
			gridfieldhide('chitgenerationinnergrid',hideprodgridcol);
			//header check box
			$(".sizemasterinner_headchkboxclass").click(function() {
				checkboxclass('sizemasterinner_headchkboxclass','sizemasterinner_rowchkboxclass');
			});
			//row based check box
			$(".sizemasterinner_rowchkboxclass").click(function() {
				$('.sizemasterinner_headchkboxclass').prop( "checked", false );
			});
		},
	});
}
//innser grid check / Uncheck operation
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").prop('checked', true);
		$("."+headerclass+"").prop('checked', true);
	} else {
		$("."+rowclass+"").prop( "checked", false );
	}
}
{//CRUD Related functions
	//Create
	function chitgenerationslipadd() {
		var formdata = $("#chitgenerationform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var accountid = $('#accountid').val();
		var purityrate = $('#hiddenpurityrate').val();
		$('#processoverlay').show();
	    $.ajax({
	        url: base_url +"Chitgeneration/chitgenerationcreate",
	        data: "datas=" + datainformation+"&accountid="+accountid+"&purityrate="+purityrate,
			dataType:'json',
			type: "POST",
			async:false,
	        success: function(msg) {
				if(msg['status'] == 'SUCCESS') {
					if(mainprintemplateid != '' || mainprintemplateid != 1) {
						printautomate(msg['salesid'],mainprintemplateid,msg['salesnumber']);
					}
					$('#chitgenerationcreationview').fadeIn(1000);
					$(".off-canvas-wrap").css({'padding-left':'65px'});
					chitgenerationgrid();
					alertpopup(savealert);
					setTimeout(function(){
						$("#processoverlay").hide();
					},25);
				} else {
					alertpopup(submiterror);
					setTimeout(function(){
						$("#processoverlay").hide();
					},25);
				}
	        },
	    });
	}
	//delete chit generation information
	function chitgenerationdelete(datarowid) {	
		var datarowid= $('#chitgenerationgrid div.gridcontent div.active').attr('id');
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
	    $.ajax({
	        url: base_url + "Chitgeneration/chitgenerationdelete",
	        data:{primaryid:datarowid},
	        type: "POST",
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				$("#basedeleteoverlay").fadeOut();
			    if (nmsg == 'Success') {	
			    	chitgenerationgrid();
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(deletealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
	            }
	        },
	    });
	}
	// Cancel chit generation information
	function chitgenerationcancel(datarowid) {	
		var datarowid = $('#chitgenerationgrid div.gridcontent div.active').attr('id');
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
	    $.ajax({
	        url: base_url + "Chitgeneration/chitgenerationcancel",
	        data:{primaryid:datarowid},
	        type: "POST",
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				$("#basecanceloverlayforcommodule").fadeOut();
			    if (nmsg == 'Success') {
			    	chitgenerationgrid();
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(deletealert);
	            } else {
					setTimeout(function() {
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
	            }
	        },
	    });
	}
}
function checkchitbookstatus(scanvalue) {
	$.ajax({
		url:base_url+'Chitgeneration/chitbookdetails',
		dataType:'json',
		data:"chitbookno="+ scanvalue,
		method: "POST",
		async:false,
		success: function(data) {
			if(data['chitbookid'] != '') {
				$('#scan_chitbookid').val(data['chitbookid']);
				$('#chitschemeid').val(data['chitschemeid']);
				chitgenerationinnergrid();
				accountinformationretrieve(scanvalue);
				$('#totalpaidamt').val(data['totentryamount']).attr('readonly',true);
				$('#totalpaidgrms').val(data['totentryweight']).attr('readonly',true);
				$('#hiddenchitlookupsid').val(data['chitlookupsid']);
				$('#hiddenintrestmodeid').val(data['intrestmodeid']);
				$('#hiddenpurityrate').val(data['purityrate']);
				if(data['chitlookupsid'] == 3) {
					$('#hiddeninterestrate,#hiddenrefundrate').val(0);
					$('#chitclosureid_div').addClass('hidedisplay');
					$('#chitclosureid').attr('data-validation-engine','');
					$('#chitclosureid').select2('val',1).trigger('change');
				} else {
					$('#hiddeninterestrate').val(data['interestrate']);
					$('#hiddenrefundrate').val(data['refundrateamt']);
					$('#chitclosureid_div').removeClass('hidedisplay');
					$('#chitclosureid').attr('data-validation-engine','validate[required]');
					$('#chitclosureid').select2('val',2).trigger('change');
				}
				if(data['schemegift'] == 'YES') {
					$('.schemegiftamt-div').removeClass('hidedisplay');
					$('#schemegiftamt').val(data['schemegiftamt']);
				} else {
					$('.schemegiftamt-div').addClass('hidedisplay');
					$('#schemegiftamt').val(0);
				}
				$('#adjustmentamount,#adjustmentweight,#bonusamount').val(0);
				if(data['chitlookupsid'] == 2) {
					$('#totalpaidgrms,#totalgrmclosure').val(0);
					$('.totalpaidgrms-div,.totalgrmclosure-div,#adjustmentwt-div').addClass('hidedisplay');
				} else {
					$('.totalpaidgrms-div,.totalgrmclosure-div,#adjustmentwt-div').removeClass('hidedisplay');
				}
				if(data['chitlookupsid'] == 3) {
					$('#adjustmentamount-div').addClass('hidedisplay');
				} else {
					$('#adjustmentamount-div').removeClass('hidedisplay');
				}
				totalamtgramclosure();
				$('#bonusamount,#schemegiftamt,#schemeinterestamt,#totalamtclosure,#totalgrmclosure').attr('readonly',true);
				Materialize.updateTextFields();
			} else {
				$('#chitbookno,#scan_chitbookid,#chitschemeid,#scanchitbookno').val('');
				chitgenerationinnergrid();
				resetFields();
				$("#resetfilter").trigger('click');
				alertpopup('Please scan active chitbooks');
			}
		},
	});
}
function accountinformationretrieve(scanvalue) {
	$.ajax({
		url:base_url+'Chitgeneration/accountinformationretrieve',
		dataType:'json',
		data:"chitbookno="+ scanvalue,
		method: "POST",
		async:false,
		success: function(data) {
			$("#accountid")
				.append($("<option></option>")
				.attr("data-accountname",data['accountname'])
				.attr("value",data['accountid'])
				.text(data['accountname']));
			$("#accountstate")
				.append($("<option></option>")
				.attr("data-statename",data['statename'])
				.attr("value",data['stateid'])
				.text(data['statename']));
			$('#accountid').select2('val',data['accountid']);
			$('#accountstate').select2('val',data['stateid']);
			$('#accountaddress').val(data['address']);
			$('#accountarea').val(data['primaryarea']);
			$('#accountpostalcode').val(data['pincode']);
			$('#accountcity').val(data['city']);
			$('#accountcomments').val(data['description']);
			$('#accountid,#accountstate').prop('disabled',true);
			$('#accountaddress,#accountarea,#accountpostalcode,#accountcity,#accountcomments').attr('readonly',true);
		},
	});
}
function totalamtgramclosure() {
	var hiddenintrestmodeid = $('#hiddenintrestmodeid').val();
	var hiddenchitlookupsid = $('#hiddenchitlookupsid').val();
	var hiddenpurityrate = $('#hiddenpurityrate').val();
	var totalpaidamount = parseFloat($('#totalpaidamt').val());
	var totalpaidgrms = parseFloat($('#totalpaidgrms').val());
	var schemeinterestamt = parseFloat($('#schemeinterestamt').val());
	var schemerefundamt = parseFloat($('#refundrateamt').val());
	var schemegiftamt = parseFloat($('#schemegiftamt').val());
	var adjustmentamount = parseFloat($('#adjustmentamount').val());
	var adjustmentweight = parseFloat($('#adjustmentweight').val());
	var totalbonusamount = (parseFloat(schemeinterestamt)+parseFloat(schemerefundamt)+parseFloat(schemegiftamt)).toFixed(amount_round);
	$('#bonusamount').val((parseFloat(totalbonusamount)).toFixed(amount_round))
	var totalamtclosure = (parseFloat(totalpaidamount)+parseFloat(adjustmentamount)+parseFloat(totalbonusamount)).toFixed(amount_round);
	if(hiddenchitlookupsid == 3 && hiddenintrestmodeid == 7) {
		totalpaidgrms = ((parseFloat(totalamtclosure)/parseFloat(hiddenpurityrate))+parseFloat(adjustmentweight)).toFixed(weight_round);
	} else {
		totalpaidgrms = (parseFloat(totalpaidgrms)+parseFloat(adjustmentweight)).toFixed(weight_round);
	}
	$('#totalamtclosure').val(totalamtclosure);
	$('#totalgrmclosure').val(totalpaidgrms);
	if(totalamtclosure < '0') {
		$('#schemeinterestamt,#schemegiftamt,#adjustmentamount,#bonusamount').val(0);
		totalamtgramclosure()
		alertpopup('Closure amount should not be in minus value');
	}
}
function printautomate(salesid,printtemplateid,snumber) {
	$('#printpdfid').val(salesid);
	$('#printsnumber').val(snumber);		
	$('#pdftemplateprview').select2('val',printtemplateid).trigger('change');
	$('#pdffilepreviewintab').trigger("click");
}