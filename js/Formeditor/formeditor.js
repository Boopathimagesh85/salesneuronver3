var inci = 0;
var iden = 0;
var grp = 0;
var cner = 0;
var grpnum = 0;
$(document).ready(function(){
	var format = $('#dataformatter').val();
	$("#propertypanel").validationEngine();
	$('#recycle').hide();
	$('#newmodnamediv').hide();
	{// For Height
		builderformheight();
	}
	$( window ).resize(function() {
		builderformheight();
	});
	{// Next Step
		$("#nextstep").click(function(){
			var modulename = $("#newmodulecrename").val();
			if(modulename == '') {
				$(".stepperul2").hide();
			} else {
				$('steppercategory3').find('.toggleicons').addClass('icon24build-chevron-down');
				$('steppercategory3').find('.toggleicons').removeClass('icon24build-chevron-up');
				$(".stepperul1").slideUp('slow');
				$(".stepperul3").slideUp('slow');
				$(".stepperul2").slideDown('slow');
				$('.steppercategory2').find('.toggleicons').addClass('icon24build-chevron-up');
				$('.steppercategory2').find('.toggleicons').removeClass('icon24build-chevron-down');
				setTimeout(function(){
					//$('.steppervertical').animate({scrollTop:380},'slow');
				},200);
			}
		});	
	}
	{// Stepper toggle accordian
		$(".steppercat").click(function() {
			$(this).find('.toggleicons').addClass('icon24build-chevron-down');
			$(this).find('.toggleicons').removeClass('icon24build-chevron-up');
			var steppercatlist = $(this).data('steppercat');
			if($(".stepperul"+steppercatlist+"").hasClass("stepperulhideotherelements")) {
				$(".stepperul"+steppercatlist+"").slideUp("slow",function() {
					$(".stepperulhideotherelements").removeClass('stepperulhideotherelements');
				});
			}else {
				$(this).find('.toggleicons').removeClass('icon24build-chevron-down');
				$(this).find('.toggleicons').addClass('icon24build-chevron-up');
				$(".stepperulhideotherelements").slideUp("slow", function() {
					$('.fbuilderaccclick').find('.icon24build-chevron-up').removeClass('icon24build-chevron-up').addClass('icon24build-chevron-down');
					setTimeout(function(){
						var ff = $(".stepperulhideotherelements").prev('li').find('.icon24build-chevron-down').removeClass('icon24build-chevron-down').addClass('icon24build-chevron-up');},10);
					$(".stepperulhideotherelements").removeClass('stepperulhideotherelements');
				});
				$(".stepperul"+steppercatlist+"").slideDown( "slow", function() {
					$(".stepperul"+steppercatlist+"").addClass('stepperulhideotherelements');
				});
			}
		});
	}
	{//Foundation Initialization
		$(document).foundation();
	}
	{//reset module name
		$('#newmodulecrename').select2('val','').trigger('change');
	}
	$('.toggle').click(function(e) {
	  	e.preventDefault();
	  
	    var $this = $(this);
	  
	    if ($this.next().hasClass('show')) {
	        $this.next().removeClass('show');
	        $this.next().slideUp(350);
	    } else {
	        $this.parent().parent().find('li .inner').removeClass('show');
	        $this.parent().parent().find('li .inner').slideUp(350);
	        $this.next().toggleClass('show');
	        $this.next().slideToggle(350);
	    }
	});
	{//for tabs
		$('.active span').removeAttr('style');
		$("#moduleinfoid").click(function() {
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat2,#fbuildercat3").hide();
			$("#fbuildercat1").show();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			$(".propertyhidedisplay").addClass('hidedisplay');
			Materialize.updateTextFields();
		});
		$("#formelementsid").click(function() {
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat2").show();
			$("#fbuildercat1,#fbuildercat3").hide();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			$(".propertyhidedisplay").addClass('hidedisplay');
			Materialize.updateTextFields();
		});
		$("#formpropid").click(function() {
			$('.fbtab-title span i').removeClass('icon24-w').addClass('icon24');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$('.active span').removeAttr('style');
			$("#fbuildercat3").show();
			$("#fbuildercat1,#fbuildercat2").hide();
			clearform('propertiesclear');
			defaultoptionsset();
			$('.active span i').removeClass('icon24').addClass('icon24-w');
			Materialize.updateTextFields();
		});
		$("#newmodulecrename").focusout(function() {
			var moname = $("#newmodulecrename").val();
			if(moname == '') {
				$("#blockspan").show();
			} else {
				$("#blockspan").hide();
			}
		});
		$("#blockspan").click(function() {
			alertpopup('Please select the module name');
		});
		//clear form fields
		$('#s2id_newmodulecrename #anchor_newmodulecrename .s2resetbtn').click(function(){
			$('#spanforaddtab').empty();
			$('#moduleactionname').empty();
			$('#moduleactionname').val('').trigger('change');
			$('.formheaderspan').addClass('hidedisplay');
			var dropdownid =['2','newmodulecrename','moduleactionname'];
			dropdownfailureerror(dropdownid);
		});
	}
	{
		$(".propertyhidedisplay").addClass('hidedisplay');
	}
	{//Full Screen Preview
		$("#normalviewpreview").hide();
		$("#fullviewpreview").click(function() {
			$(this).hide();
			$("#normalviewpreview").show();
			$("#leftsidepanel").addClass('hidedisplay');
			$("#droptarget1").removeClass('large-9 medium-9').addClass('large-12 medium-12');
			$('#preheader').removeClass('hidedisplay');
		});
		$("#normalviewpreview").click(function() {
			$(this).hide();
			$("#fullviewpreview").show();
			$("#leftsidepanel").removeClass('hidedisplay');
			$("#droptarget1").removeClass('large-12 medium-12').addClass('large-9 medium-9');
			$('#preheader').addClass('hidedisplay');
		});
	}

	var withoutgriddraggable = document.getElementById("withoutgridgen");
    withoutgriddraggable.addEventListener('dragstart', dragStart, false);
    withoutgriddraggable.addEventListener('dragend'  , dragEnd  , false);
	
	/* var withgriddraggable = document.getElementById("sectionwithgridgen");
    withgriddraggable.addEventListener('dragstart', dragStart, false);
    withgriddraggable.addEventListener('dragend'  , dragEnd  , false); */
	
	var witheditordraggable = document.getElementById("sectionwitheditorgen");
    witheditordraggable.addEventListener('dragstart', dragStart, false);
    witheditordraggable.addEventListener('dragend'  , dragEnd  , false);
	
	var sectiondraggable = document.getElementById("sectiongen");
    sectiondraggable.addEventListener('dragstart', dragStart, false);
    sectiondraggable.addEventListener('dragend'  , dragEnd  , false);

    var droptarget = document.getElementById("droptarget1");
    droptarget.addEventListener('dragenter', dragEnter,false);
    droptarget.addEventListener('dragover', dragOver,false);
    droptarget.addEventListener('dragleave', dragLeave,false);
    droptarget.addEventListener('drop', dropc, false);

    /* Draggable event handlers */
    function dragStart(event) {
        event.dataTransfer.setData('Text', event.target.id);
    }
    function dragEnd(event) {
    }
    /* Drop target event handlers */
    function dragEnter(event) {
		//event.target.style.border = "2px dashed #ff0000";
    }
    function dragOver(event) {
		event.preventDefault();
		return false;
    }
    function dragLeave(event) {
		//event.target.style.border = "none";
    }
    function dropc(event) {
		//event.target.style.border = "none";
		event.preventDefault();
		var data = event.dataTransfer.getData("Text");
		if(data == 'withoutgridgen') {//without grid tab generation
			var grpdatanum = parseInt(grp)+1;
			if(grp == 0) {
				$("#spanforaddtab").append('<div class="large-12 column tabgroupstyle"><ul class="tabs" id="appendtab" data-tab=""><li class="tab-title sidebaricons"><span class="tabclsbtn icon24build icon24build-close2" data-datatabgroupid="0" data-tabgrprestrict="No"></span><span class="spantabgrpelemgen activefbuilder" contenteditable="true" data-spantabgrpelemgennum="'+grp+'" data-tabgrpfieldid="0" id="tabgrpblabel'+grp+'">Tab Group'+grpdatanum+'</span><input name="spantabgrpinfo[]" type="hidden" id="spantabgrpinfo'+grp+'" value="1,'+grp+',Tab Group'+grpdatanum+',0" /></li></ul></div><div class="large-12 column" id="forreaddtabs">&nbsp;</div><ul id="spanforaddsection'+grp+'" class="toappendlastcontainer hidecontainer appendtabcurrentplace currentab" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>');
				//Tab group name set
				$('.spantabgrpelemgen').focusout(function(){
					var lettercount = $(this).text().length;
					var tabid = $(this).data('spantabgrpelemgennum');
					var tabfieldid = $(this).data('tabgrpfieldid');
					if(lettercount == 0) {
						$(this).text('Tab Group'+(tabid+1)+'');
					}
					var tabgrpname = $(this).text();
					var type = $('#spanforaddsection'+tabid+'').data('tabgrptype');
					$('#spantabgrpinfo'+tabid+'').val(type+','+tabid+','+tabgrpname+','+tabfieldid);
				});
				//$("#withoutgridgen").attr('draggable',false);
				{//For tab Close Hover button
					$(".sidebaricons").hover(
					  function() {
						$(this).find('.tabclsbtn').css('opacity','1');
					  }, function() {
						$(this).find('.tabclsbtn').css('opacity','0');
					  }
					);
				}
				var input = $( "#tabgrplabelspanids" );
				input.val( input.val() + ","+ grp);
				{//for remove tab and Contect area tabclsbtn
					$(".tabclsbtn").click(function(){
						var tabid = $(this).data('datatabgroupid');
						var res = $(this).data("tabgrprestrict");
						if(res=="No") {
							if(tabid != '0') {
								var oldids = $('#deletedtabids').val();
								var ids = oldids+','+tabid;
								$('#deletedtabids').val(ids);
							}
							$(this).closest("li").remove();	
							var getidfordelsection = $(this).siblings("span.spantabgrpelemgen:first").data('spantabgrpelemgennum');
							var groupremv = $("#tabgrplabelspanids").val();
							var value = $.trim(groupremv.replace(','+getidfordelsection,""));
							$("#tabgrplabelspanids").val(value);
							$("#spanforaddsection"+getidfordelsection+"").remove();
							$(".spantabgrpelemgen:first").trigger('click');
							var countoftab = $('#appendtab li').size();
							if(countoftab == 0){
								$("#spanforaddtab div #appendtab").empty();
								$('#forreaddtabs').addClass('forreaddtabs');
							}
						} else {
							alertpopup('Cannot be deleted because it contains restricted field(s) or mandatory field(s)');
						}
					});	
				}
				//To Prevent Line Break -- Enter Key
				$(".activefbuilder").keypress(function(e){ return e.which != 13; });
				grp++;
			} else {
				$('.activefbuilder').removeClass('activefbuilder');
				$("#appendtab").append('<li class="tab-title sidebaricons"><span class="tabclsbtn icon24build icon24build-close2" data-datatabgroupid="0" data-tabgrprestrict="No"></span><span class="spantabgrpelemgen activefbuilder" contenteditable="true" data-spantabgrpelemgennum="'+grp+'" data-tabgrpfieldid="0" id="tabgrpblabel'+grp+'">Tab Group'+grpdatanum+'</span><input name="spantabgrpinfo[]" type="hidden" id="spantabgrpinfo'+grp+'" value="1,'+grp+',Tab Group'+grpdatanum+',0" /></li>');
				if($('#forreaddtabs').hasClass('forreaddtabs')) {
					$('<ul id="spanforaddsection'+grp+'" class="hidecontainer hidedisplay toappendlastcontainer" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>').insertAfter('.forreaddtabs');
				} else {
					$('<ul id="spanforaddsection'+grp+'" class="hidecontainer hidedisplay toappendlastcontainer" data-spantabgrpelemnum="'+grp+'" data-tabgrptype="1"></ul>').insertAfter('.toappendlastcontainer:last');
				}
				//Tab group name set
				$('.spantabgrpelemgen').focusout(function(){
					var lettercount = $(this).text().length; 
					var tabgrpname = $(this).text();
					var tabid = $(this).data('spantabgrpelemgennum');
					var tabfieldid = $(this).data('tabgrpfieldid');
					if(lettercount == 0) {
						$(this).text('Tab Group'+(tabid+1)+'');
					}
					var type = $('#spanforaddsection'+tabid+'').data('tabgrptype');
					$('#spantabgrpinfo'+tabid+'').val(type+','+tabid+','+tabgrpname+','+tabfieldid);
				});
				var input = $( "#tabgrplabelspanids" );
				input.val( input.val() + ","+ grp);
				//$("#withoutgridgen").attr('draggable',false);			
				$(".spantabgrpelemgen").click(function(){
					$(".hidecontainer").removeClass('currentab').addClass('hidedisplay');
					$(".appendtabcurrentplace").removeClass('appendtabcurrentplace');
					$(".activefbuilder").removeClass('activefbuilder');
					$(this).addClass('activefbuilder');
					//To Prevent Line Break -- Enter Key
					$(".activefbuilder").keypress(function(e){ return e.which != 13; });
					var tabclicksh = $(this).data('spantabgrpelemgennum');
					$("#spanforaddsection"+tabclicksh+"").addClass('appendtabcurrentplace').addClass('currentab').removeClass('hidedisplay');
					$('.spantabgrpelemgen').trigger('focusout');
				});
				$('.spantabgrpelemgen.activefbuilder').trigger('click');
				{//For tab Close Hover button
					$(".sidebaricons").hover(
					  function() {
						$( this ).find('.tabclsbtn').css('opacity','1');
					  }, function() {
						$( this ).find('.tabclsbtn').css('opacity','0');
					  }
					);
				}
				{//for remove tab and Contect area tabclsbtn
					$(".tabclsbtn").click(function(){
						var tabid = $(this).data('datatabgroupid');
						var res = $(this).data("tabgrprestrict");
						if(res=="No") {
							if(tabid != '0') {
								var oldids = $('#deletedtabids').val();
								var ids = oldids+','+tabid;
								$('#deletedtabids').val(ids);
							}
							$(this).closest("li").remove();	
							var getidfordelsection = $(this).siblings("span.spantabgrpelemgen:first").data('spantabgrpelemgennum');
							var groupremv = $("#tabgrplabelspanids").val();
							var value = $.trim(groupremv.replace(','+getidfordelsection,""));
							$("#tabgrplabelspanids").val(value);
							$("#spanforaddsection"+getidfordelsection+"").remove();
							$(".spantabgrpelemgen:first").trigger('click');
							var countoftab = $('#appendtab li').size();
							if(countoftab == 0) {
								$("#spanforaddtab div #appendtab").empty();
								$('#forreaddtabs').addClass('forreaddtabs');
							}
						} else {
							alertpopup('Cannot be deleted because it contains restricted field(s) or mandatory field(s)');
						}
					});	
				}
				var appendtab = document.getElementById("appendtab");
				new Sortable(appendtab);
				Sortable.create(appendtab, {
					group: "tabgrp",
					animation: 150,
				});
				grp++;
				cner++;
			}
		} else if(data == 'sectionwithgridgen') { //section with grid tab generation
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict') || $(".appendtabcurrentplace li").hasClass('forrestrictsec')){
					alertpopup("You can't do this action");
				} else {
					$(".appendtabcurrentplace").append('<li class="forrestrict"><div class="large-12 column">&nbsp;</div><div id="spanforaddelements'+inci+'" data-forsorting="'+inci+'" data-spanforaddelements="'+inci+'" class="large-4 column end"><div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'" data-sectionfieldid="0">Tab Section</div><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section,0" /><ul class="sortable" id="sortable'+inci+'" data-datasectionattrid="'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div><div class="large-8 column"><img src="'+base_url+'img/builders/samplegrid.jpg"/></li>');
					
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',2);
					$('.spantabgrpelemgen').trigger('focusout');
					
					//tab section sorting - section with grid
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'secgridelem'
					});
					
					$(document).ready(function(){
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							var scfldid = $(this).data('sectionfieldid');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname+','+scfldid);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("attachmentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false); 
						
						var textareadraggable = document.getElementById("imgattachgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var passworddraggable = document.getElementById("passwordgen");
						passworddraggable.addEventListener('dragstart', dragStart, false);
						passworddraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var editordraggable = document.getElementById("texteditorgen");
						editordraggable.addEventListener('dragstart', dragStart, false);
						editordraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dddropdowndraggable = document.getElementById("ddrelationgen");
						dddropdowndraggable.addEventListener('dragstart', dragStart, false);
						dddropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var tagcomponentdraggable = document.getElementById("tagscomponentgen");
						tagcomponentdraggable.addEventListener('dragstart', dragStart, false);
						tagcomponentdraggable.addEventListener('dragend'  , dragEnd  , false);
						
						/*var treecomponentdraggable = document.getElementById("ddtreegen");
						treecomponentdraggable.addEventListener('dragstart', dragStart, false);
						treecomponentdraggable.addEventListener('dragend'  , dragEnd  , false);*/
						
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}

						function dragOver(event) {
							event.preventDefault(); 
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							$("#processoverlay").show();
							event.preventDefault();
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id');
							var secids = $('#'+setdataid+'').data('spanforaddelements');							
							if(data == 'textboxgen') {
								var label = labelnamecheck("Text");
								if(label > 0){
									alertpopup("Text Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',2,'+labelname+',,100,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'textareagen') {
								var label = labelnamecheck("Text Area");
								if(label > 0){
									alertpopup("Text Area Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Area";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',3,'+labelname+',,100,3,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'integerboxgen') {
								var label = labelnamecheck("Integer");
								if(label > 0){
									alertpopup("Integer Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Integer";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',4,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'decimalboxgen') {
								var label = labelnamecheck("Decimal");
								if(label > 0){
									alertpopup("Decimal Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Decimal";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',5,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'percentgen') {
								var label = labelnamecheck("Percent");
								if(label > 0){
									alertpopup("Percent Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Percent";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',6,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'currencygen') {
								var label = labelnamecheck("Currency");
								if(label > 0){
									alertpopup("Currency Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Currency";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',7,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dategen') {
								var label = labelnamecheck("Date");
								if(label > 0){
									alertpopup("Date Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Date";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',8,'+labelname+',,,No,Yes,No,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'timegen') {
								var label = labelnamecheck("Time");
								if(label > 0){
									alertpopup("Time Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Time";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',9,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'emailgen') {
								var label = labelnamecheck("Email");
								if(label > 0){
									alertpopup("Email Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Email";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',10,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'phonegen') {
								var label = labelnamecheck("Phone");
								if(label > 0){
									alertpopup("Phone Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Phone";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',11,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'urlgen') {
								var label = labelnamecheck("URL");
								if(label > 0){
									alertpopup("URL Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "URL";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',12,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'checkboxgen') {
								var label = labelnamecheck("Check Box");
								if(label > 0){
									alertpopup("Check Box Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Check Box";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',13,'+labelname+',No,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'autonumbergen') {
								var label = labelnamecheck("Auto Number");
								if(label > 0){
									alertpopup("Auto Number Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Auto Number";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',14,'+labelname+',,,,No,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'attachmentgen') {
								var label = labelnamecheck("Attachment");
								if(label > 0){
									alertpopup("Attachment Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Attachment";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',15,'+labelname+',5,,Yes,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'imgattachgen') {
								var label = labelnamecheck("Image Upload");
								if(label > 0){
									alertpopup("Image Upload Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Image Upload";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'" class="fortabicons"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',16,'+labelname+',5,.jpg|.jpeg|.exif|.tiff|.gif|.png|.ppm|.pgm|.pbm|.pnm|.bpg,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dropdowngen') {
								var label = labelnamecheck("Picklist");
								if(label > 0){
									alertpopup("Picklist Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Picklist";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',17,'+labelname+',,,,No,Yes,No,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'passwordgen') {
								var label = labelnamecheck("Password");
								if(label > 0){
									alertpopup("Password Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Password";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',22,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'texteditorgen') {
								var label = labelnamecheck("Text Editor");
								if(label > 0){
									alertpopup("Text Editor Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Editor";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',24,'+labelname+',,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddrelationgen') {
								var label = labelnamecheck("Relation");
								if(label > 0){
									alertpopup("Relation Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Relation";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',26,'+labelname+',,,,,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements 
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'tagscomponentgen') {
								var label = labelnamecheck("Tags");
								if(label > 0){
									alertpopup("Tags Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tags";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="30" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',30,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddtreegen') {
								var label = labelnamecheck("Tree");
								if(label > 0){
									alertpopup("Tree Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tree";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="21" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',21,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id);
									Materialize.updateTextFields();
								},200);
								$(".propertytext").addClass('hidedisplay');
								$(".propertyhidedisplay").removeClass('hidedisplay');								
								$('#formpropid').trigger('click');
								setTimeout(function(){$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								//Toggle Property pannel
								propertyslidetoggle();
							});
							setTimeout(function(){
								$("#processoverlay").fadeOut();
							},200);
							return false;
						}
						inci++;
					});	
				}
			}
		} else if(data == 'sectionwitheditorgen') {
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict') || $(".appendtabcurrentplace li").hasClass('forrestrictsec')){
					alertpopup("You can't do this action");
				} else {
					$(".appendtabcurrentplace").append('<li class="forrestrict"><div class="large-12 column">&nbsp;</div><div id="spanforaddelements'+inci+'" data-forsorting="'+inci+'" data-spanforaddelements="'+inci+'" class="large-4 column end"><div class="large-12 columns cleardataform" style="padding-left: 0 !important; padding-right: 0 !important;"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'" data-sectionfieldid="0">Tab Section</div><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section,0" /><ul class="sortable" id="sortable'+inci+'" data-datasectionattrid="'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div><div class="large-8 column"><img src="'+base_url+'img/builders/sampleeditor.jpg"/></li>');
				
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',3);
					$('.spantabgrpelemgen').trigger('focusout');
					
					//tab section sorting - section with editor
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'seceditorelem',
					});
					
					$(document).ready(function(){
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							var scfldid = $(this).data('sectionfieldid');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname+','+scfldid);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("attachmentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("imgattachgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false); 
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var passworddraggable = document.getElementById("passwordgen");
						passworddraggable.addEventListener('dragstart', dragStart, false);
						passworddraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var editordraggable = document.getElementById("texteditorgen");
						editordraggable.addEventListener('dragstart', dragStart, false);
						editordraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dddropdowndraggable = document.getElementById("ddrelationgen");
						dddropdowndraggable.addEventListener('dragstart', dragStart, false);
						dddropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var tagcomponentdraggable = document.getElementById("tagscomponentgen");
						tagcomponentdraggable.addEventListener('dragstart', dragStart, false);
						tagcomponentdraggable.addEventListener('dragend'  , dragEnd  , false);
						
						/*var treecomponentdraggable = document.getElementById("ddtreegen");
						treecomponentdraggable.addEventListener('dragstart', dragStart, false);
						treecomponentdraggable.addEventListener('dragend'  , dragEnd  , false);*/
						
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}

						function dragOver(event) {
							event.preventDefault();
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							$("#processoverlay").show();
							event.preventDefault(); 
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id'); 
							var secids = $('#'+setdataid+'').data('spanforaddelements');
							if(data == 'textboxgen') {
								var label = labelnamecheck("Text");
								if(label > 0){
									alertpopup("Text Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',2,'+labelname+',,100,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id'); 
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'textareagen') {
								var label = labelnamecheck("Text Area");
								if(label > 0){
									alertpopup("Text Area Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Area";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="icon24build icon24build-cursor-move2leliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',3,'+labelname+',,100,3,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'integerboxgen') {
								var label = labelnamecheck("Integer");
								if(label > 0){
									alertpopup("Integer Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Integer";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',4,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'decimalboxgen') {
								var label = labelnamecheck("Decimal");
								if(label > 0){
									alertpopup("Decimal Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Decimal";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',5,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'percentgen') {
								var label = labelnamecheck("Percent");
								if(label > 0){
									alertpopup("Percent Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Percent";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',6,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'currencygen') {
								var label = labelnamecheck("Currency");
								if(label > 0){
									alertpopup("Currency Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Currency";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',7,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dategen') {
								var label = labelnamecheck("Date");
								if(label > 0){
									alertpopup("Date Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Date";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',8,'+labelname+',,,No,Yes,No,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'timegen') {
								var label = labelnamecheck("Time");
								if(label > 0){
									alertpopup("Time Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Time";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',9,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'emailgen') {
								var label = labelnamecheck("Email");
								if(label > 0 ){
									alertpopup("Email Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Email";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" data-datafieldid="0"  value="'+secids+',10,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									$("#tabfieldlabelspanids").val().concat(getclassforsorting);
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'phonegen') {
								var label = labelnamecheck("Phone");
								if(label > 0 ){
									alertpopup("Phone Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Phone";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',11,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'urlgen') {
								var label = labelnamecheck("URL");
								if(label > 0 ){
									alertpopup("URL Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "URL";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',12,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'checkboxgen') {
								var label = labelnamecheck("Check Box");
								if(label > 0 ){
									alertpopup("Check Box Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Check Box";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',13,'+labelname+',No,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'autonumbergen') {
								var label = labelnamecheck("Auto Number");
								if(label > 0 ){
									alertpopup("Auto Number Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Auto Number";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',14,'+labelname+',,,,No,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'attachmentgen') {
								var label = labelnamecheck("Attachment");
								if(label > 0 ){
									alertpopup("Attachment Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Attachment";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',15,'+labelname+',5,,Yes,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'imgattachgen') {
								var label = labelnamecheck("Image Upload");
								if(label > 0 ){
									alertpopup("Image Upload Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Image Upload";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'" class="fortabicons"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',16,'+labelname+',5,.jpg|.jpeg|.exif|.tiff|.gif|.png|.ppm|.pgm|.pbm|.pnm|.bpg,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dropdowngen') {
								var label = labelnamecheck("Picklist");
								if(label > 0 ){
									alertpopup("Picklist Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Picklist";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',17,'+labelname+',,,,No,Yes,No,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'passwordgen') {
								var label = labelnamecheck("Password");
								if(label > 0 ){
									alertpopup("Password Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Password";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',22,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'texteditorgen') {
								var label = labelnamecheck("Text Editor");
								if(label > 0 ){
									alertpopup("Text Editor Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Editor";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',24,'+labelname+',,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddrelationgen') {
								var label = labelnamecheck("Relation");
								if(label > 0 ){
									alertpopup("Relation Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Relation";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',26,'+labelname+',,,,,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'tagscomponentgen') {
								var label = labelnamecheck("Tags");
								if(label > 0 ){
									alertpopup("Tags Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tags";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="30" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',30,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddtreegen') {
								var label = labelnamecheck("Tree");
								if(label > 0 ){
									alertpopup("Tree Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tree";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="21" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',21,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$(".propertytext").addClass('hidedisplay');
								$(".propertyhidedisplay").removeClass('hidedisplay');
								$('#formpropid').trigger('click');
								setTimeout(function(){
								$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								//Toggle Property pannel
								propertyslidetoggle();
							});
							setTimeout(function(){
								$("#processoverlay").fadeOut();
							},200);
							return false;
						}
						inci++;
					});	
				}
			}
		} else if(data == 'sectiongen') {
			if($(".appendtabcurrentplace").hasClass('currentab')) {
				grpnum = $(".currentab").data('spantabgrpelemnum');
				if($(".appendtabcurrentplace li").hasClass('forrestrict')){
					alertpopup("You can't do this action");
				} else {
					var incinm = inci;
					var secnum = parseInt(incinm)+1;
					$(".appendtabcurrentplace").append('<li class="forrestrictsec newsortsec" id="secliattrid'+inci+'"><div class="large-4 column end forselectsection paddingbtm"><div class="large-12 columns cleardataform ddddd" style="padding-left: 0 !important; padding-right: 0 !important;" id="spanforaddelements'+inci+'" data-spanforaddelements="'+inci+'" data-forsorting="'+inci+'"><div class="large-12 columns headerformcaptionstyle spansectionelemgen" contenteditable="true" data-spansectionelemgennum="'+inci+'" data-sectionfieldid="0" data-sectionfieldresct="No" id="tabseclabel'+inci+'" data-sectabgrpid="'+grpnum+'">Tab Section '+secnum+'</div><span class="icon24build icon24build-cursor-move secmovebtn acticonshover" data-datasecliattrid="'+inci+'" title="Move"></span><span class="icon24build icon24build-close secremovebtn acticonshover" data-sectionfieldid="0" data-sectionfieldresct="No" title="Remove"></span><input name="spantabsectinfo[]" type="hidden" id="spantabsectinfo'+inci+'" value="'+grpnum+','+inci+',Tab Section '+secnum+',0" /><ul class="sortable" id="sortable'+inci+'" data-datasectionattrid="'+inci+'"> </ul></div><div class="large-12 column" style="background: #f5f5f5;margin-top:0.01rem">&nbsp;</div></div></li>');
					/* Hover for remove span */
					$("#spanforaddelements"+inci+"").hover(
						function() {
							$(this).find("span.acticonshover").css("display","block");	
						}, function() {
							$(this).find("span.acticonshover").css("display","none");
					});
					{//update section ids
						var oldsecids = $("#tabseclabelspanids").val();
						var newids = ((oldsecids=='')? inci : oldsecids+','+inci);
						$("#tabseclabelspanids").val(newids);
					}
					/* For Delete Section */
					$(".secremovebtn").click(function(){
						var sectid = $(this).data('sectionfieldid');
						var secres = $(this).data('sectionfieldresct');
						if(secres == 'No') {
							if(sectid != '0') {
								var oldids = $('#deletedsectionids').val();
								var ids = oldids+','+sectid;
								$('#deletedsectionids').val(ids);
							}
							var removesection = $(this).parent('div').data('spanforaddelements');
							var groupremv = $("#tabseclabelspanids").val();
							var value = $.trim(groupremv.replace(','+removesection,""));
							$("#tabseclabelspanids").val(value);
							$("#spanforaddelements"+removesection+"").parent('.forselectsection').parent('li').remove();
						} else {
							alertpopup('Cannot be deleted because it contains restricted field(s) or mandatory field(s)');
						}
					});
					/* For Move Section from one group to another*/
					$(".secmovebtn").click(function(){
						$("#processoverlay").show();
						var secliid = $(this).data('datasecliattrid');
						$('#secdataliid').val(secliid);
						//set tab group name to drop down
						var txt = "";
						var ddndatas = $("#tabgrplabelspanids").val();
						var ddsdatas = ddndatas.split(',');
						$('#tabgrouplist').children().remove();
						$('#tabgrouplist').append($("<option value=''></option>"));
						for(var hh=0;hh < ddsdatas.length;hh++) {
							var txt = $('#tabgrpblabel'+ddsdatas[hh]+'').text();
							if($('#spanforaddsection'+ddsdatas[hh]+'').data('tabgrptype') == '1') {
								$('#tabgrouplist').append($("<option></option>").attr("value",ddsdatas[hh]).text(txt));
							}
						}
						//set tab section name
						var sectxt = "";
						var secdataids = $("#tabseclabelspanids").val();
						var secddsdatas = secdataids.split(',');
						$('#tabsectionlist').children().remove();
						$('#tabsectionlist').append($("<option value=''></option>"));
						for(var ss=0;ss <secddsdatas.length;ss++) {
							var grpid = $('#tabseclabel'+secddsdatas[ss]+'').data('sectabgrpid');
							var sectxt = $('#tabseclabel'+secddsdatas[ss]+'').text();
							if($('#spanforaddsection'+grpid+'').data('tabgrptype') == '1') {
								$('#tabsectionlist').append($("<option></option>").attr("label",""+grpid+"").attr("value",secddsdatas[ss]).text(sectxt));
							}
						}
						$("#processoverlay").fadeOut();
						setTimeout(function(){
							$('#movesectotabgrpoverlay').fadeIn();
						},150);
					});
					//for section Selection
					$("#spanforaddelements"+inci+"").click(function(){
						$('.sectionselector').removeClass('sectionselector');
						$(this).closest('.forselectsection').addClass('sectionselector');
					});
					//template type define
					var tabtype = $(".currentab").data('spantabgrpelemnum');
					$('#spanforaddsection'+tabtype+'').data('tabgrptype',1);
					$('.spantabgrpelemgen').trigger('focusout');
					
					//tab section sorting - without grid or editor
					var tabsection = document.getElementById("spanforaddsection"+grpnum+"");
					new Sortable(tabsection);
					Sortable.create(tabsection, {
						animation: 150,
						group:'tabsection',
						draggable: '.newsortsec',
						handle: '.spansectionelemgen',
					});
					//tab section elements sorting - only section elements
					var tabsectionele = document.getElementById("sortable"+inci+"");
					Sortable.create(tabsectionele, {
						group: 'sectionelement',
						//onUpdate: function (evt){ console.log('onUpdate.section:', [evt.item, evt.from]); console.log(evt.item); },
						onAdd: function (evt) {
							var dragnodedivid = evt.item.firstChild.id;
							var frmelemattrid = $('#'+dragnodedivid+'').data('frmeledivattr');
							var ulid =$('#'+dragnodedivid+'').parent().closest('ul').attr('id');
							var datasecid = $('#'+ulid+'').data('datasectionattrid');
							dragformelementsvalueorganize(frmelemattrid,datasecid);
						},
					});

					$(document).ready(function(){
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							var scfldid = $(this).data('sectionfieldid');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname+','+scfldid);
						});
						var textboxdraggable = document.getElementById("textboxgen");
						textboxdraggable.addEventListener('dragstart', dragStartx, false);
						textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
						var textareadraggable = document.getElementById("textareagen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("integerboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("decimalboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("percentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("currencygen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("dategen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("timegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("emailgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("phonegen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("urlgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("checkboxgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("autonumbergen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("attachmentgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var textareadraggable = document.getElementById("imgattachgen");
						textareadraggable.addEventListener('dragstart', dragStart, false);
						textareadraggable.addEventListener('dragend'  , dragEnd  , false); 
						
						var dropdowndraggable = document.getElementById("dropdowngen");
						dropdowndraggable.addEventListener('dragstart', dragStart, false);
						dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var passworddraggable = document.getElementById("passwordgen");
						passworddraggable.addEventListener('dragstart', dragStart, false);
						passworddraggable.addEventListener('dragend'  , dragEnd  , false);
				
						var editordraggable = document.getElementById("texteditorgen");
						editordraggable.addEventListener('dragstart', dragStart, false);
						editordraggable.addEventListener('dragend'  , dragEnd  , false);
						
						var dddropdowndraggable = document.getElementById("ddrelationgen");
						dddropdowndraggable.addEventListener('dragstart', dragStart, false);
						dddropdowndraggable.addEventListener('dragend'  , dragEnd  , false);

						var tagcomponentdraggable = document.getElementById("tagscomponentgen");
						tagcomponentdraggable.addEventListener('dragstart', dragStart, false);
						tagcomponentdraggable.addEventListener('dragend'  , dragEnd  , false);
						
						/*var treecomponentdraggable = document.getElementById("ddtreegen");
						treecomponentdraggable.addEventListener('dragstart', dragStart, false);
						treecomponentdraggable.addEventListener('dragend'  , dragEnd  , false);*/
						
						/* Draggable event handlers */
						function dragStartx(event) {
							event.dataTransfer.setData('Text', event.target.id);
						}
						function dragEndx(event) {
						}
						var sdroptarget = document.getElementById("spanforaddelements"+inci+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
						
						/* Drop target event handlers */
						function dragEnter(event) {
							//event.target.style.border = "2px dashed #ff0000";
						}

						function dragOver(event) {
							event.preventDefault();
							return false;
						}
						function dragLeave(event) {
							//event.target.style.border = "none";
						}
						//drop function call
						function drop(event) {
							$("#processoverlay").show();
							event.preventDefault(); 
							var data = event.dataTransfer.getData("Text");
							var setdataid =  $(this).attr('id');  
							var secids = $('#'+setdataid+'').data('spanforaddelements');
							if(data == 'textboxgen') {
								var label = labelnamecheck("Text");
								if(label > 0 ){
									alertpopup("Text Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',2,'+labelname+',,100,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id'); 
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'textareagen') {
								var label = labelnamecheck("Text Area");
								if(label > 0 ){
									alertpopup("Text Area Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Area";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',3,'+labelname+',,100,3,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'integerboxgen') {
								var label = labelnamecheck("Integer");
								if(label > 0 ){
									alertpopup("Integer Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Integer";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',4,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'decimalboxgen') {
								var label = labelnamecheck("Decimal");
								if(label > 0 ){
									alertpopup("Decimal Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Decimal";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',5,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'percentgen') {
								var label = labelnamecheck("Percent");
								if(label > 0 ){
									alertpopup("Percent Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Percent";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',6,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'currencygen') {
								var label = labelnamecheck("Currency");
								if(label > 0 ){
									alertpopup("Currency Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Currency";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',7,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dategen') {
								var label = labelnamecheck("Date");
								if(label > 0 ){
									alertpopup("Date Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Date";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',8,'+labelname+',,,No,Yes,No,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'timegen') {
								var label = labelnamecheck("Time");
								if(label > 0 ){
									alertpopup("Time Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Time";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',9,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'emailgen') {
								var label = labelnamecheck("Email");
								if(label > 0){
									alertpopup("Email Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Email";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',10,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}								
							} else if(data == 'phonegen') {
								var label = labelnamecheck("Phone");
								if(label > 0 ){
									alertpopup("Phone Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Phone";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',11,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'urlgen') {
								var label = labelnamecheck("URL");
								if(label > 0 ){
									alertpopup("URL Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "URL";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',12,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'checkboxgen') {
								var label = labelnamecheck("Check Box");
								if(label > 0 ){
									alertpopup("Check Box Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Check Box";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',13,'+labelname+',No,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'autonumbergen') {
								var label = labelnamecheck("Auto Number");
								if(label > 0 ){
									alertpopup("Auto Number Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Auto Number";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',14,'+labelname+',,,,No,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'attachmentgen') {
								var label = labelnamecheck("Attachment");
								if(label > 0 ){
									alertpopup("Attachment Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Attachment";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',15,'+labelname+',5,,Yes,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'imgattachgen') {
								var label = labelnamecheck("Image Upload");
								if(label > 0 ){
									alertpopup("Image Upload Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Image Upload";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'" class="fortabicons"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',16,'+labelname+',5,.jpg|.jpeg|.exif|.tiff|.gif|.png|.ppm|.pgm|.pbm|.pnm|.bpg,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dropdowngen') {
								var label = labelnamecheck("Picklist");
								if(label > 0 ){
									alertpopup("Picklist Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Picklist";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',17,'+labelname+',,,,No,Yes,No,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'passwordgen') {
								var label = labelnamecheck("Password");
								if(label > 0 ){
									alertpopup("Password Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Password";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',22,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'texteditorgen') {
								var label = labelnamecheck("Text Editor");
								if(label > 0 ){
									alertpopup("Text Editor Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Editor";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',24,'+labelname+',,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddrelationgen') {
								var label = labelnamecheck("Relation");
								if(label > 0 ){
									alertpopup("Relation Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Relation";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',26,'+labelname+',,,,,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'tagscomponentgen') {
								var label = labelnamecheck("Tags");
								if(label > 0 ){
									alertpopup("Tags Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tags";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="30" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',30,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddtreegen') {
								var label = labelnamecheck("Tree");
								if(label > 0 ){
									alertpopup("Tree Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tree";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="21" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',21,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							}
							iden++;
							//div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$(".propertytext").addClass('hidedisplay');
								$(".propertyhidedisplay").removeClass('hidedisplay');
								$('#formpropid').trigger('click');
								setTimeout(function(){
								$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								//Toggle Property pannel
								propertyslidetoggle();
							});
							setTimeout(function(){
								$("#processoverlay").fadeOut();
							},200);
							return false;
						}
						inci++;
					});	
				}
			}
			return false;
		}
    }
	{//check box event
		//Mandatory,Active,Multiple option,Check box set default check box
		$('#fieldactive').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				//$('#fildmandatory').css('pointer-events','auto');
				$('.actchk').css('pointer-events','auto');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				//$('#fildmandatory').css('pointer-events','none');
				$('.actchk').css('pointer-events','none');
				$('.actchk').prop('checked',false);
				$('.actchk').trigger('click');
				$('.actchk').trigger('click');
				setTimeout(function(){
					fromelementsvalueorganize(attrid);
				},200);
			}
		});
		$('#addalphasortorder').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
			}
		});
		//Read only Check box set default check box
		$('#fieldreadonly').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").attr('readonly','readonly').css('background','#dddddd');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").removeAttr('readonly').css('background','#ffffff');
				fromelementsvalueorganize(attrid);
			}
		});
		//Mandatory
		$('#fildmandatory').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$('#fieldactive').css('pointer-events','none');
				fromelementsvalueorganize(attrid);
				$("#frmelemlivespantit"+attrid+"").append('<span id="mandfildid'+attrid+'" class="mandatoryfildclass">*</span>');
			} else {
				$(this).val('No');
				$('#fieldactive').css('pointer-events','auto');
				fromelementsvalueorganize(attrid);
				$('#mandfildid'+attrid+'').remove();
			}
		});
		//multiple option
		$('#addfieldmultiple').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").attr('multiple','multiple');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").removeAttr('multiple');
				fromelementsvalueorganize(attrid);
			}
		});
		//for column type
		$('#fieldcolumntype').click(function(){
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#columndivid"+attrid+"").removeClass('large-12 medium-12 small-12').addClass('large-6 medium-6 small-6 end');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#columndivid"+attrid+"").removeClass('large-6 medium-6 small-6 end').addClass('large-12 medium-12 small-12');
				fromelementsvalueorganize(attrid);
			}
		});
		//check box default checked option set
		$('#addfieldsetdef').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#frmliveelm"+attrid+"").prop('checked', true);
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				$("#frmliveelm"+attrid+"").prop('checked', false);
				fromelementsvalueorganize(attrid);
			}
		});
		//Unique Check box validation
		$('#addfieldunique').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
			}
		});
		//Unique Check box validation
		$('#featuredate,#pastdate,#changeyear,#currentdate').click(function() {
			var attrid = $('#elemidentiryid').val();
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				fromelementsvalueorganize(attrid);
			} else {
				$(this).val('No');
				fromelementsvalueorganize(attrid);
			}
		});
	}
	{//properties set
		//label
		$('#frmelemattrlablname').change(function(){
			var attrid = $('#elemidentiryid').val();
			var oldtext = $('#frmelemlivespantit'+attrid+'').text();
			var txt = $(this).val();
			var newlabel = ( (txt=="")? oldtext:txt );
			$(this).val(newlabel);
			$('#frmelemlivespantit'+attrid+'').text(newlabel);
			fromelementsvalueorganize(attrid);
		});
		//default value
		$('#frmelemattrdefname').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $(this).val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//date change event
		$('#dateaddfielddefval').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $("#dateaddfielddefval").val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//time default value
		$('#timeaddfielddefval').change(function(){
			var attrid = $('#elemidentiryid').val();
			var txt = $("#timeaddfielddefval").val();
			$('#frmliveelm'+attrid+'').val(txt);
			fromelementsvalueorganize(attrid);
		});
		//length
		$('#frmelemattrlength').change(function(){
			var vallength = $(this).val();
			var datafieldid = $(this).attr('datafieldid');
			var maxlength = 0;
			$.ajax({
				url:base_url+"Formeditor/fetchfieldlength?modulefieldid="+datafieldid,
				type: "POST",
				async:false,
				cache:false,
				success :function(data) {
					maxlength = data;
				}
			});
			if(vallength > 250){
				alertpopup("Field Length Should not be greater than 250");
				$(this).val(maxlength);
			}else if (vallength < maxlength){
				alertpopup("Field Length Should not be less than Existing Length");
				$(this).val(maxlength);
			}else{
				var attrid = $('#elemidentiryid').val();
				fromelementsvalueorganize(attrid);
			}			
		});
		$('#frmelemattrtextlength').change(function(){
			var vallength = $(this).val();
			var datafieldid = $(this).attr('datafieldid');
			var maxlength = 0;
			$.ajax({
				url:base_url+"Formeditor/fetchfieldlength?modulefieldid="+datafieldid,
				type: "POST",
				async:false,
				cache:false,
				success :function(data) {
					maxlength = data;
				}
			});
			if(vallength > 600){
				alertpopup("Field Length Should not be greater than 600");
				$(this).val(maxlength);
			}else if (vallength < maxlength){
				alertpopup("Field Length Should not be less than Existing Length");
				$(this).val(maxlength);
			}else{
				var attrid = $('#elemidentiryid').val();
				fromelementsvalueorganize(attrid);
			}			
		});
		$("#elecheckshowhide").click(function(){
			if ($(this).is(':checked')) {				
				$('#elecheckshowhide').val('Yes');
				var datafieldid = $(this).attr('datafieldid');				
				$('#elecheckshfldname').empty();
				$('#elecheckshfldname').append($("<option></option>"));
				var moduleid = $("#newmodulecrename").val();
				$.ajax({
					url:base_url+"Formeditor/fetchmodulefieldname?moduleid="+moduleid+"&datafieldid="+datafieldid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						$.each(data, function(index) {
							$('#elecheckshfldname')
							.append($("<option></option>")
							.attr("data-datadiv",data[index]['columnname']+'divhid')		
							.attr("data-elecheckshfldnamehidden",data[index]['fieldlabel'])
							.attr("value",data[index]['modulefieldid'])
							.text(data[index]['fieldlabel']));
						});
						$('#elecheckshfldname').trigger('change');
					},
				});				
			}else{
				$('#elecheckshowhide').val('No');
				$('#elecheckshfldname').val("");
			}
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		$('#elecheckshfldname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//decimal
		$('#frmelemdecilength').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//description
		$('#frmelemattrdesc').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//prefix change event
		$('#frmelemprefix').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//auto start number change event
		$('#frmelemstartnum').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//suffix change event
		$('#frmelemsuffix').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//text area rows
		$('#frmelemattrrows').change(function(){
			var attrid = $('#elemidentiryid').val();
			var rows = $(this).val();
			$('#frmliveelm'+attrid+'').removeAttr("rows");
			$('#frmliveelm'+attrid+'').attr("rows", rows);
			fromelementsvalueorganize(attrid);
		});
		//automatic number type
		$('#attrautonumbertype').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//drop down value set
		$("#frmelemdatavalue").select2({
			tags:[''],
			tokenSeparators: [","],
		});
		//tag def value
		$("#tagdefaultvalue").select2({
			tags:[''],
			tokenSeparators: [","],
		});
		//tag def value
		$("#attrdataimgfiletype").select2({
			tags:['.jpg','.jpeg','.exif','.tiff','.gif','.png','.ppm','.pgm','.pbm','.pnm','.bpg'],
			tokenSeparators: [","],
		});
		//drop down value change event
		$('#frmelemdatavalue').change(function(){
			var select = $('#frmliveelm'+attrid+'');
			select.children().remove();
			$('#dddefaultvalue').children().remove();
			var ddndatas = $("#frmelemdatavalue").val();
			var ddsdatas = ddndatas.split(',');
			$('#dddefaultvalue').append($("<option>").val('').text(''));
			for(var hh=0;hh < ddsdatas.length;hh++) {
				select.append($("<option>").val(ddsdatas[hh]).text(ddsdatas[hh]));
				$('#dddefaultvalue').append($("<option>").val(ddsdatas[hh]).text(ddsdatas[hh]));
			}
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});	
		//relation module name change event
		$('#attrmodulename').change(function(){
			var attrid = $('#elemidentiryid').val();			
			fromelementsvalueorganize(attrid);
			var id=$('#attrmodulename').find('option:selected').data('datamoduleid');
			if(id){
				dropdownmodulefilednameset('frmelefiledname',id);
				dropdownmodulefilednameset('elemntsfieldname',id);
			}
			//module name set to header
		});
		//drop down default value change event
		$('#dddefaultvalue').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//tag def value change evt
		$('#tagdefaultvalue').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//module action assign
		$('#moduleactionname').change(function(){
			var data = $(this).select2('data');
			var finalResult = [];
			for( item in $(this).select2('data') ) {
				finalResult.push(data[item].id);
			};
			var valids = finalResult.join(',');
			$('#modactionslist').val(valids);
			//toolbariconsview(finalResult);
		});
		//cond field name change event
		$('#frmelefiledname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//field name change event
		$('#elemntsfieldname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//Condition change event
		$('#frmelecondname').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//condition value change event
		$('#frmelmcondval').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//file size value change event
		$('#attrdatafilesize').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
		//file type value change event
		$('#attrdataimgfiletype').change(function(){
			var attrid = $('#elemidentiryid').val();
			fromelementsvalueorganize(attrid);
		});
	}
	{//module save
		$('#modbuildersavebtn').click(function(){
			$("#normalviewpreview").trigger('click');
			var checkstepper1 = $('.stepperul1').hasClass('stepperulhideotherelements'); 
			if(checkstepper1 == false){
				$(".steppercategory1").trigger('click');
			}
			if($("#changemodname").val()== 'No'){
				$("#newmodname").removeClass("validate[required]");
			}
			$("#formmodulenamvalidate").validationEngine('validate');
		});
		$("#formmodulenamvalidate").validationEngine({
			onSuccess: function() {
				var len = $('#spanforaddtab span.mandatoryfildclass').length;
				if(len >= 1) {
					moduledatacreate();
				} else {
					alertpopup('Module should have atleast one mandatory field');
				}
			},
			onFailure: function() {
				var dropdownid =['2','newmodulecrename','moduleactionname'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	//existng module view generation
	$('#newmodulecrename').change(function(){
		var moduleid = $('#newmodulecrename').val();
		//set module name to header
		var modname = $('#newmodulecrename').find('option:selected').data('datamodname');
		var modicon = $('#newmodulecrename').find('option:selected').data('modicon');
		var modlink = $('#newmodulecrename').find('option:selected').data('modlink');
		$('#modnamediv').text(modname);
		$('#modicondiv').addClass(modicon);
		if(moduleid != '') {
			tollbaractionbasedmodule(moduleid);
			$.ajax({
				url:base_url+"Formeditor/formfieldselements",
				data:"data=&moduleid="+moduleid,
				type:"post",
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					$('#spanforaddtab').empty();
					$('.formheaderspan').removeClass('hidedisplay');
					$('#spanforaddtab').append(data.formdata);
					$('#toolactionicons').empty();
					$('#toolactionicons').append(data.icons);
					inci = $('#inci').val();
					iden = $('#iden').val();
					grp = $('#grp').val();
					cner = $('#cner').val();

					var textboxdraggable = document.getElementById("textboxgen");
					textboxdraggable.addEventListener('dragstart', dragStartx, false);
					textboxdraggable.addEventListener('dragend'  , dragEndx  , false);
						
					var textareadraggable = document.getElementById("textareagen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("integerboxgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("decimalboxgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("percentgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("currencygen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("dategen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("timegen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("emailgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("phonegen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("urlgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("checkboxgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("autonumbergen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("attachmentgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var textareadraggable = document.getElementById("imgattachgen");
					textareadraggable.addEventListener('dragstart', dragStart, false);
					textareadraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var dropdowndraggable = document.getElementById("dropdowngen");
					dropdowndraggable.addEventListener('dragstart', dragStart, false);
					dropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
			
					var passworddraggable = document.getElementById("passwordgen");
					passworddraggable.addEventListener('dragstart', dragStart, false);
					passworddraggable.addEventListener('dragend'  , dragEnd  , false);
			
					var editordraggable = document.getElementById("texteditorgen");
					editordraggable.addEventListener('dragstart', dragStart, false);
					editordraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var dddropdowndraggable = document.getElementById("ddrelationgen");
					dddropdowndraggable.addEventListener('dragstart', dragStart, false);
					dddropdowndraggable.addEventListener('dragend'  , dragEnd  , false);
					
					var tagcomponentdraggable = document.getElementById("tagscomponentgen");
					tagcomponentdraggable.addEventListener('dragstart', dragStart, false);
					tagcomponentdraggable.addEventListener('dragend'  , dragEnd  , false);
					
					/*var treecomponentdraggable = document.getElementById("ddtreegen");
					treecomponentdraggable.addEventListener('dragstart', dragStart, false);
					treecomponentdraggable.addEventListener('dragend'  , dragEnd  , false);*/
					
					/* Draggable event handlers */
					function dragStartx(event) {
						event.dataTransfer.setData('Text', event.target.id);
					}
					function dragEndx(event) {
					}
					for (i = 0; i < inci ; i++) {
						var sdroptarget = document.getElementById("spanforaddelements"+i+"");
						sdroptarget.addEventListener('dragenter', dragEnter  , false);
						sdroptarget.addEventListener('dragover' , dragOver   , false);
						sdroptarget.addEventListener('dragleave', dragLeave  , false);
						sdroptarget.addEventListener('drop',drop,false);
					}	
					/* Drop target event handlers */
					function dragEnter(event) {
						//event.target.style.border = "2px dashed #ff0000";
					}
					function dragOver(event) {
						event.preventDefault();
						return false;
					}
					function dragLeave(event) {
						//event.target.style.border = "none";
					}
					//drop function call
					function drop(event) {
						$("#processoverlay").show();
						event.preventDefault();
						var data = event.dataTransfer.getData("Text");
						var setdataid =  $(this).attr('id');
						var secids = $('#'+setdataid+'').data('spanforaddelements');
						var sectypeid = $("#spantabsectinfo"+secids+"").val();
						var sectype = sectypeid.split(',');
						if(sectype[1] != 3) {
							if(data == 'textboxgen') {
								var label = labelnamecheck("Text");
								if(label > 0 ){
									alertpopup("Text Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="2" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',2,'+labelname+',,100,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id'); 
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'textareagen') {
								var label = labelnamecheck("Text Area");
								if(label > 0 ){
									alertpopup("Text Area Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Area";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><textarea id="frmliveelm'+iden+'" rows="3"></textarea><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="3" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',3,'+labelname+',,100,3,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var tareasortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'integerboxgen') {
								var label = labelnamecheck("Integer");
								if(label > 0 ){
									alertpopup("Integer Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Integer";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="4" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',4,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'decimalboxgen') {
								var label = labelnamecheck("Decimal");
								if(label > 0 ){
									alertpopup("Decimal Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Decimal";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="5" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',5,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'percentgen') {
								var label = labelnamecheck("Percent");
								if(label > 0 ){
									alertpopup("Percent Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Percent";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="6" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',6,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'currencygen') {
								var label = labelnamecheck("Currency");
								if(label > 0 ){
									alertpopup("Currency Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Currency";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="7" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',7,'+labelname+',,2,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dategen') {
								var label = labelnamecheck("Date");
								if(label > 0 ){
									alertpopup("Date Name already present in this form. Kindly change the existing name to add new field"); 
								}else{
									labelname = "Date";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=" "><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="8" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',8,'+labelname+',,,No,Yes,No,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'timegen') {
								var label = labelnamecheck("Time");
								if(label > 0 ){
									alertpopup("Time Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Time";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="9" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',9,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'emailgen') {
								var label = labelnamecheck("Email");
								if(label > 0){
									alertpopup("Email Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Email";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="10" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',10,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'phonegen') {
								var label = labelnamecheck("Phone");
								if(label > 0 ){
									alertpopup("Phone Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Phone";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="11" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',11,'+labelname+',,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'urlgen') {
								var label = labelnamecheck("URL");
								if(label > 0 ){
									alertpopup("URL Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "URL";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="12" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',12,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'checkboxgen') {
								var label = labelnamecheck("Check Box");
								if(label > 0 ){
									alertpopup("Check Box Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Check Box";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><input type="checkbox" id="frmliveelm'+iden+'" value="" class="filled-in"><label for="frmliveelm'+iden+'" id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="13" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',13,'+labelname+',No,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'autonumbergen') {
								var label = labelnamecheck("Auto Number");
								if(label > 0 ){
									alertpopup("Auto Number Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Auto Number";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="14" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',14,'+labelname+',,,,No,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'attachmentgen') {
								var label = labelnamecheck("Attachment");
								if(label > 0 ){
									alertpopup("Attachment Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Attachment";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="15" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',15,'+labelname+',5,,Yes,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'imgattachgen') {
								var label = labelnamecheck("Image Upload");
								if(label > 0 ){
									alertpopup("Image Upload Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Image Upload";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><span class="icon-25b icon25b-display driveiconsize"></span><span class="icon-25b icon25b-dropbox driveiconsize"></span><!--<span class="fa fa-cloud driveiconsize"></span>--><div class="large-12 medium-12 small-12 column" style="height:10rem; border:1px solid #CCCCCC"></div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="16" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',16,'+labelname+',5,.jpg|.jpeg|.exif|.tiff|.gif|.png|.ppm|.pgm|.pbm|.pnm|.bpg,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'dropdowngen') {
								var label = labelnamecheck("Picklist");
								if(label > 0 ){
									alertpopup("Picklist Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Picklist";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="17" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',17,'+labelname+',,,,No,Yes,No,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var ddsortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
									$(".chzn-select").select2({blurOnChange: true});
								}
							} else if(data == 'passwordgen') {
								var label = labelnamecheck("Password");
								if(label > 0 ){
									alertpopup("Password Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Password";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="password" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="22" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',22,'+labelname+',,,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'texteditorgen') {
								var label = labelnamecheck("Text Editor");
								if(label > 0 ){
									alertpopup("Text Editor Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Text Editor";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><div id="frmliveelm'+iden+'"><img src="'+base_url+'img/texteditor.jpg" class=""/> </div><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="24" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',24,'+labelname+',,No,Yes,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddrelationgen') {
								var label = labelnamecheck("Relation");
								if(label > 0 ){
									alertpopup("Relation Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Relation";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><select data-placeholder="Select" class="chzn-select" id="frmliveelm'+iden+'"></select><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="26" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',26,'+labelname+',,,,,,,,No,Yes,No,No,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'tagscomponentgen') {
								var label = labelnamecheck("Tags");
								if(label > 0 ){
									alertpopup("Tags Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tags";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="30" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',30,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							} else if(data == 'ddtreegen') {
								var label = labelnamecheck("Tree");
								if(label > 0 ){
									alertpopup("Tree Name already present in this form. Kindly change the existing name to add new field");
								}else{
									labelname = "Tree";
									$(this).find('#sortable'+secids).append('<li id="eleliattrid'+iden+'"><div class="large-12 column frmelediv hovertoremove" id="columndivid'+iden+'" data-frmeledivattr="'+iden+'"><span class="rmvfbelements"><span class="icon24build icon24build-cursor-move2 elementmovebtn" title="Move" data-dataeleliattrid="'+iden+'"></span><span class="icon24build icon24build-close2 elementremovebtn" title="Remove" data-datafieldid="0" data-datafieldrescopt="No"></span></span><label id="frmelemlivespantit'+iden+'">'+labelname+'</label><input type="text" id="frmliveelm'+iden+'" value=""><input type="hidden" name="frmelmattrinfo[]" id="frmelmattrinfo'+iden+'" data-frmeleuitypeattr="21" data-datasectionid="'+secids+'" data-datafieldid="0" value="'+secids+',21,'+labelname+',,,No,Yes,No,0"></div></li>');
									//For Remove Form Elements
									forremoveformelements();
									//For Sorting Elements
									var getclassforsorting = $(this).attr('id');
									var getidforsorting = $("#"+getclassforsorting+"").data('forsorting');
									var sortable = document.getElementById("sortable"+getidforsorting+"");
									var input = $( "#tabfieldlabelspanids" );
									input.val( input.val() + ","+ iden);
								}
							}
						} else {
							alertpopup('Sorry, U cant add any new fields in this section.');
						}
						iden++;
						{// Div click for properties
							$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
							$('.frmelediv').click(function(){
								$('.columnselectionclass').removeClass('columnselectionclass');
								$(this).addClass('columnselectionclass');
								var id = $(this).data('frmeledivattr');
								$('#elemidentiryid').val(id);
								clearform('propertiesclear');
								defaultoptionsset();
								setTimeout(function(){
									fromelementsvaluereorganize(id); 
									Materialize.updateTextFields();
								},200);
								$(".propertytext").addClass('hidedisplay');
								$(".propertyhidedisplay").removeClass('hidedisplay');
								$('#formpropid').trigger('click');
								setTimeout(function(){$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
								$('#propertypanel').css('display','block');
								//Toggle Property pannel
								propertyslidetoggle();
							});
						}
						setTimeout(function(){
							$("#processoverlay").fadeOut();
						},200);
						return false;
					}
					{
						//Tab group name set
						$('.spantabgrpelemgen').focusout(function(){
							var lettercount = $(this).text().length; 
							var tabgrpname = $(this).text();
							var tabid = $(this).data('spantabgrpelemgennum');
							var tabfieldid = $(this).data('tabgrpfieldid');
							if(lettercount == 0){
								$(this).text('Tab Group'+(tabid+1)+'');
							}
							var type = $('#spanforaddsection'+tabid+'').data('tabgrptype');
							$('#spantabgrpinfo'+tabid+'').val(type+','+tabid+','+tabgrpname+','+tabfieldid);
						});
						//section name set
						$('.spansectionelemgen').focusout(function(){
							var tabsecname = $(this).text();
							var scid = $(this).data('spansectionelemgennum');
							var scfldid = $(this).data('sectionfieldid');
							grpnum = $(".currentab").data('spantabgrpelemnum');
							$('#spantabsectinfo'+scid+'').val(grpnum+','+scid+','+tabsecname+','+scfldid);
						});
					}
					{// For Delete Section Hover Button
						for (i = 0; i < inci ; i++) {
							$( "#spanforaddelements"+i+"" ).hover(
								function() {
									$(this).find("span.acticonshover").css("display","block");	
								}, function() {
									$(this).find("span.acticonshover").css("display","none");
							});
						}
					}
					{// For Delete Section 
						$(".secremovebtn").click(function(){
							var sectid = $(this).data('sectionfieldid');
							var secres = $(this).data('sectionfieldresct');
							if(secres == 'No') {
								if(sectid != '0') {
									var oldids = $('#deletedsectionids').val();
									var ids = oldids+','+sectid;
									$('#deletedsectionids').val(ids);
								}
								var removesection = $(this).parent('div').data('spanforaddelements');
								var groupremv = $("#tabseclabelspanids").val();
								var value = $.trim(groupremv.replace(','+removesection,""));
								$("#tabseclabelspanids").val(value);
								$("#spanforaddelements"+removesection+"").parent('.forselectsection').parent('li').remove();
							} else {
								alertpopup('Cannot be deleted because it contains atleast one required or restricted field');
							}
						});
						/* For Move Section from one group to another*/
						$(".secmovebtn").click(function(){
							$("#processoverlay").show();
							var secliid = $(this).data('datasecliattrid');
							$('#secdataliid').val(secliid);
							//set tab group name to drop down
							var txt = "";
							var ddndatas = $("#tabgrplabelspanids").val();
							var ddsdatas = ddndatas.split(',');
							$('#tabgrouplist').children().remove();
							$('#tabgrouplist').append($("<option value=''></option>"));
							for(var hh=0;hh < ddsdatas.length;hh++) {
								var txt = $('#tabgrpblabel'+ddsdatas[hh]+'').text();
								if($('#spanforaddsection'+ddsdatas[hh]+'').data('tabgrptype') == '1') {
									$('#tabgrouplist').append($("<option></option>").attr("value",ddsdatas[hh]).text(txt));
								}
							}
							//set tab section name
							var sectxt = "";
							var grpid = "";
							var secdataids = $("#tabseclabelspanids").val();
							var secddsdatas = secdataids.split(',');
							$('#tabsectionlist').children().remove();
							$('#tabsectionlist').append($("<option value=''></option>"));
							for(var ss=0;ss<secddsdatas.length;ss++) {
								grpid = $('#tabseclabel'+secddsdatas[ss]+'').data('sectabgrpid');
								var sectxt = $('#tabseclabel'+secddsdatas[ss]+'').text();
								if($('#spanforaddsection'+grpid+'').data('tabgrptype') == '1' && secddsdatas[ss]!=secliid) {
									$('#tabsectionlist').append($("<option></option>").attr("label",""+grpid+"").attr("value",secddsdatas[ss]).text(sectxt));
								}
							}
							$("#processoverlay").fadeOut();
							setTimeout(function(){
								$('#movesectotabgrpoverlay').fadeIn();
							},150);
						});
						/* For Move Section from one group to another*/
						$("li .elementmovebtn").click(function(){
							var eleliid = $(this).data('dataeleliattrid');
							$('#fielddataliid').val(eleliid);
							//set tab group name to drop down
							var txt = "";
							var ddndatas = $("#tabgrplabelspanids").val();
							var ddsdatas = ddndatas.split(',');
							$('#fieldtabgrouplist').children().remove();
							$('#fieldtabgrouplist').append($("<option value=''></option>"));
							for(var hh=0;hh < ddsdatas.length;hh++) {
								var txt = $('#tabgrpblabel'+ddsdatas[hh]+'').text();
								//if($('#spanforaddsection'+ddsdatas[hh]+'').data('tabgrptype') == '1') {
									$('#fieldtabgrouplist').append($("<option></option>").attr("value",ddsdatas[hh]).text(txt));
								//}
							}
							//set tab group name
							var sectxt = "";
							var grpid = "";
							var secdataids = $("#tabseclabelspanids").val();
							var secddsdatas = secdataids.split(',');
							$('#fieldtabsectionlist').children().remove();
							$('#fieldtabsectionlist').append($("<option value=''></option>"));
							for(var ss=0;ss <secddsdatas.length;ss++) {
								grpid = $('#tabseclabel'+secddsdatas[ss]+'').data('sectabgrpid');
								if(grpid != undefined){
									var sectxt = $('#tabseclabel'+secddsdatas[ss]+'').text();
									//if($('#spanforaddsection'+grpid+'').data('tabgrptype') == '1') {
										$('#fieldtabsectionlist').append($("<option></option>").attr("label",""+grpid+"").attr("value",secddsdatas[ss]).text(sectxt));
									//}
								}							
							}
							//set tab section name
							var fieldtxt = "";
							var flddataids = $("#tabfieldlabelspanids").val();
							var fldddsdatas = flddataids.split(',');
							$('#tabsectionfieldlist').children().remove();
							$('#tabsectionfieldlist').append($("<option value=''></option>"));
							for(var ss=0;ss <fldddsdatas.length;ss++) {
								var fieldtxt = $('#frmelemlivespantit'+fldddsdatas[ss]+'').text();
								var tabsectid = $('#frmelmattrinfo'+fldddsdatas[ss]+'').data('datasectionid');
								if(tabsectid != undefined){
									$('#tabsectionfieldlist').append($("<option></option>").attr("label",""+tabsectid+"").attr("value",fldddsdatas[ss]).text(fieldtxt));
								}
							}
							setTimeout(function(){
								$('#formelementsid').trigger('click');
								$('#movefieldtotabgrpsecoverlay').fadeIn();
							},250);
						});
					}
					{//For section Selection
						for (i = 0; i < inci ; i++) {
							$("#spanforaddelements"+i+"").click(function(){
								$('.sectionselector').removeClass('sectionselector');
								$(this).closest('.forselectsection').addClass('sectionselector');
							});
						}
					}
					{// Tabs Hover, Tab Remove, Tab Sorting
						{//For tab Close Hover button
							$( ".sidebaricons" ).hover(
							  function() {
								$( this ).find('.tabclsbtn').css('opacity','1');
							  }, function() {
								$( this ).find('.tabclsbtn').css('opacity','0');
							  }
							);
						}
						{//For remove tab and Content area tabclsbtn
							$(".tabclsbtn").click(function(){
								var tabid = $(this).data('datatabgroupid');
								var res = $(this).data("tabgrprestrict");
								if(res=="No") {
									if(tabid != '0') {
										var oldids = $('#deletedtabids').val();
										var ids = oldids+','+tabid;
										$('#deletedtabids').val(ids);
									}
									$(this).closest("li").remove();	
									var getidfordelsection = $(this).siblings("span.spantabgrpelemgen:first").data('spantabgrpelemgennum');	
									var groupremv = $("#tabgrplabelspanids").val();
									var value = $.trim(groupremv.replace(','+getidfordelsection,""));
									$("#tabgrplabelspanids").val(value);
									$("#spanforaddsection"+getidfordelsection+"").remove();
									$(".spantabgrpelemgen:first").trigger('click');
									var countoftab = $('#appendtab li').size();
									if(countoftab == 0){
										$("#spanforaddtab div #appendtab").empty();
										$('#forreaddtabs').addClass('forreaddtabs');
									}
								} else {
									alertpopup('Cannot be deleted because it contains restricted field(s) or mandatory field(s)');
								}
							});	
						}
						{//Tab Click Events
							$(".spantabgrpelemgen").click(function(){
								$(".hidecontainer").removeClass('currentab').addClass('hidedisplay');
								$(".appendtabcurrentplace").removeClass('appendtabcurrentplace');
								$(".activefbuilder").removeClass('activefbuilder');
								$(this).addClass('activefbuilder');
								//To Prevent Line Break -- Enter Key
								$(".activefbuilder").keypress(function(e){ return e.which != 13; });
								var tabclicksh = $(this).data('spantabgrpelemgennum');
								$("#spanforaddsection"+tabclicksh+"").addClass('appendtabcurrentplace').addClass('currentab').removeClass('hidedisplay');
							});
						}
					}
					{// Div click for properties
						$('.frmelediv').find('input, textarea, button, select').attr('disabled','disabled');
						$('.frmelediv').click(function(){
							$('.columnselectionclass').removeClass('columnselectionclass');
							$(this).addClass('columnselectionclass');
							var id = $(this).data('frmeledivattr');
							$('#elemidentiryid').val(id);
							clearform('propertiesclear');
							defaultoptionsset();
							setTimeout(function(){
								fromelementsvaluereorganize(id); 
								Materialize.updateTextFields();
							},200);
							$(".propertytext").addClass('hidedisplay');
							$(".propertyhidedisplay").removeClass('hidedisplay');
							$('#formpropid').trigger('click');
							setTimeout(function(){$('#frmelemattrlablname').focus().setCursorPosition(0);},300);
							$('#propertypanel').css('display','block');
							//Toggle Property pannel
							propertyslidetoggle();
						});
					}
					{//For Tab Group Sorting
						var appendtab = document.getElementById("appendtab");
						new Sortable(appendtab);
						Sortable.create(appendtab, {
							group: "tabgrp",
							animation: 150,
						});
					}
					{// Input Elements Delete and Sorting
						{//For Remove Elements
							forremoveformelements();
						}
						//tab section sorting - without grid or editor
						for (var g = 0; g < grp ; g++) {
							var tabsection = document.getElementById("spanforaddsection"+g+"");
							new Sortable(tabsection);
							Sortable.create(tabsection, {
								animation: 150,
								group:'tabsection',
								draggable: '.newsortsec', 
								handle: '.spansectionelemgen',
							});
						}
						//tab section elements sorting - only section elements
						for (var i = 0; i < inci ; i++) {
							var tabsectionele = document.getElementById("sortable"+i+"");
							Sortable.create(tabsectionele, {
								group: 'sectionelement',
								//onUpdate: function (evt){ console.log('onUpdate.section:', [evt.item, evt.from]); console.log(evt.item); },
								onAdd: function (evt) {
									var dragnodedivid = evt.item.firstChild.id;
									var frmelemattrid = $('#'+dragnodedivid+'').data('frmeledivattr');
									var ulid =$('#'+dragnodedivid+'').parent().closest('ul').attr('id');
									var datasecid = $('#'+ulid+'').data('datasectionattrid');
									dragformelementsvalueorganize(frmelemattrid,datasecid);
								},
							});
						}
					}
					//for select2 set
					$(".chzn-select").select2({blurOnChange: true});
				},
			});
		}
		//$("#moduleactionname").select2Sortable();
		//allow only numbers in user text box
		$('#frmelemstartnum').keypress(function (e) {
			var len = $(this).val().length;
			var specialKeys = new Array();
			specialKeys.push(8); //Backspace
			specialKeys.push(9); //Tab
			specialKeys.push(46); //Delete
			specialKeys.push(36); //Home
			specialKeys.push(35); //End
			specialKeys.push(37); //Left
			specialKeys.push(39);
			var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
			if(len==0) {
				var ret = ((keyCode >= 49 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			} else {
				var ret = ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
			}
			if (ret) {
				return true;
			}
			e.preventDefault();
			return false;
		});
		{// More icon Hover Delay
			$( ".top-bar-section .fa-ellipsis-v" ).hover(
				function() {
					$('.has-dropdown .dropdown.moreicons').addClass('fortabmoreicon');
				}, function() {
					setTimeout(function(){
						$('.has-dropdown .dropdown.moreicons').removeClass('fortabmoreicon');
					},1500); 
				}
			);
		}
	});
	{//Customize Conversion From All modules
		var cusmoduleid = sessionStorage.getItem("customizeid"); 
		if(cusmoduleid != null){
			setTimeout(function(){
				$('#newmodulecrename').select2('val',cusmoduleid).trigger('change');
				sessionStorage.removeItem("customizeid");
				//enable property pane
				var moname = $("#newmodulecrename").val();
				if(moname == ''){
					$("#blockspan").show();
				} else {
					$("#blockspan").hide();
				}
			},100);
		}
	}
	/*bigoverlay */
	$('#recycle').click(function(){
		$("#recycleoverlay").fadeIn();
	});
	{
		$('#changemodname').click(function(){
			if ($(this).is(':checked')) {
				$(this).val('Yes');
				$("#newmodnamediv").show();
			}else{
				$(this).val('No');
				$("#newmodnamediv").hide();
			}
		});
		$('#newmodname').focusout(function(){
			var moname = $("#newmodname").val();
			if(moname == ''){
				$("#changemodname").removeAttr('disabled');
		        $("#changemodname").removeAttr('readonly');
			}else{
				$("#changemodname").attr('disabled', 'disabled');
	            $("#changemodname").attr('readonly', 'readonly');
			}
		});
	}
	{//section / elements move overlay opn
		$('#tabgrouplist').change(function(){
			var tabgrpval = $(this).val();
			var dropdownset = ["1","tabsectionlist",tabgrpval];
			groupdropdownvalset(dropdownset);
		});
		$('#fieldtabgrouplist').change(function(){
			var tabgrpval = $(this).val();
			var dropdownset = ["1","fieldtabsectionlist",tabgrpval];
			groupdropdownvalset(dropdownset);
		});
		$('#fieldtabsectionlist').change(function(){
			var tabgrpval = $(this).val();
			var dropdownset = ["1","tabsectionfieldlist",tabgrpval];
			groupdropdownvalset(dropdownset);
		});
		//move section
		$('#movesectionoverlaybtn').click(function(){
			$("#sectiondatamovevalidate").validationEngine('validate');
		});
		$("#sectiondatamovevalidate").validationEngine({
			onSuccess: function() {
				var secliid=$('#secdataliid').val();
				var tabmoveid = $('#tabgrouplist').val();
				var tosecid = $('#tabsectionlist').val();
				var pos = $('#movepositionlist').val();
				if(tosecid != '') {
					var position = ( (pos == '') ? "after" : pos);
					if(position == "prev") {
						$('#secliattrid'+secliid+'').insertBefore('#secliattrid'+tosecid+'');
					} else if(position == "after") {
						$('#secliattrid'+secliid+'').insertAfter('#secliattrid'+tosecid+'');
					}
					//tab group id update
					var toelementinfo = $('#spantabsectinfo'+tosecid+'').val();
					var datasets = toelementinfo.split(',');
					var tabgrpid = datasets[0];
				} else {
					$('#secliattrid'+secliid+'').prependTo('#spanforaddsection'+tabmoveid+'');
					var tabgrpid = tabmoveid;
				}
				//change property
				var fromelementinfo = $('#spantabsectinfo'+secliid+'').val();
				var fromdatasets = fromelementinfo.split(',');
				fromdatasets[0] = tabgrpid;
				var newdatasets = fromdatasets.join(",");
				$('#spantabsectinfo'+secliid+'').val(newdatasets);
				$('#tabseclabel'+secliid+'').attr('data-sectabgrpid',tabgrpid);
				clearform('sectiondatamoveform');
				$('#secdataliid').val('');
				$('#movesectotabgrpoverlay').fadeOut();
			},
			onFailure: function() {
				var dropdownid =['1','tabgrouplist'];
				dropdownfailureerror(dropdownid);
				alertpopupdouble(validationalert);
			}
		});
		//field move
		$('#fieldsectionoverlaybtn').click(function(){
			$("#elementsdatamovevalidate").validationEngine('validate');
		});
		$("#elementsdatamovevalidate").validationEngine({
			onSuccess: function() {
				var fildliid=$('#fielddataliid').val();
				var tabmoveid = $('#fieldtabgrouplist').val();
				var tosecid = $('#fieldtabsectionlist').val();
				var tofldid = $('#tabsectionfieldlist').val();
				var pos = $('#fieldmovepositionlist').val();
				if(tofldid != '') {
					var position = ( (pos == '') ? "after" : pos);
					if(position == "prev") {
						$('#eleliattrid'+fildliid+'').insertBefore('#eleliattrid'+tofldid+'');
					} else if(position == "after") {
						$('#eleliattrid'+fildliid+'').insertAfter('#eleliattrid'+tofldid+'');
					}
					//tab section id update
					var toelementinfo = $('#frmelmattrinfo'+tofldid+'').val();
					var datasets = toelementinfo.split(',');
					var tabsecid = datasets[0];
				} else {
					$('#eleliattrid'+fildliid+'').prependTo('#sortable'+tosecid+'');
					var tabsecid = tosecid;
				}
				//change property
				var fromelementinfo = $('#frmelmattrinfo'+fildliid+'').val();
				var fromdatasets = fromelementinfo.split(',');
				fromdatasets[0] = tabsecid;
				var newdatasets = fromdatasets.join(",");
				$('#frmelmattrinfo'+fildliid+'').val(newdatasets);
				$('#frmelmattrinfo'+fildliid+'').attr('data-datasectionid',tabsecid);
				clearform('fielddatamoveform');
				$('#secdataliid').val('');
				$('#movefieldtotabgrpsecoverlay').fadeOut();
			},
			onFailure: function() {
				var dropdownid =['2','fieldtabgrouplist','fieldtabsectionlist'];
				dropdownfailureerror(dropdownid);
				alertpopupdouble(validationalert);
			}
		});
		/*section move overlay close*/
		$('#movesectotabgrpclose').click(function(){
			clearform('sectiondatamoveform');
			$('#secdataliid').val('');
			$("#movesectotabgrpoverlay").fadeOut();
		});
		/*element move overlay close*/
		$('#movefildtotabgrpsecclose').click(function(){
			clearform('fielddatamoveform');
			$('#secdataliid').val('');
			$("#movefieldtotabgrpsecoverlay").fadeOut();
		});
	}
	/*Close customization*/
	$('#customizationclose').click(function(){
		customizationcloseconfirmalertmsg('closemodcustomizeform','Do you wish to close form?');
		$("#closemodcustomizeform").val('Close');
		$("#closemodcustomizeform").click(function(){
			window.location=base_url+'Moduleeditor';
			$('#closemodcustomizeform').unbind();
		});
	});
});

//form elements value organize
function fromelementsvalueorganize(attrid) {
	//$("#processoverlay").show();
	var uitypeid = $('#frmelmattrinfo'+attrid+'').data('frmeleuitypeattr');
	var secid = $('#frmelmattrinfo'+attrid+'').data('datasectionid');
	var fieldid = $('#frmelmattrinfo'+attrid+'').data('datafieldid');
	var lablname = $('#frmelemattrlablname').val();
	var labldefval = $('#frmelemattrdefname').val();
	var datedefval = $('#dateaddfielddefval').val();
	var timedefval = $('#timeaddfielddefval').val();
	var txtlen = $('#frmelemattrlength').val();
	var txtarealen = $('#frmelemattrtextlength').val();
	var checkshowhide = $('#elecheckshowhide').val();
	var checkshfldname = $('#elecheckshfldname').val();
	var checkshflddiv = $('#elecheckshfldname').find('option:selected').data('datadiv');
	var desc = $('#frmelemattrdesc').val();
	var mandopt = $('#fildmandatory').val();
	var activeopt = $('#fieldactive').val();
	var readonlyopt = $('#fieldreadonly').val();
	var rowlen = $('#frmelemattrrows').val();
	var multiselect = $('#addfieldmultiple').val();
	var columtype = $('#fieldcolumntype').val();
	var decsize = $('#frmelemdecilength').val();
	var autosuffix = $('#frmelemsuffix').val();
	var startnum = $('#frmelemstartnum').val();
	var autoprefix = $('#frmelemprefix').val();
	var autonumtype = $('#attrautonumbertype').val();
	var sortorder = $('#addalphasortorder').val();
	var ddvalue = $('#frmelemdatavalue').val();
	var ddmodvalue = $('#attrmodulename').val();
	var dddefvalue = $('#dddefaultvalue').val();
	var tagdefvalue = $('#tagdefaultvalue').val();
	var ddcondname = $('#frmelecondname').val();
	var ddcondval = $('#frmelmcondval').val();
	var chkbxdef = $('#addfieldsetdef').val();
	var uniqval = $('#addfieldunique').val();
	var featdateenable = $('#featuredate').val();
	var pastdateenable = $('#pastdate').val();
	var changeyearenable = $('#changeyear').val();
	var currentdateenable = $('#currentdate').val();
	var filesize = $('#attrdatafilesize').val();
	var filetype = $('#attrdataimgfiletype').val();
	
	if(uitypeid == '2') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '3') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+txtarealen+','+rowlen+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '4') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '5') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '6') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	}  else if(uitypeid == '7') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+decsize+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '8') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+datedefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+featdateenable+','+pastdateenable+','+changeyearenable+','+currentdateenable+','+fieldid;
	} else if(uitypeid == '9') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+timedefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '10') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+uniqval+','+fieldid;
	} else if(uitypeid == '11') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+uniqval+','+fieldid;
	} else if(uitypeid == '12') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '13') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+checkshowhide+','+checkshflddiv+','+checkshfldname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+chkbxdef+','+columtype+','+fieldid;
	} else if(uitypeid == '14') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+autoprefix+','+startnum+','+autosuffix+','+autonumtype+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '15') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+filesize+','+desc+','+multiselect+','+mandopt+','+activeopt+','+columtype+','+fieldid;
	} else if(uitypeid == '16') {
		filetype = filetype.replace(/\,/g, '|');
		var attrinfo = secid+','+uitypeid+','+lablname+','+filesize+','+filetype+','+desc+','+mandopt+','+activeopt+','+columtype+','+fieldid;
	} else if(uitypeid == '17') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '18') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '19') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '20') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '21') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '22') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+labldefval+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '23') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '24') {
		var attrinfo = secid+','+uitypeid+','+lablname+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+columtype+','+fieldid;
	} else if(uitypeid == '25') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '26') {
		var fieldname = $('#elemntsfieldname').val();
		var elmfieldid = $('#elemntsfieldname').find('option:selected').data('colnamehid');;
		var elmfielduiid = $('#elemntsfieldname').find('option:selected').data('uitypeidhid');
		var elmfieldtab = $('#elemntsfieldname').find('option:selected').data('tablenamehid');
		var partblname = $('#elemntsfieldname').find('option:selected').data('parenttablehid');
		elmfieldid = ((elmfieldid!==undefined)?elmfieldid:'');
		elmfielduiid = ((elmfielduiid!==undefined)?elmfielduiid:'');
		elmfieldtab = ((elmfieldtab!==undefined)?elmfieldtab:'');
		partblname = ((partblname!==undefined)?partblname:'');
		
		var condfildname = $('#frmelefiledname').val();
		var condcolname = $('#frmelefiledname').find('option:selected').data('colnamehid');
		var condtblname = $('#frmelefiledname').find('option:selected').data('tablenamehid');
		var flduitypeid = $('#frmelefiledname').find('option:selected').data('uitypeidhid');
		condcolname = ((condcolname!==undefined)?condcolname:'');
		condtblname = ((condtblname!==undefined)?condtblname:'');
		flduitypeid = ((flduitypeid!==undefined)?flduitypeid:'');
		
		var conditions = partblname+'|'+elmfieldid+'|'+elmfieldtab+'|'+elmfielduiid+'|'+condcolname+'|'+condtblname+'|'+flduitypeid;
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddmodvalue+','+fieldname+','+condfildname+','+ddcondname+','+ddcondval+','+desc+','+conditions+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+fieldid;
	} else if(uitypeid == '27') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '28') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '29') {
		ddvalue = ddvalue.replace(/\,/g, '|');
		if(dddefvalue == 'null') {
			dddefvalue='';
		}
		var attrinfo = secid+','+uitypeid+','+lablname+','+ddvalue+','+dddefvalue+','+desc+','+mandopt+','+activeopt+','+readonlyopt+','+multiselect+','+columtype+','+sortorder+','+fieldid;
	} else if(uitypeid == '30') {
		tagdefvalue = tagdefvalue.replace(/\,/g, '|');
		var attrinfo = secid+','+uitypeid+','+lablname+','+tagdefvalue+','+desc+','+mandopt+','+activeopt+','+columtype+','+fieldid;
	}
	$('#frmelmattrinfo'+attrid+'').val(attrinfo);
	//$("#processoverlay").fadeOut();
}
//form elements values reorganize
function fromelementsvaluereorganize(id) {
	//$("#processoverlay").show();
	formelementsfieldsshowhide(id);
	var attrinfo = $('#frmelmattrinfo'+id+'').val();
	var rescopt =  $('#frmelmattrinfo'+id+'').data('datafieldrescopt');
	var datafieldid = $('#frmelmattrinfo'+id+'').data('datafieldid');
	var attrdatas = attrinfo.split(',');
	var uitypeid = attrdatas[1];
	if(attrdatas[2] != "") {
		$('#frmelemattrlablname').val(attrdatas[2]);
		$('#oldlabelname').val(attrdatas[2]);
	}
	if(uitypeid == '2') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrlength').val(attrdatas[4]);
		$('#frmelemattrlength').attr('datafieldid',datafieldid);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '3') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrtextlength').val(attrdatas[4]);
		$('#frmelemattrtextlength').attr('datafieldid',datafieldid);
		$('#frmelemattrrows').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[6]);
		var chkboxinfo = {'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'fieldcolumntype':attrdatas[10]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '4') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '5') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemdecilength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '6') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemdecilength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '7') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemdecilength').val(attrdatas[4]);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'fieldcolumntype':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '8') {
		$('#dateaddfielddefval').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8],'featuredate':attrdatas[9],'pastdate':attrdatas[10],'changeyear':attrdatas[11],'currentdate':attrdatas[12]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '9') {
		$('#timeaddfielddefval').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '10') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8],'addfieldunique':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '11') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8],'addfieldunique':attrdatas[9]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '12') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '13') {		
		$('#elecheckshfldname').select2('val',attrdatas[5]);		
		$('#frmelemattrdesc').val(attrdatas[6]);
		$('#elecheckshowhide').attr('datafieldid',datafieldid);
		var chkboxinfo = {'elecheckshowhide':attrdatas[3],'fildmandatory':attrdatas[7],'fieldactive':attrdatas[8],'fieldreadonly':attrdatas[9],'addfieldsetdef':attrdatas[10],'fieldcolumntype':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);		
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '14') {
		$('#frmelemprefix').val(attrdatas[3]);
		$('#frmelemstartnum').val(attrdatas[4]);
		$('#frmelemsuffix').val(attrdatas[5]);
		$('#frmelemattrdesc').val(attrdatas[7]);
		var chkboxinfo = {'fildmandatory':attrdatas[8],'fieldactive':attrdatas[9],'fieldreadonly':attrdatas[10],'fieldcolumntype':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		$('#attrautonumbertype').val(attrdatas[6]).trigger('change');
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '15') {
		$('#attrdatafilesize').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'addfieldmultiple':attrdatas[5],'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '16') {
		$('#attrdatafilesize').val(attrdatas[3]);
		var ddflsize = attrdatas[4]
		ddflsize = ddflsize.replace(/\|/g, ',');
		$("#attrdataimgfiletype").val(ddflsize);
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#attrdataimgfiletype").trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '17') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '18') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '19') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '20') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '21') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '22') {
		$('#frmelemattrdefname').val(attrdatas[3]);
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldreadonly':attrdatas[7],'fieldcolumntype':attrdatas[8]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '23') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '24') {
		$('#frmelemattrdesc').val(attrdatas[3]);
		var chkboxinfo = {'fildmandatory':attrdatas[4],'fieldactive':attrdatas[5],'fieldreadonly':attrdatas[6],'fieldcolumntype':attrdatas[7]};
		chkboxvalueset(chkboxinfo,rescopt);
		$("#processoverlay").fadeOut();
	} else if(uitypeid == '25') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '26') {
		$("#attrmodulename").attr("readonly",false);
		$("#elemntsfieldname").attr("readonly",false);
		$("#attrmodulename").select2('val',attrdatas[3]);
		$("#frmelecondname").select2('val',attrdatas[6]);
		$('#frmelmcondval').val(attrdatas[7]);
		$('#frmelemattrdesc').val(attrdatas[8]);
		var chkboxinfo = {'fildmandatory':attrdatas[10],'fieldactive':attrdatas[11],'fieldreadonly':attrdatas[12],'addfieldmultiple':attrdatas[13],'fieldcolumntype':attrdatas[14]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#attrmodulename").trigger('change');
			$("#frmelecondname").trigger('change');
			$('#frmelefiledname').val(attrdatas[5])
			$("#frmelefiledname").trigger('change');
			$('#elemntsfieldname').val(attrdatas[4])
			$("#elemntsfieldname").trigger('change');
			if(attrdatas[15]!=0) {
				if(attrdatas[3]!="") {
					$("#attrmodulename").attr("readonly",true);
				} else {
					$("#attrmodulename").attr("readonly",false);
				}
				if(attrdatas[4]!="") {
					$("#elemntsfieldname").attr("readonly",true);
				} else {
					$("#elemntsfieldname").attr("readonly",false);
				}
			}
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '27') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '28') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} else if(uitypeid == '29') {
		var ddndatas = attrdatas[3]
		ddndatas = ddndatas.replace(/\|/g, ',');
		$("#frmelemdatavalue").val(ddndatas);
		var ddsdatas = ddndatas.split(',');
		$("#frmelemdatavalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","," "],
		});
		$('#frmelemattrdesc').val(attrdatas[5]);
		var chkboxinfo = {'fildmandatory':attrdatas[6],'fieldactive':attrdatas[7],'fieldreadonly':attrdatas[8],'addfieldmultiple':attrdatas[9],'fieldcolumntype':attrdatas[10],'addalphasortorder':attrdatas[11]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#frmelemdatavalue").trigger('change');
			$('#dddefaultvalue').val(attrdatas[4]);
			$('#dddefaultvalue').trigger('change');
			$("#processoverlay").fadeOut();
		},50);
	} if(uitypeid == '30') {
		var ddndatas = attrdatas[3];
		ddndatas = ddndatas.replace(/\|/g,',');
		var ddsdatas = ddndatas.split(',');
		$("#tagdefaultvalue").select2({
			tags:ddsdatas,
			//tokenSeparators: [","],
		});
		$('#frmelemattrdesc').val(attrdatas[4]);
		var chkboxinfo = {'fildmandatory':attrdatas[5],'fieldactive':attrdatas[6],'fieldcolumntype':attrdatas[7]};
		chkboxvalueset(chkboxinfo,rescopt);
		setTimeout(function(){
			$("#tagdefaultvalue").select2('val',ddsdatas);
			$("#tagdefaultvalue").trigger('change');
			//$("#processoverlay").fadeOut();
		},50);
	}
}
//drag form elements value organize 
function dragformelementsvalueorganize(frmelemattrid,datasecid) {
	$("#processoverlay").show();
	var frmelementsval = $('#frmelmattrinfo'+frmelemattrid+'').val();
	var datasets = frmelementsval.split(',');
	datasets[0] = datasecid;
	var newdatasets = datasets.join(",");
	$('#frmelmattrinfo'+frmelemattrid+'').val(newdatasets);
	$('#frmelmattrinfo'+frmelemattrid+'').attr('datasectionid', datasecid);
	setTimeout(function(){
		$("#processoverlay").fadeOut();
	},150);
}
//form elements fields show hide
function formelementsfieldsshowhide(id) {
	var uiid = $('#frmelmattrinfo'+id+'').data('frmeleuitypeattr');
	if(uiid == '3') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrrowlen','attrdatatextlen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrdatadecilen','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatalen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '4') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrdatadecilen','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatalen','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '5') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatadecilen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatalen','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '6') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatadecilen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrdatalen','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '7') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','timedefval','attrmodopt','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','datedefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatalen','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '8') {
		var showdataarr = ['columntypeattr','datedefval','featuredatespan','pastdatespan','changeyearspan','currentdatespan'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','defval','attrmodfldcond','attrmodfldval','attrfldname','readonlyattr','attrchkboxsetdef','attrelefldname','attrchkboxunique','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '9') {
		var showdataarr = ['readonlyattr','columntypeattr','timedefval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '10') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrchkboxunique'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '11') {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrchkboxunique'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '12') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '13') {
		var showdataarr = ['readonlyattr','columntypeattr','attrchkboxsetdef','attrcheckshowhide','attrcheckshfldname'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','defval','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrdatarandomopt','attrmodfldcond','attrmodfldval','attrfldname','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen'];
		datahideapply(hidedataarr);
	} else if(uiid == '14') {
		var showdataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','readonlyattr','columntypeattr','attrdatarandomopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrrowlen','attrdatadecilen','datedefval','timedefval','attrmodopt','attrdatalen','defval','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '15') {
		var showdataarr = ['attrdatafilesizeopt','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','readonlyattr','columntypeattr','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '16') {
		var showdataarr = ['attrdatafilesizeopt','attrdataimgfiletypeopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrsortopt','dddefval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '17') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrsortopt','dddefval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '18') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '19') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '20') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '21') {
		var showdataarr = [];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','mulchkopt','columntypeattr','readonlyattr','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '22') {
		var showdataarr = ['readonlyattr','columntypeattr','defval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','mulchkopt','dataplvalattr','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatalen','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '23') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '24') {
		var showdataarr = [];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','attrdatalen','defval','readonlyattr','columntypeattr','mulchkopt','dataplvalattr','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '25') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '26') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrelefldname'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dddefval','attrdatalen','defval','dataplvalattr','attrchkboxsetdef','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '27') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '28') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '29') {
		var showdataarr = ['readonlyattr','columntypeattr','mulchkopt'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrmodopt','attrdatalen','defval','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrsortopt','dataplvalattr','dddefval','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else if(uiid == '30') {
		var showdataarr = ['columntypeattr','tagdefval'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','mulchkopt','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dataplvalattr','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','readonlyattr','attrdatalen','defval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	} else {
		var showdataarr = ['readonlyattr','columntypeattr','defval','attrdatalen'];
		datashowapply(showdataarr);
		var hidedataarr = ['attrdataprefix','attrdatastartnum','attrdatasuffix','mulchkopt','attrrowlen','attrdatadecilen','datedefval','timedefval','attrdatarandomopt','attrsortopt','dataplvalattr','dddefval','attrmodopt','attrmodfldcond','attrmodfldval','attrfldname','attrchkboxsetdef','attrelefldname','attrchkboxunique','featuredatespan','pastdatespan','tagdefval','attrdatafilesizeopt','attrdataimgfiletypeopt','changeyearspan','currentdatespan','attrdatatextlen','attrcheckshowhide','attrcheckshfldname'];
		datahideapply(hidedataarr);
	}
}
//hide elements property
function datahideapply(hidedataarr) {
	for(var i=0;i<hidedataarr.length;i++){
		$('#'+hidedataarr[i]+'').addClass('hidedisplay');
	}
}
//show elements property
function datashowapply(showdataarr) {
	for(var j=0;j<showdataarr.length;j++){
		$('#'+showdataarr[j]+'').removeClass('hidedisplay');
	}
}
/* //module name validate
//,funcCall[picklisttabvalidate]
function picklisttabvalidate() {
	var attrid = $('#elemidentiryid').val();
	var uitypeid = $('#frmelmattrinfo'+attrid+'').data('frmeleuitypeattr');
	if(uitypeid == "17") {
		var picklistname = $('#frmelemattrlablname').val();
		var nmsg = "";
		$.ajax({
			url:base_url+"Formeditor/picklistnamecheck",
			data:"data=&picklistname="+picklistname,
			type:"post",
			async:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "True") {
			return "Auto Relation Is Created!";
		}
	}
} */
//exisiting field check
function labelnamecheck(labelname){
	var nmsg = "";
	var count = 0;
	var fieldlabelidsval = $('#tabfieldlabelspanids').val();
	fieldlabelids = fieldlabelidsval;
	var formlabelidsplit = fieldlabelids.split(",");
		for(var i=0;i<=formlabelidsplit.length;i++) {
			var oldlabelname = $('#frmelemlivespantit'+formlabelidsplit[i]+'').text();
			oldlabelname = oldlabelname.replace("*","");
			if(oldlabelname === labelname){
				count++;
			}
		}
	return count;
}
//field name check
function labelnamevalidate() {
	var attrid = $('#elemidentiryid').val();
	var oldlabel = $('#oldlabelname').val();
	var fname = $('#frmelemattrlablname').val();
	var moduleid = $('#newmodulecrename').val();
	var nmsg = "";
	$.ajax({
		url:base_url+"Formeditor/filednamecheck",
		data:"fieldname="+fname+"&moduleid="+moduleid,
		type:"POST",
		async:false,
		cache:false,
		success :function(msg) {
			nmsg = $.trim(msg);
		},
	});
	if(nmsg != "False") {
		var fid = $('#frmelmattrinfo'+attrid+'').data('datafieldid');
		if(nmsg == fid) {
			$('#frmelemattrlablname').trigger('change');
		} else {
			$('#frmelemattrlablname').val(oldlabel);
			$('#frmelemattrlablname').trigger('change');
			return "Specified label already exist!";
		}
	}
}
//module data create
function moduledatacreate() {
	$("#processoverlay").show();
	$('.frmelediv').find('input, textarea, button, select').attr('disabled',false);
	var formbuilddata=$("#newmodulegenform").serialize();
	var modid = $('#newmodulecrename').val();
	var modname = $('#newmodulecrename').find('option:selected').data('datamodname');
	var modactions = $('#modactionslist').val();
	var newmodname = $('#newmodname').val();
	var mlink = $('#newmodulecrename').find('option:selected').data('modlink');
	if(formbuilddata != "") {
		$.ajax({
			url:base_url+"Formeditor/formdataedit",
			data:"data=&" +formbuilddata+"&modulename="+modname+"&moduleid="+modid+"&actionids="+modactions+"&newmodname="+newmodname,
			type:"post",
			cache:false,
			success :function(msg) {
				var nmsg = $.trim(msg);
				if(nmsg == 'Success') {
					$("#processoverlay").fadeOut();
					$('#spanforaddtab').empty();
					$('.formheaderspan').addClass('hidedisplay');
					clearform('propertiesdataclear');
					$('#moduleinfoid').trigger('click');
					$('#newmodulecrename').select2('val','');
					$('#moduleactionname').empty();
					$('#moduleactionname').val('').trigger('change');
					$('#modnamediv').text('Preview');
					alertpopup('Module Updated Successfully!');
					setTimeout(function(){
						window.location=base_url+mlink;
					},300);
				} else {
					$("#processoverlay").fadeOut();
					alertpopup('Module Update Failed!');
				}
			}
		});
	}
}
//default value set fun
function defaultoptionsset() {
	$('#fieldactive').prop('checked', true);
	$('#fildmandatory,#fieldactive,#fieldreadonly,#addfieldmultiple,#addchkboxfielddef,#fieldcolumntype,#addalphasortorder,#addfieldunique,#featuredate').trigger('change');
}
function checkboxvaluereset(chkboxinfo) {
	$.each( chkboxinfo, function( key,value ) {
		if ( $('#'+value+'').prop('checked') == true ){ 
			$('#'+value+'').val('Yes');
		} else {
			$('#'+value+'').val('No');
		}
	});
}
//check box value set fun
function chkboxvalueset(chkboxinfo,rescopt) {
	$.each( chkboxinfo, function( key, value ) {
		$('#'+key+'').val(value);
		if(value == 'Yes') {
			$('#'+key+'').prop('checked', true);
		} else {
			$('#'+key+'').prop('checked', false);
		} 
		if(rescopt == 'Yes') {
			if(key == 'fildmandatory' || key == 'fieldactive' || key == 'fieldreadonly') {
				$('#'+key+'').css('pointer-events','none');
			}
		} else {
			$('#'+key+'').css('pointer-events','auto');
		}
	});
	//check active field properties
	if ($('#fieldactive').is(':checked')) {
		$('#fieldactive').val('Yes');
		//$('#fildmandatory').css('pointer-events','auto');
		$('.actchk').css('pointer-events','auto');
	} else {
		$('#fieldactive').val('No');
		//$('#fildmandatory').css('pointer-events','none');
		$('.actchk').css('pointer-events','none');
		$('.actchk').prop('checked',false);
		$('.actchk').trigger('click');
		$('.actchk').trigger('click');
	}
}
//remove / move form fields
function forremoveformelements(){
	$( ".rmvfbelements" ).hover(
		function() {
		$(this).css("opacity","1");	
		}, function() {
		$(this).css("opacity","0");
	});
	$(".elementremovebtn").click(function(){
		var value = $(this).parent(".rmvfbelements").parent(".hovertoremove").parent().parent().data('datasectionattrid');
		var valuelength = $("ul").find("[data-datasectionattrid='" + value + "']").children().length;
		if(valuelength === 1){
			alertpopup("This is last field of this section. If you want to drag existing field of this form into this section, drag that field first, after that delete this field.");
		}else{
			$("#processoverlay").show();
			var fieldid = $(this).data('datafieldid');
			var resopt = $(this).data('datafieldrescopt');
			if(resopt == 'No') {
				if(fieldid != '0') {
					var oldids = $('#deletedfieldids').val();
					var ids = oldids+','+fieldid;
					$('#deletedfieldids').val(ids);
				}
				$(this).parent(".rmvfbelements").parent(".hovertoremove").parent().remove();
				clearform('propertiesdataclear');
				$('#fieldactive').prop('checked', true);
				var chkboxinfo = ['fildmandatory','fieldactive','fieldreadonly','addfieldmultiple','fieldcolumntype','addalphasortorder'];
				checkboxvaluereset(chkboxinfo);
				$('#formelementsid').trigger('click');
				if($("#forproppanelshow").hasClass('hideotherelements')) {
					$("#processoverlay").fadeOut();
				} else {
					$("#processoverlay").fadeOut();
					setTimeout(function(){
						$('.fortrigger4').trigger('click');
					},150);
				}
			} else {
				$("#processoverlay").fadeOut();
				setTimeout(function(){
					alertpopup('Restricted field cannot be removed.');
				},150);
			}
		}
	});
	/* For Move Section from one group to another*/
	$("li .elementmovebtn").click(function(){
		var eleliid = $(this).data('dataeleliattrid');
		$('#fielddataliid').val(eleliid);
		//set tab group name to drop down
		var txt = "";
		var ddndatas = $("#tabgrplabelspanids").val();
		var ddsdatas = ddndatas.split(',');
		$('#fieldtabgrouplist').children().remove();
		$('#fieldtabgrouplist').append($("<option value=''></option>"));
		for(var hh=0;hh < ddsdatas.length;hh++) {
			var txt = $('#tabgrpblabel'+ddsdatas[hh]+'').text();
			//if($('#spanforaddsection'+ddsdatas[hh]+'').data('tabgrptype') == '1') {
				$('#fieldtabgrouplist').append($("<option></option>").attr("value",ddsdatas[hh]).text(txt));
			//}
		}
		//set tab section name
		var sectxt = "";
		var grpid = "";
		var secdataids = $("#tabseclabelspanids").val();
		var secddsdatas = secdataids.split(',');
		$('#fieldtabsectionlist').children().remove();
		$('#fieldtabsectionlist').append($("<option value=''></option>"));
		for(var ss=0;ss <secddsdatas.length;ss++) {
			grpid = $('#tabseclabel'+secddsdatas[ss]+'').data('sectabgrpid');
			if(grpid != undefined){
				var sectxt = $('#tabseclabel'+secddsdatas[ss]+'').text();
				$('#fieldtabsectionlist').append($("<option></option>").attr("label",""+grpid+"").attr("value",secddsdatas[ss]).text(sectxt));
			}			
		}
		//set tab section name
		var fieldtxt = "";
		var flddataids = $("#tabfieldlabelspanids").val();
		var fldddsdatas = flddataids.split(',');
		$('#tabsectionfieldlist').children().remove();
		$('#tabsectionfieldlist').append($("<option value=''></option>"));
		for(var ss=0;ss <fldddsdatas.length;ss++) {
			var fieldtxt = $('#frmelemlivespantit'+fldddsdatas[ss]+'').text();
			var tabsectid = $('#frmelmattrinfo'+fldddsdatas[ss]+'').data('datasectionid');
			if(tabsectid != undefined){
				$('#tabsectionfieldlist').append($("<option></option>").attr("label",""+tabsectid+"").attr("value",fldddsdatas[ss]).text(fieldtxt));
			}			
		}
		setTimeout(function(){
			$('#formelementsid').trigger('click');
			$('#movefieldtotabgrpsecoverlay').fadeIn();
		},250);
	});
}
{//Focus First Letter
	$.fn.setCursorPosition = function(pos) {
		this.each(function(index, elem) {
			if (elem.setSelectionRange) {
			  elem.setSelectionRange(pos, pos);
			} else if (elem.createTextRange) {
			  var range = elem.createTextRange();
			  range.collapse(true);
			  range.moveEnd('character', pos);
			  range.moveStart('character', pos);
			  range.select();
			}
		});
		return this;
	};
}
{//Reload Restriction
	function disableF5(e) { 
		if ((e.which || e.keyCode) == 116) {
			e.preventDefault(); 
			if($.trim($('#spanforaddtab').html())) {
				$('#reloadrestriction').fadeIn();
				$('#reloadpageyes').focus();
			} else {
				location.reload();
			}
		}
	}
}
//drop down value set
function dropdownmodulefilednameset(ddname,moduleid) {
	$('#'+ddname+'').val('');
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Formeditor/ddcondfieldnamefetch?id='+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-colnamehid",data[index]['datacolmname'])
					.attr("data-tablenamehid",data[index]['datatblname'])
					.attr("data-parenttablehid",data[index]['datapartable'])
					.attr("data-uitypeidhid",data[index]['datauitype'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#'+ddname+'').trigger('change');
		},
	});
}
//module based tollbar action details fetch
function tollbaractionbasedmodule(moduleid) {
	$.ajax({
		url:base_url+"Formeditor/modcustactionassign",
		data:"mid="+moduleid,
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var grpid = data[0]['grptoolbarid'];
				var grpids = grpid.split(',');
				var tolids = [];
				$('#moduleactionname').empty();
				$('#moduleactionname').append($("<option></option>"));
				$.each(data,function(index) {
					$('#moduleactionname')
					.append($("<option></option>")
					.attr("data-tollname",data[index]['name'])
					.attr("data-tolldesc",data[index]['desc'])
					.attr("data-tolltitle",data[index]['title'])
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
					/* if($.inArray(data[index]['id'],grpids)) {
						tolids.push(data[index]['id']);
					} */
				});
				$('#moduleactionname').select2('val',grpids).trigger('change');
				//toolbariconsview(tolids);
			}
		},
	});
}
//confirmation alert popup
function customizationcloseconfirmalertmsg(idname,msg) {
	$("#basedeleteoverlayforcommodule").show();
	$(".basedelalerttxt").text(msg);
	$(".commodyescls").attr('id',idname);
	$(".commodyescls").focus();
}
//toolbar icons preview
function toolbariconsview(tolids) {
	$('#toolactionicons').empty();
	//var toolids = tolids.sort();
	var size=6;
	var msize = 1;
	var iconspan = "";
	$.each(tolids,function(index,data) {
		var iconclass=$('#moduleactionname option[value="'+data+'"]').data('tolldesc');
		var title = $('#moduleactionname option[value="'+data+'"]').data('tolltitle');
		if(size>index) {
			iconspan+='<span class="icon-box"><i class="icon-w '+iconclass+'" title="'+title+'"></i></span>';
		} else {
			if(msize==1) {
				msize=2;
				iconspan+='<nav class="top-bar" data-topbar style="background:none; height:0px; display: inline-block;line-height: 25px;"><section class="top-bar-section" style="top:-18px"><ul class="right" style="height: 20px !important"><li class="menu-dropdown icon-box"><span id="" title="" class="icon-w icon-circle-down" style="padding-top:0.2rem;padding-right:0.5rem"> </span><ul class="dropdown moreicons" style="width:12rem">';
				iconspan+='<li style="background-color:#546e7a;"><a style="font-size:1.3rem"><span title="'+title+'" class="icon-w '+iconclass+'"></span></a></li>';
			} else {
				iconspan+='<li><a style="font-size:1.3rem"><span title="'+title+'" class="icon-w '+iconclass+'"></span></a></li>';
			}
		}
	});
	if(msize==2) {
		iconspan+='</ul></li></ul></section></nav>';
	}
	$('#toolactionicons').append(iconspan);
	{// More icon Hover Delay
		$( ".top-bar-section .icon-circle-down" ).hover(
			function() {
				$('.menu-dropdown .dropdown.moreicons').addClass('buildermoreicon');
			}, function() {
				setTimeout(function(){
					$('.menu-dropdown .dropdown.moreicons').removeClass('buildermoreicon');
				},1500); 
			}
		);
	}
}
//group values show hide
function groupdropdownvalset(dropdownarray) {
	var p = parseInt(dropdownarray[0]) + 1;
	var name = dropdownarray[p];
	for(i=1;i<=dropdownarray[0];i++) {
		$("#"+dropdownarray[i]+" option").addClass("ddhidedisplay").addClass("ddclose");
		$("#"+dropdownarray[i]+" option[label='"+name+"']").removeClass("ddhidedisplay").removeClass("ddclose");
		$("#"+dropdownarray[i]+"").select2('val','');
	}
}
//Propert panel slide toggle
function propertyslidetoggle(){
	$('.stepperul3').slideDown("slow");
	$('.steppercategory3').find('.toggleicons').addClass('icon24build-chevron-up');
	$('.steppercategory3').find('.toggleicons').removeClass('icon24build-chevron-down');
	$('.stepperul2').slideUp("slow");
	$('.steppercategory2').find('.toggleicons').addClass('icon24build-chevron-down');
	$('.steppercategory2').find('.toggleicons').removeClass('icon24build-chevron-up');
	$('.stepperul1').slideUp("slow");
	$('.steppercategory1').find('.toggleicons').addClass('icon24build-chevron-down');
	$('.steppercategory1').find('.toggleicons').removeClass('icon24build-chevron-up');
}
function builderformheight(){
	//Form Height
	var totalcontainerleftheight = $( window ).height();
	$("#leftsidepanel").css('height',''+totalcontainerleftheight+'px');
	$("#droptarget1").css('height',''+totalcontainerleftheight+'px');
	var reducedheight =  totalcontainerleftheight - 35;
	$(".dropareadiv").css('height',''+reducedheight+'px');
	var reducedheightproppanel =  totalcontainerleftheight - 39;
	$(".steppervertical").css('height',''+reducedheightproppanel+'px');
}