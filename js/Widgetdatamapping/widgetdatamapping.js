$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		widgetdatamappingaddgrid();
		firstfieldfocus();
		//crud action
		crudactionenable();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	{//widget data load
		$("#widgetid").empty();
	}
	$("#editicon").hide();
	//reload
	$("#reloadicon").click(function() {
		resetFields();
		$("#frommodulefieldid,#tomodulefieldid").empty();
		$("#frommodulefieldid,#tomodulefieldid").select2('val','');
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('widgetdatamappingaddgrid');
		sectionpanelheight('groupsectionoverlay');
	});
	{//validation for Company Add 
		$('#widgetdatamappingsavebutton').click(function() {
			$("#widgetdatamappingformaddwizard").validationEngine('validate');
		});
		jQuery("#widgetdatamappingformaddwizard").validationEngine( {
			onSuccess: function() {
				widgetmappingcreate();
			},
			onFailure: function() {
				var dropdownid =['1','leadfieldid'];
				dropdownfailureerror(dropdownid);				
				alertpopup(validationalert);
			}
		});
	}
	{//validation for Company Add
		$('#widgetdatamappingdataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#widgetdatamappingformeditwizard").validationEngine('validate');
		});
		jQuery("#widgetdatamappingformeditwizard").validationEngine( {
			onSuccess: function() {
				leadmappingeditdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','leadfieldid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//lead drop down change
	$("#frommodulefieldid").change(function() {
		var moduleid = $("#widgetid").find('option:selected').data('widgetmoduleid');
		if($(this).val() > 1 && moduleid > 0) {
			var value = $("#frommodulefieldid").find('option:selected').data('uitype');
			fielddropdownloadfunction('tomodulefieldid','modulefieldid','fieldlabel','modulefield',moduleid,'moduletabid',value);		
			$('.cleardataform').validationEngine('hideAll');
		}
	});
	//moduleid
	$("#moduleid").change(function() {
		var val = $(this).val();
		widgetdataload(val);
		if(val > 1){
			$('#frommodulefieldid,#tomodulefieldid').empty();
			$('#frommodulefieldid').append($("<option></option>"));
			$.ajax({
				url:base_url+'Widgetdatamapping/modulefieldname?moduleid='+val,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$('#frommodulefieldid')
							.append($("<option></option>")
							.attr("data-id",data[index]['datasid'])
							.attr("data-uitype",data[index]['uitype'])
							.attr("value",data[index]['datasid'])
							.text(data[index]['dataname']));
						});
					}
					$('#frommodulefieldid').trigger('change');
				},
			});
		}
	});
	$("#widgetid").change(function() {
		$("#frommodulefieldid,#tomodulefieldid").select2('val','');
	});
	{//filter work
		//for toggle
		$('#widgetdatamappingviewtoggle').click(function() {
			if ($(".widgetdatamappingfilterslide").is(":visible")) {
				$('div.widgetdatamappingfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.widgetdatamappingfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#widgetdatamappingclosefiltertoggle').click(function(){
			$('div.widgetdatamappingfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#widgetdatamappingfilterddcondvaluedivhid").hide();
		$("#widgetdatamappingfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#widgetdatamappingfiltercondvalue").focusout(function(){
				var value = $("#widgetdatamappingfiltercondvalue").val();
				$("#widgetdatamappingfinalfiltercondvalue").val(value);
				$("#widgetdatamappingfilterfinalviewconid").val(value);
			});
			$("#widgetdatamappingfilterddcondvalue").change(function(){
				var value = $("#widgetdatamappingfilterddcondvalue").val();
				if( $('#widgetdatamappingfilterddcondvalue').hasClass('select2-container-multi') ) {
					widgetdatamappingmainfiltervalue=[];
					$('#widgetdatamappingfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    widgetdatamappingmainfiltervalue.push($cvalue);
						$("#widgetdatamappingfinalfiltercondvalue").val(widgetdatamappingmainfiltervalue);
					});
					$("#widgetdatamappingfilterfinalviewconid").val(value);
				} else {
					$("#widgetdatamappingfinalfiltercondvalue").val(value);
					$("#widgetdatamappingfilterfinalviewconid").val(value);
				}
			});
		}
		widgetdatamappingfiltername = [];
		$("#widgetdatamappingfilteraddcondsubbtn").click(function() {
			$("#widgetdatamappingfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#widgetdatamappingfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(widgetdatamappingaddgrid,'widgetdatamapping');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Dashboards';
	});
});
function fielddropdownloadfunction(ddname,fieldid,fieldname,tablename,moduleid,moduletabid,value) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Widgetdatamapping/fetchdddatawithcond?fieldid='+fieldid+'&fieldname='+fieldname+'&tablename='+tablename+'&moduleid='+moduleid+'&value='+value+"&moduletabid="+moduletabid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
//new data add submit function
function widgetmappingcreate() {
	var formdata = $("#widgetdatamappingaddform").serialize();
	var moduleid = $('#moduleid').find('option:selected').val();
	var widgetid = $('#widgetid').find('option:selected').val();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Widgetdatamapping/newdatacreate",
        data: "datas=" + datainformation+"&moduleid="+moduleid+"&widgetid="+widgetid,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
            	refreshgrid();
            	$(".addsectionclose").trigger("click");
				$(".ftab").trigger('click');
				resetFields();
			}
        },
    });
}
//edit operation
function leadmappingeditdataaddfun() {
	var formdata = $("#widgetdatamappingaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Widgetdatamapping/editdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE'){
            	refreshgrid();
				$(".ftab").trigger('click');
				resetFields();
				//Keyboard short cut
				saveformview = 0;
			} 
        },
    });
}
//Menu Category Add Grid
function widgetdatamappingaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $( '#widgetdatamappingaddgrid' ).width();
	var wheight = $( window ).height();
	//col sort
	var sortcol = $("#widgetdatamappingsortcolumn").val();
	var sortord = $("#widgetdatamappingsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.widgetdatamappingheadercolsort').hasClass('datasort') ? $('.widgetdatamappingheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.widgetdatamappingheadercolsort').hasClass('datasort') ? $('.widgetdatamappingheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.widgetdatamappingheadercolsort').hasClass('datasort') ? $('.widgetdatamappingheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#widgetdatamappingviewfieldids').val();
	var filterid = $("#widgetdatamappingfilterid").val();
	var conditionname = $("#widgetdatamappingconditionname").val();
	var filtervalue = $("#widgetdatamappingfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=widgetdatamapping&primaryid=widgetdatamappingid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#widgetdatamappingaddgrid').empty();
			$('#widgetdatamappingaddgrid').append(data.content);
			$('#widgetdatamappingaddgridfooter').empty();
			$('#widgetdatamappingaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('widgetdatamappingaddgrid');
			maingridresizeheightset('widgetdatamappingaddgrid');
			{//sorting
				$('.widgetdatamappingheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.widgetdatamappingheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.widgetdatamappingheadercolsort').hasClass('datasort') ? $('.widgetdatamappingheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.widgetdatamappingheadercolsort').hasClass('datasort') ? $('.widgetdatamappingheadercolsort.datasort').data('sortorder') : '';
					$("#widgetdatamappingsortorder").val(sortord);
					$("#widgetdatamappingsortcolumn").val(sortcol);
					var sortpos = $('#widgetdatamappingaddgrid .gridcontent').scrollLeft();
					widgetdatamappingaddgrid(page,rowcount);
					$('#widgetdatamappingaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('widgetdatamappingheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					widgetdatamappingaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#widgetdatamappingpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					widgetdatamappingaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#widgetdatamappingaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#widgetdatamappingpgrowcount').material_select();		
		},
	});
}
{//crud operation
	function crudactionenable() {
		{//add icon click
			$('#addicon').click(function(){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				$('#widgetdatamappingdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
				$('#widgetdatamappingsavebutton').show();
			});
		}
		$('#deleteicon').click(function() {
			var datarowid = $('#widgetdatamappingaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$('#widgetdatamappingprimarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#widgetdatamappingprimarydataid').val();
			leadmaprecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#widgetdatamappingpgnum li.active').data('pagenum');
		var rowcount = $('ul#widgetdatamappingpgnumcnt li .page-text .active').data('rowcount');
		widgetdatamappingaddgrid(page,rowcount);
	}
}
{// Record delete function
	function leadmaprecorddelete(datarowid)	{
		$.ajax({
			url: base_url + "Widgetdatamapping/deleteinformationdata?primarydataid="+datarowid,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					alertpopup('Record deleted successfully');
					$("#basedeleteoverlay").fadeOut();
				}
			},
		});
	}
}
//lead conversion data fetch
function widgetdatamappingdatafetch(datarowid){
	$.ajax({
		url: base_url + "Widgetdatamapping/datafetchineditform?primarydataid="+datarowid,
		dataType: 'json',
		async:false,
		cache:false,
		success: function(data)  {
			var leadid = data['leadid'];
			var accountid = data['accountid'];
			var contactid = data['contactid'];
			var opportunityid = data['opportunityid'];
			$("#leadfieldid").select2('val',leadid).trigger('change');
			setTimeout(function() {
				$("#moduleid").select2('val',accountid);
				$("#frommodulefieldid").select2('val',contactid);
				$("#tomodulefieldid").select2('val',opportunityid);
			},50);
		},
	});
}
//account field checked
function accountfieldcheck(){
	var fieldid = $("#moduleid").val();
	var editid = $("#editid").val();
	var tablename = "widgetdatamappingmapping";
	var tableid = "accountid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Widgetdatamapping/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Account field name already exists!";
		}
	}
}
//Contact field checked
function contactfieldcheck() {
	var fieldid = $("#frommodulefieldid").val();
	var editid = $("#editid").val();
	var tablename = "widgetdatamappingmapping";
	var tableid = "contactid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Widgetdatamapping/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Contact field name already exists!";
		}
	}
}
//Opportunity field checked
function opportunityfieldcheck(){
	var fieldid = $("#tomodulefieldid").val();
	var editid = $("#editid").val();
	var tablename = "widgetdatamappingmapping";
	var tableid = "opportunityid";
	var nmsg = "";
	if( fieldid !="" ) {
		$.ajax({
			url:base_url+"Widgetdatamapping/accountfieldnamecheck",
			data:"data=&fieldid="+fieldid+"&tablename="+tablename+"&primaryid="+editid+"&tableid="+tableid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			return "Opportunity field name already exists!";
		}
	}
}
//widget data load
function widgetdataload(moduleid) {
	$('#widgetid').empty();
	$('#widgetid').append($("<option></option>"));
	$.ajax({
		url:base_url+"Widgetdatamapping/widgetdatalaodfun?moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#widgetid')
					.append($("<option></option>")
					.attr("data-widgetmoduleid",data[index]['widgetmoduleid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['widgetlabel']));
				});
			}
		},
	});
}