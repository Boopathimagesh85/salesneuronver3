$(document).ready(function() {	
	CONVERTID = '';
	UPDATEID = 0;
	{// Foundation Initilization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{// Grid Calling
		contactcreategrid();
		//crud action
		crudactionenable();
	}
	{// Clear the form - Regenerate the number
		$('#formclearicon').click(function() {
			$('#imagenameattachdisplay').empty();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		});
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Close Add Screen
		var addcloseviewcreation =["viewcloseformiconid","contactcreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{// Tool bat click function Reload,Summary,Detail view,Mail
		$("#cloneicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				if(softwareindustryid == 4){
					contactplanaddgrid();
				}
				$("#processoverlay").show();
				contactsclonedatafetchfun(datarowid);
				$("#imagenamedivhid").removeClass('hidedisplay');
				$(".documentslogodownloadclsbtn").show();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				Materialize.updateTextFields();
				$("#primarydataid").val('');
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('contactcreategrid');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				if(softwareindustryid == 4){
					contactplanaddgrid();
				}
				//Function Call For Edit
				$("#processoverlay").show();
				froalaset(froalaarray);
				contactseditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','contactcreationformadd');
				$("#imagenamedivhid").addClass('hidedisplay');
				$(".documentslogodownloadclsbtn").hide();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','contactcreationformadd');
			$("#imagenamedivhid").removeClass('hidedisplay');
		});
		//mail validate
		$("#mailicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var validemail=validateemailanddonotmail(datarowid,'contact','emailid','donotmail');
				var datamail = validemail.split(',');
				if(datamail[0] == true) {
					if(datamail[1] == 'No'){
						var emailtosend = getvalidemailid(datarowid,'contact','emailid','','');
						sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
						sessionStorage.setItem("forsubject",'');
						sessionStorage.setItem("moduleid",'203');
						sessionStorage.setItem("recordid",datarowid);
						var fullurl = 'erpmail/?_task=mail&_action=compose';
						window.open(''+base_url+''+fullurl+'');
					} else{
						alertpopup("This contact has choosed the Do Not Mail option. You cant send a mail to this contact");
					}
				} else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Close Add Screen
		var addcloseconcreation =["closeaddform","contactcreationview","contactcreationformadd"];
		addclose(addcloseconcreation);
	}
	{// View by drop down change
		$('#dynamicdddataview').change(function() {
			refreshgrid();
		});
	}
	{// Validation for Contact Add
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				contactnewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Update Contact information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				contactdataupdateinformationfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}	
	{
		if(softwareindustryid == 4){				
			$("#contactsplaingrideditspan1").removeAttr('style');
			$('#contactsplaupdatebutton').removeClass('updatebtnclass');
			$('#contactsplaupdatebutton').removeClass('hidedisplayfwg');
			$('#contactsplaaddbutton').removeClass('addbtnclass');
			$("#contactsplaupdatebutton").hide();
			{// Dynamic form to grid
				griddataid = 0;
				gridynamicname = '';
				METHOD = 'ADD';
				$('.frmtogridbutton').click(function(){
					var operation = $(this).attr('id');
					$('#contactdetailid').val(0);
					var i = $(this).data('frmtogriddataid');
					var gridname = $(this).data('frmtogridname');			
					gridformtogridvalidatefunction(gridname,operation);
					griddataid = i;
					gridynamicname = gridname;
					$("#"+gridname+"validation").validationEngine('validate');
					METHOD = 'ADD';
				});
			}
			$("#membershipplans").change(function(){
				var planid = $(this).val();
				if(planid){
					$.ajax({
						url:base_url+"Contact/membershipplansdatafectch?planid="+planid,
						dataType:'json',
						async:false,
						cache:false,
						success:function(data) {
							var modid = data.batchnameid;
							var arraymode = modid.split(',');
							$("#batchnameid").select2('val',arraymode);
							$("#durationid").select2('val',data.durationid);
							$("#amount").val(data.unitprice);	
							dateexpiry();
							Materialize.updateTextFields();
						}
					});
				}else{
					$("#batchnameid").select2('val','');
					$("#durationid").select2('val','');
					$("#amount").val('');
					$('#expirydate').val('');
				}				
			});
			
			$('#joiningdate').change(function(){
				dateexpiry();
			});
			$('#durationid').change(function(){
				dateexpiry();
			});
			function dateexpiry(){
				var duration = $('#durationid').val();
				var joiningdate = $('#joiningdate').datepicker('getDate');
				var month = 0;
				if(joiningdate){
					if(duration == 2){
						month = 1;
						var CurrentDate = new Date(joiningdate);
						CurrentDate.setMonth(CurrentDate.getMonth() + month);								
						$('#expirydate').datetimepicker('setDate',CurrentDate);
					}else if(duration == 3){
						month = 3;
						var CurrentDate = new Date(joiningdate);
						CurrentDate.setMonth(CurrentDate.getMonth() + month);								
						$('#expirydate').datetimepicker('setDate',CurrentDate);
					}else if(duration == 4){
						month = 6;
						var CurrentDate = new Date(joiningdate);
						CurrentDate.setMonth(CurrentDate.getMonth() + month);								
						$('#expirydate').datetimepicker('setDate',CurrentDate);
					}else if(duration == 5){
						month = 12;
						var CurrentDate = new Date(joiningdate);
						CurrentDate.setMonth(CurrentDate.getMonth() + month);								
						$('#expirydate').datetimepicker('setDate',CurrentDate);
					}
				}				
			}
			$("#contactsplaingridedit1").click(function() {
				var selectedrow = $('#contactsplaaddgrid1 div.gridcontent div.active').attr('id');
				if(selectedrow) {
					formheight();
					$("#contactsplaoverlay").removeClass("closed");
					$("#contactsplaoverlay").addClass("effectbox");
					clearform('gridformclear');
					gridtoformdataset('contactsplaaddgrid1',selectedrow);
					$('#contactsplaupdatebutton').removeClass('hidedisplayfwg');	//display the UPDATE button(inner - productgrid)
					$('#contactsplaupdatebutton').show();
					$('#contactsplaaddbutton').hide();	//display the ADD button(inner-productgrid) */
					METHOD = 'UPDATE';
					UPDATEID = selectedrow;
					Materialize.updateTextFields();	
				} else {
					alertpopup('Please Select The Row To Edit');
				}
			});
			$("#contactsplaingriddel1").click(function(){
				var datarowid = $('#contactsplaaddgrid1 div.gridcontent div.active').attr('id');
				if(datarowid) {		
					if(METHOD == 'UPDATE') {
						alertpopup("A Record Under Edit Form");
					} else {
						/*delete grid data*/
						deletegriddatarow('contactsplaaddgrid1',datarowid);
					}
				} else {
					alertpopup("Please select a row");
				}
			});	
		}
	}
	{// Redirected form Home
		add_fromredirect("contactaddsrc",203);
	}
	//Contact-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique203");
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			 setTimeout(function() {
				contactseditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','contactcreationformadd');
				sessionStorage.removeItem("reportunique203");
			},50);
		}
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
		if(softwareindustryid == 2){
			//EMR Master icon
			$("#emrmastericon").click(function() {
				var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
				if (datarowid) {
					document.cookie = "patientid="+datarowid+"";
					window.location = base_url+'Emrmaster';
				} else {
					alertpopup("Please select a row");
				}
			});
			//Appointment
			$("#bookappointmenticon").click(function() {
				var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
				if (datarowid) {
					var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
					sessionStorage.setItem("patientid",datarowid);
					window.location = base_url+'Activity';
					Materialize.updateTextFields();
				} else {
					alertpopup("Please select a row");
				}
			});
			//Invoice
			$("#invoiceicon").click(function() {
				var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
				if (datarowid) {
					var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
					sessionStorage.setItem("patientid",datarowid);
					window.location = base_url+'Invoice';
					Materialize.updateTextFields();
				} else {
					alertpopup("Please select a row");
				}
			});
			//emrmaster - patient session check
			{// Redirected form emrmaster
				var patientdata = sessionStorage.getItem("patientdata");
				if(patientdata != '' && patientdata != null) {
					setTimeout(function() {
						$("#"+patientdata+"").addClass("active");
						$("#editicon").trigger('click');
						sessionStorage.removeItem("patientdata");
					},200);
				}
			}
		}
		//Payment
		$("#paymenticon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
				sessionStorage.setItem("datarowid",datarowid);
				window.location = base_url+'Payment';
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Dashboard Icon Click Event		
		$('#dashboardicon').click(function(){
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				setTimeout(function(){
					$("#processoverlay").hide();
				},1000);
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});
	//sms icon
	$("#smsicon").click(function() {
		var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					for(var i=0;i<data.length;i++) {
						if(data[i] != '') {
							mobilenumber.push(data[i]);
						}
					}
					if(mobilenumber.length > 1) {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").hide();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						mobilenumberload(mobilenumber,'smsmobilenumid');
					} else if(mobilenumber.length == 1) {
						sessionStorage.setItem("mobilenumber",mobilenumber);
						sessionStorage.setItem("viewfieldids",viewfieldids);
						sessionStorage.setItem("recordis",datarowid);
						window.location = base_url+'Sms';
					} else {
						alertpopup("Invalid mobile number");
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	//sms overlay mobile number dd change
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	//sms overlay submit
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}
	});
	{//outgoing call
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var validemail=validatecall(datarowid,'contact','donotcall');
				if(validemail == 'No') {
					var viewfieldids = $("#viewfieldids").val();
					$.ajax({
						url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
						dataType:'json',
						async:false,
						cache:false,
						success :function(data) {
							mobilenumber = [];
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1) {
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						},
					});
				} else {
					alertpopup("This Contact Choosed the Do Not Call Option. So You can't Call to This Contact");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		$("#callnumbersubmit").click(function() {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
	}
	{// Shipping billing address details fetching
		$("#primaryaddresstype,#secondaryaddresstype").change(function() {
			var id=$(this).attr('id');
			setaddress(id);
			Materialize.updateTextFields();
		});
		//shipping /billing hiding
		$("#primaryaddresstype option[value='7']").remove();
		$("#secondaryaddresstype option[value='8']").remove();
	}
	{//sync icon for contacts
		$("#synchronizeicon").click(function() {
			$("#gcontactsyncoverlay").show();
		});
	}
	$("#convertyes").click(function(){
		sessionStorage.setItem("convertcontacttoinvoice",CONVERTID);
		window.location = base_url+'Invoice';
	});	
	$("#convertno").click(function(){
		$("#coninvoverlay").fadeOut();
		var dynview = $("#dynamicdddataview").val();
		$('#contacttag').select2('val','');
		$(".ftab").trigger('click');
		resetFields();
		refreshgrid();
		$('#contactcreationformadd').hide();
		$('#contactcreationview').fadeIn(1000);		
		$("#dynamicdddataview").select2('val',dynview);
		//For Keyboard Shortcut Variables
		viewgridview = 1;
		addformupdate = 0;
		addformview = 0;
	});	
	$("#gcontactsyncoverlayclose").click(function(){
		$("#gcontactsyncoverlay").hide();
		$("#gcontactsyncoverlayform").trigger('reset');	
	});
	$("#gcontactsyncsubmit").click(function(){
		var chk = '';
		var chk = $('input[name=sync-method]:checked', '#gcontactsyncoverlayform').val();
		if (typeof chk == 'undefined'){ 
			alertpopup('Choose Sync Option'); return;
		}
		else {
			$.ajax({
				url:base_url+"Contact/googlecontactssync?option="+chk,
				async :false,
				cache:false,
				success: function(ndata){
					var nmsg =  $.trim(ndata);
					if (nmsg == 'TRUE') {
						$("#gcontactsyncoverlay").hide();
						alertpopup("Synchronization Done");
					}
				}
			});			
		}
	});
	//Calculate age from DOB
	$('#dateofbirth').change(function(){
		var user_date = $('#dateofbirth').val();
		var dateAr = user_date.split('-');
		var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);
		var birthdate = new Date(newDate);
		var cur = new Date();
		var diff = cur-birthdate;
		var age = Math.floor(diff/31536000000);
		$('#age').val(age);
		Materialize.updateTextFields();
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,contactcreategrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			contactcreategrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	{//inner-form-with-grid
		$("#contactsplaingridadd1").click(function(){
			formheight();
			$("#contactsplaoverlay").removeClass("closed");
			$("#contactsplaoverlay").addClass("effectbox");
			clearform('gridformclear');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#contactsplacancelbutton").click(function(){
			$("#contactsplaoverlay").removeClass("effectbox");
			$("#contactsplaoverlay").addClass("closed");
			$('#contactsplaupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#contactsplaaddbutton').show();//display the ADD button(inner-productgrid)*/	
		});
	}
	{// Redirected form notification overlay and widget and audit log
		var rdatarowid = sessionStorage.getItem("datarowid");		
		if(rdatarowid != '' && rdatarowid != null){
			 setTimeout(function() {
				 contactseditdatafetchfun(rdatarowid);
				showhideiconsfun('summryclick','contactcreationformadd');
				sessionStorage.removeItem("datarowid");
				Materialize.updateTextFields();
			},50);
		}
	}
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid View
	function contactcreategrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $("#contactcreategrid").width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.contactsheadercolsort').hasClass('datasort') ? $('.contactsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.contactsheadercolsort').hasClass('datasort') ? $('.contactsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.contactsheadercolsort').hasClass('datasort') ? $('.contactsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=contact&primaryid=contactid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#contactcreategrid').empty();
				$('#contactcreategrid').append(data.content);
				$('#contactcreategridfooter').empty();
				$('#contactcreategridfooter').append(data.footer);
				//data row select event
				datarowselectevt('contactcreategrid');
				//column resize
				columnresize();
				{//sorting
					$('.contactsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.contactsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#contactspgnumcnt li .page-text .active').data('rowcount');
						//Start
						var sortcol = $('.contactsheadercolsort').hasClass('datasort') ? $('.contactsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.contactsheadercolsort').hasClass('datasort') ? $('.contactsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						//End
						var sortpos = $('#contactcreategrid .gridcontent').scrollLeft();
						contactcreategrid(page,rowcount);
						$('#contactcreategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('contactsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						contactcreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#contactspgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						contactcreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var contactideditvalue = sessionStorage.getItem("contactidforedit");
					if(contactideditvalue != null) {		
						setTimeout(function(){
						contactseditdatafetchfun(contactideditvalue);
						sessionStorage.removeItem("contactidforedit");},100);
					}
				}
				//Material select
				$('.pagedropdown').material_select();
			},
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#contactspgnum li.active').data('pagenum');
		var rowcount = $('ul#contactspgnumcnt li .page-text .active').data('rowcount');
		contactcreategrid(page,rowcount);
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function() {
			addslideup('contactcreationview','contactcreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','contactcreationformadd');
			$("#imagenamedivhid").removeClass('hidedisplay');
			$(".documentslogodownloadclsbtn").show();
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			if(softwareindustryid == 4){
				contactplanaddgrid();
			}
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			$('#imagenameattachdisplay').empty();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			$("#primarydataid").val('');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
		});
		$("#editicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				if(softwareindustryid == 4){
					contactplanaddgrid();
					$('#contactsplaaddbutton').removeClass('hidedisplay');
				}
				froalaset(froalaarray);
				contactseditdatafetchfun(datarowid);
				showhideiconsfun('editclick','contactcreationformadd');
				$("#imagenamedivhid").removeClass('hidedisplay');
				$(".documentslogodownloadclsbtn").show();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#contactcreategrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			contactrecorddelete(datarowid);
		});
	}
}

{// New data add submit function
	function contactnewdataaddfun()	{
		if(softwareindustryid == 4){
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			var noofrows=0;
			var addgriddata='';
			if(deviceinfo != 'phone'){
				for(var j=0;j<datalength;j++) {
					if(gridnames[j]) {
						if(j!=0) {
							addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
							noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
						} else {
							addgriddata = getgridrowsdata(gridnames[j]);
							noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
						}
					}
				}
			}
			else{
				for(var j=0;j<datalength;j++) {
					if(gridnames[j]) {
						if(j!=0) {
							addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
							noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
						} else {
							addgriddata = getgridrowsdata(gridnames[j]);
							noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
						}
					}
				}
			}
			var sendformadddata = JSON.stringify(addgriddata);
			var tandcdata = 0;
			var elementsname = $('#elementsname').val();
			var elementstable = $('#elementstable').val();
			var elementscolmn = $('#elementscolmn').val();
			var elementspartabname = $('#elementspartabname').val();
			var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
			var resctable = $('#resctable').val();	
			var editorname = $('#editornameinfo').val();
			var editordata = froalaeditoradd(editorname);
			var formdata = $("#dataaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Contact/newdatacreate",
				data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"elementsname="+elementsname+amp+"elementstable="+elementstable+amp+"elementscolmn="+elementscolmn+amp+"elementspartabname="+elementspartabname+amp+"resctable="+resctable+amp+"griddatapartabnameinfo="+griddatapartabnameinfo+amp+"tandcdata="+tandcdata,
				type: "POST",
				cache:false,
				success: function(id) {
					$("#processoverlay").hide();
					CONVERTID = id;
					$("#coninvoverlay").fadeIn();					
				},
			});
		}else{
			var editorname = $('#editornameinfo').val();
			var editordata = froalaeditoradd(editorname);
			var formdata = $("#dataaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Contact/newdatacreate",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						var dynview = $("#dynamicdddataview").val();
						$('#contacttag').val('');
						$(".ftab").trigger('click');
						resetFields();
						refreshgrid();
						$('#contactcreationformadd').hide();
						$('#contactcreationview').fadeIn(1000);
						alertpopup(savealert);
						$("#dynamicdddataview").val(dynview);
						//For Keyboard Shortcut Variables
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();				}
				},
			});
		}
	}
}
{// Old information show in form
	function contacteditformdatainformationshow(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Contact/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#contactcreationformadd').hide();
					$('#contactcreationview').fadeIn(1000);
					refreshgrid();
					$("#processoverlay").hide();
				} else {
					showhideiconsfun('editclick','contactcreationformadd');
					addslideup('contactcreationview','contactcreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					primaryaddvalfetch(datarowid);
					var sid = $("#sourceid").val();
					if(sid == "null" ) {
						$("#sourceid").select2('val','');
					}
					if(softwareindustryid == 4){
						contactgriddetail(datarowid);
					}
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					editordatafetch(froalaarray,data);
					$("#processoverlay").hide();
					
				}
			}
		});
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid) {
		$.ajax({
			url:base_url+"Contact/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
				var textboxname = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
				var dropdowns = ['2','primaryaddresstype','secondaryaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns);
			}
		});
	}
}
{// Update old information
	function contactdataupdateinformationfun() { 
		if(softwareindustryid == 4){
			var amp = '&';
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			var noofrows=0;
			var addgriddata='';
			if(deviceinfo != 'phone'){
				for(var j=0;j<datalength;j++) {
					if(gridnames[j]) {
						if(j!=0) {
							addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
							noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
						} else {
							addgriddata = getgridrowsdata(gridnames[j]);
							noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
						}
					}
				}
			}else{
				for(var j=0;j<datalength;j++) {
					if(gridnames[j]) {
						if(j!=0) {
							addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
							noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
						} else {
							addgriddata = getgridrowsdata(gridnames[j]);
							noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
						}
					}
				}
			}		
			var sendformadddata = JSON.stringify(addgriddata);	
			//editor data
			var editorname = $('#editornameinfo').val();
			var editordata = froalaeditoradd(editorname);
			var formdata = $("#dataaddform").serialize();
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Contact/datainformationupdate",
				data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows,
				type: "POST",
				cache:false,
				success: function(id) {
						$("#processoverlay").hide();
						CONVERTID = id;
						$("#coninvoverlay").fadeIn();
				},
			});
		}else{
			var editorname = $('#editornameinfo').val();
			var editordata = froalaeditoradd(editorname);
			var formdata = $("#dataaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Contact/datainformationupdate",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$(".ftab").trigger('click');
						$('#contacttag').select2('val','');
						$('#contactcreationformadd').hide();
						$('#contactcreationview').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						//For Keyboard Shortcut Variables
						addformupdate = 0;
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();
					}
				},
			});
		}
		
	}
}
{// Delete function
	function contactrecorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Contact/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Edit Function
	function contactseditdatafetchfun(datarowid) {		
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();
		$('#imagenameattachdisplay').empty();
		if(softwareindustryid == 4){
			clearformgriddata();
		}
		resetFields();
		contacteditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function contactsclonedatafetchfun(datarowid) {
		if(softwareindustryid == 4){
			clearformgriddata();
		}
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide();
		$('#imagenameattachdisplay').empty();
		contacteditformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{
	if(softwareindustryid == 4){
		function contactplanaddgrid() {
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			for(var j=0;j<datalength;j++) {
				if(gridnames[j] == 'contactsplaaddgrid1') {
					var wwidth = $("#contactsplaaddgrid1").width();
					var wheight = $("#contactsplaaddgrid1").height();
					$.ajax({
						url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=206&moduleid=82&width="+wwidth+"&height="+wheight+"&modulename=contactplan",
						dataType:'json',
						async:false,
						cache:false,
						success:function(data) {
							$("#contactsplaaddgrid1").empty();
							$("#contactsplaaddgrid1").append(data.content);
							$("#contactsplaaddgrid1footer").empty();
							$("#contactsplaaddgrid1footer").append(data.footer);
							/* data row select event */
							datarowselectevt();
							/* column resize */
							columnresize('contactsplaaddgrid1');
							var hideprodgridcol = ['contactdetailid'];
							gridfieldhide('contactsplaaddgrid1',hideprodgridcol);
						},
					});
				}
			}
		}	
		{// Clear grid data
			function clearformgriddata() {
				var gridname = $('#gridnameinfo').val();
				var gridnames = gridname.split(',');
				var datalength = gridnames.length;
				for(var j=0;j<datalength;j++) {
					cleargriddata(gridnames[j]);
				}
			}
		}
		function gridformtogridvalidatefunction(gridnamevalue,method) {
			//Function call for validate
			$("#"+gridnamevalue+"validation").validationEngine({
				onSuccess: function() {
					var i = griddataid;
					var gridname = gridynamicname;
					var coldatas = $('#gridcolnames'+i+'').val();
					var columndata = coldatas.split(',');
					var coluiatas = $('#gridcoluitype'+i+'').val();
					var columnuidata = coluiatas.split(',');
					var datalength = columndata.length;
					/*Form to Grid*/
					if(METHOD == 'ADD' || METHOD == '') {
						formtogriddata(gridname,METHOD,'last','');
					} else if(METHOD == 'UPDATE') {
						formtogriddata(gridname,METHOD,'',UPDATEID);
					}
					/* Hide columns */
					var hideprodgridcol = ['contactdetailid'];
					gridfieldhide('contactsplaaddgrid1',hideprodgridcol);
					/* Data row select event */
					datarowselectevt();
					$('#contactsplaupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
					$('#contactsplaaddbutton').show();//display the ADD button(inner-productgrid)*/	
					$("#contactsplacancelbutton").trigger('click');
					clearform('gridformclear');
					griddataid = 0;
					gridynamicname = '';					
					METHOD = 'ADD'; //reset
				},
				onFailure: function() {
					var dropdownid =['1','membershipplans'];
					dropdownfailureerror(dropdownid);
				}
			});
		}
		function contactgriddetail(pid) {
			var gridname = $('#gridnameinfo').val();
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			for(var j=0;j<datalength;j++) {
				if(gridnames[j] == 'contactsplaaddgrid1') {
					if(pid!='') {
						$.ajax({
							url:base_url+'Contact/contactgriddetail?primarydataid='+pid,
							dataType:'json',
							async:false,
							cache:false,
							success: function(data) {
								if((data.fail) == 'FAILED') {
								} else {
									loadinlinegriddata('contactsplaaddgrid1',data.rows,'json');
									/* data row select event */
									datarowselectevt();
									/* column resize */
									columnresize('contactsplaaddgrid1');
									/* Hide columns */
									var hideprodgridcol = ['contactdetailid'];
									gridfieldhide('contactsplaaddgrid1',hideprodgridcol);
								}
							},
						});
					}
				}
				
			}
			
		}
	}	
}
{// Address get and Set values	
	//setaddress
	function setaddress(id) {
		var value=$('#'+id+'').val();
		if(id == 'primaryaddresstype') {
			var type='Billing';
			var ntype = 'primary';
		} else {
			var type='Shipping';
			var ntype = 'secondary';
		}
		//account address
		if(value == 2) {
			var accountid=$('#accountid').val();
			getaddress(accountid,'account',type,ntype);
		}
		//swap the billing to shipping
		else if(value == 8) {
			$('#primaryaddress').val($('#secondaryaddress').val());
			$('#primarypincode').val($('#secondarypincode').val());
			$('#primarycity').val($('#secondarycity').val());
			$('#primarystate').val($('#secondarystate').val());
			$('#primarycountry').val($('#secondarycountry').val());
		}
		//swap the shipping to billing
		else if(value == 7) {
			$('#secondaryaddress').val($('#primaryaddress').val());
			if(softwareindustryid == 4){
				$('#secondaryareaname').val($('#primaryareaname').val());
			}
			$('#secondarypincode').val($('#primarypincode').val());
			$('#secondarycity').val($('#primarycity').val());
			$('#secondarystate').val($('#primarystate').val());
			$('#secondarycountry').val($('#primarycountry').val());
		}
	}
	function getaddress(id,table,type,ntype) {
		if(id != null && id != '' && table != null && table !='') {
			var append=type.toLowerCase();
			$.ajax({
				url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.result == true) {
						var datanames = ['5','address','pincode','city','state','country'];
						var textboxname = ['5',''+ntype+'address',''+ntype+'pincode',''+ntype+'city',''+ntype+'state',''+ntype+'country'];
						textboxsetvalue(textboxname,datanames,data,[]);
					}
				},
			});
		}
	}
}
{// Validate the module email and do not call
	function validateemailanddonotmail(id,table,field,donotmail) {
		var status='';
		$.ajax({
			url: base_url + "Base/checkvalidemailanddonotmail?id="+id+"&table="+table+"&field="+field+"&donotmail="+donotmail,
			async:false,
			cache:false,
			success: function(msg) {
				status =msg;
			},
		});
		return status;
	}
	//validate do not call
	function validatecall(id,table,field) {
		var status='';
		$.ajax({
			url: base_url + "Base/validatecall?id="+id+"&table="+table+"&field="+field,
			async:false,
			cache:false,
			success: function(msg)  {
				status =msg;
			},
		});
		return status;
	}
}