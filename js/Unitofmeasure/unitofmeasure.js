$(document).ready(function() {
	{//Hidedisplay Buttons
		$('#unitofmeasuredataupdatesubbtn').hide();
		$('#unitofmeasuresavebutton').show();
	}
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		unitofmeasureaddgrid();
		firstfieldfocus();
		unitofmeasurecrudactionenable()
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function() {
			unitofmeasureaddgrid();
		});
	}
	{//validation for Unit Of Mesure Add  
		$('#unitofmeasuresavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#unitofmeasureformaddwizard").validationEngine('validate');			
			}
		});
		jQuery("#unitofmeasureformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#unitofmeasuresavebutton').attr('disabled','disabled'); 
				uomaddformdata();
			},
			onFailure: function() {
				var dropdownid =['1','uomtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//validation for Unit Of Mesure Edit  
		$( window ).resize(function() {
			innergridresizeheightset('unitofmeasureaddgrid');
			sectionpanelheight('unitofmeasuresectionoverlay');
		});
		//update company information
		$('#unitofmeasuredataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#unitofmeasureformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}	
		});
		jQuery("#unitofmeasureformeditwizard").validationEngine({
			onSuccess: function() {
				uomupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','uomtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}
			if(name == 'wholenumber'){
				if ($(this).is(':checked')) {
					$('#uomprecision').val(0);
					$("#uomprecision").prop("readonly",true);
					Materialize.updateTextFields();
				} else {
					$("#uomprecision").prop("readonly",false);
				}
			}
		});
	}
	{
		$(".addsectionclose").click(function(){
			$("#unitofmeasuresectionoverlay").removeClass("effectbox");
			$("#unitofmeasuresectionoverlay").addClass("closed");
		});
	}
	{//filter work
		//for toggle
		$('#unitofmeasureviewtoggle').click(function() {
			if ($(".unitofmeasurefilterslide").is(":visible")) {
				$('div.unitofmeasurefilterslide').addClass("filterview-moveleft");
			} else {
				$('div.unitofmeasurefilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#unitofmeasureclosefiltertoggle').click(function(){
			$('div.unitofmeasurefilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#unitofmeasurefilterddcondvaluedivhid").hide();
		$("#unitofmeasurefilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#unitofmeasurefiltercondvalue").focusout(function(){
				var value = $("#unitofmeasurefiltercondvalue").val();
				$("#unitofmeasurefinalfiltercondvalue").val(value);
				$("#unitofmeasurefilterfinalviewconid").val(value);
			});
			$("#unitofmeasureddcondvalue").change(function(){
				var value = $("#unitofmeasurefilterddcondvalue").val();
				if( $('#s2id_uomconversionfilterddcondvalue').hasClass('select2-container-multi') ) {
					unitofmeasuremainfiltervalue=[];
					$('#unitofmeasurefilterddcondvalue option:selected').each(function(){
					    var $uomvalue =$(this).attr('data-ddid');
					    unitofmeasuremainfiltervalue.push($uomvalue);
						$("#unitofmeasurefinalfiltercondvalue").val(unitofmeasuremainfiltervalue);
					});
					$("#unitofmeasurefilterfinalviewconid").val(value);
				} else {
					$("#unitofmeasurefinalfiltercondvalue").val(value);
					$("#unitofmeasurefilterfinalviewconid").val(value);
				}
			});
		}
		unitofmeasurefiltername = [];
		$("#unitofmeasurefilteraddcondsubbtn").click(function() {
			$("#unitofmeasurefilterformconditionvalidation").validationEngine("validate");	
		});
		$("#unitofmeasurefilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(unitofmeasureaddgrid,'unitofmeasure');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
{// Documents Add Grid
	function unitofmeasureaddgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $("#unitofmeasureaddgrid").width();
		var wheight = $("#unitofmeasureaddgrid").height();
		//col sort
		var sortcol = $("#unitofmeasuresortcolumn").val();
		var sortord = $("#unitofmeasuresortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.unitofmeasureheadercolsort').hasClass('datasort') ? $('.unitofmeasureheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.unitofmeasureheadercolsort').hasClass('datasort') ? $('.unitofmeasureheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.unitofmeasureheadercolsort').hasClass('datasort') ? $('.unitofmeasureheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#unitofmeasureviewfieldids').val();
		if(userviewid != '') {
			userviewid= '54';
		}
		var filterid = $("#unitofmeasurefilterid").val();
		var conditionname = $("#unitofmeasureconditionname").val();
		var filtervalue = $("#unitofmeasurefiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=uom&primaryid=uomid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#unitofmeasureaddgrid').empty();
				$('#unitofmeasureaddgrid').append(data.content);
				$('#unitofmeasureaddgridfooter').empty();
				$('#unitofmeasureaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('unitofmeasureaddgrid');
				{//sorting
					$('.unitofmeasureheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.unitofmeasureheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#unitofmeasurepgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.unitofmeasureheadercolsort').hasClass('datasort') ? $('.unitofmeasureheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.unitofmeasureheadercolsort').hasClass('datasort') ? $('.unitofmeasureheadercolsort.datasort').data('sortorder') : '';
						$("#unitofmeasuresortorder").val(sortord);
						$("#unitofmeasuresortcolumn").val(sortcol);
						var sortpos = $('#unitofmeasureaddgrid .gridcontent').scrollLeft();
						unitofmeasureaddgrid(page,rowcount);
						$('#unitofmeasureaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('unitofmeasureheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						unitofmeasureaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#unitofmeasurepgrowcount').change(function(){ //unitofmeasureaddgridfooter
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						unitofmeasureaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$(".gridcontent").click(function(){
					var datarowid = $('#unitofmeasureaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#unitofmeasurepgrowcount').material_select();
			},
		});
	}
}
function unitofmeasurecrudactionenable() {
	{//add icon click
		$('#unitofmeasureaddicon').click(function(e){
			sectionpanelheight('unitofmeasuresectionoverlay');
			$("#unitofmeasuresectionoverlay").removeClass("closed");
			$("#unitofmeasuresectionoverlay").addClass("effectbox");
			$("#uomprecision").prop("readonly",false);
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#unitofmeasuredataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#unitofmeasuresavebutton').show();
		});
	}
	$("#unitofmeasureediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#unitofmeasureaddgrid div.gridcontent div.active').attr('id');
		if (datarowid){
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			uomgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
			//for Keyboard	
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#unitofmeasuredeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#unitofmeasureaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');				
			combainedmoduledeletealert('uomdeleteyes');
			$("#uomdeleteyes").click(function(){
				var datarowid = $('#unitofmeasureaddgrid div.gridcontent div.active').attr('id'); 
				uomrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}	
	});	
}
{//refresh grid
	function unitofmeasurerefreshgrid() {
		var page = $('ul#unitofmeasurepgnum li.active').data('pagenum');
		var rowcount = $('ul#unitofmeasurepgnumcnt li .page-text .active').data('rowcount');
		unitofmeasureaddgrid(page,rowcount);
	}
}
{// New data add submit function
	function uomaddformdata() {
		var formdata = $("#unitofmeasureaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax( {
			url: base_url + "Unitofmeasure/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE')	{
					$('.singlecheckid').click();
					$(".addsectionclose").trigger("click");
					unitofmeasurerefreshgrid();
					$("#unitofmeasuresavebutton").attr('disabled',false); 
					$("#uomprecision").prop("readonly",false);
					alertpopup(savealert);				
				}
			},
		});
	}
}
{// Old information show in form
	function uomgetformdata(datarowid) {
		var unitofmeasureelementsname = $('#unitofmeasureelementsname').val();
		var unitofmeasureelementstable = $('#unitofmeasureelementstable').val();
		var unitofmeasureelementscolmn = $('#unitofmeasureelementscolmn').val();
		var unitofmeasureelementpartable = $('#unitofmeasureelementspartabname').val();
		$.ajax( {
			url:base_url+"Unitofmeasure/fetchformdataeditdetails?unitofmeasureprimarydataid="+datarowid+"&unitofmeasureelementsname="+unitofmeasureelementsname+"&unitofmeasureelementstable="+unitofmeasureelementstable+"&unitofmeasureelementscolmn="+unitofmeasureelementscolmn+"&unitofmeasureelementpartable="+unitofmeasureelementpartable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					unitofmeasurerefreshgrid();
					//For Touch
					smsmasterfortouch = 0;
				} else {
					sectionpanelheight('unitofmeasuresectionoverlay');
					$("#unitofmeasuresectionoverlay").removeClass("closed");
					$("#unitofmeasuresectionoverlay").addClass("effectbox");
					var txtboxname = unitofmeasureelementsname + ',unitofmeasureprimarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(','); 
					var dataname = unitofmeasureelementsname + ',primarydataid';
					var datasname = {};
					datasname = dataname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,datasname,data,dropdowns);
					if(data['wholenumber'] == 'Yes') {
						$("#uomprecision").prop("readonly",true);
					} else {
						$("#uomprecision").prop("readonly",false);
					}
				}
			}
		});
		$('#unitofmeasuredataupdatesubbtn').show().removeClass('hidedisplayfwg');
		$('#unitofmeasuresavebutton').hide();
	}
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#unitofmeasuresectionoverlay").removeClass("effectbox");
		$("#unitofmeasuresectionoverlay").addClass("closed");
		$("#wholenumber").val('No');
		$("#uomsetdefault").val('No');
	});
}
{// Update old information
	function uomupdateformdata() {
		var formdata = $("#unitofmeasureaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Unitofmeasure/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$('#unitofmeasuredataupdatesubbtn').hide();
					$('#unitofmeasuresavebutton').show();
					$('.singlecheckid').click();
					$(".addsectionclose").trigger("click");
					resetFields();
					unitofmeasurerefreshgrid();
					alertpopup(savealert);
					//for touch
					masterfortouch = 0;
				}           
			},
		});	
	}
}
{// Delete Information
	function uomrecorddelete(datarowid) {
		var unitofmeasureelementstable = $('#unitofmeasureelementstable').val();
		var elementspartable = $('#unitofmeasureelementspartabname').val();
		$.ajax({
			url: base_url + "Unitofmeasure/deleteinformationdata?unitofmeasureprimarydataid="+datarowid+"&unitofmeasureelementstable="+unitofmeasureelementstable+"&unitofmeasureparenttable="+elementspartable,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					unitofmeasurerefreshgrid();
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied")  {
					unitofmeasurerefreshgrid();
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup('Permission denied');
				} else {
					unitofmeasurerefreshgrid();
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}