'use strict';
var initialize=function(){
	$("#error").html('');
	var ezetapConfigData={
			
				"username":"7200070823",
				"demoAppKey":"d43c4a6b-147d-4a8a-acfa-eac5d237be9f",
				"appMode":"DEMO",
				"currencyCode":"INR",
				"httpPort":"8081"
			
	};
	$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0px" opacity: 0.2;/>' }); 
	
	initializeClient(ezetapConfigData)
	.then(function(response){
		$(document).ajaxStop($.unblockUI);
		$("#error").html('');
		$("#notification").html('');
		$("#initialize").hide();
		localStorage.setItem("ezetap_username",ezetapConfigData.username);
		/*showMainOption();*/
	})
	.fail(function(response){
		$(document).ajaxStop($.unblockUI);
		//$("#error").show();
		$('#ezetapresponseoverlay').fadeIn();
		$("#ezemsg").html(response.error.errorText);
		//$("#error").html(response.error.errorText);
		$("#notification").html('');
	})

	EzeAPI.handleNotification("1337",function(notificationMessage){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42"/><br>'+notificationMessage });
	},serverShutDown)
};

function takeCardPayment(){
	EzeAPI.handleNotification("1337",function(notificationMessage){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42"/><br>'+notificationMessage });
		$("#notification").html(notificationMessage);
	},serverShutDown)
	var options={}
	options.customer={};
	options.customer.mobileNo='';
	//options.customer.mobileNo=$('#ezeaccmobileno').val();
	options.customer.email=$('#ezeaccemail').val();
	
	options.references={}
	options.references.primaryRef="";
	options.references.ref2="";
	options.references.ref3="";
	options.amountCashback="";
	console.log('inside takecard payment')
		payCard($('#paymentamount').val(),'SALE',options)
		.then(function(response){
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val(JSON.stringify(response));	
			$("#paymentadd").attr('disabled',false);
			//alert(response.result.txn.receiptDate.toString('dd/mm/yyyy'));
			//$("#paymentreferencedate").val(response.result.txn.receiptDate);
			$(document).ajaxStop($.unblockUI); 
			displayResult(response);
		})
		.fail(function(response){
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val('');
			$("#paymentadd").attr("disabled", true);
			$(document).ajaxStop($.unblockUI);
			displayResult(response);
		});
}

function takeCashPayment(){
	EzeAPI.handleNotification("1337",function(notificationMessage){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42"/><br>'+notificationMessage });
	},serverShutDown)
	var options={}
	options.customer={};
	options.customer.mobileNo='';
	options.customer.email=$('#ezeaccemail').val();
	
	options.references={}
	options.references.primaryRef="";
	options.references.ref2="";
	options.references.ref3="";
	
	$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
		payCash($('#paymentamount').val(),options)
		.then(function(response){ 
			//alert(JSON.stringify(response));
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val(JSON.stringify(response));			
			$("#paymentadd").attr('disabled',false);
			$(document).ajaxStop($.unblockUI); 
			displayResult(response);
		})
		.fail(function(response){ 
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val('');
			$("#paymentadd").attr("disabled", true);
			$(document).ajaxStop($.unblockUI);
			displayResult(response);
		});
}

function takeChequePayment(){
	EzeAPI.handleNotification("1337",function(notificationMessage){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42"/><br>'+notificationMessage });
		$("#notification").html(notificationMessage);
	},serverShutDown)
	var options={}
	options.customer={};
	//options.customer.mobileNo=$('#ezeaccmobileno').val();
	options.customer.mobileNo='';
	options.customer.email=$('#ezeaccemail').val();
	
	options.references={}
	options.references.primaryRef="";
	options.references.ref2="";
	options.references.ref3="";
	
	var cheque={};
	cheque.chequeNumber="1234567865566532";
	cheque.bankCode="102";
	cheque.chequeDate="20-02-2017";
	
	$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
		payCheque($('#paymentamount').val(),cheque,options)
		.then(function(response){
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val(JSON.stringify(response));	
			$("#paymentadd").attr('disabled',false);
			$(document).ajaxStop($.unblockUI); 
			displayResult(response);
		})
		.fail(function(response){
			$("#ezetappaymentresult").val('');
			$("#ezetappaymentresult").val(response['status']);
			$("#ezetappaymentresponse").val('');	
			$("#paymentadd").attr("disabled", true);
			$(document).ajaxStop($.unblockUI);
			displayResult(response);
		});
	//}
}

function getTransactionClient(){
	EzeAPI.handleNotification("1337",function(notificationMessage){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42"/><br>'+notificationMessage });
		$("#notification").html(notificationMessage);
	},serverShutDown)
	$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
	getTransaction($('#txnId').val())
		.then(function(response){
			$(document).ajaxStop($.unblockUI); 
			displayResult(response);
		})
		.fail(function(response){
			$(document).ajaxStop($.unblockUI);
			displayResult(response);
		});
	//}
}
	
	/*function voidTxn(txnId){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
		voidTransactionClient(txnId)
		.then(function(){
			$(document).ajaxStop($.unblockUI);
			$("#result").html('Transaction voided');
			var html='<input type="button" value="Back" align="middle" style="margin: auto; "onclick="refresh()">';
			$("#backbutton").show();
			$("#backbutton").html(html);
		}).fail(function(response){
			$(document).ajaxStop($.unblockUI);
			$("#error").html(response.error.errorText);
			var html='<input type="button" value="Back" align="middle" style="margin: auto; "onclick="refresh()">';
			$("#backbutton").show();
			$("#backbutton").html(html);
		})
		
	}*/
	function logout(){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
		logoutClient()
		.then(function(){
			$(document).ajaxStop($.unblockUI);
			$('div').hide();
			$('#main').show()
			$('initialize').show();
			
		})
		.fail(function(response){
			$(document).ajaxStop($.unblockUI);
			$("#error").show();
			if(response!==null){
				$("#error").html(response.error.errorText);
			}
			
		})
	}
	
	function displayResult(response){ 
		$("#notification").html('')
		function populateTable(json){
			
			if(json.result!=null){
				
				var table="<table align='center' border='1'>";
				var keys=Object.keys(json.result)
				for(var i in keys){
					//console.log(keys)
					table+='<tr><th>'+keys[i]+'</th></tr>';
					var key=Object.keys(json.result[keys[i]]);
					//console.log(key)
					for(var j in key){
						var obj=json.result[keys[i]];
						table+="<tr>"
							table+="<td>"
							table+=key[j]
							table+="</td>"
							table+="<td>"
							table+=obj[key[j]]
							table+="</td>"
							table+="</tr>"
					}
					
				}	
				table+="</table>";
			}else{
				table=json.error.errorText;
			}
			return table;

		}
		if(response.result!=null){ 
			$("#result").html('')
			$("#error").html('')
			//$("#result").append(populateTable(response));
			$('#ezetapresponseoverlay').fadeIn();
			$("#ezemsg").html(populateTable(response));
			/*$("div").hide()
			$('#main').show()
			$("#result").show()
			$("#result").html('')
			$("#result").append(populateTable(response));
			var r=response.result.txn.txnId;
			var html='<input type="button" value="Home" align="middle" style="margin: auto; " onclick="refresh()">';
				if(response.result.txn.paymentMode==='CARD'){
					html+='<input type="button" value="Void Transaction" align="middle" style="margin: auto;" onclick="voidTxn(\''+r+'\' )">'
				}
					
			$("#backbutton").show();
			$("#backbutton").html(html);*/
		}else{
			$("#result").html('')
			$("#error").html('')
			//$("#error").append(populateTable(response));
			$('#ezetapresponseoverlay').fadeIn();
			$("#ezemsg").html(populateTable(response));
			/*$("div").hide();
			$('#main').show()
			$("#error").show();
			$("#error").html('')
			$("#error").append(populateTable(response));
			var html='<input type="button" value="Home" align="middle" style="margin: auto; "onclick="refresh()">';
			$("#backbutton").show();
			$("#backbutton").html(html);*/
		}
	}
	
	function refresh(){
		$('div').hide()
		$('#main').show()
		$("#mainOption").show()
	}
	
	/*function showMainOption(){
		var html='<input type="button" value="Card" align="middle" style="margin: auto; " onclick="showCard()">'
			html+='<input type="button" value="Cash" align="middle" style="margin: auto; " onclick="showCash()">'
				html+='<input type="button" value="Cheque" align="middle" style="margin: auto; " onclick="showCheque()">'
					html+='<input type="button" value="Get Transaction" align="middle" style="margin: auto; " onclick="showTransactionDetail()">'
						html+='<input type="button" value="Logout" align="middle" style="margin: auto; " onclick="logout()">'
		$("#mainOption").show();
		$("#mainOption").html(html);
	}*/
	
	/*function showCard(){
		$('div').hide();
		$('#main').show()
		$("#card").show();
	}
	
	function showCash(){
		$('div').hide();
		$('#main').show()
		$("#cash").show();
	}
	
	function showCheque(){
		$('div').hide();
		$('#main').show()
		$("#cheque").show();
	}*/
	
	/*function showTransactionDetail(){
		$('div').hide();
		$('#main').show()
		$("#getTransaction").show();
	}*/
	
	/*function main(){
		$('div').hide()
		$('#main').show()
		var username=localStorage.getItem("ezetap_username");
		if(username!=null && username.length>0){
			/*showMainOption();
		}else{
			$('#initialize').show();
		}
	}*/
	
	function logout(){
		$.blockUI({ message: '<img src="././img/ezetap/spinner.gif" height="42" width="42" border="0"/>' }); 
		logoutClient()
		.then(function(){
			localStorage.removeItem("ezetap_username");
			$(document).ajaxStop($.unblockUI);
			$('div').hide();
			$('#main').show();
			$('#initialize').show();
		})
		.fail(function(){
			$(document).ajaxStop($.unblockUI);
			localStorage.removeItem("ezetap_username")
			$('div').hide();
			$('#main').show();
			$('#initialize').show();
		})
	}
	
	function serverShutDown(){
		$(document).ajaxStop($.unblockUI);
		localStorage.removeItem("ezetap_username");
		$('div').hide();
		$("#main").show();
		$("#error").show();
		$("#error").html('ERROR!!! Ezetap server shutdown. Restart the server and reconnect the device again.');
	}
	
