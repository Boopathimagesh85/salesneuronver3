$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var Emrdocumentspatientid =  ca[i].split('=');
       if(Emrdocumentspatientid[0] == 'patientid'){
    	   $("#nameid").val(Emrdocumentspatientid[1]);
       }
    }
	$("#tab7").click(function(){
		Emrdocumentsaddgrid();
	});
	{
		var addcloseEmrdocumentscreation =['closeaddform','Emrdocumentscreationview','Emrdocumentscreationformadd'];
		addclose(addcloseEmrdocumentscreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('emrdocumentssectionoverlay');
		});
	}
	{$("#emrdocumentsediticon").hide();
		$('#emrdocumentsaddicon').click(function(){
			resetFields();
			sectionpanelheight('emrdocumentssectionoverlay');
			$('#emrdocumentssectionoverlay').removeClass('closed');
			$('#emrdocumentssectionoverlay').addClass('effectbox');
			$('#emrdocumentsdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#emrdocumentssavebutton').show();
			showhideiconsfun('addclick','Emrdocumentscreationformadd');
			firstfieldfocus();
			var elementname = $('#emrdocumentselementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('emrdocumentsanfieldnameinfo','emrdocumentsanfieldnameidinfo','emrdocumentsanfieldnamemodinfo','emrdocumentsanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var dcontacid = $("#emrdynamicdddataview").val();
			$("#nameid").val(dcontacid);
			fname_5106 = [];
			fsize_5106 = [];
			ftype_5106 = [];
			fpath_5106 = [];
			upfrom_5106 = [];
			fortabtouch = 0;
		});
		$('#emrdocumentsediticon').click(function(){
			var datarowid = $('#emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('emrdocumentssectionoverlay');
				$('#emrdocumentssectionoverlay').removeClass('closed');
				$('#emrdocumentssectionoverlay').addClass('effectbox');
				addslideup('Emrdocumentscreationview','Emrdocumentscreationformadd');
				$('#emrdocumentsdataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#emrdocumentssavebutton').hide();
				Emrdocumentsdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Emrdocumentscreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentsdeleteicon').click(function(){
			var datarowid = $('#emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#emrdocumentsprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('emrdocumentsdeleteyes');
				$("#emrdocumentsdeleteyes").click(function(){
					var datarowid =$("#emrdocumentsprimarydataid").val();
					Emrdocumentsrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentsreloadicon').click(function(){
			Emrdocumentsrefreshgrid();
		});
		$('#emrdocumentsformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#emrdocumentsbasedeleteyes').click(function(){
			var datarowid = $('#emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			Emrdocumentsrecorddelete(datarowid);
		});
		$('#emrdocumentsdetailedviewicon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#emrdocumentssectionoverlay').removeClass('closed');
				$('#emrdocumentssectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Emrdocumentsdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','emrdocumentscreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentseditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Emrdocumentscreationformadd');
		});
		
		$('#emrdocumentsprinticon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentscloneicon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#groupsectionoverlay').removeClass('closed');
				$('#groupsectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Emrdocumentsclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#emrdocumentssmsicon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentsmailicon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Emrdocuments','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5007');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#emrdocumentsoutgoingcallicon').click(function() {
			var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#emrdocumentscustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#emrdocumentsimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#emrdocumentsdatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#emrdocumentsfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#emrdocumentssavebutton').click(function(){
			$('#emrdocumentsformaddwizard').validationEngine('validate');
		});
		$('#emrdocumentsformaddwizard').validationEngine({
			onSuccess: function() {
				$('#emrdocumentssavebutton').attr('disabled','disabled');
				Emrdocumentsnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#emrdocumentsdataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#emrdocumentsformeditwizard').validationEngine('validate');
		});
		$('#emrdocumentsformeditwizard').validationEngine({
			onSuccess: function() {
				$('#emrdocumentsdataupdatesubbtn').attr('disabled','disabled');
				Emrdocumentsdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".emrdocumentsfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#emrdocumentssectionoverlay").removeClass("effectbox");
			$("#emrdocumentssectionoverlay").addClass("closed");
		});
	}
	{
		//filter work
		//for toggle
		$('#emrdocumentsviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.emrdocumentsfilterview').addClass("hidedisplay");
				$('.emrdocumentsfullgridview').removeClass("large-9");
				$('.emrdocumentsfullgridview').addClass("large-12");
			}else{
				$('.emrdocumentsfullgridview').removeClass("large-12");
				$('.emrdocumentsfullgridview').addClass("large-9");
				$('.emrdocumentsfilterview').removeClass("hidedisplay");
			}			
		});
		$('#emrdocumentsclosefiltertoggle').click(function(){
			$('.emrdocumentsfilterview').addClass("hidedisplay");
			$('.emrdocumentsfullgridview').removeClass("large-9");
			$('.emrdocumentsfullgridview').addClass("large-12");
		});
		//filter
		$("#emrdocumentsfilterddcondvaluedivhid").hide();
		$("#emrdocumentsfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#emrdocumentsfiltercondvalue").focusout(function(){
				var value = $("#emrdocumentsfiltercondvalue").val();
				$("#emrdocumentsfinalfiltercondvalue").val(value);
			});
			$("#emrdocumentsfilterddcondvalue").change(function(){
				var value = $("#emrdocumentsfilterddcondvalue").val();
				$("#emrdocumentsfinalfiltercondvalue").val(value);
			});
		}
		emrdocumentsfiltername = [];
		$("#emrdocumentsfilteraddcondsubbtn").click(function() {
			$("#emrdocumentsfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#emrdocumentsfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#emrdocumentsfiltercolumn").val();
				var label =$("#emrdocumentsfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#emrdocumentsfiltercondition").val();
				var value = $("#emrdocumentsfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = emrdocumentsfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#emrdocumentsfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons emrdocumentsfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				emrdocumentsfiltername.push(data);
				$("#emrdocumentsconditionname").val(emrdocumentsfiltername);
				Emrdocumentsaddgrid();
				$(".emrdocumentsfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = emrdocumentsfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#emrdocumentsconditionname").val();
							var filternewcond = nvalue.split(',');
							emrdocumentsfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#emrdocumentsconditionname").val(emrdocumentsfiltername);
							Emrdocumentsaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#emrdocumentsfiltercolumn").select2('val','');
				$("#emrdocumentsfiltercondition").select2('val','');
				$("#emrdocumentsfilterddcondvalue").select2('val','');
				$("#emrdocumentsfiltercondvalue").val('');
				$("#emrdocumentsfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Emrdocumentsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#emrdocumentsaddgridwidth").width();
	var wheight = $("#emrdocumentsaddgridwidth").height();
	var sortcol = $('.Emrdocumentssheadercolsort').hasClass('datasort') ? $('.Emrdocumentssheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Emrdocumentssheadercolsort').hasClass('datasort') ? $('.Emrdocumentssheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Emrdocumentssheadercolsort').hasClass('datasort') ? $('.Emrdocumentssheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '137';
	}
	userviewid = userviewid!= '' ? '137' : userviewid;
	var viewfieldids = 64;
	var Emrdocumentscontactid = $('#nameid').val();
	if(Emrdocumentscontactid != ''){
		var Emrdocumentsfilterid = '302|Equalto|203,306|Equalto|'+Emrdocumentscontactid+'';
	} else {
		var Emrdocumentsfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=crmfileinfo&primaryid=crmfileinfoid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Emrdocumentsfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#emrdocumentsaddgrid').empty();
			$('#emrdocumentsaddgrid').append(data.content);
			$('.emrdocumentsgridfootercontainer').empty();
			$('.emrdocumentsgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('emrdocumentsaddgrid');
			innergridresizeheightset('Emrdocumentsaddgrid');
			{//sorting
				$('.Emrdocumentssheadercolsort').click(function(){
					$('.Emrdocumentssheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Emrdocumentsspgnumcnt li .page-text .active').data('rowcount');
					Emrdocumentsaddgrid(page,rowcount);
				});
				sortordertypereset('Emrdocumentssheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Emrdocumentsaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Emrdocumentsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
		},
	});
}
function Emrdocumentsrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Emrdocumentsspgnumcnt li .page-text .active').data('rowcount');
		Emrdocumentsaddgrid(page,rowcount);
}
function Emrdocumentsclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Emrdocumentsdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Emrdocumentsnewdataaddfun() {
	var amp = '&';
	var formdata = $('#emrdocumentsaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Emrdocuments/newdatacreate',
			data: 'datas=' + datainformation,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					Emrdocumentsrefreshgrid();
					resetFields();
					$('.emrdocumentsfwgsectionoverlayclose').trigger('click');
					$('#emrdocumentssavebutton').attr('disabled',false);
					$("#keywords").select2('val',' ');
					$('#attachmentattachdisplay').empty();
					fname_5106 = [];
					fsize_5106 = [];
					ftype_5106 = [];
					fpath_5106 = [];
					upfrom_5106 = [];
					alertpopup('Documents Inserted Successfully');
					$("#processoverlay").hide();
				}
			},
	})
}
function Emrdocumentsdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#emrdocumentselementsname').val();
	var elementstable = $('#emrdocumentselementstable').val();
	var elementscolmn = $('#emrdocumentselementscolmn').val();
	var elementpartable = $('#emrdocumentselementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Emrdocuments/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('.emrdocumentsfwgsectionoverlayclose').trigger('click');
				Emrdocumentsrefreshgrid();
				$('#emrdocumentsdataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('Emrdocumentscreationview','Emrdocumentscreationformadd');
				var txtboxname = elementsname + ',emrdocumentsprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dataname = elementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			}
		}
	});
}
function Emrdocumentsdataupdatefun() {
	var amp = '&';
	var formdata = $('#emrdocumentsaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Emrdocuments/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.emrdocumentsfwgsectionoverlayclose').trigger('click');
				Emrdocumentsrefreshgrid();
				$('#emrdocumentsdataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Emrdocumentsrecorddelete(datarowid) {
	var elementstable = $('#emrdocumentselementstable').val();
	var elementspartable = $('#emrdocumentselementspartabname').val();
	 $.ajax({
        url: base_url + "Emrdocuments/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	Emrdocumentsrefreshgrid();
            	$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted Succssfully');
            } else if (nmsg == "Denied") {
            	$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
