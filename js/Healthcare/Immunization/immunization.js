$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var immunizationpatientid =  ca[i].split('=');
       if(immunizationpatientid[0] == 'patientid'){
    	   	$("#immunizationcontactid").val(immunizationpatientid[1]);
       }
    }
	$("#tab6").click(function(){
		Immunizationaddgrid();
	});
	{
		var addcloseImmunizationcreation =['closeaddform','immunizationcreationview','immunizationcreationformadd'];
		addclose(addcloseImmunizationcreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('immunizationsectionoverlay');
		});
	}
	{
		$('#immunizationaddicon').click(function(){
			sectionpanelheight('immunizationsectionoverlay');
			$('#immunizationsectionoverlay').removeClass('closed');
			$('#immunizationsectionoverlay').addClass('effectbox');
			$('#immunizationdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#immunizationsavebutton').show();
			showhideiconsfun('addclick','Immunizationcreationformadd');
			firstfieldfocus();
			var elementname = $('#immunizationelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('immunizationanfieldnameinfo','immunizationanfieldnameidinfo','immunizationanfieldnamemodinfo','immunizationanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			$("#branchid").select2('val',loggedinbranchid);
			$("#immunizationemployeeid").select2('val',loggedinuser);
			fortabtouch = 0;
		});
		$('#immunizationediticon').click(function(){
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('immunizationsectionoverlay');
				$('#immunizationsectionoverlay').removeClass('closed');
				$('#immunizationsectionoverlay').addClass('effectbox');
				addslideup('immunizationcreationview','immunizationcreationformadd');
				$('#immunizationdataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#immunizationsavebutton').hide();
				Immunizationdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','immunizationcreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationdeleteicon').click(function(){
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#immunizationprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('immunizationdeleteyes');
				$("#immunizationdeleteyes").click(function(){
					var datarowid =$("#immunizationprimarydataid").val();
					Immunizationrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationreloadicon').click(function(){
			Immunizationrefreshgrid();
		});
		$('#immunizationformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('immunizationanfieldnameinfo','immunizationanfieldnameidinfo','immunizationanfieldnamemodinfo','immunizationanfieldtabinfo');
		});
		$('#immunizationbasedeleteyes').click(function(){
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			Immunizationrecorddelete(datarowid);
		});
		$('#immunizationdetailedviewicon').click(function() {
			var datarowid = $('#Immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#immunizationsectionoverlay').removeClass('closed');
				$('#immunizationsectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Immunizationdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','immunizationcreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationeditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Immunizationcreationformadd');
		});
		
		$('#immunizationprinticon').click(function() {
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationcloneicon').click(function() {
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#immunizationsectionoverlay').removeClass('closed');
				$('#immunizationsectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Immunizationclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('immunizationanfieldnameinfo','immunizationanfieldnameidinfo','immunizationanfieldnamemodinfo','immunizationanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#immunizationsmsicon').click(function() {
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationmailicon').click(function() {
			var datarowid = $('#immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Immunization','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5004');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#immunizationoutgoingcallicon').click(function() {
			var datarowid = $('#Immunizationaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#immunizationcustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#immunizationimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#immunizationdatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#immunizationfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#immunizationsavebutton').click(function(){
			$('.ftab').trigger('click');
			$('#immunizationformaddwizard').validationEngine('validate');
		});
		$('#immunizationformaddwizard').validationEngine({
			onSuccess: function() {
				$('#immunizationsavebutton').attr('disabled','disabled');
				Immunizationnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#immunizationdataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#immunizationformeditwizard').validationEngine('validate');
		});
		$('#immunizationformeditwizard').validationEngine({
			onSuccess: function() {
				$('#immunizationdataupdatesubbtn').attr('disabled','disabled');
				Immunizationdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".immunizationfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#immunizationsectionoverlay").removeClass("effectbox");
			$("#immunizationsectionoverlay").addClass("closed");
		});
	}
	{
		//filter work
		//for toggle
		$('#immunizationviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.immunizationfilterview').addClass("hidedisplay");
				$('.immunizationfullgridview').removeClass("large-9");
				$('.immunizationfullgridview').addClass("large-12");
			}else{
				$('.immunizationfullgridview').removeClass("large-12");
				$('.immunizationfullgridview').addClass("large-9");
				$('.immunizationfilterview').removeClass("hidedisplay");
			}			
		});
		$('#immunizationclosefiltertoggle').click(function(){
			$('.immunizationfilterview').addClass("hidedisplay");
			$('.immunizationfullgridview').removeClass("large-9");
			$('.immunizationfullgridview').addClass("large-12");
		});
		//filter
		$("#immunizationfilterddcondvaluedivhid").hide();
		$("#immunizationfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#immunizationfiltercondvalue").focusout(function(){
				var value = $("#immunizationfiltercondvalue").val();
				$("#immunizationfinalfiltercondvalue").val(value);
			});
			$("#immunizationfilterddcondvalue").change(function(){
				var value = $("#immunizationfilterddcondvalue").val();
				$("#immunizationfinalfiltercondvalue").val(value);
			});
		}
		immunizationfiltername = [];
		$("#immunizationfilteraddcondsubbtn").click(function() {
			$("#immunizationfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#immunizationfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#immunizationfiltercolumn").val();
				var label =$("#immunizationfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#immunizationfiltercondition").val();
				var value = $("#immunizationfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = immunizationfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#immunizationfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons immunizationfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				immunizationfiltername.push(data);
				$("#immunizationconditionname").val(immunizationfiltername);
				immunizationaddgrid();
				$(".immunizationfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = immunizationfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#immunizationconditionname").val();
							var filternewcond = nvalue.split(',');
							immunizationfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#immunizationconditionname").val(immunizationfiltername);
							immunizationaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#immunizationfiltercolumn").select2('val','');
				$("#immunizationfiltercondition").select2('val','');
				$("#immunizationfilterddcondvalue").select2('val','');
				$("#immunizationfiltercondvalue").val('');
				$("#immunizationfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Immunizationaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#immunizationaddgridwidth").width();
	var wheight = $("#immunizationaddgridwidth").height();
	var sortcol = $('.Immunizationsheadercolsort').hasClass('datasort') ? $('.Immunizationsheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Immunizationsheadercolsort').hasClass('datasort') ? $('.Immunizationsheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Immunizationsheadercolsort').hasClass('datasort') ? $('.Immunizationsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '135';
	}
	userviewid = userviewid!= '' ? '135' : userviewid;
	var viewfieldids = 61;
	var Immunizationcontactid = $("#immunizationcontactid").val();
	if(Immunizationcontactid != ''){
		var Immunizationfilterid = '2230|Equalto|'+Immunizationcontactid+'';
	} else {
		var Immunizationfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=immunization&primaryid=immunizationid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Immunizationfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#immunizationaddgrid').empty();
			$('#immunizationaddgrid').append(data.content);
			$('.immunizationgridfootercontainer').empty();
			$('.immunizationgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('immunizationaddgrid');
			innergridresizeheightset('Immunizationaddgrid');
			{//sorting
				$('.Immunizationsheadercolsort').click(function(){
					$('.Immunizationsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Immunizationspgnumcnt li .page-text .active').data('rowcount');
					Immunizationaddgrid(page,rowcount);
				});
				sortordertypereset('Immunizationsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Immunizationaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Immunizationaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			innergridresizeheightset('Immunizationaddgrid');
		},
	});
}
function Immunizationrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Immunizationspgnumcnt li .page-text .active').data('rowcount');
		Immunizationaddgrid(page,rowcount);
}
function Immunizationclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Immunizationdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Immunizationnewdataaddfun() {
	var amp = '&';
	var formdata = $('#immunizationaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Immunization/newdatacreate',
			data: 'datas=' + datainformation,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					Immunizationrefreshgrid();
					alertpopup(savealert);
					resetFields();
					$('#immunizationsavebutton').attr('disabled',false);
					$('.immunizationfwgsectionoverlayclose').trigger('click');
					addformview = 0;
					var icontacid = $("#emrdynamicdddataview").val();
					$("#immunizationcontactid").val(icontacid);
				}
			},
	})
}
function Immunizationdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#immunizationelementsname').val();
	var elementstable = $('#immunizationelementstable').val();
	var elementscolmn = $('#immunizationelementscolmn').val();
	var elementpartable = $('#immunizationelementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Immunization/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('.immunizationfwgsectionoverlayclose').trigger('click');
				Immunizationrefreshgrid();
				$('#immunizationdataupdatesubbtn').attr('disabled',false);
			} else {
				var txtboxname = elementsname + ',immunizationprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				Materialize.updateTextFields();
				$("#immunizationprimarydataid").val(datarowid);
				$('#processoverlay').hide();
				$("#immunizationsectionoverlay").removeClass("closed");
				$("#immunizationsectionoverlay").addClass("effectbox");
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay');
			}
		}
	});
}
function Immunizationdataupdatefun() {
	var amp = '&';
	var formdata = $('#immunizationaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Immunization/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.immunizationfwgsectionoverlayclose').trigger('click');
				var icontacid = $("#emrdynamicdddataview").val();
				$("#immunizationcontactid").val(icontacid);
				Immunizationrefreshgrid();
				$('#immunizationdataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Immunizationrecorddelete(datarowid) {
	var elementstable = $('#immunizationelementstable').val();
	var elementspartable = $('#immunizationelementspartabname').val();
	$.ajax({
		url: base_url + 'Immunization/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				Immunizationrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				Immunizationrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
