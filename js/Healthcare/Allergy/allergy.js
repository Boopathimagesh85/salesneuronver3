$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var allergypatientid =  ca[i].split('=');
       if(allergypatientid[0] == 'patientid'){
    	   $("#allergycontactid").val(allergypatientid[1]);
       }
    }
	$("#tab3").click(function(){
		Allergyaddgrid();
	});
	{
		var addcloseAllergycreation =['closeaddform','Allergycreationview','Allergycreationformadd'];
		addclose(addcloseAllergycreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('allergysectionoverlay');
		});
	}
	{
		$('#allergyaddicon').click(function(){
			resetFields();
			sectionpanelheight('allergysectionoverlay');
			$('#allergysectionoverlay').removeClass('closed');
			$('#allergysectionoverlay').addClass('effectbox');
			$('#allergydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#allergysavebutton').show();
			showhideiconsfun('addclick','Allergycreationformadd');
			firstfieldfocus();
			var elementname = $('#allergyelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('allergyanfieldnameinfo','allergyanfieldnameidinfo','allergyanfieldnamemodinfo','allergyanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var acontacid = $("#emrdynamicdddataview").val();
			$("#allergycontactid").val(acontacid);
			fortabtouch = 0;
		});
		$('#allergyediticon').click(function(){
			var datarowid = $('#allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('allergysectionoverlay');
				$('#allergysectionoverlay').removeClass('closed');
				$('#allergysectionoverlay').addClass('effectbox');
				addslideup('Allergycreationview','Allergycreationformadd');
				$('#allergydataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#allergysavebutton').hide();
				Allergydataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Allergycreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#allergydeleteicon').click(function(){
			var datarowid = $('#allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#allergyprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('allergydeleteyes');
				$("#allergydeleteyes").click(function(){
					var datarowid =$("#allergyprimarydataid").val();
					Allergyrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#allergyreloadicon').click(function(){
			allergyrefreshgrid();
		});
		$('#allergyformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('allergyanfieldnameinfo','allergyanfieldnameidinfo','allergyanfieldnamemodinfo','allergyanfieldtabinfo');
		});
		$('#allergybasedeleteyes').click(function(){
			var datarowid = $('#allergyaddgrid div.gridcontent div.active').attr('id');
			Allergyrecorddelete(datarowid);
		});
		$('#allergydetailedviewicon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#allergysectionoverlay').removeClass('closed');
				$('#allergysectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Allergydataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','Allergycreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					Allergydataupdateinfofetchfun(rdatarowid);
					showhideiconsfun('summryclick','Allergycreationformadd');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$('#allergyeditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Allergycreationformadd');
		});
		
		$('#allergyprinticon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#allergycloneicon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#groupsectionoverlay').removeClass('closed');
				$('#groupsectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Allergyclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('allergyanfieldnameinfo','allergyanfieldnameidinfo','allergyanfieldnamemodinfo','allergyanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#allergysmsicon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#allergymailicon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Allergy','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5002');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#allergyoutgoingcallicon').click(function() {
			var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#allergycustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#allergyimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#allergydatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#allergyfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
		$(".allergyfwgsectionoverlayclose").click(function(){
			$("#allergysectionoverlay").removeClass("effectbox");
			$("#allergysectionoverlay").addClass("closed");
			resetFields();
		});
	}
	{
		//filter work
		//for toggle
		$('#allergyviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.allergyfilterview').addClass("hidedisplay");
				$('.allergyfullgridview').removeClass("large-9");
				$('.allergyfullgridview').addClass("large-12");
			}else{
				$('.allergyfullgridview').removeClass("large-12");
				$('.allergyfullgridview').addClass("large-9");
				$('.allergyfilterview').removeClass("hidedisplay");
			}			
		});
		$('#allergyclosefiltertoggle').click(function(){
			$('.allergyfilterview').addClass("hidedisplay");
			$('.allergyfullgridview').removeClass("large-9");
			$('.allergyfullgridview').addClass("large-12");
		});
		//filter
		$("#allergyfilterddcondvaluedivhid").hide();
		$("#allergyfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#allergyfiltercondvalue").focusout(function(){
				var value = $("#allergyfiltercondvalue").val();
				$("#allergyfinalfiltercondvalue").val(value);
			});
			$("#allergyfilterddcondvalue").change(function(){
				var value = $("#allergyfilterddcondvalue").val();
				$("#allergyfinalfiltercondvalue").val(value);
			});
		}
		allergyfiltername = [];
		$("#allergyfilteraddcondsubbtn").click(function() {
			$("#allergyfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#allergyfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#allergyfiltercolumn").val();
				var label =$("#allergyfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#allergyfiltercondition").val();
				var value = $("#allergyfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = allergyfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#allergyfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons allergyfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				allergyfiltername.push(data);
				$("#allergyconditionname").val(allergyfiltername);
				allergyaddgrid();
				$(".allergyfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = allergyfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#allergyconditionname").val();
							var filternewcond = nvalue.split(',');
							allergyfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#allergyconditionname").val(allergyfiltername);
							allergyaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#allergyfiltercolumn").select2('val','');
				$("#allergyfiltercondition").select2('val','');
				$("#allergyfilterddcondvalue").select2('val','');
				$("#allergyfiltercondvalue").val('');
				$("#allergyfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('#allergysavebutton').click(function(){
			$('#allergyformaddwizard').validationEngine('validate');
		});
		$('#allergyformaddwizard').validationEngine({
			onSuccess: function() {
				$('#allergysavebutton').attr('disabled','disabled');
				Allergynewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#allergydataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#allergyformeditwizard').validationEngine('validate');
		});
		$('#allergyformeditwizard').validationEngine({
			onSuccess: function() {
				$('#allergydataupdatesubbtn').attr('disabled','disabled');
				Allergydataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	//allergy type name based allergy name
	$("#allergytypeid").change(function(){
		var allergytypeid = $("#allergytypeid").val();
		$.ajax({
			url: base_url + "Allergy/allergynameget?typeid="+allergytypeid,
			dataType:'json',
			async :false,
			cache:false,
			success: function(data) 
			{
				if((data.fail) == 'Failure') {
				} else {
					$('#allergennameid').empty();
					$('#allergennameid').append($("<option value='' ></option>"));
					$.each(data, function(index) {
						$('#allergennameid')
						.append($("<option value='' ></option>")
						.attr("data-fieldname",data[index]['allergenname'])
						.attr("value",data[index]['id'])
						.text(data[index]['allergenname']));
					});
				}	
			},
		});
	});
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Allergyaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#allergyaddgridwidth").width();
	var wheight = $("#allergyaddgridwidth").height();
	var sortcol = $('.Allergysheadercolsort').hasClass('datasort') ? $('.Allergysheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Allergysheadercolsort').hasClass('datasort') ? $('.Allergysheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Allergysheadercolsort').hasClass('datasort') ? $('.Allergysheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '133';
	}
	userviewid = userviewid!= '' ? '133' : userviewid;
	var viewfieldids = 59;
	var Allergycontactid = $("#allergycontactid").val();
	if(Allergycontactid != ''){
		var Allergyfilterid = '2228|Equalto|'+Allergycontactid+'';
	} else {
		var Allergyfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=allergy&primaryid=allergyid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Allergyfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#allergyaddgrid').empty();
			$('#allergyaddgrid').append(data.content);
			$('.allergygridfootercontainer').empty();
			$('.allergygridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('allergyaddgrid');
			innergridresizeheightset('Allergyaddgrid');
			{//sorting
				$('.Allergysheadercolsort').click(function(){
					$('.Allergysheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Allergyspgnumcnt li .page-text .active').data('rowcount');
					Allergyaddgrid(page,rowcount);
				});
				sortordertypereset('Allergysheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Allergyaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Allergyaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			innergridresizeheightset('Allergyaddgrid');
		},
	});
}
function allergyrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Allergyspgnumcnt li .page-text .active').data('rowcount');
		Allergyaddgrid(page,rowcount);
}
function Allergyclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Allergydataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Allergynewdataaddfun() {
	var amp = '&';
	var formdata = $('#allergyaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Allergy/newdatacreate',
			data: 'datas=' + datainformation,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					allergyrefreshgrid();
					alertpopup(savealert);
					resetFields();
					$('#allergysavebutton').attr('disabled',false);
					$('.allergyfwgsectionoverlayclose').trigger('click');
					addformview = 0;
				}
			},
	})
}
function Allergydataupdateinfofetchfun(datarowid) {
	var elementsname = $('#allergyelementsname').val();
	var elementstable = $('#allergyelementstable').val();
	var elementscolmn = $('#allergyelementscolmn').val();
	var elementpartable = $('#allergyelementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Allergy/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#Allergycreationformadd').hide();
				$('#Allergycreationview').fadeIn(1000);
				allergyrefreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('Allergycreationview','Allergycreationformadd');
				var txtboxname = elementsname + ',allergyprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dataname = elementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			}
		}
	});
}
function Allergydataupdatefun() {
	var amp = '&';
	var formdata = $('#allergyaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Allergy/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.allergyfwgsectionoverlayclose').trigger('click');
				allergyrefreshgrid();
				$('#allergydataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function Allergyrecorddelete(datarowid) {
	var elementstable = $('#allergyelementstable').val();
	var elementspartable = $('#allergyelementspartabname').val();
	$.ajax({
		url: base_url + 'Allergy/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				allergyrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				allergyrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewfieldids').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
