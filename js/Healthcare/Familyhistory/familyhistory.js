$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var fa = document.cookie.split(';');
	for(var i = 0; i <fa.length; i++) {
       var familyhistorypatientid =  fa[i].split('=');
       if(familyhistorypatientid[0] == 'patientid'){
    	   $("#familyhistorycontactid").val(familyhistorypatientid[1]);
       }
    }
	$("#tab5").click(function(){
		Familyhistoryaddgrid();
	});
	{
		var addcloseFamilyhistorycreation =['closeaddform','familyhistorycreationview','familyhistorycreationformadd'];
		addclose(addcloseFamilyhistorycreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('familyhistorysectionoverlay');
		});
	}
	{
		$('#familyhistoryaddicon').click(function(){
			resetFields();
			sectionpanelheight('familyhistorysectionoverlay');
			$('#familyhistorysectionoverlay').removeClass('closed');
			$('#familyhistorysectionoverlay').addClass('effectbox');
			$('#familyhistorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#familyhistorysavebutton').show();
			showhideiconsfun('addclick','Familyhistorycreationformadd');
			firstfieldfocus();
			var elementname = $('#familyhistoryelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('familyhistoryanfieldnameinfo','familyhistoryanfieldnameidinfo','familyhistoryanfieldnamemodinfo','familyhistoryanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var fcontacid = $("#emrdynamicdddataview").val();
			$("#familyhistorycontactid").val(fcontacid);
			fortabtouch = 0;
		});
		$('#familyhistoryediticon').click(function(){
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('familyhistorysectionoverlay');
				$('#familyhistorysectionoverlay').removeClass('closed');
				$('#familyhistorysectionoverlay').addClass('effectbox');
				$('#familyhistorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#familyhistorysavebutton').hide();
				Familyhistorydataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Familyhistorycreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistorydeleteicon').click(function(){
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#familyhistoryprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('familyhistorydeleteyes');
				$("#familyhistorydeleteyes").click(function(){
					var datarowid =$("#familyhistoryprimarydataid").val();
					Familyhistoryrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistoryreloadicon').click(function(){
			familyhistoryrefreshgrid();
		});
		$('#familyhistoryformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('familyhistoryanfieldnameinfo','familyhistoryanfieldnameidinfo','familyhistoryanfieldnamemodinfo','familyhistoryanfieldtabinfo');
		});
		$('#familyhistorybasedeleteyes').click(function(){
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			Familyhistoryrecorddelete(datarowid);
		});
		$('#familyhistorydetailedviewicon').click(function() {
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#familyhistorysectionoverlay').removeClass('closed');
				$('#familyhistorysectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Familyhistorydataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','familyhistorycreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistoryeditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','familyhistorycreationformadd');
		});
		
		$('#familyhistoryprinticon').click(function() {
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistorycloneicon').click(function() {
			var datarowid = $('#Familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#familyhistorysectionoverlay').removeClass('closed');
				$('#familyhistorysectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Familyhistoryclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('familyhistoryanfieldnameinfo','familyhistoryanfieldnameidinfo','familyhistoryanfieldnamemodinfo','familyhistoryanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#familyhistorysmsicon').click(function() {
			var datarowid = $('#Familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistorymailicon').click(function() {
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Familyhistory','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5010');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#familyhistoryoutgoingcallicon').click(function() {
			var datarowid = $('#familyhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#familyhistorycustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#familyhistoryimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#familyhistorydatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#familyhistoryfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#familyhistorysavebutton').click(function(){
			$('.ftab').trigger('click');
			$('#familyhistoryformaddwizard').validationEngine('validate');
		});
		$('#familyhistoryformaddwizard').validationEngine({
			onSuccess: function() {
				$('#familyhistorysavebutton').attr('disabled','disabled');
				Familyhistorynewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#familyhistorydataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#familyhistoryformeditwizard').validationEngine('validate');
		});
		$('#familyhistoryformeditwizard').validationEngine({
			onSuccess: function() {
				$('#familyhistorydataupdatesubbtn').attr('disabled','disabled');
				Familyhistorydataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	{//Addform section overlay close
		$(".familyhistoryfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#familyhistorysectionoverlay").removeClass("effectbox");
			$("#familyhistorysectionoverlay").addClass("closed");
		});
	}
	}	
	{
		//Calculate age from DOB
		$('#birthdate').change(function(){
			var user_date = $('#birthdate').val();
			var dateAr = user_date.split('-');
			var newDate = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);
			var birthdate = new Date(newDate);
			var cur = new Date();
			var diff = cur-birthdate;
			var age = Math.floor(diff/31536000000);
			$('#age').val(age);
			Materialize.updateTextFields();
		});
	}	
	{
		//filter work
		//for toggle
		$('#familyhistoryviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.familyhistoryfilterview').addClass("hidedisplay");
				$('.familyhistoryfullgridview').removeClass("large-9");
				$('.familyhistoryfullgridview').addClass("large-12");
			}else{
				$('.familyhistoryfullgridview').removeClass("large-12");
				$('.familyhistoryfullgridview').addClass("large-9");
				$('.familyhistoryfilterview').removeClass("hidedisplay");
			}			
		});
		$('#familyhistoryclosefiltertoggle').click(function(){
			$('.familyhistoryfilterview').addClass("hidedisplay");
			$('.familyhistoryfullgridview').removeClass("large-9");
			$('.familyhistoryfullgridview').addClass("large-12");
		});
		//filter
		$("#familyhistoryfilterddcondvaluedivhid").hide();
		$("#familyhistoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#familyhistoryfiltercondvalue").focusout(function(){
				var value = $("#familyhistoryfiltercondvalue").val();
				$("#familyhistoryfinalfiltercondvalue").val(value);
			});
			$("#familyhistoryfilterddcondvalue").change(function(){
				var value = $("#familyhistoryfilterddcondvalue").val();
				$("#familyhistoryfinalfiltercondvalue").val(value);
			});
		}
		familyhistoryfiltername = [];
		$("#familyhistoryfilteraddcondsubbtn").click(function() {
			$("#familyhistoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#familyhistoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#familyhistoryfiltercolumn").val();
				var label =$("#familyhistoryfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#familyhistoryfiltercondition").val();
				var value = $("#familyhistoryfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = familyhistoryfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#familyhistoryfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons familyhistoryfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				familyhistoryfiltername.push(data);
				$("#familyhistoryconditionname").val(familyhistoryfiltername);
				familyhistoryaddgrid();
				$(".familyhistoryfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = familyhistoryfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#familyhistoryconditionname").val();
							var filternewcond = nvalue.split(',');
							familyhistoryfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#familyhistoryconditionname").val(familyhistoryfiltername);
							familyhistoryaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#familyhistoryfiltercolumn").select2('val','');
				$("#familyhistoryfiltercondition").select2('val','');
				$("#familyhistoryfilterddcondvalue").select2('val','');
				$("#familyhistoryfiltercondvalue").val('');
				$("#familyhistoryfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Familyhistoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#familyhistoryaddgridwidth").width();
	var wheight = $("#familyhistoryaddgridwidth").height();
	var sortcol = $('.Familyhistorysheadercolsort').hasClass('datasort') ? $('.Familyhistorysheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Familyhistorysheadercolsort').hasClass('datasort') ? $('.Familyhistorysheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Familyhistorysheadercolsort').hasClass('datasort') ? $('.Familyhistorysheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '139';
	}
	userviewid = userviewid!= '' ? '139' : userviewid;
	var viewfieldids = '67';
	var familyhistorycontactid = $("#familyhistorycontactid").val();
	if(familyhistorycontactid != ''){
		var Familyhistoryfilterid = '2231|Equalto|'+familyhistorycontactid+'';
	} else {
		var Familyhistoryfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=familyhistory&primaryid=familyhistoryid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Familyhistoryfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#familyhistoryaddgrid').empty();
			$('#familyhistoryaddgrid').append(data.content);
			$('.familyhistorygridfootercontainer').empty();
			$('.familyhistorygridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('familyhistoryaddgrid');
			innergridresizeheightset('Familyhistoryaddgrid');
			{//sorting
				$('.Familyhistorysheadercolsort').click(function(){
					$('.Familyhistorysheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Familyhistoryspgnumcnt li .page-text .active').data('rowcount');
					Familyhistoryaddgrid(page,rowcount);
				});
				sortordertypereset('Familyhistorysheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Familyhistoryaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Familyhistoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			innergridresizeheightset('Familyhistoryaddgrid');
		},
	});
}
function familyhistoryrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Familyhistoryspgnumcnt li .page-text .active').data('rowcount');
		Familyhistoryaddgrid(page,rowcount);
}
function Familyhistoryclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Familyhistorydataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Familyhistorynewdataaddfun() {
	var amp = '&';
	var formdata = $('#familyhistoryaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Familyhistory/newdatacreate',
			data: 'datas=' + datainformation,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					familyhistoryrefreshgrid();
					alertpopup(savealert);
					resetFields();
					$('.familyhistoryfwgsectionoverlayclose').trigger('click');
					$('#familyhistorysavebutton').attr('disabled',false);
					addformview = 0;
					var fcontacid = $("#emrdynamicdddataview").val();
					$("#familyhistorycontactid").val(fcontacid);
				}
			},
	})
}
function Familyhistorydataupdateinfofetchfun(datarowid) {
	var elementsname = $('#familyhistoryelementsname').val();
	var elementstable = $('#familyhistoryelementstable').val();
	var elementscolmn = $('#familyhistoryelementscolmn').val();
	var elementpartable = $('#familyhistoryelementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Familyhistory/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('.familyhistoryfwgsectionoverlayclose').hide();
				familyhistoryrefreshgrid();
				$('#familyhistorydataupdatesubbtn').attr('disabled',false);
			} else {
				var txtboxname = elementsname + ',familyhistoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				$("#familyhistoryprimarydataid").val(datarowid);
				$('#processoverlay').hide();
				$("#familyhistorysectionoverlay").removeClass("closed");
				$("#familyhistorysectionoverlay").addClass("effectbox");
				$('.updatebtnclass').removeClass('hidedisplay');
				$('.addbtnclass').addClass('hidedisplay');
			}
		}
	});
}
function Familyhistorydataupdatefun() {
	var amp = '&';
	var formdata = $('#familyhistoryaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Familyhistory/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.familyhistoryfwgsectionoverlayclose').hide();
				var fcontacid = $("#emrdynamicdddataview").val();
				$("#familyhistorycontactid").val(fcontacid);
				familyhistoryrefreshgrid();
				$('#familyhistorydataupdatesubbtn').attr('disabled',false);
				$("#familyhistorysavebutton").hide();
				$("#familyhistorydataupdatesubbtn,#familyhistoryfwgsectionoverlayclose").show();
				alertpopup(savealert);
			}
		},
	});
}
function Familyhistoryrecorddelete(datarowid) {
	var elementstable = $('#familyhistoryelementstable').val();
	var elementspartable = $('#familyhistoryelementspartabname').val();
	$.ajax({
		url: base_url + 'Familyhistory/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				familyhistoryrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				familyhistoryrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}