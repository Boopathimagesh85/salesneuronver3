$(document).ready(function(){
	METHOD ='ADD';
	UPDATEID = 0;
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var prescriptionpatientid =  ca[i].split('=');
       if(prescriptionpatientid[0] == 'patientid'){
    	   $("#prescriptioncontactid").val(prescriptionpatientid[1]);
       }
    }
	
	{
		$("#prescriptionspreingrideditspan1").show();
	}
	$("#tab10").click(function(){
		Prescriptionsaddgrid();
		Prescriptionspreaddgrid1();
	});
	{
		var addclosePrescriptionscreation =['closeaddform','Prescriptionscreationview','Prescriptionscreationformadd'];
		addclose(addclosePrescriptionscreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$('#prescriptionsaddicon').click(function(){
			resetFields();
			$('#prescriptionssectionoverlay').removeClass('closed');
			$('#prescriptionssectionoverlay').addClass('effectbox');
			$('.addbtnclass').removeClass('hidedisplay');
			$('.updatebtnclass').addClass('hidedisplay');
			showhideiconsfun('addclick','Prescriptionscreationformadd');
			firstfieldfocus();
			var elementname = $('#prescriptionselementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('prescriptionsanfieldnameinfo','prescriptionsanfieldnameidinfo','prescriptionsanfieldnamemodinfo','prescriptionsanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var pcontacid = $("#emrdynamicdddataview").val();
			$("#prescriptioncontactid").val(pcontacid);
			fortabtouch = 0;
		});
		$('#prescriptionsediticon').click(function(){
			var datarowid = $('#prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.updatebtnclass').removeClass('hidedisplay');
				$('.addbtnclass').addClass('hidedisplay');
				var elementname = $('#prescriptionselementsname').val();
				elementdefvalueset(elementname);
				Prescriptionsdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Prescriptionscreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptionsdeleteicon').click(function(){
			var datarowid = $('#prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#prescriptionsprimarydataid").val(datarowid);
				combainedmoduledeletealert('prescriptionsdeleteyes');
				$("#prescriptionsdeleteyes").click(function(){
					var datarowid =$("#prescriptionsprimarydataid").val();
					Prescriptionsrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptionsreloadicon').click(function(){
			prescriptionsrefreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			Prescriptionsrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Prescriptionsdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','Prescriptionscreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
			showhideiconsfun('editfromsummryclick','Prescriptionscreationformadd');
		});
		
		$('#printicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptioncloneicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				Prescriptionsclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptionsmsicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptionmailicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Prescriptions','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5012');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#prescriptionoutgoingcallicon').click(function() {
			var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#prescriptioncustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#prescriptionimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#prescriptiondatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	//validation for  Add
	$('#prescriptionsdataaddsbtn').mousedown(function(e) {
		if(e.which == 1 || e.which === undefined) {
			masterfortouch = 0;
			$('#drugnameid').removeClass('validate[required]');
			$('#duration').removeClass('validate[required,custom[integer],maxSize[100]]');
			$('#intakeid').removeClass('validate[required]');
			$("#prescriptionsformaddwizard").validationEngine('validate');
		}
	});
	jQuery("#prescriptionsformaddwizard").validationEngine({
		onSuccess: function() {
			$('#prescriptionsdataaddsbtn').attr('disabled','disabled');
			Prescriptionsnewdataaddfun();
		},
		onFailure: function() {
			$('#drugnameid').addClass('validate[required]');
			$('#duration').addClass('validate[required,custom[integer],maxSize[100]]');
			$('#intakeid').addClass('validate[required]');
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//for update
	$('#prescriptionsdataupdatesubbtn').click(function(){
		$('#drugnameid').removeClass('validate[required]');
		$('#duration').removeClass('validate[required,custom[integer],maxSize[100]]');
		$('#intakeid').removeClass('validate[required]');
		$('#prescriptionsformeditwizard').validationEngine('validate');
	});
	$('#prescriptionsformeditwizard').validationEngine({
		onSuccess: function() {
			$('#prescriptionsdataupdatesubbtn').attr('disabled','disabled');
			Prescriptionsdataupdatefun();
		},
		onFailure: function() {
			$('#drugnameid').addClass('validate[required]');
			$('#duration').addClass('validate[required,custom[integer],maxSize[100]]');
			$('#intakeid').addClass('validate[required]');
			$('#tabgropdropdown').select2('val','1');
			alertpopup(validationalert);
		}
	});
	{
		//filter work
		//for toggle
		$('#prescriptionsviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.prescriptionsfilterview').addClass("hidedisplay");
				$('.prescriptionsfullgridview').removeClass("large-9");
				$('.prescriptionsfullgridview').addClass("large-12");
			}else{
				$('.prescriptionsfullgridview').removeClass("large-12");
				$('.prescriptionsfullgridview').addClass("large-9");
				$('.prescriptionsfilterview').removeClass("hidedisplay");
			}			
		});
		$('#prescriptionsclosefiltertoggle').click(function(){
			$('.prescriptionsfilterview').addClass("hidedisplay");
			$('.prescriptionsfullgridview').removeClass("large-9");
			$('.prescriptionsfullgridview').addClass("large-12");
		});
		//filter
		$("#prescriptionsfilterddcondvaluedivhid").hide();
		$("#prescriptionsfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#prescriptionsfiltercondvalue").focusout(function(){
				var value = $("#prescriptionsfiltercondvalue").val();
				$("#prescriptionsfinalfiltercondvalue").val(value);
			});
			$("#prescriptionsfilterddcondvalue").change(function(){
				var value = $("#prescriptionsfilterddcondvalue").val();
				$("#prescriptionsfinalfiltercondvalue").val(value);
			});
		}
		prescriptionsfiltername = [];
		$("#prescriptionsfilteraddcondsubbtn").click(function() {
			$("#prescriptionsfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#prescriptionsfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#prescriptionsfiltercolumn").val();
				var label =$("#prescriptionsfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#prescriptionsfiltercondition").val();
				var value = $("#prescriptionsfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = prescriptionsfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#prescriptionsfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons prescriptionsfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				prescriptionsfiltername.push(data);
				$("#prescriptionsconditionname").val(prescriptionsfiltername);
				prescriptionsaddgrid();
				$(".prescriptionsfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = prescriptionsfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#prescriptionsconditionname").val();
							var filternewcond = nvalue.split(',');
							prescriptionsfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#prescriptionsconditionname").val(prescriptionsfiltername);
							prescriptionsaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#prescriptionsfiltercolumn").select2('val','');//filterddcondvalue
				$("#prescriptionsfiltercondition").select2('val','');
				$("#prescriptionsfilterddcondvalue").select2('val','');
				$("#prescriptionsfiltercondvalue").val('');
				$("#prescriptionsfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{//Addform section overlay close
		$(".prescriptionsfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#prescriptionssectionoverlay").removeClass("effectbox");
			$("#prescriptionssectionoverlay").addClass("closed");
		});
	}
	{//inner grid delete
		prescriptionsdetaildelete= [];
		$('#prescriptionspreingriddel1').click(function(){
			var datarowid = $('#prescriptionspreaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {
				if(datarowid != '') {
					prescriptionsdetaildelete.push(datarowid);
				}
				deletegriddatarow('prescriptionspreaddgrid1',datarowid);
				//summary calculate for treatmentquantity/cost/discount/netamount
				var tabletinmorning =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','morning','sum'));
				$('#tabletinmorning').val('');
				$('#tabletinmorning').val(tabletinmorning);
				var tabletinnoon =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','noon','sum'));
				$('#tabletsinnoon').val('');
				$('#tabletsinnoon').val(tabletinnoon);
		        var tabletinnight =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','night','sum'));
		        $('#tabletsinnight').val('');
		        $('#tabletsinnight').val(tabletinnight);
			} else {
				alertpopup('Please select a row');
			}
		});
	}
	{// Dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#prescriptionsgridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.prescriptionsfrmtogridbutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');			
			griddataid = i;
			gridynamicname = gridname;
			$("#prescriptionsaddgrid1validation").validationEngine('validate');
			METHOD = 'ADD';
		});
		$('#prescriptionspreupdatebutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');			
			griddataid = i;
			gridynamicname = gridname;
			$("#prescriptionsaddgrid1validation").validationEngine('validate');
		});
	}
	$("#prescriptionsaddgrid1validation").validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#prescriptionsgridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#prescriptionsgridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				formtogriddata(gridname,METHOD,'',UPDATEID);
			}
			//summary calculate for treatmentquantity/cost/discount/netamount
			var tabletinmorning =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','morning','sum'));
			$('#tabletinmorning').val('');
			$('#tabletinmorning').val(tabletinmorning);
			var tabletinnoon =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','noon','sum'));
			$('#tabletsinnoon').val('');
			$('#tabletsinnoon').val(tabletinnoon);
	        var tabletinnight =  parseFloat(getgridcolvalue('prescriptionspreaddgrid1','','night','sum'));
	        $('#tabletsinnight').val('');
	        $('#tabletsinnight').val(tabletinnight);
	        /* Hide columns */
			var hideprodgridcol = ['prescriptionname','contactid','prescriptionsprescriptiondetailsid'];
			gridfieldhide('prescriptionspreaddgrid1',hideprodgridcol);
			/* Data row select event */
			datarowselectevt();
			$("#drugnameid").select2('val','');
			$("#intakeid").select2('val','');
			$("#timeframeid").select2('val','');
			$("#modeid").select2('val','');
			$("#duration").val('');
			$("#morning").val('');
			$("#noon").val('');
			$("#night").val('');
			griddataid = 0;
			gridynamicname = '';
			if(METHOD == 'UPDATE') {
				$('#prescriptionspreupdatebutton').addClass('hidedisplayfwg');
				$('#prescriptionspreupdatebutton').addClass('hidedisplay');//display the UPDATE button(inner - productgrid)
				$('#prescriptionspreaddbutton').removeClass('hidedisplayfwg');
				$('#prescriptionspreaddbutton').removeClass('hidedisplay');
			}
			METHOD = 'ADD'; //reset
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{
		$("#prescriptionspreingridedit1").click(function() {
			var selectedrow = $('#prescriptionspreaddgrid1 div.gridcontent div.active').attr('id');
			if(selectedrow) {
				var prescriptiondate = $("#prescriptionname").val();
				var pcontactid = $("#prescriptioncontactid").val();
				gridtoformdataset('prescriptionspreaddgrid1',selectedrow);
				$("#prescriptionname").val(prescriptiondate);
				$("#prescriptioncontactid").val(pcontactid);
				Materialize.updateTextFields();
				$('#prescriptionspreupdatebutton').removeClass('hidedisplayfwg');
				$('#prescriptionspreupdatebutton').removeClass('hidedisplay');//display the UPDATE button(inner - productgrid)
				$('#prescriptionspreaddbutton').addClass('hidedisplayfwg');
				$('#prescriptionspreaddbutton').addClass('hidedisplay');//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
});
function Prescriptionsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#prescriptionsaddgridwidth").width();
	var wheight = $("#prescriptionsaddgridwidth").height();
	var sortcol = $('.Prescriptionssheadercolsort').hasClass('datasort') ? $('.Prescriptionssheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Prescriptionssheadercolsort').hasClass('datasort') ? $('.Prescriptionssheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Prescriptionssheadercolsort').hasClass('datasort') ? $('.Prescriptionssheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '141';
	}
	userviewid = userviewid!= '' ? '141' : userviewid;
	var viewfieldids = 69;
	var prescriptionscontactid = $('#prescriptioncontactid').val();
	if(prescriptionscontactid != ''){
		var prescriptionsfilterid = '2233|Equalto|'+prescriptionscontactid+'';
	} else {
		var prescriptionsfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=prescriptions&primaryid=prescriptionsid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+prescriptionsfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#prescriptionsaddgrid').empty();
			$('#prescriptionsaddgrid').append(data.content);
			$('.prescriptionsgridfootercontainer').empty();
			$('.prescriptionsgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('Prescriptionsaddgrid');
			{//sorting
				$('.Prescriptionssheadercolsort').click(function(){
					$('.Prescriptionssheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Prescriptionsspgnumcnt li .page-text .active').data('rowcount');
					Prescriptionsaddgrid(page,rowcount);
				});
				sortordertypereset('Prescriptionssheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Prescriptionsaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Prescriptionsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
		},
	});
}
function Prescriptionspreaddgrid1() {
	var wwidth = $('#prescriptionspreaddgrid1').width();
	var wheight = $('#prescriptionspreaddgrid1').height();
	$.ajax({
		url:base_url+'Base/localgirdheaderinformationfetch?tabgroupid=177&moduleid=69&width='+wwidth+'&height='+wheight+'&modulename=Prescriptionspreaddgrid1',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#prescriptionspreaddgrid1').empty();
			$('#prescriptionspreaddgrid1').append(data.content);
			$('#prescriptionspreaddgrid1footer').empty();
			$('#prescriptionspreaddgrid1footer').append(data.footer);
			datarowselectevt();
			columnresize('prescriptionspreaddgrid1');
			 /* Hide columns */
			var hideprodgridcol = ['prescriptionname','contactid','prescriptionsprescriptiondetailsid'];
			gridfieldhide('prescriptionspreaddgrid1',hideprodgridcol);
		},
	});
}

function Prescriptionsclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Prescriptionsdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Prescriptionsnewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = 'prescriptionspreaddgrid1';
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#prescriptionselementsname').val();
	var elementstable = $('#prescriptionselementstable').val();
	var elementscolmn = $('#prescriptionselementscolmn').val();
	var elementspartabname = $('#prescriptionselementspartabname').val();
	var griddatapartabnameinfo = $('#prescriptionsgriddatapartabnameinfo').val();
	var resctable = $('#prescriptionsresctable').val();
	var editordata = '';
	var neweditordata = '';
	var editorname = '';
	if(editorname != '') {
		var editornames  = [];
		editornames = editorname.split(',');
		$.each( editornames, function( key, name ) {
			editordata = $('#'+name+'').editable('getHTML');
			var regExp = /&nbsp;/g;
			var datacontent = editordata.match(regExp);
			if(datacontent !== null){
				var gllen = datacontent.length;
				for (var i = 0; i < gllen; i++) {
					editordata = editordata.replace(''+datacontent[i]+'','a10s');
				}
			}
			var fname = [];
			fname = name.split('_');
			neweditordata += amp+fname[0]+'_editorfilename='+editordata;
			$('#'+fname[0]+'_editorfilename').val(editordata);
		});
	}
	var formdata = $('#prescriptionsaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Prescriptions/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#drugnameid').addClass('validate[required]');
				$('#duration').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#intakeid').addClass('validate[required]');
				cleargriddata('prescriptionspreaddgrid1');
				prescriptionsrefreshgrid();
				var pcontacid = $("#emrdynamicdddataview").val();
				$("#prescriptioncontactid").val(pcontacid);
				$(".prescriptionsfwgsectionoverlayclose").trigger("click");
				$('#prescriptionssavebutton').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
{//refresh grid
	function prescriptionsrefreshgrid() {
		var page = $('ul#prescriptionspgnum li.active').data('pagenum');
		var rowcount = $('ul#prescriptionspgnumcnt li .page-text .active').data('rowcount');
		Prescriptionsaddgrid(page,rowcount);
	}
}
function Prescriptionsdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#prescriptionselementsname').val();
	var elementstable = $('#prescriptionselementstable').val();
	var elementscolmn = $('#prescriptionselementscolmn').val();
	var elementpartable = $('#prescriptionselementspartabname').val();
	var resctable = '';
	$.ajax({
		url:base_url+'Prescriptions/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				resetFields();
				$(".prescriptionsfwgsectionoverlayclose").trigger("click");
				treatmentrefreshgrid();
				$('#prescriptionsdataupdatesubbtn').attr('disabled',false);
			} else {
				var treatmentmodel = data.treatmentmodel;
				$("#prescriptionsprimarydataid").val(datarowid);
				$("#prescriptioncontactid").val(data.prescriptioncontactid);
				$("#prescriptionname").val(data.prescriptionname);
				$("#tabletinmorning").val(data.tabletinmorning);
				$("#tabletsinnoon").val(data.tabletsinnoon);
				$("#tabletsinnight").val(data.tabletsinnight);
				//prescription details fetch
				prescriptionsdetail(datarowid);
				$("#prescriptionssectionoverlay").removeClass("closed");
				$("#prescriptionssectionoverlay").addClass("effectbox");
				$('#processoverlay').hide();
			}
		}
	});
}
//prescription inner grids details fetch
function prescriptionsdetail(datarowid) {
	if(datarowid!='') {
		$.ajax({
			url:base_url+'Prescriptions/prescriptionsdetailfetch?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('prescriptionspreaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('prescriptionspreaddgrid1');
					var hideprodgridcol = ['treatmentmodel','contactid','treatmenttreatmentdetailsid'];
					gridfieldhide('prescriptionspreaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
//prescription update
function Prescriptionsdataupdatefun() {
	var amp = '&';
	var gridname = 'prescriptionspreaddgrid1';
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editordata = '';
		var neweditordata = '';
		var editorname = '';
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(',');
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').editable('getHTML');
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){
					var glent = datacontent.length;
					for (var i = 0; i < glent; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				editordata += '&'+fname[0]+'_editorfilename='+editordata;
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
		}
		var resctable = '';
	}
	var formdata = $('#prescriptionsaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+"&prescriptionsdetaildelete="+prescriptionsdetaildelete;
	}else{
		dataset = 'datas=' + datainformation+"&prescriptionsdetaildelete="+prescriptionsdetaildelete;
	}
	$.ajax({
		url: base_url+'Prescriptions/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#drugnameid').addClass('validate[required]');
				$('#duration').addClass('validate[required,custom[integer],maxSize[100]]');
				$('#intakeid').addClass('validate[required]');
				cleargriddata('prescriptionspreaddgrid1');
				prescriptionsrefreshgrid();
				var pcontacid = $("#emrdynamicdddataview").val();
				$("#prescriptioncontactid").val(pcontacid);
				$(".prescriptionsfwgsectionoverlayclose").trigger("click");
				$('#prescriptionsdataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Prescriptionsrecorddelete(datarowid) {
	var elementstable = $('#prescriptionselementstable').val();
	var elementspartable = $('#prescriptionselementspartabname').val();
	$.ajax({
		url: base_url + 'Prescriptions/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				prescriptionsrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				prescriptionsrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}