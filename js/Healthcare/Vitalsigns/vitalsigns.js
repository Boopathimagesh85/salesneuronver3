$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var Vitalsignspatientid =  ca[i].split('=');
       if(Vitalsignspatientid[0] == 'patientid'){
    	   $("#vitalsignscontactid").val(Vitalsignspatientid[1]);
       }
    }
	$("#tab2").click(function(){
		Vitalsignsaddgrid();
	});
	{
		var addcloseVitalsignscreation =['closeaddform','Vitalsignscreationview','Vitalsignscreationformadd'];
		addclose(addcloseVitalsignscreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			Vitalsignsaddgrid();
		});
	}
	{
		fortabtouch = 0;
	}
	{//set default date
	    $("#recordeddate").datepicker();
	    $("#recordeddate").datepicker("setDate", new Date());
	}
	$(window).resize(function() {
		sectionpanelheight('vitalsignssectionoverlay');
	});
	{
		$('#vitalsignsaddicon').click(function(){
			resetFields();
			sectionpanelheight('vitalsignssectionoverlay');
			$('#vitalsignssectionoverlay').removeClass('closed');
			$('#vitalsignssectionoverlay').addClass('effectbox');
			$('#vitalsignsdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#vitalsignssavebutton').show();
			showhideiconsfun('addclick','Vitalsignscreationformadd');
			firstfieldfocus();
			var elementname = $('#vitalsignselementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('vitalsignsanfieldnameinfo','vitalsignsanfieldnameidinfo','vitalsignsanfieldnamemodinfo','vitalsignsanfieldtabinfo');
			var vcontacid = $("#emrdynamicdddataview").val();
			$("#vitalsignscontactid").val(vcontacid);
			$('#tabgropdropdown').select2('val','1');
			$("#vitalemployeeid").select2('val',loggedinuser);
			fortabtouch = 0;
		});
		$('#vitalsignsediticon').click(function(){
			var datarowid = $('#vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('vitalsignssectionoverlay');
				$('#vitalsignssectionoverlay').removeClass('closed');
				$('#vitalsignssectionoverlay').addClass('effectbox');
				addslideup('Vitalsignscreationview','Vitalsignscreationformadd');
				$('#vitalsignsdataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#vitalsignssavebutton').hide();
				Vitalsignsdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Vitalsignscreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#vitalsignsdeleteicon').click(function(){
			var datarowid = $('#vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#vitalsignsprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('vitalsignsdeleteyes');
				$("#vitalsignsdeleteyes").click(function(){
					var datarowid =$("#vitalsignsprimarydataid").val();
					Vitalsignsrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#vitalsignsreloadicon').click(function(){
			vitalsignsrefreshgrid();
		});
		$('#vitalsignsformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('vitalsignsanfieldnameinfo','vitalsignsanfieldnameidinfo','vitalsignsanfieldnamemodinfo','vitalsignsanfieldtabinfo');
		});
		$('#vitalsignsbasedeleteyes').click(function(){
			var datarowid = $('#vitalsignsaddgrid div.gridcontent div.active').attr('id');
			Vitalsignsrecorddelete(datarowid);
		});
		$('#vitalsignsdetailedviewicon').click(function() {
			var datarowid = $('#Vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#vitalsignssectionoverlay').removeClass('closed');
				$('#vitalsignssectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Vitalsignsdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','Vitalsignscreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Vitalsignscreationformadd');
		});
		
		$('#vitalsignsprinticon').click(function() {
			var datarowid = $('#Vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#vitalsignscloneicon').click(function() {
			var datarowid = $('#vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#vitalsignssectionoverlay').removeClass('closed');
				$('#vitalsignssectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Vitalsignsclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('vitalsignsanfieldnameinfo','vitalsignsanfieldnameidinfo','vitalsignsanfieldnamemodinfo','vitalsignsanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#vitalsignssmsicon').click(function() {
			var datarowid = $('#Vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#vitalsignsmailicon').click(function() {
			var datarowid = $('#Vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Vitalsigns','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5001');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#vitalsignsoutgoingcallicon').click(function() {
			var datarowid = $('#vitalsignsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#vitalsignscustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#vitalsignsimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#vitalsignsdatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#vitalsignsfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		generateunitafterload('75%', 'pulse','<span style="color:#9e9e9e;">bpm</span>');
		generateunitafterload('75%', 'respiratoryrate','<span style="color:#9e9e9e;">bpm</span>');
		generateunitafterload('70%', 'bpmode','<span style="color:#9e9e9e;">mmHg</span>');
	    $("#pulse").attr({
	       "max" : 250,
	       "min" : 20
	    });
	    $("#respiratoryrate").attr({
	       "max" : 70,
	       "min" : 1
		});
	    $("#o2saturation").attr({
	       "max" : 100,
	       "min" : 55
		});
	    $("#temperaturereading").attr({
	       "max" : 122,
	       "min" : 30
		});
	}
	{
		$('#vitalsignssavebutton').click(function(){
			$('.ftab').trigger('click');
			$('#vitalsignsformaddwizard').validationEngine('validate');
		});
		$('#vitalsignsformaddwizard').validationEngine({
			onSuccess: function() {
				$('#vitalsignssavebutton').attr('disabled','disabled');
				Vitalsignsnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#vitalsignsdataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#vitalsignsformeditwizard').validationEngine('validate');
		});
		$('#vitalsignsformeditwizard').validationEngine({
			onSuccess: function() {
				$('#vitalsignsdataupdatesubbtn').attr('disabled','disabled');
				Vitalsignsdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		//filter work
		//for toggle
		$('#vitalsignsviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.vitalsignsfilterview').addClass("hidedisplay");
				$('.vitalsignsfullgridview').removeClass("large-9");
				$('.vitalsignsfullgridview').addClass("large-12");
			}else{
				$('.vitalsignsfullgridview').removeClass("large-12");
				$('.vitalsignsfullgridview').addClass("large-9");
				$('.vitalsignsfilterview').removeClass("hidedisplay");
			}			
		});
		$('#vitalsignsclosefiltertoggle').click(function(){
			$('.vitalsignsfilterview').addClass("hidedisplay");
			$('.vitalsignsfullgridview').removeClass("large-9");
			$('.vitalsignsfullgridview').addClass("large-12");
		});
		//filter
		$("#vitalsignsfilterddcondvaluedivhid").hide();
		$("#vitalsignsfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#vitalsignsfiltercondvalue").focusout(function(){
				var value = $("#vitalsignsfiltercondvalue").val();
				$("#vitalsignsfinalfiltercondvalue").val(value);
			});
			$("#vitalsignsfilterddcondvalue").change(function(){
				var value = $("#vitalsignsfilterddcondvalue").val();
				$("#vitalsignsfinalfiltercondvalue").val(value);
			});
		}
		vitalsignsfiltername = [];
		$("#vitalsignsfilteraddcondsubbtn").click(function() {
			$("#vitalsignsfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#vitalsignsfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#vitalsignsfiltercolumn").val();
				var label =$("#vitalsignsfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#vitalsignsfiltercondition").val();
				var value = $("#vitalsignsfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = vitalsignsfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#vitalsignsfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons vitalsignsfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				vitalsignsfiltername.push(data);
				$("#vitalsignsconditionname").val(vitalsignsfiltername);
				vitalsignsaddgrid();
				$(".vitalsignsfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = vitalsignsfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#vitalsignsconditionname").val();
							var filternewcond = nvalue.split(',');
							vitalsignsfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#vitalsignsconditionname").val(vitalsignsfiltername);
							vitalsignsaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#vitalsignsfiltercolumn").select2('val','');//filterddcondvalue
				$("#vitalsignsfiltercondition").select2('val','');
				$("#vitalsignsfilterddcondvalue").select2('val','');
				$("#vitalsignsfiltercondvalue").val('');
				$("#vitalsignsfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	$(".vitalsignsfwgsectionoverlayclose").click(function(){
		$("#vitalsignssectionoverlay").removeClass("effectbox");
		$("#vitalsignssectionoverlay").addClass("closed");
		resetFields();
	});
	
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Vitalsignsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#vitalsignsaddgridwidth").width();
	var wheight = $("#vitalsignsaddgridwidth").height();
	var sortcol = $('.Vitalsignssheadercolsort').hasClass('datasort') ? $('.Vitalsignssheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Vitalsignssheadercolsort').hasClass('datasort') ? $('.Vitalsignssheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Vitalsignssheadercolsort').hasClass('datasort') ? $('.Vitalsignssheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '132';
	}
	userviewid = userviewid!= '' ? '132' : userviewid;
	var viewfieldids =58;
	var Vitalsignscontactid = $("#vitalsignscontactid").val();
	if(Vitalsignscontactid != ''){
		var Allergyfilterid = '2227|Equalto|'+Vitalsignscontactid+'';
	} else {
		var Allergyfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=vitalsigns&primaryid=vitalsignsid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Allergyfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#vitalsignsaddgrid').empty();
			$('#vitalsignsaddgrid').append(data.content);
			$('.vitalsignsgridfootercontainer').empty();
			$('.vitalsignsgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('vitalsignsaddgrid');
			innergridresizeheightset('Vitalsignsaddgrid');
			{//sorting
				$('.Vitalsignssheadercolsort').click(function(){
					$('.Vitalsignssheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Vitalsignsspgnumcnt li .page-text .active').data('rowcount');
					Vitalsignsaddgrid(page,rowcount);
				});
				sortordertypereset('Vitalsignssheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Vitalsignsaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Vitalsignsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			innergridresizeheightset('Vitalsignsaddgrid');
		},
	});
}
function vitalsignsrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Vitalsignsspgnumcnt li .page-text .active').data('rowcount');
		Vitalsignsaddgrid(page,rowcount);
}
function Vitalsignsclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Vitalsignsdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Vitalsignsnewdataaddfun() {
	var amp = '&';
	var formdata = $('#vitalsignsaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Vitalsigns/newdatacreate',
		data: 'datas=' + datainformation,
		type: 'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				resetFields();
				var vcontacid = $("#emrdynamicdddataview").val();
				$("#vitalsignscontactid").val(vcontacid);
				vitalsignsrefreshgrid();
				alertpopup(savealert);
				$('.vitalsignsfwgsectionoverlayclose').trigger('click');
				$('#vitalsignssavebutton').attr('disabled',false);
				addformview = 0;
			}
		},
	})
}
function Vitalsignsdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#vitalsignselementsname').val();
	var elementstable = $('#vitalsignselementstable').val();
	var elementscolmn = $('#vitalsignselementscolmn').val();
	var elementpartable = $('#vitalsignselementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Vitalsigns/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('.vitalsignsfwgsectionoverlayclose').trigger('click');
				$('#vitalsignsdataupdatesubbtn').attr('disabled',false);
				vitalsignsrefreshgrid();
			} else {
				addslideup('Vitalsignscreationview','Vitalsignscreationformadd');
				var txtboxname = elementsname + ',vitalsignsprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dataname = elementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			}
		}
	});
}
function Vitalsignsdataupdatefun() {
	var amp = '&';
	var formdata = $('#vitalsignsaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Vitalsigns/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('.vitalsignsfwgsectionoverlayclose').trigger('click');
				$('#vitalsignsdataupdatesubbtn').attr('disabled',false);
				var vcontacid = $("#emrdynamicdddataview").val();
				$("#vitalsignscontactid").val(vcontacid);
				vitalsignsrefreshgrid();
				alertpopup(savealert);
			}
		},
	});
}
function Vitalsignsrecorddelete(datarowid) {
	var elementstable = $('#vitalsignselementstable').val();
	var elementspartable = $('#vitalsignselementspartabname').val();
	$.ajax({
		url: base_url + 'Vitalsigns/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				vitalsignsrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				vitalsignsrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
//Pricebook icon appending
// pulse pulsedivhid
function generateunitafterload(width,fieldid,addtext){
	$("#"+fieldid+"").css('display','inline-block').css('width',width);
	$("#"+fieldid+"").after(addtext);
}
