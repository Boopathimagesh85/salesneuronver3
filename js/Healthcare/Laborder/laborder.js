$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var fa = document.cookie.split(';');
	for(var i = 0; i <fa.length; i++) {
       var laborderpatientid =  fa[i].split('=');
       if(laborderpatientid[0] == 'patientid'){
    	   $("#labordercontactid").val(laborderpatientid[1]);
       }
    }
	$("#tab8").click(function(){
		Laborderaddgrid();
	});
	{
		var addcloseLabordercreation =['closeaddform','Labordercreationview','Labordercreationformadd'];
		addclose(addcloseLabordercreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('labordersectionoverlay');
		});
	}
	{
		$('#laborderaddicon').click(function(){
			resetFields();
			sectionpanelheight('labordersectionoverlay');
			$('#labordersectionoverlay').removeClass('closed');
			$('#labordersectionoverlay').addClass('effectbox');
			$('#laborderdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#labordersavebutton').show();
			showhideiconsfun('addclick','Labordercreationformadd');
			firstfieldfocus();
			var elementname = $('#laborderelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('laborderanfieldnameinfo','laborderanfieldnameidinfo','laborderanfieldnamemodinfo','laborderanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var lcontacid = $("#emrdynamicdddataview").val();
			$("#labordercontactid").val(lcontacid);
			fortabtouch = 0;
		});
		$('#laborderediticon').click(function(){
			var datarowid = $('#laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('labordersectionoverlay');
				$('#labordersectionoverlay').removeClass('closed');
				$('#labordersectionoverlay').addClass('effectbox');
				$('#laborderdataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#labordersavebutton').hide();
				Laborderdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Labordercreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#laborderdeleteicon').click(function(){
			var datarowid = $('#laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#laborderprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('laborderdeleteyes');
				$("#laborderdeleteyes").click(function(){
					var datarowid =$("#laborderprimarydataid").val();
					Laborderrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#laborderreloadicon').click(function(){
			laborderrefreshgrid();
		});
		$('#laborderformclearicon').click(function(){
			var elementname = $('#laborderelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('laborderanfieldnameinfo','laborderanfieldnameidinfo','laborderanfieldnamemodinfo','laborderanfieldtabinfo');
		});
		$('#laborderbasedeleteyes').click(function(){
			var datarowid = $('#laborderaddgrid div.gridcontent div.active').attr('id');
			Laborderrecorddelete(datarowid);
		});
		$('#laborderdetailedviewicon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#labordersectionoverlay').removeClass('closed');
				$('#labordersectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Laborderdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','Labordercreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#labordereditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Labordercreationformadd');
		});
		
		$('#laborderprinticon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#labordercloneicon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#labordersectionoverlay').removeClass('closed');
				$('#labordersectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Laborderclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('laborderanfieldnameinfo','laborderanfieldnameidinfo','laborderanfieldnamemodinfo','laborderanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#labordersmsicon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#labordermailicon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Laborder','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5011');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#laborderoutgoingcallicon').click(function() {
			var datarowid = $('#Laborderaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#labordercustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#laborderimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#laborderdatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#laborderfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#labordersavebutton').click(function(){
			$('.ftab').trigger('click');
			$('#laborderformaddwizard').validationEngine('validate');
		});
		$('#laborderformaddwizard').validationEngine({
			onSuccess: function() {
				$('#labordersavebutton').attr('disabled','disabled');
				Labordernewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#laborderdataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#laborderformeditwizard').validationEngine('validate');
		});
		$('#laborderformeditwizard').validationEngine({
			onSuccess: function() {
				$('#laborderdataupdatesubbtn').attr('disabled','disabled');
				Laborderdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".laborderfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#labordersectionoverlay").removeClass("effectbox");
			$("#labordersectionoverlay").addClass("closed");
		});
	}
	{
		//filter work
		//for toggle
		$('#laborderviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.laborderfilterview').addClass("hidedisplay");
				$('.laborderfullgridview').removeClass("large-9");
				$('.laborderfullgridview').addClass("large-12");
			}else{
				$('.laborderfullgridview').removeClass("large-12");
				$('.laborderfullgridview').addClass("large-9");
				$('.laborderfilterview').removeClass("hidedisplay");
			}			
		});
		$('#laborderclosefiltertoggle').click(function(){
			$('.laborderfilterview').addClass("hidedisplay");
			$('.laborderfullgridview').removeClass("large-9");
			$('.laborderfullgridview').addClass("large-12");
		});
		//filter
		$("#laborderfilterddcondvaluedivhid").hide();
		$("#laborderfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#laborderfiltercondvalue").focusout(function(){
				var value = $("#laborderfiltercondvalue").val();
				$("#laborderfinalfiltercondvalue").val(value);
			});
			$("#laborderfilterddcondvalue").change(function(){
				var value = $("#laborderfilterddcondvalue").val();
				$("#laborderfinalfiltercondvalue").val(value);
			});
		}
		laborderfiltername = [];
		$("#laborderfilteraddcondsubbtn").click(function() {
			$("#laborderfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#laborderfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#laborderfiltercolumn").val();
				var label =$("#laborderfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#laborderfiltercondition").val();
				var value = $("#laborderfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = laborderfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#laborderfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons laborderfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				laborderfiltername.push(data);
				$("#laborderconditionname").val(laborderfiltername);
				laborderaddgrid();
				$(".laborderfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = laborderfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#laborderconditionname").val();
							var filternewcond = nvalue.split(',');
							laborderfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#laborderconditionname").val(laborderfiltername);
							laborderaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#laborderfiltercolumn").select2('val','');//filterddcondvalue
				$("#laborderfiltercondition").select2('val','');
				$("#laborderfilterddcondvalue").select2('val','');
				$("#laborderfiltercondvalue").val('');
				$("#laborderfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Laborderaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#laborderaddgridwidth").width();
	var wheight = $("#laborderaddgridwidth").height();
	var sortcol = $('.Labordersheadercolsort').hasClass('datasort') ? $('.Labordersheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Labordersheadercolsort').hasClass('datasort') ? $('.Labordersheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Labordersheadercolsort').hasClass('datasort') ? $('.Labordersheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '140';
	}
	userviewid = userviewid!= '' ? '140' : userviewid;
	var viewfieldids = 68;
	var Labordercontactid = $("#labordercontactid").val();
	if(Labordercontactid != ''){
		var Laborderfilterid = '2232|Equalto|'+Labordercontactid+'';
	} else {
		var Laborderfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=laborder&primaryid=laborderid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Laborderfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#laborderaddgrid').empty();
			$('#laborderaddgrid').append(data.content);
			$('.labordergridfootercontainer').empty();
			$('.labordergridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('laborderaddgrid');
			innergridresizeheightset('Laborderaddgrid');
			{//sorting
				$('.Labordersheadercolsort').click(function(){
					$('.Labordersheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Laborderspgnumcnt li .page-text .active').data('rowcount');
					Laborderaddgrid(page,rowcount);
				});
				sortordertypereset('Labordersheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Laborderaddgrid(page,rowcount);
				});
			}
			innergridresizeheightset('Laborderaddgrid');
		},
	});
}
function laborderrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Laborderspgnumcnt li .page-text .active').data('rowcount');
		Laborderaddgrid(page,rowcount);
}
function Laborderclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Laborderdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Labordernewdataaddfun() {
	var amp = '&';
	var formdata = $('#laborderaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Laborder/newdatacreate',
			data: 'datas=' + datainformation,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					laborderrefreshgrid();
					alertpopup(savealert);
					resetFields();
					$('#labordersavebutton').attr('disabled',false);
					$('.laborderfwgsectionoverlayclose').trigger('click');
					addformview = 0;
					var lcontacid = $("#emrdynamicdddataview").val();
					$("#labordercontactid").val(lcontacid);
				}
			},
	})
}
function Laborderdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#laborderelementsname').val();
	var elementstable = $('#laborderelementstable').val();
	var elementscolmn = $('#laborderelementscolmn').val();
	var elementpartable = $('#laborderelementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Laborder/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('.laborderfwgsectionoverlayclose').trigger('click');
				laborderrefreshgrid();
				$('#laborderdataupdatesubbtn').attr('disabled',false);
			} else {
				var txtboxname = elementsname + ',laborderprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				$("#laborderprimarydataid").val(datarowid);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			}
		}
	});
}
function Laborderdataupdatefun() {
	var amp = '&';
	var formdata = $('#laborderaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Laborder/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.laborderfwgsectionoverlayclose').trigger('click');
				var lcontacid = $("#emrdynamicdddataview").val();
				$("#labordercontactid").val(lcontacid);
				laborderrefreshgrid();
				$('#laborderdataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function Laborderrecorddelete(datarowid) {
	var elementstable = $('#laborderelementstable').val();
	var elementspartable = $('#laborderelementspartabname').val();
	$.ajax({
		url: base_url + 'Laborder/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				laborderrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				laborderrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
