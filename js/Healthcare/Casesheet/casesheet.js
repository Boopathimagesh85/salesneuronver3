$(document).ready(function(){
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var casesheetpatientid =  ca[i].split('=');
       if(casesheetpatientid[0] == 'patientid'){
    	   $("#casesheetcontactid").val(casesheetpatientid[1]);
       }
    }
	$("#tab4").click(function(){
		Casesheetaddgrid();
	});
	{
		var addcloseCasesheetcreation =['closeaddform','Casesheetcreationview','Casesheetcreationformadd'];
		addclose(addcloseCasesheetcreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$(window).resize(function() {
			sectionpanelheight('casesheetsectionoverlay');
		});
	}
	{
		$('#casesheetaddicon').click(function(){
			addslideup('casesheetcreationview','casesheetcreationformadd');
			resetFields();
			sectionpanelheight('casesheetsectionoverlay');
			$('#casesheetsectionoverlay').removeClass('closed');
			$('#casesheetsectionoverlay').addClass('effectbox');
			$('#casesheetdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#casesheetsavebutton').show();
			showhideiconsfun('addclick','Casesheetcreationformadd');
			firstfieldfocus();
			var elementname = $('#casesheetelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('casesheetanfieldnameinfo','casesheetanfieldnameidinfo','casesheetanfieldnamemodinfo','casesheetanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			$("#recordedbyid").select2('val',loggedinuser);
			$("#locationid").select2('val',loggedinbranchid);
			var ccontacid = $("#emrdynamicdddataview").val();
			$("#casesheetcontactid").val(ccontacid);
			fortabtouch = 0;
		});
		$('#casesheetediticon').click(function(){
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('casesheetsectionoverlay');
				$('#casesheetsectionoverlay').removeClass('closed');
				$('#casesheetsectionoverlay').addClass('effectbox');
				addslideup('casesheetcreationview','casesheetcreationformadd');
				$('#casesheetdataupdatesubbtn').show().removeClass('hidedisplayfwg');
				$('#casesheetsavebutton').hide();
				Casesheetdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','casesheetcreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheetdeleteicon').click(function(){
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#casesheetprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('casesheetdeleteyes');
				$("#casesheetdeleteyes").click(function(){
					var datarowid =$("#casesheetprimarydataid").val();
					Casesheetrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheetreloadicon').click(function(){
			casesheetrefreshgrid();
		});
		$('#casesheetformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('casesheetanfieldnameinfo','casesheetanfieldnameidinfo','casesheetanfieldnamemodinfo','casesheetanfieldtabinfo');
		});
		$('#casesheetbasedeleteyes').click(function(){
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			casesheetrecorddelete(datarowid);
		});
		$('#casesheetdetailedviewicon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#casesheetsectionoverlay').removeClass('closed');
				$('#casesheetsectionoverlay').addClass('effectbox');
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Casesheetdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','casesheetcreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheeteditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','casesheetcreationformadd');
		});
		
		$('#casesheetprinticon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheetcloneicon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#casesheetsectionoverlay').removeClass('closed');
				$('#casesheetsectionoverlay').addClass('effectbox');
				$('#processoverlay').show();
				Casesheetclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('casesheetanfieldnameinfo','casesheetanfieldnameidinfo','casesheetanfieldnamemodinfo','casesheetanfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#casesheetsmsicon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheetmailicon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Casesheet','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5003');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#casesheetoutgoingcallicon').click(function() {
			var datarowid = $('#casesheetaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#casesheetcustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#casesheetimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#casesheetdatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#casesheetfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#casesheetsavebutton').click(function(){
			$('.ftab').trigger('click');
			$('#casesheetformaddwizard').validationEngine('validate');
		});
		$('#casesheetformaddwizard').validationEngine({
			onSuccess: function() {
				$('#casesheetsavebutton').attr('disabled','disabled');
				Casesheetnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#casesheetdataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#casesheetformeditwizard').validationEngine('validate');
		});
		$('#casesheetformeditwizard').validationEngine({
			onSuccess: function() {
				$('#casesheetdataupdatesubbtn').attr('disabled','disabled');
				Casesheetdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		{//Addform section overlay close
			$(".casesheetfwgsectionoverlayclose").click(function(){
				resetFields();
				$("#casesheetsectionoverlay").removeClass("effectbox");
				$("#casesheetsectionoverlay").addClass("closed");
			});
		}
	}
	{
		//filter work
		//for toggle
		$('#casesheetviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.casesheetfilterview').addClass("hidedisplay");
				$('.casesheetfullgridview').removeClass("large-9");
				$('.casesheetfullgridview').addClass("large-12");
			}else{
				$('.casesheetfullgridview').removeClass("large-12");
				$('.casesheetfullgridview').addClass("large-9");
				$('.casesheetfilterview').removeClass("hidedisplay");
			}			
		});
		$('#casesheetclosefiltertoggle').click(function(){
			$('.casesheetfilterview').addClass("hidedisplay");
			$('.casesheetfullgridview').removeClass("large-9");
			$('.casesheetfullgridview').addClass("large-12");
		});
		//filter
		$("#casesheetfilterddcondvaluedivhid").hide();
		$("#casesheetfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#casesheetfiltercondvalue").focusout(function(){
				var value = $("#casesheetfiltercondvalue").val();
				$("#casesheetfinalfiltercondvalue").val(value);
			});
			$("#casesheetfilterddcondvalue").change(function(){
				var value = $("#casesheetfilterddcondvalue").val();
				$("#casesheetfinalfiltercondvalue").val(value);
			});
		}
		casesheetfiltername = [];
		$("#casesheetfilteraddcondsubbtn").click(function() {
			$("#casesheetfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#casesheetfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#casesheetfiltercolumn").val();
				var label =$("#casesheetfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#casesheetfiltercondition").val();
				var value = $("#casesheetfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = casesheetfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#casesheetfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons casesheetfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				casesheetfiltername.push(data);
				$("#casesheetconditionname").val(casesheetfiltername);
				casesheetaddgrid();
				$(".casesheetfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = casesheetfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#casesheetconditionname").val();
							var filternewcond = nvalue.split(',');
							casesheetfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#casesheetconditionname").val(casesheetfiltername);
							casesheetaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#casesheetfiltercolumn").select2('val','');
				$("#casesheetfiltercondition").select2('val','');
				$("#casesheetfilterddcondvalue").select2('val','');
				$("#casesheetfiltercondvalue").val('');
				$("#casesheetfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Casesheetaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#casesheetaddgridwidth").width();
	var wheight = $("#casesheetaddgridwidth").height();
	var sortcol = $('.Casesheetsheadercolsort').hasClass('datasort') ? $('.Casesheetsheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Casesheetsheadercolsort').hasClass('datasort') ? $('.Casesheetsheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Casesheetsheadercolsort').hasClass('datasort') ? $('.Casesheetsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '134';
	}
	userviewid = userviewid!= '' ? '134' : userviewid;
	var viewfieldids = 60;
	var Casesheetcontactid = $("#emrdynamicdddataview").val();
	if(Casesheetcontactid != ''){
		var Casesheetfilterid = '2229|Equalto|'+Casesheetcontactid+'';
	} else {
		var Casesheetfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=casesheet&primaryid=casesheetid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+Casesheetfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#casesheetaddgrid').empty();
			$('#casesheetaddgrid').append(data.content);
			$('.casesheetgridfootercontainer').empty();
			$('.casesheetgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('casesheetaddgrid');
			innergridresizeheightset('Casesheetaddgrid');
			{//sorting
				$('.Casesheetsheadercolsort').click(function(){
					$('.Casesheetsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Casesheetspgnumcnt li .page-text .active').data('rowcount');
					Casesheetaddgrid(page,rowcount);
				});
				sortordertypereset('Casesheetsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Casesheetaddgrid(page,rowcount);
				});
			}
			innergridresizeheightset('Casesheetaddgrid');
		},
	});
}
function casesheetrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Casesheetspgnumcnt li .page-text .active').data('rowcount');
		Casesheetaddgrid(page,rowcount);
}
function Casesheetclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Casesheetdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Casesheetnewdataaddfun() {
	var amp = '&';
	var formdata = $('#casesheetaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + 'Casesheet/newdatacreate',
		data: 'datas=' + datainformation,
		type: 'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE'){
				var fcontacid = $("#emrdynamicdddataview").val();
				$("#casesheetcontactid").val(fcontacid);
				casesheetrefreshgrid();
				alertpopup(savealert);
				resetFields();
				$('#casesheetsavebutton').attr('disabled',false);
				$('.casesheetfwgsectionoverlayclose').trigger('click');
				ddformview = 0;
			}
		},
	})
}
function Casesheetdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#casesheetelementsname').val();
	var elementstable = $('#casesheetelementstable').val();
	var elementscolmn = $('#casesheetelementscolmn').val();
	var elementpartable = $('#casesheetelementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Casesheet/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('.casesheetfwgsectionoverlayclose').trigger('click');
				casesheetrefreshgrid();
				$('#casesheetdataupdatesubbtn').attr('disabled',false);
			} else {
				var txtboxname = elementsname + ',casesheetprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				$("#casesheetprimarydataid").val(datarowid);
				$('#processoverlay').hide();
				$("#casesheetsectionoverlay").removeClass("closed");
				$("#casesheetsectionoverlay").addClass("effectbox");
				$('.updatebtnclass').removeClass('hidedisplay');
				$('.addbtnclass').addClass('hidedisplay');
			}
		}
	});
}
function Casesheetdataupdatefun() {
	var amp = '&';
	var formdata = $('#casesheetaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+'Casesheet/datainformationupdate',
		data: 'datas=' + datainformation,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('.casesheetfwgsectionoverlayclose').trigger('click');
				var fcontacid = $("#emrdynamicdddataview").val();
				$("#casesheetcontactid").val(fcontacid);
				casesheetrefreshgrid();
				$('#casesheetdataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Casesheetrecorddelete(datarowid) {
	var elementstable = $('#casesheetelementstable').val();
	var elementspartable = $('#casesheetelementspartabname').val();
	$.ajax({
		url: base_url + 'Casesheet/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				casesheetrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				casesheetrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
