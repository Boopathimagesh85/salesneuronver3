$(document).ready(function(){
	METHOD ='ADD';
	UPDATEID = 0;
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var treatmentpatientid =  ca[i].split('=');
       if(treatmentpatientid[0] == 'patientid'){
    	   $("#treatmentcontactid").val(treatmentpatientid[1]);
       }
    }
	{
		$("#treatmenttreingrideditspan1").show();
	}
	$("#tab9").click(function(){
		treatmentaddgrid();
		Treatmenttreaddgrid1();
	});
	{
		var addcloseTreatmentcreation =['closeaddform','Treatmentcreationview','Treatmentcreationformadd'];
		addclose(addcloseTreatmentcreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$('#treatmentaddicon').click(function(){
			resetFields();
			$('#treatmentsectionoverlay').removeClass('closed');
			$('#treatmentsectionoverlay').addClass('effectbox');
			$('.addbtnclass').removeClass('hidedisplay')
			$('.updatebtnclass').addClass('hidedisplay')
			showhideiconsfun('addclick','Treatmentcreationformadd');
			var elementname = $('#treatmentelementsname').val();
			elementdefvalueset(elementname);
			cleargriddata('treatmenttreaddgrid1');
			randomnumbergenerate('treatmentanfieldnameinfo','treatmentanfieldnameidinfo','treatmentanfieldnamemodinfo','treatmentanfieldtabinfo');
			var tcontacid = $("#emrdynamicdddataview").val();
			$('#discount').removeClass('validate[required,custom[number],min[0],max[100],decval[2]]');
			$('#discount').addClass('validate[required,custom[number],decval[2]]');
			$("#contactid").val(tcontacid);
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
		});
		$('#treatmentediticon').click(function(){
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay')
				Treatmentdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','Treatmentcreationformadd');
				$('#discount').removeClass('validate[required,custom[number],min[0],max[100],decval[2]]');
				$('#discount').addClass('validate[required,custom[number],decval[2]]');
				fortabtouch = 1;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#treatmentdeleteicon').click(function(){
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#treatmentprimarydataid").val(datarowid);
				combainedmoduledeletealert('treatmentdeleteyes');
				$("#treatmentdeleteyes").click(function(){
					var datarowid =$("#treatmentprimarydataid").val();
					Treatmentrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#treatmentreloadicon').click(function(){
			refreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			Treatmentrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Treatmentdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','Treatmentcreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
			showhideiconsfun('editfromsummryclick','Treatmentcreationformadd');
		});
		
		$('#treatmentprinticon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#treatmentcloneicon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				Treatmentclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#smsicon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#mailicon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Treatment','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5013');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#outgoingcallicon').click(function() {
			var datarowid = $('#treatmentaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#customizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#importicon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#datamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	//validation for  Add
	$('#treatmentdataaddsbtn').mousedown(function(e) {
		if(e.which == 1 || e.which === undefined) {
			masterfortouch = 0;
			$('#productid').removeClass('validate[required]');
			$('#treatmentmodel').removeClass('validate[required]');
			$('#treatmentquantity').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#discount').removeClass('validate[required,custom[number],decval[2]]');
			$('#cost').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$("#treatmentformaddwizard").validationEngine('validate');
		}
	});
	jQuery("#treatmentformaddwizard").validationEngine({
		onSuccess: function() {
			$('#treatmentdataaddsbtn').attr('disabled','disabled');
			treatmentaddformdata();
		},
		onFailure: function() {
			$('#productid').addClass('validate[required]');
			$('#treatmentmodel').addClass('validate[required]');
			$('#treatmentquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#discount').addClass('validate[required,custom[number],decval[2]]');
			$('#cost').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//for update
	$('#treatmentdataupdatesubbtn').click(function(){
		$('#productid').removeClass('validate[required]');
		$('#treatmentmodel').removeClass('validate[required]');
		$('#treatmentquantity').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
		$('#discount').removeClass('validate[required,custom[number],decval[2]]');
		$('#cost').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
		$('#netamount').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
		$('#treatmentformeditwizard').validationEngine('validate');
	});
	$('#treatmentformeditwizard').validationEngine({
		onSuccess: function() {
			$('#treatmentdataupdatesubbtn').attr('disabled','disabled');
			Treatmentdataupdatefun();
		},
		onFailure: function() {
			$('#productid').addClass('validate[required]');
			$('#treatmentmodel').addClass('validate[required]');
			$('#treatmentquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#discount').addClass('validate[required,custom[number],decval[2]]');
			$('#cost').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
			$('#tabgropdropdown').select2('val','1');
			alertpopup(validationalert);
		}
	});
{
	treatmentdetaildelete = [];
	$('#treatmenttreingriddel1').click(function(){
		var datarowid = $('#treatmenttreaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {
			if(datarowid != '') {
				treatmentdetaildelete.push(datarowid);
			}
			deletegriddatarow('treatmenttreaddgrid1',datarowid);
			//summary calculate for treatmentquantity/cost/discount/netamount
			var summaryquantity =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','quantity','sum'));
			$('#summaryquantity').val('');
			$('#summaryquantity').val(summaryquantity);
			var summarycost =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','cost','sum'));
			$('#summarycost').val('');
			$('#summarycost').val(summarycost);
	        var summarydiscount =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','discount','sum'));
	        $('#summarydiscount').val('');
	        $('#summarydiscount').val(summarydiscount);
	        var summarynetamount =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','netamount','sum'));
	        $('#summarynetamount').val('');
	        $('#summarynetamount').val(summarynetamount);
		} else {
			alertpopup('Please select a row');
		}
	});
}
	{// Dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#treatmentgridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('#treatmenttreaddbutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');			
			griddataid = i;
			gridynamicname = gridname;
			$("#treatmentaddgrid1validation").validationEngine('validate');
			METHOD = 'ADD';
		});
		$('#treatmenttreupdatebutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');			
			griddataid = i;
			gridynamicname = gridname;
			$("#treatmentaddgrid1validation").validationEngine('validate');
		});
	}
	$("#treatmentaddgrid1validation").validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#treatmentgridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#treatmentgridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				formtogriddata(gridname,METHOD,'',UPDATEID);
			}
			//summary calculate for treatmentquantity/cost/discount/netamount
			var summaryquantity =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','quantity','sum'));
			$('#summaryquantity').val('');
			$('#summaryquantity').val(summaryquantity);
			var summarycost =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','cost','sum'));
			$('#summarycost').val('');
			$('#summarycost').val(summarycost);
	        var summarydiscount =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','discount','sum'));
	        $('#summarydiscount').val('');
	        $('#summarydiscount').val(summarydiscount);
	        var summarynetamount =  parseFloat(getgridcolvalue('treatmenttreaddgrid1','','netamount','sum'));
	        $('#summarynetamount').val('');
	        $('#summarynetamount').val(summarynetamount);
	       /* Data row select event */
			datarowselectevt();
			$("#treatmentquantity").val('');
			$("#cost").val('');
			$("#discount").val('');
			$("#netamount").val('');
			$("#treatmentcomments").text('');
			$("#treatmentcomments").val('');
			$("#productid").select2('val','');
			griddataid = 0;
			gridynamicname = '';
			if(METHOD == 'UPDATE') {
				$('#treatmenttreupdatebutton').addClass('hidedisplayfwg');
				$('#treatmenttreupdatebutton').addClass('hidedisplay');//display the UPDATE button(inner - productgrid)
				$('#treatmenttreaddbutton').removeClass('hidedisplayfwg');
				$('#treatmenttreaddbutton').removeClass('hidedisplay');//display the ADD button(inner-productgrid) */
			}
			METHOD = 'ADD'; //reset
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});
	$("#summarydiscount").keyup(function(){
		var discount = $("#summarydiscount").val();
		var netamount = $("#summarynetamount").val();
		var fnetamount = parseFloat(netamount)-parseFloat(discount);
		$("#summarynetamount").val(fnetamount);
		Materialize.updateTextFields();
	});
	{//Addform section overlay close
		$(".treatmentfwgsectionoverlayclose").click(function(){
			resetFields();
			$("#treatmentsectionoverlay").removeClass("effectbox");
			$("#treatmentsectionoverlay").addClass("closed");
		});
	}
	{
		//filter work
		//for toggle
		$('#treatmentviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.treatmentfilterview').addClass("hidedisplay");
				$('.treatmentfullgridview').removeClass("large-9");
				$('.treatmentfullgridview').addClass("large-12");
			}else{
				$('.treatmentfullgridview').removeClass("large-12");
				$('.treatmentfullgridview').addClass("large-9");
				$('.treatmentfilterview').removeClass("hidedisplay");
			}			
		});
		$('#treatmentclosefiltertoggle').click(function(){
			$('.treatmentfilterview').addClass("hidedisplay");
			$('.treatmentfullgridview').removeClass("large-9");
			$('.treatmentfullgridview').addClass("large-12");
		});
		//filter
		$("#treatmentfilterddcondvaluedivhid").hide();
		$("#treatmentfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#treatmentfiltercondvalue").focusout(function(){
				var value = $("#treatmentfiltercondvalue").val();
				$("#treatmentfinalfiltercondvalue").val(value);
			});
			$("#treatmentfilterddcondvalue").change(function(){
				var value = $("#treatmentfilterddcondvalue").val();
				$("#treatmentfinalfiltercondvalue").val(value);
			});
		}
		treatmentfiltername = [];
		$("#treatmentfilteraddcondsubbtn").click(function() {
			$("#treatmentfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#treatmentfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#treatmentfiltercolumn").val();
				var label =$("#treatmentfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#treatmentfiltercondition").val();
				var value = $("#treatmentfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = treatmentfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#treatmentfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons treatmentfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				treatmentfiltername.push(data);
				$("#treatmentconditionname").val(treatmentfiltername);
				treatmentaddgrid();
				$(".treatmentfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = treatmentfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#treatmentconditionname").val();
							var filternewcond = nvalue.split(',');
							treatmentfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#treatmentconditionname").val(treatmentfiltername);
							treatmentaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#treatmentfiltercolumn").select2('val','');//filterddcondvalue
				$("#treatmentfiltercondition").select2('val','');
				$("#treatmentfilterddcondvalue").select2('val','');
				$("#treatmentfiltercondvalue").val('');
				$("#treatmentfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	$("#treatmentquantity,#cost").focusout(function(){
		if($(this).val() != 0) {
			netamountcalculation();
		}
	});
	$("#discount").focusout(function(){
		var discount = $("#discount").val();
		var netamount = $("#netamount").val();
		if(parseFloat(discount) < parseFloat(netamount)) {
			netamountcalculation();
		} else {
			 $("#discount").val(0);
			alertpopup('Discount value should be lessthan the netamount');
		}
	});
	{
		$("#treatmenttreingridedit1").click(function() {
			var selectedrow = $('#treatmenttreaddgrid1 div.gridcontent div.active').attr('id');
			if(selectedrow) {
				var treatmentdate = $("#treatmentmodel").val();
				var contactid = $("#contactid").val();
				gridtoformdataset('treatmenttreaddgrid1',selectedrow);
				$("#treatmentmodel").val(treatmentdate);
				$("#contactid").val(contactid);
				Materialize.updateTextFields();
				$('#treatmenttreupdatebutton').removeClass('hidedisplayfwg');
				$('#treatmenttreupdatebutton').removeClass('hidedisplay');//display the UPDATE button(inner - productgrid)
				$('#treatmenttreaddbutton').addClass('hidedisplayfwg');
				$('#treatmenttreaddbutton').addClass('hidedisplay');//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
});
function netamountcalculation(){
	var treatmentquantity =  $("#treatmentquantity").val();
	if(checkVariable('treatmentquantity') != true){
		var treatmentquantity = 1 ;
	}
	var cost =  $("#cost").val();
	if(checkVariable('cost') != true){
		var cost = 0 ;
	}
	var discount =  $("#discount").val();
	if(checkVariable('discount') != true){
		var discount = 0 ;
	}
	var totalamount = parseFloat(treatmentquantity)*parseFloat(cost);
	var netamount = parseFloat(totalamount)-parseFloat(discount);
	$("#netamount").val(netamount);
	Materialize.updateTextFields();
}
//Form to grid base function //Used on the new add's/edit's
function gridformtogridvalidatefunction(gridnamevalue) { 
	//Function call for validate
	$("#treatmentaddgrid1validation").validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
				formtogriddata(gridname,METHOD,'',selectedrow);
			}
			/* Data row select event */
			datarowselectevt();
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
			METHOD = 'ADD'; //reset
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});
}

function treatmentaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#treatmentaddgridwidth").width();
	var wheight = $("#treatmentaddgridwidth").height();
	var sortcol = $('.Treatmentsheadercolsort').hasClass('datasort') ? $('.Treatmentsheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Treatmentsheadercolsort').hasClass('datasort') ? $('.Treatmentsheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Treatmentsheadercolsort').hasClass('datasort') ? $('.Treatmentsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '142';
	}
	userviewid = userviewid!= '' ? '142' : userviewid;
	var viewfieldids = '70';
	var treatmentcontactid = $('#contactid').val();
	if(treatmentcontactid != ''){
		var treatmentfilterid = '2234|Equalto|'+treatmentcontactid+'';
	} else {
		var treatmentfilterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=treatment&primaryid=treatmentid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+treatmentfilterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#treatmentaddgrid').empty();
			$('#treatmentaddgrid').append(data.content);
			$('.treatmentgridfootercontainer').empty();
			$('.treatmentgridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('treatmentaddgrid');
			{//sorting
				$('.Treatmentsheadercolsort').click(function(){
					$('.Treatmentsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Treatmentspgnumcnt li .page-text .active').data('rowcount');
					treatmentaddgrid(page,rowcount);
				});
				sortordertypereset('Treatmentsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					treatmentaddgrid(page,rowcount);
				});
			}
		},
	});
}
function Treatmenttreaddgrid1() {
	var wwidth = $('#treatmenttreaddgrid1').width();
	var wheight = $('#treatmenttreaddgrid1').height();
	$.ajax({
		url:base_url+'Base/localgirdheaderinformationfetch?tabgroupid=178&moduleid=70&width='+wwidth+'&height='+wheight+'&modulename=Treatmenttreaddgrid1',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#treatmenttreaddgrid1').empty();
			$('#treatmenttreaddgrid1').append(data.content);
			$('#treatmenttreaddgrid1footer').empty();
			$('#treatmenttreaddgrid1footer').append(data.footer);
			datarowselectevt();
			columnresize('treatmenttreaddgrid1');
			var hideprodgridcol = ['treatmentmodel','contactid','treatmenttreatmentdetailsid'];
			gridfieldhide('treatmenttreaddgrid1',hideprodgridcol);
		},
	});
}
function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Treatmentspgnumcnt li .page-text .active').data('rowcount');
		treatmentaddgrid(page,rowcount);
}
function Treatmentclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Treatmentdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function treatmentaddformdata() {
	var amp = '&';
	var addgriddata='';
	var gridname = 'treatmenttreaddgrid1';
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#treatmentelementsname').val();
	var elementstable = $('#treatmentelementstable').val();
	var elementscolmn = $('#treatmentelementscolmn').val();
	var elementspartabname = $('#treatmentelementspartabname').val();
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var resctable = 'treatmenttreatmentdetails';
	var editordata = '';
	var neweditordata = '';
	var editorname = '';
	if(editorname != '') {
		var editornames  = [];
		editornames = editorname.split(',');
		$.each( editornames, function( key, name ) {
			editordata = $('#'+name+'').editable('getHTML');
			var regExp = /&nbsp;/g;
			var datacontent = editordata.match(regExp);
			if(datacontent !== null){
				var gllen = datacontent.length;
				for (var i = 0; i < gllen; i++) {
					editordata = editordata.replace(''+datacontent[i]+'','a10s');
				}
			}
			var fname = [];
			fname = name.split('_');
			neweditordata += amp+fname[0]+'_editorfilename='+editordata;
			$('#'+fname[0]+'_editorfilename').val(editordata);
		});
	}
	var formdata = $('#treatmentaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Treatment/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$(".treatmentfwgsectionoverlayclose").trigger("click");
				$('#productid').addClass('validate[required]');
				$('#treatmentmodel').addClass('validate[required]');
				$('#treatmentquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#discount').addClass('validate[required,custom[number],decval[2]]');
				$('#cost').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				resetFields();
				var tcontacid = $("#emrdynamicdddataview").val();
				$("#contactid").val(tcontacid);
				cleargriddata('treatmenttreaddgrid1');
				treatmentrefreshgrid();
				$('#treatmentdataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
				
			}
		},
	});
}
{//refresh grid
	function treatmentrefreshgrid() {
		var page = $('ul#treatmentpgnum li.active').data('pagenum');
		var rowcount = $('ul#treatmentpgnumcnt li .page-text .active').data('rowcount');
		treatmentaddgrid(page,rowcount);
	}
}
function Treatmentdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#treatmentelementsname').val();
	var elementstable = $('#treatmentelementstable').val();
	var elementscolmn = $('#treatmentelementscolmn').val();
	var elementpartable = $('#treatmentelementspartabname').val();
	var resctable = $('#treatmentresctable').val();
	$.ajax({
		url:base_url+'Treatment/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$(".treatmentfwgsectionoverlayclose").trigger("click");
				treatmentrefreshgrid();
				$('#treatmentdataupdatesubbtn').attr('disabled',false);
			} else {
				var contactid = data.contactid;
				var treatmentmodel = data.treatmentmodel;
				$("#treatmentprimarydataid").val(datarowid);
				$("#contactid").val(contactid);
				$("#treatmentmodel").val(treatmentmodel);
				$("#summaryquantity").val(data.summaryquantity);
				$("#summarycost").val(data.summarycost);
				$("#summarydiscount").val(data.summarydiscount);
				$("#summarynetamount").val(data.summarynetamount);
				//tratment details fetch
				treatmentdetail(datarowid);
				$("#treatmentsectionoverlay").removeClass("closed");
				$("#treatmentsectionoverlay").addClass("effectbox");
				$('#processoverlay').hide();
			}
		}
	});
}
function treatmentdetail(datarowid) {
	if(datarowid!='') {
		$.ajax({
			url:base_url+'Treatment/treatmentdetailfetch?primarydataid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('treatmenttreaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('treatmenttreaddgrid1');
					var hideprodgridcol = ['treatmentmodel','contactid','treatmenttreatmentdetailsid'];
					gridfieldhide('treatmenttreaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
function Treatmentdataupdatefun() {
	var amp = '&';
	var gridname = $('#treatmentgridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editordata = '';
		var neweditordata = '';
		var editorname = '';
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(',');
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').editable('getHTML');
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){
					var glent = datacontent.length;
					for (var i = 0; i < glent; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				editordata += '&'+fname[0]+'_editorfilename='+editordata;
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
		}
		var resctable = $('#treatmentresctable').val();
	}
	var formdata = $('#treatmentaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+"&treatmentdetaildelete="+treatmentdetaildelete;
	} else {
		dataset = 'datas=' + datainformation+"&treatmentdetaildelete="+treatmentdetaildelete;
	}
	$.ajax({
		url: base_url+'Treatment/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$(".treatmentfwgsectionoverlayclose").trigger("click");
				$('#productid').addClass('validate[required]');
				$('#treatmentmodel').addClass('validate[required]');
				$('#treatmentquantity').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#discount').addClass('validate[required,custom[number],decval[2]]');
				$('#cost').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[2],maxSize[100]]');
				resetFields();
				var tcontacid = $("#emrdynamicdddataview").val();
				$("#contactid").val(tcontacid);
				treatmentdetaildelete = [];
				cleargriddata('treatmenttreaddgrid1');
				treatmentrefreshgrid();
				$('#treatmentdataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Treatmentrecorddelete(datarowid) {
	var elementstable = $('#treatmentelementstable').val();
	var elementspartable = $('#treatmentelementspartabname').val();
	$.ajax({
		url: base_url + 'Treatment/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				treatmentrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				treatmentrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}