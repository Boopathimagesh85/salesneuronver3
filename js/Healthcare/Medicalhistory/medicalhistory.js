$(document).ready(function(){
	METHOD ='ADD';
	$(document).foundation();
	maindivwidth();
	var ca = document.cookie.split(';');
	for(var i = 0; i <ca.length; i++) {
       var medicalpatientid =  ca[i].split('=');
       if(medicalpatientid[0] == 'patientid'){
    	   $("#medicalhistorycontactid").val(medicalpatientid[1]);
       }
    }
	{
		Medicalhistoryaddgrid();
	}
	{
		var addcloseMedicalhistorycreation =['closeaddform','Medicalhistorycreationview','Medicalhistorycreationformadd'];
		addclose(addcloseMedicalhistorycreation);
	}
	{
		fortabtouch = 0;
	}
	{
		$('#medicalhistoryaddicon').click(function(){
			$("#medicalhistorysectionoverlay").removeClass("closed");
			$("#medicalhistorysectionoverlay").addClass("effectbox");
			resetFields();
			$('.addbtnclass').removeClass('hidedisplay');
			$('.updatebtnclass').addClass('hidedisplay');
			showhideiconsfun('addclick','Medicalhistorycreationformadd');
			firstfieldfocus();
			var elementname = $('#medicalhistoryelementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('medicalhistoryanfieldnameinfo','medicalhistoryanfieldnameidinfo','medicalhistoryanfieldnamemodinfo','medicalhistoryanfieldtabinfo');
			$('#tabgropdropdown').select2('val','1');
			var mcontacid = $("#emrdynamicdddataview").val();
			$("#medicalhistorycontactid").val(mcontacid);
			fortabtouch = 0;
		});
		$('#medicalhistoryediticon').click(function(){
			var datarowid = $('#medicalhistoryaddgrid div.grid-view div.gridcontent div.data-content div.active').attr('id');
			if (datarowid) {
				Medicalhistorydataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','medicalhistorycreationformadd');
				fortabtouch = 1;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistorydeleteicon').click(function(){
			var datarowid = $('#medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
				clearform('cleardataform');
				$("#medicalhistoryprimarydataid").val(datarowid);
				$(".addbtnclass").removeClass('hidedisplay');
				$(".updatebtnclass").addClass('hidedisplay');			
				combainedmoduledeletealert('medicalhistorydeleteyes');
				$("#medicalhistorydeleteyes").click(function(){
					var datarowid =$("#medicalhistoryprimarydataid").val();
					Medicalhistoryrecorddelete(datarowid);
					$(this).unbind();
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistoryreloadicon').click(function(){
			medicalhistoryrefreshgrid();
		});
		$('#medicalhistoryformclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('medicalhistoryanfieldnameinfo','medicalhistoryanfieldnameidinfo','medicalhistoryanfieldnamemodinfo','medicalhistoryanfieldtabinfo');
		});
		$('#medicalhistorybasedeleteyes').click(function(){
			var datarowid = $('#medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			Medicalhistoryrecorddelete(datarowid);
		});
		$('#medicalhistorydetailedviewicon').click(function() {
			var datarowid = $('#medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				Medicalhistorydataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','medicalhistorycreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistoryeditfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','Medicalhistorycreationformadd');
		});
		
		$('#medicalhistoryprinticon').click(function() {
			var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistorycloneicon').click(function() {
			var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				Medicalhistoryclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#medicalhistorysmsicon').click(function() {
			var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistorymailicon').click(function() {
			var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'Medicalhistory','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5005');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#medicalhistoryoutgoingcallicon').click(function() {
			var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#medicalhistorycustomizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#medicalhistoryimporticon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#medicalhistorydatamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#medicalhistoryfindduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{//validation for  Add  
		$('#medicalhistorydataaddsbtn').mousedown(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#medicalhistoryformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#medicalhistoryformaddwizard").validationEngine({
			onSuccess: function() {
				$('#medicalhistorydataaddsbtn').attr('disabled','disabled');
				Medicalhistorynewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$('#medicalhistorydataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#medicalhistoryformeditwizard').validationEngine('validate');
		});
		$('#medicalhistoryformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				Medicalhistorydataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		//filter work
		//for toggle
		$('#medicalhistoryviewtoggle').click(function(){
			if ($(".filterview").is(":visible")) {
				$('.medicalhistoryfilterview').addClass("hidedisplay");
				$('.medicalhistoryfullgridview').removeClass("large-9");
				$('.medicalhistoryfullgridview').addClass("large-12");
			}else{
				$('.medicalhistoryfullgridview').removeClass("large-12");
				$('.medicalhistoryfullgridview').addClass("large-9");
				$('.medicalhistoryfilterview').removeClass("hidedisplay");
			}			
		});
		$('#medicalhistoryclosefiltertoggle').click(function(){
			$('.medicalhistoryfilterview').addClass("hidedisplay");
			$('.medicalhistoryfullgridview').removeClass("large-9");
			$('.medicalhistoryfullgridview').addClass("large-12");
		});
		//filter
		$("#medicalhistoryfilterddcondvaluedivhid").hide();
		$("#medicalhistoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#medicalhistoryfiltercondvalue").focusout(function(){
				var value = $("#medicalhistoryfiltercondvalue").val();
				$("#medicalhistoryfinalfiltercondvalue").val(value);
			});
			$("#medicalhistoryfilterddcondvalue").change(function(){
				var value = $("#medicalhistoryfilterddcondvalue").val();
				$("#medicalhistoryfinalfiltercondvalue").val(value);
			});
		}
		medicalhistoryfiltername = [];
		$("#medicalhistoryfilteraddcondsubbtn").click(function() {
			$("#medicalhistoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#medicalhistoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				var columnid =$("#medicalhistoryfiltercolumn").val();
				var label =$("#medicalhistoryfiltercolumn").find('option:selected').attr('data-label');
				var condition = $("#medicalhistoryfiltercondition").val();
				var value = $("#medicalhistoryfinalfiltercondvalue").val();
				var data = columnid+"|"+condition+"|"+value;
				var count = medicalhistoryfiltername.length;
				var usecond = label+' is '+condition+' '+value;
				$("#medicalhistoryfilterconddisplay").append("<span style='word-wrap:break-word;'>"+usecond+"</span><i class='material-icons medicalhistoryfilterdocclsbtn' data-fileid='"+count+"'>close</i>");
				medicalhistoryfiltername.push(data);
				$("#medicalhistoryconditionname").val(medicalhistoryfiltername);
				medicalhistoryaddgrid();
				$(".medicalhistoryfilterdocclsbtn").click(function() {
					var id = $(this).data("fileid");
					var count = medicalhistoryfiltername.length;
					for(var k=0;k<count;k++) {
						if( k == id) {
							var nvalue = $("#medicalhistoryconditionname").val();
							var filternewcond = nvalue.split(',');
							medicalhistoryfiltername = jQuery.grep(filternewcond, function(value) {
							  return value != filternewcond[id];
							});
							$("#medicalhistoryconditionname").val(medicalhistoryfiltername);
							medicalhistoryaddgrid();
						}
					}
					$(this).prev("span").remove();
					$(this).next("br").remove();
					$(this).remove();
				});
				$("#medicalhistoryfiltercolumn").select2('val','');//filterddcondvalue
				$("#medicalhistoryfiltercondition").select2('val','');
				$("#medicalhistoryfilterddcondvalue").select2('val','');
				$("#medicalhistoryfiltercondvalue").val('');
				$("#medicalhistoryfilterfinalviewcondvalue").val('');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}//filter ends
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	$(".medicalhistoryfwgsectionoverlayclose").click(function(){
		$("#medicalhistorysectionoverlay").removeClass("effectbox");
		$("#medicalhistorysectionoverlay").addClass("closed");
		resetFields();
	});
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function Medicalhistoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#medicalhistoryaddgridwidth").width();
	var wheight = $("#medicalhistoryaddgridwidth").height();
	var sortcol = $('.Medicalhistorysheadercolsort').hasClass('datasort') ? $('.Medicalhistorysheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.Medicalhistorysheadercolsort').hasClass('datasort') ? $('.Medicalhistorysheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.Medicalhistorysheadercolsort').hasClass('datasort') ? $('.Medicalhistorysheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	if(userviewid != '') {
		userviewid= '136';
	}
	var viewfieldids ='62';
	var contactid = $("#medicalhistorycontactid").val();
	if(contactid != ''){
		var filterid = '2226|Equalto|'+contactid+'';
	} else {
		var filterid = '';
	}
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=medicalhistory&primaryid=medicalhistoryid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#medicalhistoryaddgrid').empty();
			$('#medicalhistoryaddgrid').append(data.content);
			$('.medicalhistorygridfootercontainer').empty();
			$('.medicalhistorygridfootercontainer').append(data.footer);
			datarowselectevt();
			columnresize('Medicalhistoryaddgrid');
			{//sorting
				$('.Medicalhistorysheadercolsort').click(function(){
					$('.Medicalhistorysheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#Medicalhistoryspgnumcnt li .page-text .active').data('rowcount');
					Medicalhistoryaddgrid(page,rowcount);
				});
				sortordertypereset('Medicalhistorysheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					Medicalhistoryaddgrid(page,rowcount);
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#Medicalhistoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
		},
	});
}
function medicalhistoryrefreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#Medicalhistoryspgnumcnt li .page-text .active').data('rowcount');
		Medicalhistoryaddgrid(page,rowcount);
}
function Medicalhistoryclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		Medicalhistorydataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function Medicalhistorynewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#medicalhistorygridnameinfo').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#medicalhistoryelementsname').val();
	var elementstable = $('#medicalhistoryelementstable').val();
	var elementscolmn = $('#medicalhistoryelementscolmn').val();
	var elementspartabname = $('#medicalhistoryelementspartabname').val();
	var griddatapartabnameinfo = '';
	var resctable = $('#resctable').val();
	var editordata = '';
	var neweditordata = '';
	var editorname = '';
	if(editorname != '') {
		var editornames  = [];
		editornames = editorname.split(',');
		$.each( editornames, function( key, name ) {
			editordata = $('#'+name+'').editable('getHTML');
			var regExp = /&nbsp;/g;
			var datacontent = editordata.match(regExp);
			if(datacontent !== null){
				var gllen = datacontent.length;
				for (var i = 0; i < gllen; i++) {
					editordata = editordata.replace(''+datacontent[i]+'','a10s');
				}
			}
			var fname = [];
			fname = name.split('_');
			neweditordata += amp+fname[0]+'_editorfilename='+editordata;
			$('#'+fname[0]+'_editorfilename').val(editordata);
		});
	}
	var formdata = $('#medicalhistoryaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Medicalhistory/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$("#medicalhistorysectionoverlay").removeClass("effectbox");
				$("#medicalhistorysectionoverlay").addClass("closed");
				var mcontacid = $("#emrdynamicdddataview").val();
				$("#medicalhistorycontactid").val(mcontacid)
				medicalhistoryrefreshgrid();
				$('#medicalhistorydataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Medicalhistorydataupdateinfofetchfun(datarowid) {
	var elementsname = $('#medicalhistoryelementsname').val();
	var elementstable = $('#medicalhistoryelementstable').val();
	var elementscolmn = $('#medicalhistoryelementscolmn').val();
	var elementpartable = $('#medicalhistoryelementspartabname').val();
	var resctable = '';
	$.ajax({
		url:base_url+'Medicalhistory/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				medicalhistoryrefreshgrid();
				$('#medicalhistorydataupdatesubbtn').attr('disabled',false);
			} else {
				var txtboxname = elementsname + ',medicalhistoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				$("#medicalhistoryprimarydataid").val(datarowid);
				$('#processoverlay').hide();
				$("#medicalhistorysectionoverlay").removeClass("closed");
				$("#medicalhistorysectionoverlay").addClass("effectbox");
				$('.updatebtnclass').removeClass('hidedisplay');
				$('.addbtnclass').addClass('hidedisplay');
			}
		}
	});
}
function Medicalhistorydataupdatefun() {
	var amp = '&';
	var gridname = $('#medicalhistorygridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editordata = '';
		var neweditordata = '';
		var editorname = $('#editornameinfo').val();
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(',');
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').editable('getHTML');
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){
					var glent = datacontent.length;
					for (var i = 0; i < glent; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				editordata += '&'+fname[0]+'_editorfilename='+editordata;
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
		}
		var resctable = '';
	}
	var formdata = $('#medicalhistoryaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;
	}else{
		dataset = 'datas=' + datainformation;
	}
	$.ajax({
		url: base_url+'Medicalhistory/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$("#medicalhistorysectionoverlay").removeClass("effectbox");
				$("#medicalhistorysectionoverlay").addClass("closed");
				var mcontacid = $("#emrdynamicdddataview").val();
				$("#medicalhistorycontactid").val(mcontacid);
				medicalhistoryrefreshgrid();
				$('#medicalhistorydataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			}
		},
	});
}
function Medicalhistoryrecorddelete(datarowid) {
	var elementstable = $('#medicalhistoryelementstable').val();
	var elementspartable = $('#medicalhistoryelementspartabname').val();
	$.ajax({
		url: base_url + 'Medicalhistory/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				medicalhistoryrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				medicalhistoryrefreshgrid();
				$('#basedeleteoverlayforcommodule').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}