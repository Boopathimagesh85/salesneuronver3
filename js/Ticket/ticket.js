$(document).ready(function() {
	{// Foundation Initialization
		$(document).foundation();
	}
    {// Main div height width change
		maindivwidth();
		var maindiv = $('.maindiv').width();
		var a = screen.width;
		reportsviewgridwidth = ((a * maindiv)/100);	 
	}
    {// Grid Calling Functions
		ticketgrid();
		//crud action
		crudactionenable();
	}
    { // froala editor initial set empty
    	var editorname = $("#editornameinfo").val();
		froalaarray=[editorname,'html.set',''];
	}
	{// Clear the form - regenerate the number 
		$('#formclearicon').click(function() {
			$("#tickettags").select2('val',' ');	
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
		});
	}
	{// Close Add Form
		var addcloseinfo = ["closeaddform", "ticketgriddisplay", "ticketaddformdiv"]
		addclose(addcloseinfo);
		var addcloseviewcreation =["viewcloseformiconid","ticketgriddisplay","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
  	{// Ticket view by drop down change
		$('#dynamicdddataview').change(function() {
			ticketgrid();
		});		
	}
	$(".dropdownchange").change(function() {
		var dataattrname = ['dataidhidden'];
		var dropdownid =['employeeidddid'];
		var textboxvalue = ['employeetypeid'];
		var selectid = $(this).attr('id');
		var index = dropdownid.indexOf(selectid);
		var selected=$('#'+selectid+'').find('option:selected');
		var datavalue=selected.data(''+dataattrname[index]+'');
		$('#'+textboxvalue[index]+'').val(datavalue);
		if(selectid == 'employeeidddid') {
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data('ddid');
			$('#employeeid').val(datavalue);
		}
	});
	{// Value Set Mobile
		$(".chzn-selectcomp").select2().select2('val', 'Mobile');
	}
	{// Validation for Ticket Add  
		$('#dataaddsbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$("#processoverlay").show();
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['4','accountid','ticketsourceid','employeeid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Action Events
		$("#cloneicon").click(function() {
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				ticketsclonedatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('editclick','ticketaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('ticketgrid');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				ticketseditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','ticketaddformdiv');
				$(".fr-element").attr("contenteditable", 'false');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','ticketaddformdiv');
			$(".fr-element").attr("contenteditable", 'true');
			$(".froala-element").css('pointer-events','auto');
			$("#filenamedivhid").show();
		});
	}	
	{// Update Tickets information
		$('#dataupdatesubbtn').click(function(e) {	
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['4','accountid','ticketsourceid','employeeid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{// Account id Click Event
		$("#accountid").click(function() {
			if(softwareindustryid == 2){
				var accountid = $("#accountid").val();
				accountdetailfetch(accountid);
				Materialize.updateTextFields();
			}else{
				var accountid = $("#accountid").val();
				accountdetailfetch(accountid);
				Materialize.updateTextFields();
			}
		});
	}
	{// Ticket Date Restriction
		{// Date REstriction
			/* var dateformetcon  = 0;
			var dateformetcon = $('#ticketdate').attr('data-dateformater');
			$('#ticketdate').datetimepicker({
				dateFormat: dateformetcon,
				showTimepicker :false,
				minDate: 0,
				onSelect: function () {
					getandsetdate($('#ticketdate').val());
				},
				onClose: function () {
					$('#ticketdate').focus();
				}
			}); */
			var dateformetdata = $('#ticketdate').attr('data-dateformater');
			$('#ticketdate').datetimepicker({
				dateFormat: dateformetdata,
				showTimepicker :false,
				minDate: 0,
				changeMonth: true,
				changeYear: true,
				maxDate: null,
				yearRange : '1947:c+100',
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						getandsetdate($('#ticketdate').val());
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
					}
				},
				onClose: function () {
					$('#ticketdate').focus();
				}
			}).click(function() {
				$(this).datetimepicker('show');
			});
		}
	}	
	{// Redirected form Home
		add_fromredirect("ticketsaddsrc",227);
	}
	//Ticket-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique227"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function(){
				ticketseditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','ticketaddformdiv');		
				sessionStorage.removeItem("reportunique227");
			},50);	
		}
	}
	{// Account name based contact name
		$("#accountid").change(function(){
			if(softwareindustryid == 2 || softwareindustryid == 1){
				var accid = $("#accountid").select2('val');
				if(accid == null || accid == ""){
					accid = "1";
				}
				contactnamefetch(accid);
				Materialize.updateTextFields();
			}else{
				var accid = $("#accountid").val();
				$('#billingaddresstype,#shippingaddresstype').select2('val','2');
				Materialize.updateTextFields();
			}
			Materialize.updateTextFields();
		});
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
		$("#mailicon").click(function() {
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if(datarowid) {		
				var validemail=validateemail(datarowid,'ticket','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(datarowid,'ticket','emailid','','');
					// For Redirection
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",'');
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				} else {
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}	
		});
	}	
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					for(var i=0;i<data.length;i++) {
						if(data[i] != '') {
							mobilenumber.push(data[i]);
						}
					}
					if(mobilenumber.length > 1) {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").hide();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						mobilenumberload(mobilenumber,'smsmobilenumid');
					} else if(mobilenumber.length == 1) {
						sessionStorage.setItem("mobilenumber",mobilenumber);
						sessionStorage.setItem("viewfieldids",viewfieldids);
						sessionStorage.setItem("recordis",datarowid);
						window.location = base_url+'Sms';
					} else {
						alertpopup("Invalid mobile number");
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					for(var i=0;i<data.length;i++){
						if(data[i] != ''){
							mobilenumber.push(data[i]);
						}
					}
					if(mobilenumber.length > 1) {
						$("#c2cmobileoverlay").show();
						$("#callmoduledivhid").hide();
						$("#calcount").val(mobilenumber.length);
						$("#callrecordid").val(datarowid);
						$("#callmoduleid").val(viewfieldids);
						mobilenumberload(mobilenumber,'callmobilenum');
					} else if(mobilenumber.length == 1){
						clicktocallfunction(mobilenumber);
					} else {
						alertpopup("Invalid mobile number");
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function()  {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,ticketgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			ticketgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{// Get and Set Date
	function getandsetdate(dateset) { 
		$('#deliveryduedate').datetimepicker('option', 'minDate', dateset);
	}
}
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Tickets View Grid
	function ticketgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#ticketspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#ticketspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#ticketgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.ticketsheadercolsort').hasClass('datasort') ? $('.ticketsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.ticketsheadercolsort').hasClass('datasort') ? $('.ticketsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.ticketsheadercolsort').hasClass('datasort') ? $('.ticketsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=ticket&primaryid=ticketid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#ticketgrid').empty();
				$('#ticketgrid').append(data.content);
				$('#ticketgridfooter').empty();
				$('#ticketgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('ticketgrid');
				{//sorting
					$('.ticketsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.ticketsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#ticketspgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.ticketsheadercolsort').hasClass('datasort') ? $('.ticketsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.ticketsheadercolsort').hasClass('datasort') ? $('.ticketsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#ticketgrid .gridcontent').scrollLeft();
						ticketgrid(page,rowcount);
						$('#ticketgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('ticketsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						ticketgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#ticketspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						ticketgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var ticketsideditvalue = sessionStorage.getItem("ticketidforedit"); 
					if(ticketsideditvalue != null){
						setTimeout(function(){ 
						ticketseditdatafetchfun(ticketsideditvalue);
						sessionStorage.removeItem("ticketidforedit");},100);	
					}
				}
				//Material select
				$('#ticketspgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('ticketgriddisplay', 'ticketaddformdiv');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','ticketaddformdiv');
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			getcurrentsytemdate("ticketdate");
            froalaset(froalaarray);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				ticketseditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('editclick','ticketaddformdiv');	
				fortabtouch = 1;
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#ticketgrid div.gridcontent div.active').attr('id');
			if(datarowid) {		
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#ticketspgnum li.active').data('pagenum');
		var rowcount = $('ul#ticketspgnumcnt li .page-text .active').data('rowcount');
		ticketgrid(page,rowcount);
	}
}
{// New data add submit function
	function newdataaddfun() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
			$.ajax( {
				url: base_url + "Ticket/newdatacreate",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						var dynview = $("#dynamicdddataview").val();
						$(".ftab").trigger('click');
						resetFields();
						$('#ticketaddformdiv').hide();
						$('#ticketgriddisplay').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						$("#dynamicdddataview").select2('val',dynview);
						$("#tickettags").select2('val',' ');
						//For Keyboard Shortcut Variables
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();
					}
				},
			});
	}
}
{// Old Information show in form
	function getformdata(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		$.ajax( {
			url:base_url+"Ticket/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					$('#ticketaddformdiv').hide();
					$('#ticketgriddisplay').fadeIn(1000);
					refreshgrid();
					$("#processoverlay").hide();
				} else {
					addslideup('ticketgriddisplay', 'ticketaddformdiv');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					var tag = data['tickettags'].split(',');
					$("#tickettags").select2('val',tag);
					editordatafetch(froalaarray,data);
					var accname = data.accountid;
					var contname = data.contactid;
					$("#accountid").select2('val',accname).trigger('change');
					$("#contactid").select2('val',contname);
					$("#processoverlay").hide();
				}
			}
		});
	}
}
{// Update old information
	function updateformdata() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Ticket/datainformationupdate",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$('#ticketaddformdiv').hide();
						$('#ticketgriddisplay').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						$("#tickettags").select2('val',' ');
						//For Keyboard Shortcut Variables
						addformupdate = 0;
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();
					}
				},
			});
	}
}
{// Delete Operation
	function recorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Ticket/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
				} else if('Denied'){
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission Denied');
				}
			},
		});
	}
}
{// Contact name fetchformdataeditdetails
	function contactnamefetch(accid) {
		$('#contactid').empty();
		$('#contactid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Ticket/contactnamefetchval?accountid='+accid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#contactid')
						.append($("<option></option>")
						.attr("value",data[index]['id'])
						.text(data[index]['name']));
					});
				}
				$('#contactid').trigger('change');
			},
		});
	}
}
{// Account detail fetch
	function accountdetailfetch(accountid) {
		$.ajax({
			url:base_url+'Ticket/accountdetailfetchval?accountid='+accountid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data)  {
				$("#mobilenumber").val(data['mobile']);
				$("#emailid").val(data['email']);
			},		
		});
	}
}
{// Edit Function
	function ticketseditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		getformdata(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function ticketsclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		getformdata(datarowid);
		firstfieldfocus();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{
	function getaddress(id,table,type) {
		if(id != null && id != '' && table != null && table !='') {
			var append=type.toLowerCase();
			$.ajax({
				url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.result == true) {
						var datanames = ['5','address','pincode','city','state','country'];
						var textboxname = ['5',''+append+'address',''+append+'pincode',''+append+'city',''+append+'state',''+append+'country'];
						textboxsetvalue(textboxname,datanames,data,[]);
					}
					$("#shippingarea").val('');
				},
			});
		}
	}
}