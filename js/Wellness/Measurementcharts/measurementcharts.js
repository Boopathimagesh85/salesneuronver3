$(document).ready(function(){
	METHOD ='ADD';
	$(document).foundation();
	maindivwidth();
	{
		measurementchartsaddgrid();
	}
	{
		var addclosemeasurementchartscreation =['closeaddform','measurementchartscreationview','measurementchartscreationformadd'];
		addclose(addclosemeasurementchartscreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			measurementchartsaddgrid();
		});
	}
	{
		fortabtouch = 0;
	}
	{
		$('#addicon').click(function(){
			addslideup('measurementchartscreationview','measurementchartscreationformadd');
			resetFields();
			$('.addbtnclass').removeClass('hidedisplay')
			$('.updatebtnclass').addClass('hidedisplay')
			showhideiconsfun('addclick','measurementchartscreationformadd');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			$("#membershipplanname").empty();
			Materialize.updateTextFields();
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
		});
		$('#editicon').click(function(){
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('measurementchartscreationview','measurementchartscreationformadd');
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay')
				measurementchartsdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','measurementchartscreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#deleteicon').click(function(){
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#reloadicon').click(function(){
			refreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			measurementchartsrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				measurementchartsdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','measurementchartscreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','measurementchartscreationformadd');
		});
		$('#dashboardicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$.getScript(base_url+'js/plugins/uploadfile/jquery.uploadfile.min.js');
				$.getScript(base_url+'js/plugins/zingchart/zingchart.min.js');
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
					$('#dashboardcontainer').removeClass('hidedisplay');
					$('.actionmenucontainer').addClass('hidedisplay');
					$('.fullgridview').addClass('hidedisplay');
					$('.footercontainer').addClass('hidedisplay');
					$('.forgetinggridname').addClass('hidedisplay');
					$('#processoverlay').hide();
			} else {
				alertpopup('Please select a row');
			}
		});
		
		$('#printicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#cloneicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$('.addbtnclass').removeClass('hidedisplay');
				$('.updatebtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				showhideiconsfun('editclick','measurementchartscreationformadd');
				$('#formclearicon').hide();
				measurementchartsclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#smsicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#mailicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'measurementcharts','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','73');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#outgoingcallicon').click(function() {
			var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#customizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#importicon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#datamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	$("#membername").change(function(){
		var memberid = $(this).val();
		if(memberid){	
			$("#membershipplanname").empty();
			$.ajax({
				url:base_url+"Contact/memberdetailfetch?memberid="+memberid,
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#memid").val(data.contactnumber);
					$("#genderid").select2('val',data.genderid);
					$("#age").val(data.age);
					$('#bloodgroupid').select2('val',data.bloodgroupid);
					$("#memheight").val(data.height);
					$('#memweight').val(data.weight);
					$.each(data.data, function( key, value ) {
						$("#membershipplanname")
						.append($("<option></option>")
						.attr("data-membershipplannamehidden",value.productname)
						.attr("value",value.membershipplans)
						.attr("data-joiningdate",value.joiningdate)
						.attr("data-expirydate",value.expirydate)
						.text(value.productname));
					});			
					$("#membershipplanname").select2('val','').trigger("change");
					Materialize.updateTextFields();
				}
			});
		}else{
			$("#memid").val('');
			$("#genderid").select2('val','');
			$("#age").val('');
			$('#bloodgroupid').select2('val','');
			$("#memheight").val('');
			$('#memweight').val('');
			$("#membershipplanname").select2('val','').trigger("change");
			$("#membershipplanname").empty();	
		}				
	});
	$("#membershipplanname").change(function(){
		var planid = $(this).val();
		if(planid != null){
			var expirydate = $('option:selected', this).attr('data-expirydate');
			var joiningdate = $('option:selected', this).attr('data-joiningdate');
			$("#dateofjoining").val(joiningdate);
			$("#dateofexpiration").val(expirydate);
			Materialize.updateTextFields();
		}else{
			$("#dateofjoining").val("");
			$("#dateofexpiration").val("");
		}
	});
	{
		$('#dataaddsbtn').click(function(){
			$('.ftab').trigger('click');
			$('#measurementchartsformaddwizard').validationEngine('validate');
		});
		$('#measurementchartsformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				measurementchartsnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#measurementchartsformeditwizard').validationEngine('validate');
		});
		$('#measurementchartsformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				measurementchartsdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function measurementchartsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#measurementchartsaddgrid').width();
	var wheight = $(window).height();
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else {
		var sortcol = $('.measurementchartssheadercolsort').hasClass('datasort') ? $('.measurementchartssheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.measurementchartssheadercolsort').hasClass('datasort') ? $('.measurementchartssheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.measurementchartssheadercolsort').hasClass('datasort') ? $('.measurementchartssheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $('#conditionname').val();
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=measurementcharts&primaryid=measurementchartsid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#measurementchartsaddgrid').empty();
			$('#measurementchartsaddgrid').append(data.content);
			$('#measurementchartsaddgridfooter').empty();
			$('#measurementchartsaddgridfooter').append(data.footer);
			datarowselectevt();
			columnresize('measurementchartsaddgrid');
			{//sorting
				$('.measurementchartssheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.measurementchartssheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#measurementchartsspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.measurementchartssheadercolsort').hasClass('datasort') ? $('.measurementchartssheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.measurementchartssheadercolsort').hasClass('datasort') ? $('.measurementchartssheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#measurementchartsaddgrid .gridcontent').scrollLeft();
					measurementchartsaddgrid(page,rowcount);
					$('#measurementchartsaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('measurementchartssheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#measurementchartsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					measurementchartsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#measurementchartspgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					measurementchartsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#measurementchartspgrowcount').material_select();
		},
	});
}
function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#measurementchartsspgnumcnt li .page-text .active').data('rowcount');
		measurementchartsaddgrid(page,rowcount);
}
function measurementchartsclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		measurementchartsdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function measurementchartsnewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementspartabname = $('#elementspartabname').val();
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var resctable = $('#resctable').val();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $('#measurementchartsdataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Measurementcharts/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#measurementchartscreationformadd').hide();
				$('#measurementchartscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function measurementchartsdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Measurementcharts/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#measurementchartscreationformadd').hide();
				$('#measurementchartscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('measurementchartscreationview','measurementchartscreationformadd');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
function measurementchartsdataupdatefun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var resctable = $('#resctable').val();
	}
	var formdata = $('#measurementchartsdataaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;
	}else{
		dataset = 'datas=' + datainformation;
	}
	$.ajax({
		url: base_url+'Measurementcharts/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#measurementchartscreationformadd').hide();
				$('#measurementchartscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function measurementchartsrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Measurementcharts/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
