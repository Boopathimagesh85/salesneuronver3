$(document).ready(function(){
	{// Foundation Initialization
        $(document).foundation();
    }
	{// Maindiv height width change
        maindivwidth();
    }
	METHOD ='ADD';
	UPDATEID = 0;
	{
		dietplanaddgrid();
		dietplandieaddgrid1();
	}
	{//inner-form-with-grid
		$("#dietplandieingridadd1").click(function(){
			formheight();
			$("#dietplandieoverlay").removeClass("closed");
			$("#dietplandieoverlay").addClass("effectbox");
			clearform('gridformclear');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#dietplandiecancelbutton").click(function(){
			$("#dietplandieoverlay").removeClass("effectbox");
			$("#dietplandieoverlay").addClass("closed");
			$('#dietplandieupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#dietplandieaddbutton').show();//display the ADD button(inner-productgrid)*/	
		});
	}
	{
		var addclosedietplancreation =['closeaddform','dietplancreationview','dietplancreationformadd'];
		addclose(addclosedietplancreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			dietplanaddgrid();
		});
	}
	{
		fortabtouch = 0;
		$('#dietplandieupdatebutton').removeClass('updatebtnclass');
		$('#dietplandieupdatebutton').removeClass('hidedisplayfwg');
		$('#dietplandieaddbutton').removeClass('addbtnclass');
		$("#dietplandieupdatebutton").hide();
	}
	{
		$('#addicon').click(function(){
			addslideup('dietplancreationview','dietplancreationformadd');
			resetFields();
			cleargriddata('dietplandieaddgrid1');
			$('.addbtnclass').removeClass('hidedisplay')
			$('.updatebtnclass').addClass('hidedisplay')
			showhideiconsfun('addclick','dietplancreationformadd');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			$("#membershipplanname").empty();
			Materialize.updateTextFields();
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
		});
		$('#editicon').click(function(){
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('dietplancreationview','dietplancreationformadd');
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay')
				dietplandataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','dietplancreationformadd');
				fortabtouch = 1;
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#deleteicon').click(function(){
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#reloadicon').click(function(){
			refreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			dietplanrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				dietplandataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','dietplancreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','dietplancreationformadd');
		});
		$('#dashboardicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$.getScript(base_url+'js/plugins/uploadfile/jquery.uploadfile.min.js');
				$.getScript(base_url+'js/plugins/zingchart/zingchart.min.js');
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
					$('#dashboardcontainer').removeClass('hidedisplay');
					$('.actionmenucontainer').addClass('hidedisplay');
					$('.fullgridview').addClass('hidedisplay');
					$('.footercontainer').addClass('hidedisplay');
					$('.forgetinggridname').addClass('hidedisplay');
					$('#processoverlay').hide();
			} else {
				alertpopup('Please select a row');
			}
		});
		
		$('#printicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#cloneicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$('.addbtnclass').removeClass('hidedisplay');
				$('.updatebtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				showhideiconsfun('editclick','dietplancreationformadd');
				$('#formclearicon').hide();
				dietplanclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#smsicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#mailicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'dietplan','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','72');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#outgoingcallicon').click(function() {
			var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#customizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#importicon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#datamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	$("#membername").change(function(){
		var memberid = $(this).val();
		if(memberid){
			$("#membershipplanname").empty();
			$.ajax({
				url:base_url+"Contact/memberdetailfetch?memberid="+memberid,
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#memid").val(data.contactnumber);
					$("#genderid").select2('val',data.genderid);
					$("#age").val(data.age);
					$('#bloodgroupid').select2('val',data.bloodgroupid);
					$("#height").val(data.height);
					$('#memweight').val(data.weight);
					$.each(data.data, function( key, value ) {
						$("#membershipplanname")
						.append($("<option></option>")
						.attr("data-membershipplannamehidden",value.productname)
						.attr("value",value.membershipplans)
						.attr("data-joiningdate",value.joiningdate)
						.attr("data-expirydate",value.expirydate)
						.text(value.productname));
					});			
					$("#membershipplanname").select2('val','').trigger("change");
					Materialize.updateTextFields();
				}
			});
		}else{
			$("#memid").val('');
			$("#genderid").select2('val','');
			$("#age").val('');
			$('#bloodgroupid').select2('val','');
			$("#memheight").val('');
			$('#memweight').val('');
			$("#membershipplanname").select2('val','').trigger("change");
			$("#membershipplanname").empty();	
		}				
	});
	$("#membershipplanname").change(function(){
		var planid = $(this).val();
		if(planid != null){
			var expirydate = $('option:selected', this).attr('data-expirydate');
			var joiningdate = $('option:selected', this).attr('data-joiningdate');
			$("#dateofjoining").val(joiningdate);
			$("#dateofexpiration").val(expirydate);
			Materialize.updateTextFields();
		}else{
			$("#dateofjoining").val("");
			$("#dateofexpiration").val("");
		}
	});
	{
		$('#dataaddsbtn').click(function(){
			$('.ftab').trigger('click');
			$('#dietplanformaddwizard').validationEngine('validate');
		});
		$('#dietplanformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				dietplannewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#dietplanformeditwizard').validationEngine('validate');
		});
		$('#dietplanformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				dietplandataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
$('#dietplandieingrideditspan1').removeAttr('style');
{
	$('#dietplandieingriddel1').click(function(){
		var datarowid = $('#dietplandieaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {
			deletegriddatarow('dietplandieaddgrid1',datarowid);
		} else {
			alertpopup('Please select a row');
		}
	});
	$("#dietplandieingridedit1").click(function() {
		var selectedrow = $('#dietplandieaddgrid1 div.gridcontent div.active').attr('id');
		if(selectedrow) {
			formheight();
			$("#dietplandieoverlay").removeClass("closed");
			$("#dietplandieoverlay").addClass("effectbox");
			gridtoformdataset('dietplandieaddgrid1',selectedrow);
			Materialize.updateTextFields();
			$('#dietplandieupdatebutton').show();	//display the UPDATE button(inner - productgrid)
			$('#dietplandieaddbutton').hide();	//display the ADD button(inner-productgrid) */
			METHOD = 'UPDATE';
			UPDATEID = selectedrow;
		} else {
			alertpopup('Please Select The Row To Edit');
		}
	});
}
	{
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;		
		$('.frmtogridbutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			$('#dietplandietdetailsid').val(0);
			var gridname = $(this).data('frmtogridname');
			gridformtogridvalidatefunction(gridname);
			griddataid = i;
			gridynamicname = gridname;
			$('#'+gridname+'validation').validationEngine('validate');
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
				formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
				formtogriddata(gridname,METHOD,'',UPDATEID);
			}
			/* Hide columns */
			var hideprodgridcol = ['dietplandietdetailsid'];
			gridfieldhide('dietplandieaddgrid1',hideprodgridcol);
			datarowselectevt();
			METHOD = 'ADD';
			$('#dietplandieupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#dietplandieaddbutton').show();//display the ADD button(inner-productgrid)*/		
			clearform('gridformclear');
			$('#dietplandiecancelbutton').trigger('click');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function dietplanaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#dietplanaddgrid').width();
	var wheight = $(window).height();
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol) {
		sortcol = sortcol;
	} else {
		var sortcol = $('.dietplansheadercolsort').hasClass('datasort') ? $('.dietplansheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.dietplansheadercolsort').hasClass('datasort') ? $('.dietplansheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.dietplansheadercolsort').hasClass('datasort') ? $('.dietplansheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $('#conditionname').val();
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=dietplan&primaryid=dietplanid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#dietplanaddgrid').empty();
			$('#dietplanaddgrid').append(data.content);
			$('#dietplanaddgridfooter').empty();
			$('#dietplanaddgridfooter').append(data.footer);
			datarowselectevt();
			columnresize('dietplanaddgrid');
			{//sorting
				$('.dietplansheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.dietplansheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#dietplanspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.dietplansheadercolsort').hasClass('datasort') ? $('.dietplansheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.dietplansheadercolsort').hasClass('datasort') ? $('.dietplansheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#dietplanaddgrid .gridcontent').scrollLeft();
					dietplanaddgrid(page,rowcount);
					$('#dietplanaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('dietplansheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#dietplanaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					dietplanaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#dietplanpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					dietplanaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#dietplanpgrowcount').material_select();
		},
	});
}
function dietplandieaddgrid1() {
	var wwidth = $('#dietplandieaddgrid1').width();
	var wheight = $('#dietplandieaddgrid1').height();
	$.ajax({
		url:base_url+'Base/localgirdheaderinformationfetch?tabgroupid=182&moduleid=72&width='+wwidth+'&height='+wheight+'&modulename=dietplandieaddgrid1',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#dietplandieaddgrid1').empty();
			$('#dietplandieaddgrid1').append(data.content);
			$('#dietplandieaddgrid1footer').empty();
			$('#dietplandieaddgrid1footer').append(data.footer);
			datarowselectevt();
			columnresize('dietplandieaddgrid1');
			var hideprodgridcol = ['dietplandietdetailsid'];
			gridfieldhide('dietplandieaddgrid1',hideprodgridcol);
		},
	});
}
function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#dietplanspgnumcnt li .page-text .active').data('rowcount');
		dietplanaddgrid(page,rowcount);
}
function dietplanclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		dietplandataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function dietplannewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementspartabname = $('#elementspartabname').val();
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var resctable = $('#resctable').val();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $('#dietplandataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Dietplan/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#dietplancreationformadd').hide();
				$('#dietplancreationview').fadeIn(1000);
				refreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function dietplandataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Dietplan/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#dietplancreationformadd').hide();
				$('#dietplancreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('dietplancreationview','dietplancreationformadd');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				dietplandetailfetch(datarowid);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
function dietplandetailfetch(pid){
	if(pid!='') {
		$.ajax({
			url:base_url+'Dietplan/dietplanproductdetailfetch?primarydataid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					cleargriddata('dietplandieaddgrid1');
					loadinlinegriddata('dietplandieaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('dietplandieaddgrid1');
					/* Hide columns */
					var hideprodgridcol = ['dietplandietdetailsid'];
					gridfieldhide('dietplandieaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
function dietplandataupdatefun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var resctable = $('#resctable').val();
	}
	var formdata = $('#dietplandataaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;
	}else{
		dataset = 'datas=' + datainformation;
	}
	$.ajax({
		url: base_url+'Dietplan/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#dietplancreationformadd').hide();
				$('#dietplancreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function dietplanrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Dietplan/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
