$(document).ready(function(){
	METHOD ='ADD';
	UPDATEID = 0;
	$(document).foundation();
	maindivwidth();
	{
		workoutplansaddgrid();
		workoutplansworaddgrid1();
	}
	{
		var addcloseworkoutplanscreation =['closeaddform','workoutplanscreationview','workoutplanscreationformadd'];
		addclose(addcloseworkoutplanscreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			workoutplansaddgrid();
		});
	}
	{
		fortabtouch = 0;
		$('#workoutplansworupdatebutton').removeClass('updatebtnclass');
		$('#workoutplansworupdatebutton').removeClass('hidedisplayfwg');
		$('#workoutplansworaddbutton').removeClass('addbtnclass');
		$("#workoutplansworupdatebutton").hide();
	}
	{
		$('#addicon').click(function(){
			addslideup('workoutplanscreationview','workoutplanscreationformadd');
			resetFields();
			cleargriddata('workoutplansworaddgrid1');
			$('.addbtnclass').removeClass('hidedisplay')
			$('.updatebtnclass').addClass('hidedisplay')
			showhideiconsfun('addclick','workoutplanscreationformadd');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			$("#membershipplanname").empty();
			Materialize.updateTextFields();
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$('#editicon').click(function(){
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('workoutplanscreationview','workoutplanscreationformadd');
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay')
				workoutplansdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','workoutplanscreationformadd');
				fortabtouch = 1;
				Operation = 1; //for pagination
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#deleteicon').click(function(){
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#reloadicon').click(function(){
			refreshgrid();
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			workoutplansrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				workoutplansdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','workoutplanscreationformadd');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','workoutplanscreationformadd');
		});
		$('#dashboardicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$.getScript(base_url+'js/plugins/uploadfile/jquery.uploadfile.min.js');
				$.getScript(base_url+'js/plugins/zingchart/zingchart.min.js');
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
					$('#dashboardcontainer').removeClass('hidedisplay');
					$('.actionmenucontainer').addClass('hidedisplay');
					$('.fullgridview').addClass('hidedisplay');
					$('.footercontainer').addClass('hidedisplay');
					$('.forgetinggridname').addClass('hidedisplay');
					$('#processoverlay').hide();
			} else {
				alertpopup('Please select a row');
			}
		});
		
		$('#printicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#cloneicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$('.addbtnclass').removeClass('hidedisplay');
				$('.updatebtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				showhideiconsfun('editclick','workoutplanscreationformadd');
				$('#formclearicon').hide();
				workoutplansclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
				Operation = 0; //for pagination
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#smsicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#mailicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'workoutplans','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','71');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#outgoingcallicon').click(function() {
			var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#customizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#importicon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#datamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#dataaddsbtn').click(function(){
			$('.ftab').trigger('click');
			$('#workoutplansformaddwizard').validationEngine('validate');
		});
		$('#workoutplansformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				workoutplansnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#workoutplansformeditwizard').validationEngine('validate');
		});
		$('#workoutplansformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				workoutplansdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	$('#workoutplansworingrideditspan1').removeAttr('style');
	{
		$('#workoutplansworingriddel1').click(function(){
			var datarowid = $('#workoutplansworaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {
				deletegriddatarow('workoutplansworaddgrid1',datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
		$("#workoutplansworingridedit1").click(function() {
			var selectedrow = $('#workoutplansworaddgrid1 div.gridcontent div.active').attr('id');
			if(selectedrow) {
				gridtoformdataset('workoutplansworaddgrid1',selectedrow);
				Materialize.updateTextFields();
				$('#workoutplansworupdatebutton').show();	//display the UPDATE button(inner - productgrid)
				$('#workoutplansworaddbutton').hide();	//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
	$("#workoutplansworingridadd1").click(function(){
		formheight();
		$("#workoutplansworoverlay").removeClass("closed");
		$("#workoutplansworoverlay").addClass("effectbox");
		clearform('gridformclear');
		Materialize.updateTextFields();
		firstfieldfocus();
	});
	$("#workoutplansworcancelbutton").click(function(){
		clearform('gridformclear');
		$("#workoutplansworoverlay").removeClass("effectbox");
		$("#workoutplansworoverlay").addClass("closed");
		$('#workoutplansworupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
		$('#workoutplansworaddbutton').show();//display the ADD button(inner-productgrid)*/	
	});
	$("#membername").change(function(){
		var memberid = $(this).val();
		if(memberid){
			$("#membershipplanname").empty();
			$.ajax({
				url:base_url+"Contact/memberdetailfetch?memberid="+memberid,
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$("#memid").val(data.contactnumber);
					$("#genderid").select2('val',data.genderid);
					$("#age").val(data.age);
					$('#bloodgroupid').select2('val',data.bloodgroupid);
					$("#height").val(data.height);
					$('#memweight').val(data.weight);
					$.each(data.data, function( key, value ) {
						$("#membershipplanname")
						.append($("<option></option>")
						.attr("data-membershipplannamehidden",value.productname)
						.attr("value",value.membershipplans)
						.attr("data-joiningdate",value.joiningdate)
						.attr("data-expirydate",value.expirydate)
						.text(value.productname));
					});			
					$("#membershipplanname").select2('val','').trigger("change");
					Materialize.updateTextFields();
				}
			});
		}else{
			$("#memid").val('');
			$("#genderid").select2('val','');
			$("#age").val('');
			$('#bloodgroupid').select2('val','');
			$("#height").val('');
			$('#memweight').val('');
			$("#membershipplanname").select2('val','').trigger("change");
			$("#membershipplanname").empty();
		}				
	});
	$("#membershipplanname").change(function(){
		var planid = $(this).val();
		if(planid != null){
			var expirydate = $('option:selected', this).attr('data-expirydate');
			var joiningdate = $('option:selected', this).attr('data-joiningdate');
			$("#dateofjoining").val(joiningdate);
			$("#dateofexpiration").val(expirydate);
			Materialize.updateTextFields();
		}else{
			$("#dateofjoining").val("");
			$("#dateofexpiration").val("");
		}
	});
	{
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){
			var i = $(this).data('frmtogriddataid');
			$('#workoutplansworkoutdetailsid').val(0);
			var gridname = $(this).data('frmtogridname');
			gridformtogridvalidatefunction(gridname);
			griddataid = i;
			gridynamicname = gridname;
			$('#'+gridname+'validation').validationEngine('validate');
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
				formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
				formtogriddata(gridname,METHOD,'',UPDATEID);
			}
			/* Hide columns */
			var hideprodgridcol = ['workoutplansworkoutdetailsid'];
			gridfieldhide('workoutplansworaddgrid1',hideprodgridcol);
			datarowselectevt();
			$("#workoutplansworcancelbutton").trigger('click');
			METHOD = 'ADD';
			clearform('gridformclear');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function workoutplansaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#workoutplanspgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#workoutplanspgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#workoutplansaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol) {
		sortcol = sortcol;
	} else {
		var sortcol = $('.workoutplanssheadercolsort').hasClass('datasort') ? $('.workoutplanssheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.workoutplanssheadercolsort').hasClass('datasort') ? $('.workoutplanssheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.workoutplanssheadercolsort').hasClass('datasort') ? $('.workoutplanssheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $('#conditionname').val();
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=workoutplans&primaryid=workoutplansid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#workoutplansaddgrid').empty();
			$('#workoutplansaddgrid').append(data.content);
			$('#workoutplansaddgridfooter').empty();
			$('#workoutplansaddgridfooter').append(data.footer);
			datarowselectevt();
			columnresize('workoutplansaddgrid');
			{//sorting
				$('.workoutplanssheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.workoutplanssheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#workoutplansspgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.workoutplanssheadercolsort').hasClass('datasort') ? $('.workoutplanssheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.workoutplanssheadercolsort').hasClass('datasort') ? $('.workoutplanssheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#workoutplansaddgrid .gridcontent').scrollLeft();
					workoutplansaddgrid(page,rowcount);
					$('#workoutplansaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('workoutplanssheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#workoutplansaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					workoutplansaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#workoutplanspgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					workoutplansaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#workoutplanspgrowcount').material_select();
		},
	});
}
function workoutplansworaddgrid1() {
	var wwidth = $('#workoutplansworaddgrid1').width();
	var wheight = $('#workoutplansworaddgrid1').height();
	$.ajax({
		url:base_url+'Base/localgirdheaderinformationfetch?tabgroupid=180&moduleid=71&width='+wwidth+'&height='+wheight+'&modulename=workoutplansworaddgrid1',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#workoutplansworaddgrid1').empty();
			$('#workoutplansworaddgrid1').append(data.content);
			$('#workoutplansworaddgrid1footer').empty();
			$('#workoutplansworaddgrid1footer').append(data.footer);
			datarowselectevt();
			columnresize('workoutplansworaddgrid1');
			/* Hide columns */
			var hideprodgridcol = ['workoutplansworkoutdetailsid'];
			gridfieldhide('workoutplansworaddgrid1',hideprodgridcol);
		},
	});
}
function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#workoutplansspgnumcnt li .page-text .active').data('rowcount');
		workoutplansaddgrid(page,rowcount);
}
function workoutplansclonedatafetchfun(datarowid) {
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		workoutplansdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function workoutplansnewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementspartabname = $('#elementspartabname').val();
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var resctable = $('#resctable').val();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $('#workoutplansdataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Workoutplans/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#workoutplanscreationformadd').hide();
				$('#workoutplanscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function workoutplansdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Workoutplans/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#workoutplanscreationformadd').hide();
				$('#workoutplanscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				cleargriddata('workoutplansworaddgrid1');
				addslideup('workoutplanscreationview','workoutplanscreationformadd');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				workoutplandetailfetch(datarowid);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
function workoutplandetailfetch(pid){
	if(pid!='') {
		$.ajax({
			url:base_url+'Workoutplans/workoutplanproductdetailfetch?primarydataid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					cleargriddata('workoutplansworaddgrid1');
					loadinlinegriddata('workoutplansworaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('workoutplansworaddgrid1');
					/* Hide columns */
					var hideprodgridcol = ['workoutplansworkoutdetailsid'];
					gridfieldhide('workoutplansworaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
function workoutplansdataupdatefun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var noofrows = noofrows;
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var resctable = $('#resctable').val();
	}
	var formdata = $('#workoutplansdataaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;
	}else{
		dataset = 'datas=' + datainformation;
	}
	$.ajax({
		url: base_url+'Workoutplans/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#workoutplanscreationformadd').hide();
				$('#workoutplanscreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function workoutplansrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Workoutplans/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
