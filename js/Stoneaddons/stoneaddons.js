$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	stoneaddonsgrid();
	rate_round = $('#rate_round').val();
	// form submission preventing on enter key from fields
	$("form").bind('keypress', function(event) {
		if(event.keyCode == 13){ 
			event.preventDefault();
			return false;
		}
	});
	$(window).resize(function() {
		maingridresizeheightset('stoneaddonsgrid');
	});
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{// View by drop down change
		$('#dynamicdddataview').change(function() {
			stoneaddonsgrid();
		});
	}
	{// crud operations
		//stone add icon
		$('#addicon').click(function() {
			addslideup('stoneaddonscreationview','stoneaddonsformadd');
			$("#stoneaddonsclearicon").show();
			$('#stonechargeid,#stonechargeprimaryid').val('');
			$('#stonecharge').attr('data-primaryid','');
			stonechargegrid();
			//setTimeout(function(){
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				$('.updatebtnclass,.clonebtnclass').addClass('hidedisplay');
				$('#stoneaddonupdate,#stoneaddonupdateclone').hide();
				$("#stoneaddonsadd").show();
				$('.addbtnclass').removeClass('hidedisplay');
			//},25);
			resetFields();
			firstfieldfocus();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		//stone close icon
		var addcloseinfo =["closeaddform","stoneaddonscreationview","stoneaddonsformadd",""];
		addclose(addcloseinfo);
		var addcloseviewcreation =["viewcloseformiconid","stoneaddonscreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
		$("#dataaddsbtn").click(function() {
			var status=applyratevalidation();
			if(status == true) {
			} else {
				alertpopup("Please enter valid value");
				return false;
			}
			$("#stoneaddonaddwizard").validationEngine('validate');
		});
		jQuery("#stoneaddonaddwizard").validationEngine({
			onSuccess: function() {
				msgstatus = 0;
				addform();
			},
			onFailure: function() {				
				var dropdownid = [];
				dropdownfailureerror(dropdownid);				
			}
		});
		$("#dataupdatesubbtn").click(function() {
			var status=applyratevalidation();
			if(status == true) {
			} else{
				alertpopup("Please enter valid value");
				return false;
			}
			$("#stoneaddoneditwizard").validationEngine('validate');
		});
		// Edit
		jQuery("#stoneaddoneditwizard").validationEngine({
			onSuccess: function() {
				$('#stonecharge').attr('data-primaryid','');
				updateform();
			},
			onFailure: function() {
				var dropdownid = [];
				dropdownfailureerror(dropdownid);	
				alertpopup(validationalert);
			}
		});
		$("#stoneaddonupdateclone").mousedown(function(e) {
			var status = applyratevalidation();
			if(status == true) {
			} else {
				alertpopup("Please enter valid value");
				return false;
			}
			if(e.which == 1) {
				$("#cloneformvalidation").validationEngine('validate');
			}
		});
		jQuery("#cloneformvalidation").validationEngine({
			onSuccess: function() {
				msgstatus =1;
				addform();
			},
			onFailure: function() {	
				var dropdownid = [];
				dropdownfailureerror(dropdownid);				
			}
		});
		$('#stoneaddonsclearicon').click(function() {
			resetFields();
			$("#stonechargegrid").trigger('reloadGrid');
		});
		// Edit
		$("#editicon").click(function() {
			var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				setTimeout(function(){
					addslideup('stoneaddonscreationview','stoneaddonsformadd');
					$("#stoneaddonsclearicon").hide();
					$("#stonechargegrid").trigger('reloadGrid');
				},10);
				$('#stoneaddonsadd,#stoneaddonupdateclone').hide();
				$('.addbtnclass,.clonebtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#stoneaddonupdate').show();
				clone = 0;			
				retrive(datarowid);
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		//Delete stone charge
		$("#deleteicon").click(function() {
			var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id'); 
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
						$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}	
		});
		//stone charge yes delete
		$("#basedeleteyes").click(function() {
			var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id'); 
			$.ajax({
				url: base_url + "Stoneaddons/stonechargedelete?primaryid="+datarowid,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					if (nmsg == "SUCCESS") {
						alertpopup('Deleted successfully');
					} else {					
						alertpopup('error');
					}				
				},
			});
		});
		//Reload
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$('#stoneaccounticon').click(function() {
			var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id'); 
			if(datarowid){ 
				$('#account').select2('val','');
				getaccountgroup(datarowid); // load all accountgroup with based chargeid
				setaccountgroup(datarowid);  // set accountgroup only based on chargeid 
				getchargedetails(datarowid); // load all account with based chargeid
				getchargedetailssetdd(datarowid);  // set account only based on chargeid 
				$('#stoneaccountoverlay').fadeIn();
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$('#closestonechargeoverlay').click(function() {
			clearform('stoneaccountform');
			$('#stoneaccountoverlay').fadeOut();
		});
		$("#stoneaccountrefreshicon").click(function() {
			$("#accountstonecharge,#account").select2('val','');
		});
		$('#stoneaccountbtn').click(function() {
		   /* var value = $('#accountgroup').find('option:selected').val();
			if(value == ''){
				var accountvalue = $.trim($('#account').val());
				if(accountvalue == '') {	
					alertpopup('Select either account or accountgroup');
					return false;
				}
			} */
			$("#stoneaccountform").validationEngine('validate');
		});
		jQuery("#stoneaccountform").validationEngine({
			onSuccess: function() { 
				//alert('a'); return false;
				stoneaccountcharge();
			},
			onFailure: function() {	
				//alert('err'); return false;
				var dropdownid = ['1','account'];
				dropdownfailureerror(dropdownid);
				
			}
		});
		//clone
		$("#cloneicon").click(function() {
			var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id');			
			if(datarowid){
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				setTimeout(function(){
					addslideup('stoneaddonscreationview','stoneaddonsformadd');
					$("#stonechargegrid").trigger('reloadGrid');
				},10);
				$("#formclearicon").hide();
				$('.updatebtnclass').addClass('hidedisplay');
				$('#dataaddsbtn').css('diplay','inline-block');	
				$('.addbtnclass').removeClass('hidedisplay');
				clone = 1;
				retrive(datarowid);
				setTimeout(function(){
					$('#stonechargeprimaryid,#stonechargeprimaryname').val('');
				},10);
				Operation = 0; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,stoneaddonsgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			stoneaddonsgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	// account group change
	$('#accountgroup').change(function() {
		var value = $(this).find('option:selected').val();
		if(value == 1 || value == '') {
			$('.accountdiv').removeClass('hidedisplay');
			//$('#account').addClass('validate[required]');
		} else {
			$('.accountdiv').addClass('hidedisplay');
			//$('#account').removeClass('validate[required]');
		}
	});
	// account MultipleDD change
	$('#account').change(function() {
		var value = $(this).find('option:selected').val();
		if(value == ''){
			$('#accountgroup').removeClass('hidedisplay');
			//$('#account').addClass('validate[required]');
		}else{
			$('#accountgroup').addClass('hidedisplay');
			//$('#account').removeClass('validate[required]');
		}
	});
	// Apply Rate Change
	$(document).on("change",".stonecharge__rowtextclass",function() {
		var datarowid = $(this).data('rowid');
		var value = $(this).val();
		value = value.replace(/[^0-9\s]/gi, '');
		value = value.replace(/ /g,'');
		value = parseFloat(value).toFixed(rate_round);
		$('#stonechargegrid .gridcontent div#'+datarowid+' ul .applyrate-class .rowtext').val(value);
	});
});
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset")
	{
		setTimeout(function()
		{
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		$('#viewcreateconditiongrid').jqGrid("clearGridData", true);
	}
	else
	{
		$('#dynamicdddataview').trigger('change');
	}
}
function stoneaddonsgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#stoneaddonpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#stoneaddonpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $(window).width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#stoneaddonsortcolumn").val();
	var sortord = $("#stoneaddonsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.stoneaddonheadercolsort').hasClass('datasort') ? $('.stoneaddonheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.stoneaddonheadercolsort').hasClass('datasort') ? $('.stoneaddonheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.stoneaddonheadercolsort').hasClass('datasort') ? $('.stoneaddonheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 48;
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=stonecharge&primaryid=stonechargeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#stoneaddonsgrid').empty();
			$('#stoneaddonsgrid').append(data.content);
			$('#stoneaddonsgridfooter').empty();
			$('#stoneaddonsgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('stoneaddonsgrid');
			{//sorting
				$('.stoneaddonheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.stoneaddonheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.stoneaddonheadercolsort').hasClass('datasort') ? $('.stoneaddonheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.stoneaddonheadercolsort').hasClass('datasort') ? $('.stoneaddonheadercolsort.datasort').data('sortorder') : '';
					$("#stoneaddonsortorder").val(sortord);
					$("#stoneaddonsortcolumn").val(sortcol);
					var sortpos = $('#stoneaddonsgrid .gridcontent').scrollLeft();
					stoneaddonsgrid(page,rowcount);
					$('#stoneaddonsgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('stoneaddonheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $('.pagerowcount').data('rowcount');
					stoneaddonsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#stoneaddonpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					stoneaddonsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#stoneaddonpgrowcount').material_select();
		},
	});
}
function stonechargegrid() {
	var chargeid = $("#stonechargeid").val(); 
	chargeid = ((chargeid!='') ? chargeid : 0);
	
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#stonechargegrid").width();
	var wheight = $("#stonechargegrid").height();
	/*col sort*/
	var sortcol = $("#stonechargesortorder").val();
	var sortord = $("#stonechargesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.stonechargeheadercolsort').hasClass('datasort') ? $('.stonechargeheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.stonechargeheadercolsort').hasClass('datasort') ? $('.stonechargeheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.stonechargeheadercolsort').hasClass('datasort') ? $('.stonechargeheadercolsort.datasort').attr('id') : '0';
	
	$.ajax({
		url:base_url+"Stoneaddons/gridinformationfetch?maintabinfo=stone&primaryid=stoneid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=48"+"&filter="+chargeid,		
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#stonechargegrid').empty();
			$('#stonechargegrid').append(data.content);
			$('.stonechargegridfootercontainer').empty();
			$('.stonechargegridfootercontainer').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('stonechargegrid');
			{//sorting
				$('.stonechargeheadercolsort').click(function(){
					$('.stonechargeheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#stonechargegridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.stonechargeheadercolsort').hasClass('datasort') ? $('.stonechargeheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.stonechargeheadercolsort').hasClass('datasort') ? $('.stonechargeheadercolsort.datasort').data('sortorder') : '';
					$("#stonechargesortorder").val(sortord);
					$("#stonechargesortcolumn").val(sortcol);
					var sortpos = $('#stonechargegrid .gridcontent').scrollLeft();
					stonechargegrid(page,rowcount);
					$('#stonechargegrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('stonechargeheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#stonechargegridpgnumcnt li .page-text .active').data('rowcount');
					stonechargegrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#stonechargegridpgnumcntpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					stonechargegrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#stonechargegridpgnumcnt li .page-text .active').data('rowcount');
					stonechargegrid(page,rowcount);
				});
				$('#ratepgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#stonechargegridpgnumcntpgnum li.active').data('pagenum');
					stonechargegrid(page,rowcount);
				});
			}
			{ // apply rate
				$('.stone__rowtextclass').on('blur',function(){
					var applyrate = $(this).val();
					if (applyrate.indexOf('.') !== -1) {
						var split_val = applyrate.split('.');
						if(split_val[1].length > rate_round){
							alertpopup('Please enter proper apply rate based on rate round value');
							$(this).val('');
							return false;
						}
					}
				});
			}
		},
	});
}
//get the all row ids in grid
function getgridallrowids(gridname) {
	var rowids =  [];
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		stonerateval = $(e).find(".purchaserate-class").text();
		applyrateval = $(e).find(".rowtext").val();
		rowids.push({	
			stoneid:datarowid,
			basicrate:stonerateval,
			applyrate:applyrateval
		});
	});
	return rowids;
}
{ //crud operations
	//stone addons based submit
	function addform() {
		var formdata = $("#stoneaddonsform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var gridData =getgridallrowids('stonechargegrid');
		var allstoneid =getselectedrowids('stonechargegrid');
		var stoneaddongriddata = JSON.stringify(gridData);
		setTimeout(function() {
			$("#processoverlay").show();
		},25);
		$.ajax({
				url:base_url+"Stoneaddons/stonechargecreate",
				data: "datas=" + datainformation+amp+"stoneaddongriddata="+stoneaddongriddata+amp+"allstoneid="+allstoneid,
				type:'POST',
				async:false,
				cache:false,
				success :function(msg) {
					resetFields();
					refreshgrid();
					setTimeout(function() {
						$("#processoverlay").hide();
							},25);
					if(msg == 'SUCCESS'){
						alertpopup(savealert);
						addslideup('stoneaddonsformadd','stoneaddonscreationview');
					}else{
						setTimeout(function() {
							$("#processoverlay").hide();
								},25);
				}
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
				}
			});
     }
	function retrive(datarowid){
		resetFields();
		firstfieldfocus();
		var elementsname=['4','stonechargeprimaryid','stonechargeprimaryname','stonecharge','setdefault'];
		$.ajax(
				{
					url:base_url+"Stoneaddons/stoneaddonsretrieve?primaryid="+datarowid, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data)
					{ 
						var txtboxname = elementsname;
						var dropdowns = [];
						textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
						$('#stonechargeid').val(data.stonechargeprimaryid);
						Materialize.updateTextFields();
						setTimeout(function(){
						stonechargegrid();
						},25);
						if(data.setdefault == 'Yes'){
							$('#setdefaultcboxid').prop('checked',true);
							$('#setdefault').val(data.setdefault);
						}else {
							$('#setdefaultcboxid').prop('checked',false);
							$('#setdefault').val(data.setdefault);
						}
						if(clone == 1){
							$('#stonecharge').attr('data-primaryid','');
						}else{
							$('#stonecharge').attr('data-primaryid',datarowid);
						}
						$("#txteditchargeid").val(datarowid);
						//For Keyboard Shortcut Variables
						addformupdate = 1;
						viewgridview = 0;
						addformview = 1;
					}
				}); 
	
    }
	function updateform()
	{
		var formdata = $("#stoneaddonsform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var gridData =getgridallrowids('stonechargegrid');
		var allstoneid =getselectedrowids('stonechargegrid');
		var stoneaddongriddata = JSON.stringify(gridData);
		var datarowid = $("#txteditchargeid").val();
		setTimeout(function(){
			$('#processoverlay').show();
		},10);
	    $.ajax({
	        url: base_url + "Stoneaddons/stoneaddonsupdate",
	        data: "datas=" + datainformation+amp+"primaryid="+datarowid+amp+"stoneaddongriddata="+stoneaddongriddata+amp+"allstoneid="+allstoneid,
	        type: "POST",
			async:false,
			cache:false,
	        success: function(msg) 
	        {
				var nmsg =  $.trim(msg);
				addslideup('stoneaddonsformadd','stoneaddonscreationview');
				refreshgrid();
	            if (nmsg == 'SUCCESS'){	
						 $("#txteditchargeid").val('');
				setTimeout(function(){
						$('#processoverlay').hide();
					},10);
					alertpopup(savealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},10);
					alertpopup('error');
	            }
	          //For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
	        },
	    });
	}
}
function stoneaccountcharge() {
	var form = $("#stoneaccountform").serialize();
	var amp = '&';
	var datainformation = amp + form;
	var account= $('#account').val();
	var accountgroup = $('#accountgroup').find('option:selected').val();
	var chargeid = $('#stoneaddonsgrid div.gridcontent div.active').attr('id');
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
	$.ajax(
    {
        url: base_url +"Stoneaddons/stoneaccountcharge",
        data: "datas=" + datainformation+amp+"account="+account+amp+"chargeid="+chargeid+amp+"accountgroup="+accountgroup,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) 
        {
			if(msg == 'SUCCESS'){
				refreshgrid();
				$("#account").select2('val','');
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(savealert);
				clearform('stoneaccountform');
				$('#stoneaccountoverlay').fadeOut();
			}
			else{
				alertpopup('error');
			}
        },
    });
}
function getchargedetails(datarowid) {
	$('#account').empty();
	$.ajax(
			{
				url:base_url+"Stoneaddons/stonechargedetails?primaryid="+datarowid, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{  
					var i=0;
					$.each(data, function(index) {
					
					 $('#account')
						.append($("<option></option>")
						.attr("value",data[index]['accountid'])
						.text(data[index]['accountname']));
					 i++;
					});
				 $('#accountstonecharge').val(data.chargename);
				}
			});
}
function getchargedetailssetdd(datarowid) {
	$.ajax(
			{
				url:base_url+"Stoneaddons/stonechargedetailsset?primaryid="+datarowid, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{ 
					if(data == ''){
						$('#account').select2('val','');
					}else{
					    var accountids = data;
					    var accountid = accountids.split(',');
					    $('#account').select2('val',accountid).trigger('change');
					}
				 }
			});
}
function getaccountgroup(datarowid) {
	$('#accountgroup').empty();
	$('#accountgroup').append($("<option></option>"));
	$('#accountgroup').append($("<option value='1'>Select</option>"));
	$.ajax(
			{
				url:base_url+"Stoneaddons/getaccountgroup", 
				dataType:'json',
				data : {primaryid : datarowid},
				type: "POST",
				async:false,
				cache:false,
				success: function(data)
				{  
					var i=0;
					$.each(data, function(index) {
					 $('#accountgroup')
						.append($("<option></option>")
						.attr("value",data[index]['accountgroupid'])
						.text(data[index]['accountgroupname']));
					 i++;
					});
				 }
			});
}
function setaccountgroup(datarowid) {
	$.ajax({
				url:base_url+"Stoneaddons/setaccountgroup", 
				dataType:'json',
				data : {primaryid : datarowid},
				type: "POST",
				async:false,
				cache:false,
				success: function(data)
				{
					if(data == ''){
						$('#accountgroup').select2('val','').trigger('change');
					}else{
					    $('#accountgroup').select2('val',data).trigger('change');
					}
				}
			});
}
function applyratevalidation(){
	var textregex=/^\d*\.?\d*$/;
	var applyrate='';
	var basicrate='';
	var flag=1;
	var zeroflag=0;
	$('#stonechargegrid div.gridcontent .rowtext').each(function(){
		applyrate=$(this).val();
		basicrate  = $(this).parent().prev('li').prev('li').text();
		if(textregex.test(applyrate))
			{ 
		
				if(parseFloat(applyrate)>=parseFloat(basicrate))
				{ flag = 1;
				   zeroflag=1;
				}
				else
				{  flag = 0;
				    zeroflag=0;
				   return false; 
				}			
			}
			else
			{ flag = 0;
			  zeroflag = 0;
			  return false; 
			}
	});
		if(zeroflag==1) {
		return true;
		}
		else if(flag == 1 && zeroflag==1)
        {
			return true;
		}
		else
		{return false;
		}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#stoneaddonspgnum li.active').data('pagenum');
		var rowcount = $('ul#stoneaddonspgnumcnt li .page-text .active').data('rowcount');
		stoneaddonsgrid(page,rowcount);
	}
}
//chargenamecheck
function chargenamecheck() {
	var primaryid = $("#stonechargeprimaryid").val();
	var accname = $("#stonecharge").val();
	var elementpartable = 'stonecharge';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Charge name already exists!";
				}
			}else {
				return "Charge name already exists!";
			}
		} 
	}
}