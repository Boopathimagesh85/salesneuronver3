$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		discountauthorizationaddgrid();
		firstfieldfocus();
		discountauthorizationcrudactionenable();
	}
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	ACDELETE = 0;
	editstatus = 0;
	//hidedisplay
	$('#discountauthorizationdataupdatesubbtn,#groupcloseaddform').hide();
	$('#discountauthorizationsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			discountauthorizationaddgrid();
		});
	}
	{//validation for Add  
		$('#discountauthorizationsavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#discountauthorizationformaddwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#discountauthorizationformaddwizard").validationEngine( {
			onSuccess: function() {
				discountauthorizationaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#discountauthorizationdataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#discountauthorizationformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#discountauthorizationformeditwizard").validationEngine({
			onSuccess: function() {
				discountauthorizationupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$("#discountauthorizationreloadicon").click(function() {
			clearform('cleardataform');
			$('#discountauthorizationaddform').find('input, textarea, button, select').attr('disabled',true);
			$('#discountauthorizationaddform').find("select").select2('disable');
			storagerefreshgrid();
			$('#discountauthorizationdataupdatesubbtn').hide();
			$('#discountauthorizationsavebutton').show();
			$("#deleteicon").show();
		});
		$( window ).resize(function() {
			maingridresizeheightset('discountauthorizationaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	
	{//filter work
		//for toggle
		$('#discountauthorizationviewtoggle').click(function() {
			if ($(".discountauthorizationfilterslide").is(":visible")) {
				$('div.discountauthorizationfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.discountauthorizationfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#discountauthorizationclosefiltertoggle').click(function(){
			$('div.discountauthorizationfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#discountauthorizationfilterddcondvaluedivhid").hide();
		$("#discountauthorizationfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#discountauthorizationfiltercondvalue").focusout(function(){
				var value = $("#discountauthorizationfiltercondvalue").val();
				$("#discountauthorizationfinalfiltercondvalue").val(value);
				$("#discountauthorizationfilterfinalviewconid").val(value);
			});
			$("#discountauthorizationfilterddcondvalue").change(function(){
				var value = $("#discountauthorizationfilterddcondvalue").val();
				var fvalue = $("#discountauthorizationfilterddcondvalue option:selected").attr('data-ddid');
				discountauthorizationmainfiltervalue=[];
				if( $('#s2id_discountauthorizationfilterddcondvalue').hasClass('select2-container-multi') ) {
					discountauthorizationmainfiltervalue=[];
					$('#discountauthorizationfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    discountauthorizationmainfiltervalue.push($cvalue);
						$("#discountauthorizationfinalfiltercondvalue").val(discountauthorizationmainfiltervalue);
					});
					$("#discountauthorizationfilterfinalviewconid").val(value);
				} else {
					$("#discountauthorizationfinalfiltercondvalue").val(fvalue);
					$("#discountauthorizationfilterfinalviewconid").val(value);
				}
			});
		}
		discountauthorizationfiltername = [];
		$("#discountauthorizationfilteraddcondsubbtn").click(function() {
			$("#discountauthorizationfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#discountauthorizationfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(discountauthorizationaddgrid,'discountauthorization');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
			$("#categoryid").val("");
			$("#employeeid").prop('disabled',false);
		});
	}
	{//storage hierarchy
		$('#discountauthorizationlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentdiscountauthorization').val(name);
			$('#categoryid').val(listid);
		});	
	}
	$("#password").change(function() {
		if($("#password").val()){
			oldpasswordcheck();
		}		
	});
	/* $("#discountvalue").change(function() {
		var value = $("#discountvalue").val();
		if($("#discountvalue").val()){
			var discounttypeid = $("#discounttypeid").val();
			if(discounttypeid == 3){
				if(parseFloat(value) > 100){
					$("#discountvalue").val('');
					alertpopup('Please enter the discount value properly');
				}
			}
		}		
	}); */
	$("#creditpercent").change(function() {
		var value = $("#creditpercent").val();
		if($("#creditpercent").val()){
			if(parseFloat(value) > 100){
				$("#creditpercent").val('');
				alertpopup('Please enter the credit percent properly');
			}
		}		
	});
	$("#metalid").change(function(){
		   if(editstatus == 0){
			 $("#purityid").select2('val','');
		   }
			$("#purityid option").addClass("ddhidedisplay");
			$("#purityid option").prop('disabled',true);
			var metalid = $(this).val();		
			$("#purityid option[data-metalid='"+metalid+"']").removeClass("ddhidedisplay");
			$("#purityid option[data-metalid='"+metalid+"']").prop('disabled',false);
			editstatus = 0;
	});
	$('#employeeid').change(function(){
		var val = $(this).val();
		counterassignuservalid(val,'employeeid','discountauthorization');
	});
	$('#discountmodeid').change(function(){
		var val = $(this).val();
		if(val == 3){
			$('#metalid,#purityid').select2('val','');
			$('#categoryid').val('');
			$('#parentdiscountauthorization').val('');
			$('#categoryiddivhid,#metaliddivhid,#purityiddivhid').hide();
		}else if(val == 2){
			$('#categoryiddivhid,#metaliddivhid,#purityiddivhid').show();
		}
	});
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function discountauthorizationaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#discountauthorizationaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#discountauthorizationsortcolumn").val();
	var sortord = $("#discountauthorizationsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.discountauthorizationheadercolsort').hasClass('datasort') ? $('.discountauthorizationheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.discountauthorizationheadercolsort').hasClass('datasort') ? $('.discountauthorizationheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.discountauthorizationheadercolsort').hasClass('datasort') ? $('.discountauthorizationheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = '110';
	if(userviewid != '') {
		userviewid= '192';
	}
	var filterid = $("#discountauthorizationfilterid").val();
	var conditionname = $("#discountauthorizationconditionname").val();
	var filtervalue = $("#discountauthorizationfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=discountauthorization&primaryid=discountauthorizationid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#discountauthorizationaddgrid').empty();
			$('#discountauthorizationaddgrid').append(data.content);
			$('#discountauthorizationaddgridfooter').empty();
			$('#discountauthorizationaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('discountauthorizationaddgrid');
			{//sorting
				$('.discountauthorizationheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.discountauthorizationheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.discountauthorizationheadercolsort').hasClass('datasort') ? $('.discountauthorizationheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.discountauthorizationheadercolsort').hasClass('datasort') ? $('.discountauthorizationheadercolsort.datasort').data('sortorder') : '';
					$("#discountauthorizationsortorder").val(sortord);
					$("#discountauthorizationsortcolumn").val(sortcol);
					var sortpos = $('#discountauthorizationaddgrid .gridcontent').scrollLeft();
					discountauthorizationaddgrid(page,rowcount);
					$('#discountauthorizationaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('discountauthorizationheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					discountauthorizationaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#discountauthorizationpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					discountauthorizationaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#discountauthorizationaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#discountauthorizationpgrowcount').material_select();
		},
	});
}
function discountauthorizationcrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#discountauthorizationdataupdatesubbtn').hide();
			$('#discountauthorizationsavebutton').show();
			$("#employeeid,#discountmodeid,#discounttypeid,#metalid,#purityid,#userroleid").prop('disabled',false);
			$('.dl-menuwrapper button').prop("disabled", "");
			$('#categoryiddivhid,#metaliddivhid,#purityiddivhid').hide();
			passwordvalidate();
			loadpurity();
			$("#discounttypeid").select2('val',3).trigger('change');
			$('#categoryid').val('');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#discountauthorizationaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#discountauthorizationdataupdatesubbtn').show();
			$('#discountauthorizationsavebutton').hide();
			$('#discountmodeid,#discounttypeid,#metalid,#purityid,#userroleid').prop('disabled',true);
			$('.dl-menuwrapper button').prop("disabled", "disabled");
			passwordvalidate();
			discountauthorizationeditformdatainformationshow(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#discountauthorizationaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			$("#discountauthorizationprimarydataid").val(datarowid);
			combainedmoduledeletealert('discountauthorizationdeleteyes');
			if(ACDELETE == 0) {
				$("#discountauthorizationdeleteyes").click(function(){
					var datarowid = $("#discountauthorizationprimarydataid").val();
					discountauthorizationcorddelete(datarowid);
				});
			}
			ACDELETE =1;
		} else {
			alertpopup("Please select a row");
		}	
	});
	// discount type change
	$('#discounttypeid').change(function(){
		var val = $(this).find('option:selected').val();
		$('#discountvalue').val('');
		discountvaluevalidate(val);
	});
}
{//refresh grid
	function discountauthorizationrefreshgrid() {
		var page = $('ul#discountauthorizationpgnum li.active').data('pagenum');
		var rowcount = $('ul#discountauthorizationpgnumcnt li .page-text .active').data('rowcount');
		discountauthorizationaddgrid(page,rowcount);
	}
}
//new data add submit function
function discountauthorizationaddformdata() {
	var discauthpassword = $("#dapassword").val();
	var userrole = $.trim($("#userroleid").val()); 
	userrole = ltrim(userrole,",");
	//userrole = userrole.replace(/^,/,'');
	//userrole.split(',').slice(1);
	//alert(userrole);
	if($('#discountmodeid').val() == 3) {
		$('#metalid,#purityid').select2('val','');
	}
	var formdata = $("#discountauthorizationaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Discountauthorization/newdatacreate",
        data: "datas=" + datainformation+amp+"discauthpassword="+discauthpassword+amp+"userroleid="+userrole,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				discountauthorizationrefreshgrid();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'EXIST') {
				alertpopup('This combination already created');
			}
        },
    });
}
function ltrim(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
}
//old information show in form
function discountauthorizationeditformdatainformationshow(datarowid) {
	var discountauthorizationelementsname = $('#discountauthorizationelementsname').val();
	var discountauthorizationelementstable = $('#discountauthorizationelementstable').val();
	var discountauthorizationelementscolmn = $('#discountauthorizationelementscolmn').val();
	var discountauthorizationelementpartable = $('#discountauthorizationelementspartabname').val();
	$.ajax(	{
		url:base_url+"Discountauthorization/fetchformdataeditdetails?discountauthorizationprimarydataid="+datarowid+"&discountauthorizationelementsname="+discountauthorizationelementsname+"&discountauthorizationelementstable="+discountauthorizationelementstable+"&discountauthorizationelementscolmn="+discountauthorizationelementscolmn+"&discountauthorizationelementpartable="+discountauthorizationelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				discountauthorizationrefreshgrid();
			} else {
				var txtboxname = discountauthorizationelementsname + ',discountauthorizationprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = discountauthorizationelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				editstatus = 1;
				loadpurity();
				setTimeout(function(){
					$('#metalid').trigger('change');
					$('#purityid').select2('val',data.purityid);
					Materialize.updateTextFields();
				},200);
				$("#employeeid").prop('disabled',true);
				discountvaluevalidate(data.discounttypeid);
				Materialize.updateTextFields();
			}
		}
	});
	if($('#discountmodeid').val() == 2){ // item level
	    $('#categoryiddivhid,#metaliddivhid,#purityiddivhid').show();
		setTimeout(function(){
			gettheparentcatalog(datarowid);
		},100);
	}else if($('#discountmodeid').val() == 3){ //bill level
	    $('#categoryiddivhid,#metaliddivhid,#purityiddivhid').hide();
	}
	$('#discountauthorizationdataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#discountauthorizationsavebutton').hide();
}
//udate old information
function discountauthorizationupdateformdata() {
	var parentdiscountauthorizationid=$("#parentdiscountauthorizationid").val();
	var editdiscountauthorization=$("#discountauthorizationprimarydataid").val();
		if(parentdiscountauthorizationid!=editdiscountauthorization){
			var discauthpassword = $("#dapassword").val();
			var formdata = $("#discountauthorizationaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
		    $.ajax({
		        url: base_url + "Discountauthorization/datainformationupdate",
		        data: "datas=" + datainformation+"&discauthpassword="+discauthpassword,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
						$('#discountauthorizationdataupdatesubbtn').hide();
						$('#discountauthorizationsavebutton').show();
						discountauthorizationrefreshgrid();
						$("#groupsectionoverlay").removeClass("effectbox");
						$("#groupsectionoverlay").addClass("closed");
						resetFields();
						alertpopup(savealert);			
		            }else if(nmsg == 'FAILURE'){
						alertpopup("Check your Parent name mapping. ");
					}else if(nmsg == 'LEVEL') {
						alertpopup('Maximum level reached !!! cannot create category further.');
					}
		        },
		    });
	}
	 else{
		alertpopup("You cant map the same name as parent name");
	}
}
//udate old information
function discountauthorizationcorddelete(datarowid) {
	var discountauthorizationelementstable = $('#discountauthorizationelementstable').val();
	var discountauthorizationelementspartable = $('#discountauthorizationelementspartabname').val();
    $.ajax({
        url: base_url + "Discountauthorization/deleteinformationdata?discountauthorizationprimarydataid="+datarowid+"&discountauthorizationelementstable="+discountauthorizationelementstable+"&discountauthorizationparenttable="+discountauthorizationelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	discountauthorizationrefreshgrid();
            	$("#parentdiscountauthorizationid").val("");
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	discountauthorizationrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//load the parent id
function gettheparentcatalog(datarowid){
	$.ajax({
		url:base_url+"Discountauthorization/getcatalogparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			$('#categoryid').val(data.parentcatalogid);
			$('#parentdiscountauthorization').val(data.catalogname);
		}
	});
}
//old password check
function oldpasswordcheck() {
	var oldpassword = $("#password").val();
	nmsg = 2;
	if(oldpassword != "") {
		$.ajax({
			url:base_url+"Discountauthorization/oldpasswordcheck?oldpassword="+oldpassword,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "Fail") {
			return "*You entered the wrong password";
		}
	}
}
function loadpurity() {
	$("#purityid").empty();
	$('#purityid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Discountauthorization/loadpurity",
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
				$.each(msg, function(index) {
				$("#purityid")
				.append($("<option></option>")
				.attr("data-metalid",msg[index]['metalid'])
				.attr("data-purityidhidden",msg[index]['purityname'])
				.attr("value",msg[index]['purityid'])
				.text(msg[index]['purityname']));
			});
		},
	});
}
// check confirm password
function passwordvalidate() {
		$('#confirmpassword').removeClass('validate[required,minSize[6],maxSize[20]]');
		$('#confirmpassword').addClass('validate[required,minSize[6],maxSize[20],equals[dapassword]]');
}
function discountvaluevalidate(val){
		$('#discountvalue').removeClass('validate[required,custom[number],decval[2],maxSize[100]]');
		if(val == 2) { // amount
			$('#discountvalue').removeClass('validate[required,custom[number],decval[2],maxSize[100],min[0.1],max[100]]');
			$('#discountvalue').addClass('validate[required,custom[number],decval[2],maxSize[100],min[1]]');
		}else if(val == 3) { // percentage
			$('#discountvalue').removeClass('validate[required,custom[number],decval[2],maxSize[100],min[1]]');
			$('#discountvalue').addClass('validate[required,custom[number],decval[2],maxSize[100],min[0.1],max[100]]');
		}
}