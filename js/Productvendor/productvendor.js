$(document).ready(function() {
	PRODUCTADDONDELETE = 0;
	$(document).foundation();
	// Maindiv height width change
	maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);
	weight_round = $('#weight_round').val();
	 $('#groupcloseaddform').hide();
	 $('#productaddonviewtoggle').hide();
	{// Grid Calling Functions
		productvendorgrid();
	}
	$( window ).resize(function() {
		 sectionpanelheight('groupsectionoverlay');
		 maingridresizeheightset('productvendorgrid');
	 });
   {// For touch
		fortabtouch = 0;
	}
	{//product charge validate	
		//validation for product charge Add  
		$('#productvendoradd').click(function() {
			$("#productvendoraddvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#productvendoraddvalidate").validationEngine( {
			onSuccess: function() {
			  productvendoradd();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//update product information.
		$('#productvendorupdate').click(function() {
			$("#productvendorupdatevalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#productvendorupdatevalidate").validationEngine({
			onSuccess: function() {
				productvendorupdate();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
		$("#addicon").click(function(){
			clearform('productvendorform');
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$("#chargeproductid,#addon_purityid,#accounttypeid").prop('readonly',false);
			$("#s2id_chargeproductid").select2('focus');
			$('#productvendorupdate').hide();
			$('#productvendoradd').show();
			Materialize.updateTextFields();
		});
		$("#editicon").click(function(){			
			var datarowid = $('#productvendorgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				$("#productid,#sizemasterid,#vendorid").attr('readonly',true);
				productvendorretrive(datarowid);
				$('#productvendorupdate').show();
				$('#productvendoradd').hide();
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Delete
		$("#deleteicon").click(function(){			
			var datarowid = $.trim($('#productvendorgrid div.gridcontent div.active').attr('id'));
			if(datarowid != ''){
				combainedmoduledeletealert('addondeleteyes','Are you sure,you want to delete this addon?');
				if(PRODUCTADDONDELETE == 0) {
					$("#addondeleteyes").click(function(){				
						productchargedelete();
					});
				}
				PRODUCTADDONDELETE = 1;
			} else{
				alertpopup(selectrowalert);
			}
		});
	}
	$("#productid").change(function() {
		var productid = $("#productid").val();
		var size = $("#productid").find('option:selected').data('size');
			if(size == 'Yes') {
				$("#sizemasteriddivhid").show();
				$("#sizemasterid option").addClass("ddhidedisplay");
				$("#sizemasterid").attr("data-validation-engine","validate[required]");
				$("#sizemasterid option[data-productid="+productid+"]").removeClass("ddhidedisplay");
			} else {
				$("#sizemasteriddivhid").hide();
				$("#sizemasterid").attr("data-validation-engine","");
			}
	});
	$("#productvendortoweight").change(function(){
		setzero(['productvendorfromweight','productvendortoweight']);
		var weight = parseInt($(this).val());
		if(weight){
			var toweight = parseInt($("#productvendortoweight").val());
			var fromweight = parseInt($("#productvendorfromweight").val());
			if(toweight < fromweight){
				alertpopup("To weight should be greater than the from weight");
				$(this).val(" ");
			} else {
				var productid = $("#productid").val();
				var vendorid = $("#accountid").val();
				var sizemasterid = $("#sizemasterid").val();
				checkweightexist(productid,vendorid,sizemasterid,toweight,fromweight);
			}
		}
	});
	$('.addsectionclose').click(function(){
		$("#productaddonsectionoverlay").addClass("closed");
		$("#productaddonsectionoverlay").removeClass("effectbox");
	});
	$('#alertsdoublecloseproductrol').click(function(){
		$("#editproductvendorid").val('');
	});
	$('#alertsdoubleeditproductvendor').click(function(){
	   $('#productrolalertsdouble').fadeOut();
	   $('#productvendorupdate').show();
	   $('#productvendoradd').hide();
	   productvendorretrive($('#editproductvendorid').val());
	});
    //filter
	$("#productvendorfilterddcondvaluedivhid").hide();
	$("#productvendorfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#productvendorfiltercondvalue").focusout(function(){
			var value = $("#productvendorfiltercondvalue").val();
			$("#productvendorfinalfiltercondvalue").val(value);
			$("#productvendorfilterfinalviewconid").val(value);
		});
		$("#productvendorfilterddcondvalue").change(function(){
			var value = $("#productvendorfilterddcondvalue").val();
			productvendormainfiltervalue=[];
			$('#productvendorfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    productvendormainfiltervalue.push($mvalue);
				$("#productvendorfinalfiltercondvalue").val(productvendormainfiltervalue);
			});
			$("#productvendorfilterfinalviewconid").val(value);
		});
	}
    productvendorfiltername = [];
	$("#productvendorfilteraddcondsubbtn").click(function() {
		$("#productvendorfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#productvendorfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
		dashboardmodulefilter(productvendorgrid,'productvendor');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
	/* Product vendor Add	*/
	function productvendoradd() {
		var formdata = $("#productvendorform").serialize();	
		var amp = '&';	
		var datainformation = amp + formdata;
		$('#processoverlay').show();
		$.ajax( {
	        url: base_url +"Productvendor/createproductvendor",
	        data: "datas=" + datainformation,
			type: "POST",
			async:false,
	        success: function(msg)  {
				var nmsg =  $.trim(msg);
                if (nmsg == 'SUCCESS'){
					$('#processoverlay').hide();
					$(".addsectionclose").trigger("click");
				} else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
				producttyperefreshgrid();		
	        },
	    });
	}
	/* Product vendor delete	*/
	function productchargedelete() {	
	    var primaryid = $('#productvendorgrid div.gridcontent div.active').attr('id');
		$('#processoverlay').show();
		$.ajax({
	        url: base_url + "Productvendor/productchargedelete?primaryid="+primaryid,
			async:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
				producttyperefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
			    if (nmsg == 'SUCCESS') {	
					$('#processoverlay').hide();
					alertpopup(deletealert);
	            } else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
	            }
	        },
	    });
	}
	/* Product vendor retrive	*/
	function productvendorretrive(datarowid) {
		$('#processoverlay').show();
		var elementsname = ['5','productid','sizemasterid','productvendorfromweight','productvendortoweight','accountid'];
		//form field first focus
		firstfieldfocus();
		$.ajax( {
			url:base_url+"Productvendor/productvendorretrieve?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			success: function(data) { 
				$('#processoverlay').hide();
				var txtboxname = elementsname;
				var dropdowns = ['3','productid','sizemasterid','accountid'];
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				$('#primaryproductvendorid').val(datarowid);
				$('#productid').find('option:selected').select2('val',data.productid).trigger('change');
				Materialize.updateTextFields();				
			}
		});
	}
	/* Product vendor update	*/
	function productvendorupdate() {
		var formdata = $("#productvendorform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid = $('#primaryproductvendorid').val();
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
	        url: base_url + "Productvendor/productvendorupdate",
	        data: "datas=" + datainformation +"&primaryid="+ datarowid,
			type: "POST",
			async:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
				$("#productid,#sizemaster,#accountid").attr('readonly',false);
				$("#productvendorsectionoverlay").addClass("closed");
		        $("#productvendorsectionoverlay").removeClass("effectbox");
				clearform('productvendorform');
	            producttyperefreshgrid();
				$('#productvendorupdate').hide();
				$('#productvendoradd').show();	
				 if (nmsg == 'SUCCESS'){
					 $(".addsectionclose").trigger("click");
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(updatealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
				}             
	        },
	    });
	}
{// Product Type View Grid
	function productvendorgrid(page,rowcount) {
		var accointid='';
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 10 : rowcount;
		var wwidth = $( window ).width();
		var wheight = $( window ).height();
		/*col sort*/
		var sortcol = $("#productvendorsortcolumn").val();
		var sortord = $("#productvendorsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.productvendorheadercolsort').hasClass('datasort') ? $('.productvendorheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.productvendorheadercolsort').hasClass('datasort') ? $('.productvendorheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.productvendorheadercolsort').hasClass('datasort') ? $('.productvendorheadercolsort.datasort').attr('id') : '0';	
		var filterid = $("#productvendorfilterid").val();
		var conditionname = $("#productvendorconditionname").val();
		var filtervalue = $("#productvendorfiltervalue").val();
		var userviewid = 202;
		var viewfieldids = 139;
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=productvendor&primaryid=productvendorid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#productvendorgrid').empty();
				$('#productvendorgrid').append(data.content);
				$('#productvendorgridfooter').empty();
				$('#productvendorgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('productvendorgrid');
				{//sorting
					$('.productvendorheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.productvendorheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.productvendorheadercolsort').hasClass('datasort') ? $('.productvendorheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.productvendorheadercolsort').hasClass('datasort') ? $('.productvendorheadercolsort.datasort').data('sortorder') : '';
						$("#chitamountsortorder").val(sortord);
						$("#chitamountsortcolumn").val(sortcol);
						productvendorgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('productvendorheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						productvendorgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#productvendorpgrowcount').change(function() {
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						productvendorgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$(".gridcontent").click(function(){
					var datarowid = $('#productvendorgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#productvendorpgrowcount').material_select();
			},
		});
	}
	{//refresh grid
		function producttyperefreshgrid() {
			var page = $('ul#productpgnum li.active').data('pagenum');
			var rowcount = $('ul#productpgnumcntpgnum li .page-text .active').data('rowcount');
			productvendorgrid(page,rowcount);
		}
	}
}
//check weight already exist
function checkweightexist(productid,accountid,sizemasterid,toweight,fromweight){
	var productname=$('#productid').find('option:selected').data('productname');
	var accountname=$('#accountid').find('option:selected').data('accountname');
	var sizemastername=$('#sizemasterid').find('option:selected').data('sizemastername');
	var primaryid = $("#primaryproductvendorid").val();
	$.ajax({
	    url: base_url + "Productvendor/checkweightexist",
		 data: "productid=" + productid +"&accountid="+ accountid+"&sizemasterid="+sizemasterid+"&toweight="+toweight+"&fromweight="+fromweight+"&primaryid="+primaryid,
		type: "POST",
		dataType:'json',
		async:false,
		success: function(msg) {
			if(msg['count'] > 0){
				$('#alertproductname').text(productname);
				$('#alerttypename').text(accountname);
				$('#alertsizemastername').text(sizemastername);
				$('#alertfromweight').text(fromweight);
				$('#alerttoweight').text(toweight);
				setTimeout(function(){
					$('#productrolalertsdouble').fadeIn();
					$('#toweight').val('');
					$('#toweight').focus();
				},25);
				$('#editproductvendorid').val(msg['primaryproductvendorid']);
			}
		}
	});		
}