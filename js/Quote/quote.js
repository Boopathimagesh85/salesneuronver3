$(document).ready(function() {
	METHOD='ADD';
	PRECISION =0;
	PRODUCTTAXABLE='Yes';
	CURRENT_MODE=''; //to identify the form is add /edit
	PRICBOOKCONV_VAL = 0;
	QUANTITY_PRECISION=0; //quote precision
	MODULEID = 0;
	UPDATEID = 0;
	{// Foundation Initialization
        $(document).foundation();
    }
    {//Grid Calling
        quoteaddgrid();
        getmoduleid();
		/*crud action*/
		crudactionenable();
    }
    $( window ).resize(function() {
		maingridresizeheightset('quoteaddgrid');
		sectionpanelheight('quotationsprooverlay');
	});
    {//inner-form-with-grid
		$("#quotationsproingridadd1").click(function(){
			sectionpanelheight('quotationsprooverlay');
			$("#quotationsprooverlay").removeClass("closed");
			$("#quotationsprooverlay").addClass("effectbox");
			clearform('gridformclear');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#quotationsprocancelbutton").click(function(){
			$("#quotationsprooverlay").removeClass("effectbox");
			$("#quotationsprooverlay").addClass("closed");
			$('#quotationsproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#quotationsproaddbutton').show();//display the ADD button(inner-productgrid)*/	
		});
	}
    $('#currencyconverticon').removeClass("hidedisplay");
    {
		$('#quotationsproupdatebutton').removeClass('updatebtnclass');
		$('#quotationsproupdatebutton').removeClass('hidedisplayfwg');
		$('#quotationsproaddbutton').removeClass('addbtnclass');
		$("#quotationsproupdatebutton").hide();
		//enable readonly for overlay currency-pricebook
		$("#pricebookcurrency,#currentcurrency").select2("readonly",true);
	}
	{//Enables the Edit Buttons
		$('#quotationsproingrideditspan1').removeAttr('style');
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	$(".dropdownchange").change(function(){
		var dataattrname = ['dataidhidden'];
		var dropdownid =['employeeidddid'];
		var textboxvalue = ['employeetypeid'];
		var selectid = $(this).attr('id');
		var index = dropdownid.indexOf(selectid);
		var selected=$('#'+selectid+'').find('option:selected');
		var datavalue=selected.data(''+dataattrname[index]+'');
		$('#'+textboxvalue[index]+'').val(datavalue);
		if(selectid == 'employeeidddid') {
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data('ddid');
			$('#employeeid').val(datavalue);
		}
	});
	{//convert the quote to salesorder
		$("#converticon").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var stage=quotestage(datarowid);
				if(stage == 33) {//Booked				
					sessionStorage.setItem("convertquotetoinvoice",datarowid);
					window.location = base_url+'Invoice';
				} else {
					alertpopup("Only booked quotation can be converted");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Formclear resetting 
		$("#formclearicon").click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setdefaultproperty();
			cleargriddata('quotationsproaddgrid1');
			froalaset(froalaarray);
			$('#quotationsproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#quotationsproaddbutton').show();	//display the ADD button(inner-productgrid) 
			METHOD = 'ADD';
			//empty option of depedency dropdowns
			$('#opportunityid,#contactid').empty();
		});
	}
	//hide button
	$("#quotationsproingridexport1").hide();	
	{// Account change	
		$("#accountid").change(function() {
			var accountid=$(this).val();			
			getaddress(accountid,'account','Billing');		
			getaddress(accountid,'account','Shipping');
			$('#billingaddresstype,#shippingaddresstype').select2('val','2');	
			if(accountid != ''){
				$('#opportunityid').empty();
				var table = 'opportunity';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+accountid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
						var newddappend="";
						var newlength=data.length;
						for(var m=0;m < newlength;m++) {
							newddappend += "<option value ='" +data[m]['id']+ "'>"+data[m]['name']+"</option>";
						}						
						$('#opportunityid').append(newddappend);						
				  }    
				  $('#opportunityid').select2('val','').trigger('change');
				},
				});
				$('#leadcontacttype').trigger('change');
			}else{
				var leadcontacttype = $('#leadcontacttype').val();
				appendleadcontact(leadcontacttype,'contactid');
				var table = 'opportunity';				
				$.ajax({
				url:base_url+'Quote/accountdependddata?table='+table+'&id='+accountid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
				  if((data.fail) == 'FAILED') {
				  } else {
						var newddappend="";
						var newlength=data.length;
						for(var m=0;m < newlength;m++) {
							newddappend += "<option value ='" +data[m]['id']+ "'>"+data[m]['name']+"</option>";
						}						
						$('#opportunityid').append(newddappend);						
				  }    
				  $('#opportunityid').select2('val','').trigger('change');
				},
				});
			}
		});		
		$("#contactid").change(function() {			
			var contactid = $(this).val();
			if(checkValue(contactid) == true){ //on change of contact/lead this will set the related addres
				//reset the address if related to
				if(softwareindustryid != 2){
					var billaddtype = $('#billingaddresstype').val();
					var shipaddtype = $('#shippingaddresstype').val();
					if(billaddtype == 3){
						$('#billingaddresstype').select2('val','3').trigger('change');
					}
					if(shipaddtype == 3){
						$('#shippingaddresstype').select2('val','3').trigger('change');
					}
				}else{
					$('#patientid').val('');
					$('#subject').val('');
					$('#area').val('');
					$('#city').val('');
					$('#pincode').val('');
					$('#state').val('');	
					$('#mobile').val('');
					$('#email').val('');														
					$('#insurancenameid').select2('val','');
					$.ajax({
						url:base_url+'Base/fetchcontactvalue?contactid='+contactid,
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							$('#patientid').val(data.contactnumber);
							$('#subject').val(data.streetname);
							$('#area').val(data.primaryarea);
							$('#city').val(data.primarycity);
							$('#pincode').val(data.primarypincode);
							$('#state').val(data.primarystate);	
							$('#mobile').val(data.landlinenumber);
							$('#email').val(data.emailid);														
							$('#insurancenameid').select2('val',data.insurancenameid);
							Materialize.updateTextFields();
						}
					});			
				}
			}
		});
		//
		$("#pricebookid").change(function() {	
			var val= $(this).val();
			var currency = $('#currencyid').val();
			if(checkValue(val) == true){
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
				$.ajax({
				url:base_url+'Quote/pricebookcurrency?pricebook='+val+'&currency='+currency,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.status != 'FAILED'){
						if(currency == data.currencyid){
						} else  {	
							var conversionrate = data.rate; //to get conversion rate
							$('#currentcurrency').select2('val',currency).trigger('change').attr("disabled","disabled");
							$('#pricebookcurrency').select2('val',data.currencyid).trigger('change').attr("disabled","disabled");					
							$('#pricebook_currencyconv').val(conversionrate);							
							$('#pricebook_currencyconv').focus();							
							$("#pricebookcurrencyconvoverlay").fadeIn();
							Materialize.updateTextFields();
						}
					}
				},
				});
				$("#pricebookclickevent").hide();
			}else{
				$("#pricebookclickevent").show();
				var product = $('#productid').val();
				if(product != ''|| product != null){
					$("#productid").select2('val','');
					$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				}
			}
		});
	}
	{// Shipping billing address details fetching
		$("#billingaddresstype,#shippingaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
			Materialize.updateTextFields();
		});
		//shipping /billing hiding
		$("#billingaddresstype option[value='6']").remove();
		$("#shippingaddresstype option[value='5']").remove();
	}	
    {// Product change events 
    	$("#ordertypeid").change(function() {
    		if(softwareindustryid == 4){
    			var ordertype = $(this).val();
        		if(ordertype == 5){
        			$("#instockdivhid").addClass('hidedisplay');
        			$("#instock").removeClass('validate[custom[number],decval[3],maxSize[100]]');
        			$('#quantity').attr("readonly", true);
        			$("#durationid,#joiningdate,#expirydate").val("");
        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").removeClass('hidedisplay');
        			gridfieldhide('quotationsproaddgrid1',['instock']);
        			gridfieldshow('quotationsproaddgrid1',['durationidname','expirydate','joiningdate']);
        		}else{
        			$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").addClass('hidedisplay');
        			$('#quantity').attr("readonly",false);
        			$("#instockdivhid").removeClass('hidedisplay');
        			$("#instock").addClass('validate[custom[number],decval[3],maxSize[100]]');
        			gridfieldshow('quotationsproaddgrid1',['instock']);
        			gridfieldhide('quotationsproaddgrid1',['durationidname','durationid','expirydate','joiningdate']);
        		}
    		}    		
    		var product = $("#productid").val();
    		if(product){   
    			$('#productid').select2('val','').trigger('change');
    			$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
    			$("#productid").empty();
    		}
    	});
    	if(softwareindustryid == 4){
    		$('#joiningdate').change(function(){
    			dateexpiry();
    		});
    		$('#durationid').change(function(){
    			var joiningdate = $('#joiningdate').datepicker('getDate');
    			if(joiningdate){
    				dateexpiry();
    			}
    		});
    		function dateexpiry(){
    			var duration = $('#durationid').val();
    			var joiningdate = $('#joiningdate').datepicker('getDate');
    			var month = 0;
    			if(duration == 2){
    				month = 1;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 3){
    				month = 3;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 4){
    				month = 6;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}else if(duration == 5){
    				month = 12;
    				var CurrentDate = new Date(joiningdate);
    				CurrentDate.setMonth(CurrentDate.getMonth() + month);								
    				$('#expirydate').datetimepicker('setDate',CurrentDate);
    			}
    		}
    	}    	
    	if(softwareindustryid != 2){
	    	$("#productid").select2().on("select2-focus", function() {
	    		var productid = $("#productid").val();
	    		if(productid == ''|| productid == null){
	    			var ordertype = $('#ordertypeid').val();
	    			if(ordertype){
	    				$.ajax({
	    					url:base_url+"Base/getorderbasedproduct?ordertypeid="+ordertype+"&producthide=No",
	    					dataType:'json',
	    					async:false,
	    					cache:false,
	    					success:function(data) {
	    						$('#productid').empty();
	    						prev = ' ';
	    						var optclass = [];
	    						$.each(data, function(index) {
	    							var cur = data[index]['PId'];
	    							if(prev != cur ) {
	    								if(prev != " ") {
	    									$('#productid').append($("</optgroup>"));
	    								}
	    								if(jQuery.inArray(data[index]['optclass'], optclass) == -1) {
	    									optclass.push(data[index]['optclass']);
	    									$('#productid').append($("<optgroup  label='"+data[index]['optclass']+"' class='"+data[index]['optclass']+"'>")); 
	    								} 
	    								$("#productid optgroup[label='"+data[index]['optclass']+"']")
	        							.append($("<option></option>")
	        							.attr("class",data[index]['optionclass'])
	        							.attr("value",data[index]['value'])
	        							.attr("data-productidhidden",data[index]['data-productidhidden'])
	        							.text(data[index]['data-productidhidden']));
	    								prev = data[index]['PId'];
	    							}
	    							
	    						}); 
	    						$('#productid').select2('val','').trigger("change");
	    					}
	    				});
	    			}else{
	    				$('#productid').empty();
    					setTimeout(function(){
    						$("#productid").select2("close");
    						alertpopup("Select the Quote Type to Load the Product");
    					},100);
	    				
	    			}
	    		}    		
	    	});
    	}
		$("#productid").change(function(){
			var productval = $(this).find('option:selected').data('productidhidden');
			var ul = $('#quotationsproaddgrid1 div.gridcontent .inline-list');
			var lis = ul.children('li');
			var isInList = false;
			for(var i = 0; i < lis.length; i++){
			    if(lis[i].innerHTML === productval) {
			        isInList = true;
			        break;
			    }
			}
			if(isInList){
				 alertpopup("Product already Exist. Kindly choose another Product");
				 $('#productid').select2('val','');
				 $('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
			}else{
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#discountoverlay,#additionaloverlay,#taxoverlay').find('input:text').val('');
				$('#instock,#quantity,#unitprice,#sellingprice,#grossamount,#pretaxtotal,#chargeamount,#discountamount,#taxamount,#netamount').val('');
				$('#additionalcategory').select2('val','').trigger('change');
				var val=$(this).val();
				getproductdetails(val);
				Materialize.updateTextFields();
				$('.error').removeClass('error');
				$(".btmerrmsg").remove();
				$('.formError').remove();
				var inputs = $(this).closest('form').find(':input');
				inputs.eq( inputs.index(this)+ 1 ).focus();
			}			
		});
	}
	{// Overlay operations
		$("#discountamount").click(function(){			
			var product=checkVariable('productid');
			var discount_data = $('#discountdata').val();
			var parse_discount = $.parseJSON(discount_data);
			$("#discountoverlay").fadeIn();
			$("#discountclose").focus();
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#discounttypeid').select2('val','').trigger('change');
				$('#discountpercent').val('');		
				Materialize.updateTextFields();
			} else {								
				$('#discounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#discountpercent').val(parse_discount.value);
				Materialize.updateTextFields();
			}			
			$("#discountoverlay").fadeIn();			
			setTimeout(function()
			{
				discount_total();
				Materialize.updateTextFields();
				$("#discountclose").focus();
			},100);
		});
		$("#discounttypeid").change(function(){
			$('#discountpercent').val(0);
			setTimeout(function(){
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#discountpercent").focusout(function(){	
			setTimeout(function(){
				discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//discountrefresh-individual
		$("#discountrefresh").click(function(){
			$('#discounttypeid').select2('val','2').trigger('change');
			$('#discountpercent').val('');
			$('#singlediscounttotal').val('');			
		});
		//discountrefresh-group
		$("#groupdiscountrefresh").click(function(){
			$('#groupdiscounttypeid').select2('val','2').trigger('change');
			$('#groupdiscountpercent').val('0');
			$('#groupdiscounttotal').val('');
			Materialize.updateTextFields();
		});
		$("#groupdiscounttypeid").change(function(){
			$('#groupdiscountpercent').val(0);
			setTimeout(function() {
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		$("#groupdiscountpercent").focusout(function(){			
			setTimeout(function() {
				group_discount_total();
				Materialize.updateTextFields();
			},100);
		});
		//invite overlay fade out
		$("#discountclose").click(function() {
			$("#discountoverlay").fadeOut();
			var value=parseFloat($('#discountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#discounttypeid').select2('val','').trigger('change');
			}
			$('#pretaxtotal').focus();
		});
		//recurrence overlay fadeOut
		$("#taxclose").click(function() {
			productnetcalculate();	
			var taxid = $("#taxcatid").val();
			var product = $("#productid").val();		
			$("#taxoverlay").fadeOut();
		});
		$('#discountsubmit').click(function(e){
			$("#individualdiscountvalidation").validationEngine('validate');			
		});
		$("#individualdiscountvalidation").validationEngine({
			onSuccess: function() {									
				setTimeout(function(){
					setzero(['grossamount','pretaxtotal']); //sets zero on EMPTY					
					var discount_json ={}; //declared as object
					discount_json.typeid = $('#discounttypeid').val(); //discount typeid
					discount_json.value = $('#discountpercent').val();	//discount values		
					$('#discountdata').val(JSON.stringify(discount_json)); //saves discount data in hiddenfield(JSON object)
					var grossamount = $('#grossamount').val();
					var discount_amount=discountcalculation(discount_json,grossamount); //to calculate discount
					$('#discountamount').val(discount_amount);
					$('#quantity').trigger('focusout');		//after succesful discount trigger other events	
				},10);
				$("#discountoverlay").fadeOut();
			},
			onFailure: function() {
			}
		});	
	}
	{// On lead contact change 
		$("#leadcontacttype").change(function(){
			var val=$(this).val();	
			if(val == 2){
				appendleadcontact(val,'contactid');
				$('#leadiddivhid').hide();
				$('#contactiddivhid').show();
			}else if(val == 3){
				$('#leadiddivhid').show();
				$('#contactiddivhid').hide();
				appendleadcontact(val,'leadid');
			}			
			//reset the address if related to
			var billaddtype = $('#billingaddresstype').val();
			var shipaddtype = $('#shippingaddresstype').val();
			if(billaddtype == 3){
				$('#billingaddress,#billingpincode,#billingcity,#billingstate,#billingcountry').val('');
			}
			if(shipaddtype == 3){
				$('#shippingaddress,#shippingpincode,#shippingcity,#shippingstate,#shippingcountry').val('');
			}
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Toolbar click function	
		$("#quotationsproingriddel1").click(function(){
			var datarowid = $('#quotationsproaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {		
				if(METHOD == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('quotationsproaddgrid1',datarowid);
					var productrecords = $('#quotationsproaddgrid1 .gridcontent div.data-content div').length;
					var currentmode = $('#currentmode').val();
					if(productrecords == 0 ) {//&& currentmode == 1)
						$('#ordertypeid,#pricebookid,#taxmasterid,#additionalchargecategoryid').select2('val','');
						$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').select2("readonly", false);	
						$('#ordertypeid,#pricebookid,#taxmasterid,#additionalchargecategoryid').trigger("change");
						//Empty the Tax/Charge data-because its dependent
						$('#grouptaxgriddata,#groupchargegriddata,#grouptaxamount').val('');
						PRICBOOKCONV_VAL = 0;
						if(softwareindustryid != 2){
							$("#productid").empty();
						}						
						$("#pricebookclickevent").show();
					}
					gridsummeryvaluefetch('','','');
				}
			} else {
				alertpopup("Please select a row");
			}
		});	
		$("#cloneicon").click(function(){
			QUANTITY_PRECISION =0;
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
		        quoteprodaddgrid();
		        if(softwareindustryid != 2){
		        	chargesgrid();		    		
		        }
		        taxgrid();
				var stage=quotestage(datarowid);
				if(stage == 35){ //cancel
					alertpopup("This Quotation is Cancelled");
				} else {
					$("#processoverlay").show();
					quoteclonedatafetchfun(datarowid);	
					$("#pricebookclickevent").show();
					//for autonumber
					randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
					showhideiconsfun('editclick','acccreationformadd');
					$(".fr-element").attr("contenteditable", 'true');
					$("#quotationsproingriddel1").show();
				}	
				Operation = 0; //for pagination			
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#cancelicon").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = quotestatus(datarowid);
				if(status == 8){
					alertpopup("Quotation Converted !!! Cannot Change Status");
				}else{
					var stage = quotestage(datarowid);
				    if(stage == 33){
						alertpopup("Quotation Booked!!!Cannot Change To Cancel");
				    } else if(stage == 34){
				    	alertpopup("Quotation Lost!!!Cannot Change To Cancel");
				    }else if(stage == 35){
						$('#cancelmessage').text("Record Already Cancelled.Do u want revert to Draft?");
						$("#basecanceloverlay").fadeIn();				
						$("#cancelyes").focus();
				    } else {
						$('#cancelmessage').text("Do You Want To Cancel This Record?");
						$("#basecanceloverlay").fadeIn();				
						$("#cancelyes").focus();
				    }
				}
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		//cancel data-basecanceloverlay
		$("#cancelno").click(function(){
			$("#basecanceloverlay").fadeOut();
		});
		$("#cancelyes").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Quote/canceldata?primarydataid="+datarowid,
				cache:false,
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == true) {
						$("#basecanceloverlay").fadeOut();
						refreshgrid();					
					} 					
				},
			});		
		});		
		$("#reloadicon").click(function(){
			refreshgrid();
			froalaset(froalaarray);
		});
		//LOST status-
		$("#losticon").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = quotestatus(datarowid);
				if(status == 8){
					alertpopup("Quotation Converted !!! Cannot Change To Lost");
				}else{
					var stage = quotestage(datarowid);
					if(stage == 35){
					  alertpopup("Quotation Cancelled!!!Cannot Change To Lost");
				    } else if(stage == 33){
					  alertpopup("Quotation Booked!!!Cannot Change To Lost");
				    } else if(stage == 34){
						$('#stoprevert').show();
						$('.stopnow').hide();
						$("#lostoverlay").fadeIn();
						$("#stopyes").focus();
				    } else {
						$('#lostreason').val('');
						$('#stoprevert').hide();
						$('.stopnow').show();
						$("#lostoverlay").fadeIn();
						$("#stopyes").focus();
				    }	
				}							
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$("#lostyes").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			var reason = $('#lostreason').val();
			$.ajax({
				url: base_url + "Quote/lostquote?primarydataid="+datarowid+"&reason="+reason,
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == true) {
						refreshgrid();
						$("#lostoverlay").fadeOut();
						$('#lostreason').val('');
					}
				},
			});
		});
		//Booked status-
		$("#bookedicon").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = quotestatus(datarowid);
				if(status == 8){
					alertpopup("Quotation Converted !!! Cannot Change Status");
				}else{
					var stage = quotestage(datarowid);
				    if(stage == 33){
						$('#bookedmessage').text("Quotation Already Booked.Do u want revert to Draft?");
						$("#bookedoverlay").fadeIn();
						$("#bookedyes").focus();
				    } else  if(stage == 32){
						$('#bookedmessage').text("Book Quotation?");
						$("#bookedoverlay").fadeIn();
						$("#bookedyes").focus();
				    } else {
						alertpopup("Quotation Can't Change to Booked Stage");
					}	
				}
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$("#bookedyes").click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			$.ajax({
				url: base_url + "Quote/bookquote?primarydataid="+datarowid,
				cache:false,
				success: function(msg)  {
					var nmsg =  $.trim(msg);
					if (nmsg == true) {
						$("#bookedoverlay").fadeOut();
						refreshgrid();					
					} 					
				},
			});		
		});	
		$("#detailedviewicon").click(function() {
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
		        quoteprodaddgrid();
		        if(softwareindustryid != 2){
		        	chargesgrid();
		        }
	    		taxgrid();
				//Function Call For Edit
	    		froalaset(froalaarray);
				quoteeditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','acccreationformadd');
				$("#quotationsproingriddel1").hide();
				$("#quotationsproingridedit1").hide();
				$("#pricebookclickevent,#productsearchevent").hide();
				$(".fr-element").attr("contenteditable", 'false');
				$(".froala-element").css('pointer-events','none');
				//For tablet and mobile Tab section reset and enable tabgroup dd
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					$("#processoverlay").show();
			        quoteprodaddgrid();
			        if(softwareindustryid != 2){
			        	chargesgrid();
			        }
		    		taxgrid();
					//Function Call For Edit
		    		froalaset(froalaarray);
					quoteeditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','acccreationformadd');
					$("#quotationsproingriddel1").hide();
					$("#quotationsproingridedit1").hide();
					$("#pricebookclickevent,#productsearchevent").hide();
					$(".fr-element").attr("contenteditable", 'false');
					$(".froala-element").css('pointer-events','none');
					//For tablet and mobile Tab section reset and enable tabgroup dd
					$('#tabgropdropdown').select2('val','1');
					setTimeout(function() {
						 $('#tabgropdropdown').select2('enable');
					 },50);
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {	
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var stage=quotestage(datarowid);
				if(stage == 34){ //lost
					alertpopup("This Quotation is lost !!!");
				} else if(stage == 35){ //cancel
					alertpopup("This Quotation is Cancelled");
				} else if(stage == 33){ //booked
					alertpopup("Quotation Booked!!!Cannot Edit!!");
			    } else if(stage == 32){
					showhideiconsfun('editfromsummryclick','acccreationformadd');
					$(".fr-element").attr("contenteditable", 'true');
					$("#quotationsproingriddel1").show();
					$("#quotationsproingridedit1").show();
					$("#pricebookclickevent").show();
					if(softwareindustryid == 2){
						$("#productsearchevent").show();
					}else{
						$("#pricebookclickevent,#productsearchevent").show();
					}					
					$(".froala-element").css('pointer-events','auto');
				}else{
					alertpopup("Add Quotation Status using Edit Option");
				}
			}
			Materialize.updateTextFields();
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function() {
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			var accountid=getaccountid(datarowid,'quote','accountid');
			if (datarowid) 
			{
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','quote',datarowid);
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",emailtosend.subject);
					sessionStorage.setItem("moduleid",MODULEID);
					sessionStorage.setItem("recordid",datarowid);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				}
				else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}	
    {// Close Add Screen
        var addcloseacccreation =["closeaddform","acccreationview","acccreationformadd"]
        addclose(addcloseacccreation);
		var addcloseviewcreation =["viewcloseformiconid","acccreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
    }	
    {// View by drop down change
        $('#dynamicdddataview').change(function() {
           quoteaddgrid();
        });
    }
	{// Validation for quote Add 
		$('#dataaddsbtn').click(function(e) {
			$('#dataaddsbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,maxSize[100]]');
			if(softwareindustryid == 4){
				$('#joiningdate').removeClass('validate[required]');
				$('#expirydate').removeClass('validate[required]');
			}
			$("#formaddwizard").validationEngine('validate');
		});
		$("#formaddwizard").validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone'){
					var getrownumber = $('#quotationsproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#quotationsproaddgrid1 .gridcontent div.wrappercontent div').length;
				}
				if(getrownumber != 0) {
					$("#processoverlay").show();
					var status = $("#crmstatusid").val();
					if(status == null || status == undefined || status == ''){
						$("#crmstatusid").select2('val',32).trigger("change");
					}
					quotecreate();
				} else {
					alertpopup('Please Enter Product');
					$('#dataaddsbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
					if(softwareindustryid == 4){
						$('#joiningdate').addClass('validate[required]');
						$('#expirydate').addClass('validate[required]');
					}
				}
			},
			onFailure: function() {
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$('#dataaddsbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				if(softwareindustryid == 4){
					$('#joiningdate').addClass('validate[required]');
					$('#expirydate').addClass('validate[required]');
				}
			}
		});
	}
	{// Update quote information
		$('#dataupdatesubbtn').click(function(e) {
			$('#dataupdatesubbtn').addClass('singlesubmitonly');
			$('.ftab').trigger('click');
			$('#quantity').removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
			$('#sellingprice').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#netamount').removeClass('validate[required,custom[number],decval[7],maxSize[100]]');
			$('#productid').removeClass('validate[required]');
			$('#grossamount').removeClass('validate[required,maxSize[100]]');
			if(softwareindustryid == 4){
				$('#joiningdate').removeClass('validate[required]');
				$('#expirydate').removeClass('validate[required]');
			}
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				if(deviceinfo != 'phone'){
					var getrownumber = $('#quotationsproaddgrid1 .gridcontent div.data-content div').length;
				}else{
					var getrownumber = $('#quotationsproaddgrid1 .gridcontent div.wrappercontent div').length;	
				}			
				if(getrownumber != 0){
					$("#processoverlay").show();
					quoteupdate();
				} else {
					alertpopup('Enter The Product...');
					$('#dataupdatesubbtn').removeClass('singlesubmitonly');
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
					if(softwareindustryid == 4){
						$('#joiningdate').addClass('validate[required]');
						$('#expirydate').addClass('validate[required]');
					}
				}	
				
			},
			onFailure: function() {
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);				
				alertpopup(validationalert);
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
				$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
				$('#productid').addClass('validate[required]');
				$('#grossamount').addClass('validate[required,maxSize[100]]');
				if(softwareindustryid == 4){
					$('#joiningdate').addClass('validate[required]');
					$('#expirydate').addClass('validate[required]');
				}
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}	
	{// Dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		$('.frmtogridbutton').click(function(){
			var operation = $(this).attr('id');
			// ?Identify where the current record 
			$('#quotedetailid').val(0); //new entry of quote id set to zero.(for new entry only)
			//reset individual array
			$("#instockdivhid label").html('Stock Quantity'); //reset the stock qty label
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');			
			gridformtogridvalidatefunction(gridname,operation);
			griddataid = i;
			gridynamicname = gridname;
			$("#"+gridname+"validation").validationEngine('validate');
			METHOD = 'ADD';
			$("#quantity").removeClass();
			$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
		});
	}
	{// Module change events
		$('#moduleid').change(function(){
			var id = $(this).val();
			smsdropdownmodulevalset('formfields','formfieldsidhidden','modfiledcolumn','modfiledtable','fieldlabel','modulefieldid','columnname','tablename','modulefield','moduletabid,uitypeid',''+id+',0');
		});
	}
	{// Module fields change events
		$('#formfields').change(function(){
			if( $(this).val()!= "" ){
				var table = $(this).find('option:selected').data('modfiledtable');
				var tabcolumn = $(this).find('option:selected').data('modfiledcolumn');
				var mtext = '{'+table+'.'+tabcolumn+'}';
				var oldmtext = $('#mergetext').val();
				var ftext = oldmtext +' '+ mtext;
				$('#mergetext').val(mtext);
			}
		});
	}
	{// Terms and condition value fetch
		$("#termsandconditionid").change(function() {
			var id = $("#termsandconditionid").val();
			tandcdatafrtch(id);
		});
	}
	{// Quantity calculation\//		
		$("#sellingprice").focusout(function() {
			var vl=$(this).val();
			if(checkVariable('sellingprice') == false || isNaN(vl)){	
				$("#sellingprice").val(0);
			}
			$("#quantity").trigger('focusout');
		});
		$("#quantity").focusout(function() {		
			var temo_qty=$('#quantity').val();
			if(isNaN(temo_qty)){
				$('#quantity').val(1);
			}else{
				if(softwareindustryid == 4){
					if(checkVariable('quantity') == true ){
						$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
						var round_qty = parseFloat(this.value);
						$(this).val(round_qty.toFixed(QUANTITY_PRECISION));
						var quan = $("#quantity").val();
						var sellprice = $("#sellingprice").val();
						var gross = parseFloat(quan) * parseFloat(sellprice);
						$("#grossamount").val(gross.toFixed(PRECISION));
						setTimeout(function(){
							var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
							var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
							$('#discountamount').val(discount_amount);
							//pretax-total
							var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
							$("#pretaxtotal").val(pretax.toFixed(PRECISION));
						},10);
						update_taxgriddata();						
						if(softwareindustryid != 2){					
							update_chargegriddata();
						}	
						productnetcalculate();
					}
				}else{
					var stock = parseInt($('#instock').val());
					var qty = parseInt($('#quantity').val());
					if(qty > stock){
						alertpopup("Stock should be greater than quantity");
						$("#quantity").val(1);
					}else{
						if(checkVariable('quantity') == true ){
							$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
							$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
							var round_qty = parseFloat(this.value);
							$(this).val(round_qty.toFixed(QUANTITY_PRECISION));
							var quan = $("#quantity").val();
							var sellprice = $("#sellingprice").val();
							var gross = parseFloat(quan) * parseFloat(sellprice);
							$("#grossamount").val(gross.toFixed(PRECISION));
							setTimeout(function(){
								var discount_json = $.parseJSON($('#discountdata').val()); //individual discount data					
								var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
								$('#discountamount').val(discount_amount);
								//pretax-total
								var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
								$("#pretaxtotal").val(pretax.toFixed(PRECISION));
							},10);
							update_taxgriddata();						
							if(softwareindustryid != 2){					
								update_chargegriddata();
							}	
							productnetcalculate();
						}	
					}
				}	
			}
		});
	}	
	//pricebook_currencyconv_submit
	{
		$('#pricebook_currencyconv_submit').click(function(e){
			$("#pricebookcurrencyconv_validate").validationEngine('validate');			
		});
		jQuery("#pricebookcurrencyconv_validate").validationEngine({
			onSuccess: function() { 
				var pb_sellingprice=$("#pricebook_sellingprice").val();	 //pricebook sellingprice
				var pb_currentcurrency = $("#currentcurrency").val();
				var pb_pricebookcurrency = $("#pricebookcurrency").val();
				var pb_conversionrate=$("#pricebook_currencyconv").val();	 //pricebook sellingprice
				$("#currentcurrencyid").val(pb_currentcurrency);
				$("#pricebookcurrencyid").val(pb_pricebookcurrency);
				$("#pricebook_currencyconvrate").val(pb_conversionrate);
				PRICBOOKCONV_VAL = pb_conversionrate;
				var productid = $("#productid").val();
				if(productid){
					var finalvalue = listprice(pb_sellingprice,pb_conversionrate);
					var conversionrate = $('#currencyconvresionrate').val();
					finalvalue = listprice(finalvalue,conversionrate);
					$('#sellingprice').val(finalvalue).trigger('focusout'); //sets the value and triggerchange calc
				}	
				$("#pricebookcurrencyconvoverlay,#pricebookoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});
	}	
	{// Price book/currencyconversion overlay close
		$("#pricebookclose").click(function() {
			$("#pricebookoverlay").fadeOut();
			$('#grossamount').focus();
		});
		$("#pricebookcurrencyconvclose").click(function() {
			$("#pricebookcurrencyconvoverlay").fadeOut();
		});
		$("#productsearchclose").click(function() {
			$("#productsearchoverlay").fadeOut();
			$('#quantity').focus();
		});
	}
	{
		//currency overlay
		$("#currencyconverticon").click(function(){
			var productrecords = $('#quotationsproaddgrid1 .gridcontent div.data-content div').length;
			if(productrecords == 0){
				$("#currencyconvoverlay").fadeIn();
				$.ajax({
					url:base_url+"Base/getdefaultcurrency", 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {						
						$('#defaultcurrency').select2('val',data.currency).trigger('change').attr("readonly","readonly");
					}
				});
			}					
		});
		$("#currencyconvclose").click(function() {
			$("#currencyconvoverlay").fadeOut();
		});
		//currency 
		$("#convcurrency").change(function() {
			var defaultcurrency = $("#currencyid").val();
			var currencyid = $(this).val();
			var quotedate = $("#quotedate").val();
			if(currencyid > 0) {
					var selected=$('#currencyid').find('option:selected');
					var datavalue=selected.data('precision');
					PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null) {
					PRECISION = 0;
				}
				$.ajax({
					url:base_url+"Quote/getconversionrate?fromcurrency="+defaultcurrency+"&tocurrency="+currencyid+"&quotedate="+quotedate, 
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if(data.status == true){
							$('#currencyconv').val(data.rate);
							Materialize.updateTextFields();
						}else{
							$('#currencyconv').val(0);
							Materialize.updateTextFields();
						}
					}
				});
			} else {
				PRECISION = 0;
				$('#currencyconv').val(0);
			}
			
		});
		$('#currencyconv_submit').click(function(e){
			$("#currencyconv_validate").validationEngine('validate');			
		});
		jQuery("#currencyconv_validate").validationEngine({
			onSuccess: function() { 
				var convcurrency=$("#convcurrency").val();	 //convert currency
				var currencyconv=$("#currencyconv").val();	 //convert rate
				$("#currencyid").val(convcurrency).trigger("change");
				$("#currencyconvresionrate").val(currencyconv).trigger("change");
				$("#productid").select2('val','').trigger("change");
				$("#pricebookid").select2('val','').trigger("change");
				$("#currencyconvoverlay").fadeOut();
			},
			onFailure: function() {	
			}
		});		
		$("#currencyid").change(function(){			
			var currencyid = $(this).val();
			if(currencyid > 0){
				var selected=$('#currencyid').find('option:selected');
				var datavalue=selected.data('precision');
				PRECISION = parseFloat(datavalue);
				if(datavalue == '' || datavalue == null){
					PRECISION = 0;
				}	
				
			} else {
				PRECISION = 0;
			}
		});
	}
	{//productsearchsubmit
		$("#productsearchsubmit").click(function() {
			var selectedrow = $('#productsearchgrid div.gridcontent div.active').attr('id');
			if(selectedrow) {
				$('#productid').select2('val',selectedrow).trigger('change');
				$("#productsearchoverlay").fadeOut();
			}
		});
	}
	{// Price book submit
		$("#pricebooksubmit").click(function() {
			var selectedrow = $('#pricebookgrid div.gridcontent div.active').attr('id');
			if(selectedrow) {
				var sellingprice = getgridcolvalue('pricebookgrid',selectedrow,'sellingprice','');
				var currencyid = getgridcolvalue('pricebookgrid',selectedrow,'currencyid','');
				var current_currencyid=$("#currencyid").val();
				if(current_currencyid == currencyid) {
					$('#sellingprice').val(sellingprice).trigger('focusout'); //sets the value and triggerchange calc
					$("#pricebookoverlay").fadeOut();
				} else if(current_currencyid != currencyid) {
					var quotedate = $('#quotedate').val();
					var conversionrate = getcurrencyconversionrate(currencyid,current_currencyid,quotedate); //to get conversion rate
					$('#currentcurrency').select2('val',current_currencyid).trigger('change').attr("disabled","disabled");
					$('#pricebookcurrency').select2('val',currencyid).trigger('change').attr("disabled","disabled");				
					$("#pricebook_sellingprice").val(sellingprice);					
					$('#pricebook_currencyconv').val(conversionrate);
					$("#pricebookcurrencyconvoverlay").fadeIn();
					Materialize.updateTextFields();
				}								
			} else {
				alertpopup('Please select a row');
			} 
		});	
	}
	{//summary overlay events
		//*group discount click*//
		$("#groupdiscountamount").click(function() {			
			var discount_data = $('#groupdiscountdata').val();
			var parse_discount = $.parseJSON(discount_data);				
			if(parse_discount == null){ //if no previous data exits then refresh the discount fields
				$('#groupdiscounttypeid').select2('val','').trigger('change');
				$('#groupdiscountpercent').val('');				
			} else {							
				$('#groupdiscounttypeid').select2('val',parse_discount.typeid).trigger('change');
				$('#groupdiscountpercent').val(parse_discount.value);
			}
			$("#summarydiscountoverlay").fadeIn();				
			setTimeout(function() {
				group_discount_total();
				Materialize.updateTextFields();
			},100);			
		});	
		//*group discount close*//		
		$("#summarydiscountclose").click(function() {
			$("#summarydiscountoverlay").fadeOut();
			var value=parseFloat($('#groupdiscountpercent').val());
			if(value == '' || value == null || isNaN(value)){
				$('#groupdiscounttypeid').select2('val','').trigger('change');
			}
			$('#grouppretaxtotal').focus();
		});
		//*Summary Discount validation*//
		$('#summarydiscountadd').click(function(e){
			$("#summarydiscountvalidation").validationEngine('validate');			
		});
		$("#summarydiscountvalidation").validationEngine({
			onSuccess: function() {											
				var discount_json ={}; //declared as object
				discount_json.typeid = $('#groupdiscounttypeid').val(); //discount typeid
				discount_json.value = $('#groupdiscountpercent').val();	//discount values		
				$('#groupdiscountdata').val(JSON.stringify(discount_json)); //saves discountdata in hiddenfield(JSON)
				var netamount = parseFloat(getgridcolvalue('quotationsproaddgrid1','','netamount','sum'));
				var discount_amount=discountcalculation(discount_json,netamount); //to calculate discount
				$('#groupdiscountamount').val(discount_amount);					
				$("#summarydiscountoverlay").fadeOut();
				//?set a trigger point here
				gridsummeryvaluefetch('','','');
			},
			onFailure: function() {	
			}
		});
		{//adjustment overlay
			$("#adjustmentamount").click(function() {
				var adjustment_data = $('#groupadjustmentdata').val();
				var parse_adjustment = $.parseJSON(adjustment_data);				
				if(parse_adjustment == null){ //if no previous data exits then refresh the adjusttment fields
					$('#adjustmenttypeid').select2('val','2').trigger('change');
					$('#adjustmentvalue').val(0);				
				} else {							
					$('#adjustmenttypeid').select2('val',parse_adjustment.typeid).trigger('change');
					$('#adjustmentvalue').val(parse_adjustment.value);
				}				
				$("#adjustmentoverlay").fadeIn();
				Materialize.updateTextFields();
			});
			$("#adjustmentclose").click(function() {
				clearform('clearadjustmentform');
				$("#adjustmentoverlay").fadeOut();
				$('#grandtotal').focus();
			});
			$("#adjustmentadd").click(function() {
				$('#adjustmenttypeid').trigger('change'); //to liveup adjustmentdd
				setzero(['adjustmentvalue']);
				var adjustment_json ={}; //declared as object
				adjustment_json.typeid = $('#adjustmenttypeid').val(); //adjustment type
				adjustment_json.value = $('#adjustmentvalue').val(); // adjustment value	
				$('#groupadjustmentdata').val(JSON.stringify(adjustment_json));
				var adjustment_value=parseFloat($('#adjustmentvalue').val());
				$("#adjustmentamount").val(adjustment_value.toFixed(PRECISION));
				$("#adjustmentoverlay").fadeOut();
				//?trigger point for calculation
				gridsummeryvaluefetch('','','');
			});
		}
	}	
	{	// Redirected form Home
		add_fromredirect("quoteaddsrc",MODULEID);
	}
	//Quotation-report session check
	{//Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique216");	
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() {
				quoteeditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','acccreationformadd');
				$("#quotationsproingriddel1").hide();
				$("#quotationsproingridedit1").hide();
				sessionStorage.removeItem("reportunique216");
			},50);
		}
	}
	{// Quote product form to grid click event
		$("#quotationsproingridedit1").click(function() {
			var selectedrow = $('#quotationsproaddgrid1 div.gridcontent div.active').attr('id');
			if(selectedrow) {
				sectionpanelheight('quotationsprooverlay');
				$("#quotationsprooverlay").removeClass("closed");
				$("#quotationsprooverlay").addClass("effectbox");
				$('#productid').select2("focus");
				gridtoformdataset('quotationsproaddgrid1',selectedrow);
				Materialize.updateTextFields();
				$("#quantity").removeClass();
				$("#quantity").addClass('validate[required,custom[number],decval[3],maxSize[100]]');
				var productid = $('#productid').val();
				var pricebook = $('#pricebookid').val();
				$.ajax({
					url:base_url+'Base/getproductdetails?id='+productid+'&pricebook='+pricebook,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						QUANTITY_PRECISION = data.uomprecision;
						$("#quantity").removeClass('validate[required,custom[number],decval[3],maxSize[100]]');
						$("#quantity").addClass('validate[required,custom[number],decval['+QUANTITY_PRECISION+'],maxSize[100]]');
					}
				});				
				$('#quotationsproupdatebutton').show();	//display the UPDATE button(inner - productgrid)
				$('#quotationsproaddbutton').hide();	//display the ADD button(inner-productgrid) */
				METHOD = 'UPDATE';
				UPDATEID = selectedrow;
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
	}
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Date Restriction
		var dateformetqt = 0;
		dateformetqt= $('#quotedate').attr('data-dateformater');
		$('#quotedate').datetimepicker({
			dateFormat: dateformetqt,
			showTimepicker :false,
			minDate: 0,
			onSelect:function(){
				getandsetdate($('#quotedate').val())
			},
			onClose:function(){
				$('#quotedate').focus();
			}
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Append Icon Ater Loads Data - Workaround
		generateiconafterload('sellingprice','pricebookclickevent','sellingpricedivhid');
		productgenerateiconafterload('productid','productsearchevent');
		$("#pricebookclickevent").on('click keypress', function(e) {
			if(e.keyCode == 13 || e.keyCode === undefined){
				if(checkVariable('productid') == true) {
					$("#pricebookoverlay").fadeIn();
					pricebookgrid();
					$('#pricebookclose').focus();
				} else {
					cleargriddata('pricebookgrid');
				}
			}
		});
		$("#productsearchevent").on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				if(softwareindustryid != 2){
					var ordertype = $('#ordertypeid').val();
					if(ordertype){
						$("#productsearchoverlay").fadeIn();
						$("#productsearchclose").focus();
						productsearchgrid();
					}else{
						alertpopup("Select the Quote Type to Load the Product");
					}	
				}else{
					$("#productsearchoverlay").fadeIn();
					productsearchgrid();
				}	
			}	
		});	
	}
	{
		$("#territoryid").change(function(){
			var id = $(this).val();
			if(id){
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";								
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}else{
				$("#pricebookid").empty();
				$.ajax({
					url: base_url+"Base/getpricebookdetail?territoryid="+id,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						$("#pricebookid").empty();
						newoptions="<option></option>";
						$.each(data, function(index) {
							newoptions += "<option data-pricebookidhidden ='"+data[index]['name']+"' value ='"+data[index]['id']+"'>"+data[index]['name']+"</option>";							
						});
						$('#pricebookid').append(newoptions);
						$('#pricebookid').select2('val','').trigger('change');
					},
				});
			}
		});
		//charge change calculations
		$("#chargecategory").change(function() {
			var id = $(this).val();
			var additionalchargecategoryid = $('#additionalchargecategoryid').val(); //2-individual 3-group
			var typeid = '';
			if(additionalchargecategoryid){
				typeid = 3;
			}else{
				typeid = 2;
			}
			var territory = $('#territoryid').val(); //region
			var conversionrate = $('#currencyconvresionrate').val(); //conversion rate
			var currency = $('#currencyid').val(); //conversion rate
			cleargriddata('chargesgrid');
			var grossamount = $('#grossamount').val();
			var totalnetamount = $('#totalnetamount').val();
			if(id != '') {
				var transmethod = MODULEID;
				$.ajax({
					url:base_url+'Quote/chargecategoryload?chargecategoryid='+id+'&grossamount='+grossamount+'&territory='+territory+'&conversionrate='+conversionrate+'&currency='+currency+'&typeid='+typeid+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							loadinlinegriddata('chargesgrid',data.rows,'json');
							setTimeout(function() {
								charge_data_total();
							},200);
							/* data row select event */
							datarowselectevt();
							/* column resize */
							columnresize('chargesgrid');
						}
						Materialize.updateTextFields();
					},
				});
			}
		});
		//charge change calculations
		$("#taxcategory").change(function() {
			var id = $(this).val();
			var taxmasterid = $('#taxmasterid').val(); //2-individual 3-group
			var type = '';
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			var territory = $('#territoryid').val(); //region
			var totalnetamount = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());//group
			cleargriddata('taxgrid');
			if(id != '') {
				if(type == 2) {
					var grossamount=parseFloat($('#grossamount').val());
					var discountamount=parseFloat($('#discountamount').val());
					var finalamount=parseFloat(grossamount)-parseFloat(discountamount);
				} else {
					var finalamount= 0;
				}
				if(id != '') {
					var transmethod = MODULEID;
					$.ajax({
						url:base_url+'Quote/taxmasterload?taxmasterid='+id
						+'&finalamount='+finalamount+'&territory='+territory+'&type='+type+'&totalnetamount='+totalnetamount+'&transmethod='+transmethod,
						dataType:'json',
						async:false,
						cache:false,
						success: function(data) {
							if((data.fail) == 'FAILED') {
							} else {
								loadinlinegriddata('taxgrid',data.rows,'json');
								setTimeout(function() {
									tax_data_total();
								},200);
								/* data row select event */
								datarowselectevt();
								/* column resize */
								columnresize('chargesgrid');
							}
							Materialize.updateTextFields();
						},
					});
				}				
			}
		});		
		$("#taxdeleteicon").click(function() {
			var id = $('#taxgrid div.gridcontent div.active').attr('id');
			if(id) {
				/*delete grid data*/
				deletegriddatarow('taxgrid',id);
				setTimeout(function() {
					tax_data_total();
				},200);
				if(deviceinfo != 'phone'){
					if($('#taxgrid .gridcontent div.data-content div').length == 0) {
						$('#taxcategory').select2('val','').trigger('change');
					}
				}else{
					if($('#taxgrid .gridcontent div.wrappercontent div').length == 0) {
						$('#taxcategory').select2('val','').trigger('change');
					}
				}				
			} else {
				alertpopupdouble('Select Charge Record');
			}
		});
		$("#taxclearicon").click(function() {
			cleargriddata('taxgrid');
			$('#taxcategory').select2('val','').trigger('change');
			setTimeout(function() {
				tax_data_total();
			},200);
		});
		{// Additional amount overlay
		$("#chargeclearicon").click(function() {
			cleargriddata('chargesgrid');
			$('#chargecategory').select2('val','').trigger('change');
			setTimeout(function() {
				charge_data_total();
			},200);
		});
		/* Delete charges */
		$("#chargedeleteicon").click(function() {
			var id = $('#chargesgrid div.gridcontent div.active').attr('id');
			if(id) {
				/*delete grid data*/
				deletegriddatarow('chargesgrid',id);
				setTimeout(function() {
					charge_data_total();
				},200);
				if(deviceinfo != 'phone'){
					if($('#chargesgrid .gridcontent div.data-content div').length == 0) {
						$('#chargecategory').select2('val','').trigger('change');
					}
				}else{
					if($('#chargesgrid .gridcontent div.wrappercontent div').length == 0) {
						$('#chargecategory').select2('val','').trigger('change');
					}
				}
				
			} else {
				alertpopupdouble('Select Charge Record');
			}
		});
		$("#chargeamount").click(function() {
			//identify the type(group/individual);
			var type = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				type = 3;
			}else{
				type = 2;
			}
			if(type == 2 && checkVariable('productid') ==true){	//individual
				var g_data = $('#chargegriddata').val();
				var charge_category_id= 'chargecategory';			
				var main_data = $.parseJSON(g_data);
				if(!main_data || main_data == null) {
					$("#additionaloverlay").fadeIn();
					chargesgrid();
					$('#'+charge_category_id+'').select2('val','').trigger('change');
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0) {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						setTimeout(function(){
							$('#'+charge_category_id+'').select2('val',main_data.id);
							loadgriddata('chargesgrid',grid_data);
						},200);
					} else {
						$("#additionaloverlay").fadeIn();
						chargesgrid();
						$('#'+charge_category_id+'').select2('val','').trigger('change');
					} 
				}
				setTimeout(function() {
					charge_data_total();
				},200);
			}  
		});
		$("#additionalclose").click(function() {
			productnetcalculate();	
			var id = $("#addcatid").val();
			var product=checkVariable('productid');	
			$("#additionaloverlay").fadeOut();
			$('#netamount').focus();
		});
		$("#taxamount").click(function() { 
			//identify the type(group/individual);
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			$("#taxcategory").focus();
			if(type == 2 && checkVariable('productid') ==true && PRODUCTTAXABLE == 'Yes'){	//individual
				var g_data = $('#taxgriddata').val();
				var tax_category_id= 'taxcategory';
				var main_data = $.parseJSON(g_data);			
				if(!main_data || main_data == null){
					$("#taxoverlay").fadeIn();
					$('#'+tax_category_id+'').select2('val','').trigger('change');
					taxgrid();
				} else {
					var grid_data = main_data.data;
					if (grid_data.length > 0){		
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val',main_data.id);
						loadgriddata('taxgrid',grid_data);
					} else {
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#'+tax_category_id+'').select2('val','').trigger('change');
					} 
				}
				setTimeout(function() {
					tax_data_total();
				},200);
			}
		});
	}
		{//summary additional amount overlay
			$("#groupchargeamount").click(function() {
				//identify the type(group/individual);
				var type = '';
				var additionalchargecategoryid = $('#additionalchargecategoryid').val();
				if(additionalchargecategoryid){
					type = 3;
				}else{
					type = 2;
				}
				if(type == 3){	//group
					var g_data = $('#groupchargegriddata').val();
					var charge_category_id= 'chargecategory'; //?		
					var main_data = $.parseJSON(g_data);			
					if(!main_data || main_data == null){
						$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
						$("#additionaloverlay").fadeIn();
						chargesgrid();
					} else {
						var grid_data = main_data.data;
						if (grid_data.length > 0) {
							$("#additionaloverlay").fadeIn();
							chargesgrid();
							$('#'+charge_category_id+'').select2('val',main_data.id);
							loadgriddata('chargesgrid',grid_data);
						} else {
							$("#additionaloverlay").fadeIn();
							chargesgrid();
							$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
						} 
					}
					setTimeout(function() {
						charge_data_total();
					},200);
					$("#chargecategory").focus();
				} 
			});
		}
		{//summary tax calculation
			$("#grouptaxamount").click(function() {				
				//identify the type(group/individual);
				var type = '';
				var taxmasterid = $('#taxmasterid').val();
				if(taxmasterid){
					type = 3;
				}else{
					type = 2;
				}
				if(type == 3){	//group
					var g_data = $('#grouptaxgriddata').val();
					var tax_category_id= 'taxcategory'; //?				 
					var main_data = $.parseJSON(g_data);			
					if(!main_data || main_data == null){						
						$("#taxoverlay").fadeIn();
						taxgrid();
						$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
					} else {
						var grid_data = main_data.data;
						if (grid_data.length > 0) {
							$("#taxoverlay").fadeIn();
							taxgrid();
							$('#'+tax_category_id+'').select2('val',main_data.id);
							loadgriddata('taxgrid',grid_data);
						} else {
							$("#taxoverlay").fadeIn();
							taxgrid();
							$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
						} 
					}
				}
				setTimeout(function() {
					tax_data_total();
				},200);
			});
		}
	}
	{//group special function autoset the tax and charges in the summary
		$("#taxmasterid").change(function() {	
			if(checkVariable('taxmasterid') == true){
				//identify the type(group/individual);
				var type = '';
				var taxmasterid = $('#taxmasterid').val();
				if(taxmasterid){
					type = 3;
				}else{
					type = 2;
				}
				if(type == 3){
					$('#grouptaxgriddata').val('');
					setTimeout(function() {
						$('#taxcategory').select2('val',$('#taxmasterid').val()).trigger('change');
					},200);
					setTimeout(function() {
						var griddataid = 'grouptaxgriddata';
						var tax_category_id = 'taxmasterid';
						var tax_amount_field = 'grouptaxamount';
						var tax_json ={}; //declared as object
						tax_json.id = $('#'+tax_category_id+'').val(); //categoryid
						tax_json.data = getgridrowsdata("taxgrid");//taxgrid				
						$('#'+griddataid+'').val(JSON.stringify(tax_json));
						var totalcharge = parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
						$('#'+tax_amount_field+'').val(totalcharge.toFixed(PRECISION));
						calculatesummarydetail();
					},1000);
				}				
			}
		});
		$("#additionalchargecategoryid").change(function() {
			if(checkVariable('additionalchargecategoryid') == true){				
				$('#groupchargegriddata').val('');
				setTimeout(function() {
					$('#chargecategory').select2('val',$('#additionalchargecategoryid').val()).trigger('change');
				},200);
				setTimeout(function() {
					var griddataid = 'groupchargegriddata';
					var charge_category_id = 'additionalchargecategoryid';
					var charge_amount_field = 'groupchargeamount';	
					var charge_json ={}; //declared as object
					charge_json.id = $('#'+charge_category_id+'').val(); //categoryid
					charge_json.data = getgridrowsdata('chargesgrid');	//chargesgrid			
					$('#'+griddataid+'').val(JSON.stringify(charge_json));
					var totalcharge = parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
					$('#'+charge_amount_field+'').val(totalcharge.toFixed(PRECISION));
					calculatesummarydetail();
				},1000);
			}else{
				$('#groupchargeamount').val('');
				setTimeout(function(){
					calculatesummarydetail();
				},5);
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").hide();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						} else if(mobilenumber.length == 1) {
							sessionStorage.setItem("mobilenumber",mobilenumber);
							sessionStorage.setItem("viewfieldids",viewfieldids);
							sessionStorage.setItem("recordis",datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").show();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'smsmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmodule").change(function() {
		var moduleid =	$("#smsmoduleid").val(); //main module
		var linkmoduleid =	$("#smsmodule").val(); // link module
		var recordid = $("#smsrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
		}
	});
	$("#callmodule").change(function() {
		var moduleid =	$("#callmoduleid").val(); //main module
		var linkmoduleid =	$("#callmodule").val(); // link module
		var recordid = $("#callrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
		}
	});
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data')) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++){
							if(data[i] != ''){
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1){
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#c2cmobileoverlay").show();
						$("#callmoduledivhid").show();
						$("#callrecordid").val(datarowid);
						$("#callmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'callmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function()  {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,quoteaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
{// Set and get date 
	function getandsetdate(dateset){
		$('#validdate').datetimepicker('option', 'minDate',dateset);
	}
}
{// Individual Add Charges Calculation Function
	function individualaddchargecalculation(calctype,value,price,addamountfield) {
		setzero([value,price]);
		var calctype=$("#"+calctype+"").val();
		var value=$('#'+value+'').val();
		var price=$('#'+price+'').val();
		var addamt=0;
		switch (calctype) { 
			case '2': 
				var addamt=value;
			break;
			case '3':
				var addamt=(parseFloat(value/100)*parseFloat(price));
			break;
		}
		var faddamt=parseFloat(addamt);
		$('#'+addamountfield+'').val(faddamt.toFixed(PRECISION));	
		var sum = 0;
		$(".individualaddchargeamount").each(function(){
			sum += +$(this).val();
		});		
	}
}
{// Summary Add Charges Calculation Function
	function summaryaddchargecalculation(calctype,value,price,addamountfield) {
		setzero([value,price]);
		var calctype=$("#"+calctype+"").val();
		var value=$('#'+value+'').val();
		var price=$('#'+price+'').val();
		var addamt=0;
		switch (calctype) { 
			case '2': 
				var addamt=value;
			break;
			case '3':
				var addamt=(parseFloat(value/100)*parseFloat(price));
			break;
		}
		var tmp=parseFloat(addamt);
		$('#'+addamountfield+'').val(tmp.toFixed(PRECISION));
		var sum = 0;
		$(".summaryaddchargeamount").each(function(){
			sum += +$(this).val();
		});
		$('#summaryadditionalchargeamount,#summarytotaladdamount').val(sum.toFixed(PRECISION));
	}
}
{// View create success function
    function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main view Grid
	function quoteaddgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#quotationspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#quotationspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#quoteaddgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.quotationsheadercolsort').hasClass('datasort') ? $('.quotationsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.quotationsheadercolsort').hasClass('datasort') ? $('.quotationsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.quotationsheadercolsort').hasClass('datasort') ? $('.quotationsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=quote&primaryid=quoteid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#quoteaddgrid').empty();
				$('#quoteaddgrid').append(data.content);
				$('#quoteaddgridfooter').empty();
				$('#quoteaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('quoteaddgrid');
				{//sorting
					$('.quotationsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.quotationsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#quotepgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.quotationsheadercolsort').hasClass('datasort') ? $('.quotationsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.quotationsheadercolsort').hasClass('datasort') ? $('.quotationsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#quoteaddgrid .gridcontent').scrollLeft();
						quoteaddgrid(page,rowcount);
						$('#quoteaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('quotationsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						quoteaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#quotationspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;$('#prev').data('pagenum');
						quoteaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var quoteideditvalue = sessionStorage.getItem("quoteidforedit");
					if(quoteideditvalue != null) {
						setTimeout(function() { 
							quoteeditdatafetchfun(quoteideditvalue);
							sessionStorage.removeItem("quoteidforedit");
						},100);	
					}								
				}
				//Material select
				$('#quotationspgrowcount').material_select();
			},
		});		
	}
}
{
	function getmoduleid(){
		$.ajax({
			url:base_url+'Quote/getmoduleid',
			async:false,
			cache:false,
			success: function(data) {
				MODULEID=data;
			}
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
	        quoteprodaddgrid();
    		taxgrid();
	        if(softwareindustryid != 2){
	        	chargesgrid();
	        }		
	        // Append The Currency data
			appendcurrency('currencyid');
			e.preventDefault();
			$('#dataaddsbtn').removeClass('singlesubmitonly');
			QUANTITY_PRECISION = 0;
			addslideup('acccreationview','acccreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','acccreationformadd');
			$("#quotationsproingriddel1,#quotationsproingridedit1,#pricebookclickevent,#productsearchevent").show();
			$(".fr-element").attr("contenteditable", 'true');
			//form field first focus.
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);			        
			froalaset(froalaarray);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			setdefaultproperty();
			$("#termsandconditionid").trigger('change');
			clearformgriddata();
			if(softwareindustryid != 2){
				//$('#productid').empty();
			}
			$('#defaultcurrency').select2('val','');
			$('#convcurrency').select2('val','');
			$('#currencyconv').val('');
			$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
			setTimeout(function(){
			$('#ordertypeid,#additionalchargecategoryid,#taxmasterid,#pricebookid,#accountid,#contactid,#opportunityid,#mname').select2("val",'');
			$("#ordertypeid,#additionalchargecategoryid,#taxmasterid,#pricebookid,#currencyid,#accountid,#contactid,#leadcontacttype,#opportunityid").select2("readonly", false);},100); 
			$('#currentmode').val(1);
			var qutdate = $("#quotedate").val();
			getandsetdate(qutdate);
			appendleadcontact('2','contactid');
			$('#leadiddivhid').hide();
			$('#contactiddivhid').show();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			METHOD = 'ADD';
			firstfieldfocus(); 
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			$('#dataupdatesubbtn').removeClass('singlesubmitonly');
			QUANTITY_PRECISION =0;
			if (datarowid) {
				var status = quotestatus(datarowid);
				if(status == 8){
					alertpopup("Quotation Converted !!! Permission Denied to Edit");
				}else{
			        quoteprodaddgrid();
		    		taxgrid();
			        if(softwareindustryid != 2){
			        	chargesgrid();
			        }	
			        // Append The Currency data
					appendcurrency('currencyid');				
					var stage=quotestage(datarowid);
					if(stage == 34){ //lost
						alertpopup("This Quotation is lost !!!");
					} else if(stage == 35){ //cancel
						alertpopup("This Quotation is Cancelled");
					} else if(stage == 33){ //booked
						alertpopup("Quotation Booked!!!Cannot Edit!!");
				    } else {
				    	$("#processoverlay").show();
				    	froalaset(froalaarray);
				    	quoteeditdatafetchfun(datarowid);	
						showhideiconsfun('editclick','acccreationformadd');
						$(".fr-element").attr("contenteditable", 'true');
						$("#pricebookclickevent").show();
						$("#quotationsproingriddel1").show();	
						fortabtouch = 1;
						METHOD = 'ADD';
						//For tablet and mobile Tab section reset
						$('#tabgropdropdown').select2('val','1');
					}
				}	Operation = 1; //for pagination			
			} 
			else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#quoteaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var stage=quotestage(datarowid);
				if(stage == 34){ //lost
					alertpopup("This Quotation is lost !!!");
				} else if(stage == 35){ //cancel
					alertpopup("This Quotation is Cancelled");
				} else if(stage == 33){ //booked
					alertpopup("Quotation Booked!!!Cannot Delete!!");
			    }
				else {
					$("#basedeleteoverlay").fadeIn();
					$('#primarydataid').val(datarowid);
					$("#basedeleteyes").focus();
				}
			}
			else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			quoterecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#quotepgnum li.active').data('pagenum');
		var rowcount = $('ul#quotepgnumcnt li .page-text .active').data('rowcount');
		quoteaddgrid(page,rowcount);
	}
}
{// Product view Grid
	function quoteprodaddgrid() {
		var wwidth = $("#quotationsproaddgrid1").width();
		var wheight = $("#quotationsproaddgrid1").height();
		var tabgroupid = 0;
		if(MODULEID == 216){
			tabgroupid = 48;
		}else if(MODULEID == 94){
			tabgroupid = 247;
		}else if(MODULEID == 85){
			tabgroupid = 212;
		}
		$.ajax({
			url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid="+tabgroupid+"&moduleid="+MODULEID+"&width="+wwidth+"&height="+wheight+"&modulename=quoteproduct",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#quotationsproaddgrid1").empty();
				$("#quotationsproaddgrid1").append(data.content);
				$("#quotationsproaddgrid1footer").empty();
				$("#quotationsproaddgrid1footer").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('quotationsproaddgrid1');
				$("#durationiddivhid,#joiningdatedivhid,#expirydatedivhid").addClass('hidedisplay');
				gridfieldhide('quotationsproaddgrid1',['durationidname','durationid','expirydate','joiningdate']);
				var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','quotedetailid'];
				gridfieldhide('quotationsproaddgrid1',hideprodgridcol);
				$("#summarytaxcategory,#summaryadditionalcategory").select2('readonly',true);
			},
		});
	}
}
{// New data add submit function
	function quotecreate() {
		var amp = '&';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				}
			}
		}
		var sendformadddata = JSON.stringify(addgriddata);
		var tandcdata = 0;
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementspartabname = $('#elementspartabname').val();
		var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
		var resctable = $('#resctable').val();		
		/*editor data */ 
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Quote/newdatacreate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+amp+"elementsname="+elementsname+amp+"elementstable="+elementstable+amp+"elementscolmn="+elementscolmn+amp+"elementspartabname="+elementspartabname+amp+"resctable="+resctable+amp+"griddatapartabnameinfo="+griddatapartabnameinfo+amp+"tandcdata="+tandcdata+"&quotemodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$(".ftab").trigger('click');
					resetFields();
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn(1000);
					refreshgrid();
					$("#quoteeditor").text('');
					/* clear grid data */
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
					$('#additionalcategory,#discounttypeid').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
					//For Keyboard Shortcut Variables.
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				} else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
					$("#processoverlay").hide();
				} else {
					alertpopup("Error during create.Please refresh and try again");
					$("#processoverlay").hide();
				}
				$('#dataaddsbtn').removeClass('singlesubmitonly');
			},
		});
	}
}
{// Old information show in form
	function retrievequotedata(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Quote/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable+"&quotemodule="+MODULEID,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn(1000);
					refreshgrid();
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					//For Left Menu Confirmation
					discardmsg = 0;
					$("#processoverlay").hide();
				} else {
					addslideup('acccreationview','acccreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					$('#pricebookcurrency').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconv').val(data['pricebook_currencyconvrate']);
					$('#currentcurrency').val(data['currentcurrencyid']);
					$('#currencyconv').val(data['currencyconvresionrate']);
					$('#convcurrency').val(data['currencyid']);
					$('#pricebookcurrencyid').val(data['pricebookcurrencyid']);
					$('#pricebook_currencyconvrate').val(data['pricebook_currencyconvrate']);
					$('#currentcurrencyid').val(data['currentcurrencyid']);
					PRICBOOKCONV_VAL = data['pricebook_currencyconvrate'];
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					var ext_data = data.summary;
					//summary data settings//
					{
						$('#grouptaxgriddata').val(ext_data['grouptax']);
						$('#groupchargegriddata').val(ext_data['groupaddcharge']);
						$('#groupadjustmentdata').val(ext_data['groupadjustment']);
						$('#groupdiscountdata').val(ext_data['groupdiscount']);
					}
					if(softwareindustryid == 2){
						appendleadcontact('2','contactid');
						$('#contactid').select2('val',data['contactid']);						
					}else{
						var val=$("#leadcontacttype").val();
						$('#leadcontacttype').trigger('change');
						var pid = data['parentquoteid'];
						if(val == 2) {
							$('#contactid').select2('val',data['contactid']);
						} else {
							$('#leadid').select2('val',data['leadid']);
						}
					}					
					quoteproductdetail(datarowid);
					primaryaddvalfetch(datarowid);
					editordatafetch(froalaarray,data);
					$("#processoverlay").hide();
				}
			}
		});	
	}
}
{//terms and condition fetch
	function termsandconditionidfetch(datarowid){
		$.ajax({
			url:base_url+"Quote/tandcidfetch?dataprimaryid="+datarowid, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$("#termsandconditionid").select2('val',data).trigger('change');
			}
		});	
	}
}	
//Retrieve the Quote Product Detail
function quoteproductdetail(pid) {
	if(pid!='') {
		$.ajax({
			url:base_url+'Quote/quoteproductdetailfetch?primarydataid='+pid+"&quotemodule="+MODULEID,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('quotationsproaddgrid1',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('quotationsproaddgrid1');
					var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','quotedetailid'];
					gridfieldhide('quotationsproaddgrid1',hideprodgridcol);
				}
			},
		});
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid) {
		$.ajax({
			url:base_url+"Quote/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns); 
			}
		});
	}
}
{// Update old information
	function quoteupdate() {
		var amp = '&';
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;
				}
			}
		}		
		var sendformadddata = JSON.stringify(addgriddata);	
		//editor data
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Quote/datainformationupdate",
			data: "datas=" + datainformation+amp+"griddatas="+sendformadddata+amp+"numofrows="+noofrows+"&quotemodule="+MODULEID,
			type: "POST",
			cache:false,
			success: function(msg) {
				if (msg == true) {
					$(".ftab").trigger('click');
					resetFields();
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn(1000);
					refreshgrid();
					clearformgriddata();
					$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
					$('#additionalcategory,#discounttypeid,#taxmasterid,#additionalchargecategoryid,#groupdiscounttypeid').select2('val','').trigger('change');
					alertpopup(savealert);
					$('#quantity').addClass('validate[required,custom[number],decval[3],maxSize[100]]');
					$('#sellingprice').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#netamount').addClass('validate[required,custom[number],decval[7],maxSize[100]]');
					$('#productid').addClass('validate[required]');
					$('#grossamount').addClass('validate[required,maxSize[100]]');
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				} else if (msg == "Disallowed Key Characters.") {
					alertpopup("Restricted symbols (&^!#()) used.Please remove and try again");
					$("#processoverlay").hide();
				} else {
					alertpopup("Error during update.Please close and try again");
					$("#processoverlay").hide();
				}
				$('#dataupdatesubbtn').removeClass('singlesubmitonly');
			},
		});
	}
}
{// Record delete functionf
	function quoterecorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Quote/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable+"&quotemodule="+MODULEID,
			cache:false,
			success: function(msg)  {				
				var nmsg =  $.trim(msg);
				refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				if (nmsg == "TRUE") { 					
					alertpopup('Deleted successfully');
				} else if (nmsg == "Denied") {					
					alertpopup('Permission denied');
				}				
			},
		});
	}
}
{// Quote number generation
	function appendleadcontact(val,id) {
	   var accountid=$('#accountid').val();
	   if(val == 2) {
		var table='contact';
	   }  else {
		var table='lead';
	   }
	   $('#'+id+'').empty();
	   $('#'+id+'').append($("<option></option>"));
	   $.ajax({
			url:base_url+'Quote/getdropdownoptions?table='+table+'&accountid='+accountid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
			  if((data.fail) == 'FAILED') {
			  } else {
					$.each(data, function(index) {
						$('#'+id+'')
						.append($("<option></option>")
						.attr("value",data[index]['id'])
						.text(data[index]['name']));
					});
				} 
			   $('#'+id+'').trigger('change');
			},
	   });
	}
}
{// Address get and Set values	
	//setaddress
	function setaddress(id) {
		var value=$('#'+id+'').val();
		if(id == 'billingaddresstype') {
			var type='Billing';
		} else {
			var type='Shipping';
		}
		//account address
		if(value == 2) {
			var accountid=$('#accountid').val();
			getaddress(accountid,'account',type);
		} else if(value == 3) {
			var table= $('#leadcontacttype option:selected').text().toLowerCase();
			var gg=table.split('-');
			var addressid =$('#contactid').val();
			getaddress(addressid,gg[1],type);
		} else if(value == 5) {//swap the billing to shipping
			$('#billingaddress').val($('#shippingaddress').val());
			$('#billingpincode').val($('#shippingpincode').val());
			$('#billingcity').val($('#shippingcity').val());
			$('#billingstate').val($('#shippingstate').val());
			$('#billingcountry').val($('#shippingcountry').val());	
		} else if(value == 6) {//swap the shipping to billing
			$('#shippingaddress').val($('#billingaddress').val());
			$('#shippingpincode').val($('#billingpincode').val());
			$('#shippingcity').val($('#billingcity').val());
			$('#shippingstate').val($('#billingstate').val());
			$('#shippingcountry').val($('#billingcountry').val());	
		}
	}
	function getaddress(id,table,type) {
		if(id != null && id != '' && table != null && table !='') {
			var append=type.toLowerCase();
			$.ajax({
				url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if(data.result == true) {
						var datanames = ['5','address','pincode','city','state','country'];
						var textboxname = ['5',''+append+'address',''+append+'pincode',''+append+'city',''+append+'state',''+append+'country'];
						textboxsetvalue(textboxname,datanames,data,[]);
						Materialize.updateTextFields();
					}			
				},
			});
		}
	}
}
{// Get product details
	function getproductdetails(val) {
		var ordertypeid = $('#ordertypeid').val();
		if(checkValue(val) == true) {
			var pricebook = $('#pricebookid').val();
			var uomurl=base_url+'Uommaster';
			$.ajax({
				url:base_url+'Base/getproductdetails?id='+val+'&pricebook='+pricebook+'&ordertypeid='+ordertypeid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					var datanames = ['1','description'];
					var textboxname = ['1','descriptiondetail'];
					textboxsetvalue(textboxname,datanames,data,[]);
					productstoragefetch(val);
					if(jQuery.type(data.symbol) === "undefined"){
					}else{
						$("#instockdivhid label").html('Stock Quantity  <a href="'+uomurl+'" style="color:#575757;">('+data.symbol+')</a>'); //set the specified product's uom symbol with link
					}						
					QUANTITY_PRECISION = data.uomprecision; //set's the specified product quantity rounding.
					PRODUCTTAXABLE = data.taxable;	
					var unitprice = data.unitprice;
					if(unitprice == ''){
						var unitprice = 0;
					}	
					if(softwareindustryid == 4){
						if(ordertypeid == 5){
							$("#durationid").select2('val',data.durationid);							
						}else{			
							$("#durationid").select2('val',2);
						}
					}
					if(data.prodinprice == 0){
						$("#pricebookclickevent").show();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						if(softwareindustryid == 2){
							if(data.sellingprice != ''){
								var list_price = listprice(data.sellingprice,currencyconvresionrate);
								$('#sellingprice').val(list_price);
							}else if(data.unitprice != ''){
								var list_price = listprice(data.unitprice,currencyconvresionrate);
								$('#sellingprice').val(list_price);
							} else{
								$('#sellingprice').val(0);
							}
						}else{
							var list_price = listprice(unitprice,currencyconvresionrate);
							$('#unitprice').val(list_price);					
							//calculate selling price
							var list_price = listprice(data.sellingprice,currencyconvresionrate);
							$('#sellingprice').val(list_price);
						}
					}else{
						$("#pricebookclickevent").hide();
						var currencyconvresionrate =$('#currencyconvresionrate').val();
						var sellcurrencyconvresionrate =PRICBOOKCONV_VAL;
						if(softwareindustryid == 2){
							if(data.sellingprice != ''){
								var conversell = listprice(data.sellingprice,currencyconvresionrate);
								var list_price = listprice(conversell,sellcurrencyconvresionrate);
								$('#sellingprice').val(list_price);
							}else if(data.unitprice != ''){
								var list_price = listprice(data.unitprice,sellcurrencyconvresionrate);
								$('#sellingprice').val(list_price);
							} else{
								$('#sellingprice').val(0);
							}
						}else{
							var list_price = listprice(unitprice,currencyconvresionrate);
							$('#unitprice').val(list_price);					
							//calculate selling price
							var conversell = listprice(data.sellingprice,currencyconvresionrate);
							var list_price = listprice(conversell,sellcurrencyconvresionrate);
							$('#sellingprice').val(list_price);
						}
					}
					$('#quantity').val('1');
					//gross amount
					$("#grossamount").val($('#sellingprice').val());
					//pretax-total
					setzero(['grossamount','discountamount']);
					var pretax = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val());
					$("#pretaxtotal").val(pretax.toFixed(PRECISION));
					productnetcalculate();
				},
		    });
		}
	}
}
{// Discount calculation
	/*
	*Function to calculate the discount value
	*@param discountdata(jsonobject) - data contains discount value,discount type 
	*@param grossamount - grossamount
	*/
	function discountcalculation(discountdata,grossamount) {	
		if(discountdata != null && discountdata !=''){			
			var discount_amount = parseFloat(discountdata.value);
			if(discountdata.typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			return discount_amount.toFixed(PRECISION);
			
		} else {			
			return 0;
		}
	}
}
{// Product Net Calculation 
	function productnetcalculate() {
		setzero(['grossamount','discountamount','taxamount','chargeamount']);
		var grossamount=parseFloat($('#grossamount').val());
		var discount=parseFloat($('#discountamount').val());	
		var tax=parseFloat($('#taxamount').val());
		var charge=parseFloat($('#chargeamount').val());
		if(softwareindustryid == 2){
			var output=(grossamount-discount+tax).toFixed(PRECISION);
		}else{
			var output=(grossamount-discount+tax+charge).toFixed(PRECISION);
		}		
		$('#netamount').val(output);	
	}
}
{// Set zero if the value field is empty-to be base
	function setzero(arrvalue) {
		var a=arrvalue.length;
		for(var m=0;m < a;m++) {
			var fvalue= $('#'+arrvalue[m]+'').val();
			if(fvalue == '' || fvalue == null || fvalue == " " || isNaN(fvalue)){
				$('#'+arrvalue[m]+'').val(0);
			}
		}
	}
}	
{// Drop down values set for view
	function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url:base_url+'Quote/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-"+dataattrname,data[index]['datasid'])
						.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
						.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{// Terms and condition data fetch
	function tandcdatafrtch(id) {
		$.ajax({
			url:base_url+"Quote/termsandcontdatafetch?id="+id,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				filenamevaluefetch(data);
			},
		});
	}
}
{// Editor value fetch
	function filenamevaluefetch(filename) {
		//checks whether the Terms & condition is empty
		if(filename == '' || filename == null ) {			
			if($("#quotationstc_editor").length){
				froalaset(froalaarray);
			}
		} else {
			$.ajax({
				url: base_url + "Quote/editervaluefetch?filename="+filename,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					froalaedit(data,'quotationstc_editor');
				},
			});
		}
	}
}
{// Price book Overlay grid
	function pricebookgrid(page,rowcount) {
		var pid = $("#productid").val()!='' ? $("#productid").val() : 1;
		var territory = $("#territoryid").val() ? $("#territoryid").val() : 1;
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#pricebookgrid').width();
		var wheight = $('#pricebookgrid').height();
		/* col sort */
		var sortcol = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/pricebookview?maintabinfo=pricebookdetail&primaryid=pricebookdetailid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=219&productid='+pid+'&territory='+territory,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#pricebookgrid').empty();
				$('#pricebookgrid').append(data.content);
				$('#pricebookgridfooter').empty();
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('pricebookgrid');
				/* hide grid column */
				var hideprbokcol = ['currencyid','pricebookdetailid'];
				gridfieldhide('pricebookgrid',hideprbokcol);
				{/* sorting */
					$('.pricebooklistheadercolsort').click(function(){
						$('.pricebooklistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					sortordertypereset('pricebooklistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('#pricebooklistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						pricebookgrid(page,rowcount);
					});
				}
			},
		});
	}
	function productsearchgrid(page,rowcount) {
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#productsearchgrid').width();
		var wheight = $('#productsearchgrid').height();
		/* col sort */
		var sortcol = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').attr('id') : '0';
		if(softwareindustryid == 2){
			var ordertype = 1;
		}else{
			var ordertype = $('#ordertypeid').val();
		}	
		var moduleid = 9;
		if(softwareindustryid == 1){
			moduleid = 9;
		}else if (softwareindustryid == 2){
			moduleid = 98;
		}else if (softwareindustryid == 3){
			moduleid = 90;
		}else if (softwareindustryid == 4){
			moduleid = 89;
		}
		$.ajax({
			url:base_url+"Base/getproductsearch?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&ordertype="+ordertype+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+moduleid+'&industryid='+softwareindustryid, 
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#productsearchgrid').empty();
				$('#productsearchgrid').append(data.content);
				$('#productsearchgridfooter').empty();
				$('#productsearchgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('productsearchgrid');
				{/* sorting */
					$('.productlistheadercolsort').click(function(){
						$('.productlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					sortordertypereset('productlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('#productlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						productsearchgrid(page,rowcount);
					});
				}
			},
		});
	}
}
{// Grid summery value fetch
	function gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid) {
		var taxmode = '';
		var taxmasterid = $("#taxmasterid").val();
		if(taxmasterid){
			taxmode = 3;
		}else{
			taxmode = 2;
		}
		var additionalchargecategoryid = $("#additionalchargecategoryid").val();
		var chargemode = '';
		if(additionalchargecategoryid){
			chargemode = 3;
		}else{
			chargemode = 2;
		}
		/* sum netamount from grid */
		var totalnetamount =  parseFloat(getgridcolvalue('quotationsproaddgrid1','','netamount','sum'));
        $('#totalnetamount').val(totalnetamount.toFixed(PRECISION));
		/* update group discount */
		var gross = totalnetamount;
		var discount_json = $.parseJSON($('#groupdiscountdata').val()); //individual discount data					
		var discount_amount=discountcalculation(discount_json,gross); //to calculate discount					
		$('#groupdiscountamount').val(discount_amount);
		/* pretax-total */
		setzero(['totalnetamount','groupdiscountamount']);
		var pretax = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val());
		$("#grouppretaxtotal").val(pretax.toFixed(PRECISION));
		if(taxmode == 3) {
			//?update the tax overlaydata
			update_group_taxgriddata();
		}
		if(chargemode == 3) {
			//?update the charge overlaydata
			update_group_chargegriddata();
		}
		setTimeout(function(){
			calculatesummarydetail();
		},5); 	
	}
}
{// Calculates summary details
	function calculatesummarydetail() {
		setzero(['totalnetamount','groupdiscountamount','grouptaxamount','groupchargeamount','adjustmentamount']);
		var totalnetamount = parseFloat($("#totalnetamount").val());
		var discount = parseFloat($("#groupdiscountamount").val());
		var tax = parseFloat($("#grouptaxamount").val());
		var charge = parseFloat($("#groupchargeamount").val());
		var adjustment =parseFloat($("#adjustmentamount").val());	
		if(softwareindustryid == 2){
			var pre_adjustment_total = totalnetamount - discount+tax;
		}else{
			var pre_adjustment_total = totalnetamount - discount + tax + charge	;
		}		
		if(adjustment > 0) {
			var adjustment_data=$('#groupadjustmentdata').val();
			var parse_adjustment = $.parseJSON(adjustment_data);
			if(parse_adjustment != null){
				if(parse_adjustment.typeid == 3){
					var grandtotal = pre_adjustment_total - adjustment ;
				} else {
					var grandtotal = pre_adjustment_total + adjustment;
				}
			}
		} else {
			var grandtotal=pre_adjustment_total;
		}
		$("#grandtotal").val(parseFloat(grandtotal).toFixed(PRECISION));
		Materialize.updateTextFields();
	}
}

{// Clear grid data
	function clearformgriddata() {
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++) {
			cleargriddata(gridnames[j]);
		}
	}
}
{// Form to grid base function //Used on the new add's/edit's
	function gridformtogridvalidatefunction(gridnamevalue,method) {
		//Function call for validate
		$("#"+gridnamevalue+"validation").validationEngine({
			onSuccess: function() {
				var i = griddataid;
				var gridname = gridynamicname;
				var coldatas = $('#gridcolnames'+i+'').val();
				var columndata = coldatas.split(',');
				var coluiatas = $('#gridcoluitype'+i+'').val();
				var columnuidata = coluiatas.split(',');
				var datalength = columndata.length;
				/*Form to Grid*/
				if(METHOD == 'ADD' || METHOD == '') {
					formtogriddata(gridname,METHOD,'last','');
				} else if(METHOD == 'UPDATE') {
					formtogriddata(gridname,METHOD,'',UPDATEID);
				}
				/* Hide columns */
				var hideprodgridcol = ['taxgriddata','chargegriddata','discountdata','quotedetailid'];
				gridfieldhide('quotationsproaddgrid1',hideprodgridcol);
				/* Data row select event */
				datarowselectevt();
				$('#quotationsproupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
				$('#quotationsproaddbutton').show();//display the ADD button(inner-productgrid)*/			
				clearform('gridformclear');
				griddataid = 0;
				gridynamicname = '';
				$('#quotationsprocancelbutton').trigger('click');
				//summary calculate
				var gridsumcolname = ['grossamount','discountamount','taxamount','chargeamount','netamount'];
				var sumfieldid = ['summarygrossamount','groupdiscountamount','grouptaxamount','summaryadditionalchargeamount','summarynetamount'];
				var gridname = ['quotationsproaddgrid1','quotationsproaddgrid1','quotationsproaddgrid1','quotationsproaddgrid1','quotationsproaddgrid1'];
				gridsummeryvaluefetch(gridname,gridsumcolname,sumfieldid);
				$('.resetindividualoverlay').find('input:text').val('');
				//disable the type
				$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
				$('#taxgriddata,#chargegriddata,#discountdata').val(''); //reset the data
				$('#taxcategory,#chargecategory').select2('val',''); //reset the category
				cleargriddata('taxgrid');
				cleargriddata('chargesgrid');
				METHOD = 'ADD'; //reset
			},
			onFailure: function() {
				var dropdownid =['1','productid'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
}
{// Validate Percentage
	function validatepercentage(field) {
		var type =$('#discounttypeid').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3) {
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		} else {
		}
	}
}
{// Validation summary Percentage Validation
	function summaryvalidatepercentage(field) {
		var type =$('#groupdiscounttypeid').val();
		var value=field.attr('id');
		var num=parseFloat($('#'+value+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";	
		} else {
		}
	}
}
{// validate percentage add Charges
	function validateaddchargepercent(field) {
		var rr=field.attr('id');
		var dataid=$('#'+rr+'').data("val") ;
		var type =$('#additionalcaltype_'+dataid+'').val();
		var num=parseFloat($('#'+rr+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		}
		else{
		}  
	}
}
{// Validate summary add charges Percentage
	function validatesummaryaddchargepercent(field) {
		var rr=field.attr('id');
		var dataid=$('#'+rr+'').data("val") ;
		var type =$('#summaryadditionalcaltype_'+dataid+'').val();
		var num=parseFloat($('#'+rr+'').val());
		if(type == 3){
			if(num < 0 || num > 100)
			return "percent value 0%-100%";		
		}
		else{
		}  
	}
}
{// Edit Function
	function quoteeditdatafetchfun(datarowid) {
		clearformgriddata();		
		if($("#quotationstc_editor").length){
			froalaset(froalaarray);
		}
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();							
		retrievequotedata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();											
		setTimeout(function(){
		$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
		$("#accountid,#contactid,#leadcontacttype,#opportunityid").select2("readonly", false);},50);
		$('#currentmode').val(2);
		var qutdate = $("#quotedate").val();
		getandsetdate(qutdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function
	function quoteclonedatafetchfun(datarowid) {
		clearformgriddata();		
		if($("#quotationstc_editor").length){
			froalaset(froalaarray);
		}
		$('.resetoverlay').find('input[type=text],select,input[type=hidden]').val('');
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 								
		retrievequotedata(datarowid);
		var toresetdate = $("#validdate").val();
		firstfieldfocus();											
		setTimeout(function(){
		$('#ordertypeid,#taxmasterid,#additionalchargecategoryid,#pricebookid').attr("readonly", true);
		$("#accountid,#contactid,#leadcontacttype,#opportunityid").select2("readonly", false);},50);
		$('#currentmode').val(2);
		var qutdate = $("#quotedate").val();
		getandsetdate(qutdate);
		$("#validdate").val(toresetdate);
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
//check valid connert
function checkvalidconvertso(datarowid) {
	var status='';
	$.ajax({
		url:base_url+"Quote/checkconvertso?primarydataid="+datarowid,
		async:false,
		cache:false,
		success: function(msg) {
			status =msg;
		},
	});
	return status;
}
{//Generate icon after loads data - Workaround 
	//Pricebook icon appending
	function generateiconafterload(fieldid,clickevnid,adddiv){
		$("#"+fieldid+"").css('display','inline-block').css('width','75%');
		var icontabindex = $("#"+fieldid+"").attr('tabindex');
		$("#"+fieldid+"").after('<span class="innerfrmicon" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">library_books</i></span>'); 		
		$("<div class='row'></div>").insertAfter("#"+adddiv+"");
		$('label[for="'+fieldid+'"]').css('width','65%');
		
	}
	//this is for the product search/attribute icon-dynamic append
	function productgenerateiconafterload(fieldid,clickevnid){
		$("#s2id_"+fieldid+"").css('display','block').css('width','90%');
		var icontabindex = $("#s2id_"+fieldid+" .select2-focusser").attr('tabindex');
		$("#"+fieldid+"").after('<span class=" innerfrmicon" id="'+clickevnid+'" style="position:absolute;top:15px;right:15px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">search</i></span>');
		
	}	
}
//sanjeevi 28-01-2015 [Revised new grid by ramesh on 11-12-2015]
{//charges grid
	function chargesgrid() {
		var wwidth = $("#chargesgrid").width();
		var wheight = $("#chargesgrid").height();
		$.ajax({
			url:base_url+"Quote/chargegirdheaderinformationfetch?moduleid=223&width="+wwidth+"&height="+wheight+"&modulename=quotechargegrid",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#chargesgrid").empty();
				$("#chargesgrid").append(data.content);
				$("#chargesgridfooter").empty();
				$("#chargesgridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('chargesgrid');
				/* Calculate total charges */
				charge_data_total();
			},
		});
	}
	function taxgrid() {
		var wwidth = $("#taxgrid").width();
		var wheight = $("#taxgrid").height();
		$.ajax({
			url:base_url+"Quote/taxgirdheaderinformationfetch?moduleid=221&width="+wwidth+"&height="+wheight+"&modulename=quotetaxgrid",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#taxgrid").empty();
				$("#taxgrid").append(data.content);
				$("#taxgridfooter").empty();
				$("#taxgridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('taxgrid');
				/* Calculate total taxes */
				tax_data_total();
			},
		});	
	}
	function submitchargegrid() {
		//identify the type(group/individual);
		var additionalchargecategoryid=$('#additionalchargecategoryid').val();
		var type = '';
		if(additionalchargecategoryid){
			type = 3;
		}else{
			type = 2;
		}
		var totalcharge =  parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
		if(type == 2) {	//individual
			var griddataid = 'chargegriddata';
			var charge_category_id = 'chargecategory';
			var charge_amount_field = 'chargeamount';
		} else if(type == 3) {	//group
			var griddataid = 'groupchargegriddata';
			var charge_category_id = 'additionalchargecategoryid';
			var charge_amount_field = 'groupchargeamount';
		}
		var charge_json ={}; //declared as object
		charge_json.id = $('#'+charge_category_id+'').val(); //categoryid
		charge_json.data = getgridrowsdata('chargesgrid');//chargesgrid
		if(deviceinfo != 'phone'){
			$('#'+griddataid+'').val(JSON.stringify(charge_json));
		}		
		$('#'+charge_amount_field+'').val(totalcharge.toFixed(PRECISION));
		$('#additionaloverlay').fadeOut();
		resetoverlay('chargesgrid');
		if(type == 2){	//individual
			$('#quantity').trigger('focusout'); //automates recalculation
		} else if(type == 3) {	//group
			gridsummeryvaluefetch('','','');  //recalculate summary
		}
	}
	function submittaxgrid() {
		//identify the type(group/individual);
		var type = '';
		var taxmasterid=$('#taxmasterid').val();
		if(taxmasterid){
			type = 3;
		}else{ type = 2;}
		var totalcharge = parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
		if(type == 2){	//individual
			var griddataid = 'taxgriddata';
			var tax_category_id = 'taxcategory';
			var tax_amount_field = 'taxamount';
		} else if(type == 3) {	//group
			var griddataid = 'grouptaxgriddata';
			var tax_category_id = 'taxmasterid';
			var tax_amount_field = 'grouptaxamount';
		}
		var tax_json ={}; //declared as object
		tax_json.id = $('#'+tax_category_id+'').val(); //categoryid
		tax_json.data = getgridrowsdata('taxgrid');//taxgrid
		if(deviceinfo != 'phone'){
			$('#'+griddataid+'').val(JSON.stringify(tax_json));
		}		
		$('#'+tax_amount_field+'').val(totalcharge);
		$('#taxoverlay').fadeOut();
		resetoverlay('taxgrid');		
		if(type == 2) {	//individual
			$('#quantity').trigger('focusout'); //automates recalculation
		} else if(type == 3) {	//group
			gridsummeryvaluefetch('','','');  //recalculate summary
		}
	}
	function loadgriddata(gridid,grid_data) {
		cleargriddata(gridid);
		$('#'+gridid+' .gridcontent div.data-content').empty();
		var gridlength = grid_data.length;
		var j=0;
		for(var i=0;i<gridlength;i++) {
			/*add json data to grid*/
			addinlinegriddata(gridid,j+1,grid_data[i],'json');
			j++;
		}
		/*data row select event*/
		datarowselectevt();
		/*column resize*/
		columnresize(gridid);
	}
	function resetoverlay(gridid) {
		cleargriddata(gridid);
	}
	/*	*Automatic Recalculations And Datareset of Taxgrid data	*/
	function update_taxgriddata() {
		var main_data = $.parseJSON($('#taxgriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != '') {
			var c_main_data = main_data.data;
			if(c_main_data.length > 0) {
				var datacount = c_main_data.length;
				var c_sum = 0;
				setzero(['grossamount','discountamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++) {	//iterates to autoupdate inner records				
					var c_amount = parseFloat($('#grossamount').val()); 
					var c_discount = parseFloat($('#discountamount').val()); 
					var c_value = c_main_data[ic]['rate'];
					var value = ((parseFloat(c_value)/100)*(c_amount-c_discount)).toFixed(PRECISION);
					c_main_data[ic]['amount'] = value;			
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				var totalcharge =parseFloat(totalcharge);
				$('#taxamount').val(totalcharge.toFixed(PRECISION));
				var tax_json ={}; //declared as object
				tax_json.id = main_data.id; //categoryid
				tax_json.data = c_main_data;	//tax grid	
				$('#taxgriddata').val(JSON.stringify(tax_json));
			}
		}
	}
	/*	*	Automatic Recalculations And Datareset of Chargegrid data	*/
	function update_chargegriddata() {
		var main_data = $.parseJSON($('#chargegriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != '') {
			var c_main_data = main_data.data;
			if(c_main_data.length > 0) {
				var datacount = c_main_data.length;
				var c_sum=0;
				setzero(['grossamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++) {	//iterates to autoupdate inner records				
					var value = c_main_data[ic]['amount']; 
					if(c_main_data[ic]['calculationtypeid'] == 3) { //percentagetype					
						var c_amount = $('#grossamount').val(); 
						var c_value = c_main_data[ic]['value'];
						var value = ((parseFloat(c_value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
						c_main_data[ic]['amount'] = value;
					}
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#chargeamount').val(totalcharge);
				var charge_json ={}; //declared as object
				charge_json.id = main_data.id; //categoryid
				charge_json.data = c_main_data;	//chargesgrid
				$('#chargegriddata').val(JSON.stringify(charge_json));
			}
		}
	}
	/*	* Automatic Recalculations And Datareset of Group Tax Grid	*/
	function update_group_taxgriddata(){
		var main_data = $.parseJSON($('#grouptaxgriddata').val()); //group taxgrid data
		if(main_data != null && main_data != ''){
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum = 0;
				setzero(['totalnetamount','groupdiscountamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var c_amount = parseFloat($('#totalnetamount').val()); 
					var c_discount = parseFloat($('#groupdiscountamount').val()); 
					var c_value = c_main_data[ic]['rate'];
					var value = ((parseFloat(c_value)/100)*(c_amount-c_discount)).toFixed(PRECISION);
					c_main_data[ic]['amount'] = value;				
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#grouptaxamount').val(totalcharge);
				var tax_json ={}; //declared as object
				tax_json.id = main_data.id; //categoryid
				tax_json.data = c_main_data;	//tax grid
				$('#grouptaxgriddata').val(JSON.stringify(tax_json));
			}
		}		
	}
	function update_group_chargegriddata() {
		var main_data = $.parseJSON($('#groupchargegriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != '') {
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum=0;
				setzero(['totalnetamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var value = c_main_data[ic]['amount'];
					if(c_main_data[ic]['calculationtypeid'] == 3) { //percentagetype					
						var c_amount = $('#totalnetamount').val(); 
						var c_value = c_main_data[ic]['value'];
						var value = ((parseFloat(c_value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
						c_main_data[ic]['amount'] = value;
					}
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#groupchargeamount').val(totalcharge);
				var charge_json ={}; //declared as object
				charge_json.id = main_data.id; //categoryid
				charge_json.data = c_main_data;	//chargesgrid	
				$('#groupchargegriddata').val(JSON.stringify(charge_json));
			}
		}
	}
	/*calculate the exchange rate from basecurrency to converted currency*/
	function listprice(unitprice,rate) {
		var value = (parseFloat(unitprice)/(1/parseFloat(rate))).toFixed(PRECISION);		
		return value;
	}
	/*retrieve the conversion rate(pricebook currency to current currency)*/
	function getcurrencyconversionrate(currencyid,current_currencyid,quotedate){
		var rate = '';
		if(checkValue(currencyid) == true && checkValue(current_currencyid) == true){
			$.ajax({
				url:base_url+"index.php/Base/getcurrencyconversionrate",
				data:{fromcurrency:currencyid,tocurrency:current_currencyid,quotedate:quotedate},
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					rate = data.rate;					
				}
			});
			return rate;
		} else {
			return rate;
		}
	}
	/*	*to set the sellingprice for Product/item after pricebook selection	*/
	function setsellingprice(sellingprice) {
		var sellingprice =sellingprice.toFixed(PRECISION);
		$('#sellingprice').val(sellingprice).trigger('change'); //sets the value and trigger change to do other calc
	}
	/*	* reappend currency for dataattribute-(used because there is no data attribute in base dd)	*/
	function appendcurrency(id) {
		$('#'+id+'').empty();
		$('#'+id+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"index.php/Quote/specialcurrency",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {				
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-precision ='" +data[m]['decimal']+ "'  value = '" +data[m]['currencyid']+ "'>"+data[m]['currencyname']+"</option>";
					}
					//after run
					$('#'+id+'').append(newddappend);
				}	
			},
		});
	}
	/*	* Calculates the records of tax and sets it in summary total.	*/
	function tax_data_total() {
		var total =  parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
        $('#tax_data_total').val(total.toFixed(PRECISION));
	}
	/*	* Calculates the records of charge and sets it in summary total.	*/
	function charge_data_total() {
		var total =  parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
        $('#charge_data_total').val(total.toFixed(PRECISION));
	}
	/*	*Calculates the discount live-single.	*/
	function discount_total(){
		setzero(['grossamount','discountpercent']); //sets zero on EMPTY					
		var typeid = $('#discounttypeid').val(); //discount typeid
		var value = $('#discountpercent').val();	//discount values		
		var grossamount = $('#grossamount').val();		
		//
		if(typeid != null && typeid !=''){			
			var discount_amount = parseFloat(value);
			if(typeid == 3) {//on percentage type. 
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
		} else {			
			var amt_discount =  0;
		}
		$('#singlediscounttotal').val(amt_discount);
		//
	}
	/*	*Calculates the group discount live-.	*/
	function group_discount_total(){	
		setzero(['groupdiscountamount','totalnetamount']); //sets zero on EMPTY					
		var typeid = $('#groupdiscounttypeid').val(); //discount typeid
		var value = $('#groupdiscountpercent').val();	//discount values		
		var grossamount = $('#totalnetamount').val();		
		//
		if(typeid != null && typeid !='') {
			var discount_amount = parseFloat(value);
			if(typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
		} else {
			var amt_discount =  0;
		}
		$('#groupdiscounttotal').val(amt_discount);
		//
	}
	function calculateinnerchargeedit(){ 
			var id = $('#chargesgrid div.gridcontent div.active').attr('id');
			var tmp = getgridcolvalue('chargesgrid',id,'value','');	
			var c_mode = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				c_mode = 3;
			}else{
				c_mode = 2;
			}
			if(id != '') {
				jQuery('#chargesgrid').saveRow(id, true);
				var calctype = getgridcolvalue('chargesgrid',id,'calculationtypeid','');
				var value = getgridcolvalue('chargesgrid',id,'value','');
				if(!isNaN(value) && value !=''){
				setzero(['grossamount']); //set zero on empty
				if(calctype == 3){	//percentage
					if(c_mode == 2){
						var c_amount = parseFloat($('#grossamount').val()); //individual
						var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
					} else if(c_mode == 3) {
						var c_amount = parseFloat($('#totalnetamount').val()); //group
						var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
					}
				}				
				}else {
					var value = 0;					
				}		
				setTimeout(function() {
					charge_data_total();
				},200);
			} 
	}
	function calculateinnertaxedit(){
		var id = $('#taxgrid div.gridcontent div.active').attr('id');
		if(id != '' ) {
			var type = '';
			var taxmasterid = $('#taxmasterid').val();
			if(taxmasterid){
				type = 3;
			}else{
				type = 2;
			}
			jQuery('#taxgrid').saveRow(id, true);				
			var value = getgridcolvalue('taxgrid',id,'rate','');
			if(!isNaN(value) && value != ''){
				if(type == 2){
					setzero(['grossamount','discountamount']); //set zero on empty
					var c_amount = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val()); //individual
				} else if(type == 3) {
					var c_amount = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val()); //individual
				}
				var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);				
			}else {
				var value = 0;						
			}
			setTimeout(function(){
				tax_data_total();
			},200);
		}
	}
	/*	* Quote stage-data retrival for Lost-Cancel-Draft-Convert	*/
	function quotestage(id){
		var quotestage =0;
		if(checkValue(id) == true ){
			$.ajax({
				url:base_url+"index.php/Quote/quotecurrentstage",
				data:{quoteid:id},
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					quotestage = data.quotestage;
				}	
			});
			return quotestage;
		}
	}
	function quotestatus(id){
		var quotestatus =0;
		if(checkValue(id) == true ){
			$.ajax({
				url:base_url+"index.php/Quote/quotecurrentstatus",
				data:{quoteid:id},
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					quotestatus = data.quotestatus;
				}	
			});
			return quotestatus;
		}
	}
}
//product storage get - in stock 
function productstoragefetch(productid) {
	$.ajax({
		url:base_url+"Quote/productstoragefetchfun?productid="+productid,
		dataType : 'json',
		async:false,
		cache:false,
		success :function(data) {
			nmsg = $.trim(data);
			$("#instock").val(nmsg)
		},
	});
}