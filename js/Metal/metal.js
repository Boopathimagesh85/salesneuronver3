$(document).ready(function(){	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		metaladdgrid();
		metalcrudactionenable();
	}
	METALDELETE = 0;
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			metaladdgrid();
		});
	}
	setTimeout(function() {
		$(".ajax-file-upload>form>input").removeAttr("disabled");//remove disabled attribute for uploadFile
		}, 1000);
	//hidedisplay
	$('#metaldataupdatesubbtn,#groupcloseaddform').hide();
	$('#metalsavebutton').show();
	{
		 $("#metalreloadicon").click(function(){
			clearform('cleardataform');
			metalrefreshgrid();
			$("#metaldataupdatesubbtn").hide();
			$("#metalsavebutton").show();
	});
		$( window ).resize(function() {
			maingridresizeheightset('metaladdgrid');
			sectionpanelheight('groupsectionoverlay');
		});
		//validation for  Add  
		$('#metalsavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#metalformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#metalformaddwizard").validationEngine({
			onSuccess: function() {
				metaladdformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update metal information
		$('#metaldataupdatesubbtn').click(function(e){
			if(e.which==1 || e.which === undefined) {
				$("#metalformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#metalformeditwizard").validationEngine({
			onSuccess: function() {
				metalupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
		    resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
		});
	}
	// puirty set for rate calculation
	$("#metalpurityseticon").click(function(){		
		var datarowid = $('#metaladdgrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			$('#metalupdate').show();
			loadmetalbasedpurity(datarowid);
			setTimeout(function(){
				setmetalbasedpurity(datarowid);
			},25);
			$('#metalpurityoverlay').fadeIn();
			firstfieldfocus();
		}
		else{
			alertpopup(selectrowalert);
		}
	});
	// update purity
    $('#metalpuirty').click(function(){
    	var datarowid = $('#metaladdgrid div.gridcontent div.active').attr('id');
		var purityid=$('#metal_purity').val();
		updatepurity(purityid,datarowid);
	});
 // overlay close
	$("#metalpuirtyclose").click(function(){
		$('#metalpurityoverlay').fadeOut();
		$("#metalreloadicon").trigger('click');
	});
	//for toggle
	$('#metalviewtoggle').click(function() {
		if ($(".metalfilterslide").is(":visible")) {
			$('div.metalfilterslide').addClass("filterview-moveleft");
		}else{
			$('div.metalfilterslide').removeClass("filterview-moveleft");
		}
	});
	$('#metalclosefiltertoggle').click(function(){
		$('div.metalfilterslide').removeClass("filterview-moveleft");
	});
	//filter
	$("#metalfilterddcondvaluedivhid").hide();
	$("#metalfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#metalfiltercondvalue").focusout(function(){
			var value = $("#metalfiltercondvalue").val();
			$("#metalfinalfiltercondvalue").val(value);
			$("#metalfilterfinalviewconid").val(value);
		});
		$("#metalfilterddcondvalue").change(function(){
			var value = $("#metalfilterddcondvalue").val();
			metalmainfiltervalue=[];
			$('#metalfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    metalmainfiltervalue.push($mvalue);
				$("#metalfinalfiltercondvalue").val(metalmainfiltervalue);
			});
			$("#metalfilterfinalviewconid").val(value);
		});
	}
	metalfiltername = [];
	$("#metalfilteraddcondsubbtn").click(function() {
		$("#metalfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#metalfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(metaladdgrid,'metal');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}

//Documents Add Grid
function metaladdgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#metaladdgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#metalsortcolumn").val();
	var sortord = $("#metalsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.metalheadercolsort').hasClass('datasort') ? $('.metalheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.metalheadercolsort').hasClass('datasort') ? $('.metalheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.metalheadercolsort').hasClass('datasort') ? $('.metalheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#metalviewfieldids').val();
	userviewid = userviewid == '' ? '116' : userviewid;
	var filterid = $("#metalfilterid").val();
	var conditionname = $("#metalconditionname").val();
	var filtervalue = $("#metalfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=metal&primaryid=metalid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#metaladdgrid').empty();
			$('#metaladdgrid').append(data.content);
			$('#metaladdgridfooter').empty();
			$('#metaladdgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('metaladdgrid');
			{//sorting
				$('.metalheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.metalheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.metalheadercolsort').hasClass('datasort') ? $('.metalheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.metalheadercolsort').hasClass('datasort') ? $('.metalheadercolsort.datasort').data('sortorder') : '';
					$("#metalsortorder").val(sortord);
					$("#metalsortcolumn").val(sortcol);
					metaladdgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('metalheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					metaladdgrid(page,rowcount);
					$("#processoverlay").hide();
				});	
				$('#metalpgrowcount').change(function(){ //unitofmeasureaddgridfooter
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					metaladdgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#metaladdgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#metalpgrowcount').material_select();
		},
	});
}
function metalcrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			e.preventDefault();
			firstfieldfocus();
			$('#metaldataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#metalsavebutton').show();
			$('#metalprimarydataid').val('');
		});
	}
	$("#editicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#metaladdgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			metalgetformdata(datarowid);
			firstfieldfocus();
			masterfortouch = 1;
			Materialize.updateTextFields();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function(){
		var datarowid = $('#metaladdgrid div.gridcontent div.active').attr('id');		
		if(datarowid){
			clearform('cleardataform');
			$("#metalprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			$.ajax({
				url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=purity&fieldid=metalid&table1=rate&fieldid1=metalid&table2=''&fieldid2=''",
				success: function(msg) { 
				if(msg > 0){
				alertpopup("Kindly delete the purity linked to metal");						
				}else{
					combainedmoduledeletealert('metaldeleteyes');
					if(METALDELETE == 0) {
						$("#metaldeleteyes").click(function(){
							var datarowid =$("#metalprimarydataid").val();
							metalrecorddelete(datarowid);
						});
					}
					METALDELETE = 1;
				}
				},
				});
			
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function metalrefreshgrid() {
		var page = $('ul#metalpgnum li.active').data('pagenum');
		var rowcount = $('ul#metalpgnumcnt li .page-text .active').data('rowcount');
		metaladdgrid(page,rowcount);
	}
}
//new data add submit function
function metaladdformdata() {
    var formdata = $("#metaladdform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Metal/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
            	resetFields();
				metalrefreshgrid();
				alertpopup(savealert);			
            }
        },
    });
}
//old information show in form
function metalgetformdata(datarowid) {
	var metalelementsname = $('#metalelementsname').val();
	var metalelementstable = $('#metalelementstable').val();
	var metalelementscolmn = $('#metalelementscolmn').val();
	var metalelementpartable = $('#metalelementspartabname').val();
	$.ajax( {
		url:base_url+"Metal/fetchformdataeditdetails?metalprimarydataid="+datarowid+"&metalelementsname="+metalelementsname+"&metalelementstable="+metalelementstable+"&metalelementscolmn="+metalelementscolmn+"&metalelementpartable="+metalelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				metalrefreshgrid();
			} else {
				var txtboxname = metalelementsname + ',metalprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = metalelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
			
		}
	});
	$('#metaldataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#metalsavebutton').hide();
}
//update old information
function metalupdateformdata() {
	var formdata = $("#metaladdform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Metal/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#metaldataupdatesubbtn').hide();
				$('#metalsavebutton').show();
				resetFields();
				$(".addsectionclose").trigger("click");
				metalrefreshgrid();
				$('#metalimageattachdisplay').empty();
				alertpopup(updatealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["metalcloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//update old information.
function metalrecorddelete(datarowid) {
	var metalelementstable = $('#metalelementstable').val();
	var elementspartable = $('#metalelementspartabname').val();
    $.ajax({
        url: base_url + "Metal/deleteinformationdata?metalprimarydataid="+datarowid+"&metalelementstable="+metalelementstable+"&metalparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	metalrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	metalrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//metalnamecheck
function metalnamecheck() {
	var primaryid = $("#metalprimarydataid").val();
	var accname = $("#metalname").val();
	var elementpartable = $('#metalelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Metal name already exists!";
				}
			}else {
				return "Metal name already exists!";
			}
		} 
	}
}
//metalshortnamecheck
function metalshortnamecheck() {
	var primaryid = $("#metalprimarydataid").val();
	var accname = $("#metalshortname").val();
	var fieldname = 'shortname';
	var elementpartable = $('#metalelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Metal short name already exists!";
				}
			}else {
				return "Metal short name already exists!";
			}
		} 
	}
}
function loadmetalbasedpurity(datarowid){
	$('#metal_purity').empty();
	$('#metal_purity').select2('val','');
	$('#metal_purity').append($("<option value='0'>Select</option>"));
	$.ajax({
		url:base_url+'Metal/loadmetalbasedpurity',
		dataType:'json',
		data:"primaryid="+ datarowid,
		method: "POST",
		async:false,
		success: function(data) {
		  if((data.fail) == 'FAILED') {
		  } else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value ='" +data[m]['purityid']+ "'>"+data[m]['purityname']+"</option>";
				}						
				$('#metal_purity').append(newddappend);						
		  }    
		 },
	});
}
function setmetalbasedpurity(datarowid){
	$.ajax({
		url:base_url+'Metal/setmetalbasedpurity',
		dataType:'json',
		data:"primaryid="+ datarowid,
		method: "POST",
		async:false,
		success: function(data) {
		  $('#metal_purity').select2('val',data).trigger('change');
		 },
	});
}
function updatepurity(purityid,datarowid){
	$.ajax({
		url:base_url+'Metal/updatemetalbasedpurity',
		dataType:'text',
		data:"primaryid="+ purityid+"&metalid="+datarowid,
		method: "POST",
		async:false,
		success: function(data) {
		  if(data == 'SUCCESS') {
			  $('#metalpurityoverlay').fadeOut();
			     $("#metalreloadicon").trigger('click');
				alertpopup(updatealert);
		  } else {
			    alertpopup(submiterror);				
		  }    
		  
		},
	});
}