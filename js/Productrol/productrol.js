$(document).ready(function() {
	$(document).foundation();
	productrolonedit = 0;
	PRODUCTADDONDELETE = 0;
	// Maindiv height width change
	maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);
	$('#groupcloseaddform').hide();
	$('#productaddonviewtoggle').hide();
	{// Grid Calling Functions
		productrolgrid();
	}
	$( window ).resize(function() {
		 sectionpanelheight('groupsectionoverlay');
		 maingridresizeheightset('productrolgrid');
	 });
	{// For touch
		fortabtouch = 0;
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	{//product charge validate	
		//validation for product charge Add  
		$('#productroladd').click(function() {
			$("#productroladdvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#productroladdvalidate").validationEngine( {
			onSuccess: function() {
				productroladd();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//update product information.
		$('#productrolupdate').click(function() {
			$("#productrolupdatevalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#productrolupdatevalidate").validationEngine({
			onSuccess: function() {
				productrolupdate();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{	
		//addicon click work
		$("#addicon").click(function(){
			clearform('productaddonform');
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#productrolupdate').hide();
			$('#productroladd').show();
			//setTimeout(function() {
				$("#productid,#purityid,#rangemasterid,#sizemasterid").prop('disabled',false);
				$('#reordertypeid').select2('val',2);
				productrolonedit = 0;
			//},50);
			$("#productid").select2('focus');
			$('.sectionpanel').animate({scrollTop:0},'slow');
		    $('.addformcontainer').animate({scrollTop:0},'slow');
			Materialize.updateTextFields();
		});
		//edit icon click
		$("#editicon").click(function(){			
			var datarowid = $('#productrolgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				productrolretrive(datarowid);
				$('#productrolupdate').show();
				$('#productroladd').hide();
			} else {
				alertpopup(selectrowalert);
			}
		});
		//Delete
		$("#deleteicon").click(function(){			
			var datarowid = $.trim($('#productrolgrid div.gridcontent div.active').attr('id'));
			if(datarowid != ''){
				combainedmoduledeletealert('addondeleteyes','Are you sure,you want to delete this addon?');
				if(PRODUCTADDONDELETE == 0) {
					$("#addondeleteyes").click(function(){				
						productroldelete();
					});
				}
				PRODUCTADDONDELETE = 1;
			} else {
				alertpopup(selectrowalert);
			}
		});
		$('.addsectionclose').click(function(){
			resetFields();
			$("#productrolsectionoverlay").addClass("closed");
			$("#productrolsectionoverlay").removeClass("effectbox");
		});
		$('#alertsdoublecloseproductrol').click(function(){
			$("#primaryid").val('');
		});
		$('#alertsdoubleeditproductrol').click(function(){
		   $('#productrolalertsdouble').fadeOut();
		   $('#productrolupdate').show();
		   $('#productroladd').hide();
		   productrolretrive($('#primaryid').val());
		});
	}
	{//filter
		$("#productrolfilterddcondvaluedivhid").hide();
		$("#productrolfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#productrolfiltercondvalue").focusout(function(){
				var value = $("#productrolfiltercondvalue").val();
				$("#productrolfinalfiltercondvalue").val(value);
				$("#productrolfilterfinalviewconid").val(value);
			});
			$("#productrolfilterddcondvalue").change(function(){
				var value = $("#productrolfilterddcondvalue").val();
				productrolmainfiltervalue=[];
				$('#productrolfilterddcondvalue option:selected').each(function(){
					var $mvalue =$(this).attr('data-ddid');
					productrolmainfiltervalue.push($mvalue);
					$("#productrolfinalfiltercondvalue").val(productrolmainfiltervalue);
				});
				$("#productrolfilterfinalviewconid").val(value);
			});
		}
		productrolfiltername = [];
		$("#productrolfilteraddcondsubbtn").click(function() {
			$("#productrolfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#productrolfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
			dashboardmodulefilter(productrolgrid,'productrol');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//validation wok
		//max quantity validation
		$("#maximumquantity").change(function(){
			setzero(['maximumquantity','minimumquantity','reorderquantity']);
			var qty = parseInt($(this).val());
			if(qty){
				var reorderqty = parseInt($("#reorderquantity").val());
				var minqty = parseInt($("#minimumquantity").val());
				var maxqty = parseInt($("#maximumquantity").val());
				if(minqty > minqty){
					alertpopup("Maximum Quantity should be greater than Minimum Quantity");
					$(this).val(" ");
				}else if(reorderqty > maxqty){
					alertpopup("Maximum Quantity should be greater than or Equal to Reorder Quantity");
					$(this).val(" ");
				}					
			}
		});
		$("#minimumquantity").change(function(){
			setzero(['maximumquantity','minimumquantity','reorderquantity']);
			var qty = parseInt($(this).val());
			if(qty){
				var minqty = parseInt($("#minimumquantity").val());
				var maxqty = parseInt($("#maximumquantity").val());
				if(minqty > minqty){
					alertpopup("Maximum Quantity should be greater than Minimum Quantity");
					$(this).val(" ");
				}			
			}
		});
		$("#rangemasterid").change(function() {
			var towt = $("#rangemasterid").find("option:selected").data('torange');
			$("#toweight").val(towt);
			var fromwt = $("#rangemasterid").find("option:selected").data('fromrange');
			$("#fromweight").val(fromwt);
			var productid = $("#productid").find('option:selected').val();
			var isproductsize = $("#productid").find('option:selected').data('size');
			var purityid = $("#purityid").find('option:selected').val();
			var rangemasterid = $("#rangemasterid").find('option:selected').val();
			if(isproductsize == 'No') {
				checkweightexist(productid,rangemasterid,1,purityid);
			}
			Materialize.updateTextFields();
		});
		$("#sizemasterid").change(function() {
			var productid = $("#productid").find('option:selected').val();
			var purityid = $("#purityid").find('option:selected').val();
			var rangemasterid = $.trim($("#rangemasterid").find('option:selected').val());
			var sizemasterid = $("#sizemasterid").find('option:selected').val();
			if(rangemasterid != '' ) {
				checkweightexist(productid,rangemasterid,sizemasterid,purityid);
			}else{
				$("#sizemasterid").select2('val','');
				alertpopup('please select weight range');
			}
		});
		$("#toweight").change(function(){
			setzero(['fromweight','toweight']);
			var weight = parseInt($(this).val());
			if(weight){
				var toweight = parseInt($("#toweight").val());
				var fromweight = parseInt($("#fromweight").val());
				if(toweight < fromweight){
					alertpopup("To weight should be greater than the from weight");
					$(this).val(" ");
				} else {
					/* var productid = $("#productid").val();
					var reordertypeid = $("#reordertypeid").val();
					var sizemasterid = $("#sizemasterid").val();
					checkweightexist(productid,reordertypeid,sizemasterid,toweight,fromweight); */
				}
			}
		});
		$("#productid").change(function() {
			sizehideshow();
			$("#sizemasterid").select2('val','');
			if(productrolonedit == 0) {
				puritysetbasedonproduct();
				$("#rangemasterid").select2('val','');
				$("#fromweight,#toweight").val('');
			}
		});
		$("#purityid").change(function() {
			var metalid = $("#purityid").find('option:selected').data('metalid');
			if(metalid == 3) { //silver
				$("#rangemasterid option").addClass("ddhidedisplay");
				$("#rangemasterid option[data-metalid="+metalid+"]").removeClass("ddhidedisplay");
			} else { //gold
				$("#rangemasterid option").addClass("ddhidedisplay");
				$("#rangemasterid option[data-metalid="+metalid+"]").removeClass("ddhidedisplay");
			}
		}); 
	}
});
// Product Type View Grid
	function productrolgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		var accointid='';
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $( window ).width();
		var wheight = $( window ).height();
		/*col sort*/
		var sortcol = $("#producrolsortcolumn").val();
		var sortord = $("#productrolesortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.productrolheadercolsort').hasClass('datasort') ? $('.productrolheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.productrolheadercolsort').hasClass('datasort') ? $('.productrolheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.productrolheadercolsort').hasClass('datasort') ? $('.productrolheadercolsort.datasort').attr('id') : '0';	
		var filterid = $("#productrolfilterid").val();
		var conditionname = $("#productrolconditionname").val();
		var filtervalue = $("#productrolfiltervalue").val();
		var userviewid = 201;
		var viewfieldids = 137;
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=productrol&primaryid=productrolid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#productrolgrid').empty();
				$('#productrolgrid').append(data.content);
				$('#productrolgridfooter').empty();
				$('#productrolgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('productrolgrid');
				{//sorting
					$('.productrolheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.productrolheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.productrolheadercolsort').hasClass('datasort') ? $('.productrolheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.productrolheadercolsort').hasClass('datasort') ? $('.productrolheadercolsort.datasort').data('sortorder') : '';
						$("#producrolsortorder").val(sortord);
						$("#producrolsortcolumn").val(sortcol);
						productrolgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('productrolheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						productrolgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#productrolpgrowcount').change(function() {
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						productrolgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$(".gridcontent").click(function(){
					var datarowid = $('#productrolgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#productrolpgrowcount').material_select();
			},
		});
	}
{//refresh grid
	function productrolrefreshgrid() {
		var page = $('#prev').data('pagenum');
		var rowcount =$('.pagerowcount').data('rowcount');
		productrolgrid(page,rowcount);
	}
}
/*product rol add	*/
function productroladd() {
	var formdata = $("#productaddonform").serialize();	
	var amp = '&';	
	var datainformation = amp + formdata;
	var reordertypeid = $('#reordertypeid').find('option:selected').val();
	$('#processoverlay').show();
	$.ajax({
		url: base_url +"Productrol/createproductrol",
		data: "datas=" + datainformation+amp+"reordertypeid="+reordertypeid,
		type: "POST",
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				$(".addsectionclose").trigger("click");
				$('#processoverlay').hide();
				alertpopup('Data added successfully');
			} else if (nmsg == 'Exist') {
				alertpopup('Already the combination exist. You cant add the same combination again');
				$(".addsectionclose").trigger("click");
				$('#processoverlay').hide();
			} else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
			productrolrefreshgrid();		
		},
	});
}
//data retrive
function productrolretrive(datarowid) {
	$('#processoverlay').show();
	var elementsname = ['11','productid','reordertypeid','sizemasterid','fromweight','toweight','minimumquantity','reorderquantity','maximumquantity','purityid','rangemasterid','accountid'];
	//form field first focus
	firstfieldfocus();
	$.ajax( {
		url:base_url+"Productrol/productrolretrieve?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data) { 
			$('#processoverlay').hide();
			var txtboxname = elementsname;
			var dropdowns = ['6','productid','reordertypeid','sizemasterid','purityid','rangemasterid','accountid'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$("#primaryid").val(datarowid);
			productrolonedit=1;
			setTimeout(function() {
				$("#productid").select2('val',data.productid);
				puritysetbasedonproduct();
				$("#purityid").select2('val',data.purityid);
				sizehideshow();
			},30);
			$("#productid,#sizemasterid,#rangemasterid,#purityid").prop('disabled',true);
			Materialize.updateTextFields();
		}
	});
}
//product rol data retrive
function productrolupdate() {
	var formdata = $("#productaddonform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid = $('#primaryid').val();
	var productid = $("#productid").find('option:selected').val();
	var purityid = $("#purityid").find('option:selected').val();
	var rangemasterid = $("#rangemasterid").find('option:selected').val();
	var sizemasterid = $.trim($("#sizemasterid").find('option:selected').val());
	var reordertypeid = $('#reordertypeid').find('option:selected').val();
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
	$.ajax({
		url: base_url + "Productrol/productrolupdate",
		data: "datas=" + datainformation +"&primaryid="+ datarowid+"&productid="+productid+"&purityid="+purityid+"&rangemasterid="+rangemasterid+amp+"sizemasterid="+sizemasterid+amp+"reordertypeid="+reordertypeid,
		type: "POST",
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			$("#productrolsectionoverlay").addClass("closed");
			$("#productrolsectionoverlay").removeClass("effectbox");
			clearform('productrolform');
			productrolrefreshgrid();
			$('#productrolupdate').hide();
			$('#productroladd').show();	
			if (nmsg == 'SUCCESS') {
				$(".addsectionclose").trigger("click");
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopup(updatealert);
			} else {
				setTimeout(function() {
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
			}             
		},
	});
}
//product rol delete
function productroldelete() {	
	var primaryid = $('#productrolgrid div.gridcontent div.active').attr('id');
	$('#processoverlay').show();
	$.ajax({
		url: base_url + "Productrol/productroldelete?primaryid="+primaryid,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			productrolrefreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
			if (nmsg == 'SUCCESS') {
				$('#processoverlay').hide();
				alertpopup(deletealert);
			} else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
		},
	});
}
//check weight already exist
function checkweightexist(productid,rangemasterid,sizemasterid,purityid){
	/* var productname=$('#productid').find('option:selected').data('productname');
	var ordertypename=$('#reordertypeid').find('option:selected').data('reordertypename');
	var sizemastername=$('#sizemasterid').find('option:selected').data('sizemastername');*/
	var primaryid = $("#primaryid").val();
	 $.ajax({
	    url: base_url + "Productrol/checkweightexist",
		 data: "productid=" + productid +"&rangemasterid="+rangemasterid+"&sizemasterid="+sizemasterid+"&purityid="+purityid+"&primaryid="+primaryid,
		type: "POST",
		dataType:'json',
		async:false,
		success: function(msg) {
			if(msg['count'] > 0){
				/* $('#alertproductname').text(productname);
				$('#alerttypename').text(ordertypename);
				$('#alertsizemastername').text(sizemastername);
				$('#alertfromweight').text(fromweight);
				$('#alerttoweight').text(toweight);
				setTimeout(function(){
					$('#productrolalertsdouble').fadeIn();
					$('#toweight').val('');
					$('#toweight').focus();
				},25); */
				$('#fromweight,#toweight,#minimumquantity').val('');
				$('#rangemasterid,#accountid,#sizemasterid').select2('val','');
				alertpopup('Already this combination exist. You cant add the same combination again');
				//$('#primaryid').val(msg['productrolid']);
			}
		}
	});		
}
function puritysetbasedonproduct() {
	var purityid=$.trim($("#productid").find('option:selected').data('purityid'));
	if(purityid == '' || purityid == 'null' || purityid == 1) {
		purity_change_status = 1;  
		$('#purityid').select2('val','').trigger('change');
	}else{
		purityid=purityid.toString();
		purityshowhide('purityid',purityid);
		if (purityid.indexOf(',') > -1) { 
			$('#purityid').select2('val','').trigger('change');
			var dataarray = purityid.split(",");
		} else { 
			if(purityid != 1){
				$('#purityid').select2('val',purityid).trigger('change');
			}
		}
	}
}
function sizehideshow() {
	var productid = $("#productid").find('option:selected').val();
	var size = $("#productid").find('option:selected').data('size');
	if(size == 'Yes') {
		$("#sizemasteriddivhid").show();
		$("#sizemasterid option").addClass("ddhidedisplay");
		$("#sizemasterid").attr("data-validation-engine","validate[required]");
		$("#sizemasterid option[data-productid="+productid+"]").removeClass("ddhidedisplay");
	} else {
		$("#sizemasteriddivhid").hide();
		$("#sizemasterid").attr("data-validation-engine","");
	}
}