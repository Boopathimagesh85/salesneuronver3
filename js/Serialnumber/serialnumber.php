<script>
$(document).ready(function() 
{	
	{//Foundation Initialization
		$(document).foundation();
	}
		{
	var notH = 1,
    $pop = $('#createshortcut').hover(function(){notH^=1;});

$(document).on('mousedown keydown', function( e ){
  if(notH||e.which==27) $pop.stop().fadeOut();
});
	}
	$('#editsave').hide();
	$('#serialmasterviewtoggle').hide();
	clearform('serialnumberform');
	{// Grid Calling Function
		serialnumbergrid();
	}
	{//form field first focus
		firstfieldfocus();
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	
	{//keyboard shortcut reset global variable
	    viewgridview = 0;
		innergridview = 1;
	}
	editstatus = 0;
	$('#groupcloseaddform').hide();
	$( window ).resize(function() {
		sectionpanelheight('serialnumbersectionoverlay');
		maingridresizeheightset('serialnumbergrid');
	});
	{
		$('#startdate').change(function() {
			$("#enddate").val($('#startdate').val());
		});
		$('#enddate').change(function() {
			if($('#startdate').val() < $('#enddate').val()) {
				return true;
			}else{ 
				$("#enddate").val('');
				alertpopup('Start date should not be lower than End date');
				return false;
			}
		});
	}
	{// Tool Bar click function
		//Add
		$('#addicon').click(function() {
			sectionpanelheight('serialnumbersectionoverlay');
			$("#serialnumbersectionoverlay").removeClass("closed");
			$("#serialnumbersectionoverlay").addClass("effectbox");
			//form field first focus
			resetFields();
			editstatus = 0;
			firstfieldfocus();
			$('#addsave').show();
			$('#editsave').hide();
			$('#moduleid,#methodtype,#transactionmodeid,#estimatenumbermodeid').prop('disabled',false);
		});
		$("#editicon").click(function(){			
			var datarowid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
			var elementsname=['7','moduleid','methodtype','numbertype','startingnumber','prefix','suffix','transactionmodeid','estimatenumbermodeid','startdate','enddate','serialnumbermastername'];
			var result = [];
			if(datarowid){
				var status = 'false';
					$.ajax({
						url:base_url+"Serialnumber/retrive?primaryid="+datarowid, 
						dataType:'json',
						async:false,
						success: function(data)
						{
							var txtboxname = elementsname;
							result = data;
							var dropdowns = ['5','moduleid','methodtype','numbertype','transactionmodeid','estimatenumbermodeid'];
							textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
							//status = checkserialnumber(data.moduleid,data.table,data.methodtype,data.transactionmodeid);
							var serialnumbermastername = data.serialnumbermastername.split(",");
							setTimeout(function(){
								$('#startdate').val(data.startdate);
								$('#enddate').val(data.enddate);
								if(serialnumbermastername != ''){
								   $('#serialmastername').select2('val',serialnumbermastername).trigger('change');
								}
							},25);
							var modulename = data.modulename;
							if(status == 'true'){ 
								alertpopupdouble('You have used this serial number format in '+modulename+'. Please remove those records and edit the serial number');
								return false;
							}
						}		
					});
					if(status == 'false'){
					editstatus = 1;
					sectionpanelheight('serialnumbersectionoverlay');
					$("#serialnumbersectionoverlay").removeClass("closed");
					$("#serialnumbersectionoverlay").addClass("effectbox");
					$('#addsave').hide();
					$('#editsave').show();
					//form field first focus
					firstfieldfocus();
					$('#moduleid').select2('val',result.moduleid).trigger('change');
					$('#methodtype').select2('val',result.methodtype).trigger('change');
					$('#estimatenumbermodeid').select2('val',result.estimatenumbermodeid).trigger('change');
					$('#numbertype').select2('val',result.numbertype).trigger('change');
					$('#suffix').val(result.suffix);
					$('#moduleid,#methodtype,#transactionmodeid,#estimatenumbermodeid').prop('disabled',true);
					Materialize.updateTextFields();
				}
			}
			else {
				alertpopup(selectrowalert);
			}	
		});
		//Reload
		$("#reloadicon").click(function(){
			resetFields();
			$('#addicon').show();
			$('#editsave').hide();
			$("#accountid optgroup").removeClass("ddhidedisplay");	
			refreshgrid();
			Materialize.updateTextFields();
		});
		$("#deleteicon").click(function()
		{
			var datarowid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				var status = 'false';
					$.ajax({
						url:base_url+"Serialnumber/retrive?primaryid="+datarowid, 
						dataType:'json',
						async:false,
						success: function(data)
						{
							//status = checkserialnumber(data['moduleid'],data['table'],data['methodtype'],data['transactionmodeid']); 
							var modulename = data['modulename'];
							if(status == 'true'){ 
								alertpopupdouble('You have used this serial number format in '+modulename+'. Please remove those records and delete the serial number');
								return false;
							}
						}		
					});
				if(status == 'false') {
				$("#basedeleteoverlay").fadeIn();
				$(".ffield").focus();
				}
			}
			else {
				alertpopup("Please Select The row");
			}	
		});
		$("#basedeleteyes").click(function() {
			recorddelete();
		});
		$("#basedeleteno").click(function() {
			$("#basedeleteoverlay").fadeOut();
		});
	}
	{//serialnumber validate
		//serialnumber create_function
		//validation for serialnumber Add  
		$('#addsave').click(function() 
		{
			$("#serialnumberform").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#serialnumberform").validationEngine(
		{
			onSuccess: function()
			{ 
				var moduleid = $('#moduleid').find('option:selected').val();
				var methodtype = $('#methodtype').find('option:selected').val();
				var modtable = $('#moduleid').find('option:selected').attr('data-table');
				var modeid = '1';
				var status = checkserialnumber(moduleid,modtable,methodtype,modeid); 
				var modulename = $.trim($('#moduleid').find('option:selected').text());
				if(status == 'true'){ 
					alertpopupdouble('You have used this serail number format in '+modulename+'. Please remove those records and create the serial number');
					return false;
				}
				addform();
			},
			onFailure: function()
			{ 
				var dropdownid = ['3','moduleid','methodtype','numbertype'];				
				dropdownfailureerror(dropdownid);
			}
		});
		//update company information
		$('#editsave').click(function()
		{
		    $("#editvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#editvalidate").validationEngine({
			onSuccess: function()
			{			
				var moduleid = $('#moduleid').find('option:selected').val();
				var methodtype = $('#methodtype').find('option:selected').val();
				var modtable = $('#moduleid').find('option:selected').attr('data-table');
				modeid = '1';
				/* if(moduleid == '52'){
					var types = ['11', '13', '16', '18', '20', '21'];
					if (jQuery.inArray(methodtype, types) !='-1') {
					  modeid = 	$('#transactionmodeid').find('option:selected').val();
					}else{
					  modeid = '1';
					}
				}else{
					modeid = '1';
				} */
				var status = checkserialnumber(moduleid,modtable,methodtype,modeid); 
				var modulename = $.trim($('#moduleid').find('option:selected').text());
				if(status == 'true'){
					alertpopupdouble('You have used this serail number format in '+modulename+'. Please remove those records and create the serial number');
					return false;
				}
				updateform();
			},
			onFailure: function()
			{
				var dropdownid =['2','accounttypeid','accountid'];
				dropdownfailureerror(dropdownid);
			}	
		});
		//for date picker
		var dateformetdata = $('#startdate').attr('data-dateformater');
		$('#startdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#startdate').focus();
			}
		});			 
		var dateformetdata = $('#enddate').attr('data-dateformater');
		$('#enddate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			changeMonth: true,
			changeYear: true,
			maxDate: null,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#enddate').focus();
			}
		});
	}
	$("#moduleid").change(function(){
		var moduleid = $(this).find('option:selected').val();
		$('#methodtype').select2('val','');
		$('#numbertype').select2('val','2').trigger("change");
		$('#serialmastername').val('');
		getmethodtype(moduleid);
		/* var moduleid = $(this).find('option:selected').val();
		if(moduleid == 52) {
			$("#serialmasternamediv").removeClass("hidedisplay");
			$('#serialmastername').addClass('validate[required]');
			serialmasternametoken();
		}else{
			$("#serialmasternamediv").addClass("hidedisplay");
			$('#serialmastername').removeClass('validate[required]');
			$('#serialmastername').val('');
		} */
		
		/* if(moduleid == 44 || moduleid == 103) {
			if(editstatus == 0){
				var status = checkexistrecord(moduleid,1,1);
				if(status == 'true') {
					$('#moduleid').select2('val','');
					alertpopup('Already exist, please edit this one');
				}else{
					if(moduleid == '52'){
						$("#calculationtypeid").removeClass("hidedisplay");
						$('#transactionmodeid').c('validate[required]');
					}else{
						$("#calculationtypeid").addClass("hidedisplay");
						$('#transactionmodeid').removeClass('validate[required]');
					}
				}
			}
			$('#methodtype').select2('val','');
			getmethodtype(moduleid);
		}else{
			if(moduleid == '52'){
				$("#calculationtypeid").removeClass("hidedisplay");
				$('#transactionmodeid').addClass('validate[required]');
			}else{
				$("#calculationtypeid").addClass("hidedisplay");
				$('#transactionmodeid').removeClass('validate[required]');
			}
			$('#methodtype').select2('val','');
			getmethodtype(moduleid);
		} */
	});
	$('#numbertype').change(function(){
		var val=$(this).val();
		$("#serialmasternamediv").addClass("hidedisplay");
		$('#serialmastername').removeClass('validate[required,funcCall[serialnumbermastername]]');
		$('#startdatediv,#enddatediv').addClass("hidedisplay");
		$('#startdate,#enddate').removeClass('validate[required]');
		$('#serialmastername,#startdate,#enddate').val('');
		if(val == 2){
			$('.randomhide').addClass('hidedisplay');
			$("#startingnumber").removeClass('validate[required,custom[integer],min[1]]');
		}else{
			$('.randomhide').removeClass('hidedisplay');
			$("#startingnumber").addClass('validate[required,custom[integer],min[1]]');
			var moduleid = $("#moduleid").find('option:selected').val();
			if(moduleid == 52) {
				$("#serialmasternamediv").removeClass("hidedisplay");
				$('#serialmastername').addClass('validate[required,funcCall[serialnumbermastername]]');
				$("#startdatediv,#enddatediv").removeClass("hidedisplay");
				$('#startdate,#enddate').addClass('validate[required]');
				serialmasternametoken();
			}
		}
		Materialize.updateTextFields();
	});
	$('#addserialicon').click(function(){
		clearform('serialnumberform');
		sectionpanelheight('serialnumbersectionoverlay');
		$("#serialnumbersectionoverlay").removeClass("closed");
		$("#serialnumbersectionoverlay").addClass("effectbox")
		$('#addicon').show();
		$('#editsave').hide();
	});
	$('.addsectionclose').click(function(){
		$("#serialnumbersectionoverlay").addClass("closed");
		$("#serialnumbersectionoverlay").removeClass("effectbox");
	});
	/* $("#methodtype").change(function(){
		var methodtype = $(this).find('option:selected').val();
		var moduleid = $('#moduleid').find('option:selected').val();
		var modtable = $('#moduleid').find('option:selected').attr('data-table');
		var modulename = $.trim($('#moduleid').find('option:selected').text());
		var modeid = '1';
		if(moduleid == '52'){
			var types = ['11', '13', '16', '18','9','22','23'];
			if (jQuery.inArray(methodtype, types) !='-1') {
			  modeid = 	$('#transactionmodeid').find('option:selected').val();
			}else{
			  modeid = '1';
			}
		}else{
			modeid = '1';
		}
		if(moduleid == '50'){
			if(editstatus == 0){
				var status = checkexistrecord(moduleid,methodtype,1);
				$("#calculationtypeid").addClass("hidedisplay");
				$('#transactionmodeid').removeClass('validate[required]'); 
				if(status == 'true') {
					$('#moduleid,#methodtype').select2('val','');
					alertpopup('Already exist, please edit this one');
				}else{
					var status = checkserialnumber(moduleid,modtable,methodtype,modeid); 
					if(status == 'true'){ 
						alertpopupdouble('You have used this serail number format in '+modulename+'. Please remove those records and create the serial number');
						$("#methodtype").select2('val','');
						return false;
					}
				}
			}
		}else {
			var types = ['11','16', '18','9','22','23'];
				if (jQuery.inArray(methodtype, types) !='-1') {
					 $("#calculationtypeid").removeClass("hidedisplay");
					$('#transactionmodeid').addClass('validate[required]'); 
				}else{
					if(editstatus == 0 && moduleid == 52){
						var status = checkexistrecord(moduleid,methodtype,1);
						if(status == 'true') {
							$('#moduleid,#methodtype').select2('val','');
							alertpopup('Already exist, please edit this one');
							return false;
						}
					}
					var status = checkserialnumber(moduleid,modtable,methodtype,modeid); 
					if(status == 'true'){ 
						alertpopupdouble('You have used this serail number format in '+modulename+'. Please remove those records and create the serial number');
						$("#methodtype").select2('val','');
						return false;
					}
					$("#calculationtypeid").addClass("hidedisplay");
					$('#transactionmodeid').removeClass('validate[required]');
				}
				
		}	
			
	}); */
	// calculation type change
	/* $("#transactionmodeid").change(function(){
		var methodtype = $('#methodtype').val();
		var moduleid = $('#moduleid').find('option:selected').val();
		var modtable = $('#moduleid').find('option:selected').attr('data-table');
		var modeid = '1';
		if(moduleid == '52'){
			var types = ['11', '16', '18','9'];
			if (jQuery.inArray(methodtype, types) !='-1') {
			  modeid = 	$('#transactionmodeid').find('option:selected').val();
			}else{
			  modeid = '1';
			}
		}else{
			modeid = '1';
		}
		if(moduleid == '52'){
			if(editstatus == 0){
				var status = checkexistrecord(moduleid,methodtype,modeid);
				if(status == 'true') {
					$('#moduleid,#methodtype,#transactionmodeid').select2('val','');
					alertpopup('Already exist, please edit this one');
				}else{
					var status = checkserialnumber(moduleid,modtable,methodtype,modeid); 
					var modulename = $.trim($('#moduleid').find('option:selected').text());
					if(status == 'true'){ 
						alertpopupdouble('You have used this serail number format in '+modulename+'. Please remove those records and create the serial number');
						$("#methodtype,#transactionmodeid").select2('val','');
						return false;
					}
				}
			}
		}
	}); */
	//filter
	$("#serialnumberfilterddcondvaluedivhid").hide();
	$("#serialnumberfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#serialnumberfiltercondvalue").focusout(function(){
			var value = $("#serialnumberfiltercondvalue").val();
			$("#serialnumberfinalfiltercondvalue").val(value);
			$("#serialnumberfilterfinalviewconid").val(value);
		});
		$("#serialnumberfilterddcondvalue").change(function(){
			var value = $("#serialnumberfilterddcondvalue").val();
			serialnumbermainfiltervalue=[];
			$('#serialnumberfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    serialnumbermainfiltervalue.push($mvalue);
				$("#serialnumberfinalfiltercondvalue").val(serialnumbermainfiltervalue);
			});
			$("#serialnumberfilterfinalviewconid").val(value);
		});
	}
	serialnumberfiltername = [];
	$("#serialnumberfilteraddcondsubbtn").click(function() {
		$("#serialnumberfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#serialnumberfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(serialnumbergrid,'serialnumber');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
{// serialnumber View Grid
	function serialnumbergrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $(window).width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $("#serialnumbersortcolumn").val();
		var sortord = $("#serialnumbersortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.serialnumbermasterheadercolsort').hasClass('datasort') ? $('.serialnumbermasterheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.serialnumbermasterheadercolsort').hasClass('datasort') ? $('.serialnumbermasterheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.serialnumbermasterheadercolsort').hasClass('datasort') ? $('.serialnumbermasterheadercolsort.datasort').attr('id') : '0';
		var filterid = $("#serialnumberfilterid").val();
		var conditionname = $("#serialnumberconditionname").val();
		var filtervalue = $("#serialnumberfiltervalue").val();
		var userviewid =120;
		var viewfieldids ='45';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=varserialnumbermaster&primaryid=varserialnumbermasterid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#serialnumbergrid').empty();
				$('#serialnumbergrid').append(data.content);
				$('#serialnumbergridfooter').empty();
				$('#serialnumbergridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('serialnumbergrid');
				{//sorting
					$('.serialnumbermasterheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.serialnumbermasterheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.serialnumbermasterheadercolsort').hasClass('datasort') ? $('.serialnumbermasterheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.serialnumbermasterheadercolsort').hasClass('datasort') ? $('.serialnumbermasterheadercolsort.datasort').data('sortorder') : '';
						$("#serialnumbersortorder").val(sortord);
						$("#serialnumbersortcolumn").val(sortcol);
						var sortpos = $('#serialnumbergrid .gridcontent').scrollLeft();
						serialnumbergrid(page,rowcount);
						$('#serialnumbergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('serialnumbermasterheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						serialnumbergrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#serialnumbermasterpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						serialnumbergrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#serialnumbermasterpgrowcount').material_select();
			},
		});
	}
 }
	{//refresh grid
		function refreshgrid() {
			var page = $('ul#serialnumbermasterpgnum li.active').data('pagenum');
			var rowcount = $('ul#serialnumbermasterpgnumcnt li .page-text .active').data('rowcount');
			serialnumbergrid(page,rowcount);
		}
	}
	
{//CRUD Related functions
//Create
function addform()
{
	var numbertype=$("#numbertype").find('option:selected').val();
	var formdata = $("#serialnumberform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;	
	var fieldname = $("#moduleid").find('option:selected').data('field');		
	var serialfield = $("#moduleid").find('option:selected').data('serialfield');		
	var table = $("#moduleid").find('option:selected').data('table');
	var typename = $("#methodtype").find('option:selected').data('typename');
	var serialmastername = $('#serialmastername').val();
	$.ajax(
    {
        url: base_url +"Serialnumber/create",
        data: "datas=" +datainformation+"&fieldname="+fieldname+"&serialfield="+serialfield+"&table="+table+"&typename="+typename+"&serialmastername="+serialmastername,
		type: "POST",
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            clearform('serialnumberform');
            refreshgrid();
			$('#startingnumber').val('');
			$(".addsectionclose").trigger("click");
			if (nmsg == 'SUCCESS'){
				alertpopup(savealert);
            }
			else if (nmsg == 'FAILURE'){
				alertpopup('Please enter starting number as equal to '+digit+' ');
            }
			else {
				alertpopup(submiterror);
			}          
        },
    });
}
function updateform()
{
	var numbertype=$("#numbertype").find('option:selected').val();
	var formdata = $("#serialnumberform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
	var fieldname = $("#moduleid").find('option:selected').data('field');
    var serialfield = $("#moduleid").find('option:selected').data('serialfield');
    var table = $("#moduleid").find('option:selected').data('table');
	var typename = $("#methodtype").find('option:selected').data('typename');
	var moduleid = $.trim($('#moduleid').find('option:selected').val());
	var transactionmodeid = $.trim($('#transactionmodeid').find('option:selected').val());
	var methodtype = $.trim($('#methodtype').find('option:selected').val());
	var estimatenumbermodeid = $.trim($('#estimatenumbermodeid').find('option:selected').val());
	var serialmastername = $.trim($('#serialmastername').val());
	$.ajax({
        url: base_url + "Serialnumber/update",
        data: "datas=" + datainformation +"&primaryid="+ datarowid +"&fieldname="+fieldname+"&serialfield="+serialfield+"&table="+table+"&typename="+typename+"&moduleid="+moduleid+amp+"transactionmodeid="+transactionmodeid+amp+"methodtype="+methodtype+amp+"estimatenumbermodeid="+estimatenumbermodeid+amp+"serialmastername="+serialmastername,
		type: "POST",
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            resetFields();
            refreshgrid();
            $(".addsectionclose").trigger("click");
			$('#methodtype').empty();
			$('#addicon').show();
			$('#editsave').hide();
			if (nmsg == 'SUCCESS'){
				alertpopup(updatealert);
				Materialize.updateTextFields();
            } else {
				alertpopup(submiterror);
				Materialize.updateTextFields();
			}  
        },
    });
}
//delete serialnumber
function recorddelete()
{	
	var datarowid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
	$.ajax({
        url: base_url + "Serialnumber/delete?primaryid="+datarowid,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			refreshgrid();
			$("#basedeleteoverlay").fadeOut();
		    if (nmsg == 'SUCCESS') {				
				alertpopup(deletealert);
            } else {
				alertpopup(submiterror);
            }           
        },
    });
}
function retrive()
{
	var elementsname=['6','moduleid','methodtype','numbertype','startingnumber','prefix','suffix','transactionmodeid','startdate','enddate'];
	var datarowid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
	$.ajax(
	{
		url:base_url+"Serialnumber/retrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			var txtboxname = elementsname;
			var dropdowns = ['4','moduleid','methodtype','numbertype','transactionmodeid'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			$('#moduleid').trigger('change');
			$('#methodtype').select2('val',data.methodtype).trigger('change');
			$('#numbertype').select2('val',data.numbertype).trigger('change');
			$('#suffix').val(data.suffix);
			Materialize.updateTextFields();
		}		
	});
}
}
// get method for specific module
function getmethodtype(moduleid) {
	$('#methodtype').empty();
	$('#methodtype').append($("<option></option>"));
	if(moduleid != 52) {
		$('#methodtype').append($("<option value='1' data-typename='ALL'>ALL</option>"));
	}
	 $.ajax({
        url: base_url + "Serialnumber/getmethodtype?primaryid="+moduleid,
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) 
        { 
			if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#methodtype')
						.append($("<option></option>")
						.attr("value",data[index]['tagtypeid'])
						.attr("data-typename",data[index]['tagtypename'])
						.text(data[index]['tagtypename']));
					});
				}
				if(moduleid == 44){
					$('#methodtype').select2('val','1').trigger('change');
				}				
			Materialize.updateTextFields();
        },
    });
}
function checkstartnumber(moduleid,startno){
	$.ajax({
        url: base_url + "Serialnumber/checkstartnumber",
        data: "moduleid=" + moduleid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) 
        { 
			if(data.intialnumber == 0){
				if((parseInt)(startno) > (parseInt)(data.currentnumber)){
					return true;
				}else{
					alertpopupdouble('Please enter the value greater than '+data.currentnumber+' ');
					$('#startingnumber').val('');
					return false;
				}
			}else{
				  return true;
			}
        },
    });
}
function checkstartnumbercrud(moduleid,startno){
	var status ='false';
	$.ajax({
        url: base_url + "Serialnumber/checkstartnumber",
        data: "moduleid=" + moduleid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) 
        { 
			if(data.intialnumber == 0){
				if((parseInt)(startno) > (parseInt)(data.currentnumber)){
					 status ='true';
				}else{
					alertpopupdouble('Please enter the value greater than '+data.currentnumber+' ');
					$('#startingnumber').val('');
					status ='false';
				}
			}else{
				status ='true';
			}
        },
    });
	return status;
}
function checkserialnumber(moduleid,modtable,methodtype,modeid){  // check serial number for lot,stock entry,transaction
	if(moduleid != '52') {
		var status ='false';
		$.ajax({
			url: base_url + "Serialnumber/checkserialnumber",
			data: "moduleid=" + moduleid+"&modtable="+modtable+"&methodtype="+methodtype+"&modeid="+modeid,
			type: "POST",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) 
			{
				if(data == "true")
				{ 
					status ='true';
				}
			}
		});
		return status;
	}
}
function checkexistrecord(moduleid,methodtype,modeid){  // check serial number for lot,stock entry,transaction
	var status ='false';
	/* $.ajax({
        url: base_url + "Serialnumber/checkexistrecord",
        data: "moduleid=" + moduleid+"&methodtype="+methodtype+"&modeid="+modeid,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) 
        {
			if(data == "true")
			{ 
				status ='true';
			}
        }
    }); */
	return status;
}
function  serialmasternametoken()
{
	 var moduleid = $('#moduleid').find('option:selected').val();
	 serialmastername = [];
	 $.ajax({
        url: base_url + "Serialnumber/serialmasternameload",
       	data: "moduleid="+moduleid,
		async:false,
		cache:false,
		dataType:'json',
		success: function(data) 
        { 
			
			var i=0;
			$.each(data, function(index) {
			serialmastername[i]=data[index];
			i++;
			});
        },
    });
	 //prize description automatic tokenzation     
	$("#serialmastername").select2({
		tags: serialmastername,
		tokenSeparators: [',', ' '],
		maximumSelectionSize: 1,
		tokenizer: function(input, selection, callback) {

			// no comma no need to tokenize
			if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
				return;

			var parts = input.split(/,| /);
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				part = part.trim();
				callback({id:part,text:part});
			}
		}
	});
}
function serialnumbermastername() {
	var primaryid = $('#serialnumbergrid div.gridcontent div.active').attr('id');
	var methodtype = $("#methodtype").val();
	var accname = $("#serialmastername").val();
	var fieldname = 'serialnumbermastername';
	var elementpartable = 'varserialnumbermaster';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Serialnumber/serialnumbermasternamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname+"&methodtype="+methodtype,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Unique name already exists!";
				}
			}else {
				return "Unique name already exists!";
			}
		}
	}
}
</script>