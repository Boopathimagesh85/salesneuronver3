<script>
$(document).ready(function()
{
	//Current Module Selection For Header
	currentmoduleselect('utilitieshtrigger','mid39');

	resp = [];
	$(document).ajaxStop(function() //check result after completion of sync process slave to master
	{
		var myRadio = $('input[name=synctoolbar]');
		var checkedValue = myRadio.filter(':checked').val();
		if(checkedValue == 'SYNC')
		{
			if(jQuery.inArray("fail", resp) == -1)
			{
				syncfail();
			}
			else
			{ 
				syncsuccess();
			}
			$('#syncconformationalert').fadeOut();
			$('#sync').removeAttr('checked');
		}
	});
	// Maindiv height width change
	maindivwidth();
	
	//grid calling 
	syncjqgrid();
	//grid settings calling
	gridsettings();

	//toolbar creation
	var synctoolbarcreate = ["synctoolbar","5","t_syncgrid","sync","SYNC","10","editradiobtn","syncdelete","HARDDELETE","10","deleteradiobtn"];
	toolbarset(synctoolbarcreate); 
	
	//date setting for delete
	datesettingaccounts("harddeletestartdate","harddeleteenddate","harddeletestartdatealt","harddeleteenddatealt");
	
	{// password screen show hide - DELETE
		$('#hdconfirmyes').click(function()
		{	
			$('#deleteconfirmationspan').hide();
			$('#passwordenterspan').fadeIn();
		});
		$('#hdconfirmno').click(function()
		{
			$('#conformationalert').fadeOut();
		});
		
	}
	{// password screen show hide - SYNC
		$('#syncconfirmyes').click(function()
		{	
			$('#syncconfirmationspan').hide();
			$('#syncpasswordenterspan').fadeIn();
		});
		$('#syncconfirmno').click(function()
		{	
			$('#syncconformationalert').fadeOut();
		});
	}
	{// date checkbox
		$('#dateenabledisable').change(function(){
			if ($('#dateenabledisable').is(':checked')) {
				$("#harddeletestartdate").attr('disabled',false);
				$("#harddeleteenddate").attr('disabled',false);
			}
			else{
				$("#harddeletestartdate").attr('disabled',true).val('');
				$("#harddeleteenddate").attr('disabled',true).val('');
			}
		});
	}
	
	$("input[name=synctoolbar]:radio").change(function()
	{ 	
		var myRadio = $('input[name=synctoolbar]');
		var checkedValue = myRadio.filter(':checked').val();
		if(checkedValue == 'SYNC')
		{
			$('#syncconformationalert').fadeIn();
			$('#syncpassvfy').val('');
		}
		if(checkedValue == 'HARDDELETE')
		{
			$('#hardelpassvfy').val('');
			$('#conformationalert').fadeIn();
		}
	});
	$('#syncdatasub').click(function(){
		$("#syncwizard").validationEngine('validate');
	});
	
	jQuery("#syncwizard").validationEngine(
	{
		onSuccess: function()
		{
			//Sync authentication check
			var data = $("#syncform").serialize();
			var amp = '&';
			var syncformdata = amp + data;
			$.ajax(
			{
				url:base_url+"index.php/admin/sync/datasyncverify?sync_option=Enable",
				data: "data=" + syncformdata,
				success: function(msg)
				{
					if(msg == 'True')
					{
						$('#syncconformationalert').fadeOut();
						$('#syncprodessgoingonalert').fadeIn();
						dbsyncstart();
					}
					else
					{
						$('#syncconformationalert').fadeOut();
						alertpopup('Authentication Failed!');
					}
				},
			});
		}		
	});
	$('#harddelbtn').click(function(){
		$("#hardelwizard").validationEngine('validate');
	});
	jQuery("#hardelwizard").validationEngine(
	{
		onSuccess: function()
		{
			//Sync authentication check
			var data = $("#hardform").serialize();
			var amp = '&';
			var hardformdata = amp + data;
			$.ajax(
			{
				url:base_url+"index.php/admin/sync/datahardverify?hard_option=Enable",
				data: "data=" + hardformdata,
				success: function(msg)
				{
					if(msg == 'True')
					{ alert('ds');
						
						submitForm();
					}
					else
					{
						$('#conformationalert').fadeOut();
						alertpopup('Authentication Failed!');
					}
				},
			});
		}		
	});
	{// close overlay 
		$("#closesynalertov").click(function(){
			$("#syncconformationalert").fadeOut();
		});
		$("#closeharddalertov").click(function(){
			$("#conformationalert").fadeOut();
		});
	}
});
//dbbackup
function submitForm()
{
	$('#harddeleteprocessgoingonalert').fadeIn();
	$('#conformationalert').fadeOut();
	$.ajax({type:'POST',async:false, url:base_url+'dbbackup/backup.php', data:$('#databasebackupform').serialize(), success: function(response) {
		if(response!='DB_ERROR')
		{
			harddelstart();
		}
		else
		{
			alertpopup(response);
			$('#harddeleteprocessgoingonalert').fadeOut();
		}
	}});
	
	$.ajax({type:'POST',async:false, url:base_url+'dbbackup/del.php',
		success: function(){
			$('#harddeleteprocessgoingonalert').fadeOut();
		}
	});
}
//harddel start
{
	//slave data hard delete
	function harddelstart()
	{
		var user = $('#updateuser').val();
		var status = $('#updatestatus').val();
		var data = $('#dateenabledisable').attr('checked') ? "True" : "False";
		var date = $('#harddeletestartdate').val();
		$.ajax(
		{
			url:base_url+"sync/resetdata.php?sdel_opt=True&slection_mod=pur_mod&date_option="+data+"&date="+date+"&user="+user+"&status="+status,
			success: function(msg)
			{
				if(msg == 'Success')
				{
					$('#harddeleteprocessgoingonalert').fadeOut();
					$('#syncgrid').trigger('reloadGrid');
					alertpopup(msg);
				}
				else
				{
					$('#harddeleteprocessgoingonalert').fadeOut();
					alertpopup(msg);
				}
				$('#conformationalert').fadeOut();
				$('#syncdelete').removeAttr('checked');
			},
		});
	}
}
{
	//slave db backup
	function dbsyncstart()
	{
		$.ajax(
		{
			url:base_url+"sync/slavetomaster.php?back_option=True&slection=source",
			success: function(msg)
			{
				if(msg == '0')
				{
					masterbackup();
				}
				else
				{
					syncsuccess();
				}
			},
		});
	}
	//master db backup
	function masterbackup()
	{
		$.ajax(
		{
			url:base_url+"sync/slavetomaster.php?back_option=True&slection=destination",
			success: function(msg)
			{
				if(msg == '0')
				{
					syncdata();
				}
				else
				{
					syncsuccess();
				}
			},
		});
	}
	//sync fail
	function syncfail()
	{
		$.ajax(
		{
			url:base_url+"sync/slavetomaster.php?back_option=Revert",
			success: function(msg)
			{
				if(msg == 'success')
				{
					$('#syncprodessgoingonalert').fadeOut();
					alertpopup('Synchronize Is Fail');
				}
			},
		});
	}
	//sync success
	function syncsuccess()
	{

		$.ajax(
		{
			url:base_url+"sync/slavetomaster.php?back_option=Fix",
			success: function(msg)
			{
				if(msg == 'success')
				{
					syncupdate();
				}
				else
				{
					alert(msg);
				}
			},
		});
	}
	function syncupdate()
	{
		var user = $('#updateuser').val();
		var status = $('#updatestatus').val();
		$.ajax(
		{
			url:base_url+"sync/slavetomaster.php?back_option=Update&user="+user+"&status="+status,
			success: function(msg)
			{
				if(msg == 'success')
				{
					$('#syncprodessgoingonalert').fadeOut();
					alertpopup('Synchronize Is Done');
					$('#syncgrid').trigger('reloadGrid');
				}
				else
				{
					alert(msg);
				}
			},
		});
	}
	//data sync
	function syncdata()
	{
		transtab = new Array();
		transtab = ['accountgroupamount','accountlog','accountnameamount','approvalinlot','approvaloutlot','approvaloutlotitem','approvalreturn','approvalsalesstock','assayerissue','assayerreceipt','banklog','bhavcut','billpr','bullion','customerorder','customerorderdetail','customertransaction','custorderaddamount','custorderoldpurchase','custordersalesreturn','estimate','estimateitem','estordercomments','indirectpurchase','inventoryadditionalcharge','itemuntag','miscellaneous','outofstock','paymentadjdetail','paymentcashdetail','paymentcheckdetail','paymentcreditcarddetail','paymentlog','paymentnettransferdetail','physicalstock','pidlog','purchaseaccount','purchaseaddamount','purchaselot','purchaselotitem','purchaselottouch','receiptadjdetail','receiptcashdetail','receiptchequedetail','receiptcreditcarddetail','receiptlog','receiptnettransferdetail','retailorder','salesaccount','salesaccountaddamount','salesaccountdetail','salesaccountlog','salesaccountoldpurchase','salesaccountsalesreturn','salesreceiptlog','stocklog','stocksales','tagstocktaking','testissue','testreceipt','transactionlog','transferlog','vendororder','vendortransaction','currentstock','currentaccountstock'];
		for(var i=0; i<(transtab.length);i++)
		{
			$.ajax(
			{
				url:base_url+"sync/slavetomaster.php?sync_option=True&sync_op=trans_tab&sync_tnstable="+transtab[i],
				success: function(msg)
				{
					if(msg.length > 2)
					{
						resp.push("fail");
					}
					else
					{
						resp.push("success");
					}
				},
			});
		}
		
		mastab = new Array();
		mastab = ['accountdate','accountgroup','accountname','accountnameaddress','accountnamecontact','accountnametype','additionalchargetype','addlchargetypecat','backup','backupsetting','bank','bankaccount','boxdisplaystatus','branch','branchcontact','category','city','company','counter','customer','customeraddress','customercontact','designation','employee','employeeaddress','employeecontact','generalstatus','inventoryaddchargetype','itembox','itemcounter','journal','journaldetail','location','materialtype','metal','metalaccount','module','moduleinfo','primarytypename','product','productdiscount','productrol','productvac','purity','rate','ratedate','reorderlevel','reportgroup','restore','subcategory','subcounter','subsubcategory','tagstatus','tagtype','tasktype','toolbarname','transactionoption','transactiontype','usercredential','userrole','vaccalculation','valueaddedcharge','vendor','vendoraddress','vendorcontact','vendorproducttouch','vendortype','weightrange'];
		for(var j=0; j<(mastab.length);j++)
		{
			$.ajax(
			{
				url:base_url+"sync/slavetomaster.php?sync_option=True&sync_op=master_tab&sync_mstable="+mastab[j],
				success: function(msg)
				{
					if(msg.length > 2)
					{
						resp.push("fail");
					}
					else
					{
						resp.push("success");
					}
				},
			});
		}
		tagtab = new Array();
		tagtab = ['itemtag','tagvac'];
		for(var t=0; t<(tagtab.length);t++)
		{
			$.ajax(
			{
				url:base_url+"sync/slavetomaster.php?sync_option=True&sync_op=tag_tab&sync_tgtable="+tagtab[t],
				success: function(msg)
				{
					if(msg.length > 2)
					{
						resp.push("fail");
					}
					else
					{
						resp.push("success");
					}
				},
			});
		}
		dcvtab = new Array();
		dcvtab = ['dailycustomerstock','dailyvendorstock'];
		for(var d=0; d<(dcvtab.length);d++)
		{
			$.ajax(
			{
				url:base_url+"sync/slavetomaster.php?sync_option=True&sync_op=dvc_stock&sync_dvctable="+dcvtab[d],
				success: function(msg)
				{
					if(msg.length > 2)
					{
						resp.push("fail");
					}
					else
					{
						resp.push("success");
					}
				},
			});
		}
		dystktab = new Array();
		dystktab = ['dailystock'];
		for(var ds=0; ds<(dystktab.length);ds++)
		{
			$.ajax(
			{
				url:base_url+"sync/slavetomaster.php?sync_option=True&sync_op=dystock_tab&sync_dytable="+dystktab[ds],
				success: function(msg)
				{
					if(msg.length > 2)
					{
						resp.push("fail");
					}
					else
					{
						resp.push("success");
					}
				},
			});
		}
	}
}
function syncjqgrid()
{ 
	$("#syncgrid").jqGrid(
		{
			url:base_url+"index.php/admin/sync/syncread",
			datatype: "xml",
			colNames:[idinfo[0],'Sync Name',date[0],bcktime[0],'EmployeeName'],
			colModel:[
						{name:'backupid',index:'backup.BackupId', width:idinfo[1],hidden:true},
						{name:'name',index:'backup.Name', width:backupnm[1]},
						{name:'date',index:'backup.Date', width:bcktime[1]},
						{name:'time',index:'backup.Time', width:date[1]},
						{name:'EmployeeName',index:'employee.EmployeeName', width:type[1]}
					 ],
			rowNum:rnum,
			rowList:rlist,
			rownumbers: true,
			pager: '#syncgriddiv',
			sortname: 'BackupId',
			viewrecords: false,
			sortorder: "desc",
			multiselect: false,
			//caption: "Backup Details List",
			height:300,
			width:1050,
			toolbar: [true,"top"],
		
		});
}

function  gridsettings(){
	maingridclass('syncgrid');
	var syncistb =["1","syncgrid"];
	createistb(syncistb);
}
function maingridclass(gridid) {
	$("#"+gridid+"").jqGrid('bindKeys');
	$("#gview_"+gridid+" .label"+gridid+"").addClass("mgcrowheaderht5");
	$("#gview_"+gridid+" .content"+gridid+"").addClass("mgccontentareaht80");
	$("#gbox_"+gridid+" .pager"+gridid+"").addClass("mgcpaginationht7");
}
	
</script>