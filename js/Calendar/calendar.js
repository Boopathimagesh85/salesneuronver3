jQuery(document).ready(function()
{
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Date Range
		daterangefunction('taskstartdate','taskenddate');
		daterangefunction('activitystartdate','activityenddate');
	}	
	{// calendar view  height
		var windowheight = $(window).height();
		var calendarheight = windowheight - 52; // reducing header height
		var calcontentheight = windowheight - 112;
		$(".calendarviewcontainer").css("height", "" + calendarheight + "px");
		
	}
	ACTIVITYID = 0;
	$( window ).resize(function() {
		$(".calendarviewcontainer").css("height", "" + calendarheight + "px");
	});
	$('#authorize-button').click(function(){
		handleAuthClick();
	});
	$('#signout-button').click(function(){
		handleSignoutClick();
	});
	{// Content Load 
		$(window).on("load", function() {
		    $(".calendarviewcontainer").show();
			$('#calendar').fullCalendar({
				googleCalendarApiKey: 'AIzaSyBuu86le5tdjxl4CX9325SjExNk5W8ks1E',
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,agendaFourDay'
				},
				views: {
			        agendaFourDay: {
			            type: 'agenda',
			            duration: { days: 4 },
			            buttonText: '4 day'
			        },
			        month: {
			        	eventLimit:3
			        }
			    },
			    contentHeight:calcontentheight,
			    selectable: true,
				selectHelper: true,
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				eventClick: function(event, element) {
					$('.fc-content.weekdayoverlay').each(function(){
						$(this).click(function(e){
							e.stopPropagation();
							e.preventDefault();
							var datarowid = $(".calendaractive").attr('data-id');
							var datatype = $(".calendaractive").attr('data-type');
							var title = $(".calendaractive").attr('data-title');
							$("#eventdetailcontent").empty();
							if(datatype == '1') {
								datatype = 'Task';
							} else {
								datatype = 'Activity';
							}
							var eventdetalcontent = "<div class='large-12 columns' style='position:absolute;'><div class='overlay alertsoverlay overlayalerts' id='eventdetail'><div class='row sectiontoppadding'></div><div class='alert-panel'><div class='alertmessagearea'><div class='alert-title'>"+datatype+"</div><div class='alert-message alertinputstyle'>"+title+"</div></div><div class='alertbuttonarea'><span class='firsttab' tabindex='1000'></span><input type='button' id='alertedityes' name='' tabindex='1001' value='Edit' class='alertbtn  ffield' ><input type='button' id='alertdelyes' name='' tabindex='1002' value='Delete' class='alertbtn ' ><input type='button' id='alertcloseyes' name='' value='Cancel' tabindex='1003' class='flloop alertbtn alertsoverlaybtn' ><span class='lasttab' tabindex='1004'></span></div></div></div></div>"
							$("#eventdetailcontent").append(eventdetalcontent);
							$("#eventdetail").show();
							$("#alertedityes").click(function(){
								$("#eventdetail").hide();
								$("#creationoverlay").fadeIn();
								$("#forsubmitdata,#activityforsubmitdata").addClass('hidedisplay');
								$("#forupdatedata,#activityforupdatedata").removeClass('hidedisplay');
								var datarowid = $(".calendaractive").attr('data-id');
								var datatype = $(".calendaractive").attr('data-type');
								if(datatype == "1") {
									$(".active span").attr('style','');
									$(".active").removeClass('active');
									$("#viewtaskspan").addClass('active');
									$("#viewtaskspan").removeClass('hidedisplay');
									$("#viewactivityspan").addClass('hidedisplay');
									$("#taskspan").show();
									$("#activityspan").hide();
									fetchtaskformdata(datarowid);
								}
								else if(datatype == "2") {
									$(".active span").attr('style','');
									$(".active").removeClass('active');
									$("#viewactivityspan").addClass('active');
									$("#viewactivityspan").removeClass('hidedisplay');
									$("#viewtaskspan").addClass('hidedisplay');
									$("#taskspan").hide();
									$("#activityspan").show();
									fetchactivityformdata(datarowid);
								}
								Materialize.updateTextFields();
								$("#updatecreatetask").click(function() {
									$("#editvalidatetaskoverlay").validationEngine('validate');
								});
								$("#updatecreateactivity").click(function() {
									$("#activitytaskeditvalidation").validationEngine('validate');
								});
							});
							$("#alertdelyes").click(function(){
								var currentid = $(".calendaractive").attr('data-id');
								if(currentid) {	
									var currentid = $(".calendaractive").attr('data-id');
									var datatype = $(".calendaractive").attr('data-type');
									if(datatype == "1") {
										$("#eventdetail").hide();
										combainedmoduledeletealert('taskdeleteyes');
										$("#taskdeleteyes").click(function(){
											var datarowid = $("#brandprimarydataid").val();
											deletetaskdata(currentid,'event');
											$('#calendar').fullCalendar( 'removeEvents', currentid );
											$(this).unbind();
										});
									} else if(datatype == "2") {
										$("#eventdetail").hide();
										combainedmoduledeletealert('activitydeleteyes');
										$("#activitydeleteyes").click(function(){
											var datarowid =$("#brandprimarydataid").val();
											deleteactivitydata(currentid,'event');
											$('#calendar').fullCalendar( 'removeEvents', currentid );
											$(this).unbind();
										});
									}
								} else {
									alertpopup("Please select a row");
								}
							});
							$("#alertcloseyes").click(function(){
								$("#eventdetail").hide();
							});
						});
					});
				}, 
				eventMouseover: function(event, element) {
					$(".fc-content").click(function(){
						$(".fc-content").removeClass('calendaractive');
						$(this).removeClass('calendaractive');
						$(this).addClass('calendaractive');
					});
					{// For Update Event on Click
						$(this).find('.editevent').click(function(){
							$("#creationoverlay").fadeIn();
							$("#forsubmitdata,#activityforsubmitdata").addClass('hidedisplay');
							$("#forupdatedata,#activityforupdatedata").removeClass('hidedisplay');
							var datarowid = event.id;
							var datatype = event.type;
							if(datatype == "1") {
								$(".active span").attr('style','');
								$(".active").removeClass('active');
								$("#viewtaskspan").addClass('active');
								$("#viewtaskspan").removeClass('hidedisplay');
								$("#viewactivityspan").addClass('hidedisplay');
								$("#taskspan").show();
								$("#activityspan").hide();
								fetchtaskformdata(datarowid);
							}
							else if(datatype == "2") {
								$(".active span").attr('style','');
								$(".active").removeClass('active');
								$("#viewactivityspan").addClass('active');
								$("#viewactivityspan").removeClass('hidedisplay');
								$("#viewtaskspan").addClass('hidedisplay');
								$("#taskspan").hide();
								$("#activityspan").show();
								fetchactivityformdata(datarowid);
							}
							Materialize.updateTextFields();
							$("#updatecreatetask").click(function() {
								$("#editvalidatetaskoverlay").validationEngine('validate');
							});
							$("#updatecreateactivity").click(function() {
								$("#activitytaskeditvalidation").validationEngine('validate');
							});
						});	
					}
					
					{// For Delete
						$(this).find('.forhidedel').removeClass('hidedisplay');
						$(this).find('.delevent').click(function() {
							var currentid = event.id;
							if(currentid) {	
								var currentid = event.id;
								var datatype = event.type;
								if(datatype == "1") {
									combainedmoduledeletealert('taskdeleteyes');
									$("#taskdeleteyes").click(function(){
										var datarowid =$("#brandprimarydataid").val();
										deletetaskdata(currentid,'event');
										$('#calendar').fullCalendar( 'removeEvents', currentid );
										$(this).unbind();
									});
								} else if(datatype == "2") {
									combainedmoduledeletealert('activitydeleteyes');
									$("#activitydeleteyes").click(function(){
										var datarowid =$("#brandprimarydataid").val();
										deleteactivitydata(currentid,'event');
										$('#calendar').fullCalendar( 'removeEvents', currentid );
										$(this).unbind();
									});
								}
							} else {
								alertpopup("Please select a row");
							}
						});
					}
				},
				eventResize: function(event, element, revertFunc) {
					var datatype = event.type;
					var sdate = event.start.format();
					nsdate = sdate.split('T');
					var edate = event.end.format();
					nedate = edate.split('T');
					if(nsdate.length > 1) {
						var ntime = nsdate[1].split(':');
						var ntimeval = ntime[0]+':'+ntime[1];
					}
					if(nedate.length > 1) {
						var etime = nedate[1].split(':');
						var etimeval = etime[0]+':'+etime[1];
					}
					if (datatype == 1) {
						alertpopup('You cannot resize the task time.');
						revertFunc();
						
					} else {
						var activitystarttime = ntimeval;
						var activityendtime = etimeval;
						var datarowid = event.id;
						$("#calconfirmoverlay").fadeIn();
						$("#calconfirmyes").focus();
						$("#calconfirmyes").click(function() {
							$.ajax({
								url: base_url + "Calendar/activityresizeupdate?datarowid="+datarowid+"&activitystarttime="+activitystarttime+"&activityendtime="+activityendtime,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									$("#calconfirmoverlay").fadeOut();
									alertpopup("Data is stored successfully.");
								},
							});
						});
						$("#calconfirmno").click(function() {
							revertFunc();
							$("#calconfirmoverlay").fadeOut();
						});
					}
				},
				eventDrop: function(event, element, revertFunc) {
					var datatype = event.type;
					if (datatype == 1) {
						var sdate = event.start.format();
						nsdate = sdate.split('T');
						var svalue = nsdate[0].split('-');
						var csdate = svalue[2]+'-'+svalue[1]+'-'+svalue[0];
						var taskstartdate = csdate;
						var datarowid = event.id;
						$("#calconfirmoverlay").fadeIn();
						$("#calconfirmyes").focus();
						$("#calconfirmyes").click(function() {
							$.ajax({
								url: base_url + "Calendar/taskdragupdate?datarowid="+datarowid+"&taskstartdate="+taskstartdate,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									$("#calconfirmoverlay").fadeOut();
									alertpopup("Data is stored successfully.");
									},
								});
							});
						$("#calconfirmno").click(function() {
							revertFunc();
							$("#calconfirmoverlay").fadeOut();
						});
			        } else {
			        	var asdate = event.start.format();
						ansdate = asdate.split('T');
						var asvalue = ansdate[0].split('-');
						var acsdate = asvalue[2]+'-'+asvalue[1]+'-'+asvalue[0];
			        	var nsedate = event.end.format();
			        	if (asdate == nsedate) {
			        		nsedate = ansdate;
			        	} else {
			        		nedate = nsedate.split('T');
							var evalue = nedate[0].split('-');
							var cedate = evalue[2]+'-'+evalue[1]+'-'+evalue[0];
			        	}
						if(ansdate.length > 1) {
							var ntime = ansdate[1].split(':');
							var ntimeval = ntime[0]+':'+ntime[1];
						}
						if(nedate.length > 1) {
							var etime = nedate[1].split(':');
							var etimeval = etime[0]+':'+etime[1];
						}
			        	var activitystartdate = acsdate;
			        	var activityenddate = cedate;
			        	var activitystarttime = ntimeval;
			        	var activityendtime = etimeval;
						var datarowid = event.id;
						$("#calconfirmoverlay").fadeIn();
						$("#calconfirmyes").focus();
						$("#calconfirmyes").click(function() {
							$.ajax({
								url: base_url + "Calendar/activitydragupdate?datarowid="+datarowid+"&activitystartdate="+activitystartdate+"&activityenddate="+activityenddate+"&activitystarttime="+activitystarttime+"&activityendtime="+activityendtime,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									$("#calconfirmoverlay").fadeOut();
									alertpopup("Data is stored successfully.");
									},
								});
							});
						$("#calconfirmno").click(function() {
							revertFunc();
							$("#calconfirmoverlay").fadeOut();
						});
			        }
				},
				eventMouseout: function(event, element) {
					$(this).find('.forhidedel').addClass('hidedisplay');
				},
				select: function(start, end) {// For Create task and activity
					var sdate = start.format();
					nsdate = sdate.split('T');
					var svalue = nsdate[0].split('-');
					var csdate = svalue[2]+'-'+svalue[1]+'-'+svalue[0];
					var edate = end.format();
					nedate = edate.split('T');
					var evalue = nedate[0].split('-');
					var cedate = evalue[2]+'-'+evalue[1]+'-'+evalue[0];
					if(nsdate.length > 1) {
						var ntime = nsdate[1].split(':');
						var ntimeval = ntime[0]+':'+ntime[1];
					}
					$("*").removeClass('calendaractive');
					$("#creationoverlay").fadeIn();
					$("#viewtaskspan").removeClass('hidedisplay');
					$("#viewactivityspan").removeClass('hidedisplay');
					$('#viewtaskspan').trigger('click');
					$('#tasktitlename').focus();
					$('#tasktitlename').val('');
					$('#taskpriority').select2('val',5);
					$('#taskstatus').select2('val',64);
					$('#activitypriority').select2('val',10);
					$('#activitystatus').select2('val',59);
					$("#taskstartdate").val(csdate);
					$("#activitystartdate").val(csdate);
					$("#taskenddate").val(cedate);
					$("#activityenddate").val(cedate);
					$("#activitystarttime").val(ntimeval);
					$("#forsubmitdata").removeClass('hidedisplay');
					$("#forupdatedata").addClass('hidedisplay');
					$('#s2id_contactid a span:first').text('');
					setTimeout(function(){
						$("#taskassignto").select2('val',loggedinuser).trigger('change');
						$("#activityassignto").select2('val',loggedinuser).trigger('change');
					},10);
					$("#createtask").click(function(){
						$("#validatetaskoverlay").validationEngine('validate');
					});
					jQuery("#validatetaskoverlay").validationEngine({
						onSuccess: function() {
							//for default id
							taskdefaultidvalget();
							var formdata = $("#taskoverlayform").serialize();
							var amp = '&';
							var datainformation = amp + formdata;
							$.ajax({
								url: base_url + "Calendar/tasknewdatacreate",
								data: "datas=" + datainformation,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									var nmsg =  $.trim(msg);
									if (nmsg == 'TRUE')	{
										$("#creationoverlay").fadeOut();
										clearform('cleartaskform');
										alertpopup("Data is stored successfully.");
										//for reload
										$('#calendar').fullCalendar( 'refetchEvents' );
										$('#calendar').fullCalendar('removeEvents');
										$('#calendar').fullCalendar('rerenderEvents' );
									}
								},
							});
						},
						onFailure: function() {
							var dropdownid =['1','taskassignto'];
							dropdownfailureerror(dropdownid);
							alertpopup(validationalert);
						}
					});	
					$("#createactivity").click(function(){
						$("#activitytaskaddvalidation").validationEngine('validate');
					});
					jQuery("#activitytaskaddvalidation").validationEngine({
						onSuccess: function() {
							//for default id
							activitydefaultidvalget();
							var formdata = $("#activityoverlayform").serialize();
							var amp = '&';
							var datainformation = amp + formdata;
							$.ajax({
								url: base_url + "Calendar/activitynewdatacreate",
								data: "datas=" + datainformation,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									var nmsg =  $.trim(msg);
									$("#creationoverlay").fadeOut();
									clearform('clearactivityform');
									if(softwareindustryid != 4) {
										alertpopup("Data is stored successfully.");
									}
									//for reload
									$('#calendar').fullCalendar( 'refetchEvents' );
									$('#calendar').fullCalendar('removeEvents');
									$('#calendar').fullCalendar('rerenderEvents' );
									if(softwareindustryid == 4){
										ACTIVITYID = nmsg;
										$("#coninvoverlay").fadeIn();
									}
								},
							});
						},
						onFailure: function() {
							var dropdownid =['1','activityassignto'];
							dropdownfailureerror(dropdownid);
							alertpopup(validationalert);
						}
					});	
				},
				eventSources: [
					{
					  url: base_url+"Calendar/taskviewdatafetch",
					},
					{
					  url: base_url+"Calendar/activitiesviewdatafetch",
					}
		           
				]
			});
		//Mini Calendar
			$('#minicalendars').fullCalendar({
				header: {
					left: 'prev,next ',
					right: 'title',
				},
				height: 'auto',
				selectable: true,
				selectHelper: true,
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				eventClick: function(event, element) { }, 
				eventMouseover: function(event, element) {
				},
				eventMouseout: function(event, element) {
					$(this).find('.forhidedel').addClass('hidedisplay');
				},
				dayClick: function(date, jsEvent, view) {
					var viewName = 'agendaDay';
					$('#calendar').fullCalendar( 'changeView', viewName );
					$('#calendar').fullCalendar( 'gotoDate', date.format() );
			    },
				eventSources: [
					{
					  url: base_url+"Calendar/taskviewdatafetch",
					},
					{
					  url: base_url+"Calendar/activitiesviewdatafetch",
					},
				]
			});
		});
	}
	$('#activityendtime,#activitystarttime,#taskstarttime,#taskendtime').timepicker({
		'step':15,
		'timeFormat': 'H:i',
		'scrollDefaultNow': true,
		'disableTextInput' : 'False'
	});
	$(".closecreator").click(function(){
		clearform('cleartaskform');
		clearform('clearactivityform');
		$("#creationoverlay").fadeOut();
	});
	$("#closeactivity").click(function(){
		$("#creationoverlay").fadeOut();
		$("#activityforsubmitdata").removeClass('hidedisplay');
		$("#activityforupdatedata").addClass('hidedisplay');
		clearform('clearactivityform');
	});
	
	//edit validation for task
	jQuery("#editvalidatetaskoverlay").validationEngine({
		onSuccess: function() {
			var formdata = $("#taskoverlayform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			var datarowid = $("#taskid").val();
			$.ajax({
				url: base_url+"Calendar/taskupadatefunction?datarowid="+datarowid,
				data: "datas=" + datainformation,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg = "TRUE") {
						$("#creationoverlay").fadeOut();
						$('#calendar').fullCalendar('unselect');
						clearform('cleartaskform');
						$("#forsubmitdata").addClass('hidedisplay');
						$("#forupdatedata").removeClass('hidedisplay');
						alertpopup("Data is stored successfully.");
						//for reload
						$('#calendar').fullCalendar( 'refetchEvents' );
						$('#calendar').fullCalendar('removeEvents');
						$('#calendar').fullCalendar('rerenderEvents' );
					}
				},
			});
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//edit validation for activity
	jQuery("#activitytaskeditvalidation").validationEngine({
		onSuccess: function() {
			var formdata = $("#activityoverlayform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			var datarowid = $("#activityid").val();
			$.ajax({
				url: base_url+"Calendar/activityupadatefunction?datarowid="+datarowid,
				data: "datas=" + datainformation,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg = "TRUE") {
						$("#creationoverlay").fadeOut();
						$('#calendar').fullCalendar('unselect');
						clearform('clearactivityform');
						alertpopup("Data is stored successfully.");
						$("#createactivity").show();
						$("#activityforsubmitdata").removeClass('hidedisplay');
						$("#activityforupdatedata").addClass('hidedisplay');
						//for reload
						$('#calendar').fullCalendar( 'refetchEvents' );
						$('#calendar').fullCalendar('removeEvents');
						$('#calendar').fullCalendar('rerenderEvents' );
					}
				},
			});
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#viewtaskspan").click(function(){
		$("#taskspan").show();
		$("#activityspan").hide();
	});
	$("#viewactivityspan").click(function(){
		$("#taskspan").hide();
		$("#activityspan").show();
	});	
	$('#gcalendar').fullCalendar({
        events: { url: "https://www.google.com/Calendar/feeds/ganeshj4%40aucventures.com/public/basic",
		backgroundColor:'yellow',
        }
    });	
	$("#gototaskicon").click(function(){
		window.location = base_url+"Task";
	});
	$("#gotoactivityicon").click(function(){
		window.location = base_url+"Activity";
	});
	//related record fetch
	$("#activitymodule").change(function(){
		var value = $("#activitymodule").val();
		$.ajax({
			url:base_url+"Calendar/dropdownvaluefecth?moduleid="+value,
			dataType:'json',
			async :false,
			cache:false,
			success: function(data) 
			{
				var fieldname = data.filedname;
				var tabname = data.tablename;
				recordnamefetch(fieldname,tabname,'activityrecord');//data is a table name
			},
		});
	});	
	$("#taskmodule").change(function(){
		var value = $("#taskmodule").val();
		$.ajax({
			url:base_url+"Calendar/dropdownvaluefecth?moduleid="+value,
			dataType:'json',
			async :false,
			cache:false,
			success: function(data) 
			{
				var fieldname = data.filedname;
				var tabname = data.tablename;
				recordnamefetch(fieldname,tabname,'taskrecord');//data is a table name
			},
		});
	});
	//print icon
	$("#printicon").click(function(){
		print_calendar();
	});
	//reload
	$("#reloadicon").click(function() {
		window.location = base_url+"Calendar";
	});
	//preview close
	$("#previewclose").click(function() {
		$('#previewoverlayoverlay').fadeOut();
	});
	$("#addicon").click(function() {
		$('#activitylistoverlay').fadeIn();
		activitygrid();
		tasklistgrid();
	});
	$("#activitylistspan").click(function(){
		$("#activityexportspan").show();
		$("#taskexportspan").hide();
		tasklistgrid();
	});
	$("#tasklistspan").click(function() {
		$("#activityexportspan").hide();
		$("#taskexportspan").show();
		activitygrid();
	});
	
	{//View change
		$("#monthview").click(function(){ // month view
			var viewName = 'month';
			$('#calendar').fullCalendar( 'changeView', viewName );
		});
		$("#weekview").click(function(){ // week view
			var viewName = 'agendaWeek';
			$('#calendar').fullCalendar( 'changeView', viewName );
		});
		$("#dayview").click(function(){ // day view
			var viewName = 'agendaDay';
			$('#calendar').fullCalendar( 'changeView', viewName );
		});
		$("#fourdayview").click(function(){ // fourday view
			var viewName = 'agendaFourDay';
			$('#calendar').fullCalendar( 'changeView', viewName );
		});
	}
	//import icon
	$('#importicon').click(function() {
		var viewfieldids = '205';
		sessionStorage.setItem("importmoduleid",viewfieldids);
		window.location = base_url+'Dataimport';
	});
	//data manipulation icon
	$("#datamanipulationicon").click(function() {
		var viewfieldids = '205';
		sessionStorage.setItem("datamanipulateid",viewfieldids);
		window.location = base_url+'Massupdatedelete';
	});
	//Customize icon
	$("#customizeicon").click(function() {
		var viewfieldids = '205';
		sessionStorage.setItem("customizeid",viewfieldids);
		window.location = base_url+'Formeditor';
	});
	//Find Duplication icon
	$("#findduplicatesicon").click(function() {
		var viewfieldids = '205';
		sessionStorage.setItem("finddubid",viewfieldids);
		window.location = base_url+'Findduplicates';
	});
	$("#editicon").click(function() {
		var id = $(".calendaractive").attr('data-id');
		if(!id) {
			alertpopup('Please Select the record');
		} else {
			var datatype = $(".calendaractive").attr('data-type');
			if(datatype == '1') {
				{// For Redirection Task Edit
					var taskidedit = id;
					sessionStorage.setItem("taskidforedit", taskidedit);
				}
				var fullurl = 'Task';
				window.location = base_url+fullurl;
			} else if(datatype == '2') {
				{// For Redirection Activity Edit
					var activityidedit = id;
					sessionStorage.setItem("activityidforedit", activityidedit);
				}
				var fullurl = 'Activity';
				window.location = base_url+fullurl;
			}
		}
	});
	$("#deleteicon").click(function() {
		var id = $(".calendaractive").attr('data-id');
		if(!id) {
			alertpopup('Please Select the record');
		} else {
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
		}
	});
	$("#basedeleteyes").click(function() {
		var currentid = $(".calendaractive").attr('data-id');
		var datatype = $(".calendaractive").attr('data-type');
		if(datatype == "1"){
			deletetaskdata(currentid,'action');
		} else if(datatype == "2"){
			deleteactivitydata(currentid,'action');
		}
		$('#calendar').fullCalendar( 'removeEvents', currentid );
	});
	//clone icon
	$("#cloneicon").click(function(){
		var id = $(".calendaractive").attr('data-id');
		if(id == 'undefined' || !id) {
			alertpopup('Please Select the record');
		} else {
			var datatype = $(".calendaractive").attr('data-type');
			if(datatype == '1' ) {
				{// For Redirection Task Edit
					var taskidedit = id;
					sessionStorage.setItem("taskidforclone", taskidedit);
				}
				var fullurl = 'Task';
				window.location = base_url+fullurl;
			} else if(datatype == '2' ) {
				{// For Redirection Activity Edit
					var activityidedit = id;
					sessionStorage.setItem("activityidforclone", activityidedit);
				}
				var fullurl = 'Activity';
				window.location = base_url+fullurl;
			}
		}
	});
	//sms icon
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});
	$("#smsicon").click(function() {
		var datarowid = $(".calendaractive").attr('data-id');
		if( datarowid == undefined) {
			alertpopup('Please Select the record');
		} else {
			var datatype = $(".calendaractive").attr('data-type');
			if(datatype == '1' ) {
				var viewfieldids = '206';
			} else {
				var viewfieldids = '205';
			}
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").hide();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						} else if(mobilenumber.length == 1) {
							sessionStorage.setItem("mobilenumber",mobilenumber);
							sessionStorage.setItem("viewfieldids",viewfieldids);
							sessionStorage.setItem("recordis",datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").show();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'smsmodule');
					}
				},
			});
		}
	});
	//sms module dd change
	$("#smsmodule").change(function() {
		var moduleid =	$("#smsmoduleid").val(); //main module
		var linkmoduleid =	$("#smsmodule").val(); // link module
		var recordid = $("#smsrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
		}
	});
	//sms overlay mobile number submit 
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#mailicon").click(function() {
		var datarowid = $(".calendaractive").attr('data-id');
		if(datarowid) {	
			sessionStorage.setItem("forcomposemailid",'');
			sessionStorage.setItem("forsubject",'');
			var fullurl = 'erpmail/?_task=mail&_action=compose';
			window.open(''+base_url+''+fullurl+'');
		} else {
			alertpopup("Please select a record");
		}	
	});
	//sms mobile number select
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data')) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	//call module dd chnge
	$("#callmodule").change(function() {
		var moduleid =	$("#callmoduleid").val(); //main module
		var linkmoduleid =	$("#callmodule").val(); // link module
		var recordid = $("#callrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
		}
	});
	//c2c icon click
	$("#outgoingcallicon").click(function() {
		var datarowid = $(".calendaractive").attr('data-id');
		if (datarowid) {
			var datatype = $(".calendaractive").attr('data-type');
			if(datatype == '1' ) {
				var viewfieldids = '206';
			} else {
				var viewfieldids = '205';
			}
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++){
							if(data[i] != ''){
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1){
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#c2cmobileoverlay").show();
						$("#callmoduledivhid").show();
						$("#callrecordid").val(datarowid);
						$("#callmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'callmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	//c2c overlay close
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	//c2c mobile number submit.
	$("#callnumbersubmit").click(function()  {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{//view change
		$("#dynamicdddataview").change(function(){
			var viewid = $("#dynamicdddataview").val();
			var moduleid = $("#dynamicdddataview").find('option:selected').data('moduleid');
			//for reload 
			$('#calendar').fullCalendar('destroy');
			$('#calendar').fullCalendar({	
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay,agendaFourDay'
				},
				views: {
			        agendaFourDay: {
			            type: 'agenda',
			            duration: { days: 4 },
			            buttonText: '4 day'
			        },
			        month: {
			        	eventLimit:3
			        }
			    },
			    selectable: true,
				selectHelper: true,
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				eventClick: function(event, element) { }, 
				eventMouseover: function(event, element) {
					$(".fc-content").click(function(){
						$(".fc-content").removeClass('calendaractive');
						$(this).removeClass('calendaractive');
						$(this).addClass('calendaractive');
					});
					{// For Update Event on Click 
						$(this).find('.editevent').click(function(){
							$("#creationoverlay").fadeIn();
							$("#forsubmitdata,#activityforsubmitdata").addClass('hidedisplay');
							$("#forupdatedata,#activityforupdatedata").removeClass('hidedisplay');
							var datarowid = event.id;
							var datatype = event.type;
							if(datatype == "1") {
								$(".active span").attr('style','');
								$(".active").removeClass('active');
								$("#viewtaskspan").addClass('active');
								$("#viewtaskspan").removeClass('hidedisplay');
								$("#viewactivityspan").addClass('hidedisplay');
								$("#taskspan").show();
								$("#activityspan").hide();
								fetchtaskformdata(datarowid);
							}
							else if(datatype == "2") {
								$(".active span").attr('style','');
								$(".active").removeClass('active');
								$("#viewactivityspan").addClass('active');
								$("#viewactivityspan").removeClass('hidedisplay');
								$("#viewtaskspan").addClass('hidedisplay');
								$("#taskspan").hide();
								$("#activityspan").show();
								fetchactivityformdata(datarowid);
							}
							Materialize.updateTextFields();
							$("#updatecreatetask").click(function(){
								$("#editvalidatetaskoverlay").validationEngine('validate');
							});
							$("#updatecreateactivity").click(function(){
								$("#activitytaskeditvalidation").validationEngine('validate');
							});
						});	
					}
					{// For Delete
						$(this).find('.forhidedel').removeClass('hidedisplay');
						$(this).find('.delevent').click(function(){
							var currentid = event.id;
							if(currentid) {		
								var currentid = event.id;
								var datatype = event.type;
								if(datatype == "1") {
									combainedmoduledeletealert('taskdeleteyes');
									$("#taskdeleteyes").click(function(){
										var datarowid =$("#brandprimarydataid").val();
										deletetaskdata(currentid,'event');
										$('#calendar').fullCalendar( 'removeEvents', currentid );
										$(this).unbind();
									});
								} else if(datatype == "2") {
									combainedmoduledeletealert('activitydeleteyes');
									$("#activitydeleteyes").click(function(){
										var datarowid =$("#brandprimarydataid").val();
										deleteactivitydata(currentid,'event');
										$('#calendar').fullCalendar( 'removeEvents', currentid );
										$(this).unbind();
									});
								}
							} else {
								alertpopup("Please select a row");
							}
						});
					}
				},
				eventMouseout: function(event, element) {
					$(this).find('.forhidedel').addClass('hidedisplay');
				},
				select: function(start) {// For Create task
					var sdate = start.format();
					var svalue = sdate.split('-');
					var csdate = svalue[2]+'-'+svalue[1]+'-'+svalue[0];
					var edate = end.format();
					var evalue = edate.split('-');
					var cedate = evalue[2]+'-'+evalue[1]+'-'+evalue[0];
					$("*").removeClass('calendaractive');
					$("#creationoverlay").fadeIn();
					$("#viewtaskspan").removeClass('hidedisplay');
					$("#viewactivityspan").removeClass('hidedisplay');
					$('#viewtaskspan').trigger('click');
					$('#tasktitlename').focus();
					$("#taskstartdate").val(csdate);
					$("#activitystartdate").val(csdate);
					$("#taskenddate").val(cedate);
					$("#activityenddate").val(cedate);
					$("#forsubmitdata").removeClass('hidedisplay');
					$("#forupdatedata").addClass('hidedisplay');
					$('#s2id_contactid a span:first').text('');
					$("#createtask").click(function(){
						$("#validatetaskoverlay").validationEngine('validate');
					});
					jQuery("#validatetaskoverlay").validationEngine({
						onSuccess: function() {
							//for default id
							taskdefaultidvalget();
							var formdata = $("#taskoverlayform").serialize();
							var amp = '&';
							var datainformation = amp + formdata;
							$.ajax({
								url: base_url + "Calendar/tasknewdatacreate",
								data: "datas=" + datainformation,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {
									var nmsg =  $.trim(msg);
									if (nmsg == 'TRUE')	{
										$("#creationoverlay").fadeOut();
										clearform('cleartaskform');
										//for reload
										$('#calendar').fullCalendar( 'refetchEvents' );
										$('#calendar').fullCalendar('removeEvents');
										$('#calendar').fullCalendar('rerenderEvents' );
									}
								},
							});
						},
						onFailure: function() {
							var dropdownid =['1','taskassignto'];
							dropdownfailureerror(dropdownid);
							alertpopup(validationalert);
						}
					});	
					$("#createactivity").click(function(){
						$("#activitytaskaddvalidation").validationEngine('validate');
					});
					jQuery("#activitytaskaddvalidation").validationEngine({
						onSuccess: function() {
							//for default id
							activitydefaultidvalget();
							var formdata = $("#activityoverlayform").serialize();
							var amp = '&';
							var datainformation = amp + formdata;
							$.ajax({
								url: base_url + "Calendar/activitynewdatacreate",
								data: "datas=" + datainformation,
								type: "POST",
								async:false,
								cache:false,
								success: function(msg) {alert();
									var nmsg =  $.trim(msg);
									$("#creationoverlay").fadeOut();
									clearform('clearactivityform');
									//for reload
									$('#calendar').fullCalendar( 'refetchEvents' );
									$('#calendar').fullCalendar('removeEvents');
									$('#calendar').fullCalendar('rerenderEvents' );
									if(softwareindustryid == 4) {
										ACTIVITYID = nmsg;
										$("#coninvoverlay").fadeIn();
									}
								},
							});
						},
						onFailure: function() {
							var dropdownid =['1','activityassignto'];
							dropdownfailureerror(dropdownid);
							alertpopup(validationalert);
						}
					});	
				},
				eventSources: [
		   			{
		   			  url: base_url+"Calendar/dynamicdatafetchbasedonview?viewid="+viewid+"&moduleid="+moduleid,
		   			},
		   		]
			});
		});
		$('#dynamicdddataview').select2()
        .on("select2-loaded", function(e) {
        	//added by calendar view change for multiple group dropdown
			maxLen =0;
        	for(var j=0; j < e.items.results.length; j++){
            	console.log(e.items.results[j].children[j].text.length);
            	if (e.items.results[j].children[j].text.length > maxLen){
                    maxLen = e.items.results[j].children[j].text.length;
                    var width = maxLen*8.5;
	                $('div.bigdrop').css('width', width+'px');
                }
            }
        });
	}
	$("#taskendtime").change(function(){
		var startdate = $('#taskstartdate').datepicker('getDate');
		var enddate = $('#taskenddate').datepicker('getDate');
		var starttime = $('#taskstarttime').val();
		var endtime = $('#taskendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	    	diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			if(splitst[0] >= splitet[0]) {
				if(splitst[1] >= splitet[1]) {
					$('#taskendtime').val('');
					alertpopup('Please select the end time based on the start time');
				}
			}
		}
	});
	$("#taskstarttime,#taskenddate").change(function(){
		var startdate = $('#taskstartdate').datepicker('getDate');
		var enddate = $('#taskenddate').datepicker('getDate');
		var starttime = $('#taskstarttime').val();
		var endtime = $('#taskendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	    	diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			$('#taskendtime').val('');
		}
	});
	$("#activitystarttime,#activityenddate").change(function(){
		var startdate = $('#activitystartdate').datepicker('getDate');
		var enddate = $('#activityenddate').datepicker('getDate');
		var starttime = $('#activitystarttime').val();
		var endtime = $('#activityendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	    	diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			$('#activityendtime').val('');
		}
	});
	$("#activitystarttime").click(function(){
		$('#activityendtime').val('');
	});
	$("#activityendtime").change(function(){
		var startdate = $('#activitystartdate').datepicker('getDate');
		var enddate = $('#activityenddate').datepicker('getDate');
		var starttime = $('#activitystarttime').val();
		var endtime = $('#activityendtime').val();
		var splitst = starttime.split(':');
		var splitet = endtime.split(':');
		var oneDay = 24*60*60*1000;
	    var diff = 0;
	    if (startdate && enddate) {
	    	diff = Math.round(Math.abs((enddate.getTime() - startdate.getTime())/(oneDay)));
	    }
	    if(diff == 0) {
			if(splitst[0] >= splitet[0]) {
				if(splitst[1] >= splitet[1]) {
					$('#activityendtime').val('');
					alertpopup('Please select the end time based on the start time');
				}
			}
		}
	});
	$("#convertyes").click(function(){
		sessionStorage.setItem("convertactivitytoinvoice",ACTIVITYID);
		window.location = base_url+'Invoice';
	});	
	$("#convertno").click(function(){
		$("#coninvoverlay").fadeOut();
	});
	var select2 = $('#contactid').select2({
	    placeholder: "Search for a Members",
	    minimumInputLength: 2,
	    ajax: {
		    url: base_url+"Calendar/memberdataload_dd",
		    type: 'post',
		    dataType: 'json',
		    quietMillis: 10,
		    data: function (term, page) {
		        return {
			        q: term,
			        type: $("#salestransactiontypeid").val(),
			        page: page
		        };
		    },
		    results: function (data, page) {
		        var more = (page * 30) < data.length;
		        return { results: data, more: more };
		    }
	    },
	  initSelection: function (element, callback) {
           var id = $(element).val();
           	if(id !== "") {
               $.ajax(base_url+"Calendar/memberdataload_dd", {
                   data: {acid:id,q:'',type: $("#salestransactiontypeid").val()},
                   type: 'post',
                   dataType: "json"
               }).done(function(data) {
                   callback({ id: data[0]['id'], text: data[0]['text'] });
				});
       		}
        },
	    formatResult: repoFormatResult,
	    formatSelection: repoFormatSelection,
	    dropdownCssClass: "bigdrop",
	    escapeMarkup: function (m) { return m; }
	});
	function repoFormatResult(repo) {
	    var desc = '';
	    if (repo.text) {
	    	desc += '<small>' + repo.text +' - '+repo.mobilenumber+ '</small>';
	    }
		return desc;
	}
	function repoFormatSelection(repo) {
		return repo.text+' - '+repo.mobilenumber;
	}
	//appointment name change
});
function activitygrid(page,rowcount) {
	var invitemoduleid = '205';
	var viewid = '2';
	var maintabinfo = 'crmactivity';
	var parentid = 'crmactivityid';
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#activitygrid').width();
	var wheight = $('#activitygrid').height();
	//col sort
	var sortcol = $('.activitylistheadercolsort').hasClass('datasort') ? $('.activitylistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.activitylistheadercolsort').hasClass('datasort') ? $('.activitylistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.activitylistheadercolsort').hasClass('datasort') ? $('.activitylistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Calendar/activitylistfetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+invitemoduleid+"&viewid="+viewid+"&checkbox=true",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#activitygrid').empty();
			$('#activitygrid').append(data.content);
			$('#activitygridfooter').empty();
			$('#activitygridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('activitygrid');
			{//sorting
				$('.activitylistheadercolsort').click(function(){
					$('.activitylistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#activitylistpgnum li.active').data('pagenum');
					var rowcount = $('ul#activitylistpgnumcnt li .page-text .active').data('rowcount');
					activitygrid(page,rowcount);
				});
				sortordertypereset('activitylistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#activitylistpgnumcnt li .page-text .active').data('rowcount');
					activitygrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#activitylistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					activitygrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#activitylistpgnumcnt li .page-text .active').data('rowcount');
					activitygrid(page,rowcount);
				});
				$('#activitylistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#activitylistpgnum li.active').data('pagenum');
					activitygrid(page,rowcount);
				});
			}
			//header check box
			$(".activitylist_headchkboxclass").click(function() {
				checkboxclass('activitylist_headchkboxclass','activitylist_rowchkboxclass');
				getcheckboxrowid(invitemoduleid);
			});
			//row based check box
			$(".activitylist_rowchkboxclass").click(function(){
				$('.activitylist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(invitemoduleid);
			});			
		}
	});
}
function tasklistgrid(page,rowcount) {
	var invitemoduleid = '206';
	var viewid = '18';
	var maintabinfo = 'crmtask';
	var parentid = 'crmtaskid';
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#activitygrid').width();
	var wheight = $('#activitygrid').height();
	//col sort
	var sortcol = $('.tasklistheadercolsort').hasClass('datasort') ? $('.tasklistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.tasklistheadercolsort').hasClass('datasort') ? $('.tasklistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.tasklistheadercolsort').hasClass('datasort') ? $('.tasklistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Calendar/tasklistfetch?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+invitemoduleid+"&viewid="+viewid+"&checkbox=true",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#tasklistgrid').empty();
			$('#tasklistgrid').append(data.content);
			$('#tasklistgridfooter').empty();
			$('#tasklistgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('tasklistgrid');
			{//sorting
				$('.tasklistheadercolsort').click(function(){
					$('.tasklistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#tasklistpgnum li.active').data('pagenum');
					var rowcount = $('ul#tasklistpgnumcnt li .page-text .active').data('rowcount');
					tasklistgrid(page,rowcount);
				});
				sortordertypereset('tasklistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#tasklistpgnumcnt li .page-text .active').data('rowcount');
					tasklistgrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#tasklistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					tasklistgrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#tasklistpgnumcnt li .page-text .active').data('rowcount');
					tasklistgrid(page,rowcount);
				});
				$('#tasklistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#tasklistpgnum li.active').data('pagenum');
					tasklistgrid(page,rowcount);
				});
			}
			//for task
			//header check box
			$(".tasklist_headchkboxclass").click(function() {
				checkboxclass('tasklist_headchkboxclass','tasklist_rowchkboxclass');
				getcheckboxrowid(invitemoduleid);
				
			});
			//row based check box
			$(".tasklist_rowchkboxclass").click(function(){
				
				$('.tasklist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid(invitemoduleid);
			});
		}
	});
}
$("#exporticon").click(function(){
	var ids = $("#activitiesid").val();
	$.ajax({
		url:base_url+"Calendar/selectedeventsfetch?eventids="+ids,
		dataType:'json',
		async :false,
		cache:false,
		success: function(ndata) {
			var tzone = ndata.tzone;
			var len = ndata.length;
			if (ndata) {
				$.each(ndata, function(i, v) {
					if (i != 'tzone') {
						var tit = v.crmactivityname;
						var sdt = v.activitystartdate+"T"+v.activitystarttime+tzone;
						var edt = v.activityenddate+"T"+v.activityendtime+tzone;
						var pid = v.crmactivityid;
						var desc = v.description;
						var location = v.location;
						insertEvent(tit, desc, pid, sdt, edt, location);
					}else{
						console.log('Last field');
					}
				});
			}
		},
	});
});
$("#exporttaskicon").click(function(){
	var ids = $("#tasklistid").val();
	$.ajax({
		url:base_url+"Calendar/selectedtasksfetch?taskids="+ids,
		dataType:'json',
		async :false,
		cache:false,
		success: function(ndata) {
			var tzone = ndata.tzone;
			var len = ndata.length;
			if (ndata) {
				$.each(ndata, function(i, v) {
					if (i != 'tzone') {
						exportTask(v);
					}else{
						//return true;
						console.log('Last field');
					}
				});
			}
		},
	});
});
$("#gridlistclose").click(function(){
	$("#activitylistoverlay").fadeOut();
	clearform('gridlistexport');
});
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").attr('checked', true);
	} else {
		$("."+rowclass+"").removeAttr('checked');
	}
}
function getcheckboxrowid(invitemoduleid) {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	if(invitemoduleid == '205'){
		$("#activitiesid").val(selected);
	} else {
		$("#tasklistid").val(selected);
	}
}
{//date Range
	function daterangefunction(generalstartdate,generatenddate){ 
		var dateformetdata = $('#'+generalstartdate+'').attr('data-dateformater');
		var startDateTextBox = $('#'+generalstartdate+'');
		var endDateTextBox = $('#'+generatenddate+'');
		startDateTextBox.datetimepicker({ 
			//timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			//minDate:0,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#'+generalstartdate+'').focus();
			},
			onSelect: function (selectedDateTime) {
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					$(this).prev('div').remove();
					$('.activityenddateformError,.taskenddateformError').remove();
					$('.hasDatepicker').removeClass('error');
				}
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
		endDateTextBox.datetimepicker({ 
			//timeFormat: 'HH:mm z',
			dateFormat: dateformetdata,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate){}
				}
				$('#'+generatenddate+'').focus();
			},
			onSelect: function (selectedDateTime){
			}
		}).click(function() {
			$(this).datetimepicker('show');
		});
	}
}
//task default value getAttention
function taskdefaultidvalget(){
	$.ajax({
        url: base_url + "Calendar/taskdefaultvalueget",
		dataType: "json",
		async:false,
		cache:false,
        success: function(data) {
			var id = parseFloat(data)+parseFloat(1);
		   if (data != "fail"){
				var id = $('#taskid').val(id);
			}
            else{
				var id = $('#taskid').val('1');
            }
        },
    });
}
//activity default value getAttention
function activitydefaultidvalget(){
	$.ajax({
        url: base_url + "Calendar/activitydefaultvalueget",
		dataType: "json",
		async:false,
		cache:false,
        success: function(data) {
			var id = parseFloat(data)+parseFloat(1);
		   if (data != "fail"){
				var id = $('#activityid').val(id);
			}  else {
				var id = $('#activityid').val('1');
            }
        },
    });
}
//fetch task form data
function fetchtaskformdata(datarowid){
	$.ajax({
        url: base_url+"Calendar/fetchtaskvalue?datarowid="+datarowid,
		dataType: "json",
		async:false,
		cache:false,
        success: function(data) {
			var respstatus = data['fail'];
			if(respstatus != 'FAILED'){
				var datanames = ['10','datarowid','taskname','startdate','starttime','enddate','endtime','empid','status','priority','mid'];
				var textboxname = ['10','taskid','tasktitlename','taskstartdate','taskstarttime','taskenddate','taskendtime','taskassignto','taskstatus','taskpriority','taskmodule'];
				var dropdowns = ['4','taskassignto','taskstatus','taskpriority','taskmodule'];
				textboxsetvalue(textboxname,datanames,data,dropdowns);
				$("#taskmodule").trigger('change');
				setTimeout(function(){
					$("#taskrecord").select2('val',data['rid']);
				},1000);
			}
			else{
				clearform('cleartaskform');
			}
        },
    });
}
//fetch activity form data
function fetchactivityformdata(datarowid){
	$.ajax({
        url: base_url+"Calendar/fetchactivityvalue?datarowid="+datarowid,
		dataType: "json",
		async:false,
		cache:false,
        success: function(data) {
			var respstatus = data['fail'];
			if(respstatus != 'FAILED'){
				var datanames = ['12','datarowid','activityname','startdate','starttime','enddate','endtime','empid','status','priority','mid','productid','description'];
				var textboxname = ['12','activityid','activityname','activitystartdate','activitystarttime','activityenddate','activityendtime','activityassignto','activitystatus','activitypriority','activitymodule','productid','description'];
				var dropdowns = ['5','activityassignto','activitystatus','activitypriority','activitymodule','productid'];
				textboxsetvalue(textboxname,datanames,data,dropdowns);
				$("#activitymodule").trigger('change');
				setTimeout(function(){
					var productid = data.productid;
					$("#productid").select2('val',productid.split(','));
					$("#activityrecord").select2('val',data['rid']);
					$("#contactid").val(data['contactid']);
					var contactname = data['contactname']+' - '+data['mobilenumber'];
					$('#s2id_contactid a span:first').text(contactname);
					
				},100);
			}
			else{
				clearform('clearactivityform');
			}
        },
    });
}
//record delete for task
function deletetaskdata(currentid,type) {
	$.ajax({
        url: base_url+"Calendar/recorddeltefortask?datarowid="+currentid,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	if(type == 'event'){
            		$("#basedeleteoverlayforcommodule").fadeOut();
            	} else{
            		$("#basedeleteoverlay").fadeOut();	
            	}
				alertpopup('Task Record deleted successfully');
			}
        },
    });
}
//record delete for activity
function deleteactivitydata(currentid,type) {
	$.ajax({
        url: base_url+"Calendar/recorddelteforactivity?datarowid="+currentid,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	if(type == 'event'){
            		$("#basedeleteoverlayforcommodule").fadeOut();
            	} else{
            		$("#basedeleteoverlay").fadeOut();	
            	}
				alertpopup('Activity Record deleted successfully');
			}
        },
    });
}
//record data fetch
function recordnamefetch(fieldname,tabname,ddname) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').select2('val','');
	$.ajax({
		url:base_url+"Calendar/recordnamefetch?fieldname="+fieldname+"&tabname="+tabname,
		dataType:'json',
		async :false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {   
				$('#'+ddname+'').empty();
				$('#'+ddname+'').append($("<option></option>"));
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}

/* Calendar Print Starts */
function print_calendar() {
   // save current calendar width
	$('#calendar').fullCalendar('option', 'aspectRatio', 2.8);
    w = $('#calendar').css('width');
    h = $('#calendar').css('height');
    // prepare calendar for printing
    $('#calendar').css('width', '100%');
    $('#supportpageoverlay, #processingsrc, #notificationdivoverlay, #gcalsyncoverlay, #mobilenumbershow, #creationoverlay, #activitylistoverlay, #templatepdfoverlay, .overlay').hide();
    $('.header-content, .actionmenucontainer').hide();
    $('.fc-header').hide(); 
    $('#calendar').fullCalendar('render');    
    window.print();
    // return calendar to original, delay so the print processes the correct width
    window.setTimeout(function() {
        $('.fc-header').show();
        $('.actionmenucontainer').show();
        $('.header-content').show();
        $('#calendar').fullCalendar('option', 'aspectRatio', 1.35);
        $('#calendar').fullCalendar('option', 'height', h);
        $('#calendar').css('width', w);
        $('#calendar').fullCalendar('render');
    }, 100);
}

//google calendar
var CLIENT_ID = '857662946719-g9qdl774dncdfp45f030tq78kr5csj81.apps.googleusercontent.com';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
var SCOPES = "https://www.googleapis.com/auth/calendar";

var authorizeButton = 'authorize-button';
var signoutButton = 'signout-button';
/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    listUpcomingEvents();
  } else {
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
  var pre = document.getElementById('content');
  var textContent = document.createTextNode(message + '\n');
  pre.appendChild(textContent);
}

/**
 * Print the summary and start datetime/date of the next ten events in
 * the authorized user's calendar. If no events are found an
 * appropriate message is printed.
 */
function listUpcomingEvents() {
	eventdata = [];
	array = [];
  	gapi.client.calendar.events.list({
	    'calendarId': 'primary',
	    'timeMin': (new Date()).toISOString(),
	    'showDeleted': true,
	    'singleEvents': true,
	    'orderBy': 'startTime'
  	}).then(function(response) {
  		var events = response.result.items;
  		if (events.length > 0) {
  			for (i = 0; i < events.length; i++) {
  				var event = events[i];
  				var type = event.kind;
  				var id = event.id;
  				var status = event.status;
  				var link = event.htmlLink;
  				var name = event.summary;
  				var startdate = event.start;
  				startdatetime  = startdate.dateTime;
  				if (typeof startdatetime == 'undefined') {
  					startdatetime = startdate.date;
  				}
  				var enddate = event.end;
  				enddatetime  = enddate.dateTime;
  				if (typeof enddatetime == 'undefined') {
  					enddatetime = enddate.date;
  				}
  				googlecalendardatainsert(type,id,status,link,name,startdatetime,enddatetime);
  			}
  		} else {
  			alertpopup('There is no new event on your authorization account');
  		}
  	});
}
function googlecalendardatainsert(type,id,status,link,name,startdate,enddate) {
	$.ajax({
		url:base_url+"Calendar/googlecalendardatainsert",
		data: 'type='+type+"&id="+id+"&status="+status+"&link="+link+"&name="+name+"&startdatetime="+startdate+"&enddatetime="+enddate,
		type:"POST",
		dataType:'json',
		async :false,
		cache:false,
		success: function(data) {
			if(data == 'insert'){
				console.log('data insert completed');
			} else if(data == 'Update') {
				console.log('data update completed');
			} else if(data == 'Delete') {
				console.log('data delete completed');
			}
		},
	});
}