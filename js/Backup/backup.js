$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Grid Calling Function
		backupgrid();
	}
	$('#groupcloseaddform').hide();
	{ //keyboard shortcut reset global variable
		viewgridview = 0;
	}
	$("#backupsettingsmailoverlay").click(function() {
		var emailopt = $('#emailopt').val();
		var hiddenemailid = $('#hiddenemailid').val();
		if(emailopt == 'Yes') {
			$('#emailoption').prop('checked', true);
		} else {
			$('#emailoption').prop( "checked", false );
		}
		if(hiddenemailid != '') {
			$('#emailid').val(hiddenemailid);
		} else {
			$('#emailid').val('');
		}
		$('#salescommentoverlay').fadeIn();
		firstfieldfocus();
		Materialize.updateTextFields();
	});
	$("#backupsettingsclose").click(function() {
		$('#salescommentoverlay').fadeOut();	
	});
	
	//data base settings update
	$('#backupsettingsupdate').click(function(e){
		$("#backupsettingswizard").validationEngine('validate');
    });
	jQuery("#backupsettingswizard").validationEngine({
		onSuccess: function() {
			var data = $("#backupsettingsform").serialize();
			var emailid = $("#emailid").val();
			if($('#emailoption').prop("checked") == true) {
				var emailopt = 'Yes';
			} else {
				var emailopt = 'No';
			}
			$('#hiddenemailid').val(emailid);
			$('#emailopt').val(emailopt);
			var amp = '&';
			var backupsetformdata = amp + data+"&emailid="+emailid+"&emailopt="+emailopt;
			$.ajax({
				url: base_url + "Backup/addbackupsettings",
				data: "data=" + backupsetformdata,
				type:'POST',
				async:false,
				success: function(msg) {
					alertpopup('Data(s) stored successfully');
					$('#salescommentoverlay').fadeOut();
					backuprefreshgrid();
				},
			});
		},
		onFailure: function() {
		}
	});
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{ //Backup location creation Overlay
		$('#backuplocation').click(function(){
			var drive = $('#backupdrive').val();
			$('#drivename').select2('val',drive).trigger('change');
			$('#backuplocationovrelay').show();
			firstfieldfocus();
		});
		$('#backupoverlayclose').click(function(){
			$('#backuplocationovrelay').fadeOut();
		});
	}
	{//new database backup
		$('#backupcreate').click(function() {
			var fname = $('#backuploc').val();
			if(fname!='') {
				$('#processoverlay').show();
				$.ajax({
					type:'POST',
					async:false, 
					url:base_url+'dbbackup/backup.php', 
					data:$('#databasebackupform').serialize(), 
					success: function(response) {
						if(response=='Success') {
							$('#processoverlay').fadeOut();
							alertpopup('Data backup created successfully!');
							backuprefreshgrid();
						} else {
							$('#processoverlay').fadeOut();
							alertpopup('Data backup failed!');
						}
					},
				});
			} else {
				$('#backuplocationovrelay').show();
				firstfieldfocus();
			}
		});
		$('#sendemailbackup').click(function() {
			var fname = $('#backuploc').val();
			var emailid = $('#hiddenemailid').val();
			var mailenable = $('#emailopt').val();
			if(fname != '' && emailid != '' && mailenable == 'Yes') {
				$('#processoverlay').show();
				$.ajax({
				type:'POST',
				async:false, 
				url:base_url+'dbbackup/emailbackup.php', 
				data:$('#databasebackupform').serialize(), 
				success: function(response) {
						if(response=='Success') {
							$('#processoverlay').fadeOut();
							alertpopup('Data backup created successfully!');
							backuprefreshgrid();
						} else {
							$('#processoverlay').fadeOut();
							alertpopup('Data backup failed!');
						}
					},
				});
			}
		});
	}
	{//download backup file
		$('#backupdownload').click(function() {
			var backupids = $('#backupgrid div.gridcontent div.active').attr('id');
			if(backupids) {
				var path = base_url+'dbbackup/download.php'; 
				var form = $('<form action="'+ path +'" class="hidedisplay" name="downfile" id="downfile" method="POST" target="_blank"><input type="hidden" name="downloadid" id="downloadid" value="'+backupids+'"/></form>');
				$(form).appendTo('body');
				form.submit();
			} else {
				alertpopup('Please select a row');
			}
		});
		//delete file
		$('#backupdelete').click(function() {
			var backupids = $('#backupgrid div.gridcontent div.active').attr('id');
			if(backupids) {
				var userid=$('#updateuser').val();
				$.ajax({
					url:base_url+"dbbackup/backup.php?delid="+backupids+"&updateuserid="+userid,
					success: function(msg) {
						if(msg=='deleted') {
							alertpopup("Selected files are deleted");
							backuprefreshgrid();
						} else {
							alertpopup("Files not deleted");
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		//decryptionscript file
		$('#decryptionscript').click(function() {
			var backupids = $('#backupgrid div.gridcontent div.active').attr('id');
			if(backupids) {
				var userid=$('#updateuser').val();
				$.ajax({
					url:base_url+"dbbackup/backup.php?decryptionid="+backupids+"&updateuserid="+userid,
					success: function(msg) {
						if(msg=='decrypted') {
							alertpopup("Selected files are decrypted");
							backuprefreshgrid();
						} else {
							alertpopup("Something is problem. Please try again later.");
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		{
			$('#schedulermethodid').change(function() {
				var schmethodid = $(this).val();
				if(schmethodid > 1) {
					$.ajax({
						url:base_url+"Backup/schedulerinformation?schmethodid="+schmethodid,
						dataType:'json',
						async:false,
						cache:false,
						success: function(msg) {
							if(msg['scheduleroption'] == 'Yes') {
								$('#schedulerenable').prop('checked', true);
							} else {
								$('#schedulerenable').prop('checked', false);
							}
							if(msg['schedulerid'] != '') {
								$('#schedulename').val(msg['schedulername']).trigger('change');
								$('#schedulertypeid').select2('val',msg['schedulertypeid']).trigger('change');
								$('#intervaltime').val(msg['intervaltime']).trigger('change');
								$('#hiddenschedulerid').val(msg['schedulerid']);
							} else {
								$('#schedulertypeid').select2('val','').trigger('change');
								$('#schedulename,#intervaltime').val('').trigger('change');
								$('#hiddenschedulerid').val('1');
							}
						},
					});
				}
			});
			//Schedule Settings overlay
			$('#schedulesettings').click(function() {
				var fname = $('#backuploc').val();
				if(fname!='') {
					$('#schedulesettingsoverlay').fadeIn();
					$('#schedulertypeid,#schedulermethodid').select2('val','').trigger('change');
					$('#schedulename,#intervaltime').val('').trigger('change');
					Materialize.updateTextFields();
				} else {
					$('#backuplocationovrelay').show();
					firstfieldfocus();
				}
			});
			$('#schedulerformclose').click(function() {
				$('#schedulesettingsoverlay').fadeOut();
			});
			$('#schedulerformsubmit').click(function(e) {
				$("#schedulervalidationwizard").validationEngine('validate');
			});
			jQuery("#schedulervalidationwizard").validationEngine({
				onSuccess: function() {
					var data = $("#schedulerform").serialize();
					var amp = '&';
					var backupsetformdata = amp + data;
					if($('#schedulerenable').prop('checked') == true) {
						var schedulerenable = 'Yes';
					} else {
						var schedulerenable = 'No';
					}
					var schedulermethodname = $('#schedulermethodid').find('option:selected').attr('data-name');
					var schedulename = $('#schedulename').val();
					var schedulertypename = $('#schedulertypeid').find('option:selected').attr('data-name');
					var intervaltime = $('#intervaltime').val();
					var arraydetails = {
						Name:schedulename,
						Type:schedulermethodname,
						SchedulerType:schedulertypename,
						minuteval:intervaltime
					}
					$.ajax({
						url: base_url + "Backup/schedulerinformationupdate",
						data: "data=" + backupsetformdata+amp+"schedulerenable="+schedulerenable,
						type:'POST',
						async:false,
						success: function(msg) {
							alertpopup('Data(s) stored successfully');
							$('#schedulesettingsoverlay').fadeOut();
							if(schedulerenable == 'Yes') {
								$.ajax({
									url:base_url+"dbbackup/schedulebackup.php",
									type:'POST',
									data:{arraydetails:arraydetails},
									success: function(msg) {
										
									},
								});
							}
						},
					});
				},
				onFailure: function() {
				}
			});
		}
		//drive change events
		$('#drivename').change(function(){
			var dname = $('#drivename').val();
			$.ajax({
				url:base_url+"Backup/listdfname?dname="+dname,
				success: function(msg) {
					$('#locfoldername').select2('val','').trigger('change');
					$('#locfoldername').empty();
					$('#locfoldername').append(msg);
					setTimeout(function(){
						var fname = $('#backuploc').val();
						$('#locfoldername').select2('val',fname).trigger('change');
					},200);
				},
			});
		});
		//data base settings update
		$('#locationsubmit').click(function(e){
			$("#locationvalidationwizard").validationEngine('validate');
		});
		jQuery("#locationvalidationwizard").validationEngine({
			onSuccess: function() {
				var data = $("#locationsetform").serialize();
				var amp = '&';
				var backupsetformdata = amp + data;
				$.ajax({
					url: base_url + "Backup/addbackuplocation",
					data: "data=" + backupsetformdata,
					type:'POST',
					async:false,
					success: function(msg) {
						alertpopup('Data(s) stored successfully');
						$('#backuplocationovrelay').fadeOut();
					},
				});
			},
			onFailure: function() {
			}
		});
	}
	{//Check box Operation
		$('#emailoption').change(function() {
				if($(this).is(":checked")) {
					$("#mailspan").show();
			} else{
				$("#mailspan").hide();
			}     
		});
		$('#reccheckbox').change(function() {
				if($(this).is(":checked")) {
					$("#recurspan").show();
			} else{
				$("#recurspan").hide();
			}     
		});
	}
});
{//backup grid
	function backupgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#backupgrid').width();
	var wheight = $('#backupgrid').height();
	/*col sort*/
	var sortcol = $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.backupheadercolsort').hasClass('datasort') ? $('.backupheadercolsort.datasort').attr('id') : '0';
	var userviewid ='';
	var viewfieldids ='';
		$.ajax({
			url:base_url+"Backup/gridinformationfetch?viewid="+userviewid+"&maintabinfo=backup&primaryid=backupid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=46'+"&checkbox=true",
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#backupgrid').empty();
				$('#backupgrid').append(data.content);
				$('#backupgridfooter').empty();
				$('#backupgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('backupgrid');
				{//sorting
					$('.backupheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.backupheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortpos = $('#itemtaggrid .gridcontent').scrollLeft();
						backupgrid(page,rowcount);
						$('#smscampaignsubaddgrid1 .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('backupheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						backupgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#backuppgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						backupgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#backuppgrowcount').material_select();
			},
		});
	}
}
function backuprefreshgrid() {
	var page = $('ul#backuppgnum li.active').data('pagenum');
	var rowcount = $('ul#backuppgnumcnt li .page-text .active').data('rowcount');
	backupgrid(page,rowcount);
}