$(document).ready(function() {
	{// Foundation Initialization
        $(document).foundation();
    }

    {// Maindiv height width change
        maindivwidth();
    }   
    {// Grid Calling
        leadaccountsgrid();
        chitbookeditstatus =0;
        //crud action
		crudactionenable();
    }
    {
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// vishal code
	/* {
		$(".select2, .select2-multiple").on('select2:open', function (e) {
			$('.select2-search input').prop('focus',false);
		});
	} */
	/* {
		$(".input-field input").focus(function() {
			var a = $(this).attr('id');
			var element = document.getElementById(a);
			element.scrollIntoView();
			});
	} */
	}
	CHITBOOKDELETE = 0;
    STONESETTINGDELETE = 0;
    if(softwareindustryid == 3) {
		stonestatus = $('#stonestatus').val();
		schemestatus = $('#schemestatus').val();
		iconhideshow('stonesettingicon',stonestatus);   // stone setting hide/show 
		iconhideshow('chitbookicon',schemestatus);   // chitbook hide/show
		accountmobilevalidation = $('#accountmobilevalidation').val(); // Account creation mobile number validation from company settings
		accountmobileunique = $('#accountmobileunique').val(); // Account creation mobile number unique validation from company settings
		userroleid = $('#userroleid').val(); //get userroleid for login person
		chitauthrole = $('#chitauthrole').val(); // Account creation mobile number validation from company settings
		chitauthrole = chitauthrole.split(",");
		creditinfohideshowvar = $('#creditinfohideshow').val(); //Credit Information HideShow in formfields
		chitemployeetypeid = $('#chitemployeetypeid').val(); //retrieve employeetype for chit direct page
		accountfieldcaps = $('#accountfieldcaps').val(); // Account Field automatic caps after given value
		chitbookoverlaystatus = 0;
		stonesettingoverlaystatus = 0;
		weight_round = $('#weight_round').val();
		amount_round = $('#amount_round').val();
		$("#dutyrateiddivhid,#taxapplytypeiddivhid,#taxratedivhid,#liveimage_path,#signaturepad_path").hide();
		$("label[for='liveimage_path']").html("");
		$("label[for='signaturepad_path']").html("");
		{ // account/Parentaccount drop down data load 
			function repoFormatResult(repo) {
				var desc = '';
				if (repo.accountname) {
					if(repo.mobilenumber != ''){
						desc += '<small>' + repo.accountname +' - '+repo.mobilenumber+ '</small>';
					} else {
						desc += '<small>' + repo.accountname + '</small>';
					}
				}
				return desc;
			}
			function repoFormatSelection(repo) {
				if(repo.mobilenumber != '') {
					return repo.accountname+' - '+repo.mobilenumber;
				} else {
					return repo.accountname;
				}
			}
			var select2 = $('#parentaccountid').select2({
				placeholder: "Search for a Account",
				minimumInputLength: 2,
				ajax: {
					url: base_url+"Account/account_dd",
					type: 'post',
					dataType: 'json',
					quietMillis: 10,
					data: function (term, page) {
						return {
							q: term,
							type: $("#accounttypeid").val(),
							page: page
						};
					},
					results: function (data, page) {
						var more = (page * 30) < data.length;
						return { results: data, more: more };
					}
				},
			  initSelection: function (element, callback) {
				   var id = $(element).val();
					if(id !== "") {
					   $.ajax(base_url+"Account/account_dd", {
						   data: {acid:id,q:'',type: $("#accounttypeid").val()},
						   type: 'post',
						   dataType: "json"
					   }).done(function(data) {
						   callback({ id: data[0]['id'], text: data[0]['text'] });
					   });
					}
				},
				formatResult: repoFormatResult,
				formatSelection: repoFormatSelection,
				dropdownCssClass: "bigdrop",
				escapeMarkup: function (m) { return m; }
			});
		}
		$('#signaturepad_pathdivhid').append('<div class="wrapper"><canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas></div><div><input id="signaturepadsave" class="" type="button" value="Save" name="signaturepadsave">&nbsp;<input id="signaturepadclear" class="" type="button" value="Clear" name="signaturepadclear">&nbsp;<input id="signaturepaddelete" class="" type="button" value="Delete" name="signaturepaddelete"></div>');
		$('#liveimage_pathdivhid').append('<div class="large-12 columns">&nbsp;</div><div class="large-12 columns"><div id="taglivedisplay" class="large-12 medium-12 small-12 columns" style="border:1px solid #ffffff; height:20rem; padding:0px;"></div></div><div class="snapshotsavebtn"><input id="take-snapshot" class="btn leftformsbtn" type="button" value="Capture" tabindex="305">&nbsp;<input type="button" value="Save" id="savecapturedimg" class="btn leftformsbtn" tabindex="124">&nbsp;<input type="button" value="Remove" id="resetcapturedimg" class="btn leftformsbtn" tabindex="125"></div><textarea class="hidedisplay" id="imgurldata" tabindex="309"></textarea><textarea class="hidedisplay" id="imgurldatadelete" tabindex="310"></textarea>');
		$('#accountlogodivhid').append('<span id="imgcapture" class="" title="Click To Take Photo" style="padding:2px; color:#2574a9; font-size:26px;"><i class="material-icons">add_a_photo</i></span>');
	}
	{ // Auto Complete Address, Area, Postal Code, City by Kumaresan
		$("#billingaddress").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompleteaddress",
					success: function (data) {
						var array = $.map(data.address, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#billingaddress").val(ui.item.label);
            }
		});
		$("#primaryarea").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletearea",
					success: function (data) {
						var array = $.map(data.primaryarea, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#primaryarea").val(ui.item.label);
            }
		});
		$("#billingpincode").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletepostalcode",
					success: function (data) {
						var array = $.map(data.pincode, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#billingpincode").val(ui.item.label);
            }
		});
		$("#billingcity").autocomplete({
			source: function (request, response) {
				$.ajax({
					dataType: "json",
					data: { term: request.term, },
					type: 'POST',
					url: base_url+"Base/autocompletecity",
					success: function (data) {
						var array = $.map(data.city, function (item) {
							return {
								label: item,
								value: item
							}
						});
						//call the filter here
						response($.ui.autocomplete.filter(array, request.term));
					}
				});
			},
			minLength: 3,
			select: function (event, ui) {
				$("#billingcity").val(ui.item.label);
            }
		});
	}
	//* Signature Pad Code *//
	if(softwareindustryid == 3) {
		var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
			backgroundColor: 'rgba(255, 255, 255, 0)',
			penColor: 'rgb(0, 0, 0)'
		});
		$('#signaturepaddelete').hide();
		$('#signaturepadsave').click(function() {
			if(signaturePad.isEmpty()) {
				alertpopup("Please provide signature first.");
			} else {
				var data = signaturePad.toDataURL('image/png');
				var img_data = data.replace(/^data:image\/(png|jpg);base64,/, "");
				var deletepathvalue = '';
				$.ajax({
					url: base_url+'signaturepad.php',
					data: {img_data:img_data,deletepathvalue:deletepathvalue},
					type: 'post',
					dataType: 'json',
					success: function (response) {
						if(response['status'] == 1) {
							$('#signatureimgpath').val(response['file_name']);
							$('#signaturepadsave,#signaturepadclear').hide();
							$('#signaturepaddelete').show();
							alertpopup('Signature saved successfully!.');
						} else {
							alertpopup('There is something problem. Please try again.!');
						}
					}
				});
			}
		});
		$('#signaturepadclear').click(function() {
			signaturePad.clear();
			$('#signaturepadsave').show();
		});
		$('#signaturepaddelete').click(function() {
			if(signaturePad.isEmpty()) {
				alertpopup("Please provide signature first.");
			} else {
				var img_data = '';
				var deletepathvalue = $('#signatureimgpath').val();
				$.ajax({
					url: base_url+'signaturepad.php',
					data: {img_data:img_data,deletepathvalue:deletepathvalue},
					type: 'post',
					success: function (response) {
						if(response == 'Success') {
							signaturePad.clear();
							$('#signatureimgpath').val('');
							$('#signaturepaddelete').hide();
							$('#signaturepadsave,#signaturepadclear').show();
							alertpopup('Image has been deleted from system.');
						} else {
							alertpopup('There is something problem. Please try again.!');
						}
					}
				});
			}
		});
	}
	//* Signature Pad Code *//
	//* Live Image Capture *//
	if(softwareindustryid == 3) {
		$("#imgcapture").click(function() {
			$("#taglivedisplay").empty();
			$("#accountlogoattachdisplay").empty();
			$('#savecapturedimg').show();
			addscreencapture();
		});
		$("#imagecaptureclose").click(function(){
			$("#imagecaptureoverlay").fadeOut();
		});
		function addscreencapture() {
			// Web cam capture Add screen function
			var sayCheese = new SayCheese('#taglivedisplay', {video: true});
			sayCheese.start();
			$('#take-snapshot').click(function() {
				var width = 0, height = 0;
				sayCheese.takeSnapshot(width, height);				
				$("#savecapturedimg").show();
			});
			sayCheese.on('snapshot', function(snapshot){			  
				var img = document.createElement('img');
				$(img).on('load', function(){
					$("#accountlogoattachdisplay").empty();
					$('#accountlogoattachdisplay').prepend(img);
				});
				img.src = snapshot.toDataURL('image/png');
				$("#imgurldata").val(img.src);
			});
			$("#savecapturedimg").click(function() {
				var dataurlimg =  $("#imgurldata").val();
				setTimeout(function() {
					success(dataurlimg);
					$("#savecapturedimg").hide();	
				},10);
			});
			$("#resetcapturedimg").click(function() {		
				$("#imgurldata").val('');				
				$("#accountlogoattachdisplay").empty();
				$("#savecapturedimg").show();				
			});
		}
		{// Webcam add Function
			function success(imageData) {
				var url = base_url+'webcamcapture.php';
				var params = {image: imageData};
				$.post(url, params, function(data) {
					$("#accountlogoattachdisplay").val(data);
					$("#liveimage_path").val(data);
				});
			}
			{//image alter function
				function successdelete(imageData) {
					var url = base_url+'webcamcapture.php';
					var params = {image: imageData};
					$.post(url, params, function(data) {
						$(".snapshotcapturebtn").removeClass('hidedisplay');
						$("#imagecaptureoverlay").fadeOut();
						$("#resultbtnset").addClass('hidedisplay');
						$("#delnewimgname").val(data);
						$("#liveimage_path").val(data);
						savecapturedproimgbtn();
					});
				}
			}
		}
	}
	//* Live Image Capture *//
    chitaccounttypeid = 0;
    {// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{ // Account Name - Automatic CAPS
		if(softwareindustryid == 3 && accountfieldcaps == 1) {
			$('#name,#billingaddress,#primaryarea,#billingpincode,#billingcity,#accountshortname').css('text-transform','uppercase');
			$('#name').change(function() {
				var name = $('#name').val();
				$('#name').val(name.toUpperCase());
			});
			$('#billingaddress').change(function() {
				var address = $('#billingaddress').val();
				$('#billingaddress').val(address.toUpperCase());
			});
			$('#primaryarea').change(function() {
				var accarea = $('#primaryarea').val();
				$('#primaryarea').val(accarea.toUpperCase());
			});
			$('#billingpincode').change(function() {
				var pincode = $('#billingpincode').val();
				$('#billingpincode').val(pincode.toUpperCase());
			});
			$('#billingcity').change(function() {
				var city = $('#billingcity').val();
				$('#billingcity').val(city.toUpperCase());
			});
			$('#accountshortname').change(function() {
				var shortname = $('#accountshortname').val();
				$('#accountshortname').val(shortname.toUpperCase());
			});
		}
	}
	{// Clear the form - regenerate the number 
		$('#formclearicon').click(function() {
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");			
		});
	}	
	{// For touch
		fortabtouch = 0;
	}
	{// Close Add Screen
		var addcloseleadcreation =["closeaddform","acccreationview","acccreationformadd",""];
		addclose(addcloseleadcreation);
	}
	{// Toolbar click function Reload,Summary
		$("#cloneicon").click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$(".documentslogodownloadclsbtn").show();
				$("#processoverlay").show();
				accountsclonedatafetchfun(datarowid);
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				$("#primarydataid").val('');
				Materialize.updateTextFields();
				Operation = 0; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function() {
			refreshgrid();
			Materialize.updateTextFields();
		});
		$( window ).resize(function() {
			maingridresizeheightset('leadaccountsgrid');
		});
		$("#detailedviewicon").click(function(){
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				$("#processoverlay").show();
				froalaset(froalaarray);
				accountseditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','acccreationformadd');	
				$(".documentslogodownloadclsbtn").hide();
				//For tablet and mobile Tab section reset and enable tabgroup dd
				$('#tabgropdropdown').select2('val','1');
				$("#accountlogodivhid").addClass('hidedisplay');
				 //setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 //},50);
				 Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','acccreationformadd');
			$('#parentaccount').attr('disabled','disabled');
			$("#accountlogodivhid").removeClass('hidedisplay');
			$(".documentslogodownloadclsbtn").show();
			firstfieldfocus();
			if(softwareindustryid == 3){
				defaultdecimalvalidate();
			}
			Materialize.updateTextFields();
			//treemenu css fix
			//$('.dl-menuwrapper input').prop("disabled", false);
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var validemail=validateemailanddonotmail(datarowid,'account','emailid','donotmail');
				var datamail = validemail.split(',');
				if(datamail[0] == true) {
					if(datamail[1] == 'No'){
						var emailtosend = getvalidemailid(datarowid,'account','emailid','','');
						sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
						sessionStorage.setItem("forsubject",'');
						sessionStorage.setItem("moduleid",'202');
						sessionStorage.setItem("recordid",datarowid);
						var fullurl = 'erpmail/?_task=mail&_action=compose';
						window.open(''+base_url+''+fullurl+'');
					} else{
						alertpopup("Account has choosed the Do Not Mail Option. We cant sent mail to this account");
					}
				} else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Counter hierarchy
		$('#accountslistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var listid=$(this).attr('data-listid');
			var primaryid = $("#primarydataid").val();
			if(primaryid == listid) {
				alertpopup('You cant choose the same account as parent account');
			} else {
				$('#parentaccount').val(name);
				if(softwareindustryid == 3){
					$('#accountcatalogid').val(listid);
					if(listid == 17) {
						$("#accounttypeid").select2('val',18).trigger('change');
					} else if(listid == 19) {
						$("#accounttypeid").select2('val',19).trigger('change');
					}
					if(listid == 2) {
						$("#dutyrateiddivhid").show();
					} else {
						$("#dutyrateiddivhid,#taxapplytypeiddivhid,#taxratedivhid").hide();
						$("#taxrate").val('');
						$("#dutyrateid,#taxapplytypeid").select2('val','');
					}
				}else{
					$('#parentaccountid').val(listid);
				}
			}
		});	
	}
    {// View by drop down change
        $('#dynamicdddataview').change(function() {
            refreshgrid();
        });
    }
	{// Validation for account Add  
		$('#dataaddsbtn').click(function(e) {
			$('.ftab').trigger('click');
			Materialize.updateTextFields();
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				accountnewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Update account information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			//Materialize.updateTextFields();
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				accountdataupdateinformationfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
			loyatycheckboxaction(name);	
		});
	}
	{// Counter hierarchy
		$('#accountlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			$('#parentaccount').val(name);
			$('#parentaccountid').val(listid);
		});	
	}
	{// Redirected form Home
		add_fromredirect("accountaddsrc",91);
	}
	//Account-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique202");		
		if(uniquelreportsession != '' && uniquelreportsession != null){
			 setTimeout(function() {
				accountseditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','acccreationformadd');
				sessionStorage.removeItem("reportunique202");
				Materialize.updateTextFields();
			},50);
		}
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
	// print the chit entry
	$('#chitbookprinticon').click(function(){
		var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id'); 
		if(datarowid){
			chitbookprint();
		}else {
			alertpopup(selectrowalert);
		}
	});
	$('#chitbookhtmlprinticon').click(function(){
		var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id'); 
		var chitbookno = getgridcolvalue('chitbookgrid',datarowid,'chitbookno','');
		var templateid = getgridcolvalue('chitbookgrid',datarowid,'printtemplatesid','');
		if(datarowid){
			htmlprintbase(datarowid,templateid,chitbookno);
		}else {
			alertpopup(selectrowalert);
		}
	});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event		
		$('#dashboardicon').click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript(base_url+'js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				setTimeout(function() {
					$("#processoverlay").hide();
				},1000);
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					for(var i=0;i<data.length;i++) {
						if(data[i] != '') {
							mobilenumber.push(data[i]);
						}
					}
					if(mobilenumber.length > 1) {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").hide();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						mobilenumberload(mobilenumber,'smsmobilenumid');
					} else if(mobilenumber.length == 1) {
						sessionStorage.setItem("mobilenumber",mobilenumber);
						sessionStorage.setItem("viewfieldids",viewfieldids);
						sessionStorage.setItem("recordis",datarowid);
						window.location = base_url+'Sms';
					} else {
						alertpopup("Invalid mobile number");
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var validemail=validatecall(datarowid,'account','donotcall');
			if(validemail == 'No') {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++){
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1){
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					},
				});
			} else {
				alertpopup("This Account Choosed the Do Not Call Option. So You can't call to this account");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function() {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{ //address type change
		$("#billingaddresstype option[value='6']").remove();
		$("#shippingaddresstype option[value='5']").remove();
		$("#billingaddresstype,#shippingaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,leadaccountsgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1; 
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			leadaccountsgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	$("#firstmonthentry").mouseout(function() {
		if($("#firstmonthentry").val() == 0) {
			$("#chitcashamount").val('0');
			$("#chitcardamount").val('0');
			$("#chitchequeamount").val('0');
			$("#chitneftamount").val('0');
		}
	});
	// chit book
	$("#chitbookicon").click(function() {
		var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var accounttypename = $.trim(getgridcolvalue('leadaccountsgrid',datarowid,'accounttypename',''));
			if(accounttypename == 'Customer') {
				$('#chitbkhidid').val(datarowid);
				chitbookgrid();
				chitbookoverlaystatus = 1;
				addslideup('acccreationview','accountssettingview');
				$('.ftabchitbookoverlay,#tab1').addClass('active');
				getcurrentsytemdate("chitbookdate");
				if(jQuery.inArray( userroleid, chitauthrole )!= -1) {   //Userrole who has having permission to edit
					$("#chitbookdate").prop('disabled',false);
				} else {
					$("#chitbookdate").prop('disabled',true);
				}
				schemenameload('schemename');
				loadaccountname(datarowid);
				var btnid = ['addchitbookbtn','editchitbookbtn'];
				var btnsh = ['show','hide'];
				chitbookaddupdatebtnsh(btnid,btnsh);
				chitbookeditstatus = 0;
				Materialize.updateTextFields();
				$('#chitbookaddicon').trigger('click');
				$('#chitbookediticon,#chitbookdeleteicon,#chitbookprinticon,#chitbookhtmlprinticon').show();
				$('#memberslimit,#schemelvlactivecount,#schemependgcount,#recordsfound').val('');
			}else {
				alertpopup('Please select customer account only');
			}
		} else {
			alertpopup(selectrowalert);
		}
	});
	// chit book form validation
	$('#addchitbookbtn').click(function() { 
		$("#addchitbookvalidate").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#addchitbookvalidate").validationEngine( {
		onSuccess: function() {
			var chitlookupsid = $("#schemename").find("option:selected").data("chitlookupsid");
			var intrestmodeid = $("#schemename").find("option:selected").data("intrestmodeid");
			var variablemodeid = $("#schemename").find("option:selected").data("variablemodeid");
			if(chitlookupsid == 6) {
				/* if(intrestmodeid == 3) { 
					var bothschemeamount = $("#bothschemeamount").val();
					var amount = bothschemeamount/2;
				} else { */
					/* var amount = $("#bothschemeamount").val(); */
					if(variablemodeid == 1) {
						var amount = $("#bothschemeamount").val();
					} else {
						var amount = $("#schemeamount").val();
					}
				//}
			} else {
				if(variablemodeid == 1) {
					var amount = $("#bothschemeamount").val();
				} else {
					if(intrestmodeid == 2) {
						var schemeamount = $("#schemeamount").val();
						var amount = schemeamount/2;
					} else {
						var amount = $("#schemeamount").val();
					}
				}
			}
			var chitcashamount = $("#chitcashamount").val();
			var chitcardamount = $("#chitcardamount").val();
			var chitchequeamount = $("#chitchequeamount").val();
			var chitneftamount = $("#chitneftamount").val();
			var famount = parseFloat(chitcashamount)+parseFloat(chitcardamount)+parseFloat(chitchequeamount)+parseFloat(chitneftamount);
			if(famount == amount) {
				chitbookadd();
			} else if ($('#firstmonthentry').val() == '0'){
				$("#chitcashamount").val('0');
				$("#chitcardamount").val('0');
				$("#chitchequeamount").val('0');
				$("#chitneftamount").val('0');
				chitbookadd();
			} else {
				alertpopup('Please Enter your amount based on your chit book amount');
			}
		},
		onFailure: function() {
			var dropdownid =['3','schemename','schemeamount','firstmonthentry'];
			dropdownfailureerror(dropdownid);
		}
	});
	var dateformetdata = $('#chitbookdate').attr('data-dateformater');
	$('#chitbookdate').datetimepicker({
		dateFormat: dateformetdata,
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,
		maxDate:0,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#chitbookdate').focus();
		}
	});
	// scheme name change 
	$('#schemename').change(function(){
		var schemeid = $(this).val();
		var chitlookupsid = $(this).find("option:selected").data("chitlookupsid");
		var variablemodeid = $(this).find("option:selected").data("variablemodeid");
		$("#firstmonthentry").val('');
		$("#schemecount").val('');
		$("#schemeamtcount").val('');
		$("#giftcredits").val('');
		$('#firstmonthentry').val(1); 
		$('#schemeamount').select2('val','');
		if(variablemodeid == 1) {
			$("#schemeamountdivhid").addClass('hidedisplay');
			$("#bothschemeamountdivhid").removeClass('hidedisplay');
			$("#schemeamount").removeClass('validate[required]');
			$("#bothschemeamount").addClass('validate[required,custom[number]]');
		} else {
			$("#schemeamountdivhid").removeClass('hidedisplay');
			$("#bothschemeamountdivhid").addClass('hidedisplay');
			$("#schemeamount").addClass('validate[required]');
			$("#bothschemeamount").removeClass('validate[required,custom[number]]');
		}
		var datarowid = $('#chitbookaccountid').val();
		if(chitbookeditstatus == 0){
			loadaccountname(datarowid);
		}
		schemenamechange(schemeid,'schemeamount','noofmonths','chitbookschemebaseamount','amountprefix','chit_token_no');
		chitbooknoget(schemeid);
		$('#memberslimit,#schemelvlactivecount,#schemependgcount,#recordsfound').val('');
		schemeactivecount(schemeid);
		Materialize.updateTextFields();
	});
	// scheme amount select2
	$('#schemeamount').change(function(){
		var chitbookschemeamount = $.trim($(this).val()); 
		var chitschemebaseamount=$("#chitbookschemebaseamount").val();
		if(chitschemebaseamount == 0) {
			var chitcredits= 0;
		}else{
			var chitcredits=(chitbookschemeamount/chitschemebaseamount).toFixed(0); 
		}
		$("#giftcredits").val(chitcredits);
		var scheme_id=$('#schemename').val();
		chitbookamountcount(scheme_id,chitbookschemeamount);
		var prefixvalue = $("#schemeamount").find('option:selected').data('amountprefix');  
		$("#schemeamtprefix").val(prefixvalue);
		var intrestmodeid = $('#schemename').find("option:selected").data("intrestmodeid");
		if(intrestmodeid == 2) {
			$("#chitcashamount").val(chitbookschemeamount/2);
			$("#chitinterestamount").val(chitbookschemeamount/2);
			$("#chitcardamount").val(0);
			$("#chitchequeamount").val(0);
		} else {
			$("#chitcashamount").val(chitbookschemeamount);
			$("#chitcardamount,#chitchequeamount,#chitinterestamount,#chitneftamount").val(0);
		}
		Materialize.updateTextFields();
	});
	//for amount chnage
	$("#chitcashamount,#chitcardamount,#chitchequeamount,#chitneftamount").change(function() {
		setzero(['chitcashamount','chitcardamount','chitchequeamount','chitinterestamount','chitneftamount']);
		var chitlookupsid = $('#schemename').find("option:selected").data("chitlookupsid");
		var intrestmodeid = $('#schemename').find("option:selected").data("intrestmodeid");
		if(chitlookupsid == 6) {
			/* if(intrestmodeid == 3) {
				var bothschemeamount =$("#bothschemeamount").val();
				var amount = bothschemeamount/2;
			} else { */
				var amount = $("#bothschemeamount").val();	
			//}
		} else {
			if(intrestmodeid == 2) { // type b
				var schemeamount =$("#schemeamount").val();
				var amount = schemeamount/2;
			} else {
				var amount = $("#schemeamount").val();	
			}
		}
		var chitcashamount = $("#chitcashamount").val();
		var chitcardamount = $("#chitcardamount").val();
		var chitchequeamount = $("#chitchequeamount").val();
		var chitneftamount = $("#chitneftamount").val();
		var famount = parseFloat(chitcashamount)+parseFloat(chitcardamount)+parseFloat(chitchequeamount)+parseFloat(chitneftamount);
		if(famount > amount) {
			$("#chitcashamount,#chitcardamount,#chitchequeamount,#chitneftamount").val('');
			alertpopup('Please enter the amount based on your chit book');
		}
	});
	//for both concept
	$("#bothschemeamount").change(function() {
		var amount = $("#bothschemeamount").val();
		/* var maxschemeamount = $("#schemename").find("option:selected").data("maxschemeamount");
		if(amount > maxschemeamount) {
			$("#bothschemeamount").val('');
			alertpopup('Please enter the amount lessthan '+maxschemeamount+'');
		} else { */
			$("#chitcashamount").val(amount);
			$("#chitcardamount").val(0);
			$("#chitchequeamount").val(0);
			$("#chitinterestamount").val(0);
		/* } */
	});
	//chit book Add
	$("#chitbookaddicon").click(function(){
		sectionpanelheight('chitbooksectionoverlay');
		$("#chitbooksectionoverlay").removeClass("closed");
		$("#chitbooksectionoverlay").addClass("effectbox");
		Materialize.updateTextFields();
		firstfieldfocus();
	});
	$("#chitbooksectionclose").click(function(){
		$("#chitbooksectionoverlay").removeClass("effectbox");
		$("#chitbooksectionoverlay").addClass("closed");
	});
	
	//stone setting Add
	$("#stonesettingaddicon").click(function(){
		sectionpanelheight('stonesettingsectionoverlay');
		$("#stonesettingsectionoverlay").removeClass("closed");
		$("#stonesettingsectionoverlay").addClass("effectbox");
		Materialize.updateTextFields();
		firstfieldfocus();
	});
	$("#stonesettingsectionclose").click(function(){
		$("#stonesettingsectionoverlay").removeClass("effectbox");
		$("#stonesettingsectionoverlay").addClass("closed");
	});
	//chit book Edit
	$("#chitbookediticon").click(function(){
		showhideiconsfun('summryclick','chitbookform');	
        var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			var btnid = ['addchitbookbtn','editchitbookbtn'];
			var btnsh = ['hide','show'];
			chitbookaddupdatebtnsh(btnid,btnsh);
			chitbookretrive(datarowid);
			$('#chitbookediticon,#chitbookdeleteicon,#chitbookprinticon,#chitbookhtmlprinticon').hide();
			chitbookeditstatus = 1;
		} else {
			alertpopup(selectrowalert);
		}
	});
	$('#editchitbookbtn').click(function() { 
		chitbookupdate();
	});
	//delete chitbook
	$("#chitbookdeleteicon").click(function(){		 	
		var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$("#updatechitbookid").val(datarowid);
			$.ajax({
				url: base_url + "Chitbookentry/chitbookdata?primaryid="+datarowid,
				cache:false,
				dataType:'json',
				success: function(msg) {  
					if(msg == '' || msg == "") {
						combainedmoduledeletealert('chitbookdeleteyes','Are you sure,you want to delete this chitbook?');
						var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id');
						if(CHITBOOKDELETE == 0) {
							$("#chitbookdeleteyes").click(function(){
								var id = $("#updatechitbookid").val();			
								chitbookdelete(id);
							});	
						}
						CHITBOOKDELETE = 1;
					} else {
						combainedmoduledeletealert('chitbookdeleteyes','Receipt count:'+msg.receiptcount+' Collection amount:'+msg.collectionamount+' Are you sure,you want to delete this chitbook?');
						var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id');
						if(CHITBOOKDELETE == 0) {
							$("#chitbookdeleteyes").click(function(){
								var id = $("#updatechitbookid").val();								
								chitbookdelete(id);
							});	
						}
						CHITBOOKDELETE = 1;
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	//chitbook Reload
	$("#chitbookrefreshicon").click(function(){
		resetFields();
		chtibookrefreshgrid();
		setTimeout(function(){
			 getcurrentsytemdate("chitbookdate");
			 var accountname = $('#chit_account_name').val();
			 $('#chitbookaccountname').val(accountname);
			 $('#chitbookname').val(accountname);
			 $('#printtemplate').select2('val','YES');
			 Materialize.updateTextFields();
          },100);		  			
		$('#schemename,#schemeamount,#printtemplate,#firstmonthentry,#addchitbookbtn,#chitbookaccountid,#chitbookschemebaseamount,#chitschemename,#chitschemeprefix,#chitbookprimaryname,#chitbkhidid,#schemeamtprefix,#chitbookdate,#noofmonths,#chittoken,#schemeamtcount,#giftcredits').attr('disabled',false);
		$('#editchitbookbtn').hide();
		$('#addchitbookbtn').show();
		
	});
	// close click
	$('#chitbookoverlayclose').click(function() {
		$('#memberslimit,#schemelvlactivecount,#schemependgcount,#recordsfound').val('');
		resetFields();
		if(chitbookoverlaystatus == 1){
			addslideup('accountssettingview','acccreationview');
			refreshgrid();
			chitbookoverlaystatus = 0;
		}
	});
	$('#stonesettingoverlayclose').click(function() {
		resetFields();
		if(stonesettingoverlaystatus == 1){
			addslideup('stonesettingsview','acccreationview');
			$('#purity').attr('disabled',false);
			$('#addstonesettingbtn,#stoneedit,#stonedeleteicon').show();
			$('#updatestonesettingbtn').hide();
			stonesettingoverlaystatus = 0;
		}
	});
	$("#stonesettingicon").click(function(){
		var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id'); 
		if(datarowid) {
			var accounttypename = $.trim(getgridcolvalue('leadaccountsgrid',datarowid,'accounttypename',''));
			if(accounttypename == 'Customer' || accounttypename == 'Vendor') {
				stonesettingoverlaystatus = 1;
				addslideup('acccreationview','stonesettingsview');
				$('.ftabstonesettingoverlay,#tab1').addClass('active');
				$('#stoneaccountid').val(datarowid);
				stonesettinggrid();
			}else {
				alertpopup('Please select either customer or vendor account only');
			}
		} else {
			alertpopup(selectrowalert);
		}	
	});
	// stone setting form validation
	$('#addstonesettingbtn').click(function() { 
		$("#addstonesettingvalidate").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
    jQuery("#addstonesettingvalidate").validationEngine( {
		onSuccess: function() {
			stonesettingadd();
		},
		onFailure: function() {
			var dropdownid =['2','purity','stonecalc'];
			dropdownfailureerror(dropdownid);
		}
	});
	//stone edit
	$("#stoneedit").click(function() {
		var datarowid = $('#stonesettinggrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			var btnid = ['addstonesettingbtn','updatestonesettingbtn'];
			var btnsh = ['hide','show'];
			stoneaddupdatebtnsh(btnid,btnsh);
			$('#stonedeleteicon,#stoneedit').hide();
			stoneretrive(datarowid);
		} else {
			alertpopup(selectrowalert);
		}
	});
	//update scheme information
	$('#updatestonesettingbtn').click(function() {
		$("#editstonesettingvalidate").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
    jQuery("#editstonesettingvalidate").validationEngine({
		onSuccess: function() {			
			stonesettingupdate();
		},
		onFailure: function() {
			var dropdownid =['2','purity','stonecalc'];
			dropdownfailureerror(dropdownid);
		}	
	});
	//delete stone setting
	$("#stonedeleteicon").click(function(){		 	
		var datarowid = $('#stonesettinggrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$("#updatestonesettingid").val(datarowid);combainedmoduledeletealert('stonesettingdeleteyes','Are you sure,you want to delete this stone setting?');
			if(STONESETTINGDELETE == 0) {
				$("#stonesettingdeleteyes").click(function(){ 				
					var datarowid = $("#updatestonesettingid").val();																		
					stonesettingdelete(datarowid);
				});
				STONESETTINGDELETE = 1;
			}
		} else {
			alertpopup(selectrowalert);
		}
	});
	//Account Group
	$("#accountgroupicon").click(function(){
		window.location =base_url+'Accountgroup';
	});
	$(".parentclear").click(function(){
		$("#parentaccountid").val(1);
	});
	//key-press ,add events
	$('#chitbookname,#firstmonthentry').keypress(function(e) {
		if (e.which == 13) {
			$('#addchitbookbtn').trigger('click');
		}
	});
	if(softwareindustryid == 3){  // jewel login
		$('#accounttypeid').change(function() { // remove mandatory for accounttype  - other 
			var accountypeid =  $(this).val();
			$("span","#mobilenumberdivhid label").remove();
			if(accountypeid == 10) {
				$('#mobilenumber').removeClass('validate[required,custom[phone],maxSize[100],funcCall[mobilenumbercheck]] error');
				$('#mobilenumber').addClass('validate[custom[phone],maxSize[100],funcCall[mobilenumbercheck]]');
				$('.mobilenumberformError').hide();
				$("span","#mobilenumberdivhid label").remove();
				$('#accountcatalogid').select2('val','').trigger('change');
			} else {
				if(accountmobilevalidation == 'YES') {
					$('#mobilenumber').addClass('validate[required,custom[phone],maxSize[100],funcCall[mobilenumbercheck]]');
					$('#mobilenumberdivhid label').append('<span class="mandatoryfildclass">*</span>');
				} else {
					$('#mobilenumber').addClass('validate[custom[phone],maxSize[100],funcCall[mobilenumbercheck]]');
					$('#mobilenumberdivhid label').append('<span class="mandatoryfildclass"></span>');
				}
				$('#accountcatalogid').select2('val','').trigger('change');
			}
			if(accountypeid == '6') {
				$('#loyaltycarddivhid,#loyaltyiddivhid,#dateofbirthdivhid,#religioniddivhid,#userchittypeiddivhid').removeClass('hidedisplay');
				$('#accountcatalogid').select2('val',33).trigger('change');
				//$('#parentaccountiddivhid').removeClass('large-12 medium-12 small-12 columns').addClass('large-6 medium-6 small-6 columns');
			} else if(accountypeid == '19') {
				$('#loyaltycarddivhid,#loyaltyiddivhid,#dateofbirthdivhid,#religioniddivhid,#userchittypeiddivhid').addClass('hidedisplay');
				$('#parentaccountiddivhid').removeClass('large-6 medium-6 small-6 columns').addClass('large-12 medium-12 small-12 columns');
				$('#accountcatalogid').select2('val',19).trigger('change');
				$('#userchittypeid').select2('val','');
			} else if(accountypeid == '18') {
				$('#loyaltycarddivhid,#loyaltyiddivhid,#dateofbirthdivhid,#religioniddivhid,#userchittypeiddivhid').addClass('hidedisplay');
				$('#parentaccountiddivhid').removeClass('large-6 medium-6 small-6 columns').addClass('large-12 medium-12 small-12 columns');
				$('#accountcatalogid').select2('val',17).trigger('change');
				$('#userchittypeid').select2('val','');
			} else if(accountypeid == '16') {
				$('#loyaltycarddivhid,#loyaltyiddivhid,#dateofbirthdivhid,#religioniddivhid,#userchittypeiddivhid').addClass('hidedisplay');
				$('#parentaccountiddivhid').removeClass('large-6 medium-6 small-6 columns').addClass('large-12 medium-12 small-12 columns');
				$('#accountcatalogid').select2('val',27).trigger('change');
				$('#userchittypeid').select2('val','');
			} else{
				$('#loyaltycarddivhid,#loyaltyiddivhid,#dateofbirthdivhid,#religioniddivhid,#userchittypeiddivhid').addClass('hidedisplay');
				$('#parentaccountiddivhid').removeClass('large-6 medium-6 small-6 columns').addClass('large-12 medium-12 small-12 columns');
				$('#accountcatalogid').select2('val','').trigger('change');
				$('#userchittypeid').select2('val','');
			}
		});
		$('#userchittypeid').change(function() { // remove mandatory for employee field
			var userchittypeid =  $(this).val();
			if(userchittypeid == 3) {
				$('#employeeiddivhid').removeClass('hidedisplay');
				$("#employeeid").addClass('validate[required]');
			} else {
				$('#employeeiddivhid').addClass('hidedisplay');
				$("#employeeid").removeClass('validate[required]');
				$("#employeeid").select2('val','');
			}
		});
	}
	{// Redirected form notification overlay and widget and audit log
		var rdatarowid = sessionStorage.getItem("datarowid");		
		if(rdatarowid != '' && rdatarowid != null){
			 setTimeout(function() {
				accountseditdatafetchfun(rdatarowid);
				showhideiconsfun('summryclick','acccreationformadd');
				sessionStorage.removeItem("datarowid");
				Materialize.updateTextFields();
			},50);
		}
	}
	{//gst work - gowtham
		if($("#gstapplicableid").val() != 1) { // if gst is not enabled in company setting
			$("#tab2").hide();
		} else {// if gst is  enabled  in company setting
			$("#tab2").show();
		}
		$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid").hide();
		$("#accountgstdetailscboxid").click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid").show();
			} else {
				$('#'+name+'').val('No');
				$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid").hide();
				$("#gsthsnsaccode,#gsthsnsacdescription,#gstnnumner").val('');
				$("#taxmasterid,#supplytypeid").select2('val','');
			}
		});
	}
	{//hide id based on acount group
		$("#dutyrateid").click(function() {
			var dutyrateid = $("#dutyrateid").val();
			if(dutyrateid == 2) {
				$("#taxapplytypeiddivhid,#taxratedivhid").show();
				$('#taxrate').addClass('validate[custom[number],min[0.1],max[100],decval[2]]');
			} else {
				$('#taxrate').removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
				$("#taxapplytypeiddivhid,#taxratedivhid").hide();
				$("#taxapplytypeid").select2('val','');
			}
		});
	}
	$("#openingbalance").keypress(function(e){
		 var keyCode = e.which;
		 var start = e.target.selectionStart;
		 var end = e.target.selectionEnd;
		 if($(this).val().substring(start, end) == '') {
			 // Not allow special 
			  if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
				  var newValue = this.value;
					if(keyCode != 8 && keyCode != 0){
						if (hasDecimalPlace(newValue, amount_round)) {
							e.preventDefault();
						}
					}		
			 }else{
				 e.preventDefault();
			 }
		 }else{
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
			}else{
				e.preventDefault();
			}
		 }
	});
	$("#openinggram").keypress(function(e){
		 var keyCode = e.which;
		 var start = e.target.selectionStart;
		 var end = e.target.selectionEnd;
		 if($(this).val().substring(start, end) == '') {
			 // Not allow special 
			  if ( ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
				  var newValue = this.value;
					if(keyCode != 8 && keyCode != 0){
						if (hasDecimalPlace(newValue, weight_round)) {
							e.preventDefault();
						}
					}		
			 }else{
				 e.preventDefault();
			 }
		 }else{
			if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0 || keyCode == 45)) {
			}else{
				e.preventDefault();
			}
		 }
	});
	$('#openingbalance,#openinggram').change(function(){
		 var textVal = $(this).val();
		if (textVal.indexOf('-') != -1) {
			if(textVal.match(/\-/g).length > 1)
			{
				alertpopup('you cannot enter - more than once');
				$(this).val(0);
				return false;
			}
		}
	});
	$(document).on( "click", ".removeliveimagedata", function() {
		$(this).closest("#taglivedisplay").empty();
		$("#liveimage_path").val('');
	});	
});
{// View create success function
    function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main view Grid
	function leadaccountsgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		if(Operation == 1){
			var rowcount = $('#accountspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#accountspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		}
		var wwidth = $("#leadaccountsgridwidth").width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		if(userviewid == 129) {
			var maintabinfo ='chitbook';
			var primaryid = 'chitbookid';
		} else {
			var maintabinfo ='account';
			var primaryid = 'accountid';
		}
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo="+maintabinfo+"&primaryid="+primaryid+"&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#leadaccountsgrid').empty();
				$('#leadaccountsgrid').append(data.content);
				$('#leadaccountsgridfooter').empty();
				$('#leadaccountsgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('leadaccountsgrid');
				{//sorting
					$('.accountsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.accountsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortpos = $('#leadaccountsgrid .gridcontent').scrollLeft();
						leadaccountsgrid(page,rowcount);
						$('#leadaccountsgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						var sortcol = $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.accountsheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						$("#processoverlay").hide();
					});
					sortordertypereset('accountsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						leadaccountsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#accountspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						leadaccountsgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var accountideditvalue = sessionStorage.getItem("accountidforedit");
					if(accountideditvalue != null) {
						setTimeout(function() {
						accountseditdatafetchfun(accountideditvalue);
						sessionStorage.removeItem("accountidforedit");
						},100);
					}
				}
				//Material select
				$('#accountspgrowcount').material_select();
			},
		});
	}
	{//refresh grid
		function refreshgrid() {
			var page = $('ul#accountspgnum li.active').data('pagenum');
			var rowcount = $('ul#accountspgnumcnt li .page-text .active').data('rowcount');
			leadaccountsgrid(page,rowcount);
		}
	}
}
{//chit book grid
	function chitbookgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $('#chitbookgrid').width();
		var wheight = $('#chitbookgrid').height();
		//col sort
		var sortcol = $("#chitbooksortcolumn").val();
		var sortord = $("#chitbooksortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.chitbookheadercolsort').hasClass('datasort') ? $('.chitbookheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.chitbookheadercolsort').hasClass('datasort') ? $('.chitbookheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.chitbookheadercolsort').hasClass('datasort') ? $('.chitbookheadercolsort.datasort').attr('id') : '0';
		var chitaccountid = $('#chitbkhidid').val() ? $('#chitbkhidid').val() : '1';
		var filterid ='|1746';
		var conditionname = '|IN';
		var filtervalue = '|'+chitaccountid;
		var userviewid =129;
		var viewfieldids =153;
		var footername = 'chitbookfooter';
		var cuscondfieldsname='account.accountid';
		var cuscondvalues=$('#chitbkhidid').val();
		var conditionoperator='IN';
		var cuscondtablenames="";
		var cusjointableid='';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitbook&primaryid=chitbookid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+'&footername='+footername+"&cuscondfieldsname="+cuscondfieldsname+"&cuscondvalues="+cuscondvalues+"&cuscondtablenames="+cuscondtablenames+"&cusjointableid="+cusjointableid+"&conditionoperator="+conditionoperator,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#chitbookgrid').empty();
				$('#chitbookgrid').append(data.content);
				$('#chitbookgridfooter').empty();
				$('#chitbookgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('chitbookgrid');
				{//sorting
					$('.chitbookheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.chitbookheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#accountspgnumcnt li .page-text .active').data('rowcount');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.chitbookheadercolsort').hasClass('datasort') ? $('.chitbookheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.chitbookheadercolsort').hasClass('datasort') ? $('.chitbookheadercolsort.datasort').data('sortorder') : '';
						$("#chitbooksortorder").val(sortord);
						$("#chitbooksortcolumn").val(sortcol);
						chitbookgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('chitbookheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						chitbookgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#chitbookfooterpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						chitbookgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#chitbookfooterpgrowcount').material_select();
			},
		});
	 }
	{//refresh grid
		function chtibookrefreshgrid() {
			var page = $('ul#accountspgnum li.active').data('pagenum');
			var rowcount = $('ul#accountspgnumcnt li .page-text .active').data('rowcount');
			chitbookgrid(page,rowcount);
		}
	}
}
{//stone settings grid
	function stonesettinggrid(page,rowcount) {
		var accointid=$('#stoneaccountid').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var wwidth = $('#stonesettinggrid').width();
		var wheight = $('#stonesettinggrid').height();
		/*col sort*/
		var sortcol = $("#stonesettingsortcolumn").val();
		var sortord = $("#stonesettingsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.stonesettingheadercolsort').hasClass('datasort') ? $('.stonesettingheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.stonesettingheadercolsort').hasClass('datasort') ? $('.stonesettingheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.stonesettingheadercolsort').hasClass('datasort') ? $('.stonesettingheadercolsort.datasort').attr('id') : '0';	
		var footername = 'stonesetting';
		var filterid =accointid;
		var conditionname = '';
		var filtervalue = '';
		$.ajax({
			url:base_url+"Account/gridinformationfetch?maintabinfo=stonesetting&primaryid=stonesettingid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=91&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#stonesettinggrid').empty();
				$('#stonesettinggrid').append(data.content);
				$('#stonesettinggridfooter').empty();
				$('#stonesettinggridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('stonesettinggrid');
				{//sorting
					$('.stonesettingheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.stonesettingheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#stonesettinggridpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.stonesettingheadercolsort').hasClass('datasort') ? $('.stonesettingheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.stonesettingheadercolsort').hasClass('datasort') ? $('.stonesettingheadercolsort.datasort').data('sortorder') : '';
						$("#stonesettingsortorder").val(sortord);
						$("#stonesettingsortcolumn").val(sortcol);
						stonesettinggrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('stonesettingheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						stonesettinggrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#stonesettingpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						stonesettinggrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				$('#stonesettingpgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(){
			addslideup('acccreationview','acccreationformadd'); 
			$(".off-canvas-wrap").css({'padding-left':'0px'});
			//$('#slide-out').css('display','none'); 
			var treevalidation = $('#treevalidationcheck').val();
			var treelabel = $('#treefiledlabelcheck').val();
			resetFields();
			$('#treevalidationcheck').val(treevalidation);
			$('#treefiledlabelcheck').val(treelabel);
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','acccreationformadd');
			$("#accountlogodivhid").removeClass('hidedisplay');
			$(".documentslogodownloadclsbtn").show();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber 
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			$('#accountlogoattachdisplay').empty();
			$('#stateid').select2('val','2');
			$('#countryid').select2('val','2');
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			$("#primarydataid").val('');
			$('#accounttypeid').prop("disabled", false);
			fortabtouch = 0;
			autogenmaterialupdate();
			//setTimeout(function(){
				//form field first focus
				firstfieldfocus();
			//},100);
			$('#parentaccount').attr('disabled','disabled');
			//treemenu css fix
			//$('.dl-menuwrapper input').prop("disabled", false);
			$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid,#dutyrateiddivhid,#taxapplytypeiddivhid,#taxratedivhid").hide();
			if(softwareindustryid == 3) {
				branchhideshow('companyid','companyiddivhid',$('#companyidval').val());
				branchhideshow('branchid','branchiddivhid',$('#branchidval').val());
				$("#loyaltyid").prop("disabled",true);
				defaultdecimalvalidate();
				$('#accountcatalogid').select2('val','33');
				dropdownnooption('stonechargeid','stonechargeiddivhid',$('#stonechargeid').val());
				$('#accountcatalogid').select2('val',28).trigger('change');
				creditinfohideshow();
				loademployeename();
				$('#userchittypeid').trigger('change');
				$('#signaturepadclear').trigger('click');
		    }
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
			Materialize.updateTextFields();
			
		});
		$("#editicon").click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				dropdownnooption('stonechargeid','stonechargeiddivhid',$('#stonechargeid').val());
				accountseditdatafetchfun(datarowid);
				if(softwareindustryid == 3){
					defaultdecimalvalidate();
					creditinfohideshow();
					loademployeename();
					$('#userchittypeid').trigger('change');
					$('#accounttypeid').trigger('change');
				}
				$(".documentslogodownloadclsbtn").show();
				$('#parentaccount').attr('disabled','disabled');
				$("#accountlogodivhid").removeClass('hidedisplay');
				firstfieldfocus();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Materialize.updateTextFields();
				//treemenu css fix
				//$('.dl-menuwrapper input').prop("disabled", false);
				$('#accounttypeid').prop("disabled", true);
				Operation = 1; //for pagination
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#leadaccountsgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$.ajax({ // checking whether this value is already in usage.
						url: base_url + "Base/multilevelmapping?level="+2+"&datarowid="+datarowid+"&table=chitbook&fieldid=accountid&table1=cardcommission&fieldid1=accountid&table2=''&fieldid2=''",
						success: function(msg) { 
							if(msg > 0){
								alertpopup("This account already mapped either in chitbook or card commission.So unable to delete this one");				
							}else{
								$('#primarydataid').val(datarowid);
								$("#basedeleteoverlay").fadeIn();
								$("#basedeleteyes").focus();
							}
						},
					});
			} else {
				alertpopup("Please select a row");
			}	
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			accountrecorddelete(datarowid);
		});
	}
}
{// New data add submit function
	function accountnewdataaddfun()	{
		
		var parent = $("#parentaccountid").val();
		if(!parent){
			$("#parentaccountid").val(1);
		}
		var companyid = $("#companyid").val();
		if(companyid == 1) {
			$("#companyid").select2('val',2);
		}
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		chitaccounttypeid = $("#accounttypeid").val();
		var signatureimgpath = $('#signatureimgpath').val();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Account/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				var result_data = nmsg.split(",");
				if (result_data[0] == 'TRUE') {
					$(".ftab").trigger('click');
					var treevalidation = $('#treevalidationcheck').val();
					var treelabel = $('#treefiledlabelcheck').val();
					resetFields();
					$('#treevalidationcheck').val(treevalidation);
					$('#treefiledlabelcheck').val(treelabel);
					$('#acccreationformadd').hide();
					if(chitemployeetypeid == 4 && chitaccounttypeid == 6) {
						$("#processoverlay").hide();
						$('#chitbkhidid').val(result_data[1]);
						chitbookgrid();
						chitbookoverlaystatus = 1;
						addslideup('acccreationformadd','accountssettingview');
						$('.ftabchitbookoverlay').addClass('active');
						getcurrentsytemdate("chitbookdate");
						if(jQuery.inArray( userroleid, chitauthrole )!= -1) {   //Userrole who has having permission to edit
							$("#chitbookdate").prop('disabled',false);
						} else {
							$("#chitbookdate").prop('disabled',true);
						}
						schemenameload('schemename');
						loadaccountname(result_data[1]);
						var btnid = ['addchitbookbtn','editchitbookbtn'];
						var btnsh = ['show','hide'];
						chitbookaddupdatebtnsh(btnid,btnsh);
						chitbookeditstatus = 0;
						Materialize.updateTextFields();
						$('#chitbookaddicon').trigger('click');
						$('#chitbookediticon,#chitbookdeleteicon,#chitbookprinticon,#chitbookhtmlprinticon').show();
						$('#memberslimit,#schemelvlactivecount,#schemependgcount,#recordsfound').val('');
					} else {
						$('#acccreationview').fadeIn(1000);
						//$("#slide-out").removeClass('hidedisplay');
						$(".off-canvas-wrap").css({'padding-left':'65px'});
						refreshgrid();
						alertpopup(savealert);
						//For Keyboard Shortcut Variables
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();
					}
				}
			},
		});
	}
}
{// Old information show in form
	function accountaccountdataupdateinformationfun(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Account/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn(1000);
					$("#processoverlay").hide();
					refreshgrid();
				} else if((data.fail) == 'sndefault'){
					alertpopup('Cannot Edit/Delete default records');
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn(1000);
					$("#processoverlay").hide();
				} else {
					showhideiconsfun('editclick','acccreationformadd');
					addslideup('acccreationview','acccreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					primaryaddvalfetch(datarowid);
					if(softwareindustryid != 3){
						parentaccountnamefetch(datarowid);
					}
					//attachment
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					editordatafetch(froalaarray,data);
					loyatycheckboxaction('loyaltycard');
					$("#processoverlay").hide();
					if(data.accountgroupid == 1){
						$('#accountgroupiddivhid').addClass('hidedisplay');
					}else{
						$('#accountgroupiddivhid').removeClass('hidedisplay');
						$("#accountgroupid").prop("disabled", true);
					}
					if(data.accountgstdetails == 'Yes') {
						$('#accountgstdetails').val('Yes');
						$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid").show();
					} else {
						$('#accountgstdetails').val('No');
						$("#gsthsnsaccodedivhid,#gsthsnsacdescriptiondivhid,#taxmasteriddivhid,#supplytypeiddivhid,#gstnnumnerdivhid").hide();
					}
					if(softwareindustryid == 3){
						referralnamehideshow(data.primarydataid);
						branchhideshow('companyid','companyiddivhid',data.companyid);
						branchhideshow('branchid','branchiddivhid',data.branchid);
						$('#accounttypeid').select2('val',data.accounttypeid);
						if(data.dutyrateid == 2) {
							
						}else{
							$('#taxrate').removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
						}
						if(data.accountcatalogid == 2) {
							$("#dutyrateiddivhid").show();
							if(data.dutyrateid == 2) {
								$("#taxapplytypeiddivhid,#taxratedivhid").show();
								$('#taxrate').addClass('validate[custom[number],min[0.1],max[100],decval[2]]');
							} else {
								$('#taxrate').removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
								$("#taxapplytypeiddivhid,#taxratedivhid").hide();
								$("#taxapplytypeid").select2('val','');
							}
						}else{
							$("#dutyrateiddivhid,#taxapplytypeiddivhid,#taxratedivhid").hide();
						}
						if(data.signaturepad_path != '') {
							var w = document.getElementById("signature-pad"),
								c = w.querySelector("canvas"),
								b = document.getElementById("replace");
							var signaturePad = new SignaturePad(w);
							signaturePad.clear();
							signaturePad.fromDataURL(data.signaturepad_path);
							$('#signatureimgpath').val(data.signaturepad_path);
							$('#signaturepaddelete').show();
							$('#signaturepadsave,#signaturepadclear').hide();
						} else {
							var w = document.getElementById("signature-pad"),
								c = w.querySelector("canvas"),
								b = document.getElementById("replace");
							var signaturePad = new SignaturePad(w);
							signaturePad.clear();
						}
						if(data.liveimage_path != '') {
							var sayCheese = new SayCheese('#taglivedisplay', {video: false});
							$("#taglivedisplay").empty();
							var closeicon = $('<i class="material-icons removeliveimagedata">close</i>');
							var img = $('<img id="companylogodynamic" style="height:91%;width:100%">');
							img.attr('src', data.liveimage_path);
							img.appendTo('#taglivedisplay');
							closeicon.appendTo('#taglivedisplay');
						}
					}
				}
			}
		});	
	}
}
{// Parent account name value fetch
	function parentaccountnamefetch(pid) {
		$.ajax({
			url:base_url+"Account/parentaccountnamefetch?id="+pid,
			dataType:'json',
			async:false,
			cache:false,
			success:function (data)	{
				$('#parentaccountid').val(data.parentcategoryid);
				$('#parentaccount').val(data.categoryname);
			}
		});
	}
}
{// Primary address value fetch function 
	function primaryaddvalfetch(datarowid)	{
		$.ajax({
			url:base_url+"Account/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var datanames = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var textboxname = ['12','billingaddresstype','billingaddress','billingpincode','billingcity','billingstate','billingcountry','shippingaddresstype','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry'];
				var dropdowns = ['2','billingaddresstype','shippingaddresstype'];
				textboxsetvalue(textboxname,datanames,data,dropdowns);
				if(data.parent_accountid != 0) {
					$("#parentaccountid").val(data.parent_accountid);
					$('#s2id_parentaccountid a span:first').text(data.parent_accountname);
				} else {
					$('#s2id_parentaccountid a span:first').text('');
				}
			}
		});
	}
}
{// Update old information
	function accountdataupdateinformationfun() {
		var companyid = $("#companyid").val();
		if(companyid == 1) {
			$("#companyid").select2('val',2);
		}
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditordataget(editorname);
		var signatureimgpath = $('#signatureimgpath').val();
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Account/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$(".ftab").trigger('click');
					$('#acccreationformadd').hide();
					$('#acccreationview').fadeIn();
					refreshgrid();
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Record delete function
	function accountrecorddelete(datarowid)	{
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Account/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Deleted successfully');
					$("#processoverlay").hide();
				} else if (nmsg == "Denied") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
					$("#processoverlay").hide();
				} else if(nmsg == "CASHCOUNTER") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('This account is liknked in cash counter. So you cant delte this account');
					$("#processoverlay").hide();
				} else if(nmsg == "sndefault") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Cannot Edit/Delete default records');
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Edit Function 
	function accountseditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		$('#accountlogoattachdisplay').empty();
		resetFields();
		accountaccountdataupdateinformationfun(datarowid);
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function 
	function accountsclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		$('#accountlogoattachdisplay').empty();
		accountaccountdataupdateinformationfun(datarowid);
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
function accountshortnamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#accountshortname").val();
	var fieldname = 'accountshortname';
	var elementpartable = $('#elementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		}
	}
}
function mobilenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#mobilenumber").val();
	var fieldname = 'mobilenumber';
	var elementpartable = $('#elementspartabname').val();
	if(accountmobileunique == '1') {
		if( accname != "" ) {
			$.ajax({
				url:base_url+"Base/uniqueshortdynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						return "Mobile number already exists!";
					}
				}else {
					return "Mobile number already exists!";
				}
			}
		}
	}
}
//set address
function setaddress(id) {
	var value=$('#'+id+'').val();
	if(id == 'billingaddresstype') {
		var type='billing';
	} else {
		var type='shipping';
	}
	if(value == 5){
		$('#billingaddress').val($('#shippingaddress').val());
		$('#billingpincode').val($('#shippingpincode').val());
		$('#billingcity').val($('#shippingcity').val());
		$('#billingstate').val($('#shippingstate').val());
		$('#billingcountry').val($('#shippingcountry').val());	
	} else if(value == 6){
		$('#shippingaddress').val($('#billingaddress').val());
		$('#shippingpincode').val($('#billingpincode').val());
		$('#shippingcity').val($('#billingcity').val());
		$('#shippingstate').val($('#billingstate').val());
		$('#shippingcountry').val($('#billingcountry').val());	
	}
	Materialize.updateTextFields();
}
{// Validate the module email and do not call
	function validateemailanddonotmail(id,table,field,donotmail) {
		var status='';
		$.ajax({
			url: base_url + "Base/checkvalidemailanddonotmail?id="+id+"&table="+table+"&field="+field+"&donotmail="+donotmail,
			async:false,
			cache:false,
			success: function(msg) {
				status =msg;
			},
		});
		return status;
	}
	//validate do not call
	function validatecall(id,table,field) {
		var status='';
		$.ajax({
			url: base_url + "Base/validatecall?id="+id+"&table="+table+"&field="+field,
			async:false,
			cache:false,
			success: function(msg)  {
				status =msg;
			},
		});
		return status;
	}
	function loadaccountname(datarowid){
		$.ajax({
			url: base_url + "Account/accountname?primaryid="+datarowid,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(msg) {
				var accountname=msg.accountname;
				$('#chitbookaccountname').val(accountname);
				$('#chitbookname').val(accountname);
				$('#chit_account_name').val(accountname);
				$('#chitbookaccountid').val(datarowid);
				if(chitbookeditstatus == 1){
					$("#chitbookname").focus();
				} else {
					$('#firstmonthentry').val(1);
					$('#printtemplate').select2('val','YES');
					$("#schemename").select2('focus');
				}
			},
		});
	}
	// chit book add/update button show or hide
	function chitbookaddupdatebtnsh(btnid,btnsh) {
		for(var m=0;m < btnid.length;m++) {
			if(btnsh[m] == 'show') {
				$('#'+btnid[m]+'').show();
				$('#'+btnid[m]+'').attr('data-shstatus','show');
			} else if(btnsh[m] == 'hide') {
				$('#'+btnid[m]+'').hide();
				$('#'+btnid[m]+'').attr('data-shstatus','hide');
			}
		}
	}
	function chitbookadd() {
		var formdata = $("#chitbookform").serialize();
		var chitschemetypeid = $('#chitlookupsid').val();
		var variablemodeid = $("#schemename").find("option:selected").data("variablemodeid");
		var purityid = $('#purityid').val();
		var chitcashamount = $('#chitcashamount').val();
		var chitcardamount = $('#chitcardamount').val();
		var chitchequeamount = $('#chitchequeamount').val();
		var chitneftamount = $('#chitneftamount').val();
		if(chitschemetypeid == 3){ // scheme is grams
			var ratestatus = checkrateforpurity(purityid);
			if(ratestatus == 'false'){
				alertpopup('Please add rate to purity for this scheme');
				return false;
			}else{
				
			}
		}
		var amp = '&';
		var datainformation = amp + formdata;
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax( {
			url: base_url +"Account/chitbookcreate",
			data: "datas=" + datainformation+amp+chitcashamount+amp+chitcardamount+amp+chitchequeamount+amp+chitneftamount+"&variablemodeid="+variablemodeid,
			type: "POST",
			success: function(msg) { 
				var nmsg =  $.trim(msg);
				chtibookrefreshgrid();
				if (nmsg == 'SUCCESS'){
					alertpopup(savealert);
					$('#processoverlay').hide();
					$("#schemename").trigger('change');
					$("#chitcashamount,#chitcardamount,#chitchequeamount,#chitinterestamount,#chitneftamount,#bothschemeamount").val(0);
					$("#schemeamount").select2('focus');
				} else {
					$('#processoverlay').hide();
					alertpopup('error');
				}
			},
		});
	}
	//chit bookno generate
	function chitbooknoget(scheme_name) {
		var sheme_id=scheme_name;
		$.ajax({
			url:base_url+"Account/chitbooknoget?primaryid="+sheme_id, 
			dataType:'json',
			async:false,
			success: function(data) {
				$('#chitbookno').val(data.chitbookno);
				$('#chittoken').val(data.lasttokenno);
				$('#schemecount').val(parseInt(checknan(data.chitschemelevelcount))+1);
				$('#chitschemename').val(data.schemename);
				$('#chitschemeprefix').val(data.schemeprefix);
				$('#chitlookupsid').val(data.chitlookupsid);
				$('#purityid').val(data.purityid);
			}
		});	
	}
	// Scheme level active chitbook details
	function schemeactivecount(scheme_name) {
		var sheme_id=scheme_name;
		$.ajax({
			url:base_url+"Account/schemeactivecount?primaryid="+sheme_id, 
			dataType:'json',
			async:false,
			success: function(data) {
				$('#memberslimit').val(data.memberslimit);
				$('#schemelvlactivecount').val(data.schemelvlactivecount);
				$('#schemependgcount').val(data.schemependgcount);
				$('#recordsfound').val(data.recordsfound);
				$pendingcount = $('#schemependgcount').val();
				$recordsfound = $('#recordsfound').val();
				if(chitbookeditstatus == 0) {
					if($recordsfound == 'yes') {
						if($pendingcount == '0') {
							$('#schemename').select2('val','').trigger('change');
							alertpopup('Reached maximum level of Memebers in this Scheme.');
						}
					}
				}
			}
		});
	}
	// chitbook retrive
	function chitbookretrive(datarowid) {
		var elementsname=['7','schemename','noofmonths','chitbookname','firstmonthentry','chitbookno','giftcredits','chittoken'];
		$.ajax({
			url:base_url+"Account/chitbookretrive?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			success: function(data) {
				var txtboxname = elementsname;
				var dropdowns = ['3','schemename','firstmonthentry','chitbooktokenpattern'];
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				chitbookeditstatus=1;
				$('#chitbookname,#editchitbookbtn').attr('disabled',false);
				$('#schemename').select2('val',data.schemeid).trigger('change');
				var chitlookupsid = $('#schemename').find("option:selected").data("chitlookupsid");
				var variablemodeid = $('#schemename').find("option:selected").data("variablemodeid");
				if(chitlookupsid == 6) {
					//$('#bothschemeamount').val(data.amount);
					if(variablemodeid == 1) {
						$('#bothschemeamount').val(data.amount).trigger('change');
					} else {
						$('#schemeamount').select2('val',data.amount).trigger('change');
					}
				} else {
					if(variablemodeid == 1) {
						$('#bothschemeamount').val(data.amount).trigger('change');
					} else {
						$('#schemeamount').select2('val',data.amount).trigger('change');
					}
				}
				setTimeout(function() {
					$('#firstmonthentry').val(data.firstmonthentry);
					if(data.firstmonthentry == 0) {
						$("#chitcashamount").val('0');
						$("#chitcardamount").val('0');
						$("#chitchequeamount").val('0');
						$("#chitneftamount").val('0');
					}
				},50);
				$("#updatechitbookid").val(datarowid);
				Materialize.updateTextFields();
			}
		});
	}
	// chitbook update
	function chitbookupdate() {
		var formdata = $("#chitbookform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid = $('#updatechitbookid').val();
		setTimeout(function() {
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url + "Account/chitbookupdate",
			data: "datas=" + datainformation +"&primaryid="+ datarowid,
			type: "POST",
			success: function(msg) {
				var nmsg =  $.trim(msg);
				chtibookrefreshgrid();
				resetFields();
				$('#schemename,#schemeamount,#printtemplate,#firstmonthentry,#addchitbookbtn,#chitbookaccountid,#chitbookschemebaseamount,#chitschemename,#chitschemeprefix,#chitbookprimaryname,#chitbkhidid,#schemeamtprefix,#chitbookdate,#noofmonths,#chittoken,#schemeamtcount,#giftcredits').attr('disabled',false);
				var accountid=$('#chitbookaccountid').val();
				getcurrentsytemdate("chitbookdate");
				schemenameload('schemename');
				chitbookeditstatus=0;
				loadaccountname(accountid);
				var btnid = ['addchitbookbtn','editchitbookbtn'];
				var btnsh = ['show','hide'];
				chitbookaddupdatebtnsh(btnid,btnsh);
				if (nmsg == 'SUCCESS') {				
					setTimeout(function() {
						$('#processoverlay').hide();
					},25);
					alertpopup(updatealert);
					Materialize.updateTextFields();
				} else {
					setTimeout(function(){
					$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
					Materialize.updateTextFields();
				}
				$('#chitbookname').val('');
				$('#chitbookediticon,#chitbookdeleteicon,#chitbookprinticon,#chitbookhtmlprinticon').show();
			},
		});
	}
	// chit book delete 
	function chitbookdelete(datarowid) {	
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url + "Account/chitbookdelete",
			data:{primaryid:datarowid},
			async:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				chtibookrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				if (nmsg == 'SUCCESS') {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopupdouble(deletealert);
				} else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopupdouble(submiterror);
				}
			},
		});
	}
	function accounttypeshowhide() {				
		//show/hide account type based
		var accounttypeid = $('#accounttypeid').val();
		$("#accountgroupid option").addClass("ddhidedisplay");
		$("#accountgroupid optgroup").addClass("ddhidedisplay");
		$("#accountgroupid option").prop('disabled',true);
		if(accounttypeid == "6"){ 
			$("#accountgroupid option[data-accountype='6']").removeClass("ddhidedisplay");
			$("#accountgroupid option[data-accountype='6']").closest('optgroup').removeClass("ddhidedisplay");
			$("#accountgroupid option[data-accountype='6']").prop('disabled',false);
		} else if(accounttypeid == "16"){
			$("#accountgroupid option[data-accountype='16']").removeClass("ddhidedisplay");
			$("#accountgroupid option[data-accountype='16']").closest('optgroup').removeClass("ddhidedisplay");	
			$("#accountgroupid option[data-accountype='16']").prop('disabled',false);
		}
		$("#accountgroupid option:enabled").closest('optgroup').removeClass("ddhidedisplay");
	}
  function stonesettingadd() {
		var formdata = $("#stonesettingform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var accountid =$('#stoneaccountid').val();
		var purity = $('#purity').val();  
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url +"Account/stonsettingcreate",
			data: "datas=" + datainformation+"&accountid="+accountid+"&purity="+purity,
			type: "POST",
			async:false,
			cache:false,
			success: function(msg) {  
				var nmsg =  $.trim(msg);
				if (nmsg == 'SUCCESS') {
					stonesettinggrid();
					setTimeout(function(){
					$('#processoverlay').hide();
					},25);
					resetFields();
					alertpopup(savealert);
				} else {
					$('#processoverlay').hide();
					alertpopup(submiterror);
				}
			},
		});
	}
	// stone retrive
	function stoneretrive(datarowid) {
		$.ajax({
			url:base_url+"Account/stonesettingretrive?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#stonesettingid').val(data.stonesettingid);
				//multiselect group value settings
				var purityids = data.purity;
				var dataarray = purityids.split(",");
				$('#purity').select2('val',dataarray).trigger('change');
				$('#purity').attr('disabled',true);
				$('#netwtcalctype').select2('val',data.netwtcalctype).trigger('change');
				$('#mccalctype').select2('val',data.mccalctype).trigger('change');
				$("#updatestonesettingid").val(datarowid);
				Materialize.updateTextFields();
			}
		});
	}
	//stone setting update
	function stonesettingupdate() {
		var formdata = $("#stonesettingform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var datarowid= $('#updatestonesettingid').val();
		var accountid =$('#stoneaccountid').val();
		var purity = $('#purity').val();
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url + "Account/stonesettingupdate",
			data: "datas=" + datainformation+"&accountid="+accountid+"&purity="+purity+"&primaryid="+datarowid,
			type: "POST",
			async:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				resetFields();
				$('#updatestonesettingbtn').hide();
				$('#addstonesettingbtn,#stoneedit,#stonedeleteicon').show();
				$('#purity').attr('disabled',false);
				if (nmsg == 'SUCCESS'){
					stonesettinggrid();
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(updatealert);
					Materialize.updateTextFields();
				} else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
					Materialize.updateTextFields();
				}            
			},
		});
	}
	function stonesettingdelete(datarowid) {
		setTimeout(function() {
			$('#processoverlay').show();
		},25);
		$.ajax({
			url: base_url + "Account/stonesettingdelete",
			data:{primaryid:datarowid},
			async:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				$("#basedeleteoverlayforcommodule").fadeOut();
				if (nmsg == 'SUCCESS') {
					stonesettinggrid();
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopupdouble(deletealert);
				} else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopupdouble(submiterror);
				}
			},
		});
	}
	// stone setting add/update button show or hide
	function stoneaddupdatebtnsh(btnid,btnsh) {
		for(var m=0;m < btnid.length;m++) {
			if(btnsh[m] == 'show') {
				$('#'+btnid[m]+'').show();
				$('#'+btnid[m]+'').attr('data-shstatus','show');
			} else if(btnsh[m] == 'hide') {
				$('#'+btnid[m]+'').hide();
				$('#'+btnid[m]+'').attr('data-shstatus','hide');
			}		
		}
	}
	// chit book amount getGridParam
	function chitbookamountcount(scheme_id,chitbookschemeamount) {
		var shemeid=scheme_id;
		var schemeamount=chitbookschemeamount;
		$.ajax({
			url:base_url+"Account/chitbookamountget", 
			dataType:'json',
			data:{primaryid:shemeid,schemeamount:schemeamount},
			async:false,
			success: function(data) { 	
				$('#schemeamtcount').val(parseInt(checknan(data.chitschemeamountlevelcount))+1);	
			}
		});	
	}
}
function autogenmaterialupdate(){
	if($('#accountnumber').val().length > 0){
		$('#accountnumber').siblings('label, i').addClass('active');
	}else{}
}
function chitbookprint() {
	var datarowid = $('#chitbookgrid div.gridcontent div.active').attr('id');
	var chitbookno=getgridcolvalue('chitbookgrid',datarowid,'chitbookno','');
	//setTimeout(function(){
		//$('#processoverlay').show();
	//},25);
	$.ajax({
		url: base_url + "Account/chitbookprint?primaryid="+datarowid+"&chitbookno="+chitbookno,
		dataType:'json',
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				//setTimeout(function(){
				//$('#processoverlay').hide();
				//},25);
				alertpopupdouble('Print Successfully');
			} else {
				//setTimeout(function(){
				//$('#processoverlay').hide();
				//},25);
				alertpopupdouble(submiterror);
			}
		},
	});
}
function checkrateforpurity(purityid){
	var status ='false';
	$.ajax({
		url: base_url + "Account/checkrateforpurity",
		dataType:'json',
		data:{purityid:purityid},
		async:false,
		type: "POST",
		success: function(msg) {
			if(msg  == 0){
				status ='false';
			}else{
				status ='true';
			}
		},
	});
	return status;
}
function referralnamehideshow(primarydataid){
	var parentaccountid = "parentaccountid";
	$("#"+parentaccountid+" option").removeClass("ddhidedisplay");
	$("#"+parentaccountid+" optgroup").removeClass("ddhidedisplay");
	$("#"+parentaccountid+" option").prop('disabled',false);
	$("#"+parentaccountid+" option[value="+primarydataid+"]").addClass("ddhidedisplay");
	$("#"+parentaccountid+" option[value="+primarydataid+"]").closest('optgroup').addClass("ddhidedisplay");
	$("#"+parentaccountid+" option[value="+primarydataid+"]").prop('disabled',true);
}
//loyalty checkbox 
function loyatycheckboxaction(name) {
	switch(name){				
		case 'loyaltycard':			
			if($('#'+name+'cboxid').is(':checked')){
				$("#loyaltyid").prop("disabled",false);
			} else {				
				$("#loyaltyid").prop("disabled",true);
				$("#loyaltyid").val('');
			}
		break;
	}
}
function branchhideshow(ddname,hidediv,data){ // company ,branch hide if it have only one data
  var i =0;
		var id = 0;
		$("span","#branchiddivhid label").remove();
		$("#"+ddname+" option[value]").each(function() {
			i++;
		});
		if(i == 1) {
			$("#"+ddname+"").select2('val',data);
			$("#"+hidediv+"").addClass('hidedisplay');
			if(ddname == 'branchid'){
				$("#"+ddname+"").removeClass('validate[required]');
			}
		}else{
			if(ddname == 'branchid'){
				$("#"+ddname+"").addClass('validate[required]');
				$('#branchiddivhid label').append('<span class="mandatoryfildclass">*</span>');
				$("#"+hidediv+"").removeClass('static-field large-6 medium-6 small-6 columns');
				$("#"+hidediv+"").addClass('static-field large-12 medium-12 small-12 columns');
				$("#"+ddname+"").select2('val',2);
			}
		}
}
// decimal validation
function defaultdecimalvalidate() {
	$('#openinggram').removeClass('validate[custom[number],decval[0],maxSize[15]] forsucesscls');
	$('#openingbalance').removeClass('validate[custom[number],decval[0],maxSize[10]] forsucesscls');
	$('#openingbalance').addClass('validate[decval['+amount_round+'],maxSize[10]] forsucesscls');
	$('#openinggram').addClass('validate[decval['+weight_round+'],maxSize[15]] forsucesscls');
	$('#gramcreditlimit').removeClass('validate[custom[number],decval[2],maxSize[15]] forsucesscls');
	$('#gramcreditlimit').addClass('validate[custom[number],decval['+weight_round+'],maxSize[15]] forsucesscls');
}
// Drop down has none option
function dropdownnooption(ddname,hidediv,data) {
	var i =0;
	$("#"+ddname+" option[value='1']").each(function() {
			i++;
	});
	if(i == 0) {
		$('#'+ddname+'').append($("<option value='1' data-"+ddname+"hidden='None'>None</option>"));
	}
}
// Credit Information HideShow based on Companysettings
function creditinfohideshow() {
	if(creditinfohideshowvar == 0) {
		$('#creditdaysdivhid,#gramcreditlimitdivhid,#creditlimitdivhid').addClass('hidedisplay');
	} else {
		$('#creditdaysdivhid,#gramcreditlimitdivhid,#creditlimitdivhid').removeClass('hidedisplay');
	}
}
function hasDecimalPlace(value, x) { // restrit decimal value based on weight round from company settings
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}
function printautomate(salesid,printtemplateid,snumber){
	$('#printpdfid').val(salesid);
	$('#printsnumber').val(snumber);		
	$('#pdftemplateprview').select2('val',printtemplateid).trigger('change');
	//$('#chitbookhtmlprinticon').trigger("click");
}
function htmlprintbase(id,templateid,snumber){
	var printtypeid = '';
	var printername = '';
	$.ajax({
		url:base_url+"Account/printtemplatedetails",
		data:{templateid:templateid},		
		type:'POST',
		async:false,
		cache:false,
		dataType:'json',
		success:function(data) {
			printtypeid = data.printtypeid;
			printername = data.printername;
		}
	});
	
	$.ajax({
		url:base_url+'Base/templatefilepdfpreview',
		data:{id:id,snumber:snumber,templateid:templateid},					
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'SUCCESS') {
				//alert(printtypeid);
				if(printtypeid == '3') { //html print-new tab
					var htmlurl = base_url+'billshtml/'+snumber+'.html';
					var w = window.open(htmlurl, '_blank');									
					//w.focus();
				}else if(printtypeid == '4') {
					var excelurl = base_url+'excel.php';
					var w = window.open(excelurl, '_blank');									
					//w.focus();
				}else if(printtypeid == '2') { //pdf
					var pdfurl = base_url+'printtemplate/receipt.pdf';
					var w = window.open(pdfurl, '_blank');									
					//w.focus();
				}
			} else if(printtypeid == '6' && msg != '') { //SILENT HTML
				$('#printingdiv').append(msg);
						printHtml();
			} else if(printtypeid == '5' && msg != '') { //EPSON JS
				epsonxmlprint(msg,printername)
			} else {
				alertpopup('Error on preview,try again!!!');
			}	 				
		},
	});
}
function loademployeename(){ // load employee name based chit type - Home collection
	$('#employeeid').empty();	
    $('#employeeid').append($("<option></option>"));
	var tablename = 'account';
	$.ajax({
        url: base_url + "Account/loademployeename",
		data:{table:tablename},
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg) {
			for(var i = 0; i < msg.length; i++) {
				 $('#employeeid')
				.append($("<option></option>")
				.attr('data-employeeidhidden',msg[i]['employeename'])
				.attr("value",msg[i]['employeeid'])
				.text(msg[i]['employeename']));
			} 
        },
    });
}