<script>
$(document).ready(function() { 
	{//Foundation Initialization
		$(document).foundation();
	}
    prizedesctoken();
	chitbooknotoken()
	prizeheadernametoken();
	{// Grid Calling Function
		chitamountgrid();
	}
	amount_round = $('#amount_round').val();
	chitinteresttype = $('#chitinteresttype').val();
	$('#groupcloseaddform').hide();
	$( window ).resize(function() {
		sectionpanelheight('schemeoverlay');
		sectionpanelheight('chitamountoverlay');
		sectionpanelheight('giftoverlay');
		sectionpanelheight('prizeentryoverlay');
	});
	$("#tab1").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
	});
	$("#tab2").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
	});
	$("#tab3").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
	});
	$("#tab4").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab4class").removeClass('hidedisplay');
	});
	{//inner-form-with-grid
		$("#chitamountadd").click(function(){
			sectionpanelheight('chitamountoverlay');
			$("#chitamountoverlay").removeClass("closed");
			$("#chitamountoverlay").addClass("effectbox");
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#chitamountcancel").click(function(){
			$("#chitamountoverlay").removeClass("effectbox");
			$("#chitamountoverlay").addClass("closed");
		});
		$("#schemeadd").click(function(){
			sectionpanelheight('schemeoverlay');
			resetFields();
			$("#schemeoverlay").removeClass("closed");
			$("#schemeoverlay").addClass("effectbox");
			getcurrentsytemdate("schemedate");
			getcurrentsytemdate("schemeapplyfromdate");
			getcurrentsytemdate("schemeapplytodate");
			loadchitamount();
			$('#chitype').select2('val',2).trigger('change');
			$('#luckydraw').select2('val','NO').trigger('change');
			$('#intrestmodeid').select2('val','');
			$("#updateschemeid").val('');
			$("#baseamount").val(0);
			$('#variablemodeid').select2('val','0').trigger('change');
			Materialize.updateTextFields();
			$('#schemename').focus();
		});
		$("#schemecancel").click(function(){
			$('#schemeupdate').hide();
			$('#schemecreate').show();
			$('#schemedelete,#schemeedit').show();
			$('#chitbooktokenpattern').attr('disabled',false);
			$('#chitype').attr('disabled',false);
			$("#schemeoverlay").removeClass("effectbox");
			$("#schemeoverlay").addClass("closed");
		});
		$("#giftadd").click(function(){
			sectionpanelheight('giftoverlay');
			resetFields();
			$("#giftoverlay").removeClass("closed");
			$("#giftoverlay").addClass("effectbox");
			getcurrentsytemdate("giftdate");
			$('.giftdatediv').addClass('hidedisplay');
			schemenameloadbasedongift('schemeid');
			$("span",".noofprizes label").remove();
			$('#schemeid,#amountgift,#typegift').prop('disabled',false);
			Materialize.updateTextFields();
			$('#schemeid').select2('focus');
		});
		$("#giftcancel").click(function(){
			$('#giftupdate').hide();
			$('#giftcreate').show();
			$('#giftdelete,#giftedit').show();
			$("#giftoverlay").removeClass("effectbox");
			$("#giftoverlay").addClass("closed");
		});
		$("#prizeentryadd").click(function(){
			sectionpanelheight('prizeentryoverlay');
			resetFields();
			$("#prizeentryoverlay").removeClass("closed");
			$("#prizeentryoverlay").addClass("effectbox");
			getcurrentsytemdate("prizeentrydate");
			prizeschemenameload();
			$('.prizeentrydatediv').addClass('hidedisplay');
			Materialize.updateTextFields();
			$('#prizeschemeid').select2('focus');
		});
		$("#prizeentrycancel").click(function(){
			$("#prizeentryoverlay").removeClass("effectbox");
			$("#prizeentryoverlay").addClass("closed");
		});
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	SCHEMEDELETE = 0;
	GIFTDELETE = 0;
	PRIZEENTRYDELETE = 0;
	{//first field focus
		$('#schemename').focus();
		firstfieldfocus();
	}
	//scheme validate
	//scheme create_function
	//validation for scheme Add  
	$('#schemecreate').click(function() {  
		$("#schemeaddwizard").validationEngine('validate');
	});
	$('#tab3').click(function() {
	   giftgrid();
	});
	jQuery("#schemeaddwizard").validationEngine({
		onSuccess: function() {
			schemeadd();
		},
		onFailure: function() {
			//var dropdownid =['5','chitype','luckydraw','gift','amount','purityid'];
			//dropdownfailureerror(dropdownid);
		}
	});
	// scheme name change 
	$('#schemeid').change(function() {
		var schemeid = $(this).val();
		$('#amountgift,#typegift').select2('val','');
		$('#prizeamount,#prizename,#noofprizes,#prizedescription').val('');
		schemenamechangenew(schemeid,'amountgift','schememonths','schemebaseamount','amountprefix','default','yes');
	});
	//Sheme Reload
	$("#schemerefresh").click(function(){
		resetFields();
		schemerefreshgrid();
		$('#schemeupdate').hide();
		$('#schemecreate').show();
		$('#schemedelete,#schemeedit').show();
		$('#chitbooktokenpattern').attr('disabled',false);
		setTimeout(function(){
			getcurrentsytemdate("schemedate");
			$('#chitype').select2('val',2).trigger('change');
			$('#luckydraw').select2('val','NO').trigger('change');
			$('#gift').select2('val','NO');
			$('#chitbooktokenpattern').select2('val','scheme token');
		},100);
		Materialize.updateTextFields();
	});		
	//Gift Reload
	$("#giftrefresh").click(function(){
		resetFields();
		giftrefreshgrid();
		$('#giftupdate').hide();
		$('#giftcreate').show();
		$('#giftdelete,#giftedit').show();
		setTimeout(function(){
			getcurrentsytemdate("giftdate");
		},100);
		Materialize.updateTextFields();
	});		
	//delete scheme detail
	$("#schemedelete").click(function(){			
		var datarowid = $('#schemegrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=chitbook&fieldid=chitschemeid&table1=''&fieldid1=''&table2=''&fieldid2=''",
				success: function(msg) {
					if(msg > 0) {
						alertpopup("This scheme already mapped in chitbook.So unable to delete this one");					
					} else {
						combainedmoduledeletealert('schemedeleteyes','Are you sure,you want to delete this scheme ?');
						if(SCHEMEDELETE == 0) {
							$("#schemedeleteyes").click(function() {
								var datarowid = $('#schemegrid div.gridcontent div.active').attr('id');
								if(typeof  datarowid != 'undefined') {	
									schemedelete(datarowid);
								}
							});
						}
						SCHEMEDELETE = 1;
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	// delete gift detail
	$("#giftdelete").click(function() {
		var datarowid = $('#giftgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			GIFTDELETE = 0;
			combainedmoduledeletealert('giftdeleteyes','Are you sure,you want to delete this gift ?');
			if(GIFTDELETE == 0) {
				$("#giftdeleteyes").click(function() {
					var datarowid = $('#giftgrid div.gridcontent div.active').attr('id');
					if(typeof  datarowid != 'undefined') {
						// Gift Or Prize - entries against respective module.
						$.ajax({
							url: base_url + "Schememaster/giftdeletecheck?datarowid="+datarowid,
							async:false,
							success: function(msg) {
								if(msg == "GIFT") {
									$("#basedeleteoverlayforcommodule").fadeOut();
									alertpopupdouble("Can't able to delete Gif/Prize Entry. Against Gift Entries used in Prize Issued.!");
									$(this).unbind();
								} else if(msg == "PRIZE") {
									$("#basedeleteoverlayforcommodule").fadeOut();
									alertpopupdouble("Can't able to delete Gif/Prize Entry. Against Prize Entries used in Prize Entry module.!");
									$(this).unbind();
								} else if(msg == "SUCCESS") {
									giftdelete(datarowid);
									$(this).unbind();
								} else {
									$("#basedeleteoverlayforcommodule").fadeOut();
									alertpopupdouble("There is something problem to delete. Please try again!");
									$(this).unbind();
								}
							},
						});
					}
				});
			}
			GIFTDELETE = 1;
		} else {
			alertpopup(selectrowalert);
		}
	});
	// gift form validation
	$('#giftcreate').click(function() { 
		$("#giftaddwizard").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#giftaddwizard").validationEngine({
		onSuccess: function() {
			giftadd();
			resetFields();
		},
		onFailure: function() {
			var dropdownid =['3','schemeid','amountgift','typegift'];
			dropdownfailureerror(dropdownid);
		}
	});
	//scheme edit
	$("#schemeedit").click(function() {
		var datarowid = $('#schemegrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var btnid = ['schemecreate','schemeupdate'];
			var btnsh = ['hide','show'];
			schemeaddupdatebtnsh(btnid,btnsh);
			$('#schemedelete,#schemeedit').hide();
			$('#chitype').attr('disabled',true);
			$('#chitbooktokenpattern').prop('disabled',true);
			loadchitamount();
			schemeretrive(datarowid);
			Materialize.updateTextFields();
		} else {
			alertpopup(selectrowalert);
		}
	});
	//gift edit
	$("#giftedit").click(function() {
		var datarowid = $('#giftgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$("#giftoverlay").removeClass("closed");
			$("#giftoverlay").addClass("effectbox");
			var btnid = ['giftcreate','giftupdate'];
			var btnsh = ['hide','show'];
			giftaddupdatebtnsh(btnid,btnsh);
			$('#giftdelete,#giftedit').hide();
			schemenameloadbasedongift('schemeid');
			$('.giftdatediv').addClass('hidedisplay');
			$('#schemeid,#amountgift,#typegift').prop('disabled',true);
			giftretrive(datarowid);
			Materialize.updateTextFields();
		} else {
			alertpopup(selectrowalert);
		}
	});
	//update scheme information
	$('#schemeupdate').click(function() {
		$("#schemeeditwizard").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#schemeeditwizard").validationEngine({
		onSuccess: function() {
			$('#schemename').attr('data-primaryid','');
			$('#prefixvalue').attr('data-primaryid','');
			schemeupdate();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});	
	// update gift information
	$('#giftupdate').click(function() {
		$("#gifteditwizard").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#gifteditwizard").validationEngine({
		onSuccess: function() {			
			giftupdate();
			resetFields();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}
	});	
	$('#chitype').change(function() {
		var chittype=$(this).val();
		if(chitinteresttype == '0') {
			$("label[for='interestrate']").html("Interest Rate(%)");
			$("label[for='refundrate']").html("Refund Rate(%)");
		} else if(chitinteresttype == '1') {
			$("label[for='interestrate']").html("Interest Flat Amt");
			$("label[for='refundrate']").html("Refund Flat Amt");
		}
		if(chittype == 2) {
			$('#purityiddivhid,#minnoofmonthsdivhid,#maxschemeamountdivhid').addClass('hidedisplay');
			$('#intrestmodeiddivhid,#interestratedivhid,#refundratedivhid').removeClass('hidedisplay');
			//$('#intrestmodeiddivhid,#amountdivhid').removeClass('hidedisplay');
			$('#purityid').removeClass('validate[required]');
			//$('#amount').addClass('validate[required]');
			$("#intrestmodeid option").addClass("ddhidedisplay").attr('disabled', true);
			$("#intrestmodeid option[data-interestorgram=1]").removeClass("ddhidedisplay").attr('disabled', false);
			$('#minnoofmonths').removeClass('validate[required,custom[onlyWholeNumber],min[1]]');
			if(chitinteresttype == '0') {
				$('#interestrate,#refundrate').addClass('validate[required,custom[number],min[1],max[100],decval[2]]');
			} else if(chitinteresttype == '1') {
				$('#interestrate,#refundrate').addClass('validate[required,custom[number],min[1],maxSize[10],decval[2]]');
			}
			$('#maxschemeamount').removeClass('validate[required,custom[integer]]');
			$('#minnoofmonths').val('0');
			//$('#interestrate').val('0');
			$('#purityid').select2('val','');
		} else if(chittype == 3) {
			//$('#purityiddivhid,#amountdivhid').removeClass('hidedisplay');
			$('#purityiddivhid,#intrestmodeiddivhid').removeClass('hidedisplay');
			$('#interestratedivhid,#minnoofmonthsdivhid,#maxschemeamountdivhid,#refundratedivhid').addClass('hidedisplay');
			$('#purityid').addClass('validate[required]');
			$('#minnoofmonths').removeClass('validate[required,custom[onlyWholeNumber],min[1]]');
			$('#maxschemeamount').removeClass('validate[required,custom[integer]]');
			$("#intrestmodeid option").addClass("ddhidedisplay").attr('disabled', true);
			$("#intrestmodeid option[data-interestorgram=2]").removeClass("ddhidedisplay").attr('disabled', false);
			if(chitinteresttype == '0') {
				$('#interestrate,#refundrate').removeClass('validate[required,custom[number],min[1],max[100],decval[2]]');
			} else if(chitinteresttype == '1') {
				$('#interestrate,#refundrate').removeClass('validate[required,custom[number],min[1],maxSize[10],decval[2]]');
			}
			//$('#amount').addClass('validate[required]');
			$('#minnoofmonths').val('0');
			$('#interestrate,#refundrate').val('0');
		} else if(chittype == 6) {
			$('#intrestmodeiddivhid,#maxschemeamountdivhid,#minnoofmonthsdivhid').addClass('hidedisplay');
			$('#purityiddivhid,#interestratedivhid,#refundratedivhid').removeClass('hidedisplay');
			//$('#purityiddivhid,#interestratedivhid,#amountdivhid').removeClass('hidedisplay');
			//$('#amount,#purityid').addClass('validate[required]');
			$('#purityid').addClass('validate[required]');
			$('#intrestmodeid').select2('val','');
			$('#minnoofmonths').removeClass('validate[required,custom[onlyWholeNumber],min[1]]');
			if(chitinteresttype == '0') {
				$('#interestrate,#refundrate').addClass('validate[required,custom[number],min[1],max[100],decval[2]]');
			} else if(chitinteresttype == '1') {
				$('#interestrate,#refundrate').addClass('validate[required,custom[number],min[1],maxSize[10],decval[2]]');
			}
			$('#maxschemeamount').removeClass('validate[required,custom[integer]]');
			$('#minnoofmonths').val('0');
		}
	});
	$('#variablemodeid').change(function() {
		var variablemodeid = $(this).val();
		if(variablemodeid == 0) {
			$('#amountdivhid').removeClass('hidedisplay');
			$('#averagemonthsdivhid').addClass('hidedisplay');
			$('#amount').addClass('validate[required]');
			$('#averagemonths').removeClass('validate[required,custom[onlyWholeNumber],min[1],maxSize[5]]');
			$('#averagemonths').val('0');
		} else {
			$('#amountdivhid').addClass('hidedisplay');
			$('#averagemonthsdivhid').removeClass('hidedisplay');
			$('#amount').removeClass('validate[required]');
			$('#averagemonths').addClass('validate[required,custom[onlyWholeNumber],min[1],maxSize[5]]');
			$('#amount').select2('val','');
		}
	});
	//for date picker
	$('#prizeentrydate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: 0,
		maxDate:0,
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != '') {
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			}
		},
		onClose: function () {
			$('#prizeentrydate').focus();
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	$('#tab4').click(function() {
		prizenetrygrid();
	});
	// scheme name change 
	$('#prizeschemeid').change(function() {
		var prizeschemeid = $(this).val();
		$('#prizeselectamount,#prizeheadername,#prizeentryname').select2('val','');
		$('#prizecount,#pendingentry,#tokenno,#chitbookno').val('');
		schemenamechangenew(prizeschemeid,'prizeselectamount','default','default','default','default','');
	});	
	$('#prizeselectamount').change(function() {
		var prizeselectamount = $(this).val();
		var schemeid = $('#prizeschemeid').find('option:selected').val();
		$('#prizeentryname').empty();
		$('#prizeheadername').select2('val','');
		$('#prizecount,#pendingentry,#tokenno,#chitbookno').val('');
		getprizename(schemeid,prizeselectamount);
	});
	// prize entry form validation
	$('#prizeentrycreate').click(function() {
		$("#prizeentryformaddwizard").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#prizeentryformaddwizard").validationEngine({
		onSuccess: function() {
			var pending=$('#pendingentry').val();
			if(parseInt(pending) < 1) {
				alertpopup('Completed prize entry for this Scheme!');
				resetFields();
				setTimeout(function() {
					getcurrentsytemdate("prizeentrydate");
				},100);
			} else {
				prizeentryadd();
			}
		},
		onFailure: function() {
			var dropdownid =['3','prizeschemeid','prizeselectamount','prizeentryname'];
			dropdownfailureerror(dropdownid);
		}
	});
	$('#tokenno').change(function() {
		var tokenno=$(this).val();
		var schemeid = $('#prizeschemeid').find('option:selected').val();
		var prizeid = $('#prizeentryname').find('option:selected').val();
		$('#chitbookno').val('');
		getchitbookno(schemeid,tokenno,prizeid);
		Materialize.updateTextFields();
	});
	$('#chitbookno').change(function() {
		$('#tokenno').val('');
	});
	$('#prizeentryname').change(function() {  
		var prizegiftid=$(this).val();
		$('#prizecount,#pendingentry,#tokenno,#chitbookno').val('');
		var schemeid = $('#prizeschemeid').val();
		var prizeselectamount = $('#prizeselectamount').val();
		var prizeheadername = $('#prizeheadername').val();
		getpendingentry(schemeid,prizegiftid,prizeselectamount,prizeheadername);
		Materialize.updateTextFields();
	});
	$('#prizenetryrefresh').click(function() {
		resetFields();
		setTimeout(function() {
			getcurrentsytemdate("prizeentrydate");
		},5);
		prizenetryrefreshgrid();
		Materialize.updateTextFields();
	});
	//delete gift detail
	$("#prizenetrydelete").click(function() {			
		var datarowid = $('#prizenetrygrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			combainedmoduledeletealert('prizeentrydeleteyes','Are you sure,you want to delete this prize entry?');
			if(PRIZEENTRYDELETE == 0) {		
				$("#prizeentrydeleteyes").click(function() {	
					var datarowid = $('#prizenetrygrid div.gridcontent div.active').attr('id');	
					if(typeof  datarowid != 'undefined') {		
						prizeentrydelete(datarowid);
					}
				});
			}
			PRIZEENTRYDELETE = 1;
		} else {
			alertpopup(selectrowalert);
		}
	});	
	$('#schemedate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: 0,
		maxDate:0,
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			}
		},
		onClose: function () {
			$('#schemedate').focus();
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	var dateschemeapplyfromdate = $('#schemeapplyfromdate').attr('data-dateformater');
	$('#schemeapplyfromdate').datetimepicker({
		dateFormat: dateschemeapplyfromdate,
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,
		maxDate:0,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#schemeapplyfromdate').focus();
		}
	});
	var dateschemeapplytodate = $('#schemeapplytodate').attr('data-dateformater');
	$('#schemeapplytodate').datetimepicker({
		dateFormat: dateschemeapplytodate,
		showTimepicker :false,
		minDate: 0,
		changeMonth: true,
		changeYear: true,
		maxDate: null,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#schemeapplytodate').focus();
		}
	});
	$('#tab2').click(function() {
		schemegrid();
	});	
	$('#giftdate').datetimepicker({
		dateFormat: 'dd-mm-yy',
		showTimepicker :false,
		minDate: 0,
		maxDate:0,
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			}
		},
		onClose: function () {
			$('#giftdate').focus();
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	{ // chit amount crud operations
		$('#chitamountcreate').click(function() {  
			$("#chitamountaddwizard").validationEngine('validate');
		});
		jQuery("#chitamountaddwizard").validationEngine({
			onSuccess: function() {
				chitamountadd();
				resetFields();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	$("#chitamountedit").click(function(){			
		var datarowid = $('#schemegrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			var btnid = ['chitamountcreate','chitamountupdate'];
			var btnsh = ['hide','show'];
			schemeaddupdatebtnsh(btnid,btnsh);
			$('#chitamountedit,#chitamountdelete').hide();
			chitamountretrive();
		} else {
			alertpopup(selectrowalert);
		}
	});
	//update chit amount information
	$('#chitamountupdate').click(function() {
		$("#chitamounteditwizard").validationEngine('validate');
		$(".mmvalidationnewclass .formError").addClass("errorwidth");
	});
	jQuery("#chitamounteditwizard").validationEngine({
		onSuccess: function() {
			chitamountupdate();
			resetFields();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
		}	
	});
	// delete chit amount detail
	$("#chitamountdelete").click(function() {
		var datarowid = $('#chitamountgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Schememaster/checkamountdata?datarowid="+datarowid,
				success: function(msg) { 
					if(msg > 0){
						alertpopup("This chitamount already mapped in chitscheme.So unable to delete this one");
					}else{
						combainedmoduledeletealert('chitamountdeleteyes','Are you sure,you want to delete this chit amount details?');
						$("#chitamountdeleteyes").click(function(){	
							var datarowid = $('#chitamountgrid div.gridcontent div.active').attr('id');	
							if(typeof  datarowid != 'undefined') {
								chitamountdelete(datarowid);
								$(this).unbind();
							}
						});
					}
				},
			});
		} else {
			alertpopup(selectrowalert);
		}
	});
	//chit amount Reload
	$("#chitamountrefresh").click(function() {
		resetFields();
		refreshgrid();
		$('#chitamountupdate').hide();
		$('#chitamountcreate').show();
		$('#chitamountdelete,#chitamountedit').show();
	});
	$('#typegift').change(function() {
		var value = $(this).val();
		if(value == 4) {
			$('.giftentrytemplateid,.noofprizes').addClass('hidedisplay');
			$('#noofprizes').removeClass('validate[required,custom[integer],maxSize[10],min[1]]');
			$("span",".noofprizes label").remove();
			$('#noofprizes').val(0);
		} else {
			$('#noofprizes').addClass('validate[required,custom[integer],maxSize[10],min[1]]');
			$('.giftentrytemplateid,.noofprizes').removeClass('hidedisplay');
			$('.noofprizes label').append('<span class="mandatoryfildclass">*</span>');
		}
	});
	// lucky draw change
	$("#luckydraw").change(function() {
		if($(this).val() == 'YES') {
			$("#chitbooktokenpattern").prop('disabled',false);
			$("#baseamountdivhid,#chitbooktokenpattern_div").removeClass('hidedisplay');
			$('#chitbooktokenpattern').addClass('validate[required]');
			$('#baseamount').addClass('validate[required,custom[onlyWholeNumber],maxSize[10],min[1]]');
		} else {
			$("#baseamountdivhid,#chitbooktokenpattern_div").addClass('hidedisplay');
			$("#baseamount").val('0');
			$("#chitbooktokenpattern").select2('val','');
			$('#chitbooktokenpattern').removeClass('validate[required]');
			$('#baseamount').removeClass('validate[required,custom[onlyWholeNumber],maxSize[10],min[1]]');
		}
	});
	// gift and prize - no of prizes change
	$("#noofprizes").change(function() {
		var value = $.trim($(this).val());
		var schemeid = $('#schemeid').find('option:selected').val();
		var amountgift = $('#amountgift').find('option:selected').val();
		checkprizeentry(value,schemeid,amountgift);
	});
	// mode change
	$('#intrestmodeid').change(function() {
		var value = $.trim($(this).val());
		var comments = $('#intrestmodeid').find('option:selected').attr('data-attr-value');
		alertpopup(comments);
		if(value == 3 || value == 2) {
			if(chitinteresttype == '0') {
				$('#interestrate').addClass('validate[required,custom[number],min[1],max[100],decval[2]]');
			} else if(chitinteresttype == '1') {
				$('#interestrate').addClass('validate[required,custom[number],min[1],maxSize[10],decval[2]]');
			}
			$('#interestratedivhid').removeClass('hidedisplay');
		} else if(value == 4) {
			$('#interestrate').val(0);
			if(chitinteresttype == '0') {
				$('#interestrate').removeClass('validate[required,custom[number],min[1],max[100],decval[2]]');
			} else if(chitinteresttype == '1') {
				$('#interestrate').removeClass('validate[required,custom[number],min[1],maxSize[10],decval[2]]');
			}
			$('#interestratedivhid').addClass('hidedisplay');
		}
	});
});
{ // scheme grid
	function schemegrid(page,rowcount) {
			var defrecview = $('#mainviewdefaultview').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
			var wwidth = $('#schemegrid').width();
			var wheight = $('#schemegrid').height();
			/*col sort*/
			var sortcol = $("#schemesortcolumn").val();
			var sortord = $("#schemesortorder").val();
			if(sortcol){
				sortcol = sortcol;
			} else{
				var sortcol = $('.schemeheadercolsort').hasClass('datasort') ? $('.schemeheadercolsort.datasort').data('sortcolname') : '';
			}
			if(sortord){
				sortord = sortord;
			} else{
				var sortord =  $('.schemeheadercolsort').hasClass('datasort') ? $('.schemeheadercolsort.datasort').data('sortorder') : '';
			}
			var headcolid = $('.schemeheadercolsort').hasClass('datasort') ? $('.schemeheadercolsort.datasort').attr('id') : '0';
			var filterid = '';
			var userviewid =124;
			var viewfieldids =49;
			var footername = 'schemefooter';
			$.ajax({
				url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitscheme&primaryid=chitschemeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$('#schemegrid').empty();
					$('#schemegrid').append(data.content);
					$('#schemegridfooter').empty();
					$('#schemegridfooter').append(data.footer);
					//data row select event
					datarowselectevt();
					//column resize
					columnresize('schemegrid');
					{//sorting
						$('.schemeheadercolsort').click(function(){
							$("#processoverlay").show();
							$('.schemeheadercolsort').removeClass('datasort');
							$(this).addClass('datasort');
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							var sortcol = $('.schemeheadercolsort').hasClass('datasort') ? $('.schemeheadercolsort.datasort').data('sortcolname') : '';
							var sortord =  $('.schemeheadercolsort').hasClass('datasort') ? $('.schemeheadercolsort.datasort').data('sortorder') : '';
							$("#schemesortorder").val(sortord);
							$("#schemesortcolumn").val(sortcol);
							schemegrid(page,rowcount);
							$("#processoverlay").hide();
						});
						sortordertypereset('schemeheadercolsort',headcolid,sortord);
					}
					{//pagination
						$('.pvpagnumclass').click(function(){
							$("#processoverlay").show();
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							schemegrid(page,rowcount);
							$("#processoverlay").hide();
						});
						$('#schemefooterpgrowcount').change(function(){
							$("#processoverlay").show();
							var rowcount = $(this).val();
							$('.pagerowcount').attr('data-rowcount',rowcount);
							$('.pagerowcount').text(rowcount);
							var page = $('#prev').data('pagenum');
							schemegrid(page,rowcount);
							$("#processoverlay").hide();
						});
					}
					//Material select
					$('#schemefooterpgrowcount').material_select();
				},
			});
		}
		{//refresh grid
			function schemerefreshgrid() {
				var page = $('ul#savingschemepgnum li.active').data('pagenum');
				var rowcount = $('ul#savingschemepgnumcnt li .page-text .active').data('rowcount');
				schemegrid(page,rowcount);
			}
		}
}
{//Scheme gift View Grid
	function giftgrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#giftgrid').width();
		var wheight = $('#giftgrid').height();
		/*col sort*/
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.giftheadercolsort').hasClass('datasort') ? $('.giftheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord){
			sortord = sortord;
		} else{
			var sortord =  $('.giftheadercolsort').hasClass('datasort') ? $('.giftheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.giftheadercolsort').hasClass('datasort') ? $('.giftheadercolsort.datasort').attr('id') : '0';
		var filterid = '';
		var userviewid =125;
		var viewfieldids =49;
		var footername = 'giftfooter';
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitgift&primaryid=chitgiftid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#giftgrid').empty();
				$('#giftgrid').append(data.content);
				$('#giftgridfooter').empty();
				$('#giftgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('giftgrid');
				{//sorting
					$('.giftheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.giftheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						var sortcol = $('.giftheadercolsort').hasClass('datasort') ? $('.giftheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.giftheadercolsort').hasClass('datasort') ? $('.giftheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						giftgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					sortordertypereset('giftheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						giftgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#giftfooterpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						giftgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#giftfooterpgrowcount').material_select();
				Materialize.updateTextFields();
			},
		});
	}
	{//refresh grid
		function giftrefreshgrid() {
			var page = $('ul#savingschemepgnum li.active').data('pagenum');
			var rowcount = $('ul#savingschemepgnumcnt li .page-text .active').data('rowcount');
			giftgrid(page,rowcount);
		}
	}
 }
{ // chit amount grid
	function chitamountgrid(page,rowcount) {
			var defrecview = $('#mainviewdefaultview').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
			var wwidth = $('#chitamountgrid').width();
			var wheight = $('#chitamountgrid').height();
			/*col sort*/
			var sortcol = $("#chitamountsortcolumn").val();
			var sortord = $("#chitamountsortorder").val();
			if(sortcol){
				sortcol = sortcol;
			} else{
				var sortcol = $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').data('sortcolname') : '';
			}
			if(sortord){
				sortord = sortord;
			} else{
				var sortord =  $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').data('sortorder') : '';
			}
			var headcolid = $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').attr('id') : '0';
			var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
			if(userviewid == 129) {
				var maintabinfo ='chitbook';
				var primaryid = 'chitbookid';
			} else {
				var maintabinfo ='account';
				var primaryid = 'accountid';
			}
			var headcolid = $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').attr('id') : '0';
			var filterid = '';
			var userviewid =122;
			var viewfieldids =49;
			var footername = 'chitamountfooter';
			$.ajax({
				url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=chitamount&primaryid=chitamountid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$('#chitamountgrid').empty();
					$('#chitamountgrid').append(data.content);
					$('#chitamountgridfooter').empty();
					$('#chitamountgridfooter').append(data.footer);
					//data row select event
					datarowselectevt();
					//column resize
					columnresize('chitamountgrid');
					{//sorting
						$('.chitamountheadercolsort').click(function(){
							$("#processoverlay").show();
							$('.chitamountheadercolsort').removeClass('datasort');
							$(this).addClass('datasort');
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							var sortcol = $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').data('sortcolname') : '';
							var sortord =  $('.chitamountheadercolsort').hasClass('datasort') ? $('.chitamountheadercolsort.datasort').data('sortorder') : '';
							$("#chitamountsortorder").val(sortord);
							$("#chitamountsortcolumn").val(sortcol);
							chitamountgrid(page,rowcount);
							$("#processoverlay").hide();
						});
						sortordertypereset('chitamountheadercolsort',headcolid,sortord);
					}
					{//pagination
						$('.pvpagnumclass').click(function(){
							$("#processoverlay").show();
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							chitamountgrid(page,rowcount);
							$("#processoverlay").hide();
						});
						$('#chitamountfooterpgrowcount').change(function(){
							$("#processoverlay").show();
							var rowcount = $(this).val();
							$('.pagerowcount').attr('data-rowcount',rowcount);
							$('.pagerowcount').text(rowcount);
							var page = $('#prev').data('pagenum');
							chitamountgrid(page,rowcount);
							$("#processoverlay").hide();
						});
					}
					//Material select
					$('#chitamountfooterpgrowcount').material_select();
					Materialize.updateTextFields();
				},
			});
		}
		{//refresh grid
			function refreshgrid() {
				var page = $('ul#savingschemepgnum li.active').data('pagenum');
				var rowcount = $('ul#savingschemepgnumcnt li .page-text .active').data('rowcount');
				chitamountgrid(page,rowcount);
			}
		}
}
{//CRUD Related functions
//scheme Create
function schemeadd() {
    var finalpattern = $('#chitbooknopattern').val().split(','); 
	var scheme_prefix='Scheme Prefix';
	var amount_suffix='Amount Suffix';
	var scheme_count='Scheme Count';
	if (jQuery.inArray(scheme_prefix, finalpattern)!='-1' && jQuery.inArray(amount_suffix, finalpattern)!='-1' && jQuery.inArray(scheme_count, finalpattern)!='-1'){
	} else {
		alertpopup('Please include scheme prefix,scheme count and amount suffix in chitbook no pattern');
		return false;
	}
	if($('#chitregtemplateid').val() == 0 || $.trim($('#chitregtemplateid').val()) == '') {
		var chitregdata='';
	} else {
		var chitregdata=$('#chitregtemplateid').find('option:selected').data('printtype');
	}
    if($('#chitentrytemplateid').val() == 0 || $.trim($('#chitentrytemplateid').val()) == '') {
		var chitentrydata='';
	} else {
		var chitentrydata=$('#chitentrytemplateid').find('option:selected').data('printtype');
	}
	var formdata = $("#schemeform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var data = $('#amount').select2('data');
	var finalResult = [];
	for(item in $('#amount').select2('data')) {
		finalResult.push(data[item].id);
	};
	var selectid = finalResult.join(',');
	$('#chitamounthide').val(selectid);
	var chitamount = $("#chitamounthide").val();
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url +"Schememaster/schemecreate",
        data: "datas=" + datainformation+"&chitreg="+chitregdata+"&chitentry="+chitentrydata+"&chitamount="+chitamount,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			schemerefreshgrid();
			if (nmsg == 'SUCCESS') {
				$('#processoverlay').hide();
				resetFields();
				setTimeout(function(){
					$('#schemename').focus();
					$('#chitype').select2('val',2).trigger('change');
					$('#luckydraw').select2('val','NO');
					$('#gift').select2('val','NO');
					getcurrentsytemdate("schemedate");
					$('#chitregtemplateid').select2('val',0).trigger('change');
					$('#chitentrytemplateid').select2('val',0).trigger('change');
					$('#chitamounthide').val('');
				},100);
				$('#schemecancel').trigger('click');
				alertpopup(savealert);
            }
			else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
        },
    });
}
// scheme add/update button show or hide
function schemeaddupdatebtnsh(btnid,btnsh)
{
	for(var m=0;m < btnid.length;m++)
	{
		if(btnsh[m] == 'show')
		{
			$('#'+btnid[m]+'').show();
			$('#'+btnid[m]+'').attr('data-shstatus','show');
		}
		else if(btnsh[m] == 'hide')
		{
			$('#'+btnid[m]+'').hide();
			$('#'+btnid[m]+'').attr('data-shstatus','hide');
		}
		
	}
}
// gift add/update button show or hide
function giftaddupdatebtnsh(btnid,btnsh)
{
	for(var m=0;m < btnid.length;m++)
	{
		if(btnsh[m] == 'show')
		{
			$('#'+btnid[m]+'').show();
			$('#'+btnid[m]+'').attr('data-shstatus','show');
		}
		else if(btnsh[m] == 'hide')
		{
			$('#'+btnid[m]+'').hide();
			$('#'+btnid[m]+'').attr('data-shstatus','hide');
		}
		
	}
}
//scheme detail update
function schemeupdate()
{
	var finalpattern = $('#chitbooknopattern').val().split(','); 
	var scheme_prefix='Scheme Prefix';
	var amount_suffix='Amount Suffix';
	var scheme_count='Scheme Count';
	if (jQuery.inArray(scheme_prefix, finalpattern)!='-1' && jQuery.inArray(amount_suffix, finalpattern)!='-1' && jQuery.inArray(scheme_count, finalpattern)!='-1'){
	}else{
		alertpopup('Please include scheme prefix,scheme count and amount suffix in chitbook no pattern');
		return false;
	}
	if($('#chitregtemplateid').val() == 0 || $.trim($('#chitregtemplateid').val()) == '')
	{
		var chitregdata='';
	}else { 
		var chitregdata=$('#chitregtemplateid').find('option:selected').data('printtype');
	}
    if($('#chitentrytemplateid').val() == 0 || $.trim($('#chitentrytemplateid').val()) == '')
	{
		var chitentrydata='';
	}
	else {
		var chitentrydata=$('#chitentrytemplateid').find('option:selected').data('printtype');
	}
	var formdata = $("#schemeform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid = $("#updateschemeid").val();
	var data = $('#amount').select2('data');
	var finalResult = [];
	for( item in $('#amount').select2('data') ) {
		finalResult.push(data[item].id);
	};
	var selectid = finalResult.join(',');
	$('#chitamounthide').val(selectid);
	var chitamount = $("#chitamounthide").val();
	var chitbooktokenpattern = $("#chitbooktokenpattern").val();
	var chitype = $("#chitype").val();
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/schemeupdate",
        data: "datas=" + datainformation +"&primaryid="+ datarowid+"&chitreg="+chitregdata+"&chitentry="+chitentrydata+"&chitamount="+chitamount+amp+"chitbooktokenpattern="+chitbooktokenpattern+"&chitype="+chitype,
		type: "POST",
		async:false,
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            resetFields();
            schemerefreshgrid();
			$('#schemeupdate').hide();
			$('#schemecreate').show();
			$('#schemedelete,#schemeedit').show();
			Materialize.updateTextFields();
			if (nmsg == 'SUCCESS'){
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				$('#schemecancel').trigger('click');
				alertpopup(updatealert);
				setTimeout(function(){
					getcurrentsytemdate("schemedate");
					$('#chitbooktokenpattern').attr('disabled',false);
					$('#chitype').attr('disabled',false);
					$('#chitregtemplateid').select2('val',0).trigger('change');
				    $('#chitentrytemplateid').select2('val',0).trigger('change');
					$('#chitamounthide').val('');
				},100);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
			}            
        },
    });
}
//gift detail update
function giftupdate()
{
	if($('#giftentrytemplateid').val() == 0)
	{
		var prizedata='';
	}else {
		var prizedata=$('#giftentrytemplateid').find('option:selected').data('printtype');
	}
	var schemeid = $('#schemeid').val();
	var amountgift = $('#amountgift').val();
	var typegift = $('#typegift').val();
	var formdata = $("#giftform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid = $('#giftpriceid').val();
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/giftupdate",
        data: "datas=" + datainformation +"&primaryid="+ datarowid+"&prizetemp="+prizedata+"&schemeid="+schemeid+"&amountgift="+amountgift+"&typegift="+typegift,
		type: "POST",
		async:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            resetFields();
            giftrefreshgrid();
			$('#giftupdate').hide();
			$('#giftcreate').show();
			$('#giftdelete,#giftedit').show();
			Materialize.updateTextFields();
			if (nmsg == 'SUCCESS'){
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				$('#giftcancel').trigger('click');
				alertpopup(updatealert);
				setTimeout(function(){
				    getcurrentsytemdate("giftdate");
	            },100);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
			}            
        },
    });
}

}
{// crud related function
// scheme gift create
function giftadd()
{
	if($('#giftentrytemplateid').val() == 0)
	{
		var prizedata='';
	}else {
		var prizedata=$('#giftentrytemplateid').find('option:selected').data('printtype');
	}
	var formdata = $("#giftform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax(
    {
        url: base_url +"Schememaster/schemegiftcreate",
        data: "datas=" + datainformation+"&prizetemp="+prizedata,
		type: "POST",
        success: function(msg)
        {
			var nmsg =  $.trim(msg);
			giftrefreshgrid();
			if (nmsg == 'SUCCESS'){
				$('#processoverlay').hide();
				$('#giftcancel').trigger('click');
			alertpopup(savealert);
			setTimeout(function(){
					getcurrentsytemdate("giftdate");
					$('#prizename,#noofprizes,#prizeamount,#prizedescription').val('');
					$('#prizeamount').focus();
					prizedesctoken();
			},100);
            }
			else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
        },
    });
}
}
// scheme retrive
	
	function schemeretrive(datarowid) {
	var elementsname=['22','schemedate','schemename','prefixvalue','chitype','purityid','noofmonths','luckydraw','baseamount','gift','chitbooktokenpattern','chitbooknopattern','amount','description','updateschemeid','chitschemeprimaryname','chitschemesecondaryname','chitregtemplateid','chitentrytemplateid','intrestmodeid','interestrate','minnoofmonths','maxschemeamount','accountid','schemeapplyfromdate','schemeapplytodate','memberslimit','variablemodeid'];
	$.ajax({
		url:base_url+"Schememaster/schemeretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data) {
			if(data.ischitbook == 0) {
				$("#schemeoverlay").removeClass("closed");
				$("#schemeoverlay").addClass("effectbox");
				var txtboxname = elementsname;
				var dropdowns = ['8','chitype','luckydraw','gift','chitbooktokenpattern','purityid','chitregtemplateid','chitentrytemplateid','intrestmodeid','accountid','variablemodeid'];
				textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
				var chitamounts = data.amount;
				chitamounts =chitamounts.toString();
				if (chitamounts.indexOf(',') > -1) {
					var dataarray = chitamounts.split(",");
					$('#amount').select2('val',dataarray).trigger('change');
				}else{
					$('#amount').select2('val',data.amount).trigger('change')
				}
				$("#chitype").trigger('change');	
				$("#luckydraw").trigger('change');
				$('#accountid').select2('val',data.accountid).trigger('change');
				$('#variablemodeid').select2('val',data.variablemodeid).trigger('change');
				$('#averagemonths').val(data.averagemonths);
				$('#memberslimit').val(data.memberslimit);
				$('#schemeapplyfromdate').val(data.schemeapplyfromdate);
				$('#schemeapplytodate').val(data.schemeapplytodate);
				if(data.chitype == 2) {
					$("#intrestmodeid").trigger('change');
				}
				var chitbooknopattern = data.chitbooknopattern;
				var datachitbooknopattern = chitbooknopattern.split(",");
				$('#chitbooknopattern').select2('val',datachitbooknopattern).trigger('change');
				var chitbooktokenpattern = data.chitbooktokennopattern; 
				$('#chitbooktokenpattern').select2('val',chitbooktokenpattern).trigger('change');
				$('#schemename').prop('data-primaryid',datarowid);
				$('#prefixvalue').prop('data-primaryid',datarowid);
				$("#updateschemeid").val(datarowid);
				$("#interestrate").val(data.interestrate);
				$("#refundrate").val(data.refundrate);
			}else{
				$('#schemedelete,#schemeedit').show();
				alertpopup('Already this scheme based chitbook created.So Unable to edit');
			}
		}
	});
}
// gift retrive
function giftretrive(datarowid) {   
      $('#amountgift').empty();	
      $('#amountgift').append($("<option></option>"));
	   	
	var elementsname=['giftdate','schemeid','amountgift','typegift','prizeamount','prizename','noofprizes','prizedescription'];
	$.ajax(
	{
		url:base_url+"Schememaster/giftretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			$('#schemeid').select2('val',data.schemeid).trigger('change');
			var dropdowns = ['schemeid','amountgift','typegift'];
			var txtboxname = elementsname;
			textboxsetvaluenew(txtboxname,txtboxname,data,dropdowns);
			$("#giftpriceid").val(datarowid);
			$('#typegift').trigger('change');
	   }
	});
}
//delete scheme information
function schemedelete(datarowid)
{	
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/schemedelete",
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			schemerefreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
		    if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
//delete gift information
function giftdelete(datarowid) {
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/giftdelete",
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			giftrefreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
		    if(nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
// prize description automatic tokenziation
function  prizedesctoken() {
	prizedesc = [];
	$.ajax({
		url: base_url + "Schememaster/prizedescload",
		async:false,
		cache:false,
		dataType:'json',
		success: function(data) {
			var i=0;
			$.each(data, function(index) {
				prizedesc[i]=data[index];
				i++;
			});
        },
    });
}
//chitbookno automatic tokenzation
function chitbooknotoken() {
	$("#chitbooknopattern").select2({
		tags: ['Scheme Prefix','Scheme Name','Amount Suffix','Amount','Scheme Count','Amount Count'],
		tokenSeparators: [',', ' '],
		tokenizer: function(input, selection, callback) {
			// no comma no need to tokenize
			if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
				return;
			var parts = input.split(/,| /);
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				part = part.trim();
				callback({id:part,text:part});
			}
		}
	});
}
// prize header name automatic tokenziation
function  prizeheadernametoken() {
	prizeheadername = [];
	$.ajax({
		url: base_url + "Schememaster/prizeheadernameload",
		async:false,
		cache:false,
		dataType:'json',
		success: function(data) {
			var i=0;
			$.each(data, function(index) {
				prizeheadername[i]=data[index];
				i++;
			});
        },
	});
	//prize description automatic tokenzation     
	$("#prizeheadername").select2({
		tags: prizeheadername,
		tokenSeparators: [',', ' '],
		maximumSelectionSize: 1,
		tokenizer: function(input, selection, callback) {
			// no comma no need to tokenize
			if (input.indexOf(',') < 0 && input.indexOf(' ') < 0)
				return;
			var parts = input.split(/,| /);
			for (var i = 0; i < parts.length; i++) {
				var part = parts[i];
				part = part.trim();
				callback({id:part,text:part});
			}
		}
	});
}
function getprizename(schemeid,prizeselectamount) {
	$('#prizeentryname').empty();	
    $('#prizeentryname').append($("<option></option>"));
	$.ajax({
        url: base_url + "Schememaster/getprizename",
        data:{schemeid:schemeid,prizeselectamount:prizeselectamount},
		async:false,
		dataType:'json',
		success: function(msg) { 
			for(var i = 0; i < msg.length; i++) {
				$('#prizeentryname')
				.append($("<option></option>")
				.attr('data-noofprizes',msg[i]['noofprizes'])
				.attr('data-prizeamount',msg[i]['giftamount'])
				.attr("value",msg[i]['prizeid'])
				.text(msg[i]['prizename']));
			}
        },
    });
}
function prizeentryadd() {
	var prizeamount=$('#prizeentryname').find('option:selected').data('prizeamount'); 
	var formdata = $("#prizeentryform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	setTimeout(function() {
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url +"Schememaster/prizeentrycreate",
        data: "datas=" + datainformation+"&prizeamount="+prizeamount,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'SUCCESS') {
				$('#prizeentrycancel').trigger('click');
				$('#processoverlay').hide();
				prizenetryrefreshgrid();
				var pending=$('#pendingentry').val();
				//$('#prizeentryname').trigger('change');
			    $('#tokenno,#chitbookno').val('');
			    $('#tokenno').focus();
			    prizeheadernametoken();
			    alertpopup(savealert);
	        } else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
        },
    });
}
function getchitbookno(schemeid,tokenno,prizeid) {
	$.ajax({
        url: base_url + "Schememaster/getchitbookno",
        data:{schemeid:schemeid,tokenno:tokenno,prizeid:prizeid},
		async:false,
		dataType:'json',
		success: function(msg) {
			if(msg == 'NO') {
				$("#tokenno").val('');
				alertpopup('Please enter proper Token Number!');
			} else {
				$('#chitbookno').val(msg);
			}
		},
    });
}
{ // Prize entry View Grid
	function prizenetrygrid(page,rowcount) {
			var defrecview = $('#mainviewdefaultview').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
			var wwidth = $('#prizenetrygrid').width();
			var wheight = $('#prizenetrygrid').height();
			/*col sort*/
			var sortcol = $('.savingschemeheadercolsort').hasClass('datasort') ? $('.savingschemeheadercolsort.datasort').data('sortcolname') : '';
			var sortord =  $('.savingschemeheadercolsort').hasClass('datasort') ? $('.savingschemeheadercolsort.datasort').data('sortorder') : '';
			var headcolid = $('.savingschemeheadercolsort').hasClass('datasort') ? $('.savingschemeheadercolsort.datasort').attr('id') : '0';
			var filterid = '';
			var userviewid =127;
			var viewfieldids ='49';
			var footername = 'prizeentry';
			$.ajax({
				url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=prizeentry&primaryid=prizeentryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername,
				contentType:'application/json; charset=utf-8',
				dataType:'json',
				async:false,
				cache:false,
				success:function(data) {
					$('#prizenetrygrid').empty();
					$('#prizenetrygrid').append(data.content);
					$('#prizenetrygridfooter').empty();
					$('#prizenetrygridfooter').append(data.footer);
					//data row select event
					datarowselectevt();
					//column resize
					columnresize('prizenetrygrid');
					{//sorting
						$('.savingschemeheadercolsort').click(function(){
							$("#processoverlay").show();
							$('.savingschemeheadercolsort').removeClass('datasort');
							$(this).addClass('datasort');
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							prizenetrygrid(page,rowcount);
							$("#processoverlay").hide();
						});
						sortordertypereset('savingschemeheadercolsort',headcolid,sortord);
					}
					{//pagination
						$('.pvpagnumclass').click(function(){
							$("#processoverlay").show();
							var page = $(this).data('pagenum');
							var rowcount = $('.pagerowcount').data('rowcount');
							prizenetrygrid(page,rowcount);
							$("#processoverlay").hide();
						});
						$('#prizeentrypgrowcount').change(function(){
							$("#processoverlay").show();
							var rowcount = $(this).val();
							$('.pagerowcount').attr('data-rowcount',rowcount);
							$('.pagerowcount').text(rowcount);
							var page = $('#prev').data('pagenum');
							prizenetrygrid(page,rowcount);
							$("#processoverlay").hide();
						});
					}
					//Material select
					$('#prizeentrypgrowcount').material_select();
					Materialize.updateTextFields();
				},
			});
		}
		{//refresh grid
			function prizenetryrefreshgrid() {
				var page = $('ul#savingschemepgnum li.active').data('pagenum');
				var rowcount = $('ul#savingschemepgnumcnt li .page-text .active').data('rowcount');
				prizenetrygrid(page,rowcount);
			}
		}
}
// get prending prize entry
function getpendingentry(schemeid,prizegiftid,prizeselectamount,prizeheadername) {
	$.ajax({
        url: base_url + "Schememaster/getpendingentry",
        data:{schemeid:schemeid,prizegiftid:prizegiftid,prizeselectamount:prizeselectamount,prizeheadername:prizeheadername},
		async:false,
		dataType:'json',
		success: function(msg) 
        { 
			var noofprizes=$('#prizeentryname').find('option:selected').data('noofprizes');
			$('#prizecount').val(noofprizes);
			var result=parseInt(noofprizes)-parseInt(msg);
			$('#pendingentry').val(result);
			if(result == 0) {
				alertpopup('Completed prize entry for this Scheme!');
				$('#prizeentryname').select2('val','');
				$('#prizecount').val(0);
				$('#pendingentry').val(0);
			}
		},
    });
}
//delete prize entry information
function prizeentrydelete(datarowid)
{	
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/prizeentrydelete",
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			prizenetryrefreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
		    if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
function prizeschemenameload() {
	$('#prizeschemeid').empty();
	$('#prizeschemeid').append($("<option></option>"));
	 $.ajax({
        url: base_url + "Schememaster/prizeschemenameload",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) 
        { 
			if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#prizeschemeid')
						.append($("<option></option>")
						.attr("value",data[index]['chitschemeid'])
						.text(data[index]['schemename']));
					});
				}	
				
        },
    });
}
{ //chit amount crud operations
	function chitamountadd()
   {   
    var formdata = $("#chitamountform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax(
    {
        url: base_url +"Schememaster/chitamountcreate",
        data: "datas=" + datainformation,
		type: "POST",
        success: function(msg)
        {
			var nmsg =  $.trim(msg);
			refreshgrid();
			if (nmsg == 'SUCCESS'){
				$('#processoverlay').hide();
						resetFields();
				 $('#chitamountcancel').trigger('click');
				alertpopup(savealert);
            }
			else {
				$('#processoverlay').hide();
				alertpopup(submiterror);
			}
        },
    });
}
	function chitamountretrive()
{
	var elementsname=['2','chitamount','amountsuffix'];
	var datarowid = $('#schemegrid').jqGrid('getGridParam','selrow');
	$.ajax(
	{
		url:base_url+"Schememaster/chitamountretrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			var txtboxname = elementsname;
			var dropdowns = [];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
		}
	});
}
function chitamountupdate()
{
	var formdata = $("#chitamountform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid= $('#schemegrid').jqGrid('getGridParam','selrow');
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/chitamountupdate",
        data: "datas=" + datainformation +"&primaryid="+ datarowid,
		type: "POST",
		async:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            resetFields();
			refreshgrid();
			$('#chitamountupdate').hide();
			$('#chitamountcreate').show();
			$('#chitamountdelete,#chitamountedit').show();
			if (nmsg == 'SUCCESS'){
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(updatealert);
	         } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
			}            
        },
    });
}
//delete chit amount information
function chitamountdelete(datarowid)
{	
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Schememaster/chitamountdelete",
        data:{primaryid:datarowid},
		async:false,
		success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			refreshgrid();
			$("#basedeleteoverlayforcommodule").fadeOut();
		    if (nmsg == 'SUCCESS') {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(deletealert);
            } else {
				setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopupdouble(submiterror);
            }
        },
    });
}
function loadchitamount(){
    $('#amount').empty();
	$('#amount').append($("<option></option>"));
	$.ajax(
			{
				url:base_url+"Schememaster/loadchitamount", 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{  
					var i=0;
					$.each(data, function(index) {
					 $('#amount')
						.append($("<option></option>")
						.attr("value",data[index]['chitamount']+'-'+data[index]['amountsuffix'])
						.text(data[index]['chitamount']+'-'+data[index]['amountsuffix']));
					 i++;
					});
				 }
			});
}
function checkboxvaluetruefalse(fieldid)
{
	var idvalue = $('#'+fieldid+'').val();
	if(checkValue(idvalue) == false)
	{
			idvalue = '';
	}
	return idvalue;
}
// scheme name load based on gift or luckdraw is yes
function schemenameloadbasedongift(scheme) {
	$('#'+scheme+'').empty();
	$('#'+scheme+'').append($("<option></option>"));
	 $.ajax({
        url: base_url + "Schememaster/schemenameloadbaseongift",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) { 
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+scheme+'')
					.append($("<option></option>")
					.attr("value",data[index]['chitschemeid'])
					.text(data[index]['schemename']));
				});
			}	
        },
    });
}
function checkuniquenamenew(){
	var primaryid = $("#updateschemeid").val();
	var accname = $("#prefixvalue").val();
	var fieldname = 'schemeprefix';
	var elementpartable = 'chitscheme';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		} 
	}
}
}
function checkamountsuffix(){
	var primaryid = '';
	var accname = $("#amountsuffix").val();
	var fieldname = 'amountsuffix';
	var elementpartable = 'chitamount';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		} 
	}
}
function varcheckuniquename() {
	var primaryid = $("#updateschemeid").val();
	var accname = $("#schemename").val();
	var elementpartable = 'chitscheme';
	if(accname !="") {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Scheme name already exists!";
				}
			} else {
				return "Scheme name already exists!";
			}
		} 
	}
}

//Kumaresan - Unique Gif/Prize name
function giftprizeuniquename() {
	var primaryid = $("#giftpriceid").val();
	var accname = $("#prizename").val();
	var elementpartable = 'chitgift';
	if(accname !="") {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Gift/Prize name already exists!";
				}
			} else {
				return "Gift/Prize name already exists!";
			}
		} 
	}
}
// check no of prizes based on scheme with amount based on chitbook customer
function checkprizeentry(value,schemeid,amountgift) {
	$.ajax({
        url: base_url + "Schememaster/checkprizeentry",
        data:{schemeid:schemeid,value:value,amountgift:amountgift},
		async:false,
		type:"post",
		success: function(msg) 
        {
			if(msg == 'SUCCESS') {
			}else{
				$('#noofprizes').val('');
				alertpopup('Give no of prizes based on customers');
				return false;
			}
        },
    });
}
</script>