$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$('.nbfilterdisplaymainviewclose').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	{//tag file global variables
		addchargeoperation = 'ADD'; //identify whether the add.charge is edit/add mode.
		MODE = 'ADD'; //for validation purpose
	}
	{// Grid Calling Function
		sizemastergrid();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	//$(window).resize(function() {
		maingridresizeheightset('sizemastergrid');
	//});
	purchasewastage ='No';
	wastagecalctype = 1;
	{ // action icons
		//add icon
		$("#addicon").click(function(){
			resetFields();
			addslideup('sizemastercreationview','sizemasterformadd'); 
			//loadchargedproduct();
			$("#resetfilter").trigger('click');
			//sizemasterinnergrid();
			Materialize.updateTextFields();
		});
		{// Close Add Screen
			{//close
				var addcloseinfo = ["sizemasterclose","sizemastercreationview","sizemasterformadd"];
				addclose(addcloseinfo);
			}
			$("#sizemasterclose").click(function() {
				var addcloseleadcreation = ["sizemasterclose","sizemastercreationview","sizemasterformadd"];
				addclose(addcloseleadcreation);
				//sizemasterrefreshgrid();
			});
		}
		$("#sizemasterclose").click(function(){
			$("#categoryid,#productid").select2('val','');
			$("#sizename,#description").val('');
			$("#description").text(''); 
            $("#alertsfcloseyes").click(function() {
                var clsform =$("#alertsfcloseyes").val();
                if(clsform == "Yes") {
					cleargriddata('sizemasterinnergrid');
					location.reload();
                }
            });
		});
		//Delete icon
		$("#deleteicon").click(function(){
			var datarowid = $('#sizemastergrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$("#basedeleteoverlay").fadeIn();
			} else {
				alertpopup(selectrowalert);
			}
		});
		$("#basedeleteyes").click(function(){
			sizemasterdelete();
		});
		//Edit iocn
		$("#editicon").click(function(){			
			var datarowid = $('#sizemastergrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$("#editproductid,#editcategoryid").prop('disabled',true);
				sizemasterdetailfetch(datarowid);
			} else {
				alertpopup(selectrowalert);
			}
		});
		//edit cancel icon'
		$("#sizemastercancel").click(function() {
			$("#editproductid,#editcategoryid").select2('val','');
			$("#editsizename").val('');
			$("#editdescription").text('');
			$("#sizemasteredit").fadeOut();
		});
	}
	{//on change trigger grid
		$("#categoryid").change(function(){
			$("#productid").select2('val','').trigger('change');
			$("#productid optgroup").addClass("ddhidedisplay");
			$("#productid optgroup option").prop('disabled',true);
			var categorylabel = $(this).find('option:selected').data('name');		
			$("#productid optgroup[label='"+categorylabel+"']").removeClass("ddhidedisplay");
			$("#productid optgroup[label='"+categorylabel+"'] option").prop('disabled',false);
			//sizemasterinnergrid();
		});
		$("#productid").change(function() {
			sizemasterinnergrid();
			/*setTimeout(function() {
				$(".sizemasterinner_headchkboxclass").trigger('click');
			},50);*/
		});
	}
	{//**sizemaster form validation**//
		$('#sizemastersubmit,#submitsizeadd').click(function() {
			$("#addsizemastervalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		
		$( "#sizename" ).keypress(function(e) {
			if (e.keyCode == 13) {
				$("#addsizemastervalidate").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			  }
		});
		jQuery("#addsizemastervalidate").validationEngine({
			onSuccess: function() {
				var productid = getselectedrowids('sizemasterinnergrid');
				if(productid != '') {
					sizemasteradd();
				} else {
					alertpopup('Please select the product(s)');
				}
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
			}
		});
		//sizemaster update validate
		$('#sizemasterupdate').click(function(){
			$("#sizemastereditform").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#sizemastereditform").validationEngine({
			onSuccess: function(){			
				sizemasterupdate();
			},
			onFailure: function(){
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
			}	
		});
	}
	{//filter
		$("#sizemasterfilterddcondvaluedivhid").hide();
		$("#sizemasterfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#sizemasterfiltercondvalue").focusout(function(){
				var value = $("#sizemasterfiltercondvalue").val();
				$("#sizemasterfinalfiltercondvalue").val(value);
				$("#sizemasterfilterfinalviewconid").val(value);
			});
			$("#sizemasterfilterddcondvalue").change(function(){
				var value = $("#sizemasterfilterddcondvalue").val();
				sizemastermainfiltervalue=[];
				$('#sizemasterfilterddcondvalue option:selected').each(function(){
					var $mvalue =$(this).attr('data-ddid');
					sizemastermainfiltervalue.push($mvalue);
					$("#sizemasterfinalfiltercondvalue").val(sizemastermainfiltervalue);
				});
				$("#sizemasterfilterfinalviewconid").val(value);
			});
		}
		sizemasterfiltername = [];
		$("#sizemasterfilteraddcondsubbtn").click(function() {
			$("#sizemasterfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#sizemasterfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(sizemastergrid,'sizemaster');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$("#resetfilter").click(function() {
		$("#categoryid").select2('val','').trigger('change');
		$("#sizename,#description").val('');
		
	});
});
{//sizemaster grid view
	function sizemastergrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#sizemastergrid').width();
		var wheight =  $(window).height();
		/*col sort*/
		var sortcol = $("#sizemastersortcolumn").val();
		var sortord = $("#sizemastersortorder").val();
		if(sortcol) {
			sortcol = sortcol;
		} else{
			var sortcol = $('.sizemasterheadercolsort ').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').attr('id') : '0';
		var filterid = $("#sizemasterfilterid").val();
		var conditionname = $("#sizemasterconditionname").val();
		var filtervalue = $("#sizemasterfiltervalue").val();
		var userviewid = 199;
		var viewfieldids = 134;
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=sizemaster&primaryid=sizemasterid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#sizemastergrid').empty();
				$('#sizemastergrid').append(data.content);
				$('#sizemastergridfooter').empty();
				$('#sizemastergridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('sizemastergrid');
				{//sorting
					$('.sizemasterheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.sizemasterheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount =  $('.pagerowcount').data('rowcount');
						var sortcol = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
						$("#sizemastersortorder").val(sortord);
						$("#sizemastersortcolumn").val(sortcol);
						var sortpos = $('#sizemastergrid .gridcontent').scrollLeft();
						sizemastergrid(page,rowcount);
						$('#sizemastergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('sizemasterheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function() {
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						sizemastergrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#sizemasterpgrowcount').change(function() {
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						sizemastergrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#sizemasterpgrowcount').material_select();
			},
		});	
	}
}
//size master product innmer grid
function sizemasterinnergrid() {
	chargeid = 0;
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 1000 : rowcount;
	var wwidth = $("#sizemasterinnergrid").width();
	var wheight = $("#sizemasterinnergrid").height();
	/*col sort*/
	var sortcol = $("#sizemastersortcolumn").val();
	var sortord = $("#sizemastersortcolumn").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').attr('id') : '0';
	var categoryid = $("#categoryid").val();
	var productid = $("#productid").val();
	$.ajax({
		url:base_url+"Sizemaster/gridinformationfetch?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=90"+"&filter="+chargeid+"&categoryid="+categoryid+"&productid="+productid,			
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#sizemasterinnergrid').empty();
			$('#sizemasterinnergrid').append(data.content);
			$('#sizemasterinnergridfooter').empty();
			$('#sizemasterinnergridfooter').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('sizemasterinnergrid');
			{//sorting
				$('.sizemasterheadercolsort').click(function(){
					$('.sizemasterheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#sizemastergridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
					$("#sizemastersortorder").val(sortord);
					$("#sizemastersortcolumn").val(sortcol);
					var sortpos = $('#sizemasterinnergrid .gridcontent').scrollLeft();
					sizemasterinnergrid(page,rowcount);
					$('#sizemasterinnergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('sizemasterheadercolsort',headcolid,sortord);
			}
			var hideprodgridcol = ['innerbranchid','innermetalid','innerpurityid'];
			gridfieldhide('sizemasterinnergrid',hideprodgridcol);
			//header check box
			$(".sizemasterinner_headchkboxclass").click(function() {
				checkboxclass('sizemasterinner_headchkboxclass','sizemasterinner_rowchkboxclass');
			});
			//row based check box
			$(".sizemasterinner_rowchkboxclass").click(function() {
				$('.sizemasterinner_headchkboxclass').prop( "checked", false );
			});
		},
	});
}
//innser grid check / Uncheck operation
function checkboxclass(headerclass,rowclass) {
	if($("."+headerclass+"").prop('checked') == true) {
		$("."+rowclass+"").prop('checked', true);
		$("."+headerclass+"").prop('checked', true);
	} else {
		$("."+rowclass+"").prop( "checked", false );
	}
}
{//CRUD Related functions
	//Create
	function sizemasteradd() {

		var formdata = $("#sizemasterform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var productid = getselectedrowids('sizemasterinnergrid');
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
	    $.ajax({
	        url: base_url +"Sizemaster/sizemastercreate",
	        data: "datas=" + datainformation+"&productid="+productid,
			type: "POST",
	        success: function(msg) { 
	 			var nmsg =  $.trim(msg);
	            //clearform('sizemasterformadd');
	            //$('#sizemasterformadd').hide();
	 			//$('#sizemastercreationview').fadeIn(1000);
				$("#sizename,#description").val('');
	            $("#processoverlay").hide();
				$("#sizename").focus();
	            sizemastergrid();
				if(nmsg == 'Success'){
					//alertpopup(savealert);
	            } else {
					alertpopup(submiterror);
				}
	        },
	    });
	}
	//retrieve sizemaster data
	function sizemasterdetailfetch(datarowid) {
		$.ajax( {
			url:base_url+"Sizemaster/sizemasterretrive?primaryid="+datarowid, 
			dataType:'json',
			async:false,
			success: function(data) {
				$("#editproductid").select2('val',data.productid);
				$("#editcategoryid").select2('val',data.categoryid);
				$("#editsizename").val(data.name);
				$("#editdescription").val(data.description);
				$("#editsizemasterid").val(datarowid);
				Materialize.updateTextFields();
				$("#sizemasteredit").fadeIn();
			}
		});
	}
	//sizemaster detail update
	function sizemasterupdate() {
		var formdata = $("#sizemastereditform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var editcategoryid = $('#editcategoryid').find('option:selected').val();
		var editproductid = $('#editproductid').find('option:selected').val();
		setTimeout(function() {
			$('#processoverlay').show();
		},25);
	    $.ajax({
	        url: base_url+"Sizemaster/sizemasterupdate",
	        data: "datas="+datainformation+amp+"editproductid="+editproductid+amp+"editcategoryid="+editcategoryid,
			type: "POST",
	        success: function(msg) {
				var nmsg =  $.trim(msg);
				$('#sizemasteredit').fadeOut();
				if (nmsg == 'Success') {
					sizemastergrid();
					setTimeout(function() {
						$('#processoverlay').hide();
						Materialize.updateTextFields();
					},100);
					alertpopup(savealert);
	            } else {
					setTimeout(function() {
						$('#processoverlay').hide();
						Materialize.updateTextFields();
					},25);
					alertpopup(submiterror);
				}
	        },
	    });
	}
	//delete size master information
	function sizemasterdelete(datarowid) {	
		var datarowid= $('#sizemastergrid div.gridcontent div.active').attr('id');
		setTimeout(function(){
			$('#processoverlay').show();
		},25);
	    $.ajax({
	        url: base_url + "Sizemaster/sizemasterdelete",
	        data:{primaryid:datarowid},
	        type: "POST",
			success: function(msg)  {
				var nmsg =  $.trim(msg);
				$("#basedeleteoverlay").fadeOut();
			    if (nmsg == 'Success') {	
			    	sizemastergrid();
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(deletealert);
	            } else {
					setTimeout(function(){
						$('#processoverlay').hide();
					},25);
					alertpopup(submiterror);
	            }
	        },
	    });
	}
}
{//refresh grid
	function sizemasterrefreshgrid() {
		var page = $('ul#sizemasterpgrowcount li.active').data('pagenum');
		var rowcount = $('sizemasterpgrowcount').find('option:selected').val();
		sizemastergrid(page,rowcount);
	}
}
function sizemasternamecheckonedit() {
	var primaryid =$("#editsizemasterid").val();
	var accname = $("#editsizename").val();
	var productid = $("#editproductid").val();
	var fieldname = 'sizemastername';
	var elementpartable = 'sizemaster';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Sizemaster/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname+"&productid="+productid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Size name already exists!";
				}
			} else {
				return "Size name already exists!";
			}
		}
	}
}