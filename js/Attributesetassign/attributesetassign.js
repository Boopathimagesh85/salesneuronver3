$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		attributesetassignaddgrid();
		firstfieldfocus();
		attributesetassigncrudactionenable();
	}
	//hidedisplay
	$('#attributesetassigndataupdatesubbtn').hide();
	$('#attributesetassignsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			attributesetassignaddgrid();
		});
		$('#newattributeid').change(function(){
			$("#s2id_newattributeid,.select2-results").removeClass('error');
			$("#s2id_newattributeid").find('ul').removeClass('error');
		});
	}
	{
		//validation for Company Add  
		$('#attributesetassignsavebutton').mousedown(function(e) {
			var attribute = $('#newattributeid').val().length;
			if(attribute > 2){
				if(e.which == 1 || e.which === undefined) {
					masterfortouch = 0;
					$("#attributesetassignformaddwizard").validationEngine('validate');
				}
			}else{
				alertpopup("To create attribute set, need two attribute");
			}			
		});
		jQuery("#attributesetassignformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#attributesetassignsavebutton').attr('disabled','disabled'); 
				attributesetassignaddformdata();
				var attributeid=$('#attributeid').select2('val');
			},
			onFailure: function() {
				var dropdownid =['2','newattributeid'];
				dropdownfailureerror(dropdownid);
				$("#s2id_newattributeid").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
	}
	{
		$( window ).resize(function() {
			innergridresizeheightset('attributesetassignaddgrid');
		});
		//update company information
		$('#attributesetassigndataupdatesubbtn').mousedown(function(e) {
			var attribute = $('#newattributeid').val().length;
			if(attribute > 1){
				if(e.which == 1 || e.which === undefined) {
					$("#attributesetassignformeditwizard").validationEngine('validate');
				}
			}else{
				alertpopup("To update attribute set, need two attribute");
			}			
		});
		jQuery("#attributesetassignformeditwizard").validationEngine({
			onSuccess: function() {
				attributesetassignupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','newattributeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	{//filter work
		//for toggle
		$('#attributesetassignviewtoggle').click(function() {
			if ($(".attributesetassignfilterslide").is(":visible")) {
				$('div.attributesetassignfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.attributesetassignfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#attributesetassignclosefiltertoggle').click(function(){
			$('div.attributesetassignfilterslide').removeClass("filterview-moveleft");
		});
		$('#attributesetassignclosefiltertoggle').click(function(){
			$('.attributesetassignfilterview').addClass("hidedisplay");
			$('.attributesetassignfullgridview').removeClass("large-9");
			$('.attributesetassignfullgridview').addClass("large-12");
		});
		//filter
		$("#attributesetassignfilterddcondvaluedivhid").hide();
		$("#attributesetassignfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#attributesetassignfiltercondvalue").focusout(function(){
				var value = $("#attributesetassignfiltercondvalue").val();
				$("#attributesetassignfinalfiltercondvalue").val(value);
				$("#attributesetassignfilterfinalviewconid").val(value);
			});
			$("#attributesetassignfilterddcondvalue").change(function(){
				var value = $("#attributesetassignfilterddcondvalue").val();
				attributesetassignmainfiltervalue=[];
				$('#attributesetassignfilterddcondvalue option:selected').each(function(){
				    var $asvalue =$(this).attr('data-ddid');
				    attributesetassignmainfiltervalue.push($asvalue);
					$("#attributesetassignfinalfiltercondvalue").val(attributesetassignmainfiltervalue);
				});
				$("#attributesetassignfilterfinalviewconid").val(value);
			});
		}
		attributesetassignfiltername = [];
		$("#attributesetassignfilteraddcondsubbtn").click(function() {
			$("#attributesetassignfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#attributesetassignfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(attributesetassignaddgrid,'attributesetassign');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function attributesetassignaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#attributesetassignaddgrid").width();
	var wheight = $("#attributesetassignaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#attributesetassignsortcolumn").val();
	var sortord = $("#attributesetassignsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.attributesetassignheadercolsort').hasClass('datasort') ? $('.attributesetassignheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.attributesetassignheadercolsort').hasClass('datasort') ? $('.attributesetassignheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.attributesetassignheadercolsort').hasClass('datasort') ? $('.attributesetassignheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 15;
	if(userviewid != '') {
		userviewid= '61';
	}
	userviewid = userviewid!= '' ? '61' : userviewid;
	var filterid = $("#attributesetassignfilterid").val();
	var conditionname = $("#attributesetassignconditionname").val();
	var filtervalue = $("#attributesetassignfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=attributesetassign&primaryid=attributesetassignid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#attributesetassignaddgrid').empty();
			$('#attributesetassignaddgrid').append(data.content);
			$('#attributesetassignaddgridfooter').empty();
			$('#attributesetassignaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('attributesetassignaddgrid');
			{//sorting
				$('.attributesetassignheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.attributesetassignheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#attributesetassignpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.attributesetassignheadercolsort').hasClass('datasort') ? $('.attributesetassignheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.attributesetassignheadercolsort').hasClass('datasort') ? $('.attributesetassignheadercolsort.datasort').data('sortorder') : '';
					$("#attributesetassignsortorder").val(sortord);
					$("#attributesetassignsortcolumn").val(sortcol);
					var sortpos = $('#attributesetassignaddgrid .gridcontent').scrollLeft();
					attributesetassignaddgrid(page,rowcount);
					$('#attributesetassignaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('attributesetassignheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount =$('.pagerowcount').data('rowcount');
					attributesetassignaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#attributesetassignpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					attributesetassignaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#attributesetassignaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#attributesetassignpgrowcount').material_select();
		},
	});	
}
function attributesetassigncrudactionenable() {
	{//add icon click
		$('#attributesetassignaddicon').click(function(e){
			$("#attributesetassignsectionoverlay").removeClass("closed");
			$("#attributesetassignsectionoverlay").addClass("effectbox");
			$("#attributesetassigndeleteicon").show();
			e.preventDefault();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#attributesetassigndataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#attributesetassignsavebutton').show();
		});
	}
	$("#attributesetassignediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#attributesetassignaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			attributesetassigngetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#attributesetassigndeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#attributesetassignaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {	
			clearform('cleardataform');
			$("#attributesetassignprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('attributesetassigndelete');
			$("#attributesetassigndelete").click(function(){
				var datarowid = $("#attributesetassignprimarydataid").val(); 
				attributesetassignrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
}
{//refresh grid
	function attributesetassignrefreshgrid() {
		var page = $('ul#attributesetassignpgnum li.active').data('pagenum');
		var rowcount = $('ul#attributesetassignpgnumcnt li .page-text .active').data('rowcount');
		attributesetassignaddgrid(page,rowcount);
	}
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#attributesetassignsectionoverlay").removeClass("effectbox");
		$("#attributesetassignsectionoverlay").addClass("closed");
	});
}
//new data add submit function
function attributesetassignaddformdata() {
	var attributeid=$('#newattributeid').val();
	var formdata = $("#attributesetassignaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	attributeid = attributeid.toString();
	var newval = attributeid.replace(/(^,)|(,$)/g, "");
	$.ajax( {
        url: base_url + "Attributesetassign/newdatacreate",
        data: "datas=" + datainformation+"&multiattributeid="+newval,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				clearform('cleardataform');
				resetFields();
				attributesetassignrefreshgrid();
				$("#attributesetassignsavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}
//old information show in form
function attributesetassigngetformdata(datarowid) {
	var attributesetassignelementsname = $('#attributesetassignelementsname').val();
	var attributesetassignelementstable = $('#attributesetassignelementstable').val();
	var attributesetassignelementscolmn = $('#attributesetassignelementscolmn').val();
	var attributesetassignelementpartable = $('#attributesetassignelementspartabname').val();
	$.ajax( {
		url:base_url+"Attributesetassign/fetchformdataeditdetails?attributesetassignprimarydataid="+datarowid+"&attributesetassignelementsname="+attributesetassignelementsname+"&attributesetassignelementstable="+attributesetassignelementstable+"&attributesetassignelementscolmn="+attributesetassignelementscolmn+"&attributesetassignelementpartable="+attributesetassignelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$("#attributesetassigndeleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				attributesetassignrefreshgrid();
			} else {
				$("#attributesetassignsectionoverlay").removeClass("closed");
				$("#attributesetassignsectionoverlay").addClass("effectbox");
				addslideup('taskview','taskaddform');
				var txtboxname = attributesetassignelementsname + ',attributesetassignprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = attributesetassignelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
		}
	});
	$('#attributesetassigndataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#attributesetassignsavebutton').hide();
}
//udate old information
function attributesetassignupdateformdata() {
	var attributeid=$('#newattributeid').val();
	var formdata = $("#attributesetassignaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	attributeid = attributeid.toString();
	var newval = attributeid.replace(/(^,)|(,$)/g, "");
    $.ajax({
        url: base_url + "Attributesetassign/datainformationupdate",
        data: "datas=" + datainformation+"&multiattributeid="+newval,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				$('#attributesetassigndataupdatesubbtn').hide();
				$('#attributesetassignsavebutton').show();
				$("#attributesetassigndeleteicon").show();
				resetFields();
				attributesetassignrefreshgrid();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;	
            }
        },
    });	
}
//udate old information
function attributesetassignrecorddelete(datarowid) {
	var attributesetassignelementstable = $('#attributesetassignelementstable').val();
	var attributesetassigelementspartable = $('#attributesetassignelementspartabname').val();
    $.ajax({
        url: base_url + "Attributesetassign/deleteinformationdata?attributesetassignprimarydataid="+datarowid+"&attributesetassignelementstable="+attributesetassignelementstable+"&attributesetassignparenttable="+attributesetassigelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	attributesetassignrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	attributesetassignrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//attributesetassignnamecheck
function attributesetassignnamecheck() {
	var primaryid = $("#attributesetassignprimarydataid").val();
	var accname = $("#attributesetassignname").val();
	var elementpartable = $('#attributesetassignelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Attribute Set Name already exists!";
				}
			}else {
				return "Attribute Set Name already exists!";
			}
		} 
	}
}