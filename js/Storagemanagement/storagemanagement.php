<script>
$(document).ready(function() 
{
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Grid Calling Function
		storagemanagementgrid();
	}
	{//form field first focus
		firstfieldfocus();
	}
	{// Close Add Screen
		var addcloseleadcreation =["addsectionclose","storagemanagementview","storagemanagementformadd",""];
		addclose(addcloseleadcreation);
	}
		{// Close Add Screen
		$("#addsectionclose").click(function()
		
	{	
	var addcloseleadcreation =["addsectionclose","chitgenerationcreationview","chitgenerationformadd",""];
			addclose(addcloseleadcreation);
		cleargriddata('innergrid');
		refreshgrid();
	});
	}
	pdfpreviewtemplateload(59);
	// round value setTimeout
	weight_round = $('#weight_round').val();
	$("#addicon").click(function(){
		resetFields();
		cleargriddata('innergrid');
		addslideup('storagemanagementview','storagemanagementformadd');
		getcurrentsytemdate('storagemanagementdate');
		$("#salespersonid").select2('val',$('#hiddenemployeeid').val());	
		$("#session").select2('val','No').trigger('change');
		$("#mode").select2('val','One');
		firstfieldfocus();
		$("#counterid").focus();
		Materialize.updateTextFields();
	});
	// session change
	$('#session').change(function() {
		var sessionvalue = $("#session").find('option:selected').val();
		if(sessionvalue == 'Yes') {
			$('.sessioniddiv').show();
			serialnumbergenerate();	
		}else if(sessionvalue == 'No') {
			$('#sessionid').val('');
			$('.sessioniddiv').hide();
		}
	});
	// mode change
	$('#mode').change(function() {
		var sessionvalue = $("#mode").find('option:selected').val();
		if(sessionvalue == 'One') {
			$('#counter').prop('disabled',false);
		}else if(sessionvalue == 'Many') {
			$('#counter').select2('val','').trigger('change');
			$('#counter').prop('disabled',true);
			$('#description').val('');
		}
	});
	// counterid change
	$('#counterid').change(function() {
		var cid = $(this).val();
		if(isNaN(cid)){
			$(this).val('');
			$('#counter').select2('val','').trigger('change');
			//alertpopup('Enter only valid counterid number');
			$('#lasterrrorwt,#lasterrorpieces,#counterweight,#openingweight,#openingpieces,#inweight,#inpieces,#outweight,#outpieces,#closingweight,#closingpieces,#manualclosingweight,#manualclosingpieces,#errorweight,#errorpieces').val(0);
			$('#description').val('');
			$('#tagtemplateid,#printtemplateid').select2('val',1);
		}else{
			$('#counter').select2('val',cid).trigger('change');
		}
		
	});
	$('#counterid').keypress(function(e) {
		if (e.which == 13) {
			$('#counterid').trigger('change');
		}
	});
	// mode change
	$('#counter').change(function() {
		var counter = $.trim($("#counter").find('option:selected').val());
		if(counter > 1){
			var sessionvalue = $("#mode").find('option:selected').val();
			$('#lasterrrorwt,#lasterrorpieces,#counterweight,#openingweight,#openingpieces,#inweight,#inpieces,#outweight,#outpieces,#closingweight,#closingpieces,#manualclosingweight,#manualclosingpieces,#errorweight,#errorpieces').val(0);
			$('#description').val('');
			$('#tagtemplateid,#printtemplateid').select2('val',1);
			if(sessionvalue == 'One') {
				if(counter != '' && counter != '1') {
					loadsummary(counter);
					innergrid();
					$("#manualclosingweight").focus(); 
				}
			}else if(sessionvalue == 'Many') {
				
			}
			$('#counterid').val(counter);
		}else{
			$('#counterid').val('');
			$('#lasterrrorwt,#lasterrorpieces,#counterweight,#openingweight,#openingpieces,#inweight,#inpieces,#outweight,#outpieces,#closingweight,#closingpieces,#manualclosingweight,#manualclosingpieces,#errorweight,#errorpieces').val(0);
			$('#description').val('');
			$('#tagtemplateid,#printtemplateid').select2('val',1);
			//alertpopup('Enter only valid counterid number');
		}
		Materialize.updateTextFields();
	});
	// manualclosingweight change
	$('#manualclosingweight').change(function() {
		var manualclosingweight = $(this).val();
		if(manualclosingweight != '') {
			var closingweight = $('#closingweight').val();
			var errorwt = 0;
			errorwt = parseFloat(manualclosingweight) - parseFloat(closingweight);
			$('#errorweight').val(parseFloat(errorwt).toFixed(weight_round));
		} else {
			$('#manualclosingweight').val('0');
		}
	});
	// manualclosingpieces change
	$('#manualclosingpieces').change(function() {
		var manualclosingpcs = $(this).val();
		if(manualclosingpcs != '') {
			var closingpieces = $('#closingpieces').val();
			var errorpcs = 0;
			errorpcs = parseFloat(manualclosingpcs) - parseFloat(closingpieces);
			$('#errorpieces').val(errorpcs);
		} else {
			$('#manualclosingpieces').val('0');
			$('#errorpieces').val('0');
		}
	});
	//for date picker
	var dateformetdata = $('#storagemanagementdate').attr('data-dateformater');
	$('#storagemanagementdate').datetimepicker({
		dateFormat: dateformetdata,
		showTimepicker :false,
		minDate: null,
		changeMonth: true,
		changeYear: true,
		maxDate:0,
		yearRange : '1947:c+100',
		onSelect: function () {
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
			}
		},
		onClose: function () {
			$('#storagemanagementdate').focus();
		}
	});
		/* $('#storagemanagementdate').datetimepicker({
				dateFormat: 'dd-mm-yy',
				showTimepicker :false,
				minDate:null,
				maxDate:0,
				onSelect: function () {
					var checkdate = $(this).val();
					if(checkdate != ''){
						$(this).next('span').remove('.btmerrmsg'); 
						$(this).removeClass('error');
						$(this).prev('div').remove();
					}
				},
				onClose: function () {
					$('#storagemanagementdate').focus();
				}
		}).click(function() {
				$(this).datetimepicker('show');
		}); */
		{//**Validation ADD*//
			$('#addstoragemanagement').click(function() 
			{
				$("#addstoragemanagmentvalidate").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			});
			//add	
			jQuery("#addstoragemanagmentvalidate").validationEngine(
			{
				onSuccess: function()
				{
					addstoragemanagement();
				},
				onFailure: function()
				{	
					var dropdownid =['1','counter'];
					 dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
			$('#manualclosingweight,#manualclosingpieces').keypress(function(e) {
				if (e.which == 13) {
					$('#manualclosingweight,#manualclosingpieces').trigger('change');
					$("#addstoragemanagmentvalidate").validationEngine('validate');
					$(".mmvalidationnewclass .formError").addClass("errorwidth");
				}
			});
		}
});

// Rate View Grid
function storagemanagementgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#storagemanagementgrid').width();
	var wheight = $(window).height();
	/*col sort*/
	var sortcol = $("#storagemanagementsortcolumn").val();
	var sortord = $("#storagemanagementsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.storagemanagementheadercolsort').hasClass('datasort') ? $('.storagemanagementheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.storagemanagementheadercolsort').hasClass('datasort') ? $('.storagemanagementheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.storagemanagementheadercolsort').hasClass('datasort') ? $('.storagemanagementheadercolsort.datasort').attr('id') : '0';
	var filterid = $("#utilityfilterid").val();
	var conditionname = $("#utilityconditionname").val();
	var filtervalue = $("#utilityfiltervalue").val();
	var userviewid =206;
	var viewfieldids =59;
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=storagemanagement&primaryid=storagemanagementid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#storagemanagementgrid').empty();
			$('#storagemanagementgrid').append(data.content);
			$('#storagemanagementgridfooter').empty();
			$('#storagemanagementgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('storagemanagementgrid');
			{//sorting
				$('.storagemanagementheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.storagemanagementheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.storagemanagementheadercolsort').hasClass('datasort') ? $('.storagemanagementheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.storagemanagementheadercolsort').hasClass('datasort') ? $('.storagemanagementheadercolsort.datasort').data('sortorder') : '';
					$("#storagemanagementsortorder").val(sortord);
					$("#storagemanagementsortcolumn").val(sortcol);
					var sortpos = $('#storagemanagementgrid .gridcontent').scrollLeft();
					storagemanagementgrid(page,rowcount);
					$('#storagemanagementgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('storagemanagementheadercolsort',headcolid,sortord);
			}
			{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						storagemanagementgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#storagemanagementpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						storagemanagementgrid(page,rowcount);
						$("#processoverlay").hide();
					});
			}
			//Material select
				$('#storagemanagementpgrowcount').material_select();
		},
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#storagemanagementpgnum li.active').data('pagenum');
		var rowcount = $('ul#storagemanagementpgnumcnt li .page-text .active').data('rowcount');
		storagemanagementgrid(page,rowcount);
	}
}

{// Auto number generation 
	function serialnumbergenerate() {
		$.ajax({
			url: base_url + "Storagemanagement/serialnumbergenerate",
			async:false,
			type: "POST",
			cache:false,
			success: function(data) {
				$('#sessionid').val(data);
				Materialize.updateTextFields();
			},
		});
	}
}
// load summary data based on counter
function loadsummary(counter) {
	var storagemanagementdate =  $('#storagemanagementdate').val(); 
	$.ajax({
			url: base_url + "Storagemanagement/loadsummary",
			data:{counter:counter,storagemanagementdate:storagemanagementdate},
			async:false,
			type: "POST",
			dataType:'json',
			cache:false,
			success: function(data) {
				$('#openingweight').val(data['openingwt']);
				$('#openingpieces').val(data['openingpcs']);
				$('#inweight').val(data['inwt']);
				$('#inpieces').val(data['inpcs']);
				$('#outweight').val(data['outwt']);
				$('#outpieces').val(data['outpcs']);
				$('#closingweight').val(data['closewt']);
				$('#closingpieces').val(data['closepcs']);
				$('#counterweight').val(data['boxweight']);
			    $('#description').val(data['description']);
				$('#manualclosingweight').val(data['closewt']);
				$('#manualclosingpieces').val(data['closepcs']);
				$('#openingtagweight').val(data['openingtagweight']);
				$('#intagwt').val(data['intagwt']);
				$('#outtagwt').val(data['outtagwt']);
				$('#lasterrrorwt').val(data['lasterrrorwt']);
				$('#lasterrorpieces').val(data['lasterrorpieces']);
				$('#closingtagweight').val(data['closingtagweight']);
			    $('#lastprintdatetime').val(data['lastprintdatetime']);
				$('#printyesno').val(data['print']);
				$('#tagtemplateid').select2('val',data['lasttagtemplateid']);
				$('#printtemplateid').select2('val',data['lastprinttemplatesid']);
				Materialize.updateTextFields();
			},
		});
}
// add storagemanagement
function  addstoragemanagement() {
	if($('#printyesno').val() == 1) { //If printyesno value is 0 [it will print], 1 means no print.
		alertpopup('Please select any Tag/Print Template to save.');
	} else {
		var form = $("#storagemanagmentform").serialize();
		$('select[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		$('input[disabled]').each( function() { //attach license fields
			form = form + '&' + $(this).attr('name') + '=' + $(this).val();
		});
		
		form = form + '&' + $('#closingtagweight').attr('id') + '=' +  $('#closingtagweight').val();
		form = form + '&' + $('#lasterrrorwt').attr('id') + '=' +  $('#lasterrrorwt').val();
		form = form + '&' + $('#lasterrorpieces').attr('id') + '=' +  $('#lasterrorpieces').val();
		var amp = '&'; 
		var datainformation = amp + form;
		$('#processoverlay').show();
		$.ajax({
			url: base_url +"Storagemanagement/addstoragemanagement",
			data: "datas=" + datainformation,
			dataType:'json',
			type: "POST",
			success: function(msg) {
				if (msg == 'SUCCESS' || msg['status'] == "SUCCESS") {
					if(msg['status'] == "SUCCESS" && msg['templateid'] > 1) { //for Printtemplate Generate number
						printautomate(msg['storagemgmtid'],msg['templateid'],msg['salesnumber']);
					}
					cleargriddata('innergrid');
					$('#counter').select2('val','').trigger('change');
					$("#counterid").val('');
					$("#counterid").focus();
					//$("#counter").select2('focus');
					$('#lastprintdatetime').val('');
					$('#processoverlay').hide();
				}
			},
		});
	}
}
// load structure counter
function loadproductcounter(status) {
	$('#counter').empty();
	$('#counter').append($("<option value=''></option>"));
	var accountid = $('#accountid').val();
	$.ajax({
		url: base_url+"Storagemanagement/loadproductcounter",
		dataType:'json',
		type: "POST",
		async:false,
		success: function(msg) {
			$.each(msg, function(index) {
			$("#counter")
			.append($("<option></option>")
			.attr("data-lastprintdatetime",msg[index]['lastprintdatetime'])
			.attr("data-boxweight",msg[index]['boxweight'])
			.attr("data-description",msg[index]['comment'])
			.attr("value",msg[index]['counterid'])
			.text(msg[index]['counterid']+'-'+msg[index]['countername']));
		});
		},
	});
}
//storage management inner grid
function innergrid() {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 1000 : rowcount;
	var wwidth = $("#innergrid").width();
	var wheight = $("#innergrid").height();
	/*col sort*/
	var sortcol = $("#sizemastersortcolumn").val();
	var sortord = $("#sizemastersortcolumn").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').attr('id') : '0';
	var counter = $.trim($("#counter").find('option:selected').val());
	var storagemanagementdate =  $('#storagemanagementdate').val(); 
	var lastprintdatetime =  $('#lastprintdatetime').val();
	$.ajax({
		url:base_url+"Storagemanagement/gridinformationfetch?maintabinfo=Storagemanagement&primaryid="+counter+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=59"+"&storagemanagementdate="+storagemanagementdate+"&lastprintdatetime="+lastprintdatetime,			
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#innergrid').empty();
			$('#innergrid').append(data.content);
			$('#innergridfooter').empty();
			$('#innergridfooter').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('innergrid');
			{//sorting
				$('.sizemasterheadercolsort').click(function(){
					$('.sizemasterheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#sizemastergridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.sizemasterheadercolsort').hasClass('datasort') ? $('.sizemasterheadercolsort.datasort').data('sortorder') : '';
					$("#sizemastersortorder").val(sortord);
					$("#sizemastersortcolumn").val(sortcol);
					var sortpos = $('#sizemasterinnergrid .gridcontent').scrollLeft();
					sizemasterinnergrid(page,rowcount);
					$('#sizemasterinnergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('sizemasterheadercolsort',headcolid,sortord);
			}
			//header check box
			$(".sizemasterinner_headchkboxclass").click(function() {
				checkboxclass('sizemasterinner_headchkboxclass','sizemasterinner_rowchkboxclass');
			});
			//row based check box
			$(".sizemasterinner_rowchkboxclass").click(function() {
				$('.sizemasterinner_headchkboxclass').prop( "checked", false );
			});
		},
	});
}
function printautomate(salesid,printtemplateid,snumber){
	$('#printpdfid').val(salesid);
	$('#printsnumber').val(snumber);		
	$('#pdftemplateprview').select2('val',printtemplateid).trigger('change');
	$('#pdffilepreviewintab').trigger("click");
}
</script>