$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{
		$("#soundssouingridreloadspan1").show();
	}
	$('#groupcloseaddform').hide();
	{//Grid Calling
		soundsaddgrid();
		firstfieldfocus();
		crudactionenable();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	{
		$("#addicon").click(function(){
			callsettingsddload('callsettingsid');
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
		});
	}
	{//sound reload operation
		$("#reloadicon").click(function(){
			resetFields();
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('soundsaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
		//sound add
		$("#soundssavebutton").click(function() {	
			if($("#filenameattachdisplay").is(':empty')){
				alertpopup('Please Upload File...');
			}else{
				$("#soundsformaddwizard").validationEngine('validate');
			}
			//For Touch
			masterfortouch = 0;
		});
		jQuery("#soundsformaddwizard").validationEngine({
			onSuccess: function() {
				soundfileupload();
			},
			onFailure: function() {
				var dropdownid =['',''];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//filter work
		//for toggle
		$('#soundsviewtoggle').click(function() {
			if ($(".soundsfilterslide").is(":visible")) {
				$('div.soundsfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.soundsfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#soundsclosefiltertoggle').click(function(){
			$('div.soundsfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#soundsfilterddcondvaluedivhid").hide();
		$("#soundsfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#soundsfiltercondvalue").focusout(function(){
				var value = $("#soundsfiltercondvalue").val();
				$("#soundsfinalfiltercondvalue").val(value);
				$("#soundsfilterfinalviewconid").val(value);
			});
			$("#soundsfilterddcondvalue").change(function(){
				var value = $("#soundsfilterddcondvalue").val();
				if( $('#s2id_soundsfilterddcondvalue').hasClass('select2-container-multi') ) {
					soundsmainfiltervalue=[];
					$('#soundsfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    soundsmainfiltervalue.push($cvalue);
						$("#soundsfinalfiltercondvalue").val(soundsmainfiltervalue);
					});
					$("#soundsfilterfinalviewconid").val(value);
				} else {
					$("#soundsfinalfiltercondvalue").val(value);
					$("#soundsfilterfinalviewconid").val(value);
				}
			});
		}
		soundsfiltername = [];
		$("#soundsfilteraddcondsubbtn").click(function() {
			$("#soundsfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#soundsfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(soundsaddgrid,'sounds');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$('#groupcloseaddform').click(function(){
		window.location =base_url+'Callcampaign';
	});
});
//sound Add Grid
function soundsaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#soundsaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $('.soundsheadercolsort').hasClass('datasort') ? $('.soundsheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.soundsheadercolsort').hasClass('datasort') ? $('.soundsheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.soundsheadercolsort').hasClass('datasort') ? $('.soundsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#soundsviewfieldids').val();
	var filterid = $("#soundsfilterid").val();
	var conditionname = $("#soundsconditionname").val();
	var filtervalue = $("#soundsfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=sounds&primaryid=soundsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#soundsaddgrid').empty();
			$('#soundsaddgrid').append(data.content);
			$('#soundsaddgridfooter').empty();
			$('#soundsaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('soundsaddgrid');
			maingridresizeheightset('soundsaddgrid');
			{//sorting
				$('.soundsheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.soundsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#soundspgnumcnt li .page-text .active').data('rowcount');
					var sortpos = $('#soundsaddgrid .gridcontent').scrollLeft();
					soundsaddgrid(page,rowcount);
					$('#soundsaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('soundsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					soundsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#soundspgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					soundsaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#soundsaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#soundspgrowcount').material_select();
		},
	});
}
function crudactionenable() {
	//sound delete 
	$("#deleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#soundsaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			$("#soundsprimarydataid").val(datarowid);
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#basedeleteyes").click(function() {
		var datarowid = $("#soundsprimarydataid").val();
		if(datarowid) {
			soundsdelete(datarowid);
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#soundspgnum li.active').data('pagenum');
		var rowcount = $('ul#soundspgnumcnt li .page-text .active').data('rowcount');
		soundsaddgrid(page,rowcount);
	}
}
//for sms credential edit data fetch
function soundfileupload() {
	var soundsname= $("#soundsname").val();
	var filename = $('#filename').val();
	var filesize = $('#filename_size').val();
	var filetype = $('#filename_type').val();
	var filepath = $('#filename_path').val();
	var filefrom = $('#filename_fromid').val();
	var apikey = $("#callsettingsid").find('option:selected').data('apikey');
	var callsettingsid = $('#callsettingsid').val(); 
	$.ajax({
		url: base_url + "Sounds/soundsfileuploadindialstreet",
		data:'soundsname='+soundsname+"&filename="+filename+"&filesize="+filesize+"&filetype="+filetype+"&filepath="+filepath+"&basepath="+base_url+"&filefrom="+filefrom+"&callsettingsid="+callsettingsid+"&apikey="+apikey,
		type:'POST',
		async:false,
		cache:false,
        success: function(data) {
        	var nmsg =  $.trim(data);
            if (nmsg == 'TRUE'){
            	refreshgrid();
				resetFields();
            	$("#groupsectionoverlay").addClass("closed");
    			$("#groupsectionoverlay").removeClass("effectbox");
				alertpopup('Record Inserted Successfully');
            }
		},
	});
}
//sounds delete
function soundsdelete(ids) {
	$.ajax({
		url: base_url + "Sounds/soundsdelete",
		data:"primaryid="+ids,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	refreshgrid();
				resetFields();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Record Deleted Successfully');
			}
		},
	});
}
{//sms settings name drop down reload
	function callsettingsddload(ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Sounds/callsettingsddload",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-apikey ='" +data[m]['apikey']+ "'  data-name ='" +data[m]['dataname']+ "' value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}