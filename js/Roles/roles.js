$(document).ready(function()
{	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		rolesaddgrid();
		usersaddgrid();
		moduleaddgrid();
		fieldsaddgrid();
		fieldvaluesaddgrid();
		submoduleaddgrid();
		gridsettings();
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$("#groupcloseaddform").addClass('hidedisplay');
	$("#profileicon").click(function(){
		window.location =base_url+'Profile';
	});
	{
		$("#roleaddicon").click(function(){
			$("#rolessectionoverlay").removeClass("closed");
			$("#rolessectionoverlay").addClass("effectbox");
			$("#roleupdatebtn").hide();
			$("#rolesubmitbtn").show();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{
		$("#useraddicon").click(function(){
			$("#usersectionoverlay").removeClass("closed");
			$("#usersectionoverlay").addClass("effectbox");
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{
		$("#rolecancelbutton").click(function(){
			$("#rolessectionoverlay").addClass("closed");
			$("#rolessectionoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{
		$("#usercancelbutton").click(function(){
			$("#usersectionoverlay").addClass("closed");
			$("#usersectionoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	$("#roleediticon").click(function(){
		var datarowid = $('#rolesaddgrid').jqGrid('getGridParam', 'selrow');
		if (datarowid) {
			resetFields();
			$("#rolesubmitbtn").hide();
			$("#roleupdatebtn").show();
			rolesddeload(datarowid);
			roleseditdatafetchfun(datarowid);
			$("#rolessectionoverlay").removeClass("closed");
			$("#rolessectionoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			//For Touch
			masterfortouch = 1;
			//keyboard Shortcut
			saveformview=1;
			if(datarowid=='2') {
				$('#parentrolediv').addClass('hidedisplay');
				$('#parentroleid').removeClass('validate[required]');
			} else {
				$('#parentrolediv').removeClass('hidedisplay');
				$('#parentroleid').addClass('validate[required]');
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	//role delete
	$("#roledeleteicon").click(function(){
		var datarowid = $('#rolesaddgrid').jqGrid('getGridParam', 'selrow');
		if (datarowid) {
			if(datarowid != 2) {
				deleteroleddvalfetch(datarowid);
				$("#roledeleteformoverlay").fadeIn();
				$("#basedeleteyes").focus();
				$("#delroleid").val(datarowid);
			} else {
				alertpopup("You don't have any permission to delete this default Role");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#basedeleteyes").click(function(){
		$("#userroledeletevalidation").validationEngine('validate');
	});
	jQuery("#userroledeletevalidation").validationEngine({
		onSuccess: function() {
			var updateroleid = $('#conformroleid').val();
			var delroleid = $('#delroleid').val();
			roledeletefunction(delroleid,updateroleid);
		},
		onFailure: function() {
			var dropdownid =['1','conformroleid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//reload role grid
	$("#rolereloadicon").click(function(){
		clearform('cleardataform');
		clearinlinesrchandrgrid('rolesaddgrid');
		$("#rolesubmitbtn").show();
		$("#roleupdatebtn").hide();
		$("#roledeleteicon").show();
	});
	{// Validation for role Add
		$('#rolesubmitbtn').on('mousedown click',function(e){
			if(e.which == 1 || e.which === undefined) {
				$("#userroleaddvalidation").validationEngine('validate');
			}
		});
		jQuery("#userroleaddvalidation").validationEngine({
			onSuccess: function() {
				rolenewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['2','profileid','parentroleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Validation for role Update  
		$('#roleupdatebtn').on('mousedown click',function(e){
			if(e.which==1 || e.which === undefined) {
				$("#userroleupdatevalidation").validationEngine('validate');
			}
		});
		jQuery("#userroleupdatevalidation").validationEngine({
			onSuccess: function() {
				roleupdatedataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				$('.ftab').trigger('click');
				alertpopup(validationalert);
			}
		});
	}
	//user tab - role drop down change grid reload
	$("#assignuserroleid").change(function() {
		usersaddgrid();
	});
	//user - multi select
	$("#userroleempid").change(function() {
		var data = $('#userroleempid').select2('data');
		var finalResult = [];
		for( item in $('#userroleempid').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#multiuserid").val(selectid);
		$('#userroleassidnupdatevalidationvalidation').validationEngine('hide');
		$("#s2id_userroleempid,.select2-results").removeClass('error');
		$("#s2id_userroleempid").find('ul').removeClass('error');
	});
	//user role submit button
	$("#userrolesubmitbutton").click(function(){
		$("#userroleassidnaddvalidation").validationEngine('validate');
	});
	jQuery("#userroleassidnaddvalidation").validationEngine({
		onSuccess: function() {
			userroleassignadddata();
		},
		onFailure: function() {
			var dropdownid =['1','userroleempid'];
			dropdownfailureerror(dropdownid);
			$("#s2id_userroleempid").find('ul').addClass('error');
			alertpopup(validationalert);
		}
	});
	//user edit icon
	$("#userediticon").click(function() {
		var datarowid = $('#usersaddgrid').jqGrid('getGridParam', 'selrow');
		if (datarowid) {
			$("#userreloadicon").hide();
			$("#userroleempid").select2('val',datarowid).trigger('change');
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#userreloadicon").click(function() {
		clearform('cleardataform');
		clearinlinesrchandrgrid('usersaddgrid');
	});
	//module tab
	$("#moduleroleid").change(function() {
		moduleaddgrid();
	});
	{//custom action overlay actions
		//more action overlay
		$('#moreactionbtn').click(function(){
			var datarowid = $('#moduleaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				//fetch selected module ids
				var allmodulevalueid = $('#moduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#moduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var moduleids = JSON.stringify(content);
				var customaction = $("#moduleaddgrid").jqGrid('getCell',datarowid,'customactions');
				if(customaction != ' ' && customaction != '') {
					if($('#moduleaddgrid .custom_cbox'+datarowid+' input').prop('checked')) {
						$('#modulegriddatarowid').val(datarowid);
						$.ajax({
							url:base_url+"Roles/customtoolbarinfoget",
							data:'datainformation=&toolbarids='+customaction+"&modids="+moduleids,
							type:'POST',
							dataType:'json',
							async:false,
							cache:false,
							success :function(datas) {
								$('.customactiondata').empty();
								$('.customactiondata').append(datas['datasets']);
								$('#modulegriddataactionid').val(datas['moduleaction']);
								setTimeout(function() {
									setTimeout(function(){
										if($("#customactionform .checkboxcusact[type='checkbox']").prop("checked")) {
											$('#checkuncheck').prop('checked',true);
										} else {
											$('#checkuncheck').prop('checked',false);
										}
									},100);
									$("#customtoolsoverlay").fadeIn();
								},100);
								//custom icon check/uncheck events
								$('.checkboxcusact').click(function(){
									if($("#customactionform .checkboxcusact[type='checkbox']").prop("checked")) {
										$('#checkuncheck').prop('checked',true);
									} else {
										$('#checkuncheck').prop('checked',false);
									}
								});
							}
						});
					} else {
						if($('#moduleaddgrid .custom_cbox').hasClass('custom_cbox'+datarowid+'')) {
							alertpopup("Please enable custom option");
						}
					}
				} else {
					alertpopup("Custom action(s) not exit");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//set custom action id 
		$('#custactionverlaybtn').click(function(){
			var rowid = $('#modulegriddatarowid').val();
			var n = jQuery(".checkboxcusact:checked").length;
			var usertoolbarid = new Array();
			if (n > 0) {
				$(".checkboxcusact:checked").each(function(){
					usertoolbarid.push($(this).val());
				});
			} else {
				$('#moduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			var usrtoolbarid = usertoolbarid.join(',');
			var modtoolbarid = $('#modulegriddataactionid').val();
			var custtoolbarids = usrtoolbarid+"||"+modtoolbarid;
			$("#moduleaddgrid").jqGrid('setCell',rowid,'customactions',custtoolbarids);
			setTimeout(function() {
				$("#customtoolsoverlay").fadeOut();
				$('#modulegriddatarowid').val('');
				$('#modulegriddataactionid').val('');
			},100);
		});
		//more action overlay close
		$('#cusoverlayclose').click(function(){
			var rowid = $('#modulegriddatarowid').val();
			var n = jQuery(".checkboxcusact:checked").length;
			if (n <= 0) {
				$('#moduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			$("#customtoolsoverlay").fadeOut();
			$('#modulegriddatarowid').val('');
		});
	}
	{//sub module process overlay
		//sub module list icon
		$('#moremoduleicon').click(function(){
			var datarowid = $('#moduleaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				var submodule = $("#moduleaddgrid").jqGrid('getCell',datarowid,'submodules');
				if(submodule == '*') {
					if($('#moduleaddgrid .moduleid_cbox'+datarowid+' input').prop('checked')) {
						$('#mastermodgridrowid').val(datarowid);
						$("#submoduleoverlay").fadeIn();
						var submoduledata = $("#moduleaddgrid").jqGrid('getCell',datarowid,'submodulesdata');
						var data = jQuery.parseJSON(submoduledata);
						if(submoduledata !== '' ) {
							$("#submoduleaddgrid").jqGrid("clearGridData", true);
							setTimeout(function(){
								$("#submoduleaddgrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
								for (var x = 0; x < data.length; x++) {
									console.log(data[x]);
									$("#submoduleaddgrid").addRowData(data[x]['modid'], data[x]);
								}
							},300);
						} else {
							submoduleaddgrid();
						}
					} else {
						alertpopup("Please enable module");
					}
				} else {
					alertpopup("Sub module(s) not exit");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//sub module overlay close
		$('#sbumodoverlayclose').click(function(){
			var rowid = $('#mastermodgridrowid').val();
			var achkcount = $('#submoduleaddgrid .moduleid_cbox input').length;
			var ascount = 0;
			var acount = 0;
			$('#submoduleaddgrid .moduleid_cbox input').each(function () {
				ascount = (this.checked ? ++acount : ascount);
			});
			if(ascount==0) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',false);
			}
			$("#submoduleoverlay").fadeOut();
			$("#submoduleaddgrid").jqGrid("clearGridData", true);
			$('#mastermodgridrowid').val('');
		});
		//sub module more action overlay
		$('#submodmoreactionbtn').click(function(){
			var datarowid = $('#submoduleaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				//fetch selected modude ids
				var allmodulevalueid = $('#moduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#moduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var moduleids = JSON.stringify(content);
				//fetch selected sub module ids
				var allsubmodulevalueid = $('#submoduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allsubmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#submoduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var submoduleids = JSON.stringify(content);
				var customaction = $("#submoduleaddgrid").jqGrid('getCell',datarowid,'customactions');
				if(customaction != ' ' && customaction != '') {
					if($('#submoduleaddgrid .custom_cbox'+datarowid+' input').prop('checked')) {
						$('#submodulegriddatarowid').val(datarowid);
						$.ajax({
							url:base_url+"Roles/subcustomtoolbarinfoget",
							data:'datainformation=&toolbarids='+customaction+"&submodids="+submoduleids+"&modids="+moduleids,
							type:'POST',
							dataType:'json',
							async:false,
							cache:false,
							success :function(datas) {
								$('.subcustomactiondata').empty();
								$('.subcustomactiondata').append(datas['datasets']);
								$('#submodulegriddataactionid').val(datas['moduleaction']);
								setTimeout(function() {
									setTimeout(function(){
										if($("#subcustomactionform .subcheckboxcusact[type='checkbox']").prop("checked")) {
											$('#subcheckuncheck').prop('checked',true);
										} else {
											$('#subcheckuncheck').prop('checked',false);
										}
									},100);
									$("#subcustomtoolsoverlay").fadeIn();
								},100);
								//sub module custom icon check/uncheck events
								$('.subcheckboxcusact').click(function(){
									if($("#subcustomactionform .subcheckboxcusact[type='checkbox']").prop("checked")) {
										$('#subcheckuncheck').prop('checked',true);
									} else {
										$('#subcheckuncheck').prop('checked',false);
									}
								});
							}
						});
					} else {
						if($('#submoduleaddgrid .custom_cbox').hasClass('custom_cbox'+datarowid+'')) {
							alertpopupdouble("Please enable custom option");
						}
					}
				} else {
					alertpopup("Custom action(s) not exit");
				}
			} else {
				alertpopupdouble("Please select a row");
			}
		});
		//sub module save
		$('#rolesubmodeulesbt').click(function(){
			var rowid = $('#mastermodgridrowid').val();
			var achkcount = $('#submoduleaddgrid .moduleid_cbox input').length;
			var ascount = 0;
			var acount = 0;
			$('#submoduleaddgrid .moduleid_cbox input').each(function () {
				ascount = (this.checked ? ++acount : ascount);
			});
			if(ascount!=0) {
				var gridData = jQuery("#submoduleaddgrid").jqGrid('getRowData');
				var submodulegriddata = JSON.stringify(gridData);
				$("#moduleaddgrid").jqGrid('setCell',rowid,'submodulesdata',submodulegriddata);
			} else {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',false);
			}
			$("#submoduleoverlay").fadeOut();
			$("#submoduleaddgrid").jqGrid("clearGridData", true);
			$('#mastermodgridrowid').val('');
		});
		//set sub module custom action
		$('#subcustactionverlaybtn').click(function(){
			var rowid = $('#submodulegriddatarowid').val();
			var n = jQuery("#subcustomactionform input:checkbox:checked").length;
			var usertoolbarid = new Array();
			if (n > 0){ 
				$("#subcustomactionform input:checkbox:checked").each(function(){
					usertoolbarid.push($(this).val());
				});
			} else {
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			var usrtoolbarid = usertoolbarid.join(',');
			var modtoolbarid = $('#submodulegriddataactionid').val();
			var custtoolbarids = usrtoolbarid+"||"+modtoolbarid;
			$("#submoduleaddgrid").jqGrid('setCell',rowid,'customactions',custtoolbarids);
			setTimeout(function() {
				$("#subcustomtoolsoverlay").fadeOut();
				$('#submodulegriddatarowid').val('');
				$('#submodulegriddataactionid').val('');
			},100);
		});
		//custom action sub module overlay close
		$('#subcusoverlayclose').click(function(){
			var rowid = $('#submodulegriddatarowid').val();
			var n = jQuery("#subcustomactionform input:checkbox:checked").length;
			if (n <= 0){
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			$("#subcustomtoolsoverlay").fadeOut();
			$('#submodulegriddatarowid').val('');
			$('#submodulegriddataactionid').val('');
		});
	}
	//role based module submit
	$("#rolemodeulesbt").click(function() {
		//module grid details
		var count = jQuery("#moduleaddgrid").jqGrid('getGridParam', 'records');
		var allmoduleid = $('#moduleaddgrid').jqGrid('getDataIDs');
		var gridData = {};
		var mid = '';
		var create = '';
		var read = '';
		var update = '';
		var cdelete = '';
		var custom = '';
		var caction = '';
		var submod = '';
		var submoddata = '';
		$.each(allmoduleid,function(index,value) {
			gridData[index] = {};
			gridData[index]['moduleid'] = {};
			gridData[index]['create'] = {};
			gridData[index]['read'] = {};
			gridData[index]['update'] = {};
			gridData[index]['delete'] = {};
			gridData[index]['custom'] = {};
			gridData[index]['customactions'] = {};
			gridData[index]['submodules'] = {};
			gridData[index]['submodulesdata'] = {};
			mid = $('#moduleaddgrid').jqGrid('getCell',value,'moduleid');
			create = $('#moduleaddgrid').jqGrid('getCell',value,'create');
			read = $('#moduleaddgrid').jqGrid('getCell',value,'read');
			update = $('#moduleaddgrid').jqGrid('getCell',value,'update');
			cdelete = $('#moduleaddgrid').jqGrid('getCell',value,'delete');
			custom = $('#moduleaddgrid').jqGrid('getCell',value,'custom');
			caction = $('#moduleaddgrid').jqGrid('getCell',value,'customactions');
			submod = $('#moduleaddgrid').jqGrid('getCell',value,'submodules');
			submoddata = $('#moduleaddgrid').jqGrid('getCell',value,'submodulesdata');
			gridData[index]['moduleid']=mid;
			gridData[index]['create']=create;
			gridData[index]['read']=read;
			gridData[index]['update']=update;
			gridData[index]['delete']=cdelete;
			gridData[index]['custom']=custom;
			gridData[index]['customactions']=caction;
			gridData[index]['submodules']=submod;
			gridData[index]['submodulesdata']=submoddata;
		});
		var modulegriddata = JSON.stringify(gridData);
		var roleid = $("#moduleroleid").val();
		var amp = '&';
		var datainfo = "modulegriddata="+modulegriddata+amp+"roleid="+roleid+amp+"allmoduleid="+allmoduleid;
		if(roleid != '') {
			$("#processoverlay").show();
			$.ajax({
				url:base_url+"Roles/rolemodulesubmit",
				data:'datainformation='+amp+datainfo,
				type:'POST',
				cache:false,
				success :function(msg) {
					setTimeout(function() {
						moduleaddgrid();
					},100);
					$("#processoverlay").hide();
					alertpopup('Data(s) stored successfully');
				}
			});
		} else {
			alertpopup('Please select the role');
		}
	});
	//role-module drop down chnage
	$("#fieldmoduleid").change(function() {
		var modid = $("#fieldmoduleid").val();
		var roleid = $("#fieldroleid").val();
		if(roleid != "") {
			fieldsaddgrid();
		} else {
			$("#fieldmoduleid").select2('val','');
			alertpopup('Please select the role');
		}
	});
	//field role change events
	$("#fieldroleid,#valueroleid").change(function() {
		var roleid = $(this).val();
		$('#fieldmoduleid,#valuemoduleid').empty();
		$('#fieldmoduleid,#valuemoduleid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Roles/fetchrolebasedmodulefetch?rid='+roleid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					prev = ' ';
					$.each(data, function(index) {
						var cur = data[index]['cateid'];
						if(prev != cur ) {
							if(prev != " ") {
								$('#fieldmoduleid,#valuemoduleid').append($("</optgroup>"));
							}
							$('#fieldmoduleid,#valuemoduleid').append($("<optgroup label='"+data[index]['catename']+"' class='"+data[index]['catename']+"'>"));
							prev = data[index]['cateid'];
						}
						$("#fieldmoduleid optgroup[label='"+data[index]['catename']+"']")
						.append($("<option></option>")
						.attr("data-moduleid",data[index]['datasid'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
						
						$("#valuemoduleid optgroup[label='"+data[index]['catename']+"']")
						.append($("<option></option>")
						.attr("data-moduleid",data[index]['datasid'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
			},
		});
	});
	//role- module based field submit(active - readonly)
	$("#rolefieldsbtbtn").click(function() {
		var count = jQuery("#fieldsaddgrid").jqGrid('getGridParam', 'records');
		var gridData = jQuery("#fieldsaddgrid").jqGrid('getRowData');
		var roleid = $("#fieldroleid").val();
		var moduleid = $("#fieldmoduleid").val();
		var allmodulevalueid = $('#fieldsaddgrid').jqGrid('getDataIDs');
		var mf = '';
		var mi = '';
		var ac = '';
		var ro = '';
		var content = {};
		$.each(allmodulevalueid,function(index,value) {
			content[index] = {};
			content[index]['modulefieldid'] = {};
			content[index]['moduleid'] = {};
			content[index]['active'] = {};
			content[index]['visible'] = {};
			content[index]['readonly'] = {};
			mf = $('#fieldsaddgrid').jqGrid('getCell',value,'modulefieldid');
			mi = $('#fieldsaddgrid').jqGrid('getCell',value,'moduleid');
			ac = $('#fieldsaddgrid').jqGrid('getCell',value,'active');
			vi = $('#fieldsaddgrid').jqGrid('getCell',value,'visible');
			ro = $('#fieldsaddgrid').jqGrid('getCell',value,'readonly');
			content[index]['modulefieldid']=mf;
			content[index]['moduleid']=mi;
			content[index]['active']=ac;
			content[index]['visible']=vi;
			content[index]['readonly']=ro;
		});
		var fieldgriddata = JSON.stringify(content);
		var amp = '&';
		var datainfo = "fieldgriddata="+fieldgriddata+amp+"roleid="+roleid;
		if(roleid != '' && moduleid != '') {
			$("#processoverlay").show();
			$.ajax({
				url:base_url+"Roles/rolemodulefieldsubmitbtn",
				type:'POST',
				data:"data="+amp+datainfo,
				cache:false,
				success :function(msg) {
					setTimeout(function() {
						fieldsaddgrid();
					},100);
					$("#processoverlay").hide();
					alertpopup('Data(s) stored successfully');
				}
			});
		} else {
			alertpopup('Please select the user role and module');
		}
	});
	//drop down values grid reload
	$("#valuefieldid").change(function() {
		var val = $("#valuefieldid").val();
		if(val != '') {
			fieldvaluesaddgrid();
		}
	});
	{//values field drop down value fetch
		$("#valuemoduleid").change(function() {
			var modid = $("#valuemoduleid").val();
			var roleid = $("#valueroleid").val();
			if(modid != "" && roleid != "") {
				valuedropdownload(roleid,modid);
				fieldvaluesaddgrid();
			} else {
				$("#valuefieldid").empty();
				$("#valuemoduleid").select2('val','');
				alertpopup('Please select the role name');
			}
		});
		//value role id
		$('#valueroleid').change(function(){
			$("#valuefieldid").empty();
			$("#valuefieldid").select2('val','');
			$("#valuemoduleid").select2('val','');
			fieldvaluesaddgrid();
		});
		//value role id
		$('#fieldroleid').change(function(){
			$("#fieldmoduleid").select2('val','');
			fieldsaddgrid();
		});
	}
	//drop down value set to user role
	$("#rolevaluesubmit").click(function() {
		var roleid = $("#valueroleid").val();
		var modulefield = $("#valuefieldid").val();
		var modid = $("#valuefieldid").find('option:selected').data('moduletabid');
		var count = jQuery("#fieldvaluesaddgrid").jqGrid('getGridParam', 'records');
		var gridData = jQuery("#fieldvaluesaddgrid").jqGrid('getRowData');
		var allmodulevalueid = $('#fieldvaluesaddgrid').jqGrid('getDataIDs');
		var val = '';
		var content = {};
		$.each(allmodulevalueid,function(index,value) {
			content[index] = {};
			content[index]['fieldid'] = {};
			content[index]['visible'] = {};
			val = $('#fieldvaluesaddgrid').jqGrid('getCell',value,'visible');
			content[index]['fieldid']=value;
			content[index]['visible']=val;
		});
		var fieldgriddata = JSON.stringify(content);
		var fieldvalue = $("#valuefieldid").val();
		var tableid = $("#valuefieldid").find('option:selected').data('columnname');
		if(fieldvalue != '') {
			$("#processoverlay").show();
			var amp = '&';
			var datainformation = "roleid="+roleid+amp+"modid="+modid+amp+"modulefield="+modulefield+amp+"fieldgriddata="+fieldgriddata+amp+"tableid="+tableid;
			$.ajax({
				url: base_url + "Roles/modulevaluesubmit",
				type:'POST',
				data:"data="+amp+datainformation,
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						setTimeout(function(){
						fieldvaluesaddgrid();
						},100);
						$("#processoverlay").hide();
						alertpopup('Data(s) stored successfully');
					}
				},
			});
		} else {
			alertpopup('Please select the field value');
		}
	});
	//user role tab
	$("#tab1").click(function() {
		// To View the Content Area 
		$('#gview_rolesaddgrid').css('opacity','0');
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
		rolesaddgrid();
		//for touch
		masterfortouch = 0;
		mastertabid = 1;
	});
	$("#tab2").click(function() {
		$("#assignuserroleid").select2('val','');
		// To View the Content Area 
		$('#gview_usersaddgrid').css('opacity','0');
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
		$('#usersearchicon').hide();
		rolesddeload('1');
		setTimeout(function() {
			usersaddgrid();
		}, 20);
		//for touch
		masterfortouch = 0;
		mastertabid = 2;
	});
	//user role module tab
	$("#tab3").click(function() {
		$("#moduleroleid").select2('val','');
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
		moduleaddgrid();
		//for touch
		masterfortouch = 0;
		mastertabid = 3;
	});
	//user role filed tab
	$("#tab4").click(function() {
		$("#fieldroleid,#fieldmoduleid").select2('val','');
		$(".tabclass").addClass('hidedisplay');
		$("#tab4class").removeClass('hidedisplay');
		fieldsaddgrid();
		//for touch
		masterfortouch = 0;
		mastertabid = 4;
	});
	//user role filed val tab
	$("#tab5").click(function() {
		$("#valueroleid,#valuefieldid,#valuemoduleid").select2('val','');
		$(".tabclass").addClass('hidedisplay');
		$("#tab5class").removeClass('hidedisplay');
		// To View the Content Area 
		$('#gview_fieldvaluesaddgrid').css('opacity','0');
		setTimeout(function ()  {
			fieldvaluesaddgrid();
		},100);
		//for touch
		masterfortouch = 0;
		mastertabid = 5;
	});
	{
		//custom action overlay
		$('#checkuncheck').click(function(){
			if($('#checkuncheck').prop('checked')) {
				$('.checkboxcusact').prop('checked',true);
			} else {
				$('.checkboxcusact').prop('checked',false);
			}
		});
		//sub module custom action overlay
		$('#subcheckuncheck').click(function(){
			if($('#subcheckuncheck').prop('checked')) {
				$('.subcheckboxcusact').prop('checked',true);
			} else {
				$('.subcheckboxcusact').prop('checked',false);
			}
		});
	}
	{//refresh icons actions
		//module grid reload & role reset
		$('#rolemodreload').click(function(){
			$('#moduleroleid').select2('val','').trigger('change');
		});
		//module field grid reload, roles & module reset
		$('#rolefieldreload').click(function(){
			$("#fieldroleid").select2('val','').trigger('change');
		});
		//module values grid reload, roles,module & field reset
		$('#rolefieldvalreload').click(function(){
			$("#valueroleid").select2('val','').trigger('change');
		});
	}
	$("#profileicon").removeClass('hidedisplay');
	$("#rolemodsearchicon").addClass('hidedisplay');
	
});

//Roles Add Grid
function rolesaddgrid() {
	$('#rolesaddgrid').jqGrid('GridUnload');
	$("#rolesaddgrid").jqGrid({
		url:base_url + "Roles/userrolegriddatafetch",
		datatype: "xml",
		colNames:['S No','Parent Role','Roles Name','Profile Name','Description','Status'],
   	    colModel:
        [
			{name:'userroleid',index:'userrole.userroleid', width:200,hidden:true}, 
			{name:'parentrole',index:'userrole.userrolename', width:200},
			{name:'rolename',index:'userrole.userrolename', width:200},
			{name:'profilename',index:'profile.profilename', width:200},
			{name:'description',index:'userrole.description', width:200},
			{name:'statusname',index:'status.statusname', width:200}
		],
		pager:'#rolesaddgridnav',
		sortname:'userroleid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',				
	});
	//Inner Grid Height and Width Set Based On Screen Size
	innergridwidthsetbasedonscreen('rolesaddgrid');
	formwithgridclass("rolesaddgrid");
	var rolesistb = ["1","rolesaddgrid"];
	createistb(rolesistb);
	$(".ui-pg-input").attr('readonly',true);
}
//Modules Add Grid
function usersaddgrid() {
	var userroleid = $("#assignuserroleid").val();
	userroleid = ((userroleid!='') ? userroleid : 1);
	$('#usersaddgrid').jqGrid('GridUnload');
	$("#usersaddgrid").jqGrid({
		url:base_url+'Roles/rolebasedusernameshow?userroleid='+userroleid,
		datatype: "xml",
		colNames:['Employee Number','User Name','User LastName','User Role','Status'],
		colModel:
		[
			{name:'employeeid',index:'employeeid', width:1,hidden:true}, 
			{name:'employeename',index:'employeename', width:100},
			{name:'lastname',index:'lastname', width:100},
			{name:'userrolename',index:'userrolename', width:100},
			{name:'statusname',index:'statusname', width:1,hidden:true}
		],
		pager:'#usersaddgridnav',
		sortname:'employeename',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
	});
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('usersaddgrid');
	formwithgridclass("usersaddgrid");
	var usrrolesistb = ["1","usersaddgrid"];
	createistb(usrrolesistb);
	$(".ui-pg-input").attr('readonly',true);
}
//Module Add Grid
function moduleaddgrid() {
	var roleid = $("#moduleroleid").val();
	roleid = ( (roleid == "")? "1" : roleid );
	$('#moduleaddgrid').jqGrid('GridUnload');
	$("#moduleaddgrid").jqGrid({
		url:base_url+'Roles/rolebasedmodulelist?roleid='+roleid,
		datatype: "xml",
		colNames:['Menu Category','Module Name','<input type="checkbox" id="moduleid_cnbox" /> Module Visible','<input type="checkbox" id="create_cnbox" /> Create','<input type="checkbox" id="read_cnbox" /> View','<input type="checkbox" id="update_cnbox" />  Edit','<input type="checkbox" id="delete_cnbox" /> Delete','<input type="checkbox" id="custom_cnbox" /> Custom','Status','Custom Actions','Sub Module','Sub Module Data'],
   	    colModel:
        [
			{name:'menucategoryname',index:'menucategory.menucategoryname', sortable: false, width:150},
			{name:'modulename',index:'module.modulename', sortable: false, width:150},
			{name:'moduleid',index:'moduleid', editable: true, search: false, classes:'moduleid_cbox', sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'moduleid');}, width:30 },
			{name:'create',index:'', editable: true, search: false, classes:'create_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'create');}, width:30 },
			{name:'read',index:'', editable: true, search: false, classes:'read_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'read');}, width:30 },
			{name:'update',index:'', editable: true, search: false, classes:'update_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'update');}, width:30 },
			{name:'delete',index:'', editable: true, search: false, classes:'delete_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'delete');}, width:30 },
			{name:'custom',index:'', editable: true, search: false, classes:'custom_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'custom');}, width:30 },
			{name:'status',index:'statusname', align:"left", sortable: false, search: false, width:30 },
			{name:'customactions',index:'customactions', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'customactions');},hidden:true, width:30},
			{name:'submodules',index:'submodules', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'submodules');},hidden:true, width:30},
			{name:'submodulesdata',index:'submodulesdata', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'submodulesdata');},hidden:true, width:30}
		],
		pager:'#moduleaddgridnav',
		sortname:'moduleid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:20000,
		rowList:[1000,2000],
		loadComplete: function() {
			//visible
			$("th#moduleaddgrid_moduleid input").unbind('click');
			$("th#moduleaddgrid_moduleid input").click(function(e){
				if( $("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked') ) {
					$('#moduleaddgrid .moduleid_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'moduleid');
				} else {
					$('#moduleaddgrid .moduleid_cbox input').prop('checked',false);
					checkuncheckheaderandrowfield(false,'moduleid');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//create
			$("th#moduleaddgrid_create input").unbind('click');
			$("th#moduleaddgrid_create input").click(function(e){
				if( $("th#moduleaddgrid_create input#create_cnbox").prop('checked') ) {
					$('#moduleaddgrid .create_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'create');
				} else {
					$('#moduleaddgrid .create_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
				
			});
			//view
			$("th#moduleaddgrid_read input").unbind('click');
			$("th#moduleaddgrid_read input").click(function(e){
				if( $("th#moduleaddgrid_read input#read_cnbox").prop('checked') ) {
					$('#moduleaddgrid .read_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'view');
				} else {
					$('#moduleaddgrid .read_cbox input').prop('checked',false);
					checkuncheckheaderandrowfield(false,'view');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Edit
			$("th#moduleaddgrid_update input").unbind('click');
			$("th#moduleaddgrid_update input").click(function(e){
				if( $("th#moduleaddgrid_update input#update_cnbox").prop('checked') ) {
					$('#moduleaddgrid .update_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'edit');
				} else {
					$('#moduleaddgrid .update_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Delete
			$("th#moduleaddgrid_delete input").unbind('click');
			$("th#moduleaddgrid_delete input").click(function(e){
				if( $("th#moduleaddgrid_delete input#delete_cnbox").prop('checked') ) {
					$('#moduleaddgrid .delete_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'delete');
				} else {
					$('#moduleaddgrid .delete_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Custom
			$("th#moduleaddgrid_custom input").unbind('click');
			$("th#moduleaddgrid_custom input").click(function(e){
				if( $("th#moduleaddgrid_custom input#custom_cnbox").prop('checked') ) {
					$('#moduleaddgrid .custom_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'custom');
				} else {
					$('#moduleaddgrid .custom_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			$('#moduleaddgrid .moduleid_cbox input').click(function(){ //visible
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			});
			$('#moduleaddgrid .create_cbox input').click(function(){//create
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			});
			$('#moduleaddgrid .read_cbox input').click(function(){//view
				chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			});
			$('#moduleaddgrid .update_cbox input').click(function(){//Edit
				chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			});
			$('#moduleaddgrid .delete_cbox input').click(function(){//Delete
				chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			});
			$('#moduleaddgrid .custom_cbox input').click(function(){//Custom
				chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			});
			//remove checked attribute of restricted fields
			$('#moduleaddgrid .cbox_ronly input').prop('checked',false);
			//row check check/uncheck events
			//moduleid
			$('#moduleaddgrid .moduleid_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked')) {
					$("#moduleaddgrid").setSelection(rowid, true);
					var submodule = $("#moduleaddgrid").jqGrid('getCell',rowid,'submodules');
					if(submodule == '*') {
						$('#moremoduleicon').trigger('click');
					}
					checkuncheckrowfield(true,'moduleid',rowid);
				} else {
					checkuncheckrowfield(false,'moduleid',rowid);
				}
			});
			//create
			$('#moduleaddgrid .create_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'create',rowid);
				} else {
					checkuncheckrowfield(false,'create',rowid);
				}
			});
			//view
			$('#moduleaddgrid .read_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .read_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'view',rowid);
				} else {
					checkuncheckrowfield(false,'view',rowid);
				}
			});
			//edit
			$('#moduleaddgrid .update_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'edit',rowid);
				} else {
					checkuncheckrowfield(false,'edit',rowid);
				}
			});
			//delete
			$('#moduleaddgrid .delete_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'delete',rowid);
				} else {
					checkuncheckrowfield(false,'delete',rowid);
				}
			});
			//custom
			$('#moduleaddgrid .custom_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked')) {
					$("#moduleaddgrid").setSelection(rowid, true);
					var customaction = $("#moduleaddgrid").jqGrid('getCell',rowid,'customactions');
					if(customaction != ' ' && customaction != '') {
						$('#moreactionbtn').trigger('click');
					}
					checkuncheckrowfield(true,'custom',rowid);
				} else {
					checkuncheckrowfield(false,'custom',rowid);
				}
			});
		}
	});
	//pagination remove
	$("#moduleaddgridnav_center .ui-pg-table").addClass('hidedisplay');	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('moduleaddgrid');
	formwithgridclass("moduleaddgrid");
	var modulesistb = ["1","moduleaddgrid"];
	createistb(modulesistb);
}
//Module Add Grid
function submoduleaddgrid() {
	var roleid = $("#moduleroleid").val();
	var moduleid = $("#mastermodgridrowid").val();
	roleid = ( (roleid == "")? "1" : roleid );
	moduleid = ( (moduleid == "")? "1" : moduleid );
	$('#submoduleaddgrid').jqGrid('GridUnload');
	$("#submoduleaddgrid").jqGrid({
		url:base_url+'Roles/rolebasedsubmodulelist?rid='+roleid+"&mid="+moduleid,
		datatype: "xml",
		colNames:['Moduleid','Menu Category','Module Name','<input type="checkbox" id="moduleid_cnbox" /> Module Visible','<input type="checkbox" id="create_cnbox" /> Create','<input type="checkbox" id="read_cnbox" /> View','<input type="checkbox" id="update_cnbox" />  Edit','<input type="checkbox" id="delete_cnbox" /> Delete','<input type="checkbox" id="custom_cnbox" /> Custom','Status','Custom Actions'],
   	    colModel:
        [
			{name:'modid',index:'modid', sortable: false,hidden:true},
			{name:'menucategoryname',index:'menucategoryname', sortable: false, width:150},
			{name:'modulename',index:'modulename', sortable: false, width:150},
			{name:'moduleid',index:'moduleid', editable: true, classes:'moduleid_cbox', sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'moduleid');}, width:50 },
			{name:'create',index:'', editable: true, classes:'create_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'create');}, width:30 },
			{name:'read',index:'', editable: true, classes:'read_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'read');}, width:30 },
			{name:'update',index:'', editable: true, classes:'update_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'update');}, width:30 },
			{name:'delete',index:'', editable: true, classes:'delete_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'delete');}, width:30 },
			{name:'custom',index:'', editable: true, classes:'custom_cbox', align:"left", sortable: false, editoptions: { value: "true:false"},formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'custom');}, width:30 },
			{name:'status',index:'statusname', align:"left", sortable: false, width:200 },
			{name:'customactions',index:'customactions', align:"left", sortable: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'customactions');},hidden:true, width:30 }
		],
		pager:'#submoduleaddgridnav',
		sortname:'moduleid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function() {
			//visible
			$("th#submoduleaddgrid_moduleid input").unbind('click');
			$("th#submoduleaddgrid_moduleid input").click(function(e){
				if( $("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .moduleid_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'moduleid');
				} else {
					$('#submoduleaddgrid .moduleid_cbox input').prop('checked',false);
					subcheckuncheckheaderandrowfield(false,'moduleid');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//create
			$("th#submoduleaddgrid_create input").unbind('click');
			$("th#submoduleaddgrid_create input").click(function(e){
				if( $("th#submoduleaddgrid_create input#create_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .create_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'create');
				} else {
					$('#submoduleaddgrid .create_cbox input').prop('checked',false);
					subcheckuncheckheaderandrowfield(false,'create');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//view
			$("th#submoduleaddgrid_read input").unbind('click');
			$("th#submoduleaddgrid_read input").click(function(e){
				if( $("th#submoduleaddgrid_read input#read_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .read_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'view');
				} else {
					$('#submoduleaddgrid .read_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Edit
			$("th#submoduleaddgrid_update input").unbind('click');
			$("th#submoduleaddgrid_update input").click(function(e){
				if( $("th#submoduleaddgrid_update input#update_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .update_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'edit');
				} else {
					$('#submoduleaddgrid .update_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Delete
			$("th#submoduleaddgrid_delete input").unbind('click');
			$("th#submoduleaddgrid_delete input").click(function(e){
				if( $("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .delete_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'delete');
				} else {
					$('#submoduleaddgrid .delete_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Custom
			$("th#submoduleaddgrid_custom input").unbind('click');
			$("th#submoduleaddgrid_custom input").click(function(e){
				if( $("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .custom_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'custom');
				} else {
					$('#submoduleaddgrid .custom_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			$('#submoduleaddgrid .moduleid_cbox input').click(function(){ //visible
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			});
			$('#submoduleaddgrid .create_cbox input').click(function(){//create
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			});
			$('#submoduleaddgrid .read_cbox input').click(function(){//view
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			});
			$('#submoduleaddgrid .update_cbox input').click(function(){//Edit
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			});
			$('#submoduleaddgrid .delete_cbox input').click(function(){//Delete
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			});
			$('#submoduleaddgrid .custom_cbox input').click(function(){//Custom
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			});
			//remove checked attribute of restricted fields
			$('#submoduleaddgrid .cbox_ronly input').prop('checked',false);
			//row check check/uncheck events
			//moduleid
			$('#submoduleaddgrid .moduleid_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked')) {
					$("#submoduleaddgrid").setSelection(rowid, true);
					setTimeout(function(){
						$('#submodmoreactionbtn').trigger('click');
					},50);
					subcheckuncheckrowfield(true,'moduleid',rowid);
				} else {
					subcheckuncheckrowfield(false,'moduleid',rowid);
				}
			});
			//create
			$('#submoduleaddgrid .create_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'create',rowid);
				} else {
					subcheckuncheckrowfield(false,'create',rowid);
				}
			});
			//view
			$('#submoduleaddgrid .read_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'view',rowid);
				} else {
					subcheckuncheckrowfield(false,'view',rowid);
				}
			});
			//edit
			$('#submoduleaddgrid .update_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'edit',rowid);
				} else {
					subcheckuncheckrowfield(false,'edit',rowid);
				}
			});
			//delete
			$('#submoduleaddgrid .delete_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'delete',rowid);
				} else {
					subcheckuncheckrowfield(false,'delete',rowid);
				}
			});
			//custom
			$('#submoduleaddgrid .custom_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked')) {
					$("#submoduleaddgrid").setSelection(rowid,true);
					setTimeout(function(){
						$('#submodmoreactionbtn').trigger('click');
					},50);
					subcheckuncheckrowfield(true,'custom',rowid);
				} else {
					subcheckuncheckrowfield(false,'custom',rowid);
				}
			});
		}
	});
	//pagination remove
	$("#submoduleaddgridnav_center .ui-pg-table").addClass('hidedisplay');	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('submoduleaddgrid');
	formwithgridclass("submoduleaddgrid");
}
//chk/unchk all module
function checkuncheckheaderandrowfield(option,fieldtype) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			$('#moduleaddgrid .read_cbox input').prop('checked',option);
			$('#moduleaddgrid .update_cbox input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#moduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#moduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#moduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#moduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'create':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .read_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_read input#read_cnbox").prop('checked',option);
			//row8
			break;
		case 'view':
			/*row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			break;*/
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			$('#moduleaddgrid .update_cbox input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#moduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#moduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#moduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'edit':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'delete':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'custom':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			break;
	}
}
//chk/unchk single row
function checkuncheckrowfield(option,fieldtype,rowid) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'create':
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			}
			break;
		case 'view':
			$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'edit':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'delete':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'custom':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			}
			break;
	}
}
//chk/unchk all sub module
function subcheckuncheckheaderandrowfield(option,fieldtype) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'create':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'view':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'edit':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'delete':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'custom':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
	}
}
//chk/unchk sub module single row
function subcheckuncheckrowfield(option,fieldtype,rowid) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'create':
			//row
			$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'view':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'edit':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'delete':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'custom':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
	}
}
//Field Add Grid
function fieldsaddgrid() {
	$('#fieldsaddgrid').jqGrid('GridUnload');
	var roleid = $("#fieldroleid").val();
	var modid = $("#fieldmoduleid").val();
	$("#fieldsaddgrid").jqGrid({
		url:base_url+'Roles/modulebasedfieldslist?roleid='+roleid+"&moduleid="+modid,
		datatype: "xml",
		colNames:['Field Id','Module Id','Field Name','<input type="checkbox" id="active_cnbox" /> Active','<input type="checkbox" id="visible_cnbox" /> Visible','<input type="checkbox" id="readonly_cnbox" /> Readonly','Status'],
		colModel:
        [
			{name:'modulefieldid',index:'modulefieldid',hidden:true},
			{name:'moduleid',index:'moduleid',hidden:true},
			{name:'fieldlabel',index:'fieldlabel', sortable: false},
			{name:'active',index:'', editable: true,classes:'active_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'active'); } },
			{name:'visible',index:'', editable: true,classes:'visible_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'visible'); } },
			{name:'readonly',index:'', editable: true,classes:'readonly_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'readonly'); } },
			{name:'status',index:'', align:"left", sortable: false},
		],
		pager:'#fieldsaddgridnav',
		sortname:'modulefieldid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function() {
			//Active
			$("th#fieldsaddgrid_active input").unbind('click');
			$("th#fieldsaddgrid_active input").click(function(e){
				if( $("th#fieldsaddgrid_active input#active_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .active_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .active_cbox input').prop('checked',false);
					$('th#fieldsaddgrid_active input#active_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Visible
			$("th#fieldsaddgrid_visible input").unbind('click');
			$("th#fieldsaddgrid_visible input").click(function(e){
				if( $("th#fieldsaddgrid_visible input#visible_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .visible_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .visible_cbox input').prop('checked',false);
					$('th#fieldsaddgrid_visible input#visible_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//read only
			$("th#fieldsaddgrid_readonly input").unbind('click');
			$("th#fieldsaddgrid_readonly input").click(function(e){
				if( $("th#fieldsaddgrid_readonly input#readonly_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .readonly_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .readonly_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_active','active_cnbox','active_cbox');
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_visible','visible_cnbox','visible_cbox');
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_readonly','readonly_cnbox','readonly_cbox');
			$('#fieldsaddgrid .active_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_active','active_cnbox','active_cbox');
			});
			$('#fieldsaddgrid .visible_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_visible','visible_cnbox','visible_cbox');
			});
			$('#fieldsaddgrid .readonly_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_readonly','readonly_cnbox','readonly_cbox');
			});
		}
	});
	//pagination remove
	$("#fieldsaddgridnav_center .ui-pg-table").addClass('hidedisplay');
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('fieldsaddgrid');
	formwithgridclass('fieldsaddgrid');
}
//Field values Add Grid
function fieldvaluesaddgrid() {
	var modulefieldid = $("#valuefieldid").val();
	var tableid = $("#valuefieldid").find('option:selected').data('columnname');
	var modid = $("#valuefieldid").find('option:selected').data('moduletabid');
	var roleid = $("#valueroleid").val();
	$('#fieldvaluesaddgrid').jqGrid('GridUnload');
	$("#fieldvaluesaddgrid").jqGrid({
		url:base_url+'Roles/modulefieldvalueget?tableid='+tableid+"&modid="+modid+"&roleid="+roleid+"&modulefieldid="+modulefieldid,
		datatype: "xml",
		colNames:['S No','Field Values','<input type="checkbox" id="visible_cnbox" /> Visible','Status'],
   	    colModel:
        [
			{name:'fieldid',index:'fieldid', width:1,hidden:true},
			{name:'fieldname',index:'fieldname', sortable: false},
			{name:'visible',index:'', editable: true, classes:'visible_cbox', sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'visible');} },
			{name:'status',index:'', align:"left", sortable: false}
		],
		pager:'#fieldvaluesaddgridnav',
		sortname:'fieldid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function() {
			//active
			$("th#fieldvaluesaddgrid_visible input").unbind('click');
			$("th#fieldvaluesaddgrid_visible input").click(function(e){
				if( $("th#fieldvaluesaddgrid_visible input#visible_cnbox").prop('checked') ) {
					$('#fieldvaluesaddgrid .visible_cbox input').prop('checked',true);
				} else {
					$('#fieldvaluesaddgrid .visible_cbox input').prop('checked',false);
					$('th#fieldvaluesaddgrid_visible input#visible_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('fieldvaluesaddgrid','fieldvaluesaddgrid_visible','visible_cnbox','visible_cbox');
			$('#fieldvaluesaddgrid .visible_cbox input').click(function(){
				chkunckkheader('fieldvaluesaddgrid','fieldvaluesaddgrid_visible','visible_cnbox','visible_cbox');
			});
		}
	});
	//pagination remove
	$("#fieldvaluesaddgridnav_center .ui-pg-table").addClass('hidedisplay');
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('fieldvaluesaddgrid');
	formwithgridclass("fieldvaluesaddgrid");
}
//checks to remove the restricted tool bar
function hidegridcell(rowId, cellValue, rawObject, cm, rdata,cellpoint) {
	if(rdata[cellpoint] == 'empty') {
		return result = ' class="cbox_ronly" style="opacity:0"';
	} else {
		return result = ' class="'+cellpoint+'_cbox '+cellpoint+'_cbox'+rowId+'" data-datacellrowid="'+rowId+'" ';
	}
}
//check/uncheck header check box
function chkunckkheader(gname,gdthname,hchkname,gdchkname) {
	var achkcount = $('#'+gname+' .'+gdchkname+' input').length;
	var ascount = 0;
	var acount = 0;
	$('#'+gname+' .'+gdchkname+' input').each(function () {
		ascount = (this.checked ? ++acount : ascount);
	});
	if( (achkcount==ascount) && (achkcount != '0') ) {
		$('th#'+gdthname+' input#'+hchkname+'').prop('checked',true);
	} else {
		$('th#'+gdthname+' input#'+hchkname+'').prop('checked',false);
	}
}
// Grid Height Width Settings
function gridsettings()
{
	formwithgridclass("rolesaddgrid");
	formwithgridclass("usersaddgrid");
	formwithgridclass("moduleaddgrid");
	formwithgridclass("fieldsaddgrid");
	formwithgridclass("fieldvaluesaddgrid");
	formwithgridclass("submoduleaddgrid");
	var rolesmodistb = ["6", "rolesaddgrid","usersaddgrid","moduleaddgrid","fieldsaddgrid","fieldvaluesaddgrid","submoduleaddgrid"];
	createistb(rolesmodistb);
}
{// New data add submit function
	function rolenewdataaddfun() {
		$("#processoverlay").show();
		var formdata = $("#userrolecreateionform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Roles/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#processoverlay").hide();
					resetFields();
					$("#rolesaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('rolesaddgrid');
					rolesddeload('1');
					$("#rolessectionoverlay").addClass("closed");
					$("#rolessectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
				} else {
					alertpopup('New roles information creation failed.');
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{//roles data fetch
	function roleseditdatafetchfun(datarowid) {
		$.ajax({
			url:base_url+"Roles/fetchformdataeditdetails?dataprimaryid="+datarowid, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var userparentrole = data['userparentrole'];
				var userrolename = data['userrolename'];
				var description = data['description'];
				var profileid = data['profileid'];
				$("#parentroleid").select2('val',userparentrole).trigger('change');
				$("#profileid").select2('val',profileid).trigger('change');
				$("#rolename").val(userrolename);
				$("#roledescription").val(description);
				$("#primaryid").val(datarowid);
			}
		});	
	}
}
{//role update data function
	function roleupdatedataaddfun() {
		var formdata = $("#userrolecreateionform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Roles/dataupdatefunction",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$("#rolesaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('rolesaddgrid');
					$("#rolesubmitbtn,#roledeleteicon").show();
					$("#roleupdatebtn").hide();
					rolesddeload('1');
					$("#rolessectionoverlay").addClass("closed");
					$("#rolessectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
					//For Touch
					masterfortouch = 0;
					//keyboard shortcut variable
					saveformview=0;
					$('#parentrolediv').removeClass('hidedisplay');
					$('#parentroleid').addClass('validate[required]');
				}
			},
		});
	}
}
{//role delete function
	function roledeletefunction(datarowid,updateroleid) {
		$.ajax({
			url: base_url + "Roles/roledeletefunction?primaryid="+datarowid+"&updateroleid="+updateroleid,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$("#rolesaddgrid").trigger('reloadGrid');
					clearinlinesrchandrgrid('rolesaddgrid');
					$("#roledeleteformoverlay").fadeOut();
					rolesddeload('1');
					alertpopup(savealert);
				}
			},
		});	
	}
}
//field name drop down
function fielddropdownloadfunction(userroleid) {
	$('#userroleempid').empty();
	$('#userroleempid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Roles/fetchdddatawithcond?userroleid='+userroleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#userroleempid')
					.append($("<option></option>")
					.attr("data-id",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#userroleempid').trigger('change');
		},
	});
}
{//user role assign add
	function userroleassignadddata() {
		var formdata = $("#userroleassignform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var roleid = $("#assignuserroleid").val();
		var userid = $("#multiuserid").val();
		if(roleid != "" && userid != "") {
			$.ajax({
				url: base_url + "Roles/userroleassignadd",
				data: "datas=" + datainformation+"&roleid="+roleid,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$("#usersaddgrid").trigger('reloadGrid');
						$("#userreloadicon").show();
						clearinlinesrchandrgrid('usersaddgrid');
						$("#usersectionoverlay").addClass("closed");
						$("#usersectionoverlay").removeClass("effectbox");
						alertpopup(savealert);
					} else if (nmsg == "false")  {
					}
				},
			});
		} else {
			alertpopup('Please select the role or user name');
		}
	}
}
{//dd reload
	function rolesddeload(roleid) {
		$('#parentroleid,#assignuserroleid,#moduleroleid,#fieldroleid,#valueroleid').empty();
		$('#parentroleid,#assignuserroleid,#moduleroleid,#fieldroleid,#valueroleid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Base/fetchdddataviewddval?dataname=userrolename&dataid=userroleid&datatab=userrole',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						if(roleid!=data[index]['datasid']) {
							$('#parentroleid,#assignuserroleid,#moduleroleid,#fieldroleid,#valueroleid')
							.append($("<option></option>")
							.attr("data-roleid",data[index]['datasid'])
							.attr("value",data[index]['datasid'])
							.text(data[index]['dataname']));
						}
					});
				}	
			},
		});
	}
}

//value drop down load
function valuedropdownload(roleid,modid) {
	$('#valuefieldid').select2('val','');
	$('#valuefieldid').empty();
	$('#valuefieldid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Roles/valuedropdownloadfun?roleid='+roleid+'&modid='+modid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#valuefieldid')
					.append($("<option></option>")
					.attr("data-columnname",data[index]['columnname'])
					.attr("data-moduletabid",data[index]['modtabid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#valuefieldid').trigger('change');
		},
	});
}
//profile unique name chk
function roleuniquenamechk() {
	var value = $('#rolename').val();
	var primaryid = $('#primaryid').val();
	var nmsg = "";
	if( value !="" ) {
		$.ajax({
			url:base_url+"Roles/roleuniquenamecheck",
			data:"data=&name="+value,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Role name already exists!";
				}
			} else {
				return "Role name already exists!";
			}
		}
	}
}
//delete role dd val fetch
function deleteroleddvalfetch(userroleid) {
	$('#conformroleid').empty();
	$('#conformroleid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Roles/deleteroleddvalfetch?userroleid='+userroleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#conformroleid')
					.append($("<option></option>")
					.attr("data-roleid",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}