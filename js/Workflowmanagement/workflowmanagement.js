$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	//rule criteria cond count
	rulecondcount=0;
	condgridload = 0;
	{//Grid Settings and calling
		workflowmanagementviewgrid();
		workflowmanagementinnergrid();
		firstfieldfocus();
	}
	$("#closeaddform").removeClass('hidedisplay');
	{
		{//Overlay Top Bar Code
			$(".mdl-stepper-step").click(function() {
				$(".mdl-stepper-step").removeClass('active-step');
				$(this).addClass('active-step');
				var subfomval = $(this).data('subform');
				$(".hiddensubform").hide();
				$("#subformspan"+subfomval+"").show();
			});
		}
		var wheight = $(window).height();
		var frmheight = wheight-150;
		$('.stepperformcontainer').css('height',frmheight+'px');
	}
	{
		//add workflow
		$("#addicon").click(function() {
			$(".fortrigger2").trigger("click");
			addslideup('workflowmanagementview','workflowform');
			resetFields();
			firstfieldfocus();
			//default values
			$('#ruleactivecls').prop('checked',true);
			$('#ruleactive').val('Yes');
			$('#recordacttrigtypeid').val('3');
			$('#fieldvalchktypeid').val('All');
			cleargriddata('workflowmanagementinnergrid');
			$("#forproppanelshow").hide();
			$('#alertsave').val('No');
			$('#tasksave').val('No');
			$('#fieldupdatesave').val('No');
			$('#rulealerttype').select('val','').trigger('change');
			$('#recordtriggertype').select2('val','1').trigger('change');
			$("#rulealertupdate").addClass('hidedisplay');
			$("#rulealertsave").removeClass('hidedisplay');
			$("#newrulemoduleid").attr('readonly',false);
			$("#recordtriggertype").attr('readonly',false);
			$("#tab1").trigger('click');
		});
		$("#editicon").click(function() {
			var datarowid = $('#workflowmanagementviewgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				$("#tab1").trigger('click');
				cleargriddata('workflowmanagementinnergrid');
				workfloweditdatafetch(datarowid);
				$("#rulealertupdate").removeClass('hidedisplay');
				$("#rulealertupdate").attr('style','');
				$("#rulealertsave").addClass('hidedisplay');
				$(".documentslogodownloadclsbtn").show();
				$('#parentaccount').attr('disabled','disabled');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//Activate workflow
		$("#enableicon").click(function() {
			var datarowid = $('#workflowmanagementviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var statusname = getgridcolvalue('workflowmanagementviewgrid',datarowid,'statusname','');
				if(statusname != 'Active') {
					workflowconfirmalertmsg('workflowenableyes','Activate the workflow?');
					$('#workflowenableyes').val('Enable');
					$("#workflowenableyes").click(function(){
						var datarowid = $('#workflowmanagementviewgrid div.gridcontent div.active').attr('id');
						workflowdataactive(datarowid);
						$('#workflowenableyes').unbind();
					});
				} else {
					alertpopup("Selected workflow already active!");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//Inactivate workflow
		$("#disableicon").click(function() {
			var datarowid = $('#workflowmanagementviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var statusname = getgridcolvalue('workflowmanagementviewgrid',datarowid,'statusname','');
				if(statusname == 'Active') {
					workflowconfirmalertmsg('workflowdeleteyes','Inactivate the workflow?');
					$('#workflowdeleteyes').val('Disable');
					$("#workflowdeleteyes").click(function(){
						var datarowid = $('#workflowmanagementviewgrid div.gridcontent div.active').attr('id');
						workflowdatainactive(datarowid);
						$('#workflowdeleteyes').unbind();
					});
				} else {
					alertpopup("Selected workflow already inactive!");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Close Add Screen
		var addclosedataimport = ["closeaddform","workflowmanagementview","workflowform",""];
	}
	//refresh
	$('#refresh').click(function() {
		refreshgrid();
	});
	{//Rule creation accordian
		$(".workflowaccclick").click(function() {
			$(this).find('.toggleicons').addClass('icon14-plus-square-o');
			$(this).find('.toggleicons').removeClass('icon14-minus-square-o');
			var fbuildercatlist = $(this).data('elementcat');
			if($(".elementlist"+fbuildercatlist+"").hasClass( "hideotherelements" )){
				$(".elementlist"+fbuildercatlist+"").slideUp( "slow", function() {
					$(".hideotherelements").removeClass('hideotherelements');
				});
			} else {
				$(this).find('.toggleicons').removeClass('icon14-plus-square-o');
				$(this).find('.toggleicons').addClass('icon14-minus-square-o');
				$(".hideotherelements").slideUp( "slow", function() {
					$('.workflowaccclick').find('.icon14-minus-square-o').removeClass('icon14-minus-square-o').addClass('icon14-plus-square-o');
					setTimeout(function(){
						var ff = $(".hideotherelements").prev('li').find('.icon14-plus-square-o').removeClass('icon14-plus-square-o').addClass('icon14-minus-square-o');
					},10);
					$(".hideotherelements").removeClass('hideotherelements');
				});
				$(".elementlist"+fbuildercatlist+"").slideDown( "slow", function() {
					$(".elementlist"+fbuildercatlist+"").addClass('hideotherelements');
				});
			}
		});
	}
	{//New rule creation validation
		$("#informationsbtn").click(function() {
			$('#ruleinfovalidate').validationEngine('validate');
		});
		$('#ruleinfovalidate').validationEngine({
			onSuccess:function() {
				var i= $('#recordtriggertype').val();
				if(i == "1") {
					$( ".fortrigger3" ).trigger( "click" );
					$('#recordaction').show();
					$('#datefieldvalue').hide();
					$("#tab2").trigger('click');
				} else if(i == "2") {
					$( ".fortrigger3" ).trigger( "click" );
					$('#recordaction').hide(); // hide the first one
					$('#datefieldvalue').show(); // show the other one
					$("#tab2").trigger('click');
				}
			},
			onFailure:function() {
				var dropdownid =['2','newrulemoduleid','recordtriggertype'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	$("#tab2").click(function() {
		var i= $('#recordtriggertype').val();
		var newrulemoduleid= $('#newrulemoduleid').val();
		var newrulename= $('#newrulename').val();
		if((!i) || (!newrulemoduleid) || (!newrulename)) {
			$("#tab1").trigger('click');
			alertpopup('Please select the required information');
		} else {
			$("#informationsbtn").trigger('click');
		}
	});
	$("#tab3").click(function() {
		var i= $('#recordtriggertype').val();
		var newrulemoduleid= $('#newrulemoduleid').val();
		var newrulename= $('#newrulename').val();
		if((!i) || (!newrulemoduleid) || (!newrulename)) {
			$("#tab1").trigger('click');
			alertpopup('Please select the required information');
		} else {
			$("#recordactionsbtn").trigger('click');
		}
	});
	$("#tab4").click(function() {
		var i= $('#recordtriggertype').val();
		var newrulemoduleid= $('#newrulemoduleid').val();
		var newrulename= $('#newrulename').val();
		if((!i) || (!newrulemoduleid) || (!newrulename)) {
			$("#tab1").trigger('click');
			alertpopup('Please select the required information');
		}/*  else {
			$("#alertnextbtn").trigger('click');
		} */
	});
	{// Function For Check box
		$('#ruleactivecls').click(function(){
			if ($(this).is(':checked')) {
				$('#ruleactive').val('Yes');
			} else {
				$('#ruleactive').val('No');
			}
		});
	}
	{//field action radio button events.
		$("input[name$='dataruleradiobtn']").click(function() {
			var i = $(this).val();
			if(i=="4") {
				$("#selectfields").show();
				$('#rulesdatafields').addClass(' validate[required]');
			} else if(i=='1' || i=="2" || i=="3" ||  i=="5") {
				$("#selectfields").hide();
				$('#rulesdatafields').removeClass(' validate[required]');
			}
		});
		//set the check box property on click of div
		$('.dataruleradiobtndiv').click(function() {
			var id = $(this).data('radid');
			$('#'+id+'').attr('checked',true);
			var i = $('#'+id+'').val();
			if(i=="4") {
				$("#selectfields").show();
				$('#rulesdatafields').addClass(' validate[required]');
			} else if(i=='1' || i=="2" || i=="3" ||  i=="5") {
				$("#selectfields").hide();
				$('#rulesdatafields').removeClass(' validate[required]');
			}
		});
	}
	{//recrd based rules create
		$("#recordactionsbtn").click(function() {
			$('#recordactionvalidate').validationEngine('validate');
		});
		$('#recordactionvalidate').validationEngine({
			onSuccess:function() {
				//field crud type chk
				var recacttrgtypeid = $('input[name=dataruleradiobtn]:checked','#recordbasdactionform').val();
				$('#recordacttrigtypeid').val(recacttrgtypeid);
				//field val chk type
				var fldvalchktype = $('input[name=fieldvalchktype]:checked','#recordbasdactionform').val();
				$('#fieldvalchktypeid').val(fldvalchktype);
				//Next tab
				$(".fortrigger4").trigger("click");
				$("#criteria").show();
				var condcnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
				if(condcnt==0) {
					$("#rulecrtandorconddivhid").hide();
					$('.condmandclass').text('');
					$('#rulecrtandorcond').removeClass('validate[required]');
				} else {
					$("#rulecrtandorconddivhid").show();
					$('.condmandclass').text('*');
					$('#rulecrtandorcond').addClass('validate[required]');
				}
				if(condgridload==0) {
					condgridload=1;
				}
				$("#tab3").trigger('click');
			},
			onFailure:function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//date based rules create
		$("#datefieldvaluesbtn").click(function() {
			$('#dateactionvalidate').validationEngine('validate');
		});
		$('#dateactionvalidate').validationEngine({
			onSuccess:function() {
				$(".fortrigger4").trigger("click");
				$("#criteria").show();
				$("#tab3").trigger('click');
			},
			onFailure:function() {
				var dropdownid =['3','datefieldids','datefldfrequencytype','datefldexecutiontype'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Rule criteria cond create
		$('#rulecritsubbtn').click(function() {
			$('#rulecriteriavalidate').validationEngine('validate');
		});
		$('#rulecriteriavalidate').validationEngine({
			onSuccess:function() {
				rulecondcount++;
				formtogriddata('workflowmanagementinnergrid','ADD','last','');
				clearform('rulecriteriacondclear');
				/* Data row select event */
				datarowselectevt();
				if(rulecondcount==1) {
					$('.andormandclass').text('*');
					$('#rulecrtandorcond').addClass('validate[required]');
					var dropdownid =['3','rulecrtfieldname','rulecrtfieldcond','rulecrtandorcond'];
					dropdownfailureerror(dropdownid);
					var condcnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#rulecrtandorconddivhid").hide();
						$('.andormandclass').text('');
						$('#rulecrtandorcond').removeClass('validate[required]');
					} else {
						$("#rulecrtandorconddivhid").show();
						$('.andormandclass').text('*');
						$('#rulecrtandorcond').addClass('validate[required]');
					}
				} else {
					var condcnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#rulecrtandorconddivhid").hide();
						$('.andormandclass').text('');
						$('#rulecrtandorcond').removeClass('validate[required]');
						var dropdownid =['2','rulecrtfieldname','rulecrtfieldcond'];
						dropdownfailureerror(dropdownid);
					} else {
						$("#rulecrtandorconddivhid").show();
						$('.andormandclass').text('*');
						$('#rulecrtandorcond').addClass('validate[required]');
						var dropdownid =['3','rulecrtfieldname','rulecrtfieldcond','rulecrtandorcond'];
						dropdownfailureerror(dropdownid);
					}
				}
			},
			onFailure:function() {
				var dropdownid =['4','rulecrtfieldname','rulecrtfieldcond','rulecrtandorcond','rulecrdddfieldval'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Rule send alert validation
		$('#rulesendalertsave').click(function(){
			$('#rulealertadddatavalidate').validationEngine('validate');
		});
		$('#rulealertadddatavalidate').validationEngine({
			onSuccess:function(){
				$(".fortrigger6").trigger("click");
				$('.rulecrateclass').removeClass('hidedisplay');
				$('#alertsave').val('Yes');
				$("#rulesendalertsave").show();
				$("#rulesendalertcancel").show();
				alertpopup('Send Alerts Created');
			},
			onFailure:function(){
				$('.rulecrateclass').addClass('hidedisplay');
				$('#alertsave').val('No');
				var dropdownid =['15','rulealerttype','rulealertemailtempid','rulealertemailrecipient','rulealertsmstemp','rulealertsmstransmode','rulealertsmssendid','rulealertsmstypeid','rulealertsmsrecep','rulealertcalltransmode','rulealertcallnumbertype','rulealertcallernumber','rulealertcalltemp','rulealertcallrecpnum','rulealertusertemp','rulealertuserrecep'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//cancel rule send alert
		$('#rulesendalertcancel').click(function(){
			clearform('rulealertdataclear');
			$(".fortrigger6").trigger("click");
			$('#rulealerttype').val('').trigger('change');
			$('#alertsave').val('No');
			$('.rulecrateclass').removeClass('hidedisplay');
			$("#rulesendalertcancel").show();
			$("#rulesendalertsave").show();
			alertpopup('Alerts was Cancelled');
		});
	}
	{//Rule Action assign task validation
		$('#ruleassigntasksave').click(function(){
			$('#ruletaskadddatavalidate').validationEngine('validate');
		});
		$('#ruletaskadddatavalidate').validationEngine({
			onSuccess:function(){
				$(".fortrigger7").trigger("click");
				$('.rulecrateclass').removeClass('hidedisplay');
				$('#tasksave').val('Yes');
				alertpopup('Task Created');
			},
			onFailure:function(){
				$('.rulecrateclass').addClass('hidedisplay');
				$('#tasksave').val('No');
				var dropdownid =['5','assigntaskduedate','assigntasktimetype','taskstatusid','taskpriorityid','assigntaskemployeeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//cancel rule assign task
		$('#ruleassigntaskcancel').click(function(){
			clearform('ruletaskdataclear');
			$('#tasksave').val('No');
			$(".fortrigger7").trigger("click");
			$('.rulecrateclass').removeClass('hidedisplay');
			alertpopup('Task Cancelled');
		});
		//Notify assignee checkbox events
		$('#tasknotifyassignee').click(function(){
			if ($(this).is(':checked')) {
				$('#tasknotifyassignee').val('Yes');
			} else {
				$('#tasknotifyassignee').val('No');
			}		
		});
	}
	{//Rule Action Update fields validation
		$('#ruleupdatefldssave').click(function(){
			$('#ruleupdatefieldadddatavalidate').validationEngine('validate');
		});
		$('#ruleupdatefieldadddatavalidate').validationEngine({
			onSuccess:function(){
				$('#fieldupdatesave').val('Yes');
				$(".fortrigger8").trigger("click");
				$('.rulecrateclass').removeClass('hidedisplay');
				alertpopup('Field Update was Created');
			},
			onFailure:function(){
				$('.rulecrateclass').addClass('hidedisplay');
				$('#fieldupdatesave').val('No');
				var dropdownid =['1','ruleupdatefieldname'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//cancel rule update fields
		$('#ruleupdatefldcancel').click(function(){
			clearform('ruleupdatefielddataclear');
			$('#fieldupdatesave').val('No');
			$(".fortrigger8").trigger("click");
			$('.rulecrateclass').removeClass('hidedisplay');
			alertpopup('Field Update was Cancelled');
		});
	}
	{//module change event
		$('#newrulemoduleid').change(function(){
			var modname = $('#newrulemoduleid').find('option:selected').data('modname');
			$('.alertmodsapn').text(modname);
			fieldnamelist();
			conditionfieldnamelist();
			assigntaskdudedatefieldnamelist();
		});
		//trigger type change event
		$('#recordtriggertype').change(function(){
			if($('#newrulemoduleid').val() != '') {
				fieldnamelist();
			}
		});
		//fields update fields count restriction
		$("#rulesdatafields").select2({
			maximumSelectionSize: 3
		});
		//to avoid this shrinking select2
		$('.select2-container').css('display','block');
		//record rule trig type radio event
		$('.datarulechkbtnclass').click(function(){
			//field crud type chk
			var recacttrgtypeid = $('input[name=dataruleradiobtn]:checked','#recordbasdactionform').val();
			$('#recordacttrigtypeid').val(recacttrgtypeid);
		});
		//record rule field update type
		$('.fldvalchkbtnclass').click(function(){
			//field val chk type
			var fldvalchktype = $('input[name=fieldvalchktype]:checked','#recordbasdactionform').val();
			$('#fieldvalchktypeid').val(fldvalchktype);
		});
		//execution type change event
		$('#datefldexecutiontype').change(function(){
			var typeval = $(this).val();
			if(typeval!='On' && typeval!='') {
				$('#afterbeforesapn').removeClass('hidedisplay');
				$('#afterbeforetime').val('');
			} else {
				$('#afterbeforesapn').addClass('hidedisplay');
				$('#afterbeforetime').val('');
			}
		});
		//frequency type change event
		$('#datefldfrequencytype').change(function() {
			var vals = ( ($(this).val()!='')?$(this).find('option:selected').data('type'):'' );
			$('#afterbeforelab').text(vals);
			var value = $(this).val();
			$('#repeateveryday').val(1);
			$('#weeklydiv,#monthlydiv').removeClass('hidedisplay');
			$('#recurrencedayid,#recurrencemonthdayid').select2('val','');
			if(value == 'Once' || value == 'Daily' || value == 'Yearly') {
				$('#weeklydiv,#monthlydiv').addClass('hidedisplay');
			} else if(value == 'Weekly') {
				$('#weeklydiv').removeClass('hidedisplay');
				$('#monthlydiv').addClass('hidedisplay');
			} else if(value == 'Monthly') {
				$('#monthlydiv').removeClass('hidedisplay');
				$('#weeklydiv').addClass('hidedisplay');
			}
		});
		{//time picker for time of exection
			$('#datefldexectiontime').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true
			});
		}
	}
	{//rule criteria condition
		$("#rulecrtfieldname").change(function(){
			var fieldlabel = $("#rulecrtfieldname").val();
			var uitype = $("#rulecrtfieldname").find('option:selected').data('uitype');
			var dataid = $("#rulecrtfieldname").find('option:selected').data('id');
			$('#rulecrtfieldnameid').val(dataid);
			$('#rulecrituitypeid').val(uitype);
			$('#rulecrdtimefieldval').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true
			});
			if(uitype == '2' || uitype == '3' || uitype == '4'|| uitype == '5' || uitype == '6'|| uitype == '7'|| uitype == '10' || uitype == '11' || uitype == '12'|| uitype == '14') {
				showhideintextbox('rulecrdddfieldval','rulecrdfieldval');
				$("#rulecrdddfieldvaldivhid,#rulecrddatefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdfieldvaldivhid").show();
				$("#rulecrdfieldval").val('');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]'); 
				$("#rulecrdfieldval").addClass('validate[required,maxSize[100]]'); 
				$("#rulecrdddfieldval").removeClass('validate[required]');
				$("#rulecrddatefieldval").removeClass('validate[required]');
				$('#rulecrdtimefieldval').timepicker('disable');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrdtimefieldval').removeClass('validate[required]');
			} else if(uitype == '17' || uitype == '19' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '23') {
				showhideintextbox('rulecrdfieldval','rulecrdddfieldval');
				$("#rulecrdfieldvaldivhid,#rulecrddatefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdddfieldvaldivhid").show();
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrdddfieldval").addClass('validate[required]');
				$("#rulecrddatefieldval").removeClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]'); 
				$("#rulecrddatefieldval").datepicker("disable");
				$('#rulecrdtimefieldval').timepicker('disable');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrdtimefieldval').removeClass('validate[required]');
				modulefiledpicklistvalfetch(dataid,uitype,'rulecrdddfieldval');
			} else if(uitype == '18' || uitype == '20' || uitype == '29') {
				showhideintextbox('rulecrdfieldval','rulecrdddfieldval');
				$("#rulecrddatefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdddfieldval").addClass('validate[required]');
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrddatefieldval").removeClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]');
				$("#rulecrddatefieldval").datepicker("disable");
				$('#rulecrdtimefieldval').timepicker('disable');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrdtimefieldval').removeClass('validate[required]');
				modfieldgrppicklistvalfetch(dataid,uitype,'rulecrdddfieldval');
			} else if(uitype == '13') {
				showhideintextbox('rulecrdfieldval','rulecrdddfieldval');
				$("#rulecrdfieldvaldivhid,#rulecrddatefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdddfieldvaldivhid").show();
				$("#rulecrddatefieldval").removeClass('validate[required]');
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrdddfieldval").addClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrdtimefieldval').removeClass('validate[required]');
				$("#rulecrddatefieldval").datepicker("disable");
				$('#rulecrdtimefieldval').timepicker('disable');
				checkboxvalueget('rulecrdddfieldval');
			} else if(uitype == '8') {
				showhideintextbox('rulecrdfieldval','rulecrddatefieldval');
				$("#rulecrdddfieldvaldivhid,#rulecrdtimefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrddatefieldvaldivhid").show();
				$('#rulecrddatefieldval').datetimepicker("destroy");
				$('#rulecrdtimefieldval').timepicker('disable');
				$('#rulecrddatefieldval').val('');
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrdddfieldval").removeClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]');
				$('#rulecrddatefieldval').addClass('validate[required]');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrdtimefieldval').removeClass('validate[required]');
				if(fieldlabel == 'Date of Birth' || fieldlabel == 'Start Date' || fieldlabel == 'End Date') {
					var dateformetdata = $('#rulecrddatefieldval').attr('data-dateformater');
					$('#rulecrddatefieldval').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: null,
						changeMonth: true,
						changeYear: true,
						maxDate:0,
						yearRange : '1947:c+100',
						onSelect: function () {
							var checkdate = $(this).val();
							if(checkdate != '') {
								$("#rulecritcondval").val(checkdate);
							}
						},
						onClose: function () {
							$('#rulecrddatefieldval').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				} else {
					var dateformetdata = $('#rulecrddatefieldval').attr('data-dateformater');
					$('#rulecrddatefieldval').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: 0,
						onSelect: function () {
							var checkdate = $(this).val();
							$("#rulecritcondval").val(checkdate);
						},
						onClose: function () {
							$('#rulecrddatefieldval').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				}
			} else if(uitype == '9') {
				showhideintextbox('rulecrdfieldval','rulecrdtimefieldval');
				$("#rulecrdddfieldvaldivhid").hide();
				$("#rulecrddatefieldvaldivhid").hide();
				$("#rulecrdddfieldvaldivhid,#rulecrddatefieldvaldivhid,#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdtimefieldvaldivhid").show();
				$('#rulecrdtimefieldval').val('');
				$('#rulecrddatefieldval').val('');
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrdddfieldval").removeClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]');
				$('#rulecrddatefieldval').removeClass('validate[required]');
				$('#rulecrdtimefieldval').addClass('validate[required]');	
				$('#rulecrdtimefieldval').timepicker({
					'step':15,
					'timeFormat': 'H:i',
					'scrollDefaultNow': true
				});
			} else {
				showhideintextbox('rulecrdddfieldval','rulecrdfieldval');
				$("#rulecrddatefieldvaldivhid").hide();
				$("#rulecrdtimefieldvaldivhid").hide();
				$("#rulecrdddfieldval").addClass('validate[required]');
				$('#rulecrdtimefieldval').timepicker('disable');
				$('#rulecrdtimefieldval').val('');
				$('#rulecrddatefieldval').val('');
				$("#rulecrdddfieldval").select2('val',"");
				$("#rulecrddatefieldval").removeClass('validate[required]');
				$("#rulecrdtimefieldval").removeClass('validate[required]');
				$("#rulecrdfieldval").removeClass('validate[required,maxSize[100]]');
				$("#rulecrddatefieldval").datepicker("disable");
				alertpopup('Please choose another field name');
			}
		});
		//rule criteria condition delete
		$('#rulecritconddel').click(function() {
			var ruleconrowid = $('#workflowmanagementinnergrid div.gridcontent div.active').attr('id');
 			if (ruleconrowid) {
				deletegriddatarow('workflowmanagementinnergrid',ruleconrowid);
				var condcnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
				if(ruleconrowid != '') {
					var did = $('#workflowconrowcolids').val();
					$('#workflowconrowcolids').val(did+','+ruleconrowid);
				}
				if(condcnt==0) {
					$('#rulecrtandorconddivhid').hide();
					$('.andormandclass').text('');
					$('#rulecrtandorcond').removeClass('validate[required]');
				} else {
					$('#rulecrtandorconddivhid').show();
					$('.andormandclass').text('*');
					$('#rulecrtandorcond').addClass('validate[required]'); 
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		{//rule criteria cond field value set
			$("#rulecrdddfieldval,#rulecrdfieldval,#rulecrdtimefieldval").change(function(){
				var value = $(this).val();
				$("#rulecritcondval").val(value);
			});
		}
	}
	{// Rule Action show/hide
		$("#alertnextbtn").click(function(){
			$(".fortrigger4").trigger("click");
			$("#forproppanelshow").slideDown();
			$('.rulecrateclass').removeClass('hidedisplay');
			$("#tab4").trigger("click");
		});
		$(".fortrigger6").click(function(){
			$("#alertullist").show();
		});
		$(".fortrigger7").click(function(){
			$("#assigntasks").slideDown();
		});
		$(".fortrigger8").click(function(){
			$("#updatefields").slideDown();
		});
	}
	{// Rule Action show/hide form fields
		$("#rulealerttype").change(function(){
			var i= $('#rulealerttype').val();
			if(i=="email") {
				$('#emailfields').show();
				$('#callfields').hide();
				$('#smsfields').hide();
				$('#desktopfields').hide();
				$('.mailmandclass').addClass('validate[required]');
				$('.smsmandclass').removeClass('validate[required]');
				$('.callmandclass').removeClass('validate[required]');
				$('.usrmandclass').removeClass('validate[required]');
				ruleactionfieldnamelist('rulealertemailrecipient','email');
				emailtemplateinformationfetch('rulealertemailtempid');
			} else if(i=="sms") {
				$('#desktopfields').hide();
				$('#emailfields').hide();
				$('#callfields').hide(); 
				$('#smsfields').show();
				$('.smsmandclass').addClass('validate[required]');
				$('.mailmandclass').removeClass('validate[required]');
				$('.callmandclass').removeClass('validate[required]');
				$('.usrmandclass').removeClass('validate[required]');
				ruleactionfieldnamelist('rulealertsmsrecep','sms');
			} else if(i=="call") {
				$('#desktopfields').hide();
				$('#emailfields').hide();
				$('#smsfields').hide();
				$('#callfields').show();
				$('.callmandclass').addClass('validate[required]');
				$('.mailmandclass').removeClass('validate[required]');
				$('.smsmandclass').removeClass('validate[required]');
				$('.usrmandclass').removeClass('validate[required]');
				ruleactionfieldnamelist('rulealertcallrecpnum','call');
			} else if(i=="desktop") {
				$('#emailfields').hide();
				$('#smsfields').hide();
				$('#callfields').hide();
				$('#desktopfields').show();
				$('.usrmandclass').addClass('validate[required]');
				$('.mailmandclass').removeClass('validate[required]');
				$('.smsmandclass').removeClass('validate[required]');
				$('.callmandclass').removeClass('validate[required]');
				ruleactionfieldnamelist('rulealertuserrecep','desktop');
				desktoptemplateinformationfetch('rulealertusertemp');
			} else {
				$('#desktopfields').hide();
				$('#emailfields').hide();
				$('#callfields').hide(); 
				$('#smsfields').hide();
			}
		});
	}
	{//sms template
		$('#rulealertsmstransmode,#rulealertsmssendid').change(function(){
			var modid = $('#newrulemoduleid').val();
			var smssendtype = $("#rulealertsmstransmode").val();
			var senderid = $("#rulealertsmssendid").find('option:selected').val();
			$('#rulealertsmstemp').empty();
			$('#rulealertsmstemp').append($("<option></option>"));
			if((senderid != null && smssendtype =='2') || smssendtype == '3' || smssendtype == '4') {
				$.ajax({
					url: base_url+ "Workflowmanagement/smstemplateddvalfetch?smssendtype="+smssendtype+"&senderid="+senderid,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							var newddappend="";
							var newlength=data.length;
							for(var m=0;m < newlength;m++) {
								newddappend += "<option data-moduleid="+data[m]['moduleid']+" data-typeid="+data[m]['typeid']+" value = '"+data[m]['datasid']+"'>"+data[m]['dataname']+" </option>";
							}
							$('#rulealertsmstemp').append(newddappend);
						}
						$('#rulealertsmstemp').trigger('change');
					},
				});
			} else {
				alertpopup('Please select the sender id or transaction mode');
			}
		});
		$("#rulealertsmstransmode").change(function(){
			var typeid = $("#rulealertsmstransmode").val();
			$('#rulealertsmssendid').empty();
			$('#rulealertsmssendid').append($("<option></option>"));
			$.ajax({
				url: base_url+"Workflowmanagement/smssenderddvalfetch?typeid="+typeid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$('#rulealertsmssendid')
							.append($("<option></option>")
							.attr("data-apikey",data[index]['apikey'])
							.attr("data-sendername",data[index]['dataname'])
							.attr("value",data[index]['datasid'])
							.text(data[index]['dataname']));
						});
					}	
				},
			});
		});
		//number type change event
		$('#rulealertcallnumbertype').change(function(){
			numbertype = $(this).val();
			$('#rulealertcallernumber').empty();
			$('#rulealertcallernumber').append($("<option></option>"));
			if(numbertype!='') {
				$.ajax({
					url: base_url+ "Workflowmanagement/callnumberfetch?numbertype="+numbertype,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							var newddappend="";
							var newlength=data.length;
							for(var m=0;m < newlength;m++) {
								newddappend += "<option value='"+data[m]['datasid']+"'>"+data[m]['dataname']+"</option>";
							}
							$('#rulealertcallernumber').append(newddappend);
						}
						$('#rulealertcallernumber').trigger('change');
					},
				});
			}
		});
		//call transaction mode change event
		$('#rulealertcalltransmode').change(function(){
			transmode = $(this).val();
			$('#rulealertcalltemp').empty();
			$('#rulealertcalltemp').append($("<option></option>"));
			if(transmode!='') {
				$.ajax({
					url: base_url+ "Workflowmanagement/calltemplatefetch?transactionmode="+transmode,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							var newddappend="";
							var newlength=data.length;
							for(var m=0;m < newlength;m++) {
								newddappend += "<option data-moduleid="+data[m]['moduleid']+" value='"+data[m]['datasid']+"'>"+data[m]['dataname']+"</option>";
							}
							$('#rulealertcalltemp').append(newddappend);
						}
						$('#rulealertcalltemp').trigger('change');
					},
				});
			}
		});
	}
	{//Rule Actions - Update fields
		$("#ruleupdatefieldname").change(function(){
			var fieldlabel = $("#ruleupdatefieldname").val();
			var uitype = $("#ruleupdatefieldname").find('option:selected').data('uitype');
			var dataid = $("#ruleupdatefieldname").find('option:selected').data('id');
			$('#ruleupdatefieldid').val(dataid);
			$('#ruleupdatefielduitype').val(uitype);
			if(uitype == '2' || uitype == '3' || uitype == '4'|| uitype == '5' || uitype == '6'|| uitype == '7'|| uitype == '10' || uitype == '11' || uitype == '12'|| uitype == '14') {
				showhideintextbox('ruleupdateddfieldval','ruleupdatefieldvalue');
				$("#ruleupdateddfieldvaldivhid,#ruleupdatetimevaluedivhid,#ruleupdatedatefieldvaldivhid").hide();
				$("#ruleupdatefieldvaluedivhid").show();
				$("#ruleupdatedatefieldvaldivhid").addClass('hidedisplay');
				$("#ruleupdatefieldvalue").val('');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#ruleupdatefieldvalue").addClass('validate[required,maxSize[100]]'); 
				$("#ruleupdateddfieldval").removeClass('validate[required]');
				$("#ruleupdatedatefieldval").removeClass('validate[required]');
			} else if(uitype == '17' || uitype == '19' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '23') {
				showhideintextbox('ruleupdatefieldvalue','ruleupdateddfieldval');
				$("#ruleupdatedatefieldvaldivhid").addClass('hidedisplay');
				$("#ruleupdatefieldvaluedivhid,#ruleupdatetimevaluedivhid,#ruleupdatedatefieldvaldivhid").hide();
				$("#ruleupdateddfieldvaldivhid").show();
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdateddfieldval").addClass('validate[required]');
				$("#ruleupdatedatefieldval").removeClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#ruleupdatedatefieldval").datepicker("disable");
				modulefiledpicklistvalfetch(dataid,uitype,'ruleupdateddfieldval');
			} else if(uitype == '18' || uitype == '20' || uitype == '29') {
				showhideintextbox('ruleupdatefieldvalue','ruleupdateddfieldval');
				$("#ruleupdatefieldvaluedivhid,#ruleupdatetimevaluedivhid,#ruleupdatedatefieldvaldivhid").hide();
				$("#ruleupdateddfieldvaldivhid").show();
				$("#ruleupdateddfieldval").addClass('validate[required]');
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdatedatefieldval").removeClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]');
				$("#ruleupdatedatefieldval").datepicker("disable");
				modfieldgrppicklistvalfetch(dataid,uitype,'ruleupdateddfieldval');
			} else if(uitype == '13') {
				showhideintextbox('ruleupdatefieldvalue','ruleupdateddfieldval');
				$("#ruleupdatefieldvaluedivhid,#ruleupdatetimevaluedivhid,#ruleupdatedatefieldvaldivhid").hide();
				$("#ruleupdateddfieldvaldivhid").show();
				$("#ruleupdatedatefieldval").removeClass('validate[required]');
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdateddfieldval").addClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]');
				$("#ruleupdatedatefieldval").datepicker("disable");
				checkboxvalueget('ruleupdateddfieldval');
			} else if(uitype == '8') {
				showhideintextbox('ruleupdatefieldvalue','ruleupdatedatefieldval');
				$("#ruleupdatefieldvaluedivhid,#ruleupdatetimevaluedivhid,#ruleupdateddfieldvaldivhid").hide();
				$("#ruleupdatedatefieldvaldivhid").show();
				$('#rulecrddatefieldval').datetimepicker("destroy");
				$('#ruleupdatedatefieldval').val('');
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdateddfieldval").removeClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]');
				$('#ruleupdatedatefieldval').addClass('validate[required]');				
				if(fieldlabel == 'Date of Birth' || fieldlabel == 'Start Date' || fieldlabel == 'End Date') {
					var dateformetdata = $('#ruleupdatedatefieldval').attr('data-dateformater');
					$('#ruleupdatedatefieldval').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: null,
						changeMonth: true,
						changeYear: true,
						maxDate:0,
						yearRange : '1947:c+100',
						onSelect: function () {
							var checkdate = $(this).val();
							if(checkdate != '') {
								$("#rulecritcondval").val(checkdate);
							}
						},
						onClose: function () {
							$('#ruleupdatedatefieldval').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				} else {
					var dateformetdata = $('#ruleupdatedatefieldval').attr('data-dateformater');
					$('#ruleupdatedatefieldval').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: 0,
						onSelect: function () {
							var checkdate = $(this).val();
							$("#rulecritcondval").val(checkdate);
						},
						onClose: function () {
							$('#ruleupdatedatefieldval').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				}
			} else if(uitype == '9') {
				showhideintextbox('ruleupdatefieldvalue','ruleupdatedatefieldval');
				$("#ruleupdateddfieldvaldivhid").hide();
				$("#ruleupdatefieldvaluedivhid,#ruleupdatedatefieldvaldivhid,#ruleupdateddfieldvaldivhid").hide();
				$("#ruleupdatetimevaluedivhid").show();
				$('#ruleupdatetimevalue').val('');
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdateddfieldval").removeClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]');
				$('#ruleupdatetimevalue').addClass('validate[required]');	
				$('#ruleupdatetimevalue').timepicker({
					'step':15,
					'timeFormat': 'H:i',
					'scrollDefaultNow': true
				});
			} else {
				showhideintextbox('ruleupdateddfieldval','ruleupdatefieldvalue');
				$("#ruleupdatedatefieldvaldivhid").hide();
				$("#ruleupdateddfieldval").addClass('validate[required]');
				$("#ruleupdateddfieldval").select2('val',"");
				$("#ruleupdatedatefieldval").removeClass('validate[required]');
				$("#ruleupdatefieldvalue").removeClass('validate[required,maxSize[100]]');
				$("#ruleupdatedatefieldval").datepicker("disable");
				alertpopup('Please choose another field name');
			}
		});
		{//rule action field update value set
			$("#ruleupdatedatefieldval,#ruleupdatefieldvalue,#ruleupdateddfieldval,#ruleupdatetimevalue").change(function(){
				var value = $(this).val();
				$("#fieldupdatevalue").val(value);
			});
		}
	}
	{//New creation
		$('#rulealertsave').click(function(){
			var amp = '&';
			var ruledata = $("#newruleinfoform").serialize();
			var recorddata = $("#recordbasdactionform").serialize();
			var datedata = $("#datebaseactionform").serialize();
			var alertdata = $("#rulealertaddform").serialize();
			var taskdata = $("#ruletaskaddform").serialize();
			var fieldupdatedata = $("#ruleupdatefieldaddform").serialize();
			//rule criteria cond
			var criteriacnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
			var griddata = getgridrowsdata('workflowmanagementinnergrid');
			var criteriagriddata = JSON.stringify(griddata);
			var datas = amp + ruledata + amp + recorddata + amp + datedata + amp + alertdata + amp + taskdata + amp + fieldupdatedata + amp + 'count='+criteriacnt + amp + 'critgriddata='+criteriagriddata;
			$.ajax({
				url:base_url+"Workflowmanagement/newworkflowcreate",
				data:"data="+datas,
				type:'post',
				async:false,
				cache:false,
				success: function(msg) {
					var smsg = $.trim(msg);
					if(smsg=='Success') {
						workalertpopup('Workflow created successfully.');
						resetFields();
						$('#workflowform').hide();
						$('#workflowmanagementview').fadeIn(1000);
						cleargriddata('workflowmanagementinnergrid');//workflowmanagementinnergrid
						workflowmanagementviewgrid();
					} else {
						workalertpopup('Workflow creation failed.');
						resetFields();
						$('#workflowform').hide();
						$('#workflowmanagementview').fadeIn(1000);
						cleargriddata('workflowmanagementinnergrid');
						workflowmanagementviewgrid();
					}
				},
			});
		});
	}
	function workalertpopup(txtmsg)	{
		$(".alertinputstyle").text(txtmsg);
		$('#alerts').fadeIn('fast');
		$("#alertsclose").focus();
	}
	//Update
	$('#rulealertupdate').click(function(){
		var amp = '&';
		var primaryid = $("#primaryid").val();
		var ruledata = $("#newruleinfoform").serialize();
		var recorddata = $("#recordbasdactionform").serialize();
		var datedata = $("#datebaseactionform").serialize();
		var alertdata = $("#rulealertaddform").serialize();
		var taskdata = $("#ruletaskaddform").serialize();
		var fieldupdatedata = $("#ruleupdatefieldaddform").serialize();
		//rule criteria cond
		var criteriacnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
		var griddata = getgridrowsdata('workflowmanagementinnergrid');
		var criteriagriddata = JSON.stringify(griddata);
		var condeleid = $("#workflowconrowcolids").val();
		var datas = amp + ruledata + amp + recorddata + amp + datedata + amp + alertdata + amp + taskdata + amp + fieldupdatedata + amp + 'count='+criteriacnt + amp + 'critgriddata='+criteriagriddata+"&primaryid="+primaryid+"&workflowconrowcolids="+condeleid;
		$.ajax({
			url:base_url+"Workflowmanagement/updateworkflowcreate",
			data:"data="+datas,
			type:'post',
			async:false,
			cache:false,
			success: function(msg) {
				var smsg = $.trim(msg);
				if(smsg=='Success') {
					workalertpopup('Workflow Updated successfully.');
					resetFields();
					$('#workflowform').hide();
					$('#workflowmanagementview').fadeIn(1000);
					cleargriddata('workflowmanagementinnergrid');
					workflowmanagementviewgrid();
				} else {
					workalertpopup('Workflow Updation failed.');
					resetFields();
					$('#workflowform').hide();
					$('#workflowmanagementview').fadeIn(1000);
					cleargriddata('workflowmanagementinnergrid');
					workflowmanagementviewgrid();
				}
			},
		});
	});
	//form close
	$("#closeaddform").click(function() {
		var addclosedataimport =["closeaddform","workflowmanagementview","workflowform",""];
		addclose(addclosedataimport);
	});
	//filter
	$("#workflowmanagementfilterddcondvaluedivhid").hide();
	$("#workflowmanagementfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#workflowmanagementfiltercondvalue").focusout(function(){
			var value = $("#workflowmanagementfiltercondvalue").val();
			$("#workflowmanagementfinalfiltercondvalue").val(value);
			$("#workflowmanagementfilterfinalviewconid").val(value);
		});
		$("#workflowmanagementfilterddcondvalue").change(function(){
			var value = $("#workflowmanagementfilterddcondvalue").val();
			workflowmanagementmainfiltervalue=[];
			$('#workflowmanagementfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    workflowmanagementmainfiltervalue.push($mvalue);
				$("#workflowmanagementfinalfiltercondvalue").val(workflowmanagementmainfiltervalue);
			});
			$("#workflowmanagementfilterfinalviewconid").val(value);
		});
	}
	workflowmanagementfiltername = [];
	$("#workflowmanagementfilteraddcondsubbtn").click(function() {
		$("#workflowmanagementfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#workflowmanagementfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(workflowmanagementviewgrid,'workflowmanagement');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
{// View Grid
	function workflowmanagementviewgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#workflowmanagementviewgrid').width();
		var wheight = $(window).height();
		/*col sort*/
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.workflowviewheadercolsort').hasClass('datasort') ? $('.workflowviewheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.workflowviewheadercolsort').hasClass('datasort') ? $('.workflowviewheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.workflowviewheadercolsort').hasClass('datasort') ? $('.workflowviewheadercolsort.datasort').attr('id') : '0';
		var userviewid = 187;
		var viewfieldids = '36';
		var filterid = $("#workflowmanagementfilterid").val();
		var conditionname = $("#workflowmanagementconditionname").val();
		var filtervalue = $("#workflowmanagementfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=workflow&primaryid=workflowid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#workflowmanagementviewgrid').empty();
				$('#workflowmanagementviewgrid').append(data.content);
				$('#workflowmanagementviewgridfooter').empty();
				$('#workflowmanagementviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('workflowmanagementviewgrid');
				{//sorting
					$('.workflowviewheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.workflowviewheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#workflowviewpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.workflowviewheadercolsort').hasClass('datasort') ? $('.workflowviewheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.workflowviewheadercolsort').hasClass('datasort') ? $('.workflowviewheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#workflowmanagementviewgrid .gridcontent').scrollLeft();
						workflowmanagementviewgrid(page,rowcount);
						$('#workflowmanagementviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('workflowviewheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						workflowmanagementviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#workflowmanagerpgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						workflowmanagementviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//Material select
				$('#workflowmanagerpgrowcount').material_select();
			},
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#workflowviewpgnum li.active').data('pagenum');
		var rowcount = $('ul#workflowviewpgnumcnt li .page-text .active').data('rowcount');
		workflowmanagementviewgrid(page,rowcount);
	}
}
{//inner grid
	function workflowmanagementinnergrid() {
		var wwidth = '830';
		var wheight = $("#workflowmanagementinnergrid").height();
		$.ajax({
			url:base_url+"Workflowmanagement/workflowconditioninformationfetch?moduleid=36&width="+wwidth+"&height="+wheight+"&modulename=workflow condition",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#workflowmanagementinnergrid").empty();
				$("#workflowmanagementinnergrid").append(data.content);
				$("#workflowmanagementinnergridfooter").empty();
				$("#workflowmanagementinnergridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('workflowmanagementinnergrid');
			},
		});
	}
}

{
	//active workflow
	function workflowdataactive(datarowid) {
		$.ajax({
			url:base_url+"Workflowmanagement/activateworkflowdata?primaryid="+datarowid,
			cache:false,
			success:function(msg) {
				var nmsg = $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup("Workflow activated successfully");
					refreshgrid();
				} else {
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup("Workflow activation failed");
				}
			},
		});
	}
	//inactive workflow
	function workflowdatainactive(datarowid) {
		$.ajax({
			url:base_url+"Workflowmanagement/inactivateworkflowdata?primaryid="+datarowid,
			cache:false,
			success:function(msg) {
				var nmsg = $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup("Workflow inactivated successfully");
					refreshgrid();
				} else {
					$("#basedeleteoverlayforcommodule").fadeOut();
					alertpopup("Workflow inactivation failed");
				}
			},
		});
	}
	//confirmation alert popup
	function workflowconfirmalertmsg(idname,msg) {
		$("#basedeleteoverlayforcommodule").show();
		$(".basedelalerttxt").text(msg);
		$(".commodyescls").attr('id',idname);
		$(".commodyescls").focus();
	}
}
{//field name list based on trigger type
	function fieldnamelist() {
		var ids = $('#newrulemoduleid').val();
		var triggertype = $('#recordtriggertype').find('option:selected').val();
		if(ids!='' && triggertype!='') {
			$.ajax({
				url: base_url+"Workflowmanagement/fielddropdownload?ids="+ids+"&type="+triggertype,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$('#datefieldids,#rulesdatafields').empty();
					$('#datefieldids,#rulesdatafields').append($("<option></option>"));
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$('#datefieldids,#rulesdatafields')
							.append($("<option></option>")
							.attr("data-uitype",data[index]['uitype'])
							.attr("value",data[index]['dataids'])
							.text(data[index]['label']));
						});
					}
					$('#datefieldids,#rulesdatafields').trigger('change');
				},
			});
		}
	}
	//fetch date and time fields list for task
	function assigntaskdudedatefieldnamelist() {
		var ids = $('#newrulemoduleid').val();
		if(ids!='') {
			$.ajax({
				url: base_url+"Workflowmanagement/fielddropdownload?ids="+ids+"&type=2",
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					$('#assigntaskduedate').empty();
					$('#assigntaskduedate').append($("<option></option>"));
					$('#assigntaskduedate').append($("<option value='1' data-uitype='1'>Workflow Trigger Date</option>"));
					if((data.fail) == 'FAILED') {
					} else {
						$.each(data, function(index) {
							$('#assigntaskduedate')
							.append($("<option></option>")
							.attr("data-uitype",data[index]['uitype'])
							.attr("value",data[index]['dataids'])
							.text(data[index]['label']));
						});
					}
					$('#assigntaskduedate').trigger('change');
				},
			});
		}
	}
}
{//Rule actions fields list
	function ruleactionfieldnamelist(ddname,type) {
		var ids = $('#newrulemoduleid').val();
		$.ajax({
			url: base_url+"Workflowmanagement/ruleactionfieldnamefetch?ids="+ids+"&type="+type,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#'+ddname+'').empty();
				$('#'+ddname+'').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					prev = ' ';
					$.each(data, function(index) {
						var cur = data[index]['modid'];
						if(prev != cur ) {
							if(prev != " ") {
								$('#'+ddname+'').append($("</optgroup>"));
							}
							$('#'+ddname+'').append($("<optgroup label='"+data[index]['modname']+"' class='"+data[index]['modname']+"'>"));
							prev = data[index]['modid'];
						}
						$("#"+ddname+" optgroup[label='"+data[index]['modname']+"']")
						.append($("<option></option>")
						.attr("class",data[index]['dataids']+'pclass')
						.attr("value",data[index]['dataids'])
						.attr("data-modid",data[index]['modid'])
						.text(data[index]['label']));
					});
				}
			},
		});
	}
}
{//email template information fetch
	function emailtemplateinformationfetch(ddname) {
		var ids = $('#newrulemoduleid').val();
		$.ajax({
			url: base_url+"Workflowmanagement/emailtemplateinformationfetch?ids="+ids,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#'+ddname+'').empty();
				$('#'+ddname+'').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataids'])
						.text(data[index]['datanames']));
					});
				}
			},
		});
	}
}
{//email template information fetch
	function desktoptemplateinformationfetch(ddname) {
		var ids = $('#newrulemoduleid').val();
		$.ajax({
			url: base_url+"Workflowmanagement/desktoptemplateinformationfetch?ids="+ids,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#'+ddname+'').empty();
				$('#'+ddname+'').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataids'])
						.text(data[index]['datanames']));
					});
				}
			},
		});
	}
}
{//condition field name show
	function conditionfieldnamelist() {
		var ids = $('#newrulemoduleid').val();
		$.ajax({
			url:base_url+"Workflowmanagement/rulecondfiledsdropdown?ids="+ids,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#rulecrtfieldname,#ruleupdatefieldname').empty();
				$('#rulecrtfieldname,#ruleupdatefieldname').append($("<option></option>"));
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data,function(index) {
						$('#rulecrtfieldname,#ruleupdatefieldname')
						.append($("<option></option>")
						.attr("data-uitype",data[index]['uitype'])
						.attr("data-id",data[index]['dataids'])
						.attr("value",data[index]['label'])
						.text(data[index]['label']));
					});
				}
			},
		});
	}
}
//field name based drop down value - picklist
function modulefiledpicklistvalfetch(fieldid,uitype,dropdownid) {
	var moduleid = $("#newrulemoduleid").val();
	$.ajax({
		url: base_url+"Workflowmanagement/modulefiledpicklistvalfetch?fieldid="+fieldid+"&moduleid="+moduleid+"&uitype="+uitype,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#'+dropdownid+'').empty();
			$('#'+dropdownid+'').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data,function(index) {
					$('#'+dropdownid+'')
					.append($("<option></option>")
					.attr("value",data[index]['datasid']+'-'+data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}
//field name based drop down value - group picklist
function modfieldgrppicklistvalfetch(fieldid,uitype,dropdownid) {
	var moduleid = $("#newrulemoduleid").val();
	$.ajax({
        url: base_url+"Workflowmanagement/modulefiledpicklistvalfetch?fieldid="+fieldid+"&moduleid="+moduleid+"&uitype="+uitype,
		dataType:"json",
		async:false,
		cache:false,
        success: function(data) {
			$('#'+dropdownid+'').empty();
			$('#'+dropdownid+'').append($("<option></option>"));
			if(data['status']!='fail') {
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['PId'];
					if(prev != cur ) {
						if(prev != " ") {
							$('#'+dropdownid+'').append($("</optgroup>"));
						}
						$('#'+dropdownid+'').append($("<optgroup label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
						prev = data[index]['PId'];
					}
					$("#"+dropdownid+" optgroup[label='"+data[index]['PName']+"']")
					.append($("<option></option>")
					.attr("class",data[index]['CId']+'pclass')
					.attr("value",data[index]['CName'])
					.attr("data-ddid",data[index]['CId'])
					.attr("data-dataidhidden",data[index]['PId'])
					.text(data[index]['CName']));
				});
			}
		},
    });
}
//check box value get
function checkboxvalueget(dropdownid) {
	$.ajax({
		url: base_url+"Workflowmanagement/checkboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#'+dropdownid+'').empty();
			$('#'+dropdownid+'').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16') {
						$('#'+dropdownid+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					}
				});
			}
		},
	});
}
//show hide fields
function showhideintextbox(hideid,showid) {
	$("#"+hideid+"divhid").addClass('hidedisplay');
	$("#"+showid+"divhid").removeClass('hidedisplay');
}
//work flow edit detail fetch
function workfloweditdatafetch(datarowid) {
	$.ajax({
		url:base_url+"Workflowmanagement/fetchformdataeditdetails?dataprimaryid="+datarowid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$(".ftab").trigger('click');
				$('#workflowform').hide();
				$('#workflowmanagementview').fadeIn(1000);
				$("#processoverlay").hide();
				refreshgrid();
			} else {
				showhideiconsfun('editclick','workflowform');
				addslideup('workflowmanagementview','workflowform');
				var primaryid = data.id;
				var name = data.name;
				var moduleid = data.moduleid;
				var description = data.description;
				var active = data.active;
				var triggertype = data.triggertype;
				$("#ruledescription").val(description);
				$("#primaryid").val(primaryid);
				$("#newrulename").val(name);
				$("#recordtriggertype").select2('val',triggertype).trigger('change');
				$("#recordtriggertype").attr('readonly',true);
				$("#newrulemoduleid").select2('val',moduleid).trigger('change');
				$("#newrulemoduleid").attr('readonly',true);
				if(active != 'Yes') {
					$("#ruleactivecls").attr('checked',false);
					$("#ruleactive").val(active);
				} else {
					$("#ruleactivecls").attr('checked',true);
					$("#ruleactive").val(active);
				}
				if(triggertype == 1) {
					var triggerfrequency = data.triggerfrequency;
					var triggerfield = data.triggerfield;
					var excufreq = data.excufreq;
					$(".fortrigger2").trigger("click");
					if(triggerfrequency == '1') {
						$("#datacreate").prop('checked', true);
					} else if(triggerfrequency == '2') {
						$("#dataupdate").prop('checked', true);
					} else if(triggerfrequency == '3') {
						$("#datacreateupdate").prop('checked', true);
					} else if(triggerfrequency == '4') {
						$("#datafieldupdate").prop('checked', true);
						$("input[name$='dataruleradiobtn'][value$='"+triggerfrequency+"']").trigger('click');
					} else if(triggerfrequency == '5') {
						$("#datadelete").prop('checked', true);	
					}
					$('#recordacttrigtypeid').val(triggerfrequency);
					var fieldsplit = triggerfield.split(',');
					$("#rulesdatafields").select2('val',fieldsplit).trigger('change');
					$("input[name$='fieldvalchktype'][value$='"+excufreq+"']").prop('checked', true).trigger('click');
					$("#fldvalchkbtnclass").trigger('click');
				} else {
					var triggerfield = data.triggerfield;
					$("#datefieldids").select2('val',triggerfield).trigger('change');
					var triggerfrequency = data.triggerfrequency;
					$("#datefldfrequencytype").select2('val',triggerfrequency).trigger('change');
					$('#repeateveryday').val(data.triggerrepeatevery);
					if(data.triggerfrequency == 'Weekly') {
						var weeklyddvalue = data.triggerexecutiononday.split(",");
						if(weeklyddvalue != ''){
							$("#recurrencedayid").select2('val',weeklyddvalue).trigger('change');
						}
					} else if(data.triggerfrequency == 'Monthly') {
						var monthlyddvalue = data.triggerexecutiononday;
						$("#recurrencemonthdayid").select2('val',monthlyddvalue).trigger('change');
					}
					var exectype = data.exectype;
					$("#datefldexecutiontype").select2('val',exectype).trigger('change');
					if(exectype != 'On') {
						$("#afterbeforetime").val(data.excufreq);
					}
					var exectime = data.exectime;
					$("#datefldexectiontime").val(data.exectime);
				}
				//rule trigger condition grid information fetch
				rulecreteriagridfetch(datarowid);
				//rule alert data set
				var alerttype = data.alerttype;
				$("#rulealerttype").select2('val',alerttype).trigger('change');
				var alertname = data.alertname;
				$('#rulealertname').val(alertname);
				if(alerttype == 'desktop') {
					var usertemplateid = data.templateid;
					$('#rulealertusertemp').select2('val',usertemplateid).trigger('change');
					var userrecipient = data.recipient;
					$('#rulealertuserrecep').select2('val',userrecipient).trigger('change');
					var additonalrecipient = data.additonalrecipient.split(',');
					if(additonalrecipient != '') {
						$('#rulealertadduserrecep').select2('val',additonalrecipient).trigger('change');
					}
				} else if(alerttype == 'email') {
					var emailtemplateid = data.templateid;
					$('#rulealertemailtempid').select2('val',emailtemplateid).trigger('change');
					var emailrecipient = data.recipient;
					$('#rulealertemailrecipient').select2('val',emailrecipient).trigger('change');
					var additonalrecipient = data.additonalrecipient.split(',');
					if(additonalrecipient != '') {
						$('#rulealertadduserrecep').select2('val',additonalrecipient).trigger('change');
					}
					$('#rulealertaddemailrecp').val(data.manadditonalrecipient);
				} else if(alerttype == 'sms') {
					var transactionmode = data.actionmode;
					$('#rulealertsmstransmode').select2('val',transactionmode).trigger('change');
					var senderid = data.senderid;
					$('#rulealertsmssendid').select2('val',senderid).trigger('change');
					var smstype = data.actiontype;
					$('#rulealertsmstypeid').select2('val',smstype).trigger('change');
					var smstemplateid = data.templateid;
					$('#rulealertsmstemp').select2('val',smstemplateid).trigger('change');
					var smsrecipient = data.recipient;
					$('#rulealertsmsrecep').select2('val',smsrecipient).trigger('change');
					var additonalrecipient = data.additonalrecipient.split(',');
					if(additonalrecipient != '') {
						$('#rulealertadduserrecep').select2('val',additonalrecipient).trigger('change');
					}
					$('#rulealertsmsaddrecep').val(data.manadditonalrecipient);
				} else if(alerttype == 'call') {
					var transactionmode = data.actionmode;
					$('#rulealertcalltransmode').select2('val',transactionmode).trigger('change');
					var numbertype = data.actiontype;
					$('#rulealertcallnumbertype').select2('val',numbertype).trigger('change');
					var callernumber = data.senderid;
					$('#rulealertcallernumber').select2('val',callernumber).trigger('change');
					var callsoundid = data.callsoundid;
					$('#rulealertcallsoundfile').select2('val',callsoundid).trigger('change');
					var calltemplateid = data.templateid;
					$('#rulealertcalltemp').select2('val',calltemplateid).trigger('change');
					var receivernumber = data.recipient;
					$('#rulealertcallrecpnum').select2('val',receivernumber).trigger('change');
					var additonalrecipient = data.additonalrecipient;
					$('#rulealertcalladdrecpnum').val(additonalrecipient);
				}
				//rule task data set
				var assigntaskname = data.taskname;
				$("#assigntaskname").val(assigntaskname);
				var duedate = data.duedate;
				$("#assigntaskduedate").select2('val',duedate).trigger('change');
				var taskalerttype = data.taskalerttype;
				$("#assigntasktimetype").select2('val',taskalerttype).trigger('change');
				var taskalertdays = data.taskalertdays;
				$("#assigntaskdays").val(taskalertdays);
				var taskstatusid = data.taskstatusid;
				$("#taskstatusid").select2('val',taskstatusid).trigger('change');
				var taskpriorityid = data.taskpriorityid;
				$("#taskpriorityid").select2('val',taskpriorityid).trigger('change');
				var taskassignedto = data.taskassignedto;
				$("#assigntaskemployeeid").select2('val',taskassignedto).trigger('change');
				var notifyassignee = data.notifyassignee;
				if(notifyassignee != 'Yes') {
					$("#tasknotifyassignee").attr('checked',false);
					$("#tasknotifyassignee").val(active);
				} else {
					$("#tasknotifyassignee").attr('checked',true);
					$("#tasknotifyassignee").val(active);
				}
				var description = data.taskdesciption;
				$("#taskdescription").val(description);
				//update field data set
				var actionfieldname = data.actionfieldname;
				$("#ruleupdatefieldsubject").val(actionfieldname);
				var modulefieldid = data.fieldlabel;
				$("#ruleupdatefieldname").select2('val',modulefieldid);//.trigger('change');
				var value = data.value;
				$("#ruleupdatefieldvalue").val(value);
				$("#processoverlay").hide();
				Materialize.updateTextFields();
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});	
}
//rule creteria grid information fetch
function rulecreteriagridfetch(datarowid) {
	$.ajax({
		url: base_url+"Workflowmanagement/rulecreteriagridfetchfrun?workflowid="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
        	if(data.fail != 'FAILED'){
        		loadinlinegriddata('workflowmanagementinnergrid',data.rows,'json');
    			/* data row select event */
    			datarowselectevt();
    			/* column resize */
    			columnresize('workflowmanagementinnergrid');
    			var hideprodgridcol = [];
    			gridfieldhide('workflowmanagementinnergrid',hideprodgridcol);
    			var condcnt = $('#workflowmanagementinnergrid .gridcontent div.data-content div').length;
				if(condcnt==0) {
					$("#rulecrtandorconddivhid").hide();
					$('.condmandclass').text('');
					$('#rulecrtandorcond').removeClass('validate[required]');
				} else {
					$("#rulecrtandorconddivhid").show();
					$('.condmandclass').text('*');
					$('#rulecrtandorcond').addClass('validate[required]');
				}
        	}
		},
	});
}
//account unique name check
function workflownamecheck() {
	var primaryid = $("#primaryid").val();
	var accname = $("#newrulename").val();
	var elementpartable = 'workflow';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Workflow rule name already exists!";
				}
			} else {
				return "Workflow rule name already exists!";
			}
		} 
	}
}