$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		cashcounteraddgrid();
		firstfieldfocus();
		storagecrudactionenable();
	}
	STORAGEDELETE = 0;
	retrievestatus = 0;
	//hidedisplay
	$('#cashcounterdataupdatesubbtn,#groupcloseaddform').hide();
	$('#cashcountersavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			cashcounteraddgrid();
		});
	}
	{// Drop Down Change Function employee 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{//validation for Add  
		$('#cashcountersavebutton').click(function(e) {
			$('#storagename,#storagesuffix').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#cashcounterformaddwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#cashcounterformaddwizard").validationEngine( {
			onSuccess: function() {
				cashcounteraddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#cashcounterdataupdatesubbtn').click(function(e) {
			$('#storagename,#storagesuffix').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#cashcounterformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#cashcounterformeditwizard").validationEngine({
			onSuccess: function() {
				storageupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$( window ).resize(function() {
			maingridresizeheightset('cashcounteraddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	
	{//filter work
		//for toggle
		$('#cashcounterviewtoggle').click(function() {
			if ($(".cashcounterfilterslide").is(":visible")) {
				$('div.cashcounterfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.cashcounterfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#cashcounterclosefiltertoggle').click(function(){
			$('div.cashcounterfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#cashcounterfilterddcondvaluedivhid").hide();
		$("#cashcounterfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#cashcounterfiltercondvalue").focusout(function(){
				var value = $("#cashcounterfiltercondvalue").val();
				$("#cashcounterfinalfiltercondvalue").val(value);
				$("#cashcounterfilterfinalviewconid").val(value);
			});
			$("#cashcounterfilterddcondvalue").change(function(){
				var value = $("#cashcounterfilterddcondvalue").val();
				var fvalue = $("#cashcounterfilterddcondvalue option:selected").attr('data-ddid');
				cashcounteremainfiltervalue=[];
				if( $('#s2id_cashcounterfilterddcondvalue').hasClass('select2-container-multi') ) {
					cashcounteremainfiltervalue=[];
					$('#cashcounterfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    cashcounteremainfiltervalue.push($cvalue);
						$("#cashcounterfinalfiltercondvalue").val(cashcounteremainfiltervalue);
					});
					$("#cashcounterfilterfinalviewconid").val(value);
				} else {
					$("#cashcounterfinalfiltercondvalue").val(fvalue);
					$("#cashcounterfilterfinalviewconid").val(value);
				}
			});
		}
		cashcounterfiltername = [];
		$("#cashcounterfilteraddcondsubbtn").click(function() {
			$("#cashcounterfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#cashcounterfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(cashcounteraddgrid,'cashcounter');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
			$("#parentcashcounterid").val("");
			$("#employeeid").prop('disabled',false);
	   });
	}
	{//cash counter hierarchy
		$('#cashcounterlistuldata li').click(function() {
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentcashcounter').val(name);
			$('#parentcashcounterid').val(listid);
			if(level >= maxlevel){
				alertpopup('Maximum cash counter level reached');
				return false;
			}
			var editid = $('#cashcounterprimarydataid').val();
			if(listid != editid) {
				storagerefreshgrid();
			} else {
				alertpopup('You cant choose the same cash counter as parentcashcounter');
			}
		});	
	}
	{//Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function cashcounteraddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#cashcounteraddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#cashcountersortcolumn").val();
	var sortord = $("#cashcountersortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.cashcounterheadercolsort').hasClass('datasort') ? $('.cashcounterheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.cashcounterheadercolsort').hasClass('datasort') ? $('.cashcounterheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.cashcounterheadercolsort').hasClass('datasort') ? $('.cashcounterheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#cashcounterviewfieldids').val();
	if(userviewid == '') {
		userviewid= '193';
	}
	var filterid = $("#cashcounterfilterid").val();
	var conditionname = $("#cashcounterconditionname").val();
	var filtervalue = $("#cashcounterfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=cashcounter&primaryid=cashcounterid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#cashcounteraddgrid').empty();
			$('#cashcounteraddgrid').append(data.content);
			$('#cashcounteraddgridfooter').empty();
			$('#cashcounteraddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('cashcounteraddgrid');
			{//sorting
				$('.cashcounterheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.cashcounterheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.cashcounterheadercolsort').hasClass('datasort') ? $('.cashcounterheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.cashcounterheadercolsort').hasClass('datasort') ? $('.cashcounterheadercolsort.datasort').data('sortorder') : '';
					$("#cashcountersortorder").val(sortord);
					$("#cashcountersortcolumn").val(sortcol);
					var sortpos = $('#cashcounteraddgrid .gridcontent').scrollLeft();
					cashcounteraddgrid(page,rowcount);
					$('#cashcounteraddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('cashcounterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					cashcounteraddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#cashcounterpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					cashcounteraddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#cashcounteraddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#cashcounterpgrowcount').material_select();
		},
	});
}
function storagecrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			$('#branchid').select2('val',$('#branchidval').val()).trigger('change');
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#cashcounterdataupdatesubbtn').hide();
			$('#cashcountersavebutton').show();
			$('#cashcountertypeid').select2('val','2');
			retrievestatus = 0;
			passwordvalidate();
			$("#accountid").prop('disabled',false);
			$("#cashcountertypeid").prop('disabled',false);
			$("#employeeid").prop('disabled',false);
			//loadusername(1);
			loadaccountname(1);
			$('#cashcounterprimarydataid').val('');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#cashcounteraddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#cashcounterdataupdatesubbtn').show();
			$('#cashcountersavebutton').hide();
			retrievestatus = 1;
			passwordvalidate();
			//loadusername(0);
			loadaccountname(0);
			storageeditformdatainformationshow(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#cashcounteraddgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			$.ajax({ // checking whether this value is already in usage.
						url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=cashcounter&fieldid=parentcashcounterid&table1=''&fieldid1=''&table2=''&fieldid2=''",
						success: function(msg) { 
							if(msg > 0){
								alertpopup("This cashcounter have child cashcounter.So unable to delete this one");
							}else{
								$("#cashcounterprimarydataid").val(datarowid);
								combainedmoduledeletealert('storagedeleteyes');
								if(STORAGEDELETE == 0) {
									$("#storagedeleteyes").click(function(){
										var datarowid = $("#cashcounterprimarydataid").val();
										storagerecorddelete(datarowid);
									});
								}
								STORAGEDELETE =1;
							}
						},
					});	
			
			
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function storagerefreshgrid() {
		var page = $('ul#storagepgnum li.active').data('pagenum');
		var rowcount = $('ul#storagepgnumcnt li .page-text .active').data('rowcount');
		cashcounteraddgrid(page,rowcount);
	}
}
//new data add submit function
function cashcounteraddformdata() {
	var formdata = $("#cashcounteraddform").serialize();
	var branchid = $("#branchid").val();
	var empid = $.trim($("#employeeid").val());
	empids = ltrim(empid,",");
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Cashcounter/newdatacreate",
        data: "datas=" + datainformation+amp+"employeeid="+empids+amp+"branchid="+branchid,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				storagerefreshgrid();
				setTimeout(function(){parenttreereloadfun(0);},500);
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level of sub cash counter reached !!! cannot create counter further.');
			}
        },
    });
}
//old information show in form
function storageeditformdatainformationshow(datarowid) {
	var cashcounterelementsname = $('#cashcounterelementsname').val();
	var cashcounterelementstable = $('#cashcounterelementstable').val();
	var cashcounterelementscolmn = $('#cashcounterelementscolmn').val();
	var cashcounterelementspartabname = $('#cashcounterelementspartabname').val();
	$.ajax(	{
		url:base_url+"Cashcounter/fetchformdataeditdetails?cashcounterprimarydataid="+datarowid+"&cashcounterelementsname="+cashcounterelementsname+"&cashcounterelementstable="+cashcounterelementstable+"&cashcounterelementscolmn="+cashcounterelementscolmn+"&cashcounterelementspartabname="+cashcounterelementspartabname, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				storagerefreshgrid();
			} else {
				var txtboxname = cashcounterelementsname + ',cashcounterprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = cashcounterelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var empdata = data.employeeid.split(',');
				if(empdata != '') {
					$('#employeeid').select2('val',empdata).trigger('change');
				}
				$("#branchid").prop('disabled',true);
				$("#employeeid").prop('disabled',true);
				$("#cashcountertypeid").prop('disabled',true);
				$("#accountid").prop('disabled',true);
				Materialize.updateTextFields();
			}
		}
	});
	parenttreereloadfun(datarowid);
	$('#cashcounterdataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#cashcountersavebutton').hide();
}
//udate old information
function storageupdateformdata() {
	var parentstorageid=$("#parentcashcounterid").val();
	var editstorageid=$("#cashcounterprimarydataid").val();
	var oldpassword = $("#oldpassword").val();
	if(parentstorageid!=editstorageid){
		var formdata = $("#cashcounteraddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Cashcounter/datainformationupdate",
			data: "datas=" + datainformation+"&oldpassword="+oldpassword,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$('#cashcounterdataupdatesubbtn').hide();
					$('#cashcountersavebutton').show();
					storagerefreshgrid();
					retrievestatus = 0;
					setTimeout(function(){parenttreereloadfun(0);},500);
					$("#groupsectionoverlay").removeClass("effectbox");
					$("#groupsectionoverlay").addClass("closed");
					resetFields();
					alertpopup(savealert);			
				}else if(nmsg == 'FAILURE'){
					alertpopup("Check your Parent cash counter mapping. ");
				}else if(nmsg == 'LEVEL') {
					alertpopup('Maximum level of sub cash counter reached !!! cannot create category further.');
				}
			},
		});
	} else{
		alertpopup("Don't map with same Cash Counter");
	}
}
//udate old information
function storagerecorddelete(datarowid) {
	var cashcounterelementstable = $('#cashcounterelementstable').val();
	var cashcounteelementspartable = $('#cashcounterelementspartabname').val();
    $.ajax({
        url: base_url + "Cashcounter/deleteinformationdata?cashcounterprimarydataid="+datarowid+"&cashcounterelementstable="+cashcounterelementstable+"&cashcounteelementspartable="+cashcounteelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	storagerefreshgrid();
            	$("#parentcashcounterid").val("");
				$("#basedeleteoverlayforcommodule").fadeOut();
				retrievestatus = 0;
				setTimeout(function(){parenttreereloadfun(0);},500);
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	storagerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            } else if(nmsg == "Parent" ) {
				storagerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Please remove the related Sub cash counter before delete this cash counter');
			}
        },
    });
}
{// Tree reload
	function parenttreereloadfun(datarowid)	{
		var tablename = "cashcounter";
		var mandfield = $('#treevalidationcheck').val();
		var fieldlab = $('#treefiledlabelcheck').val();
		var parentcounterid = $('#parentcashcounterid').val();
		$.ajax({
			url: base_url + "Cashcounter/treedatafetchfun",
			data: "tabname="+tablename+'&mandval='+mandfield+'&fieldlabl='+fieldlab+'&rowid='+datarowid+'&primaryid='+parentcounterid,
			type: "POST",
			cache:false,
			success: function(data) {
				$('.'+tablename+'treediv').empty();
				$('.'+tablename+'treediv').append(data);
				$('#cashcounterlistuldata li').click(function() {
					var level = parseInt($(this).attr('data-level'));
					var maxlevel = parseInt($('#maxcategorylevel').val());
					var name=$(this).attr('data-listname');
					var listid=$(this).attr('data-listid');
					$('#parentcashcounter').val(name);
					$('#parentcashcounterid').val(listid);
					if(level >= maxlevel){
						alertpopup('Maximum cash counter level reached');
						return false;
					}
				});
				$(function() {
					$('#dl-menu').dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
					});
				});
				if(retrievestatus == 1) {			
					gettheparentcounter(datarowid);
				}
			},
		});
	}
}
//load the parent id
function gettheparentcounter(datarowid){
	$.ajax({               
		url:base_url+"Cashcounter/getcounterparentid?id="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			if(data.parentcashcounterid != '') {
				$('#parentcashcounterid').val(data.parentcashcounterid);
				$('#parentcashcounter').val(data.cashcountername);	
			}			
		}
	});
}
function loadusername(datastatus){ // //load username except cashcountter assigned
	$('#employeeid').empty();	
    $('#employeeid').append($("<option></option>"));
	var tablename = 'cashcounter';
	$.ajax({
        url: base_url + "Cashcounter/loadusername",
		data:{datastatus:datastatus,table:tablename},
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg) 
        { 
			for(var i = 0; i < msg.length; i++) {
				 $('#employeeid')
				.append($("<option></option>")
				.attr('data-employeeidhidden',msg[i]['employeename'])
				.attr("value",msg[i]['employeeid'])
				.text(msg[i]['employeename']));
			} 
        },
    });
}
function loadaccountname(datastatus){ // load account name
	$('#accountid').empty();	
    $('#accountid').append($("<option></option>"));
	var tablename = 'cashcounter';
	$.ajax({
        url: base_url + "Cashcounter/loadaccountname",
		data:{datastatus:datastatus,table:tablename},
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg) 
        { 
			for(var i = 0; i < msg.length; i++) {
				 $('#accountid')
				.append($("<option></option>")
				.attr('data-accountidhidden',msg[i]['accountname'])
				.attr("value",msg[i]['accountid'])
				.text(msg[i]['accountname']));
			} 
        },
    });
}
//Kumaresan - Cash Counter name unique check
function cashcounternamecheck() {
	var primaryid = $("#cashcounterprimarydataid").val();
	var accname = $("#cashcountername").val();
	var elementpartable = 'cashcounter';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Cash Counter Name already exists!";
				}
			}else {
				return "Cash Counter Name already exists!";
			}
		} 
	}
}
//Kumaresan - Short name unique check
function cashcountershortnamecheck() {
	var primaryid = $("#cashcounterprimarydataid").val();
	var accname = $("#shortname").val();
	var fieldname = 'shortname';
	var elementpartable = 'cashcounter';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		}
	}
}
// check confirm password
function passwordvalidate() {
		$('#confirmpassword').removeClass('validate[required,minSize[6],maxSize[20]]');
		$('#confirmpassword').addClass('validate[required,minSize[6],maxSize[20],equals[password]]');
}
function ltrim(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('^\\s+') : new RegExp('^'+chr+'+');
  return str.replace(rgxtrim, '');
}