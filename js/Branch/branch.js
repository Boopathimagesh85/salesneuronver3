//JavaScript Ready Functions
$(document).ready(function() {
	$(document).foundation();	
	// Maindiv height width change
	maindivwidth();	
	// Grid Calling Functions
	branchjqgrid();
	//crud action
	crudactionenable();
	if(softwareindustryid == 3){
    	 defaultstateid = $('#defaultstateid').val();
    	 defaultcountryid = $('#defaultcountryid').val();
	}
	var addcloseinfo = ["closeaddform", "branchgriddisplay", "branchform"]
	addclose(addcloseinfo);
	{//branch view by dropdown change
		$('#dynamicdddataview').change(function(){
			branchjqgrid();
		});
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{//validation for branch Add  
		$('#dataaddsbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		$("#formaddwizard").validationEngine({
			onSuccess: function() {
				addformdata();
			},
			onFailure: function() {
				var dropdownid =['2','companyid','branchtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//update branch information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		$("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','companyid','branchtypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{//action events		
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('branchgrid');
		});
		$("#detailedviewicon").click(function(){
			var datarowid = $('#branchgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				froalaset(froalaarray);
				getformdata(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','branchform');
				$("#dataaddsbtn,#formclearicon").hide();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					getformdata(rdatarowid);
					showhideiconsfun('summryclick','branchform');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function(){
			showhideiconsfun('editfromsummryclick','branchform');		
		});
		{//Form Clear
			$("#formclearicon").click(function(){
				var elementname = $('#elementsname').val();
				elementdefvalueset(elementname);
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			});
		}
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,branchjqgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// branch View Grid
function branchjqgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#branchpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#branchpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	}
	var wwidth = $('#branchgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.branchheadercolsort').hasClass('datasort') ? $('.branchheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.branchheadercolsort').hasClass('datasort') ? $('.branchheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.branchheadercolsort').hasClass('datasort') ? $('.branchheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=branch&primaryid=branchid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#branchgrid').empty();
			$('#branchgrid').append(data.content);
			$('#branchgridfooter').empty();
			$('#branchgridfooter').append(data.footer);
			//getaddinfo();
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('branchgrid');
			{//sorting
				$('.branchheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.branchheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#branchpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.branchheadercolsort').hasClass('datasort') ? $('.branchheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.branchheadercolsort').hasClass('datasort') ? $('.branchheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#branchgrid .gridcontent').scrollLeft();
					branchjqgrid(page,rowcount);
					$('#branchgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('branchheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function() {
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					branchjqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#branchpgrowcount').change(function() {
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					branchjqgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#branchgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#branchpgrowcount').material_select();
		},
	});	
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function() {
			addslideup('branchgriddisplay','branchform');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#dataaddsbtn").show();
			showhideiconsfun('addclick','branchform');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			if(softwareindustryid == 3){
				$('#stateid').select2('val',defaultstateid);
				$('#countryid').select2('val',defaultcountryid);
			}
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			Materialize.updateTextFields();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			$("#companyid").select2('val',loggedinbranchid);
			$("#primarydataid").val('');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function() {
			var datarowid = $('#branchgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				showhideiconsfun('editclick','branchform');
				froalaset(froalaarray);
				getformdata(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
				fortabtouch = 1;
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 1; //for pagination
			} else {
				alertpopup('Please select a row');
			}
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#branchpgnumcnt li .page-text .active').data('rowcount');
		branchjqgrid(page,rowcount);
	}
}
//new data add submit function
function addformdata() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
    var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Branch/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$(".ftab").trigger('click');
				resetFields();
				$('#branchform').hide();
				$('#branchgriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
            } else {
				$(".ftab").trigger('click');
				resetFields();
				$('#branchform').hide();
				$('#branchgriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(nmsg);
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
			}
        },
    });
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax({
		url:base_url+"Branch/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#branchform').hide();
				$('#branchgriddisplay').fadeIn(1000);
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				addslideup('branchgriddisplay','branchform');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				editordatafetch(froalaarray,data);
			}
		}
	});
}
//udate old information
function updateformdata() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Branch/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$('#branchform').hide();
				$('#branchgriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
//branchname check
function branchnamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#branchname").val();
	var elementpartable = $('#elementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Branch name already exists!";
				}
			} else {
				return "Branch name already exists!";
			}
		} 
	}
}
function shortbranchnamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#branchshortname").val();
	var fieldname = 'branchshortname';
	var elementpartable = 'branch';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Branch short name already exists!";
				}
			}else {
				return "Branch short name already exists!";
			}
		} 
	}
}

function emailidcheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#emailid").val();
	var fieldname = 'emailid';
	var elementpartable = 'branch';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "emailid already exists!";
				}
			}else {
				return "emailid already exists!";
			}
		} 
	}
}

function mobilenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#mobilenumber").val();
	var fieldname = 'mobilenumber';
	var elementpartable = 'branch';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Mobile number already exists!";
				}
			}else {
				return "Mobile number already exists!";
			}
		} 
	}
}

function phonenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#landlinenumber").val();
	var fieldname = 'landlinenumber';
	var elementpartable = 'branch';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Phone number already exists!";
				}
			}else {
				return "Phone number already exists!";
			}
		} 
	}
}