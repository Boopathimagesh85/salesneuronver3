$(document).ready(function()
{	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		profileaddgrid();
		moduleaddgrid();
		fieldsaddgrid();
		fieldvaluesaddgrid();
		submoduleaddgrid();
		gridsettings();
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$("#groupcloseaddform").click(function(){
		window.location =base_url+'Roles';
	});
	{
		$("#profileaddicon").click(function(){
			$("#profilesectionoverlay").removeClass("closed");
			$("#profilesectionoverlay").addClass("effectbox");
			$("#profileupdatebtn").hide();
			$("#profileaddbtn").show();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{
		$("#profilecancelbutton").click(function(){
			$("#profilesectionoverlay").addClass("closed");
			$("#profilesectionoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{//Profile actions
		rowid = [];
		//profile reload icons
		$("#profilereload").click(function() {
			$("#profileupdatebtn").hide();
			$("#profiledelete,#profileaddbtn").show();
			resetFields();
			clearinlinesrchandrgrid('profileaddgrid');
		});
		//profile edit icon
		$("#profileedit").click(function() {
			var datarowid = $('#profileaddgrid').jqGrid('getGridParam', 'selrow');
			if(datarowid){
				resetFields();
				$("#profileupdatebtn").show();
				$("#profileaddbtn").hide();
				profiledatafetchfunction(datarowid);
				$("#profilesectionoverlay").removeClass("closed");
				$("#profilesectionoverlay").addClass("effectbox");
				Materialize.updateTextFields();
				firstfieldfocus();
				// For Touch
				masterfortouch = 1;
			//keyboard Shortcut
			saveformview=1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//profile delete icon
		$("#profiledelete").click(function(){
			var datarowid = $('#profileaddgrid').jqGrid('getGridParam', 'selrow');
			if(datarowid){		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}	
		});
		$("#basedeleteyes").click(function(){
			var datarowid = $('#profileaddgrid').jqGrid('getGridParam','selrow'); 
			profiledeletefun(datarowid);
		});
		// Validation for Profile Add  
		$('#profileaddbtn').mousedown(function(e){
			$('#profiledescription').focus();
			// For Touch
			setTimeout(function(){
				if(e.which == 1 || e.which === undefined) {
					masterfortouch = 0;
					$("#profileaddvalidation").validationEngine('validate');
				}
			},100);
		});
		jQuery("#profileaddvalidation").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				profilenewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		// Validation for Profile Update  
		$('#profileupdatebtn').mousedown(function(e){
			if(e.which == 1 || e.which === undefined) {
				$("#profileupdatevalidation").validationEngine('validate');
			}
		});
		jQuery("#profileupdatevalidation").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				profileupdatedataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//module tab actions
		$("#moduleprofileid").change(function() {
			moduleaddgrid();
		});
	}
	{//Fields value get
		$("#fieldmoduleid").change(function() {
			var fieldproid = $('#fieldprofileid').val();
			if(fieldproid != "") {
				fieldsaddgrid();
			} else {
				$("#fieldmoduleid").select2('val','');
				alertpopup('Please select the profile name');
			}
		});
		//value role id
		$('#fieldprofileid').change(function(){
			$("#fieldmoduleid").select2('val','');
			fieldsaddgrid();
		});
		//fetch moduleids based on profile
		$("#fieldprofileid,#valueprofileid").change(function() {
			var profileid = $(this).val();
			$('#fieldmoduleid,#valuemoduleid').empty();
			$('#fieldmoduleid,#valuemoduleid').append($("<option></option>"));
			$.ajax({
				url:base_url+'Profile/fetchprofilebasedmodulefetch?pid='+profileid,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					if((data.fail) == 'FAILED') {
					} else {
						prev = ' ';
						$.each(data, function(index) {
							var cur = data[index]['cateid'];
							if(prev != cur ) {
								if(prev != " ") {
									$('#fieldmoduleid,#valuemoduleid').append($("</optgroup>"));
								}
								$('#fieldmoduleid,#valuemoduleid').append($("<optgroup label='"+data[index]['catename']+"' class='"+data[index]['catename']+"'>"));
								prev = data[index]['cateid'];
							}
							$("#fieldmoduleid optgroup[label='"+data[index]['catename']+"']").append($("<option></option>").attr("data-moduleid",data[index]['datasid']).attr("value",data[index]['datasid']).text(data[index]['dataname']));
							
							$("#valuemoduleid optgroup[label='"+data[index]['catename']+"']").append($("<option></option>").attr("data-moduleid",data[index]['datasid']).attr("value",data[index]['datasid']).text(data[index]['dataname']));
						});
					}
				},
			});
		});
	}
	{//values field drop down value fetch
		$("#valuemoduleid").change(function() {
			var modid = $("#valuemoduleid").val();
			var profid = $("#valueprofileid").val();
			if(modid != "" && profid != "") {
				valuedropdownload(profid,modid);	
			} else {
				$("#valuemoduleid").select2('val','');
				$("#valuefieldid").empty();
				alertpopup('Please select the module (or) profile name');
			}
		});
	}
	//filed drop down change
	$('#valuefieldid').change(function() {
		fieldvaluesaddgrid();
	});
	//profile value
	$("#profilevaluesubmit").click(function() {
		//var modid = $("#valuemoduleid").val();
		var modid = $("#valuefieldid").find('option:selected').data('moduletabid');
		var profid = $("#valueprofileid").val();
		var modulefield = $("#valuefieldid").val();
		var allmodulevalueid = $('#fieldvaluesaddgrid').jqGrid('getDataIDs');
		var count = jQuery("#fieldvaluesaddgrid").jqGrid('getGridParam', 'records');
		var val = '';
		var content = {};
		$.each(allmodulevalueid,function(index,value) {
			content[index] = {};
			content[index]['fieldid'] = {};
			content[index]['status'] = {};
			val = $('#fieldvaluesaddgrid').jqGrid('getCell',value,'status');
			content[index]['fieldid']=value;
			content[index]['status']=val;
		});
		var fieldgriddata = JSON.stringify(content);
		if(modulefield !== null && modulefield !== '' && count>1) {
			$("#processoverlay").show();
			valuefieldidinsertion(profid,modid,modulefield,fieldgriddata);
		} else {
			$("#processoverlay").hide();
			alertpopup('Please Select the Field Value');
		}
		// For Touch
		masterfortouch = 0;
	});
	//tab click events
	$("#tab1").click(function()	{
		$(".tabclass").addClass('hidedisplay');
		$("#tab1class").removeClass('hidedisplay');
		mastertabid=1; masterfortouch = 0;	
	});
	$("#tab2").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab2class").removeClass('hidedisplay');
		$('#moduleprofileid').select2('val','');
		$('#profilemodsearchicon').hide();
		moduleaddgrid();
		//for touch
		mastertabid =2;
	});
	$("#tab3").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab3class").removeClass('hidedisplay');
		$("#fieldprofileid,#fieldmoduleid").select2('val','');
		fieldsaddgrid();
		//for touch
		mastertabid =3;
	});
	$("#tab4").click(function() {
		$(".tabclass").addClass('hidedisplay');
		$("#tab4class").removeClass('hidedisplay');
		$("#valueprofileid,#valuemoduleid,#valuefieldid").select2('val','')
		fieldvaluesaddgrid();
		//for touch
		mastertabid =4;
	});
	{//custom action overlay actions
		//more action overlay
		$('#moreactionbtn').click(function(){
			var datarowid = $('#moduleaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				//fetch selected module ids
				var allmodulevalueid = $('#moduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#moduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var moduleids = JSON.stringify(content);
				var customaction = $("#moduleaddgrid").jqGrid('getCell',datarowid,'customactions');
				if(customaction!=' ' && customaction!='') {
					if($('#moduleaddgrid .custom_cbox'+datarowid+' input').prop('checked')) {
						$('#modulegriddatarowid').val(datarowid);
						$.ajax({
							url:base_url+"Profile/customtoolbarinfoget",
							data:'datainformation=&toolbarids='+customaction+"&modids="+moduleids,
							type:'POST',
							dataType:'json',
							async:false,
							cache:false,
							success :function(datas) {
								$('.customactiondata').empty();
								$('.customactiondata').append(datas['datasets']);
								$('#modulegriddataactionid').val(datas['moduleaction']);
								setTimeout(function() {
									setTimeout(function(){
										if($("#customactionform .checkboxcusact[type='checkbox']").prop("checked")) {
											$('#checkuncheck').prop('checked',true);
										} else {
											$('#checkuncheck').prop('checked',false);
										}
									},100);
									$("#customtoolsoverlay").fadeIn();
								},100);
								//custom icon check/uncheck events
								$('.checkboxcusact').click(function(){
									if($("#customactionform .checkboxcusact[type='checkbox']").prop("checked")) {
										$('#checkuncheck').prop('checked',true);
									} else {
										$('#checkuncheck').prop('checked',false);
									}
								});
							}
						});
					} else {
						if($('#moduleaddgrid .custom_cbox').hasClass('custom_cbox'+datarowid+'')) {
							alertpopup("Please enable a custom option");
						}
					}
				} else {
					alertpopup("Custom action(s) not exit");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//set custom action id
		$('#custactionverlaybtn').click(function(){
			var rowid = $('#modulegriddatarowid').val();
			var n = jQuery(".checkboxcusact:checked").length;
			var usertoolbarid = new Array();
			if (n > 0){
				$(".checkboxcusact:checked").each(function(){
					usertoolbarid.push($(this).val());
				});
			} else {
				$('#moduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			var usrtoolbarid = usertoolbarid.join(',');
			var modtoolbarid = $('#modulegriddataactionid').val();
			var custtoolbarids = usrtoolbarid+"||"+modtoolbarid;
			$("#moduleaddgrid").jqGrid('setCell',rowid,'customactions',custtoolbarids);
			setTimeout(function() {
				$("#customtoolsoverlay").fadeOut();
				$('#modulegriddatarowid').val('');
				$('#modulegriddataactionid').val('');
			},100);
		});
		//more action overlay close
		$('#cusoverlayclose').click(function(){
			var rowid = $('#modulegriddatarowid').val();
			var n = jQuery(".checkboxcusact:checked").length;
			var usertoolbarid = new Array();
			if (n <= 0) {
				$('#moduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			$("#customtoolsoverlay").fadeOut();
			$('#modulegriddatarowid').val('');
		});
	}
	{//sub module process overlay
		//sub module list icon
		$('#moremoduleicon').click(function(){
			var datarowid = $('#moduleaddgrid').jqGrid('getGridParam','selrow');
			if (datarowid) {
				var submodule = $("#moduleaddgrid").jqGrid('getCell',datarowid,'submodules');
				if(submodule == '*') {
					if($('#moduleaddgrid .moduleid_cbox'+datarowid+' input').prop('checked')) {
						$('#mastermodgridrowid').val(datarowid);
						$("#submoduleoverlay").fadeIn();
						var submoduledata = $("#moduleaddgrid").jqGrid('getCell',datarowid,'submodulesdata');
						var data = jQuery.parseJSON(submoduledata);
						if(submoduledata !== '' ) {
							$("#submoduleaddgrid").jqGrid("clearGridData",true);
							setTimeout(function(){
								$("#submoduleaddgrid").setGridParam({datatype: 'json'});
								for (var x = 0; x < data.length; x++) {
									$("#submoduleaddgrid").addRowData(x,data[x]);
								}
								$("#submoduleaddgrid").trigger("reloadGrid");
							},500);
						} else {
							submoduleaddgrid();
						}
					} else {
						alertpopup("Please enable module");
					}
				} else {
					alertpopup("Sub module(s) not exit");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//sub module overlay close
		$('#sbumodoverlayclose').click(function(){
			var achkcount = $('#submoduleaddgrid .moduleid_cbox input').lenght;
			var ascount = 0;
			var acount = 0;
			$('#submoduleaddgrid .moduleid_cbox input').each(function () {
				ascount = (this.checked ? ++acount : ascount);
			});
			var rowid = $('#mastermodgridrowid').val();
			if(ascount==0) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',false);
			}
			$('#mastermodgridrowid').val('');
			$("#submoduleoverlay").fadeOut();
		});
		//sub module more action overlay
		$('#submodmoreactionbtn').click(function(){
			var datarowid = $('#submoduleaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) {
				//fetch selected modude ids
				var allmodulevalueid = $('#moduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#moduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var moduleids = JSON.stringify(content);
				//fetch selected sub module ids
				var allsubmodulevalueid = $('#submoduleaddgrid').jqGrid('getDataIDs');
				var mi = '';
				var content = {};
				$.each(allsubmodulevalueid,function(index,value) {
					content[index] = {};
					mi = $('#submoduleaddgrid .moduleid_cbox'+value+' input').prop('checked') ? value : 1;
					content[index]=mi;
				});
				var submoduleids = JSON.stringify(content);
				var customaction = $("#submoduleaddgrid").jqGrid('getCell',datarowid,'customactions');
				if(customaction != '' && customaction != ' ') {
					if($('#submoduleaddgrid .custom_cbox'+datarowid+' input').prop('checked')) {
						$('#submodulegriddatarowid').val(datarowid);
						$.ajax({
							url:base_url+"Profile/subcustomtoolbarinfoget",
							data:'datainformation=&toolbarids='+customaction+"&submodids="+submoduleids+"&modids="+moduleids,
							type:'POST',
							dataType:'json',
							async:false,
							cache:false,
							success :function(datas) {
								$('.subcustomactiondata').empty();
								$('.subcustomactiondata').append(datas['datasets']);
								$('#submodulegriddataactionid').val(datas['moduleaction']);
								setTimeout(function() {
									setTimeout(function(){
										if($("#subcustomactionform .subcheckboxcusact[type='checkbox']").prop("checked")) {
											$('#subcheckuncheck').prop('checked',true);
										} else {
											$('#subcheckuncheck').prop('checked',false);
										}
									},100);
									$("#subcustomtoolsoverlay").fadeIn();
								},100);
								//sub module custom icon check/uncheck events
								$('.subcheckboxcusact').click(function(){
									if($("#subcustomactionform .subcheckboxcusact[type='checkbox']").prop("checked")) {
										$('#subcheckuncheck').prop('checked',true);
									} else {
										$('#subcheckuncheck').prop('checked',false);
									}
								});
							}
						});
					} else {
						if($('#submoduleaddgrid .custom_cbox').hasClass('custom_cbox'+datarowid+'')) {
							alertpopupdouble("Please enable a custom option");
						}
					}
				} else {
					alertpopup("More action(s) not exit");
				}
			} else {
				alertpopupdouble("Please select a row");
			}
		});
		//sub module save
		$('#rolesubmodeulesbt').click(function(){
			var achkcount = $('#submoduleaddgrid .moduleid_cbox input').length;
			var ascount = 0;
			var acount = 0;
			$('#submoduleaddgrid .moduleid_cbox input').each(function () {
				ascount = (this.checked ? ++acount : ascount);
			});
			var rowid = $('#mastermodgridrowid').val();
			if(ascount!=0) {
				var gridData = jQuery("#submoduleaddgrid").jqGrid('getRowData');
				var submodulegriddata = JSON.stringify(gridData);
				$("#moduleaddgrid").jqGrid('setCell',rowid,'submodulesdata',submodulegriddata);
			} else {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',false);
			}
			$("#submoduleoverlay").fadeOut();
			$("#submoduleaddgrid").jqGrid("clearGridData", true);
			$('#mastermodgridrowid').val('');
		});
		//set sub module custom action
		$('#subcustactionverlaybtn').click(function(){
			var rowid = $('#submodulegriddatarowid').val();
			var n = jQuery("#subcustomactionform input:checkbox:checked").length;
			var usertoolbarid = new Array();
			if (n > 0){ 
				$("#subcustomactionform input:checkbox:checked").each(function(){
					usertoolbarid.push($(this).val());
				});
			} else {
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			var usrtoolbarid = usertoolbarid.join(',');
			var modtoolbarid = $('#submodulegriddataactionid').val();
			var custtoolbarids = usrtoolbarid+"||"+modtoolbarid;
			$("#submoduleaddgrid").jqGrid('setCell',rowid,'customactions',custtoolbarids);
			setTimeout(function() {
				$("#subcustomtoolsoverlay").fadeOut();
				$('#submodulegriddatarowid').val('');
				$('#submodulegriddataactionid').val('');
			},100);
		});
		//custom action sub module overlay close
		$('#subcusoverlayclose').click(function(){
			var rowid = $('#submodulegriddatarowid').val();
			var n = jQuery("#subcustomactionform input:checkbox:checked").length;
			if (n <= 0){
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').trigger('click');
				$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',false);
			}
			$("#subcustomtoolsoverlay").fadeOut();
			$('#submodulegriddatarowid').val('');
			$('#submodulegriddataactionid').val('');
		});
	}
	{//profile based module and action submit
		$("#profilefieldsbtbtn").click(function() {
		//module grid details
			var allmoduleid = $('#moduleaddgrid').jqGrid('getDataIDs');
			var gridData = {};
			var mid = '';
			var create = '';
			var read = '';
			var update = '';
			var cdelete = '';
			var custom = '';
			var caction = '';
			var submod = '';
			var submoddata = '';
			$.each(allmoduleid,function(index,value) {
				gridData[index] = {};
				gridData[index]['moduleid'] = {};
				gridData[index]['create'] = {};
				gridData[index]['read'] = {};
				gridData[index]['update'] = {};
				gridData[index]['delete'] = {};
				gridData[index]['custom'] = {};
				gridData[index]['customactions'] = {};
				gridData[index]['submodules'] = {};
				gridData[index]['submodulesdata'] = {};
				mid = $('#moduleaddgrid').jqGrid('getCell',value,'moduleid');
				create = $('#moduleaddgrid').jqGrid('getCell',value,'create');
				read = $('#moduleaddgrid').jqGrid('getCell',value,'read');
				update = $('#moduleaddgrid').jqGrid('getCell',value,'update');
				cdelete = $('#moduleaddgrid').jqGrid('getCell',value,'delete');
				custom = $('#moduleaddgrid').jqGrid('getCell',value,'custom');
				caction = $('#moduleaddgrid').jqGrid('getCell',value,'customactions');
				submod = $('#moduleaddgrid').jqGrid('getCell',value,'submodules');
				submoddata = $('#moduleaddgrid').jqGrid('getCell',value,'submodulesdata');
				gridData[index]['moduleid']=mid;
				gridData[index]['create']=create;
				gridData[index]['read']=read;
				gridData[index]['update']=update;
				gridData[index]['delete']=cdelete;
				gridData[index]['custom']=custom;
				gridData[index]['customactions']=caction;
				gridData[index]['submodules']=submod;
				gridData[index]['submodulesdata']=submoddata;
			});
			var modulegriddata = JSON.stringify(gridData);
			var profileid = $("#moduleprofileid").val();
			var amp = '&';
			var datainformation = amp+'modulegriddata='+modulegriddata+amp+'profileid='+profileid+amp+'allmoduleid='+allmoduleid;
			if(profileid != '') {
				$("#processoverlay").show();
				$.ajax({
					url:base_url+"Profile/profilemodulesubmit",
					type: "POST",
					data: "datas=" + datainformation,
					cache:false,
					success :function(msg) {
						setTimeout(function() {
							moduleaddgrid();
						},100);
						$("#processoverlay").hide();
						alertpopup('Data(s) Stored Successfully');
					}
				});
			} else {
				alertpopup('Please select the profile');
			}
			// For Touch
			masterfortouch = 0;
		});
	}
	{//profile module field submit
		$("#profilemodulefieldsbtbtn").click(function() {
			var profid = $("#fieldprofileid").val();
			var modid = $("#fieldmoduleid").val();
			if(profid != '' && modid != '') {
				modulefieldactiveupdate();
			} else {
				$("#processoverlay").hide();
				alertpopup('Please select the profile and module');
			}
			// For Touch
			masterfortouch = 0;
		});
	}
	//field value profile dd change
	$('#valueprofileid').change(function(){
		$("#valuefieldid").empty();
		$("#valuefieldid").select2('val','');
		$("#valuemoduleid").select2('val','');
		fieldvaluesaddgrid();
	});
	{
		//custom action overlay
		$('#checkuncheck').click(function(){
			if($('#checkuncheck').prop('checked')) {
				$('.checkboxcusact').prop('checked',true);
			} else {
				$('.checkboxcusact').prop('checked',false);
			}
		});
		//sub module custom action overlay
		$('#subcheckuncheck').click(function(){
			if($('#subcheckuncheck').prop('checked')) {
				$('.subcheckboxcusact').prop('checked',true);
			} else {
				$('.subcheckboxcusact').prop('checked',false);
			}
		});
	}
	{//refresh icons actions
		//module grid reload & role reset
		$('#profilemodreload').click(function(){
			$('#moduleprofileid').select2('val','').trigger('change');
		});
		//module field grid reload, roles & module reset
		$('#profilefieldreload').click(function(){
			$("#fieldprofileid").select2('val','').trigger('change');
		});
		//module values grid reload, roles,module & field reset
		$('#profilevaluereload').click(function(){
			$("#valueprofileid").select2('val','').trigger('change');
		});
	}
});
//Profile Add Grid
function profileaddgrid() {
	$("#profileaddgrid").jqGrid({
		url:base_url + "Profile/profilegriddatafetch",
		datatype: "xml",
		colNames:['Profile Id','Profile Name','Description','Status'],
   	    colModel:
        [
			{name:'profileid',index:'profileid',hidden:true}, 
			{name:'profilename',index:'profilename'},
			{name:'description',index:'description'},
			{name:'statusname',index:'statusname'}
		],
		pager:'#profileaddgridnav',
		sortname:'profileid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
	});
	//Inner Grid Height and Width Set Based On Screen Size
	innergridwidthsetbasedonscreen('profileaddgrid');
	formwithgridclass("profileaddgrid");
	$(".ui-pg-input").attr('readonly',true);
}
//Module Add Grid
function moduleaddgrid() {
	$('#moduleaddgrid').jqGrid('GridUnload');
	var id = 1;
	var profileid = $("#moduleprofileid").val();
	id = ( (profileid == "")? "1" : profileid );
	$("#moduleaddgrid").jqGrid({
		url:base_url+'Profile/profilebasedmodulelist?profileid='+id,
		datatype: "xml",
		colNames:['Module Id','Menu Category','Module Name','<input type="checkbox" id="moduleid_cnbox" /> Module Visible','<input type="checkbox" id="create_cnbox" /> Create','<input type="checkbox" id="read_cnbox" /> View','<input type="checkbox" id="update_cnbox" />  Edit','<input type="checkbox" id="delete_cnbox" /> Delete','<input type="checkbox" id="custom_cnbox" /> Custom','Status','Custom Actions','Sub Module','Sub Module Data'],
   	    colModel:
        [
			{name:'mid',index:'module.moduleid',hidden:true, sortable: false}, 
			{name:'menucategoryname',index:'menucategory.menucategoryname', sortable: false, width:150},
			{name:'modulename',index:'module.modulename', sortable: false, width:150},
			{name:'moduleid',index:'moduleid', editable: true, search: false, classes:'moduleid_cbox', sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'moduleid');}, width:20 },
			{name:'create',index:'', editable: true, search: false, classes:'create_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'create');}, width:20 },
			{name:'read',index:'', editable: true, search: false, classes:'read_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'read');}, width:20},
			{name:'update',index:'', editable: true, search: false, classes:'update_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'update');}, width:20},
			{name:'delete',index:'', editable: true, search: false, classes:'delete_cbox', align:"left", sortable: false, editoptions: { value: "true:false:empty" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'delete');}, width:20},
			{name:'custom',index:'', editable: true, search: false, classes:'custom_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'custom');}, width:20},
			{name:'status',index:'statusname', width:100, align:"left", sortable: false, search: false,},
			{name:'customactions',index:'customactions', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'customactions');},hidden:true, width:30},
			{name:'submodules',index:'submodules', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'submodules');},hidden:true, width:30},
			{name:'submodulesdata',index:'submodulesdata', align:"left", sortable: false, search: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'submodulesdata');},hidden:true, width:30}
		],
		pager:'#moduleaddgridnav',
		sortname:'moduleid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:1000,
		rowList:[500,1000],
		loadComplete: function() {			
			//visible
			$("th#moduleaddgrid_moduleid input").unbind('click');
			$("th#moduleaddgrid_moduleid input").click(function(e){
				if( $("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked') ) {
					$('#moduleaddgrid .moduleid_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'moduleid');
				} else {
					$('#moduleaddgrid .moduleid_cbox input').prop('checked',false);
					checkuncheckheaderandrowfield(false,'moduleid');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//create
			$("th#moduleaddgrid_create input").unbind('click');
			$("th#moduleaddgrid_create input").click(function(e){
				if( $("th#moduleaddgrid_create input#create_cnbox").prop('checked') ) {
					$('#moduleaddgrid .create_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'create');
				} else {
					$('#moduleaddgrid .create_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//view
			$("th#moduleaddgrid_read input").unbind('click');
			$("th#moduleaddgrid_read input").click(function(e){
				if( $("th#moduleaddgrid_read input#read_cnbox").prop('checked') ) {
					$('#moduleaddgrid .read_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'view');
				} else {
					$('#moduleaddgrid .read_cbox input').prop('checked',false);
					checkuncheckheaderandrowfield(false,'view');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Edit
			$("th#moduleaddgrid_update input").unbind('click');
			$("th#moduleaddgrid_update input").click(function(e){
				if( $("th#moduleaddgrid_update input#update_cnbox").prop('checked') ) {
					$('#moduleaddgrid .update_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'edit');
				} else {
					$('#moduleaddgrid .update_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Delete
			$("th#moduleaddgrid_delete input").unbind('click');
			$("th#moduleaddgrid_delete input").click(function(e){
				if( $("th#moduleaddgrid_delete input#delete_cnbox").prop('checked') ) {
					$('#moduleaddgrid .delete_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'delete');
				} else {
					$('#moduleaddgrid .delete_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Custom
			$("th#moduleaddgrid_custom input").unbind('click');
			$("th#moduleaddgrid_custom input").click(function(e){
				if( $("th#moduleaddgrid_custom input#custom_cnbox").prop('checked') ) {
					$('#moduleaddgrid .custom_cbox input').prop('checked',true);
					checkuncheckheaderandrowfield(true,'custom');
				} else {
					$('#moduleaddgrid .custom_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			$('#moduleaddgrid .moduleid_cbox input').click(function(){ //visible
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			});
			$('#moduleaddgrid .create_cbox input').click(function(){//create
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			});
			$('#moduleaddgrid .read_cbox input').click(function(){//view
				chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			});
			$('#moduleaddgrid .update_cbox input').click(function(){//Edit
				chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			});
			$('#moduleaddgrid .delete_cbox input').click(function(){//Delete
				chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			});
			$('#moduleaddgrid .custom_cbox input').click(function(){//Custom
				chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
			});
			//row check check/uncheck events
			//moduleid
			$('#moduleaddgrid .moduleid_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked')) {
					$("#moduleaddgrid").setSelection(rowid, true);
					var submodule = $("#moduleaddgrid").jqGrid('getCell',rowid,'submodules');
					if(submodule == '*') {
						$('#moremoduleicon').trigger('click');
					}
					checkuncheckrowfield(true,'moduleid',rowid);
				} else {
					checkuncheckrowfield(false,'moduleid',rowid);
				}
			});
			//create
			$('#moduleaddgrid .create_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'create',rowid);
				} else {
					checkuncheckrowfield(false,'create',rowid);
				}
			});
			//view
			$('#moduleaddgrid .read_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .read_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'view',rowid);
				} else {
					checkuncheckrowfield(false,'view',rowid);
				}
			});
			//edit
			$('#moduleaddgrid .update_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'edit',rowid);
				} else {
					checkuncheckrowfield(false,'edit',rowid);
				}
			});
			//delete
			$('#moduleaddgrid .delete_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked')) {
					checkuncheckrowfield(true,'delete',rowid);
				} else {
					checkuncheckrowfield(false,'delete',rowid);
				}
			});
			//custom
			$('#moduleaddgrid .custom_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked')) {
					$("#moduleaddgrid").setSelection(rowid, true);
					var customaction = $("#moduleaddgrid").jqGrid('getCell',rowid,'customactions');
					if(customaction != ' ' && customaction != '') {
						$('#moreactionbtn').trigger('click');
					}
					checkuncheckrowfield(true,'custom',rowid);
				} else {
					checkuncheckrowfield(false,'custom',rowid);
				}
			});
		}
	});
	//pagination remove
	$("#moduleaddgridnav_center .ui-pg-table").addClass('hidedisplay');	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('moduleaddgrid');
	formwithgridclass('moduleaddgrid');
	var modulesistb = ["1","moduleaddgrid"];
	createistb(modulesistb);
}
//Module Add Grid
function submoduleaddgrid() {
	var moduleid = $("#mastermodgridrowid").val();
	moduleid = ( (moduleid == "")? "1" : moduleid );
	var profileid = $("#moduleprofileid").val();
	profileid = ( (profileid == "")? "1" : profileid );
	$('#submoduleaddgrid').jqGrid('GridUnload');
	$("#submoduleaddgrid").jqGrid({
		url:base_url+'Profile/profilebasedsubmodulelist?pid='+profileid+"&mid="+moduleid,
		datatype: "xml",
		colNames:['Moduleid','Menu Category','Module Name','<input type="checkbox" id="moduleid_cnbox" /> Module Visible','<input type="checkbox" id="create_cnbox" /> Create','<input type="checkbox" id="read_cnbox" /> View','<input type="checkbox" id="update_cnbox" />  Edit','<input type="checkbox" id="delete_cnbox" /> Delete','<input type="checkbox" id="custom_cnbox" /> Custom','Status','Custom Actions'],
   	    colModel:
        [
			{name:'modid',index:'modid', sortable: false,hidden:true},
			{name:'menucategoryname',index:'menucategoryname', sortable: false, width:150},
			{name:'modulename',index:'modulename', sortable: false, width:200},
			{name:'moduleid',index:'moduleid', editable: true, classes:'moduleid_cbox', sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'moduleid');}, width:200 },
			{name:'create',index:'', editable: true, classes:'create_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'create');}, width:25 },
			{name:'read',index:'', editable: true, classes:'read_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'read');}, width:30 },
			{name:'update',index:'', editable: true, classes:'update_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'update');}, width:25 },
			{name:'delete',index:'', editable: true, classes:'delete_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'delete');}, width:25 },
			{name:'custom',index:'', editable: true, classes:'custom_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'custom');}, width:25 },
			{name:'status',index:'statusname', align:"left", sortable: false, width:250 },
			{name:'customactions',index:'customactions', align:"left", sortable: false, formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'customactions');},hidden:true, width:10 }
		],
		pager:'#submoduleaddgridnav',
		sortname:'moduleid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function() {
			//visible
			$("th#submoduleaddgrid_moduleid input").unbind('click');
			$("th#submoduleaddgrid_moduleid input").click(function(e){
				if( $("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .moduleid_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'moduleid');
				} else {
					$('#submoduleaddgrid .moduleid_cbox input').prop('checked',false);
					subcheckuncheckheaderandrowfield(false,'moduleid');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//create
			$("th#submoduleaddgrid_create input").unbind('click');
			$("th#submoduleaddgrid_create input").click(function(e){
				if( $("th#submoduleaddgrid_create input#create_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .create_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'create');
				} else {
					$('#submoduleaddgrid .create_cbox input').prop('checked',false);
					subcheckuncheckheaderandrowfield(false,'create');
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//view
			$("th#submoduleaddgrid_read input").unbind('click');
			$("th#submoduleaddgrid_read input").click(function(e){
				if( $("th#submoduleaddgrid_read input#read_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .read_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'view');
				} else {
					$('#submoduleaddgrid .read_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Edit
			$("th#submoduleaddgrid_update input").unbind('click');
			$("th#submoduleaddgrid_update input").click(function(e){
				if( $("th#submoduleaddgrid_update input#update_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .update_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'edit');
				} else {
					$('#submoduleaddgrid .update_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Delete
			$("th#submoduleaddgrid_delete input").unbind('click');
			$("th#submoduleaddgrid_delete input").click(function(e){
				if( $("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .delete_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'delete');
				} else {
					$('#submoduleaddgrid .delete_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//Custom
			$("th#submoduleaddgrid_custom input").unbind('click');
			$("th#submoduleaddgrid_custom input").click(function(e){
				if( $("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked') ) {
					$('#submoduleaddgrid .custom_cbox input').prop('checked',true);
					subcheckuncheckheaderandrowfield(true,'custom');
				} else {
					$('#submoduleaddgrid .custom_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			$('#submoduleaddgrid .moduleid_cbox input').click(function(){ //visible
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			});
			$('#submoduleaddgrid .create_cbox input').click(function(){//create
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			});
			$('#submoduleaddgrid .read_cbox input').click(function(){//view
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			});
			$('#submoduleaddgrid .update_cbox input').click(function(){//Edit
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			});
			$('#submoduleaddgrid .delete_cbox input').click(function(){//Delete
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			});
			$('#submoduleaddgrid .custom_cbox input').click(function(){//Custom
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			});
			//row check check/uncheck events
			//moduleid
			$('#submoduleaddgrid .moduleid_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked')) {
					$("#submoduleaddgrid").setSelection(rowid, true);
					setTimeout(function(){
						$('#submodmoreactionbtn').trigger('click');
					},50);
					subcheckuncheckrowfield(true,'moduleid',rowid);
				} else {
					subcheckuncheckrowfield(false,'moduleid',rowid);
				}
			});
			//create
			$('#submoduleaddgrid .create_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'create',rowid);
				} else {
					subcheckuncheckrowfield(false,'create',rowid);
				}
			});
			//view
			$('#submoduleaddgrid .read_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'view',rowid);
				} else {
					subcheckuncheckrowfield(false,'view',rowid);
				}
			});
			//edit
			$('#submoduleaddgrid .update_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'edit',rowid);
				} else {
					subcheckuncheckrowfield(false,'edit',rowid);
				}
			});
			//delete
			$('#submoduleaddgrid .delete_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked')) {
					subcheckuncheckrowfield(true,'delete',rowid);
				} else {
					subcheckuncheckrowfield(false,'delete',rowid);
				}
			});
			//custom
			$('#submoduleaddgrid .custom_cbox').click(function(){
				var rowid = $(this).data('datacellrowid');
				if($('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked')) {
					$("#submoduleaddgrid").setSelection(rowid,true);
					setTimeout(function(){
						$('#submodmoreactionbtn').trigger('click');
					},50);
					subcheckuncheckrowfield(true,'custom',rowid);
				} else {
					subcheckuncheckrowfield(false,'custom',rowid);
				}
			});
		}
	});
	//pagination remove
	$("#submoduleaddgridnav_center .ui-pg-table").addClass('hidedisplay');	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('submoduleaddgrid');
	formwithgridclass("submoduleaddgrid");
}
//chk/unchk all module
function checkuncheckheaderandrowfield(option,fieldtype) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			$('#moduleaddgrid .read_cbox input').prop('checked',option);
			$('#moduleaddgrid .update_cbox input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#moduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#moduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#moduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#moduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'create':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .read_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_read input#read_cnbox").prop('checked',option);
			break;
		case 'view':
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			$('#moduleaddgrid .update_cbox input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#moduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#moduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#moduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'edit':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'delete':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#moduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#moduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'custom':
			//row
			$('#moduleaddgrid .moduleid_cbox input').prop('checked',option);
			//header
			$("th#moduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			break;
	}
}
//chk/unchk single row
function checkuncheckrowfield(option,fieldtype,rowid) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
		break;
		case 'create':
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			}
		break;
		case 'view':
			//row
			$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#moduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('moduleaddgrid','moduleaddgrid_custom','custom_cnbox','custom_cbox');
		break;
		case 'edit':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			}
		break;
		case 'delete':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#moduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('moduleaddgrid','moduleaddgrid_create','create_cnbox','create_cbox');
			}
		break;
		case 'custom':
			//row
			if(option == true) {
				$('#moduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('moduleaddgrid','moduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			}
		break;
	}
}
//chk/unchk all sub module
function subcheckuncheckheaderandrowfield(option,fieldtype) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'create':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_read input#read_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_update input#update_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_delete input#delete_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_custom input#custom_cnbox").prop('checked',option);
			break;
		case 'view':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'edit':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'delete':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
		case 'custom':
			//row
			$('#submoduleaddgrid .moduleid_cbox input').prop('checked',option);
			$('#submoduleaddgrid .create_cbox input').prop('checked',option);
			//header
			$("th#submoduleaddgrid_moduleid input#moduleid_cnbox").prop('checked',option);
			$("th#submoduleaddgrid_create input#create_cnbox").prop('checked',option);
			break;
	}
}
//chk/unchk sub module single row
function subcheckuncheckrowfield(option,fieldtype,rowid) {
	switch (fieldtype) {
		case 'moduleid':
			//row
			$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'create':
			//row
			$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .read_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .update_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .delete_cbox'+rowid+' input').prop('checked',option);
			$('#submoduleaddgrid .custom_cbox'+rowid+' input').prop('checked',option);
			//check / uncheck header chkbox
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_read','read_cnbox','read_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_update','update_cnbox','update_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_delete','delete_cnbox','delete_cbox');
			chkunckkheader('submoduleaddgrid','submoduleaddgrid_custom','custom_cnbox','custom_cbox');
			break;
		case 'view':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
			}
			break;
		case 'edit':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'delete':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
		case 'custom':
			//row
			if(option == true) {
				$('#submoduleaddgrid .moduleid_cbox'+rowid+' input').prop('checked',option);
				$('#submoduleaddgrid .create_cbox'+rowid+' input').prop('checked',option);
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_moduleid','moduleid_cnbox','moduleid_cbox');
				chkunckkheader('submoduleaddgrid','submoduleaddgrid_create','create_cnbox','create_cbox');
			}
			break;
	}
}
//Field Add Grid
function fieldsaddgrid() {
	var profid= $("#fieldprofileid").val();
	var modid = $("#fieldmoduleid").val();
	var pid = ((profid=="")? '1':profid);
	var mid = ((modid=="")? '1':modid);
	$('#fieldsaddgrid').jqGrid('GridUnload');
	$("#fieldsaddgrid").jqGrid({
		url:base_url+'Profile/modulebasedfieldslist?profileid='+pid+"&moduleid="+mid,
		datatype: "xml",
		colNames:['Field Id','Module Id','Field Name','<input type="checkbox" id="active_cnbox" /> Active','<input type="checkbox" id="visible_cnbox" /> Visible','<input type="checkbox" id="readonly_cnbox" /> Readonly','Status'],
		colModel:
        [
			{name:'modulefieldid',index:'modulefieldid',hidden:true},
			{name:'moduleid',index:'moduleid',hidden:true},
			{name:'fieldlabel',index:'fieldlabel', sortable: false},
			{name:'active',index:'', editable: true,classes:'active_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'active'); } },
			{name:'visible',index:'', editable: true,classes:'visible_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'visible'); } },
			{name:'readonly',index:'', editable: true,classes:'readonly_cbox', align:"left", sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) { return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'readonly'); } },
			{name:'status',index:'', align:"left", sortable: false}
		],
		pager:'#fieldsaddgridnav',
		sortname:'modulefieldid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		rowNum:2000,
		rowList:[1000,2000],
		toolbar: [false,"top"],
		height: '100%',
		width: 'auto',
		loadComplete: function () {
			//active
			$("th#fieldsaddgrid_active input").unbind('click');
			$("th#fieldsaddgrid_active input").click(function(e){
				if( $("th#fieldsaddgrid_active input#active_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .active_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .active_cbox input').prop('checked',false);
					$('th#fieldsaddgrid_active input#active_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//visible
			$("th#fieldsaddgrid_visible input").unbind('click');
			$("th#fieldsaddgrid_visible input").click(function(e){
				if( $("th#fieldsaddgrid_visible input#visible_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .visible_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .visible_cbox input').prop('checked',false);
					$('th#fieldsaddgrid_visible input#visible_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//read only
			$("th#fieldsaddgrid_readonly input").unbind('click');
			$("th#fieldsaddgrid_readonly input").click(function(e){
				if( $("th#fieldsaddgrid_readonly input#readonly_cnbox").prop('checked') ) {
					$('#fieldsaddgrid .readonly_cbox input').prop('checked',true);
				} else {
					$('#fieldsaddgrid .readonly_cbox input').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_active','active_cnbox','active_cbox');
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_visible','visible_cnbox','visible_cbox');
			chkunckkheader('fieldsaddgrid','fieldsaddgrid_readonly','readonly_cnbox','readonly_cbox');
			$('#fieldsaddgrid .active_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_active','active_cnbox','active_cbox');
			});
			$('#fieldsaddgrid .visible_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_visible','visible_cnbox','visible_cbox');
			});
			$('#fieldsaddgrid .readonly_cbox input').click(function(){
				chkunckkheader('fieldsaddgrid','fieldsaddgrid_readonly','readonly_cnbox','readonly_cbox');
			});
		}
	});
	$("#fieldsaddgrid_rn input[type='checkbox']").hide();
	var firstcolhide = $("#fieldsaddgrid_rn").next('th').attr('id');
	$('#'+firstcolhide+' input[type="checkbox"]').css('opacity','0');
	$('#'+firstcolhide+' .ui-jqgrid-sortable').css('left','-2px');	
	//pagination remove
	$("#fieldsaddgridnav_center .ui-pg-table").addClass('hidedisplay');
	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('fieldsaddgrid');
	formwithgridclass('fieldsaddgrid');
}
//Field values Add Grid
function fieldvaluesaddgrid() {
	var modulefieldid = $("#valuefieldid").val();
	var tablefieldid = $("#valuefieldid").find('option:selected').data('columnname');
	var modid = $("#valuefieldid").find('option:selected').data('moduletabid');
	var profid = $("#valueprofileid").val();
	$('#fieldvaluesaddgrid').jqGrid('GridUnload');
	$("#fieldvaluesaddgrid").jqGrid({
		url:base_url+'Profile/profilefieldvalueget?tablefieldid='+tablefieldid+"&modid="+modid+"&profid="+profid+"&modulefieldid="+modulefieldid,
		datatype: "xml",
		colNames:['S No','Field Values','<input type="checkbox" id="status_cnbox" /> Visible','Status'],
   	    colModel:
        [
			{name:'fieldid',index:'fieldid', width:1,hidden:true}, 
			{name:'fieldname',index:'fieldname', sortable: false},
			{name:'status',index:'', editable: true, classes:'status_cbox', sortable: false, editoptions: { value: "true:false" },formatter:'checkbox', formatoptions: {disabled : false}, cellattr: function(rowId, cellValue, rawObject, cm, rdata) {  return hidegridcell(rowId, cellValue, rawObject, cm, rdata,'status');} },
			{name:'statusname',index:'', align:"left", sortable: false}
		],
		pager:'#fieldvaluesaddgridnav',
		sortname:'fieldid',
		sortorder:'asc',
		viewrecords:false,
		loadonce:false,
		autowidth:false,
		toolbar: [false, "top"],
		height: '100%',
		width: 'auto',
		rowNum:2000,
		rowList:[1000,2000],
		loadComplete: function () {
			//active
			$("th#fieldvaluesaddgrid_status input").unbind('click');
			$("th#fieldvaluesaddgrid_status input").click(function(e){
				if( $("th#fieldvaluesaddgrid_status input#status_cnbox").prop('checked') ) {
					$('#fieldvaluesaddgrid .status_cbox input').prop('checked',true);
				} else {
					$('#fieldvaluesaddgrid .status_cbox input').prop('checked',false);
					$('th#fieldvaluesaddgrid_status input#status_cnbox').prop('checked',false);
				}
				e = e||event;/* get IE event ( not passed ) */ 
				e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
			});
			//check / uncheck header chkbox
			chkunckkheader('fieldvaluesaddgrid','fieldvaluesaddgrid_status','status_cnbox','status_cbox');
			$('#fieldvaluesaddgrid .status_cbox input').click(function(){
				chkunckkheader('fieldvaluesaddgrid','fieldvaluesaddgrid_status','status_cnbox','status_cbox');
			});
		}
	});
	$("#fieldsaddgrid_rn input[type='checkbox']").hide();
	var firstcolhide = $("#fieldsaddgrid_rn").next('th').attr('id');
	$('#'+firstcolhide+' input[type="checkbox"]').css('opacity','0');
	$('#'+firstcolhide+' .ui-jqgrid-sortable').css('left','-2px');	
	//pagination remove
	$("#fieldvaluesaddgridnav_center .ui-pg-table").addClass('hidedisplay');
	
	//Inner Grid Height and Width Set Based On Screen Size
	localinnergridwidthsetbasedonscreen('fieldvaluesaddgrid');
	formwithgridclass('fieldvaluesaddgrid');
}
//checks to remove the restricted toolbar
function hidegridcell(rowId, cellValue, rawObject, cm, rdata,cellpoint) {
	if(rdata[cellpoint] == 'empty') {
		return result = ' class="cbox_ronly" style="opacity:0"';
	} else {
		return result = ' class="'+cellpoint+'_cbox '+cellpoint+'_cbox'+rowId+'" data-datacellrowid="'+rowId+'" ';
	}
}
//check/uncheck header check box
function chkunckkheader(gname,gdthname,hchkname,gdchkname) {
	var achkcount = $('#'+gname+' .'+gdchkname+' input').length;
	var ascount = 0;
	var acount = 0;
	$('#'+gname+' .'+gdchkname+' input').each(function () {
		ascount = (this.checked ? ++acount : ascount);
	});
	if( (achkcount==ascount) && (achkcount != '0') ) {
		$('th#'+gdthname+' input#'+hchkname+'').prop('checked',true);
	} else {
		$('th#'+gdthname+' input#'+hchkname+'').prop('checked',false);
	}
}
// Grid Height Width Settings
function gridsettings() {
	formwithgridclass("profileaddgrid");
	formwithgridclass("moduleaddgrid");
	formwithgridclass("fieldsaddgrid");
	formwithgridclass("fieldvaluesaddgrid");
	formwithgridclass("submoduleaddgrid");
	var profileistb = ["5", "profileaddgrid","moduleaddgrid","fieldsaddgrid","fieldvaluesaddgrid","submoduleaddgrid"];
	createistb(profileistb);
}
//profile add function
function profilenewdataaddfun() {
	var amp = '&';
	var formdata = $("#profilecreationform").serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url+"Profile/profileinformationcreate",
		data: "datas="+datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'TRUE') {
				resetFields();
				$("#profileaddgrid").trigger('reloadGrid');
				clearinlinesrchandrgrid('profileaddgrid');
				profiledropdowndataload();
				$("#profilesectionoverlay").addClass("closed");
				$("#profilesectionoverlay").removeClass("effectbox");
				alertpopup(savealert);
			} else {
				alertpopup('Operation failed.');
			}
			$("#processoverlay").hide();
		},
	});
}
//profile update function
function profileupdatedataaddfun() {
	var formdata = $("#profilecreationform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Profile/profileinformationupdate",
		data: "datas=" + datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'TRUE') {
				resetFields();
				$("#profileaddgrid").trigger('reloadGrid');
				clearinlinesrchandrgrid('profileaddgrid');
				$("#profileupdatebtn").hide();
				$("#profileaddbtn,#profiledelete").show();
				profiledropdowndataload();
				$("#profilesectionoverlay").addClass("closed");
				$("#profilesectionoverlay").removeClass("effectbox");
				alertpopup(savealert);
				// For Touch
				masterfortouch = 0;
				//keyboard shortcut variable
					saveformview=0;
			} else {
				alertpopup('Operation failed.');
			}
			$("#processoverlay").hide();
		},
	});
}
//profile delete function
function profiledeletefun(datarowid) {
	$.ajax({
		url: base_url + "Profile/profiledeletefunction?primaryid="+datarowid,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'True') {
				resetFields();
				$("#profileaddgrid").trigger('reloadGrid');
				clearinlinesrchandrgrid('profileaddgrid');
				$("#basedeleteoverlay").fadeOut();
				profiledropdowndataload();
				alertpopup('Profile deleted successfully');
			} else if(nmsg == "Fail") {
				resetFields();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Sorry profile not deleted');
			}
		},
	});	
}
//profile data fetch
function profiledatafetchfunction(datarowid) {
	$.ajax({
		url: base_url + "Profile/profiledatafetch?primaryid="+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var pname = data['pname'];
			var description = data['description'];
			$("#profilename").val(pname);
			$("#profiledescription").val(description);
			$("#profileprimaryid").val(datarowid);
			$("#profilename").focus();
		},
	});	
}
{//Profile drop down reload
	function profiledropdowndataload() {
		$('#moduleprofileid,#fieldprofileid,#valueprofileid').empty();
		$('#moduleprofileid,#fieldprofileid,#valueprofileid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Base/fetchdddataviewddval?dataname=profilename&dataid=profileid&datatab=profile',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#moduleprofileid,#fieldprofileid,#valueprofileid')
						.append($("<option></option>")
						.attr("data-profileid",data[index]['datasid'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}
			},
		});
	}
}
//module based field update
function modulefieldactiveupdate() {
	var gridData = $("#fieldsaddgrid").jqGrid('getRowData');
	var fieldgriddata = JSON.stringify(gridData);
	var profileid = $("#fieldprofileid").val();
	var moduleid = $("#fieldmoduleid").val();
	var amp = '&';
	var allmodulevalueid = $('#fieldsaddgrid').jqGrid('getDataIDs');
	var mf = '';
	var mi = '';
	var ac = '';
	var ro = '';
	var content = {};
	$.each(allmodulevalueid,function(index,value) {
		content[index] = {};
		content[index]['modulefieldid'] = {};
		content[index]['moduleid'] = {};
		content[index]['active'] = {};
		content[index]['visible'] = {};
		content[index]['readonly'] = {};
		mf = $('#fieldsaddgrid').jqGrid('getCell',value,'modulefieldid');
		mi = $('#fieldsaddgrid').jqGrid('getCell',value,'moduleid');
		ac = $('#fieldsaddgrid').jqGrid('getCell',value,'active');
		vi = $('#fieldsaddgrid').jqGrid('getCell',value,'visible');
		ro = $('#fieldsaddgrid').jqGrid('getCell',value,'readonly');
		content[index]['modulefieldid']=mf;
		content[index]['moduleid']=mi;
		content[index]['active']=ac;
		content[index]['visible']=vi;
		content[index]['readonly']=ro;
	});
	var fieldgriddata = JSON.stringify(content);
	var datainfo = "fieldgriddata="+fieldgriddata+amp+"profileid="+profileid+amp+"moduleid="+moduleid;
	if(profileid != '' && moduleid != '') {
		$("#processoverlay").show();
		$.ajax({
			url:base_url+"Profile/modulefieldsmodeupdate",
			type:'POST',
			data:"data="+amp+datainfo,
			cache:false,
			success :function(msg) {
				setTimeout(function() {
					fieldsaddgrid();
				},100);
				$("#processoverlay").hide();
				alertpopup('Data(s) stored successfully');
			}
		});
	} else {
		alertpopup('Please select the user role and module');
	}
}
//value drop down load
function valuedropdownload(profid,modid) {
	$('#valuefieldid').select2('val','');
	$('#valuefieldid').empty();
	$('#valuefieldid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Profile/valuedropdownloadfun?profid='+profid+'&modid='+modid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#valuefieldid')
					.append($("<option></option>")
					.attr("data-columnname",data[index]['columnname'])
					.attr("data-moduletabid",data[index]['modtabid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#valuefieldid').trigger('change');
		},
	});
}
//field value insertion
function valuefieldidinsertion(profid,modid,modulefield,fieldgriddata) {
	var amp = '&';
	var datainformation = amp+'profid='+profid+amp+'modid='+modid+amp+'modulefield='+modulefield+amp+'fieldgriddata='+fieldgriddata;
	$.ajax({
		url: base_url + "Profile/modulevaluesubmit",
		type: "POST",
		data: "datas=" + datainformation,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg == 'TRUE') {
				setTimeout(function() {
					fieldvaluesaddgrid();
				},100);
				$("#processoverlay").hide();
				alertpopup('Data(s) stored Successfully');
			} else{
				setTimeout(function() {
					fieldvaluesaddgrid();
				},100);
				$("#processoverlay").hide();
				alertpopup('Data(s) failed to store');
			}
		},
	});
}
//profile unique name chk
function profileuniquenamechk() {
	var value = $('#profilename').val();
	var primaryid = $('#profileprimaryid').val();
	var nmsg = "";
	if( value !="" ) {
		$.ajax({
			url:base_url+"Profile/profileuniquenamecheck",
			data:"data=&name="+value,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Profile name already exists!";
				}
			} else {
				return "Profile name already exists!";
			}
		}
	}
}