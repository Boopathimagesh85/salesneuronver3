$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		cardcommissionaddgrid();
		firstfieldfocus();
		cardcommissioncrudactionenable();
	}
	ACDELETE = 0;
	//hidedisplay
	$('#cardcommissiondataupdatesubbtn,#groupcloseaddform').hide();
	$('#cardcommissionsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			cardcommissionaddgrid();
		});
	}
	{//validation for Add  
		$('#cardcommissionsavebutton').click(function(e) {
			$('#cardcommissionname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#cardcommissionformaddwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#cardcommissionformaddwizard").validationEngine( {
			onSuccess: function() {
				cardcommissionaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update information
		$('#cardcommissiondataupdatesubbtn').click(function(e) {
			$('#cardcommissionname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#cardcommissionformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#cardcommissionformeditwizard").validationEngine({
			onSuccess: function() {
				cardcommissionupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				alertpopup(validationalert);
			}	
		});
	}
	{//action events
		$("#cardcommissionreloadicon").click(function() {
			clearform('cleardataform');
			$('#cardcommissionaddform').find('input, textarea, button, select').attr('disabled',true);
			$('#cardcommissionaddform').find("select").select2('disable');
			storagerefreshgrid();
			$('#cardcommissiondataupdatesubbtn').hide();
			$('#cardcommissionsavebutton').show();
			$("#deleteicon").show();
		});
		$( window ).resize(function() {
			maingridresizeheightset('cardcommissionaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
	}
	
	{//filter work
		//for toggle
		$('#cardcommissionviewtoggle').click(function() {
			if ($(".cardcommissionfilterslide").is(":visible")) {
				$('div.cardcommissionfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.cardcommissionfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#cardcommissionclosefiltertoggle').click(function(){
			$('div.cardcommissionfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#cardcommissionfilterddcondvaluedivhid").hide();
		$("#cardcommissionfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#cardcommissionfiltercondvalue").focusout(function(){
				var value = $("#cardcommissionfiltercondvalue").val();
				$("#cardcommissionfinalfiltercondvalue").val(value);
				$("#cardcommissionfilterfinalviewconid").val(value);
			});
			$("#cardcommissionfilterddcondvalue").change(function(){
				var value = $("#cardcommissionfilterddcondvalue").val();
				var fvalue = $("#cardcommissionfilterddcondvalue option:selected").attr('data-ddid');
				cardcommissionmainfiltervalue=[];
				if( $('#s2id_cardcommissionfilterddcondvalue').hasClass('select2-container-multi') ) {
					cardcommissionmainfiltervalue=[];
					$('#cardcommissionfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    cardcommissionmainfiltervalue.push($cvalue);
						$("#cardcommissionfinalfiltercondvalue").val(cardcommissionmainfiltervalue);
					});
					$("#cardcommissionfilterfinalviewconid").val(value);
				} else {
					$("#cardcommissionfinalfiltercondvalue").val(fvalue);
					$("#cardcommissionfilterfinalviewconid").val(value);
				}
			});
		}
		cardcommissionfiltername = [];
		$("#cardcommissionfilteraddcondsubbtn").click(function() {
			$("#cardcommissionfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#cardcommissionfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(cardcommissionaddgrid,'cardcommission');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
			$("#deleteicon").show();
		});
	}
	
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function cardcommissionaddgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#cardcommissionaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#cardcommissionsortcolumn").val();
	var sortord = $("#cardcommissionsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.cardcommissionheadercolsort').hasClass('datasort') ? $('.cardcommissionheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.cardcommissionheadercolsort').hasClass('datasort') ? $('.cardcommissionheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.cardcommissionheadercolsort').hasClass('datasort') ? $('.cardcommissionheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = '107';
	if(userviewid != '') {
		userviewid= '189';
	}
	var filterid = $("#cardcommissionfilterid").val();
	var conditionname = $("#cardcommissionconditionname").val();
	var filtervalue = $("#cardcommissionfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=cardcommission&primaryid=cardcommissionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#cardcommissionaddgrid').empty();
			$('#cardcommissionaddgrid').append(data.content);
			$('#cardcommissionaddgridfooter').empty();
			$('#cardcommissionaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('cardcommissionaddgrid');
			{//sorting
				$('.cardcommissionheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.cardcommissionheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.cardcommissionheadercolsort').hasClass('datasort') ? $('.cardcommissionheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.cardcommissionheadercolsort').hasClass('datasort') ? $('.cardcommissionheadercolsort.datasort').data('sortorder') : '';
					$("#cardcommissionsortorder").val(sortord);
					$("#cardcommissionsortcolumn").val(sortcol);
					var sortpos = $('#cardcommissionaddgrid .gridcontent').scrollLeft();
					cardcommissionaddgrid(page,rowcount);
					$('#cardcommissionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('cardcommissionheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					cardcommissionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#cardcommissionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					cardcommissionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#cardcommissionaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#cardcommissionpgrowcount').material_select();
		},
	});
}
function cardcommissioncrudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			e.preventDefault();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#cardcommissiondataupdatesubbtn').hide();
			$('#cardcommissionsavebutton').show();
			$("#accountid,#cardtypeid").prop('disabled',false);
			$('#cardcommissionprimarydataid').val('');
			loadaccountname(1);
			commissionchargevalidate();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#cardcommissionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			resetFields();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			$('#cardcommissiondataupdatesubbtn').show();
			$('#cardcommissionsavebutton').hide();
			loadaccountname(0);
			cardcommissioneditformdatainformationshow(datarowid);
			commissionchargevalidate();
			$("#accountid,#cardtypeid").prop('disabled',true);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function() {
		var datarowid = $('#cardcommissionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			$("#cardcommissionprimarydataid").val(datarowid);
			combainedmoduledeletealert('cardcommissiondeleteyes');
			if(ACDELETE == 0) {
				$("#cardcommissiondeleteyes").click(function(){
					var datarowid = $("#cardcommissionprimarydataid").val();
					cardcommissiondelete(datarowid);
				});
			}
			ACDELETE =1;
		} else {
			alertpopup("Please select a row");
		}	
	});
}
{//refresh grid
	function cardcommissionrefreshgrid() {
		var page = $('ul#cardcommissionpgnum li.active').data('pagenum');
		var rowcount = $('ul#cardcommissionpgnumcnt li .page-text .active').data('rowcount');
		cardcommissionaddgrid(page,rowcount);
	}
}
//new data add submit function
function cardcommissionaddformdata() {
	var formdata = $("#cardcommissionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Cardcommission/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				cardcommissionrefreshgrid();
				$("#groupsectionoverlay").removeClass("effectbox");
				$("#groupsectionoverlay").addClass("closed");
				alertpopup(savealert);				
            }else if(nmsg == 'LEVEL') {
				alertpopup('Maximum level of account reached !!! cannot create category further.');
			}
        },
    });
}
//old information show in form
function cardcommissioneditformdatainformationshow(datarowid) {
	var cardcommissionelementsname = $('#cardcommissionelementsname').val();
	var cardcommissionelementstable = $('#cardcommissionelementstable').val();
	var cardcommissionelementscolmn = $('#cardcommissionelementscolmn').val();
	var cardcommissionelementpartable = $('#cardcommissionelementspartabname').val();
	$.ajax(	{
		url:base_url+"Cardcommission/fetchformdataeditdetails?cardcommissionprimarydataid="+datarowid+"&cardcommissionelementsname="+cardcommissionelementsname+"&cardcommissionelementstable="+cardcommissionelementstable+"&cardcommissionelementscolmn="+cardcommissionelementscolmn+"&cardcommissionelementpartable="+cardcommissionelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				cardcommissionrefreshgrid();
			} else {
				var txtboxname = cardcommissionelementsname + ',cardcommissionprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = cardcommissionelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				Materialize.updateTextFields();
			}
		}
	});
	$('#cardcommissiondataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#cardcommissionsavebutton').hide();
}
//udate old information
function cardcommissionupdateformdata() {
	var parentaccountcatalogid=$("#parentaccountcatalogid").val();
	var editaccountcatalog=$("#cardcommissionprimarydataid").val();
		if(parentaccountcatalogid!=editaccountcatalog){
			var formdata = $("#cardcommissionaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
		    $.ajax({
		        url: base_url + "Cardcommission/datainformationupdate",
		        data: "datas=" + datainformation,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
						$('#cardcommissiondataupdatesubbtn').hide();
						$('#cardcommissionsavebutton').show();
						cardcommissionrefreshgrid();
						$("#groupsectionoverlay").removeClass("effectbox");
						$("#groupsectionoverlay").addClass("closed");
						resetFields();
						alertpopup(savealert);			
		            }else if(nmsg == 'FAILURE'){
						alertpopup("Check your Parent name mapping. ");
					}else if(nmsg == 'LEVEL') {
						alertpopup('Maximum level reached !!! cannot create category further.');
					}
		        },
		    });
	}
	 else{
		alertpopup("You cant map the same name as parent name");
	}
}
//udate old information
function cardcommissiondelete(datarowid) {
	var cardcommissionelementstable = $('#cardcommissionelementstable').val();
	var cardcommissionelementspartable = $('#cardcommissionelementspartabname').val();
    $.ajax({
        url: base_url + "Cardcommission/deleteinformationdata?cardcommissionprimarydataid="+datarowid+"&cardcommissionelementstable="+cardcommissionelementstable+"&cardcommissionparenttable="+cardcommissionelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	cardcommissionrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	cardcommissionrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
function cardcommissioncheck() {
	var primaryid = $("#cardcommissionprimarydataid").val();
	var accname = $("#cardcommissionname").val();
	var elementpartable = 'cardcommission';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Card name already exists!";
				}
			}else {
				return "Card name already exists!";
			}
		} 
	}
}
//Kumaresan - Short name unique check
function cardcommissionshortnamecheck() {
	var primaryid = $("#cardcommissionprimarydataid").val();
	var accname = $("#cardcommissionshortname").val();
	var fieldname = 'cardcommissionshortname';
	var elementpartable = 'cardcommission';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Card short name already exists!";
				}
			}else {
				return "Card short name already exists!";
			}
		}
	}
}
function loadaccountname(datastatus){ // load account name
	$('#accountid').empty();	
    $('#accountid').append($("<option></option>"));
	var tablename = 'cardcommission';
	$.ajax({
        url: base_url + "Cardcommission/loadaccountname",
		data:{datastatus:datastatus,table:tablename},
		method: "POST",
        async:false,
		dataType:'json',
		success: function(msg) 
        { 
			for(var i = 0; i < msg.length; i++) {
				 $('#accountid')
				.append($("<option></option>")
				.attr('data-accountidhidden',msg[i]['accountname'])
				.attr("value",msg[i]['accountid'])
				.text(msg[i]['accountname']));
			} 
        },
    });
}
// validate for the field commission and surcharge field
function commissionchargevalidate() {
	$('#commission,#surcharge').removeClass('validate[custom[number],min[0.1],max[100],decval[3]] error');
	$('#commission,#surcharge').addClass('validate[custom[number],max[100],decval[3]]');
}