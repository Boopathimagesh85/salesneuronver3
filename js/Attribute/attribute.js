$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		attributeaddgrid();
		firstfieldfocus();
		attributecrudactionenable();
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			attributeaddgrid();
		});
	}
	//hidedisplay
	$('#attributedataupdatesubbtn').hide();
	$('#attributesavebutton').show();
	{
		//validation for Company Add  
		$('#attributesavebutton').click(function(e) { 	
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#attributeformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#attributeformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#attributesavebutton').attr('disabled','disabled'); 
				attributeaddformdata();
			},
			onFailure: function() {				
				alertpopup(validationalert);
			}
		});
		$( window ).resize(function() {
			innergridresizeheightset('attributeaddgrid');
		});
	}
	{
		//update company information
		$('#attributedataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#attributeformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#attributeformeditwizard").validationEngine({
			onSuccess: function()
			{
				attributeupdateformdata();
			},
			onFailure: function()
			{
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}	
	{//filter work
		//for toggle
		$('#attributeviewtoggle').click(function() {
			if ($(".attributefilterslide").is(":visible")) {
				$('div.attributefilterslide').addClass("filterview-moveleft");
			}else{
				$('div.attributefilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#attributeclosefiltertoggle').click(function(){
			$('div.attributefilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#attributefilterddcondvaluedivhid").hide();
		$("#attributefilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#attributefiltercondvalue").focusout(function(){
				var value = $("#attributefiltercondvalue").val();
				$("#attributefinalfiltercondvalue").val(value);
				$("#attributefilterfinalviewconid").val(value);
			});
			$("#attributefilterddcondvalue").change(function(){
				var value = $("#attributefilterddcondvalue").val();
				attributemainfiltervalue=[];
				$('#attributefilterddcondvalue option:selected').each(function(){
				    var $avalue =$(this).attr('data-ddid');
				    attributemainfiltervalue.push($avalue);
					$("#attributefinalfiltercondvalue").val(attributemainfiltervalue);
				});
				$("#attributefilterfinalviewconid").val(value);
			});
		}
		attributefiltername = [];
		$("#attributefilteraddcondsubbtn").click(function() {
			$("#attributefilterformconditionvalidation").validationEngine("validate");	
		});
		$("#attributefilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(attributeaddgrid,'attribute');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#attributesectionoverlay").removeClass("effectbox");
		$("#attributesectionoverlay").addClass("closed");
		$("#attributemandatory").val('No');
	});
}
//Documents Add Grid
function attributeaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#attributeaddgrid").width();
	var wheight = $("#attributeaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#attributesortcolumn").val();
	var sortord = $("#attributesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.attributeheadercolsort').hasClass('datasort') ? $('.attributeheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.attributeheadercolsort').hasClass('datasort') ? $('.attributeheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.attributeheadercolsort').hasClass('datasort') ? $('.attributeheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#attributeviewfieldids').val();
	if(userviewid != '') {
		userviewid= '58';
	}
	var filterid = $("#attributefilterid").val();
	var conditionname = $("#attributeconditionname").val();
	var filtervalue = $("#attributefiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=attribute&primaryid=attributeid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#attributeaddgrid').empty();
			$('#attributeaddgrid').append(data.content);
			$('#attributeaddgridfooter').empty();
			$('#attributeaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('attributeaddgrid');
			{//sorting
				$('.attributeheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.attributeheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#attributepgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.attributeheadercolsort').hasClass('datasort') ? $('.attributeheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.attributeheadercolsort').hasClass('datasort') ? $('.attributeheadercolsort.datasort').data('sortorder') : '';
					$("#attributesortorder").val(sortord);
					$("#attributesortcolumn").val(sortcol);
					var sortpos = $('#attributeaddgrid .gridcontent').scrollLeft();
					attributeaddgrid(page,rowcount);
					$('#attributeaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('attributeheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount =$('.pagerowcount').data('rowcount');
					attributeaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#attributepgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					attributeaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#attributeaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#attributepgrowcount').material_select();
		},
	});
}
function attributecrudactionenable() {
	{//add icon click
		$('#attributeaddicon').click(function(e){
			$("#attributesectionoverlay").removeClass("closed");
			$("#attributesectionoverlay").addClass("effectbox");
			e.preventDefault();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#attributedataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#attributesavebutton').show();
		});
	}
	$("#attributeediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#attributeaddgrid div.gridcontent div.active').attr('id');
		if (datarowid)  {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			attributegetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#attributedeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#attributeaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			clearform('cleardataform');
			$("#attributeprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');				
			combainedmoduledeletealert('attributedeleteyes');
			$("#attributedeleteyes").click(function(){
				var datarowid = $("#attributeprimarydataid").val();
				attributerecorddelete(datarowid);
				$(this).unbind();
			});
		}
		else {
			alertpopup("Please select a row");
		}
	});	
}
{//refresh grid
	function attributerefreshgrid() {
		var page = $('ul#attributepgnum li.active').data('pagenum');
		var rowcount = $('ul#attributepgnumcnt li .page-text .active').data('rowcount');
		attributeaddgrid(page,rowcount);
	}
}
//new data add submit function
function attributeaddformdata() {
    var formdata = $("#attributeaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Attribute/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				$('.singlecheckid').click();
				resetFields();
				attributerefreshgrid();
				$("#attributesavebutton").attr('disabled',false); 
				alertpopup(savealert);
				
            }
        },
    });
}
//old information show in form
function attributegetformdata(datarowid) {
	var attributeelementsname = $('#attributeelementsname').val();
	var attributeelementstable = $('#attributeelementstable').val();
	var attributeelementscolmn = $('#attributeelementscolmn').val();
	var attributeelementpartable = $('#attributeelementspartabname').val();
	$.ajax( {
		url:base_url+"Attribute/fetchformdataeditdetails?attributeprimarydataid="+datarowid+"&attributeelementsname="+attributeelementsname+"&attributeelementstable="+attributeelementstable+"&attributeelementscolmn="+attributeelementscolmn+"&attributeelementpartable="+attributeelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				attributerefreshgrid();
			} else {
				$("#attributesectionoverlay").removeClass("closed");
				$("#attributesectionoverlay").addClass("effectbox");
				var txtboxname = attributeelementsname + ',attributeprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = attributeelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
		}
	});
	$('#attributedataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#attributesavebutton').hide();
}
//udate old information
function attributeupdateformdata() {
	var formdata = $("#attributeaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Attribute/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				$('#attributedataupdatesubbtn').hide();
				$('#attributesavebutton').show();
				$('.singlecheckid').click();
				resetFields();
				attributerefreshgrid();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;	
            }
        },
    });	
}
//udate old information
function attributerecorddelete(datarowid) {
	var attributeelementstable = $('#attributeelementstable').val();
	var attributeelementspartable = $('#attributeelementspartabname').val();
    $.ajax({
        url: base_url + "Attribute/deleteinformationdata?attributeprimarydataid="+datarowid+"&attributeelementstable="+attributeelementstable+"&attributeelementspartable="+attributeelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	attributerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	attributerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//attnamecheck
//Attribute name check
function attnamecheck() {
	var primaryid = $("#attributeprimarydataid").val();
	var accname = $("#attributename").val();
	var elementpartable = $('#attributeelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Attribute name already exists!";
				}
			}else {
				return "Attribute name already exists!";
			}
		} 
	}
}