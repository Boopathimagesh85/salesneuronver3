$(document).ready(function() {	
	$(document).foundation();
	{// Grid Calling
		receivesmscreategrid();
	}
	$("#dataupdatesubbtn").hide();
	//Overlay Side Bar Code
	$(".sidebaricons").click(function()	{
		var subfomval = $(this).data('subform');
		$(".hiddensubform").hide();
		$("#subformspan"+subfomval+"").fadeIn('slow');
	});	
	$('#reloadicon').click(function() {
		refreshgrid();
	});
	maingridresizeheightset('receivesmscreategrid');
	// Maindiv height width change
	maindivwidth();
	//onclose Move home page 
	$('#closesmsoverlayform,#closemailoverlayform').click(function(){
		window.location =base_url+'Home';
	});
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			receivesmscreategrid();
		});
	}
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,receivesmscreategrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
{// Main Grid Function
	function receivesmscreategrid(page,rowcount) {
		var defrecview = $('#mainviewdefaultview').val();
		if(Operation == 1){
			var rowcount = $('#receivesmspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#receivesmspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		}
		var wwidth = $('#receivesmscreategrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=vmnreceivesms&primaryid=vmnreceivesmsid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#receivesmscreategrid').empty();
				$('#receivesmscreategrid').append(data.content);
				$('#receivesmscreategridfooter').empty();
				$('#receivesmscreategridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('receivesmscreategrid');
				{//sorting
					$('.smsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.smsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#smspgsmscreategridnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.smsheadercolsort').hasClass('datasort') ? $('.smsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#receivesmscreategrid .gridcontent').scrollLeft();
						receivesmscreategrid(page,rowcount);
						$('#receivesmscreategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('smsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						receivesmscreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#receivesmspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						receivesmscreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#receivesmscreategrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#receivesmspgrowcount').material_select();
			},
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smspgnum li.active').data('pagenum');
		var rowcount = $('ul#smspgnumcnt li .page-text .active').data('rowcount');
		receivesmscreategrid(page,rowcount);
	}
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}