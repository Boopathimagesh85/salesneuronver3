$(document).ready(function()
{
	$(document).foundation();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);
	{ // froala editor initial set empty
		froalaarray=['personalsettingssignature_editor','html.set',''];
	}
	$(".personalsettingaddform").css("opacity",1);
	//hide buttion
	$("#dataaddsbtn").hide();
	$("#formclearicon").hide();
	$('#emailid').attr('readonly',true);
	$('#userroleid').prop('readonly',true);
	$('#branchid').prop('readonly',true);
	{//detail view fetch
		$('#personalsettingsform :input').attr('disabled',true);
		$(".alertbtnno").attr('disabled',false);
		setTimeout(function() {
			getformdata();
			Materialize.updateTextFields();
			$("#printtempeditocn").removeAttr('disabled',false);
		},1000);
	}
	$("#newediticon").removeClass('hidedisplay');
	$(".advanceddropbox").addClass('hidedisplay');	
	$("#groupcloseaddform").addClass('hidedisplay');
	{	//action create,update,delete
		$("#newediticon").click(function(){
			$('.ftab').trigger('click');
			$(".advanceddropbox").removeClass('hidedisplay');	
			$("#newediticon").addClass('hidedisplay');
			$(".authiconhideclass").addClass('hidedisplay');
			$('#personalsettingsform :input').attr('disabled', false);
			$(".fr-toolbar").removeClass('hidedisplay');
			$('.fr-element').attr('contenteditable','true');
			$("#employeeimagedivhid").removeClass('hidedisplay');
			//for touch
			masterfortouch =1;
			//For Keyboard Shortcut Variables
			addformupdate = 1;
			viewgridview = 0;
			addformview = 1;
		});
		//close add form
		$("#closeaddform").click(function() {
			 $("#personalsettingsoverlay").show();
		});
		$("#personalsettingscloseyes").click(function(){
			$(".personalicon").addClass('hidedisplay');	
			$(".editiconhideclass").removeClass('hidedisplay');
			$(".authiconhideclass").removeClass('hidedisplay');
			$('#personalsettingsform :input').attr('disabled', true);
			getformdata();
			$("#personalsettingsoverlay").hide();
			//for touch
			masterfortouch =0;
			//For Keyboard Shortcut Variables
			viewgridview = 1;
		});
		$("#userpasswordov").click(function(){
			$("#passwordoverlay").show();
			var empid = $("#primarydataid").val();
			$("#empid").val(empid);
			$("#oldpassword").val('');
			//for touch
			masterfortouch =2;
		});
		$("#passwordchangeclose").click(function() {
			$("#passwordoverlay").hide();
			clearform('clearpassworddataform');
		});
		//update personal settings information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		$("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','branchid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
		//update user personal settings information
		$('#passwordeditsbtbtn').click(function(e) {
			$("#userpasswordvalidation").validationEngine('validate');
		});
		$("#userpasswordvalidation").validationEngine({
			onSuccess: function() {
				var newpass = $("#newpassword").val();
				var conpass = $("#confirmnewpassword").val();
				if(newpass == conpass) {
					paasssowrdsubmit();
				} else {
					alertpopup('New Password and Confirm Password Should Be Same');
				}
			},
			onFailure: function() {
				var dropdownid =['1','branchid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//verify the mail sent with mandrill account or not -- if yes hide the outgoing server fields.. if no don't...
		subaccountdetailsfetch();
	}
	{//mail provider drop down change
		$("#mailproviderid").change(function() {
			var mailproviderid = $("#mailproviderid").val();
			mailsettingdetaisfetch(mailproviderid);
		});
	}
	//signature count fetch
	$("#smssignature").keydown(function(e) {
		var message = $("#smssignature").val();
		var lengthofchar = message.length;
		if(lengthofchar != 0) {
			smssignaturecount(message,lengthofchar);	
		}
	});
	setTimeout(function(){
		$(".froala-element").css('pointer-events','none');
		$(".froala-editor").css('pointer-events','none');
		$('.froala-wrapper .froala-element').attr('contenteditable',false);
	},2000);
	{ //address type change
		$("#primaryaddresstype option[value='7']").remove();
		$("#secondaryaddresstype option[value='8']").remove();
		$("#primaryaddresstype,#secondaryaddresstype").change(function(){
			var id=$(this).attr('id');
			setaddress(id);
		});
	}	
	$("#employeeimagedivhid").addClass('hidedisplay');
});
//old information show in form
function getformdata() {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
		var noofemployee = $('#employeeid').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+"Personalsettings/fetchformdataeditdetails?elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$(".personalicon").addClass('hidedisplay');	
				$(".editiconhideclass").removeClass('hidedisplay');
				$(".authiconhideclass").removeClass('hidedisplay');
				$('#personalsettingsform :input').attr('disabled', true);
				alertpopup('Permission denied');
			} else {
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
				var datarowid = data['primarydataid'];
				primaryaddvalfetch(datarowid);
				var filename = data['personalsettingssignature_editorfilename'];
				setTimeout(function(){
					if(filename !== '' || filename !== null) {
						filenamevaluefetch(filename);
					}
					if(noofemployee == '1')
					{
						$('#employeenumber:input').attr("readonly",false);
					}else{
						$('#employeenumber:input').attr("readonly",true);
					}
				},200);
				$(".fr-toolbar").addClass('hidedisplay');
				$(".fr-element").attr("contenteditable", 'false');
				//mail Settings details Fetch
				mailsettingsdetialfetch(datarowid);
			}
		}
	});
}
//mail settings details fetch
function mailsettingsdetialfetch(datarowid) {
	$.ajax({
		url:base_url+"Personalsettings/mailsettingsdetialfetch?dataprimaryid="+datarowid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var datanames = ['9','username','password','incmeserver','incmeport','outcomeserver','outcomeport','incomessl','outgngssl','mailproid'];
			var textboxname = ['9','emailusername','emailpassword','incomingserver','incomingport','outgoingserver','outgoingport','incomingssl','outgoingssl','mailproviderid'];
			var dropdowns = ['3','incomingssl','outgoingssl','mailproviderid'];
			textboxsetvalue(textboxname,datanames,data,dropdowns); 
			setTimeout(function(){
				var mailproid = data['mailproid'];
			},100);
			Materialize.updateTextFields();
		},
	});
}
//primary address value fetch function 
function primaryaddvalfetch(datarowid) {
	$.ajax({
		url:base_url+"Personalsettings/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=Billing", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var datanames = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
			var textboxname = ['12','primaryaddresstype','primaryaddress','primarypincode','primarycity','primarystate','primarycountry','secondaryaddresstype','secondaryaddress','secondarypincode','secondarycity','secondarystate','secondarycountry'];
			var dropdowns = ['2','primaryaddresstype','secondaryaddresstype'];
			textboxsetvalue(textboxname,datanames,data,dropdowns); 
			setTimeout(function(){
				var primaryaddresstype = data['primaryaddresstype'];
				var secondaryaddresstype = data['secondaryaddresstype'];
			},100);
			Materialize.updateTextFields();
		}
	});
}
//editor value fetch
function filenamevaluefetch(filename) {
	$.ajax({
		url: base_url+"Personalsettings/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data)   {
			 froalaedit(data,'personalsettingssignature_editor');
		},
	});
}
//update information
function updateformdata() {
	var amp = '&';
	//editor data
	var editordata = '';
	var neweditordata = '';
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var inssl = $("#incomingssl").find('option:selected').data('incomingsslhidden');
	var outssl = $.trim($("#outgoingssl").find('option:selected').data('outgoingsslhidden'));
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
        url: base_url + "Personalsettings/datainformationupdate",
        data: "datas=" + datainformation+"&inssl="+inssl+"&outssl="+outssl,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				froalaset(froalaarray);
				$('.froala-wrapper .froala-element').attr('contenteditable',false);
				$(".froala-editor").css('pointer-events','none');
				$(".ftab").trigger('click');
				$('#employeeform').hide();
				$('#employeegriddisplay').fadeIn(1000);
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				window.location=base_url+'Personalsettings';
			}
        },
    });
}
//password submit
function paasssowrdsubmit() {
	var formdata = $("#userpasswordform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	 $.ajax({
        url: base_url + "Personalsettings/passwordupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				clearform('clearpassworddataform');
				$("#passwordoverlay").fadeOut();
				alertpopup('Password Updated Successfully');
				//for touch
				masterfortouch = 0;
            }
        },
    });
}
//old password check
function oldpasswordcheck() {
	var oldpassword = $("#oldpassword").val();
	nmsg = 2;
	if(oldpassword != "") {
		$.ajax({
			url:base_url+"Personalsettings/oldpasswordcheck?oldpassword="+oldpassword,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg == "Fail") {
			return "*You entered the wrong password";
		}
	}
}
function newoldpasswordcheck()
{
	var oldpassword = $("#oldpassword").val();
	var newpassword = $("#newpassword").val();
	if(oldpassword==newpassword)
	{
		return "*New password should not match with current password";
	}
}
//check the mail add ons added or not 
function subaccountdetailsfetch() {
	$.ajax({
		url:base_url+"Personalsettings/subaccountdetailsfetch",
		type:"post",
		async:false,
		cache:false,
		success :function(msg) {
			nmsg = $.trim(msg);
			if(nmsg == 'TRUE') {
				$("#outgoingportdivhid,#outgoingserverdivhid,#outgoingssldivhid").hide();
			} else {
				$("#outgoingportdivhid,#outgoingserverdivhid,#outgoingssldivhid").show();
			}
		},
	});
}
//mail provider details fetch
function mailsettingdetaisfetch(mailproviderid) {
	$.ajax({
		url:base_url+"Personalsettings/mailsettingdatafetch",
		data:"providerid="+mailproviderid,
		type:"post",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			var datanames = ['6','incmeserver','incmeport','outcomeserver','outcomeport','incomessl','outgngssl'];
			var textboxname = ['6','incomingserver','incomingport','outgoingserver','outgoingport','incomingssl','outgoingssl'];
			var dropdowns = ['2','incomingssl','outgoingssl'];
			textboxsetvalue(textboxname,datanames,data,dropdowns);
			Materialize.updateTextFields();
		},
	});
}
//signature count
function smssignaturecount(text,lengthofchar) {
	$("#smssignaturedivhid").find('span').remove();
	$("#smssignaturedivhid").append('<span style="color:#546E7A;font-size:0.7rem; padding-left:30px;">'+lengthofchar+' characters(s)<span style="padding-left:30px;"> Max '+250+' Chars</span></span>'); 
}
//set address
function setaddress(id) {
	var value=$('#'+id+'').val();
	if(id == 'primaryaddresstype') {
		var type='primary';
	} else {
		var type='secondary';
	}
	//swap the billing to shipping
	if(value == 8) {
		$('#primaryaddress').val($('#secondaryaddress').val());
		$('#primarypincode').val($('#secondarypincode').val());
		$('#primarycity').val($('#secondarycity').val());
		$('#primarystate').val($('#secondarystate').val());
		$('#primarycountry').val($('#secondarycountry').val());	
	} else if(value == 7) {//swap the shipping to billing
		$('#secondaryaddress').val($('#primaryaddress').val());
		$('#secondaryarea').val($('#primaryarea').val());
		$('#secondarypincode').val($('#primarypincode').val());
		$('#secondarycity').val($('#primarycity').val());
		$('#secondarystate').val($('#primarystate').text());
		$('#secondarycountry').val($('#primarycountry').text());	
	}
	Materialize.updateTextFields();
}
function employeemobilenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#mobilenumber").val();
	var fieldname = 'mobilenumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Mobile number already exists!";
				}
			}else {
				return "Mobile number already exists!";
			}
		} 
	}
}

function employeephonenumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#landlinenumber").val();
	var fieldname = 'landlinenumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Phone number already exists!";
				}
			}else {
				return "Phone number already exists!";
			}
		} 
	}
}
function employeeothernumbercheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#othernumber").val();
	var fieldname = 'othernumber';
	var elementpartable = 'employee';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Other number already exists!";
				}
			}else {
				return "Other number already exists!";
			}
		} 
	}
}