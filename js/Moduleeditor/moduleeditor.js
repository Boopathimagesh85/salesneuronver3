$(document).ready(function(){
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		moduleeditorgrid();	
	}
	//module list reload
	$('#modulereloadicon').click(function(){
		resetFields();
		refreshgrid();
	});
	//icon show hide
	$('#modulereloadicon').removeClass('hidedisplay');
	$('#moduledeleteicon').removeClass('hidedisplay');
	$('#moduleenableicon').removeClass('hidedisplay');
	$('#reloadicon').addClass('hidedisplay');
	$('#exporticon').addClass('hidedisplay');
	$("#modulebuildericon").removeClass('hidedisplay');
	$("#modulecustomizeicon").addClass('hidedisplay');
	//Add new module
	$('#modulebuildericon').click(function(){
		window.location = base_url+'Modulebuilder';
	});
	//customize selected module
	$('#modulecustomizeicon').click(function(){
		var datarowid = $('#moduleeditorgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			$.ajax({
				url: base_url + "Moduleeditor/moduleeditorcheck",
				data: "datas=&mdata=259&cdata="+datarowid,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'True') {
						sessionStorage.setItem("customizeid",datarowid);
						window.location = base_url+'Formeditor';
					} else {
						alertpopup('Customization is not allowed to this selected module. Because selected module is not customizable');
					}
				},
			});
		} else {
			alertpopup("Please Select The row");
		}
	});
	//module list delete
	$("#moduledeleteicon").click(function(){
		var datarowid = $('#moduleeditorgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			var statusid = getgridcolvalue('moduleeditorgrid',datarowid,'status','');
			if(statusid != '2') {
				moduleeditoralertmsg('modulelistdeleteyes','Disable the module?');
				$("#modulelistdeleteyes").click(function(){
					var datarowid = $('#moduleeditorgrid div.gridcontent div.active').attr('id');
					modulelistdatadelete(datarowid);
					$('#modulelistdeleteyes').unbind();
				});
			} else {
				alertpopup("Selected module already inactive!");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	//module list enable
	$("#moduleenableicon").click(function(){
		var datarowid = $('#moduleeditorgrid div.gridcontent div.active').attr('id');
		if(datarowid){
			var statusid = getgridcolvalue('moduleeditorgrid',datarowid,'status','');
			if(statusid != '1') {
				moduleeditoralertmsg('modulelistenableyes','Enable the module?');
				$("#modulelistenableyes").click(function(){
					var datarowid = $('#moduleeditorgrid div.gridcontent div.active').attr('id');
					modulelistdataenable(datarowid);
					$('#modulelistenableyes').unbind();
				});
			} else {
				alertpopup("Selected module already activated!");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	$( window ).resize(function(){
		maingridresizeheightset('moduleeditorgrid');
	});
	$("#moduleeditorfilterddcondvaluedivhid").hide();
	$("#moduleeditorfilterdatecondvaluedivhid").hide();
	{	//field value set
		$("#moduleeditorfiltercondvalue").focusout(function(){
			var value = $("#moduleeditorfiltercondvalue").val();
			$("#moduleeditorfinalfiltercondvalue").val(value);
			$("#moduleeditorfilterfinalviewconid").val(value);
		});
		$("#moduleeditorfilterddcondvalue").change(function(){
			var value = $("#moduleeditorfilterddcondvalue").val();
			moduleeditormainfiltervalue=[];
			$('#moduleeditorfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    moduleeditormainfiltervalue.push($mvalue);
				$("#moduleeditorfinalfiltercondvalue").val(moduleeditormainfiltervalue);
			});
			$("#moduleeditorfilterfinalviewconid").val(value);
		});
	}
	moduleeditorfiltername = [];
	$("#moduleeditorfilteraddcondsubbtn").click(function() {
		$("#moduleeditorfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#moduleeditorfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(moduleeditorgrid,'moduleeditor');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
});
{/* alertpopup */
	function moduleeditoralertmsg(idname,msg) {
		$('#basedeleteoverlayforcommodule').fadeIn();
		$('#alertmsgme').text(msg);
		$('.commodyescls').attr('id',idname);
		$(".commodyescls").focus();
	}
}
//Module Manager list Grid
function moduleeditorgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#moduleeditorgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.moduleeditorheadercolsort').hasClass('datasort') ? $('.moduleeditorheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.moduleeditorheadercolsort').hasClass('datasort') ? $('.moduleeditorheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.moduleeditorheadercolsort').hasClass('datasort') ? $('.moduleeditorheadercolsort.datasort').attr('id') : '0';
	var filterid = $("#moduleeditorfilterid").val();
	var conditionname = $("#moduleeditorconditionname").val();
	var filtervalue = $("#moduleeditorfiltervalue").val();
	$.ajax({
		url:base_url+"Moduleeditor/modulelistgriddatafetch?maintabinfo=module&primaryid=moduleid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=257'+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#moduleeditorgrid').empty();
			$('#moduleeditorgrid').append(data.content);
			$('#moduleeditorgridfooter').empty();
			$('#moduleeditorgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('moduleeditorgrid');
			var hideprodgridcol = ['status'];
			gridfieldhide('moduleeditorgrid',hideprodgridcol);
			{//sorting
				$('.moduleeditorheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.moduleeditorheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#moduleeditorpgnum li.active').data('pagenum');
					var rowcount = $('ul#moduleeditorpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.moduleeditorheadercolsort').hasClass('datasort') ? $('.moduleeditorheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.moduleeditorheadercolsort').hasClass('datasort') ? $('.moduleeditorheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#moduleeditorgrid .gridcontent').scrollLeft();
					moduleeditorgrid(page,rowcount);
					$('#moduleeditorgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('moduleeditorheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					moduleeditorgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#moduleeditorpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					moduleeditorgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#moduleeditorpgrowcount').material_select();
			maingridresizeheightset('moduleeditorgrid');
		},
	});
}
//Refresh module manager grid
function refreshgrid() {
	var page = $('ul#moduleeditorpgnum li.active').data('pagenum');
	var rowcount = $('ul#moduleeditorpgnumcnt li .page-text .active').data('rowcount');
	moduleeditorgrid(page,rowcount);
}
//Diable module list
function modulelistdatadelete(datarowid) {
	$.ajax({
		url: base_url+"Moduleeditor/deletemodulelistdata?primaryid="+datarowid,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("Disabled Successfully");
			} else { }
		},
	});
}
//Enable module list
function modulelistdataenable(datarowid) {
	$.ajax({
		url: base_url+"Moduleeditor/enablemodulelistdata?primaryid="+datarowid,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup("Enabled Successfully");
			} else { }
		},
	});
}