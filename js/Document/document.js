$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}
	// Maindiv height width change
	maindivwidth();
	//Grid Calling
	documentsaddgrid();
	//crud action
	crudactionenable();
	//default value set
	$("#documentsfolingrideditspan1").show();
	{//grid width fix
		$("#tab1").click(function(){
			documentsfoladdgrid1();
		});
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{//inner-form-with-grid
		$("#documentsfolingridadd1").click(function(){
			sectionpanelheight('documentsfoloverlay');
			$("#documentsfoloverlay").removeClass("closed");
			$("#documentsfoloverlay").addClass("effectbox");
			$('#setdefaultcboxid,#publiccboxid').prop('checked', false);
			$("#setdefault,#public").val('No');
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$(".addsectionclose").click(function(){
			$("#documentsfoloverlay").removeClass("effectbox");
			$("#documentsfoloverlay").addClass("closed");
		});
	}
	var elementname = $('#elementsname').val();
	elementdefvalueset(elementname);
	$("#documentscommonid").removeClass('validate[required]');
	{//Close Add Screen
		var addcloseoppocreation =["closeaddform","documentsview","documentsaddform"]
		addclose(addcloseoppocreation);
		var addcloseviewcreation =["viewcloseformiconid","documentsview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);		
	}
	{//Form Clear
		$("#formclearicon").click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			folderrefreshgrid();
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			$("#documentscommonid").removeClass('validate[required]');
			//file name
			fname_333 = [];
			fsize_333 = [];
			ftype_333 = [];
			fpath_333 = [];
			$('#filenameattachdisplay').empty();
		});
	}
	//folder name grid reload
	$('#tab2').click(function(){
		folderdatareload();
		documentsaddgrid();
	});
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#documentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Redirected form Home		
		add_fromredirect("documentaddsrc",207);
	}
	{//file delete
		$('#deleteleaddocsyes').click(function(){
			var gsr = $('#documentsaddgrid').jqGrid('getGridParam','selrow');
			var id= $('#documentsaddgrid').jqGrid('getCell',gsr,'LeadFileInfoId');
			$.ajax( {
				url:base_url+"Document/documentfiledel?lcfileid="+id, 
				async:false,
				cache:false,
				success: function(data) {                
					if(data == 'Success') {  
						refreshgrid();
						folderrefreshgrid()
						toolbarenable('leaddocstoolbar');
						$("#leaddocsdelete").removeAttr('checked');
						$("#deleteoverlayleaddocs").fadeOut();
					} else {
						toolbarenable('leaddocstoolbar');
						$("#leaddocsdelete").removeAttr('checked');
						$("#deleteoverlayleaddocs").fadeOut();
						alertpopup("Can't delete row");
					}
				}
			});
		});
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			documentsaddgrid();
		});
	}
	{// Function For Check box
		$('#forgdrive').click(function(){
			if ($(this).is(':checked')) {
				$('#forgdrive').val('Yes');
			}else{
				$('#forgdrive').val('No');
			}	
		});
	}
	{//tool bar click function
	   $("#downloadicon").click(function(){
		   var datarowid = $('#documentsaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				documentsdownloadfun(datarowid);
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function(){
			refreshgrid();
			folderrefreshgrid();
			resetFields();
			$('#filenameattachdisplay').empty();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			filename = [];
			filesize = [];
			filetype = [];
			filepath = [];
		});
		$( window ).resize(function() {
			maingridresizeheightset('documentsaddgrid');
			sectionpanelheight('documentsfoloverlay');
		});
	}
	{//validation for Company Add  
		$('#dataaddsbtn').click(function() {
			$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			if($("#filenameattachdisplay").is(':empty')){
				alertpopup('Please Upload File...');
			} else {
				$("#formaddwizard").validationEngine('validate');
			}
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$('#dataaddsbtn').attr('disabled','disabled');
				$("#processoverlay").show();
				documentnewdataaddfun();
			},
			onFailure: function() {
				$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//module name based field name
	$("#documentsmoduleid").change(function(){
		var moduleid = $("#documentsmoduleid").val();
		$("#documentscommonid").addClass('validate[required]');
		$.ajax({
			url: base_url + "Document/tablenameget?moduleid="+moduleid,
			dataType:'json',
			async :false,
			cache:false,
			success: function(data) 
			{
				var fieldname = data.fieldname;
				var tabname = data.tablename;
				usernamefetch(fieldname,tabname);
			},
		});
	});
	$("#documentscommonid").change(function(){
		var id = $("#documentscommonid").val();
		$("#commonid").val(id);
	});
	{//Folder option
		$("#documentsfoladdbutton").click(function() {
			$('#documentsfoladdgrid1validation').validationEngine('validate');
			Materialize.updateTextFields();
		});
		$('#documentsfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				documentsfolderinsertfun();
				Materialize.updateTextFields();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//documents folder data fetch
		$("#documentsfolingridedit1").click(function() {
			var datarowid = $('#documentsfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				resetFields();
				sectionpanelheight('documentsfoloverlay');
				$("#documentsfoloverlay").removeClass("closed");
				$("#documentsfoloverlay").addClass("effectbox");
				$("#documentsfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#documentsfoladdbutton,#documentsfolingriddel1").hide();
				documentsfolderdatafetch(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
			} else {
				alertpopup('Please select a row');
			}
		});
		//documents update
		$("#documentsfolupdatebutton").click(function() {
			$('#documentsfoleditgrid1validation').validationEngine('validate');
			setTimeout(function(){
				Materialize.updateTextFields();
				firstfieldfocus();
			},100);
		});
		$('#documentsfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				documentsfolderupdate();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//documents update
		$("#documentsfolingriddel1").click(function() {
			var datarowid = $('#documentsfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
		//folder reload
		$("#documentsfolingridreload1").click(function() {
			//resetFields();
			$("#foldernamename").val('');
			$("#description").val('');
			$("#setdefaultcboxid").attr('checked',false);
			$("#publiccboxid").attr('checked',false);
			$("#setdefault").val('No');
			$("#public").val('No');
			folderrefreshgrid();
			$("#documentsfolupdatebutton").hide().addClass('hidedisplayfwg');
			$("#documentsfoladdbutton,#documentsfolingriddel1").show();
			Materialize.updateTextFields();
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,documentsaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			documentsaddgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Documents View Grid
	function documentsaddgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#documentspgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#documentspgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#documentsaddgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.documentsheadercolsort').hasClass('datasort') ? $('.documentsheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.documentsheadercolsort').hasClass('datasort') ? $('.documentsheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.documentsheadercolsort').hasClass('datasort') ? $('.documentsheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=crmfileinfo&primaryid=crmfileinfoid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#documentsaddgrid').empty();
				$('#documentsaddgrid').append(data.content);
				$('#documentsaddgridfooter').empty();
				$('#documentsaddgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('documentsaddgrid');
				{//sorting
					$('.documentsheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.documentsheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul.pagination-container li .page-text .active').data('rowcount');
						var sortcol = $('.documentsheadercolsort').hasClass('datasort') ? $('.documentsheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.documentsheadercolsort').hasClass('datasort') ? $('.documentsheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#documentsaddgrid .gridcontent').scrollLeft();
						documentsaddgrid(page,rowcount);
						$('#documentsaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('documentsheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						documentsaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});	
					$('#documentspgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						documentsaddgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#documentsaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#documentspgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(){
		documentsfoladdgrid1();
			addslideup('documentsview','documentsaddform');
			resetFields();
			$('#filenameattachdisplay').empty();
			$("#filename").val('');
			$("#filename_fromid").val('');
			$("#filename_size").val('');
			$("#filename_type").val('');
			$("#filename_path").val('');
			$("#fileid").val('');
			//file name
			fname_333 = [];
			fsize_333 = [];
			ftype_333 = [];
			fpath_333 = [];
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formfields").empty();
			$('#tab2').trigger('click');
			froalaset(froalaarray);
			Materialize.updateTextFields();
			//For tablet and mobile Tab section reset
			if(deviceinfo == 'phone'){
				$('#tabgropdropdown').select2('val','2');
			} else {
				$('#tabgropdropdown').select2('val','1');
			}
			firstfieldfocus();
			Operation = 0; //for pagination
		});
		$("#deleteicon").click(function(){
			var datarowid = $('#documentsaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){	
				$.ajax({
					url:base_url+"Document/getuploadid?id="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success:function(data){
						$('#primarydataid').val(datarowid);
						if(data == 4) {
							$("#tohidecheckbox").show();
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						} else {
							$("#tohidecheckbox").hide();
							$("#basedeleteoverlay").fadeIn();
							$("#basedeleteyes").focus();
						}
					}
				});
			}
			else {
				alertpopup("Please select a row");
			}	
		});
		$("#basedeleteyes").click(function(){
			var datarowid = $('#primarydataid').val();
			documentrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul.pagination li.active').data('pagenum');
		var rowcount = $('ul.pagination-container li .page-text .active').data('rowcount');
		documentsaddgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.folderlistpgnumcnt li .page-text .active').data('rowcount');
		documentsfoladdgrid1(page,rowcount);
	}
}
//sms master folder  Grid
function documentsfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#documentsfoladdgrid1').width();
	var wheight = $('#documentsfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Document/documentsfoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=207',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#documentsfoladdgrid1').empty();
			$('#documentsfoladdgrid1').append(data.content);
			$('#documentsfoladdgrid1footer').empty();
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('documentsfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					documentsfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smsmasterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					documentsfoladdgrid1(page,rowcount);
				});	
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					documentsfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#folderlistpgrowcount').material_select();
		},
	});
}
//new data add submit function
function documentnewdataaddfun() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $("#dataaddform").serialize();
	//console.log(formdata);
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Document/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$("#documentsaddgrid").trigger('reloadGrid');
				resetFields();
				$("#keywords").select2('val',' ');
				refreshgrid();
				folderrefreshgrid()
				$('#filenameattachdisplay').empty();
				filename = [];
				filesize = [];
				filetype = [];
				filepath = [];
				$('#documentsaddform').hide();
				$('#documentsview').fadeIn(1000);
				var elementname = $('#elementsname').val();
				elementdefvalueset(elementname);
				$("#documentscommonid").removeClass('validate[required]');
				alertpopup('Documents Inserted Successfully');
				$("#processoverlay").hide();
			 }
        },
    });
}
//update old information
function documentrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Document/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted Succssfully');
            } else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
//documents download function
function documentsdownloadfun(datarowid) {
	$.ajax({
		url:base_url+'Document/docdownvalfetch',
		type:'POST',
		data:'id='+datarowid,
		dataType:'json',
		async:false,
		cache:false,
		success:function(msg) {
			if(msg['failed'] != 'Failure') {
				var mode = msg['mode'];
				var name = msg['name'];
				var size = msg['size'];
				var path = msg['path'];
				var type = msg['type'];
				if(mode == 2) {
					var filepath = base_url+path;
					var filedownpath = base_url+'Document/download';
					var form = $('<form action="'+filedownpath +'" class="hidedisplay" name="filedownload" id="filedownload" method="POST" target="_blank"><input type="hidden" name="path" id="path" value="'+path+'" /><input type="hidden" name="filename" id="filename" value="'+name+'" /><input type="hidden" name="filesize" id="filesize" value="'+size+'" /><input type="hidden" name="filetype" id="filetype" value="'+type+'" /></form>');
					$(form).appendTo('body');
					form.submit();
				} else if(mode == 3){
					var filepath = path;
					window.location.href = filepath.replace("?dl=0","?dl=1");
				}  else {
					var filepath = path;
					window.location.href = filepath;
				}
				
			} else {
				alertpopup('Sorry file not available');
			}
		},
	});	
}
function usernamefetch(fieldname,tabname) {
	var ddname = 'documentscommonid';
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url: base_url + "Document/recordnamefetch?fieldname="+fieldname+"&tabname="+tabname,
		dataType:'json',
		async :false,
		cache:false,
		success: function(data) 
		{
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-fieldname",data[index]['fieldname'])
					.attr("value",data[index]['id'])
					.text(data[index]['name']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
{//folder operation
	function documentsfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Document/documentsfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					folderrefreshgrid();
					cleargriddata('documentsfoladdgrid1');
					$('#dataaddsbtn').attr('disabled',false);
					//section close
					$('#documentsfolcancelbutton').trigger('click');
					alertpopup(savealert);
					folderdatareload();
					folderrefreshgrid();
					Materialize.updateTextFields();
				}
			},
		});
	}
	//Edit data Fetch
	function documentsfolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Document/documentsfolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes'){
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
					var ppublic = data.ppublic;
					if(ppublic == 'Yes'){
						$("#publiccboxid").prop('checked',true);
						$("#public").val('Yes');
					} else {
						$("#publiccboxid").prop('checked',false);
						$("#public").val('No');
					}
				}
			},
		});
	}
	//SMS templates folder add function
	function documentsfolderupdate(){
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Document/documentsfolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					folderrefreshgrid();
					cleargriddata('documentsfoladdgrid1');
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					folderdatareload();
					$("#documentsfoladdbutton,#documentsfolingriddel1").show();
					$("#documentsfolupdatebutton").hide();
					//section close
					$('#documentsfolcancelbutton').trigger('click');
					alertpopup(savealert);
				}
			},
		});
	}
	//delete folder 
	function deletefolderoperation(id){
		$.ajax({
			url:base_url+'Document/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#documentsfoladdgrid1').attr('disabled',false);
					
				} else if(nmsg == 'DEFAULT') {
					alertpopup("Can't delete default records.");
					folderrefreshgrid();
					$('#documentsfoladdgrid1').attr('disabled',false);
				}
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#foldernameid').empty();
		$('#foldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Document/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=207',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#foldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes'){
							$('#foldernameid').select2('val',data[index]['datasid']).trigger('change');
						}
					});
				}
			},
		});
	}
}
//Unique Folder Name check - Kumaresan
function foldernamecheck() {
	var primaryid = $("#foldernameeditid").val();
	var foldernamename = $("#foldernamename").val();
	var nmsg = "";
	if( foldernamename != "" ) {
		$.ajax({
			url:base_url+"Document/foldernameunique",
			data:"&foldernamename="+foldernamename,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder Name already exists!";
				}
			} else {
				return "Folder Name already exists!";
			}
		} 
	}
}