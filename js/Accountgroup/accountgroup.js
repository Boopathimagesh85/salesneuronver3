$(document).ready(function() 
{
	{//Foundation Initialization
		$(document).foundation();
	}
	$('#editsave,#groupcloseaddform').hide();
	{// Grid Calling Function
		accountgroupgrid();
	}	
	{
	//form field first focus
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	 $( window ).resize(function() {
		 sectionpanelheight('accountgroupsectionoverlay');
	});
	{// Tool Bar click function
	//Add
		$("#groupaddicon").click(function(){
			resetFields();
			sectionpanelheight('accountgroupsectionoverlay');
			$("#accountgroupsectionoverlay").removeClass("closed");
			$("#accountgroupsectionoverlay").addClass("effectbox");
			$('#addicon').show();
			$('#editsave').hide();
			firstfieldfocus();
			$('#accounttypeid').prop('disabled',false);
			$("#accounttypeid").select2('val',6).trigger('change');
			$("#primaryaccountgroupid").val('');
			Materialize.updateTextFields();
		});
		$('.addsectionclose').click(function(){
			$("#accountgroupsectionoverlay").addClass("closed");
			$("#accountgroupsectionoverlay").removeClass("effectbox");
		});
		//Edit 
		$("#editicon").click(function(){			
			var datarowid = $('#accountgroupgrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				sectionpanelheight('accountgroupsectionoverlay');
				$("#accountgroupsectionoverlay").removeClass("closed");
				$("#accountgroupsectionoverlay").addClass("effectbox");
				firstfieldfocus();
				$('#addicon').hide();
				$('#editsave').show();
				retrive();
				$('#accounttypeid').prop('disabled',true);
				Materialize.updateTextFields();
			}
			else {
				alertpopup("Please Select The row");
			}	
		});
		$("#deleteicon").click(function()
		{
			var datarowid =  $('#accountgroupgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				$.ajax({
					url: base_url + "Base/checkmapping?level="+1+"&datarowid="+datarowid+"&table=account&fieldid=accountgroupid",
					success: function(msg) { 
					if(msg > 0){
					alertpopup("This accountgroup is already in usage. You cannot delete this accountgroup.");						
					}else{
						$("#basedeleteoverlay").fadeIn();
					}
					},
					});			
			}
			else {
				alertpopup("Please Select The row");
			}	
		});
		$("#basedeleteyes").click(function()
		{
			recorddelete();
		});
	}
	{//accountgroup validate
		//accountgroup create_function
		//validation for accountgroup Add  
		$('#addicon').click(function() 
		{
			$("#shortname").mouseout();
			$("#accountgroupform").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#accountgroupform").validationEngine(
		{
			onSuccess: function()
			{
				addform();
				Materialize.updateTextFields();
			},
			onFailure: function()
			{
				var dropdownid =['2','accountid','accounttypeid'];				
				dropdownfailureerror(dropdownid);
			}
		});
		//update company information
		$('#editsave').click(function()
		{
			$("#shortname").mouseout();
			$("#editvalidate").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#editvalidate").validationEngine({
			onSuccess: function()
			{			
				$('#accountgroupname').attr('data-primaryid','');
				updateform();
				Materialize.updateTextFields();
			},
			onFailure: function()
			{
				var dropdownid =['2','accounttypeid','accountid'];
				dropdownfailureerror(dropdownid);
			}	
		});
	}
	//$("#accountid optgroup").addClass("ddhidedisplay");
	$("#accounttypeid").change(function(){
		$("#accountid").select2("val",'');	
		$("#accountid optgroup").addClass("ddhidedisplay");		
		var accountttypegroup = $("#accounttypeid").find('option:selected').data('name');		
		$("#accountid optgroup[label='"+accountttypegroup+"']").removeClass("ddhidedisplay");
	});
	//change event account-type 
	$('#memberaccounttypeid').change(function(){
		var value= $('#memberaccounttypeid').find('option:selected').val();
		if(value == '' || value == 'null'){
			
		}else{
			loadaccountgroup();
		}
		if(checkVariable('memberaccounttypeid') == true){
			accounttypeshowhide();
		}
		cleargriddata('memberaddgrid');
		$('#accountgroupid').select2('val','');
	});
	//account group dropdown change
	$("#accountgroupid").change(function() {
		memberaddgrid();
	});
	// group based user submit
	$("#rolemodeulesbt").click(function() {
		 
		var gridData =getgridallrowids('memberaddgrid');
		var allmemberid = getselectedrowids('memberaddgrid');
		var membergriddata = JSON.stringify(gridData); 
		var groupid = $.trim($("#accountgroupid").val());
		var typeid = $.trim($("#memberaccounttypeid").val());
		var amp = '&';
		var datainfo="membergriddata="+membergriddata+amp+"groupid="+groupid+amp+"allmemberid="+allmemberid; 
		if(groupid != '' && typeid != '') {
			$("#processoverlay").show();
			$.ajax({
				url:base_url+"Accountgroup/accountmembersubmit",
				data:'datainformation='+amp+datainfo,
				type:'POST',
				success :function(msg) {
					if(msg == 'success') {
						setTimeout(function() {
							memberaddgrid();
						},100);
						$("#processoverlay").hide();
						alertpopup('Group Member added successfully');
					}
					else{
						
					}
				}
			});
			
		} else {
			alertpopup('Please select the account group and type');
		}
		});
		$('#tab2').click(function(){
			$(".tabclass").addClass('hidedisplay');
			$("#tab2class").removeClass('hidedisplay');
			$("#memberaccounttypeid,#accountgroupid").select2('val','');
			memberaddgrid();
		});
		$('#tab1').click(function(){
			$(".tabclass").addClass('hidedisplay');
			$("#tab1class").removeClass('hidedisplay');
			$('#accountgroupid').empty();
			//accountgrouprefreshgrid();
			accountgroupgrid() ;
		});
		$('#groupcloseaddform').click(function(){
			window.location =base_url+'Account';
		});
});

{// accountgroup View Grid
	function accountgroupgrid(page,rowcount)  {
		var defrecview = $('#mainviewdefaultview').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
		var wwidth = $('#accountgroupgrid').width();
		var wheight = $('#accountgroupgrid').height();
		/*col sort*/
		var sortcol = $("#accountgroupsortcolumn").val();
		var sortord = $("#accountgroupsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.accountgroupheadercolsort').hasClass('datasort') ? $('.accountgroupheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.accountgroupheadercolsort').hasClass('datasort') ? $('.accountgroupheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.accountgroupheadercolsort').hasClass('datasort') ? $('.accountgroupheadercolsort.datasort').attr('id') : '0';	
		var filterid = '';
		var userviewid = 123;
		var footername = 'accountgroup';
		var viewfieldids = $("#Moduleid").val(); 
		$.ajax({
		url:base_url+"Accountgroup/accountgroupgridinformationfetch?viewid="+userviewid+"&maintabinfo=accountgroup&primaryid=accountgroupid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&footername='+footername+'&moduleid=41',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#accountgroupgrid').empty();
			$('#accountgroupgrid').append(data.content);
			$('#accountgroupgridfooter').empty();
			$('#accountgroupgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('accountgroupgrid');
			{//sorting
				$('.accountgroupheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.accountgroupheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount =  $('.pagerowcount').data('rowcount');
					var sortcol = $('.accountgroupheadercolsort').hasClass('datasort') ? $('.accountgroupheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.accountgroupheadercolsort').hasClass('datasort') ? $('.accountgroupheadercolsort.datasort').data('sortorder') : '';
					$("#accountgroupsortorder").val(sortord);
					$("#accountgroupsortcolumn").val(sortcol);
					accountgroupgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('accountgroupheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount =  $('.pagerowcount').data('rowcount');
					accountgroupgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#accountgrouppgrowcount').change(function(){			
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;//$('#prev').data('pagenum');
					accountgroupgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#accountgrouppgrowcount').material_select();
		},
	});
	}
	{//refresh grid
		function accountgrouprefreshgrid() {
			var page = $('.pvpagnumclass').data('pagenum');
			var rowcount = $('ul#accountgrouppgnumcnt .page-text .active').data('rowcount');
			accountgroupgrid(page,rowcount);
		}
	}
}
// member grid
{
	
	function memberaddgrid(page,rowcount) {
		var groupid = $("#accountgroupid").val(); 
		var typeid = $("#memberaccounttypeid").val(); 
		groupid = ( (groupid == "")? "1" : groupid );
		typeid = ( (typeid == "")? "1" : typeid );
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
		var modulename = 'membergroup';
		var wwidth = $('#memberaddgrid').width();
		var wheight = $('#memberaddgrid').height();
		
		/*col sort*/
		var sortcol = $("#membergroupsortcolumn").val();
		var sortord = $("#membergroupsortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.membergroupheadercolsort').hasClass('datasort') ? $('.membergroupheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.membergroupheadercolsort').hasClass('datasort') ? $('.membergroupheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').attr('id') : '0';
		var cuscondfieldsname='account.accountgroupid,account.accounttypeid';
		var cuscondvalues=groupid+"|"+1+","+typeid;
		var cuscondtablenames="";
		var cusjointableid='';
		var conditionoperator='IN,Equalto';
		
		var filterid = '';
		var userviewid =130;
		var viewfieldids =202; 
		var footername = 'membergroupfooter';
		$.ajax({
		url:base_url+"Accountgroup/gridinformationfetch?viewid="+userviewid+"&maintabinfo=account&primaryid=accountid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+"&cuscondfieldsname="+cuscondfieldsname+"&cuscondvalues="+cuscondvalues+"&cuscondtablenames="+cuscondtablenames+"&cusjointableid="+cusjointableid+"&conditionoperator="+conditionoperator+"&groupid="+groupid+'&footername='+footername,		
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#memberaddgrid').empty();
			$('#memberaddgrid').append(data.content);
			$('#memberaddgridgridfooter').empty();
			$('#memberaddgridgridfooter').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('memberaddgrid');
			{//sorting
				$('.membergroupheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.membergroupheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.membergroupheadercolsort').hasClass('datasort') ? $('.membergroupheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.membergroupheadercolsort').hasClass('datasort') ? $('.membergroupheadercolsort.datasort').data('sortorder') : '';
					$("#membergroupsortorder").val(sortord);
					$("#membergroupsortcolumn").val(sortcol);
					var sortpos = $('#memberaddgrid .gridcontent').scrollLeft();
					memberaddgrid(page,rowcount);
					$('#memberaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('membergroupheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					memberaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#membergroupfooterpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					memberaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#membergroupfooterpgrowcount').material_select();
			/*check box select events*/
			gridcheckboxselectevents('memberaddgrid');
			{/* check box events */
				$('#memberaddgrid div .headercheckbox').click(function(){
					if($('#memberaddgrid div .headercheckbox').prop('checked') == true) {
						$('#memberaddgrid div.gridcontent .rowcheckbox').prop('checked',true);
					} else {
						$('#memberaddgrid div.gridcontent .rowcheckbox').prop('checked',false);
					}
					if($('#memberaddgrid div.gridcontent .rowcheckbox').not(':checked').length>0) {
						$('#memberaddgridexpingridsel3').addClass('icon-blank32');
						$('#memberaddgridexpingridsel3').addClass('unchecked');
						$('#memberaddgridexpingridsel3').removeClass('icon-check51');
						$('#memberaddgridexpingridsel3').removeClass('checked');
						$('#allrecordexport').val('No');
					} else {
						$('#memberaddgridexpingridsel3').removeClass('icon-blank32');
						$('#memberaddgridexpingridsel3').removeClass('unchecked');
						$('#memberaddgridexpingridsel3').addClass('icon-check51');
						$('#memberaddgridexpingridsel3').addClass('checked');
						$('#allrecordexport').val('Yes');
					}
				});
				$('#memberaddgrid div.gridcontent .rowcheckbox').click(function(){
					if($('#memberaddgrid div.gridcontent .rowcheckbox').not(':checked').length>0) {
						$('#memberaddgrid div .headercheckbox').prop('checked',false);
						$('#memberaddgridexpingridsel3').addClass('icon-blank32');
						$('#memberaddgridexpingridsel3').addClass('unchecked');
						$('#memberaddgridexpingridsel3').removeClass('icon-check51');
						$('#memberaddgridexpingridsel3').removeClass('checked');
						$('#allrecordexport').val('No');
					} else {
						$('#memberaddgrid div .headercheckbox').prop('checked',true);
						$('#memberaddgridexpingridsel3').removeClass('icon-blank32');
						$('#memberaddgridexpingridsel3').removeClass('unchecked');
						$('#memberaddgridexpingridsel3').addClass('icon-check51');
						$('#memberaddgridexpingridsel3').addClass('checked');
						$('#allrecordexport').val('Yes');
					}
				});
			}
		},
	});
	}
}
{//CRUD Related functions
//Create
function addform()
{
	var formdata = $("#accountgroupform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var accountmembers = $('#accountid').val();
    $.ajax(
    {
        url: base_url +"Accountgroup/create",
        data: "datas=" +datainformation+"&accountmembers="+accountmembers,
		type: "POST",
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            accountgroupgrid() ;
            $(".addsectionclose").trigger("click");
			if (nmsg == 'SUCCESS'){
				alertpopup(savealert);
            }
			else {
				alertpopup(submiterror);
			}          
        },
    });
}
function updateform()
{
	var formdata = $("#accountgroupform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var datarowid= $('#accountgroupgrid div.gridcontent div.active').attr('id');
	var accountmembers = $('#accountid').val();
	var accounttypeid = $('#accounttypeid').find('option:selected').val();
    $.ajax({
        url: base_url + "Accountgroup/update",
        data: "datas=" + datainformation +"&primaryid="+ datarowid +"&accountmembers="+accountmembers+amp+"accounttypeid="+accounttypeid,
		type: "POST",
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			resetFields();
			$('#addicon').show();
			$('#editsave').hide();
			 $(".addsectionclose").trigger("click");
            if (nmsg == 'SUCCESS'){
				 accountgroupgrid() ;
				alertpopup(updatealert);
				Materialize.updateTextFields();
            } else {
				alertpopup(submiterror);
				Materialize.updateTextFields();
			}  
        },
    });
}
//delete accountgroup
function recorddelete()
{	
	var datarowid=$('#accountgroupgrid div.gridcontent div.active').attr('id');
	$.ajax({
        url: base_url + "Accountgroup/delete?primaryid="+datarowid,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			$("#basedeleteoverlay").fadeOut();
			 $(".addsectionclose").trigger("click");
		    if (nmsg == 'SUCCESS') {
				accountgroupgrid() ;
				alertpopup(deletealert);
            } else {
				alertpopup(submiterror);
            }           
        },
    });
}
function retrive()
{
	var elementsname=['7','primaryaccountgroupid','accountgroupprimaryname','accountgroupshortprimaryname','accountgroupname','shortname','description','accounttypeid'];
	var datarowid= $('#accountgroupgrid div.gridcontent div.active').attr('id');
	//form field first focus
	firstfieldfocus();
	$.ajax(
	{
		url:base_url+"Accountgroup/retrive?primaryid="+datarowid, 
		dataType:'json',
		async:false,
		success: function(data)
		{
			var txtboxname = elementsname;
			var dropdowns = ['1','accounttypeid'];
			textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
			Materialize.updateTextFields();
			var accountid = data.accountid;
			var dataarray = accountid.split(",");
			$('#accountid').select2('val',dataarray).trigger('change');
			$('#accountgroupname').attr('data-primaryid',datarowid);
		}		
	});
}
}
// load account group based on account type
function accounttypeshowhide()
{				
	//show/hide account type based
	var accounttypeid = $('#memberaccounttypeid').find('option:selected').val();
	$("#accountgroupid option").addClass("ddhidedisplay");
	$("#accountgroupid optgroup").addClass("ddhidedisplay");
	$("#accountgroupid option").prop('disabled',true);
	if(accounttypeid == "6"){
		$("#accountgroupid option[data-accountype='6']").removeClass("ddhidedisplay");
		$("#accountgroupid option[data-accountype='6']").closest('optgroup').removeClass("ddhidedisplay");
		$("#accountgroupid option[data-accountype='6']").prop('disabled',false);
	} else if(accounttypeid == "16"){
		$("#accountgroupid option[data-accountype='16']").removeClass("ddhidedisplay");
		$("#accountgroupid option[data-accountype='16']").closest('optgroup').removeClass("ddhidedisplay");	
		$("#accountgroupid option[data-accountype='16']").prop('disabled',false);
	}
	$("#accountgroupid option:enabled").closest('optgroup').removeClass("ddhidedisplay");
}
//checks to remove the restricted tool bar
function hidegridcell(rowId, cellValue, rawObject, cm, rdata,cellpoint) {
	if(rdata[cellpoint] == 'empty') {
		return result = ' class="cbox_ronly" style="opacity:0"';
	} else {
		return result = ' class="'+cellpoint+'_cbox '+cellpoint+'_cbox'+rowId+'" data-datacellrowid="'+rowId+'" ';
	}
}
// load accountgroup
function loadaccountgroup() {
	$('#accountgroupid').append($("<option></option>"));
	$('#accountgroupid').empty();
	$('#accountgroupid').append($("<option></option>").attr("value","1").text('Select'));
	
	$.ajax(
			{
				url:base_url+"Accountgroup/loadaccountgroup", 
				dataType:'json',
				async:false,
				cache:false,
				success: function(data)
				{  
					var i=0;
					$.each(data, function(index) {
					 $('#accountgroupid')
						.append($("<option></option>")
						.attr("value",data[index]['accountgroupid'])
						.attr("data-accountype",data[index]['accounttypeid'])
						.text(data[index]['accountgroupname']));
					 i++;
					});
				 }
			});
}
function getselectedrowids(gridname) 
{
	var rowids =  new Array();
	$('#'+gridname+' div.gridcontent .rowcheckbox').map(function(i,e) {
		checkboxid = $(e).attr('id');
		if($('#'+gridname+' div.gridcontent #'+checkboxid).prop('checked') == true) {
			datarowid = $('#'+gridname+' div.gridcontent #'+checkboxid).data('rowid');
			rowids.push(datarowid);
		}
	});
	return rowids;
}
//get the row data of the sected row
function getgridrowsdata(gridname) {
	if(deviceinfo != 'phone'){
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			$(fieldname).each(function(i,e) {
				value='';
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+fieldclass[i]).text();
				value = (value !== 'null') ? value : '';
				datas[''+e+''] = new Array();			
				datas[''+e+''] = value;
			});
			dataarr.push(datas);
			j++;
		});
		return dataarr;
	}else{
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.wrappercontent div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			$(fieldname).each(function(i,e) {
				value='';
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' .'+fieldclass[i]).text();
				value = (value !== 'null') ? value : '';
				datas[''+e+''] = new Array();			
				datas[''+e+''] = value;
			});
			dataarr.push(datas);
			j++;
		});		
		return dataarr;
	}	
}
//Kumaresan - Unique Account Group name
function checkuniquename() {
	var primaryid = $("#primaryaccountgroupid").val();
	var accname = $("#accountgroupname").val();
	var elementpartable = 'accountgroup';
	if(accname !="") {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Account group name already exists!";
				}
			} else {
				return "Account group name already exists!";
			}
		} 
	}
}
//Kumaresan - Unique Account Group Short name
function accountgroupshortnamecheck() {
	var primaryid = $("#primaryaccountgroupid").val();
	var accname = $("#shortname").val();
	var fieldname = 'shortname';
	var elementpartable = 'accountgroup';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniqueshortdynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid+"&fieldname="+fieldname,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Short name already exists!";
				}
			}else {
				return "Short name already exists!";
			}
		} 
	}
}