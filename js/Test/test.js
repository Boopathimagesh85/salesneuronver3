$(document).ready(function(){
	METHOD ='ADD';
	$(document).foundation();
	maindivwidth();
	{
		testaddgrid();
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{
		var addclosetestcreation =['closeaddform','testcreationview','testcreationformadd'];
		addclose(addclosetestcreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			testaddgrid();
		});
	}
	{
		fortabtouch = 0;
	}
	{
		$('#addicon').click(function(){
			addslideup('testcreationview','testcreationformadd');
			resetFields();
			clearformgriddata();
			$('.addbtnclass').removeClass('hidedisplay')
			$('.updatebtnclass').addClass('hidedisplay')
			showhideiconsfun('addclick','testcreationformadd');
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			froalaset(froalaarray);
			Materialize.updateTextFields();
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			viewgridview = 0;
			addformview = 1;
			Operation = 0;
		});
		$('#editicon').click(function(){
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				clearformgriddata();
				addslideup('testcreationview','testcreationformadd');
				$('.updatebtnclass').removeClass('hidedisplay')
				$('.addbtnclass').addClass('hidedisplay')
				froalaset(froalaarray);
				testdataupdateinfofetchfun(datarowid);
				firstfieldfocus();
				showhideiconsfun('editclick','testcreationformadd');
				fortabtouch = 1;
				Operation = 1;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#deleteicon').click(function(){
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#reloadicon').click(function(){
			refreshgrid();
			Materialize.updateTextFields();
		});
		$(' window ').resize(function(){
			maingridresizeheightset(testaddgrid);
		});
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
			froalaset(froalaarray);
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			testrecorddelete(datarowid);
		});
		$('#detailedviewicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$('.addbtnclass').addClass('hidedisplay');
				$('.updatebtnclass').removeClass('hidedisplay');
				$('#formclearicon').hide();
				resetFields();
				froalaset(froalaarray);
				testdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','testcreationformadd');
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#editfromsummayicon').click(function() {
				showhideiconsfun('editfromsummryclick','testcreationformadd');
				firstfieldfocus();
				Materialize.updateTextFields();
		});
		$('#dashboardicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$.getScript(base_url+'js/plugins/uploadfile/jquery.uploadfile.min.js');
				$.getScript(base_url+'js/plugins/zingchart/zingchart.min.js');
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
					$('#dashboardcontainer').removeClass('hidedisplay');
					$('.actionmenucontainer').addClass('hidedisplay');
					$('.fullgridview').addClass('hidedisplay');
					$('.footercontainer').addClass('hidedisplay');
					$('.forgetinggridname').addClass('hidedisplay');
					setTimeout(function() {
					$('#processoverlay').hide();
			},1000);
			} else {
				alertpopup('Please select a row');
			}
		});
		
		$('#printicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$('#templatepdfoverlay').fadeIn();
				var viewfieldids = $('#viewfieldids').val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#cloneicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#processoverlay').show();
				$('.addbtnclass').removeClass('hidedisplay');
				$('.updatebtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				showhideiconsfun('editclick','testcreationformadd');
				$('#formclearicon').hide();
				testclonedatafetchfun(datarowid);
				$('#tabgropdropdown').select2('val','1');
				randomnumbergenerate('anfieldnameinfo','anfieldnameidinfo','anfieldnamemodinfo','anfieldtabinfo');
				Operation = 0;
				Materialize.updateTextFields();
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#smsicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
				$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$('#mobilenumbershow').show();
							$('#smsmoduledivhid').hide();
							$('#smsrecordid').val(datarowid);
							$('#smsmoduleid').val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						}else if(mobilenumber.length == 1) {
							sessionStorage.setItem('mobilenumber',mobilenumber);
							sessionStorage.setItem('viewfieldids',viewfieldids);
							sessionStorage.setItem('recordis',datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup('Invalid mobile number');
						}
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#mailicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid)
			{
				var emailtosend = getvalidemailid(datarowid,'test','emailid','','');
				sessionStorage.setItem('forcomposemailid',emailtosend.emailid);
				sessionStorage.setItem('forsubject',emailtosend.subject);
				sessionStorage.setItem('moduleid','5001');
				sessionStorage.setItem('recordid',datarowid);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#smsmobilenumid').change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data') ) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$('#smsmobilenum').val(selectid);
		});
		$('#mobilenumbersubmit').click(function() {
			var datarowid = $('#smsrecordid').val();
			var viewfieldids =$('#smsmoduleid').val();
			var mobilenumber =$('#smsmobilenum').val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem('mobilenumber',mobilenumber);
				sessionStorage.setItem('viewfieldids',viewfieldids);
				sessionStorage.setItem('recordis',datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}
		});
		$('#clicktocallclose').click(function() {
			$('#clicktocallovrelay').hide();
		});
		$('#mobilenumberoverlayclose').click(function() {
			$('#mobilenumbershow').hide();
		});
		$('#outgoingcallicon').click(function() {
			var datarowid = $('#testaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $('#viewfieldids').val();
					$.ajax({
					url:base_url+'Base/mobilenumberinformationfetch?viewid='+viewfieldids+'&recordid='+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++){
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').hide();
								$('#calcount').val(mobilenumber.length);
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup('Invalid mobile number');
							}
							} else {
								$('#c2cmobileoverlay').show();
								$('#callmoduledivhid').show();
								$('#callrecordid').val(datarowid);
								$('#callmoduleid').val(viewfieldids);
								moduledropdownload(viewfieldids,datarowid,'callmodule');
							}
					},
					});
			} else {
					alertpopup('Please select a row');
			}
		});
		$('#c2cmobileoverlayclose').click(function() {
			$('#c2cmobileoverlay').hide
		});
		$('#callnumbersubmit').click(function() {
			var mobilenum = $('#callmobilenum').val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup('Please Select the mobile number to call');
			} else {
				clicktocallfunction(mobilenum);
			}
		});
		$('#customizeicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('customizeid',viewfieldids);
			window.location = base_url+'Formeditor';
		});
		$('#importicon').click(function(){
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('importmoduleid',viewfieldids);
			window.location = base_url+'Dataimport';
		});
		$('#datamanipulationicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('datamanipulateid',viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		$('#findduplicatesicon').click(function() {
			var viewfieldids = $('#viewfieldids').val();
			sessionStorage.setItem('finddubid',viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{
		$('#dataaddsbtn').click(function(){
			$('.ftab').trigger('click');
			$('#testformaddwizard').validationEngine('validate');
		});
		$('#testformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				testnewdataaddfun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(){
			$('.ftab').trigger('click');
			$('#testformeditwizard').validationEngine('validate');
		});
		$('#testformeditwizard').validationEngine({
			onSuccess: function() {
				$('#dataupdatesubbtn').attr('disabled','disabled');
				testdataupdatefun();
			},
			onFailure: function() {
				$('#tabgropdropdown').select2('val','1');
				alertpopup(validationalert);
			}
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
{
filtername = [];
	$('#filteraddcondsubbtn').click(function() {
		$('#filterformconditionvalidation').validationEngine('validate');
	});
	$('#filterformconditionvalidation').validationEngine({
		onSuccess: function() {
			mainviewfilterwork(testaddgrid);
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
}
});
function gridformtogridvalidatefunction(gridnamevalue){
	$('#'+gridnamevalue+'validation').validationEngine({
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			if(METHOD == 'ADD' || METHOD == ''){ //ie add operation
					formtogriddata(gridname,METHOD,'last','');
			}
			if(METHOD == 'UPDATE'){  //ie edit operation
					var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
					formtogriddata(gridname,METHOD,'',selectedrow);
			}
			datarowselectevt();
			METHOD = 'ADD';
			var hideoverlay = $('#griddataoverlayhide'+i+'').val();
			var hideoverlaydata = hideoverlay.split(',');
			$('#'+hideoverlaydata[1]+'').hide();
			$('#'+hideoverlaydata[0]+'').show();
			clearform('gridformclear');
			$('#'+hideoverlaydata[2]+'').trigger('click');
			griddataid = 0;
			gridynamicname = '';
		},
		onFailure: function() {
		}
	});
}
function testaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#testpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else {
	var rowcount = $('#testpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $(window).width();
	var wheight = $(window).height();
	var sortcol = $('#sortcolumn').val();
	var sortord = $('#sortorder').val();
	if(sortcol){
		sortcol = sortcol;
	} else {
		var sortcol = $('.testsheadercolsort').hasClass('datasort') ? $('.testsheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord){
		sortord = sortord;
	} else {
	var sortord =  $('.testsheadercolsort').hasClass('datasort') ? $('.testsheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.testsheadercolsort').hasClass('datasort') ? $('.testsheadercolsort.datasort').attr('id') : '0';
	var userviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $('#filterid').val();
	var conditionname = $('#conditionname').val();
	var filtervalue = $('#filtervalue').val();
	$.ajax({
		url:base_url+'Base/gridinformationfetch?viewid='+userviewid+'&maintabinfo=test&primaryid=testid&viewfieldids='+viewfieldids+'&page='+page+'&records='+rowcount+'&width='+wwidth+'&height='+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#testaddgrid').empty();
			$('#testaddgrid').append(data.content);
			$('#testaddgridfooter').empty();
			$('#testaddgridfooter').append(data.footer);
			datarowselectevt();
			columnresize('testaddgrid');
			{//sorting
				$('.testsheadercolsort').click(function(){
					$('#processoverlay').show();
					$('.testsheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					testaddgrid(page,rowcount);
					var sortcol = $('.testheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.testheadercolsort').hasClass('datasort') ? $('.accountsheadercolsort.datasort').data('sortorder') : '';
					$('#sortorder').val(sortord);
					$('#sortcolumn').val(sortcol);
					$('#processoverlay').hide();
				});
				sortordertypereset('testsheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$('#processoverlay').show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					testaddgrid(page,rowcount);
					$('#processoverlay').hide();
				});
				$('#testpgrowcount').change(function(){
					Operation = 0;
					$('#processoverlay').show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					testaddgrid(page,rowcount);
					$('#processoverlay').hide();
				});
			}
					$('#testpgrowcount').material_select();
		},
	});
}
function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#testspgnumcnt li .page-text .active').data('rowcount');
		testaddgrid(page,rowcount);
}
{
	function clearformgriddata() {
			var gridname = $('#gridnameinfo').val();
			if(gridname){
			var gridnames = gridname.split(',');
			var datalength = gridnames.length;
			for(var j=0;j<datalength;j++) {
				cleargriddata(gridnames[j]);
			}
		}
	}
}
function testclonedatafetchfun(datarowid) {
		clearformgriddata();
		$('.addbtnclass').removeClass('hidedisplay');
		$('.updatebtnclass').addClass('hidedisplay');
		$('#formclearicon').hide();
		resetFields();
		testdataupdateinfofetchfun(datarowid);
		firstfieldfocus();
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		discardmsg = 1;
}
function testnewdataaddfun() {
	var amp = '&';
	var addgriddata='';
	var gridname = $('#gridnameinfo').val();
	if(gridname != '') {
		var gridnames  = [];
		gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata = ''
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}
		else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat( getgridrowsdata(gridnames[j]) );
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var tandcdata = 0;
	var noofrows = noofrows;
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementspartabname = $('#elementspartabname').val();
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var resctable = $('#resctable').val();
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var formdata = $('#testdataaddform').serialize();
	var datainformation = amp + formdata;
	$.ajax({
		url:base_url+'Test/newdatacreate',
		data: 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows+amp+'elementsname='+elementsname+amp+'elementstable='+elementstable+amp+'elementscolmn='+elementscolmn+amp+'elementspartabname='+elementspartabname+amp+'resctable='+resctable+amp+'griddatapartabnameinfo='+griddatapartabnameinfo+amp+'tandcdata='+tandcdata,
		type:'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#testcreationformadd').hide();
				$('#testcreationview').fadeIn(1000);
				refreshgrid();
				clearformgriddata();
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function testdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Test/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('.ftab').trigger('click');
				resetFields();
				$('#testcreationformadd').hide();
				$('#testcreationview').fadeIn(1000);
				refreshgrid();
				$('#dataupdatesubbtn').attr('disabled',false);
			} else {
				addslideup('testcreationview','testcreationformadd');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				editordatafetch(froalaarray,fdata);
				Materialize.updateTextFields();
				$('#processoverlay').hide();
			}
		}
	});
}
function editordatafetch(froalaarray,data) {
	var filename = [];
	var editorname = froalaarray[0].split(',');
	for(var i=0;i<editorname.length;i++) {
		filename[i] =  fdata[editorname[i]+'filename'];
	}
	if(filename.length > 0) {
		$.ajax({
			url:base_url+'Base/editervaluefetch',
				data:'filename='+filename,
				type:'POST',
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					for(var j=0;j<data.length;j++) {
						$('#'+editorname[j]+'filename').val();
						froalaedit(data[j],editorname[j]);
					}
				},
		});
	}
}
function testgriddetail(pid) {
	if(pid !='') {
	}
}
function testdataupdatefun() {
	var amp = '&';
	var gridname = $('#gridnameinfo').val();
	if(gridname != ''){
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		var noofrows=0;
		var addgriddata='';
		if(deviceinfo != 'phone'){
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;
				}
			}
		}else{
			for(var j=0;j<datalength;j++) {
				if(j!=0) {
					addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
					noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				} else {
					addgriddata = getgridrowsdata(gridnames[j]);
					noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent .data-rows').length;
				}
			}
		}
	}
	var sendformadddata = JSON.stringify(addgriddata);
	var noofrows = noofrows;
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	var resctable = $('#resctable').val();
	var formdata = $('#testdataaddform').serialize();
	var datainformation = amp + formdata;
	var dataset = '';
	if(gridname != ''){
		dataset = 'datas=' + datainformation+amp+'griddatas='+sendformadddata+amp+'numofrows='+noofrows;
	}else{
		dataset = 'datas=' + datainformation;
	}
	$.ajax({
		url: base_url+'Test/datainformationupdate',
		data:dataset,
		type: 'POST',
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				$('.ftab').trigger('click');
				resetFields();
				$('#testcreationformadd').hide();
				$('#testcreationview').fadeIn(1000);
				refreshgrid();
				clearformgriddata();
				$('#dataupdatesubbtn').attr('disabled',false);
				alertpopup(savealert);
			} else if (nmsg == 'false') {
			}
		},
	});
}
function testrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Test/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == 'Denied') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Permission denied');
			}
		}
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewcreatemoduleid').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
