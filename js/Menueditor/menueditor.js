$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{//Grid Settings and calling
		menuaddgrid();		
		firstfieldfocus();
	}
	 $( window ).resize(function() {
		 sectionpanelheight('categorysectionoverlay');
		 sectionpanelheight('menusectionoverlay');
		});
	{//enable support icon
		$(".supportoverlay").css("display","inline-block");
	}
	$("#menutype").trigger('change');
	{//Menu type Change Function
		$("#menutype").change(function(){
			var typevalue = $("#menutype").val();
			if(typevalue == 'External') {
				$("#hyperlinknamediv").show();
				$("#modulenamediv").hide();
				$('#modulelink').addClass('validate[required,custom[csturl]]');
				$('#modulelink').val('http://');
				Materialize.updateTextFields();
			} else if(typevalue == 'Internal') {
				$("#hyperlinknamediv").hide();
				$("#modulenamediv").show();
				$('#modulelink').removeClass('validate[required,custom[csturl]]');
				$('#modulelink').val('');
				Materialize.updateTextFields();
			}
		});
	}
	$("#moduletypeid").change(function(){
		var moduletypeid = $("#moduletypeid").val();
		if(moduletypeid == 1) {
			$("#menutypedivhid").removeClass('hidedisplay');
			$("#modulename").attr('class','chzn-select validate[required]');
			Materialize.updateTextFields();
		} else if(moduletypeid == 2) {
			$("#menutypedivhid").addClass('hidedisplay');
			$("#modulename").attr('class','chzn-select');
			Materialize.updateTextFields();
		}
	});
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	function setmenutype() {
	ddname = 'menutype';
		$("#"+ddname+" option").addClass("ddhidedisplay");
		$("#"+ddname+" option").prop('disabled',true);
		$("#"+ddname+" option[value=External]").removeClass("ddhidedisplay");
		$("#"+ddname+" option[value=External]").prop('disabled',false);
}		   
	{//menu category action events
		$('#categorysavebutton').show();
		$('#groupcloseaddform').hide();
		//menu category reload
		$('#categoryreloadicon').click(function(){
			clearform('cleardataform');
			categoryrefreshgrid();
			$('#menucateacttype').val('0');
			$("#categorydataupdatesubbtn").hide();
			$("#categorysavebutton").show();
		});
		{
			$("#categoryaddicon").click(function(){
				sectionpanelheight('categorysectionoverlay');
				$("#categorysectionoverlay").removeClass("closed");
				$("#categorysectionoverlay").addClass("effectbox");
				resetFields();
				Materialize.updateTextFields();
				firstfieldfocus();
			});
		}
		{
			$("#menuaddicon").click(function(){
				$('#moduletypeid,#menutype,#modulename').select2("readonly",false);
				$('#menucategorylistbutton').prop('disabled',false);
				$("#menusavebutton").show();
				$("#menudataupdatesubbtn").hide();
				//$('#modulelink').attr('readonly', true);
				setTimeout(function(){
				setmenutype();
				},100);
				$('#hyperlinknamediv').hide();
				$('#modulenamediv').hide();
				var readOnlyLength = $('#modulelink').val().length;
				$('#modulelink').text(readOnlyLength);
				$('#modulelink').on('keypress, keydown', function(event) {
				var $field = $(this);
				$('#modulelink').text(event.which + '-' + this.selectionStart);
				if ((event.which != 37 && (event.which != 39)) &&
				((this.selectionStart < readOnlyLength) ||
				((this.selectionStart == readOnlyLength) && (event.which == 8)))) {
				return false;
				}
			});
				sectionpanelheight('menusectionoverlay');
				$("#menusectionoverlay").removeClass("closed");
				$("#menusectionoverlay").addClass("effectbox");
				resetFields();
				Materialize.updateTextFields();
				firstfieldfocus();
			});
		}
		{
			$("#categorycancelbutton").click(function(){
				$("#categorysectionoverlay").addClass("closed");
				$("#categorysectionoverlay").removeClass("effectbox");
				Materialize.updateTextFields();
			});
		}
		{
			$("#menucancelbutton").click(function(){
				$("#menusectionoverlay").addClass("closed");
				$("#menusectionoverlay").removeClass("effectbox");
				Materialize.updateTextFields();
			});
		}
		//menu category edit
		$('#categoryediticon').click(function(){
			var datarowid = $('#menucategoryaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var status = getgridcolvalue('menucategoryaddgrid',datarowid,'status','');
				if(status == 1) {
					resetFields();
					$("#menutype").select2('readonly',false);
					$("#modulename").select2('readonly',false);
					$('.reloadiconclass').hide();
					$('#menucateacttype').val('1');
					menucategetformdata(datarowid);
					sectionpanelheight('categorysectionoverlay');
					$("#categorysectionoverlay").removeClass("closed");
					$("#categorysectionoverlay").addClass("effectbox");
					//For Touch
					masterfortouch = 1;
					//Keyboard shortcut
					saveformview = 1;
					Materialize.updateTextFields();
					firstfieldfocus();
				} else {
					alertpopup("Kindly activate menu category");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//menu category delete
		$("#categorydeleteicon").click(function(){
			var datarowid = $('#menucategoryaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var status = getgridcolvalue('menucategoryaddgrid',datarowid,'status','');
				if(status == 1) {
					disablemoduleeditoralertmsg('menucatedeleteyes','Disable the category?','menucatedeleteyes');
					$("#menucatedeleteyes").click(function(){
						var datarowid = $('#menucategoryaddgrid div.gridcontent div.active').attr('id');
						menucategorydatadelete(datarowid);
						$(this).unbind();
					});
					
				} else {
					alertpopup("Menu category already inactive");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//menu category enable
		$("#categoryenableicon").click(function(){
			var datarowid = $('#menucategoryaddgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				var status = getgridcolvalue('menucategoryaddgrid',datarowid,'status','');
				if(status != 1) {
					moduleeditoralertmsg('menucateviewyes','Enable the category?','menucateviewyes');
					$("#menucateviewyes").click(function(){
						var datarowid = $('#menucategoryaddgrid div.gridcontent div.active').attr('id');
						menucategorydataenable(datarowid);
						$(this).unbind();
					});
				} else {
					alertpopup("Menu category already activated!");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//validation for menu category
		$('#categorysavebutton').on('click',function(e){
			if(e.which==1 || e.which === undefined) {
				$("#categoryformaddwizard").validationEngine('validate');
				//For Touch
				masterfortouch = 0;
			}
		});
		$("#categoryformaddwizard").validationEngine({
			onSuccess: function() {
				menucateaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//validation for menu category edit
		$('#categorydataupdatesubbtn').on('click',function(e){
			if(e.which==1 || e.which === undefined) {
				$("#categoryformeditwizard").validationEngine('validate');
			}
		});
		$("#categoryformeditwizard").validationEngine({
			onSuccess: function() {
				menucateupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{
		//menu list reload
		$('#menureloadicon').click(function(){
			clearform('cleardataform');
			menurefreshgrid()
			$('#menulistacttype').val('0');
			$('#menutype').attr("readonly",false);
			$('#modulename').attr("readonly",false);
			$("#menudataupdatesubbtn").hide();
			$("#menusavebutton").show();
			setTimeout(function(){
				$('#menutype').select2('val','Internal').trigger('change');
			},300);
		});
		//menu list edit
		$('#menuediticon').click(function(){
			var datarowid = $('#menuaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var status = getgridcolvalue('menuaddgrid',datarowid,'status','');
				if(status == 1) {
					resetFields();
					$('#menureloadicon').hide();
					$('#hyperlinknamediv').hide();
					$('#modulenamediv').show();
					menulistgetformdata(datarowid);
					$('#menulistacttype').val('1');
					sectionpanelheight('menusectionoverlay');
					$("#menusectionoverlay").removeClass("closed");
					$("#menusectionoverlay").addClass("effectbox");
					//For Touch
					masterfortouch = 3;
					//Keyboard shortcut
					saveformview = 1;
					Materialize.updateTextFields();
					firstfieldfocus();
				} else {
					alertpopup("Kindly activate menu list!");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//menu list delete
		$("#menudeleteicon").click(function(){
			var datarowid = $('#menuaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = getgridcolvalue('menuaddgrid',datarowid,'status','');
				if(status == 1) {
					disablemoduleeditoralertmsg('menulistdeleteyes','Disable the menu item?','menulistdeleteyes');
					$("#menulistdeleteyes").click(function(){
						var datarowid = $('#menuaddgrid div.gridcontent div.active').attr('id');
						menulistdatadelete(datarowid);
						$(this).unbind();
					});
				} else {
					alertpopup("Menu list already inactive");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#menuenableicon").click(function(){
			var datarowid = $('#menuaddgrid div.gridcontent div.active').attr('id');
			if(datarowid){
				var status = getgridcolvalue('menuaddgrid',datarowid,'status','');
				if(status == 2) {
					moduleeditoralertmsg('menulistenableyes','Enable the menu item?','menulistenableyes');
					$("#menulistenableyes").click(function(){
						var datarowid = $('#menuaddgrid div.gridcontent div.active').attr('id');
						menulistdataenable(datarowid);
						$(this).unbind();
					});
				} else {
					alertpopup("Menu list is in active mode");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//validation for menu list add
		$('#menusavebutton').on('click mousedown',function(){
			var menutype = $('#menutype').val();
			if(menutype=='External') {
				$('#modulename').removeClass('validate[required]');
				//For Touch
				masterfortouch = 2;
			} else {
				$('#modulelink').val('');
				Materialize.updateTextFields();
			}
			$("#menuformaddwizard").validationEngine('validate');
		});
		$("#menuformaddwizard").validationEngine({
			onSuccess: function() {
				var menutype = $('#menutype').val();
				if(menutype=='External') {
					$('#modulename').addClass('validate[required]');
				} else {
					$('#modulelink').val('');
					Materialize.updateTextFields();
				}
				menulistaddformdata();
			},
			onFailure: function() {
				var dropdownid =['2','menucategoryid','modulename'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				var menutype = $('#menutype').val();
				if(menutype=='External') {
					$('#modulename').addClass('validate[required]');
				}
			}
		});
		//validation for menu category edit
		$('#menudataupdatesubbtn').on('click mousedown',function(e){
			if(e.which==1 || e.which === undefined) {
				$("#menuformeditwizard").validationEngine('validate');
			}
		});
		$("#menuformeditwizard").validationEngine({
			onSuccess: function() {
				menulistupdateformdata();
				$('#menureloadicon').show();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		}
	{//parent menu category changes
		$('#dl-parentmenucategoryid').dlmenu({
			animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
		// parent category select
		$('#parentmenucategorylistuldata li').click(function() { 
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid'); 
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt($('#maxcategorylevel').val());
			$('#parentmenucategoryname').val(name);
			$('#parentmenucategoryid').val(listid);
			//reload grid
			 if(level >= maxlevel) {
				alertpopup('Maximum category level reached');
			}
		});
		$('#dl-menucategoryid').dlmenu({
			animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
		// parent category select
		$('#menucategorylistuldata li').click(function() { 
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid'); 
			var level = parseInt($(this).attr('data-level'));
			var maxlevel = parseInt(2);
			$('#modulemenucategoryname').val(name);
			$('#menucategoryid').val(listid);
			//reload grid
			 if(level >= maxlevel) {
				alertpopup('Maximum category level reached');
			}
		});
	}
});
//dynamic alert popup
function moduleeditoralertmsg(idname,msg,enableicon) {
	$('#basedeleteoverlayforcommodule').fadeIn();
	$('.alert-message').text(msg);
	$('.commodyescls').attr('id',idname);
	$(".commodyescls").focus();
	$("#"+enableicon+"").val('Enable');
}
function disablemoduleeditoralertmsg(idname,msg,disableicon){
	$('#basedeleteoverlayforcommodule').fadeIn();
	$('.alert-message').text(msg);
	$('.commodyescls').attr('id',idname);
	$(".commodyescls").focus();
	$("#"+disableicon+"").val('Disable');
}
//Menu Category Add Grid
function menucategoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#menucategoryaddgrid').width();
	var wheight = $('#menucategoryaddgrid').height();
	//col sort
	var sortcol = $("#menucategorysortcolumn").val();
	var sortord = $("#menucategorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.menucategorylistheadercolsort').hasClass('datasort') ? $('.menucategorylistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.menucategorylistheadercolsort').hasClass('datasort') ? $('.menucategorylistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.menucategorylistheadercolsort').hasClass('datasort') ? $('.menucategorylistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Menueditor/newmenucategorygriddatafetch?maintabinfo=menucategory&primaryid=menucategoryid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=243',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#menucategoryaddgrid').empty();
			$('#menucategoryaddgrid').append(data.content);
			$('#menucategoryaddgridfooter').empty();
			$('#menucategoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('menucategoryaddgrid');
			{//sorting
				$('.menucategorylistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.menucategorylistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.menucategorylistheadercolsort').hasClass('datasort') ? $('.menucategorylistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.menucategorylistheadercolsort').hasClass('datasort') ? $('.menucategorylistheadercolsort.datasort').data('sortorder') : '';
					$("#menucategorysortorder").val(sortord);
					$("#menucategorysortcolumn").val(sortcol);
					var sortpos = $('#menucategoryaddgrid .gridcontent').scrollLeft();
					menucategoryaddgrid(page,rowcount);
					$('#menucategoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('menucategorylistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					menucategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#menucategorylistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					menucategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#menucategorylistpgrowcount').material_select();
			}
			var hideprodgridcol = ['status'];
			gridfieldhide('menucategoryaddgrid',hideprodgridcol);
			if(deviceinfo != 'phone') {
				{//Grid data row sorting
					var sortable = document.getElementById("menucategorylist-sort");
					Sortable.create(sortable,{
						group: 'menucategorylistelement',
						onEnd: function (evt) {
							sortorderupdate('menucategoryaddgrid','menucategory','menucategoryid','sortorder');
					    },
					});
				}
			}
		}
	});
}
{//refresh grid
	function categoryrefreshgrid() {
		var page = $('ul#menucategorylistpgnum li.active').data('pagenum');
		var rowcount = $('ul#menucategorylistpgnumcnt li .page-text .active').data('rowcount');
		menucategoryaddgrid(page,rowcount);
	}
	function menurefreshgrid() {
		var page = $('ul#menulistpgnum li.active').data('pagenum');
		var rowcount = $('ul#menulistpgnumcnt li .page-text .active').data('rowcount');
		menuaddgrid(page,rowcount);
	}
}
//Menu Add Grid
function menuaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#menuaddgrid').width();
	var wheight = $('#menuaddgrid').height();
	//col sort
	var sortcol = $("#menusortcolumn").val();
	var sortord = $("#menusortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.menulistheadercolsort').hasClass('datasort') ? $('.menulistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.menulistheadercolsort').hasClass('datasort') ? $('.menulistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.menulistheadercolsort').hasClass('datasort') ? $('.menulistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Menueditor/newmenulistgriddatafetch?maintabinfo=module&primaryid=moduleid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=243',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#menuaddgrid').empty();
			$('#menuaddgrid').append(data.content);
			$('#menuaddgridfooter').empty();
			$('#menuaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('menuaddgrid');
			{//sorting
				$('.menulistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.menulistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.menulistheadercolsort').hasClass('datasort') ? $('.menulistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.menulistheadercolsort').hasClass('datasort') ? $('.menulistheadercolsort.datasort').data('sortorder') : '';
					$("#menusortorder").val(sortord);
					$("#menusortcolumn").val(sortcol);
					menuaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('menulistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					menuaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#menulistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					menuaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				//Material select
				$('#menulistpgrowcount').material_select();
			}
			var hideprodgridcol = ['status'];
			gridfieldhide('menuaddgrid',hideprodgridcol);
			if(deviceinfo != 'phone') {
				{//Grid data row sorting
					var sortable = document.getElementById("menulist-sort");
					Sortable.create(sortable,{
						group: 'menulistelement',
						onEnd: function (evt) {
							sortorderupdate('menuaddgrid','module','moduleid','sortorder');
					    },
					});
				}
			}
		}
	});
}
{//data row sort
	function sortorderupdate(gridname,tablename,tableid,sortfieldname) {
		var datarowid = getgridallrowids(gridname);
		$.ajax({
			url:base_url+"Menueditor/datarowsorting",
			data:"datas="+"&rowids="+datarowid+'&tablename='+tablename+'&primaryid='+tableid+'&sortfield='+sortfieldname,
			type:"POST",
			aync:false,
			cache:false,
			success: function(msg) {
			},
		});
	}
}
{	//menu category add function
	function menucateaddformdata() {
		var formdata = $("#menucatedataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url:base_url + "Menueditor/newmenucategorycreate",
			data:"datas=" + datainformation,
			type:"POST",
			aync:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					categoryrefreshgrid();
					$("#categorysectionoverlay").addClass("closed");
					$("#categorysectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
				}
			},
		});
	}
	//menu category edit form data fetch
	function menucategetformdata(datarowid) {
		var menucategoryelementsname = $('#menucategoryelementsname').val();
		var menucategoryelementstable = $('#menucategoryelementstable').val();
		var menucategoryelementscolmn = $('#menucategoryelementscolmn').val();
		var menucategoryelementspartabname = $('#menucategoryelementspartabname').val();
		$.ajax({
			url:base_url+"Menueditor/fetchmenucateformdatadetails?menucategoryprimarydataid="+datarowid+"&menucategoryelementsname="+menucategoryelementsname+"&menucategoryelementstable="+menucategoryelementstable+"&menucategoryelementscolmn="+menucategoryelementscolmn+"&menucategoryelementspartabname="+menucategoryelementspartabname, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					clearinlinesrchandrgrid('menucategoryaddgrid');
				} else {
					var txtboxname = menucategoryelementsname + ',menucategoryprimarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(','); 
					var dataname = menucategoryelementsname + ',primarydataid';
					var datasname = {};
					datasname = dataname.split(',');
					var dropdowns = [];
					$("#menucategoryname").val(data.menucategoryname);
					$("#menucategorydescp").val(data.menucategorydescp);
					$("#menucategorylevel").val(data.menucategorylevel);
					$("#parentmenucategoryid").val(data.parentmenucategoryid);
					$("#menucategoryprimarydataid").val(datarowid);
					parentaccountnamefetch(datarowid);
					Materialize.updateTextFields();
				}
			}
		});
		$('#categorydataupdatesubbtn').show();
		$('#categorysavebutton').hide();
	}
	{// Parent account name value fetch
		function parentaccountnamefetch(pid) {
			$.ajax({
				url:base_url+"Menueditor/parentaccountnamefetch?id="+pid,
				dataType:'json',
				async:false,
				cache:false,
				success:function (data)	{
					$('#parentmenucategoryid').val(data.parentcategoryid);
					$('#parentmenucategoryname').val(data.categoryname);
				}
			});
		}
	}
	//menu category data update
	function menucateupdateformdata() {
		var formdata = $("#menucatedataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Menueditor/menucategorydataupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#menucateacttype').val('0');
					categoryrefreshgrid();
					$("#categorysectionoverlay").addClass("closed");
					$("#categorysectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
					$('#categorydataupdatesubbtn').hide();
					$('#categorysavebutton').show();
					$('.reloadiconclass').show();
					//For Touch
					masterfortouch = 0;
					//Keyboard shortcut
					saveformview = 0;
				}
			},
		});
	}
	//delete menu category
	function menucategorydatadelete(datarowid) {
		$.ajax({
			url: base_url + "Menueditor/deletemenucategorydata?primaryid="+datarowid,
			async:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					setTimeout(function(){
						categoryrefreshgrid();
						alertpopup("Disabled successfully");
					},400);
				} else if (nmsg == "false")  {
					categoryrefreshgrid();
					alertpopup('Permission denied');
				}
			},
		});
	}
	//enable menu category
	function menucategorydataenable(datarowid) {
		$.ajax({
			url: base_url + "Menueditor/enablemenucategorydata?primaryid="+datarowid,
			async:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					setTimeout(function(){
						categoryrefreshgrid();
						alertpopup("Enabled Successfully");
					},400);
				} else if (nmsg == "false") {
				}
			},
		});
	}
	//menu category name check
	function menucatenamecheck() {
		var fieldvalue = $('#menucategoryname').val();
		var nmsg = "";
		if( fieldvalue !="" ) {
			$.ajax({
				url:base_url+"Menueditor/uniquenamecheck",
				data:"data=&tablename=menucategory&fieldname=menucategoryid&whfield=menucategoryname&fieldvalue="+fieldvalue,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				var acttype = $('#menucateacttype').val(); 
				if(acttype == 1) {
					var menucateid = $('#menucategoryprimarydataid').val();
					if(menucateid != nmsg) {
						return "Category already exists!";
					}
				} else {
					return "Category already exists!";
				}
			}
		}
	}
}
{//menu list action function
	function menulistaddformdata() {
		var formdata = $("#menudataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Menueditor/newmenulistcreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {					
					$("#menusectionoverlay").addClass("closed");
					$("#menusectionoverlay").removeClass("effectbox");
					alertpopup(savealert);
					menurefreshgrid();
					$("#menuaddgrid").trigger('reloadGrid');					
					resetFields();
					setTimeout(function(){
						dynamicloaddropdown('modulename','moduleid','modulename','moduleid','module','','');
						$('#menutype').select2('val','Internal').trigger('change');
					},500);
				}
			},
		});
	}
	//menu list edit form data fetch
	function menulistgetformdata(datarowid) {
		var menulistelementsname = $('#menulistelementsname').val();
		var menulistelementstable = $('#menulistelementstable').val();
		var menulistelementscolmn = $('#menulistelementscolmn').val();
		var menulistelementspartabname = $('#menulistelementspartabname').val();
		$.ajax({
			url:base_url+"Menueditor/fetchmenulistformdatadetails?menulistprimarydataid="+datarowid+"&menulistelementsname="+menulistelementsname+"&menulistelementstable="+menulistelementstable+"&menulistelementscolmn="+menulistelementscolmn+"&menulistelementspartabname="+menulistelementspartabname, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					resetFields();
					clearinlinesrchandrgrid('menuaddgrid');
				} else {
					var txtboxname = menulistelementsname + ',menulistprimarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(','); 
					var dataname = menulistelementsname + ',primarydataid';
					var datasname = {};
					datasname = dataname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,datasname,data,dropdowns);
					menucategorynamefetch(data.menucategoryid);
					$('#moduletypeid').select2('val',data.moduletypeid);
				}
				$('#menutype').trigger('change');
				setTimeout(function(){
					if($('#menutype').val() == 'Internal') {
						$('#modulelink').val('');
						Materialize.updateTextFields();
					} else {
						$('#modulelink').val(data.modulelink);
						Materialize.updateTextFields();
					}
				},200);
				//menu type change
				$('#menutype').change(function(){
					if($('#menutype').val() == 'Internal') {
						$('#modulelink').val('');
						Materialize.updateTextFields();
					} else {
						$('#modulelink').val(data.modulelink);
						Materialize.updateTextFields();
					}
				});
				
			}
		});
		$('#menudataupdatesubbtn').show();
		$('#menusavebutton').hide();
		setTimeout(function(){
			$('#moduletypeid,#menutype,#modulename').select2("readonly",true);
			$('#menucategorylistbutton').prop('disabled',true);
		},200);
	}
	{// Parent account name value fetch
		function menucategorynamefetch(pid) {
			$.ajax({
				url:base_url+"Menueditor/parentmenunamefetch?id="+pid,
				dataType:'json',
				async:false,
				cache:false,
				success:function (data)	{
					$('#menucategoryid').val(data.parentcategoryid);
					$('#modulemenucategoryname').val(data.categoryname);
				}
			});
		}
	}
	//menu list data update
	function menulistupdateformdata() {
		$('#menutype').select2("readonly",false);
		$('#modulename').select2("readonly",false);
		var formdata = $("#menudataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Menueditor/menulistdataupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#menusectionoverlay").addClass("closed");
					$("#menusectionoverlay").removeClass("effectbox");
					resetFields();
					alertpopup(savealert);
					$('#menulistacttype').val('0');
					menurefreshgrid();
					$('#menudataupdatesubbtn').hide();
					$('#menusavebutton').show();
					setTimeout(function(){
						$('#menutype').select2('val','Internal').trigger('change');
					},300);
					//For Touch
					masterfortouch = 2;
					//Keyboard shortcut
					saveformview = 0;
				}
			},
		});
	}
	//delete menu list
	function menulistdatadelete(datarowid) {
		$.ajax({
			url: base_url + "Menueditor/deletemenulistdata?primaryid="+datarowid,
			async:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					setTimeout(function(){ 
						alertpopup("Disabled successfully");
						menurefreshgrid();
					},400);
				} else if (nmsg == "false")  {
					menurefreshgrid();
					alertpopup('Permission denied');
				}
			},
		});
	}
	// menu liat enalbe
	function menulistdataenable(datarowid) {
		$.ajax({
			url: base_url + "Menueditor/enablemenulistdata?primaryid="+datarowid,
			async:false,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#basedeleteoverlayforcommodule").fadeOut();
					setTimeout(function(){ 
						alertpopup("Enabled successfully");
						menurefreshgrid();
					},400);
				} else if (nmsg == "false")  {
					menurefreshgrid();
					alertpopup('Permission denied');
				}
			},
		});
	}
	//menu category name check
	function menunamecheck() {
		var fieldvalue = $('#menuname').val();
		var nmsg = "";
		$.ajax({
			url:base_url+"Menueditor/uniquenamecheck",
			data:"data=&tablename=module&fieldname=moduleid&whfield=menuname&fieldvalue="+fieldvalue,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			var acttype = $('#menulistacttype').val(); 
			if(acttype == 1) {
				var menucateid = $('#menulistprimarydataid').val();
				if(menucateid != nmsg) {
					return "Menu name already exists";
				}
			} else {
				return "Menu name already exists";
			}
		}
	}
}