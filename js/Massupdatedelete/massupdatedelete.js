$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	$("#closeaddform").removeClass('hidedisplay');
	{
		//button show hide
		$("#massupdatedeletecriaddbutton").show();
		$("#massupdatedeletecriupdatebutton").hide();
		$("#massupdatedeleteactaddbutton").show();
		$("#massupdatedeleteactupdatebutton").hide();
		$("#massupdatedeleteexpupdatebutton").hide();
		$("#creteriadatevaluedivhid").hide();
		$("#datefieldvaluedivhid").hide();
		//drop down show hide
		$("#criteriaddfieldvaluedivhid").hide();
		$("#ddfieldvaluedivhid").hide();
		//action icon show/hide
		$("#massupdatedeleteexpingridexportspan3").show();
		$("#massupdatedeleteexpingridselspan3").show();
		$("#massupdatedeleteexpingriddelspan3").hide();
		$("#massupdatedeletecricancelbutton").hide();
	}
	{//Grid Calling
		massupdatedeletecriaddgrid1();
		massupdatedeleteactaddgrid2();
		massupdatedeleteexpaddgrid3();
		firstfieldfocus();
	}
	{ //keyboard shortcut reset global variable
		viewgridview = 0;
	}
	{
		$("#closeaddform").click(function(){
			window.location =base_url+'Dataimport';
		});
	}
	{//Touch Work
		$('.datamanipulationtouch').hammer({
			recognizers: [
				         [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, threshold: 50, pointers:2}],
				         ]
						}	
			).on('swipe', function(ev) {
				if(ev.gesture.direction == "8" && ev.gesture.pointers.length == "2") {
					//do nothing
				 }else if(ev.gesture.direction == "16" && ev.gesture.pointers.length == "2") {
					 $("#closeaddform").trigger('click');// Close form
				 }else if(ev.gesture.direction == "2" && ev.gesture.pointers.length == "2"){
					 $(".mdl-card").find('.activetab').prev().trigger('click');//Left swipe
				 }else if(ev.gesture.direction  == "4" && ev.gesture.pointers.length == "2"){
					 $(".mdl-card").find('.activetab').next().trigger('click');//Right swipe
				 }
			});
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			massupdatedeletecriaddgrid1();
		});
	}
	{// tab Group Changes
		$('#tab1 span').removeAttr('style');
		$('#tab1').click(function(){
			$("#tab3,#tab2").removeClass('activetab');
			$(this).addClass('active-step activetab');
			$("#subformspan1").show();
			$("#subformspan2,#subformspan3").hide();
			var value = $("#value").val();
			$("#value").val(value);
			Materialize.updateTextFields();
		});
		$('#tab2').click(function(){
			var checktab = $("#moduleid").val();
			if(checktab == '') {
				alertpopup('Please select module name.');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			} else {
				$("#tab1").addClass('editable-step');
				$("#tab1,#tab3").removeClass('activetab');
				$(this).addClass('active-step activetab');
				$("#subformspan2").show();
				$("#subformspan1,#subformspan3").hide();
			}
			var value = $("#fieldvalue").val();
			$("#fieldvalue").val(value);
			Materialize.updateTextFields();
		});
		$('#tab3').click(function(){
		var checktab = $("#moduleid").val();
			if(checktab == '') {
				alertpopup('Please select module name.');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
			} else {
				$("#tab2").addClass('editable-step');
				$("#tab2,#tab1").removeClass('activetab');
				$(this).addClass('active-step activetab');
				$("#subformspan3").show();
				$("#subformspan2,#subformspan1").hide();
			}
		});
	}
	var viewid ="";
	var maintabinfo = '';
	var	parentid = '';
	{
		$("#massupdatedeletecriingridadd1").click(function(){
			$("#massupdatedeletecrioverlay").removeClass("closed");
			$("#massupdatedeletecrioverlay").addClass("effectbox");
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	{
		$("#massupdatedeletecricancelbutton").click(function(){
			$("#massupdatedeletecrioverlay").addClass("closed");
			$("#massupdatedeletecrioverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{
		$("#massupdatedeleteactingridadd2").click(function(){
			$("#massupdatedeleteactoverlay").removeClass("closed");
			$("#massupdatedeleteactoverlay").addClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{
		$("#massupdatedeleteactcancelbutton").click(function(){
			$("#massupdatedeleteactoverlay").addClass("closed");
			$("#massupdatedeleteactoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{
		$("#massupdatedeleteexpingridadd3").click(function(){
			$("#massupdatedeleteexpoverlay").removeClass("closed");
			$("#massupdatedeleteexpoverlay").addClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{
		$("#massupdatedeleteexpcancelbutton").click(function(){
			$("#massupdatedeleteexpoverlay").addClass("closed");
			$("#massupdatedeleteexpoverlay").removeClass("effectbox");
			Materialize.updateTextFields();
		});
	}
	{//dynamic form to grid
		$('#massupdatedeletecriaddbutton').click(function(){
			var modid = $("#moduleid").val();
			var ffield = $("#formfields").val();
			var condid = $("#massconditionid").val();
			var value = $("#value").val();
			var i = $(this).data('frmtogriddataid');
			var gridname = $(this).data('frmtogridname');
			gridformtogridvalidatefunction(gridname);
			griddataid = i;
			gridynamicname = gridname;
			if(gridname!="massupdatedeleteactaddgrid2") {
				andorcondvalidationreset();
				$("#"+gridname+"validation").validationEngine('validate');
			}
			setTimeout(function() { massupdatedeleteactaddgrid2(); },100);
			setTimeout(function() { massupdatedeleteexpaddgrid3(); },100);
		});
	}
	var checktab = $("#moduleid").val();
	if(checktab == '') {
		$('.sidebaricons').unbind();
	} else {
		$('.sidebaricons').bind();
	}
	//module drop down change function
	$("#moduleid").change(function() {
		var id = $(this).val();
		$('.condmandclass').text('');
		$("#massandorcondiddivhid").hide();
		$('#massandorcondid').removeClass('validate[required]');
		//field clear function
		moduleddchangefieldclear();
		viewcreationfieldget(id);
		viewcreationeditorfoeldget(id);
		dataexportfieldnameset(id);
		setTimeout(function() {
			viewdropdownload(id);
			massupdatedeleteactaddgrid2();
			massupdatedeleteexpaddgrid3();
			$("#massconditionid").select2('val','');
			$("#massandorcondid").select2('val','');
			$("#value").val('');
			$("#jointable").val('');
			$("#indexname").val('');
			$("#fieldname,#ddfieldvalue").select2('val','');
			$("#ddfieldvalue").empty();
			$("#fieldvalue").val('');
		},100);
	});
	$("#massupdatedeletecriingriddel1").click(function() {
		var datarowid = $('#massupdatedeletecriaddgrid1 div.gridcontent div.active').attr('id');
		if(datarowid) {
			deletegriddatarow('massupdatedeletecriaddgrid1',datarowid);
			$('#massupdatedeletecriaddgrid1 .gridcontent div.wrappercontent div ul:first li.massandorcondidname-class').text('');
			var condcnt = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
			if(condcnt==0) {
				$("#massandorcondiddivhid").hide();
				$('#massandorcondid').removeClass('validate[required]');
				$('.condmandclass').text('');
			}
			massupdatedeleteactaddgrid2();
		} else {
			alertpopup("Please select a row");
		}
	});
	//mass delete 
	$("#massupdatedeleteactingriddel2").click(function() {
		var ids = getselectedrowids('massupdatedeleteactaddgrid2');
		if(ids.length>0) {
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#basedeleteyes").click(function() {
		var ids = getselectedrowids('massupdatedeleteactaddgrid2');
		var parenttbl = $("#parenttable").val();
		if(ids.length>0 && parenttbl != "") {
			massupdatedelte(parenttbl,ids);
		} else {
			alertpopup('Please Select The Record(s) For Delete');
		}
	});
	//mass update
	$("#massupdatedeleteactaddbutton").click(function(){
		$("#massupdatedeleteactaddgrid2validation").validationEngine('validate');
	});
	$("#massupdatedeleteactaddgrid2validation").validationEngine({
		onSuccess: function() {
			var ids = getselectedrowids('massupdatedeleteactaddgrid2');
			if(ids.length>0) {
				var parenttable = $("#parenttable").val();
				var ddvalue = $("#fieldname").val();
				var fieldname = $("#fieldname").find('option:selected').data('modfiledcolumn');
				var uitype = $("#fieldname").find('option:selected').data('uitype');
				var fieldvalue = $("#finalfieldvalue").val();
				massupdateprocss(parenttable,ddvalue,fieldname,fieldvalue,ids,uitype);
			} else {
				alertpopup('Please Select The Record(s) For Update');
			}
		},
		onFailure: function() {
			var dropdownid =['1','fieldname'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//field name change
	$("#fieldname").change(function() {
		var fieldlable = $("#fieldname").val();
		var fieldid = $("#fieldname").find('option:selected').data('formfieldsidhidden');
		var parenttable = $("#fieldname").find('option:selected').data('parenttable');
		$("#parenttable").val(parenttable);
		var data = $("#fieldname").find('option:selected').data('uitype');
		if(data == '2' || data == '3' || data == '4'|| data == '5' || data == '6'|| data == '7'|| data == '10' || data == '11' || data == '12'|| data == '14') {
			showhideintextbox('ddfieldvalue','fieldvalue');
			$('#fieldvalue').val('');
			$("#datefieldvaluedivhid").hide();
			$("#datefieldvalue").removeClass('validate[required]');
			$("#fieldvalue").prop("disabled", false);
			$("#ddfieldvalue").removeClass('validate[required]');
			$('#fieldvalue').timepicker('remove');
			$("#fieldvalue").addClass('validate[required,maxSize[100]]');
		} else if(data == '17' || data == '28' ) {
			showhideintextbox('fieldvalue','ddfieldvalue');
			$("#datefieldvaluedivhid").hide();
			$("#datefieldvalue").removeClass('validate[required]');
			$("#ddfieldvalue").addClass('validate[required]');
			$("#fieldvalue").removeClass('validate[required,maxSize[100]]');
			var fieldname = $("#fieldname").find('option:selected').data('modfiledcolumn');
			fieldmassnamebesdpicklistddvalue(fieldname);
		} else if(data == '18' || data == '19'|| data == '20' || data == '21'|| data == '25' || data == '26' || data == '27'  || data == '29'  || data == '32') {
			showhideintextbox('fieldvalue','ddfieldvalue');
			$("#ddfieldvalue").addClass('validate[required]');
			$("#fieldvalue").removeClass('validate[required,maxSize[100]]');
			$("#datefieldvaluedivhid").hide();
			$("#datefieldvalue").removeClass('validate[required]');
			var fieldname = $("#fieldname").find('option:selected').data('modfiledcolumn');
			fieldmassnamebesdddvalue(fieldname);
		} else if(data == '13') {
			$("#fieldvalue").datepicker("disable");
			$("#ddfieldvalue").addClass('validate[required]');
			$("#fieldvalue").removeClass('validate[required,maxSize[100]]');
			showhideintextbox('fieldvalue','ddfieldvalue');
			newcheckboxvalueget('ddfieldvalue');
			$("#datefieldvaluedivhid").hide();
			$("#datefieldvalue").removeClass('validate[required]');
		} else if(data == '8') {
			$("#datefieldvaluedivhid").show();
			$('#fieldvaluedivhid,#ddfieldvaluedivhid').hide();
			$('#fieldvalue').val('');
			$('#fieldvalue').prop("disabled", false);
			$('#fieldvalue').datetimepicker("destroy");
			$("#ddfieldvalue").removeClass('validate[required]');
			$("#fieldvalue").removeClass('validate[required,maxSize[100]]');
			if(fieldlable == 'Date of Birth'){
				$('#datefieldvalue').datetimepicker('destroy');
				var dateformetdata = $('#datefieldvalue').attr('data-dateformater');
				$('#datefieldvalue').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : '1947:c+100',
					onSelect:function(){
						var checkdate = $(this).val();
						if(checkdate != '') {
							$("#finalfieldvalue").val(checkdate);
						}
					},
					onClose:function(){
						$(this).focus();$(this).focusout();$(this).focus();
					}
				}).click(function(){
					$(this).datetimepicker('show');
				});
			} else {
				$('#datefieldvalue').datetimepicker('destroy');
				var dateformetdata = $('#datefieldvalue').attr('data-dateformater');
				$('#datefieldvalue').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					maxDate: null,
					onSelect:function(){
						var checkdate = $(this).val();
						$("#finalfieldvalue").val(checkdate);
					},
					onClose:function(){
						$('#datefieldvalue').focus();
					}
				}).click(function(){
					$(this).datetimepicker('show');
				});
			}
		} else if(data == '9') {
			$("#fieldvaluedivhid").show();
			$("#ddfieldvaluedivhid,#datefieldvaluedivhid").hide();
			$("#datefieldvaluedivhid").hide();
			$("#datefieldvalue").removeClass('validate[required]');
			$("#ddfieldvalue").removeClass('validate[required]');
			$('#fieldvalue').datetimepicker("destroy");
			$('#fieldvalue').val('');
			$('#fieldvalue').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true,
			});
			var checkdate = $("#fieldvalue").val();
			if(checkdate != '') {
				$("#finalfieldvalue").val(checkdate);
			}
		} else {
			alertpopup('Please choose another field name');
		}
	});
	{
		//field value set
		$("#fieldvalue").focusout(function(){
			var value = $("#fieldvalue").val();
			$("#finalfieldvalue").val(value);
		});
		$("#ddfieldvalue").change(function(){
			var value = $("#ddfieldvalue").val();
			$("#finalfieldvalue").val(value);
		});
	}
    //formfields  hidden value assign
	$("#formfields").change(function() {
		var fieldlable = $("#formfields").find('option:selected').data('label');
		var jointable = $("#formfields").find('option:selected').data('jointable');
		var indexname = $("#formfields").find('option:selected').data('indexname');
		var id = $("#formfields").find('option:selected').data('id');
		var moduleid = $("#moduleid").val();
		$("#jointable").val(jointable);
		$("#indexname").val(indexname);
		$("#fieldid").val(id);
		$("#massconditionid").empty();
		$("#massconditionid").select2('val','');
		var uitypeid = $("#formfields").find('option:selected').data('uitype');
		$.ajax({
			url:base_url+"Base/loadconditionbyuitype?uitypeid="+uitypeid, 
			dataType:'json',
			async:false,
			success: function(cdata) { 
				$.each(cdata, function(index){					
					$('#massconditionid')
					.append($("<option></option>")
					.attr("value",cdata[index]['whereclauseid'])
					.attr("data-massconditionidhidden",cdata[index]['whereclausename'])
					.text(cdata[index]['whereclausename']));
				});	
			}
		});
		if(uitypeid == '2' || uitypeid == '3' || uitypeid == '4'|| uitypeid == '5' || uitypeid == '6' || uitypeid == '7' || uitypeid == '10' || uitypeid == '11' || uitypeid == '12' || uitypeid == '14') {
			showhideintextbox('criteriaddfieldvalue','value');
			$('#value').val('');
			$("#creteriadatevaluedivhid").hide();
			$("#creteriadatevaluedivhid").removeClass('validate[required]');
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$('#value').timepicker('remove');
			$("#value").addClass('validate[required,maxSize[100]]');		
		} else if(uitypeid == '17' || uitypeid == '28') {
			$("#creteriadatevaluedivhid").hide();
			showhideintextbox('value','criteriaddfieldvalue');
			$("#value").removeClass('validate[required]');
			$("#criteriaddfieldvalue").addClass('validate[required,maxSize[100]]');
			$("#value").datepicker("disable");
			fieldviewnamebespicklistdddvalue(indexname);
		}else if(uitypeid == '18' || uitypeid == '19'|| uitypeid == '20' || uitypeid == '21'|| uitypeid == '25' || uitypeid == '26' || uitypeid == '27' || uitypeid == '29' || uitypeid == '32' ) {
			$("#creteriadatevaluedivhid").hide();
			showhideintextbox('value','criteriaddfieldvalue');
			$("#value").removeClass('validate[required]');
			$("#criteriaddfieldvalue").addClass('validate[required,maxSize[100]]');
			$("#value").datepicker("disable");
			fieldviewnamebesdddvalue(indexname);
		} else if(uitypeid == '13') {
			$("#creteriadatevaluedivhid").hide();
			showhideintextbox('value','criteriaddfieldvalue');
			$("#criteriaddfieldvalue").addClass('validate[required,maxSize[100]]');
			$("#value").removeClass('validate[required]');
			$("#value").datepicker("disable");
			newcheckboxvalueget('criteriaddfieldvalue');
		} else if(uitypeid == '8') {
			$("#creteriadatevaluedivhid").show();
			$("#criteriaddfieldvaluedivhid,#valuedivhid").hide();
			$('#value').val('');
			$("#criteriaddfieldvalue,#value").removeClass('validate[required]');
			if(fieldlable == 'Date of Birth'){
				alert();
				$('#creteriadatevalue').datetimepicker('destroy');
				var dateformetdata = $('#creteriadatevalue').attr('data-dateformater');
				$('#creteriadatevalue').datetimepicker({
					dateFormat: 'dd-mm-yy',
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : '1947:c+100',
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != ''){
							$("#critreiavalue").val(checkdate);
						}
					},
					onClose: function () {
						$('#creteriadatevalue').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
			} else {
				$('#creteriadatevalue').datetimepicker('destroy');
				var dateformetdata = $('#creteriadatevalue').attr('data-dateformater');
				$('#creteriadatevalue').datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: 0,
					maxDate:0,
					onSelect: function () {
						var checkdate = $(this).val();
						$("#critreiavalue").val(checkdate);
					},
					onClose: function () {  
						$('#creteriadatevalue').focus();
					}
				}).click(function() {
					$(this).datetimepicker('show');
				});
			}
		} else if(uitypeid == '9') {
			$("#valuedivhid").show();
			$("#criteriaddfieldvaluedivhid,#creteriadatevaluedivhid").hide();
			$('#value').val('');
			$("#criteriaddfieldvalue").removeClass('validate[required]');
			$('#value').timepicker({
				'step':15,
				'timeFormat': 'H:i',
				'scrollDefaultNow': true,
			});
			var checkdate = $("#value").val();
			if(checkdate != ''){
				$("#critreiavalue").val(checkdate);
			}
		} else {
			alertpopup('Please choose another field name');
		}
	});
	{
		//focus out in value
		$("#value").focusout(function(){
			var value = $("#value").val();
			$("#critreiavalue").val(value);
		});
		//dropdown change value
		$("#criteriaddfieldvalue").change(function(){
			var value = $("#criteriaddfieldvalue").val();
			$("#critreiavalue").val(value);
		});
    }
	$("#exportcolumnname").change(function() {
		$("#s2id_exportcolumnname,.select2-results").removeClass('error');
		$("#s2id_exportcolumnname").find('ul').removeClass('error');
		var data = $('#exportcolumnname').select2('data');
		var finalResult = [];
		for( item in $('#exportcolumnname').select2('data') ) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$('#viewcolumnids').val(selectid);
	});
	//excel data export based on columns
	$('#massupdatedeleteexpaddbutton').click(function(){
		$('#massupdatedeleteexpaddgrid3validation').validationEngine('validate');
	});
	$('#massupdatedeleteexpaddgrid3validation').validationEngine({
		onSuccess: function() {
			dataexportasexcel();
		},
		onFailure: function() {
			var dropdownid =[2,'exportcolumnname','exportfiletypeid'];
			dropdownfailureerror(dropdownid);
			$("#s2id_moduleid").find('ul').addClass('error');
			alertpopup(validationalert);
		}
	});
	//All record check box
	$('#massupdatedeleteexpingridsel3').click(function(){
		if($(this).hasClass('unchecked')) {
			$(this).removeClass('icon24-checkbox-blank-outline');
			$(this).removeClass('unchecked');
			$(this).addClass('icon24-checkbox-marked');
			$(this).addClass('checked');
			$('#allrecordexport').val('Yes');
			setTimeout(function(){
				$('#massupdatedeleteexpaddgrid3 div .headercheckbox').prop('checked',true);
				$('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').prop('checked',true);
			},100);
		} else if($(this).hasClass('checked')) {
			$(this).addClass('icon24-checkbox-blank-outline');
			$(this).addClass('unchecked');
			$(this).removeClass('icon24-checkbox-marked');
			$(this).removeClass('checked');
			$('#allrecordexport').val('No');
			setTimeout(function(){
				$('#massupdatedeleteexpaddgrid3 div .headercheckbox').prop('checked',false);
				$('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').prop('checked',false);
			},100);
		}
	});
	{//Mass update/ Mass Delete /Export Conversion From All modules
		var datamoduleid = sessionStorage.getItem("datamanipulateid");
		if(datamoduleid != null) {
			setTimeout(function(){
				$('#moduleid').select2('val',datamoduleid).trigger('change');
				sessionStorage.removeItem("datamanipulateid");
			},100);				
		}
	}
	{// For Grid Width
		$('#tab2').click(function(){
			massupdatedeleteactaddgrid2();
		});
	}
	{// For Grid Width
		$('#tab3').click(function(){
			massupdatedeleteexpaddgrid3();
		});
	}
	//AND/OR condiron hide show 
	var condcnt = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
	if(condcnt==0) {
		$("#massandorcondiddivhid").hide();
	} else{
		$("#massandorcondiddivhid").show();
	}
	//view drop down change
	$("#moduleviewid").change(function() {
		massupdatedeleteactaddgrid2();
	});
});
/*Criteria (condition) Add Grid*/
function massupdatedeletecriaddgrid1() {
	var wwidth = $("#massupdatedeletecriaddgrid1").width();
	var wheight = $("#massupdatedeletecriaddgrid1").height();
	$.ajax({
		url:base_url+"Base/localgirdheaderinformationfetch?tabgroupid=103&moduleid=252&width="+wwidth+"&height="+wheight+"&modulename=Massupdate Criteria",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#massupdatedeletecriaddgrid1").empty();
			$("#massupdatedeletecriaddgrid1").append(data.content);
			$("#massupdatedeletecriaddgrid1footer").empty();
			$("#massupdatedeletecriaddgrid1footer").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('massupdatedeletecriaddgrid1');
			var hidecritgridcol = ['creteriadatevalue','value','criteriaddfieldvaluename','jointable','indexname','fieldid'];
			gridfieldhide('massupdatedeletecriaddgrid1',hidecritgridcol);
		},
	});
}
/*Action grid*/
function massupdatedeleteactaddgrid2(page,rowcount) {
	var id = $("#moduleid").val();
	var viewid = $("#moduleviewid").val();
	if(id != "" && id != 1) {
		$.ajax({
			url:base_url+"Massupdatedelete/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata.table != "") {
					maintabinfo = pdata.table;
					$("#parenttable").val(pdata.table);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
					modulename = pdata.modname;
				} else {
					maintabinfo = 'company';
					parentid = 'companyid';
					modulename = 'company';
				}
			},
		});
	} else {
		viewid = '84';
		maintabinfo = 'company';
		parentid = 'companyid';
		modulename = 'company';
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	/*criteria grid details*/
	var cricount = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
	var gridData = getgridrowsdata('massupdatedeletecriaddgrid1');
	var criteriagriddata = JSON.stringify(gridData);
	/*pagination*/
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#massupdatedeleteactaddgrid2').width();
	var wheight = $('#massupdatedeleteactaddgrid2').height();
	/*col sort*/
	var sortcol = $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	$.ajax({
		url:base_url+"Massupdatedelete/gridvalinformationfetch?viewid="+viewid+"&maintabinfo="+maintabinfo+"&primaryid="+parentid+"&viewfieldids="+id+"&griddata="+criteriagriddata+"&cricount="+cricount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&modulename="+modulename+"&page="+page+"&records="+rowcount+"&footername=updatedelete",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#massupdatedeleteactaddgrid2').empty();
			$('#massupdatedeleteactaddgrid2').append(data.content);
			$('#massupdatedeleteactaddgrid2footer').empty();
			$('#massupdatedeleteactaddgrid2footer').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('massupdatedeleteactaddgrid2');
			{/*sorting*/
				$('.'+modulename+'headercolsort').click(function(){
					$('.'+modulename+'headercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					massupdatedeleteactaddgrid2();
				});
				sortordertypereset(''+modulename+'headercolsort',headcolid,sortord);
			}
			{/*pagination*/
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('#massupdatedeleteactaddgrid2footer ul li .page-text .active').data('rowcount');
					massupdatedeleteexpaddgrid2(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('#massupdatedeleteactaddgrid2footer ul li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					massupdatedeleteexpaddgrid2(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('#massupdatedeleteactaddgrid2footer ul li .page-text .active').data('rowcount');
					massupdatedeleteactaddgrid2(page,rowcount);
				});
				$('#'+modulename+'pgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('#massupdatedeleteactaddgrid2footer ul li.active').data('pagenum');
					massupdatedeleteactaddgrid2(page,rowcount);
				});
			}
			/*check box select events*/
			gridcheckboxselectevents('massupdatedeleteactaddgrid2');
			//Material select
			$('#updatedeletepgrowcount').material_select();
		},
	});
}
/*data export grid*/
function massupdatedeleteexpaddgrid3(page,rowcount) {
	var id = $("#moduleid").val();
	var viewid = $("#moduleviewid").val();
	if(id != "" && id != 1) {
		$.ajax({
			url:base_url+"Massupdatedelete/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata.table;
					$("#parenttable").val(pdata.table);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
					modulename = pdata.modname;
				} else {
					maintabinfo = 'company';
					parentid = 'companyid';
					modulename = 'company';
				}
			},
		});
	} else {
		viewid = '84';
		maintabinfo = 'company';
		parentid = 'companyid';
		modulename = 'company';
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	/*criteria grid details*/
	var cricount = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
	var gridData = getgridrowsdata('massupdatedeletecriaddgrid1');
	var criteriagriddata = JSON.stringify(gridData);
	/*pagination*/
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#massupdatedeleteexpaddgrid3').width();
	var wheight = $('#massupdatedeleteexpaddgrid3').height();
	/*col sort*/
	var sortcol = $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.'+modulename+'headercolsort').hasClass('datasort') ? $('.'+modulename+'headercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	$.ajax({
		url:base_url+"Massupdatedelete/gridvalinformationfetch?viewid="+viewid+"&maintabinfo="+maintabinfo+"&primaryid="+parentid+"&viewfieldids="+id+"&griddata="+criteriagriddata+"&cricount="+cricount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&modulename="+modulename+"&page="+page+"&records="+rowcount+"&footername=export",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#massupdatedeleteexpaddgrid3').empty();
			$('#massupdatedeleteexpaddgrid3').append(data.content);
			$('#massupdatedeleteexpaddgrid3footer').empty();
			$('#massupdatedeleteexpaddgrid3footer').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('massupdatedeleteexpaddgrid3');
			{/*sorting*/
				$('.'+modulename+'headercolsort').click(function(){
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					massupdatedeleteexpaddgrid3(page,rowcount);
				});
				sortordertypereset(''+modulename+'headercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					massupdatedeleteexpaddgrid3(page,rowcount);
				});
				$('#exportpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					massupdatedeleteexpaddgrid3(page,rowcount);
				});
				//Material select
				$('#exportpgrowcount').material_select();
			}
			//Material select
			/*check box select events*/
			gridcheckboxselectevents('massupdatedeleteexpaddgrid3');
			{/* check box events */
				$('#massupdatedeleteexpaddgrid3 div .headercheckbox').click(function(){
					if($('#massupdatedeleteexpaddgrid3 div .headercheckbox').prop('checked') == true) {
						$('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').prop('checked',true);
					} else {
						$('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').prop('checked',false);
					}
					if($('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').not(':checked').length>0) {
						$('#massupdatedeleteexpingridsel3').addClass('icon-blank32');
						$('#massupdatedeleteexpingridsel3').addClass('unchecked');
						$('#massupdatedeleteexpingridsel3').removeClass('icon-check51');
						$('#massupdatedeleteexpingridsel3').removeClass('checked');
						$('#allrecordexport').val('No');
					} else {
						$('#massupdatedeleteexpingridsel3').removeClass('icon-blank32');
						$('#massupdatedeleteexpingridsel3').removeClass('unchecked');
						$('#massupdatedeleteexpingridsel3').addClass('icon-check51');
						$('#massupdatedeleteexpingridsel3').addClass('checked');
						$('#allrecordexport').val('Yes');
					}
				});
				$('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').click(function(){
					if($('#massupdatedeleteexpaddgrid3 div.gridcontent .rowcheckbox').not(':checked').length>0) {
						$('#massupdatedeleteexpaddgrid3 div .headercheckbox').prop('checked',false);
						$('#massupdatedeleteexpingridsel3').addClass('icon-blank32');
						$('#massupdatedeleteexpingridsel3').addClass('unchecked');
						$('#massupdatedeleteexpingridsel3').removeClass('icon-check51');
						$('#massupdatedeleteexpingridsel3').removeClass('checked');
						$('#allrecordexport').val('No');
					} else {
						$('#massupdatedeleteexpaddgrid3 div .headercheckbox').prop('checked',true);
						$('#massupdatedeleteexpingridsel3').removeClass('icon-blank32');
						$('#massupdatedeleteexpingridsel3').removeClass('unchecked');
						$('#massupdatedeleteexpingridsel3').addClass('icon-check51');
						$('#massupdatedeleteexpingridsel3').addClass('checked');
						$('#allrecordexport').val('Yes');
					}
				});
			}
		},
	});
}

function gridformtogridvalidatefunction(gridnamevalue) {
	jQuery("#"+gridnamevalue+"validation").validationEngine({
		validateNonVisibleFields:false,
		onSuccess: function() {
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			formtogriddata(gridname,'ADD','last','');
			/* Show / Hide columns */
			var hidecritgridcol = ['creteriadatevalue','value','criteriaddfieldvaluename','jointable','indexname','fieldid'];
			gridfieldhide('massupdatedeletecriaddgrid1',hidecritgridcol);
			var showcritgridcol = ['formfields'];
			/* Data row select event */
			datarowselectevt();
			clearformsol();
			griddataid = 0;
			gridynamicname = '';
			var condcnt = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
			if(condcnt==0) {
				$("#massandorcondiddivhid").hide();
				$('#massandorcondid').removeClass('validate[required]');
				$('.condmandclass').text('');
			} else {
				$("#massandorcondiddivhid").show();
				$('#massandorcondid').addClass('validate[required]');
				$('.condmandclass').text('*');
			}
		},
		onFailure: function() {
			var dropdownid =['4','formfields','moduleid','massconditionid','massandorcondid'];
			dropdownfailureerror(dropdownid);
		}
	});
}
/*excel data export*/
function dataexportasexcel() {
	var viewcreationid = $('#defaultviewcreationid').val();
	var viewcolumnid = $('#viewcolumnids').val();
	var moduleid = $('#moduleid').val();
	var modulename = $('#moduleid').find('option:selected').data('moduleidhidden');
	var rowid = $('#parenttableid').val();
	var parenttable = $('#parenttable').val();
	var rowids = getselectedrowids('massupdatedeleteexpaddgrid3');
	var allrecord = $('#allrecordexport').val();
	var expfiletype = $('#exportfiletypeid').find('option:selected').data('exportfiletypeidhidden');
	var ExportPath = base_url+"Massupdatedelete/exceldataexport";
	var pageno = $('#massupdatedeleteexpaddgrid3footer ul li.active').data('pagenum');
	if(pageno == undefined) {
		pageno = 1;
	}
	var recordcount = $('#massupdatedeleteexpaddgrid3footer ul li .page-text .active').data('rowcount');
	if(recordcount == undefined) {
		recordcount = 100;
	}
	var conddatacount = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
	var condgriddata = getgridrowsdata('massupdatedeletecriaddgrid1');
	var criteriagriddata = JSON.stringify(condgriddata);
	
	var form = $('<form action="'+ExportPath +'" class="hidedisplay" name="excelexport" id="excelexport" method="POST" target="_blank"><input type="hidden" name="viewid" id="viewid" value="'+viewcreationid+'" /><input type="hidden" name="viewcolids" id="viewcolids" value="'+viewcolumnid+'" /><input type="hidden" name="viewmoduleid" id="viewmoduleid" value="'+moduleid+'" /><input type="hidden" name="viewmodulename" id="viewmodulename" value="'+modulename+'" /><input type="hidden" name="parenttabid" id="parenttabid" value="'+rowid+'" /><input type="hidden" name="parenttable" id="parenttable" value="'+parenttable+'" /><input type="hidden" name="currentpageids" id="currentpageids" value="'+rowids+'" /><input type="hidden" name="exportfiletype" id="exportfiletype" value="'+expfiletype+'" /><input type="hidden" name="allpageids" id="allpageids" value="'+allrecord+'" /><input type="hidden" name="pagenumber" id="pagenumber" value="'+pageno+'" /><input type="hidden" name="recordcounts" id="recordcounts" value="'+recordcount+'" /><input type="hidden" name="conddatacount" id="conddatacount" value="'+conddatacount+'" /><textarea name="congriddatas" id="congriddatas">'+criteriagriddata+'</textarea></form>');
	$(form).appendTo('body');
	form.submit();
}
/*Drop down values set for view*/
function formfieldsdropdownvalget(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Massupdatedelete/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#'+ddname+'').trigger('change');
		},
	});
}
function clearformsol() {
	$("#formfields").select2('val',"");
	$("#criteriaddfieldvalue").select2('val',"");
	$("#massconditionid").select2('val',"");
	$("#massandorcondid").select2('val',"");
	$("#value").val("");
	$("#jointable").val('');
	$("#indexname").val('');
	$("#critreiavalue").val('');
	$("#fieldid").val('');
}
/*for mass delete*/
function massupdatedelte(parenttable,ids) {
	var modulename = $('#moduleid').find('option:selected').data('moduleidhidden');
	var moduleid = $('#moduleid').val();
	$.ajax({
		url: base_url + "Massupdatedelete/massdeletefunction?parenttable="+parenttable+"&ids="+ids+"&moduleid="+moduleid+"&modulename="+modulename,
		cache:false,
		async:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$("#basedeleteoverlay").fadeOut();
				alertpopup("Deleted successfully");
				massupdatedeleteactaddgrid2();
				massupdatedeleteexpaddgrid3();
			} else { }
		},
	});
}
/*for mass update*/
function massupdateprocss(parenttable,ddvalue,fieldname,fieldvalue,ids,uitype){
	var modulename = $('#moduleid').find('option:selected').data('moduleidhidden');
	var moduleid = $('#moduleid').val();
	$.ajax({
		url: base_url + "Massupdatedelete/massupdatefunction?parenttable="+parenttable+"&fieldname="+fieldname+"&fieldvalue="+fieldvalue+"&uitype="+uitype+"&ids="+ids+"&modulename="+modulename+"&moduleid="+moduleid+"&ddvalue="+ddvalue,
		cache:false,
		async:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				massupdatedeleteactaddgrid2();
				alertpopup("Updated Successfully");
				$("#fieldname,#ddfieldvalue").select2('val','');
				$("#fieldvalue").val('');
				$("#datefieldvalue").val('');
				$("#finalfieldvalue").val('');
				$("#ddfieldvalue").empty();
			} else { }
		},
	});
}
/*field name based drop down value - picklist*/
function fieldmassnamebesdpicklistddvalue(fieldid) {
	var moduleid = $("#moduleid").val();
	$.ajax({
		url: base_url + "Massupdatedelete/fieldnamebesdpicklistddvalue?fieldid="+fieldid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#ddfieldvalue').empty();
			$('#ddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#ddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#ddfieldvalue').trigger('change');			
		},
	});
}
/*main drop down value*/
function fieldmassnamebesdddvalue(fieldid) {
	var moduleid = $("#moduleid").val();
	$.ajax({
		url: base_url + "Massupdatedelete/fieldnamebesdddvalue?fieldid="+fieldid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#ddfieldvalue').empty();
			$('#ddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#ddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#ddfieldvalue').trigger('change');			
		},
	});
}
/*field name based drop down value*/
function fieldviewnamebespicklistdddvalue(indexname) {
	var moduleid = $("#moduleid").val();
	$.ajax({
		url: base_url + "Massupdatedelete/fieldviewnamebespicklistdddvalue?fieldname="+indexname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#criteriaddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
/*field name based drop down value*/
function fieldviewnamebesdddvalue(indexname) {
	var moduleid = $("#moduleid").val();
	$.ajax({
		url: base_url + "Massupdatedelete/viewfieldnamebesdddvalue?fieldname="+indexname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#criteriaddfieldvalue').empty();
			$('#criteriaddfieldvalue').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#criteriaddfieldvalue')
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#criteriaddfieldvalue').trigger('change');			
		},
	});
}
/*check box value get*/
function newcheckboxvalueget(dropdownname) {
	$.ajax({
		url: base_url + "Massupdatedelete/newcheckboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownname+"").empty();
			$("#"+dropdownname+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownname+"")
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownname+"").trigger('change');			
		},
	});
}
/*view creation field get*/
function viewcreationfieldget(ids) {
	$.ajax({
		url: base_url + "Massupdatedelete/viewdropdownload?ids="+ids+'&autonum=no',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#formfields').empty();
			$('#formfields').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#formfields')
					.append($("<option></option>")
					.attr("data-ffname",data[index]['dataname'])
					.attr("data-jointable",data[index]['jointable'])
					.attr("data-indexname",data[index]['indexname'])
					.attr("data-uitype",data[index]['uitype'])
					.attr("data-id",data[index]['datasid'])
					.attr("data-label",data[index]['dataname'])
					.attr("data-formfieldshidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname'])); 
				});
			}
		},
	});
}
function viewcreationeditorfoeldget(ids) {
	$.ajax({
		url: base_url + "Massupdatedelete/viewdropdownload?ids="+ids+'&autonum=no',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#fieldname').empty();
			$('#fieldname').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
						$('#fieldname')
						.append($("<option></option>")
						.attr("data-formfieldsidhidden",data[index]['datasid'])
						.attr("data-modfiledcolumn",data[index]['indexname'])
						.attr("data-parenttable",data[index]['parenttable'])
						.attr("data-uitype",data[index]['uitype'])
						.attr("data-id",data[index]['datasid'])
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
				});
			}
		},
	});
}
function showhideintextbox(hideid,showid) {
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
/*data export column*/
function dataexportfieldnameset(ids) {
	$.ajax({
		url: base_url + "Massupdatedelete/viewdropdownload?ids="+ids+'&autonum=yes',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#exportcolumnname').empty();
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
						$('#exportcolumnname')
						.append($("<option></option>")
						.attr("data-formfieldsidhidden",data[index]['datasid'])
						.attr("data-modfiledcolumn",data[index]['indexname'])
						.attr("data-parenttable",data[index]['parenttable'])
						.attr("data-uitype",data[index]['uitype'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
				});
			}
		},
	});
	setTimeout(function() {
		$.ajax({
			url:base_url+"Massupdatedelete/exportcoldatafetch?modid="+ids,
			async:false,
			dataType:'json',
			cache:false,
			success:function(data) {
				if(data != "") { 
					var viewcolid = data.split(',');
					$('#exportcolumnname').select2('val',viewcolid).trigger('change');
				}
			},
		});
	},300);
}
/*field clear*/
function moduleddchangefieldclear() {
	cleargriddata('massupdatedeletecriaddgrid1');
	$("#formfields").empty();
	$("#formfields").select2('val',"");
	$("#criteriaddfieldvalue").select2('val',"");
	$("#massconditionid").select2('val',"");
	$("#criteriaddfieldvalue").empty();
	$("#value").val('');
	$("#jointable").val('');
	$("#indexname").val('');
	$("#fieldid").val('');
	$("#massandorcondid").select2('val',"");
}
//AND/OR  condition validation reset
function andorcondvalidationreset() {
	var viewcount = $('#massupdatedeletecriaddgrid1 .gridcontent div.data-content div').length;
	if(viewcount == 0) {
		$('#massandorcondiddivhid').hide();
		$('#massandorcondid').removeClass('validate[required]');
		$('.condmandclass').text('');
	} else {
		$('#massandorcondiddivhid').show();
		$('#massandorcondid').addClass('validate[required]');
		$('.condmandclass').text('*');
	}
}
//view drop down load
function viewdropdownload(invitemoduleid) {
	$('#moduleviewid').empty();
	$.ajax({
		url: base_url+"Massupdatedelete/viewdropdownloadfun?invitemoduleid="+invitemoduleid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#moduleviewid').append(newddappend);
			}	
		},
	});
}