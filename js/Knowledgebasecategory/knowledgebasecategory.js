$(document).ready(function()
{	
	//hidedisplay
	$('#knowledgebasecategorydataupdatesubbtn').hide();
	$('#knowledgebasecategorysavebutton').show();
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		knowledgebasecategoryaddgrid();
		gridsettings();
		firstfieldfocus();
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			//leadactivitiesgrid();
			knowledgebasecategoryaddgrid();
		});
	}
	{
		//validation for Company Add  
		$('#knowledgebasecategorysavebutton').click(function() 
		{
			$("#knowledgebasecategoryformaddwizard").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#knowledgebasecategoryformaddwizard").validationEngine(
		{
			onSuccess: function()
			{
				addformdata();
			},
			onFailure: function()
			{
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{
		//update company information
		$('#knowledgebasecategorydataupdatesubbtn').click(function() {
		
			$("#knowledgebasecategoryformeditwizard").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#knowledgebasecategoryformeditwizard").validationEngine({
			onSuccess: function()
			{
				updateformdata();
			},
			onFailure: function()
			{
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//counter hierarchy
		$('#categorylistuldata li').click(function() 
		{
			var name=$(this).attr('data-listname');
			var listid=$(this).attr('data-listid');
			$('#parentcategory').val(name);
			$('#parentcategoryid').val(listid);
			knowledgebasecategoryaddgrid();
		});	
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
   {//category action
	$("#categoryediticon").click(function()
	{
			var datarowid = $('#knowledgebasecategoryaddgrid').jqGrid('getGridParam', 'selrow');
			if (datarowid) 
			{
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				getformdata(datarowid);
				firstfieldfocus();
			} 
			else
			{
				alertpopup("Please select a row");
			}
	});
		$("#categoryreloadicon").click(function(){
				resetFields();
				clearinlinesrchandrgrid('knowledgebasecategoryaddgrid');
		});	
		$("#categorydeleteicon").click(function(){
			var datarowid = $('#knowledgebasecategoryaddgrid').jqGrid('getGridParam', 'selrow');
			if(datarowid){		
			$("#basedeleteoverlay").fadeIn();
			$("#basedeleteyes").focus();
			}
			else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
		var datarowid = $('#knowledgebasecategoryaddgrid').jqGrid('getGridParam','selrow'); 
		recorddelete(datarowid);
	});
   }
   {// From Computer
		var categorysettings = {
			url: base_url+"upload.php",
			method: "POST",
			fileName: "myfile",
			multiple: true,
			dataType:'json',
			allowedTypes: "png,jpg,jpeg",
			async:false,
			onSuccess:function(files,data,xhr)
			{
				var arr = data.split(',');
				var name = $('.dyimageupload').data('imgattr');
				$('#categoryimage').val(arr[3]);
				alertpopup('Logo Uploaded Successfully');
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#categoryimagemulitplefileuploader").uploadFile(categorysettings);
		//file upload drop down change function
		$("#categoryfileuploadfromid").change(function()
		{
			var valoption = $("#categoryfileuploadfromid").val();
			if(valoption == '2') {				
				$(".triggeruploadNaN").trigger('click');
			} else {
				$(".dropin-btn-status").trigger('click');
			} 
		});	
	}
	var addcloseviewcreation =["viewcloseformiconid","knowledgebasecategoryaddformview","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
});
{//view create success function
	function viewcreatesuccfun(viewname)
	{
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset")
		{
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			$('#viewcreateconditiongrid').jqGrid("clearGridData", true);
		}
		else
		{
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function knowledgebasecategoryaddgrid() 
{ 
   $("#knowledgebasecategoryaddgrid").jqGrid('GridUnload');
		//tree parent id
	/* var parentcategoryid = $('#parentcategoryid').val();
	var cuscondtablenames = 'category';
	var cusjointableid = 'categoryid';
	var cuscondfildname = 'category.parentcategoryid';
	var cuscondvalue = ''+parentcategoryid+''; */
		//label set to grid header
		var viewname = $('#dynamicdddataview').val();
		$('#viewgridheaderid').text(viewname);
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid,
			dataType:'json',
			async:false,
			cache:false,
			success :function(data)
			{ 
				var colsname = data.colname;
				var colData = '';
				colData = columnsData(data.colmodelname,data.colmodelindex,data.colmodeluitype,data.colmodelviewtype);
					colData = eval('{' + colData + '}');
				$("#knowledgebasecategoryaddgrid").jqGrid(
				{
					url:base_url+"Base/gridvalinformationfetch?viewid="+userviewid+"&maintabinfo=knowledgebasecategory&parentid=knowledgebasecategoryid&viewfieldids="+viewfieldids,
					datatype: "xml",
					colNames:colsname,
					colModel:colData,
					pager:'#knowledgebasecategoryaddgridnav',
					sortname:'knowledgebasecategory.knowledgebasecategoryid',
					sortorder:'desc',
					viewrecords:false,
					loadonce:false,
					toolbar: [false, "top"],
					height: 300,
					width: 1505
				});
			},
		});
		//Grid Settings
		formwithgridclass("knowledgebasecategoryaddgrid");
		var leadviewcreationistb =["1","knowledgebasecategoryaddgrid"];
		createistb(leadviewcreationistb);
}
// Grid Height Width Settings
function gridsettings() 
{
	formwithgridclass("knowledgebasecategoryaddgrid");
	var leaddocsistb = ["1", "knowledgebasecategoryaddgrid"];
	createistb(leaddocsistb);
}
//create counter
function addformdata()
{
    var formdata = $("#categoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax(
    {
        url: base_url + "Category/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE')
            {
				resetFields();
				clearinlinesrchandrgrid('knowledgebasecategoryaddgrid');
				alertpopup(savealert);
				setTimeout(function(){treereloadfun();},500); 
		    } 
         
        },
    });
}
//tree reload
function treereloadfun()
{
	var tablename = "category";
	$.ajax({
		url: base_url + "Category/treedatafetchfun",
        data: "tabname="+tablename,
		type: "POST",
		cache:false,
        success: function(data) {
			$('.'+tablename+'treediv').empty();
			$('.'+tablename+'treediv').append(data);
			$('#categorylistuldata li').click(function() {
				var name=$(this).attr('data-listname');
				var listid=$(this).attr('data-listid');
				$('#parentcategory').val(name);
				$('#parentcategoryid').val(listid);
				knowledgebasecategoryaddgrid();
			});
			$(function() {
				$( '#dl-menu' ).dlmenu({
						animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
				});
			});		
        },
    });
}
//retrive counter data on edit
function getformdata(datarowid)
{
	var categoryelementsname = $('#categoryelementsname').val();
	var categoryelementstable = $('#categoryelementstable').val();
	var categoryelementscolmn = $('#categoryelementscolmn').val();
	var categoryelementpartable = $('#categoryelementspartabname').val();
	$.ajax(
	{
		url:base_url+"Category/fetchformdataeditdetails?categoryprimarydataid="+datarowid+"&categoryelementsname="+categoryelementsname+"&categoryelementstable="+categoryelementstable+"&categoryelementscolmn="+categoryelementscolmn+"&categoryelementpartable="+categoryelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)
		{
			var txtboxname = categoryelementsname + ',categoryprimarydataid';
			var textboxname = {};
			textboxname = txtboxname.split(','); 
			var dataname = categoryelementsname + ',primarydataid';
			var datasname = {};
			datasname = dataname.split(',');
			var dropdowns = [];
			textboxsetvaluenew(textboxname,datasname,data,dropdowns);
		}
	});
	gettheparentcounter(datarowid);
	$('#knowledgebasecategorydataupdatesubbtn').show();
	$('#knowledgebasecategorysavebutton').hide();
}
//update counter
function updateformdata()
{
	var formdata = $("#categoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Category/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE')
            {
				$('#knowledgebasecategorydataupdatesubbtn').hide();
				$('#knowledgebasecategorysavebutton').show();
				clearinlinesrchandrgrid('knowledgebasecategoryaddgrid');
				resetFields();
				alertpopup(savealert);
				setTimeout(function(){treereloadfun();},500); 				
            }           
        },
    });	
}
//delete counter
function recorddelete(datarowid)
{
	var categoryelementstable = $('#categoryelementstable').val();
	var elementspartable = $('#categoryelementspartabname').val();
    $.ajax({
        url: base_url + "Category/deletetreeinformationdata?categoryprimarydataid="+datarowid+"&categoryelementstable="+categoryelementstable+"&categoryparenttable="+elementspartable,
		cache:false,
        success: function(msg) 
        {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE')
            {
				$("#knowledgebasecategoryaddgrid").trigger('reloadGrid');
				$("#basedeleteoverlay").fadeOut();
				setTimeout(function(){treereloadfun();},500); 
            } 
            else if (nmsg == "false") 
            {
            
            }
        },
    });
}
function reloadthecounterlistelement()
{
	$.ajax({               
				url:base_url+"Category/reloadcounterlist",
				dataType:'json',
				async:false,
				cache:false,
				success:function (data)
				{
					console.log(data);
				}
				});
}
//load the parent id
function gettheparentcounter(datarowid)
{
	$.ajax({               
			url:base_url+"Category/getcounterparentid?id="+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success:function (data)
			{
				
				$('#parentcategoryid').val(data.parentcategoryid);
				$('#parentcategory').val(data.categoryname);
			}
	});
}