$(document).ready(function(){	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		brandaddgrid();
		firstfieldfocus();
		brandcrudactionenable();
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			brandaddgrid();
		});
	}
	setTimeout(function() {
		$(".ajax-file-upload>form>input").removeAttr("disabled");//remove disabled attribute for uploadFile
		}, 1000);
	//hidedisplay
	$('#branddataupdatesubbtn').hide();
	$('#brandsavebutton').show();
	{
		$( window ).resize(function() {
			innergridresizeheightset('brandaddgrid');
		});
		//validation for  Add  
		$('#brandsavebutton').click(function(e) {
			$('#brandname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#brandformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#brandformaddwizard").validationEngine({
			onSuccess: function() {
				$('#brandsavebutton').attr('disabled','disabled');
				$("#processoverlay").show();
				brandaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}	
	{//update brand information
		$('#branddataupdatesubbtn').click(function(e){
			$('#brandname').mouseout();
			if(e.which==1 || e.which === undefined) {
				$("#brandformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#brandformeditwizard").validationEngine({
			onSuccess: function() {
				$('#branddataupdatesubbtn').attr('disabled','disabled');
				$("#processoverlay").show();
				brandupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	{//filter work
		//for toggle
		$('#brandviewtoggle').click(function(){
			if ($(".brandfilterslide").is(":visible")) {
				$('div.brandfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.brandfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#brandclosefiltertoggle').click(function(){
			$('div.brandfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#brandfilterddcondvaluedivhid").hide();
		$("#brandfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#brandfiltercondvalue").focusout(function(){
				var value = $("#brandfiltercondvalue").val();
				$("#brandfinalfiltercondvalue").val(value);
				$("#brandfilterfinalviewconid").val(value);
			});
			$("#brandfilterddcondvalue").change(function(){
				var value = $("#brandfilterddcondvalue").val();
				mainfiltervalue=[];
				$('#brandfilterddcondvalue option:selected').each(function(){
				    var $value =$(this).attr('data-ddid');
				    mainfiltervalue.push($value);
					$("#brandfinalfiltercondvalue").val(mainfiltervalue);
				});
				$("#brandfilterfinalviewconid").val(value);
			});
		}
		brandfiltername = [];
		$("#brandfilteraddcondsubbtn").click(function() {
			$("#brandfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#brandfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(brandaddgrid,'brand');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			resetFields();
			$("#brandsectionoverlay").removeClass("effectbox");
			$("#brandsectionoverlay").addClass("closed");
		});
	}
	$("#brandname").on('mouseout', function(){
		var primaryid = $("#brandprimarydataid").val();
		var accname = $("#brandname").val();
		var elementpartable = $('#brandelementspartabname').val();
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						alertpopup("Brand name already exists!");
						$("#brandname").val("");
					}
				}else {
					alertpopup("Brand name already exists!");
					$("#brandname").val("");
				}
			} 
		}
	});		
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}

//Documents Add Grid
function brandaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#brandaddgrid").width();
	var wheight = $("#brandaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#brandsortcolumn").val();
	var sortord = $("#brandsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.brandheadercolsort').hasClass('datasort') ? $('.brandheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.brandheadercolsort').hasClass('datasort') ? $('.brandheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.brandheadercolsort').hasClass('datasort') ? $('.brandheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#brandviewfieldids').val();
	if(userviewid != '') {
		userviewid= '57';
	}
	userviewid = userviewid!= '' ? '57' : userviewid;
	var filterid = $("#brandfilterid").val();
	var conditionname = $("#brandconditionname").val();
	var filtervalue = $("#brandfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=brand&primaryid=brandid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#brandaddgrid').empty();
			$('#brandaddgrid').append(data.content);
			$('#brandaddgridfooter').empty();
			$('#brandaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('brandaddgrid');
			{//sorting
				$('.brandheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.brandheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#brandpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.brandheadercolsort').hasClass('datasort') ? $('.brandheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.brandheadercolsort').hasClass('datasort') ? $('.brandheadercolsort.datasort').data('sortorder') : '';
					$("#brandsortorder").val(sortord);
					$("#brandsortcolumn").val(sortcol);
					var sortpos = $('#brandaddgrid .gridcontent').scrollLeft();
					brandaddgrid(page,rowcount);
					$('#brandaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('brandheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					brandaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#brandpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					brandaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#brandpgrowcount').material_select();
		},
	});	
}
function brandcrudactionenable() {
	{//add icon click
		$('#brandaddicon').click(function(e){
			$("#brandsectionoverlay").removeClass("closed");
			$("#brandsectionoverlay").addClass("effectbox");
			e.preventDefault();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#branddataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#brandsavebutton').show();
		});
	}
	$("#brandediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#brandaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$('#brandimageattachdisplay').empty();
			brandgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#branddeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#brandaddgrid div.gridcontent div.active').attr('id');		
		if(datarowid){
			clearform('cleardataform');
			$("#brandprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');			
			combainedmoduledeletealert('branddeleteyes');
			$("#branddeleteyes").click(function(){
				var datarowid =$("#brandprimarydataid").val();
				brandrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function brandrefreshgrid() {
		var page = $('ul#brandpgnum li.active').data('pagenum');
		var rowcount = $('ul#brandpgnumcnt li .page-text .active').data('rowcount');
		brandaddgrid(page,rowcount);
	}
}
//new data add submit function
function brandaddformdata() {
    var formdata = $("#brandaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Brand/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$('#processoverlay').hide();
            	$(".addsectionclose").trigger("click");
				resetFields();
				brandrefreshgrid();
				$("#brandsavebutton").attr('disabled',false);
				$('#brandimageattachdisplay').empty();
				alertpopup(savealert);			
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#brandsectionoverlay").removeClass("effectbox");
		$("#brandsectionoverlay").addClass("closed");
		$("#setdefault").val("No");
	});
}
//old information show in form
function brandgetformdata(datarowid) {
	var brandelementsname = $('#brandelementsname').val();
	var brandelementstable = $('#brandelementstable').val();
	var brandelementscolmn = $('#brandelementscolmn').val();
	var brandelementpartable = $('#brandelementspartabname').val();
	$.ajax( {
		url:base_url+"Brand/fetchformdataeditdetails?brandprimarydataid="+datarowid+"&brandelementsname="+brandelementsname+"&brandelementstable="+brandelementstable+"&brandelementscolmn="+brandelementscolmn+"&brandelementpartable="+brandelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				brandrefreshgrid();
			} else {
				$("#brandsectionoverlay").removeClass("closed");
				$("#brandsectionoverlay").addClass("effectbox");
				var txtboxname = brandelementsname + ',brandprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = brandelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var attachfldname = $('#brandattachfieldnames').val();
				var attachcolname = $('#brandattachcolnames').val();
				var attachuitype = $('#brandattachuitypeids').val();
				var attachfieldid = $('#brandattachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
			}
			
		}
	});
	$('#branddataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#brandsavebutton').hide();
}
//update old information
function brandupdateformdata() {
	var formdata = $("#brandaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Brand/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$('#processoverlay').hide();
            	$(".addsectionclose").trigger("click");
				$('#branddataupdatesubbtn').hide();
				$('#brandsavebutton').show();
				resetFields();
				$("#branddeleteicon").show();
				$("#branddataupdatesubbtn").attr('disabled',false);
				brandrefreshgrid();
				$('#brandimageattachdisplay').empty();
				alertpopup(updatealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["brandcloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//update old information.
function brandrecorddelete(datarowid) {
	var brandelementstable = $('#brandelementstable').val();
	var elementspartable = $('#brandelementspartabname').val();
    $.ajax({
        url: base_url + "Brand/deleteinformationdata?brandprimarydataid="+datarowid+"&brandelementstable="+brandelementstable+"&brandparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	brandrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	brandrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}