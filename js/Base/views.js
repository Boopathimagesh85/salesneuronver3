jQuery(document).ready(function() {
	cntaddcount=0;
	footaddcount = 0;
	FOOTER = 0;	
	{		
		$(".chzn-select").select2({
			containerCss: {
		    display: "block"
			}
			});
	}
	$(".select2-container").hover(function() {
		$( this ).find('.s2resetbtn').css('opacity','1');
	},function(){
		$( this ).find('.s2resetbtn').css('opacity','0');
	});
	{//for view creation
		// Drop Down Change Function
		$(".viewdropdownchange").change(function() {
			var dataattrname = ['reportcondcolumnid','footercolid'];
			var dropdownid = ['viewcondcolumn','columnname'];
			var textboxvalue = ['condcolumnid','footercolid'];
			var selectid = $(this).attr('id');
			var choseninfo = "s2id_";
			var dropdowninfo = choseninfo + selectid;
			$('#' + dropdowninfo + ' .select2-choice').removeClass('error');
			var index = dropdownid.indexOf(selectid);
			var selected = $('#' + selectid + '').find('option:selected');
			var datavalue = selected.data('' + dataattrname[index] + '');
			$('#' + textboxvalue[index] + '').val(datavalue);
		});
		/* hide process */
		$("#viewddcondvaluedivhid").hide();
		$("#viewdatecondvaluedivhid").hide();
		/* tab group */
		$('.viewtab').click(function(){
			var colname = $("#viewcolname").val();
			if(colname != null) {
				$(".viewtab").removeClass("active");
				$(this).addClass("active");
		        var e = $(this).data("subform");
		        $(".hidviewsubform").hide();
		        $("#viewsubformspan"+e).show();
			} else {
				if($(this).hasClass('footertab')) {
					alertpopup('Please select column(s)');
				}
			}
		});
		/* Add Condition Btn */
		$('#addconditionbtndiv').click(function() {
			$("#addconditionbtndiv,#woconditionbtndiv").hide();
			$("#conditionformdiv").show();
		});
		$('#cancelconditions').click(function() {
			$("#addconditionbtndiv,#woconditionbtndiv").show();
			$("#conditionformdiv").hide();
		});
		$("#viewcolname").select2Sortable();
		//view create multi select drop down value
		$("#viewcolname").change(function() {
			$('.viewformvalidation').validationEngine('hide');
			$("#s2id_viewcolname,.select2-results").removeClass('error');
			$("#s2id_viewcolname").find('ul').removeClass('error');
			var data = $('#viewcolname').select2('data');
			var finalResult = [];
			for( item in $('#viewcolname').select2('data') ) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			if(selectid != '') {
				$('#viewcolnameid').val(selectid);
				dropdownvalsetuitypeviewcols('viewcondcolumn','condcolumnid','ViewCreationColumnName','ViewCreationColumnId','viewcreationcolumns','uitypeid','viewcreationcolmodelindexname','ViewCreationColumnId',selectid);			
			}
			//remove the where clause-for unselected COLUMN-Name
			removeconditioncolumn();
		});
		//view condition drop down change function
		$("#viewcondcolumn").change(function(){
			//Based on reportcondition dropdwon seletection, the condition will load from uitype table
			$("#viewcondition,#viewaggregate").empty();
			$("#viewcondition,#viewaggregate").select2('val','')
			$('#viewcondition').append($("<option></option>"));
			$('#viewaggregate').append($("<option value='ACTUAL'>Actual Values</option>"));
			var fieldlable = $("#viewcondcolumn").val();
			$('.viewcleardataform').validationEngine('hideAll');
			var data = $("#viewcondcolumn").find('option:selected').data('uitypeid');
			conditiondropdownload(data);
			/*$.ajax({
				url:base_url+"Base/loadaggregatemethodbyuitype?uitypeid="+data, 
				dataType:'json',
				async:false,
				success: function(data) { 
						$.each(data, function(index){					
							$('#reportaggregate')
							.append($("<option></option>")
							.attr("value",data[index]['aggregatemethodkey'])
							.attr("data-whereclauseid",data[index]['whereclauseid'])
							.text(data[index]['aggregatemethodname']));
						});	
						$('#reportaggregate').select2('val','ACTUAL');
				}
			});*/
			if(data == '2' || data == '3' || data == '4'|| data == '5' || data == '6'|| data == '7'|| data == '10' || data == '11' || data == '12'|| data == '14'|| data == '30') {
				showhideintextbox('viewddcondvalue','viewcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewcondvalue").val('');
				$('#viewcondvalue').timepicker('remove');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#viewcondvalue").addClass('validate[required,maxSize[100]]'); 
				$("#viewddcondvalue").removeClass('validate[required]');
				$("#viewdatecondvalue").removeClass('validate[required]');
			} else if(data == '17' || data == '28') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").select2('val',"");
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]'); 
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdpicklistddvalue(fieldname,data,'viewddcondvalue');
			} else if(data == '18' || data == '19' || data == '21' || data == '22' || data == '25' || data == '26' || data == '29') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvalue(fieldname,'viewddcondvalue'); 
			} else if(data == '20') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				empgroupdrodownset('viewddcondvalue');
			}  else if(data == '32') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvalue('employeename','viewddcondvalue'); 
			} else if(data == '13') {
				$("#viewcondvalue").datepicker("disable");
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				checkboxvalueget('viewddcondvalue');
			} else if(data == '31') {
				//for hide
				$('#viewdatecondvalue').datetimepicker("destroy");
				$("#viewddcondvaluedivhid").hide();
				$("#viewcondvaluedivhid").hide();
				$("#viewdatecondvaluedivhid").show();
				$('#viewdatecondvalue').val('');
				//showhideintextbox('viewddcondvalue','viewcondvalue');
				$("#viewddcondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$('#viewdatecondvalue').addClass('validate[required]');
			   /*  moment.locale('fr');
				console.log(moment(1316116057189).fromNow())
				console.log(moment.locales()); */
				/* const element = document.querySelector('#viewdatecondvalue');
				const picker = new MaterialDatePicker({
					el: element,
					openedBy: 'click'
				}); */
				  const picker = new MaterialDatePicker()
					.on('submit', (val) => {
					 var valuedate = val.toDate() +'\n';
					 $('#finalviewcondvalue,#finalviewcondvaluename,#viewdatecondvalue').val(convert(valuedate));
					 Materialize.updateTextFields();
					})
					document.querySelector('#viewdatecondvalue')
					.addEventListener('click', () => picker.open() || picker.set(moment()));      
           
			}else if(data == '8') {
				//for hide
				$('#viewdatecondvalue').datetimepicker("destroy");
				$("#viewddcondvaluedivhid").hide();
				$("#viewcondvaluedivhid").hide();
				$("#viewdatecondvaluedivhid").show();
				$('#viewdatecondvalue').val('');
				//showhideintextbox('viewddcondvalue','viewcondvalue');
				$("#viewddcondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$('#viewdatecondvalue').addClass('validate[required]');				
				if(fieldlable == 'Date of Birth' || fieldlable == 'Start Date' || fieldlable == 'End Date') {
					var dateformetdata = $('#viewdatecondvalue').attr('data-dateformater');
					$('#viewdatecondvalue').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: null,
						changeMonth: true,
						changeYear: true,
						maxDate:0,
						yearRange : '1947:c+100',
						onSelect: function () {
							var checkdate = $(this).val();
							if(checkdate != '') {
								$("#finalviewcondvalue").val(checkdate);
								$("#finalviewcondvaluename").val(checkdate);
							}
						},
						onClose: function () {
							$(this).focus();$(this).focusout();$(this).focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				} else {
					var dateformetdata = $('#viewdatecondvalue').attr('data-dateformater');
					$('#viewdatecondvalue').datetimepicker({
						dateFormat: dateformetdata,
						showTimepicker :false,
						minDate: 0,
						onSelect: function () {
							var checkdate = $(this).val();
							$('#viewdatecondvalue').focusout();
							$("#finalviewcondvalue").val(checkdate);
							$("#finalviewcondvaluename").val(checkdate);
						},
						onClose: function () {
							$('#viewdatecondvalue').focus();
						}
					}).click(function() {
						$(this).datetimepicker('show');
					});
				}
			} else if(data == '9') {
				$('#viewdatecondvalue').datetimepicker("destroy");
				//for hide
				$("#viewddcondvaluedivhid").hide();
				$("#viewcondvaluedivhid").show();
				$("#viewdatecondvaluedivhid").hide();
				$('#viewcondvalue').val('');
				$("#viewddcondvalue").removeClass('validate[required]');
				$("#viewcondvalue").addClass('validate[required,maxSize[100]]');
				$('#viewdatecondvalue').removeClass('validate[required]');	
				$('#viewcondvalue').timepicker({
					'step':15,
					'timeFormat': 'H:i',
					'scrollDefaultNow': true,
				});
				var checkdate = $("#viewcondvalue").val();
				if(checkdate != ''){
					$("#finalviewcondvalue").val(checkdate);
					$("#finalviewcondvaluename").val(checkdate);
				}
			} else if(data == '27') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				fieldnamebesdddvaluewithcond(fieldname,'viewddcondvalue'); 
			} else if(data == '23') {
				showhideintextbox('viewcondvalue','viewddcondvalue');
				$("#viewdatecondvaluedivhid").hide();
				$("#viewddcondvalue").addClass('validate[required]');
				$("#viewddcondvalue").select2('val',"");
				$("#viewdatecondvalue").removeClass('validate[required]');
				$("#viewcondvalue").removeClass('validate[required,maxSize[100]]');
				$("#viewcondvalue").datepicker("disable");
				var fieldname = $("#viewcondcolumn").find('option:selected').data('indexname');
				attributepurityvalueget(fieldname,'viewddcondvalue'); 
			} else {
				$("#viewcondcolumn").select2('val','');
				alertpopup('Please choose another field name');
			}
		});
		{
			//field value set
			$("#viewcondvalue").focusout(function(){
				var value = $("#viewcondvalue").val();
				$("#finalviewcondvalue").val(value);
				$("#finalviewcondvaluename").val(value);
			});
			$("#viewddcondvalue").change(function(){
				var value = $("#viewddcondvalue").val();
				var dataid = $("#viewddcondvalue").find('option:selected').data('ddid');
				mainfiltervalue=[];
				$('#viewddcondvalue option:selected').each(function(){
				    var $value =$(this).attr('data-ddid');
				    mainfiltervalue.push($value);
					$("#finalviewcondvalue").val(mainfiltervalue);
				});
				$("#finalviewcondvaluename").val(value);
			});
		}
		//view submit validation
		$('#viewwoconsubmitbtn').on('click mousedown',function(e){
			if(e.which == 1 || e.which === undefined) {
				nviewcolids = [];
				$('#viewmaincolumnname_to option').each(function(i, selected) {
					nviewcolids[i]=$(this).val();
				});
				if(nviewcolids.length > 0){
					$(".viewformvalidation").validationEngine('validate');
				} else{
					alertpopup('Please select the view columns');
				}
			}
		});
		$(".viewformvalidation").validationEngine({
			onSuccess: function() {
				dynamicviewgridadd();
			},
			onFailure: function() {
				var valueofmulselect = $("#viewcolname").val();
				if(valueofmulselect ==''){
					var dropdownid =['1','viewcolname'];
					dropdownfailureerror(dropdownid);
					$("#s2id_viewcolname").find('ul').addClass('error');
				}
			}
		});
		//view edit submit validation
		$('#vieweditconsubmitbtn').on('click mousedown',function(e){
			if(e.which == 1 || e.which === undefined) {
				uviewcolids = [];
				$('#viewmaincolumnname_to option').each(function(i, selected) {
					uviewcolids[i]=$(this).val();
				});
				if(uviewcolids.length > 1){
					$(".vieweditformvalidation").validationEngine('validate');
				} else{
					alertpopup('Please select the view columns');
				}
			}
		});
		$(".vieweditformvalidation").validationEngine({
			onSuccess: function() {
				dynamicviewgridupdate();
			},
			onFailure: function() {
				var dropdownid =['1','viewcolname'];
				dropdownfailureerror(dropdownid);
				$("#s2id_viewcolname").find('ul').addClass('error');
			}
		}); 
		//view condition form to grid
		$('#viewaddcondsubbtn').click(function() {
			andorcondvalidationreset();
			setTimeout(function(){
				$("#viewformconditionvalidation").validationEngine('validate');
			},50);
		});
		$("#viewformconditionvalidation").validationEngine({
			onSuccess: function() {
				cntaddcount++;
				formtogriddata('viewcreateconditiongrid','ADD','last','');
				clearform('viewcondclear');
				/* Data row select event */
				datarowselectevt();
				if(cntaddcount==1) {
					$('.condmandclass').text('*');
					$('#viewandorcond').addClass('validate[required]');
					var dropdownid =['3','viewcondcolumn','viewcondition','viewandorcond'];
					dropdownfailureerror(dropdownid);
					var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#viewandorconddivhid").hide();
					} else {
						$("#viewandorconddivhid").show();
					}
				} else {
					var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$('.condmandclass').text('');
						$("#viewandorconddivhid").hide();
						$('#viewandorcond').removeClass('validate[required]');
						var dropdownid =['2','viewcondcolumn','viewcondition'];
						dropdownfailureerror(dropdownid);
					} else {
						$('.condmandclass').text('*');
						$("#viewandorconddivhid").show();
						$('#viewandorcond').addClass('validate[required]');
						var dropdownid =['3','viewcondcolumn','viewcondition','viewandorcond'];
						dropdownfailureerror(dropdownid);
					}
				}
			},
			onFailure: function() {
				var dropdownid =['4','viewcondcolumn','viewcondition','viewandorcond','viewddcondvalue'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				if($('#viewcolname').val() == '') {
					$("#s2id_viewcolname").find('ul').addClass('error');
				}
			}
		});
	}	
	var addcloseviewcreation =["viewcloseformiconid","gridviewdivforsh","viewcreationformdiv",""];
	addcloseview(addcloseviewcreation);
	{
		//view condition delete
		$('#conddeleteicon').click(function(){
			var viewconrowid = $('#viewcreateconditiongrid div.gridcontent div.active').attr('id');
 			if (viewconrowid) {
				deletegriddatarow('viewcreateconditiongrid',viewconrowid);
				$('#viewcreateconditiongrid .gridcontent div.wrappercontent div ul:first li.massandorcondidname-class').text('');
				if(viewconrowid != '') {
					var did = $('#conrowcolids').val();
					$('#conrowcolids').val(did+','+viewconrowid);
				}
				var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
				if(condcnt==0) {
					$('.condmandclass').text('');
					$("#viewandorconddivhid").hide();
					$('#viewandorcond').removeClass('validate[required]');
				} else {
					$('.condmandclass').text('*');
					$("#viewandorconddivhid").show();
					$('#viewandorcond').addClass('validate[required]');
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{// Onclick View DD
		$(".viewselecticonclass").click(function () {
			tabletheadertap = 0;
			$("#viewddoverlay").show();
		});
		$("#viewselectioncloseid").click(function () {
			$("#viewddoverlay").hide(); 
		});
	}
	$("#viewreloadiconbtn").click(function () {
		viewcondvaluereset();
		clearform('viewcleardataform');
		clearform('viewclearfooterform');
		$('#viewdefaultopt,#viewpublicopt').val('No');
		$("#viewdefault,#viewfavourite").prop('checked',false);
		cleargriddata('viewcreateconditiongrid');
		$('#viewcondcolumn').empty();
		$('#viewcondcolumn').append($("<option></option>"));
		setTimeout(function() {
			var vcselectid = $('#defviewfiledids').val();0
			var selectid = vcselectid.split(',');
			$('#viewcolname').select2('val',selectid).trigger('change');
		},400);
	});
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}		
		});
	}
	{
		/* code for left menu */
			var topmenupanelht =  $( window ).height();
			var remaintopmenupanelht = topmenupanelht - 152;
			$(".topmenu-panel").css('height',remaintopmenupanelht);
			$(".collapsible-header").click(function() {	
				var leftpanelht =  $( window ).height();
				var remainleftpanelht = leftpanelht - 112;
				$(".panelcontent").css('height',remainleftpanelht);
			});
			$( window ).resize(function() {
				var leftpanelht =  $( window ).height();
				var remainleftpanelht = leftpanelht - 112;
				$(".panelcontent").css('height',remainleftpanelht);
			});
			$(".button-collapse").sideNav();
			$('.dropdown-button').dropdown({
			      inDuration: 300,
			      outDuration: 225,
			      constrain_width: false, // Does not change width of dropdown to that of the activator
			      hover: false, // Activate on hover
			      gutter: 0, // Spacing from edge
			      belowOrigin: true, // Displays dropdown below the button
			      alignment: 'right' // Displays dropdown with edge aligned to the left of button
			    }
			  );
		/* code for left menu */
	}
	/* Select Column */
	$("#viewmaincolumnname_rightSelected,#viewmaincolumnname_rightAll").click(function(){
		$('#viewmaincolumnname_to option').each(function(i, selected) {
			if($(this).attr('data-applysummary') == '') {
				$(this).attr('data-applysummary','no');				
			}
		});
		$('#viewmaincolumnname_to').trigger('click');
	});
	$('#view_startdate,#view_enddate').attr("disabled",true);
	$('#view_range').change(function() {
		if($(this).val() == 'custom') {
			var uitypeid = $("#view_datefield").find('option:selected').data('uitypeid');
			if(uitypeid == '31'){
				$('.viewdatepickerdiv').addClass('hidedisplay');
				$('.viewdatetimepickerdiv').removeClass('hidedisplay');
				datetimepickerstartendtrigger();
			}else{
				$('.viewdatepickerdiv').removeClass('hidedisplay');
				$('.viewdatetimepickerdiv').addClass('hidedisplay');
				$('#view_startdate,#view_enddate').removeAttr('disabled');
			   $('#view_startdate').datetimepicker('option', 'minDate', null);
			}
		} else {
			$('#view_startdate,#view_enddate').val('');
			$('#view_startdate,#view_enddate').attr("disabled",true);
		}
	});
});
//view name validate
function viewnamecheck() {
	var viewname = $('#viewname').val();
	var viewmoduleids = $('#viewcreatemoduleid').val();
	var nmsg = "";
	var user = "";
	if( viewname !="" ) {
		$.ajax({
			url:base_url+"Base/dynamicviewnamecheck",
			data:"data=&viewname="+viewname+"&viewmodids="+viewmoduleids,
			type:"post",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				nmsg = $.trim(data.msg);
				user = $.trim(data.user);
			},
		});
		if(nmsg != "False") {
			var viewacttype = $('#viewactiontype').val();
			if(viewacttype == 1) {
				var viewid = $('#viewdduserviewid').val();
				if(viewid != nmsg) {
					if(user == "True") {
						return "View name exists as private";
					} else {
						return "View name already exists!";
					}
				}
			} else {
				if(user == "True") {
					return "View name exists as private";
				} else {
					return "View name already exists!";
				}
			}
		}
	}
}
//View condition Grid
function viewcreateconditiongrid(page,rowcount) {
	var wwidth = $("#viewcreateconditiongrid").width();
	var wheight = $("#viewcreateconditiongrid").height();
	$.ajax({
		url:base_url+"Base/viewconddatafetch?moduleid=1&width="+wwidth+"&height="+wheight+"&modulename=Condition Grid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#viewcreateconditiongrid").empty();
			$("#viewcreateconditiongrid").append(data.content);
			$("#viewcreateconditiongridfooter").empty();
			$("#viewcreateconditiongridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('viewcreateconditiongrid');
		},
	});
}
//edit information show in form
function viewcreationeditshowinfo(viewid) {
	$.ajax({
		url:base_url+"index.php/Base/fetchvieweditdetails?userviewid="+viewid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				$('#conrowcolids').val('0');
				$('#viewdduserviewid').val(data['viewid']);
				$('#viewname').val(data['viewname']);
				var viewcolids = data['viewcolid'];
				var viewcolid = viewcolids.split(',');
				$.each(viewcolids.split(","), function(i,e){			
					$("#viewmaincolumnname option[value="+e+"]").attr("selected",true);
					$("#viewmaincolumnname_rightSelected").trigger('click');
				});
				var viewpublic = data.vpublic;
				if(viewpublic == 'Yes') {
					$("#viewfavourite").prop('checked',true);
					$("#viewpublicopt").val('Yes');
				} else {
					$("#viewfavourite").prop('checked',false);
					$("#viewpublicopt").val('No');
				}
				var viewdefault = data.viewdefault;
				if(viewdefault == 'Yes') {
					$("#viewdefault").prop('checked',true);
					$("#viewdefaultopt").val('Yes');
				} else {
					$("#viewdefault").prop('checked',false);
					$("#viewdefaultopt").val('No');
				}
				$('#viewcolnameid').val(viewcolids);
				//view data filter
				setTimeout(function(){
					$("#view_datefield").select2('val',data.viewdatecolumnid);
					$("#view_range").select2('val',data.viewdatemethod);
					var uitypeid = $("#view_datefield").find('option:selected').data('uitypeid');
					if(uitypeid == '31'){
						$('.viewdatepickerdiv').addClass('hidedisplay');
						$('.viewdatetimepickerdiv').removeClass('hidedisplay');
						$("#view_startdatetime").val(data.viewstartdate);
						$("#view_enddatetime").val(data.viewenddate);
						datetimepickerstartendtrigger();
					}else{
						$('.viewdatepickerdiv').removeClass('hidedisplay');
						$('.viewdatetimepickerdiv').addClass('hidedisplay');
						$("#view_startdate").val(data.viewstartdate);
						$("#view_enddate").val(data.viewenddate);
					}
					Materialize.updateTextFields();
				},100);
				Footer = 1;
				//view set default value get
				var moduleid = $("#viewcreatemoduleid").val();
				Materialize.updateTextFields();
				//viewsetdef(viewid,moduleid);
				conditiongriddatafetch(viewid);
			} else {
				alertpopup('You dont have a permission to use this view');
			}
		}
	});
}
//AND/OR  condition validation reset
function andorcondvalidationreset() {
	var viewcount = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
	if(viewcount == 0) {
		$('#viewandorcond').removeClass('validate[required]');
		$('.condmandclass').text('');
	} else {
		$('#viewandorcond').addClass('validate[required]');
		$('.condmandclass').text('*');
	}
}
//set defaule val get
function viewsetdef(viewid,moduleid) {
	$.ajax({
		url:base_url+"index.php/Base/fetchdefval?userviewid="+viewid+"&moduleid="+moduleid, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if(data == 'Yes') {
				$("#viewdefault").prop('checked',true);
				$("#viewdefaultopt").val('Yes');
			} else {
				$("#viewdefault").prop('checked',false);
				$("#viewdefaultopt").val('No');
			}
		}
	});
}
//dynamic view add function
function dynamicviewgridadd() {
	var data = $('#viewcolname').select2('data');
	var finalResult = [];
	var viewcolids = [];
	$('#viewmaincolumnname_to option').each(function(i, selected) {
		viewcolids[i]=$(this).val();
	});
	$('#viewcolnameid').val(viewcolids);
	var viewcondcount = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('viewcreateconditiongrid');
	var viewdata = JSON.stringify(griddata);
	//footer content
	var footercount = '';
	var footergriddata = '';
	var footerdata = '';
	var viewname = $("#viewname").val();
	var viewcolnameid = $("#viewcolnameid").val();
	var viewmoduleid = $("#viewcreatemoduleid").val();
	var viewdefault = $("#viewdefaultopt").val();
	var viewpublic = $("#viewpublicopt").val();
	var date_columnid=$('#view_datefield').val();
	var date_method=$('#view_range').val();
	var date_mode=$('#view_range').find('option:selected').data('mode');	
	var date_start=$('#view_startdate').val();
	var date_end=$('#view_enddate').val();
	var date_starttime=$('#view_startdatetime').val();
	var date_endtime=$('#view_enddatetime').val();
	var datauitypeid = $("#view_datefield").find('option:selected').data('uitypeid');
	$.ajax({
		url:base_url+"Base/viewcreatecon",
		type:"POST",
		data:"data="+viewdata+"&count="+viewcondcount+"&viewname="+viewname+"&viewcolids="+viewcolids+"&moduleid="+viewmoduleid+"&setdefault="+viewdefault+"&setpublic="+viewpublic+"&fcount="+footercount+"&fdata="+footerdata+"&date_columnid="+date_columnid+"&date_method="+date_method+"&date_start="+date_start+"&date_end="+date_end+"&date_mode="+date_mode+"&date_starttime="+date_starttime+"&date_endtime="+date_endtime+"&datauitypeid="+datauitypeid,
		cache:false,
		async:false,
		success :function(msg) {
			var nmsg = $.trim(msg);
			if(nmsg == "Success") {
				viewcreatesuccfun(viewname);
				clearform('viewcleardataform');
				clearform('viewclearfooterform');
				viewcondvaluereset();
				$('#viewcreationformdiv').hide();
				$('.gridviewdivforsh').fadeIn(900);
				alertpopup('View is created successfully.');
				Footer = 0;
			} else {
				viewcreatesuccfun('dontset');
				clearform('viewcleardataform');
				clearform('viewclearfooterform');
				viewcondvaluereset();
				$('#viewcreationformdiv').hide();
				$('.gridviewdivforsh').fadeIn(900);
				alertpopup('View is not created.');
				Footer = 0;
			}
			//For Keyboard Shortcut Variables
			viewgridview = 1;
			addformview = 0;
		}
	});
}
//dynamic view update function
function dynamicviewgridupdate() {
	var viewcondcount = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
	var griddata = getgridrowsdata('viewcreateconditiongrid');
	var viewdata = JSON.stringify(griddata);
	//footer content
	var footercount = '';
	var footergriddata ='';
	var footerdata = '';
	var viewname = $("#viewname").val();
	var data = $('#viewcolname').select2('data');
	var finalResult = [];
	var viewcolids = [];
	$('#viewmaincolumnname_to option').each(function(i, selected) {
		viewcolids[i]=$(this).val();
	});
	$('#viewcolnameid').val(viewcolids);
	var viewmoduleid = $("#viewcreatemoduleid").val();
	var viewid = $("#viewdduserviewid").val();
	var condeleid = $("#conrowcolids").val();
	var viewdefault = $("#viewdefaultopt").val();
	var viewpublic = $("#viewpublicopt").val();
	var footerrowcolids = $("#footerrowcolids").val();
	var date_columnid=$('#view_datefield').val();
	var date_method=$('#view_range').val();
	var date_mode=$('#view_range').find('option:selected').data('mode');	
	var date_start=$('#view_startdate').val();
	var date_end=$('#view_enddate').val();
	var date_starttime=$('#view_startdatetime').val();
	var date_endtime=$('#view_enddatetime').val();
	var datauitypeid = $("#view_datefield").find('option:selected').data('uitypeid');
	$.ajax({
		url:base_url+"Base/viewupdatecondition",
		type:'POST',
		data:"data=" +viewdata+"&count="+viewcondcount+"&viewname="+viewname+"&viewcolids="+viewcolids+"&moduleid="+viewmoduleid+"&viewdduserviewid="+viewid+"&conrowcolids="+condeleid+"&setdefault="+viewdefault+"&setpublic="+viewpublic+"&fcount="+footercount+"&fdata="+footerdata+"&footerrowcolids="+footerrowcolids+"&date_columnid="+date_columnid+"&date_method="+date_method+"&date_start="+date_start+"&date_end="+date_end+"&date_mode="+date_mode+"&date_starttime="+date_starttime+"&date_endtime="+date_endtime+"&datauitypeid="+datauitypeid,
		cache:false,
		async:false,
		success :function(msg) {
			var nmsg = $.trim(msg);
			var nmsg = $.trim(msg);
			if(nmsg == "Success") {
				viewcreatesuccfun(viewname);
				clearform('viewcleardataform');
				clearform('viewcondclear');
				viewcondvaluereset();
				$("#viewddcondvalue").empty('');
				$("#viewcondvalue").val('');
				$("#viewdefaultopt,#viewpublicopt").val('No');
				$("#viewdefault,#viewfavourite").prop('checked',false);
				$('#viewcreationformdiv').hide();
				$('.gridviewdivforsh').fadeIn(900);
				alertpopup('View is updated successfully.');
				Footer = 0;
			} else {
				viewcreatesuccfun(viewname);
				clearform('viewcleardataform');
				clearform('viewcondclear');
				viewcondvaluereset();
				$("#viewddcondvalue").empty('');
				$("#viewcondvalue").val('');
				$("#viewdefaultopt,#viewpublicopt").val('No');
				$("#viewdefault,#viewfavourite").prop('checked',false);
				$('#viewcreationformdiv').hide();
				$('.gridviewdivforsh').fadeIn(900);
				alertpopup('View is not updated.');
				Footer = 0;
			}
			//For Keyboard Shortcut Variables
			addformupdate = 0;
			viewgridview = 1;
			addformview = 0;
		}
	});
}
function showhideintextbox(hideid,showid){
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
//field name based drop down value
function fieldnamebesdddvalue(fieldid,dropdownid) {
	$.ajax({
		url: base_url + "Base/fieldnamebesdddvalue?fieldid="+fieldid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownid+"")
					.append($("<option></option>")
					.attr("data-ddid",data[index]['datasid'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
function empgroupdrodownset(dropdownid) {
	$.ajax({
        url: base_url + "Base/viewemployeegroupsdata",
		dataType: "json",
		async: false,
		cache:false,
        success: function(data) {
			if(data['status']!='fail') {
				$('#'+dropdownid+'').empty();
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['PId'];
					if(prev != cur ) {
						if(prev != " ") {
							$('#'+dropdownid+'').append($("</optgroup>"));
						}
						$('#'+dropdownid+'').append($("<optgroup  label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
						prev = data[index]['PId'];
					}
					$("#"+dropdownid+" optgroup[label='"+data[index]['PName']+"']")
					.append($("<option></option>")
					.attr("class",data[index]['CId']+'pclass')
					.attr("value",data[index]['CName'])
					.attr("data-ddid",data[index]['CId'])
					.attr("data-dataidhidden",data[index]['PId'])
					.text(data[index]['CName']));
				});
			}
        },
    });
}
//field name value assign with condition
function fieldnamebesdddvaluewithcond(fieldname,dropdownid) {
	var moduleid = 	$("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/fieldnamebesdddvaluewithcond?fieldid="+fieldname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownid+"")
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
//attribute value get
function attributepurityvalueget(fieldname,dropdownid){
	var moduleid = $("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/attributevalfetch?fieldid="+fieldname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					var attributeid = data[index]['Id'];
					dynamicattributedropdownvalfetch(attributeid,dropdownid);
				});
			}
		},
	});
}
//attribute value get
function dynamicattributedropdownvalfetch(attributeid,dropdownid){
	var moduleid = $("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/attributedropdownvaluefetch?fieldid="+attributeid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownid+"")
					.append($("<option></option>")
					.attr("value",data[index]['Name'])
					.text(data[index]['Name']));
				});
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
//field name based drop down value - picklist
function fieldnamebesdpicklistddvalue(fieldid,uitype,ddname){
	var moduleid = $("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/fieldnamebesdpicklistddvalue?fieldid="+fieldid+"&moduleid="+moduleid+"&uitype="+uitype,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#'+ddname+'').empty();
			$('#'+ddname+'').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-ddid",data[index]['datasid'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$('#'+ddname+'').trigger('change');			
		},
	});
}
//check box value get
function checkboxvalueget(ddname){
	$.ajax({
		url: base_url + "Base/checkboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#'+ddname+'').empty();
			$('#'+ddname+'').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16') {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					}
				});
			}
			$('#'+ddname+'').trigger('change');			
		},
	});
}
function conditiongriddatafetch(viewid) {
	if(viewid!='') {
		$.ajax({
			url:base_url+'Base/conditiongriddatafetch?userviewid='+viewid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata('viewcreateconditiongrid',data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize('viewcreateconditiongrid');
					var hideprodgridcol = [];
					gridfieldhide('viewcreateconditiongrid',hideprodgridcol);
					var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#viewandorconddivhid").hide();
					} else {
						$("#viewandorconddivhid").show();
					}
				}
			},
		});
	}
}
//condition drop down load
function conditiondropdownload(data) {
	$('#viewcondition').empty();
	$.ajax({
		url:base_url+"Base/loadconditionbyuitype?uitypeid="+data, 
		dataType:'json',
		async:false,
		success: function(data) { 
			$.each(data, function(index){					
				$('#viewcondition')
				.append($("<option></option>")
				.attr("value",data[index]['whereclausename'])
				.text(data[index]['whereclausename']));
			});	
		}
	});
}
function convert(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
		hours  = ("0" + date.getHours()).slice(-2);
        minutes = ("0" + date.getMinutes()).slice(-2);
		seconds = ("0" + date.getSeconds()).slice(-2);
    return day + '-' + mnth + '-' + date.getFullYear()+ ' ' + hours +':'+minutes+':'+seconds;
}
function datetimepickerstartendtrigger(){
	const picker = new MaterialDatePicker()
		.on('submit', (val) => {
		 var valuedate = val.toDate() +'\n';
		 $('#view_startdatetime').val(convert(valuedate));
		 Materialize.updateTextFields();
		})
		document.querySelector('#view_startdatetime')
		.addEventListener('click', () => picker.open() || picker.set(moment()));
	const pickerend = new MaterialDatePicker()
		.on('submit', (val) => {
		 var valuedateend = val.toDate() +'\n';
		 $('#view_enddatetime').val(convert(valuedateend));
		 Materialize.updateTextFields();
		})
		document.querySelector('#view_enddatetime')
		.addEventListener('click', () => pickerend.open() || pickerend.set(moment()));
}