$(document).ready(function() 
{	
	{// Scroll left menu on click
		scrollleftmenu();
		className = 'category';
		minifitertype = 'dashboard';
		filterstatus = 0;
	}
	ovrpopuphideshow = $('#ovrpopuphideshow').val();
	{// left menu fold-open - GRA
		$('#homeicon').click(function(){
			//openleftMenusside();//append black overlay
			window.location.href =base_url+'Home';
		});
		$('.homepagediv').click(function(){
			$("#slide-out").css('left','0px');
			openleftMenusside();//append black overlay
			
		});
		$('.main-menu li.main-category > a').on('click keypress',function(e){
			var className = $(this).attr('class');
			if(className == 'category'){
				openleftMenusside();//append black overlay
			}
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('.main-category .sub-menu-1').removeClass('open-submenu');
				if($('.sub-menu-1').hasClass('open-submenu') || $(this).hasClass('active-category')){
					$(this).parent().find('.sub-menu-1').removeClass('open-submenu');
					$('.main-menu li.main-category > a').each(function(i, obj) {
						$(this).removeClass('active-category');
		 			});
				}else{
					$('.main-menu li.main-category > a').each(function(i, obj) {
						$(this).removeClass('active-category');
		 			});
					$(this).parent().find('.sub-menu-1').addClass('open-submenu');
					$(this).addClass('active-category');
				}
			}
		});
		$('.main-menu li.sub-category > a').on('click keypress',function(e){
			if(e.keyCode == 13 || e.keyCode === undefined){
				$('.main-category .sub-menu-2').removeClass('open-submenu');
				if($('.sub-menu-2').hasClass('open-submenu') || $(this).hasClass('active-subcategory')){
					$(this).parent().find('.sub-menu-2').removeClass('open-submenu');
					$('.main-menu li.sub-category > a').each(function(i, obj) {
						if($(this).hasClass('active-subcategory')){
							$('.active-subcategory').css('background','#455a64');
						}
						$(this).removeClass('active-subcategory');
						
		 			});
				}else{
					$('.main-menu li.sub-category > a').each(function(i, obj) {
						if($(this).hasClass('active-subcategory')){
							$('.active-subcategory').css('background','#455a64');
						}
						$(this).removeClass('active-subcategory');
		 			});
					$(this).parent().find('.sub-menu-2').addClass('open-submenu');
					$(this).addClass('active-subcategory');
					$(this).css('background','#37474f');
				}
			}
		});
		$(".side-nav.fixed .main-category").hover(function() {
			var sideclass = $(".side-nav.fixed").attr('class');
			if(sideclass == 'side-nav fixed') {
				$( this ).find('.maincattitle').css('display','block');
			} else {
				$( this ).find('.maincattitle').css('display','');
			}
		},function(){
			$( this ).find('.maincattitle').css('display','');
		});
		setTimeout(function(){
			
			$(".navOpts").css('display','none');
		},500);
	}
	$('#createcheck').click(function() {
		$("#createshortcut").toggle();
	});
	$('#accountshortcut').click(function() {
    	window.location = base_url+'Account';
    });
	$('#productshortcut').click(function() {
    	window.location = base_url+'Product';
    });
	$('#stockshortcut').click(function() {
    	window.location = base_url+'Itemtag';
    });
	$('#transactionshortcut').click(function() {
    	window.location = base_url+'Sales';
    });
	$('#chitshortcut').click(function() {
    	window.location = base_url+'Chitbookentry';
    });
	$('#productaddonshortcut').click(function() {
    	window.location = base_url+'Productaddon';
    });
	$('#personalsettingsicon').click(function() {
    	window.location = base_url+'Personalsettings';
    });
	$("#fullscreen").on("click", function() 
 {
  document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen();
  $("#fullscreen").css('display','none');
  $("#fullscreenexit").css('display','block');
  
 });
 $("#fullscreenexit").on("click", function() 
 {
  document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen();
  $("#fullscreenexit").css('display','none');
  $("#fullscreen").css('display','block');
  
 });
	{// show first transitionhiddenform to visible implemented for mobile- GRA
		$( ".addformcontainer .transitionhiddenform , .salesaddformcontainer .transitionhiddenform, .mastermodules .transitionhiddenform" ).first().each(function(i) {
			var deviceas = deviceinfo;
			if(deviceas == "phone") {
				$('.addformcontainer .transitionhiddenform , .salesaddformcontainer .transitionhiddenform, .mastermodules .transitionhiddenform').first().addClass('form-active');  
				//$(this).nextAll().css({"left":"100%" , "position":"absolute"});
				//$(this).prevAll().css({"left":"-100%" , "position":"absolute"});
			}
		});
	}
	{// tabgroup position view in container for mobile - GRA 
		 $('.mobiletabgroup li').on('click',function(){
			  var pos=$(this).position().left; //get left position of li alert(pos);
			  var currentscroll=$(".mobiletabgroup").scrollLeft(); // get current scroll position alert(currentscroll);
			  pos=(pos-currentscroll); // for center position if you want adjust then change this alert(pos);
			  $('.mobiletabgroup').animate({
			            scrollLeft: pos
			  },1200);
			});
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#overlayfilterdisplaymainview').css({"display":"block"});
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#overlayfilterdisplaymainview').css({"display":"none"});
		});
	}
	{// mobile action overlay - GRA
		$('.dropdown-button-above').dropdown({
		      inDuration: 300,
		      outDuration: 225,
		      constrain_width: false, // Does not change width of dropdown to that of the activator
		      hover: true, // Activate on hover
		      gutter: 0, // Spacing from edge
		      belowOrigin: false, // Displays dropdown below the button
		    }
		  );
	}
	{//Module view Dropdown additional class added for full width set - GRA
		setTimeout(function(){
			//$("#dynamicdddataview").select2({dropdownCssClass : 'bigdrop'});
		},100);
	}
	{// Focus To first field on canceling overlays - GRA
		$('.alertbtn').click(function(){
			var firstInput = $('form').find('input[type=text],input[type=password],input[type=radio],input[type=checkbox],textarea,select').filter(':visible:first');
			if (firstInput != null && $(this).val() == 'Cancel') {
			   var deviceas = deviceinfo;
			   if(deviceas != "phone") {
				   firstInput.focus();
			   }
			   $('.ui-timepicker-wrapper').hide();
			   $('.ui-datepicker').hide();
			   $('.sectionpanel').animate({scrollTop:0},'slow');
			   $('.addformcontainer').animate({scrollTop:0},'slow');
			}
		});
	}
	{ //KeyBoard Shortcuts
		//Globle Variables For Keyboard ShortCut
		viewgridview = 1;
		addformview = 0;
		addformupdate = 0;
		innergridview = 0;
		filtername = [];
		filtercid=[];
		filtervid=[];
		Operation = 0; //0 for add/clone and 1 for Edit for pagination work by gowtham
		document.onkeyup=function(e){
		  var e = e || window.event; // for IE to cover IEs window event-object
		  if(e.altKey && e.which == 65) {
		    alert('Keyboard shortcut working!');
		    return false;
		  }
		}
	}
	{ // Hotkeys - Shortcut functionality
		// Functionality for CRUD
		hotkeys($('#commonkey_action').val(), function (event, handler) {
			var arrayfunc = $('#commonkey_action').val().split(",");
			var keyindval = arrayfunc.indexOf(handler.key);
			var fieldidarray = $('#commonkey_action').attr('data-keyactionid').split(",");
			if (typeof fieldidarray[keyindval] !== 'undefined') {
				$(''+fieldidarray[keyindval]+'').trigger('click');
			}
		});
		// Left Menu, Rate, Notification, ShortcutKey & Other Common Module Functionality
		hotkeys($('#all_commonkey_action').val(), function (event, handler) {
			var arrayfunc = $('#all_commonkey_action').val().split(",");
			var keyindval = arrayfunc.indexOf(handler.key);
			var fieldidarray = $('#all_commonkey_action').attr('data-keyactionid').split(",");
			if (typeof fieldidarray[keyindval] !== 'undefined') {
				$(''+fieldidarray[keyindval]+'').trigger('click');
			}
		});
	}
	//stone entry overlay
	operationstone='ADD';
	//time picker close on select2 open fix for overlapping issue
	$('.chzn-select').select2().on("select2-opening", function(e) {
		$('.ui-timepicker-wrapper').hide();
		$('.ui-datepicker').hide();
	})
	// vishal code for mobile dd
	{
		var deviceas = deviceinfo;
	    if(deviceas == "phone") {
			$(".chzn-select").on("select2-open", function() { 
				
				$('.select2-results').css('max-height', '400px');
				$('.select2-input').css('display', 'none');
				$('.bigdrop .select2-input').css('display', '');
				$('.select2-input').prop('readonly', true);
				$('.bigdrop .select2-input').prop('readonly', false);
				$( ".select2-results" ).after( "<span id='showddsearch' class='addbtnclass icon-box' style='top: 70px; position: relative;width:10em;height:3em;'> <input id='ishowddsearch' name='ishowddsearch'  value='Search' style='width:100% !important;float: left;height: 100%;' class='alertbtnyes  ffield' type='button'></span> ");
			});
			$(".chzn-select").on("select2-close", function() { 
				$(".select2-drop #showddsearch").remove(); 
			}); 
			
			/* $(".input-field input").focus(function() {
				var a = $(this).attr('id');
				var element = document.getElementById(a);
				element.scrollIntoView();
				var h = $(this).closest(".scrollbarclass").scrollTop(); 
				$(this).closest(".scrollbarclass").scrollTop(h-30);
				
				});	 */
		}
	}
	$('.addformcontainer').scroll(function(){
		$('.ui-timepicker-wrapper').hide();
		$('.ui-datepicker').hide();
	});
	//backspace restriction
	$(document).keydown(function(e) {
         var element = e.target.nodeName.toLowerCase();
         if ((element != 'input' && element != 'textarea') || $(e.target).attr("readonly") || (e.target.getAttribute("type") ==="checkbox") ||  ($(e.target).hasClass("backsapceclass")) ) {
                 if (e.keyCode === 8) {
                	 return false;
                 }
         }
	});
	{/* Chrome fix for settimeout function when open module in new tab - GRA*/ 
		(function() {
			var time = 1000,
				delta = 100,
				tid;
			tid = setInterval(function() {
				if ( window.blurred ) { return; }    
				time -= delta;
				if ( time <= 0 ) {
					clearInterval(tid);
					formheight(); // time passed - form height set function
				}
			}, delta);
		})();
		window.onblur = function() { window.blurred = true; };
		window.onfocus = function() { window.blurred = false; };
	}
	{//remove froala editor  trace
		$('a[href*="https://froala.com/wysiwyg-editor"]').hide();
	}	
	{//Default addform Height Set
		formheight();
	}		
	{//window resize form height set
		$( window ).resize(function() {
			formheight();
			//Tabgroup More Dropdown
			if($('.desktoptabgroup.tabgroupstyle').is(':visible')){
				fwgautocollapse();
			}
		});
	}
	{// For Pop Content
		//For Save
		savealert = "Data is stored successfully.";
		validationalert = "Please enter mandatory fields";
		updatealert = "Updated successfully";
		deletealert = "Deleted successfully";
		submiterror = "Sorry error occurred !!Please try again";
		selectrowalert = "Please select a row";
	}
	{// On Load Fade In - verify
		$('body').fadeIn(1500);
	}
	{// Global value for close and submit
		globalclosetrigger = "";
		globalsubmit = "";
		$('#alertscloseno').click(function (e) {
			globalsubmit = "";
			globalclosetrigger = "";
			$(".alertsoverlay").fadeOut('fast');
		});
	}
	{// select2  Styles and Initialisation 
		$(".chzn-select").select2({blurOnChange: true});
		$(".extra .select2-drop").addClass('containerwidth');
		Materialize.updateTextFields();
	}
	// Disable INput search on select2 for tablets/mobiles
	{
		var deviceas = deviceinfo;
		if(deviceas=="phone"){
		$('.chzn-select').select2(
		{
				//minimumResultsForSearch: -1// at least 20 results must be displayed
		});
			Materialize.updateTextFields();
	}
	}
	{// In-line Search Show Hide
		$(".inlinefocus").addClass('hidedisplay');
		$(".inlinesearhbase").addClass('inlinesearchsh');
		$('.searchiconclass').toggle(function(){
			$(".inlinefocus").removeClass('hidedisplay');
			$(".inlinesearhbase").removeClass('inlinesearchsh');
			$(".stdinlinesize").removeClass('onclickreduceheight');

			var inlinesrchheightaddform = $( window ).height();
			var remainingheight = inlinesrchheightaddform - 129;
			$('.ui-jqgrid-bdiv.mgccontentareaht80').css('height',remainingheight);
		},function(){
			$(".inlinefocus").addClass('hidedisplay');
			$(".stdinlinesize").addClass('onclickreduceheight');
			$(".inlinesearhbase").addClass('inlinesearchsh');

			var inlinesrchheightaddform = $( window ).height(); 
			var remainingheight = inlinesrchheightaddform - 102;
			$('.ui-jqgrid-bdiv.mgccontentareaht80').css('height',remainingheight);
		});
    }
	{// For validation Class remove on Focus Out (remove the red border for dropdown)
		$(".chzn-select").change(function() {
			var getid = $(this).attr('id');
			var forremoveerror = "s2id_"+getid+"";
			$('#'+forremoveerror+' .select2-choice').removeClass('error');
		});
    }
	{// Alerts pop up close(single)
		$(".alertsoverlaybtn").click(function() {
			$(".alertsoverlay").fadeOut('fast');
			 errorfieldfocus(); //GRA- to focus first field after success message
		});
	}
	{// Alerts pop up close (Double alert)
		$(".alertsdoubleoverlaybtn").click(function() {
			$("#alertsdouble").fadeOut('fast');
			errorfieldfocus(); //GRA- to focus first field after success message
		});
	}
	{// General Form Clear 
		$("#formclearicon").click(function() {
			resetFields();
		});
	}
	{// Set read-only Select2
		$('.readonly').select2('readonly',true);
		$('body').dblclick(function(e){
			e.preventDefault();
		});
	}
	{// Double Submit Restriction base Function - Main Grid 
		$("#dataaddsbtn").click(function() {
			document.getElementById('dataaddsbtn').style.pointerEvents = 'none';
			setTimeout(function(){
			document.getElementById('dataaddsbtn').style.pointerEvents = 'auto';},150);  
		});
		$("#dataupdatesubbtn").click(function() {
			document.getElementById('dataupdatesubbtn').style.pointerEvents = 'none';
			setTimeout(function(){
			document.getElementById('dataupdatesubbtn').style.pointerEvents = 'auto';},150);  
		});
	}
	{// for focus looping alert
		focusloopfun();
	}
	{// Reload Hide on Edit
		$(".editiconclass").click(function() {
			$("#formclearicon").hide();
			//$(".off-canvas-wrap").css({'padding-left':'0px'});
			//$("#slide-out").hide(); 
		});
		$(".addiconclass").click(function()	{
			$("#formclearicon").show();
			 //$(".off-canvas-wrap").css({'padding-left':'0px'});
			 //$("#slide-out").hide(); 
		});
	}
	{// Drop Down Change Function (multiple value set) - Ramesh
		$(".ddcahangeclass").change(function(){
			var dropid = $(this).attr('id');
			var txtid = $('#'+dropid+'').data('ddid');
			if( $('#s2id_'+dropid+'').hasClass('select2-container-multi') ) {
				var data = $('#'+dropid+'').select2('data');
				var finalResult = [];
				for( item in $('#'+dropid+'').select2('data') ) {
					finalResult.push(data[item].id);
				};
				var selectid = finalResult.join(',');
				$('#'+txtid+'').val(selectid);
			} else {
				var ddval = $('#'+dropid+'').val();
				$('#'+txtid+'').val(ddval);
			}
		});
	}
	{// Select2 drop down Reset button icon Hover
		$( ".select2-choice" ).hover(function() {
			$( this ).children('i').removeClass('hidedisplay');
		}, function() {
			$( this ).children('i').addClass('hidedisplay');
		});
	}
	{//Overlay Top Bar Code Tab group
		$(".sidebaricons").click(function() {
			 var deviceas = deviceinfo;
			 //if(deviceas != "phone") {
				$(".tab-title").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('subform');
				$(".hiddensubform").hide();
				$("#subformspan"+subfomval+"").show();
			 /* }else{//mobile form transition 
				$(".tab-title").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('subform');
				$('.sidebaricons').css('pointerEvents','none');
				$('.addformcontainer').scrollTop('0');
				//$(".transitionhiddenform").removeClass('form-active');
				$("#subformspan"+subfomval+"").addClass('form-active').next().addClass('form-active').prev().addClass('form-active');
				$("#subformspan"+subfomval+"").nextAll().css({"left":"100%" , "position":"absolute"});
				$("#subformspan"+subfomval+"").prevAll().css({"left":"-100%" , "position":"absolute"});
				$( "#subformspan"+subfomval+"" ).animate({
					left:"0",
				  }, 50 );
				setTimeout(function(){
					$("#subformspan"+subfomval+"").prevAll().removeClass('form-active');
					$("#subformspan"+subfomval+"").nextAll().removeClass('form-active');
					$('.sidebaricons').css('pointerEvents','auto');
				},500);
			 } */
			 if($('.addsectionclose').is(':visible')){
					//section close on tab click
					 $('.addsectionclose:visible').trigger('click');
				}
		});
	}
	{//company setting Overlay
		$(".companysidebaricons").click(function() {
			 var deviceas = deviceinfo;
			 if(deviceas != "phone") {
				$(".tab-title").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('overlaysubform');
				$(".hiddensubform").hide();
				$("#overlaysubformspan"+subfomval+"").show();
			 }else{
				$(".tab-title").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('overlaysubform');
				$('.companysidebaricons').css('pointerEvents','none');
				$('.addformcontainer').scrollTop('0');
				//$(".transitionhiddenform").removeClass('form-active');
				$("#overlaysubformspan"+subfomval+"").addClass('form-active').next().addClass('form-active').prev().addClass('form-active');
				$("#overlaysubformspan"+subfomval+"").nextAll().css({"left":"100%" , "position":"absolute"});
				$("#overlaysubformspan"+subfomval+"").prevAll().css({"left":"-100%" , "position":"absolute"});
				$( "#overlaysubformspan"+subfomval+"" ).animate({
					left:"0",
				  }, 50 );
				setTimeout(function(){
					$("#overlaysubformspan"+subfomval+"").prevAll().removeClass('form-active');
					$("#overlaysubformspan"+subfomval+"").nextAll().removeClass('form-active');
					$('.companysidebaricons').css('pointerEvents','auto');
				},500);
			 }
			 if($('.addsectionclose').is(':visible')){
					//section close on tab click
					 $('.addsectionclose:visible').trigger('click');
				}
		});
	}
	{//Filter,mini dashboard Tab
		$(".sidebariconsmini").click(function() {
			 var deviceas = deviceinfo;
			 if(deviceas != "phone") {
				$(".tab-titlemini").removeClass('active');
				$(this).addClass('active');
			 }else{//mobile form transition 
				$(".tab-titlemini").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('subform');
				$('.sidebariconsmini').css('pointerEvents','none');
				$('.addformcontainer').scrollTop('0');
				//$(".transitionhiddenform").removeClass('form-active');
				$("#subformspan"+subfomval+"").addClass('form-active').next().addClass('form-active').prev().addClass('form-active');
				$("#subformspan"+subfomval+"").nextAll().css({"left":"100%" , "position":"absolute"});
				$("#subformspan"+subfomval+"").prevAll().css({"left":"-100%" , "position":"absolute"});
				$( "#subformspan"+subfomval+"" ).animate({
					left:"0",
				  }, 50 );
				setTimeout(function(){
					$("#subformspan"+subfomval+"").prevAll().removeClass('form-active');
					$("#subformspan"+subfomval+"").nextAll().removeClass('form-active');
					$('.sidebariconsmini').css('pointerEvents','auto');
				},500);
			 }
			 if($('.addsectionclose').is(':visible')){
					//section close on tab click
					 $('.addsectionclose:visible').trigger('click');
				}
		});
	}
	{//Dashboard Top base Function
		$(".dbsidebaricons").click(function() {
			var deviceas = deviceinfo;
			 if(deviceas != "phone") {
			 	$(".dbtab-title").removeClass('active');
				$(this).addClass('active');
				var subfomval = $(this).data('dbsubform');
				$(".dbhiddensubform").hide();
				$("#dbsubformspan"+subfomval+"").show();
			 }else{
					var activetab = $(this).next().hasClass('active');
					$(".dbtab-title").removeClass('active');
					$(this).addClass('active');
					var subfomval = $(this).data('dbsubform');
					$('.dbsidebaricons').css('pointerEvents','none');
					if(activetab){
						$(".transitionhiddenform").removeClass('form-active');
						$("#dbsubformspan"+subfomval+"").next().addClass('form-active');
						$("#dbsubformspan"+subfomval+"").prev().addClass('form-active');
						$("#dbsubformspan"+subfomval+"").addClass('form-active');
						$( "#dbsubformspan"+subfomval+"").nextAll().css('left','100%');
						$( "#dbsubformspan"+subfomval+"").prevAll().css('left','-100%');
						$("#dbsubformspan"+subfomval+"").prevAll().css('position','absolute');
						$("#dbsubformspan"+subfomval+"").nextAll().css('position','absolute');
					}else{
						$(".transitionhiddenform").removeClass('form-active');
						$("#dbsubformspan"+subfomval+"").next().addClass('form-active');
						$("#dbsubformspan"+subfomval+"").prev().addClass('form-active');
						$("#dbsubformspan"+subfomval+"").addClass('form-active');
						$( "#dbsubformspan"+subfomval+"").nextAll().css('left','100%');
						$( "#dbsubformspan"+subfomval+"").prevAll().css('left','-100%');
						$("#dbsubformspan"+subfomval+"").prevAll().css('position','absolute');
						$("#dbsubformspan"+subfomval+"").nextAll().css('position','absolute');
					}
					$( "#dbsubformspan"+subfomval+"" ).animate({
						left:"0",
					  }, 50 );
					setTimeout(function(){
						$("#dbsubformspan"+subfomval+"").prevAll().removeClass('form-active');
						$("#dbsubformspan"+subfomval+"").nextAll().removeClass('form-active');
						$('.dbsidebaricons').css('pointerEvents','auto');
					},500);
			 }
			 if($('.addsectionclose').is(':visible')){
					//section close on tab click
					 $('.addsectionclose:visible').trigger('click');
				}
		});	
	} 
	{// For Print - Ramesh
		//pdf print close
		$('#pdfclose,#pdfpreviewcancel').click(function() {
			$('#defaultpdftemplateid').val('').trigger('change');
			$('#printmultipletempids').val('').trigger('change');
			$('#printmultipleview').select2('val','0').trigger('change');
			$("#templatepdfoverlay").fadeOut();
		});
		//print preview close
		$('#printclose,#printpreviewcancel').click(function(){
			$("#templateprintoverlay").fadeOut();
		});
		 //preview template
		$('#pdffilepreviewintab').click(function(){
			var templateid = $('#pdftemplateprview').val();
			if(checkValue(templateid)== false || templateid <= 1 ){
				$('#pdfclose').trigger('click')
				alertpopupdouble('No Print');
				return false;
			} else {
			    $('#pdfviewoverlayformvalidation').validationEngine('validate');
			}
		});
		$('#pdfviewoverlayformvalidation').validationEngine({
			onSuccess: function() {
				var randomnumber = Math.floor((Math.random() * 100) + 1);
				var templateid = $('#pdftemplateprview').val();
				var date = $('#dateofstock').val();
				var todate = $('#dateofstockto').val();
				posmode = 'Yes';
				$('#pdfclose').trigger('click');
				$("#pdfpaths").empty();//unset and resets
				$('#printingdiv').empty();
				var id = $('#printpdfid').val();
				var snumber = $('#printsnumber').val();
				snumber  = snumber.split('/').join('');
				var stocksessionid = $('#stocksessionid').val();
				var printtypeid = $('#pdftemplateprview').find("option:selected").data("printtypeid");
				var printername = $('#pdftemplateprview').find("option:selected").data("printername");
				var pdfmoduleid = $('#pdftemplateprview').find("option:selected").data("moduleid");
				var viewtype = "print";
				$.ajax({
					url:base_url+'Base/templatefilepdfpreview',
					data:{id:id,snumber:snumber,templateid:templateid,stockdate:date,stocktodate:todate,stocksessionid:stocksessionid,pdfmoduleid:pdfmoduleid,randomnumber:randomnumber,viewtype:viewtype},
					cache:false,
					success: function(msg) {
						var nmsg =  $.trim(msg);
						if(nmsg == 'SUCCESS') {	
							if(printtypeid == '3') { //html print-new tab
								var htmlurl = base_url+'billshtml/'+snumber+randomnumber+'.html';
								var w = window.open(htmlurl, '_blank');
								//w.focus();
							}else if(printtypeid == '4') {
								var excelurl = base_url+'excel.php';
								var w = window.open(excelurl, '_blank');									
								//w.focus();
							}else if(printtypeid == '2') { //pdf
								var pdfurl = base_url+'printtemplate/receipt.pdf';
								var w = window.open(pdfurl, '_blank');									
								//w.focus();
							}
						} else if(printtypeid == '6' && msg != '') { //SILENT HTML
							$('#printingdiv').append(msg);
							printHtml();
						} else if(printtypeid == '5' && msg != '') { //EPSON JS
							epsonxmlprint(msg,printername)
						} else {
							alertpopup('Error on preview,try again!!!');
						}
					},
				});
			},
			onFailure: function() {
				
				var dropdownid =['1','pdftemplateprview'];
				dropdownfailureerror(dropdownid);
			}
		});
	}
	{// For Notification - Sanjeevi
		$('.bellnotificationclass').click(function(){
			$.ajax({
				url:base_url+"Home/notificationoverlayfetch",
				cache:false,
				success :function(data) {
					$("#notificationoverlayappend").empty();
					$("#notificationoverlayappend").append(data);
					$("#notificationdivoverlay").show();
					$('#notificationdivoverlayclose').click(function(){
						$('#notificationdivoverlay').hide();
						$("#notificationdivmobileoverlay").parent().removeClass("move-right");
					});
				}
			});
			var datatype='newactivefeeds';
			$.ajax({
				url:base_url+"Home/getnotificationdata",
				data: {datatype:datatype,lastnode:0},			
				type:"POST",
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					var loop=data.length;			
					feedcontent(loop,'feedsliveid','feedsliveidclass',data);
				}
			});
			//reset the main counter on click of the counter
			$.ajax({
				url:base_url+"Home/resetnotificationcounter",
				cache:false,
				success :function(msg) {
					//$('.notificationbadg').text(0);
					//$('.notificationbadg').removeClass('belliconcolor');
					//$('#bellnotificationicon').removeClass('icon24-notifications_active');
					//$('#bellnotificationicon').addClass('icon24-notifications_none');
					$("#bellnotification").empty();
	        		$("#bellnotification").append('<i title="Filter" id="bellnotificationicon" class="material-icons" >notifications_none</i>');
				}
			});
		});
		//getnotificationcount();
		/* setInterval(function() {
			getnotificationcount();
		},300000); */
	}
	{// On click Overlay Alerts fadeout - verify
		$('#alerts').click(function(){
			$(this).fadeOut();	
		}); 
	}
	{// On Enter Check-box Trigger
		$( ".checkboxcls" ).keypress(function(e) {
			if (e.keyCode == 13) {
				$(this).trigger('click');
			  }
		});
	}
	{//drop down value close
		$('.s2resetbtn').click(function(){
			var e = $(this).parent().closest("div").attr("id"),
            a = e.substr(5); //s2id_
        $("#" + a).hasClass("select2-container-multi") ? 0 == $("#" + a).prop("readonly") ? 0 == $("#" + a).prop("disable") ? ($("#" + a).select2("val", ""), this.selectedIndex = 0) : "" : 0 == $("#" + a).prop("disable") ? ($("#" + a).select2("val", ""), this.selectedIndex = 0) : "" : 0 == $("#" + a).prop("readonly") ? 0 == $("#" + a).prop("disable") ? ($("#" + a).select2("val", ""), this.selectedIndex = 0) : "" : 0 == $("#" + a).prop("disable") ? ($("#" + a).select2("val", ""), this.selectedIndex = 0) : ""
        	if(a == 'employeeidddid'){
        		$("#employeetypeid").val('1');
        	}
		});
	}
	{//view all notification
		$('#showalllist').click(function(){
			window.location.href =base_url+'Auditlog';
		});
	}
	{// Gowtham verify
		$("#rcmaillink,.rcmaillink").click(function() {
	        $("#roundcubelogin").submit()
	    });
	}
	{//support page overlay
		$('.supportoverlay').click(function(){
			$.ajax({
				url:base_url+"Home/supportoverlayfetch",
				cache:false,
				success :function(data) {
					$("#supportoverlayappend").empty();
					$("#supportoverlayappend").append(data);
					$("#supportpageoverlay").show();
					$("#supportmobileoverlay").parent().addClass("move-right");
					//support overlay close
					$('.supportpageoverlayclose').click(function(){
						$('#supportpageoverlay').hide();
						$("#supportmobileoverlay").parent().removeClass("move-right");
					});
					{// support Overlay Top Bar Code
						$(".tabliicons").click(function() {
							$(".tablititle").removeClass('tabactive');
							$(this).addClass('tabactive');
							$('.supportoverlayul li i').removeClass('icon24');
							$('.supportoverlayul li i').addClass('icon24-w');
							$('.supportoverlayul li.tabactive i').removeClass('icon24-w');
							$('.supportoverlayul li.tabactive i').addClass('icon24');
							var supportsubformval = $(this).data('supportsubform');
							$(".hiddensupportsubform").hide();
							if(supportsubformval == 4) {
								$(".support-updatepan").empty;
								$("#supportsubformpan"+supportsubformval+"").show();
								fetchupdatedata();
							} else {
								$("#supportsubformpan"+supportsubformval+"").show();
							}
						});	
					}
				}
			});
		});
	}
	//select2 close show/hide
	setTimeout(function(){
		$(".select2-container").hover(function() {
			$( this ).find('.s2resetbtn').css('opacity','1');
		},function(){
			$( this ).find('.s2resetbtn').css('opacity','0');
		});
	},2000);
	//file uploder - need to verify- gowtham
	$('.filechooserdrive').click(function(){
		var type = $(this).data('type');
		var buttonid = $(this).data('buttonid');
		if(type=='dropbox') {
			$('#span'+buttonid+' a .dropin-btn-status').trigger('click');
		} else if(type=='desktop') {
			$("#"+buttonid+"uploadoverlay").fadeIn(100);
		}
	});
	//layout click events
	$('.mobfilterspan').click(function(){
		alertpopup('Service available in paid version');
	});
	//action overlay show/hide [drop menu] - CHECK AND REMOVE
	$('.action-click').click(function(){
		$('.actionoverlay').toggle();
	});
	$('.actionoverlay li , .button-collapse').click(function(){
		$('.actionoverlay').hide();
		$('.action-drop').hide();
	});
	//user menu click function - CHECK AND REMOVE
	$('.userimg').click(function(){
		$("#usermobileoverlay").parent().addClass("move-right");
	});
	$(".usermobileoverlayclose").click(function() {
        $("#usermobileoverlay").parent().removeClass("move-right")
    });
	/* clear demo data */
	$("#dataclearicon").click(function(){
		combinedmodalertdynamictxt('cleardatabtn','Are you sure to clean demo data?');
		$("#cleardatabtn").click(function(){
			$('#basedeleteoverlayforcommodule').hide();
			$("#processoverlay").show();
			$.ajax({
				url:base_url+"Subscriptionmanager/cleardemodata",
				type:'POST',
				async:false,
				cache:false,
				success:function(data) {
					var nmsg =  $.trim(data);
					if(nmsg == 'success') {
						$('#basedeleteoverlayforcommodule').fadeOut();
						alertpopup('your demo data is cleared successfully.');
					} else {
						$('#basedeleteoverlayforcommodule').fadeOut();
						alertpopup('your demo data is not cleared.');
					}
				}
			});
			$("#processoverlay").hide();
		});
	});
	/*plan upgrade*/
	$('#umplanupgrade').click(function(){
		$("#upgradepalnoverlay").fadeIn();
	});
	{//Toggle Search filter
		$('#filtericon').on('click', function() { 
			$(".filterbox").toggleClass("filterbox-change");
		});	
	}
	$(".multicolumnfilter").change(function(){
		var data = $(this).find("option:selected").data("uitype");
		var modname = $(this).find("option:selected").data("modname");
		var fieldname = $(this).find("option:selected").data("indexname");
		var moduleid = $(this).find("option:selected").data("moduleid");
		var fieldlable = $(this).find("option:selected").data("label");
		if(data == "2" || data == "3" || data == "4"|| data == "5" || data == "6"|| data == "7"|| data == "10" || data == "11" || data == "12"|| data == "14"|| data == "30") {
			showhideintextbox(""+modname+"filterddcondvalue",""+modname+"filtercondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filtercondvalue").val("");
			//$("#"+modname+"filtercondvalue").timepicker("remove");
			$("#"+modname+"filtercondvalue").removeAttr('class');
			$("#"+modname+"filterddcondvalue").removeClass("validate[required]");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			fieldhidedisplay(data,""+modname+"filtercondvalue");
		} else if(data == "17" || data == "28") {
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]"); 
			$("#"+modname+"filtercondvalue").datepicker("disable");
			fieldnamebesdpicklistddvalue(fieldname,data,""+modname+"filterddcondvalue",moduleid);
		} else if(data == "18" || data == "19" || data == "21" || data == "22" || data == "25" || data == "26" || data == "29" ) {
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			$("#"+modname+"filtercondvalue").datepicker("disable");
			if(moduleid == 254){
				$mfid = $(this).val();
				leadconversionfieldnamebesdddvalue(fieldname,""+modname+"filterddcondvalue",moduleid,$mfid); 
			} else {
				fieldnamebesdddvalue(fieldname,""+modname+"filterddcondvalue",moduleid); 
			}
		} else if(data == "20") {
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			$("#"+modname+"filtercondvalue").datepicker("disable");
			empgroupdrodownset(""+modname+"filterddcondvalue");
		} else if(data == "13") {
			$("#"+modname+"filtercondvalue").datepicker("disable");
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			checkboxvalueget(""+modname+"filterddcondvalue");
		} else if(data == "8") {
			//for hide
			$("#"+modname+"filterdatecondvalue").datetimepicker("destroy");
			$("#"+modname+"filterddcondvaluedivhid").hide();
			$("#"+modname+"filtercondvaluedivhid").hide();
			$("#"+modname+"filterdatecondvaluedivhid").show();
			$("#"+modname+"filterdatecondvalue").val("");
			$("#"+modname+"filterddcondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			$("#"+modname+"filterdatecondvalue").addClass("validate[required]");				
			if(fieldlable == "Date of Birth" || fieldlable == "Start Date" || fieldlable == "End Date") {
				var dateformetdata = $("#"+modname+"filterdatecondvalue").attr("data-dateformater");
				$("#"+modname+"filterdatecondvalue").datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : "1947:c+100",
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != "") {
							$("#"+modname+"filterfinalviewcondvalue").val(checkdate);
						}
					},
					onClose: function () {
						$("#"+modname+"filterdatecondvalue").focus();
					}
				}).click(function() {
					$(this).datetimepicker("show");
				});
			} else {
				var dateformetdata = $("#"+modname+"filterdatecondvalue").attr("data-dateformater");
				$("#"+modname+"filterdatecondvalue").datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					maxDate:null,
					minDate: null,
					onSelect: function () {
						var checkdate = $(this).val();
						$("#"+modname+"filterfinalviewcondvalue").val(checkdate);
					},
					onClose: function () {
						$("#"+modname+"filterdatecondvalue").focus();
					}
				}).click(function() {
					$(this).datetimepicker("show");
				});
			}
		} else if(data == "9") {
			$("#"+modname+"filterdatecondvalue").datetimepicker("destroy");
			//for hide
			$("#"+modname+"filterddcondvaluedivhid").hide();
			$("#"+modname+"filtercondvaluedivhid").show();
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filtercondvalue").val("");
			$("#"+modname+"filterddcondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").addClass("validate[required,maxSize[100]]");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");	
			$("#"+modname+"filtercondvalue").timepicker({
				"step":15,
				"timeFormat": "H:i",
				"scrollDefaultNow": true,
			});
			var checkdate = $("#"+modname+"filtercondvalue").val();
			if(checkdate != ""){
				$("#"+modname+"filterfinalviewcondvalue").val(checkdate);
			}
		} else if(data == "27") {
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			$("#"+modname+"filtercondvalue").datepicker("disable");
			fieldnamebesdddvaluewithcond(fieldname,""+modname+"filterddcondvalue"); 
		} else if(data == "23") {
			showhideintextbox(""+modname+"filtercondvalue",""+modname+"filterddcondvalue");
			$("#"+modname+"filterdatecondvaluedivhid").hide();
			$("#"+modname+"filterddcondvalue").addClass("validate[required]");
			$("#"+modname+"filterddcondvalue").select2("val","");
			$("#"+modname+"filterdatecondvalue").removeClass("validate[required]");
			$("#"+modname+"filtercondvalue").removeClass("validate[required,maxSize[100]]");
			$("#"+modname+"filtercondvalue").datepicker("disable");
			attributepurityvalueget(fieldname,""+modname+"filterddcondvalue"); 
		} else {
			alertpopup("Please choose another field name");
		}
	});
	function showhideintextbox(hideid,showid){
		$("#"+hideid+"divhid").hide();
		$("#"+showid+"divhid").show();
	}
	{//Addform section overlay close
		$(".addsectionclose").click(function(){
			$("#groupsectionoverlay").removeClass("effectbox");
			$("#groupsectionoverlay").addClass("closed");
		});
	}	
	{
		/* code for left menu */
			leftpanelheight();
			$( window ).resize(function() {
				leftpanelheight();
			});
			 var deviceas = deviceinfo;
			 if(deviceas == "phone") {
				 $(".button-collapse").sideNav();
			 }
			$(".button-collapse").click(function(){
				$("#search").focus();
			});
			$('.dropdown-button').dropdown({
			      inDuration: 300,
			      outDuration: 225,
			      constrain_width: false, // Does not change width of dropdown to that of the activator
			      hover: false, // Activate on hover
			      gutter: 0, // Spacing from edge
			      belowOrigin: true, // Displays dropdown below the button
			      alignment: 'right' // Displays dropdown with edge aligned to the left of button
			    }
			  );
		/* code for left menu */
	}
	$(".viewaddiconclass").click(function(){
		var moduleid = $("#viewfieldids").val();
		$.ajax({
			url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
			cache:false,
			success: function(data) {  
				$("#viewcreationformdiv").empty();
				$("#viewcreationformdiv").append(data);
				$('.gridviewdivforsh').hide();
				$('#viewcreationformdiv').fadeIn(900);
				$("#viewreloadiconbtn").show();
				leftpanelheight();
				var vcselectid = $('#defviewfiledids').val();
				if(vcselectid == '') {
					var selectid = $('#viewcolname').val();
				} else {
					var selectid = vcselectid.split(',');
				}
				if(selectid === null){
					var selectid=' ';
				}
				viewcondvaluereset();
				$("#viewwoconsubmitbtn").show();
				$("#vieweditconsubmitbtn").hide();
				//clearform('viewcleardataform');
				$("#s2id_viewcolname").find('ul').removeClass('error');
				$("#viewname").focus();
				setTimeout(function() {
					$('#viewcolname').select2('val',selectid);
					$('#defviewfiledids').val(selectid);
					$('#viewactiontype').val('0');
					$('#viewdefaultopt,#viewpublicopt').val('No');
					$("#viewdefault,#viewfavourite").prop('checked',false);
					$("#viewheadername").text('View Creation');
				},400);
				viewcreateconditiongrid();
				loadcolumnname(moduleid);
				daterangefunction();
				//viewcreatefootergrid();
				setTimeout(function(){
					andorcondvalidationreset();
				},50);
				var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
				if(condcnt==0) {
					$("#viewandorconddivhid").hide();
				} else{
					$("#viewandorconddivhid").show();
				}
				formheight();
				//For Keyboard Shortcut Variables
				viewgridview = 0;
				addformview = 1;
			},
		});
	});
	//view edit icon
	$(".viewediticonclass").click(function(){
		viewcondvaluereset();
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		$.ajax({
			url:base_url+"Base/viewcreateuseridget",
			data: "userviewid="+userviewid,
			type: "POST",
			dataType :'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var moduleid = $("#viewfieldids").val();
					$.ajax({
						url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
						cache:false,
						success: function(viewdata) {
							$("#viewcreationformdiv").empty();
							$("#viewcreationformdiv").append(viewdata);
							$("#viewreloadiconbtn").hide();
							$('.gridviewdivforsh').hide();
							$('#viewcreationformdiv').fadeIn(900);
							$("#viewwoconsubmitbtn").hide();
							$("#vieweditconsubmitbtn").show();
							$('#viewactiontype').val('1');
							$("#viewname").focus();
							$("#viewheadername").text('View Update');
							leftpanelheight();
							loadcolumnname(moduleid);
							daterangefunction();
							viewcreateconditiongrid();
							setTimeout(function(){
								andorcondvalidationreset();
							},50);
							var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
							if(condcnt==0) {
								$("#viewandorconddivhid").hide();
							} else{
								$("#viewandorconddivhid").show();
							}
							formheight()
							if(userviewid!="") {
								viewcreationeditshowinfo(userviewid);
							}
							//For Keyboard Shortcut Variables
							addformupdate = 1;
							viewgridview = 0;
							addformview = 1;
						},
					});
				} else {
					alertpopup("This not a public view. So you cant edit this view");
				}
			}
		});
	});
	{//view delete
		$('.viewdeleteiconclass').click(function(){
			$.ajax({
				url: base_url + "Base/viewdeleteformfetch",
				success: function(data) {
					$("#viewdeleteoverlayconform").empty();
					$("#viewdeleteoverlayconform").append(data);
					$("#viewdeleteoverlayconform").show();
					$("#viewdeleteconyes").focus();
					$('#viewdeleteconyes').click(function(){
						var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
						$.ajax({
							url:base_url+"Base/gridinformationdelete?viewid="+userviewid,
							cache:false,
							success :function(msg) {
								var nmsg = $.trim(msg);
								if(nmsg == "Success") {
									viewcreatesuccfun("dontset");
									$("#viewdeleteoverlayconform").hide();
									$("#viewddoverlay").hide();
									alertpopup('View is deleted successfully.');
								} else if(nmsg == "Fail") {
									$("#viewdeleteoverlayconform").hide();
									$("#viewddoverlay").hide();
									alertpopup("It is default view of this application.it can't be deleted by any user");
								}
							},
						});
					});
					$('#viewdeleteconno').click(function(){	
						$("#viewdeleteoverlayconform").fadeOut();
					});
				},
			});
			
		});
	}
	{//view clone
		$('.viewcloneiconclass').click(function() {
			var moduleid = $("#viewfieldids").val();
			$.ajax({
				url: base_url + "Base/viewcreateandecitformfetch?moduleid="+moduleid,
				cache:false,
				success: function(viewdata) {  
					$("#viewcreationformdiv").empty();
					$("#viewcreationformdiv").append(viewdata);
					$("#viewreloadiconbtn").hide();
					$('.gridviewdivforsh').hide();
					$('#viewcreationformdiv').fadeIn(900);
					viewcondvaluereset();
					$("#viewwoconsubmitbtn").show();
					$("#vieweditconsubmitbtn").hide();
					$('#viewactiontype').val('1');
					$("#viewname").focus();
					$(".ftab").trigger('click');
					leftpanelheight();
					loadcolumnname(moduleid);
					daterangefunction();
					viewcreateconditiongrid();
					setTimeout(function(){
						andorcondvalidationreset();
					},50);
					var condcnt = $('#viewcreateconditiongrid .gridcontent div.data-content div').length;
					if(condcnt==0) {
						$("#viewandorconddivhid").hide();
					} else{
						$("#viewandorconddivhid").show();
					}
					formheight()
					var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
					if(userviewid!="") {
						viewcreationeditshowinfo(userviewid);
					}
					$('#viewdduserviewid').val('');
					//For Keyboard Shortcut Variables
					viewgridview = 0;
					addformview = 1;
				},				
			});
		});
	}
	{ //scripts present in grid menu header - GRA
		setTimeout(function(){
			var height=$(window).height();
			var wfheight = $('.footer-content').height();
			var newheight = wfheight + 100
			height = height-newheight;
			$('.tabletactionoverlay').attr('style', 'height: '+height+'px !important');
			$('#s2id_dynamicdddataview b , #s2id_conversationmoduleid b , #s2id_mainviewreportfolder b').removeClass("material-icons arrow_drop_up")
			$('#s2id_dynamicdddataview b , #s2id_conversationmoduleid b , #s2id_mainviewreportfolder b').addClass("material-icons details")
		},10);
		$( window ).resize(function() {
			var height=$(window).height();
			var wfheight = $('.footer-content').height();
			var newheight = wfheight + 100
			height = height-newheight;
			$('.tabletactionoverlay').attr('style', 'height: '+height+'px !important');
		});
		$('.tabletmorespan').click(function(){
				$('.tabletactionoverlay').toggle();
		});
		$('.tabletactionoverlay li').click(function(){
			$('.tabletactionoverlay').hide();
		});
		/* vardaan code*/
		{
		//Stone-Entry Add button Click
		$('.stoneaddon').click(function(){
			operationstone = $(this).data('id');
			$("#stoneentryaddvalidate").validationEngine('validate');
		});
		jQuery("#stoneentryaddvalidate").validationEngine({
		onSuccess: function() {
			var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
			var stocktypeid = $("#stocktypeid").find('option:selected').val();
			transactiontype = typeof transactiontype == 'undefined' ? '1' : transactiontype;
			stocktypeid = typeof stocktypeid == 'undefined' ? '1' : stocktypeid;
			var gridname ='stoneentrygrid';
			var coldatas = $('#gridcolnames').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(operationstone == 'ADD' || operationstone == '') {
				if(transactiontype == 11 && (stocktypeid == 11 || stocktypeid == 12 || stocktypeid == 13)) {
					alertpopupdouble('You cannot create new stone in Sales!');
				} else {
					formtogriddata_jewel(gridname,operationstone,'last','');
				}
			} else if(operationstone == 'UPDATE') {
				var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
				formtogriddata_jewel(gridname,operationstone,'',selectedrow);
				$('#stoneid').prop('disabled',false);
			}
			$('#stoneentrycalctypeid').prop('disabled', false);
			$('#purchaserate,#stonepieces,#caratweight,#stonegram').prop('readonly', false);
			var hideprodgridcol = ['stonegroupid','stoneid','stoneentrycalctypeid','totalcaratwt','totalnetwt'];
			gridfieldhide('stoneentrygrid',hideprodgridcol);
			jewelmoduleid = typeof jewelmoduleid == 'undefined' ? '1' : jewelmoduleid;
			if(jewelmoduleid == 50) {
				var lotid = $.trim($('#lotid').find('option:selected').val());
				lotid = typeof lotid == 'undefined' ? '1' : lotid;
				if(lotid > 1) {
					var lot_lottypeid = $.trim($('#lotid').find('option:selected').data('lottypeid'));
					if(lot_lottypeid == 2) {
						// Do not make manual entries
						$('#stoneid,#stoneentrycalctypeid').prop('disabled',true);
						$('#tagstoneentryedit').hide();
					} else {
						// Do only manual entries
						$('#stoneid,#stoneentrycalctypeid').prop('disabled',false);
						$('#tagstoneentryedit').show();
					}
				}
			} else {
				// Do only manual entries
				$('#stoneid,#stoneentrycalctypeid').prop('disabled',false);
				$('#tagstoneentryedit').show();
			}
			clearform('gridformclear');
			/* Data row select event */
			datarowselectevt();
			clearform('tagadditionalchargeclear_stone');
			operationstone = 'ADD';
			$('#tageditstone').hide();
			$('#tagaddstone,#tagstoneentrydelete').show();
			getstonegridsummary();
			setTimeout(function(){
		 		Materialize.updateTextFields();
		 	},100);
		},
		onFailure: function() {
			var dropdownid =[''];
			dropdownfailureerror(dropdownid);
		}
		});
		//Stone-Entry Edit-Form
		$("#tagstoneentryedit").click(function() {
			var transactiontype = $("#salestransactiontypeid").find('option:selected').val();
			transactiontype = typeof transactiontype == 'undefined' ? '1' : transactiontype;
			var stocktypeid = $("#stocktypeid").find('option:selected').val();
			stocktypeid = typeof stocktypeid == 'undefined' ? '1' : stocktypeid;
			var selectedrow = $('#stoneentrygrid div.gridcontent div.active').attr('id');
			if(selectedrow) {
				gridtoformdataset('stoneentrygrid',selectedrow);
				$('#tageditstone').show();
				$('#tagaddstone,#tagstoneentryedit,#tagstoneentrydelete').hide();
				Materialize.updateTextFields();
				operationstone = 'UPDATE';
				$('#stoneid').prop('disabled',true);
				if(transactiontype == 11) {
					$('#purchaserate,#basicrate').prop('readonly', false);
					if(stocktypeid != 19) {
						$('#stoneentrycalctypeid').prop('disabled',true);
						$('#purchaserate,#stonepieces,#caratweight,#stonegram').prop('readonly', true);
					} else if(stocktypeid == 19) {
						$('#stoneentrycalctypeid').prop('disabled',true);
						$('#basicrate,#stonepieces,#caratweight,#stonegram').prop('readonly', true);
					}
				}
			} else {
				alertpopup('Please Select The Row To Edit');
			}
		});
		//Stone-Entry Delete-Form
		$("#tagstoneentrydelete").click(function(){
			var datarowid = $('#stoneentrygrid div.gridcontent div.active').attr('id');
			if(datarowid) {		
				if(operationstone == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('stoneentrygrid',datarowid);
					getstonegridsummary();
				}
			} else {
				alertpopup(selectrowalert);
			}
		});
	}
	}
	{//dynamic view default set
		$('#dynamicdddataview').change(function(){
			$('#s2id_dynamicdddataview').removeClass('select2-container-active');
			var moduleviewid = $('#dynamicdddataview').find('option:selected').data('dynamicdddataviewid');
			var viewmoduleid = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/viewdefaultset",
				data: "datas=&viewid="+moduleviewid+"&moduleid="+viewmoduleid,
				type: "POST",
				async:false,
				cache:false,
				success: function(msg) {
				}
			});
		});
	}
	//picklist grouping change
	$(".picklistgroup").change(function() {
		var optvalue = $(this).val();
		var ddname=$(this).attr('data-dependency');
		var parentid=$(this).attr('id');
		var labelname=$(this).find('option:selected').data(''+parentid+'hidden');
		picklistgroupshow(parentid,optvalue,labelname,ddname);
	});
	{// code for select2dropdown tabbing problem on tab key based select
		adddataattrfrotabindextodd();
	}
    //for multimodules - -gowtham
    $('.filterview').click(function(){
		$('.dashboardview,.mininotesview').addClass('hidedisplay');
		$('.filterview').addClass('filertabbgcolor');
		$('.minidashboard,.mininotes').removeClass('filertabbgcolor');
		$('.filterview1').removeClass('hidedisplay');
		$(".tab-titlemini").removeClass('active');
		$("#fiteractive").addClass('active');
	});
    //for multi modules
    $('.mininotes').click(function(){
		var griddataid = $(this).data("griddataid");
		minifitertype = 'notes';
		var datarowid = $('#'+griddataid+' div.gridcontent div.active').attr('id');
		if (datarowid) {
			$(".tab-titlemini").removeClass('active');
			$(".notesmini").addClass('active');
			$('.mininotes').addClass('filertabbgcolor');
			$('.filterview,.minidashboard').removeClass('filertabbgcolor');
			//$("#processoverlay").fadeIn();
			var viewmoduleids = $("#viewfieldids").val();
			$('.mininotesview').removeClass('hidedisplay');
			$('.dashboardview,.filterview1').addClass('hidedisplay');
			miniloadconversationlog('noteswidget',viewmoduleids,datarowid); // notes tab
			//$("#processoverlay").fadeOut();
		} else {
			$(".notesmini").removeClass('active');
			$(".filtermini").addClass('active');
			alertpopup("Please select a row");
		}
	});
    //conversation log data fetch
	//loadconversationlog();
    $('#peronalsettingiconmain').click(function() {
    	window.location = base_url+'Personalsettings';
    });
    $('#logouticon').click(function() {
    	window.location = base_url+'Login/logout';
    });
    $('#submitaticket').click(function() {
    	window.open('https://aucventures.freshdesk.com/support/tickets/new');
    });
	$('#livechat').click(function() {
    	window.open('http://www.salesneuron.com/');
    });
    $('#support').click(function() {
    	window.open('https://aucventures.freshdesk.com/support/login');
    });
    $("#helpcontactus").click(function() {
    	window.open('http://www.salesneuron.com/contactus.php');
    });
    $('#feedback').click(function() {
    	//$("#feedbackoverlay").fadeIn();
    });
    $('#reqtraining').click(function() {
    	$("#requestatrainigoverlay").fadeIn();
    });
	{
	var notH = 1,
    $pop = $('#createshortcut').hover(function(){notH^=1;});

$(document).on('mousedown keydown', function( e ){
  if(notH||e.which==27) $pop.stop().fadeOut();
});
	}

    $('#reportbug').click(function() {
    	$(".navOpts").removeAttr('style');
    	$(".navOpts").trigger('click');
    });
    $('#requesttrainingclose').click(function() {
    	$("#requestatrainigoverlay").fadeOut();
    	$("#rname").val('');
    	$("#rmobile").val('');
    	$("#trainingfor").val('');
    	$("#rdate").val('');
    	$("#rtime").val('');
    	$("#rcomments").val('');
    });
    $('#requesttrainingsubmit').click(function() {
    	$("#requesttrainingoverlayvalidation").validationEngine('validate');
	});
    $("#requesttrainingoverlayvalidation").validationEngine( {
		onSuccess: function() {
			var formdata = $("#requestatrainigform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Base/requestatrainigmailsent",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					alertpopup('Request a training mail sent successfully. One of our sales team will get in touch with you shortly.');
					 $('#requesttrainingclose').trigger('click');
				},
			});
	    },
		onFailure: function() {
			alertpopup(validationalert);
		}
	});
    $('#upgradepalnclose').click(function() {
    	$("#upgradepalnoverlay").fadeOut();
    	$("#uname").val('');
    	$("#umobile").val('');
    	$("#ucomapnyname").val('');
    	$("#ucomments").val('');
    });
    $('#upgradepalnsubmit').click(function() {
    	$("#upgradepalnoverlayvalidation").validationEngine('validate');
	});
	$("#upgradepalnoverlayvalidation").validationEngine( {
		onSuccess: function() {
			var formdata = $("#upgradepalnform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			$.ajax({
				url: base_url + "Base/upgradeplanmailsent",
				data: "datas=" + datainformation,
				type: "POST",
				cache:false,
				success: function(msg) {
					alertpopup('Thank you for your interest. One of our sales team will get in touch with you shortly.');
					 $('#requesttrainingclose').trigger('click');
				},
			});
		},
		onFailure: function() {
			alertpopup(validationalert);
		}
	});
	$("#stockicon").click(function(){
		alertpopup('itemtag');
		$('#processoverlay').show();
		itemsalestagentry(50)
	});
});
//show hide text boxes
function showhideintextbox(hideid,showid){
	$("#"+hideid+"divhid").hide();
	$("#"+showid+"divhid").show();
}
//attribute value get
function attributepurityvalueget(fieldname,dropdownid){
	var moduleid = $("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/attributevalfetch?fieldid="+fieldname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					var attributeid = data[index]['Id'];
					dynamicattributedropdownvalfetch(attributeid,dropdownid);
				});
			}
		},
	});
}
//field name based drop down value - picklist
function fieldnamebesdpicklistddvalue(fieldid,uitype,ddname,moduleid){
	$.ajax({
		url: base_url + "Base/fieldnamebesdpicklistddvalue?fieldid="+fieldid+"&moduleid="+moduleid+"&uitype="+uitype,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
        	i=0;
        	k=0;
			$('#'+ddname+'').empty();
			$("#filterchechboxcondvaluedivhid").empty();
			$('#'+ddname+'').append($("<option></option>"));
			$checkboxdata = '';
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					i++;
				});
					$("#filtercondvaluedivhid").removeClass('hidedisply');
					$("#filterchechboxcondvaluedivhid").addClass('hidedisply');
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.attr("data-ddid",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
			}
			if(i > 5) {
				$('#'+ddname+'').trigger('change');	
			}
		},
	});
}
//field name based drop down value
function fieldnamebesdddvalue(fieldid,dropdownid,moduleid) {
	$.ajax({
		url: base_url + "Base/fieldnamebesdddvalue?fieldid="+fieldid+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				if(fieldid == 'sizemastername') {
					$.each(data, function(index) {
						$("#"+dropdownid+"")
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.attr("data-ddid",data[index]['datasid'])
						.text(data[index]['productname']+' - '+data[index]['dataname']));
					});
				} else {
					$.each(data, function(index) {
						$("#"+dropdownid+"")
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.attr("data-ddid",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
//lead conversion based field name value set
function leadconversionfieldnamebesdddvalue(fieldid,dropdownid,moduleid,$mfid){
	$.ajax({
		url: base_url + "Base/leadconversionfieldnamebesdddvalue?fieldid="+fieldid+"&moduleid="+moduleid+"&mfid="+$mfid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownid+"")
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.attr("data-ddid",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
function empgroupdrodownset(dropdownid) {
	$.ajax({
        url: base_url + "Base/viewemployeegroupsdata",
		dataType: "json",
		async: false,
		cache:false,
        success: function(data) {
			if(data['status']!='fail') {
				$('#'+dropdownid+'').empty();
				$("#"+dropdownid+"").append($("<option></option>"));
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['PId'];
					if(prev != cur ) {
						if(prev != " ") {
							$('#'+dropdownid+'').append($("</optgroup>"));
						}
						$('#'+dropdownid+'').append($("<optgroup  label='"+data[index]['PName']+"' class='"+data[index]['PName']+"'>"));
						prev = data[index]['PId'];
					}
					$("#"+dropdownid+" optgroup[label='"+data[index]['PName']+"']")
					.append($("<option></option>")
					.attr("class",data[index]['CId']+'pclass')
					.attr("value",data[index]['CName'])
					.attr("data-ddid",data[index]['CId'])
					.attr("data-dataidhidden",data[index]['PId'])
					.text(data[index]['CName']));
				});
			}
        },
    });
}
//check box value get
function checkboxvalueget(ddname){
	$.ajax({
		url: base_url + "Base/checkboxvalueget",
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$('#'+ddname+'').empty();
			$('#'+ddname+'').append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					if(data[index]['uitype'] != '15' || data[index]['uitype'] != '16') {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					}
				});
			}
			$('#'+ddname+'').trigger('change');			
		},
	});
}
//field name value assign with condition
function fieldnamebesdddvaluewithcond(fieldname,dropdownid) {
	var moduleid = 	$("#viewcreatemoduleid").val();
	$.ajax({
		url: base_url + "Base/fieldnamebesdddvaluewithcond?fieldid="+fieldname+"&moduleid="+moduleid,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			$("#"+dropdownid+"").empty();
			$("#"+dropdownid+"").append($("<option></option>"));
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$("#"+dropdownid+"")
					.append($("<option></option>")
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}
			$("#"+dropdownid+"").trigger('change');			
		},
	});
}
{// Load print template file - Ramesh
	function pdfpreviewtemplateload(viewfieldids) {
		$('#pdftemplateprview,#printtemplatepreview').empty();
		$('#pdftemplateprview,#printtemplatepreview').append($("<option value=''></option>"));
		$.ajax({
			url:base_url+'Base/pdfpreviewfetchdddataviewddval?dataname=printtemplatesname&dataid=printtemplatesid&datatab=printtemplates&viewfieldids='+viewfieldids+"&column=stockdate"+"&columnsecond=printtypeid"+"&columnthird=printername",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#pdftemplateprview,#printtemplatepreview')
						.append($("<option></option>")
						.attr("data-foldernameidhidden",data[index]['dataname'])
						.attr("data-stockdate",data[index]['stockdate'])
						.attr("data-printtypeid",data[index]['printtypeid'])
						.attr("data-printername",data[index]['printername'])
						.attr("data-moduleid",viewfieldids)
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}
				$('#pdftemplateprview,#printtemplatepreview').trigger('change');
			},
		});
	}
}

{// Close button press
	// @ closebutton[0] - close icon id
	// @ closebutton[1] - To show div id
	// @ closebutton[2] - To Hide div id
    function addclose(closebutton) {
	   $('#'+closebutton[0]+'').click(function(){
            if( globalclosetrigger == "1") {
              $('#'+closebutton[2]+'').hide();
              $('#'+closebutton[1]+'').fadeIn(1500);
            } else { 
            	alertpopupformclose("Do you wish to close form ?");
                $("#alertsfcloseyes").focus();
                $("#alertsfcloseyes").click(function(){
                    var clsform =$("#alertsfcloseyes").val();                 
					if(clsform == "Yes") {
                        $("#alertscloseyn").fadeOut("fast");
                        $('#'+closebutton[2]+'').hide();
                        $('#'+closebutton[1]+'').fadeIn(1000);
						resetFields();
						$('.btmerrmsg').remove();
						$(".ftab").trigger('click');
						//For Keyboard Shortcut Variables
						addformupdate = 0;
						viewgridview = 1;
						addformview = 0;
						//$(".off-canvas-wrap").css({'padding-left':'65px'});
						//$("#slide-out").show(); 
                    }
                });
            }
        });
    }
    //Trigger Close Form
    function triggerclose(triggerclosearray) {
       $('#'+triggerclosearray[0]+'').trigger("click");
    }
}
{// Reset all The fields 
	 function resetFields(element) {
		$('.cleardataform').children().find('input[type=text],input[type=number],input[type=date],input[type=email],input[type=url],input[type=password],textarea').each(function(){
		   $(this).val('');
		});
		$('.cleardataform').children().find('input[type=checkbox]').each(function(){
		   $(this).val('');
		   $(this).next('input').val('');
		   $(this).attr('checked',false);
		});
		$('.cleardataform').children().find('.chzn-select').each(function(){
		   $(this).select2('val','');
		   this.selectedIndex = 0;
		});
		$('.cleardataform').children().find('.select2-container ul.select2-choices li.select2-search-choice').each(function(){
			$(this).remove();
		});
		$('.cleardataform').children().find('.uploadcontainerstyle img').remove();
		$('.cleardataform').children().find('.uploadcontainerstyle i').remove();
		$(".error").removeClass('error');
		$('.btmerrmsg').remove();
		$('.cleardataform').children().find('.froala-view p').remove();
		$('.cleardataform').validationEngine('hideAll');
	}
}
{// Responsive grid class functions - old concept[need to remove once jqgrid concept removed]
	//grid responsive setting Documents View grid
	function formwithgridclass(gridid) {
		$("#"+gridid+"").jqGrid('bindKeys');
		$("#gview_"+gridid+" .label"+gridid+"").addClass("docscfrowheaderht12");
		$("#gview_"+gridid+" .content"+gridid+"").addClass("docscfcontentareaht73");
		$("#gbox_"+gridid+" .pager"+gridid+"").addClass("docscfpaginationht12"); 
	}
	//grid responsive setting Reports view Grid
	function repotsformwithgridclass(gridid) {
		$("#"+gridid+"").jqGrid('bindKeys');
		$("#gview_"+gridid+" .label"+gridid+"").addClass("reportsrowheaderht12");
		$("#gview_"+gridid+" .content"+gridid+"").addClass("reportscontentareaht73");
		$("#gbox_"+gridid+" .pager"+gridid+"").addClass("reportspaginationht12"); 
	}
	//grid responsive setting Condition view grid
	function viewconditiongridclass(gridid) {
		$("#"+gridid+"").jqGrid('bindKeys');
		$("#gview_"+gridid+"").addClass("vctotalht88");
		$("#gview_"+gridid+" .content"+gridid+"").addClass("vccfcontentareaht73");
		$("#gbox_"+gridid+" .pager"+gridid+"").addClass("vccfpaginationht12"); 
	}
}
{// Drop down failure Add Error Border
	function dropdownfailureerror(dropdownid) {
		var choseninfo = "s2id_";
		for(i = 1;i<=dropdownid[0];i++) {
			var checkinfo = $('#'+dropdownid[i]+'').val();
			if(checkinfo == "") {
				var classchk = $("#"+dropdownid[i]+"[class*='validate']");
				var dropdowninfo = choseninfo + dropdownid[i];
				if(classchk[0]!==undefined) {
					$('#'+dropdowninfo+' .select2-choice').addClass('error');
				}
			}
		}
	}
}
{// Text box functions - Ramesh
	function textboxsetvalue(textboxname,datanames,data,dropdowns) {
		for( i=1;i<=textboxname[0];i++) { 
			var value = data[''+datanames[i]+''];
			if(dropdowns.indexOf(textboxname[i])== -1) {
				$('#'+textboxname[i]+'').val(value);
			} else {
				$('#'+textboxname[i]+'').select2("val",value);
				$('#'+textboxname[i]+' option').prop('disabled',false).removeClass("ddhidedisplay");
			}
		}
	}
	//form fields value set new function
	function textboxsetvaluenew(textboxname,datanames,data,dropdowns) {
		$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
		for( i=0;i<(textboxname.length);i++) {
			if($('#'+textboxname[i]+'').hasClass('empdd')) {
				var ddval = data['employeetypeid']+':'+data[''+datanames[i]+''];
				$('#'+textboxname[i]+'ddid').select2("val",ddval);
				$('#'+textboxname[i]+'').val(data[''+datanames[i]+'']);
			} else if(typeof $('#'+textboxname[i]+'').attr('data-elementtype') === 'undefined') {
				var value = data[''+datanames[i]+''];
				var type = $('#'+textboxname[i]+'').getType();
				if(type == "select") {
					if( $('#s2id_'+textboxname[i]+'').hasClass('select2-container-multi') ) {
						if(value){
							var mulval = value.split(',');
							mulval = mulval.filter(function(n){ return n !='' }); // filter the array for null vaules
						} else{
							var mulval = '';
						}
						$('#'+textboxname[i]+'').select2("val",mulval);
					} else {
						$('#'+textboxname[i]+'').select2("val",value);
					}
				} else if(type == "text" || type == "number" || type == "date" || type == "email" || type == "url") {
					$('#'+textboxname[i]+'').val(value);
				} else if(type == "hidden") {
					$('#'+textboxname[i]+'').val(value);
					if(value == 'Yes') {
						$('#'+textboxname[i]+'cboxid').prop('checked',true);
					} else {
						$('#'+textboxname[i]+'cboxid').prop('checked',false);
					}
				} else if(type == "password") {
				} else if(type == "textarea") {
					$('#'+textboxname[i]+'').val(value);
				}
			} else {
				var value = data[''+datanames[i]+''];
				var mulvalue = value.split(',');
				$('#'+textboxname[i]+'').select2('val',mulvalue);
				$('#'+textboxname[i]+'').trigger('change');
			}
		}
	}
	//Attachment value set
	function attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data) {
		var fieldname = {};
		fieldname = attachfldname.split(',');
		var colname = {};
		colname = attachcolname.split(',');
		var uitype = {};
		uitype = attachuitype.split(',');
		var fieldid = {};
		fieldids = attachfieldid.split(',');
		for(i=0;i<(attachfldname.length);i++) {
			if(uitype[i]=='15') {
				$("#"+fieldname[i]+"attachdisplay").empty();
				var fnames = 'fname_'+fieldids[i];
				var fsizes = 'fsize_'+fieldids[i];
				var ftypes = 'ftype_'+fieldids[i];
				var fpaths = 'fpath_'+fieldids[i];
				var fromids = 'upfrom_'+fieldids[i];
				fnames = [];
				fsizes = [];
				ftypes = [];
				fpaths = [];
				fromids = [];
				var fieldvals = {};
				fieldvals = data[colname[i]].split(',');
				var size = {};
				size = data[colname[i]+"_size"].split(',');
				var type = {};
				type = data[colname[i]+"_type"].split(',');
				var path = {};
				path = data[colname[i]+"_path"].split(',');
				var fromid = {};
				fromid = data[colname[i]+"_fromid"].split(',');
				
				for(j=0;j<(fieldvals.length);j++) {
					fnames.push(fieldvals[j]);
					fsizes.push(size[j]);
					ftypes.push(type[j]);
					fpaths.push(path[j]);
					fromids.push(fromid[j]);
					
					$("#"+fieldname[i]).val(fnames);
					$("#"+fieldname[i]+"_size").val(fsizes);
					$("#"+fieldname[i]+"_type").val(ftypes);
					$("#"+fieldname[i]+"_path").val(fpaths);
					$("#"+fieldname[i]+"_fromid").val(fromids);
					
					var count = fnames.length;
					var fdelid = count-1;
					if(count <= 10) {
						var spantag = $("<span style='word-wrap:break-word;'>"+fieldvals[j]+"</span><i data-imgid="+fdelid+" class='material-icons documentsdownloadclsbtn "+fieldname[i]+"docclsbtn' dat-fieldid="+fieldids[i]+" data-fieldname="+fieldname[i]+">close</i><br/>");
						if(fieldvals[j]!='') {
							spantag.appendTo("#"+fieldname[i]+"attachdisplay");
						}
					}
					//delete files
					$("."+fieldname[i]+"docclsbtn").click(function() {
						var id = $(this).data("imgid");
						var fid = $(this).data("fieldid");
						var fname = $(this).data("fieldname");
						var fldnames = fname.split(',');
						var count = fldnames.length;
						for(var k=0;k<count;k++) {
							if( k == id) {
								fldnames.splice($.inArray(fldnames[id],fldnames),1);
								setTimeout(function(){
									$("#"+fname+"").val(fldnames);
								},100);
							}
						}
						$(this).prev("span").remove();
						$(this).next("br").remove();
						$(this).remove();
					});
				}
			} else if(uitype[i]=='16') { 
				/* file mapping for image component*/
				$("#"+fieldname[i]).val(data[colname[i]]);
				$("#"+fieldname[i]+"_size").val(data[colname[i]+"_size"]);
				$("#"+fieldname[i]+"_type").val(data[colname[i]+"_type"]);
				$("#"+fieldname[i]+"_path").val(data[colname[i]+"_path"]);
				$("#"+fieldname[i]+"_fromid").val(data[colname[i]+"_fromid"]);
				/* display image */
				$("#"+fieldname[i]+"attachdisplay").empty();
				var img = $("<img style='height:100%;width:100%;'>");
				if(data[colname[i]+"_path"]) {
					var fpath = data[colname[i]+"_fromid"]=="1" ?  base_url+data[colname[i]+"_path"] : data[colname[i]+"_path"];
					img.attr("src",fpath);
					img.appendTo("#"+fieldname[i]+"attachdisplay");
					if(fpath!='') {
						$("#"+fieldname[i]+"attachdisplay").append("<i class='icon24 material-icons documentslogodownloadclsbtn "+fieldname[i]+"docclsbtn' data-txtid='"+fieldname[i]+"'>close</i>");
					}
				}
				
				/* delete files */
				$("."+fieldname[i]+"docclsbtn").click(function() {
					var id = $(this).data('txtid');
					$("#"+id+"attachdisplay").empty();
					$("#"+id+"").val("");
					$("#"+id+"_size").val("");
					$("#"+id+"_type").val("");
					$("#"+id+"_path").val("");
					$("#"+id+"_fromid").val("");
					$(this).remove();
				});
			}
		}
	}
	//form elements default value set function
	function elementdefvalueset(elementname) {
		var textboxname = {};
		textboxname = elementname.split(',');
		$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
		for( i=0;i<(textboxname.length);i++) {
			if(typeof $('#'+textboxname[i]+'').attr('elementtype') === 'undefined') {
				var value = $('#'+textboxname[i]+'').data('defvalattr');
				var type = $('#'+textboxname[i]+'').getType();
				if(type == "select") {
					$('#'+textboxname[i]+'').select2("val",value);
				} else if(type == "text") {
					if($('#'+textboxname[i]+'').attr('data-elementtype') == 'tag'){
						if(value){
							var mulval = value.split(',');
							$('#'+textboxname[i]+'').select2("val",mulval);
						}
						$('#'+textboxname[i]+'').val(value);
					} else{
						$('#'+textboxname[i]+'').val(value);
					}
				} else if(type == "hidden") {
					$('#'+textboxname[i]+'').val(value);
					if(value == 'Yes') {
						$('#'+textboxname[i]+'cboxid').prop('checked',true);
					} else if(value == 'No'){
						$('#'+textboxname[i]+'cboxid').prop('checked',false);
					}
				} else if(type == "password") {
					$('#'+textboxname[i]+'').val(value);
				} else if(type == "textarea") {
					$('#'+textboxname[i]+'').val(value);
				}
			} else {
				if(typeof $('#'+textboxname[i]+'').data('defvalattr') === 'undefined') {
					var value = $('#'+textboxname[i]+'').data('defvalattr');
					var ddsdatas = ddndatas.split(',');
					$('#'+textboxname[i]+'').select2('val',ddsdatas);
					$('#'+textboxname[i]+'').trigger('change');
				}
			}
		}
	}
}
{// Main div area width set
	function maindivwidth() {
		var a = screen.width;
		var b = 1024;
		var c = b - a;
		var d = 80;
		var x = 128;
		var y = c/x;
		var e = d - y;
		var newheight = (a * e)/100;
		var percent = parseFloat((newheight/a)*100) + parseFloat(10);
		$('.maindiv').css({width:''+percent+'%'});
	}
}
{// Clear form 
	function clearform(cleardivid) {
		//setTimeout(function(){
			$('.'+cleardivid+'').validationEngine('hideAll');
			$('.'+cleardivid+' .select2-choice').removeClass('error');
			$('.'+cleardivid+' :input').removeClass('error');
			$(':input','.'+cleardivid)
			.not(':button, :submit, :reset, :hidden')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');
			$('.'+cleardivid+' input[type=hidden]').val('');
			$('.'+cleardivid+' textarea').val('');
			$('.'+cleardivid+' .chzn-select').select2('val','');
			$('.'+cleardivid+' .captionddse').select2('val','');
			$('.'+cleardivid+' .commenttxtarea').val('');
			$('.'+cleardivid+' .overlay .cctxt').val('');
		//},50);		
	}
}
{// Alert pop-up Basic 
	function alertpopup(txtmsg)	{
		$(".alertinputstyle").text(txtmsg);
		$('#alerts').fadeIn('fast');
		$("#alertsclose").focus();
		if(ovrpopuphideshow == 1) {
			$('#alerts').fadeOut(2000);
		}
	}
	function alertpopupdouble(txtmsg) {
		$(".alertinputstyledouble").text(txtmsg);
		$('#alertsdouble').fadeIn('fast');
		$("#alertsdoubleclose").focus();
		if(ovrpopuphideshow == 1) {
			$('#alertsdouble').fadeOut(2000);
		}
	}
}
{// Alert pop-up For Base Delete
	function combainedmoduledeletealert(idname) { 
		$('#basedeleteoverlayforcommodule').fadeIn();
		$('.commodyescls').attr('id',idname);
		$(".commodyescls").focus();
	}
}
{// Alert pop-up For Base Delete with dynamic messages //Ramesh
	function combinedmodalertdynamictxt(idname,content) {
		$('#basedeleteoverlayforcommodule').fadeIn();
		$('.commodyescls').attr('id',idname);
		$('.commodyescls').focus();
		$('.basedelalerttxt').text(content);
	}
}
{// Create In-line search tool bar -[need to remove once jqgrid concept removed]
	function createistb(gridid) {
		for( i=1;i<=gridid[0] ;i++) {
			jQuery("#"+gridid[i]+"").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
			var grididname=gridid[i];
			jQuery("#"+grididname+"").jqGrid('setGridParam', {gridComplete:function(){
				if(typeof(grididname) != "undefined" && grididname !== null) {
					var nm = jQuery("#"+grididname+"").getGridParam("postData").filters;
					if(typeof(nm) != "undefined" && nm !== null) {
						var n = jQuery("#"+grididname+"").jqGrid('getGridParam', 'records');
						if(n == 0){												  
							alertpopup("No Relevant Records Available");
						}
					}
				}
			}});
		}
	}
}
{// Alert pop-up form close
	function alertpopupformclose(txtmsg) {
		$(".alertinputstyle").text(txtmsg);
		$('#alertscloseyn').fadeIn('fast');		
	}
}
{// Clear in-line search
	function clearinlinesrchandrgrid(gridnamereload) {
		clearform('ui-search-toolbar');
		$("#"+gridnamereload+"").jqGrid('setGridParam', { search: false, postData: { "filters": ""} }).trigger("reloadGrid");
	}
}
{// Generation of col model for dynamic data viewing grid - [need to remove once jqgrid concept removed]
	function columnsData(Dataname,Dataindex,Datatype,Dataviewtype) {
		var dateformat=(($("#compdateformat").length)?$('#compdateformat').val():'m/d/Y');
		var colwidth = [100,100,100,100,100,100,100,100,100,100];
		var formater = "";
		var str = "[";
		for (var i = 0; i < Dataname.length; i++) {
			var dataview = "";
			var vtype = Dataviewtype[i];
			if(vtype == "0") { dataview = 'hidden: true ,width: 1,'; }
			if (Dataname[i] == "Id") {
				if(Datatype[i] == "8") {
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+"formatter:'date',formatoptions: {srcformat: 'ISO8601Long',newformat: '"+dateformat+" h:i:s'},}";
				} else {
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', hidden: true,width:1}";
				}
			} else {
				if(Datatype[i] == "8") {
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+"formatter:'date',formatoptions: {srcformat: 'ISO8601Long',newformat: '"+dateformat+" h:i:s'}}";
				} else {
					str = str + "{name:'" + Dataname[i] + "', index:'" + Dataindex[i] + "', "+dataview+" resizable:false}";
				}
			}
			if (i != Dataname.length - 1) {
				str = str + ",";
			}
		}
		str = str + "]";
		return str;
	}
}
{// Drop Down Value Set - General Drop down Values - [used in all modules @gowtham]
	function dropdownvalsetview(ddname,dataattrname,dataname,dataid,datatable,whfield,whvalue) {
		$('#'+ddname+'').empty();
		$.ajax({
			url:base_url+'Base/fetchdddataviewddval?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {   
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-"+dataattrname,data[index]['datasid'])
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					});
				}	
			},             
		});
	}
}
{// Drop down values set for view - UI Type - [used in view js file]
	function dropdownvalsetuitypeviewcols(ddname,dataattrname,dataname,dataid,datatable,uitype,indexname,whfield,whvalue) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url:base_url+'Base/fetchdduitypedataviewddval?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+"&uitype="+uitype+"&indexname="+indexname,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {   
				if((data.fail) == 'FAILED') {
				} else {   
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-indexname",data[index]['indexname'])
						.attr("data-uitypeid",data[index]['uitype'])
						.attr("data-"+dataattrname,data[index]['datasid'])
						.attr("value",data[index]['dataname'])
						.text(data[index]['dataname']));
					});
				}
			},             
		});
	}
}
{// Function used to set the options in to the drop down ,set id with value and names with options
	function dynamicloaddropdown(ddname,dataattrname,dataname,dataid,datatable,whfield,whvalue) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url:base_url+'Base/fetchdddataviewddval?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {   
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-"+dataattrname,data[index]['datasid'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
			},             
		});
	}
}
{// For Add Form Show Hide
	// @ fadeoutscr - Id of div to hide 
	// @ faseinscr - Id of div to show
	function addslideup(fadeoutscr,faseinscr) {
		
		$('#'+fadeoutscr+'').hide();
		$('#'+faseinscr+'').fadeIn(1,"jswing").css('display','block');
		//setTimeout(function(){
			$('#'+faseinscr+'').css('display','block');
			
		//},1000);
		//TabGroup Dropdown More 
		//autocollapse(faseinscr);
		$( window ).resize(function() {
			if($('.desktoptabgroup.tabgroupstyle').is(':visible')){
			//Dropdown More 
			 //autocollapse(faseinscr);
			}else{//Do Nothing
			}
		});
		$(".sidebaricons").removeClass('active');
		$(".sidebaricons.ftab").addClass('active');
	}
}
{// Form field first focus
	function firstfieldfocus() {
		var firstInput = $('form').find('input[type=text],input[type=password],input[type=radio],input[type=checkbox],textarea,select').filter(':visible:first');
		if (firstInput != null) {
		   firstInput.focus();
		   $('.ui-timepicker-wrapper').hide();
		   $('.ui-datepicker').hide();
		   $('.sectionpanel').animate({scrollTop:0},'slow');
		   $('.addformcontainer').animate({scrollTop:0},'slow');
		}
	}
}
{// Set the Checked value -- Checkbox -- [used in tax/product module]
	function setthecheckedvalue(checkarray) {
		for(var j=0;j<checkarray.length;j++) {
			var value=$('#'+checkarray[j]+'').val();
			if (value == 'Yes') {
				$("#"+checkarray[j]+"checked").removeClass('hidedisplay');
				$("#"+checkarray[j]+"unchecked").addClass('hidedisplay');	
			} else if (value == 'No') {	
				$("#"+checkarray[j]+"unchecked").removeClass('hidedisplay');
				$("#"+checkarray[j]+"checked").addClass('hidedisplay');
			}
		}
	}
}
{// Check the variable 
	function checkVariable(id) {
		var val=$('#'+id+'').val();
		if(val == null || val == ''|| val ==' '){
			return false;
		} else {
			return true;
		}
	}
}
{// Check the value
	function checkValue(val) {
		if(val == null || val == ''|| val ==' '){
			return false;
		} else {
			return true;
		}
	}
}
{// Get and Set current date 
	function getcurrentsytemdate(idvalue){
		$("#"+idvalue+"").datetimepicker('setDate', (new Date()) );
	}
}
{ // get current system date with added n days
	function getcurrentsytemdatewithndays(idvalue,n){
    var t = new Date();
	t.setDate(t.getDate() + n); 
    $("#"+idvalue+"").datetimepicker('setDate',(t));
}
}
{// For alert tabbing
	function focusloopfun(){ 
		$('.lasttab').on('focus', function() { 
			$('.ffield').focus();
			$('.ffieldd').select2('focus');
		});
		$('.firsttab').on('focus', function() {
			$('.flloop').focus();
		}); 
	}
}
{// Sets company setting
	function setdefaultproperty() {
		$.ajax({
			url:base_url+"Base/getdefaultcurrency", 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$('#currencyid').select2('val',data.currency).trigger('change');
			}
		});		
	}
}
{// For summary show hide icons
	 function showhideiconsfun(casevalue,formidname){
			switch(casevalue) { 
			case 'summryclick':
				$('#'+formidname+' :input').attr('disabled', true);
				$('.summaryicon').attr('disabled', false);
				$('.updatebtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').removeClass('hidedisplay');
				break;
			case 'editfromsummryclick':
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$('#'+formidname+' :input').attr('disabled', false);
				break;
			case 'addclick':
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				$('#'+formidname+' :input').attr('disabled', false);
				break;
			case 'editclick':
				$('#'+formidname+' :input').attr('disabled', false);
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				break;
			default:
		}
	 }
}
{// Inner Grid Width issue - Dynamic [Non base modules]
	function innergridwidthsetbasedonscreen(innergridname){
		$("#"+innergridname+"").bind("jqGridAfterLoadComplete", function () {
			var $this = $(this), iCol, iRow, rows, row, cm, colWidth,
			$cells = $this.find(">tbody>tr>td"),
			$colHeaders = $(this.grid.hDiv).find(">.ui-jqgrid-hbox>.ui-jqgrid-htable>thead>.ui-jqgrid-labels>.ui-th-column>div"),
			colModel = $this.jqGrid("getGridParam", "colModel"),
			n = $.isArray(colModel) ? colModel.length : 0,
			idColHeadPrexif = "jqgh_" + this.id + "_";

			$cells.wrapInner("<span class='mywrapping'></span>");
			$colHeaders.wrapInner("<span class='mywrapping'></span>");
		
			var currenscreenwidth = $("#"+innergridname+"width").width(); //alert(currenscreenwidth);
			var datatotalwidth = 0;
			var columnpro = [];
			var columnwidth = [];
			for (iCol = 0; iCol < n; iCol++) {
				cm = colModel[iCol];
				colWidth = $("#" + idColHeadPrexif + $.jgrid.jqID(cm.name) + ">.mywrapping").outerWidth() + 30; // 25px for sorting icons
				for (iRow = 0, rows = this.rows; iRow < rows.length; iRow++) {
					row = rows[iRow];
					if ($(row).hasClass("jqgrow")) {
						colWidth = Math.max(colWidth, $(row.cells[iCol]).find(".mywrapping").outerWidth()+10);
					}
				}
				datatotalwidth += colWidth;
				columnpro.push(iCol);
				columnwidth.push(colWidth);
				$this.jqGrid("setColWidth", iCol, colWidth);
			}
			var eachcollength=0;
			if(datatotalwidth < currenscreenwidth){ 
				var remainingwidth = currenscreenwidth - datatotalwidth;
				eachcollength = remainingwidth / (n-1);
			}
			for (var h = 1; h <= columnpro.length; h++) {
				$this.jqGrid("setColWidth", columnpro[h], columnwidth[h]+eachcollength);
			}
			// To View the Content Area 
			$('#gview_'+innergridname+'').css('opacity','1');
		});
	}
}
{// Inner Grid Width issue local - Static [Non BAse modules]
	function localinnergridwidthsetbasedonscreen(innergridname) {
		var localdatatotalwidth = 0;
		var eachdatawidth = 0;
		var currenscreenwidth = $("#"+innergridname+"width").width(); 
		var colnam = jQuery("#"+innergridname+"").jqGrid('getGridParam','colNames');
		var exactcount = colnam.length;
		var colmodelwidth = jQuery("#"+innergridname+"").jqGrid('getGridParam','colModel');
		for(i=1; i < exactcount; i++){
			eachdatawidth += colmodelwidth[i].width; 
		}
		var colmodelname = jQuery("#"+innergridname+"").jqGrid('getGridParam','colModel');
		var hiddenfields = [];
		for(i=1; i < exactcount; i++){ 
			if(colmodelname[i].hidden === false){
				hiddenfields.push(colmodelname[i].name); 
			}
		}
		if(eachdatawidth < currenscreenwidth){
			//jQuery("#"+innergridname+"").jqGrid('setGridWidth',currenscreenwidth);
			for(i=0; i < hiddenfields.length; i++){ 
				var eachdatawidth = currenscreenwidth/hiddenfields.length;
				jQuery("#"+innergridname+"").jqGrid("setColWidth", hiddenfields[i], eachdatawidth);
			}
			// To View the Content Area 
			$('#gview_'+innergridname+'').css('opacity','1');
		} else {
			var extraremainingwidth = eachdatawidth - currenscreenwidth;
			//jQuery("#"+innergridname+"").jqGrid('setGridWidth',currenscreenwidth+extraremainingwidth);
			for(i=0; i < hiddenfields.length; i++) {
				var eachdatawidth = (currenscreenwidth+extraremainingwidth)/hiddenfields.length;
				jQuery("#"+innergridname+"").jqGrid("setColWidth", hiddenfields[i], eachdatawidth);
			}
			// To View the Content Area 
			$('#gview_'+innergridname+'').css('opacity','1');
		}
	}
}
{// Check cancel status-validate 
	function checkcancelstatus(moduleid,moduletable,id) {
		var status='';
		$.ajax({
			url: base_url + "Base/checkcancelstatus?primarydataid="+id+"&moduleid="+moduleid+"&moduletable="+moduletable,
			async:false,
			cache:false,
			success: function(msg) {
				status =msg;
			},
		});
		return status;
	}
}

{// Validate the module email
	function validateemail(id,table,field) {
		var status='';
		$.ajax({
			url: base_url + "Base/checkvalidemail?id="+id+"&table="+table+"&field="+field,
			async:false,
			cache:false,
			success: function(msg)  {
				status =msg;
			},
		});
		return status;
	}
}
{// Retrieves the email id of the given module
	function getvalidemailid(id,table,field,maintable,mainprimary) {
		var emaildata=[];
		$.ajax({
			url: base_url + "Base/getvalidemaildetail?id="+id+"&table="+table+"&field="+field+"&maintable="+maintable+"&mainprimary="+mainprimary,
			async:false,
			dataType:'json',
			cache:false,
			success: function(data)  {
				emaildata =data;
			},
		});
		return emaildata;
	}
}
{// Retrieves the account id from given module-datarowid,'opportunity','accountid' - Gowtham
	function getaccountid(id,table,field) {
		var accountid='';
		$.ajax({
			url: base_url + "Base/getaccountid?id="+id+"&table="+table+"&field="+field,
			async:false,
			dataType:'json',
			cache:false,
			success: function(data)  {
				accountid =data.accountid;
			},
		});
		return accountid;
	}
}
{// Auto number generation  - Ramesh
	function randomnumbergenerate(fname,fnameid,fnamemoduleid,fnametable) {
		var fldname = $('#'+fname+'').val();
		var fldnameid = $('#'+fnameid+'').val();
		var fldmodule = $('#'+fnamemoduleid+'').val();
		var fldtable = $('#'+fnametable+'').val();
		var filednames = fldname.split(',');
		var filedtab = fldtable.split(',');
		var filednamesid = fldnameid.split(',');
		var filedmoduleid = fldmodule.split(',');
		for(var h=0;h<filednames.length;h++) {
			if(filednames[h]!='') {
				$.ajax({
					url: base_url + "Base/randomnumgenerate",
					async:false,
					data: "datas=&mid="+filedmoduleid[h]+"&tabname="+filedtab[h]+"&fname="+filednames[h]+"&fnameid="+filednamesid[h],
					type: "POST",
					dataType:'json',
					cache:false,
					success: function(data) {
						$('#'+filednames[h]+'').val(data);
					},
				});
			}
		}
	}
}
//notification content feed
function feedcontent(e, a, i, t) {
    notificationaction = ["added", "updated", "deleted", "active", "cancelled", "lost", "convert", "stopped", "import", "duplicate", "export", "massdelete", "massupdate", "conversation", "invite", "replied"], notificationicons = ["icon24 icon24-add", "fa fa-pencil-square-o", "fa fa-trash-o", "fa fa-asterisk", "fa fa-times-circle", "fa fa-thumbs-o-down", "fa fa-hand-o-right", "fa fa-power-off", "fa fa-upload", "fa fa-search-plus", "fa fa-download", "fa fa-database", "fa fa-database", "fa fa-comment-o", "fa fa-star", "fa fa-comments-o"], notificationlink = ["1", "1", "2", "1", "5", "5", "6", "5", "7", "0", "7", "0", "0", "3", "4", "3"], $("#" + a).empty(), $("#" + i).empty();
    var o = base_url + "Employee";
    $("#" + a).append('<div class="large-12 columns scrollbarclass notisectionpanel" id="' + i + '" style="height:23rem;overflow-x: hidden;"></div><div class="divider large-12 columns"></div><div class="large-12 columns submitkeyboard sectionalertbuttonarea" style="text-align: center"><input name="notificationdivoverlayclose" id="notificationdivoverlayclose" class="addkeyboard alertbtnno" value="Close" tabindex="157" type="button"></div>');
    for (var d = 0; e > d; d++) {
    	var displaydata= t[d].message;
		$("#" + i).append('<li><div>'+t[d].employeetwoname+'</div><i class="notification-date">'+t[d].duration+'</i><p>'+displaydata+'</p><img class="notification-image" alt="" src="'+t[d].imagepath+'"></li>');	
    }
    if(t.length <= 0) {
    	$("#" + i).append('<div style="padding-top:100px;padding-left:100px;padding-right:100px"><div style="text-align: center;padding-top: 21px;color: #000000;background-color: #ffebed;border-radius: 5px;border: 1px solid #d7a7b3;line-height: rem;padding-bottom: 10px;" class="large-12 columns dsbtaskblock tasknamestyle">Currently there is no new notification </div></div>');
    }
    $("#feedsliveidclass").scroll(function() {
        $(this)[0].scrollHeight - $(this).scrollTop() == $(this).outerHeight() && loadfeedend()
    }), 
    $("#clearnotification").click(function() {
        $("#feedsliveidclass").empty(), $("#feedsliveidclass").append('<div style="padding-top:100px;padding-left:100px;padding-right:100px"><div style="text-align:center;padding-top:21px;color:#000000;background-color: #ffebed;border-radius: 5px;border: 1px solid #d7a7b3;line-height: 1rem;" class="large-12 columns dsbtaskblock tasknamestyle">Currently there is no new notification </div></div>')
    }),
    $(".notiredirecturl").click(function(){
		var redirecturl = $(this).attr('data-redirecturl');
		var datarowid = $(this).attr('data-datarowid');
		sessionStorage.setItem("datarowid",datarowid);
		window.location = redirecturl;	
	}),
	$('#notificationdivoverlayclose').click(function(){
		$('#notificationdivoverlay').hide();
		$("#notificationdivmobileoverlay").parent().removeClass("move-right");
	});
}
//here feeding the content to div
function loadfeedend() {
    var e = $(".feedchildnode:last").attr("data-notificationlogid"),
        a = "my",
        i = base_url + "Employee";
    $.ajax({
        url: base_url + "Home/getnotificationdata",
        data: {
            datatype: a,
            lastnode: e
        },
        type: "POST",
        dataType: "json",
        async: !1,
        cache: !1,
        success: function(e) {
            for (var a = e.length, t = 0; a > t; t++) {
            	var displaydata= e[t].message;
                $("#feedsliveidclass").append('<li><div>'+ e[t].employeetwoname+'</div><div class="notification-date">'+ e[t].duration+'</div><p>'+displaydata+'</p><img class="notification-image" alt="" src="'+ e[t].imagepath+'"><div class="large-12 columns feedchildnode" data-notificationlogid="' + e[t].logid + '"> &nbsp;</div></li>')
            }
            $(".notiredirecturl").click(function(){
        		var redirecturl = $(this).attr('data-redirecturl');
        		var datarowid = $(this).attr('data-datarowid');
        		sessionStorage.setItem("datarowid",datarowid);
        		window.location = redirecturl;	
        	})
        }
    })
}
{//Set zero if the given field is empty else silent
	function setzero(arrvalue) {
		var a=arrvalue.length;
		for(var m=0;m < a;m++) {
			var fvalue= $('#'+arrvalue[m]+'').val();
			if(fvalue == '' || fvalue == null || fvalue == " ") {
				$('#'+arrvalue[m]+'').val(0);
			}
		}
	}
	/*
	* Module Add on the redirect-from the widget add actions
	*/
	function add_fromredirect(recognize,currentmoduleid) {
		var mrecognize = sessionStorage.getItem(recognize);
		var widgetid = sessionStorage.getItem("widgetid"); 
		var moduleid = sessionStorage.getItem("moduleid");				
		var rowid = sessionStorage.getItem("rowid");
		if(mrecognize == 'formhome') {
			sessionStorage.removeItem(recognize);
			sessionStorage.removeItem("widgetid");
			sessionStorage.removeItem("moduleid");
			sessionStorage.removeItem("rowid");	
			setTimeout(function() {	
				if(widgetid > 0 && moduleid > 1 && rowid > 0){
					$("#addicon").trigger('click');
					addwidgetdataset(rowid,moduleid,widgetid,currentmoduleid);
				}
			},1000);	
		}
	}
	/*
	* Retrieves the data for the  current based based on widgetmodule data
	*/
	function addwidgetdataset(rowid,moduleid,widgetid,currentmoduleid){
		$.ajax({
			url:base_url+"Base/getwidgetrelateddata?rowid="+rowid+'&moduleid='+moduleid+'&widgetid='+widgetid, 
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {			
				if(data != '' || data != null) {
					if(data.mstatus == 'Success'){
						var field = data.field;
						var fielduitype = data.fielduitype;
						var wdata = data.wdata;											
						for(var index =0;index < field.length;index++){							
							var datafield = field[index];							
							if( fielduitype[index] == '17' || fielduitype[index] == '18' || fielduitype[index] == '19' || fielduitype[index] == '20' || fielduitype[index] == '23' || fielduitype[index] == '29'){
								$('#'+field[index]+'').select2('val',wdata[datafield]).trigger('change');
							}
							else{
								$('#'+field[index]+'').val(wdata[datafield]);
							}
						}							
					}
				}	
				//special case
				if(currentmoduleid == 205){ //activities
					$('#actvitymoduleid').select2('val',moduleid).trigger('change');
					$('#actvitycommonid').select2('val',rowid).trigger('change');
				}
				else if(currentmoduleid == 207){ //documents					
					$('#documentsmoduleid').select2('val',moduleid).trigger('change');
					$('#documentscommonid').select2('val',rowid).trigger('change');
				}
				else if(currentmoduleid == 206){ //task
					$('#taskmoduleid').select2('val',moduleid).trigger('change');
					$('#taskcommonid').select2('val',rowid).trigger('change');
				}
			}
		});
	}
}
//click to call function
function clicktocallfunction(mobilenum) {
	$.ajax({
		url:base_url+"Base/clicktocallfunction", 
		data: "mobilenumber=" + mobilenum,
		type: "POST",
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
				$("#c2cmobileoverlay").hide();
			} else {
				if(data == 'OK') {
					$("#c2cmobileoverlay").hide();
					alertpopup('Call Submitted Successfully and will get u soon..');
				} else {
					$("#c2cmobileoverlay").hide();
					alertpopup('Error with click to call process. try again later.');
				}
			}	
		},
	});
}
//sms and click to call mobile number overlay generation
function mobilenumberload(mobilenumber,ddname) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+"Base/mobilenumberddload", 
		data: "mobilenumber=" + mobilenumber,
		type: "POST",
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['dataname']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#'+ddname+'').append(newddappend);
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
//sms and click to call module dd load
function moduledropdownload(moduleid,recordid,ddname) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+"Base/moduleddload", 
		data: "moduleid="+moduleid+"&recordid="+recordid,
		type: "POST",
		dataType : 'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option value = '" +data[m]['moduleid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#'+ddname+'').append(newddappend);
			}	
		},
	});
}
//mobile number data fetch based on modules.
function mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,ddname) {
	$.ajax({
		url:base_url+"Base/mobilenumberfetchbasedonmodule?moduleid="+moduleid+"&linkmoduleid="+linkmoduleid+"&recordid="+recordid,
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			mobilenumber = [];
			if(data != 'No mobile number') {
				for(var i=0;i<data.length;i++) {
					if(data[i] != '') {
						mobilenumber.push(data[i]);
					}
				}
				if(mobilenumber.length >= 1) {
					mobilenumberload(mobilenumber,ddname);
				} else {
					$("#smsmobilenumid,#callmobilenum").select2('val','');
					$("#smsmobilenumid,#callmobilenum").empty();
				}
			} else {
				$("#smsmobilenumid,#callmobilenum").select2('val','');
				$("#smsmobilenumid,#callmobilenum").empty();
			}
		},
	});
}
{//unique field validation
	function uniquefieldvalidate(field) {
		var value = encodeURIComponent(String(field.val()));
		var txtboxid = field.attr('id');
		var tblcolname = field.data('colmname');
		var tblname = field.data('tablname');
		var primaryid = $('#primarydataid').val();
		var nmsg = "";
		if(value!='') {
			$.ajax({
				url:base_url+"Base/uniquefieldvalcheck",
				data:"data=&v="+value+"&t="+tblname+"&c="+tblcolname,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != "") {
					if(primaryid != nmsg) {
						return "Value already exists!";
					}
				} else {
					return "Value already exists!";
				}
			}
		}
	}
}
{//fetch update data set - support overlu update
	function fetchupdatedata() {
		$.ajax({
			url:base_url+"Base/fetchupdatedata",
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				for(var i=0;i<data.length;i++){
					$(".support-updatepan").append('<div class="support-update"><div class="update-icon"><span class="fa fa-phone fa-2x" style="line-height:31px;padding:0 1px;"></span></div><div class="update-content"><ul style="list-style-type:none; margin:0.2rem;"><li class="header">' + a[e] + '</li><li > Have any sales related queries & require immediate assistance</li></ul></div></div>');
				}
			},
		});
	}
}
//notification content get
function getnotificationcount() {
    $.ajax({
        url: base_url + "Home/getnotificationcount",
        type: "POST",
        dataType: "json",
        async: !1,
        cache: !1,
        success: function(e) {
        	//e.count > 0 ? ($(".notificationbadg").text(e.count), $(".notificationbadg").addClass("belliconcolor"), $("#bellnotificationicon").addClass("icon24-notifications_active"), $("#bellnotificationicon").removeClass("icon24-notifications_none")): "logout" == e.count ? window.location = base_url + "Login/logout" : ($(".notificationbadg").text(), $("#bellnotificationicon").addClass("icon24-notifications_none"), $(".notificationbadg").removeClass("belliconcolor"), $("#bellnotificationicon").removeClass("icon24-notifications_active"))
        	if(e.count > 0) {
        		$("#bellnotification").empty();
        		$("#bellnotification").append('<span class="notificationbadg belliconcolor">0</span>');
        	} else {
        		$("#bellnotification").empty();
        		$("#bellnotification").append('<i title="Filter" id="bellnotificationicon" class="material-icons" >notifications_none</i>');
        	}
        }
    })
}
function datarowselectevt() {
	$('div.gridcontent div.data-rows').click(function() {
		$('div.gridcontent div.data-rows,div.gridcontent div.data-rows ul').removeClass('active');
		$(this).addClass('active');
		var attrid = $(this).attr('id');
		$('div.gridcontent div #'+attrid+' ul').addClass('active');
	});
	//Mobile Grid UI settings
	//additionalmobilegriduisettings();
}
//data row header group in audit log / conversation
function datarowheadergroup(gridname) {
	$('.rowgroupsheader').click(function(){
		var val = $(this).text();
		var grpid = $(this).data('groupid');
		if(val=='-') {
			$(this).text('+');
			$("#"+gridname+" div.gridcontent .rowgroup"+grpid).attr('style', 'display: none');
		} else if(val=='+') {
			$(this).text('-');
			$("#"+gridname+" div.gridcontent .rowgroup"+grpid).attr('style', 'display: block');
		}
	});
}
//sort order work in all modules
function sortordertypereset(classname,headcolid,sortord) {
	if(headcolid!='0') {
		$('#'+headcolid+'.'+classname).addClass('datasort');
		if(sortord=='ASC') {
			$('.'+classname+'.datasort i').removeClass('icon24-sort');
			$('.'+classname+'.datasort i').removeClass('icon24-sort-desc');
			$('.'+classname+'.datasort i').addClass('icon24-sort-asc');
			$('.'+classname+'.datasort').data('sortorder','DESC');
		} else if(sortord=='DESC') {
			$('.'+classname+'.datasort i').removeClass('icon24-sort');
			$('.'+classname+'.datasort i').removeClass('icon24-sort-asc');
			$('.'+classname+'.datasort i').addClass('icon24-sort-desc');
			$('.'+classname+'.datasort').data('sortorder','ASC');
		}
	}
}
//column resize in all modules
function columnresize(gridid) {
	gridid = typeof gridid == 'undefined' ? '' : gridid;
	var griddivid = '';
	if(gridid == '') {
		griddivid = '';
	} else {
		griddivid = '#'+gridid;
	}
	$(griddivid+' .gridcontent').scroll(function () {
	   var distance = parseInt($(''+griddivid+' .gridcontent').scrollLeft());
	   $(griddivid+' .header-caption').css("left","-"+distance+"px");
    });
   $(griddivid+' .headerresizeclass')
	   .resizable({
			handles:"e",
			maxHeight:35,
			minHeight:35,
			minWidth:100,
			resize: function(e, ui) {
				var liclass=$(this).attr("data-class");
				var liwidth=$(this).width();
				var windowwidth = $(griddivid+'width').width();
				var lowerwidth = $(griddivid+' .header-caption').width();
				$('.'+liclass).css("width",(liwidth+40));
				$(this).data("width",(liwidth+40));
				var curheadercolid = $(this).attr('id');
				var litotwidth=10;
				var j=0;
				setTimeout(function(){
					$(griddivid+' .header-caption ul li').each( function() {
						litotwidth += $(this).data('width');
					   j++;
					});
					j--;
					if(windowwidth>lowerwidth) {
						var gridwidth = lowerwidth<windowwidth ? windowwidth-lowerwidth : 1;
						var setcolwidth = gridwidth/j;
						$(griddivid+' .header-caption ul li').each( function(i,e) {
							var colid = $(e).attr('id');
							var colclass = $(e).attr("data-class");
							if(curheadercolid!=colid) {
								var oldwidth = $(griddivid+' .header-caption ul li#'+colid).data("width");
								$(griddivid+' .header-caption ul li#'+colid).css("width",( oldwidth+parseInt(setcolwidth) ));
								$('.'+colclass).css("width",( oldwidth+parseInt(setcolwidth) ));
								$(griddivid+' .header-caption ul li#'+colid).data("width",( oldwidth+parseInt(setcolwidth) ));
							}
						});
					}
					var litotwidth=10;
					$(griddivid+' .header-caption ul li').each( function() {
					   litotwidth += $(this).data('width');
					});
					$(griddivid+' .header-caption').css("width",litotwidth);
					$(griddivid+' .gridcontent .data-content').css("width",litotwidth);
				},1);
			},
			stop: function(e, ui) {
				var liwidth=$(this).width();
				var colwidth = (liwidth+19);
				var colid = $(this).data('viewcolumnid');
				var viewid = $(this).data('viewid');
				var pos = $(this).data('position');
				var windowwidth = $(griddivid+'width').width();
				var lowerwidth = $(griddivid+' .header-caption').width();
				var litotwidth=40;
				$.ajax({
					url:base_url+"Base/headercolumnresizeset?colid="+colid+"&colwidth="+colwidth+"&viewid="+viewid+"&position="+pos,
					contentType:'application/json; charset=utf-8',
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
					}
				});
			}
	   });
}
//main grid resize
function maingridresizeheightset(gridname) {
	var wheight = $(window).height();
	var wfheight = $('.gridfooter').height();
	var gridheight =93; 
	var gridcontentheight =134+80; 
	var onwheight = wheight-gridheight;
	var nwheight = wheight-gridcontentheight;
	 var deviceas = deviceinfo;
	/* if(deviceas=="phone") {
	   // For mobile
		var gridheight =99+wfheight; 
		var gridcontentheight =99+wfheight; 
		var onwheightmobile = wheight-gridheight;
		var nwheightmobile = wheight-gridcontentheight;
		$('#'+gridname+'').css('height',onwheightmobile+'px');
		$('#'+gridname+' .gridcontent').css('height',nwheightmobile+'px');
	}else{ */
		//$('#'+gridname+'').css('height',onwheight+'px');
		$('#'+gridname+' .gridcontent').css('height',nwheight+'px');
	//}
}
//inner grid resize
function innergridresizeheightset(gridname) {
	var wheight = $(window).height();
	var nwheight = wheight-232;
	var onwheight = wheight-156;
	var onwheightmbl = wheight-148;
	var frmheight = wheight-73;
	 var deviceas = deviceinfo;
	/* if(deviceas=="phone") {
		$('#'+gridname+'').css('height',onwheightmbl+'px');
		$('#'+gridname+' .gridcontent').css('height',onwheightmbl+'px');
		$('.addformcontainer').css('height',frmheight+'px');
	}else{ */
		$('#'+gridname+'').css('height',onwheight+'px');
		$('#'+gridname+' .gridcontent').css('height',nwheight+'px');
		$('.addformcontainer').css('height',frmheight+'px');
	//}
}
//form to grid 
function formtogriddata(gridname,method,position,datarowid) {
	method = typeof method == 'undefined' ? 'ADD' : method;
	var dataval = "";
	var align = '';
	var opndivcontent = '';
	var content = '';
	var clsdivcontent = '';	
	/* if(deviceinfo != 'phone'){ */ 
		var n = $('#'+gridname+' .gridcontent div.data-content div').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.data-content').empty();
		} else{
			n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
			n = parseInt(n);
		} 
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<ul class="inline-list">';
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   var txtboxid = $(e).attr('id');
		   var uitype = $('#'+txtboxid).data('uitype');
		   var txtboxname = $('#'+txtboxid).data('fieldname');
		   var txtwidth = $('#'+txtboxid).data('width');
		   var txtclass = $('#'+txtboxid).data('class');
		   var viewtype = $('#'+txtboxid).data('viewtype');
		   var viewstyle = (viewtype=='0') ? ' display:none':'';
		   if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
			   vals=[];
			   datalength = $('#'+txtboxname+' option:selected').length;
			   if(datalength > 1){
				   $("#"+txtboxname+" > option:selected").each(function (index, el, list) {
					   vals.push(el.getAttribute("data-"+txtboxname+"hidden"));
		            });
				   dataval = vals;
			   }else{
				   dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
			   }
			   datavalue = dataval === undefined ? '' : dataval;
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
		   } else {
			   dataval = $('#'+txtboxname+'').val();
			   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
			   content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
		   }
		});
		content+='</ul>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.data-content:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.data-content').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).append(content);
			}
		}
	/* }
	else{
		var n = $('#'+gridname+' .gridcontent div.bottom-wrap').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.bottom-wrap').empty();
		}
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<div class="bottom-wrap">';
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
			   var txtboxid = $(e).attr('id');
			   var uitype = $('#'+txtboxid).data('uitype');
			   var txtboxname = $('#'+txtboxid).data('fieldname');
			   var txtboxlabel = $('#'+txtboxid).data('label');
			   var txtwidth = $('#'+txtboxid).data('width');
			   var txtclass = $('#'+txtboxid).data('class');
			   var viewtype = $('#'+txtboxid).data('viewtype');
			   var viewstyle = (viewtype=='0') ? ' display:none':'';
			   if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
				   vals=[];
				   datalength = $('#'+txtboxname+' option:selected').length;
				   if(datalength > 1){
					   $("#"+txtboxname+" > option:selected").each(function (index, el, list) {
						   vals.push(el.getAttribute("data-"+txtboxname+"hidden"));
			            });
					   dataval = vals;
				   }else{
					   dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
				   }
				   datavalue = dataval === undefined ? '' : dataval;
				   if(datavalue != '' || datavalue != '0'){
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+datavalue+'</span>';
				  }
			   } else {
				   dataval = $('#'+txtboxname+'').val();
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }	  
			});
		content+='</div>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.wrappercontent:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.wrappercontent').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.wrappercontent div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.wrappercontent div#'+datarowid).append(content);
			}
		}
	} */	
}
//grid to form
function gridtoformdataset(gridname,datarowid) {
	//if(deviceinfo != 'phone'){
		var value="";
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
			   var txtboxid = $(e).attr('id');
			   var uitype = $('#'+txtboxid).data('uitype');
			   var txtboxname = $('#'+txtboxid).data('fieldname');
			   var txtclass = $('#'+txtboxid).data('class');
			   if( uitype != '17' || uitype != '18' || uitype != '19' || uitype != '20' || uitype != '23' || uitype != '25' || uitype != '26' || uitype != '27' || uitype != '28' || uitype != '29') {
				   value = $('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+txtclass).text();
				   if(value.indexOf(',') != -1){
					      var valsplit = value.split(',');
						   $('#'+txtboxname).select2("val",valsplit);
					}else{
						   if($('#'+txtboxname).prop('type') == 'select-one') {
							   if(txtboxname == 'stoneentrycalctypeid') {
									$('#'+txtboxname).select2("val",value).trigger('change');
							   } else {
								 $('#'+txtboxname).select2("val",value);  
							   }
							}else {
								$('#'+txtboxname).val(value);
							} 
					}
				  }
		});
	/* }
	else{
		var value="";
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
			   var txtboxid = $(e).attr('id');
			   var uitype = $('#'+txtboxid).data('uitype');
			   var txtboxname = $('#'+txtboxid).data('fieldname');
			   var txtclass = $('#'+txtboxid).data('class');
			   if( uitype != '17' || uitype != '18' || uitype != '19' || uitype != '20' || uitype != '23' || uitype != '25' || uitype != '26' || uitype != '27' || uitype != '28' || uitype != '29') {
				   value = $('#'+gridname+' .gridcontent div.wrappercontent div#'+datarowid+' .'+txtclass).text();
				   if($('#'+txtboxname).prop('type') == 'select-one') {
						$('#'+txtboxname).select2("val",value);
					} else {
						$('#'+txtboxname).val(value);
					}
			   }
		});
	} */
	
}
//load inner grid data
function loadinlinegriddata(gridname,datas,type,checkbox) {
	checkbox = typeof checkbox == 'undefined' ? '' : checkbox;
	var opndivcontent='';
	var content='';
	var clsdivcontent='';
	
	if(type=='json') {
		//if(deviceinfo != 'phone'){
			var n = $('#'+gridname+' .gridcontent div.data-content div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.data-content').empty();
			}
			$.each(datas,function(key,value){
				opndivcontent = '<div class="data-rows" id="'+value['id']+'">';
				content = '<ul class="inline-list">';
				var j = 0;
				var m = 0;
				$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
					var txtboxid = $(e).attr('id');
					var uitype = $('#'+txtboxid).data('uitype');
					var txtboxname = $('#'+txtboxid).data('fieldname');
					var txtwidth = $('#'+txtboxid).data('width');
					var txtclass = $('#'+txtboxid).data('class');
					var viewtype = $('#'+txtboxid).data('viewtype');
					var viewstyle = (viewtype=='0') ? ' display:none':'';
					if(checkbox == 'checkbox' && m==0) {
						content +='<li style="width:'+txtwidth+'px;'+viewstyle+'"><input type="checkbox" id="rowchkbox'+value['id']+'" name="rowchkbox'+value['id']+'" data-rowid="'+value['id']+'" class="'+gridname+'_rowchkboxclass '+txtclass+' rowcheckbox " value="'+value['id']+'" /></li>';
						m=1;
					} else {
						if(txtboxname == 'tagimage') {
							dataval = '';
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+' innergridimagedisplay" id="innergridimagedisplay" style="width:'+txtwidth+'px;'+viewstyle+'"><i class="material-icons">broken_image</i></li>';
						} else {
							dataval = value['cell'][j];
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
						}
						j++;
					}
					
				});
				content+='</ul>';
				clsdivcontent='</div>';
				var finalcontent = opndivcontent+content+clsdivcontent;
				$('#'+gridname+' .gridcontent div.data-content').append(finalcontent);
			});		
		/* }
		else{
			var n = $('#'+gridname+' .gridcontent div.wrappercontent div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.wrappercontent').empty();
			}
			$.each(datas,function(key,value){
				opndivcontent = '<div class="data-rows" id="'+value['id']+'">';
				content = '<div class="bottom-wrap">';
				var j = 0;
				var m = 0;
				$('#'+gridname+' div.header-caption span').map(function(i,e) {
					var txtboxid = $(e).attr('id');
					var uitype = $('#'+txtboxid).data('uitype');
					var txtboxname = $('#'+txtboxid).data('fieldname');
					var txtboxlabel = $('#'+txtboxid).data('label');
					//var txtwidth = $('#'+txtboxid).data('width');
					var txtclass = $('#'+txtboxid).data('class');
					var viewtype = $('#'+txtboxid).data('viewtype');
					var viewstyle = (viewtype=='0') ? ' display:none':'';
					if(checkbox == 'checkbox' && m==0) {
						content +='<span style="'+ viewstyle+'"><input type="checkbox" id="rowchkbox'+value['id']+'" name="rowchkbox'+value['id']+'" data-rowid="'+value['id']+'" class="'+txtclass+' rowcheckbox" value="'+value['id']+'" /></span>';
						m=1;
					} else {
						dataval = value['cell'][j];
						if(dataval != ''&& dataval != 0 && dataval != null){
							align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
							content+='<span class="content" style="'+ viewstyle+'">'+txtboxlabel+':'+'<span class="'+align+' '+txtclass+'" style="'+ viewstyle+'">'+dataval+'</span></span>';							
						}
						j++;
					}
				});
				content+='</div>';
				clsdivcontent='</div>';
				var finalcontent = opndivcontent+content+clsdivcontent;
				$('#'+gridname+' .gridcontent div.wrappercontent').append(finalcontent);
			});
		} */
	}
}
//inline grid data
function addinlinegriddata(gridname,id,datas,type) {
	var opndivcontent='';
	var content='';
	var clsdivcontent='';
	if(type=='json') {
		//if(deviceinfo != 'phone'){
			var n = $('#'+gridname+' .gridcontent div.data-content div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.data-content').empty();
			}
			opndivcontent = '<div class="data-rows" id="'+id+'">';
			content = '<ul class="inline-list">';
			var j = 0;
			$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
				var dataval='';
				var txtboxid = $(e).attr('id');
				var uitype = $('#'+txtboxid).data('uitype');
				var txtboxname = $('#'+txtboxid).data('fieldname');
				var txtwidth = $('#'+txtboxid).data('width');
				var txtclass = $('#'+txtboxid).data('class');
				var viewtype = $('#'+txtboxid).data('viewtype');
				var viewstyle = (viewtype=='0') ? ' display:none':'';
				if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
					dataval = datas[txtboxname+'name'];
				} else {
					dataval = datas[txtboxname];
				}
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
				j++;
			});
			content+='</ul>';
			clsdivcontent='</div>';
			var finalcontent = opndivcontent+content+clsdivcontent;
			$('#'+gridname+' .gridcontent div.data-content').append(finalcontent);		
		/* }else{
			var n = $('#'+gridname+' .gridcontent div.wrappercontent div').length;
			if(n=='0' || n=='') {
				$('#'+gridname+' .gridcontent div.wrappercontent').empty();
			}
			opndivcontent = '<div class="data-rows" id="'+id+'">';
			content = '<div class="bottom-wrap">';
			var j = 0;
			$('#'+gridname+' div.header-caption span').map(function(i,e) {
				var dataval='';
				var txtboxid = $(e).attr('id');
				var uitype = $('#'+txtboxid).data('uitype');
				var txtboxname = $('#'+txtboxid).data('fieldname');
				var txtboxlabel = $('#'+txtboxid).data('label');
				var txtwidth = $('#'+txtboxid).data('width');
				var txtclass = $('#'+txtboxid).data('class');
				var viewtype = $('#'+txtboxid).data('viewtype');
				var viewstyle = (viewtype=='0') ? ' display:none':'';
				if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
					dataval = datas[txtboxname+'name'];
				} else {
					dataval = datas[txtboxname];
				}
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<span class="content" style="'+ viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				j++;
			});
			content+='</div>';
			clsdivcontent='</div>';
			var finalcontent = opndivcontent+content+clsdivcontent;
			$('#'+gridname+' .gridcontent div.wrappercontent').append(finalcontent);		
		} */
	}
}
//hide any field in grid
function gridfieldhide(gridname,hidearray) {
	//if(deviceinfo != 'phone'){
		
		for (var i in hidearray) {
			try {
				$("#"+gridname+" .header-caption ul ."+hidearray[i]+"-class").attr('style', 'display: none');
				$("#"+gridname+" .header-caption ul ."+hidearray[i]+"-class").data('viewtype', '0');
				$("#"+gridname+" .gridcontent ul ."+hidearray[i]+"-class").attr('style', 'display: none');
			} catch(err) {
			}
		}
		var gridwidth=0;
		var j=0;
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
			   var txtboxid = $(e).attr('id');
			   var txtwidth = $('#'+txtboxid).data('width');
			   var viwtype = $('#'+txtboxid).data('viewtype');
			   if(viwtype=='1') {
				   gridwidth=parseInt(gridwidth)+parseInt(txtwidth);
				   j++;
			   }
		});
		var oldsize = $("#"+gridname).width();
		gridwidth = oldsize>gridwidth ? oldsize : gridwidth;
		var setwidth = gridwidth/j;
		gridwidth = parseInt(gridwidth)+parseInt(setwidth);
		if(gridwidth < 1200){
			gridwidth = 1270
		}
		//col width
		var colwidth = gridwidth/j;
		if(gridwidth>0) {//grid-view
			$("#"+gridname+" .header-caption").attr('style','width:'+gridwidth+'px');
			$("#"+gridname+" .gridcontent div.data-content").attr('style','width:'+gridwidth+'px');
		}
	/* }
	else{
		for (var i in hidearray) {
			try {
				$("#"+gridname+" .header-caption ."+hidearray[i]+"-class").data('viewtype', '0');
				$("#"+gridname+" .bottom-wrap ."+hidearray[i]+"-class").attr('style', 'display: none');
			} catch(err) {
			}
		}
	} */
	
}
//display the hiiden fileds in grid
function gridfieldshow(gridname,showarray) {
	for (var i in showarray) {
		try {
			$("#"+gridname+" .header-caption ul ."+showarray[i]+"-class").attr('style', 'display: block');
			$("#"+gridname+" .header-caption ul ."+showarray[i]+"-class").data('viewtype', '1');
			$("#"+gridname+" .gridcontent ul ."+showarray[i]+"-class").attr('style', 'display: block');
		} catch(err) {
		}
	}
	var gridwidth=0;
	var j=0;
	$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   var txtboxid = $(e).attr('id');
		   var txtwidth = $('#'+txtboxid).data('width');
		   var viwtype = $('#'+txtboxid).data('viewtype');
		   if(viwtype=='1') {
			   gridwidth=parseInt(gridwidth)+parseInt(txtwidth);
			   j++;
		   }
	});
	var oldsize = $("#"+gridname).width();
	gridwidth = oldsize>gridwidth ? oldsize : gridwidth;
	if(gridwidth>0) {
		$("#"+gridname+" .header-caption").attr('style','width:'+gridwidth+'px');
		$("#"+gridname+" .gridcontent div.data-content").attr('style','width:'+gridwidth+'px');
	}
}
//get the valuu of the selected column value
function getgridcolvalue(gridname,rowid,colname,option) {
	var val='0.00';
	var datarowid = '';
	if(option=='sum') {
		//if(deviceinfo !='phone') {
			$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				val = parseFloat(val) + parseFloat($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
			});
		/* } else {
			$('#'+gridname+' .gridcontent div.wrappercontent .data-rows').map(function(i,e) {
				datarowid = $(e).attr('id');
				val = parseFloat(val) + parseFloat($('#'+gridname+' div.wrappercontent div#'+datarowid+' .'+colname+'-class').text());
			});
		} */
		
	} else if(option!='sum' && rowid!='') {
		//if(deviceinfo !='phone'){
			val = $('#'+gridname+' .gridcontent div#'+rowid+' ul .'+colname+'-class').text();
		/* }else{
			val = $('#'+gridname+' .gridcontent div.wrappercontent div#'+rowid+' div.bottom-wrap span.'+colname+'-class').text();
			var newval = val.split(' : ');
			var val = newval[1];
		} */
	}
	return val;
}
//get the valuu of the selected column value with some contdiion
function getgridcolvaluewithcontition(gridname,rowid,colname,option,contitonid,conditionvalue) {
	var val='0.00';
	var datarowid = '';
	if(option=='sum') {
		//if(deviceinfo !='phone') {
			$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				/* if($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+contitonid+'-class').text() == 'Cash') {
					val = parseFloat(val) + parseFloat($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
				} else  */if($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+contitonid+'-class').text() == conditionvalue) {
					val = parseFloat(val) + parseFloat($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
				} 
			});
		
	}else if(option=='count') {
		//if(deviceinfo !='phone') {
			
			$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				if($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+contitonid+'-class').text() == conditionvalue) {
					val = parseFloat(val) + 1;
				} 
			});
	}
	return val;
}
//get the row data of the sected row
function getgridrowsdata(gridname) {
	//if(deviceinfo != 'phone'){
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			//console.log(fieldname);
			$(fieldname).each(function(i,e) {
				value='';
				
				//console.log('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul .'+fieldclass[i]);
				value = $('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul .'+fieldclass[i]).text();
				value = (value !== 'null') ? value : '';
				//console.log(value);
				datas[''+e+''] = new Array();	
				datas[''+e+''] = value;
				//console.log(datas);
			});
			dataarr.push(datas);
			j++;
		});
		return dataarr;
	/* }else{
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.wrappercontent div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			$(fieldname).each(function(i,e) {
				value='';
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' .'+fieldclass[i]).text();
				value = (value !== 'null') ? value : '';
				datas[''+e+''] = new Array();			
				datas[''+e+''] = value;
			});
			dataarr.push(datas);
			j++;
		});		
		return dataarr;
	} */	
}
//get the row data of checkbox checked sected row
function getgridcheckedrowsdata(gridname) {
	var txtboxid = '';
	var uitype = '';
	var txtboxname = '';
	var txtclass = '';
	var value = '';
	var fieldname = new Array();
	var fieldclass = new Array();
	$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
	   txtboxid = $(e).attr('id');
	   uitype = $('#'+txtboxid).data('uitype');
	   txtboxname = $('#'+txtboxid).data('fieldname');
	   txtclass = $('#'+txtboxid).data('class');
	   value = '';
	   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
		   fieldname.push(txtboxname);
		   fieldclass.push(txtclass);
	   } else {
		   fieldname.push(txtboxname+'name');
		   fieldclass.push(txtclass);
	   }
	});
	// Check Selected row
	var rowids = [];
	$('#'+gridname+' div.gridcontent .rowcheckbox').map(function(i,e) {
		checkboxid = $(e).attr('id');
		if($('#'+gridname+' div.gridcontent #'+checkboxid).prop('checked') == true) {
			mandatarowid = $('#'+gridname+' div.gridcontent #'+checkboxid).data('rowid');
			rowids.push(mandatarowid);
		}
	});
	rowids = rowids+'';
	rowids = rowids.split(',');
	var j=0;
	var dataarr = new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		if(jQuery.inArray(datarowid, rowids) != -1) {
			var datas = {};
			//console.log(fieldname);
			$(fieldname).each(function(i,e) {
				value='';
				
				//console.log('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul .'+fieldclass[i]);
				if(fieldname[i] == 'finalgrswt') {
					value = $('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul li.'+fieldclass[i]+' input').val();
				} else {
					value = $('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul .'+fieldclass[i]).text();
				}
				value = (value !== 'null') ? value : '';
				//console.log(value);
				datas[''+e+''] = new Array();	
				datas[''+e+''] = value;
				//console.log(datas);
			});
			dataarr.push(datas);
			j++;
		}
	});
	return dataarr;
}
//delete the selected row
function deletegriddatarow(gridname,rowid) {
	if(rowid!='') {
		/* if(deviceinfo != 'phone'){
			$('#'+gridname+' .gridcontent div#'+rowid).remove();
			var j=1;
		}else{ */
			$('#'+gridname+' .gridcontent div#'+rowid).remove();
			var j=1;
			$('#'+gridname+' .gridcontent div.data-rows').map(function(i,e) {
				datarowid = $(e).attr('id');
				$('#'+gridname+' .gridcontent div#'+datarowid).attr('id',j);
				j++;
			});						
		//}		
	}
}
//get the all row ids in grid
function getgridallrowids(gridname) {
	var rowids =  new Array();
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		rowids.push(datarowid);
	});
	return rowids;
}
// get the selected row id
function getselectedrowids(gridname) {
	var rowids =  new Array();
	$('#'+gridname+' div.gridcontent .rowcheckbox').map(function(i,e) {
		checkboxid = $(e).attr('id');
		if($('#'+gridname+' div.gridcontent #'+checkboxid).prop('checked') == true) {
			datarowid = $('#'+gridname+' div.gridcontent #'+checkboxid).data('rowid');
			rowids.push(datarowid);
		}
	});
	return rowids;
}
//get the selected the check box based record ids
function gridcheckboxsetselection(gridname,checkboxid) {
	var checkboxids = checkboxid.split();
	$('#'+gridname+' div.gridcontent .rowcheckbox').map(function(i,e) {
		checkboxid = $(e).attr('id');
		var chkboxid = $('#'+gridname+' div.gridcontent #'+checkboxid).data('rowid');
		if(jQuery.inArray( chkboxid, checkboxids )!= -1) {
			$('#'+gridname+' div.gridcontent #'+checkboxid).prop('checked',true);
		}
	});
}
//checkbox click function
function gridcheckboxselectevents(gridname) {
	$('#'+gridname+' div .headercheckbox').click(function(){
		if($('#'+gridname+' div .headercheckbox').prop('checked') == true) {
			$('#'+gridname+' div.gridcontent .rowcheckbox').prop('checked',true);
		} else {
			$('#'+gridname+' div.gridcontent .rowcheckbox').prop('checked',false);
		}
	});
	$('#'+gridname+' div.gridcontent .rowcheckbox').click(function(){
		if($('#'+gridname+' div.gridcontent .rowcheckbox').not(':checked').length>0) {
			$('#'+gridname+' div .headercheckbox').prop('checked',false);
		} else {
			$('#'+gridname+' div .headercheckbox').prop('checked',true);
		}
	});
}
//clear the grid
function cleargriddata(gridname) {
	if(gridname){
		/* if(deviceinfo != 'phone'){
			$('#'+gridname+' .gridcontent div.data-content').empty();
			$('#'+gridname+' .gridcontent div.data-content').append('&nbsp;');
		}
		else{ */
			$('#'+gridname+' .gridcontent div.wrappercontent').empty();
			$('#'+gridname+' .gridcontent div.wrappercontent').append('&nbsp;');
		//}
	}	
}
/*Form Height Set*/
function formheight(){
	var e = $(window).height(),
        a = e - 93;
		f = e - 64;
		g = e - 36;
		h = e - 137-24; 
		i = e - 153;
		j = e - 450;
		k = e - 93;
		l = e - 80;
		m = e - 83;
		n = e - 135;
	var deviceas = deviceinfo;
	/* if(deviceas=="phone"){
		setTimeout(function(){
			//left menu container height set
			$('.off-canvas-wrap').css('height',e);
			$('.left-off-canvas-menu').css('height',e);
			$('.left-off-canvas-menu').css('position','fixed');
			$('.left-off-canvas-menu').css('overflow','hidden');
			//left menu modulelist height set
			var menuheight = e - $('.homeul').height();
			$('.headermenu').css('height',menuheight);
			
			//addform height set
			$(".addformcontainer").css("height", "" + k + "px");
			$(".actionbarformcontainer").css("height", "" + l + "px");
			$(".salesaddformcontainer").css("height", "" + m + "px");
			$('.marketplaceformcontainer').css('height',n);
		},10);
	}else{ */
		setTimeout(function(){
			/*left menu container height set*/
			$('.off-canvas-wrap').css('height',e);
			$('.left-off-canvas-menu').css('height',e);
			$('.left-off-canvas-menu').css('position','fixed');
			$('.left-off-canvas-menu').css('overflow','hidden');
			/*left menu modulelist height set*/
			var menuheight = e - $('.homeul').height();
			$('.headermenu').css('height',menuheight);
			/*addform height set*/
			$(".addformcontainer").css("height", "" + a + "px");
			$(".actionbarformcontainer").css("height", "" + f + "px");
			$(".formwithouttabgroup").css("height", "" + g + "px");
			$(".salesaddformcontainer").css("height", "" + a + "px");
			$('.marketplaceformcontainer').css('height',n);
		},10);
		$(".searchfilteroverlay .filterheight").css('height',h+'px');
		$(".innersearchfilteroverlay .filterheight").css('height',i+'px');
		//$(".searchfilteroverlay .filterfieldbox").css('max-height',j+'px');
	//}
}
function sectionpanelheight(overlayid){
	{//form-with-innergrid section overlay height mobile
		var deviceas = deviceinfo;
		/* if(deviceas=="phone"){
			var e = $(window).height();
			var secheadht = $('#'+overlayid+' .sectionheaderformcaptionstyle').outerHeight();
			var secbtnareaht = $('#'+overlayid+' .sectionalertbuttonarea').outerHeight();
			o = e - secheadht - secbtnareaht -98;
			singlesectionaddformheight =  e - secheadht - secbtnareaht;
			$('#'+overlayid+' .sectionpanel').css("height", "" + o +"px");
			$('.singlesectionaddform #'+overlayid+' .sectionpanel').css("height", "" + singlesectionaddformheight +"px");
		} */
	}
}
/*
Vardaan Integration module
*/
// scheme name change - chit gift
function schemenamechangenew(schemeid,schemeamount,schememonth,shemebaseamount,amountprefix,chit_token_no,schemegift){
	$('#'+schemeamount+'').empty();	
    $('#'+schemeamount+'').append($("<option></option>"));
    $.ajax({
		url:base_url+"Schememaster/schemeamountretrive?primaryid="+schemeid, 
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var str=data.amount;
			var str_array = str.split(','); 
            for(var i = 0; i < str_array.length; i++) {
				str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
				var str_array_split = str_array[i].split('-');   
                $('#'+schemeamount+'')
				.append($("<option></option>")
				.attr('data-'+amountprefix+'',str_array_split[1])
				.attr("value",str_array_split[0])
				.text(str_array_split[0]));
			}
			$('#'+schememonth+'').val(data.noofmonths);
			$('#'+shemebaseamount+'').val(data.baseamount);
			var luckydraw=data.luckydraw; 
			if(luckydraw == 'NO') {
				$('.'+chit_token_no+'').addClass('hidedisplay');
			} else {
				$('.'+chit_token_no+'').removeClass('hidedisplay');
			}
				if(schemegift == 'yes'){
					$("#typegift option").addClass("ddhidedisplay");
					$("#typegift optgroup").addClass("ddhidedisplay");
					$("#typegift option").prop('disabled',true);
					if(data.luckydraw == 'YES' && data.gift == 'YES'){
						$("#typegift option[value="+4+"]").removeClass("ddhidedisplay");
						$("#typegift option[value="+4+"]").closest('optgroup').removeClass("ddhidedisplay");
						$("#typegift option[value="+4+"]").prop('disabled',false);
						$("#typegift option[value="+5+"]").removeClass("ddhidedisplay");
						$("#typegift option[value="+5+"]").closest('optgroup').removeClass("ddhidedisplay");
						$("#typegift option[value="+5+"]").prop('disabled',false);
					}else if(data.luckydraw == 'NO' && data.gift == 'NO'){
						alertpopup('This scheme do not have gift and prize');
					}
					else if(data.luckydraw == 'YES'){
						$("#typegift option[value="+5+"]").removeClass("ddhidedisplay");
						$("#typegift option[value="+5+"]").closest('optgroup').removeClass("ddhidedisplay");
						$("#typegift option[value="+5+"]").prop('disabled',false);
					}else if(data.gift == 'YES'){
						$("#typegift option[value="+4+"]").removeClass("ddhidedisplay");
						$("#typegift option[value="+4+"]").closest('optgroup').removeClass("ddhidedisplay");
						$("#typegift option[value="+4+"]").prop('disabled',false);
					}
			  }
		 }
	});
}
//scheme name change
function schemenamechange(schemeid,schemeamount,schememonth,shemebaseamount,amountprefix,chit_token_no){
	$('#'+schemeamount+'').empty();	
    $('#'+schemeamount+'').append($("<option></option>"));
    $.ajax({
		url:base_url+"Schememaster/schemeamountretrive?primaryid="+schemeid, 
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var str=data.amount;
			var str_array = str.split(','); 
            for(var i = 0; i < str_array.length; i++) {
				str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
				var str_array_split = str_array[i].split('-');   
                $('#'+schemeamount+'')
				.append($("<option></option>")
				.attr('data-'+amountprefix+'',str_array_split[1])
				.attr("value",str_array_split[0])
				.text(str_array_split[0]));
			}
			$('#'+schememonth+'').val(data.noofmonths);
			$('#'+shemebaseamount+'').val(data.baseamount);
			var luckydraw=data.luckydraw; 
			if(luckydraw == 'NO') {
				$('.'+chit_token_no+'').addClass('hidedisplay');
			} else {
				$('.'+chit_token_no+'').removeClass('hidedisplay');
			}
		}
	});
}
function select2ddenabledisable(fieldid,state) {
	if(state == 'enable') {
		// enable
		$("#"+fieldid+"").prop("disabled", false);
		var tabindexvalue = $("#"+fieldid+"").data("tabindexvalue");
	} else if(state == 'disable') {
		// disable
		$("#"+fieldid+"").prop("disabled", true);
		var tabindexvalue = '-1';
	}
	$('#s2id_'+fieldid+' :input[role=button]').attr("tabindex", tabindexvalue);
	$('#s2id_'+fieldid+':input[role=combobox]').attr("tabindex", tabindexvalue);
	var select2autogenid = $('#s2id_'+fieldid+' :input[role=button]').attr("id");
	$('#'+select2autogenid+'').attr("tabindex", tabindexvalue);
	$('#'+select2autogenid+'_search').attr("tabindex", tabindexvalue);
}
// close view
function addcloseview(closebutton) {
    $('#'+closebutton[0]+'').click(function() {
        if( globalclosetrigger == "1") { 
          $('#'+closebutton[2]+'').hide();
          $('.'+closebutton[1]+'').fadeIn(1500);
        } else { 
            alertpopupformclose("Do you wish to close form ?");
            $("#alertsfcloseyes").focus();
            $("#alertsfcloseyes").click(function() {
                var clsform =$("#alertsfcloseyes").val();
                if(clsform == "Yes") {
                    $("#alertscloseyn").fadeOut("fast");
                    $('#'+closebutton[2]+'').hide();
                    $('.'+closebutton[1]+'').fadeIn(1500,"jswing");
					resetFields();
					$('.btmerrmsg').remove();
					$(".ftab").trigger('click');
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
                }
            });
        }
    });
}
// scheme name load
function schemenameload(scheme) {
	$('#'+scheme+'').empty();
	$('#'+scheme+'').append($("<option></option>"));
	 $.ajax({
        url: base_url + "Schememaster/schemenameload",
       	async:false,
		cache:false,
		dataType:'json',
		success: function(data) { 
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+scheme+'')
					.append($("<option></option>")
					.attr("value",data[index]['chitschemeid'])
					.attr("data-maxschemeamount",data[index]['maxschemeamount'])
					.attr("data-minnoofmonths",data[index]['minnoofmonths'])
					.attr("data-chitlookupsid",data[index]['chitlookupsid'])
					.attr("data-intrestmodeid",data[index]['intrestmodeid'])
					.attr("data-variablemodeid",data[index]['variablemodeid'])
					.text(data[index]['schemename']));
				});
			}	
        },
    });
}
//used to check the given data is NAN or not. if its NAN will set output value to zero
function checknan(output) {
	if(isNaN(output)){
		output = 0;
	} else if(output == '') {
		output = 0;
	}
	return output;
}
{//checks the unique name of the giveninputs
	//return error if value exists already-*validation done with spaces
	function varcheckuniquename(field){	
		var table = field.attr('data-table');
		var fieldname = field.attr('data-validatefield');
		var value = field.attr('value');	
		var editid = field.attr('data-primaryid');	
		value=$.trim(value);
		var primarydataname = $('#'+table+'primaryname').val();	
		if(primarydataname != value){
			if(value.length > 0){
				var check = '';
				$.ajax({
					url: base_url + "Base/checkuniquename",
					data:{table:table,fieldname:fieldname,value:value,editid:editid},
					async:false,
					success: function(msg) {
						check =  $.trim(msg);
					},
				});
				if (check == 'YES') {
					return "*value already exists";
				}
			}
		} 
	}
}
//gird value and data reset
function viewcondvaluereset() {
	cleargriddata('viewcreateconditiongrid');
	$('#viewcondcolumn').empty();
	$('#viewcondcolumn').append($("<option></option>"));
}
{ // froala editor common function call //modules: printtemplate,user,email template,email compaign(partial),quote,sales order,termsandcondition,purchase order,invoice,solution,contract,Materialrequisition,personal settings
	function froalaeditoradd(editorname){
		var amp = '&';
		var editordata = '';
		var neweditordata = '';
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(','); 
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').froalaEditor('html.get',true);
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){	
					for (var i = 0; i < datacontent.length; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				neweditordata += amp+fname[0]+'_editorfilename='+editordata; 
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
			return editordata;
		}
	}
	function froalaeditordataget(editorname){
		var amp = '&';
		var editordata = '';
		var neweditordata = '';
		if(editorname != '') {
			var editornames  = [];
			editornames = editorname.split(','); 
			$.each( editornames, function( key, name ) {
				editordata = $('#'+name+'').froalaEditor('html.get',true);
				var regExp = /&nbsp;/g;
				var datacontent = editordata.match(regExp);
				if(datacontent !== null){	
					for (var i = 0; i < datacontent.length; i++) {
						editordata = editordata.replace(''+datacontent[i]+'','a10s');
					}
				}
				var fname = [];
				fname = name.split('_');
				neweditordata += amp+fname[0]+'_editorfilename='+editordata; 
				$('#'+fname[0]+'_editorfilename').val(editordata);
			});
			return neweditordata;
		}
	}
	function froalaset(name) {
		if(name[0]){
			nname = name[0].split(',');
			for(var i=0;i<nname.length;i++) {
				var res=$('#'+nname[i]+'').froalaEditor(name[1],name[2]);
			}
			return res;
		}
	}
	function froalaedit(data,name){ 
		 if(data != 'Fail') {
					var regExp = /a10s/g;
					var datacontent = data.match(regExp);
					if(datacontent !== null){	
						for (var i = 0; i < datacontent.length; i++) {
							data = data.replace(''+datacontent[i]+'','&nbsp;');
						}
					}
					$('#'+name+'').froalaEditor("html.set", data);
		} else {
				$('#'+name+'').froalaEditor("html.set", "");
		} 
	}
	//automated-form rule setting 1)validation 2)showhides 3)enable/disable.
//@param id- it has the rule data set //json type
//@param types
	function fieldruleset(id,type) {
	var mainjshidtakeordercategory = $('#mainjshidtakeordercategory').val();
	var mainjshidtransactioncategoryfield = $('#mainjshidtransactioncategoryfield').val();
	var data = $('#'+id+'').val();
	var data = $.parseJSON(data);	
	if(checkValue(data) == true) {
		//enable/disable
		var enable_data = data['enable'];
		//validate
		var validate_data = data['validate'];
		
		//show/hide.
		var show_data = data['hide'];
		var fields_data = data['fields'];
		i=0;
		insertedfieldid = [];
		$.each(fields_data[type], function(index, value) {
			
			insertedfieldid[i] = fields_data[type][value];
			i++;
		});
		
		//validate type
		$('.clearonstocktypechange').hide();
		$('.clearonstocktypechange input').next('span').remove('.btmerrmsg'); 
		$('.clearonstocktypechange input').removeClass('error');
		$('.clearonstocktypechange input').prev('div').remove();
		$('.clearonstocktypechange').each( function() {
			
			var shid = $(this).attr('id');
			var res = shid.split("-");
			var index = res[0];
			if(jQuery.inArray(index, insertedfieldid) === -1) {
				$('#'+index+'-div').hide();
				$('#'+index+'').attr('data-validation-engine','');
			} else {
				//alert(index+' '+show_data[type][index]);
				if(show_data[type][index] == 'hide') {
					$('#'+index+'-div').hide();					
				} else {
					$('#'+index+'-div').show();
				}
				if(index == 'category' && mainjshidtransactioncategoryfield == 0 && mainjshidtakeordercategory == 1) {
					var i = 0
					$.ajax({
					url:base_url+'Base/categorytreerequiredfield',
					data:{},					
					success: function(msg) {
							if(msg != '' && i == 0) {
								$('#category-div div').remove();
								$('#category-div').append('<div id="dl-promenucategory" class="dl-menuwrapper" style="z-index:2;margin-bottom: 1rem"><button name="treebutton" type="button" id="prodcategorylistbutton" class="btn dl-trigger treemenubtnclick" tabindex="" style="height:2.2rem;color:#ffffff;font-size: 1.45rem;padding: 0.1rem;padding-top:0.3rem;background-color:#546e7a;"><i class="material-icons">format_indent_increase</i></button><input name="categorynamehidden" type="text" id="categorynamehidden" style="width:83%;display:inline;position:absolute;height:2.12rem;padding-left: 5px;" readonly="readonly" tabindex="380"><ul class="dl-menu maintreegenerate large-12 medium-6 small-12 column" id="prodcategorylistuldata">'+msg+'</ul></div>');
							}
							{
								$('#dl-promenucategory').dlmenu({
									animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
								});
								// parent category select
								$('#prodcategorylistuldata li').click(function() {
									var name = $(this).attr('data-listname');
									var categoryid =$.trim($(this).attr('data-listid')); 
									var level = parseInt($(this).attr('data-level'));
									$('#categorynamehidden').val(name);
									if(categoryid != '') {
										$('#category').val(categoryid).trigger('change');
										$('#treecategoryname').val(name);
									} else {
										$('#category').val('');
										$('#treecategoryname').val('');
									}
								});
							}
						}
					});
				}
				//check enable disables
				var etype = $('#'+index+'').prop("tagName"); // input type
				if(etype != 'SELECT') {
					if(enable_data[type][index] == 'disabled') {
						$('#'+index+'').prop('disabled', true);
					} else if(enable_data[type][index] == 'enabled') {
						$('#'+index+'').prop('disabled', false);
					}
				} else if(etype == 'SELECT') {
					if(enable_data[type][index] == 'disabled') {
						select2ddenabledisable(index,'disable');
					} else if(enable_data[type][index] == 'enabled') {
						select2ddenabledisable(index,'enable');
					}
				}
				//alert(validate_data[type][index]);
				value = validate_data[type][index];
				//apply the validations	
				if(index == 'rfidtagnumber') {
					$('#'+index+'').attr('data-validation-engine','');
				} else {
					$('#'+index+'').attr('data-validation-engine',value);
				}
			
				//apply * for required fields.
				$('#'+index+'_req').text('');			
				var need = value.indexOf("required");
				if(need > -1 ){
					$('#'+index+'_req').text('*');
				}
			
			}
		});
	}
}
function paymentfieldruleset(id,type) {
	var data = $('#'+id+'').val();
	var data = $.parseJSON(data);	
	if(checkValue(data) == true) {
		//enable/disable
		var enable_data = data['enable'];
		//validate
		var validate_data = data['validate'];
		
		//show/hide.
		var show_data = data['hide'];
		var fields_data = data['fields'];
		i=0;
		insertedfieldid = [];
		$.each(fields_data[type], function(index, value) {
			
			insertedfieldid[i] = fields_data[type][value];
			i++;
		});
		
		//validate type
		$('.clearonpaymentstocktypechange').hide();
		$('.clearonpaymentstocktypechange input').next('span').remove('.btmerrmsg'); 
		$('.clearonpaymentstocktypechange input').removeClass('error');
		$('.clearonpaymentstocktypechange input').prev('div').remove();
		$('.clearonpaymentstocktypechange').each( function() { 	
			
			var shid = $(this).attr('id');
			var res = shid.split("-");
			var index = res[0];
		if(jQuery.inArray(index, insertedfieldid) === -1){
		
			$('#'+index+'-div').hide();
			$('#'+index+'').attr('data-validation-engine','');
		}
		else
		{
			//alert(index+' '+show_data[type][index]);
			if(show_data[type][index] == 'hide'){				
				$('#'+index+'-div').hide();					
			} else {
				$('#'+index+'-div').show();
			}
			//check enable disables
			var etype = $('#'+index+'').prop("tagName"); // input type
			if(etype != 'SELECT') {
				if(enable_data[type][index] == 'disabled'){
					$('#'+index+'').prop('disabled', true);
				} else if(enable_data[type][index] == 'enabled'){
					$('#'+index+'').prop('disabled', false);
				}
			} else if(etype == 'SELECT') {
				if(enable_data[type][index] == 'disabled'){
					select2ddenabledisable(index,'disable');
				} else if(enable_data[type][index] == 'enabled'){
					select2ddenabledisable(index,'enable');
				}
			}
			//alert(validate_data[type][index]);
			value = validate_data[type][index];
			//apply the validations	
			$('#'+index+'').attr('data-validation-engine',value);
			//apply * for required fields.
			$('#'+index+'_req').text('');			
			var need = value.indexOf("required");
			if(need > -1 ){
				$('#'+index+'_req').text('*');
			}
			
		}	
		});
	}
}
//form to grid 
function formtogriddata_jewel(gridname,method,position,datarowid) {
	method = typeof method == 'undefined' ? 'ADD' : method;
	var dataval = "";
	var align = '';
	var opndivcontent = '';
	var content = '';
	var clsdivcontent = '';	
	//if(deviceinfo != 'phone'){
		var n = $('#'+gridname+' .gridcontent div.data-content div').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.data-content').empty();
		} else{
			n = $('#'+gridname+' .gridcontent div.data-content div:last').attr('id');
			n = parseInt(n);
		}
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<ul class="inline-list">';
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
			var txtboxid = $(e).attr('id');
			var uitype = $('#'+txtboxid).data('uitype');
			var txtboxname = $('#'+txtboxid).data('fieldname');
			var txtboxlabel = $('#'+txtboxid).data('label');
			var txtwidth = $('#'+txtboxid).data('width');
			var txtclass = $('#'+txtboxid).data('class');
			var viewtype = $('#'+txtboxid).data('viewtype');
			var viewstyle = (viewtype=='0') ? ' display:none':'';
			if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
				dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
				datavalue = dataval === undefined ? '' : dataval;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			} else if(uitype == '31') {
				dataval = $('#stoneid').find('option:selected').data('stoneidgroupid');
				datavalue = dataval === undefined ? '' : dataval;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			} else if(uitype == '32') {
				dataval = $('#stoneid').find('option:selected').data('stoneidgroupname');
				datavalue = dataval === undefined ? '' : dataval;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			} else if(uitype == '33') {
				dataval = $('#stoneid').find('option:selected').data('stoneidhidden');
				datavalue = dataval === undefined ? '' : dataval;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			} else if(uitype == '34') {
				dataval = $('#stoneentrycalctypeid').find('option:selected').data('stonecalcidhidden');
				datavalue = dataval === undefined ? '' : dataval;
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+datavalue+'</li>';
			} else {
				dataval = $('#'+txtboxname+'').val();
				align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				content+='<li class="'+align+' '+txtclass+'" style="width:'+txtwidth+'px;'+viewstyle+'">'+dataval+'</li>';
			}
		});
		content+='</ul>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.data-content:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.data-content').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.data-content div#'+datarowid).append(content);
			}
		}
	/*}
	 else{
		var n = $('#'+gridname+' .gridcontent div.bottom-wrap').length;
		if(n=='0' || n=='') {
			$('#'+gridname+' .gridcontent div.bottom-wrap').empty();
		}
		opndivcontent = '<div class="data-rows" id="'+(n+1)+'">';
		content = '<div class="bottom-wrap">';
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
			   var txtboxid = $(e).attr('id');
			   var uitype = $('#'+txtboxid).data('uitype');
			   var txtboxname = $('#'+txtboxid).data('fieldname');
			   var txtboxlabel = $('#'+txtboxid).data('label');
			   var txtwidth = $('#'+txtboxid).data('width');
			   var txtclass = $('#'+txtboxid).data('class');
			   var viewtype = $('#'+txtboxid).data('viewtype');
			   var viewstyle = (viewtype=='0') ? ' display:none':'';
			   if( uitype == '17' || uitype == '18' || uitype == '19' || uitype == '20' || uitype == '23' || uitype == '25' || uitype == '26' || uitype == '27' || uitype == '28' || uitype == '29') {
				   dataval = $('#'+txtboxname+'').find('option:selected').data(''+txtboxname+'hidden');
				   datavalue = dataval === undefined ? '' : dataval;
				   if(datavalue != '' || datavalue != '0'){
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+datavalue+'</span>';
				  }
			   } else if(uitype == '31'){
				   dataval = $('#stoneid').find('option:selected').data('stoneidgroupid');
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }
				else if(uitype == '32'){
				   dataval = $('#stoneid').find('option:selected').data('stoneidgroupname');
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }
			   else if(uitype == '33'){
				   dataval = $('#stoneid').find('option:selected').data('stoneidhidden');
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }
			   else if(uitype == '34'){
				   dataval = $('#stoneentrycalctypeid').find('option:selected').data('stonecalcidhidden');
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }
			   else {
				   dataval = $('#'+txtboxname+'').val();
				   if(dataval != '' || dataval != '0'){					   
				   align = (typeof value === 'number')?(dataval % 1 === 0 ? 'textcenter' : 'textleft'):'textleft';
				   content+='<span class="content" style="'+viewstyle+'">'+txtboxlabel+':'+'</span><span class="'+align+' '+txtclass+' content" style="'+viewstyle+'">'+dataval+'</span>';
				   }
			   }	  
			});
		content+='</div>';
		clsdivcontent='</div>';
		if(method == 'ADD' || method == '') {
			var finalcontent = opndivcontent+content+clsdivcontent;
			if(position == 'last') {
				$('#'+gridname+' .gridcontent div.wrappercontent:last').append(finalcontent);
			} else {
				$('#'+gridname+' .gridcontent div.wrappercontent').prepend(finalcontent);
			}
		} else {
			if(datarowid!='') {
				$('#'+gridname+' .gridcontent div.wrappercontent div#'+datarowid).empty();
				$('#'+gridname+' .gridcontent div.wrappercontent div#'+datarowid).append(content);
			}
		}
	}	 */
}
function getstonegridsummary() {
	var celValue='';
	var totdiapieces=0;
	var totdiacaratwt=0;
	var totdiawt=0;
	var totdiaamt=0;
	var totcspieces=0;
	var totcscaratwt=0;
	var totcswt=0;
	var totcsamt=0;
	var totpearlpieces=0;
	var totpearlcaratwt=0;
	var totpearlwt=0;
	var totpearlamt=0;
	var ids =getgridallrowids('stoneentrygrid');
	if(ids != '') {
		for (var i = 0; i < ids.length; i++) {
			var rowId = ids[i];
			var stoneidgroupid = getgridcolvalue('stoneentrygrid',rowId,'stonegroupid','');
			if(stoneidgroupid==2){
				totdiapieces=parseInt(totdiapieces)+parseInt(getgridcolvalue('stoneentrygrid',rowId,'stonepieces',''));
				totdiacaratwt=parseFloat(totdiacaratwt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'caratweight',''));
				totdiawt=parseFloat(totdiawt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'totalweight',''));
				totdiaamt =parseFloat(totdiaamt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'stonetotalamount',''));
			} else if(stoneidgroupid==3){
				totcspieces=parseInt(totcspieces)+parseInt(getgridcolvalue('stoneentrygrid',rowId,'stonepieces',''));
				totcscaratwt=parseFloat(totcscaratwt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'caratweight',''));
				totcswt=parseFloat(totcswt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'totalweight',''));
				totcsamt =parseFloat(totcsamt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'stonetotalamount',''));
			} else if(stoneidgroupid==4){
				totpearlpieces=parseInt(totpearlpieces)+parseInt(getgridcolvalue('stoneentrygrid',rowId,'stonepieces',''));
				totpearlcaratwt=parseFloat(totpearlcaratwt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'caratweight',''));
				totpearlwt=parseFloat(totpearlwt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'totalweight',''));
				totpearlamt =parseFloat(totpearlamt)+parseFloat(getgridcolvalue('stoneentrygrid',rowId,'stonetotalamount',''));
			}
		}
		var stonepieces = getgridcolvalue('stoneentrygrid','','stonepieces','sum');
		var caratweight = getgridcolvalue('stoneentrygrid','','caratweight','sum');
		var totalcaratweight = getgridcolvalue('stoneentrygrid','','totalcaratweight','sum');
		var totalweight= getgridcolvalue('stoneentrygrid','','totalweight','sum');
		var totalamount = getgridcolvalue('stoneentrygrid','','stonetotalamount','sum');
		$("#totpieces").html(stonepieces);
		$("#caratwt").html(caratweight.toFixed(round));
		$("#totcwt").html(totalcaratweight.toFixed(round));
		$("#totwt").html(totalweight.toFixed(round));
		$("#totamt").html(totalamount.toFixed(roundamount));
		$("#totdiapieces").html(totdiapieces);
		$("#totcspieces").html(totcspieces);
		$("#totpearlpieces").html(totpearlpieces);
		$("#totdiacaratwt").html(totdiacaratwt.toFixed(round));
		$("#totcscaratwt").html(totcscaratwt.toFixed(round));
		$("#totpearlcaratwt").html(totpearlcaratwt.toFixed(round));
		
		$("#totdiawt").html(totdiawt.toFixed(round));
		$("#totcswt").html(totcswt.toFixed(round));
		$("#totpearlwt").html(totpearlwt.toFixed(round));
		
		$("#totdiaamt").html(totdiaamt.toFixed(roundamount));
		$("#totcsamt").html(totcsamt.toFixed(roundamount));
		$("#totpearlamt").html(totpearlamt.toFixed(roundamount)); 
	}else{
		$("#totpieces,#caratwt,#totcwt,#totwt,#totamt,#totdiapieces,#totcspieces,#totpearlpieces,#totdiacaratwt,#totcscaratwt,#totpearlcaratwt,#totdiawt,#totcswt,#totpearlwt,#totdiaamt,#totcsamt,#totpearlamt").html(0);
	}
}
function stoneentrycal() {
	var pieces=$('#stonepieces').val();
	var caratwt=$('#caratweight').val();
	var stonegram=$('#stonegram').val();
	var totalcaratwt ='';
	var totalamt = '';
	var totalwt = '';
	var stoneentrycalctype = $('#stoneentrycalctypeid').find('option:selected').val();
	{ // Old Purchase calculation - for sales
		var oldjewelsstonecalc = $('#oldjewelsstonecalc').val();
		oldjewelsstonecalc = typeof oldjewelsstonecalc == 'undefined' ? '0' : oldjewelsstonecalc;
		var stocktypeid = $("#stocktypeid").find('option:selected').val();
		stocktypeid = typeof stocktypeid == 'undefined' ? '1' : stocktypeid;
	}
	$("span","#stonepiecesdiv label").remove();
	$("span","#stonegramdiv label").remove();
	$("span","#caratweightdiv label").remove();
	if(pieces!='' || caratwt!='' || stonegram!='' && stoneentrycalctype != '') {
		var basicrate=$("#basicrate").val();
		var purchaserate=$("#purchaserate").val();
		totalcaratwt=pieces*caratwt;
		if(stoneentrycalctype == 2){  // carat wise
			$('#caratweightdiv label').append('<span class="mandatoryfildclass">*</span>');
			if(oldjewelsstonecalc == 1 && stocktypeid == 19 || (stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81)) {
				totalamt=(purchaserate*caratwt);
			} else {
				totalamt=(basicrate*caratwt);
			}
			totalwt=(caratwt)/5;
			$("#totalweight").val(totalwt.toFixed(round));
		}else if(stoneentrycalctype == 4){  // Gram wise
			$('#stonegramdiv label').append('<span class="mandatoryfildclass">*</span>');
			if(oldjewelsstonecalc == 1 && stocktypeid == 19 || (stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81)) {
				totalamt=(purchaserate*stonegram);
			} else { 
				totalamt=(basicrate*stonegram);
			}
			totalwt=stonegram;
			$("#totalweight").val(totalwt);
		}else if(stoneentrycalctype == 3){   // pieces wise
			if(oldjewelsstonecalc == 1 && stocktypeid == 19 || (stocktypeid == 17 || stocktypeid == 80 || stocktypeid == 81)) {
				totalamt=(purchaserate*pieces);
			} else {
				totalamt=(basicrate*pieces);
			}
			if(caratwt != '' && caratwt != 0){ // carat weight change
			 totalwt=(caratwt)/5;
			 $("#totalweight").val(totalwt.toFixed(round));
			 $('#stonegram').attr('data-validation-engine','');
			 $("span","#stonegramdiv label").remove();
			 $('#caratweightdiv label').append('<span class="mandatoryfildclass">*</span>');
			 $('#caratweight').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.01]]');
			}else if(stonegram != '' && stonegram != 0){	 // gram weight change
			  totalwt=stonegram;
			  $("#totalweight").val(totalwt);
			  $('#caratweight').removeClass('error');
			  $("span","#caratweightdiv label").remove();
			  $('#caratweight').attr('data-validation-engine','');
			  $('#stonegram').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.01]]');
			  $('#stonegramdiv label').append('<span class="mandatoryfildclass">*</span>');
			}
		} 
		$("#totalcaratweight").val(totalcaratwt.toFixed(round));
		$("#stonetotalamount").val(totalamt.toFixed(roundamount));
		$('#totalcaratweight').attr('readonly', true);
		$('#totalweight').attr('readonly', true);
		$('#stonetotalamount').attr('readonly', true);
		setTimeout(function(){
			Materialize.updateTextFields();
		},100);
	}
}
function stoneentrygrid() {
		var wwidth = $("#stoneentrygrid").width();
		var wheight = $("#stoneentrygrid").height();
		$.ajax({
			url:base_url+"Sales/localgirdheaderinformationfetch?width="+wwidth+"&height="+wheight+"&modulename=sales",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#stoneentrygrid").empty();
				$("#stoneentrygrid").append(data.content);
				$("#stoneentrygridfooter").empty();
				$("#stoneentrygridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('stoneentrygrid');
				 var hideprodgridcol = ['stonegroupid','stoneid','stoneentrycalctypeid','totalcaratweight'];
				gridfieldhide('stoneentrygrid',hideprodgridcol);
		  },
		});
	}	
}
//picklist group show hide
function picklistgroupshow(parentid,optvalue,labelname,ddname){
	var dropdownarray = ddname.split(',');
	for(i=0;i<dropdownarray.length;i++) {
		$("#"+dropdownarray[i]+" optgroup").addClass("ddhidedisplay").addClass("ddclose");
		$("#"+dropdownarray[i]+" option").addClass("ddhidedisplay").addClass("ddclose");
		$("#"+dropdownarray[i]+" optgroup[label="+labelname+"]").removeClass("ddhidedisplay").removeClass("ddclose");
		$("#"+dropdownarray[i]+" optgroup[label="+labelname+"] option").removeClass("ddhidedisplay").removeClass("ddclose");
		$("#"+dropdownarray[i]+"").select2('val','');
	}
}
//GRA For First error Field
function errorfieldfocus(){
	setTimeout(function(){
	var errorfirstInput = $('form').find('.error:first').focus().select2("focus").filter(':visible:first');
	if (errorfirstInput != null) {
		errorfirstInput.focus();
		}
	},100);
}
//Tabgroup More Dropdown viewgrid-addform
function autocollapse(faseinscr){
	var tabs = $('.desktoptabgroup .tabs:visible');
	var tabsHeight = tabs.innerHeight();
	if (tabsHeight >= 32) {
		while (tabsHeight > 32) {console.log(tabsHeight);
			$('#'+faseinscr+' .desktoptabgroup .tabs:visible .moretab').css('display','inline-block');
			$('#'+faseinscr+' .desktoptabgroup .tabs:visible .morelabel').css('display','inline-block');
			var children = tabs.children('li:nth-child(-n+2)');
			var count = children.length;console.log(children);
			$(children[count - 1]).prependTo('#'+faseinscr+' .desktoptabgroup .tabs .dropdown-content');
			tabsHeight = tabs.innerHeight();
			//moredropdown change
			$("#tabgroupmoredropdown li").click(function(){
				$(".morelabel").addClass("active");
				newid = $(this).attr("id");
				newdatasfrm	= $(this).data('subform');
				newtablabel = $(this).text();
				$(".morelabel").attr('data-subform',newdatasfrm);
				$(".morelabel").data('subform',newdatasfrm);
				$(".morelabel").attr('id',newid);
				$(".morelabel span").text(newtablabel);
			});
			$("#tabgroupmoredropdown li:last-child").trigger('click');
			$(".ftab").trigger('click');
		}
	} else {
		while (tabsHeight < 32 && (tabs.children('li').length > 0)) {console.log(tabsHeight);
			$('#'+faseinscr+' .desktoptabgroup .tabs:visible .moretab').css('display','inline-block');
			var collapsed = $('#'+faseinscr+' .desktoptabgroup .tabs .dropdown-content').children('li');
			var count = collapsed.length;
			$(collapsed[0]).insertBefore(tabs.children('li:last-child'));
			tabsHeight = tabs.innerHeight();
		}
		if (tabsHeight > 32) { // double chk height again
			autocollapse(faseinscr);
		}
	} 
}
//Tabgroup More Dropdown form-with-grid
function fwgautocollapse(){
	var tabs = $('.desktoptabgroup .tabs:visible');
	var tabsHeight = tabs.innerHeight();
	if (tabsHeight >= 32) {
		while (tabsHeight > 32) {console.log(tabsHeight);
			$('.desktoptabgroup .tabs:visible .moretab').css('display','inline-block');
			$('.desktoptabgroup .tabs:visible .morelabel').css('display','inline-block');
			var children = tabs.children('li:nth-child(-n+2)');
			var count = children.length;
			$(children[count - 1]).prependTo('.desktoptabgroup .tabs:visible .dropdown-content');
			tabsHeight = tabs.innerHeight();
			//moredropdown change
			$("#mastertabgroupmoredropdown li").click(function(){
				newid = $(this).attr("id");
				newdatasfrm	= $(this).data('dbsubform');
				newtablabel = $(this).text();
				$(".morelabel").attr('data-dbsubform',newdatasfrm);
				$(".morelabel").data('dbsubform',newdatasfrm);
				$(".morelabel").attr('id',newid);
				$(".morelabel span").text(newtablabel);
				setTimeout(function(){
					$(".morelabel").addClass("active");
				},100);
			});
			$("#mastertabgroupmoredropdown li:last-child").trigger('click');
			setTimeout(function(){
				$(".ftab").trigger('click');
			},100);
		}
	} else {
		while (tabsHeight < 32 && (tabs.children('li').length > 0)) {console.log(tabsHeight);
			$('.desktoptabgroup .tabs:visible .moretab').css('display','inline-block');
			var collapsed = $('.desktoptabgroup .tabs .dropdown-content').children('li');
			var count = collapsed.length;
			$(collapsed[0]).insertBefore(tabs.children('li:last-child'));
			tabsHeight = tabs.innerHeight();
		}
		if (tabsHeight > 32) { // double chk height again
			fwgautocollapse();
		}
	} 
}
//used to load the fields based on modules in View CRUD Form and some other modules
function loadcolumnname(moduleid) {
	$("#viewmaincolumnname,#viewmaincolumnname_to").empty();
	$.ajax({
		url:base_url+"Base/setmainviewcolumndata?moduleid="+moduleid,
		async:false,
		success: function(data) {
			$("#viewmaincolumnname").append(data);	
		}	
	});
	setTimeout(function() {
		$.ajax({
			url:base_url+"Reportsview/loadcolumnname?moduleid="+moduleid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				$("#view_datefield,#viewcondcolumn").empty();
				$("#view_datefield,#viewcondcolumn").append("<option></option>");
				$.each(data, function(index){			
					//calculation field load
					if(data[index]['uitypeid'] == '8'  ){						 
						$('#view_datefield')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])
							.attr("data-uitypeid",data[index]['uitypeid'])	
							.text(data[index]['viewcreationcolumnname']));
					} else if(data[index]['uitypeid'] == '31'){
						$('#view_datefield')
							.append($("<option></option>")
							.attr("value",data[index]['viewcreationcolumnid'])
							.attr("data-uitypeid",data[index]['uitypeid'])	
							.text(data[index]['viewcreationcolumnname']));
						$('#viewcondcolumn')
							.append($("<option></option>")
							.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
							.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
							.attr("value",data[index]['viewcreationcolumnname'])
							.text(data[index]['viewcreationcolumnname']));	
					}else {
						$('#viewcondcolumn')
							.append($("<option></option>")
							.attr("data-indexname",data[index]['viewcreationcolmodelindexname'])
							.attr("data-dbindexname",data[index]['viewcreationjoincolmodelname'])
							.attr("data-uitypeid",data[index]['uitypeid'])
							.attr("data-reportcondcolumnid",data[index]['viewcreationcolumnid'])
							.attr("value",data[index]['viewcreationcolumnname'])
							.text(data[index]['viewcreationcolumnname']));							
					}				
				});			
			}
		});
	},100);
}
//date Range
function daterangefunction() { 
	$('#view_startdate').datetimepicker("destroy");
	$('#view_enddate').datetimepicker("destroy");
	var dateformetdata = $('#view_startdate').attr('data-dateformater');
	var startDateTextBox = $('#view_startdate');
	var endDateTextBox = $('#view_enddate');
	startDateTextBox.datetimepicker({ 
		timeFormat: 'HH:mm z',
		dateFormat: dateformetdata,
		minDate:0,
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
					Materialize.updateTextFields();
			}
			else {
				endDateTextBox.val(dateText);
				Materialize.updateTextFields();
			} 
			$('#view_startdate').focus();
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
			var checkdate = $(this).val();
			if(checkdate != ''){
				$(this).next('span').remove('.btmerrmsg'); 
				$(this).removeClass('error');
				$(this).prev('div').remove();
			} 
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
	endDateTextBox.datetimepicker({ 
		timeFormat: 'HH:mm z',
		dateFormat: dateformetdata,
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
					Materialize.updateTextFields();
			}
			else {
				startDateTextBox.val(dateText);
				Materialize.updateTextFields();
			}
			$('#view_enddate').focus();
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	}).click(function() {
		$(this).datetimepicker('show');
	});
}
{//filter work
	function fieldhidedisplay(uitype,ddname) {
		if(uitype == 2 || uitype == 3 || uitype == 14) {
			$("#"+ddname+"").addClass('validate[required,maxSize[100]]');
		} else if(uitype == 4) {
			$("#"+ddname+"").addClass('validate[required,custom[integer],maxSize[100]]');
		} else if(uitype == 5) {
			$("#"+ddname+"").addClass('validate[required,custom[number],decval[2],maxSize[100]]');
		} else if(uitype == 6) {
			$("#"+ddname+"").addClass('validate[required,custom[number],min[0],max[100],decval[2]]');
		} else if(uitype == 7) {
			$("#"+ddname+"").addClass('validate[required,maxSize[100]]');
		} else if(uitype == 10) {
			$("#"+ddname+"").addClass('validate[required,custom[multimail]]');
		} else if(uitype == 11) {
			$("#"+ddname+"").addClass('validate[required,custom[phone]]');
		} else if(uitype == 12) {
			$("#"+ddname+"").addClass('validate[required,custom[csturl],maxSize[100]]');
		}
	}
}
//filter work for main view module
function mainviewfilterwork(gridname) {
	var columnid =$(".filteractive").val();
	var label =$(".filteractive").find('option:selected').attr('data-label');
	var condition = $("#filtercondition").val();
	var value = $("#filterfinalviewcondvalue").val();
	var valuename = $("#filterfinalviewconid").val();
	{ // get filterdata for verification
		var fid = $("#filterid").val();
		var cname = $("#conditionname").val();
		var fvalue = $("#filtervalue").val();
	}
	var count = filtername.length;
	var usecond = label+' is '+condition+' '+valuename;
	$("#filterconddisplay").append("<span class='innnerfilterresult'>"+usecond+" <i id='filterclear"+count+"' class='material-icons filterdocclsbtn' data-closeid='filterclear"+count+"' data-fileid='"+count+"'>close</i></span>");
	if(filtername.length>0){
		var filterid = $("#filterid").val()+'|'+columnid;
		var filtercondition = $("#conditionname").val()+'|'+condition;
		var filtervalue = $("#filtervalue").val()+'|'+value;
		$("#filterid").val(filterid);
		$("#conditionname").val(filtercondition);
		$("#filtervalue").val(filtervalue);
	} else {
		$("#filterid").val(columnid);
		$("#conditionname").val(condition);
		$("#filtervalue").val(value);
	}
	filtername.push(columnid);
	filtercid.push(condition);
	filtervid.push(value);
	gridname();
	$("#filterclear"+count+"").click(function() {
		var id = $(this).data("fileid");
		//for condition value split
		filtername.splice($.inArray(filtername[id],filtername),1);
		var fname = filterarrayget(filtername);
		$("#filterid").val(fname);
		//console.log($("#filterid").val());
		//for filter id split
		filtercid.splice($.inArray(filtercid[id],filtercid),1);
		var fid = filterarrayget(filtercid);
		$("#conditionname").val(fid);
		//console.log($("#conditionname").val());
		//for filter value
		filtervid.splice($.inArray(filtervid[id],filtervid),1);
		var fval = filterarrayget(filtervid);
		$("#filtervalue").val(fval);
		//console.log($("#filtervalue").val());
		$(this).parent("span").remove();
		$(this).next("br").remove();
		$(this).remove();
		gridname();
	});
	$(".columnfilter").select2('val','');//filterddcondvalue
	$("#filtercondition").select2('val','');
	$("#filterddcondvalue").select2('val','');
	$("#filtercondvalue").val('');
	$("#filterfinalviewcondvalue").val('');
}
//dropdownfilterwork
function dropdownfilterchange(gridname) {
	var ddlength = $("#filterdropdownlenth").val();
	var modulefieldid = ''
	var filtervalue = '';
	var filtercondition = '';
	for(var i=0;i<ddlength;i++) {
		var uitype = $("#ddfilerchange"+i+"").attr('data-uitype');
		if(uitype == 13) {
			var id = $("#ddfilerchange"+i+"").attr('data-modulefieldid');
			var condition = 'Equalto';
			var value = $("#checkbox"+i+"").val();
			if(value == 'No') {
				value = '';
			}
		} else {
			var id = $("#ddfilerchange"+i+"").attr('data-modulefieldid');
			var value = $("#ddfilerchange"+i+"").val();
			var condition = 'IN';
		}
		if(value != ''){
			if(modulefieldid != '') {
				modulefieldid = modulefieldid+'|'+id;
			} else {
				modulefieldid = id;
			}
			if(filtervalue != '') {
				filtervalue = filtervalue+'|'+value;
			} else {
				filtervalue = value;
			}
			if(filtercondition != '') {
				filtercondition = filtercondition+'|'+condition;
			} else {
				filtercondition = condition;
			}
		}
	}
	$("#ddfilterid").val(modulefieldid);
	$("#ddconditionname").val(filtercondition);
	$("#ddfiltervalue").val(filtervalue);
	gridname();
}
//dashboard module type filter work
function dashboardmodulefilter(gridname,modulename) { 
	var gridfunctionname = gridname.name;
	var columnid =$("#"+modulename+"filtercolumn").val();
	var label =$("#"+modulename+"filtercolumn").find('option:selected').attr('data-label');
	var condition = $("#"+modulename+"filtercondition").val();
	var value = $("#"+modulename+"finalfiltercondvalue").val();
	var valuename = $("#"+modulename+"filterfinalviewconid").val();
	var count = filtername.length;
	var usecond = label+' is '+condition+' '+valuename;
	$("#"+modulename+"filterconddisplay").append("<span class='innnerfilterresult'>"+usecond+" <i class='material-icons "+modulename+"filterdocclsbtn"+count+"' data-fileid='"+count+"'>close</i></span>");
	if(filtername.length>0) {
		var filterid = $("#"+modulename+"filterid").val()+'|'+columnid;
		var filtercondition = $("#"+modulename+"conditionname").val()+'|'+condition;
		var filtervalue = $("#"+modulename+"filtervalue").val()+'|'+value;
		$("#"+modulename+"filterid").val(filterid);
		$("#"+modulename+"conditionname").val(filtercondition);
		$("#"+modulename+"filtervalue").val(filtervalue);
		
	} else {
		$("#"+modulename+"filterid").val(columnid);
		$("#"+modulename+"conditionname").val(condition);
		$("#"+modulename+"filtervalue").val(value);
	}
	filtername.push(columnid);
	filtercid.push(condition);
	filtervid.push(value);
	var page = $("#"+gridfunctionname+"footer .paging").data('pagenum'); 
	var rowcount = $("#"+gridfunctionname+"footer .pagerowcount").data('rowcount'); 
	gridname();
	$("."+modulename+"filterdocclsbtn"+count+"").click(function() {
		var id = $(this).data("fileid");
		var count = filtername.length;
		/* for(var k=0;k<count;k++) {
			if( k == id) { */
			
				filtername.splice($.inArray(filtername[id],filtername),1);
				var fname = filterarrayget(filtername);
				$("#"+modulename+"filterid").val(fname);
				filtercid.splice($.inArray(filtercid[id],filtercid),1);
				var fid = filterarrayget(filtercid);
				$("#"+modulename+"conditionname").val(fid);	
				filtervid.splice($.inArray(filtervid[id],filtervid),1);
				var fval = filterarrayget(filtervid);
				$("#"+modulename+"filtervalue").val(fval);
				
				$(this).parent("span").remove();
				$(this).next("br").remove();
				$(this).remove();
				gridname();
			//}
		//}
	});
	$("#"+modulename+"filtercolumn").select2('val','');
	$("#"+modulename+"filtercondition").select2('val','');
	$("#"+modulename+"filterddcondvalue").select2('val','');
	$("#"+modulename+"filtercondvalue").val('');
	$("#"+modulename+"filterfinalviewcondvalue").val('');
}
//filter value data fetch with pipe line symbol seperate
function filterarrayget(filterdata) {
	var data = '';
	for(var k=0;k<filterdata.length;k++) {
		if(data != '') {
			data = data+'|'+filterdata[k];
		} else {
			data = filterdata[k];
		}
	}
	return data;
}
function purityshowhide(purityname,purityid){
	$("#"+purityname+" option").addClass("ddhidedisplay");
	$("#"+purityname+" optgroup").addClass("ddhidedisplay");
	$("#"+purityname+" option").prop('disabled',true);
	if(purityid == ''){
		$("#"+purityname+" option").removeClass("ddhidedisplay");
		$("#"+purityname+" optgroup").removeClass("ddhidedisplay");
		$("#"+purityname+" option").prop('disabled',false);
	}else{
		if(purityid.indexOf(',') > -1) {
			var purity = purityid.split(',');
			for(j=0;j<(purity.length);j++) {
				$("#"+purityname+" option[value="+purity[j]+"]").removeClass("ddhidedisplay");
				$("#"+purityname+" option[value="+purity[j]+"]").closest('optgroup').removeClass("ddhidedisplay");
				$("#"+purityname+" option[value="+purity[j]+"]").prop('disabled',false);
			}
		}else{
			var purity = purityid;
			$("#"+purityname+" option[value="+purity+"]").removeClass("ddhidedisplay");
			$("#"+purityname+" option[value="+purity+"]").closest('optgroup').removeClass("ddhidedisplay");
			$("#"+purityname+" option[value="+purity+"]").prop('disabled',false);
		}
	}
}
function iconhideshow(id,status){
	 if(status == 'YES'){
		 $('#'+id+'').show();
	 }else{
		 $('#'+id+'').hide();
	 }
}
//validate greater-grosswt>stonewt groswt>netwt
function validategreater(field){
	var input_low = field.attr('id');
	var input_high = field.data('validategreat');
	
	setzero([input_low,input_high]);	//set zero on NULL/EMPTY 
	
	var input_high_val = parseFloat($('#'+input_high+'').val());
	var input_low_val = parseFloat($('#'+input_low+'').val());
	if(purchasewastage != 'Yes' && wastagecalctype != '2') {
		if(input_low_val > input_high_val){
			return "cannot be greater than "+input_high+"";
		}
	}
}
function adddataattrfrotabindextodd(){
	var tbi = 100;	
	$(":input").each(function (i) { $(this).attr('tabindex', tbi + 1); tbi++; });
	$("#search").attr("tabindex","500");
	 $("body").find("select").each(function (i) {
		var ddid = $(this).attr('id');
		var ddidtabindex = $('#s2id_'+ddid+' :input[role=button]').attr("tabindex");
		$('#'+ddid+'').attr('data-tabindexvalue', ddidtabindex);
	}); 
}	
function additionalmobilegriduisettings(){//Mobile view records - GRA 
	 var deviceas = deviceinfo;
	/*  if(deviceas == "phone") {
		{// Random color code generate for  mobile view icons in records
			var colors = ['#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5','#2196F3','#03A9F4','#00BCD4','#546e7a','#4CAF50','#8BC34A','#CDDC39','#FFEB3B','#FFC107','#FF9800','#FF5722'];
			$('.random-bgcolor').each(function() {
			    $(this).css('background-color', colors[Math.floor(Math.random() * colors.length)]);
			});
		}
		{//Grid Records slidUp - GRA
			var items = $( ".data-rows" ); 
			for ( var i=0; i < items.length; i++ ) {
			    // get function in closure, so i can iterate
			    var toggleItemMove = getToggleItemMove( i );
			    // stagger transition with setTimeout
			    setTimeout( toggleItemMove, i * 50 );
			  }
			function getToggleItemMove( i ) {
			  var item = items[i];
			  return function() {
				  $(item).addClass('slideUp');
			  }
			}
		}
	 } */
}
// left menu open - GRA
function openleftMenusside(){
	if ($('#sidenav-overlay').length === 0) {
        var overlay = $('<div id="sidenav-overlay"></div>');
        overlay.css('opacity', 0).click( function(){
          removeMenusside();
          leftpanelheight();
          overlay.velocity({opacity: 0}, {duration: 300, queue: false, easing: 'easeOutQuad',
	             complete: function() {
	               $(this).remove();
	             } });
          $('#search').focus();
        });
        $('body').append(overlay);
        // Percentage overlay
        $('#sidenav-overlay').velocity({opacity: 1 }, {duration: 300, queue: false, easing: 'easeInQuad'});
      }
	  
	if($('.side-nav').hasClass('fixed-open')){
		leftpanelheight();
	}
	else
	{
		$('.side-nav').toggleClass('fixed-open');
		leftpanelheight();
	}
}
// left menu close - GRA
function removeMenusside(overlay){
	$('.side-nav').toggleClass('fixed-open');
	$('#slide-out').css('left','-70px');
	$('.sub-menu').each(function(i, obj) {
		$('.side-nav .sub-menu').removeClass('open-submenu');
	});
	$('.main-menu li.main-category > a').each(function(i, obj) {
		$(this).removeClass('active-category');
	});
	$('#search').val('');
	$("#appendvalue").html('');
}
//left menu pannel height set
function leftpanelheight(){
	var topmenupanelht =  $(window).height();
	var titlebarht = $('#slide-out .lm-title-bar:visible').outerHeight();
	var searchbarht = $('.search:visible').outerHeight();
	var scrollnavht = 0;//$('.scroll-nav').outerHeight();
	var remaintopmenupanelht = topmenupanelht - titlebarht - scrollnavht;
	$(".topmenu-panel").css('height',remaintopmenupanelht);
}
// scroll leftmenu
function scrollleftmenu(){
 $('.moveup').click(function(){
	 var scrolled = $('.topmenu-panel').scrollTop();
	  scrolled = scrolled+55;
	 $('.topmenu-panel').animate({scrollTop: scrolled}); 
 });
  $('.movedown').click(function(){
	  var scrolled = $('.topmenu-panel').scrollTop();
	   scrolled = scrolled-55;
	 $('.topmenu-panel').animate({scrollTop: scrolled}); 
 });
}
//editer value fetch
function editordatafetch(froalaarray,fdata) {
	var filename = [];
	var editorname = froalaarray[0].split(',');
	if(editorname != ''){
		for(var i=0;i<editorname.length;i++) {
			filename[i] =  fdata[editorname[i]+'filename'];
		}
		if(filename.length > 0) {
			$.ajax({
				url: base_url + "Base/editervaluefetch",
				data: "filename="+filename,
				dataType:'json',
				type: "POST",
				async:false,
				cache:false,
		        success: function(data) {
		        	for(var j=0;j<data.length;j++) {
		        		$("#"+editorname[j]+'filename').val();
		        		froalaedit(data[j],editorname[j]);
		        	}
				},
			});		
		}
	}
}
//for mini dashboard
function minidashboardshow(datarowid,notestatus){
	$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
	$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
	$.getScript(base_url+'js/Home/home.js',function(){
	  dynamicchartwidgetcreatemini(datarowid,notestatus);
	});
}
//Mini - conversation in filter content
function miniloadconversationlog(divid,moduleid,datarowid)	{
	createupdateconversation(divid);
	getconversationminidashboarddata(divid,moduleid,datarowid);
}
//create and update conversation data
function createupdateconversation(divid){
	$('#postsubmit').click(function(){
		var conversationmsg = $('#homedashboardconvid').val();
		var conversationtt = $('#homedashboardtitle').val();
		var file=$('#cc_filepath').val();
		var filename=$('#cc_filename').val();
		var privacy=$('#c_privacy').val();
		var moduleid = $("#minimoduleid").val();
		var datarowid = $("#minicommonid").val();
		if(conversationmsg != "") {
			$.ajax({
				url:base_url+'Home/conversationcreate',				
				data:{convmsg:conversationmsg,convtitle:conversationtt,moduleid:moduleid,rowid:datarowid,privacy:privacy,file:file,fname:filename},
				type:"POST",
				async:false,
				cache:false,
				success :function(msg) {
					var nmsg = $.trim(msg);
					if(nmsg == true) {
						$('#unique_rowid').val('');
						$('#homedashboardconvid').val('');
						$('#homedashboardtitle').val('');
						$('#cc_filepath').val('');
						$('#c_privacy').val('');
						$('#c_fileid').text('');
						getconversationminidashboarddata('noteswidget',moduleid,datarowid);					
					}
				}
			});
		}
	});
	//Edit post submit
	$('#editpostsubmit').click(function() {
		//get the reply data  
		var convrowid=$('#unique_rowid').val();
		var conversationmsg = $('#homedashboardconvid').val();
		var conversationtt = '';
		var file=$('#cc_filepath').val(); 
		var privacy=$('#c_privacy').val();
		var filename = $('#c_fileid').text();
		var moduleid = $("#minimoduleid").val();
		var datarowid = $("#minicommonid").val();
		if(convrowid) {
			$.ajax({
				url:base_url+"Home/updateconversationdata",
				data:{rowid:convrowid,moduleid:moduleid,c_message:conversationmsg,convtitle:conversationtt,privacy:privacy,file:file,filename:filename},
				type:"POST",
				async:false,	
				cache:false,
				success :function(msg) {
					$('#unique_rowid').val('');
					$('#homedashboardconvid').val('');
					$('#homedashboardtitle').val('');
					$('#cc_filepath').val('');
					$('#c_privacy').val('');
					$('#c_fileid').text('');
					$(".addtitlestyle").removeClass('hidedisplay');
					$('.titleinputspan').addClass('hidedisplay');
					$('#editpostsubmit,#postcancel').addClass('hidedisplay');
					$('#postsubmit').removeClass('hidedisplay');
					$('.convfileupload').hide();
					//reload the div
					getconversationminidashboarddata('noteswidget',moduleid,datarowid);	
				}
			});
		}
	});
	{// file upload
		$("#convmulitplefileuploader").hide();
		$(".ajax-file-upload-filename").hide();
		$(".ajax-upload-dragdrop").hide();
		setTimeout(function(){
			$(".ajax-file-upload-container").hide();
			$(".ajax-file-upload-filename").hide(); 
		}, 100);
		fileuploadmoname = 'convfileupload';
		var conversationsettings = {
			url: base_url+"upload.php",
			method: "POST",
			multiple:false,
			showStatusAfterSuccess: false,
			fileName: "myfile",
			allowedTypes: "*",
			dataType:'json',
			async:false,
			onSuccess:function(files,data,xhr) {
				if(data != "MaxSize" && data != "Size" && data != "MaxFile"){
					var arr = jQuery.parseJSON(data);	
					var c_filepath=arr.path;
					$(".convfileupload").show();
					$('#cc_filepath').val(c_filepath);
					$('#c_fileid').text(arr.fname);
				}else if(data == "Size"){
					alertpopupdouble("File Size exceeds the limit. You can upload upto 5mb at a time.");
				} else if(data == "MaxSize"){
					alertpopupdouble("You have reached your maximun storage limit. if you want to upload documents you want to buy the storage limit from add ons");
				} else if(data == "MaxFile"){
					alertpopupdouble("Maximum number fo files exceeded.");
				}else{
					alertpopupdouble("Upload Failed");
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#convmulitplefileuploader").uploadFile(conversationsettings);
		$(".uploadattachments").click(function(){
			$(".ajax-file-upload>form>input:first").trigger('click');	//$(".triggeruploadconvfileupload:last").trigger('click');
		});
		{//file upload removal-
			$('#removefile').click(function(){	
				$('#c_fileid').text('');
				$('#cc_filepath').val('');
				$('#cc_filename').val('');
				$(".convfileupload").hide();
			});
		}
	}
}
//get the conversation data based on the record
function getconversationminidashboarddata(divid,moduleid,datarowid) {
	$("#minimoduleid").val(moduleid);
	$("#minicommonid").val(datarowid);
	$.ajax({
		url:base_url+"Home/getconversationdata",
		data: {lastnode:0,moduleid:moduleid,rowid:datarowid},			
		type:"POST",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {							
			var loop=data.length;	
			conversationdatablogcreation(data,moduleid,datarowid);				
		}
	});
}
//conversation data show
function conversationdatablogcreation(data,moduleid,datarowid) {
	var conversationappend=$("#noteswidget");
	conversationappend.empty('');
	var employeeurl=base_url+'Employee';
	for(var i=0; i<data.length;i++) {
		if(data[i]['file']) {
			var file=data[i]['file'];
			var filefolder=base_url+data[i]['filepath'];
			var document= '<div class="large-6 columns" style="text-align:left"><span class="convfiledownload">Attached File : <a class="c_filedownload" data-file="'+filefolder+'">'+file+'</a></span></div>';
		} else {
			var document= '<div class="large-6 columns" style="text-align:left"></div>';
		}
		if(data[i]['time']){
			var time = data[i]['time'];
		} else {
			var time = 'Now';
		}
		if (data[i]['editable'] == "yes") {
			var editablefield='<span><span class="widgetediticon c_edit" data-rowid="'+data[i]['c_id']+'" title="Edit"><i class="material-icons">edit</i></span></span><span><span class="widgetdeleteicon c_delete" data-rowid="'+data[i]['c_id']+'" title="Delete"><i class="material-icons">delete</i></span></span>'
		}
		conversationappend.append('<li class="member-data"><div class="member-detail"><span>'+data[i]['username']+' ('+time+')</span><span>'+data[i]['c_name']+'</span></div><div class="member-image">'+data[i]['username'].substr(0, 1)+'<span class=""></span></div><span class="iconset hidedisplay" style="">'+editablefield+'</span></li>');
	}
	//For Icon hover 
	$(".member-data").hover(function() {
		$(this).find('.iconset').removeClass('hidedisplay');
	}, function() {
		$(this).find('.iconset').addClass('hidedisplay');
	});
	//conversation delete
	$('.c_delete').click(function() {
		var c_delete_rowid=$(this).attr("data-rowid");
		if(c_delete_rowid !='' ||c_delete_rowid != null){
			$.ajax({
				url:base_url+"Home/conversationdelete",
				data:{rowid:c_delete_rowid},
				type:"POST",
				async:false,
				cache:false,
				success :function(msg) {
					getconversationminidashboarddata('noteswidget',moduleid,datarowid);
				}
			});
		}
	});
	//conversation editdata
	$('.c_edit').click(function() {
		var actionid=$(this).attr("data-actionid");
		var c_edit_rowid=$(this).attr("data-rowid");
		var i=$(this).attr("data-eq");
		$('#postsubmit').addClass('hidedisplay');
		$("#editpostsubmit").removeClass('hidedisplay');
		$('#postcancel').removeClass('hidedisplay');
		$('#cc_filepath').val('');
		$('#c_fileid').text('');
		if(c_edit_rowid !='' ||c_edit_rowid != null){
			$.ajax({
				url:base_url+"Home/getconversationeditdata",
				data:{rowid:c_edit_rowid},
				type:"POST",
				async:false,
				dataType:'json',
				cache:false,
				success :function(data) {
					//set the data
					setTimeout(function() {
						$("#homedashboardtitle").val(data.c_title);
						$("#homedashboardconvid").val(data.c_message);
						$('#unique_rowid').val(data.c_rowid);
						if(checkValue(data.c_file) == true){
							$('.convfileupload').show();
							$('#cc_filepath').val(data.c_filepath);
							$('#c_fileid').text(data.c_file);
						} else {
							$("#removefile").trigger('click');
						}
					}, 100);
					$('#homedashboardconvid').val(data.c_name);
					$('.addtitlestyle').trigger('click');
				}
			});
		}
		//scrollbarclass
		$('.c_scroll').animate({scrollTop:0},'slow');
	});
	$('.mention').smention(""+base_url+""+"Home/getuser",{
		after : " "
	});
}
function counterassignuservalid(val,column,table){
	$.ajax({               
		url:base_url+"Base/assignuservalid",
		data: "id="+val+'&columnname='+column+'&table='+table,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success:function (data)	{
			if(data.rows == 0){
			}else{
				alertpopup('This user already assigned.Please choose another user');
				$('#employeeid').select2('val','');
				return false();
			}
		}
	});
}
function itemsalestagentry(moduleid) {
	$.ajax({
		url: base_url + "Base/stockentrytransactionidfetch",
		data: "moduleid=" + moduleid,
		type: "POST",
		cache:false,
		success: function(msg) {
			$('#processoverlay').hide();
			alertpopup('Stock details Added');
		},
	});
	
}
//filter data fetch and set
function getfilterdatas(moduleid,gridid) {
	$.ajax({
		url: base_url + "Base/basemodulefilterdatafetch",
		data: "moduleid=" + moduleid+"&gridid="+gridid,
		type: "POST",
		cache:false,
		success: function(msg) {
			$("#filterviewdivhid").empty();
			$("#filterviewdivhid").append(msg);
			setTimeout(function() {
				$('.chzn-select').select2();
				//filter
				$("#filterddcondvaluedivhid").hide();
				$("#filterassignddcondvaluedivhid").hide();
				$("#filterdatecondvaluedivhid").hide();
				$(".searchfilteroverlay li .s2resetbtn").empty();
				$(".filterclose1").on("click", function() {
					$("#filtericon").trigger("click");
				});
				$('#closefiltertoggle').click(function() {
					$("#mainviewgriddivhid").attr('class','large-12 medium-12 columns paddingzero fullgridview');
					$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero hidedisplay');
				});
				// filter tab show
				$('#filterview').click(function() {
					$('.dashboardview,.mininotesview').addClass('hidedisplay');
					$('#filterview').addClass('filertabbgcolor');
					$('#minidashboard,#mininotes').removeClass('filertabbgcolor');
					$('.filterview1').removeClass('hidedisplay');
					$(".tab-titlemini").removeClass('active');
					$("#fiteractive").addClass('active');
				});
				$('#minidashboard').click(function() {
					var griddataid = $('#minidashboard').attr("data-griddataid");
					minifitertype = 'dashboard';
					var datarowid = $('#'+griddataid+' div.gridcontent div.active').attr('id');
					if (datarowid) {
						$(".tab-titlemini").removeClass('active');
						$("#dashboardactive").addClass('active');
						$('#minidashboard').addClass('filertabbgcolor');
						$('#filterview,#mininotes').removeClass('filertabbgcolor');
						$('.dashboardview').removeClass('hidedisplay');
						$('.filterview1,.mininotesview').addClass('hidedisplay');
						minidashboardshow(datarowid,0); // dashboard tab
					} else {
						$(".dashboardmini").removeClass('active');
						$(".filtermini").addClass('active');
						alertpopup("Please select a row");
					}
				});
				$('#mininotes').click(function() {
					var griddataid = $('#viewtoggle').attr("data-gridid");
					minifitertype = 'notes';
					var datarowid = $('#'+griddataid+' div.gridcontent div.active').attr('id');
					if (datarowid) {
						$(".tab-titlemini").removeClass('active');
						$("#notiactive").addClass('active');
						$('#mininotes').addClass('filertabbgcolor');
						$('#filterview,#minidashboard').removeClass('filertabbgcolor');
						//$("#processoverlay").fadeIn();
						var viewmoduleids = $("#viewfieldids").val();
						$('.mininotesview').removeClass('hidedisplay');
						$('.dashboardview,.filterview1').addClass('hidedisplay');
						miniloadconversationlog('noteswidget',viewmoduleids,datarowid); // notes tab
						//$("#processoverlay").fadeOut();
					} else {
						$(".notesmini").removeClass('active');
						$(".filtermini").addClass('active');
						alertpopup("Please select a row");
					}
				});
				$(".ddfilerchange").change(function() {
					dropdownfilterchange(gridid);
				});
				$('.checkboxcls').click(function() {
					var name = $(this).data('hidname');
					if ($(this).is(':checked')) {
						$('#'+name+'').val('Yes');
					} else {
						$('#'+name+'').val('No');
					}
				});
				mainfilterdropdownchange();
				filtername = [];
				filtercid=[];
				filtervid=[];
				$("#filteraddcondsubbtn").click(function() {
					//mainviewfilterwork(gridid);
					$("#filterformconditionvalidation").validationEngine("validate");	
				});
				 $("#filterformconditionvalidation").validationEngine({
					onSuccess: function() {
						mainviewfilterwork(gridid);
					},
					onFailure: function() {
						alertpopup(validationalert);
					}
				});
			},20);
			$('#processoverlay').hide();
		},
	});
}
//For main view with grid modules(Only for base modules)
function mainfilterdropdownchange() {
	mainfiltervalue = [];
	$(".columnfilter").change(function() {
		var cfvallue = $(this).val();
		$(".columnfilter").select2('val','');
		$(".columnfilter").removeClass('filteractive');
		$(this).select2('val',cfvallue);
		$(this).addClass('filteractive');
		var data = $(this).find("option:selected").data("uitype");
		$('#filtercondition').empty();
		$.ajax({
			url:base_url+"Base/loadconditionbyuitype?uitypeid="+data, 
			dataType:'json',
			async:false,
			success: function(data) { 
				$.each(data, function(index){					
					$('#filtercondition')
					.append($("<option></option>")
					.attr("value",data[index]['whereclausename'])
					.text(data[index]['whereclausename']));
				});	
			}
		});
		var fieldname = $(this).find("option:selected").data("indexname");
		var moduleid = $(this).find("option:selected").data("moduleid");
		var fieldlable = $(this).find("option:selected").data("label");
		if(data == "2" || data == "3" || data == "4"|| data == "5" || data == "6"|| data == "7"|| data == "10" || data == "11" || data == "12"|| data == "14"|| data == "30") {
			showhideintextbox("filterddcondvalue","filtercondvalue");
			$("#filterdatecondvaluedivhid").hide();
			$("#filterassignddcondvaluedivhid").hide();
			$("#filtercondvalue").val("");
			//$("#filtercondvalue").timepicker("remove");
			$("#filtercondvalue").removeAttr('class');
			$("#filterddcondvalue").removeClass("validate[required]");
			$("#filterdatecondvalue").removeClass("validate[required]");
			$("#filterassignddcondvalue").removeClass("validate[required]");
			fieldhidedisplay(data,'filtercondvalue');
		} else if(data == "8") {
			//for hide
			$("#filterdatecondvalue").datetimepicker("destroy");
			$("#filterddcondvaluedivhid").hide();
			$("#filterassignddcondvaluedivhid").hide();
			$("#filtercondvaluedivhid").hide();
			$("#filterdatecondvaluedivhid").show();
			$("#filterdatecondvalue").val("");
			$("#filterddcondvalue").removeClass("validate[required]");
			$("#filtercondvalue").removeAttr('class');
			$("#filterdatecondvalue").addClass("validate[required]");
			$("#filterassignddcondvalue").removeClass("validate[required]");
			if(fieldlable == "Date of Birth" || fieldlable == "Start Date" || fieldlable == "End Date") {
				var dateformetdata = $("#filterdatecondvalue").attr("data-dateformater");
				$("#filterdatecondvalue").datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:0,
					yearRange : "1947:c+100",
					onSelect: function () {
						var checkdate = $(this).val();
						if(checkdate != "") {
							$("#filterfinalviewcondvalue").val(checkdate);
							$("#filterfinalviewconid").val(checkdate);
						}
					},
					onClose: function () {
						$("#filterdatecondvalue").focus();
					}
				}).click(function() {
					$(this).datetimepicker("show");
				});
			} else {
				var dateformetdata = $("#filterdatecondvalue").attr("data-dateformater");
				$("#filterdatecondvalue").datetimepicker({
					dateFormat: dateformetdata,
					showTimepicker :false,
					minDate: null,
					changeMonth: true,
					changeYear: true,
					maxDate:null,
					onSelect: function () {
						var checkdate = $(this).val();
						$("#filterfinalviewcondvalue").val(checkdate);
						$("#filterfinalviewconid").val(checkdate);
					},
					onClose: function () {
						$("#filterdatecondvalue").focus();
					}
				}).click(function() {
					$(this).datetimepicker("show");
				});
			}
		} else if(data == "9") {
			$("#filterdatecondvalue").datetimepicker("destroy");
			//for hide
			$("#filterddcondvaluedivhid").hide();
			$("#filterassignddcondvaluedivhid").hide();
			$("#filtercondvaluedivhid").show();
			$("#filterdatecondvaluedivhid").hide();
			$("#filtercondvalue").val("");
			$("#filterddcondvalue").removeClass("validate[required]");
			$("#filtercondvalue").addClass("validate[required,maxSize[100]]");
			$("#filterdatecondvalue").removeClass("validate[required]");
			$("#filterassignddcondvalue").removeClass("validate[required]");
			$("#filtercondvalue").timepicker({
				"step":15,
				"timeFormat": "H:i",
				"scrollDefaultNow": true,
			});
			var checkdate = $("#filtercondvalue").val();
			if(checkdate != ""){
				$("#filterfinalviewcondvalue").val(checkdate);
				$("#filterfinalviewconid").val(checkdate);
			}
		} else {
			alertpopup("Please choose another field name");
		}
	});
	{//field value set
		$("#filtercondvalue").focusout(function(){
			var value = $("#filtercondvalue").val();
			$("#filterfinalviewcondvalue").val(value);
			$("#filterfinalviewconid").val(value);
		});
		$("#filterddcondvalue").change(function(){
			var value = $("#filterddcondvalue").val();
			mainfiltervalue=[];
			$('#filterddcondvalue option:selected').each(function(){
				var $value =$(this).attr('data-ddid');
				mainfiltervalue.push($value);
				$("#filterfinalviewcondvalue").val(mainfiltervalue);
			});
			$("#filterfinalviewconid").val(value);
		});
		//only for assign to
		$("#filterassignddcondvalue").change(function() {
			var valuename = $("#filterassignddcondvalue").val();
			var id =$('#filterassignddcondvalue').find('option:selected').data('ddid');
			var typeid =$('#filterassignddcondvalue').find('option:selected').data('dataidhidden');
			var value = typeid+'--'+id;
			$("#filterfinalviewcondvalue").val(value);
			$("#filterfinalviewconid").val(valuename);
		});
	}
	//condition dd change
	$("#filtercondition").change(function() {
		var whereid = $("#filtercondition").val();
		var uitype = $(".columnfilter").find("option:selected").data("uitype");
		var fieldname = $(".columnfilter").find("option:selected").data("indexname");
		var moduleid = $(".columnfilter").find("option:selected").data("moduleid");
		if(whereid == 'Between') {
			showhideintextbox("filtercondvalue","filterddcondvalue");
			$("#filterassignddcondvaluedivhid").hide();
			$("#filterdatecondvaluedivhid").hide();
			$("#filterddcondvalue").select2("val","");
			$("#filterddcondvalue").addClass("validate[required]");
			//$("#filterddcondvalue").removeAttr("multiple");
			//$("#filterddcondvalue").attr("multiple","multiple");
			$("#filterdatecondvalue").removeClass("validate[required]");
			$("#filterassignddcondvalue").removeClass("validate[required]");
			$("#filtercondvalue").removeAttr('class');
			$("#filtercondvalue").datepicker("disable");
			fieldnamebesdpicklistddvalue(fieldname,uitype,"filterddcondvalue",moduleid);
		} else {
			showhideintextbox("filterddcondvalue","filtercondvalue");
			$("#filterdatecondvaluedivhid").hide();
			$("#filterassignddcondvaluedivhid").hide();
			$("#filtercondvalue").val("");
			//$("#filtercondvalue").timepicker("remove");
			//$("#filtercondvalue").removeClass("validate[required,maxSize[100]]"); 
			//$("#filtercondvalue").addClass("validate[required,maxSize[100]]");
			$("#filtercondvalue").removeAttr('class');
			$("#filterddcondvalue").removeClass("validate[required]");
			$("#filterdatecondvalue").removeClass("validate[required]");
			$("#filterassignddcondvalue").removeClass("validate[required]");
			fieldhidedisplay(uitype,'filtercondvalue');
		}
	});
}
//get the valuu of the selected column value
function getgridcolcolumnvalue(gridname,rowid,colname) {
	var val=[];
	var datarowid = '';
	$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
		datarowid = $(e).attr('id');
		if($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text() != '') {
			val.push($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
		}
	});
	return val;
}
function getaddinfo(){
		var modtabgrp = $("#modtabgrp").val();
		$.ajax({
			url:base_url+"Base/gridaddfetch?modtabgrp="+modtabgrp,
			async:false,
			cache:false,
			success :function(data) {
			 $('#formeditwizard').append(data);
			$(".chzn-select").select2({blurOnChange: true});
			$(".extra .select2-drop").addClass('containerwidth');
			Materialize.updateTextFields();
				
			},
		}); 
}
function printHtml() {
	$("#printingdiv").printThis();
}
function epsonxmlprint(data,printerip) {
	var url = 'http://'+printerip+'/cgi-bin/epos/service.cgi?devid=local_printer&timeout=10000';
	var req =
		'<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' +
			'<s:Body>' +
				data +
			'</s:Body>' +
		'</s:Envelope>'; 

	// Send print document
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
	xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
	xhr.setRequestHeader('SOAPAction', '""');
	xhr.onreadystatechange = function () {
		// Receive response document
		if (xhr.readyState == 4) {
			// Parse response document
			if (xhr.status == 200) {
				//alert(xhr.responseXML.getElementsByTagName('response')[0].getAttribute('success'));
			}
			else {
				alert('Network error occured.');
			}
		}
	};
	xhr.send(req);

}
 // float value restict based on companysetting round  
 function hasDecimalPlace(value, x) { // restrit decimal value based on weight round from company settings
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}
// To retrieve multiple print ids from sales table.
function retrievemultipleprintid(id) {
	
	
}