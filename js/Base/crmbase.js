/*
* This JS file handles Local GRID data manupulation and For Calculation of User Data
* Use for Quote,SalesOrder,Invoice,PurchaseOrder,Material Requisition
*
*/
$(document).ready(function() {
	{// Price book/currencyconversion overlay close
		$("#pricebookclose").click(function()
		{
			$("#pricebookoverlay").fadeOut();
		});
		$("#currencyconvclose").click(function()
		{
			$("#currencyconvoverlay").fadeOut();
		});
		$("#productsearchclose").click(function()
		{
			$("#productsearchoverlay").fadeOut();
		});
	}
});
//**Functions*//
{// Price book Overlay grid
	function pricebookgrid(page,rowcount) {
		var pid = $("#productid").val()!='' ? $("#productid").val() : 1;
		var territory = $("#territoryid").val() ? $("#territoryid").val() : 1;
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#pricebookgrid').width();
		var wheight = $('#pricebookgrid').height();
		/* col sort */
		var sortcol = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.pricebooklistheadercolsort').hasClass('datasort') ? $('.pricebooklistheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/pricebookview?maintabinfo=pricebookdetail&primaryid=pricebookdetailid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=219&productid='+pid+'&territory='+territory,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#pricebookgrid').empty();
				$('#pricebookgrid').append(data.content);
				$('#pricebookgridfooter').empty();
				$('#pricebookgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('pricebookgrid');
				/* hide grid column */
				var hideprbokcol = ['currencyid','pricebookdetailid'];
				gridfieldhide('pricebookgrid',hideprbokcol);
				{/* sorting */
					$('.pricebooklistheadercolsort').click(function(){
						$('.pricebooklistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					sortordertypereset('pricebooklistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#pricebooklistpgnumcnt li .page-text .active').data('rowcount');
						pricebookgrid(page,rowcount);
					});
					$('#pricebooklistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#pricebooklistpgnum li.active').data('pagenum');
						pricebookgrid(page,rowcount);
					});
				}
			},
		});
	}
	// Product search Overlay grid
	function productsearchgrid(page,rowcount) {
		/*pagination*/
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#productsearchgrid').width();
		var wheight = $('#productsearchgrid').height();
		/* col sort */
		var sortcol = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.productlistheadercolsort').hasClass('datasort') ? $('.productlistheadercolsort.datasort').attr('id') : '0';
		if(softwareindustryid == 2){
			var ordertype = 1;
		}else{
			var ordertype = $('#ordertypeid').val();
		}
		var moduleid = 9;
		if(softwareindustryid == 1){
			moduleid = 9;
		}else if (softwareindustryid == 2){
			moduleid = 98;
		}else if (softwareindustryid == 3){
			moduleid = 90;
		}else if (softwareindustryid == 4){
			moduleid = 89;
		}
		if(MODULEID == 225 || MODULEID == 99 || MODULEID == 88 ){
			ordertype = 0;
		}
		$.ajax({
			url:base_url+"Base/getproductsearch?maintabinfo=product&primaryid=productid&page="+page+"&records="+rowcount+"&ordertype="+ordertype+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid='+moduleid+'&industryid='+softwareindustryid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#productsearchgrid').empty();
				$('#productsearchgrid').append(data.content);
				$('#productsearchgridfooter').empty();
				$('#productsearchgridfooter').append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('productsearchgrid');
				{/* sorting */
					$('.productlistheadercolsort').click(function(){
						$('.productlistheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					sortordertypereset('productlistheadercolsort',headcolid,sortord);
				}
				{/* pagination */
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#productlistpgnumcnt li .page-text .active').data('rowcount');
						productsearchgrid(page,rowcount);
					});
					$('#productlistpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#productlistpgnum li.active').data('pagenum');
						productsearchgrid(page,rowcount);
					});
				}
			},
		});
	}
	// module bumber search Overlay grid
	function modulenumbersearchgrid(moduleid,page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $('#modulenumbersearchgrid').width();
		var wheight = $('#modulenumbersearchgrid').height();
		//col sort
		var sortcol = $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.modulenumberheadercolsort').hasClass('datasort') ? $('.modulenumberheadercolsort.datasort').attr('id') : '0';
		$.ajax({
			url:base_url+"Base/getmoduledatanumber?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+"&moduleid="+moduleid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$('#modulenumbersearchgrid').empty();
				$('#modulenumbersearchgrid').append(data.content);
				$('#modulenumbersearchgridfooter').empty();
				$('#modulenumbersearchgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				/*column resize*/
				columnresize('modulenumbersearchgrid');
				{//sorting
					$('.modulenumberheadercolsort').click(function(){
						$('.modulenumberheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					sortordertypereset('modulenumberheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pagerowcount').click(function(){
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						var rowcount = $(this).data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('ul#modulenumberpgnumcnt li .page-text .active').data('rowcount');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
					$('#modulenumberpgrowcount').change(function(){
						var rowcount = $(this).val();
						var page = $('ul#modulenumberpgnum li.active').data('pagenum');
						modulenumbersearchgrid(moduleid,page,rowcount);
					});
				}
			}
		});
	}
}
{//grid-common
	//charges gridheader details load
	function chargesgrid() {
		var wwidth = $("#chargesgrid").width();
		var wheight = $("#chargesgrid").height();
		$.ajax({
			url:base_url+"Quote/chargegirdheaderinformationfetch?moduleid=223&width="+wwidth+"&height="+wheight+"&modulename=salesorderchargegrid",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#chargesgrid").empty();
				$("#chargesgrid").append(data.content);
				$("#chargesgridfooter").empty();
				$("#chargesgridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('chargesgrid');
				/* Calculate total charges */
				charge_data_total();
			},
		});
	}
	//tax grid header details load
	function taxgrid() {
		var wwidth = $("#taxgrid").width();
		var wheight = $("#taxgrid").height();
		$.ajax({
			url:base_url+"Quote/taxgirdheaderinformationfetch?moduleid=221&width="+wwidth+"&height="+wheight+"&modulename=salesordertaxgrid",
			dataType:'json',
			async:false,
			cache:false,
			success:function(data) {
				$("#taxgrid").empty();
				$("#taxgrid").append(data.content);
				$("#taxgridfooter").empty();
				$("#taxgridfooter").append(data.footer);
				/* data row select event */
				datarowselectevt();
				/* column resize */
				columnresize('taxgrid');
				/* Calculate total taxes */
				tax_data_total();
			},
		});	
	}
}
	/*
	*It clear the grid rows in all the inner tab -grids
	*/
	function clearformgriddata() {
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++)
		{
			cleargriddata(gridnames[j]);
		}
	}
	/*
	*It resets any existing tax/charge/grid data
	*/
	function reset_quote_tax_charge(){
		$('#taxgriddata,#chargegriddata,#discountdata').val(''); //reset the data
		$('#grouptaxgriddata,#groupchargegriddata,#groupdiscountdata').val(''); //reset the data
		$('#taxcategory,#chargecategory').select2('val',''); //reset the category
		cleargriddata('taxgrid');
		cleargriddata('chargesgrid');
	}
	/*
	*Charge grid on submit
	*/
	function submitchargegrid(){		
		//identify the type(group/individual);
		var additionalchargecategoryid=$('#additionalchargecategoryid').val();
		var type = '';
		if(additionalchargecategoryid){
			type = 3;
		}else{
			type = 2;
		}
		var totalcharge =  parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
		if(type == 2) {	//individual
			var griddataid = 'chargegriddata';
			var charge_category_id = 'chargecategory';
			var charge_amount_field = 'chargeamount';
		} else if(type == 3) {	//group
			var griddataid = 'groupchargegriddata';
			var charge_category_id = 'additionalchargecategoryid';
			var charge_amount_field = 'groupchargeamount';
		}
		var charge_json ={}; //declared as object
		charge_json.id = $('#'+charge_category_id+'').val(); //categoryid
		charge_json.data = getgridrowsdata('chargesgrid');//chargesgrid
		if(deviceinfo != 'phone'){
			$('#'+griddataid+'').val(JSON.stringify(charge_json));
		}
		$('#'+charge_amount_field+'').val(totalcharge.toFixed(PRECISION));
		$('#additionaloverlay').fadeOut();
		resetoverlay('chargesgrid');
		if(type == 2){	//individual
			$('#quantity').trigger('focusout'); //automates recalculation
		} else if(type == 3) {	//group
			gridsummeryvaluefetch('','','');  //recalculate summary
		}
	}
	/*
	*Tax grid on submit
	*/
	function submittaxgrid(){		
		//identify the type(group/individual);
		var type = '';
		var taxmasterid=$('#taxmasterid').val();
		if(taxmasterid){
			type = 3;
		}else{ type = 2;}
		var totalcharge = parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
		if(type == 2){	//individual
			var griddataid = 'taxgriddata';
			var tax_category_id = 'taxcategory';
			var tax_amount_field = 'taxamount';
		} else if(type == 3) {	//group
			var griddataid = 'grouptaxgriddata';
			var tax_category_id = 'taxmasterid';
			var tax_amount_field = 'grouptaxamount';
		}
		var tax_json ={}; //declared as object
		tax_json.id = $('#'+tax_category_id+'').val(); //categoryid
		tax_json.data = getgridrowsdata('taxgrid');//taxgrid
		if(deviceinfo != 'phone'){
		$('#'+griddataid+'').val(JSON.stringify(tax_json));
		}
		$('#'+tax_amount_field+'').val(totalcharge);
		$('#taxoverlay').fadeOut();
		resetoverlay('taxgrid');		
		if(type == 2) {	//individual
			$('#quantity').trigger('focusout'); //automates recalculation
		} else if(type == 3) {	//group
			gridsummeryvaluefetch('','','');  //recalculate summary
		}
	}
	/*
	* Load the Grid Contents(ie)load reows locally.
	*/
	function loadgriddata(gridid,grid_data){
		cleargriddata(gridid);
		var gridlength = grid_data.length;
		var j=0;
		for(var i=0;i<=gridlength;i++){	
			addinlinegriddata(gridid,j+1,grid_data[i],'json');
			j++;
		} 
	}
	/*
	* Clear the Grid Rows.
	*/
	function resetoverlay(gridid){
		cleargriddata(gridid);
	}
	/*
	*calculate the exchange rate from basecurrency to converted currency
	*/
	function listprice(unitprice,rate){		
		var value = (parseFloat(unitprice)/(1/parseFloat(rate))).toFixed(PRECISION);		
		return value;
	}
	/*
	*Calculates the discount live-single.
	*/
	function discount_total(){	
		setzero(['grossamount','discountpercent']); //sets zero on EMPTY					
		var typeid = $('#discounttypeid').val(); //discount typeid
		var value = $('#discountpercent').val();	//discount values		
		var grossamount = $('#grossamount').val();		
		//
		if(typeid != null && typeid !=''){			
			var discount_amount = parseFloat(value);
			if(typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
			
		} else {			
			var amt_discount =  0;
		}
		$('#singlediscounttotal').val(amt_discount);
		//
	}
	/*
	*Calculates the group discount live-.
	*/
	function group_discount_total(){	
		setzero(['groupdiscountamount','totalnetamount']); //sets zero on EMPTY					
		var typeid = $('#groupdiscounttypeid').val(); //discount typeid
		var value = $('#groupdiscountpercent').val();	//discount values		
		var grossamount = $('#totalnetamount').val();		
		//
		if(typeid != null && typeid !=''){			
			var discount_amount = parseFloat(value);
			if(typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			var amt_discount =  discount_amount.toFixed(PRECISION);
			
		} else {			
			var amt_discount =  0;
		}
		$('#groupdiscounttotal').val(amt_discount);
		//
	}
	/*
	*Calculates the Charge grid-InnerEdit-.
	*/
	function calculateinnerchargeedit(){ 
			var id =  $('#chargesgrid div.gridcontent div.active').attr('id');
			var tmp = getgridcolvalue('chargesgrid',id,'value','');			
			var c_mode = '';
			var additionalchargecategoryid = $('#additionalchargecategoryid').val();
			if(additionalchargecategoryid){
				c_mode = 3;
			}else{
				c_mode = 2;
			}
			if(id != ''){				
				jQuery('#chargesgrid').saveRow(id, true);
				var calctype = getgridcolvalue('chargesgrid',id,'calculationtypeid','');
				var value = getgridcolvalue('chargesgrid',id,'value','');
				if(!isNaN(value) && value !=''){
				setzero(['grossamount']); //set zero on empty
				if(calctype == 3){	//percentage
					if(c_mode == 2){
						var c_amount = parseFloat($('#grossamount').val()); //individual
						var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
					} else if(c_mode == 3) {
						var c_amount = parseFloat($('#totalnetamount').val()); //group
						var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
					}
				}
				} else {
					var value = 0;					
				}			
				setTimeout(function()
				{
					charge_data_total();
				},200);
			} 
	}
	/*
	*Calculates the Tax grid-InnerEdit-.
	*/
	function calculateinnertaxedit(){
		var id = $('#taxgrid div.gridcontent div.active').attr('id');
			if(id != '' ){
				var type = '';
				var taxmasterid=$('#taxmasterid').val();
				if(taxmasterid){
					type = 3;
				}else{ type = 2;}
				jQuery('#taxgrid').saveRow(id, true);				
				var value = getgridcolvalue('taxgrid',id,'rate','');
				if(!isNaN(value) && value != ''){
					if(type == 2){
						setzero(['grossamount','discountamount']); //set zero on empty
						var c_amount = parseFloat($('#grossamount').val())-parseFloat($('#discountamount').val()); //individual
					} else if(type == 3) {
						var c_amount = parseFloat($('#totalnetamount').val())-parseFloat($('#groupdiscountamount').val()); //individual
					}
					var value = ((parseFloat(value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
					
				} else {
					var value = 0;						
				}
				setTimeout(function()
				{
					tax_data_total();
				},200);
			}
	}
	/*
	* Calculates the records of tax and sets it in summary total.
	*/
	function tax_data_total(){	
		var total =  parseFloat(getgridcolvalue('taxgrid','','amount','sum'));
        $('#tax_data_total').val(total.toFixed(PRECISION));
	}
	/*
	* Calculates the records of charge and sets it in summary total.
	*/
	function charge_data_total(){	
		var total =  parseFloat(getgridcolvalue('chargesgrid','','amount','sum'));
		$('#charge_data_total').val(total);
	}	
	/*retrieve the conversion rate(pricebook currency to current currency)*/
	function getcurrencyconversionrate(currencyid,current_currencyid){
		var rate = '';
		if(checkValue(currencyid) == true && checkValue(current_currencyid) == true){
			$.ajax({
			url:base_url+"index.php/Base/getcurrencyconversionrate",
			data:{fromcurrency:currencyid,tocurrency:currencyid},
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
					rate = data.rate;
				}		
			});
			return rate;
		} else {
			return rate;
		}
	}
	/*
	*Function to calculate the discount value
	*@param discountdata(jsonobject) - data contains discount value,discount type 
	*@param grossamount - grossamount
	*/
	function discountcalculation(discountdata,grossamount) {	
		if(discountdata != null && discountdata !=''){			
			var discount_amount = parseFloat(discountdata.value);
			if(discountdata.typeid == 3) //on percentage type.
			{
				var discount_amount = (parseFloat(discount_amount)/100)*parseFloat(grossamount);			
			}			
			return discount_amount.toFixed(PRECISION);
			
		} else {			
			return 0;
		}
	}
	/*
	*to set the sellingprice for Product/item after pricebook selection
	*/
	function setsellingprice(sellingprice){
		var sellingprice =sellingprice.toFixed(PRECISION);
		$('#sellingprice').val(sellingprice).trigger('change'); //sets the value and trigger change to do other calc
	}
	/*
	*It resets any existing tax/charge/grid data
	*/
	function reset_quote_tax_charge(){
		$('#taxgriddata,#chargegriddata,#discountdata').val(''); //reset the data
		$('#grouptaxgriddata,#groupchargegriddata,#groupdiscountdata').val(''); //reset the data
		$('#taxcategory,#chargecategory').select2('val',''); //reset the category
		cleargriddata('taxgrid');//reset the tax-grid data
		cleargriddata('chargesgrid');//reset the charge-grid data
	}
	/*
	*Automatic Recalculations And Datareset of Taxgrid data
	*/
	function update_taxgriddata(){	
		var main_data = $.parseJSON($('#taxgriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != ''){
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum = 0;
				setzero(['grossamount','discountamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var c_amount = parseFloat($('#grossamount').val()); 
					var c_discount = parseFloat($('#discountamount').val()); 
					var c_value = c_main_data[ic]['rate'];
					var value = ((parseFloat(c_value)/100)*(c_amount-c_discount)).toFixed(PRECISION);
					c_main_data[ic]['amount'] = value;			
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				var totalcharge =parseFloat(totalcharge);
				$('#taxamount').val(totalcharge.toFixed(PRECISION));
				var tax_json ={}; //declared as object
				tax_json.id = main_data.id; //categoryid
				tax_json.data = c_main_data;	//tax grid	
				$('#taxgriddata').val(JSON.stringify(tax_json));
			}
		}
	}
	/*
	*	Automatic Recalculations And Datareset of Chargegrid data
	*/
	function update_chargegriddata(){
		var main_data = $.parseJSON($('#chargegriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != ''){
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum=0;
				setzero(['grossamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var value = c_main_data[ic]['amount'];
					if(c_main_data[ic]['calculationtypeid'] == 3){ //percentagetype					
						var c_amount = $('#grossamount').val(); 
						var c_value = c_main_data[ic]['value'];
						var value = ((parseFloat(c_value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
						c_main_data[ic]['amount'] = value;
					}
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#chargeamount').val(totalcharge);
				var charge_json ={}; //declared as object
				charge_json.id = main_data.id; //categoryid
				charge_json.data = c_main_data;	//chargesgrid	
				$('#chargegriddata').val(JSON.stringify(charge_json));
			}
		}
	}
	/*
	* Automatic Recalculations And Datareset of Group Tax Grid
	*/
	function update_group_taxgriddata(){
		var main_data = $.parseJSON($('#grouptaxgriddata').val()); //group taxgrid data
		if(main_data != null && main_data != ''){
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum = 0;
				setzero(['totalnetamount','groupdiscountamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var c_amount = parseFloat($('#totalnetamount').val()); 
					var c_discount = parseFloat($('#groupdiscountamount').val()); 
					var c_value = c_main_data[ic]['rate'];
					var value = ((parseFloat(c_value)/100)*(c_amount-c_discount)).toFixed(PRECISION);
					c_main_data[ic]['amount'] = value;				
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#grouptaxamount').val(totalcharge);
				var tax_json ={}; //declared as object
				tax_json.id = main_data.id; //categoryid
				tax_json.data = c_main_data;	//tax grid	
				$('#grouptaxgriddata').val(JSON.stringify(tax_json));
			}
		}
	}
	/*
	* Automatic Recalculations And Datareset of Group Charge Grid
	*/
	function update_group_chargegriddata(){
		var main_data = $.parseJSON($('#groupchargegriddata').val()); //individual chargegrid data
		if(main_data != null && main_data != ''){
			var c_main_data = main_data.data;
			if(c_main_data.length > 0){
				var datacount = c_main_data.length;
				var c_sum=0;
				setzero(['totalnetamount']); //set zero on empty
				for(var ic=0;ic < datacount;ic++){	//iterates to autoupdate inner records				
					var value = c_main_data[ic]['amount'];
					if(c_main_data[ic]['calculationtypeid'] == 3){ //percentagetype					
						var c_amount = $('#totalnetamount').val(); 
						var c_value = c_main_data[ic]['value'];
						var value = ((parseFloat(c_value)/100)*parseFloat(c_amount)).toFixed(PRECISION);
						c_main_data[ic]['amount'] = value;
					}
					c_sum = parseFloat(c_sum)+parseFloat(value);
				}
				var totalcharge = c_sum.toFixed(PRECISION);
				$('#groupchargeamount').val(totalcharge);
				var charge_json ={}; //declared as object
				charge_json.id = main_data.id; //categoryid
				charge_json.data = c_main_data;	//chargesgrid	
				$('#groupchargegriddata').val(JSON.stringify(charge_json));
			}
		}
	}
	/*
	* reappend currency for dataattribute-(used because there is no data attribute in base dd)
	*/
	function appendcurrency(id) {
		$('#'+id+'').empty();
		$('#'+id+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"index.php/Quote/specialcurrency",
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {				
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-precision ='" +data[m]['decimal']+ "'  value = '" +data[m]['currencyid']+ "'>"+data[m]['currencyname']+"</option>";
					}
					//after run
					$('#'+id+'').append(newddappend);
				}	
			},
		});		
	}
	//
	function resetconversiondata(data){	
		$.fn.getType = function(){return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }
		$.each( data, function( key, vale ) {
			if($('#'+key+'').hasClass('empdd')) {
				var ddval = '2:'+vale;
				$('#'+key+'ddid').select2("val",ddval);
				$('#'+key+'').val(vale);
			} else {
				var value = vale;				
				var type = $('#'+key+'').getType();
				if(type == "select") {
					if( $('#s2id_'+key+'').hasClass('select2-container-multi') ) {
						var mulval = value.split(',');
						$('#'+key+'').select2("val",mulval);
					} else {
						$('#'+key+'').select2("val",value);
					}
				} else if(type == "text") {
					$('#'+key+'').val(value);
				} else if(type == "hidden") {
					$('#'+key+'').val(value);
					if(value == 'Yes') {
						$('#'+key+'cboxid').prop('checked',true);
					} else {
						$('#'+key+'cboxid').prop('checked',false);
					}
				} else if(type == "textarea") {
					$('#'+key+'').val(value);
				}
			}
		});
	}	