$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		callcampaignviewgrid();
		//crud action
		crudactionenable();
	}
	{//inner-form-with-grid
		$("#callcampaignsubingridadd1").click(function(){
			sectionpanelheight('callcampaignsuboverlay');
			$("#callcampaignsuboverlay").removeClass("closed");
			$("#callcampaignsuboverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#callcampaignsubcancelbutton").click(function(){
			$("#callcampaignsuboverlay").removeClass("effectbox");
			$("#callcampaignsuboverlay").addClass("closed");
		});
	}
	//read only for mobile number text area
	$("#communicationto,#description").attr('readonly',true);
	// default arrays
	subscriber =[];
	dndnumbers =[];
	nondndnumbers =[];
	valnumbers =[];
	invnumbers= [];
	$('#formclearicon').click(function(){
		$("#formfields").empty();
		$("#templatetypeid").val('2');
		$("#tab1").trigger('click');
		$("#callgroupsid,#smssegmentsubscriber").val('');
		$("#callgroupsid,#smssegmentsubscriber").select2('val','');
		$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid,#grouptypeid,#groupmoduleid").val('');
		$("#leadtemplatecontentdivhid").find('span').remove();
	});
	//Close Add Screen
	var addcloseoppocreation =["closeaddform","callcampaigncreationview","callcampaigncreationformadd"]
	addclose(addcloseoppocreation);	
	var addcloseviewcreation =["viewcloseformiconid","callcampaigncreationview","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	// For touch
	fortabtouch = 0;
	//Toolbar Icon Change Function View
	$("#reloadicon").click(function() {
		refreshgrid();
	});
	$( window ).resize(function() {
		maingridresizeheightset('callcampaignviewgrid');
		sectionpanelheight('callcampaignsuboverlay');
	});
	$("#callcampaignsubingriddel1,#callcampaignsubaddbutton").hide();
	//group dd change
	$("#callgroupsid").change(function() {
		$("#selectedsegmentsubscriberid,#segmentsubscriberid,#autoupdate").val('');
		$('#callsegmentsid').select2('val','');
		var groupid  = $("#callgroupsid").val();
		//grouptypeget(groupid);
		segmentdropdownvalfetch(groupid);
		//grid load
		callcampaignsubaddgrid1();
		var total = $("#callsubscriberlist-sort > .data-rows").length;
		$("#totalsubscribercount").val(total);
		Materialize.updateTextFields();
	});
	{
		//Call Groups
		$("#groupsiconicon").click(function(){
			var type = 'Call';
			sessionStorage.setItem("callmoduletype",type);
			window.location =base_url+'Smsgroups';
		});
		//Call segments
		$("#segmentsiconicon").click(function(){
			window.location =base_url+'Smssegments';
		});
		//Call Subscribers
		$("#subscribersiconicon").click(function(){
			window.location =base_url+'Smssubscribers';
		});
		//Sounds
		$("#soundsiconicon").click(function(){
			window.location =base_url+'Sounds';
		});
	}
	//close
	$('#closeaddoppocreation').click(function() {
		$('#dynamicdddataview').trigger('change');
	});
	//view by drop down change
	$('#dynamicdddataview').change(function() {
		callcampaignviewgrid();
	});
	//call campaign
	$('#dataaddsbtn').click(function() {
		$("#formaddwizard").validationEngine('validate');
	});
	jQuery("#formaddwizard").validationEngine({
		onSuccess: function() {
			var apikey = $("#callernumber").find('option:selected').data('apikey');
			var caller = $("#callernumber").find('option:selected').data('caller');
			$("#processoverlay").show();
			var formdata= $("#dataaddform").serialize();
			var amp = '&';
			var calldatas = amp + formdata ;
			$.ajax({                       
				url:base_url+"Callcampaign/voicecallcampaign",  
				data: "data="+calldatas+"&apikey="+apikey+"&caller="+caller, 
				dataType:'json',
				async:false,
				cache:false,
				success: function(msg) {
					if(msg == 'Success') {
						resetFields();
						$('#callcampaigncreationformadd').hide();
						$('#callcampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('span').remove();
						refreshgrid();
						$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid").val('');
						$("#processoverlay").hide();
						alertpopup('Voice Call successfully completed. will get a call soon.');
					} else {
						resetFields();
						$('#callcampaigncreationformadd').hide();
						$('#callcampaigncreationview').fadeIn(1000);
						$("#leadtemplatecontentdivhid").find('span').remove();
						refreshgrid();
						$("#segmentsubscriberid,#autoupdate,#selectedsegmentsubscriberid").val('');
						$("#processoverlay").hide();
						alertpopup('Error during the voice call. reload it and try again later.');
					}
				},                
			});  
		},
		onFailure: function() {
			var dropdownid =['2','smstypeid','senderid'];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//segment drop down change
	$('#callsegmentsid').change(function() {
		var smsgroupid = $("#callgroupsid").val();
		if(smsgroupid) {
			var id = $(this).val();
			var selectid = id;
			if(selectid == '') {
				$("#segmentsubscriberid,#autoupdate").val('');
			}
			relatedsubscriberdatafetch(selectid);
			callcampaignsubaddgrid1();
			setTimeout(function() {
				var total = $("#callsubscriberlist-sort > .data-rows").length;
				$("#totalsubscribercount").val(total);
			}, 1000);
		} else {
			$('#callsegmentsid').select2('val','');
			alertpopup('Please Select the Group name');
		}
	});
	$("#tab2").click(function() {
		var campaignname = $("#smscampaignname").val();
		var groupname = $("#smsgroupsid").val();
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(campaignname != '' && groupname != '') {
			if(subscriberid != '') {
				$("#communicationto,#leadtemplatecontent").val('');
				$("#leadtemplatecontentdivhid").find('span').remove();
				mobilenumberget(subscriberid);
				$("#smstypeid").select2('val','2');
				$("#leadtemplatecontent").trigger('focusout');
			} else {
				$("#tab1").trigger('click');
				setTimeout(function() {
					//transtion 
					$('#subformspan1').addClass('form-active');
				},500);
				alertpopup('Please select the Campaign subscribers');
			}
		} else {
			$("#tab1").trigger('click');
			setTimeout(function() {
				//transtion 
				$('#subformspan1').addClass('form-active');
			},500);
			alertpopup('Please select the Campaign name and Group name');
		}
		Materialize.updateTextFields();
	});
	//number type change
	$("#callnumbertypeid").change(function() {
		var numtype = $("#callnumbertypeid").val();
		if(numtype != '') {
			callertnumberfetch(numtype,'callernumber');
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,callcampaignviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			callcampaignviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function callcampaignviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#callcampaignpgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#callcampaignpgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $(window).width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.callcampaignheadercolsort').hasClass('datasort') ? $('.callcampaignheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.callcampaignheadercolsort').hasClass('datasort') ? $('.callcampaignheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.callcampaignheadercolsort').hasClass('datasort') ? $('.callcampaignheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=callcampaign&primaryid=callcampaignid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#callcampaignviewgrid').empty();
				$('#callcampaignviewgrid').append(data.content);
				$('#callcampaignviewgridfooter').empty();
				$('#callcampaignviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('callcampaignviewgrid');
				{//sorting
					$('.callcampaignheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.callcampaignheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#smscampaignpgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.callcampaignheadercolsort').hasClass('datasort') ? $('.callcampaignheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.callcampaignheadercolsort').hasClass('datasort') ? $('.callcampaignheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#callcampaignviewgrid .gridcontent').scrollLeft();
						callcampaignviewgrid(page,rowcount);
						$('#callcampaignviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('callcampaignheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						callcampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#callcampaignpgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						callcampaignviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#callcampaignviewgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#callcampaignpgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			callcampaignsubaddgrid1();
			addslideup('callcampaigncreationview','callcampaigncreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formfields").empty();
			$("#templatetypeid").val('2');
			$('#tab1').trigger('click');
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			//For Touch
			smsmasterfortouch = 0;
			resetFields();
			setTimeout(function(){
				Materialize.updateTextFields();
				//form field first focus
				firstfieldfocus();
			},100);
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#smscampaignpgnum li.active').data('pagenum');
		var rowcount = $('ul#smscampaignpgnumcnt li .page-text .active').data('rowcount');
		callcampaignviewgrid(page,rowcount);
	}
}
//sms master folder  Grid
function callcampaignsubaddgrid1() {
	var callgroupsid = $("#callgroupsid").val();
	if(callgroupsid == ''){ callgroupsid == '1'; }
	var segmentid = $("#callsegmentsid").val();
	var smssegmentsubscriber = $("#segmentsubscriberid").val();
	var autoupdate = $("#autoupdate").val();
	if(smssegmentsubscriber == ''){ smssegmentsubscriber == ''; }
	var id = '271';
	if(id != "") {
		$.ajax({
			url:base_url+"Callcampaign/defaultviewfetch?modid="+id,
			async:false,
			cache:false,
			success:function(data) {
				if(data != "") {
					viewid = data;
					$("#defaultviewcreationid").val(viewid);
				} else {
					viewid = '271';
					$("#defaultviewcreationid").val(viewid);
				}
			},
		});
		$.ajax({
			url:base_url+"Callcampaign/parenttableget?moduleid="+id,
			dataType:'json',
			async:false,
			cache:false,
			success:function(pdata) {
				if(pdata != "") {
					maintabinfo = pdata;
					$("#parenttable").val(pdata);
					parentid = maintabinfo+"id";
					$("#parenttableid").val(parentid);
				} else {
					maintabinfo = 'subscribers';
					parentid = 'subscribersid';
					$("#parenttable").val(maintabinfo);
					$("#parenttableid").val(parentid);
				}
			},
		});
	} else {
		viewid = '271';
		maintabinfo = 'subscribers';
		parentid = 'subscribersid';
		$("#defaultviewcreationid").val(viewid);
		$("#parenttable").val(maintabinfo);
		$("#parenttableid").val(parentid);
	}
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#callcampaignsubaddgrid1').width();
	var wheight = $('#callcampaignsubaddgrid1').height();
	//col sort
	var sortcol = $('.callsubscriberlistheadercolsort').hasClass('datasort') ? $('.callsubscriberlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.callsubscriberlistheadercolsort').hasClass('datasort') ? $('.callsubscriberlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.callsubscriberlistheadercolsort').hasClass('datasort') ? $('.callsubscriberlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Callcampaign/callsubscribervalueget?maintabinfo="+maintabinfo+"&primaryid="+parentid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=271'+"&callgroupsid="+callgroupsid+"&segmentid="+segmentid+"&smssegmentsubscriber="+smssegmentsubscriber+"&autoupdate="+autoupdate+"&checkbox=true",
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#callcampaignsubaddgrid1').empty();
			$('#callcampaignsubaddgrid1').append(data.content);
			$('.subgridfootercontainer').empty();
			$('.subgridfootercontainer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('callcampaignsubaddgrid1');
			{//sorting
				$('.callsubscriberlistheadercolsort').click(function(){
					$('.callsubscriberlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#callsubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#callsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					var sortpos = $('#callcampaignsubaddgrid1 .gridcontent').scrollLeft();
					callcampaignsubaddgrid1(page,rowcount);
					$('#callcampaignsubaddgrid1 .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('callsubscriberlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#callsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					callcampaignsubaddgrid1(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#callsubscriberlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					callcampaignsubaddgrid1(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#callsubscriberlistpgnumcnt li .page-text .active').data('rowcount');
					callcampaignsubaddgrid1(page,rowcount);
				});
				$('#callsubscriberlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#callsubscriberlistpgnum li.active').data('pagenum');
					callcampaignsubaddgrid1(page,rowcount);
				});
			}
			//header check box
			$(".callsubscriberlist_headchkboxclass").click(function() {
				if($(".callsubscriberlist_headchkboxclass").prop('checked') == true) {
					$(".callsubscriberlist_rowchkboxclass").attr('checked', true);
				} else {
					$('.callsubscriberlist_rowchkboxclass').removeAttr('checked');
				}
				getcheckboxrowid();
			});
			//row based check box
			$(".callsubscriberlist_rowchkboxclass").click(function(){
				$('.callsubscriberlist_headchkboxclass').removeAttr('checked');
				getcheckboxrowid();
			});
		}
	});	
}
function getcheckboxrowid() {
	var selected = [];
	$('.gridcontent input:checked').each(function() {
		selected.push($(this).attr('data-rowid'));
	});
	var total = $("#callsubscriberlist-sort > .data-rows").length;
	$("#totalsubscribercount").val(total);
	$("#selectedsegmentsubscriberid").val(selected);
	$("#validsubscribercount").val(selected.length);
	Materialize.updateTextFields();
}
{//subscriber id fetch 
	function relatedsubscriberdatafetch(id) {
		$.ajax({
			url:base_url+'Callcampaign/relatedsubscriberidget',
			data:'segmentid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var autoupdate = data.autoupdate;
					$("#autoupdate").val(autoupdate);
					$("#segmentsubscriberid").val(data.subscriberid);
					$("#segmentsubscriberid").val($("#segmentsubscriberid").val());
				}
			},
		});
	}
}
//total subscriber data getAttention
function totalsubscriberget(datarowid) {
	subscriber.push(datarowid);
	$("#totalsubscribercount").val(datarowid.length);
	$("#selectedsegmentsubscriberid").val(datarowid);
	var selsegment = $("#selectedsegmentsubscriberid").val();
	$("#validsubscribercount").val(datarowid.length);
}
//mobile number get
function mobilenumberget(subscriberid) {
	$.ajax({
		url:base_url+'Callcampaign/mobilenumberget',
		data:'subscriberid='+subscriberid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) != 'FAILED') {
				$("#mobilenumber").val(data);
			}
		},
	});
}
{//Sender Id drop down value get
	function callertnumberfetch(numtype,ddname) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Callcampaign/callertnumberfetch?numtype="+numtype,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-apikey",data[index]['apikey'])
						.attr("data-caller",data[index]['dataname'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
//credit detail fetch function
function creditsdetailfetch(addontype) {
	$.ajax({
		url: base_url + "Callcampaign/accountcresitdetailsfetch",
		data: "addontype=" + addontype,
		type: "POST",
		cache:false,
		success: function(creditdata) {
			if((creditdata.fail) != 'FAILED') {
				$("#totalavailabecredit").val(creditdata);
			}
		},
	});
}
{//used for check the selected values in subscriber grid
	function checktheselectedinvitevalues() {
		var subscriberid = $("#selectedsegmentsubscriberid").val();
		if(subscriberid != '') {
			var subid = subscriberid.split(',');
			checkboxcheck(subid);
		}
	}
	function checkboxcheck(subid) {
		for(var i=0;i <subid.length;i++){
			jQuery("#callcampaignsubaddgrid1").setSelection(subid[i],true);
		}
	}
}
//segments dd load load
function segmentdropdownvalfetch(listid) {
	$('#callsegmentsid').empty();
	$.ajax({
		url: base_url+"Callcampaign/segmentddload?listid="+listid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#callsegmentsid')
					.append($("<option value=''>Select</option>")
					.attr("data-callsegmentsidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		}
	});
}