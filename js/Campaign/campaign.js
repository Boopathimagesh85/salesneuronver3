$(document).ready(function() {
	$(document).foundation();
    // Main div height width change
    maindivwidth();
	var maindiv = $('.maindiv').width();
	var a = screen.width;
	reportsviewgridwidth = ((a * maindiv)/100);	 	
    // Grid Calling Functions
    campaigngrid();
    //crud action
	crudactionenable();
    //close
    var addcloseinfo = ["closeaddform", "campaigngriddisplay", "campaignaddformdiv",""]
    addclose(addcloseinfo);
    var addcloseviewcreation =["viewcloseformiconid","campaigngriddisplay","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
	{// View creation Function
		//branch view by drop down change
		$('#dynamicdddataview').change(function(){
			campaigngrid();
		});
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{// Drop Down Change Function 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
    $(".chzn-selectcomp").select2().select2('val', 'Mobile');
	{
		//validation for Campaign Add  
		$('#dataaddsbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	} 
	{// For touch
		fortabtouch = 0;
	}
	{//action events clone,details view,reload and refresh
		$("#cloneicon").click(function() {
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				campaignsclonedatafetchfun(datarowid);
				showhideiconsfun('addclick','campaignaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Materialize.updateTextFields();
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function(){
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('campaigngrid');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				froalaset(froalaarray);
				campaignseditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','campaignaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					campaignseditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','campaignaddformdiv');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','campaignaddformdiv');
			Materialize.updateTextFields();
		});
		$('#formclearicon').click(function() {
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			//Reset Date Picker
			daterangefunction();
		});
		//update Campaign information
		$('#dataupdatesubbtn').click(function(e) {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Date Range
		daterangefunction();
	}
	{// Redirected form Home
		var fromhomevalue = sessionStorage.getItem("campaignaddsrc");    
		if(fromhomevalue == 'formhome'){
			setTimeout(function(){ 
			$(".addicon").trigger('click');
				sessionStorage.removeItem("campaignaddsrc");
			},100);	
		}
	}
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique213");		
		if(uniquelreportsession != '' && uniquelreportsession != null) {	
			setTimeout(function() { 
				campaignseditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','campaignaddformdiv');	
				sessionStorage.removeItem("reportunique213");
			},50);	
		} 
	}
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,campaigngrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			campaigngrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewfieldids").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// Campaign View Grid
function campaigngrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#campaignpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#campaignpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	}
	var wwidth = $('#campaigngrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.campaignheadercolsort').hasClass('datasort') ? $('.campaignheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.campaignheadercolsort').hasClass('datasort') ? $('.campaignheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.campaignheadercolsort').hasClass('datasort') ? $('.campaignheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=campaign&primaryid=campaignid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#campaigngrid').empty();
			$('#campaigngrid').append(data.content);
			$('#campaigngridfooter').empty();
			$('#campaigngridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('campaigngrid');
			{//sorting
				$('.campaignheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.campaignheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#campaignpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.campaignheadercolsort').hasClass('datasort') ? $('.campaignheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.campaignheadercolsort').hasClass('datasort') ? $('.campaignheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#campaigngrid .gridcontent').scrollLeft();
					campaigngrid(page,rowcount);
					$('#campaigngrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('campaignheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					campaigngrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#campaignpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					campaigngrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{// Redirected form Home
				var campaignideditvalue = sessionStorage.getItem("campaignidforedit"); 
				if(campaignideditvalue != null){
					setTimeout(function(){ 
					campaignseditdatafetchfun(campaignideditvalue);
					sessionStorage.removeItem("campaignidforedit");},100);	
				}
			}
			//Material select
			$('#campaignpgrowcount').material_select();
		},
	});	
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			currencydynamicloaddropdown();
			e.preventDefault();
			addslideup('campaigngriddisplay', 'campaignaddformdiv');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','campaignaddformdiv');	
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Materialize.updateTextFields();
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				froalaset(froalaarray);
				campaignseditdatafetchfun(datarowid);
				showhideiconsfun('editclick','campaignaddformdiv');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Materialize.updateTextFields();
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(){
			var datarowid = $('#campaigngrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#campaignpgnum li.active').data('pagenum');
		var rowcount = $('ul#campaignpgnumcnt li .page-text .active').data('rowcount');
		campaigngrid(page,rowcount);
	}
}
//new data add submit function
function newdataaddfun(){
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
    var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Campaign/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				var dynview = $("#dynamicdddataview").val();
				$(".ftab").trigger('click');
				resetFields();
				$('#campaignaddformdiv').hide();
				$('#campaigngriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				var dynview = $("#dynamicdddataview").val();
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	$.ajax({
		url:base_url+"Campaign/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#campaignaddformdiv').hide();
				$('#campaigngriddisplay').fadeIn(1000);
				refreshgrid();
			} else {
				addslideup('campaigngriddisplay', 'campaignaddformdiv');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				editordatafetch(froalaarray,data);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
			}
		}
	});
}
//update old information
function updateformdata() {
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditordataget(editorname);
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Campaign/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$('#campaignaddformdiv').hide();
				$('#campaigngriddisplay').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
            }
        },
    });
}
//delete operation
function recorddelete(datarowid){ 
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Campaign/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup("Deleted successfully");
            } else if (nmsg == "Denied")  {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
{//date Range
	function daterangefunction(){ 
		$('#startdate').datetimepicker("destroy");
		$('#enddate').datetimepicker("destroy");
		var startdateformater = $('#startdate').attr('data-dateformater');
		var startDateTextBox = $('#startdate');
		var endDateTextBox = $('#enddate');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			showTimepicker :false,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
				}
				else {
					endDateTextBox.val(dateText);
				}
				$('#startdate').focus();
				Materialize.updateTextFields();
			},
			onSelect: function (selectedDateTime){
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			}
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			showTimepicker :false,
			minDate:null,
			maxDate:null,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
				}
				else {
					startDateTextBox.val(dateText);
				}
				$('#enddate').focus();
			},
			onSelect: function (selectedDateTime){
			}
		});
	}
}
{// Edit Function
	function campaignseditdatafetchfun(datarowid){		
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		getformdata(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function campaignsclonedatafetchfun(datarowid){
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		getformdata(datarowid);
		$("#primarydataid").val('');
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 0;
		viewgridview = 0;
		addformview = 1;
	}
}
function currencydynamicloaddropdown() {
	$('#currencyid').empty();
	$('#currencyid').append($("<option></option>"));
	$.ajax({
		url:base_url+"Campaign/currencyddload",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#currencyid')
					.append($("<option></option>")
					.attr("data-currencyid",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
		},
	});
}
//account unique name check
function campaignnamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#campaignname").val();
	var elementpartable = $('#elementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Campaign name already exists!";
				}
			} else {
				return "Campaign name already exists!";
			}
		} 
	}
}