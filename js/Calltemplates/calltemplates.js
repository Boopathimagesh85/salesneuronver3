$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		calltemplatesviewgrid();
		//crud action
		crudactionenable();
	}
	//form clear icon
	$('#formclearicon').click(function(){
		$("#formfields,#relatedtomoduleid").empty();
		$("#moduleid,#smsgrouptypeid").attr('readonly',false);
		$("#templatetypeid").val('2');
		var elementname = $('#elementsname').val();
		elementdefvalueset(elementname);
		refreshgrid();
		$("#calltemplatecontent_editorfilenamedivhid").find('p').css('opacity','1');
	});
	//folder name grid reload
	$('#tab1').click(function(){
		$("#foldernamename").val('');
		$("#description").val('');
		$("#setdefault").val('No');
		$("#setdefaultcboxid").removeAttr('checked',false)
	});
	{//Close Add Screen
		var addcloseoppocreation =["closeaddform","calltemplatescreationview","calltemplatescreationformadd"]
		addclose(addcloseoppocreation);
		var addcloseviewcreation =["viewcloseformiconid","calltemplatescreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// For touch
		fortabtouch = 0;
	}
	{//Folder Button Show/Hide
		$("#calltemplatesfolingrideditspan1,#calltemplatesfolingridreloadspan1").show();
	}
	{//Toolbar Icon Change Function View
		//reload click
		$("#reloadicon").click(function(){
			refreshgrid();
			$("#calltemplatecontent_editorfilenamedivhid").find('p').css('opacity','0');
		});
		$( window ).resize(function() {
			maingridresizeheightset('calltemplatesviewgrid');
			innergridresizeheightset('calltemplatesfoladdgrid1');
		});
	}
	//close
	$('#closeaddoppocreation').click(function(){
		$('#dynamicdddataview').trigger('change');
	});
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			calltemplatesviewgrid();
		});
	}
	{//validation for call templates Add  
		$('#dataaddsbtn').click(function(e)  {
			$('#foldernamename').removeClass('validate[required]');
			$("#formaddwizard").validationEngine('validate');
			//For Touch
			smsmasterfortouch = 0;
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$('#foldernamename').addClass('validate[required]');
				$('#dataaddsbtn').attr('disabled','disabled');
				newdataaddfun();
			},
			onFailure: function() {
				$('#foldernamename').addClass('validate[required]');
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update call templates information
		$('#dataupdatesubbtn').click(function(e) {
			$('#foldernamename').removeClass('validate[required]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$('#foldernamename').addClass('validate[required]');
				$('#dataupdatesubbtn').attr('disabled','disabled');
				dataupdateinformationfun();
			},
			onFailure: function() {
				$('#foldernamename').addClass('validate[required]');
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	//module change events
	$('#moduleid').change(function() {
		var type = $('#smsgrouptypeid').val();
		if(type != '') {
			var id = $(this).val();
			relatedtodropdownload(id);
		} else {
			$(this).select2('val','');
			alertpopup('Please select the template mode');
		}
	});
	//related module dd change
	$("#relatedtomoduleid").change(function() {
		var module = $("#moduleid").val();
		if(module != '' || module != null) {
			var id = $(this).val();
			calldropdownmodulevalset('formfields','formfieldsidhidden','modfiledcolumn','modfiledtable','fieldlabel','modulefieldid','columnname','tablename','modulefield','moduletabid,uitypeid',''+id+',0');
		} else {
			alertpopup('Please select the module list');
		}
	});
	//module fields change events
	$('#formfields').change(function() {
		var relatedmodule = $("#relatedtomoduleid").val();
		if(relatedmodule != '' || relatedmodule != null) {
			if( $(this).val()!= "" ) {
				var parmodid = $('#moduleid').val();
				var fildmodid = $('#relatedtomoduleid').val();
				var fildmodtype = $('#relatedtomoduleid').find('option:selected').data('mergemoduletype');
				if(parmodid!='' && fildmodid!='') {
					var table = $(this).find('option:selected').data('modfiledtable');
					var tabcolumn = $(this).find('option:selected').data('modfiledcolumn');
					var mtext = '{'+fildmodtype+table+'.'+tabcolumn+'}';
					var oldmtext = $('#mergetext').val();
					var ftext = oldmtext +' '+ mtext;
					$('#mergetext').val(mtext);
				}
			}
		} else {
			alertpopup('Please select the related module list');
		}
	});
	{//Folder Operation
		//Insertion
		$("#calltemplatesfoladdbutton").click(function() {
			$('#calltemplatesfoladdgrid1validation').validationEngine('validate');
		});
		$('#calltemplatesfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				calltemplatesfolderinsertfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Edit Operation
		$("#calltemplatesfolingridedit1").click(function() {
			var datarowid = $('#calltemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				resetFields();
				$("#calltemplatesfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#calltemplatesfoladdbutton,#calltemplatesfolingriddel1").hide();
				calltemplatefolderdatafetch(datarowid);
				firstfieldfocus();
				//For Touch
				smsmasterfortouch = 3;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please select a row');
			}
		});
		//Update operation
		$("#calltemplatesfolupdatebutton").click(function() {
			$('#calltemplatesfoleditgrid1validation').validationEngine('validate');
		});
		$('#calltemplatesfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				calltemplatesfolupdatefun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//delete operation
		$("#calltemplatesfolingriddel1").click(function(){
			var datarowid = $('#calltemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
	}
	{//Template Count
		$("#calltemplatecontent_editorfilenamedivhid").append('<p align="right" style="color:#546E7A;font-size:0.8rem; margin:0;opacity:1;">&nbsp;</p>');
		$('#calltemplatecontent_editorfilename').keyup(function(e) {
			var $this = $(this);
			setTimeout(function() {
				var text = $this.val();
				var lengthofchar = text.length;
				$("#calltemplatecontent_editorfilenamedivhid").find('p').text(''+lengthofchar+'/1000');
				if(lengthofchar > 0) {
					var mid = $("#moduleid").val();
					if(mid != '') {
						$("#moduleid").attr('readonly',true);
					} else { $("#moduleid").attr('readonly',false); }
				} else { 
					$("#calltemplatecontent_editorfilenamedivhid").find('p').css('opacity','0');
					$("#moduleid").attr('readonly',false); 
				}
			}, 0);
		});
	}
	//folder reload
	$("#calltemplatesfolingridreload1").click(function() {
		refreshgrid();
		folderrefreshgrid();
		folderdatareload();
		$("#calltemplatesfoladdbutton,#calltemplatesfolingriddel1").show();
		$("#calltemplatesfolupdatebutton").hide();
		resetFields();
		Materialize.updateTextFields();
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,calltemplatesviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
});
//drop down values set for view
function calldropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Calltemplates/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function calltemplatesviewgrid(page,rowcount) {
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		var wwidth = $(window).width();
		var wheight = $(window).height();
		var sortcol = $('.calltemplatesheadercolsort').hasClass('datasort') ? $('.calltemplatesheadercolsort.datasort').data('sortcolname') : '';
		var sortord =  $('.calltemplatesheadercolsort').hasClass('datasort') ? $('.calltemplatesheadercolsort.datasort').data('sortorder') : '';
		var headcolid = $('.calltemplatesheadercolsort').hasClass('datasort') ? $('.calltemplatesheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#conditionname").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=calltemplates&primaryid=calltemplatesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#calltemplatesviewgrid').empty();
				$('#calltemplatesviewgrid').append(data.content);
				$('.gridfootercontainer').empty();
				$('.gridfootercontainer').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('calltemplatesviewgrid');
				{//sorting
					$('.calltemplatesheadercolsort').click(function(){
						$('.calltemplatesheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#calltemplatespgnumcnt li .page-text .active').data('rowcount');
						calltemplatesviewgrid(page,rowcount);
					});
					sortordertypereset('calltemplatesheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						calltemplatesviewgrid(page,rowcount);
					});				
				}				
			},
		});		
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			$('#newsmssettingsid').removeClass('validate[required]');
			addslideup('calltemplatescreationview','calltemplatescreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formfields").empty();
			$("#templatetypeid").val('2');
			$("#moduleid,#smssendtypeid").attr('readonly',false);
			$("#calltemplatecontent_editorfilenamedivhid").find('p').css('opacity','1');
			$('#tab2').trigger('click');
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','2');
			//fortabtouch = 0;
			//For Touch
			smsmasterfortouch = 0;
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			firstfieldfocus();
			calltemplatesfoladdgrid1();
			Materialize.updateTextFields();
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#calltemplatesviewgrid div.gridcontent div.active').attr('id');
			if (datarowid)  {				
				$(".addbtnclass").addClass('hidedisplay');
				$(".updatebtnclass").removeClass('hidedisplay');
				$("#templatetypeid").val('2');
				$('#tab2').trigger('click');
				$("#smssendtypeid").attr('readonly',true);
				editformdatainformationshow(datarowid);
				$("#formfields").empty();
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','2');
				fortabtouch = 1;
				//For Touch
				smsmasterfortouch = 1;
				Materialize.updateTextFields();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#calltemplatesviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $(this).data('pagenum');
		var rowcount = $('ul#calltemplatespgnumcnt li .page-text .active').data('rowcount');
		calltemplatesviewgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul#folderlistpgnumcnt li .page-text .active').data('rowcount');
		calltemplatesfoladdgrid1(page,rowcount);
	}
}
//Call templates folder Grid
function calltemplatesfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#calltemplatesfoladdgrid1').width();
	var wheight = $('#calltemplatesfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Calltemplates/calltemplatesfoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=39',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#calltemplatesfoladdgrid1').empty();
			$('#calltemplatesfoladdgrid1').append(data.content);
			$('.folgridfootercontainer').empty();
			$('.folgridfootercontainer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*//column resize*/
			columnresize('calltemplatesfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $('ul#folderlistpgnumcnt li .page-text .active').data('rowcount');
					calltemplatesfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smsmasterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#folderlistpgnumcnt li .page-text .active').data('rowcount');
					calltemplatesfoladdgrid1(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					calltemplatesfoladdgrid1(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#folderlistpgnumcnt li .page-text .active').data('rowcount');
					calltemplatesfoladdgrid1(page,rowcount);
				});
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					calltemplatesfoladdgrid1(page,rowcount);
				});
			}
		},
	});	
}
//new data add submit function
function newdataaddfun() {
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var message = $("#calltemplatecontent_editorfilename").val();
	var neweditordata = '&calltemplatecontent_editorfilename='+message;
	$.ajax(  {
		url: base_url + "Calltemplates/newdatacreate",
		data: "datas=" + datainformation+"&datass="+neweditordata,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#newsmssettingsid').removeClass('validate[required]');
				$('#calltemplatescreationformadd').hide();
				$('#calltemplatescreationview').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
			} else if (nmsg == "false") {
			
			}
		},
	});
}
//old information show in form
function editformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax(	{
		url:base_url+"Calltemplates/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)	{
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#calltemplatescreationformadd').hide();
				$('#calltemplatescreationview').fadeIn(1000);
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				addslideup('calltemplatescreationview','calltemplatescreationformadd');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				tandceditervalueget(data.calltemplatecontent_editorfilename);
				setTimeout(function(){
					$("#moduleid").trigger('change');
				}, 500);
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
//editor value fetch
function tandceditervalueget(filename) {
	$.ajax({
		url: base_url+"Calltemplates/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if(data != 'Fail') {
				$("#calltemplatecontent_editorfilename").val(data);
			} else {
				$("#calltemplatecontent_editorfilename").val('');
			}
		},
	});
}
//update old information
function dataupdateinformationfun() {
	var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Calltemplates/datainformationupdate",
		data: "datas=" + datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#calltemplatescreationformadd').hide();
				$('#calltemplatescreationview').fadeIn(1000);
				refreshgrid();
				alertpopup(savealert);
				//For Touch
				smsmasterfortouch = 0;
			} else if (nmsg == "false") {	}
		},
	});
	setTimeout(function()  {
		var closetrigger = ["closeaddleadcreation"];
		triggerclose(closetrigger);
	}, 1000);
	
}
//delete information
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Calltemplates/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
{//Related modules drop down load 
	function relatedtodropdownload(id) {
		$('#relatedtomoduleid').empty();
		$('#relatedtomoduleid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Calltemplates/relatedmoduleidget',
			data:'moduleid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#relatedtomoduleid')
						.append($("<option></option>")
						.attr("data-mergemoduleidhidden",data[index]['datasid'])
						.attr("data-mergemoduletype",data[index]['datatype'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#relatedtomoduleid').trigger('change');
			},
		});
	}
}
{//folder operation
	function calltemplatesfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		$.ajax({
			url:base_url+'Calltemplates/calltemplatesfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					alertpopup(savealert);
					folderdatareload();
					Materialize.updateTextFields();
				} else if (nmsg == 'false') { }
			},
		});
	}
	//Edit data Fetch - for folder
	function calltemplatefolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Calltemplates/calltemplatefolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes'){
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
				}
			},
		});
	}
	//Call templates folder add function
	function calltemplatesfolupdatefun(){
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		$.ajax({
			url:base_url+'Calltemplates/calltemplatesfolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					folderdatareload();
					$("#calltemplatesfoladdbutton,#calltemplatesfolingriddel1").show();
					$("#calltemplatesfolupdatebutton").hide();
					alertpopup(savealert);
					//For Touch
					smsmasterfortouch = 2;
				} else if (nmsg == 'false') { }
			},
		});
	}
	//delete folder 
	function deletefolderoperation(id){
		$.ajax({
			url:base_url+'Calltemplates/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#calltemplatesfoladdgrid1').attr('disabled',false);
					folderdatareload();
					alertpopup("Deleted successfully");
				} else if (nmsg == 'false') { }
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#callfoldernameid').empty();
		$('#callfoldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Calltemplates/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=39',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#callfoldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#callfoldernameid').trigger('change');
			},
		});
	}
}