$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}		
	{//button show hide
		$("#callsettingssetingridreloadspan1").hide();
		$('#dataaddsbtn,#dataupdatesubbtn').hide();
		$("#username,#password,#apikey").attr('readonly',false);
	}
	{//Grid Calling
		callsettingssetaddgrid1();
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	$( window ).resize(function() {
		sectionpanelheight('callsettingssetoverlay');
	});
	{//inner-form-with-grid
		$("#callsettingssetingridadd1").click(function(){
			sectionpanelheight('callsettingssetoverlay');
			$("#callsettingssetoverlay").removeClass("closed");
			$("#callsettingssetoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#callsettingssetcancelbutton").click(function(){
			$("#callsettingssetoverlay").removeClass("effectbox");
			$("#callsettingssetoverlay").addClass("closed");
		});
	}
	{//sms credential operation
		//sms credentialedit operation
		$("#callsettingssetingridedit1").click(function() {
			var ids = $('#callsettingssetaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#callsettingssetaddbutton,#callsettingssetingriddel1").hide();
				$("#callsettingssetupdatebutton").show();
				callsettingsdatafetch(datarowid);
				//for tab touch
				//For Touch
				masterfortouch = 1;
				//keyboard shortcut
				saveformview=1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms settings delete 
		$("#callsettingssetingriddel1").click(function() {
			var ids = $('#callsettingssetaddgrid1 div.gridcontent div.active').attr('id');
			if(ids) {		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var ids = $('#callsettingssetaddgrid1 div.gridcontent div.active').attr('id');
			if(ids){
				callsettingsdelete(ids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms reload operation
		$("#callsettingssetingridreload1").click(function() {
			resetFields();
			callsettingsrefreshgrid();
			$("#callsettingssetupdatebutton").hide();
			$("#smsservicetypeid").attr('readonly',false);
			$("#callsettingssetaddbutton").show();
		});
		//sms Credential add
		$("#callsettingssetaddbutton").click(function() {
			$("#callsettingssetaddgrid1validation").validationEngine('validate');
			//For Touch
			masterfortouch = 0;
		});
		jQuery("#callsettingssetaddgrid1validation").validationEngine({
			onSuccess: function() {
				callcredentialadd();
			},
			onFailure: function() {
				var dropdownid =['4','providerid','callservicetypeid','callservicemodeid','callnumbertypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//sms credential update
		$("#callsettingssetupdatebutton").click(function() {
			$("#callsettingsseteditgrid1validation").validationEngine('validate');
		});
		jQuery("#callsettingsseteditgrid1validation").validationEngine({
			onSuccess: function() {
				callcredentialupdate();
			},
			onFailure: function() {
				var dropdownid =['4','providerid','callservicetypeid','callservicemodeid','callnumbertypeid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Function For Check box
		$('.checkboxcls').click(function() {
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	//service type dd change
	$("#callservicetypeid").change(function() {
		var servicetype = $("#callservicetypeid").val();
		$("#callservicemodeid").select2('val','');
		commondropdownload('callservicemodeid','callservicemodeidhidden','callservicemode','callservicemodeid','callservicemodename');
		if(servicetype == 2) {
			$("#callservicemodeid option[value='3']").remove();	
		} else if(servicetype == 3) {
			$("#callservicemodeid option[value='2']").remove();	
		}
	});
	//service mode dd change
	$("#callservicemodeid").change(function() {
		var servicetype = $("#callservicemodeid").val();
		$("#callnumbertypeid").select2('val','');
		commondropdownload('callnumbertypeid','callnumbertypeidhidden','callnumbertype','callnumbertypeid','callnumbertypename');
		if(servicetype == 2) {
			$("#callnumbertypeid option[value='4']").remove();	
		} 
	});
	//provider dd change 
	$("#providerid").change(function() {
		var provider = $("#providerid").val();
		if(provider == 2) {
			$("#username,#password,#apikey").attr('readonly',false);
			$("#username,#password,#apikey").val('');
			Materialize.updateTextFields();
		} else {
			$("#username,#password,#apikey").attr('readonly',false);
			$("#username,#password,#apikey").val('');
			Materialize.updateTextFields();
		}	
	});
	//Close icon action
	$('#closeaddform').click(function(){
		window.location =base_url+'Calls';
	});
});
//Criteria (condition) Add Grid
function callsettingssetaddgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#callsettingssetaddgrid1').width();
	var wheight = $('#callsettingssetaddgrid1').height();
	//if(typeof wwidth !== 'undefined' || typeof wheight !== 'undefined') {
		wwidth = wwidth;
		wheight = wheight;
	/* } else {
		wwidth = '1337';
		wheight = '382';
	} */
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.callsettingslistheadercolsort').hasClass('datasort') ? $('.callsettingslistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.callsettingslistheadercolsort').hasClass('datasort') ? $('.callsettingslistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.callsettingslistheadercolsort').hasClass('datasort') ? $('.callsettingslistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Callsettings/callcredentialgriddatafetch?maintabinfo=callsettings&primaryid=callsettingsid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=30',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#callsettingssetaddgrid1').empty();
			$('#callsettingssetaddgrid1').append(data.content);
			$('#callsettingssetaddgrid1footer').empty();
			$('#callsettingssetaddgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('callsettingssetaddgrid1');
			{//sorting
				$('.callsettingslistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.callsettingslistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.callsettingslistheadercolsort').hasClass('datasort') ? $('.callsettingslistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.callsettingslistheadercolsort').hasClass('datasort') ? $('.callsettingslistheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					callsettingssetaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('callsettingslistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					callsettingssetaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#callsettingslistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					callsettingssetaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#callsettingslistpgrowcount').material_select();
		}
	});
}
function callsettingsrefreshgrid() {
	var page = $('ul#callsettingslistpgnum li.active').data('pagenum');
	var rowcount = $('ul#callsettingslistpgnumcnt li .page-text .active').data('rowcount');
	callsettingssetaddgrid1(page,rowcount);
}
//for sms credential add
function callcredentialadd() {
	var servicetype= $("#callservicetypeid").val();
	var provider= $("#providerid").val();
	var servicemode= $("#callservicemodeid").val();
	var numbertype= $("#callnumbertypeid").val();
	var mobilenumber= $("#mobilenumber").val();
	var username= $("#username").val();
	var password= $("#password").val();
	var apikey= $("#apikey").val();
	var servicename = $("#callservicetypeid").find('option:selected').data('callservicetypeidhidden');
	var modename = $("#callservicemodeid").find('option:selected').data('callservicemodeidhidden');
	var numtypename = $("#callnumbertypeid").find('option:selected').data('callnumbertypeidhidden');
	$.ajax({
		url: base_url + "Callsettings/callcredentialadd",
		data:'provider='+provider+"&servicetype="+servicetype+"&username="+username+"&password="+password+"&servicemode="+servicemode+"&numbertype="+numbertype+"&mobilenumber="+mobilenumber+"&apikey="+apikey+"&servicename="+servicename+"&modename="+modename+"&numtypename="+numtypename,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	$("#callsettingssetoverlay").removeClass("effectbox");
    			$("#callsettingssetoverlay").addClass("closed");
            	callsettingsrefreshgrid();
				resetFields();
				alertpopup('Data Stored Successfully');
			}
		},
	});
}
//for sms credential edit data fetch
function callsettingsdatafetch(datarowid) {
	$("#callsettingssetupdatebutton").removeClass('hidedisplayfwg');
	$.ajax({
		url: base_url + "Callsettings/callcredentialeditdatafetch",
		data:'datarowid='+datarowid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			var provider = data.provider;
			var user = data.username;
			var pass = data.password;
			var apikey = data.apikey;
			var numbertype = data.numbertype;
			var servicemode = data.servicemode;
			var servicetype = data.servicetype;
			var mobile = data.mobile;
			$("#callservicetypeid").select2('val',servicetype);
			$("#providerid").select2('val',provider);
			$("#callservicemodeid").select2('val',servicemode);
			$("#callnumbertypeid").select2('val',numbertype);
			$("#mobilenumber").val(mobile);
			$("#username").val(user);
			$("#password").val(pass);
			$("#apikey").val(apikey);
			$("#primarydataid").val(datarowid);
		},
	});
}
//sms credential update
function callcredentialupdate() {
	var primaryid = $("#primarydataid").val();
	var provider= $("#providerid").val();
	var servicemode= $("#callservicemodeid").val();
	var numbertype= $("#callnumbertypeid").val();
	var servicetype= $("#callservicetypeid").val();
	var mobile= $("#mobilenumber").val();
	var username= $("#username").val();
	var password= $("#password").val();
	var apikey= $("#apikey").val();
	$.ajax({
		url: base_url + "Callsettings/callcredentialupdate",
		data:'provider='+provider+"&servicemode="+servicemode+"&username="+username+"&password="+password+"&primaryid="+primaryid+"&numbertype="+numbertype+"&servicetype="+servicetype+"&mobile="+mobile+"&apikey="+apikey,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
				$("#callsettingssetupdatebutton").hide();
				$("#callsettingssetaddbutton").show();
				$("#callsettingssetingriddel1").show();
				callsettingsrefreshgrid();
				resetFields();
				alertpopup('Data Stored Successfully');
				//For Touch
				masterfortouch = 0;
				//keyboar shortcut
				saveformview=0;
			}
		},
	});
}
//sms settings delete
function callsettingsdelete(ids) {
	$.ajax({
		url: base_url + "Callsettings/callcredentialdelete",
		data:"primaryid="+ids,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	callsettingsrefreshgrid();
				resetFields();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Record Deleted Successfully');
			}
		},
	});
}
//dd load
function commondropdownload(ddname,hiddenname,table,filedid,filedname) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url: base_url+"Callsettings/servicemodeddreload?table="+table+"&filedid="+filedid+"&filedname="+filedname,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-"+hiddenname+" ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#'+ddname+'').append(newddappend);
			}	
		},
	});
}