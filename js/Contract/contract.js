$(document).ready(function() {
	CONVERTID = '';
	$(document).foundation();
    // Main div height width change
    maindivwidth();
    METHOD = 'ADD';
    {
    	//default value set
		$("#contractfolingrideditspan1").show();
		{//grid width fix
			if(softwareindustryid != 4){
				$("#tab1").click(function(){
					contractfoladdgrid1();
				});
			}
		}
    }
	documentsdelid = [];
    // Grid Calling Functions
    contractgrid();
	//crud action
	crudactionenable();
	$("#contractattupdatebutton").hide();
	$("#contractattaddbutton").removeClass('hidedisplayfwg');
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	//folder name grid reload
	$('#tab2').click(function(){
		folderdatareload();
		contractgrid();
	});
	{//inner-form-with-grid
		$("#contractfolingridadd1").click(function(){
			sectionpanelheight('contractfoloverlay');
			$("#contractfoloverlay").removeClass("closed");
			$("#contractfoloverlay").addClass("effectbox");
			$('#foldernamename,#description').val('');
			$('#setdefaultcboxid,#publiccboxid').prop('checked', false);
			$("#setdefault,#public").val('No');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#contractfolcancelbutton").click(function(){
			$("#contractfolingridedit1,#contractfolingriddel1").show();
			$("#contractfoloverlay").removeClass("effectbox");
			$("#contractfoloverlay").addClass("closed");
		});
		$("#contractattingridadd2").click(function(){
			//Sectionpanel height set
			sectionpanelheight('contractattoverlay');
			$("#contractattoverlay").removeClass("closed");
			$("#contractattoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#contractattcancelbutton").click(function(){
			$("#contractattoverlay").removeClass("effectbox");
			$("#contractattoverlay").addClass("closed");
		});
	}
	{//inner-form-with-grid
		$("#contractattingridadd1").click(function(){
			formheight();
			$("#contractattoverlay").removeClass("closed");
			$("#contractattoverlay").addClass("effectbox");
			clearform('gridformclear');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#dietplandiecancelbutton").click(function(){
			$("#contractattoverlay").removeClass("effectbox");
			$("#contractattoverlay").addClass("closed");
			$('#dietplandieupdatebutton').hide();	//hide the UPDATE button(inner - productgrid)
			$('#dietplandieaddbutton').show();//display the ADD button(inner-productgrid)*/	
		});
	}
	$('#formclearicon').click(function(){
		froalaset(froalaarray);
		$("#keywords").select2('val',' ');
		cleargriddata('contractattaddgrid2');
		clearformgriddata();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
		$("#crmfileinfoid").val('0');
		$('#filenameattachdisplay').empty();
	});
	//close
    var addcloseinfo = ["closeaddform", "contractgriddisplay", "contractaddformdiv"]
    addclose(addcloseinfo);
	var addcloseviewcreation =["viewcloseformiconid","contractgriddisplay","viewcreationformdiv",""];
	addclose(addcloseviewcreation);
  	{// View creation Function
		//branch view by drop down change
		$('#dynamicdddataview').change(function(){
			contractgrid();
		});
	}
	$(".chzn-selectcomp").select2().select2('val', 'Mobile');
	{//validation for Campaign Add  
		$('#dataaddsbtn').click(function(e) {
			$('#tab2').trigger('click');
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$("#processoverlay").show();
				newdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#foldernameid").removeClass('validate[required]');
			}
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Terms and condition value fetch
		$("#termsandconditionid").change(function() {
			var id = $("#termsandconditionid").val();
			tandcdatafrtch(id);
		});
	}
	{// Terms and condition data fetch
		function tandcdatafrtch(id) {
			$.ajax({
				url:base_url+"Contract/termsandcontdatafetch?id="+id,
				dataType:'json',
				async:false,
				cache:false,
				success: function(data) {
					filenamevaluefetch(data);
				},
			});
		}
	}
	{// Editor value fetch
		function filenamevaluefetch(filename) {
			//checks whether the Terms & condition is empty
			if(filename == '' || filename == null ) {			
				if($("#contractconditions_editor").length){
					froalaset(froalaarray);
				}
			} else {
				$.ajax({
					url: base_url + "Contract/editervaluefetch?filename="+filename,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						froalaedit(data,'contractconditions_editor');
					},
				});
			}
		}
	}
	$("#convertyes").click(function(){
		sessionStorage.setItem("convertcontracttoinvoice",CONVERTID);
		window.location = base_url+'Invoice';
	});	
	$("#convertno").click(function(){
		$("#coninvoverlay").fadeOut();
		$(".ftab").trigger('click');
		resetFields();
		$('#contractaddformdiv').hide();
		$('#contractgriddisplay').fadeIn(1000);
		refreshgrid();
		clearformgriddata();
		alertpopup(savealert);
		froalaset(froalaarray);
		$("#keywords").select2('val',' ');
		cleargriddata('contractattaddgrid2');
		$("#foldernameid").addClass('validate[required]');
		//For Keyboard Shortcut Variables
		viewgridview = 1;
		addformview = 0;
	});	
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{//Toolbar actions
		$("#cloneicon").click(function() {
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				contractattaddgrid2();
				contractclonedatafetchfun(datarowid);
				Materialize.updateTextFields();
				$("#contractattingriddel2").show();
				showhideiconsfun('editclick','contractaddformdiv');
				$("#contractattaddbutton").removeClass('hidedisplayfwg');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
		});
		$("#reloadicon").click(function(){
			refreshgrid();
			$("#crmfileinfoid").val('0');
		});
		$( window ).resize(function() {
			maingridresizeheightset('contractgrid');
			//Sectionpanel height set
			sectionpanelheight('contractattoverlay');
			sectionpanelheight('contractfoloverlay');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				contractattaddgrid2();
				contracteditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				showhideiconsfun('summryclick','contractaddformdiv');
				$("#contractattingriddel2").hide();				
				$(".froala-element").css('pointer-events','none');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
		});
		{// Redirected form notification overlay and widget and audit log
			var rdatarowid = sessionStorage.getItem("datarowid");		
			if(rdatarowid != '' && rdatarowid != null){
				 setTimeout(function() {
					$("#processoverlay").show();
					froalaset(froalaarray);
					contractattaddgrid2();
					contracteditdatafetchfun(rdatarowid);
					showhideiconsfun('summryclick','contractaddformdiv');
					sessionStorage.removeItem("datarowid");
					Materialize.updateTextFields();
				},50);
			}
		}
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','contractaddformdiv');
			$('#contractattupdatebutton').removeClass('hidedisplayfwg').show();
			$("#contractattingriddel2").show();
			$(".froala-element").css('pointer-events','auto');			
		});
		$("#contractattingriddel2").click(function() {
			var datarowid = $('#contractattaddgrid2 div.gridcontent div.active').attr('id');
			if(datarowid) {		
				if(METHOD == 'UPDATE') {
					alertpopup("A Record Under Edit Form");
				} else {
					/*delete grid data*/
					deletegriddatarow('contractattaddgrid2',datarowid);
				}
			} else {
				alertpopup("Please select a row");
			}
		});
		//update Campaign information
		$('#dataupdatesubbtn').click(function(e) {
			$('#tab2').trigger('click');
			$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#foldernameid").removeClass('validate[required]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$("#processoverlay").show();
				updateformdata();
			},
			onFailure: function() {
				var dropdownid =['1','accountid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				$("#foldernamename").removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$("#foldernameid").removeClass('validate[required]');
			}	
		});
	}
	{//Folder option
		$("#contractfoladdbutton").click(function() {
			$("#foldernamename").focusout();
			$('#contractfoladdgrid1validation').validationEngine('validate');
			Materialize.updateTextFields();
		});
		$('#contractfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				contractfolderinsertfun();
				Materialize.updateTextFields();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Contract folder data fetch
		$("#contractfolingridedit1").click(function() {
			var datarowid = $('#contractfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				//resetFields();
				sectionpanelheight('contractfoloverlay');
				$("#contractfoloverlay").removeClass("closed");
				$("#contractfoloverlay").addClass("effectbox");
				$("#contractfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#contractfoladdbutton,#contractfolingridedit1").hide();
				contractfolderdatafetch(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
			} else {
				alertpopup('Please select a row');
			}
		});
		//Contract update
		$("#contractfolupdatebutton").click(function() {
			$("#foldernamename").focusout();
			$('#contractfoleditgrid1validation').validationEngine('validate');
			setTimeout(function(){
				Materialize.updateTextFields();
				firstfieldfocus();
			},100);
		});
		$('#contractfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				contractfolderupdate();
				$("#contractfolingridedit1,#contractfolingriddel1").show();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Contract folder delete
		$("#contractfolingriddel1").click(function() {
			var datarowid = $('#contractfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
		//folder reload
		$("#contractfolingridreload1").click(function() {
			//resetFields();
			$("#foldernamename").val('');
			$("#description").val('');
			$("#setdefaultcboxid").attr('checked',false);
			$("#publiccboxid").attr('checked',false);
			$("#setdefault").val('No');
			$("#public").val('No');
			folderrefreshgrid();
			$("#contractfolupdatebutton").hide().addClass('hidedisplayfwg');
			$("#contractfoladdbutton,#contractfolingriddel1").show();
			Materialize.updateTextFields();
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	{//dynamic form to grid
		griddataid = 0;
		gridynamicname = '';
		$('#contractattaddbutton,#contractattupdatebutton').click(function(){
			if($("#filenameattachdisplay").is(':empty')){
				alertpopup('Please Upload File...');
			} else {
				var i = $(this).data('frmtogriddataid');
				var gridname = $(this).data('frmtogridname');
				gridformtogridvalidatefunction(gridname);
				griddataid = i;
				gridynamicname = gridname;
				$("#"+gridname+"validation").validationEngine('validate');
				METHOD = 'ADD';
			}
		});
	}	
	{//shipping billing address details fetching
		$("#billingaddresstype,#shippingaddresstype").change(function() {
			var id=$(this).attr('id');
			setaddress(id);
			Materialize.updateTextFields();
		});
		//shipping /billing hiding
		$("#billingaddresstype option[value='6']").remove();
		$("#shippingaddresstype option[value='5']").remove();
	}	
	var dateformetdata = $('#invoicedate').attr('data-dateformater');
	$('#invoicedate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			minDate: null,
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#invoicedate').focus();
			}
		});
		
	{// Redirected form Home
		var fromhomevalue = sessionStorage.getItem("contractaddsrc");    
		if(fromhomevalue == 'formhome'){
			setTimeout(function(){ 
			$("#addicon").trigger('click');
			sessionStorage.removeItem("contractaddsrc");},100);
		}
	}
	{//account name based contact name
		if(softwareindustryid == 2){
			$("#accountid").change(function(){
				var accid = $("#accountid").val();
				getaddress(accid,'account','Billing');		
				getaddress(accid,'account','Shipping');
				$('#billingaddresstype,#shippingaddresstype').select2('val','2');
				Materialize.updateTextFields();
			});
		} else {
			$("#accountid").change(function(){
				var accid = $("#accountid").select2('val');
				if(accid == null || accid == ""){
					accid = "1";
				}
				contactnamefetch(accid);
				getaddress(accid,'account','Billing');		
				getaddress(accid,'account','Shipping');
				$('#billingaddresstype,#shippingaddresstype').select2('val','2');
				Materialize.updateTextFields();
			});
		}		
	}
	//Contracts-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique230"); 
		if(uniquelreportsession != '' && uniquelreportsession != null) {
			setTimeout(function() { 
				contracteditdatafetchfun(uniquelreportsession);
				showhideiconsfun('summryclick','contractaddformdiv');
				sessionStorage.removeItem("reportunique230");
			},50);	
		}
	}
	{//print icon
		$('#printicon').click(function(){
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function(){
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
	}
	{// Date REstriction
		var dateformetcon  = 0;
		dateformetcon = $('#startdate').attr('data-dateformater');
		$('#startdate').datetimepicker({
			dateFormat: dateformetcon,
			showTimepicker :false,
			minDate: 0,
			onSelect: function () {
				getandsetdate($('#startdate').val())
			},
			onClose: function () {
				$('#startdate').focus();
			}
		});
	}
	$("#mailicon").click(function(){
		var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
		var accountid=getaccountid(datarowid,'contract','accountid');
		if (datarowid) {
			var validemail=validateemail(accountid,'account','emailid');
			if(validemail == true) {
				var emailtosend = getvalidemailid(accountid,'account','emailid','','');
				sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
				sessionStorage.setItem("forsubject",emailtosend.subject);
				var fullurl = 'erpmail/?_task=mail&_action=compose';
				window.open(''+base_url+''+fullurl+'');
			} else {
				alertpopup("Invalid email address");
			}
		} else {
			alertpopup("Please select a row");
		}
	});
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//click to call and sms icon process - gowtham
		$("#mobilenumberoverlayclose").click(function() {
			$("#mobilenumbershow").hide();
		});	
		//sms icon click
		$("#smsicon").click(function() {
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != '') {
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#mobilenumbershow").show();
								$("#smsmoduledivhid").hide();
								$("#smsrecordid").val(datarowid);
								$("#smsmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'smsmobilenumid');
							} else if(mobilenumber.length == 1) {
								sessionStorage.setItem("mobilenumber",mobilenumber);
								sessionStorage.setItem("viewfieldids",viewfieldids);
								sessionStorage.setItem("recordis",datarowid);
								window.location = base_url+'Sms';
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").show();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'smsmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});	
		//sms icon dd change
		$("#smsmodule").change(function() {
			var moduleid =	$("#smsmoduleid").val(); //main module
			var linkmoduleid =	$("#smsmodule").val(); // link module
			var recordid = $("#smsrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
			}
		});
		//c2c module change
		$("#callmodule").change(function() {
			var moduleid =	$("#callmoduleid").val(); //main module
			var linkmoduleid =	$("#callmodule").val(); // link module
			var recordid = $("#callrecordid").val();
			if(linkmoduleid != '' || linkmoduleid != null) {
				mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
			}
		});
		//sms mobile number select
		$("#smsmobilenumid").change(function() {
			var data = $('#smsmobilenumid').select2('data');
			var finalResult = [];
			for( item in $('#smsmobilenumid').select2('data')) {
				finalResult.push(data[item].id);
			};
			var selectid = finalResult.join(',');
			$("#smsmobilenum").val(selectid);
		});
		//sms overlay submit
		$("#mobilenumbersubmit").click(function() {
			var datarowid = $("#smsrecordid").val();
			var viewfieldids =$("#smsmoduleid").val();
			var mobilenumber =$("#smsmobilenum").val();
			if(mobilenumber != '' || mobilenumber != null) {
				sessionStorage.setItem("mobilenumber",mobilenumber);
				sessionStorage.setItem("viewfieldids",viewfieldids);
				sessionStorage.setItem("recordis",datarowid);
				window.location = base_url+'Sms';
			} else {
				alertpopup('Please Select the mobile number...');
			}	
		});
		//c2c icon click
		$("#outgoingcallicon").click(function() {
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				var viewfieldids = $("#viewfieldids").val();
				$.ajax({
					url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
					dataType:'json',
					async:false,
					cache:false,
					success :function(data) {
						mobilenumber = [];
						if(data != 'No mobile number') {
							for(var i=0;i<data.length;i++) {
								if(data[i] != ''){
									mobilenumber.push(data[i]);
								}
							}
							if(mobilenumber.length > 1) {
								$("#c2cmobileoverlay").show();
								$("#callmoduledivhid").hide();
								$("#calcount").val(mobilenumber.length);
								$("#callrecordid").val(datarowid);
								$("#callmoduleid").val(viewfieldids);
								mobilenumberload(mobilenumber,'callmobilenum');
							} else if(mobilenumber.length == 1){
								clicktocallfunction(mobilenumber);
							} else {
								alertpopup("Invalid mobile number");
							}
						} else {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").show();
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							moduledropdownload(viewfieldids,datarowid,'callmodule');
						}
					},
				});
			} else {
				alertpopup("Please select a row");
			}
		});
		//c2c overlay close
		$("#c2cmobileoverlayclose").click(function() {
			$("#c2cmobileoverlay").hide();
		});
		//c2c submit.
		$("#callnumbersubmit").click(function()  {
			var mobilenum = $("#callmobilenum").val();
			if(mobilenum == '' || mobilenum == null) {
				alertpopup("Please Select the mobile number to call");
			} else {
				clicktocallfunction(mobilenum);
			}
		});
	}
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,contractgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			contractgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
function getandsetdate(dateset) { 
	$('#expirydate').datetimepicker('option', 'minDate', dateset);
}
//view create success function
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset") {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
// Contract View Grid
function contractgrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#contractpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#contractpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $('#contractgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.contractheadercolsort').hasClass('datasort') ? $('.contractheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.contractheadercolsort').hasClass('datasort') ? $('.contractheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.contractheadercolsort').hasClass('datasort') ? $('.contractheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=contract&primaryid=contractid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#contractgrid').empty();
			$('#contractgrid').append(data.content);
			$('#contractgridfooter').empty();
			$('#contractgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('contractgrid');
			{//sorting
				$('.contractheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.contractheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#contractpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.contractheadercolsort').hasClass('datasort') ? $('.contractheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.contractheadercolsort').hasClass('datasort') ? $('.contractheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#contractgrid .gridcontent').scrollLeft();
					contractgrid(page,rowcount);
					$('#contractgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('contractheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					contractgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#contractpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					contractgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			{// Redirected form Home
				var contractideditvalue = sessionStorage.getItem("contractidforedit"); 
				if(contractideditvalue != null){
					setTimeout(function(){ 
						contracteditdatafetchfun(contractideditvalue);
						sessionStorage.removeItem("contractidforedit");
					},100);	
				}
			}
			//Material select
			$('#contractpgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			firstfieldfocus();
			clearformgriddata();
			e.preventDefault();
			addslideup('contractgriddisplay', 'contractaddformdiv');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			froalaset(froalaarray);
			$("#keywords").select2('val',' ');
			$("#crmfileinfoid").val('0');
			$('#filenameattachdisplay').empty();
			$("#contractattingriddel2").show();
			if(softwareindustryid != 4){
				$('#tab2').trigger('click');
				folderrefreshgrid();
				folderdatareload();
				contractattaddgrid2();
				cleargriddata('contractattaddgrid2');
			} else {
				$('#tab1').trigger('click');
				$('#productid').empty();
				contractattaddgrid2();
				cleargriddata('contractattaddgrid2');
			}
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			$("#contractattingriddel2").removeClass('forhidereloadbtn');
			$("#formclearicon").show();
			showhideiconsfun('addclick','contractaddformdiv');
			autogenmaterialupdate();
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				if(softwareindustryid != 4){
					$('#tab2').trigger('click');
					folderrefreshgrid();
					folderdatareload();
					contractattaddgrid2();
					cleargriddata('contractattaddgrid2');
				} else {
					$('#tab1').trigger('click');
					$('#productid').empty();
					contractattaddgrid2();
					cleargriddata('contractattaddgrid2');
				}
				froalaset(froalaarray);
				contracteditdatafetchfun(datarowid);
				Materialize.updateTextFields();
				$("#contractattingriddel2").show();
				showhideiconsfun('editclick','contractaddformdiv');
				$("#contractattaddbutton").removeClass('hidedisplayfwg').show();
				fortabtouch = 1;
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#contractgrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#contractpgnum li.active').data('pagenum');
		var rowcount = $('ul#contractpgnumcnt li .page-text .active').data('rowcount');
		contractgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.folderlistpgnumcnt li .page-text .active').data('rowcount');
		contractfoladdgrid1(page,rowcount);
	}
}
//Contract folder  Grid
function contractfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#contractfoladdgrid1').width();
	var wheight = $('#contractfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Contract/contractfoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=230',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#contractfoladdgrid1').empty();
			$('#contractfoladdgrid1').append(data.content);
			$('#contractfoladdgrid1footer').empty();
			$('#contractfoladdgrid1footer').append();
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('contractfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('ul#folderlistpgnum li.active').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					contractfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smsmasterheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					contractfoladdgrid1(page,rowcount);
				});	
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					contractfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#folderlistpgrowcount').material_select();
		},
	});
}
// Main view Grid
function contractattaddgrid2() {
	refreshgrid();
	if(softwareindustryid == 4) {
		$tabgroupid = 198;
		$moduleid = 80;
		$gridname ='contractattaddgrid1';
	} else{
		$tabgroupid = 76;
		$moduleid = 230;
		$gridname ='contractattaddgrid2';
	}
	var wwidth = $("#"+$gridname+"").width();
	var wheight = $("#"+$gridname+"").height();
	$.ajax({
		url:base_url+"Contract/localgirdheaderinformationfetch?tabgroupid="+$tabgroupid+"&moduleid="+$moduleid+"&width="+wwidth+"&height="+wheight+"&modulename=contractattach",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#"+$gridname+"").empty();
			$("#"+$gridname+"").append(data.content);
			$("#"+$gridname+"footer").empty();
			$("#"+$gridname+"footer").append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize($gridname);
			var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
			gridfieldhide($gridname,hideprodgridcol);
		},
	});
}
//new data add submit function
function newdataaddfun() {
    var formdata = $("#dataaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var neweditordata = froalaeditordataget(editorname);
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	var griddatapartabnameinfo = $('#griddatapartabnameinfo').val();
	var viewfieldids = $('#viewfieldids').val();
	if(softwareindustryid == 4){
		$.ajax({
	        url: base_url + "Contract/newdatacreate",
	        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&datass="+neweditordata+"&griddatapartabnameinfo="+griddatapartabnameinfo+"&viewfieldids="+viewfieldids,
			type: "POST",
			cache:false,
	        success: function(id) {
	        	$("#processoverlay").hide();
	    		CONVERTID = id;
	    		$("#coninvoverlay").fadeIn();	
	        },
	    });		
	}else{
		$.ajax({
	        url: base_url + "Contract/newdatacreate",
	        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&datass="+neweditordata+"&griddatapartabnameinfo="+griddatapartabnameinfo+"&viewfieldids="+viewfieldids,
			type: "POST",
			cache:false,
	        success: function(msg) {
				var nmsg =  $.trim(msg);
	            if (nmsg == 'TRUE') {
					$("#tab2").trigger('click');
					resetFields();
					$('#contractaddformdiv').hide();
					$('#contractgriddisplay').fadeIn(1000);
					refreshgrid();
					clearformgriddata();
					alertpopup(savealert);
					froalaset(froalaarray);
					$("#keywords").select2('val',' ');
					cleargriddata('contractattaddgrid2');
					$("#foldernameid").addClass('validate[required]');
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
	            }
	        },
	    });
	}    
}
//old information show in form
function getformdata(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax( {
		url:base_url+"Contract/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$(".ftab").trigger('click');
				$('#solutionsaddformdiv').hide();
				$('#solutionsgriddisplay').fadeIn(1000);
				refreshgrid();
				//For Keyboard Shortcut Variables
				addformupdate = 0;
				viewgridview = 1;
				addformview = 0;
				//For Left Menu Confirmation
				discardmsg = 0;
				$("#processoverlay").hide();
			} else {
				addslideup('contractgriddisplay', 'contractaddformdiv');
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var attachfldname = $('#attachfieldnames').val();
				var attachcolname = $('#attachcolnames').val();
				var attachuitype = $('#attachuitypeids').val();
				var attachfieldid = $('#attachfieldids').val();
				primaryaddvalfetch(datarowid);
				secondaryaddvalfetch(datarowid);
				contractdocumentdetailsdetail(datarowid);
				editordatafetch(froalaarray,data);
				$("#processoverlay").hide();
			}
		},
	});
}
//primary address value fetchfunction 
function primaryaddvalfetch(datarowid) {
	$.ajax({
		url:base_url+"Contract/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=4", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var datanames = ['7','address','pincode','city','state','country','addresstypeid'];
			var textboxname = ['7','billingaddress','billingpincode','billingcity','billingstate','billingcountry','billingaddresstype'];
			var dropdowns = ['1','billingaddresstype'];
			textboxsetvalue(textboxname,datanames,data,dropdowns); 
			$("#billingaddresstype").select2('val','2');
		}
	});
}
//secondary address value fetch function
function secondaryaddvalfetch(datarowid) {
	$.ajax({
		url:base_url+"Contract/primaryaddressvalfetch?dataprimaryid="+datarowid+"&addtype=5", 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var datanames = ['7','address','pincode','city','state','country','addresstypeid'];
			var textboxname = ['7','shippingaddress','shippingpincode','shippingcity','shippingstate','shippingcountry','shippingaddresstype'];
			var dropdowns = ['1','shippingaddresstype'];
			textboxsetvalue(textboxname,datanames,data,dropdowns); 
			$("#shippingaddresstype").select2('val','2');
		}
	});
}
function contractdocumentdetailsdetail(pid) {
	if(softwareindustryid ==4 ){
		$gridname = 'contractattaddgrid1';
	} else {
		$gridname = 'contractattaddgrid2';
	}
	if(pid!='') {
		$.ajax({
			url:base_url+'Contract/documentsdetailfetch?primarydataid='+pid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					loadinlinegriddata($gridname,data.rows,'json');
					/* data row select event */
					datarowselectevt();
					/* column resize */
					columnresize($gridname);
					var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
					gridfieldhide($gridname,hideprodgridcol);
				}
			},
		});
	}
}
//update old information
function updateformdata() {
	var amp = '&';
	//editor data
	var editorname = $('#editornameinfo').val();
	var editordata = froalaeditoradd(editorname);
	var neweditordata = froalaeditordataget(editorname);
	var gridname = $('#gridnameinfo').val();
	var gridnames = gridname.split(',');
	var datalength = gridnames.length;
	var noofrows=0;
	var addgriddata='';
	if(deviceinfo != 'phone'){
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.data-content div').length;;
			}
		}
	}else{
		for(var j=0;j<datalength;j++) {
			if(j!=0) {
				addgriddata = addgriddata.concat(getgridrowsdata(gridnames[j]));
				noofrows = noofrows+','+$('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			} else {
				addgriddata = getgridrowsdata(gridnames[j]);
				noofrows = $('#'+gridnames[j]+' .gridcontent div.wrappercontent div').length;;
			}
		}
	}	
	var sendformadddata = JSON.stringify(addgriddata);
	var formdata = $("#dataaddform").serialize();
	var datainformation = amp + formdata;
	if(softwareindustryid == 4){
		 $.ajax({
		        url: base_url + "Contract/datainformationupdate",
		        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&datass="+neweditordata,
				type: "POST",
				cache:false,
		        success: function(id) {
		        	$("#processoverlay").hide();
		    		CONVERTID = id;
		    		$("#coninvoverlay").fadeIn();
		        },
		    });
	}else{
		 $.ajax({
		        url: base_url + "Contract/datainformationupdate",
		        data: "datas=" + datainformation+"&griddatas="+sendformadddata+"&numofrows="+noofrows+"&datass="+neweditordata,
				type: "POST",
				cache:false,
		        success: function(msg) {
					var nmsg =  $.trim(msg);
		            if (nmsg == 'TRUE') {
						$("#tab2").trigger('click');
						resetFields();
						$('#contractaddformdiv').hide();
						$('#contractgriddisplay').fadeIn(1000);
						refreshgrid();
						clearformgriddata();
						alertpopup(savealert);
						$("#foldernameid").addClass('validate[required]');
						//For Keyboard Shortcut Variables
						addformupdate = 0;
						viewgridview = 1;
						addformview = 0;
						$("#processoverlay").hide();
		            }
		        },
		    });
	}   
}
{// Clear grid data
	function clearformgriddata() {
		var gridname = $('#gridnameinfo').val();
		var gridnames = gridname.split(',');
		var datalength = gridnames.length;
		for(var j=0;j<datalength;j++) {
			cleargriddata(gridnames[j]);
		}
	}
}
//delete operation
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Contract/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            }  else if (nmsg == "Denied") {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
			}
        },
    });
}
//setaddress
function setaddress(id) {
	var value=$('#'+id+'').val();
	if(id == 'billingaddresstype') {
		var type='Billing';
	} else {
		var type='Shipping';
	}
	//account address
	if(value == 2) {
		var accountid=$('#accountid').val();
		getaddress(accountid,'account',type);
	} else if(value == 3) {   
		var addressid =$('#contactid').val();
		getaddress(addressid,'contact',type);
	}
	//swap the billing to shipping
	else if(value == 5){
		$('#billingaddress').val($('#shippingaddress').val());
		$("#billingarea").val($('#shippingarea').val());
		$('#billingpincode').val($('#shippingpincode').val());
		$('#billingcity').val($('#shippingcity').val());
		$('#billingstate').val($('#shippingstate').val());
		$('#billingcountry').val($('#shippingcountry').val());	
	}
	//swap the shipping to billing
	else if(value == 6){
		$('#shippingaddress').val($('#billingaddress').val());
		$("#shippingarea").val($('#billingarea').val());
		$('#shippingpincode').val($('#billingpincode').val());
		$('#shippingcity').val($('#billingcity').val());
		$('#shippingstate').val($('#billingstate').val());
		$('#shippingcountry').val($('#billingcountry').val());	
	}
}
function getaddress(id,table,type) {
	if(id != null && id != '' && table != null && table !='') {
		var append=type.toLowerCase();
		$.ajax({
			url:base_url+'Base/getcrmaddress?table='+table+'&id='+id+'&type='+type,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if(data.result == true) {
					var datanames = ['5','address','pincode','city','state','country'];
					var textboxname = ['5',''+append+'address',''+append+'pincode',''+append+'city',''+append+'state',''+append+'country'];
					textboxsetvalue(textboxname,datanames,data,[]);
				}
				$("#shippingarea").val('');
			},
		});
	}
}
function gridformtogridvalidatefunction(gridnamevalue) {
	jQuery("#"+gridnamevalue+"validation").validationEngine({
		validateNonVisibleFields:false,
		onSuccess: function() {  
			var i = griddataid;
			var gridname = gridynamicname;
			var coldatas = $('#gridcolnames'+i+'').val();
			var columndata = coldatas.split(',');
			var coluiatas = $('#gridcoluitype'+i+'').val();
			var columnuidata = coluiatas.split(',');
			var datalength = columndata.length;
			/*Form to Grid*/
			if(METHOD == 'ADD' || METHOD == '') {
				formtogriddata(gridname,METHOD,'last','');
			} else if(METHOD == 'UPDATE') {
				var selectedrow = $('#'+gridname+' div.gridcontent div.active').attr('id');
				formtogriddata(gridname,METHOD,'',selectedrow);
			}
			/* Hide columns */
			var hideprodgridcol = ['crmfileinfoid','filename_fromid','filename_path'];
			gridfieldhide('contractattaddgrid2',hideprodgridcol);
			/* Data row select event */
			datarowselectevt();
			clearform('gridformclear');
			$("#contractattcancelbutton").trigger('click');
			$("#keywords").select2('val','');
			setTimeout(function(){
				fname_828 = [];
				fsize_828 = [];
				ftype_828 = [];
				fpath_828 = [];
				$("#crmfileinfoid").val('0');
				$('#filenameattachdisplay').empty();
			});
		},
		onFailure: function(){
			var dropdownid =['1','foldernameid'];
			dropdownfailureerror(dropdownid);
		}
	});
}
{// Edit Function
	function contracteditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();
		cleargriddata('contractattaddgrid2');
		froalaset(froalaarray);
		$('#filenameattachdisplay').empty();
		getformdata(datarowid);
		firstfieldfocus();
		$("#crmfileinfoid").val('0');
		$("#contractattingriddel2").addClass('forhidereloadbtn');
		$(".forhidereloadbtn").click(function(){
			$("#formclearicon").hide();
		});
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{// Clone Function
	function contractclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		cleargriddata('contractattaddgrid2');
		froalaset(froalaarray);
		$('#filenameattachdisplay').empty();
		getformdata(datarowid);
		firstfieldfocus();
		//for autonumber
		randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");	
		$("#crmfileinfoid").val('0');
		$("#contractattingriddel2").addClass('forhidereloadbtn');
		$(".forhidereloadbtn").click(function(){
			$("#formclearicon").hide();
		});
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
	}
}
{//contact name fetch
	function contactnamefetch(accid){
		$('#contactid').empty();
		$('#contactid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Contract/fetchcontatdatawithmultiplecond?accid='+accid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#contactid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#contactid').trigger('change');
			},
		});
	}
}
{
	if(softwareindustryid == 4){
		$("#contactid").change(function(){
			var contactid = $(this).val();
			if(contactid){
				$('#productid').empty();
				$.ajax({
					url:base_url+'Contract/fetchmembersdetailsbasedondate?contactid='+contactid,
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							$.each(data, function(index) {
								$('#productid')
								.append($("<option></option>")
								.attr("value",data[index]['productid'])
								.attr("data-productidhidden",data[index]['productname'])
								.text(data[index]['productname']));
							});
						}	
						$('#productid').select2('val','').trigger('change');
					},
				});
			}			
		});
		$("#productid").change(function(){
			var planid = $(this).val();
			if(planid){
				$.ajax({
					url:base_url+"Contract/membershipplansdatafectchoncontact?planid="+planid,
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#existingdurationid").select2('val',data.durationid).trigger("change");
						$("#existingplancost").val(data.amount);	
						$('#startdate').datetimepicker('setDate',data.joiningdate);
						$('#expirydate').datetimepicker('setDate',data.expirydate);
						Materialize.updateTextFields();
					}
				});
			}else{
				$("#existingdurationid").select2('val','');
				$("#existingplancost").val('');
				$('#startdate').val('');
				$('#expirydate').val('');
			}				
		});
		$("#renewalmembershipplanname").change(function(){
			var planid = $(this).val();
			if(planid){
				$.ajax({
					url:base_url+"Contact/membershipplansdatafectch?planid="+planid,
					dataType:'json',
					async:false,
					cache:false,
					success:function(data) {
						$("#renewaldurationid").select2('val',data.durationid);
						$("#renewalplancost").val(data.unitprice);	
						Materialize.updateTextFields();
					}
				});
			}else{
				$("#renewaldurationid").select2('val','');
				$("#renewalplancost").val('');
				$('#renewalstartdate').val('');
				$('#renewalexpirydate').val('');
			}				
		});	
		$('#renewalstartdate').change(function(){
			dateexpiry();
		});
		$('#renewaldurationid').change(function(){
			dateexpiry();
		});
		function dateexpiry(){
			var duration = $('#renewaldurationid').val();
			var joiningdate = $('#renewalstartdate').datepicker('getDate');
			var month = 0;
			if(joiningdate){
				if(duration == 2){
					month = 1;
					var CurrentDate = new Date(joiningdate);
					CurrentDate.setMonth(CurrentDate.getMonth() + month);								
					$('#renewalexpirydate').datetimepicker('setDate',CurrentDate);
				}else if(duration == 3){
					month = 3;
					var CurrentDate = new Date(joiningdate);
					CurrentDate.setMonth(CurrentDate.getMonth() + month);								
					$('#renewalexpirydate').datetimepicker('setDate',CurrentDate);
				}else if(duration == 4){
					month = 6;
					var CurrentDate = new Date(joiningdate);
					CurrentDate.setMonth(CurrentDate.getMonth() + month);								
					$('#renewalexpirydate').datetimepicker('setDate',CurrentDate);
				}else if(duration == 5){
					month = 12;
					var CurrentDate = new Date(joiningdate);
					CurrentDate.setMonth(CurrentDate.getMonth() + month);								
					$('#renewalexpirydate').datetimepicker('setDate',CurrentDate);
				}
			}				
		}
	}
}
function autogenmaterialupdate(){
	 if($('#contractnumber').val().length > 0){
		  $('#contractnumber').siblings('label, i').addClass('active');
	 }else{}
}
{//folder operation
	function contractfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Contract/contractfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#foldernamename,#description").val('');
					$("#publiccboxid,#setdefaultcboxid").prop('checked',false);
					folderrefreshgrid();
					folderdatareload();
					cleargriddata('contractfoladdgrid1');
					$('#dataaddsbtn').attr('disabled',false);
					//section close
					$('#contractfolcancelbutton').trigger('click');
					alertpopup(savealert);
					folderdatareload();
					folderrefreshgrid();
					Materialize.updateTextFields();
				}
			},
		});
	}
	//Edit data Fetch
	function contractfolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Contract/contractfolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes'){
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
					var ppublic = data.ppublic;
					if(ppublic == 'Yes'){
						$("#publiccboxid").prop('checked',true);
						$("#public").val('Yes');
					} else {
						$("#publiccboxid").prop('checked',false);
						$("#public").val('No');
					}
					$("#foldernameeditid").val(datarowid);
				}
			},
		});
	}
	//SMS templates folder add function
	function contractfolderupdate(){
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		var ppublic = $("#public").val();
		$.ajax({
			url:base_url+'Contract/contractfolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid+'&ppublic='+ppublic,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$("#foldernamename,#description").val('');
					$("#publiccboxid,#setdefaultcboxid").prop('checked',false);
					folderrefreshgrid();
					cleargriddata('contractfoladdgrid1');
					folderrefreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					folderdatareload();
					$("#contractfoladdbutton,#contractfolingriddel1").show();
					$("#contractfolupdatebutton").hide();
					//section close
					$('#contractfolcancelbutton').trigger('click');
					alertpopup(savealert);
				}
			},
		});
	}
	//delete folder 
	function deletefolderoperation(id){
		$.ajax({
			url:base_url+'Contract/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#contractfolingriddel1').attr('disabled',false);
					alertpopup("Deleted successfully");
				} else if(nmsg == 'DEFAULT') {
					alertpopup("Can't delete default records.");
					folderrefreshgrid();
					$('#documentsfoladdgrid1').attr('disabled',false);
				}
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#foldernameid').empty();
		$('#foldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Contract/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=230',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#foldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes'){
							$('#foldernameid').select2('val',data[index]['datasid']).trigger('change');
						}
					});
				}
			},
		});
	}
}
//Unique Folder Name check - Kumaresan
function foldernamecheck() {
	var primaryid = $("#foldernameeditid").val();
	var foldernamename = $("#foldernamename").val();
	var nmsg = "";
	if( foldernamename != "" ) {
		$.ajax({
			url:base_url+"Contract/foldernameunique",
			data:"&foldernamename="+foldernamename,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder Name already exists!";
				}
			} else {
				return "Folder Name already exists!";
			}
		} 
	}
}