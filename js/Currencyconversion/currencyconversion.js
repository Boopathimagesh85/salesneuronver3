$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		currencyconversionaddgrid();
		firstfieldfocus();
		currencyconversioncrudactionenable();
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			currencyconversionaddgrid();
		});
	}
	//hidedisplay
	$('#currencyconversiondataupdatesubbtn').hide();
	$('#currencyconversionsavebutton').show();
	{//keyboard shortcut reset global variable
		viewgridview=0;
		addformview=0;
	}
	$('#tab2').click(function()  {
		setTimeout(function() {
				currencyconversionaddgrid();
		}, 100);
	});	
	{//validation for Company Add  
		$('#currencyconversionsavebutton').click(function()  {
			$("#currencyconversionformaddwizard").validationEngine('validate');
			//for touch
			masterfortouch = 0;	
		});	
		jQuery("#currencyconversionformaddwizard").validationEngine( {
			onSuccess: function() {
				currencyconvaddformdata();
			},
			onFailure: function() {
				var dropdownid =['2','currencytoid','currencyid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$("#currencyconversionreloadicon").click(function(){
			clearform('cleardataform');
			currencyconversionrefreshgrid();
			//Reset Date Picker
			daterangefunction();
			$('#currencyconversiondataupdatesubbtn').hide();
			$('#currencyconversionsavebutton').show();
			$("#currencyconversiondeleteicon").show();
		});
		$( window ).resize(function() {
			innergridresizeheightset('currencyconversionaddgrid');
			sectionpanelheight('currencyconversionsectionoverlay');
		});
	}
	{//update company information
		$('#currencyconversiondataupdatesubbtn').click(function() {
			$("#currencyconversionformeditwizard").validationEngine('validate');
		});
		jQuery("#currencyconversionformeditwizard").validationEngine({
			onSuccess: function() {
				currencyconvupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','currencytoid','currencyid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Date Range
		daterangefunction();
	}		
	{//filter work
		//for toggle
		$('#currencyconversionviewtoggle').click(function() {
			if ($(".currencyconversionfilterslide").is(":visible")) {
				$('div.currencyconversionfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.currencyconversionfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#currencyconversionclosefiltertoggle').click(function(){
			$('div.currencyconversionfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#currencyconversionfilterddcondvaluedivhid").hide();
		$("#currencyconversionfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#currencyconversionfiltercondvalue").focusout(function(){
				var value = $("#currencyconversionfiltercondvalue").val();
				$("#currencyconversionfinalfiltercondvalue").val(value);
			});
			$("#currencyconversionfilterddcondvalue").change(function(){
				var value = $("#currencyconversionfilterddcondvalue").val();
				if( $('#s2id_currencyconversionfilterddcondvalue').hasClass('select2-container-multi') ) {
					currencyconversionmainfiltervalue=[];
					$('#currencyconversionfilterddcondvalue option:selected').each(function(){
					    var $currencyconvvalue =$(this).attr('data-ddid');
					    currencyconversionmainfiltervalue.push($currencyconvvalue);
						$("#currencyconversionfinalfiltercondvalue").val(currencyconversionmainfiltervalue);
					});
					$("#currencyconversionfilterfinalviewconid").val(value);
				} else {
					$("#currencyconversionfinalfiltercondvalue").val(value);
					$("#currencyconversionfilterfinalviewconid").val(value);
				}
			});
		}
		currencyconversionfiltername = [];
		$("#currencyconversionfilteraddcondsubbtn").click(function() {
			$("#currencyconversionfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#currencyconversionfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(currencyconversionaddgrid,'currencyconversion');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//Documents Add Grid
function currencyconversionaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#currencyconversionaddgrid").width();
	var wheight = $("#currencyconversionaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#currencyconversionsortcolumn").val();
	var sortord = $("#currencyconversionsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.currencyconversionheadercolsort').hasClass('datasort') ? $('.currencyconversionheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.currencyconversionheadercolsort').hasClass('datasort') ? $('.currencyconversionheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.currencyconversionheadercolsort').hasClass('datasort') ? $('.currencyconversionheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#currencyconversionviewfieldids').val();
	if(userviewid != '') {
		userviewid= '79';
	}
	var filterid = $("#currencyconversionfilterid").val();
	var conditionname = $("#currencyconversionconditionname").val();
	var filtervalue = $("#currencyconversionfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=currencyconversion&primaryid=currencyconversionid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#currencyconversionaddgrid').empty();
			$('#currencyconversionaddgrid').append(data.content);
			$('#currencyconversionaddgridfooter').empty();
			$('#currencyconversionaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('currencyconversionaddgrid');
			{//sorting
				$('.currencyconversionheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.currencyconversionheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#currencyconversionpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.currencyconversionheadercolsort').hasClass('datasort') ? $('.currencyconversionheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.currencyconversionheadercolsort').hasClass('datasort') ? $('.currencyconversionheadercolsort.datasort').data('sortorder') : '';
					$("#currencyconversionsortorder").val(sortord);
					$("#currencyconversionsortcolumn").val(sortcol);
					var sortpos = $('#currencyconversionaddgrid .gridcontent').scrollLeft();
					currencyconversionaddgrid(page,rowcount);
					$('#currencyconversionaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('currencyconversionheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					currencyconversionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#currencyconversionpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					currencyconversionaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#currencyconversionaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#currencyconversionpgrowcount').material_select();
		},
	});
}
function currencyconversioncrudactionenable() {
	{//add icon click
		$('#currencyconversionaddicon').click(function(e){
			sectionpanelheight('currencyconversionsectionoverlay');
			$("#currencyconversionsectionoverlay").removeClass("closed");
			$("#currencyconversionsectionoverlay").addClass("effectbox");
			e.preventDefault();
			currencydynamicloaddropdown();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#currencyconversiondataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#currencyconversionsavebutton').show();
		});
	}
	$("#currencyconversionediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#currencyconversionaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$('#currencyconversiondataupdatesubbtn').show().removeClass('hidedisplayfwg');
			$('#currencyconversionsavebutton').hide();
			currencydynamicloaddropdown();
			currencyconvgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			//for touch
			masterfortouch = 1;	
			//Keyboard short cut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#currencyconversiondeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#currencyconversionaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('currencyconvdeleteyes');
			$("#currencyconvdeleteyes").click(function(){
				var datarowid = $('#currencyconversionaddgrid div.gridcontent div.active').attr('id');
				currencyconvrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function currencyconversionrefreshgrid() {
		var page = $('ul#currencyconversionpgnum li.active').data('pagenum');
		var rowcount = $('ul#currencyconversionpgnumcnt li .page-text .active').data('rowcount');
		currencyconversionaddgrid(page,rowcount);
	}
}
//new data add submit function
function currencyconvaddformdata() {
    var formdata = $("#currencyconversionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var fromid = $("#currencyid").val();
	var toid = $("#currencytoid").val();
	if(fromid != toid) {
		$.ajax( {
			url: base_url + "Currencyconversion/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$(".addsectionclose").trigger("click");
					currencyconversionrefreshgrid();
					$("#currencyconversionsavebutton").attr('disabled',false); 
					alertpopup(savealert);
				}
			},
		});
	} else{
		alertpopup('From Currency and To Currency Should not be SAME');
		$("#currencyconversionsavebutton").attr('disabled',false); 
	}
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#currencyconversionsectionoverlay").removeClass("effectbox");
		$("#currencyconversionsectionoverlay").addClass("closed");
	});
}
//old information show in form
function currencyconvgetformdata(datarowid) {
	var currencyelementsname = $('#currencyconversionelementsname').val();
	var currencyelementstable = $('#currencyconversionelementstable').val();
	var currencyelementscolmn = $('#currencyconversionelementscolmn').val();
	var currencyelementpartable = $('#currencyconversionelementspartabname').val();
	$.ajax(	{
		url:base_url+"Currency/fetchformdataeditdetails?currencyprimarydataid="+datarowid+"&currencyelementsname="+currencyelementsname+"&currencyelementstable="+currencyelementstable+"&currencyelementscolmn="+currencyelementscolmn+"&currencyelementpartable="+currencyelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$("#currencyconversiondeleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				currencyconversionrefreshgrid();
			} else {
				sectionpanelheight('currencyconversionsectionoverlay');
				$("#currencyconversionsectionoverlay").removeClass("closed");
				$("#currencyconversionsectionoverlay").addClass("effectbox");
				var txtboxname = currencyelementsname + ',currencyconversionprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = currencyelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}
		}
	});
	$('#currencydataupdatesubbtn').show();
	$('#currencysavebutton').hide();
}
//update old information
function currencyconvupdateformdata() {
	var formdata = $("#currencyconversionaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var fromid = $("#currencyid").val();
	var toid = $("#currencytoid").val();
	if(fromid != toid) {
		$.ajax({
			url: base_url + "Currencyconversion/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if(nmsg == 'TRUE') {
					$('#currencyconversiondataupdatesubbtn').hide();
					$("#currencyconversiondeleteicon").show();
					$('#currencyconversionsavebutton').show();
					resetFields();
					$(".addsectionclose").trigger("click");
					currencyconversionrefreshgrid();
					alertpopup(savealert);
					//for touch
					masterfortouch = 0;	
					//Keyboard short cut
					saveformview = 0;
				}
			},
		});	
	}
	else{
		alertpopup('From Currency and To Currency Should not be SAME');
	}
}
//Record Delete
function currencyconvrecorddelete(datarowid) {
	var currencyconversionelementstable = $('#currencyconversionelementstable').val();
	var currencyconversionelementspartable = $('#currencyconversionelementspartabname').val();
    $.ajax({
        url: base_url + "Currencyconversion/deleteinformationdata?currencyconversionprimarydataid="+datarowid+"&currencyconversionelementstable="+currencyconversionelementstable+"&currencyconversionelementspartable="+currencyconversionelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	currencyconversionrefreshgrid();
            	$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "false")  {
            	currencyconversionrefreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
{//date Range
	function daterangefunction() { 
		$('#validfrom').datetimepicker("destroy");
		$('#validto').datetimepicker("destroy");
		var startdateformater = $('#validfrom').attr('data-dateformater');
		var startDateTextBox = $('#validfrom');
		var endDateTextBox = $('#validto');
		startDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			minDate:0,
			onClose: function(dateText, inst) {
				if (endDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						endDateTextBox.datetimepicker('setDate', testStartDate);
					Materialize.updateTextFields();
				}
				else {
					endDateTextBox.val(dateText);
					Materialize.updateTextFields();
				}
				$('#validfrom').focus();
			},
			onSelect: function (selectedDateTime){
				endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
					Materialize.updateTextFields();
				}
			}
		});
		endDateTextBox.datetimepicker({ 
			timeFormat: 'HH:mm z',
			dateFormat: startdateformater,
			onClose: function(dateText, inst) {
				if (startDateTextBox.val() != '') {
					var testStartDate = startDateTextBox.datetimepicker('getDate');
					var testEndDate = endDateTextBox.datetimepicker('getDate');
					if (testStartDate > testEndDate)
						startDateTextBox.datetimepicker('setDate', testEndDate);
						Materialize.updateTextFields();
				}
				else {
					startDateTextBox.val(dateText);
					Materialize.updateTextFields();
				}
				$('#validto').focus();
			},
			onSelect: function (selectedDateTime){
				startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
				Materialize.updateTextFields();
			}
		});
	}
}
//Currency Conversion name check
function chargescategorynamecheck() {
	var primaryid = $("#chargescategoryprimarydataid").val();
	var accname = $("#additionalchargecategoryname").val();
	var elementpartable = $('#chargescategoryelementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Currency conversion name already exists!";
				}
			}else {
				return "Currency conversion name already exists!";
			}
		} 
	}
}
function currencydynamicloaddropdown() {
	$('#currencyid,#currencytoid').empty();
	$('#currencyid,#currencytoid').append($("<option></option>"));
	$.ajax({
		url:base_url+"Currencyconversion/currencyddload",
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#currencyid,#currencytoid')
					.append($("<option></option>")
					.attr("data-currencyid",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
		},
	});
}