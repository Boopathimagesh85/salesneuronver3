$(document).ready(function() { 
	{//Foundation Initialization
		$(document).foundation();
	}
	counterusestatus = $('#counteryesno').val();
	storagedisplay = $('#storagedisplay').val();
	storagequantity = $('#storagequantity').val();
	//sales date change
	salesdatechangeablefor = $("#salesdatechangeablefor").val();
	splittaggrid();
	stoneentrygrid();
	rate_round = $('#rate_round').val();
	round = $("#weight_round").val(); // To get the value of weight round.
	dia_weight_round = parseFloat($('#dia_weight_round').val());
	roundamount = parseFloat($('#amount_round').val());
	stonepiecescalc = $('#stonepiecescalc').val();
	//form submission preventing on enter key from fields
	$("form").bind('keypress', function(event){
		if(event.keyCode == 13){ 
			event.preventDefault();
			return false;
		}
	});
	generateiconafterload('splitstoneweight','stoneweighticon');
	{ // Stone Details Automatic
		$("#s2id_stoneid").css('display','inline-block').css('width','90%');
		var icontabindex = $("#s2id_stoneid .select2-focusser").attr('tabindex');
		$("#stoneid").after('<span class="innerfrmicon" id="automaticstonedetails" style="position:absolute;top:0px;cursor:pointer" tabindex="'+icontabindex+'"><i class="material-icons">radio_button_checked</i></span>');
	}
	$( window ).resize(function() {
		maingridresizeheightset('splittaggrid');
	});
	{// View by drop down change
		$('#dynamicdddataview').change(function(){
			splittaggrid();
		});
	}
	{// Close Add Screen
		var addcloseleadcreation =["closeaddform","splittagcreationview","splittagformadd",""];
		addclose(addcloseleadcreation);
	}
	{// crud operations
		//stone add icon
		$('#addicon').click(function(){ 
			addslideup('splittagcreationview','splittagformadd');
			resetFields();
			$("#splittagclearicon").show();
			getcurrentsytemdate('splittagdate');
			splittaginnergrid();
			setTimeout(function(){
				$('.editformsummmarybtnclass').addClass('hidedisplay');
				$('.updatebtnclass,.clonebtnclass').addClass('hidedisplay');
				$('#splittagupdate,#splittagupdateclone').hide();
				$("#splittagadd").show();
				$('.addbtnclass').removeClass('hidedisplay');
			},25);
			$("#itemtagnumber,#itemtagid").val('');
			$("#product").select2('val',1);
			$("#grossweight,#stoneweight,#netweight,#hidstonecaratweight,#hidstonegram,#hidstonepieces,#allentryids,#checktagstone").val('0');
			$("#splitpieces").val('1');
			$("#date,#product,#purityid,#splitcounterid,#splitsize").prop('disabled', true);
			$("#itemtagnumber").attr('readonly',false);
			Materialize.updateTextFields();
			firstfieldfocus();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		//stone close icon
		var addcloseviewcreation =["viewcloseformiconid","splittagcreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
		$("#dataaddsbtn").click(function() {
			setzero(['splitgrossweight','grossweight']);
			var gwt = getgridcolvalue_split('splittaginnergrid','','splitgrossweight','sum');
			var oldgross = $('#grossweight').val();
			var stonecaratweight = $('#hidstonecaratweight').val();
			var stonegram = $('#hidstonegram').val();
			var stonepieces = $('#hidstonepieces').val();
			var checktagstone = $('#checktagstone').val();
			if(parseFloat(gwt) == parseFloat(oldgross) ) {
				if((stonecaratweight > 0 || stonegram > 0 || stonepieces > 0) && checktagstone == 1) {
					var result = checkallstonedatacomplete();
					if(result == 'FAIL') {
						alertpopup('Tag Stone Entries and Split tag stone entries are not completed!');
						return false;
					}
				}
				$("#splittagform").validationEngine('validate');
			} else {
				alertpopup('Please add split tag gross weight equal to the itemtag gross weight');
			}
		});
		jQuery("#splittagform").validationEngine({
			onSuccess: function() {
				msgstatus = 0;
				addform();
			},
			onFailure: function() {				
				var dropdownid =[];
				dropdownfailureerror(dropdownid);				
			}
		});
		//Delete stone charge
		$("#deleteicon").click(function(){
			var datarowid = $('#splittaggrid div.gridcontent div.active').attr('id'); 
			if(datarowid){
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}	
		});
		//stone charge yes delete
		$("#basedeleteyes").click(function() { 
			var datarowid = $('#splittaggrid div.gridcontent div.active').attr('id'); 
			$.ajax({
				url: base_url + "Splittag/splittagdelete?primaryid="+datarowid,
				success: function(msg) {				
					var nmsg =  $.trim(msg);
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					if (nmsg == "SUCCESS") {
						alertpopup('Deleted successfully');
					} else  {					
						alertpopup('error');
					}				
				},
			});
		});	
	}
	$('#splitgrossweight').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#splittagdetailsubmit').trigger('click');
		}
	});
	$('#splittagdetailsubmit').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#splittagdetailsubmit').trigger('click');
		}
	});
	{//filter
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,stoneaddonsgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
	}
	{//date
		if(salesdatechangeablefor != 1) { // for admin role only sales date will accessable
			if(loggedinuserrole == 2) {// admin role
				mindate = null;
				maxdate = null;
			} else { //other role
				mindate =0;
				maxdate =0;
			}
		}
		//for date picker
		var dateformetdata = $('#splittagdate').attr('data-dateformater');
		$('#splittagdate').datetimepicker({
			dateFormat: dateformetdata,
			showTimepicker :false,
			changeMonth: true,
			changeYear: true,
			minDate: mindate,
			maxDate: maxdate,
			yearRange : '1947:c+100',
			onSelect: function () {
				var checkdate = $(this).val();
				if(checkdate != ''){
					$(this).next('span').remove('.btmerrmsg'); 
					$(this).removeClass('error');
				}
			},
			onClose: function () {
				$('#splittagdate').focus();
			}
		});
		//Item tag data fetch
		$("#itemtagnumber").change(function(){
			var tagnumber = $('#itemtagnumber').val();
			$.ajax({
				url:base_url+"Splittag/retrievetagdata",
				data:{tagnumber:tagnumber},
				type:"GET",
				dataType:'json',
				async:false,
				success :function(data) {
					if(data.output == 'SOLD') {
						clearfield();
						alertpopup("Tag No : "+tagnumber+" is sold,enter a active tag");
					} else if(data.output == 'NO') {
						clearfield();
						alertpopup("Tag No : "+tagnumber+" is not exist");
					} else if(data.grossweight == 0) {
						clearfield();
						alertpopup("Price Tag is not applicable");
					} else {
						var elementsname=['5','itemtagid' ,'product','grossweight','stoneweight','netweight','hidstonecaratweight','hidstonegram','hidstonepieces','allentryids'];
						var txtboxname = elementsname;
						var dropdowns = ['1','product'];
						textboxsetvalue(txtboxname,txtboxname,data,dropdowns);
						$("#purityid").select2('val',data.purityid);
						$("#splitproduct").select2('val',data.product).trigger('change');
						$("#splitcounterid").select2('val',data.counterid);
						$("#splitsize").select2('val',data.size);
						$("#purityid,#splitcounterid").attr('readonly',true);
						$("#hidstonecaratweight").val(data.stonecaratweight);
						$("#hidstonegram").val(data.stonegram);
						$("#hidstonepieces").val(data.stonepieces);
						$("#allentryids").val(data.allentryids);
						$("#checktagstone").val(data.checktagstone);
						if(data.stonecaratweight > 0 || data.stonegram > 0 || data.stonepieces > 0) {
							$('#splitstoneweight').attr('readonly',true);
							$('#stoneweighticon').show();
						} else {
							$('#splitstoneweight').attr('readonly',true);
							$('#stoneweighticon').hide();
						}
						Materialize.updateTextFields();
					}
				}	
			});
		});
		$("#purityid").change(function(){
			$('#splitproduct,#splitcounterid,#splitsize').select2('val','');
			$("#splitproduct option").addClass("ddhidedisplay");
			$("#splitproduct optgroup").addClass("ddhidedisplay");
			$("#splitproduct option").prop('disabled',true);
			var purityid=$(this).val();
			var tag_entrytype = 4;
			var tagtype = 2;
			if(tag_entrytype == 4){
				$("#splitproduct option[data-purityid="+purityid+"]").removeClass("ddhidedisplay");
				$("#splitproduct option[data-purityid="+purityid+"]").closest('optgroup').removeClass("ddhidedisplay");
				$("#splitproduct option[data-purityid="+purityid+"]").prop('disabled',false);
			}
			Materialize.updateTextFields();
		});
		//product changes//
		$("#splitproduct").change(function(){
			var productidval = $(this).val();
			//display only product-related counters
			if(checkValue(productidval) == true){
				loadproductbasedcounteritem('splitproduct','splitcounterid');
			}
			producteditstatus=0;
			var stoneapplicable = $(this).find('option:selected').data('stoneapplicable');
			stonedivhideshow(stoneapplicable);
			sizehideshow(productidval);
			Materialize.updateTextFields();
		});
	}
	{
		//auto calculate ->grosswt/netweight/stoneweight
		$(".weightcalculate").change(function(event){			
			setzero(['splitgrossweight','splitstoneweight','splitnetweight']);			
			var fieldid = $(this).attr("id");
			var type = $('#'+fieldid+'').data('calc');
			var regxg =/^(\d*\.?\d*)$/;
			var stocknetweight = $('#stocknetweight').val();
			var stockgrossweight = $('#stockgrossweight').val();
			var stockpieces = $('#stockpieces').val();
			var tag_entrytype = $('#tagentrytypeid').find('option:selected').val();
			var stone = $("#splitproduct").find('option:selected').data('stone');
			var stonevalcetype = $("#splitproduct").find('option:selected').data('stonevalcetype');
			var metalid = $("#purityid").find('option:selected').data('metalid');
			if(type == 'GWT'){
				var grosswt = $('#'+fieldid+'').val();
				if (!regxg.test(grosswt)) {
				}else{
					  if (grosswt.toString().indexOf('.') != -1) {
							var substr = grosswt.split('.');
							var roundlength=substr[1].length; 
						}else{
							var roundlength=0;
						}
						if(round < roundlength)
						{
							return false;
						}
					var stonewt = $('#splitstoneweight').val();
					if(stone == 'Yes'){ //if stone applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var netwt = (parseFloat(grosswt)).toFixed(round);
						} else { // reduce the stone
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						}
						var thisval=parseFloat($("#splitgrossweight").val()).toFixed(round);
							$("#splitgrossweight").val(thisval);
							$('#splitstoneweight').val(parseFloat(stonewt).toFixed(round));
							$('#splitnetweight').val(netwt);
					} else { //if stone not applicable for this product  do the following
						var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						var thisval=parseFloat($("#splitgrossweight").val()).toFixed(round);
						$("#splitgrossweight").val(thisval);
						$('#splitstoneweight').val(parseFloat(stonewt).toFixed(round));
						$('#splitnetweight').val(netwt);
					}
					Materialize.updateTextFields();
				}
			} else if(type == 'SWT') {
				var grosswt = $('#splitgrossweight').val();
				var stonewt = $('#'+fieldid+'').val();
				if (!regxg.test(stonewt)) {
				}else{
					if(stone == 'Yes'){ //if stone applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var netwt = (parseFloat(grosswt)).toFixed(round);
						} else { // reduce the stone
							var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						}
						var thisval=parseFloat($("#splitstoneweight").val()).toFixed(round);
						$('#splitstoneweight').val(thisval);
						$('#splitnetweight').val(netwt);
					} else { //if stone not applicable for this product  do the following
						var thisval=parseFloat($("#splitstoneweight").val()).toFixed(round);
						var netwt = (parseFloat(grosswt)-parseFloat(stonewt)).toFixed(round);
						$('#splitstoneweight').val(thisval);
						$('#splitnetweight').val(netwt);
					}
					
					Materialize.updateTextFields();
				}
			} else if(type == 'NWT') {
				var grosswt = $('#splitgrossweight').val();
				var netwt = $('#splitnetweight').val();
				if (!regxg.test(netwt)) {
				}else{
					if(stone == 'Yes'){ //if stone not applicable for this product  do the following
						if(stonevalcetype== 3) { // dont reduce the stone
							var stonewt = (parseFloat(grosswt)).toFixed(round);
						} else { //reduce the stone
							var stonewt = (parseFloat(grosswt)-parseFloat(netwt)).toFixed(round);
						}
						var thisval=parseFloat($("#splitnetweight").val()).toFixed(round);
						$('#splitstoneweight').val(stonewt);
						$('#splitnetweight').val(thisval);
					} else { //if stone not applicable for this product  do the following
						var thisval=parseFloat($("#splitnetweight").val()).toFixed(round);
						var stonewt = (parseFloat(grosswt)-parseFloat(netwt)).toFixed(round);
						$('#splitstoneweight').val(stonewt);
						$('#splitnetweight').val(thisval);
					}
					Materialize.updateTextFields();
				}
			}
		});
	}
	{//Rule criteria cond create
		$('#splittagdetailsubmit').click(function() {
			if($("#itemtagnumber").val() != '') {
				setzero(['splitgrossweight','grossweight']);
				var gwt = getgridcolvalue_split('splittaginnergrid','','splitgrossweight','sum');
				var newgwt = $('#splitgrossweight').val();
				var gross = parseFloat(parseFloat(gwt)+parseFloat(newgwt)).toFixed(round);
				var oldgross = $('#grossweight').val();
				if(parseFloat(gross) > parseFloat(oldgross)) {
					alertpopup('Split gross weight is greater than the itemtag gross weight');
				} else {
					var purityidname = $("#purityid").find('option:selected').data("purityname");
					var productname = $("#splitproduct").find('option:selected').data("label");
					var counteridname = $("#splitcounterid").find('option:selected').data("countername");
					var sizename = $("#splitsize").find('option:selected').data("sizemastername");
					$("#purityidname").val(purityidname);
					$("#splitproductname").val(productname);
					$("#splitcountername").val(counteridname);
					$("#splitsizename").val(sizename);
					$("#splitpieces").val('1');
					$('#splitdetailaddform').validationEngine('validate');
				}
			} else {
				alertpopup('Please provide the itemtag for spliting');
			}			
		});
		$('#splitdetailaddform').validationEngine({
			onSuccess:function() {
				formtogriddata('splittaginnergrid','ADD','last','');
				$("#splitgrossweight,#splitnetweight,#splitstoneweight,#hiddenstonedata,#hiddenstonedetail,#hiddenstoneentryid").val('');
				cleargriddata('stoneentrygrid');
				$("#itemtagnumber").attr('readonly',true);
				$("#splitgrossweight").focus();
				/* Data row select event */
				datarowselectevt();
				$("#splitpieces").val('1');
			},
			onFailure:function() {
				alertpopup(validationalert);
			}
		});
		//rule criteria condition delete
		$('#splitdetaildelete').click(function() {
			var ruleconrowid = $('#splittaginnergrid div.gridcontent div.active').attr('id');
 			if (ruleconrowid) {
				deletegriddatarow('splittaginnergrid',ruleconrowid);
			} else {
				alertpopup("Please select a row");
			}
			var count = $('#splittaginnergrid .gridcontent div.data-content div').length;
			if(count == 0) {
				$("#itemtagnumber").attr('readonly',false);
			}
		});
		$("#stoneweighticon").on('click keypress', function(e) {
			$("#s2id_stoneid").css('display','inline-block').css('width','90%');
			$('#automaticstonedetails').trigger('click');
			// Do not make manual entries
			$('#tagstoneentryedit').hide();
			$('.stonedetailsform,.stoneiconlisthide').removeClass('hidedisplay');
			$('.tagadditionalchargeclear_stone > div').removeClass('hidedisplay');
			$('.taghide,.totacarat').addClass('hidedisplay');
			stoneweighticon = 1;
			$('#savestoneentrydata').show();
			addslideup('splittagformadd','stonedetailsform');
			$('#s2id_stoneid').select2('focus');
			clearform('stoneoverlay');
			clearform('tagadditionalchargeclear_stone');
			$('#stoneid,#stoneentrycalctypeid').prop('disabled',true);
		});
		$("#automaticstonedetails").on('click keypress', function(e) {
			var stoneid = $('#stoneid').find('option:selected').val();
			var stoneentrycalctypeid = $('#stoneentrycalctypeid').find('option:selected').val();
			if(stoneid > 1 || stoneentrycalctypeid > 1) {
				alertpopupdouble("Automatic stone details in form field. Kindly Submit and check for another stone.!");
				return false;
			} else {
				if($('#tagaddstone').css('display') != 'none') {
					$('#automaticstonedetailoverlay').fadeIn();
					cleargriddata('autostonegrid');
					automaticpurchasestonedetails();
				} else if($('#tageditstone').css('display') != 'none') {
					alertpopupdouble("Stone Details is in Edit Mode. Kindly Update and check again!");
					return false;
				}
			}
		});
		// Items Grid Submit
		$("#autostonegridsubmit").click(function() {
			var stoneentryid = getselectedrowids('autostonegrid');
			if(stoneentryid != '') {
				var checkedlength = $('#autostonegrid .gridcontent div.data-content div input:checked').length;
				if(checkedlength > 1) {
					alertpopupdouble('Please choose the single product details');
					return false;
				} else {
					$('#autostonegrid .gridcontent input:checked').each(function() {
						salesdetailid = $(this).attr('data-rowid');
						$("#stoneid").select2('val',$('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stoneid-class').text());
						$("#stoneentrycalctypeid").select2('val',$('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stoneentrycalctypeid-class').text()).trigger('change');
						$("#purchaserate").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .purchaserate-class').text());
						$("#basicrate").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .basicrate-class').text());
						$("#stonegram").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonegram-class').text());
						$("#stonepieces").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonepieces-class').text());
						$("#caratweight").val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .caratweight-class').text());
						$('#autolotpieces').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonepieces-class').text());
						$('#autolotgramwt').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .stonegram-class').text());
						$('#autolotcaratwt').val($('#autostonegrid .gridcontent div#'+salesdetailid+' ul .caratweight-class').text());
					});
					stoneentrycal();
					Materialize.updateTextFields();
					$('#automaticstonedetailoverlay').fadeOut();
				}
			} else {
				alertpopupdouble('Please select the Items(s)');
			}
		});
		// Return Items grid Close
		$("#autostonegridclose").click(function() {
			$('#automaticstonedetailoverlay').fadeOut();
		});
	}
	$('#stoneentrycalctypeid').change(function() {
		var typeval = $(this).val();
		$('#stonegram,#caratweight,#totalweight,#stonetotalamount').val(0);
		$('#stonegram,#caratweight,#stonepieces').prop('readonly',false);
		$("span","#stonepiecesdiv label").remove();
		$("span","#stonegramdiv label").remove();
		$("span","#caratweightdiv label").remove();
		$('#stonepieces').val(1);
		setTimeout(function() {
			if(typeval == 4) { // gram wise
				$('#stonegram').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],min[0.01]]');
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#caratweight').attr('data-validation-engine','');
				$('#caratweight').removeClass('error');
				$('#caratweight').prop('readonly',true);
				$("span","#caratweightdiv label").remove();
				$('#stonegramdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#caratweightdiv").hide();
				$("#stonegramdiv").show();
			} else if(typeval == 2) {  // carat wise
				$('#caratweight').attr('data-validation-engine','validate[required,decval['+dia_weight_round+'],custom[number],min[0.01]]');
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#stonegram').attr('data-validation-engine','');
				$('#stonegram').removeClass('error');
				$('#stonegram').prop('readonly',true);
				$("span","#stonegramdiv label").remove();
				$('#caratweightdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#stonegramdiv").hide();
				$("#caratweightdiv").show();
			} else if(typeval == 3) {  // piece wise
				$('#caratweight,#stonegram').attr('data-validation-engine','');
				$('#caratweight,#stonegram').removeClass('error');
				$("span","#caratweightdiv label").remove();
				$('#caratweight').prop('readonly',true);
				$('#stonepieces').attr('data-validation-engine','validate[required,custom[onlyWholeNumber],min[1]]');
				$('#stonepiecesdiv label').append('<span class="mandatoryfildclass">*</span>');
				$("#caratweightdiv").hide();
				$("#stonepiecesdiv").show();
				if(stonepiecescalc == '0') {
					$("#stonegramdiv").hide();
					$("span","#stonegramdiv label").remove();
					$('#stonegram').prop('readonly',true);
				} else if(stonepiecescalc == '1') {
					$('#stonegram').attr('data-validation-engine','validate[decval['+round+'],custom[number],min[0]]');
					$('#stonegram').prop('readonly',false);
					$("#stonegramdiv").show();
				}
			}
			Materialize.updateTextFields();
		},100);
	});
	// Stone Cara , Total weight and Total Amount Calculation
	$('#basicrate,#stonepieces,#stonegram').blur(function() {
		stoneentrycal();
	});
	$('#stonepieces').change(function() {
		var lotvalue = $(this).val();
		var autolotpieces = $('#autolotpieces').val();
		if(parseInt(lotvalue) > parseInt(autolotpieces)) {
			alertpopupdouble("Can't able to give value greater than created Stock Entry Pieces!");
			$(this).val(autolotpieces);
			return false;
		}
		stoneentrycal();
	});
	$('#stonegram').change(function() {
		var gramvalue = $(this).val();
		var autolotgramwt = $('#autolotgramwt').val();
		if(parseFloat(gramvalue) > parseFloat(autolotgramwt)) {
			alertpopupdouble("Can't able to give value greater than created Stock Entry Gram Weight!");
			$(this).val(autolotgramwt);
			return false;
		}
		var caratval = gramvalue * 5;
		$('#caratweight').val(parseFloat(caratval).toFixed(round));
		stoneentrycal();
	});
	$('#caratweight').blur(function() {
		var caratvalue = $(this).val();
		var autolotcaratwt = $('#autolotcaratwt').val();
		if(parseFloat(caratvalue) > parseFloat(autolotcaratwt)) {
			alertpopupdouble("Can't able to give value greater than created Stock Entry Carat Weight!");
			$(this).val(autolotcaratwt);
			return false;
		}
		var stoneentrycalctype = $('#stoneentrycalctypeid').find('option:selected').val();
		if(stoneentrycalctype == 3) {   // pieces wise
			$('#stonegram').val('');
		}
		stoneentrycal();
	});
	$('#stoneentrycalctypeid').blur(function() {
		stoneentrycal();
	});
	//Save the Stone-Entry Form
	$("#savestoneentrydata").click(function() {
		if($('#tageditstone').css('display') != 'none') {
			alertpopupdouble("Stone Details is in Edit Mode. Kindly Update and check again!");
			return false;
		} else {
			var netwtcalctype = $("#netwtcalid").val(); // Based on company setting value,we will calculate the val of Net Wt
			var totnetwt = '';
			if(netwtcalctype == 2) {
				totnetwt=(parseFloat($("#totcswt").html()) + parseFloat($("#totpearlwt").html())).toFixed(round);
			} else if(netwtcalctype == 3) {
				totnetwt=(parseFloat($("#totwt").html())).toFixed(round);
			} else if(netwtcalctype == 4) { // G.wt - D
				totnetwt=(parseFloat($("#totdiawt").html())).toFixed(round);
			} else if(netwtcalctype == 5) { // G.wt
				totnetwt=0;
			} else if(netwtcalctype == 6) { // G.wt - C.S => Stone weight = Total stone weight - stone weight [only reduced stone type weight] 
				totnetwt=(parseFloat($("#totwt").html()) - parseFloat($("#totcswt").html())).toFixed(round);
			}
			addslideup('stonedetailsform','splittagformadd');
			var charge_json = {}; //declared as object			
			charge_json = getgridrowsdata("stoneentrygrid");	
			if(charge_json == '') {
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").val(0).trigger('change');
				$('#hiddenstonedata,#hiddenstonedetail').val('');
			} else {
				var stonegridata = JSON.stringify(charge_json);
				stonejsonconvertdata(stonegridata);
				$('#hiddenstonedata').val(JSON.stringify(charge_json));
				$("#stoneweight").val(totnetwt).trigger('change');
				$("#stoneweight").attr('readonly', true);
				$("#stoneweight").trigger('blur');
			}
			clearform('tagadditionalchargeclear_stone');
		}
	});
	//close popup of the Stone Entry
	$(".groupcloseaddformclass").click(function() {
		addslideup('stonedetailsform','splittagformadd');
	});
});
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $("#viewcreatemoduleid").val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
	if(viewname != "dontset")
	{
		setTimeout(function()
		{
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		$('#viewcreateconditiongrid').jqGrid("clearGridData", true);
	}
	else
	{
		$('#dynamicdddataview').trigger('change');
	}
}
function splittaggrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	if(Operation == 1){
		var rowcount = $('#splittagpgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#splittagpgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	}
	var wwidth = $(window).width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#splittagsortcolumn").val();
	var sortord = $("#splittagsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.splittagheadercolsort').hasClass('datasort') ? $('.splittagheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.splittagheadercolsort').hasClass('datasort') ? $('.splittagheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.splittagheadercolsort').hasClass('datasort') ? $('.splittagheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = 48;
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=splittag&primaryid=splittagid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#splittaggrid').empty();
			$('#splittaggrid').append(data.content);
			$('#splittaggridfooter').empty();
			$('#splittaggridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('splittaggrid');
			{//sorting
				$('.splittagheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.splittagheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.splittagheadercolsort').hasClass('datasort') ? $('.splittagheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.splittagheadercolsort').hasClass('datasort') ? $('.splittagheadercolsort.datasort').data('sortorder') : '';
					$("#splittagsortorder").val(sortord);
					$("#splittagsortcolumn").val(sortcol);
					var sortpos = $('#splittaggrid .gridcontent').scrollLeft();
					splittaggrid(page,rowcount);
					$('#splittaggrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('splittagheadercolsort',headcolid,sortord);
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#splittaggrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			{//pagination
				$('.pvpagnumclass').click(function(){
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $('.pagerowcount').data('rowcount');
					stoneaddonsgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#splittagpgrowcount').change(function(){
					Operation = 0;
					$("#processoverlay").show();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					splittaggrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#splittaggrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#splittagpgrowcount').material_select();
		},
	});
}
//View condition Grid
function splittaginnergrid(page,rowcount) {
	var wwidth = $("#splittaginnergrid").width();
	var wheight = $("#splittaginnergrid").height();
	$.ajax({
		url:base_url+"Splittag/splittaginnergridheaderfetch?moduleid=140&width="+wwidth+"&height="+wheight+"&modulename=Split tag inner grid",
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$("#splittaginnergrid").empty();
			$("#splittaginnergrid").append(data.content);
			$("#splittaginnergridfooter").empty();
			$("#splittaginnergridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('splittaginnergrid');
		},
	});
}
{ //crud operations
	//stone addons based submit
	function addform() {
		var formdata = $("#splittagform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		//splittag inner grid
		var criteriacnt = $('#splittaginnergrid .gridcontent div.data-content div').length;
		var griddata = getgridrowsdata('splittaginnergrid');
		var criteriagriddata = JSON.stringify(griddata);
		$("#processoverlay").show();
		$.ajax({
			url:base_url+"Splittag/splittagcreate",
			data: "datas=" + datainformation+amp+"splittaggriddata="+criteriagriddata+amp+"count="+criteriacnt,
			type:'POST',
			async:false,
			cache:false,
			success :function(msg) {
				$("#processoverlay").hide();
				if(msg == 'SUCCESS') {
					alertpopup(savealert);
					addslideup('splittagformadd','splittagcreationview');
					splittaggrid();
				} else {
					setTimeout(function() {
						$("#processoverlay").hide();
					},25);
				}
				//For Keyboard Shortcut Variables
				viewgridview = 1;
				addformview = 0;
			}
		});
    }
}
function loadproductbasedcounteritem(prodid,counterid) { 
	var val = $("#"+prodid+"").val();
	if(checkValue(val) == true){
		var counteriddata = $('#'+prodid+'').find('option:selected').data('counterid');	
		$("#"+counterid+" option").addClass("ddhidedisplay");
		$("#"+counterid+" optgroup").addClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',true);
		if(checkValue(counterid) == true) { 
			var counterids = counteriddata.toString().split(','); 
			for(var mk = 0; mk < counterids.length; mk++) {
				$("#"+counterid+" option[value='"+counterids[mk]+"']").closest('optgroup').removeClass("ddhidedisplay");
				$("#"+counterid+" option[value='"+counterids[mk]+"']").removeClass("ddhidedisplay");	
				$("#"+counterid+" option[value='"+counterids[mk]+"']").prop('disabled',false);
				 if(storagedisplay != '1'){
					var storagevalue ='';
				 	if(storagedisplay == '2'){
						storagevalue = '3';
					}else if(storagedisplay == '3'){
						storagevalue = '2';
					}
				 $("#"+counterid+" option[data-countertypeid='"+storagevalue+"']").addClass("ddhidedisplay");	
				 $("#"+counterid+" option[data-countertypeid='"+storagevalue+"']").prop('disabled',true);
				} 				
				if(counterids.length == 1){
					if(counterusestatus == 'yes'){
						 if(storagequantity == 1){
						 }else{
							$("#"+counterid+"").select2('val',counterids[mk]).trigger('change');
						 }
					}
				} else if(counterids.length > 1){
					if(counterusestatus == 'yes'){
						 if(storagequantity == 1){
						 }else{
						$("#"+counterid+"").select2('val',counterids[0]).trigger('change');
						 }
					}
				} else{
						$("#"+counterid+"").select2('val','');
				}
			}
		}
	} else {
		$("#"+counterid+" option").removeClass("ddhidedisplay");
		$("#"+counterid+" optgroup").removeClass("ddhidedisplay");
		$("#"+counterid+" option").prop('disabled',false);	
	}
	$("#"+counterid+" option:enabled").closest('optgroup').removeClass("ddhidedisplay");	
}
function stonedivhideshow(stoneapplicable) {
	if(stoneapplicable == 'Yes'){  // stone hide/show based on product
		$('#splitstoneweight-div').show();
		$('#splitstoneweight').attr('data-validation-engine','validate[required,decval['+round+'],custom[number],maxSize[15]]');
	}else{
		$('#splitstoneweight').attr('data-validation-engine','');
		$('#splitstoneweight-div').hide();
	}
}
// size hide show based on product
function sizehideshow(productvalue) {
	if(productvalue != 1 || productvalue !='') {
		var sizeapplicable = $("#splitproduct").find('option:selected').data('size');
		if(sizeapplicable == 'Yes') {
			$("#splitsize-div").show();
			$("#splitsize option").addClass("ddhidedisplay");
			$("#splitsize option").prop('disabled',true);
			$("#splitsize option[data-productid="+productvalue+"]").removeClass("ddhidedisplay");
			$("#splitsize option[data-productid="+productvalue+"]").prop('disabled',false);
		} else {
			$("#splitsize-div").hide();
		}
	}
}
// clear the fieldid
function clearfield() {
	$('#grossweight,#stoneweight,#netweight,#itemtagnumber,#splitgrossweight,#splitstoneweight,#splitnetweight,#hidstonecaratweight,#hidstonegram,#hidstonepieces,#allentryids,#checktagstone').val('');
	$('#hiddenstonedata,#hiddenstonedetail,#hiddenstoneentryid').val('');
	$('#product,#purityid,#splitproduct,#splitcounterid').select2('val','');
}
//get the valuu of the selected column value
function getgridcolvalue_split(gridname,rowid,colname,option) {
	var val='0.00';
	var datarowid = '';
	if(option=='sum') {
		if(deviceinfo !='phone') {
			$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
				datarowid = $(e).attr('id');
				val = parseFloat(val) + parseFloat($('#'+gridname+' .gridcontent div#'+datarowid+' ul .'+colname+'-class').text());
			});
		}
		
	} else if(option!='sum' && rowid!='') {
		if(deviceinfo !='phone'){
			val = $('#'+gridname+' .gridcontent div#'+rowid+' ul .'+colname+'-class').text();
		}else{
			val = $('#'+gridname+' .gridcontent div.wrappercontent div#'+rowid+' div.bottom-wrap span.'+colname+'-class').text();
			var newval = val.split(' : ');
			var val = newval[1];
		}
	}
	return parseFloat(val).toFixed(round);
}
//Generate stone icon
function generateiconafterload(fieldid,clickevnid) {
	if($("#hiddenstoneyesno").val() == 1) {
		$("#"+fieldid+"").css('display','inline').css('width','75%');	
		$("#"+fieldid+"").after('<span class="material-icons innerfrmicon stoneweighticoncls" id="'+clickevnid+'" style="position:relative;top:3px;left:5px;cursor:pointer" tabindex="246"><i class="material-icons">group_work</i></span>');
		$('label[for="'+fieldid+'"]').css('width','65%');
	}
}
// Retrieve Live Stone details
// Order Tracking Overlay grid.
function automaticpurchasestonedetails(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#autostonegrid").width();
	var wheight = $("#autostonegrid").height();
	/*col sort*/
	var sortcol = $("#returnitemssortcolumn").val();
	var sortord = $("#returnitemssortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.orderitemheadercolsort').hasClass('datasort') ? $('.orderitemheadercolsort.datasort').data('sortorder') : '';
	}
	var filterid = '1';
	var conditionname = '';
	var filtervalue = '';
	var itemtagid = $('#itemtagid').val();
	var gridstoneids = getgridcolcolumnvalue('stoneentrygrid','','stoneid');
	var count = $('#splittaginnergrid .gridcontent div.data-content div.data-rows').length;
	var stoneid_list = new Array();
	var stonelist_arr = [];
	var checktagstone = $('#checktagstone').val();
	if(count > 0 && checktagstone == 1) {
		var inc = 0;
		$('#splittaginnergrid .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var result = JSON.parse($('#splittaginnergrid .gridcontent div#'+datarowid+' ul .hiddenstonedata-class').text());
			$.each(result, function(index) {
				if(jQuery.inArray(result[index].stoneid, stoneid_list) == -1) {
					stoneid_list.push(result[index].stoneid);
				}
				stonelist_arr[inc] = {
					stoneid: result[index].stoneid,
					stonepieces: result[index].stonepieces,
					caratweight: result[index].caratweight,
					stonegram: result[index].stonegram,
					totalcaratweight: result[index].totalcaratweight,
					totalweight: result[index].totalweight,
					stonetotalamount: result[index].stonetotalamount
				};
				inc++;
			});
		});
	}
	$.ajax({
		url:base_url+"Splittag/autostonegridheaderinformationfetch?maintabinfo=itemtag&primaryid="+itemtagid+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=50&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue+"&gridstoneids="+gridstoneids+"&stoneid_list="+stoneid_list+"&stonelist_arr="+JSON.stringify(stonelist_arr)+'&checktagstone='+checktagstone,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		type: "POST",
		async:false,
		cache:false,
		success:function(data) {
			$("#autostonegrid").empty();
			$("#autostonegrid").append(data.content);
			$("#autostonegridfooter").empty();
			$("#autostonegridfooter").append(data.footer);
			/* data row select event */
			datarowselectevt();
			/* column resize */
			columnresize('autostonegrid');
			var hideprodgridcol = ['stoneid','stonetypeid','stoneentrycalctypeid'];
			gridfieldhide('autostonegrid',hideprodgridcol);
		},
	});
}
function checkallstonedatacomplete() {
	var returnvalue = 'FAIL';
	var itemtagid = $('#itemtagid').val();
	var gridstoneids = getgridcolcolumnvalue('stoneentrygrid','','stoneid');
	var count = $('#splittaginnergrid .gridcontent div.data-content div.data-rows').length;
	var stoneid_list = new Array();
	var stonelist_arr = [];
	var checktagstone = $('#checktagstone').val();
	if(count > 0 && checktagstone == 1) {
		var inc = 0;
		$('#splittaginnergrid .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var stonecheck = $('#splittaginnergrid .gridcontent div#'+datarowid+' ul .hiddenstonedata-class').text();
			if(stonecheck != '') {
				var result = JSON.parse($('#splittaginnergrid .gridcontent div#'+datarowid+' ul .hiddenstonedata-class').text());
				$.each(result, function(index) {
					if(jQuery.inArray(result[index].stoneid, stoneid_list) == -1) {
						stoneid_list.push(result[index].stoneid);
					}
					stonelist_arr[inc] = {
						stoneid: result[index].stoneid,
						stonepieces: result[index].stonepieces,
						caratweight: result[index].caratweight,
						stonegram: result[index].stonegram,
						totalcaratweight: result[index].totalcaratweight,
						totalweight: result[index].totalweight,
						stonetotalamount: result[index].stonetotalamount
					};
					inc++;
				});
			}
		});
	}
	$.ajax({
		url:base_url+"Splittag/checkallstonedatacomplete?primaryid="+itemtagid+"&gridstoneids="+gridstoneids+"&stoneid_list="+stoneid_list+"&stonelist_arr="+JSON.stringify(stonelist_arr)+'&checktagstone='+checktagstone,
		type: "POST",
		async:false,
		cache:false,
		success:function(msg) {
			returnvalue = msg;
		},
	});
	return returnvalue;
}
function stonejsonconvertdata(stonegridata) {
	$.ajax({
        url: base_url +"Splittag/stonejsonconvertdata",
        data: "datas=" + stonegridata,
		type: "POST",
		success: function(msg) {
			$('#hiddenstonedetail').val(msg);
		},
    });
}