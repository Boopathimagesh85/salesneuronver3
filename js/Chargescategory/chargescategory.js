$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Main div height width change
		maindivwidth();
	}	
	{//Grid Calling
		
		setTimeout(function() {
			chargescategoryaddgrid();
		}, 100);
		firstfieldfocus();
		chargescategorycrudactionenable();
	}
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			chargescategoryaddgrid();
		});
	}
	//hidedisplay
	$('#chargescategorydataupdatesubbtn').hide();
	$('#chargescategorysavebutton').show();
	{
	//keyboard shortcut reset global variable
		viewgridview=0;
	}
	{
		//validation for Company Add  
		$('#chargescategorysavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				// For Touch
				masterfortouch = 0;	
				$("#chargescategoryformaddwizard").validationEngine('validate');
			}
		});
		jQuery("#chargescategoryformaddwizard").validationEngine( {	
			onSuccess: function() {
				$('#chargescategorysavebutton').attr('disabled','disabled');
				$('.singlecheckid').trigger('click');
				addchargeaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});	
		$("#chargescategoryreloadicon").click(function(){
			clearform('cleardataform');
			chargescategoryrefreshgrid();
			$('#chargescategorydataupdatesubbtn').hide();
			$('#chargescategorysavebutton').show();
		});
		$( window ).resize(function() {
			innergridresizeheightset('chargescategoryaddgrid');
			sectionpanelheight('chargescategorysectionoverlay');
		});
	}
	{
		//update information
		$('#chargescategorydataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#chargescategoryformeditwizard").validationEngine('validate');
			}
		});
		jQuery("#chargescategoryformeditwizard").validationEngine({
			onSuccess: function() {
				addchargeupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}	
	{//filter work
		//for toggle
		$('#chargescategoryviewtoggle').click(function() {
			if ($(".chargescategoryfilterslide").is(":visible")) {
				$('div.chargescategoryfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.chargescategoryfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#chargescategoryclosefiltertoggle').click(function(){
			$('div.chargescategoryfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#chargescategoryfilterddcondvaluedivhid").hide();
		$("#chargescategoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#chargescategoryfiltercondvalue").focusout(function(){
				var value = $("#chargescategoryfiltercondvalue").val();
				$("#chargescategoryfinalfiltercondvalue").val(value);
				$("#chargescategoryfilterfinalviewconid").val(value);
			});
			$("#chargescategoryfilterddcondvalue").change(function(){
				var value = $("#chargescategoryfilterddcondvalue").val();
				if( $('#s2id_chargescategoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					chargescategorymainfiltervalue=[];
					$('#chargescategoryfilterddcondvalue option:selected').each(function(){
					    var $cvalue =$(this).attr('data-ddid');
					    chargescategorymainfiltervalue.push($cvalue);
						$("#chargescategoryfinalfiltercondvalue").val(chargescategorymainfiltervalue);
					});
					$("#chargescategoryfilterfinalviewconid").val(value);
				} else {
					$("#chargescategoryfinalfiltercondvalue").val(value);
					$("#chargescategoryfilterfinalviewconid").val(value);
				}
			});
		}
		chargescategoryfiltername = [];
		$("#chargescategoryfilteraddcondsubbtn").click(function() {
			$("#chargescategoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#chargescategoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(chargescategoryaddgrid,'chargescategory');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#chargescategorysectionoverlay").removeClass("effectbox");
		$("#chargescategorysectionoverlay").addClass("closed");
	});
}
//Documents Add Grid
function chargescategoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#chargescategoryaddgrid").width();
	var wheight = $("#chargescategoryaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#chargescategorysortcolumn").val();
	var sortord = $("#chargescategorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.chargescategoryheadercolsort').hasClass('datasort') ? $('.chargescategoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.chargescategoryheadercolsort').hasClass('datasort') ? $('.chargescategoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.chargescategoryheadercolsort').hasClass('datasort') ? $('.chargescategoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#chargescategoryviewfieldids').val();
	if(userviewid != '') {
		userviewid= '71';
	}
	var filterid = $("#chargescategoryfilterid").val();
	var conditionname = $("#chargescategoryconditionname").val();
	var filtervalue = $("#chargescategoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=additionalchargecategory&primaryid=additionalchargecategoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#chargescategoryaddgrid').empty();
			$('#chargescategoryaddgrid').append(data.content);
			$('#chargescategoryaddgridfooter').empty();
			$('#chargescategoryaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('chargescategoryaddgrid');
			{//sorting
				$('.chargescategoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.chargescategoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#chargescategorypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.chargescategoryheadercolsort').hasClass('datasort') ? $('.chargescategoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.chargescategoryheadercolsort').hasClass('datasort') ? $('.chargescategoryheadercolsort.datasort').data('sortorder') : '';
					$("#chargescategorysortorder").val(sortord);
					$("#chargescategorysortcolumn").val(sortcol);
					var sortpos = $('#chargescategoryaddgrid .gridcontent').scrollLeft();
					chargescategoryaddgrid(page,rowcount);
					$('#chargescategoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('chargescategoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					chargescategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#chargescategorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					chargescategoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$(".gridcontent").click(function(){
					var datarowid = $('#chargescategoryaddgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#chargescategorypgrowcount').material_select();
			}
		},
	});	
}
function chargescategorycrudactionenable() {
	{//add icon click
		$('#chargescategoryaddicon').click(function(e){
			sectionpanelheight('chargescategorysectionoverlay');
			$("#chargescategorysectionoverlay").removeClass("closed");
			$("#chargescategorysectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#chargescategorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#chargescategorysavebutton').show();
		});
	}
	$("#chargescategoryediticon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargescategoryaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			addchargegetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			// For Touch
			masterfortouch = 1;
			//Keyboard short cut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#chargescategorydeleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#chargescategoryaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {		
			combainedmoduledeletealert('chargescategorydeleteyes');
			$("#chargescategorydeleteyes").click(function(){
				var datarowid = $('#chargescategoryaddgrid div.gridcontent div.active').attr('id');
				addchargerecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function chargescategoryrefreshgrid() {
		var page = $('ul#chargescategorypgnum li.active').data('pagenum');
		var rowcount = $('ul#chargescategorypgnumcnt li .page-text .active').data('rowcount');
		chargescategoryaddgrid(page,rowcount);
	}
}
//new data add submit function
function addchargeaddformdata() {
    var formdata = $("#chargescategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Chargescategory/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				var def = $("#setdefault").val();
				if(def == '1') {
					$('.singlecheckid').trigger('click');
				}
				$(".addsectionclose").trigger("click");
				resetFields();
				chargescategoryrefreshgrid();
				$("#chargescategorysavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}

//old information show in form
function addchargegetformdata(datarowid) {
	var additionalchargeelementsname = $('#chargescategoryelementsname').val();
	var additionalchargeelementstable = $('#chargescategoryelementstable').val();
	var additionalchargeelementscolmn = $('#chargescategoryelementscolmn').val();
	var additionalchargeelementpartable = $('#chargescategoryelementspartabname').val();
	$.ajax( {
		url:base_url+"Chargescategory/fetchformdataeditdetails?additionalchargeprimarydataid="+datarowid+"&additionalchargeelementsname="+additionalchargeelementsname+"&additionalchargeelementstable="+additionalchargeelementstable+"&additionalchargeelementscolmn="+additionalchargeelementscolmn+"&additionalchargeelementpartable="+additionalchargeelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				clearinlinesrchandrgrid('chargescategoryaddgrid');
			} else {
				sectionpanelheight('chargescategorysectionoverlay');
				$("#chargescategorysectionoverlay").removeClass("closed");
				$("#chargescategorysectionoverlay").addClass("effectbox");
				var txtboxname = additionalchargeelementsname + ',chargescategoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = additionalchargeelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				var id=data['primarydataid'];
				$("#chargescategoryprimarydataid").val(id);
				var description=data['description'];
				$("#description").text(description);
				var checkarray=data['setdefault'];
				if(checkarray == '1')
				{
					$('.singleuncheckid').trigger('click');
				} 
			}
			
		}
	});
	$('#chargescategorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#chargescategorysavebutton').hide();
	
}
//update old information
function addchargeupdateformdata() {
	var formdata = $("#chargescategoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Chargescategory/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
				var def = $("#setdefault").val();
				if(def == '1') {
					$('.singlecheckid').trigger('click');
				}
				$('#chargescategorydataupdatesubbtn').hide();
				$('#chargescategorysavebutton').show();
				$(".addsectionclose").trigger("click");
				resetFields();
				chargescategoryrefreshgrid();
				alertpopup(savealert);
				// For Touch
				masterfortouch = 0;
				//Keyboard short cut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function addchargerecorddelete(datarowid) {
	var additionalchargeelementstable = $('#chargescategoryelementstable').val();
	var additionalchargeelementspartable = $('#chargescategoryelementspartabname').val();
    $.ajax({
        url: base_url + "Chargescategory/deleteinformationdata?additionalchargeprimarydataid="+datarowid+"&additionalchargeelementstable="+additionalchargeelementstable+"&additionalchargeelementspartable="+additionalchargeelementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	chargescategoryrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	chargescategoryrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//Charges category name check
function chargescategorynamecheck() {
	var primaryid = $("#chargescategoryprimarydataid").val();
	var accname = $("#additionalchargecategoryname").val();
	var elementpartable = $('#chargescategoryelementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Charges category name already exists!";
				}
			}else {
				return "Charges category name already exists!";
			}
		} 
	}
}