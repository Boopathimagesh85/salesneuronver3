$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}	
	{//Grid Calling
		batchaddgrid();
		firstfieldfocus();
		crudactionenable();
	}
	{//keyboard shortcut reset global variable
		 viewgridview = 0;
		 innergridview = 1;
	}
	//hidedisplay
	$('#batchdataupdatesubbtn').hide();
	$('#batchsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			batchaddgrid();
		});
	}	
	{	//reload concept
		 $( window ).resize(function() {
			maingridresizeheightset('batchaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
		//validation for  Add  
		$('#batchsavebutton').click(function() {
			masterfortouch = 0;
			$("#batchformaddwizard").validationEngine('validate');
		});
		jQuery("#batchformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#batchsavebutton').attr('disabled','disabled');				
				batchaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update company information
		$('#batchdataupdatesubbtn').click(function() {
			$("#batchformeditwizard").validationEngine('validate');
			$(".mmvalidationnewclass .formError").addClass("errorwidth");
		});
		jQuery("#batchformeditwizard").validationEngine({
			onSuccess: function() {
				$('#batchdataupdatesubbtn').attr('disabled','disabled');
				batchupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	var $dd = $('#productid');
	if($dd.find('option').length <= 1){
	   alertpopup('Please Select the Has Batch option on create or update the product. Has batch Enabled Products only applicable for the Batch Module');
	}
	//Date Range
	{//filter work
		//for toggle
		$('#batchviewtoggle').click(function() {
			if ($(".batchfilterslide").is(":visible")) {
				$('div.batchfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.batchfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#batchclosefiltertoggle').click(function(){
			$('div.batchfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#batchfilterddcondvaluedivhid").hide();
		$("#batchfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#batchfiltercondvalue").focusout(function(){
				var value = $("#batchfiltercondvalue").val();
				$("#batchfinalfiltercondvalue").val(value);
				$("#batchfilterfinalviewconid").val(value);
			});
			$("#batchfilterddcondvalue").change(function(){
				var value = $("#batchfilterddcondvalue").val();
				$("#batchfinalfiltercondvalue").val(value);
				if( $('#s2id_batchfilterddcondvalue').hasClass('select2-container-multi') ) {
					batchmainfiltervalue=[];
					$('#batchfilterddcondvalue option:selected').each(function(){
					    var $value =$(this).attr('data-ddid');
					    batchmainfiltervalue.push($value);
						$("#batchfinalfiltercondvalue").val(batchmainfiltervalue);
					});
					$("#batchfilterfinalviewconid").val(value);
				} else {
					$("#batchfinalfiltercondvalue").val(value);
					$("#batchfilterfinalviewconid").val(value);
				}
			});
		}
		batchfiltername = [];
		$("#batchfilteraddcondsubbtn").click(function() {
			$("#batchfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#batchfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(batchaddgrid,'batch');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}

//Documents Add Grid
function batchaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#batchaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#batchsortcolumn").val();
	var sortord = $("#batchsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.batchheadercolsort').hasClass('datasort') ? $('.batchheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.batchheadercolsort').hasClass('datasort') ? $('.batchheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.batchheadercolsort').hasClass('datasort') ? $('.batchheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#batchviewfieldids').val();
	var filterid = $("#batchfilterid").val();
	var conditionname = $("#batchconditionname").val();
	var filtervalue = $("#batchfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=batch&primaryid=batchid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#batchaddgrid').empty();
			$('#batchaddgrid').append(data.content);
			$('#batchaddgridfooter').empty();
			$('#batchaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('batchaddgrid');
			maingridresizeheightset('batchaddgrid');
			{//sorting
				$('.batchheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.batchheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#batchpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.batchheadercolsort').hasClass('datasort') ? $('.batchheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.batchheadercolsort').hasClass('datasort') ? $('.batchheadercolsort.datasort').data('sortorder') : '';
					$("#batchsortorder").val(sortord);
					$("#batchsortcolumn").val(sortcol);
					var sortpos = $('#batchaddgrid .gridcontent').scrollLeft();
					batchaddgrid(page,rowcount);
					$('#batchaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('batchheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					batchaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#batchpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					batchaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#batchaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#batchpgrowcount').material_select();
		},
	});	
}
function crudactionenable() {
	{//add icon click
		$('#addicon').click(function(e){
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#batchdataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#batchsavebutton').show();
		});
	}
	//edit icon		
	$("#editicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#batchaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			$('#batchimagedisplay').empty();
			batchgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	//delete icon
	$("#deleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#batchaddgrid div.gridcontent div.active').attr('id');
		if(datarowid) {
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');			
			combainedmoduledeletealert('batchdeleteyes');
				$("#batchdeleteyes").click(function(){
					var datarowid = $('#batchaddgrid div.gridcontent div.active').attr('id');
					batchrecorddelete(datarowid);
					$(this).unbind();
				});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#batchpgnum li.active').data('pagenum');
		var rowcount = $('ul#batchpgnumcnt li .page-text .active').data('rowcount');
		batchaddgrid(page,rowcount);
	}
}
//new data add submit function
function batchaddformdata() {
    var formdata = $("#batchaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Batch/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$(".addsectionclose").trigger("click");
				$("#batchsavebutton").attr('disabled',false);
				$('#batchimagedisplay').empty();
				alertpopup(savealert);
				refreshgrid();					
			} 
        },
    });
}
//old information show in form
function batchgetformdata(datarowid) {
	var batchelementsname = $('#batchelementsname').val();
	var batchelementstable = $('#batchelementstable').val();
	var batchelementscolmn = $('#batchelementscolmn').val();
	var batchelementpartable = $('#batchelementspartabname').val();
	$.ajax({
		url:base_url+"Batch/fetchformdataeditdetails?batchprimarydataid="+datarowid+"&batchelementsname="+batchelementsname+"&batchelementstable="+batchelementstable+"&batchelementscolmn="+batchelementscolmn+"&batchelementpartable="+batchelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				sectionpanelheight('groupsectionoverlay');
				$("#groupsectionoverlay").removeClass("closed");
				$("#groupsectionoverlay").addClass("effectbox");
				var txtboxname = batchelementsname + ',batchprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = batchelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
			}		
		}
	});
	$('#batchdataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#batchsavebutton').hide();
}
//udate old information
function batchupdateformdata() {
	var formdata = $("#batchaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Batch/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#batchdataupdatesubbtn').hide();
				$('#batchsavebutton').show();
				$(".addsectionclose").trigger("click");
				resetFields();	
				$("#batchdataupdatesubbtn").attr('disabled',false);
				refreshgrid();
				$('#batchimagedisplay').empty();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["batchcloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//update old information
function batchrecorddelete(datarowid) {
	var batchelementstable = $('#batchelementstable').val();
	var elementspartable = $('#batchelementspartabname').val();
    $.ajax({
        url: base_url + "Batch/deleteinformationdata?batchprimarydataid="+datarowid+"&batchelementstable="+batchelementstable+"&batchparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);           
			refreshgrid();
			if (nmsg == 'TRUE')	{
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
			} else if (nmsg == "Denied")  {
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
			}		
        },
    });
}
//batchnamecheck
function batchnamecheck() {
	var primaryid = $("#batchprimarydataid").val();
	var accname = $("#batchname").val();
	var elementpartable = $('#batchelementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Batch Name already exists!";
				}
			} else {
				return "Batch Name already exists!";
			}
		} 
	}
}