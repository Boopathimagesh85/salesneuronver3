$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		territoryaddgrid();
		crudactionenable();
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	{//enable support icon
		$(".supportoverlay").css("display","inline-block");
	}
	$('#groupcloseaddform').hide();
	//hidedisplay
	$('#territorydataupdatesubbtn').hide();
	$('#territorysavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			territoryaddgrid();
		});
	}
	{//toolbasr actions
		 $("#reloadicon").click(function() {
			clearform('cleardataform');
			refreshgrid();			
			$('#territoryimagedisplay').empty();
			$('#territorydataupdatesubbtn').hide();
			$('#territorysavebutton').show();
			$("#deleteicon").show();
		});
		 $( window ).resize(function() {
			maingridresizeheightset('territoryaddgrid');
			sectionpanelheight('groupsectionoverlay');
		});
		//validation for  Add  
		$('#territorysavebutton').click(function(e) {
			$('territoryname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				masterfortouch = 0;
				$("#territoryformaddwizard").validationEngine('validate');
			}
		});
		$("#territoryformaddwizard").validationEngine({
			onSuccess: function() {
				$('#territorysavebutton').attr('disabled','disabled');
				territoryaddformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//update company information
		$('#territorydataupdatesubbtn').click(function(e) {
			$('territoryname').mouseout();
			if(e.which == 1 || e.which === undefined) {
				$("#territoryformeditwizard").validationEngine('validate');
				$(".mmvalidationnewclass .formError").addClass("errorwidth");
			}
		});
		jQuery("#territoryformeditwizard").validationEngine({
			onSuccess: function() {
				$('#territorydataupdatesubbtn').attr('disabled','disabled');
				territoryupdateformdata();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}	
	$('#territoryname').mouseout(function(){
		var primaryid = $("#territoryprimarydataid").val();
		var accname = $("#territoryname").val();
		var elementpartable = $('#territoryelementspartabname').val();
		if( accname !="" ) {
			$.ajax({
				url:base_url+"Base/uniquedynamicviewnamecheck",
				data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
				type:"post",
				async:false,
				cache:false,
				success :function(msg) {
					nmsg = $.trim(msg);
				},
			});
			if(nmsg != "False") {
				if(primaryid != ""){
					if(primaryid != nmsg) {
						alertpopup("Territory name already exists!");
						$("#territoryname").val("");
					}
				}else {
					alertpopup("Territory name already exists!");
					$("#territoryname").val("");
				}
			} 
		}
	});		
	{//filter work
		//for toggle
		$('#territoriesviewtoggle').click(function() {
			if ($(".territoryfilterslide").is(":visible")) {
				$('div.territoryfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.territoryfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#territoryclosefiltertoggle').click(function(){
			$('div.territoryfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#territoryfilterddcondvaluedivhid").hide();
		$("#territoryfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#territoryfiltercondvalue").focusout(function(){
				var value = $("#territoryfiltercondvalue").val();
				$("#territoryfinalfiltercondvalue").val(value);
			});
			$("#territoryfilterddcondvalue").change(function(){
				var value = $("#territoryfilterddcondvalue").val();
				if( $('#s2id_territoryfilterddcondvalue').hasClass('select2-container-multi') ) {
					territorymainfiltervalue=[];
					$('#territoryfilterddcondvalue option:selected').each(function(){
					    var $talue =$(this).attr('data-ddid');
					    territorymainfiltervalue.push($cvalue);
						$("#territoryfinalfiltercondvalue").val(territorymainfiltervalue);
					});
					$("#territoryfilterfinalviewconid").val(value);
				} else {
					$("#territoryfinalfiltercondvalue").val(value);
					$("#territoryfilterfinalviewconid").val(value);
				}
			});
		}
		territoryfiltername = [];
		$("#territoryfilteraddcondsubbtn").click(function() {
			$("#territoryfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#territoryfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(territoryaddgrid,'territory');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
	function showhideintextbox(hideid,showid){
		$("#"+hideid+"divhid").hide();
		$("#"+showid+"divhid").show();
	}
}
//Documents Add Grid
function territoryaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#territoryaddgrid').width();
	var wheight = $(window).height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#territorysortcolumn").val();
	var sortord = $("#territorysortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.territoryheadercolsort').hasClass('datasort') ? $('.territoryheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.territoryheadercolsort').hasClass('datasort') ? $('.territoryheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.territoryheadercolsort').hasClass('datasort') ? $('.territoryheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#territoryviewfieldids').val();
	userviewid = userviewid == '' ? '77' : userviewid;
	var filterid = $("#territoryfilterid").val();
	var conditionname = $("#territoryconditionname").val();
	var filtervalue = $("#territoryfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=territory&primaryid=territoryid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#territoryaddgrid').empty();
			$('#territoryaddgrid').append(data.content);
			$('#territoryaddgridfooter').empty();
			$('#territoryaddgridfooter').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			//column resize
			columnresize('territoryaddgrid');
			maingridresizeheightset('territoryaddgrid');
			{//sorting
				$('.territoryheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.territoryheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#territorypgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.territoryheadercolsort').hasClass('datasort') ? $('.territoryheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.territoryheadercolsort').hasClass('datasort') ? $('.territoryheadercolsort.datasort').data('sortorder') : '';
					$("#territorysortorder").val(sortord);
					$("#territorysortcolumn").val(sortcol);
					var sortpos = $('#territoryaddgrid .gridcontent').scrollLeft();
					territoryaddgrid(page,rowcount);
					$('#territoryaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('territoryheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					territoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#territorypgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					territoryaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#territoryaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#territorypgrowcount').material_select();
		},
	});	
}
function crudactionenable() {
	{//add icon click
		$('#addicon').click(function(e) {
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();			
			Materialize.updateTextFields();
			firstfieldfocus();
			$('#territorydataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#territorysavebutton').show();
		});
	}
	$("#editicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#territoryaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();			
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');			
			$('#territoryimagedisplay').empty();
			sectionpanelheight('groupsectionoverlay');
			$("#groupsectionoverlay").removeClass("closed");
			$("#groupsectionoverlay").addClass("effectbox");
			territorygetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#deleteicon").click(function(e){
		e.preventDefault();
		var datarowid = $('#territoryaddgrid div.gridcontent div.active').attr('id');			
		if(datarowid){
			clearform('cleardataform');
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');			
			combainedmoduledeletealert('territorydeleteyes');
			$("#territorydeleteyes").click(function(){
				var datarowid = $('#territoryaddgrid div.gridcontent div.active').attr('id');
				territoryrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#territorypgnum li.active').data('pagenum');
		var rowcount = $('ul#territorypgnumcnt li .page-text .active').data('rowcount');
		territoryaddgrid(page,rowcount);
	}
}
//new data add submit function
function territoryaddformdata() {
    var formdata = $("#territoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax(  {
        url: base_url + "Territory/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				resetFields();
				$("#territorysavebutton").attr('disabled',false);
				$('#territoryimagedisplay').empty();
				$(".addsectionclose").trigger("click");            	
				alertpopup(savealert);		
				refreshgrid();
					
            }
        },
    });
}
//old information show in form
function territorygetformdata(datarowid) {
	var territoryelementsname = $('#territoryelementsname').val();
	var territoryelementstable = $('#territoryelementstable').val();
	var territoryelementscolmn = $('#territoryelementscolmn').val();
	var territoryelementpartable = $('#territoryelementspartabname').val();
	$.ajax( {
		url:base_url+"Territory/fetchformdataeditdetails?territoryprimarydataid="+datarowid+"&territoryelementsname="+territoryelementsname+"&territoryelementstable="+territoryelementstable+"&territoryelementscolmn="+territoryelementscolmn+"&territoryelementpartable="+territoryelementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$("#deleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				refreshgrid();
			} else {
				var txtboxname = territoryelementsname + ',territoryprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = territoryelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				$("#territoryfileuploadfromid").select2("val","");
				var image = data['territoryimage'];
				if(image !="") {
					var id = data['fileuploadfromid'];
					if(id == 2){
						$('#territoryimagedisplay').empty();
						var img = $('<img id="territoryimagedynamic" style="height:100%;width:100%">');
						img.attr('src', base_url+image);
						img.appendTo('#territoryimagedisplay');
						$('#territoryimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#territoryfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#territoryimagedisplay').empty();
							$('#territoryimage').val('');
						});
					} else{
						$('#territoryimagedisplay').empty();
						var img = $('<img id="territoryimagedynamic" style="height:100%;width:100%">');
						img.attr('src',image);
						img.appendTo('#territoryimagedisplay');
						$('#territoryimagedisplay').append('<i class="fa fa-times documentslogodownloadclsbtn"></i>');
						$("#territoryfileuploadfromid").select2("val","");
						$(".documentslogodownloadclsbtn").click(function(){
							$(this).remove();
							$('#territoryimagedisplay').empty();
							$('#territoryimage').val('');
						});
					}
				} else{
					$('#territoryimagedisplay').empty();
					$("#territoryfileuploadfromid").select2("val","");
				}
			}
			
		}
	});
	$('#territorydataupdatesubbtn').show().removeClass('hidedisplayfwg');
	$('#territorysavebutton').hide();
}
//update old information
function territoryupdateformdata() {
	var formdata = $("#territoryaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Territory/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
				$('#territorydataupdatesubbtn').hide();
				$('#territorysavebutton').show();
				resetFields();
				$(".addsectionclose").trigger("click");
				$("#deleteicon").show();
				$("#territorydataupdatesubbtn").attr('disabled',false);
				refreshgrid();
				$('#territoryimagedisplay').empty();
				alertpopup(savealert);
				//for touch
				masterfortouch = 0;				
            }
        },
    });
	setTimeout(function() {
		var closetrigger = ["territorycloseadd"];
		triggerclose(closetrigger);
	}, 1000);
}
//udate old information
function territoryrecorddelete(datarowid) {
	var territoryelementstable = $('#territoryelementstable').val();
	var elementspartable = $('#territoryelementspartabname').val();
    $.ajax({
        url: base_url + "Territory/deleteinformationdata?territoryprimarydataid="+datarowid+"&territoryelementstable="+territoryelementstable+"&territoryparenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            }  else if (nmsg == "false")  {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
