$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		pricebookdetailaddgrid();
		firstfieldfocus();
		pricebookdetailcrudactionenable();
	}
	//hidedisplay
	$('#pricebookdetaildataupdatesubbtn').hide();
	$('#pricebookdetailsavebutton').show();
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			pricebookdetailaddgrid();
		});
	}
	{//keyboard shortcut reset global variable
		viewgridview=0;
	}
	{//validation for Company Add  
		$('#pricebookdetailsavebutton').click(function(e) {
			if(e.which == 1 || e.which === undefined) {	
				$("#pricebookdetailformaddwizard").validationEngine('validate');
				masterfortouch = 0;
			}	
		});
		jQuery("#pricebookdetailformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#pricebookdetailsavebutton').attr('disabled','disabled');
				pricebookdetailsaddformdata(); 
			},
			onFailure: function() {
				var dropdownid =['2','pricebookid','productid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$( window ).resize(function() {
			innergridresizeheightset('pricebookdetailaddgrid');
		});
	}
	{//update company information
		$('#pricebookdetaildataupdatesubbtn').click(function(e) {
			if(e.which == 1 || e.which === undefined) {
				$("#pricebookdetailformeditwizard").validationEngine('validate');
			}
		});
		jQuery("#pricebookdetailformeditwizard").validationEngine({
			onSuccess: function() {
				pricebookdetailsupdateformdata();
			},
			onFailure: function() {
				var dropdownid =['2','pricebookid','productid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	$("#productid").change(function(){
		var pid = $("#productid").val();
		productvaluefetch(pid);
	});	
	{//filter work
		//for toggle
		$('#pricebookdetailviewtoggle').click(function() {
			if ($(".pricebookdetailfilterslide").is(":visible")) {
				$('div.pricebookdetailfilterslide').addClass("filterview-moveleft");
			}else{
				$('div.pricebookdetailfilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#pricebookdetailclosefiltertoggle').click(function(){
			$('div.pricebookdetailfilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#pricebookdetailfilterddcondvaluedivhid").hide();
		$("#pricebookdetailfilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#pricebookdetailfiltercondvalue").focusout(function(){
				var value = $("#pricebookdetailfiltercondvalue").val();
				$("#pricebookdetailfinalfiltercondvalue").val(value);
				$("#pricebookdetailfilterfinalviewconid").val(value);
			});
			$("#pricebookdetailfilterddcondvalue").change(function(){
				var value = $("#pricebookdetailfilterddcondvalue").val();
				pricebookdetailmainfiltervalue=[];
				$('#pricebookdetailfilterddcondvalue option:selected').each(function(){
				    var $pvalue =$(this).attr('data-ddid');
				    pricebookdetailmainfiltervalue.push($pvalue);
					$("#pricebookdetailfinalfiltercondvalue").val(pricebookdetailmainfiltervalue);
				});
				$("#pricebookdetailfilterfinalviewconid").val(value);
			});
		}
		pricebookdetailfiltername = [];
		$("#pricebookdetailfilteraddcondsubbtn").click(function() {
			$("#pricebookdetailfilterformconditionvalidation").validationEngine("validate");	
		});
		$("#pricebookdetailfilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(pricebookdetailaddgrid,'pricebookdetail');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}	
}
//price book details Add Grid
function pricebookdetailaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#pricebookdetailaddgrid").width();
	var wheight = $("#pricebookdetailaddgrid").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#pricebookdetailsortcolumn").val();
	var sortord = $("#pricebookdetailsortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.pricebookdetailheadercolsort').hasClass('datasort') ? $('.pricebookdetailheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.pricebookdetailheadercolsort').hasClass('datasort') ? $('.pricebookdetailheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.pricebookdetailheadercolsort').hasClass('datasort') ? $('.pricebookdetailheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#pricebookdetailviewfieldids').val();
	if(userviewid != '') {
		userviewid= '63';
	}
	var filterid = $("#pricebookdetailfilterid").val();
	var conditionname = $("#pricebookdetailconditionname").val();
	var filtervalue = $("#pricebookdetailfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=pricebookdetail&primaryid=pricebookdetailid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#pricebookdetailaddgrid').empty();
			$('#pricebookdetailaddgrid').append(data.content);
			$('#pricebookdetailaddgridfooter').empty();
			$('#pricebookdetailaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('pricebookdetailaddgrid');
			{//sorting
				$('.pricebookdetailheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.pricebookdetailheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#pricebookdetailpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.pricebookdetailheadercolsort').hasClass('datasort') ? $('.pricebookdetailheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.pricebookdetailheadercolsort').hasClass('datasort') ? $('.pricebookdetailheadercolsort.datasort').data('sortorder') : '';
					$("#pricebookdetailsortorder").val(sortord);
					$("#pricebookdetailsortcolumn").val(sortcol);
					var sortpos = $('#pricebookdetailaddgrid .gridcontent').scrollLeft();
					pricebookdetailaddgrid(page,rowcount);
					$('#pricebookdetailaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('pricebookdetailheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					pricebookdetailaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#pricebookdetailpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					pricebookdetailaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#pricebookdetailaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#pricebookdetailpgrowcount').material_select();
		},
	});
}
function pricebookdetailcrudactionenable() {
	{//add icon click
		$('#pricebookdetailaddicon').click(function(e){
			$("#pricebookdetailsectionoverlay").removeClass("closed");
			$("#pricebookdetailsectionoverlay").addClass("effectbox");
			e.preventDefault();
			resetFields();			
			$('#pricebookdetaildataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#pricebookdetailsavebutton').show();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
	}
	$("#pricebookdetaildeleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#pricebookdetailaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){		
			clearform('cleardataform');
			$("#pricebookdetailprimarydataid").val(datarowid);
			$(".addbtnclass").removeClass('hidedisplay');
			$(".updatebtnclass").addClass('hidedisplay');
			combainedmoduledeletealert('pricebookdetailsdelete');
			$("#pricebookdetailsdelete").click(function(){
				var datarowid = $("#pricebookdetailprimarydataid").val();
				pricebookdetailsrecorddelete(datarowid);
				$(this).unbind();
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#pricebookdetailediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#pricebookdetailaddgrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			$('#pricebookdetaildataupdatesubbtn').show().removeClass('hidedisplayfwg');
			$('#pricebookdetailsavebutton').hide();
			pricebookdetailsgetformdata(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
			//Keyboard shortcut
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function pricebookdetailrefreshgrid() {
		var page = $('ul#pricebookdetailpgnum li.active').data('pagenum');
		var rowcount = $('ul#pricebookdetailpgnumcnt li .page-text .active').data('rowcount');
		pricebookdetailaddgrid(page,rowcount);
	}
}
//new data add submit function
function pricebookdetailsaddformdata() {
    var formdata = $("#pricebookdetailaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax( {
        url: base_url + "Pricebookdetail/newdatacreate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	$(".addsectionclose").trigger("click");
				resetFields();
				pricebookdetailrefreshgrid();
				$("#pricebookdetailsavebutton").attr('disabled',false); 
				alertpopup(savealert);
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#pricebookdetailsectionoverlay").removeClass("effectbox");
		$("#pricebookdetailsectionoverlay").addClass("closed");
	});
}
//old information show in form
function pricebookdetailsgetformdata(datarowid) {
	var pricebookdetailelementsname = $('#pricebookdetailelementsname').val();
	var pricebookdetailelementstable = $('#pricebookdetailelementstable').val();
	var pricebookdetailelementscolmn = $('#pricebookdetailelementscolmn').val();
	var pricebookdetailelementspartabname = $('#pricebookdetailelementspartabname').val();
	$.ajax( {
		url:base_url+"Pricebookdetail/fetchformdataeditdetails?pricebookdetailprimarydataid="+datarowid+"&pricebookdetailelementsname="+pricebookdetailelementsname+"&pricebookdetailelementstable="+pricebookdetailelementstable+"&pricebookdetailelementscolmn="+pricebookdetailelementscolmn+"&pricebookdetailelementspartabname="+pricebookdetailelementspartabname, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				$("#pricebookdetaildeleteicon").show();
				alertpopup('Permission denied');
				resetFields();
				pricebookdetailrefreshgrid();
			} else {
				$("#pricebookdetailsectionoverlay").removeClass("closed");
				$("#pricebookdetailsectionoverlay").addClass("effectbox");
				var txtboxname = pricebookdetailelementsname + ',pricebookdetailprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = pricebookdetailelementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				$("#productid").trigger('change');
			}
			
		}
	});
}
//update old information
function pricebookdetailsupdateformdata() {
	var formdata = $("#pricebookdetailaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
    $.ajax({
        url: base_url + "Pricebookdetail/datainformationupdate",
        data: "datas=" + datainformation,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {
				$('#pricebookdetaildataupdatesubbtn').hide();
				$('#pricebookdetailsavebutton').show();
				$("#pricebookdetaildeleteicon").show();
				$(".addsectionclose").trigger("click");
				resetFields();
				pricebookdetailrefreshgrid();
				alertpopup(savealert);
				//For Touch work  change to add state
				masterfortouch = 0;
				//Keyboard short cut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function pricebookdetailsrecorddelete(datarowid) {
	var pricebookdetailelementstable = $('#pricebookdetailelementstable').val();
	var pricebookdetailelementspartabname = $('#pricebookdetailelementspartabname').val();
    $.ajax({
        url: base_url + "Pricebookdetail/deleteinformationdata?pricebookdetailprimarydataid="+datarowid+"&pricebookdetailelementstable="+pricebookdetailelementstable+"&pricebookdetailelementspartabname="+pricebookdetailelementspartabname,
		cache:false,
        success: function(msg)  {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	pricebookdetailrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "Denied")  {
            	pricebookdetailrefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
function productvaluefetch(pid) {
	$.ajax({
		url:base_url +"Pricebookdetail/productdetialget?pid="+pid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			var des = data.des;
			var unit = data.unit;
			$("#unitprice").val(unit);
			$("#description").text(des);
			Materialize.updateTextFields();
        },
	});
}