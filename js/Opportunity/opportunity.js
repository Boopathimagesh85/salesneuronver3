$(document).ready(function() {	
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{// Grid Calling
		opportunitycreategrid();
		//crud action
		crudactionenable();
	}
	{
		if(softwareindustryid == 2){
			$('#accountiddivhid').addClass('hidedisplay');
			$('#contactiddivhid').addClass('hidedisplay');
			$("#opportunitytypeid").change(function() {
				var opptypeid = $(this).val();
				if(opptypeid == 2){
					$('#accountiddivhid').addClass('hidedisplay');
					$("#accountid").removeClass('validate[required]');
					$("#contactid").addClass('validate[required]');	
					$('#contactiddivhid').removeClass('hidedisplay');
				}
				if(opptypeid == 3){
					$('#contactiddivhid').addClass('hidedisplay');
					$("#contactid").removeClass('validate[required]');
					$("#accountid").addClass('validate[required]');
					$('#accountiddivhid').removeClass('hidedisplay');
				}
			});
		}	
	}
	{
		var editorname = $('#editornameinfo').val();
		froalaarray=[editorname,'html.set',''];
	}
	{//account name based contact name
		if(softwareindustryid != 2){
			$("#accountid").change(function(){
				var accid = $("#accountid").val();
				if(accid){
					contactnamefetch(accid);
				}else{
					contactnamefetch(0);
				}				
			});
		}		
	}
	{// Clear the form - regenerate the number 
		$('#formclearicon').click(function(){
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");			
		});
	}	
	{// Close Add Screen
		var addcloseoppocreation =["closeaddform","opportunitycreationview","opportunitycreationformadd"]
		addclose(addcloseoppocreation);	
	}
	{// Close Add Screen
		var addcloseviewcreation =["viewcloseformiconid","opportunitycreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{// For touch
		fortabtouch = 0;
	}
	{// Drop Down Change Function stonetype editstonetype 
		$(".dropdownchange").change(function() {
			var dataattrname = ['dataidhidden'];
			var dropdownid =['employeeidddid'];
			var textboxvalue = ['employeetypeid'];
			var selectid = $(this).attr('id');
			var index = dropdownid.indexOf(selectid);
			var selected=$('#'+selectid+'').find('option:selected');
			var datavalue=selected.data(''+dataattrname[index]+'');
			$('#'+textboxvalue[index]+'').val(datavalue);
			if(selectid == 'employeeidddid') {
				var selected=$('#'+selectid+'').find('option:selected');
				var datavalue=selected.data('ddid');
				$('#employeeid').val(datavalue);
			}
		});
	}
	{// Tool bar action icons
		$("#cloneicon").click(function() {
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				opportunityclonedatafetchfun(datarowid);
				showhideiconsfun('editclick','opportunitycreationformadd');	
				//for autonumber
				randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
				$("#primarydataid").val('');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				Operation = 0; //for pagination
			} else { 
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#reloadicon").click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('opportunitycreategrid');
		});
		$("#detailedviewicon").click(function() {
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				$("#processoverlay").show();
				froalaset(froalaarray);
				opportunityeditdatafetchfun(datarowid);
				showhideiconsfun('summryclick','opportunitycreationformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
			} else {
				alertpopup("Please select a row");
			}
			Materialize.updateTextFields();
		});
		$("#editfromsummayicon").click(function() {
			showhideiconsfun('editfromsummryclick','opportunitycreationformadd');
		});
		//send mail
		//mail validate-
		$("#mailicon").click(function(){
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			var accountid=getaccountid(datarowid,'opportunity','accountid');
			if (datarowid) {
				var validemail=validateemail(accountid,'account','emailid');
				if(validemail == true){
					var emailtosend = getvalidemailid(accountid,'account','emailid','','');
					sessionStorage.setItem("forcomposemailid",emailtosend.emailid);
					sessionStorage.setItem("forsubject",'');
					sessionStorage.setItem("moduleid",'204');
					sessionStorage.setItem("recordid",datarowid);
					var fullurl = 'erpmail/?_task=mail&_action=compose';
					window.open(''+base_url+''+fullurl+'');
				}
				else{
					alertpopup("Invalid email address");
				}
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//Expected amount
		$("#probability").focusout(function() {
			var amount = $("#amount").val();
			var probability = $("#probability").val();
			if(amount != ''){
				var expectedamount = parseFloat(amount)*parseFloat(probability/100);
				$("#expectedamount").val(expectedamount);
			} else {
				$("#amount").val(amount);
				
			}
		});
	}
	{// View by drop down change
		$('#dynamicdddataview').change(function() {
			opportunitycreategrid();
		});
	}
	{// Validation for Opportunity Add  
		$('#dataaddsbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formaddwizard").validationEngine('validate');
		});
		jQuery("#formaddwizard").validationEngine({
			onSuccess: function(){
				$("#processoverlay").show();
				opportunitynewdataaddfun();
			},
			onFailure: function(){
				var dropdownid =['2','accountid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{// Update Opportunity information
		$('#dataupdatesubbtn').click(function() {
			$('.ftab').trigger('click');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function(){
				$("#processoverlay").show();
				opportunitydataupdateinformationfun();
			},
			onFailure: function(){
				var dropdownid =['4','accountid','crmstatusid'];
				dropdownfailureerror(dropdownid);
				
				alertpopup(validationalert);
			}	
		});
	}
	{// Redirected form Home		
		add_fromredirect("opportunityaddsrc",204);
	}
	//Opportunity-report session check
	{// Redirected form Home
		var uniquelreportsession = sessionStorage.getItem("reportunique204");		
		if(uniquelreportsession != '' && uniquelreportsession != null) {	
			setTimeout(function() {
				//check the valid
				sessionStorage.removeItem("reportunique204"); 
			},50);	
		} 
	}
	{//print icon
		$('#printicon').click(function() {
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#printpdfid').val(datarowid);
				$("#templatepdfoverlay").fadeIn();
				var viewfieldids = $("#viewfieldids").val();
				pdfpreviewtemplateload(viewfieldids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//import icon
		$('#importicon').click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("importmoduleid",viewfieldids);
			window.location = base_url+'Dataimport';
		});
		//data manipulation icon
		$("#datamanipulationicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("datamanipulateid",viewfieldids);
			window.location = base_url+'Massupdatedelete';
		});
		//Customize icon
		$("#customizeicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("customizeid",viewfieldids);
			window.location = base_url+'Formeditor';
		});
		//Find Duplication icon
		$("#findduplicatesicon").click(function() {
			var viewfieldids = $("#viewfieldids").val();
			sessionStorage.setItem("finddubid",viewfieldids);
			window.location = base_url+'Findduplicates';
		});
	}
	{// Dashboard Icon Click Event
		$('#dashboardicon').click(function(){
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {		
				$("#processoverlay").show();
				$.getScript(base_url+"js/plugins/uploadfile/jquery.uploadfile.min.js");
				$.getScript(base_url+"js/plugins/zingchart/zingchart.min.js");
				$.getScript('js/Home/home.js',function(){
					dynamicchartwidgetcreate(datarowid);
				});
				$("#dashboardcontainer").removeClass('hidedisplay');
				$('.actionmenucontainer').addClass('hidedisplay');
				$('.fullgridview').addClass('hidedisplay');
				$('.footercontainer').addClass('hidedisplay');
				$(".forgetinggridname").addClass('hidedisplay');
				$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#clicktocallclose").click(function() {
		$("#clicktocallovrelay").hide();
	});	
	$("#mobilenumberoverlayclose").click(function() {
		$("#mobilenumbershow").hide();
	});	
	$("#smsicon").click(function() {
		var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++) {
							if(data[i] != '') {
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#mobilenumbershow").show();
							$("#smsmoduledivhid").hide();
							$("#smsrecordid").val(datarowid);
							$("#smsmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'smsmobilenumid');
						} else if(mobilenumber.length == 1) {
							sessionStorage.setItem("mobilenumber",mobilenumber);
							sessionStorage.setItem("viewfieldids",viewfieldids);
							sessionStorage.setItem("recordis",datarowid);
							window.location = base_url+'Sms';
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#mobilenumbershow").show();
						$("#smsmoduledivhid").show();
						$("#smsrecordid").val(datarowid);
						$("#smsmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'smsmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});	
	$("#smsmodule").change(function() {
		var moduleid =	$("#smsmoduleid").val(); //main module
		var linkmoduleid =	$("#smsmodule").val(); // link module
		var recordid = $("#smsrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'smsmobilenumid');
		}
	});
	$("#callmodule").change(function() {
		var moduleid =	$("#callmoduleid").val(); //main module
		var linkmoduleid =	$("#callmodule").val(); // link module
		var recordid = $("#callrecordid").val();
		if(linkmoduleid != '' || linkmoduleid != null) {
			mobilenumberinformationfetch(moduleid,linkmoduleid,recordid,'callmobilenum');
		}
	});
	$("#smsmobilenumid").change(function() {
		var data = $('#smsmobilenumid').select2('data');
		var finalResult = [];
		for( item in $('#smsmobilenumid').select2('data')) {
			finalResult.push(data[item].id);
		};
		var selectid = finalResult.join(',');
		$("#smsmobilenum").val(selectid);
	});
	$("#mobilenumbersubmit").click(function() {
		var datarowid = $("#smsrecordid").val();
		var viewfieldids =$("#smsmoduleid").val();
		var mobilenumber =$("#smsmobilenum").val();
		if(mobilenumber != '' || mobilenumber != null) {
			sessionStorage.setItem("mobilenumber",mobilenumber);
			sessionStorage.setItem("viewfieldids",viewfieldids);
			sessionStorage.setItem("recordis",datarowid);
			window.location = base_url+'Sms';
		} else {
			alertpopup('Please Select the mobile number...');
		}	
	});
	$("#outgoingcallicon").click(function() {
		var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
		if (datarowid) {
			var viewfieldids = $("#viewfieldids").val();
			$.ajax({
				url:base_url+"Base/mobilenumberinformationfetch?viewid="+viewfieldids+"&recordid="+datarowid,
				dataType:'json',
				async:false,
				cache:false,
				success :function(data) {
					mobilenumber = [];
					if(data != 'No mobile number') {
						for(var i=0;i<data.length;i++){
							if(data[i] != ''){
								mobilenumber.push(data[i]);
							}
						}
						if(mobilenumber.length > 1) {
							$("#c2cmobileoverlay").show();
							$("#callmoduledivhid").hide();
							$("#calcount").val(mobilenumber.length);
							$("#callrecordid").val(datarowid);
							$("#callmoduleid").val(viewfieldids);
							mobilenumberload(mobilenumber,'callmobilenum');
						} else if(mobilenumber.length == 1){
							clicktocallfunction(mobilenumber);
						} else {
							alertpopup("Invalid mobile number");
						}
					} else {
						$("#c2cmobileoverlay").show();
						$("#callmoduledivhid").show();
						$("#callrecordid").val(datarowid);
						$("#callmoduleid").val(viewfieldids);
						moduledropdownload(viewfieldids,datarowid,'callmodule');
					}
				},
			});
		} else {
			alertpopup("Please select a row");
		}
	});
	$("#c2cmobileoverlayclose").click(function() {
		$("#c2cmobileoverlay").hide();
	});
	$("#callnumbersubmit").click(function()  {
		var mobilenum = $("#callmobilenum").val();
		if(mobilenum == '' || mobilenum == null) {
			alertpopup("Please Select the mobile number to call");
		} else {
			clicktocallfunction(mobilenum);
		}
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,opportunitycreategrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			opportunitycreategrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
});
{// View create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','ViewCreationName','ViewCreationId','viewcreation','ViewCreationModuleId',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function opportunitycreategrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#opportunitypgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#opportunitypgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#opportunitycreategrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.opportunityheadercolsort').hasClass('datasort') ? $('.opportunityheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.opportunityheadercolsort').hasClass('datasort') ? $('.opportunityheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.opportunityheadercolsort').hasClass('datasort') ? $('.opportunityheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=opportunity&primaryid=opportunityid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#opportunitycreategrid').empty();
				$('#opportunitycreategrid').append(data.content);
				$('#opportunitycreategridfooter').empty();
				$('#opportunitycreategridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('opportunitycreategrid');
				{//sorting
					$('.opportunityheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.opportunityheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul#opportunityspgnumcnt li .page-text .active').data('rowcount');
						var sortcol = $('.opportunityheadercolsort').hasClass('datasort') ? $('.opportunityheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.opportunityheadercolsort').hasClass('datasort') ? $('.opportunityheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#opportunitycreategrid .gridcontent').scrollLeft();
						opportunitycreategrid(page,rowcount);
						$('#opportunitycreategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('opportunityheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						opportunitycreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#opportunitypgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;//$('#prev').data('pagenum');
						opportunitycreategrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				{// Redirected form Home
					var opportunityideditvalue = sessionStorage.getItem("opportunityidforedit"); 
					if(opportunityideditvalue != null){
						setTimeout(function(){ 
						opportunityeditdatafetchfun(opportunityideditvalue);
						sessionStorage.removeItem("opportunityidforedit");
						},100);	
					}
				}
				//Material select
				$('#opportunitypgrowcount').material_select();
			},
		});
	}
}
{//crud operation
	function crudactionenable() {
		$("#addicon").click(function(e) {
			e.preventDefault();
			addslideup('opportunitycreationview','opportunitycreationformadd');
			resetFields();
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			showhideiconsfun('addclick','opportunitycreationformadd');	
			//form field first focus
			firstfieldfocus();
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			$("#primarydataid").val('');
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			froalaset(froalaarray);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			fortabtouch = 0;
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
		});
		$("#editicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$("#processoverlay").show();
				froalaset(froalaarray);
				opportunityeditdatafetchfun(datarowid);
				showhideiconsfun('editclick','opportunitycreationformadd');
				//For tablet and mobile Tab section reset
				$('#tabgropdropdown').select2('val','1');
				fortabtouch = 1;
				Operation = 1; //for pagination
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function(e) {
			e.preventDefault();
			var datarowid = $('#opportunitycreategrid div.gridcontent div.active').attr('id');
			if(datarowid){		
				$('#primarydataid').val(datarowid);
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			opportunityrecorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#opportunityspgnum li.active').data('pagenum');
		var rowcount = $('ul#opportunityspgnumcnt li .page-text .active').data('rowcount');
		opportunitycreategrid(page,rowcount);
	}
}
{// New data add submit function
	function opportunitynewdataaddfun() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditoradd(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		$.ajax({
			url: base_url + "Opportunity/newdatacreate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					var dynview = $("#dynamicdddataview").val();
					$('#opportunitytags').select2('val','');	
					$(".ftab").trigger('click');
					resetFields();
					$('#opportunitycreationformadd').hide();
					$('#opportunitycreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					$("#dynamicdddataview").select2('val',dynview);
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Old information show in form
	function editformdatainformationshow(datarowid) {
		var elementsname = $('#elementsname').val();
		var elementstable = $('#elementstable').val();
		var elementscolmn = $('#elementscolmn').val();
		var elementpartable = $('#elementspartabname').val();
		var resctable = $('#resctable').val();
		$.ajax({
			url:base_url+"Opportunity/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,  
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'Denied') {
					alertpopup('Permission denied');
					$(".ftab").trigger('click');
					$('#opportunitycreationformadd').hide();
					$('#opportunitycreationview').fadeIn(1000);
					refreshgrid();
					$("#processoverlay").hide();
				} else {
					addslideup('opportunitycreationview','opportunitycreationformadd');
					var txtboxname = elementsname + ',primarydataid';
					var textboxname = {};
					textboxname = txtboxname.split(',');
					var dropdowns = [];
					textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
					var attachfldname = $('#attachfieldnames').val();
					var attachcolname = $('#attachcolnames').val();
					var attachuitype = $('#attachuitypeids').val();
					var attachfieldid = $('#attachfieldids').val();
					attachedfiledisplay(attachfldname,attachcolname,attachuitype,attachfieldid,data);
					editordatafetch(froalaarray,data);
					$("#processoverlay").hide();
				}
			}
		});
	}
}

{// Update old information
	function opportunitydataupdateinformationfun() {
		var editorname = $('#editornameinfo').val();
		var editordata = froalaeditordataget(editorname);
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		 $.ajax({
			url: base_url + "Opportunity/datainformationupdate",
			data: "datas=" + datainformation,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$(".ftab").trigger('click');
					$("#opportunitytags").select2('val','');
					$('#opportunitycreationformadd').hide();
					$('#opportunitycreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
					$("#processoverlay").hide();
				}
			},
		});
	}
}
{// Delete data information
	function opportunityrecorddelete(datarowid) {
		var elementstable = $('#elementstable').val();
		var elementspartable = $('#elementspartabname').val();
		$.ajax({
			url: base_url + "Opportunity/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup("Deleted successfully");
				} else if (nmsg == "Denied") {
					refreshgrid();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Permission denied');
				}
			},
		});
	}
}
{// Edit Function
	function opportunityeditdatafetchfun(datarowid) {
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields();
		editformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Shortcut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
{// Clone Function
	function opportunityclonedatafetchfun(datarowid) {
		$(".addbtnclass").removeClass('hidedisplay');
		$(".updatebtnclass").addClass('hidedisplay');
		$("#formclearicon").hide(); 
		resetFields();
		editformdatainformationshow(datarowid);
		firstfieldfocus();
		//For Keyboard Short cut Variables
		addformupdate = 1;
		viewgridview = 0;
		addformview = 1;
		Materialize.updateTextFields();
	}
}
//probability chack
function probabilitycheck() {
	var prob = $("#probability").val();
	if(prob > 100){
		return "Value should Less Than 100!";
	}
}
{//contact name fetch
	function contactnamefetch(accid) {
		$('#contactid').empty();
		$('#contactid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Opportunity/fetchcontatdatawithmultiplecond?accid='+accid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#contactid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}	
				$('#contactid').trigger('change');
			},
		});
	}
}
//account unique name check
function opportunitynamecheck() {
	var primaryid = $("#primarydataid").val();
	var accname = $("#opportunityname").val();
	var elementpartable = $('#elementspartabname').val();
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Opportunity name already exists!";
				}
			}else {
				return "Opportunity name already exists!";
			}
		} 
	}
}