<script>
$(document).ready(function() 
{
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Grid Calling Function
		rategrid();
	}
	{//form field first focus
		firstfieldfocus();
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	{ // Overlay form modules - filter display
		$('#viewoverlaytoggle').on('click',function() {
			$('#filterdisplaymainviewnonbase').css({"display":"block"});
		});
		$(document).on( "click", ".nbfilterdisplaymainviewclose", function() {
			$('#filterdisplaymainviewnonbase').css({"display":"none"});
		});
	}
	rate_round = $('#rate_round').val();
	$('#groupcloseaddform').hide();
	$('#rateviewtoggle').hide();
	ratearray = $('#ratearray').val();
	ratearrayjson = {};
	ratearrayjson = ratearray;
	 $( window ).resize(function() {
		  maingridresizeheightset('rategrid');
		 sectionpanelheight('ratesectionoverlay');
		});
	$("#addicon").click(function(){
		resetFields();
		$("#branchid").select2('val','2');
		addslideup('rateview','rateformadd'); 
		rateinnergrid();
		firstfieldfocus();
		Materialize.updateTextFields();
	});
	
	{//Rate events
		//rate entry		
		$('#addrate').click(function() {
			addrate();
		});
		//Reload
		$("#raterefresh").click(function(){
			resetFields();
			$('#metalid,#purityid').select2('val','');
			$('#currentrate').val('');
			refreshgrid();
			Materialize.updateTextFields();
		});
		$("#branchid").change(function() {
			rateinnergrid();
		});
		//metal grouping
		$("#metalid").change(function(){
			$("#purityid").select2('val','');
			$("#purityid optgroup").addClass("ddhidedisplay");
			$("#purityid optgroup option").prop('disabled',true);
			var metallabel = $(this).find('option:selected').data('name');		
			$("#purityid optgroup[label='"+metallabel+"']").removeClass("ddhidedisplay");
			$("#purityid optgroup[label='"+metallabel+"'] option").prop('disabled',false);
			var arraydata = $('#ratearray').val();
			if(arraydata != ''){
				arraydata = arraydata.split(",");
				$("#purityid").select2('val',arraydata).trigger('change');
			}
			rateinnergrid();
		});
		$("#purityid").change(function(){
			var puirtyid=$(this).val();
			if(puirtyid == '' || puirtyid == 'null')
			{
				$("#s2id_purityid").find('ul').addClass('error');
			}else{
				$("#s2id_purityid").find('ul').removeClass('error');
			}
			rateinnergrid();
		});
	}
	{// Close Add Screen
		$("#addsectionclose").click(function()
	{	var addcloseleadcreation =["addsectionclose","rateview","rateformadd",""];
		addclose(addcloseleadcreation);
		cleargriddata('rateinnergrid');
		refreshgrid();
	});
	}
	$('#addsectionclose').click(function(){
		$("#branchid").select2('val','');
		$("#metalid").select2('val','');
		$("#purityid").select2('val','');
	});
	  //filter
	$("#utilityfilterddcondvaluedivhid").hide();
	$("#utilityfilterdatecondvaluedivhid").hide();
	{	//field value set 
		$("#utilityfiltercondvalue").focusout(function(){
			var value = $("#utilityfiltercondvalue").val();
			$("#utilityfinalfiltercondvalue").val(value);
			$("#utilityfilterfinalviewconid").val(value);
		});
		$("#utilityfilterddcondvalue").change(function(){
			var value = $("#utilityfilterddcondvalue").val();
			utilitymainfiltervalue=[];
			$('#utilityfilterddcondvalue option:selected').each(function(){
			    var $mvalue =$(this).attr('data-ddid');
			    utilitymainfiltervalue.push($mvalue);
				$("#utilityfinalfiltercondvalue").val(utilitymainfiltervalue);
			});
			$("#utilityfilterfinalviewconid").val(value);
		});
		$("#utilityfilterdatecondvalue").change(function(){
			var value = $("#utilityfilterdatecondvalue").val();
			$("#utilityfinalfiltercondvalue").val(value);
			$("#utilityfilterfinalviewconid").val(value);
			
		});
	}
	utilityfiltername = [];
	$("#utilityfilteraddcondsubbtn").click(function() {
		$("#utilityfilterformconditionvalidation").validationEngine("validate");	
	});
	$("#utilityfilterformconditionvalidation").validationEngine({
		onSuccess: function() {
			dashboardmodulefilter(rategrid,'utility');
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#resetfilter").click(function() {
		$("#branchid").select2('val','2');
		$("#metalid").select2('val','');
		$("#purityid").select2('val','');
		rateinnergrid();
	});
});
//Delete
$("#deleteicon").click(function(){
	var datarowid = $('#rategrid div.gridcontent div.active').attr('id');
	if(datarowid){	
		$("#basedeleteoverlay").fadeIn();
			$(".ffield").focus();
	} else {
		alertpopup(selectrowalert);
	}
});
$("#basedeleteyes").click(function(){
	ratedelete();
});
// Rate View Grid
function rategrid(page,rowcount) {
	var defrecview = $('#mainviewdefaultview').val();
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? defrecview : rowcount;
	var wwidth = $('#rategrid').width();
	var wheight = $(window).height();
	/*col sort*/
	var sortcol = $("#ratesortcolumn").val();
	var sortord = $("#ratesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').attr('id') : '0';
	var filterid = $("#utilityfilterid").val();
	var conditionname = $("#utilityconditionname").val();
	var filtervalue = $("#utilityfiltervalue").val();
	var userviewid =119;
	var viewfieldids =43;
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=rate&primaryid=rateid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			
			$('#rategrid').empty();
			$('#rategrid').append(data.content);
			$('#rategridfooter').empty();
			$('#rategridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
				columnresize('rategrid');
			{//sorting
				$('.rateheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.rateheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortorder') : '';
					$("#ratesortorder").val(sortord);
					$("#ratesortcolumn").val(sortcol);
					var sortpos = $('#rategrid .gridcontent').scrollLeft();
					rategrid(page,rowcount);
					$('#rategrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('rateheadercolsort',headcolid,sortord);
			}
			{//pagination
					$('.pvpagnumclass').click(function(){
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						rategrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#ratepgrowcount').change(function(){
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = $('#prev').data('pagenum');
						rategrid(page,rowcount);
						$("#processoverlay").hide();
					});
			}
			//Material select
				$('#ratepgrowcount').material_select();
		},
	});
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#ratepgnum li.active').data('pagenum');
		var rowcount = $('ul#ratepgnumcnt li .page-text .active').data('rowcount');
		rategrid(page,rowcount);
	}
}
function rateinnergrid() {
	var chargeid = $("#rateinnergrid").val(); 
	chargeid = ((chargeid!='') ? chargeid : 0);
	
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $("#rateinnergrid").width();
	var wheight = $("#rateinnergrid").height();
	/*col sort*/
	var sortcol = $("#ratesortcolumn").val();
	var sortord = $("#ratesortcolumn").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').attr('id') : '0';
	var branchid = $("#branchid").val();
	var metalid = $("#metalid").val();
	var purityid = $("#purityid").val();
	$.ajax({
		url:base_url+"Utility/gridinformationfetch?maintabinfo=purity&primaryid=purityid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+"&sortcol="+sortcol+"&sortord="+sortord+"&moduleid=48"+"&filter="+chargeid+"&branchid="+branchid+"&metalid="+metalid+"&purityid="+purityid,			
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#rateinnergrid').empty();
			$('#rateinnergrid').append(data.content);
			$('.rateinnergridfootercontainer').empty();
			$('.rateinnergridfootercontainer').append(data.footer);
			/*data row select event*/
			datarowselectevt();
			/*column resize*/
			columnresize('rateinnergrid');
			{//sorting
				$('.rateheadercolsort').click(function(){
					$('.rateheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#rategridpgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.rateheadercolsort').hasClass('datasort') ? $('.rateheadercolsort.datasort').data('sortorder') : '';
					$("#ratesortorder").val(sortord);
					$("#ratesortcolumn").val(sortcol);
					var sortpos = $('#rateinnergrid .gridcontent').scrollLeft();
					rateinnergrid(page,rowcount);
					$('#rateinnergrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
				});
				sortordertypereset('rateheadercolsort',headcolid,sortord);
			}
			var hideprodgridcol = ['innerbranchid','innermetalid','innerprimaryid','innerpurityid'];
			gridfieldhide('rateinnergrid',hideprodgridcol);
			{//pagination
				$('.pagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#rategridpgnumcnt li .page-text .active').data('rowcount');
					rateinnergrid(page,rowcount);
				});
				$('.pagerowcount').click(function(){
					var page = $('ul#rategridpgnumcnt li.active').data('pagenum');
					var rowcount = $(this).data('rowcount');
					rateinnergrid(page,rowcount);
				});
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('ul#rategridpgnumcnt li .page-text .active').data('rowcount');
					rateinnergrid(page,rowcount);
				});
				$('#ratepgrowcount').change(function(){
					var rowcount = $(this).val();
					var page = $('ul#rategridpgnumcnt li.active').data('pagenum');
					rateinnergrid(page,rowcount);
				});
			}
			{ // apply rate
				$('.rateinner__rowtextclass').keypress(function(e) {
					var keyCode = e.which;
					var start = e.target.selectionStart;
					var end = e.target.selectionEnd;
					if($.trim($(this).val().substring(start, end)) === '') {
						// Not allow special 
						if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
							var newValue = $(this).val();
							if(keyCode != 8 && keyCode != 0) {
								if (hasDecimalPlace(newValue, rate_round)) {
									e.preventDefault();
								}
							}
							if(newValue.length > 10) {
								e.preventDefault();
							}
						} else {
							e.preventDefault();
						}
					} else {
						if (((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 0)) {
						} else {
							e.preventDefault();
						}
					}
				});
				// Change Rate accordingly to company settings.
				$('.rowtext').change(function() {
					var baseratevalue = $(this).val();
					var id = $(this).closest('div').attr('id');
					var primaryid = $('#rateinnergrid .gridcontent div#'+id+' ul .innerprimaryid-class').text();
					if(primaryid == '1') {
						var purityid = $('#rateinnergrid .gridcontent div#'+id+' ul .innerpurityid-class').text();
						retrieveratecalculationdetails(purityid,baseratevalue);
					}
				});	
			}
		},
	});
}
{//CRUD Related functions
//Create
function addrate() { 
	var gridData =rategetgridrowsdata('rateinnergrid');
	var rateaddongriddata = JSON.stringify(gridData);
	var amp = '&';
    $.ajax({
        url: base_url +"Utility/addrate",
        data: "rateaddongriddata="+rateaddongriddata+amp,
		type: "POST",
        success: function(msg) {
            $("#processoverlay").show();
			var nmsg =  $.trim(msg);
            clearform('addrateform');
            refreshgrid();
            $('#rateformadd').hide();
			$('#rateview').fadeIn(1000);
            $("#processoverlay").hide();
			if (nmsg == 'SUCCESS'){
				alertpopup(savealert);
				location.reload();
            } else {
				alertpopup(submiterror);
			}
			
        },
    });
}
function rategetgridrowsdata(gridname) {
	if(deviceinfo != 'phone'){
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption ul li').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.data-content div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			$(fieldname).each(function(i,e) {
				value='';
				if(e == 'rate') {
					value = $('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul li #rateinner_rowtext'+datarowid+'').val();
					if(value == undefined) {
						value = 0;
					}
				} else {
					value = $('#'+gridname+' .gridcontent div.data-content div#'+datarowid+' ul .'+fieldclass[i]).text();
				}
				value = (value !== 'null') ? value : '0';
				datas[''+e+''] = new Array();	
				datas[''+e+''] = value;
			});
			dataarr.push(datas);
			j++;
		});
		return dataarr;
	}else{
		var txtboxid = '';
		var uitype = '';
		var txtboxname = '';
		var txtclass = '';
		var value = '';
		var fieldname = new Array();
		var fieldclass = new Array();
		$('#'+gridname+' div.header-caption span').map(function(i,e) {
		   txtboxid = $(e).attr('id');
		   uitype = $('#'+txtboxid).data('uitype');
		   txtboxname = $('#'+txtboxid).data('fieldname');
		   txtclass = $('#'+txtboxid).data('class');
		   value = '';
		   if( uitype != '17' && uitype != '18' && uitype != '19' && uitype != '20' && uitype != '23' && uitype != '25' && uitype != '26' && uitype != '27' && uitype != '28' && uitype != '29') {
			   fieldname.push(txtboxname);
			   fieldclass.push(txtclass);
		   } else {
			   fieldname.push(txtboxname+'name');
			   fieldclass.push(txtclass);
		   }
		});
		var j=0;
		var dataarr = new Array();
		$('#'+gridname+' .gridcontent div.wrappercontent div').map(function(i,e) {
			datarowid = $(e).attr('id');
			var datas = {};
			$(fieldname).each(function(i,e) {
				value='';
				value = $('#'+gridname+' .gridcontent div#'+datarowid+' .'+fieldclass[i]).text();
				value = (value !== 'null') ? value : '';
				datas[''+e+''] = new Array();			
				datas[''+e+''] = value;
			});
			dataarr.push(datas);
			j++;
		});		
		return dataarr;
	}	
}
//delete rate information
function ratedelete(datarowid)
{	
	var datarowid= $('#rategrid div.gridcontent div.active').attr('id');
	setTimeout(function(){
		$('#processoverlay').show();
	},25);
    $.ajax({
        url: base_url + "Utility/ratedelete",
        data:{primaryid:datarowid},
		success: function(msg) 
        {
			var nmsg =  $.trim(msg);
			 refreshgrid();
			$("#basedeleteoverlay").fadeOut();
		    if (nmsg == 'SUCCESS') {	
					setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(deletealert);
            } else {
					setTimeout(function(){
					$('#processoverlay').hide();
				},25);
				alertpopup(submiterror);
            }
        },
    });
}
function hasDecimalPlace(value, x) { // restrit decimal value based on weight round from company settings
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}
function retrieveratecalculationdetails(purityid,baseratevalue) {
	$('#processoverlay').show();
    $.ajax({
        url: base_url + "Utility/retrieveratecalculationdetails",
        data:{purityid:purityid},
		dataType:'json',
		async:false,
		success: function(data) {
			if(data['fail'] == 'FAILED') {
				$('#processoverlay').hide();
			} else {
				$.each(data, function(index) {
					var applyrateinpurityid = 0;
					var applypurityid = data[index]['nonprimarypurityid'];
					var ratecalculationid = data[index]['ratecalculationid'];
					var ratepercentvalue = data[index]['ratepercentvalue'];
					if(ratecalculationid == 2) {
						applyrateinpurityid = ((parseFloat(ratepercentvalue)/100) * parseFloat(baseratevalue)).toFixed(rate_round);
					} else if(ratecalculationid == 3) {
						applyrateinpurityid = ((parseFloat(ratepercentvalue)/100) * parseFloat(baseratevalue)).toFixed(rate_round);
						applyrateinpurityid = ((parseFloat(baseratevalue)) - parseFloat(applyrateinpurityid)).toFixed(rate_round);
					}
					$('#rateinnergrid .gridcontent div.data-content div').map(function(i,e) {
						datarowid = $(e).attr('id');
						var innerpurityid = $('#rateinnergrid .gridcontent div#'+datarowid+' ul .innerpurityid-class').text();
						if(applypurityid == innerpurityid) {
							$('#rateinnergrid .gridcontent div#'+datarowid+' ul .innercurrentrate-class .rowtext').val(applyrateinpurityid);
						}
					});
				});
				$('#processoverlay').hide();
			}
        },
    });
}
}
</script>