$(document).ready(function() {
	$('body').fadeIn(1500);
	$(document).foundation();
	maindivwidth();
	{
		printtemplatesaddgrid();
		printtemplatesfoladdgrid1();
		//crud action
		crudactionenable();
	}
	{
		var addcloseprinttemplatescreation =['closeaddform','printtemplatescreationview','printtemplatescreationformadd'];
		addclose(addcloseprinttemplatescreation);
	}
	{
		$('#dynamicdddataview').change(function(){
			refreshgrid();
		});
	}
	editstatus=0;
	/* formula_array=[];
	formula_array=[{
			name: 'Formula Apply',
			username: 'F='
		}, {
			username: '|RS-'
		}, {
			name: 'Row Count',
			username: '|RC-'
		},
		{
			name: 'Row Value',
			username: '|RG-'
		},
		{
			name: 'Upwards Sum',
			username: '|DS-'
		},
		{
			name: 'Upwards Value',
			username: '|DG-'
		},
		{
			name: 'Upwards Count',
			username: '|DC-'
		}
	]; */
	iconafterload('fieldname','formatterevent');
	//hide pdf/print in add form
	$('#pdfheadericonspan').hide();
	$('#pdfheadericonspan').hide();
	$('#printtemplatesfolingridreloadspan1').show();
	$('#printtemplatesfolingrideditspan1').show();
	$('#printtemplatesfoladdbutton').show();
	$('#printtemplatesfolupdatebutton').hide();
	$('.pdfheadericonspan').removeClass('hidedisplay');
	{//For touch
		fortabtouch = 0;
	}
	{//inner-form-with-grid
		$("#printtemplatesfolingridadd1").click(function(){
			sectionpanelheight('printtemplatesfoloverlay');
			$("#printtemplatesfoloverlay").removeClass("closed");
			$("#printtemplatesfoloverlay").addClass("effectbox");
			resetFields();
			$("#foldersetdefaultcboxid").prop('checked',false);
			$("#foldersetdefault").val('No');
			$('#printtemplatesfoladdbutton').show();
			$('#printtemplatesfolupdatebutton').hide();
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$(".addsectionclose").click(function(){
			$("#printtemplatesfoloverlay").removeClass("effectbox");
			$("#printtemplatesfoloverlay").addClass("closed");
		});
	}
	{//action events
		$('#reloadicon').click(function() {
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('printtemplatesaddgrid');
			sectionpanelheight('printtemplatesfoloverlay');
		});
		$('#formclearicon').click(function() {
			var froalaarray=['printtemplatestemplatedesign_editor','html.set',''];
			froalaset(froalaarray);
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
		});
		$('#printpreviewicon').click(function(){
			var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				addslideup('printtemplatescreationview','printpreviewform');
				$.ajax({
					url:base_url+'Printtemplates/templatecontentpreview',
					data:'datas=&templateid='+datarowid,
					type:'POST',
					cache:false,
					success: function(content) {
						var regExp = /a10s/g;
						var datacontent = content.match(regExp);
						if(datacontent !== null){	
							for (var i = 0; i < datacontent.length; i++) {
								content = content.replace(''+datacontent[i]+'','&nbsp;');
							}
						}
						$('#templatepreviewframe').attr('srcdoc',content);
					},
				});
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#previewclose').click(function(){
			addslideup('printpreviewform','printtemplatescreationview');
		});
		//Detailed View
		$("#detailedviewicon").click(function(){
			var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				//Function Call For Edit
				$("#processoverlay").show();
				printtemplatesdataupdateinfofetchfun(datarowid);
				showhideiconsfun('summryclick','printtemplatescreationformadd');
				//For tablet and mobile Tab section reset and enable tabgroup dd
				$('#tabgropdropdown').select2('val','1');
				 setTimeout(function() {
					 $('#tabgropdropdown').select2('enable');
				 },50);
				 $(".editformsummmarybtnclass").removeClass('hidedisplay');
				 $(".addbtnclass").addClass('hidedisplay');
					$(".updatebtnclass").addClass('hidedisplay');
					$("#formclearicon").hide();	
					$(".pdfheadericonspan").hide();
					$("#processoverlay").hide();
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	$("#editfromsummayicon").click(function() {
		showhideiconsfun('editfromsummryclick','printtemplatescreationformadd');
		$(".froala-element").css('pointer-events','auto');
		$(".addbtnclass").addClass('hidedisplay');
		$(".updatebtnclass").removeClass('hidedisplay');
		$("#formclearicon").hide();
		$(".editformsummmarybtnclass").hide();		
	});
	{
	    //Pdf Icon
		$('#pdfheadericonbtn').click(function(){
			$("#templatepdfoverlay").fadeIn();
		});
		//print icon
		$('#printheadericonbtn').click(function(){
			$("#templateprintoverlay").fadeIn();
		});
		$('#pdfclose,#pdfpreviewcancel').click(function(){
			$("#templatepdfoverlay").fadeOut();
		});
		//print close icon
		$('#printclose,#printpreviewcancel').click(function(){
			$("#templateprintoverlay").fadeOut();
		});
		//folder add form refersh
		$('#printtemplatesfolingridreload1').click(function(){
			clearform('gridformclear');
			folderrefreshgrid();
			$("#printtemplatesfolingriddel1,#printtemplatesfoladdbutton").show();
			$("#printtemplatesfolupdatebutton").hide()
		});
		//add new folder
		$('#dataaddsbtn').click(function(){
			$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]] error');
			$('#printtemplatesformaddwizard').validationEngine('validate');
		});
		$('#printtemplatesformaddwizard').validationEngine({
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				printtemplatesnewdataaddfun();
			},
			onFailure: function() {
				var dropdownid =['2','moduleid','foldernameid'];
				dropdownfailureerror(dropdownid);
				$("#s2id_moduleid").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
		$('#dataupdatesubbtn').click(function(e){
			$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]] error');
			$('#printtemplatesformeditwizard').validationEngine('validate');
		});
		$('#printtemplatesformeditwizard').validationEngine({
			onSuccess: function() {
				$('#foldernamename').removeClass('validate[required,funcCall[foldernamecheck]]');
				$('#dataupdatesubbtn').attr('disabled','disabled');
				printtemplatesdataupdatefun();
			},
			onFailure: function() {
				$('#foldernamename').removeClass('validate[required,funcCall[foldernamecheck]]');
				var dropdownid =['2','moduleid'];
				dropdownfailureerror(dropdownid);
				$("#s2id_moduleid").find('ul').addClass('error');
				alertpopup(validationalert);
			}
		});
	}
	{
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			} else {
				$('#'+name+'').val('No');
			}
		});
	}
	//module change events
	$('#mergemoduleid').change(function(){
		var moduleid = $(this).val();
		$('#fieldname').select2('val','').trigger('change');
		if(moduleid!="") {
			loadmodulefield(moduleid);
			loadwherecalusefield(moduleid);
		}
		if(moduleid == 52){  // transaction module
			$('#salestransactiontypeiddivhid').removeClass('hidedisplay');
			$('#salestransactiontypeid').addClass('validate[required]');
		}else{
			$('#salestransactiontypeiddivhid').addClass('hidedisplay');
			$('#salestransactiontypeid').removeClass('validate[required]');
		}
	});
	//module fields change events
	$('#fieldname,#whereclauseid,#total,#mergeid,#orderby,#textlength,#seconditeration,#tablekeyvalueid,#numberstoword').change(function() {
		if($(this).val() != "" ) {
			var parmodid = $('#moduleid').val();
			var fildmodid = $('#mergemoduleid').val();
			var fildmodtype = $('#mergemoduleid').find('option:selected').data('mergemoduletype');
			if(parmodid != '' && fildmodid != '') {
				var colname = $('#fieldname').find('option:selected').data('colname');
				var id = $('#fieldname').find('option:selected').data('id');
				var whereclaues = $.trim($('#whereclauseid').val());
				var orderby = $.trim($('#orderby').val());
				var total = $.trim($('#total').val());
				var numtowords = $.trim($('#numberstoword').val());
				var seconditeration = $.trim($('#seconditeration').val());
				var mergeid = $.trim($('#mergeid').val());
				var textlength = $.trim($('#textlength').val());
				var mtext = [];
				if(seconditeration == 'no') {
					$('#whereclauseiddivhid').show();
					$('#tablekeyvalueiddivhid').hide();
					mtext+='{';
				} else {
					$('#whereclauseiddivhid').hide();
					$('#tablekeyvalueiddivhid').show();
					mtext+='zyx';
				}
				if(id != '') {
					mtext+=id+'.'+colname;
				}
				if(whereclaues != '') {
					mtext+='|w:'+whereclaues;
				}
				if(seconditeration == 'yes') {
					var key = $('#tablekeyvalueid').find('option:selected').data('tablekeyvalueidhidden');
					mtext+='|k:'+key+'|v:undefined';
				}
				if(orderby != 'none') {
					mtext+='|O:'+orderby;
				}
				if(total == 'yes') {
					mtext+='|T:y';
				}
				if(numtowords == 'yes') {
					mtext+='|words';
				}
				if(textlength != '' && textlength != '0') { 
					mtext+='|C:'+textlength;
				}
				if(mergeid != '') { 
					mtext+='|CL:'+mergeid;
				}
				if(seconditeration == 'no') {
					mtext+='}';
				} else {
					mtext+='zyx';
				}
				$('#mergetext').val(mtext);
			}
		} else {
			$('#mergetext').val('');
		}
		Materialize.updateTextFields();
	});
	{ //currency formatter opn
		$('#formatterevent').click(function(){
			var id = $('#mergemoduleid').val();
			if(id!="") {
				$('#moneyconvertoverlay').fadeIn();
				currencyworddropdownmodulevalset('amttowordfieldname','formfieldsidhidden','modfiledcolumn','modfiledtable','fieldlabel','modulefieldid','columnname','tablename','modulefield','moduletabid,uitypeid',''+id+',5|7');
			}
		});
		//formater fields mapping
		$('#saveformatorword').click(function(){
			var ctype = $('#curencyformatype').val();
			var field = $('#amttowordfieldname').val();
			if( field!= "" && ctype!="") {
				var parmodid = $('#moduleid').val();
				var fildmodid = $('#mergemoduleid').val();
				if(parmodid!='' && fildmodid!='') {
					var relelem = ( (parmodid!=fildmodid)? 'REL:':'' );
					var table = $('#amttowordfieldname').find('option:selected').data('modfiledtable');
					var tabcolumn = $('#amttowordfieldname').find('option:selected').data('modfiledcolumn');
					var mtext = '{'+relelem+table+'.'+ctype+'-'+tabcolumn+'}';
					var oldmtext = $('#mergetext').val();
					var ftext = oldmtext +' '+ mtext;
					$('#mergetext').val(mtext);
				}
			}
			$('#moneyconvertoverlay').fadeOut();
		});
		$('#formatorclose').click(function(){
			$('#moneyconvertoverlay').fadeOut();
		});
	}
	$("#moduleid").change(function() {
		var mid = $("#moduleid").val();
		if(mid!='') {
			relatedmodulelistshow(mid);
		} else {
			$('#mergemoduleid').empty();
		}
		$('#mergetext').val('');
		$('#fieldname').select2('val','').trigger('change');
	});
	//folder name operation
	$('#folderfoldernameid').val('');
	$("#printtemplatesfoladdbutton").mousedown(function(e){
		if(e.which == 1 || e.which === undefined) {
			$('#printtemplatesfoladdgrid1validation').validationEngine('validate');
		}
	});
	$('#printtemplatesfoladdgrid1validation').validationEngine({
		onSuccess: function() {
			printtemplatesfolderdataupdatefun();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	$("#printtemplatesfolupdatebutton").mousedown(function(e){
		if(e.which==1) {
			$('#printtemplatesfoleditgrid1validation').validationEngine('validate');
		}
	});
	$('#printtemplatesfoleditgrid1validation').validationEngine({
		onSuccess: function() {
			printtemplatesfolupdatefun();
		},
		onFailure: function() {
			var dropdownid =[];
			dropdownfailureerror(dropdownid);
			alertpopup(validationalert);
		}
	});
	//edit operation
	$("#printtemplatesfolingridedit1").click(function(){
		var datarowid = $('#printtemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
		if (datarowid) {
			resetFields();
			sectionpanelheight('printtemplatesfoloverlay');
			$("#printtemplatesfoloverlay").removeClass("closed");
			$("#printtemplatesfoloverlay").addClass("effectbox");
			//$('#printtemplatesfolingriddel1').hide();
			$("#printtemplatesfolupdatebutton").show().removeClass('hidedisplayfwg');
			$("#printtemplatesfoladdbutton").hide();
			printtemplatefolderdatafetch(datarowid);
			Materialize.updateTextFields();
			firstfieldfocus();
		} else {
			alertpopup('Please select a row');
		}
	});
	//delete operation
	$("#printtemplatesfolingriddel1").click(function(){
		var datarowid = $('#printtemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
		if (datarowid) {
			$.ajax({ // checking whether this value is already in usage.
				url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=printtemplates&fieldid=foldernameid&table1=''&fieldid1=''&table2=''&fieldid2=''",
				success: function(msg) { 
				if(msg > 0){
					alertpopup("This folder already mapped in printtemplate.Unable to delete");						
				}else{
					deletefolderoperation(datarowid);
				}
				},
				});
			
		} else {
			alertpopup('Please select a row');
		}
	});
	//preview and print template load
	$('#pdfheadericonbtn,#printheadericonbtn').click(function(){
		previewtemplateload();
	});
	//folder name grid reload
	$('#tab1').click(function(){
		folderrefreshgrid();
		$('#dataaddsbtn,#dataupdatesubbtn').hide();
	});
	$( window ).resize(function() {
		maingridresizeheightset('printtemplatesaddgrid');
	});
	$('#printformatid').change(function(){
		var formattype=$(this).val();
		 $('#height,#width').val('');
		 $('#pageformatid').select2('val','').trigger('change');
		 if(formattype == 3){
			$('#pageformatiddivhid').addClass('hidedisplay');
			$('#height,#width').prop('readonly',false);
		}else {
			$('#pageformatiddivhid').removeClass('hidedisplay');
			$('#height,#width').prop('readonly',true);
		}
	});
	$('#pageformatid').change(function(){
		var fomattype=$(this).val();
		if(fomattype != '') {
			getformatdata(fomattype);
		}
		Materialize.updateTextFields();
	});
	$('#tab2,#tab3').click(function(){
		if(editstatus == 0){
		  $('#dataaddsbtn').show();
		}else{
		  $('#dataupdatesubbtn').show();
		}
	});
	// mention plugin
	/* $.getJSON(base_url+'Tagtemplate/loadautocompletedata', function(data) {
		$("#mergetext").mention({
			users: data,
			after : " "
		});
	}); */
	/* $("#formulatext").mention({
		users: formula_array
	}); */
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,printtemplatesaddgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			printtemplatesaddgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	// Print Type Trigger Change
	$('#printtypeid').change(function() {
		var value = $(this).val();
		if(value == 5) {
			$("#templateeditor_div" ).hide();
			$("#texteditor_div" ).show();
			$('#textlengthdivhid').after('<div class="large-6 medium-6 small-6 columns" id="uploadtxtfilediv" ><label>Upload Text File</label><div id="tagfileuploadfromid" style="text-align:center; border:1px solid  #ffffff;"><span class="" title="Click to upload" style="padding:2px; color:#2574a9; font-size:26px;"><span id=""><i class="material-icons">file_upload</i></span></span></div></div><input id="tagimage" type="hidden" name="tagimage" value="" tabindex="114">							<span id="taglogomulitplefileuploader" style="display:none;">upload</span>');
		} else {
			$("#texteditor_div" ).hide();
			$("#templateeditor_div" ).show();
			$('#uploadtxtfilediv').remove();
		}
	});
	{// File upload				
		fileuploadmoname = 'tagfileupload';
		var companysettings = {
			url: base_url+"upload_file.php",
			method: "POST",
			fileName: "myfile",
			allowedTypes: "txt",
			dataType:'json',
			async:false,			
			onSuccess:function(files,data,xhr) { 
				if(data != 'Size') {
					var prnfile = data.split('*'); 
					var arr = prnfile[0].split(',');
					var name = $('.dyimageupload').data('imgattr');
					$('#tagimage').val(arr[3]); 
					$('#prnfilename').val(prnfile[0]); 
					$('#tageditor').val(prnfile[1]);
					Materialize.updateTextFields();
					alertpopup('Your file is uploaded successfully.');
					} else {
					alertpopup('Image size too large. Image size must be less than 5 megabytes.');
				}
			},
			onError: function(files,status,errMsg) {
			}
		}
		$("#taglogomulitplefileuploader").uploadFile(companysettings);
		//file upload drop down change function
		$(document).on( "click", "#tagfileuploadfromid", function() {
			$(".triggeruploadtagfileupload:last").trigger('click');
		});
	}
});
//printtemplates folder  Grid
function printtemplatesfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $('#printtemplatesfoladdgrid1').width();
	var wheight = $('#printtemplatesfoladdgrid1').height();
	//col sort
	var sortcol = $('.printtemplatefolderheadercolsort').hasClass('datasort') ? $('.printtemplatefolderheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.printtemplatefolderheadercolsort').hasClass('datasort') ? $('.printtemplatefolderheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.printtemplatefolderheadercolsort').hasClass('datasort') ? $('.printtemplatefolderheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Printtemplates/printtemplatefoldervalueget?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=251',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#printtemplatesfoladdgrid1').empty();
			$('#printtemplatesfoladdgrid1').append(data.content);
			$('#printtemplatesfoladdgrid1footer').empty();
			$('#printtemplatesfoladdgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('printtemplatesfoladdgrid1');
			{//sorting
				$('.printtemplatefolderheadercolsort').click(function(){
					$('.printtemplatefolderheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					printtemplatesfoladdgrid1(page,rowcount);
				});
				sortordertypereset('printtemplatefolderheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					printtemplatesfoladdgrid1(page,rowcount);
				});
				$('#printtemplatefolderpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					printtemplatesfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#printtemplatefolderpgrowcount').material_select();
		}
	});
}

/* Main grid printtemplateaddgrid*/
function printtemplatesaddgrid(page,rowcount) {
	if(Operation == 1){
		var rowcount = $('#printtemplatespgrowcount').val();
		var page = $('.paging').data('pagenum');
	} else{
		var rowcount = $('#printtemplatespgrowcount').val();
		page = typeof page == 'undefined' ? 1 : page;
		rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	}
	var wwidth = $('#printtemplatesaddgrid').width();
	var wheight = $(window).height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.printtemplatesheadercolsort').hasClass('datasort') ? $('.printtemplatesheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.printtemplatesheadercolsort').hasClass('datasort') ? $('.printtemplatesheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.printtemplatesheadercolsort').hasClass('datasort') ? $('.printtemplatesheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#viewfieldids').val();
	var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
	var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
	var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=printtemplates&primaryid=printtemplatesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#printtemplatesaddgrid').empty();
			$('#printtemplatesaddgrid').append(data.content);
			$('#printtemplatesaddgridfooter').empty();
			$('#printtemplatesaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('printtemplatesaddgrid');
			{//sorting
				$('.printtemplatesheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.printtemplatesheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul.printtemplatespgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.printtemplatesheadercolsort').hasClass('datasort') ? $('.printtemplatesheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.printtemplatesheadercolsort').hasClass('datasort') ? $('.printtemplatesheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					var sortpos = $('#printtemplatesaddgrid .gridcontent').scrollLeft();
					printtemplatesaddgrid(page,rowcount);
					$('#printtemplatesaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
					
				});
				sortordertypereset('printtemplatesheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function() {
					Operation = 0;
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					printtemplatesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#printtemplatespgrowcount').change(function() {
					Operation = 0;
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = 1;
					printtemplatesaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//onclick 
			$(".gridcontent").click(function(){
				var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#printtemplatespgrowcount').material_select();
		},
	});
}

{//refresh grid
	function refreshgrid() {
		var page = $('ul#printtemplatespgnum li.active').data('pagenum');
		var rowcount = $('ul.printtemplatespgnumcnt li .page-text .active').data('rowcount');
		printtemplatesaddgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#printtemplatefooterpgnum li.active').data('pagenum');
		var rowcount = $('ul.printtemplatefooterpgnumcnt li .page-text .active').data('rowcount');
		printtemplatesfoladdgrid1(page,rowcount);
	}
}
//create new template
function printtemplatesnewdataaddfun() {
	var amp = '&';
	var editorname = $('#editornameinfo').val();
	var printtypeid = $('#printtypeid').find('option:selected').val();
	if(printtypeid == 5) {
		var editordata = $('#tageditor').val();
		var filename = createeditorfile($('#tageditor').val(),$('#templatename').val());
	} else if(printtypeid == 3 || printtypeid == 6) {
		var editordata = froalaeditoradd(editorname);
	}
	var formdata = $('#printtemplatesdataaddform').serialize();
	var datainformation = amp + formdata;
	if(editordata != "") {
		$.ajax({
			url:base_url+'Printtemplates/newdatacreate',
			data:'datas='+datainformation+amp+"printtypeid="+printtypeid,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$('.ftab').trigger('click');
					resetFields();
					$('#printtemplatescreationformadd').hide();
					$('#printtemplatescreationview').fadeIn(1000);
					refreshgrid();
					$('#dataaddsbtn').attr('disabled',false);
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					viewgridview = 1;
					addformview = 0;
				}
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
}
function createeditorfile(prn,filedetails) {
	var amp = '&';
	var filename = ''
	$.ajax({
        url: base_url +"Printtemplates/createeditorfile",
        data: "prn="+prn+amp+"filename="+filedetails,
		type: "POST",
        success: function(msg) {
			var nmsg =  $.trim(msg);
			if(nmsg != '') {
				filename = nmsg;
			}
		},
	});
	return filename;
}
function printtemplatesdataupdateinfofetchfun(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax({
		url:base_url+'Printtemplates/fetchformdataeditdetails?dataprimaryid='+datarowid+'&elementsname='+elementsname+'&elementstable='+elementstable+'&elementscolmn='+elementscolmn+'&elementspartabname='+elementpartable+'&resctable='+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				$('#printtemplatescreationformadd').hide();
				$('#printtemplatescreationview').fadeIn(1000);
				refreshgrid();
				//For Touch
				smsmasterfortouch = 0;
			} else {
				addslideup('printtemplatescreationview','printtemplatescreationformadd');				
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var printtypeid = data['printtypeid'];
				$('#printtypeid').select2('val',printtypeid).trigger('change');
				var filename = data['printtemplatestemplatedesign_editorfilename'];
				if(printtypeid == 5) {
					retrievetexteditordata(filename);
				} else if(printtypeid == 3 || printtypeid == 6) {
					filenamevaluefetch(filename);
				}
				setTimeout(function(){
					$('#foldernameid').select2('val',data['foldernameid']);
				},100);	
				$('#printformatid').trigger('change');
				setTimeout(function(){
					$('#pageformatid').select2('val',data['pageformatid']).trigger('change');
				},50);
				Materialize.updateTextFields();
			}
		}
	});
}
function filenamevaluefetch(filename){
	$.ajax({
		url: base_url + "Printtemplates/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data)   {
			 froalaedit(data,'printtemplatestemplatedesign_editor');
		},
	});
}
{//crud operation
	function crudactionenable() {
		$('#addicon').click(function(){
			addslideup('printtemplatescreationview','printtemplatescreationformadd');
			resetFields();
			folderdatareload();
			$('#tab2').trigger('click');
			$('#moduleid').select2('readonly',false);
			$('#mergemoduleid').select2('readonly',false);
			$('.addbtnclass').removeClass('hidedisplay');
			$('.updatebtnclass').addClass('hidedisplay');
			$('#dataupdatesubbtn').hide();
			$('.editformsummmarybtnclass').addClass('hidedisplay');	
			$('.pdfheadericonspan').addClass('hidedisplay');
			var froalaarray=['printtemplatestemplatedesign_editor','html.set',''];
			froalaset(froalaarray);
			$("#setasdefault").val('No');
			setTimeout(function() {
					printtemplatesfoladdgrid1();
			}, 100);
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			Materialize.updateTextFields();
			//for autonumber
			randomnumbergenerate("anfieldnameinfo","anfieldnameidinfo","anfieldnamemodinfo","anfieldtabinfo");
			fortabtouch = 0;
			$('#mergetext').val('');
			$('#primarydataid').val('');
			$('#dataaddsbtn').show();
			totalddcreate();
			$('#printformatid').select2('val',2).trigger('change');
			Materialize.updateTextFields();
			//For Keyboard Shortcut Variables
			viewgridview = 0;
			addformview = 1;
			Operation = 0; //for pagination
			editstatus = 0;
		});
		$('#editicon').click(function() {
			var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				editstatus=1;
				$('#tab2').trigger('click');
				$('#moduleid').select2('readonly', true);
				$('#dataaddsbtn').hide();
				folderdatareload();
				$('.updatebtnclass').removeClass('hidedisplay');
				$('.addbtnclass').addClass('hidedisplay');
				$('.editformsummmarybtnclass').addClass('hidedisplay');	
				$('.pdfheadericonspan').addClass('hidedisplay');
				printtemplatesdataupdateinfofetchfun(datarowid);
				Materialize.updateTextFields();
				firstfieldfocus();
				fortabtouch = 1;
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
				setTimeout(function(){ $('#moduleid').trigger('change'); },200);
				$('#mergetext').val('');
				totalddcreate();
				setTimeout(function(){
					var modid = $('#moduleid').val();
					$('#mergemoduleid').select2('val',modid);
					$('#mergemoduleid').trigger('change');
				},200);
				Operation = 1; //for pagination
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#deleteicon').click(function(){
			var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
			if (datarowid) {
				$('#basedeleteoverlay').fadeIn();
			} else {
				alertpopup('Please select a row');
			}
		});
		$('#basedeleteyes').click(function(){
			var datarowid = $('#printtemplatesaddgrid div.gridcontent div.active').attr('id');
			printtemplatesrecorddelete(datarowid);
		});
			
		
		
	}
}
// Update Function
function printtemplatesdataupdatefun() {
	var amp = '&';
	var editorname = $('#editornameinfo').val();
	var printtypeid = $('#printtypeid').find('option:selected').val();
	if(printtypeid == 5) {
		var editordata = $('#tageditor').val();
		var filename = createeditorfile($('#tageditor').val(),$('#templatename').val());
	} else if(printtypeid == 3 || printtypeid == 6) {
		var editordata = froalaeditoradd(editorname);
	}
	var formdata = $('#printtemplatesdataaddform').serialize();
	var datainformation = amp + formdata;
	if(editordata != ""){
		$.ajax({
			url: base_url+'Printtemplates/datainformationupdate',
			data: 'datas='+datainformation+amp+"printtypeid="+printtypeid,
			type: 'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					$('.ftab').trigger('click');
					resetFields();
					$('#printtemplatescreationformadd').hide();
					$('#printtemplatescreationview').fadeIn(1000);
					refreshgrid();
					$('#dataupdatesubbtn').attr('disabled',false);
					alertpopup(savealert);
					//For Keyboard Shortcut Variables
					addformupdate = 0;
					viewgridview = 1;
					addformview = 0;
				}
			},
		});
	} else {
		alertpopup('Please enter the values in template editor');
	}
}
function printtemplatesrecorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
	$.ajax({
		url: base_url + 'Printtemplates/deleteinformationdata?primarydataid='+datarowid+'&elementstable='+elementstable+'&parenttable='+elementspartable,
		async:false,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				refreshgrid();
				$('#basedeleteoverlay').fadeOut();
				alertpopup('Record deleted successfully');
			} else if (nmsg == "false")  {
				$("#smsmasterviewgrid").trigger('reloadGrid');
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
	});
}
function viewcreatesuccfun(viewname) {
	var viewmoduleids = $('#viewfieldids').val();
	dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
	if(viewname != 'dontset') {
		setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
		}, 1000);
		cleargriddata('viewcreateconditiongrid');
	} else {
		$('#dynamicdddataview').trigger('change');
	}
}
//drop down values set for view
function templatedropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Printtemplates/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$('#'+ddname+'')
				.append($("<option></option>")
				.attr("data-"+dataattrname,1)
				.attr("data-"+otherdataattrname1,'Number')
				.attr("data-"+otherdataattrname2,'S')
				.attr("value",'1-S.Number')
				.text("S.Number"));
				var i=2;
				$.each(data, function(index) {
					if(data[index]['otherdataattrname2']!='') {
						$('#'+ddname+'')
						.append($("<option></option>")
						.attr("data-"+dataattrname,data[index]['datasid'])
						.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
						.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
						.attr("value",data[index]['datasid']+"-"+data[index]['dataname'])
						.text(data[index]['dataname']));
						i++;
					}
				});
			}
			$('#'+ddname+'').trigger('change');
		},
	});
}
//amount to words conversion
function currencyworddropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option value=''></option>"));
	$.ajax({
		url:base_url+'Printtemplates/fetchamtworddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var sno=1;
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['datasid']+"-"+data[index]['dataname'])
					.text(data[index]['dataname']));
					sno++;
				});
			}
			$('#'+ddname+'').trigger('change');
		},
	});
}
//related module list
function relatedmodulelistshow(moduleid) {
	$('#mergemoduleid').empty();
	$('#mergemoduleid').append($("<option></option>"));
	var modulename=$('#moduleid').find('option:selected').data('moduleidhidden');
	$.ajax({
		url:base_url+'Printtemplates/relatedmodulelistfetch',
		data:'moduleid='+moduleid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$('#mergemoduleid').append($("<option></option>").attr("value",moduleid).text(modulename));
				$.each(data, function(index) {
					$('#mergemoduleid')
					.append($("<option></option>")
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			setTimeout(function() {
						 $('#mergemoduleid').select2('val',moduleid).trigger('change');
			},50);
			
		},             
	});
}
//print templates folder add function
function printtemplatesfolderdataupdatefun(){
	var foldername = $("#foldernamename").val();
	var description = $("#folderdescription").val();
	var setdefault = $("#foldersetdefault").val();
	$.ajax({
		url:base_url+'Printtemplates/printtemplatefolderadd',
		data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault,
		type:'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#folderfoldernameid').val('');
				$('#printtemplatesfolupdatebutton').hide();
				$('#printtemplatesfoladdbutton').show();
				$("#printtemplatesfolingriddel1,#printtemplatesfoladdbutton").show();
				folderrefreshgrid();
				$("#printtemplatesfolcancelbutton").trigger('click');
				$('#dataaddsbtn').attr('disabled',false);
				alertpopup(savealert);
				folderdatareload();
			}
		},
	});
}
//new folder name load
function folderdatareload() {
	$('#foldernameid').empty();
	$('#foldernameid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Printtemplates/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var status;
				$.each(data, function(index) {
					$('#foldernameid')
					.append($("<option></option>")
					.attr("data-foldernameidhidden",data[index]['datasid'])
					.attr("value",data[index]['datasid'])
					.attr("data-default",data[index]['default'])
					.text(data[index]['dataname']));
					 if(data[index]['default'] == 'Yes'){
						status=data[index]['datasid'];
					}
				});
				 setTimeout(function(){
			     $('#foldernameid').select2('val',status).trigger('change');
		       },25); 
		}	
		},
	});
}
//load template file
function previewtemplateload() {
	$('#pdftemplateprview,#printtemplatepreview').empty();
	$('#pdftemplateprview,#printtemplatepreview').append($("<option value=''>select</option>"));
	$.ajax({
		url:base_url+'Base/fetchdddataviewddval?dataname=printtemplatesname&dataid=printtemplatesid&datatab=printtemplates',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#pdftemplateprview,#printtemplatepreview')
					.append($("<option></option>")
					.attr("data-foldernameidhidden",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#pdftemplateprview,#printtemplatepreview').trigger('change');
		},
	});
}
//print templates folder add function
function printtemplatesfolupdatefun() {
	var foldernameid = $("#folderfoldernameid").val();
	var foldername = $("#foldernamename").val();
	var description = $("#folderdescription").val();
	var setdefault = $("#foldersetdefault").val();
	$.ajax({
		url:base_url+'Printtemplates/printtemplatefolderupdate',
		data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid,
		type:'POST',
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				resetFields();
				$('#printtemplatesfolingridreload1').show();
				$("#printtemplatesfolupdatebutton").hide();
				$("#printtemplatesfoladdbutton,#printtemplatesfolingriddel1").show();
				folderrefreshgrid();
				$('#dataaddsbtn').attr('disabled',false);
				$("#printtemplatesfolcancelbutton").trigger('click');
				alertpopup(savealert);
				folderdatareload();
			}
		},
	});
}
//print tempaltes folder data fetch
function printtemplatefolderdatafetch(folderid){
	$("#folderfoldernameid").val(folderid);
	$.ajax({
		url:base_url+'Printtemplates/printtemplatefolderdatafetch?folderid='+folderid,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var foldername = data.foldername;
				var description = data.description;
				$("#foldernamename").val(foldername);
				$("#folderdescription").val(description);
				var setdefault = data.setdefault;
				if(setdefault == 'Yes'){
					$("#foldersetdefaultcboxid").prop('checked',true);
					$("#foldersetdefault").val('Yes');
				}else{
					$("#foldersetdefaultcboxid").prop('checked',false);
					$("#foldersetdefault").val('No');
				}
			}
		},
	});
}
//delete folder 
function deletefolderoperation(id){
	$.ajax({
		url:base_url+'Printtemplates/folderdeleteoperation?id='+id,
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
			if (nmsg == 'TRUE') {
				folderrefreshgrid();
				$('#printtemplatesfolingriddel1').attr('disabled',false);
				alertpopup("Deleted successfully");
			}
		},
	});
}
//print template name check
function printtempnamecheck() {
	var value = $('#templatename').val();
	var primaryid = $('#primarydataid').val();
	var nmsg = "";
	if( value !="" ) {
		$.ajax({
			url:base_url+"Printtemplates/printtemplateuniquename",
			data:"data=&printtempname="+value,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Name already exists!";
				}
			} else {
				return "Name already exists!";
			}
		}
	}
}
/* //print template name check
function printtempfoldernamecheck() {
	var value = $('#foldernamename').val();
	var primaryid = $('#folderfoldernameid').val();
	var nmsg = "";
	if( value !="" ) {
		$.ajax({
			url:base_url+"Printtemplates/printtemplatefolderuniquename",
			data:"data=&foldername="+value,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Name already exists!";
				}
			} else {
				return "Name already exists!";
			}
		}
	}
} */
//folder name check
function foldernamecheck() {
	var primaryid = $("#folderfoldernameid").val();
	var accname = $("#foldernamename").val();
	var elementpartable = 'foldername';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Printtemplates/folderuniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder name already exists!";
				}
			}else {
				return "Folder name already exists!";
			}
		} 
	}
}
//this is for the product search/attribute icon-dynamic append
function iconafterload(fieldid,clickevnid){
	$("#"+fieldid+"").after('<span class="icon icon-paragraph-justify" title="Currency Formatter" id="'+clickevnid+'" style="left:4px;top:4px;cursor:pointer;position:relative;"></span>');
}
function getformatdata(fomattype) {
	
	$.ajax({
    url: base_url + "Printtemplates/getformatdata",
    data: "fomattype="+fomattype,
    async:false,
	cache:false,
	type: "POST",
	dataType:'json',
	success: function(data) 
    { 
	  if(data != 'NO') {
			  $('#height').val(data.height);	
			  $('#width').val(data.width);	
	  } else {
		  
	  }
    },
});
}
function loadmodulefield(moduleid){
	var relatedmoduleid='';
	$('#fieldname').empty();
	$('#fieldname').append($("<option value=''>Select</option>"));
	if(checkValue(moduleid) == true || checkValue(relatedmoduleid) == true){				
		$.ajax({
		url: base_url+"Tagtemplate/modulefieldload",
		data:{moduleid:moduleid,relatedmoduleid:relatedmoduleid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) {	
			dropdownid='fieldname';
			if((data.fail) == 'FAILED') {
			} else {
				
				prev = ' ';
				$.each(data, function(index) {
					var cur = data[index]['optgroupid'];
					if(prev != cur ) {
						if(prev != " ") {
							$('#fieldname').append($("</optgroup>"));
						}
						$('#fieldname').append($("<optgroup  label='"+data[index]['pname']+"'>"));
						prev = data[index]['optgroupid'];
					}
					$("#"+dropdownid+" optgroup[label='"+data[index]['pname']+"']")
					.append($("<option></option>")
					.attr("data-optgroupid",data[index]['optgroupid'])
					.attr("data-key",data[index]['key'])
					.attr("value",data[index]['pid'])
					.attr("data-id",data[index]['pid'])
					.attr("data-colname",data[index]['label'])
					.text(data[index]['label']));
				});
			}	
		},
		});
	}
}
function loadwherecalusefield(moduleid){
	$('#whereclauseid').empty();
	$('#whereclauseid').append($("<option></option>"));
	if(checkValue(moduleid) == true){				
		$.ajax({
		url: base_url+"Printtemplates/whereclauseload",
		data:{moduleid:moduleid},
		dataType:'json',
		type: "POST",
		async:false,
		success: function(data) {	
				$.each(data, function(index) {
				$("#whereclauseid")
				.append($("<option></option>")
				.attr("value",data[index]['id'])
				.text(data[index]['id']+'-'+data[index]['description']));
			});
		},
		});
	}
}
function totalddcreate() {
	$('#total,#seconditeration,#numberstoword').empty();
	$('#total,#seconditeration,#numberstoword').append($("<option></option>"));
	$('#total,#seconditeration,#numberstoword').append($("<option value='no'></option>").text('no'));
	$('#total,#seconditeration,#numberstoword').append($("<option value='yes'></option>").text('yes'));
	$('#total,#seconditeration,#numberstoword').select2('val','no');
	$('#orderby').empty();
	$('#orderby').append($("<option value='none'></option>").text('none'));
	$('#orderby').append($("<option value='asc'></option>").text('asc'));
	$('#orderby').append($("<option value='desc'></option>").text('desc'));
	$('#orderby').select2('val','none');
}
function retrievetexteditordata(filename) {
	//checks whether empty
	if(filename == '' || filename == null )	{
		$("#tageditor").editable("setHTML", "");
	} else {
		$.ajax({
			url: base_url + "Printtemplates/gettemplatesource?templatename="+filename,
			async:false,
			cache:false,
			success: function(data) {
				if(data != 'Fail') {					
					$("#tageditor").val(data);
				} else {
					$("#tageditor").val(data);
				}
			},
		});
	}	
}