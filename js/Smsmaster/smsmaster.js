$(document).ready(function() {
	{// Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		smsmasterviewgrid();
		//crud action
		crudactionenable();
	}
	{//inner-form-with-grid
		$("#smstemplatesfolingridadd1").click(function(){
			sectionpanelheight('smstemplatesfoloverlay');
			$("#smstemplatesfoloverlay").removeClass("closed");
			$("#smstemplatesfoloverlay").addClass("effectbox");
			$("#smstemplatesfoladdbutton").show();
			$("#smstemplatesfolupdatebutton").hide();
			resetFields();
			$('#setdefaultcboxid').prop('checked', false);
			$("#setdefault").val('No');
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#smstemplatesfolcancelbutton").click(function(){
			$("#smstemplatesfoloverlay").removeClass("effectbox");
			$("#smstemplatesfoloverlay").addClass("closed");
			$("#smstemplatesfolingridadd1,#smstemplatesfolingridedit1,#smstemplatesfolingriddel1").show();
		});
	}
	$('#formclearicon').click(function() {
		$("#formfields,#relatedtomoduleid").empty();
		$("#moduleid,#smsgrouptypeid,#smssendtypeid").attr('readonly',false);
		$("#templatetypeid").val('2');
		var elementname = $('#elementsname').val();
		elementdefvalueset(elementname);
		$("#leadtemplatecontent_editorfilenamedivhid").find('p').css('opacity','0');
	});
	{//Hide show form template type
		$('#templatetypeid').change(function() {
			$('#moduleid,#leadtemplatecontent_editorfilename,#mergetext').val('');
			$('#relatedtomoduleid,#formfields,#mergetext,#notificationlogtypeid').empty();
			$('#smsgrouptypeid,#smssendtypeid,#newsmssettingsid,#notificationlogtypeid').select2('val','');
			$('#relatedtomoduleid,#formfields').select2('val','');
			var ttid = $("#templatetypeid").val();
			hideshowfields(ttid);
		});
	}
	{//Folder Tab Checkbox
		$("#setdefaultcboxid").change(function(){
		    if($(this).is(":checked")){
		        $("#setdefault").val("Yes");
		    } else {
		        $("#setdefault").val("No");
		    }
		});
	}
	//folder name grid reload
	$('#tab1').click(function() {
		folderrefreshgrid();
		$("#foldernamename").val('');
		$("#description").val('');
		$("#foldernameeditid").val('');
		$("#setdefaultcboxid").attr('checked',false);
		$("#setdefault").val('No');
		$("#smstemplatesfolingriddel1,#smstemplatesfoladdbutton").show();
		$("#smstemplatesfolupdatebutton").hide();
	});
	{//Close Add Screen
		var addcloseoppocreation =["closeaddform","smsmastercreationview","smsmastercreationformadd"]
		addclose(addcloseoppocreation);
		var addcloseviewcreation =["viewcloseformiconid","smsmastercreationview","viewcreationformdiv",""];
		addclose(addcloseviewcreation);
	}
	{
		$('#alertsfcloseyes').click(function() {
			$("#primarydataid").val('');
		});
	}
	{// For touch
		fortabtouch = 0;
	}
	{//Folder Button Show/Hide
		$("#smstemplatesfolingrideditspan1,#smstemplatesfolingridreloadspan1").show();
	}
	{//reload icon
		$("#reloadicon").click(function(){
			refreshgrid();
		});
		$( window ).resize(function() {
			maingridresizeheightset('smsmasterviewgrid');
			sectionpanelheight('smstemplatesfoloverlay');
		});
	}
	$("#newsmssettingsid").click(function() {
		var senderid = $("#newsmssettingsid").val();
		$("#smssettingsid").val(senderid);
	});
	//close
	$('#closeaddoppocreation').click(function(){
		$('#dynamicdddataview').trigger('change');
	});
	{//view by drop down change
		$('#dynamicdddataview').change(function(){
			smsmasterviewgrid();
		});
	}
	{	//validation for SMS Master Add  
		$('#dataaddsbtn').click(function() {
			$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			var ttid = $("#templatetypeid").val();
			if(ttid != 2 ) {
				$("#newsmssettingsid").removeClass('validate[required]');
			}
			$("#formaddwizard").validationEngine('validate');
			//For Touch
			smsmasterfortouch = 0;
		});
		jQuery("#formaddwizard").validationEngine( {
			onSuccess: function() {
				$('#dataaddsbtn').attr('disabled','disabled');
				newdataaddfun();
				$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			},
			onFailure: function() {
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
				setTimeout(function() {
					$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				}, 500);
			}
		});
	}
	{	//update SMS Master information
		$('#dataupdatesubbtn').click(function() {
			$('#foldernamename').removeClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
			$("#formeditwizard").validationEngine('validate');
		});
		jQuery("#formeditwizard").validationEngine({
			onSuccess: function() {
				$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				$('#dataupdatesubbtn').attr('disabled','disabled');
				dataupdateinformationfun();
			},
			onFailure: function() {
				$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
				var dropdownid =['1','foldernameid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	//module change events
	$('#moduleid').change(function() {
		var id = $(this).val();
		var type = $('#smsgrouptypeid').val();
		var ttid = $("#templatetypeid").val();
		$('#relatedtomoduleid,#formfields').empty();
		$('#relatedtomoduleid,#formfields').select2('val','');
		if(id != '') {
			if(ttid == 2){
				if(type != '') {
					relatedtodropdownload(id);
				} else {
					$(this).select2('val','');
					alertpopup('Please select the template mode');
				}
			}else if(ttid == 3){
				relatedtodropdownload(id);
			}else if(ttid == 4){
				notificationtypeget(id);
				relatedtodropdownload(id);
			}
		}
		$('#mergetext').val('');
	});
	$("#relatedtomoduleid").change(function() {
		var module = $("#moduleid").val();
		$('#formfields,#mergetext').empty();
		$('#formfields').select2('val','');
		$('#mergetext').val('');
		if(module != '' || module != null) {
			var relatedmoduleid = $(this).val();
			loadmodulefield(module,relatedmoduleid);
		} else {
			alertpopup('Please select the module list');
		}
	});
	//module fields change events
	$('#formfields').change(function() {
		$('#mergetext').val('');
		if( $(this).val()!= "" ) {
			var parmodid = $('#moduleid').val();
			var fildmodid = $('#relatedtomoduleid').val();
			var fildmodtype = $('#relatedtomoduleid').find('option:selected').data('mergemoduletype');
			if(parmodid!='' && fildmodid!='') {
				var columnid=$(this).find('option:selected').data('id');
				var columnname=$(this).find('option:selected').data('colname');
				var mtext = '{'+columnid+'.'+columnname+'}';
				$('#mergetext').val(mtext);
			}
		}
		Materialize.updateTextFields();
	});
	{//Folder Operation
		//Insertion
		$("#smstemplatesfoladdbutton").click(function() {
			$('#smstemplatesfoladdgrid1validation').validationEngine('validate');
		});
		$('#smstemplatesfoladdgrid1validation').validationEngine({
			onSuccess: function() {
				smsmasterfolderinsertfun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//Edit Operation
		$("#smstemplatesfolingridedit1").click(function() {
			var datarowid = $('#smstemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				sectionpanelheight('smstemplatesfoloverlay');
				$("#smstemplatesfoloverlay").removeClass("closed");
				$("#smstemplatesfoloverlay").addClass("effectbox");
				$("#smstemplatesfolupdatebutton").show().removeClass('hidedisplayfwg');
				$("#smstemplatesfoladdbutton,#smstemplatesfolingriddel1").hide();
				smstemplatefolderdatafetch(datarowid);
				firstfieldfocus();
				//For Touch
				smsmasterfortouch = 3;
				Materialize.updateTextFields();
			} else {
				alertpopup('Please select a row');
			}
		});
		//Update operation
		$("#smstemplatesfolupdatebutton").click(function() {
			$('#smstemplatesfoleditgrid1validation').validationEngine('validate');
		});
		$('#smstemplatesfoleditgrid1validation').validationEngine({
			onSuccess: function() {
				smstemplatesfolupdatefun();
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//delete operation
		$("#smstemplatesfolingriddel1").click(function() {
			var datarowid = $('#smstemplatesfoladdgrid1 div.gridcontent div.active').attr('id');
			if (datarowid) {
				deletefolderoperation(datarowid);
			} else {
				alertpopup('Please select a row');
			}
		});
	}
	{//Template Count
		$("#leadtemplatecontent_editorfilenamedivhid").append('<p align="right" style="color:#546E7A;font-size:0.8rem; margin:0;opacity:1;">&nbsp;</p>');
		$('#leadtemplatecontent_editorfilename').keyup(function(e) {
			var $this = $(this);
			setTimeout(function() {
				var text = $this.val();
				var lengthofchar = text.length;
				$("#leadtemplatecontent_editorfilenamedivhid").find('p').text(''+lengthofchar+'/1000');
				var type = $("#smsgrouptypeid").val();
				if(lengthofchar > 0) {
					var mid = $("#moduleid").val();
					if((mid == '' && type == '4')  || mid != '') {
						$("#moduleid,#smsgrouptypeid").attr('readonly',true);
					} else {
						$("#moduleid,#smsgrouptypeid").attr('readonly',false);
					}
				} else {
					if(type ==2){
						$("#moduleid").attr('readonly',true);
					}
					$("#smsgrouptypeid").attr('readonly',false);
				}
			}, 0);
		});
	}
	$("#smstemplatesfolingridreloadspan1").hide();
	//folder reload
	$("#smstemplatesfolingridreload1").click(function() {
		folderdatareload();
		folderrefreshgrid();
		$("#smstemplatesfoladdbutton,#smstemplatesfolingriddel1").show();
		$("#smstemplatesfolupdatebutton").hide();
		resetFields();
		Materialize.updateTextFields();
	});
	$("#smssendtypeid").change(function() {
		var typeid = $("#templatetypeid").val();
		var smssendtype = $("#smssendtypeid").val();
		if(typeid == 2){
			if(smssendtype == 2) {
				$("#newsmssettingsid").addClass('validate[required]');
			} else {
				$("#newsmssettingsid").removeClass('validate[required]');
			}
			senderidddvalfetch(smssendtype);
		}
	});
	//sms template type
	$("#smsgrouptypeid").change(function() {
		$("#mergetext").attr('readonly',true);
		$("#relatedtomoduleid,#formfields,#newsmssettingsid").empty();
		$("#smssendtypeid,#newsmssettingsid").select2('val','');
		readonlybasedontype();
	});
	{ // Filter Work by kumaresan
		$('#viewtoggle').click(function() {
			if(filterstatus == 0) {
				$("#processoverlay").show();
				$("#viewtoggle").show();
				var moduleid = $("#viewfieldids").val();
				getfilterdatas(moduleid,smsmasterviewgrid);
			}
			$("#filterviewdivhid").attr('class','large-3 medium-3 columns paddingzero ');
			$('#filterdisplaymainview').fadeIn();
			filterstatus = 1;
		});
		$(document).on("click",".filterdisplaymainviewclearclose",function() {
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div').children().select2('data',null);
			$('#filterconddisplay').children().remove();
			$('#filterformconditionvalidation .filterfieldbox .wrappercontent div .checkboxcls').prop("checked", false);
			$("#filterid,#ddfilterid,#conditionname,#ddconditionname,#filtervalue,#ddfiltervalue").val('');
			smsmasterviewgrid();
		});
		$(document).on( "click", ".filterdisplaymainviewclose", function() {
			$('#filterdisplaymainview').fadeOut();
		});
	}
	$("#tab2").click(function(){
		folderdatareload();
	});
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
				$('#setdefault').val('Yes');
			} else {
				$('#'+name+'').val('No');
				$('#setdefault').val('No');
			}
		});
	}
});
//drop down values set for view
function smsdropdownmodulevalset(ddname,dataattrname,otherdataattrname1,otherdataattrname2,dataname,dataid,otherdataname1,otherdataname2,datatable,whfield,whvalue) {
	$('#'+ddname+'').empty();
	$('#'+ddname+'').append($("<option></option>"));
	$.ajax({
		url:base_url+'Smsmaster/fetchmaildddatawithmultiplecond?dataname='+dataname+'&dataid='+dataid+'&datatab='+datatable+'&whdatafield='+whfield+'&whval='+whvalue+'&othername1='+otherdataname1+'&othername2='+otherdataname2,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#'+ddname+'')
					.append($("<option></option>")
					.attr("data-"+dataattrname,data[index]['datasid'])
					.attr("data-"+otherdataattrname1,data[index]['otherdataattrname1'])
					.attr("data-"+otherdataattrname2,data[index]['otherdataattrname2'])
					.attr("value",data[index]['dataname'])
					.text(data[index]['dataname']));
				});
			}	
			$('#'+ddname+'').trigger('change');
		},
	});
}
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewfieldids").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
				$('#dynamicdddataview').select2('val',viewname);
				$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
{// Main Grid Function
	function smsmasterviewgrid(page,rowcount) {
		if(Operation == 1){
			var rowcount = $('#smstemplatespgrowcount').val();
			var page = $('.paging').data('pagenum');
		} else{
			var rowcount = $('#smstemplatespgrowcount').val();
			page = typeof page == 'undefined' ? 1 : page;
			rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
		}
		var wwidth = $('#smsmasterviewgrid').width();
		var wheight = $(window).height();
		//col sort
		var sortcol = $("#sortcolumn").val();
		var sortord = $("#sortorder").val();
		if(sortcol){
			sortcol = sortcol;
		} else{
			var sortcol = $('.smstemplatesheadercolsort').hasClass('datasort') ? $('.smstemplatesheadercolsort.datasort').data('sortcolname') : '';
		}
		if(sortord) {
			sortord = sortord;
		} else {
			var sortord =  $('.smstemplatesheadercolsort').hasClass('datasort') ? $('.smstemplatesheadercolsort.datasort').data('sortorder') : '';
		}
		var headcolid = $('.smstemplatesheadercolsort').hasClass('datasort') ? $('.smstemplatesheadercolsort.datasort').attr('id') : '0';
		var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
		var viewfieldids = $('#viewfieldids').val();
		var filterid = $("#filterid").val()+'|'+$("#ddfilterid").val();
		var conditionname = $("#conditionname").val()+'|'+$("#ddconditionname").val();
		var filtervalue = $("#filtervalue").val()+'|'+$("#ddfiltervalue").val();
		$.ajax({
			url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=templates&primaryid=templatesid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
			contentType:'application/json; charset=utf-8',
			dataType:'json',
			async:false,
			cache:false,
			success :function(data) {
				$('#smsmasterviewgrid').empty();
				$('#smsmasterviewgrid').append(data.content);
				$('#smsmasterviewgridfooter').empty();
				$('#smsmasterviewgridfooter').append(data.footer);
				//data row select event
				datarowselectevt();
				//column resize
				columnresize('smsmasterviewgrid');
				{//sorting
					$('.smstemplatesheadercolsort').click(function(){
						$("#processoverlay").show();
						$('.smstemplatesheadercolsort').removeClass('datasort');
						$(this).addClass('datasort');
						var page = $(this).data('pagenum');
						var rowcount = $('ul.pagination-container li .page-text .active').data('rowcount');
						var sortcol = $('.smstemplatesheadercolsort').hasClass('datasort') ? $('.smstemplatesheadercolsort.datasort').data('sortcolname') : '';
						var sortord =  $('.smstemplatesheadercolsort').hasClass('datasort') ? $('.smstemplatesheadercolsort.datasort').data('sortorder') : '';
						$("#sortorder").val(sortord);
						$("#sortcolumn").val(sortcol);
						var sortpos = $('#smsmasterviewgrid .gridcontent').scrollLeft();
						smsmasterviewgrid(page,rowcount);
						$('#smsmasterviewgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
						$("#processoverlay").hide();
					});
					sortordertypereset('smstemplatesheadercolsort',headcolid,sortord);
				}
				{//pagination
					$('.pvpagnumclass').click(function(){
						Operation = 0;
						$("#processoverlay").show();
						var page = $(this).data('pagenum');
						var rowcount = $('.pagerowcount').data('rowcount');
						smsmasterviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
					$('#smstemplatespgrowcount').change(function(){
						Operation = 0;
						$("#processoverlay").show();
						var rowcount = $(this).val();
						$('.pagerowcount').attr('data-rowcount',rowcount);
						$('.pagerowcount').text(rowcount);
						var page = 1;
						smsmasterviewgrid(page,rowcount);
						$("#processoverlay").hide();
					});
				}
				//onclick 
				$(".gridcontent").click(function(){
					var datarowid = $('#smsmasterviewgrid div.gridcontent div.active').attr('id');
					if(datarowid){
						if(minifitertype == 'dashboard'){
							$("#minidashboard").trigger('click');
						} else if(minifitertype == 'notes') {
							$("#mininotes").trigger('click');
						}
					}
				});
				//Material select
				$('#smstemplatespgrowcount').material_select();
			},
		});
	}
}
//sms master folder  Grid
function smstemplatesfoladdgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 1000 : rowcount;
	var wwidth = $('#smstemplatesfoladdgrid1').width();
	var wheight = $('#smstemplatesfoladdgrid1').height();
	//col sort
	var sortcol = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortcolname') : '';
	var sortord =  $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').data('sortorder') : '';
	var headcolid = $('.folderlistheadercolsort').hasClass('datasort') ? $('.folderlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smsmaster/smstemplatefoldernamegridfetch?maintabinfo=foldername&primaryid=foldernameid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=208',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smstemplatesfoladdgrid1').empty();
			$('#smstemplatesfoladdgrid1').append(data.content);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('smstemplatesfoladdgrid1');
			{//sorting
				$('.folderlistheadercolsort').click(function(){
					$('.folderlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smstemplatesfoladdgrid1(page,rowcount);
				});
				sortordertypereset('smstemplatesheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smstemplatesfoladdgrid1(page,rowcount);
				});
				$('#folderlistpgrowcount').change(function(){
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('.pvpagnumclass').data('pagenum');
					smstemplatesfoladdgrid1(page,rowcount);
				});
			}
			//Material select
			$('#folderlistpgrowcount').material_select();
		},
	});
}
{//crud operation
	function crudactionenable() {
		//Toolbar Icon Change Function View
		$("#addicon").click(function() {
			smstemplatesfoladdgrid1();
			addslideup('smsmastercreationview','smsmastercreationformadd');
			$(".updatebtnclass").addClass('hidedisplay');
			$(".addbtnclass").removeClass('hidedisplay');
			$("#formfields").empty();
			$("#templatetypeid").val('2');
			$("#leadtemplatecontent_editorfilenamedivhid").find('p').css('opacity','1');
			$('#tab2').trigger('click');
			var elementname = $('#elementsname').val();
			elementdefvalueset(elementname);
			//For tablet and mobile Tab section reset
			$('#tabgropdropdown').select2('val','1');
			$("#templatetypeid").select2('val','2');
			$("select#notificationlogtypeid").removeClass('validate[required]');	
			$("#templatesetdefaultdivhid").hide();
			$("#notificationlogtypeiddivhid").hide();
			hideshowfields('2');
			folderdatareload();
			$("#templatetypeid").attr('disabled',false);
			$("#smssendtypeid,#smsgrouptypeid,#newsmssettingsid").attr('disabled',false);
			$("#moduleid,#notificationlogtypeid").attr('disabled',false);
			//fortabtouch = 0;
			//For Touch
			smsmasterfortouch = 0;
			firstfieldfocus();
			Materialize.updateTextFields();
			Operation = 0; //for pagination
		});
		$("#editicon").click(function() {
			var datarowid = $('#smsmasterviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				smstemplatesfoladdgrid1();
				setTimeout(function(){
					$("#templatetypeid").attr('readonly',true);
					$(".addbtnclass").addClass('hidedisplay');
					$(".updatebtnclass").removeClass('hidedisplay');
					$('#tab2').trigger('click');
					$("#smsgrouptypeid,#smssendtypeid").attr('readonly',true);
					$("#smstemplatesfoladdbutton").removeClass('hidedisplayfwg').show();
					editformdatainformationshow(datarowid);
					$("#formfields").empty();
					//For tablet and mobile Tab section reset
					$('#tabgropdropdown').select2('val','1');
					readonlybasedontype();
					var grptype = $('#smsgrouptypeid').select2('val');
					if(grptype == 3){
						$('#moduleid').attr('readonly',true);
					}
					fortabtouch = 1;
					//For Touch
					smsmasterfortouch = 1;
					Materialize.updateTextFields();
					Operation = 1; //for pagination
				},100);
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#deleteicon").click(function() {
			var datarowid = $('#smsmasterviewgrid div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#basedeleteoverlay").fadeIn();
				$('#primarydataid').val(datarowid);
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function() {
			var datarowid = $('#primarydataid').val();
			recorddelete(datarowid);
		});
	}
}
{//refresh grid
	function refreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.pagination-container li .page-text .active').data('rowcount');
		smsmasterviewgrid(page,rowcount);
	}
	function folderrefreshgrid() {
		var page = $('ul#folderlistpgnum li.active').data('pagenum');
		var rowcount = $('ul.folderlistpgnumcnt li .page-text .active').data('rowcount');
		smstemplatesfoladdgrid1(page,rowcount);
	}
}

//new data add submit function
function newdataaddfun() {
	var smssendtype = $("#smssendtypeid").val();
	var typeid = $("#templatetypeid").val();
	var senderid = $("#newsmssettingsid").val();
	if(typeid == 4) {
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var message = $("#leadtemplatecontent_editorfilename").val();
		var neweditordata = '&leadtemplatecontent_editorfilename='+message;
		var senderid = $("#newsmssettingsid").find('option:selected').data('sendername');
		$.ajax({
			url: base_url + "Smsmaster/newdatacreate",
			data: "datas=" + datainformation+"&datass="+neweditordata+"&senderid="+senderid,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#newsmssettingsid').removeClass('validate[required]');
					$('#smsmastercreationformadd').hide();
					$('#smsmastercreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					setTimeout(function() {
						$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
					}, 1000);
				}
			},
		});
	} else {
		if((smssendtype == 2 && senderid != '') || (smssendtype == 2 && typeid == 3) || smssendtype == 3) {
			var formdata = $("#dataaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			var message = $("#leadtemplatecontent_editorfilename").val();
			var neweditordata = '&leadtemplatecontent_editorfilename='+message;
			var senderid = $("#newsmssettingsid").find('option:selected').data('sendername');
			$.ajax({
				url: base_url + "Smsmaster/newdatacreate",
				data: "datas=" + datainformation+"&datass="+neweditordata+"&senderid="+senderid,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$('#newsmssettingsid').removeClass('validate[required]');
						$('#smsmastercreationformadd').hide();
						$('#smsmastercreationview').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						setTimeout(function() {
							$('#foldernamename').addClass('validate[required,maxSize[100],funcCall[foldernamecheck]]');
						}, 1000);
					}
				},
			});
		} else {
			$('#newsmssettingsid').addClass('validate[required]');
		}
	}
}
//old information show in form
function editformdatainformationshow(datarowid) {
	var elementsname = $('#elementsname').val();
	var elementstable = $('#elementstable').val();
	var elementscolmn = $('#elementscolmn').val();
	var elementpartable = $('#elementspartabname').val();
	var resctable = $('#resctable').val();
	$.ajax(	{
		url:base_url+"Smsmaster/fetchformdataeditdetails?dataprimaryid="+datarowid+"&elementsname="+elementsname+"&elementstable="+elementstable+"&elementscolmn="+elementscolmn+"&elementpartable="+elementpartable+"&resctable="+resctable,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data)	{
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				$('#smsmastercreationformadd').hide();
				$('#smsmastercreationview').fadeIn(1000);
				//For Touch
				smsmasterfortouch = 0;
			} else {
				addslideup('smsmastercreationview','smsmastercreationformadd');				
				var txtboxname = elementsname + ',primarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,textboxname,data,dropdowns);
				var smsgrouptypeid = data.smsgrouptypeid;
				var smssendtypeid = data.smssendtypeid;
				var moduleid = data.moduleid;
				var senderid = data.smssettingsid;
				var setdefault = data.templatesetdefault;
				tandceditervalueget(data.leadtemplatecontent_editorfilename);
				setTimeout(function() {
					hideshowfields(data.templatetypeid);
					$("#smssendtypeid").select2('val',smssendtypeid);
					$("#smsgrouptypeid").select2('val',smsgrouptypeid);
					$("#smssendtypeid,#smsgrouptypeid,#newsmssettingsid").attr('disabled',true);
					$("#notificationlogtypeid").attr('disabled',true);
					if(moduleid != '1') {
						$("#moduleid").select2('val',moduleid).trigger('change');
						$("#moduleid").attr('disabled',true);
					}
					$("#smssendtypeid").trigger('change');
					$("#newsmssettingsid").select2('val',senderid);
					$("#templatetypeid").attr('disabled',true);
					if(setdefault == 'Yes') {
						$("#setdefault").val('Yes');
						$("#templatesetdefault").val('Yes');
						$("#templatesetdefaultcboxid").attr('checked', true);
					} else {
						$("#setdefault").val('No');
						$("#templatesetdefault").val('No');
						$("#templatesetdefaultcboxid").attr('checked', false);
					}
				}, 200);
				//For Keyboard Shortcut Variables
				addformupdate = 1;
				viewgridview = 0;
				addformview = 1;
			}
		}
	});
}
//editor value fetch
function tandceditervalueget(filename) {
	$.ajax({
		url: base_url+"Smsmaster/editervaluefetch?filename="+filename,
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			if(data != 'Fail') {
				$("#leadtemplatecontent_editorfilename").val(data);
			} else {
				$("#leadtemplatecontent_editorfilename").val('');
			}
		},
	});
}
//update old information
function dataupdateinformationfun() {
	var smssendtype = $("#smssendtypeid").val();
	var senderid = $("#newsmssettingsid").val();
	if(smssendtype == null || smssendtype == 4 || smssendtype == ''){
		var formdata = $("#dataaddform").serialize();
		var amp = '&';
		var datainformation = amp + formdata;
		var senderid = $("#newsmssettingsid").find('option:selected').data('sendername');
		$.ajax({
			url: base_url + "Smsmaster/datainformationupdate",
			data: "datas=" + datainformation+"&senderid="+senderid,
			type: "POST",
			cache:false,
			success: function(msg) {
				var nmsg = $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					$('#smsmastercreationformadd').hide();
					$('#smsmastercreationview').fadeIn(1000);
					refreshgrid();
					alertpopup(savealert);
					//For Touch
					smsmasterfortouch = 0;
				}
			},
		});
		setTimeout(function() {
			var closetrigger = ["closeaddleadcreation"];
			triggerclose(closetrigger);
		}, 1000);
	} else{
		if((smssendtype == 2 && senderid != '') || smssendtype == 3) {
			var formdata = $("#dataaddform").serialize();
			var amp = '&';
			var datainformation = amp + formdata;
			var senderid = $("#newsmssettingsid").find('option:selected').data('sendername');
			$.ajax({
				url: base_url + "Smsmaster/datainformationupdate",
				data: "datas=" + datainformation+"&senderid="+senderid,
				type: "POST",
				cache:false,
				success: function(msg) {
					var nmsg =  $.trim(msg);
					if (nmsg == 'TRUE') {
						resetFields();
						$('#smsmastercreationformadd').hide();
						$('#smsmastercreationview').fadeIn(1000);
						refreshgrid();
						alertpopup(savealert);
						//For Touch
						smsmasterfortouch = 0;
					}
				},
			});
			setTimeout(function() {
				var closetrigger = ["closeaddleadcreation"];
				triggerclose(closetrigger);
			}, 1000);
		} else {
			$('#newsmssettingsid').addClass('validate[required]');
		}
	}
	
}
//udate old information
function recorddelete(datarowid) {
	var elementstable = $('#elementstable').val();
	var elementspartable = $('#elementspartabname').val();
    $.ajax({
        url: base_url + "Smsmaster/deleteinformationdata?primarydataid="+datarowid+"&elementstable="+elementstable+"&parenttable="+elementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "false")  {
            	refreshgrid();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
{//Related modules drop down load 
	function relatedtodropdownload(id) {
		var modulename=$('#moduleid').find('option:selected').data('moduleidhidden');
		$('#relatedtomoduleid').empty();
		$('#relatedtomoduleid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Smsmaster/relatedmoduleidget',
			data:'moduleid='+id,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$('#relatedtomoduleid').append($("<option></option>").attr("value",id).text(modulename));
					$.each(data, function(index) {
						$('#relatedtomoduleid')
						.append($("<option></option>")
						.attr("data-mergemoduleidhidden",data[index]['datasid'])
						.attr("data-mergemoduletype",data[index]['datatype'])
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
				}
			},
		});
	}
}
{//folder operation
	function smsmasterfolderinsertfun() {
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		$.ajax({
			url:base_url+'Smsmaster/smsmasterfolderinsert',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					resetFields();
					folderrefreshgrid();
					$('#smstemplatesfolupdatebutton').hide();
					$('#smstemplatesfoladdbutton').show();
					$("#smstemplatesfolcancelbutton").trigger('click');
					alertpopup(savealert);
					Materialize.updateTextFields();
				}
			},
		});
	}
	//Edit data Fetch
	function smstemplatefolderdatafetch(datarowid) {
		$("#foldernameeditid").val(datarowid);
		$.ajax({
			url:base_url+'Smsmaster/smstemplatefolderdatafetch?datarowid='+datarowid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var foldername = data.foldername;
					var description = data.description;
					$("#foldernamename").val(foldername);
					$("#description").val(description);
					var setdefault = data.setdefault;
					if(setdefault == 'Yes') {
						$("#setdefaultcboxid").prop('checked',true);
						$("#setdefault").val('Yes');
					} else {
						$("#setdefaultcboxid").prop('checked',false);
						$("#setdefault").val('No');
					}
				}
			},
		});
	}
	//SMS templates folder add function
	function smstemplatesfolupdatefun() {
		var foldernameid = $("#foldernameeditid").val();
		var foldername = $("#foldernamename").val();
		var description = $("#description").val();
		var setdefault = $("#setdefault").val();
		$.ajax({
			url:base_url+'Smsmaster/smsmplatefolderupdate',
			data:'foldername='+foldername+'&description='+description+'&setdefault='+setdefault+"&foldernameid="+foldernameid,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					folderdatareload();
					$('#smstemplatesfolupdatebutton').hide();
					$("#smstemplatesfoladdbutton,#smstemplatesfolingriddel1").show();
					$("#smstemplatesfolcancelbutton").trigger('click');
					alertpopup(savealert);
					//For Touch
					smsmasterfortouch = 2;
				}
			},
		});
	}
	//delete folder
	function deletefolderoperation(id) {
		$.ajax({
			url:base_url+'Smsmaster/folderdeleteoperation?id='+id,
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE') {
					folderrefreshgrid();
					$('#smstemplatesfoladdgrid1').attr('disabled',false);
					folderdatareload();
					alertpopup("Deleted successfully");
				} else if(nmsg == 'DEFAULT') {
					alertpopup("Can't delete default records.");
					folderrefreshgrid();
					$('#solutionsfolingriddel1').attr('disabled',false);
				}
			},
		});
	}
	//Folder Drop Down load
	function folderdatareload() {
		$('#smsfoldernameid').empty();
		$('#smsfoldernameid').append($("<option></option>"));
		$.ajax({
			url:base_url+'Smsmaster/fetchdddataviewddval?dataname=foldernamename&dataid=foldernameid&datatab=foldername&moduleid=208',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$('#smsfoldernameid')
						.append($("<option></option>")
						.attr("value",data[index]['datasid'])
						.text(data[index]['dataname']));
					});
					$.each(data, function(index) {
						if(data[index]['setdefault'] == 'Yes'){
							$('#smsfoldernameid').select2('val',data[index]['datasid']).trigger('change');
						}
					});
				}
				$('#smsfoldernameid').trigger('change');
			},
		});
	}
}
//sender id value fetch based on the settings name
function senderidddvalfetch(smssendtype) {
	$('#newsmssettingsid').empty();
	$('#newsmssettingsid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Smsmaster/smsssenderddreload?smssendtype="+smssendtype,
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				var newddappend="";
				var newlength=data.length;
				for(var m=0;m < newlength;m++) {
					newddappend += "<option data-apikey ='" +data[m]['apikey']+ "' data-sendername ='" +data[m]['dataname']+ "' value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
				}
				//after run
				$('#newsmssettingsid').append(newddappend);
			}
			$('#newsmssettingsid').trigger('change');
		},
	});
}
function foldernamecheck() {
	var primaryid = $("#foldernameeditid").val();
	var accname = $("#foldernamename").val();
	var elementpartable = 'foldername';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Smsmaster/folderuniquenamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Folder name already exists!";
				}
			}else {
				return "Folder name already exists!";
			}
		} 
	}
}
//SMS Template name check
function smstemplatenamecheck() {
	var templatetypeid = $('#templatetypeid').val();
	var primaryid = $("#primarydataid").val();
	var accname = $("#leadtemplatename").val();
	var elementpartable = $('#elementspartabname').val();
	var templatesname = $('#templatesname').val();
	var nmsg = "";
	if( templatesname !="" ) {
		$.ajax({
			url:base_url+"Smsmaster/templatesnameunique",
			data:"data=&templatesname="+templatesname+"&templatetypeid="+templatetypeid+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "SMS Template already exists!";
				}
			}else {
				return "SMS Template already exists!";
			}
		} 
	}
}

function hideshowfields(ttid){
	if(ttid == 3) {
		$("#smsgrouptypeid").removeClass('validate[required]');
		$("#newsmssettingsid").removeClass('validate[required]');
		$("#notificationlogtypeid").removeClass('validate[required]');
		$("#notificationlogtypeiddivhid").hide();
		$("#newsmssettingsiddivhid").hide();
		$("#smsgrouptypeiddivhid").hide();
		$("#templatesetdefaultdivhid").hide();
		$("#notificationlogtypeiddivhid").hide();
		$("#smssendtypeid").addClass('validate[required]');
		$("#smssendtypeiddivhid").show();
		$("#moduleid,#smssendtypeid").attr('readonly',false);
		$('#moduleid,#smssendtypeid').select2('val','');
	} else if(ttid == 2){
		$("#notificationlogtypeid").removeClass('validate[required]');
		$("#newsmssettingsiddivhid").show();
		$("#smsgrouptypeiddivhid").show();
		$("#templatesetdefaultdivhid").hide();
		$("#notificationlogtypeiddivhid").hide();
		$("#smssendtypeiddivhid").show();
		$("#smssendtypeid").addClass('validate[required]');
		$("#smsgrouptypeid").addClass('validate[required]');
		$("#newsmssettingsid").addClass('validate[required]');
		$("#smsgrouptypeid").attr('readonly',false);
		$("#smsgrouptypeid,#moduleid").select2('val','');
		$("#moduleid,#smssendtypeid,#newsmssettingsid,#templatetypeid").attr('readonly',false);
	} else if(ttid == 4){
		$("#smsgrouptypeid").removeClass('validate[required]');
		$("#newsmssettingsid").removeClass('validate[required]');
		$("#newsmssettingsiddivhid,#newsmssettingsid").removeClass('validate[required]');
		$("#smssendtypeid").removeClass('validate[required]');
		$("#smssendtypeiddivhid").hide();
		$("#newsmssettingsiddivhid").hide();
		$("#smsgrouptypeiddivhid").hide();
		$("#templatesetdefaultdivhid").show();
		$("#notificationlogtypeiddivhid").show();
		$("#notificationlogtypeid").addClass('validate[required]');
		$("#moduleid").attr('readonly',false);
		$('#moduleid,#notificationlogtypeid').select2('val','');
	}
}

//merge modulebased form fields load
function loadmodulefield(moduleid,relatedmoduleid){ 
	$('#formfields').empty();
	$('#formfields').append($("<option></option>"));
	$.ajax({
			url: base_url+"Smsmaster/modulefieldload",
			data:{moduleid:moduleid,relatedmoduleid:relatedmoduleid},
			dataType:'json',
			type: "POST",
			async:false,
			success: function(data) {	
				dropdownid='formfields';
				if((data.fail) == 'FAILED') {
				} else {
					$.each(data, function(index) {
						$("#"+dropdownid+"")
						.append($("<option></option>")
						.attr("data-key",data[index]['key'])
						.attr("value",data[index]['pid'])
						.attr("data-id",data[index]['pid'])
						.attr("data-colname",data[index]['label'])
						.text(data[index]['label']));
					});
				}	
			},
		});
	//}
}

function readonlybasedontype(){
	var type = $("#smsgrouptypeid").val();
	$("#relatedtomoduleid,#formfields").select2('val','');
	$("#mergetext").val('');
	if(type == 2) {
		$("#moduleid").select2('val',271);
		$("#moduleid").trigger('change');
		$("#moduleid").attr('readonly',true);
		$("#mergetext").attr('readonly',true);
		$("#relatedtomoduleid, #formfields").attr('readonly',false);
	} else if(type == 3) {
		var mid = $("#moduleid").val();
		if(mid == '271'){
			$("#moduleid").select2('val','');
			$("#moduleid").attr('readonly',false);
			$("#relatedtomoduleid, #formfields, #mergetext").attr('readonly',false);
		} else {
			$("#moduleid").attr('readonly',false);
			$("#relatedtomoduleid, #formfields").attr('readonly',false);
			$("#mergetext").attr('readonly',true);
		}
	} else if(type == 4) {
		$("#newsmssettingsid").attr('readonly',false);
		$("#moduleid").select2('val','');
		$("#moduleid, #relatedtomoduleid, #formfields, #mergetext").attr('readonly',true);
	}
}
//notification type get for notification template
function notificationtypeget(id) {
	$('#notificationlogtypeid').empty();
	$.ajax({
		url:base_url+'Smsmaster/notificationtypeget',
		data:'moduleid='+id,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#notificationlogtypeid')
					.append($("<option></option>")
					.attr("data-typename",data[index]['dataname'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}	
			$('#notificationlogtypeid').trigger('change');
		},
	});
}