$(document).ready(function() {
	{//Foundation Initialization
		$(document).foundation();
	}
	{// Maindiv height width change
		maindivwidth();
	}
	{//disable credits
		$("#availablecredits").attr('readonly', 'readonly');
	}
	{//keyboard shortcut reset global variable
		viewgridview = 0;
		innergridview = 1;
	}
	$( window ).resize(function() {
		sectionpanelheight('smssettingscreoverlay');
		sectionpanelheight('smssettingssenoverlay');
		sectionpanelheight('smssettingskeyoverlay');
	});
	$('#dataaddsbtn,#dataupdatesubbtn').hide();
	{//inner-form-with-grid
		$("#smssettingscreingridadd1").click(function(){
			resetFields();
			sectionpanelheight('smssettingscreoverlay');
			$("#smssettingscreoverlay").removeClass("closed");
			$("#smssettingscreoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#smssettingscrecancelbutton").click(function(){
			$("#smssettingscreoverlay").removeClass("effectbox");
			$("#smssettingscreoverlay").addClass("closed");
		});
		$("#smssettingsseningridadd2").click(function(){
			resetFields();
			sectionpanelheight('smssettingssenoverlay');
			$("#smssettingssenoverlay").removeClass("closed");
			$("#smssettingssenoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#smssettingssencancelbutton").click(function(){
			$("#smssettingssenoverlay").removeClass("effectbox");
			$("#smssettingssenoverlay").addClass("closed");
		});
		$("#smssettingskeyingridadd3").click(function(){
			resetFields();
			sectionpanelheight('smssettingskeyoverlay');
			$("#smssettingskeyoverlay").removeClass("closed");
			$("#smssettingskeyoverlay").addClass("effectbox");
			Materialize.updateTextFields();
			firstfieldfocus();
		});
		$("#smssettingskeycancelbutton").click(function(){
			$("#smssettingskeyoverlay").removeClass("effectbox");
			$("#smssettingskeyoverlay").addClass("closed");
		});
	}
	{//button show hide
		$("#smssettingscreingrideditspan1").show();
		$("#smssettingscreingridreloadspan1, #smssettingsseningridreloadspan2,#smssettingskeyingridreloadspan3").hide();
		$("#smssettingscreingrideditspan1,#smssettingsseningrideditspan2,#smssettingskeyingrideditspan3").hide();
	}
	{//Virtual Type hide
		$("#virtualmobilenumbertypeiddivhid,#virtualmobilenumberdivhid,#longnumbertypeiddivhid").hide();
		setTimeout(function() {
			$("#smsservicetypeid,#virtualmobilenumbertypeid").trigger('change');
		},100);
		$("#smsservicetypeid").change(function() {
			var value = $("#smsservicetypeid").val();
			if(value == '2'){
				$("#virtualmobilenumbertypeiddivhid,#virtualmobilenumberdivhid,#longnumbertypeiddivhid").hide();
				$("#virtualmobilenumbertypeid").select2('val','');
				$("#virtualmobilenumber").val('');
				$("#credentialsmssendtypeiddivhid").show();
				$("#virtualmobilenumber").removeClass('validate[required,custom[phone]]');
				$("#virtualmobilenumbertypeid").removeClass('validate[required]');
				$("#credentialsmssendtypeid").addClass('validate[required]');
			} else if(value == '3') {
				$("#virtualmobilenumbertypeiddivhid,#virtualmobilenumberdivhid").show();
				$("#credentialsmssendtypeiddivhid").hide();
				$("#credentialsmssendtypeid").select2('val','');
				$("#credentialsmssendtypeid").removeClass('validate[required]');
				$("#virtualmobilenumber").removeClass('validate[required,custom[phone]]');
				$("#virtualmobilenumber").addClass('validate[required,custom[phone]]');
				$("#virtualmobilenumbertypeid").addClass('validate[required]');
			}
		});
		$("#virtualmobilenumbertypeid").change(function() {
			var type = $("#virtualmobilenumbertypeid").val();
			if(type == '2'){
				$("#longnumbertypeiddivhid").hide();
				$("#longnumbertypeid").select2('val','');
				$("#longnumbertypeid").val('');
				$("#longnumbertypeid").removeClass('validate[required]');
				$("#virtualmobilenumber").val('56767');
			} else if(type == '3') {
				$("#longnumbertypeid").addClass('validate[required]');
				$("#longnumbertypeiddivhid").show();
				$("#virtualmobilenumber").val('');
			}
			Materialize.updateTextFields();
		});
	}
	{//Grid Calling
		smssettingscreaddgrid1();
		smssettingssenaddgrid2();
		smssettingskeyaddgrid3();
		firstfieldfocus();
	}
	{// tab Group Changes
		$('#tab1 span').removeAttr('style');
		$('#tab1').click(function(){
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).find('span').removeAttr('style');
			$("#subformspan1").show();
			$("#subformspan2,#subformspan3,#subformspan4").hide();
			$("#smssettingscreingriddel1,#smssettingscreaddbutton").show();
			$("#smssettingscreupdatebutton").hide();
			//For Touch
			masterfortouch = 0;
		});
		$('#tab2').click(function() {
			var smstype = '2';
			smssettingsnameddreload('smsprovidersettingsid',smstype);
			// To View the Content Area 
			$('#gview_smssettingssenaddgrid2').css('opacity','0');
			setTimeout(function() {
				smssettingssenaddgrid2();
			}, 20);
			$('#senderid').addClass('validate[minSize[6],custom[onlyLetterNSp]]');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).find('span').removeAttr('style');
			$("#subformspan2").show();
			$("#subformspan1,#subformspan3,#subformspan4").hide();
			$("#smssettingssenaddbutton").show();
			$("#smssettingssenupdatebutton").hide();
			resetFields();
			//For Touch
			masterfortouch = 0;
		});
		$('#tab3').click(function(){
			var smstype = 3;
			var settnameid = '';
			smssettingsnameddreload('keywordsmsprovidersettingsid',smstype);
			senderidddvalfetch(settnameid);
			// To View the Content Area 
			$('#gview_smssettingskeyaddgrid3').css('opacity','0');
			setTimeout(function() {
				smssettingskeyaddgrid3();
			}, 20); 
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).find('span').removeAttr('style');
			$("#subformspan3").show();
			$("#subformspan1,#subformspan2,#subformspan4").hide();
			$("#smssettingskeyaddbutton,#smssettingskeyingriddel3").show();
			$("#smssettingskeyupdatebutton").hide();
			resetFields();
			//For Touch
			masterfortouch = 0;
		});
		$('#tab4').click(function(){
			var smstype = 2;
			var setnameid = '';
			// To View the Content Area 
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).find('span').removeAttr('style');
			$("#subformspan4").show();
			$("#subformspan1,#subformspan2,#subformspan3").hide();
			resetFields();
			//For Touch
			masterfortouch = 0;
		});
	}
	{//sms credential operation
		//sms credentialedit operation
		$("#smssettingscreingridedit1").click(function() {
			var ids = $('#smssettingscreaddgrid1 div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#smssettingscreaddbutton,#smssettingscreingriddel1").hide();
				$("#smssettingscreupdatebutton").show();
				$("#smsservicetypeid").attr('readonly',true);
				smssettingsdatafetch(datarowid);
				//for tab touch
				//For Touch
				masterfortouch = 1;
				//keyboard shortcut
				saveformview=1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms settings delete 
		$("#smssettingscreingriddel1").click(function(){
			var ids = $('#smssettingscreaddgrid1 div.gridcontent div.active').attr('id');	
			if(ids) {		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
			var ids = $('#smssettingscreaddgrid1 div.gridcontent div.active').attr('id');	
			if(ids){
				smssettingsdelete(ids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms reload operation
		$("#smssettingscreingridreload1").click(function(){
			resetFields();
			smscredentiallisrefreshgrid();
			$("#smssettingscreupdatebutton").hide();
			$("#smsservicetypeid").attr('readonly',false);
			$("#smssettingscreaddbutton").show();
			$("#smssettingscreingriddel1").show();
		});
		//sms Credential add
		$("#smssettingscreaddbutton").click(function(){
			$("#smssettingscreaddgrid1validation").validationEngine('validate');
			//For Touch
			masterfortouch = 0;
		});
		jQuery("#smssettingscreaddgrid1validation").validationEngine({
			onSuccess: function() {
				smscredentialadd();
			},
			onFailure: function() {
				var dropdownid =['1','providerid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//sms credential update
		$("#smssettingscreupdatebutton").click(function(){
			$("#smssettingscreeditgrid1validation").validationEngine('validate');
		});
		jQuery("#smssettingscreeditgrid1validation").validationEngine({
			onSuccess: function() {
				smscredentialupdate();
			},
			onFailure: function() {
				var dropdownid =['1','providerid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//sms sender id operation
		//sms senderid edit operation
		$("#smssettingsseningridedit2").click(function() {
			var datarowid = $('#smssettingssenaddgrid2 div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#smssettingssenaddbutton,#smssettingsseningriddel2").hide();
				$("#smssettingssenupdatebutton").show();
				smssenderiddatafetch(datarowid);
				//For Touch
				masterfortouch = 1;
				//keyboard shortcut
				saveformview=1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms senderid delete 
		$("#smssettingsseningriddel2").click(function(){
			var ids = $('#smssettingssenaddgrid2 div.gridcontent div.active').attr('id');
			if(ids) {		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
			var ids = $('#smssettingssenaddgrid2 div.gridcontent div.active').attr('id');
			if(ids){
				smssenderiddelete(ids);
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms senderid reload operation
		$("#smssettingsseningridreload2").click(function(){
			resetFields();
			smssettingssensrefreshgrid();
			$("#smssettingssenaddbutton").show();
			$("#smssettingssenupdatebutton").hide();
		});
		//sms senderid add
		$("#smssettingssenaddbutton").click(function() {
			$("#smssettingssenaddgrid2validation").validationEngine('validate');
			//For Touch
			masterfortouch = 0;
			//keyboard shortcut
			saveformview=0;
		});
		jQuery("#smssettingssenaddgrid2validation").validationEngine({
			onSuccess: function() {
				smssenderidadd();
			},
			onFailure: function() {
				var dropdownid =['1','smsprovidersettingsid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//sms senderid update
		$("#smssettingssenupdatebutton").click(function() {
			$("#smssettingsseneditgrid2validation").validationEngine('validate');
		});
		jQuery("#smssettingsseneditgrid2validation").validationEngine({
			onSuccess: function() {
				smssenderidupdate();
			},
			onFailure: function() {
				var dropdownid =['1','smsprovidersettingsid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//SMS Keyword Tab
		$("#smssettingskeyaddbutton").click(function() {
			$("#smssettingskeyaddgrid3validation").validationEngine('validate');
		});
		jQuery("#smssettingskeyaddgrid3validation").validationEngine({
			onSuccess: function() {
				smskeywordsubmit();
			},
			onFailure: function() {
				var dropdownid =['1','keywordsmsprovidersettingsid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//edit operation 
		$("#smssettingskeyingridedit3").click(function() {
			var datarowid = $('#smssettingskeyaddgrid3 div.gridcontent div.active').attr('id');
			if(datarowid) {
				$("#smssettingskeyaddbutton").hide();
				$("#smssettingskeyupdatebutton").show();
				$("#smssettingskeyingriddel3").hide();
				smskeyworddatafetch(datarowid);
				//For Touch
				masterfortouch = 1;
				//keyboard shortcut
				saveformview=1;
			} else {
				alertpopup("Please select a row");
			}
		});
		//sms keyword update
		$("#smssettingskeyupdatebutton").click(function() {
			$("#smssettingskeyeditgrid3validation").validationEngine('validate');
		});
		jQuery("#smssettingskeyeditgrid3validation").validationEngine({
			onSuccess: function() {
				smskeywordupdate();
			},
			onFailure: function() {
				var dropdownid =['1','keywordsmsprovidersettingsid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		//sms keyword refresh
		$("#smssettingskeyingridreload3").click(function() {
			resetFields();
			smssettingsefreshgrid();
			$("#smssettingskeyaddbutton").show();
			$("#smssettingskeyingriddel3").show();
			$("#smssettingskeyupdatebutton").hide();
		});
		//sms keyword delete
		$("#smssettingskeyingriddel3").click(function() {
			var ids = $('#smssettingskeyaddgrid3 div.gridcontent div.active').attr('id');
			if(ids) {		
				$("#basedeleteoverlay").fadeIn();
				$("#basedeleteyes").focus();
			} else {
				alertpopup("Please select a row");
			}
		});
		$("#basedeleteyes").click(function(){
			var ids = $('#smssettingskeyaddgrid3 div.gridcontent div.active').attr('id');
			if(ids){
				smskeyworddelete(ids);
			} else {
				alertpopup("Please select a row");
			}
		});
	}
	{//sms tab
		{//sms Sender default value set
			$("#smssendersavebutton").click(function() {
				$("#smssettingsaddvalidate").validationEngine('validate');	
				//For Touch
				generalfortouch = 2;	
			});
			jQuery("#smssettingsaddvalidate").validationEngine({
				onSuccess: function() {
					smssettingdefaultsenderidset();
				},
				onFailure: function() {	
					var dropdownid =['2','settingsnameid','defsenderid'];
					dropdownfailureerror(dropdownid);
					alertpopup(validationalert);
				}
			});
		}	
		{//Available credits check
			$("#smssendtypeid").change(function() {
				setTimeout(function() {
					var typeid = $("#smssendtypeid").val();
					if(typeid == '2') {
						var addontype = '2';
					} else if(typeid == '3') {
						var addontype = '4';
					} else if(typeid == '4') {
						var addontype = '5';
					} 
					creditsdetailfetch(addontype);
				},10);
			});
		}
	}
	{// Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	$('#closeaddform').click(function(){
		window.location =base_url+'Sms';
	});
});
//Criteria (condition) Add Grid
function smssettingscreaddgrid1(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smssettingscreaddgrid1').width();
	var wheight = $('#smssettingscreaddgrid1').height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.smscredentiallistheadercolsort').hasClass('datasort') ? $('.smscredentiallistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.smscredentiallistheadercolsort').hasClass('datasort') ? $('.smscredentiallistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.smscredentiallistheadercolsort').hasClass('datasort') ? $('.smscredentiallistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smssettings/smscredentialgriddatafetch?maintabinfo=smsprovidersettings&primaryid=smsprovidersettingsid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=269',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smssettingscreaddgrid1').empty();
			$('#smssettingscreaddgrid1').append(data.content);
			$('#smssettingscreaddgrid1footer').empty();
			$('#smssettingscreaddgrid1footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('smssettingscreaddgrid1');
			{//sorting
				$('.smscredentiallistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.smscredentiallistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.smscredentiallistheadercolsort').hasClass('datasort') ? $('.smscredentiallistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.smscredentiallistheadercolsort').hasClass('datasort') ? $('.smscredentiallistheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					smssettingscreaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('smscredentiallistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smssettingscreaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#smscredentiallistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					smssettingscreaddgrid1(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#smscredentiallistpgrowcount').material_select();
			var hideprodgridcol = ['username','password','apikey','statusname'];
			gridfieldhide('smssettingscreaddgrid1',hideprodgridcol);
		}
	});
}
{//refresh grid
	function smscredentiallisrefreshgrid() {
		var page = $('ul#smscredentiallistpgnum li.active').data('pagenum');
		var rowcount = $('ul#smscredentiallistpgnumcnt li .page-text .active').data('rowcount');
		smssettingscreaddgrid1(page,rowcount);
	}
	function smssettingssensrefreshgrid() {
		var page = $('ul#smssenderidlistpgnum li.active').data('pagenum');
		var rowcount = $('ul#smssenderidlistpgnumcnt li .page-text .active').data('rowcount');
		smssettingssenaddgrid2(page,rowcount);
	}
	function smssettingsefreshgrid() {
		var page = $('ul#smskeywordslistpgnum li.active').data('pagenum');
		var rowcount = $('ul#smssenderidlistpgnumcnt li .page-text .active').data('rowcount');
		smssettingskeyaddgrid3(page,rowcount);
	}
}
//action grid
function smssettingssenaddgrid2(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smssettingssenaddgrid2').width();
	var wheight = $('#smssettingssenaddgrid2').height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.smssenderidlistheadercolsort').hasClass('datasort') ? $('.smssenderidlistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.smssenderidlistheadercolsort').hasClass('datasort') ? $('.smssenderidlistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.smssenderidlistheadercolsort').hasClass('datasort') ? $('.smssenderidlistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smssettings/smssenderidgriddatafetch?maintabinfo=smssettings&primaryid=smssettingsid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=269',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smssettingssenaddgrid2').empty();
			$('#smssettingssenaddgrid2').append(data.content);
			$('#smssettingssenaddgrid2footer').empty();
			$('#smssettingssenaddgrid2footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('smssettingssenaddgrid2');
			{//sorting
				$('.smssenderidlistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.smssenderidlistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.smssenderidlistheadercolsort').hasClass('datasort') ? $('.smssenderidlistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.smssenderidlistheadercolsort').hasClass('datasort') ? $('.smssenderidlistheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					smssettingssenaddgrid2(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('smssenderidlistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smssettingssenaddgrid2(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#smssenderidlistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					smssettingssenaddgrid2(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#smssenderidlistpgrowcount').material_select();
			var hideprodgridcol = ['apikey','statusname'];
			gridfieldhide('smssettingssenaddgrid2',hideprodgridcol);
		}
	});
}
//action grid
function smssettingskeyaddgrid3(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 100 : rowcount;
	var wwidth = $('#smssettingskeyaddgrid3').width();
	var wheight = $('#smssettingskeyaddgrid3').height();
	//col sort
	var sortcol = $("#sortcolumn").val();
	var sortord = $("#sortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.smskeywordslistheadercolsort').hasClass('datasort') ? $('.smskeywordslistheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.smskeywordslistheadercolsort').hasClass('datasort') ? $('.smskeywordslistheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.smskeywordslistheadercolsort').hasClass('datasort') ? $('.smskeywordslistheadercolsort.datasort').attr('id') : '0';
	$.ajax({
		url:base_url+"Smssettings/smskeywordgriddatafetch?maintabinfo=smskeywords&primaryid=smskeywordsid&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&moduleid=269',
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success:function(data) {
			$('#smssettingskeyaddgrid3').empty();
			$('#smssettingskeyaddgrid3').append(data.content);
			$('#smssettingskeyaddgrid3footer').empty();
			$('#smssettingskeyaddgrid3footer').append(data.footer);
			//data row select event
			datarowselectevt();
			/*column resize*/
			columnresize('smssettingskeyaddgrid3');
			{//sorting
				$('.smskeywordslistheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.smskeywordslistheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $('.pvpagnumclass').data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					var sortcol = $('.smskeywordslistheadercolsort').hasClass('datasort') ? $('.smskeywordslistheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.smskeywordslistheadercolsort').hasClass('datasort') ? $('.smskeywordslistheadercolsort.datasort').data('sortorder') : '';
					$("#sortorder").val(sortord);
					$("#sortcolumn").val(sortcol);
					smssettingskeyaddgrid3(page,rowcount);
					$("#processoverlay").hide();
				});
				sortordertypereset('smskeywordslistheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					smssettingskeyaddgrid3(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#smskeywordslistpgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					smssettingskeyaddgrid3(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			//Material select
			$('#smskeywordslistpgrowcount').material_select();
			var hideprodgridcol = ['statusname'];
			gridfieldhide('smssettingssenaddgrid2',hideprodgridcol);
		}
	});
}
//for sms credential add
function smscredentialadd() {
	var provider= $("#providerid").val();
	var settingsname= $("#settingsname").val();
	var username= $("#username").val();
	var password= $("#password").val();
	var smstype= $("#smsservicetypeid").val();
	var tranactionmode= $("#credentialsmssendtypeid").val();
	var apikey= $("#apikey").val();
	var virtualmobtype= $("#virtualmobilenumbertypeid").val();
	var virtualmobile= $("#virtualmobilenumber").val();
	var longnumbertypeid= $("#longnumbertypeid").val();
	var vmntype = $("#virtualmobilenumbertypeid").find('option:selected').data('virtualmobilenumbertypeidhidden');
	var longnumype = $("#longnumbertypeid").find('option:selected').data('longnumbertypeidhidden');
	$.ajax({
		url: base_url + "Smssettings/smscredentialadd",
		data:'provider='+provider+"&settingsname="+settingsname+"&username="+username+"&password="+password+"&smstype="+smstype+"&virtualmobtype="+virtualmobtype+"&virtualmobile="+virtualmobile+"&longnumbertypeid="+longnumbertypeid+"&vmntype="+vmntype+"&longnumype="+longnumype+"&tranactionmode="+tranactionmode+"&apikey="+apikey,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	$("#smssettingscreoverlay").removeClass("effectbox");
    			$("#smssettingscreoverlay").addClass("closed");
				smscredentiallisrefreshgrid();
				resetFields();
				alertpopup('Data Stored Successfully');
			}
		},
	});
}
//for sms credential edit data fetch
function smssettingsdatafetch(datarowid) {
	$("#smssettingscreupdatebutton").removeClass('hidedisplayfwg');
	$.ajax({
		url: base_url + "Smssettings/smscredentialeditdatafetch",
		data:'datarowid='+datarowid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
        success: function(data) {
			var sname = data['settname'];
			var provider = data.provider;
			var user = data.user;
			var pass = data.pass;
			var smsservicetype = data.smsservicetype;
			var vmtype = data.vmtype;
			var virtualmobilenumber = data.virtualmobilenumber;
			$("#settingsname").val(sname);
			$("#providerid").select2('val',provider);
			$("#username").val(user);
			$("#password").val(pass);
			$("#smsservicetypeid").select2('val',smsservicetype);
			$("#smsservicetypeid").trigger('change');
			$("#virtualmobilenumbertypeid").select2('val',vmtype);
			$("#virtualmobilenumber").val(virtualmobilenumber);
			$("#credentialprimaryid").val(datarowid);
		},
	});
}
//sms credential update
function smscredentialupdate() {
	var primaryid = $("#credentialprimaryid").val();
	var provider= $("#providerid").val();
	var settingsname= $("#settingsname").val();
	var username= $("#username").val();
	var password= $("#password").val();
	var smstype= $("#smsservicetypeid").val();
	var virtualmobtype= $("#virtualmobilenumbertypeid").val();
	var virtualmobile= $("#virtualmobilenumber").val();
	$.ajax({
		url: base_url + "Smssettings/smscredentialupdate",
		data:'provider='+provider+"&settingsname="+settingsname+"&username="+username+"&password="+password+"&primaryid="+primaryid+"&smstype="+smstype+"&virtualmobtype="+virtualmobtype+"&virtualmobile="+virtualmobile,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
				$("#smssettingscreupdatebutton").hide();
				$("#smssettingscreaddbutton").show();
				$("#smssettingscreingriddel1").show();
				smscredentiallisrefreshgrid();
				$("#virtualmobilenumbertypeiddivhid,#virtualmobilenumberdivhid,#longnumbertypeiddivhid").hide();
				$("#smsservicetypeid").attr('readonly',false);
				resetFields();
				alertpopup('Data Stored Successfully');
				//For Touch
				masterfortouch = 0;
				//keyboar shortcut
				saveformview=0;
			}
		},
	});
}
//sms settings delete
function smssettingsdelete(ids) {
	$.ajax({
		url: base_url + "Smssettings/smscredentialdelete",
		data:"primaryid="+ids,
		type:'POST',
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
            	smscredentiallisrefreshgrid();
        		resetFields();
				$("#basedeleteoverlay").fadeOut();
				alertpopup('Record Deleted Successfully');
			}
		},
	});
}
{//sms sender id operation
	function smssenderidadd() {
		var settingsid= $("#smsprovidersettingsid").val();
		var senderid= $("#senderid").val();
		var setdefault= $("#setdefault").val();
		var filename = $('#filename').val();
		var filesize = $('#filename_size').val();
		var filetype = $('#filename_type').val();
		var filepath = $('#filename_path').val();
		$.ajax({
			url: base_url + "Smssettings/smssenderidadd",
			data:'settingsid='+settingsid+"&senderid="+senderid+"&setdefault="+setdefault+"&filename="+filename+"&filesize="+filesize+"&filetype="+filetype+"&filepath="+filepath,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					$("#smssettingssenoverlay").removeClass("effectbox");
					$("#smssettingssenoverlay").addClass("closed");
					smssettingssensrefreshgrid();
					$('#filenameattachdisplay').empty();
					filename = [];
					filesize = [];
					filetype = [];
					filepath = [];
					$('#filename').val('');
					$('#flesize').val('');
					$('#filetype').val('');
					$('#filepath').val('');
					resetFields();
					alertpopup('Data Stored Successfully');
				}
			},
		}); 
	}
	//sms senderid data fetch
	function smssenderiddatafetch(datarowid) {
		$("#smssettingssenupdatebutton").removeClass('hidedisplayfwg');
		$.ajax({
			url: base_url + "Smssettings/smssenderiddatafetch",
			data:'datarowid='+datarowid,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var prosettingsid = data['prosettingsid'];
				var apikey = data.apikey;
				var senderid = data.senderid;
				var setdefault = data.setdefault;
				$("#senderid").val(senderid);
				$("#smsprovidersettingsid").select2('val',prosettingsid);
				$("#apikey").val(apikey);
				$("#senderprimaryid").val(datarowid);
				if(setdefault == 'Yes') {
					$('.checkboxcls').attr('checked', true);
					$("#setdefault").val(setdefault);
				} else {
					$('.checkboxcls').attr('checked', false);
					$("#setdefault").val(setdefault);
				}
			},
		});
	}
	//sms sender id update
	function smssenderidupdate() {
		var primaryid= $("#senderprimaryid").val();
		var settingsid= $("#smsprovidersettingsid").val();
		var senderid= $("#senderid").val();
		var apikey= $("#apikey").val();
		var setdefault= $("#setdefault").val();
		$.ajax({
			url: base_url + "Smssettings/smssenderidupdate",
			data:'settingsid='+settingsid+"&senderid="+senderid+"&apikey="+apikey+"&primaryid="+primaryid+"&setdefault="+setdefault,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					$("#smssettingssenupdatebutton").hide();
					$("#smssettingssenaddbutton").show();
					smssettingssensrefreshgrid();
					resetFields();
					alertpopup('Data Stored Successfully');
					//For Touch
					masterfortouch = 0;
					//keyboard shortcut
					saveformview = 0;
				}
			},
		});
	}
	//sms sender id delete
	function smssenderiddelete(ids) {
		$.ajax({
			url: base_url + "Smssettings/smssenderiddelete",
			data:"primaryid="+ids,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					smssettingssensrefreshgrid();
					resetFields();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Record Deleted Successfully');
				}
			},
		});
	}
}
{//sms settings name drop down reload
	function smssettingsnameddreload(ddname,smstype) {
		$('#'+ddname+'').empty();
		$('#'+ddname+'').append($("<option></option>"));
		$.ajax({
			url: base_url+"Smssettings/smsstttingsnameddreload?smstype="+smstype,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-keywordsmsprovidersettingsidhidden ='" +data[m]['dataname']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#'+ddname+'').append(newddappend);
				}	
				$('#'+ddname+'').trigger('change');
			},
		});
	}
}
{//SMS Keyword Tab
	//sms keyword submit
	function smskeywordsubmit() {
		var settingsid = $("#keywordsmsprovidersettingsid").val();
		var keywordname = $("#smskeywordsname").val();
		var description = $("#description").val();
		var autoreplymessage = $("#autoreplymessage").val();
		var forwardto = $("#forwardto").val();
		var forwardemail = $("#forwardemail").val();
		var keywordvalidity = $("#keywordvalidity").val();
		var smssettingsid = $("#smssettingsid").val();
		var autoreplyonexpiry = $("#autoreplyonexpiry").val();
		$.ajax({
			url: base_url + "Smssettings/smskeywordadd",
			data:'settingsid='+settingsid+"&keywordname="+keywordname+"&description="+description+"&autoreplymessage="+autoreplymessage+"&forwardto="+forwardto+"&forwardemail="+forwardemail+"&keywordvalidity="+keywordvalidity+"&smssettingsid="+smssettingsid+"&autoreplyonexpiry="+autoreplyonexpiry,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					$("#smssettingskeyoverlay").removeClass("effectbox");
					$("#smssettingskeyoverlay").addClass("closed");
					smssettingsefreshgrid();
					resetFields();
					alertpopup('Data Stored Successfully');
					//For Touch
					masterfortouch = 0;
					//keyboard shortcut
					saveformview = 0;
				}
			},
		});
	}
	//edit operation data fetch
	function smskeyworddatafetch(datarowid) {
		$("#smssettingskeyupdatebutton").removeClass('hidedisplayfwg');
		$.ajax({
			url: base_url + "Smssettings/smskeyworddatafetch",
			data:'datarowid='+datarowid,
			type:'POST',
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				var keywordname = data['keywordname'];
				var settingsid = data.settingsid;
				var description = data.description;
				var autoreplymessage = data.autoreplymessage;
				var forwardto = data.forwardto;
				var forwardemail = data.forwardemail;
				var keywordvalidity = data.keywordvalidity;
				var smssettingsid = data.smssettingsid;
				var autoreplyonexpiry = data.autoreplyonexpiry;
				$("#smskeywordsname").val(keywordname);
				$("#keywordsmsprovidersettingsid").select2('val',settingsid).trigger('change');
				$("#smssettingsid").select2('val',smssettingsid);
				$("#description").val(description);
				$("#autoreplymessage").val(autoreplymessage);
				$("#forwardto").val(forwardto);
				$("#forwardemail").val(forwardemail);
				$("#keywordvalidity").val(keywordvalidity);
				$("#autoreplyonexpiry").val(autoreplyonexpiry);
				$("#kewordstatus").val(datarowid);
			},
		});
	}
	//sms keyword update
	function smskeywordupdate() {
		var id = $("#kewordstatus").val();
		var settingsid = $("#keywordsmsprovidersettingsid").val();
		var keywordname = $("#smskeywordsname").val();
		var description = $("#description").val();
		var autoreplymessage = $("#autoreplymessage").val();
		var forwardto = $("#forwardto").val();
		var forwardemail = $("#forwardemail").val();
		var keywordvalidity = $("#keywordvalidity").val();
		var smssettingsid = $("#smssettingsid").val();
		var autoreplyonexpiry = $("#autoreplyonexpiry").val();
		$.ajax({
			url: base_url + "Smssettings/smskeyworupdate",
			data:'settingsid='+settingsid+"&keywordname="+keywordname+"&description="+description+"&autoreplymessage="+autoreplymessage+"&forwardto="+forwardto+"&forwardemail="+forwardemail+"&keywordvalidity="+keywordvalidity+"&smssettingsid="+smssettingsid+"&autoreplyonexpiry="+autoreplyonexpiry+"&id="+id,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					$("#smssettingskeyupdatebutton").hide();
					$("#smssettingskeyaddbutton").show();
					$("#smssettingskeyingriddel3").show();
					smssettingsefreshgrid();
					resetFields();
					alertpopup('Data Stored Successfully');
					//For Touch
					masterfortouch = 0;
					//keyboard shortcut
					saveformview = 0;
				}
			},
		});
	}
	//sms keyword delete
	function smskeyworddelete(ids) {
		$.ajax({
			url: base_url + "Smssettings/smskeyworddelete",
			data:"primaryid="+ids,
			type:'POST',
			cache:false,
			success: function(msg) {
				var nmsg =  $.trim(msg);
				if (nmsg == 'TRUE'){
					smssettingsefreshgrid();;
					resetFields();
					$("#basedeleteoverlay").fadeOut();
					alertpopup('Record Deleted Successfully');
				}
			},
		});
	}
	//sender id value fetch based on the settings name
	function senderidddvalfetch(settnameid) {
		$('#smssettingsid').empty();
		$('#smssettingsid').append($("<option></option>"));
		$.ajax({
			url: base_url+"Smssettings/smsssenderddreload?settnameid="+settnameid,
			dataType:'json',
			async:false,
			cache:false,
			success: function(data) {
				if((data.fail) == 'FAILED') {
				} else {
					var newddappend="";
					var newlength=data.length;
					for(var m=0;m < newlength;m++) {
						newddappend += "<option data-apikey ='" +data[m]['apikey']+ "'  value = '" +data[m]['datasid']+ "'>"+data[m]['dataname']+ " </option>";
					}
					//after run
					$('#smssettingsid').append(newddappend);
				}
				$('#smssettingsid').trigger('change');
			},
		});
	}
}
{//default setting and sender id get
	function defaultsetinagsenderidget() {
		$.ajax({
			url: base_url + "Smssettings/defaultsettingsenderidget",
			dataType:'json',
			async: false,
			cache:false,
			success: function(data) {
				if((data.fail) != 'FAILED') {
					var settingsnameid = data.settingsid;
					var senderid = data.senderid;
					setTimeout(function(){
						$("#smsprovsettingsid").select2('val',settingsnameid);
						$("#defsenderid").select2('val',senderid);
					},100);
				}
			},
		});
	}
}
//sender drop down reload
function senderidddload(setnameid) {
	$('#defsenderid').empty();
	$('#defsenderid').append($("<option></option>"));
	$.ajax({
		url: base_url+"Smssettings/senderdddatafetch",
		data:'setnameid='+setnameid,
		type:'POST',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#defsenderid')
					.append($("<option></option>")
					.attr("data-settinsid",data[index]['dataname'])
					.attr("data-apikey",data[index]['apikey'])
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
			}
			$('#defsenderid').trigger('change');
		},
	});
}
//credit detail fetch function
function creditsdetailfetch(addontype) {
	$.ajax({
		url: base_url + "Smssettings/accountcresitdetailsfetch",
		data: "addontype=" + addontype,
		type: "POST",
		dataType:'json',
		async:false,
		cache:false,
		success: function(msg) {
			$("#availablecredits").val(msg);
			Materialize.updateTextFields();
		},
	});
}
//sms default sender id set
function smssettingdefaultsenderidset() {
	var formdata = $("#smssettingsform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	$.ajax({
		url: base_url + "Smssettings/smsdefaultsenderidset",
		data: "datas=" + datainformation,
		type: "POST",
		cache:false,
		success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE'){
				alertpopup('SMS Setting Saved!!');
			} else {
				alertpopup('SMS Setting Error!!');
			}
		},
	});
}
//Settings name check
function settingsnamecheck() {
	var primaryid = $("#credentialprimaryid").val();
	var accname = $("#settingsname").val();
	var elementpartable = 'smsprovidersettings';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Settings Name already exists!";
				}
			} else {
				return "Settings Name already exists!";
			}
		} 
	}
}
//Sender id name check
function senderidcheck() {
	var primaryid = $("#senderprimaryid").val();
	var accname = $("#senderid").val();
	var elementpartable = 'smssettings';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Settings Name already exists!";
				}
			} else {
				return "Settings Name already exists!";
			}
		}
	}
}
//Keyword name check
function keywordscheck() {
	var primaryid = $("#kewordstatus").val();
	var accname = $("#smskeywordsname").val();
	var elementpartable = 'smskeywords';
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Base/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != "") {
				if(primaryid != nmsg) {
					return "Keywords already exists!";
				}
			} else {
				return "Keywords already exists!";
			}
		} 
	}
}