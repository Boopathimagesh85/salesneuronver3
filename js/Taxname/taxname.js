$(document).ready(function() {	
	{//Foundation Initialization
		$(document).foundation();
	}	
	{// Maindiv height width change
		maindivwidth();
	}
	{//Grid Calling
		taxnameaddgrid();
		firstfieldfocus();
		taxnamecrudactionenable();
	}	
	{//view by drop down change
		$('#dynamicdddataview').change(function(){	
			taxnameaddgrid();
		});
	}
	//hidedisplay
	$('#taxnamedataupdatesubbtn').hide();
	$('#cumulativetaxiddivhid').hide(); //hide the compound tax on the simple,deduct
	$('#taxnamesavebutton').show();
	{//taxruleid	
		$('#taxruleid').change(function(){	
			var newvariable = $(this).val();
			if($(this).val() == 3){ //compound type
				$('#cumulativetaxiddivhid').show();
				$('#cumulativetaxid').empty();
				$('#cumulativetaxid').append($("<option></option>"));
				$.ajax({
					url:base_url+'Taxname/getsimpletax',
					dataType:'json',
					async:false,
					cache:false,
					success: function(data) {
						if((data.fail) == 'FAILED') {
						} else {
							var newddappend="";
							var newlength=data.length;
							for(var m=0;m < newlength;m++) {
						newddappend += "<option value = '" +data[m]['id']+ "'>"+data[m]['name']+'('+data[m]['rate']+ "%)</option>";
							}
							//after run					
							$('#cumulativetaxid').append(newddappend);
						}	
					},
				});
			} else{
				$('#cumulativetaxiddivhid').hide();
			}
		});
	}
	{//validation for Company Add  
		$('#taxnamesavebutton').click(function(e) { 
			if(e.which == 1 || e.which === undefined) {
				$("#taxnameformaddwizard").validationEngine('validate');
				masterfortouch = 0;
			}
		});
		jQuery("#taxnameformaddwizard").validationEngine( {
			onSuccess: function() {
				$('#taxnamesavebutton').attr('disabled','disabled');  
				taxmasterdetailsaddformdata();
			},
			onFailure: function() {
				$("#taxnamesavebutton").attr('disabled',false); 
				var dropdownid =['2','taxmasterid','taxruleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
		$("#taxnamereloadicon").click(function(){
			clearform('cleardataform');
			taxnamerefreshgrid();
			$("#taxruleid,#taxmodeid").select2("readonly", false); //calculation is readonly on edit
			$('#taxnamedataupdatesubbtn').hide();
			$('#taxnamesavebutton').show();
		});
		$( window ).resize(function() {
			innergridresizeheightset('taxnameaddgrid');
			sectionpanelheight('taxnamesectionoverlay');
		});
	}
	{//update company information
		$('#taxnamedataupdatesubbtn').click(function(e) { 
			if(e.which == 1 || e.which === undefined) {
				$("#taxnameformeditwizard").validationEngine('validate');
				//$(this).attr('disabled','disabled'); 
			}
		});
		jQuery("#taxnameformeditwizard").validationEngine({
			onSuccess: function() {
				taxmasterdetailsupdateformdata();
			},
			onFailure: function() {
				//$("#taxnamedataupdatesubbtn").attr('disabled',false); 
				var dropdownid =['2','taxmasterid','taxruleid'];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}	
		});
	}
	{//Function For Check box
		$('.checkboxcls').click(function(){
			var name = $(this).data('hidname');
			if ($(this).is(':checked')) {
				$('#'+name+'').val('Yes');
			}else{
				$('#'+name+'').val('No');
			}		
		});
	}
	{//filter work
		//for toggle
		$('#taxnameviewtoggle').click(function() {
			if ($(".taxnamefilterslide").is(":visible")) {
				$('div.taxnamefilterslide').addClass("filterview-moveleft");
			}else{
				$('div.taxnamefilterslide').removeClass("filterview-moveleft");
			}
		});
		$('#taxnameclosefiltertoggle').click(function(){
			$('div.taxnamefilterslide').removeClass("filterview-moveleft");
		});
		//filter
		$("#taxnamefilterddcondvaluedivhid").hide();
		$("#taxnamefilterdatecondvaluedivhid").hide();
		{	//field value set
			$("#taxnamefiltercondvalue").focusout(function(){
				var value = $("#taxnamefiltercondvalue").val();
				$("#taxnamefinalfiltercondvalue").val(value);
				$("#taxnamefilterfinalviewconid").val(value);
			});
			$("#taxnamefilterddcondvalue").change(function(){
				var value = $("#taxnamefilterddcondvalue").val();
				if( $('#s2id_taxnamefilterddcondvalue').hasClass('select2-container-multi') ) {
					taxnamemainfiltervalue=[];
					$('#taxnamefilterddcondvalue option:selected').each(function(){
					    var $tnvalue =$(this).attr('data-ddid');
					    taxnamemainfiltervalue.push($tnvalue);
						$("#taxnamefinalfiltercondvalue").val(taxnamemainfiltervalue);
					});
					$("#taxnamefilterfinalviewconid").val(value);
				} else {
					$("#taxnamefinalfiltercondvalue").val(value);
					$("#taxnamefilterfinalviewconid").val(value);
				}
			});
		}
		taxnamefiltername = [];
		$("#taxnamefilteraddcondsubbtn").click(function() {
			$("#taxnamefilterformconditionvalidation").validationEngine("validate");	
		});
		$("#taxnamefilterformconditionvalidation").validationEngine({
			onSuccess: function() {
				dashboardmodulefilter(taxnameaddgrid,'taxname');
			},
			onFailure: function() {
				var dropdownid =[];
				dropdownfailureerror(dropdownid);
				alertpopup(validationalert);
			}
		});
	}
	{//gst tax work -- gowtham
		if($("#gstapplicable").val() != 1) { // if gst not enabled
			$("#taxgstratedivhid,#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
			//$("#taxabilitytypeiddivhid").hide();
		} else { // if gst  enabled
			$("#taxapplicablefromdatedivhid").hide();
			$("#taxmasterid").change(function() {
				var val = $("#taxmasterid").find("option:selected").data('gstapply');
				if(val == 'Yes') {
					//$("#taxabilitytypeiddivhid").show();
					$("#taxgstratedivhid,#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
					$("#taxratedivhid").removeClass('large-12 medium-12 small-12 columns').addClass('large-6 medium-6 small-6 columns');
				} else {
					$("#taxratedivhid").removeClass('large-6 medium-6 small-6 columns').addClass('large-12 medium-12 small-12 columns');
					//$("#taxabilitytypeiddivhid").hide();
					$("#taxgstratedivhid,#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
					//$("#taxabilitytypeid").select2('val','');
					$("#taxgstrate,#taxcessrate,#taxapplicablefromdate").val('');
				}
			});
			$("#taxgstratedivhid,#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
			$("#taxabilitytypeid").change(function() {
				var val = $("#taxabilitytypeid").find('option:selected').val();
				if(val == 2 || val == 3) {
					$("#taxratedivhid,#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
					$("#taxrate,#taxcessrate,#taxapplicablefromdate").val('');
					$('#taxrate').removeClass('validate[required,custom[number],min[0.1],max[100],decval[2]]');
					$("#taxcessrate").removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
				} else if(val == 4) {
					$("#taxratedivhid,#taxcessratedivhid").hide();
					$("#taxapplicablefromdatedivhid").show();
					$("#taxrate,#taxcessrate").val('');
					$('#taxrate').removeClass('validate[required,custom[number],min[0.1],max[100],decval[2]]');
					$("#taxcessrate").removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
				} else if(val == 5){
					$("#taxratedivhid").show();
					$('#taxrate').removeClass('validate[required,custom[number],min[0.1],max[100],decval[2]]');
					$('#taxcessrate').removeClass('validate[custom[number],min[0.1],max[100],decval[2]]');
					$('#taxrate').addClass('validate[required,custom[number],min[0],max[100],decval[2]]');
					$("#taxcessrate").addClass('validate[custom[number],min[0],max[100],decval[2]]');
                } else {
					$("#taxcessratedivhid,#taxapplicablefromdatedivhid").hide();
					$("#taxcessrate,#taxapplicablefromdate").val('');
				}
			});
		}	
	}
});
{//view create success function
	function viewcreatesuccfun(viewname) {
		var viewmoduleids = $("#viewcreatemoduleid").val();
		dropdownvalsetview('dynamicdddataview','dynamicdddataviewid','viewcreationname','viewcreationid','viewcreation','viewcreationmoduleid',viewmoduleids);
		if(viewname != "dontset") {
			setTimeout(function() {
			$('#dynamicdddataview').select2('val',viewname);
			$('#dynamicdddataview').trigger('change');
			}, 1000);
			cleargriddata('viewcreateconditiongrid');
		} else {
			$('#dynamicdddataview').trigger('change');
		}
	}
}
//Documents Add Grid
function taxnameaddgrid(page,rowcount) {
	page = typeof page == 'undefined' ? 1 : page;
	rowcount = typeof rowcount == 'undefined' ? 30 : rowcount;
	var wwidth = $("#taxnameaddgridwidth").width();
	var wheight = $("#taxnameaddgridwidth").height();
	var viewname = $('#dynamicdddataview').val();
	$('#viewgridheaderid').text(viewname);
	//col sort
	var sortcol = $("#taxnamesortcolumn").val();
	var sortord = $("#taxnamesortorder").val();
	if(sortcol){
		sortcol = sortcol;
	} else{
		var sortcol = $('.taxnameheadercolsort').hasClass('datasort') ? $('.taxnameheadercolsort.datasort').data('sortcolname') : '';
	}
	if(sortord) {
		sortord = sortord;
	} else {
		var sortord =  $('.taxnameheadercolsort').hasClass('datasort') ? $('.taxnameheadercolsort.datasort').data('sortorder') : '';
	}
	var headcolid = $('.taxnameheadercolsort').hasClass('datasort') ? $('.taxnameheadercolsort.datasort').attr('id') : '0';
	var userviewid = $("#dynamicdddataview").find('option:selected').data('dynamicdddataviewid');
	var viewfieldids = $('#taxnameviewfieldids').val();
	if(userviewid != '') {
		userviewid= '75';
	}
	var filterid = $("#taxnamefilterid").val();
	var conditionname = $("#taxnameconditionname").val();
	var filtervalue = $("#taxnamefiltervalue").val();
	$.ajax({
		url:base_url+"Base/gridinformationfetch?viewid="+userviewid+"&maintabinfo=tax&primaryid=taxid&viewfieldids="+viewfieldids+"&page="+page+"&records="+rowcount+"&width="+wwidth+"&height="+wheight+'&sortcol='+sortcol+'&sortord='+sortord+'&filter='+filterid+'&conditionname='+conditionname+'&filtervalue='+filtervalue,
		contentType:'application/json; charset=utf-8',
		dataType:'json',
		async:false,
		cache:false,
		success :function(data) {
			$('#taxnameaddgrid').empty();
			$('#taxnameaddgrid').append(data.content);
			$('#taxnameaddgridfooter').empty();
			$('#taxnameaddgridfooter').append(data.footer);
			//data row select event
			datarowselectevt();
			//column resize
			columnresize('taxnameaddgrid');
			innergridresizeheightset('taxnameaddgrid');
			{//sorting
				$('.taxnameheadercolsort').click(function(){
					$("#processoverlay").show();
					$('.taxnameheadercolsort').removeClass('datasort');
					$(this).addClass('datasort');
					var page = $(this).data('pagenum');
					var rowcount = $('ul#taxnamepgnumcnt li .page-text .active').data('rowcount');
					var sortcol = $('.taxnameheadercolsort').hasClass('datasort') ? $('.taxnameheadercolsort.datasort').data('sortcolname') : '';
					var sortord =  $('.taxnameheadercolsort').hasClass('datasort') ? $('.taxnameheadercolsort.datasort').data('sortorder') : '';
					$("#taxnamesortorder").val(sortord);
					$("#taxnamesortcolumn").val(sortcol);
					var sortpos = $('#taxnameaddgrid .gridcontent').scrollLeft();
					taxnameaddgrid(page,rowcount);
					$('#taxnameaddgrid .gridcontent').scrollLeft(sortpos);//scroll to sorted position
					$("#processoverlay").hide();
				});
				sortordertypereset('taxnameheadercolsort',headcolid,sortord);
			}
			{//pagination
				$('.pvpagnumclass').click(function(){
					$("#processoverlay").show();
					var page = $(this).data('pagenum');
					var rowcount = $('.pagerowcount').data('rowcount');
					taxnameaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
				$('#taxnamepgrowcount').change(function(){
					$("#processoverlay").show();
					var rowcount = $(this).val();
					$('.pagerowcount').attr('data-rowcount',rowcount);
					$('.pagerowcount').text(rowcount);
					var page = $('#prev').data('pagenum');
					taxnameaddgrid(page,rowcount);
					$("#processoverlay").hide();
				});
			}
			$(".gridcontent").click(function(){
				var datarowid = $('#taxnameaddgrid div.gridcontent div.active').attr('id');
				if(datarowid){
					if(minifitertype == 'dashboard'){
						$("#minidashboard").trigger('click');
					} else if(minifitertype == 'notes') {
						$("#mininotes").trigger('click');
					}
				}
			});
			//Material select
			$('#taxnamepgrowcount').material_select();
		},
	});
}
function taxnamecrudactionenable() {
	{//add icon click
		$('#taxnameaddicon').click(function(e){
			sectionpanelheight('taxnamesectionoverlay');
			$("#taxnamesectionoverlay").removeClass("closed");
			$("#taxnamesectionoverlay").addClass("effectbox");
			e.preventDefault();
			taxcategorydatareload();
			Materialize.updateTextFields();
			firstfieldfocus();
			taxmodehideshow();
			$('#taxmasterid,#transactionmethodid').select2('readonly',false);
			$('#taxname,#taxrate').attr('readonly',false);
			$("#taxruleid,#taxmodeid").select2("readonly", false);
			//$("#taxcessrate").removeClass('validate[custom[number],min[0],max[100],decval[2]]');
			//$("#taxcessrate").addClass('validate[custom[number],min[0],max[100],decval['+rate_round+']]');
			resetFields();
			$('#taxratedivhid').hide();
			$('#taxabilitytypeid').select2('val',5).trigger('change');
			$('#taxnamedataupdatesubbtn').hide().removeClass('hidedisplayfwg');
			$('#taxnamesavebutton').show();
			$("#taxnameprimarydataid").val('');
			$("#taxruleid").select2('val',2);
		});
	}
	$("#taxnameediticon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxnameaddgrid div.gridcontent div.active').attr('id');
		if (datarowid)  {
			resetFields();
			$(".addbtnclass").addClass('hidedisplay');
			$(".updatebtnclass").removeClass('hidedisplay');
			$('#taxmasterid').select2('readonly',true);
			//$('#taxname').attr('readonly',true);
			$('#transactionmethodid').select2('readonly',true);
			//$('#taxrate').attr('readonly',true);
			//$("#taxcessrate").removeClass('validate[custom[number],min[0],max[100],decval[2]]');
			//$("#taxcessrate").addClass('validate[custom[number],min[0],max[100],decval['+rate_round+']]');
			taxmasterdetailsgetformdata(datarowid);
			taxmodehideshow();
			Materialize.updateTextFields();
			firstfieldfocus();
			masterfortouch = 1;
			//Keyboard short cuts
			saveformview = 1;
		} else {
			alertpopup("Please select a row");
		}
	});
	//delete icon
	$("#taxnamedeleteicon").click(function(e) {
		e.preventDefault();
		var datarowid = $('#taxnameaddgrid div.gridcontent div.active').attr('id');
		if(datarowid){	
			$('#taxnameprimarydataid').val(datarowid);
			$.ajax({
			url: base_url + "Base/multilevelmapping?level="+1+"&datarowid="+datarowid+"&table=salesdetailtax&fieldid=taxid&table1=''&fieldid1=''&table2=''&fieldid2=''",
			success: function(msg) {
			if(msg > 0){
			alertpopup("This tax name is already in usage. You cannot delete this tax name");						
			}else{
				combainedmoduledeletealert('taxnamedeleteyes');
				$("#taxnamedeleteyes").click(function(){
				var datarowid = $('#taxnameaddgrid div.gridcontent div.active').attr('id');
				taxmasterdetailsrecorddelete(datarowid);
				$(this).unbind();
			});
			}
			},
			}); 		
		}	
		 else {
			alertpopup("Please select a row");
		}
	});
}
{//refresh grid
	function taxnamerefreshgrid() {
		var page = $('ul#taxnamepgnum li.active').data('pagenum');
		var rowcount = $('ul#taxnamepgnumcnt li .page-text .active').data('rowcount');
		taxnameaddgrid(page,rowcount);
	}
}
//new data add submit function
function taxmasterdetailsaddformdata() {
    var formdata = $("#taxnameaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var compoundon= $('#cumulativetaxid').val();
    $.ajax({
        url: base_url + "Taxname/newdatacreate",
        data: "datas=" + datainformation+"&compoundon="+compoundon,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE')  {
            	$(".addsectionclose").trigger("click");
				var def = $("#taxbasicrate").val();
				if(def == 'Yes') {
					$('.singlecheckid').trigger('click');
				}
				resetFields();
				taxnamerefreshgrid();
				$("#taxnamesavebutton").attr('disabled',false); 
				alertpopup(savealert);
				$('#taxruleid,#taxmodeid').select2('val','').trigger('change');
            }
        },
    });
}
{//Addform section overlay close
	$(".addsectionclose").click(function(){
		resetFields();
		$("#taxnamesectionoverlay").removeClass("effectbox");
		$("#taxnamesectionoverlay").addClass("closed");
		$("#taxgstrate,#taxcessrate,#taxapplicablefromdate").val('');
		$("#taxabilitytypeid").select2('val','');
		$("#taxmasterid").trigger('change');
		$("#taxabilitytypeid").trigger('change');
	});
}
//old information show in form
function taxmasterdetailsgetformdata(datarowid) {
	var taxmasterdetailselementsname = $('#taxnameelementsname').val();
	var taxmasterdetailselementstable = $('#taxnameelementstable').val();
	var taxmasterdetailselementscolmn = $('#taxnameelementscolmn').val();
	var taxmasterdetailselementpartable = $('#taxnameelementspartabname').val();
	$.ajax(	{
		url:base_url+"Taxname/fetchformdataeditdetails?taxmasterdetailsprimarydataid="+datarowid+"&taxmasterdetailselementsname="+taxmasterdetailselementsname+"&taxmasterdetailselementstable="+taxmasterdetailselementstable+"&taxmasterdetailselementscolmn="+taxmasterdetailselementscolmn+"&taxmasterdetailselementpartable="+taxmasterdetailselementpartable, 
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'Denied') {
				alertpopup('Permission denied');
				resetFields();
				taxnamerefreshgrid();
			} else {
				sectionpanelheight('taxnamesectionoverlay');
				$("#taxnamesectionoverlay").removeClass("closed");
				$("#taxnamesectionoverlay").addClass("effectbox");
				$("#taxruleid,#taxmodeid").select2("readonly", true); //calculation is readonly on edit
				var txtboxname = taxmasterdetailselementsname + ',taxnameprimarydataid';
				var textboxname = {};
				textboxname = txtboxname.split(','); 
				var dataname = taxmasterdetailselementsname + ',primarydataid';
				var datasname = {};
				datasname = dataname.split(',');
				var dropdowns = [];
				textboxsetvaluenew(textboxname,datasname,data,dropdowns);
				$('#taxruleid,#taxmodeid').trigger('change');
				$("#taxmasterid").trigger('change');
				$("#taxabilitytypeid").trigger('change');
				var checkarray=['taxbasicrate'];
				setthecheckedvalue(checkarray);
				//Compound Tax Application
				if(data.compoundon != '' && data.taxruleid == 3){
					var vcselectid=data.compoundon;
					var selectid = vcselectid.split(',');
					$('#cumulativetaxid').select2('val',selectid).trigger('change');
				}
			}
		}
	});
	$('#taxnamedataupdatesubbtn').show().removeClass('hidedisplayfwg');;
	$('#taxnamesavebutton').hide();
	
}
//update old information
function taxmasterdetailsupdateformdata() {
	var formdata = $("#taxnameaddform").serialize();
	var amp = '&';
	var datainformation = amp + formdata;
	var compoundon= $('#cumulativetaxid').val();
    $.ajax({
        url: base_url + "Taxname/datainformationupdate?compoundon="+compoundon,
        data: "datas=" + datainformation+"&compoundon="+compoundon,
		type: "POST",
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if(nmsg == 'TRUE') {	
            	$(".addsectionclose").trigger("click");
				var def = $("#taxbasicrate").val();
				if(def == 'Yes') {
					$('.singlecheckid').trigger('click');
				}				
				$('#taxnamedataupdatesubbtn').hide();
				$('#taxnamesavebutton').show();
				resetFields();
				taxnamerefreshgrid();
				$("#taxnamedataupdatesubbtn").attr('disabled',false); 
				$('#taxruleid,#taxmodeid').select2('val','').trigger('change');
				$("#taxruleid,#taxmodeid").select2("readonly", false); //calculation is readonly on edit
				alertpopup(savealert);
				//For Touch work  to change to add state
				masterfortouch = 0;
				//Keyboard shortcut
				saveformview = 0;
            }
        },
    });	
}
//Record Delete
function taxmasterdetailsrecorddelete(datarowid) {
	var taxmasterdetailselementstable = $('#taxnameelementstable').val();
	var taxmasterdetailselementspartable = $('#taxnameelementspartabname').val();
    $.ajax({
        url: base_url + "Taxname/deleteinformationdata?taxmasterdetailsprimarydataid="+datarowid+"&taxmasterdetailselementstable="+taxmasterdetailselementstable+"&taxmasterdetailselementspartable="+taxmasterdetailselementspartable,
		cache:false,
        success: function(msg) {
			var nmsg =  $.trim(msg);
            if (nmsg == 'TRUE') {
            	taxnamerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Deleted successfully');
            } else if (nmsg == "false")  {
            	taxnamerefreshgrid();
				$("#basedeleteoverlayforcommodule").fadeOut();
				alertpopup('Permission denied');
            }
        },
    });
}
//tax name 
function taxnamecheck() {
	var primaryid = $("#taxnameprimarydataid").val();
	var accname = $("#taxname").val();
	var elementpartable = $('#taxnameelementspartabname').val();
	var nmsg = "";
	if( accname !="" ) {
		$.ajax({
			url:base_url+"Taxname/uniquedynamicviewnamecheck",
			data:"data=&accname="+accname+"&elementspartabname="+elementpartable+"&primaryid="+primaryid,
			type:"post",
			async:false,
			cache:false,
			success :function(msg) {
				nmsg = $.trim(msg);
			},
		});
		if(nmsg != "False") {
			if(primaryid != ""){
				if(primaryid != nmsg) {
					return "Tax name already exists!";
				}
			}else {
				return "Tax name already exists!";
			}
		} 
	}
}
//tax category data reload
function taxcategorydatareload() {
	$('#taxmasterid').empty();
	$('#taxmasterid').append($("<option></option>"));
	$.ajax({
		url:base_url+'Taxname/fetchdddataviewddval?dataname=taxmastername&dataid=taxmasterid&datatab=taxmaster',
		dataType:'json',
		async:false,
		cache:false,
		success: function(data) {
			if((data.fail) == 'FAILED') {
			} else {
				$.each(data, function(index) {
					$('#taxmasterid')
					.append($("<option></option>")
					.attr("data-gstapply",data[index]['gstapply'])		
					.attr("value",data[index]['datasid'])
					.text(data[index]['dataname']));
				});
				$.each(data, function(index) {
					if(data[index]['setdefault'] == 'Yes'){
						$('#taxmasterid').select2('val',data[index]['datasid']).trigger('change');
					}
				});
			}
		},
	});
}
function taxmodehideshow(){
	if(softwareindustryid == 3){
		$("#taxmodeid").select2('val',2).trigger("change");
		$('#taxmodeiddivhid').hide();
		$('#accountiddivhid').show();
		$('#taxmodeid').removeClass('validate[required]');
	}else{
		$('#taxmodeiddivhid').show();
		$('#accountiddivhid').hide();
		$('#taxmodeid').addClass('validate[required]');
	}
}