<?php
function base64_to_jpeg( $base64_string, $output_file )
{
    $ifp = fopen( $output_file, "wb" );
    $base64img = str_replace('data:image/png;base64,', '', $base64_string); 
    fwrite( $ifp, base64_decode($base64img) ); 
    fclose( $ifp ); 
    $bytes = strlen(base64_decode($base64_string));
    $roughsize = (((int)$bytes) / 1024) * 0.75;
    $roughsize = $roughsize / 10;
    $rsize = round($roughsize,2);
    $arr = array('out' => $output_file, 'size' => $rsize );
    return( $arr ); 
}
if(isset($_REQUEST['image']))
{
    $data=$_REQUEST['image'];

    $img = 'webcam_'.time().'.png';

    $imagename = base64_to_jpeg($data,'uploads/'.$img);
    $size = $imagename['size'];
    $imagename = $imagename['out'];
    $d = compress($imagename, $imagename, 80);
    if($d){
        echo json_encode(array('fname'=>$img,'fsize'=>$size.' KB','ftype'=>'image/png','path'=>'uploads/'.$img));
    }
}
else
{
    echo "You Dont Have Permission To Access This File!...";
}
function compress($source, $destination, $quality)
{ 
    $info = getimagesize($source); 
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source); 
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source); 
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source); 
    imagejpeg($image, $destination, $quality); 
    return $image." ".$destination;
} 

?>