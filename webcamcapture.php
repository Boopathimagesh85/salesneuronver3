<?php
function base64_to_jpeg( $base64_string, $output_file )
{
    $ifp = fopen( $output_file, "wb" );
	$base64img = str_replace('data:image/png;base64,', '', $base64_string);	
    fwrite( $ifp, base64_decode($base64img) ); 
    fclose( $ifp ); 
    return( $output_file ); 
}
if(isset($_REQUEST['image']))
{
	$data=$_REQUEST['image'];
	if(isset($_REQUEST['imgurl'])) 
	{
		$imgurl = $_REQUEST['imgurl'];
	}
	else
	{
		$imgurl = 'uploads/tag/'.'prodimg_'.time().'.png';
	}
	$imagename = base64_to_jpeg($data,$imgurl);
	$d = compress($imagename, $imagename, 80);
	echo $imagename;
}
else
{
	echo "You Dont Have Permission To Access This File!...";
}
function compress($source, $destination, $quality)
{ 
	$info = getimagesize($source); 
	if ($info['mime'] == 'image/jpeg') 
		$image = imagecreatefromjpeg($source); 
	elseif ($info['mime'] == 'image/gif') 
		$image = imagecreatefromgif($source); 
	elseif ($info['mime'] == 'image/png') 
		$image = imagecreatefrompng($source); 
	imagejpeg($image, $destination, $quality); 
	return $destination; 
} 

?>