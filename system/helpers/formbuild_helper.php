<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Salesneuron Form Build Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Rameshkumar Guru
 * @link		http://www.salesneuron.com
 * @version		1.1
 **/
// ------------------------------------------------------------------------
if ( ! function_exists('actionmenu')) {
	/* for grid action  menu generate - form with overlau module*/
	function actionmenu($value = array(),$size,$des = array(),$title = array(),$mname = null,$iconcolor = null) {
		$CI =& get_instance();
		$menu = "";
		$i=1;
		$m=0;
		$k=0;
		$menu .= "<ul class='' style=''><li class='action-icons' style='position:relative;top:-2px;'>";
		foreach($value as $key => $value) {
			if($i <= $size) {
				$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
				$name = strtolower($fname);
				if($name == 'add') {
					$menu .= '<span id="'.$mname.$name.'icon" title="'.$title[$m].'" class="icon-box '.$name.'iconclass " style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;padding-top:2px;padding-bottom:2px;padding-left:10px;padding-right:10px;position:relative;top: 4px;">'.$title[$m].' </span>'.PHP_EOL;
				} else {
					if($k==0){
						$menu .= '<div class="drop-container advanceddropbox dropdown-button" data-activates="'.$mname.'advanceddrop" style="background-color: #29b6f6 ;border-radius: 2px;font-size: 13px;padding-top:5px;padding-bottom:5px;padding-left:10px;padding-right:10px;position:relative;top: 4px;">More<i title="" class="material-icons" id="viewcategory" style="position:relative;left:3px;">arrow_drop_down</i></div><ul id="'.$mname.'advanceddrop" class="navaction-drop arrow_box active" style="">';
					} 
					$menu .= '<li id="'.$mname.$name.'icon"><span class="icon-box"><i class="'.$iconcolor.$des[$m].' '.$name.'iconclass " title="'.$title[$m].'"></i>'.$title[$m].'</span></li>';
					$k++;
				}
				
				$m++;
			}
			$i++;
		}
		if($k>0) {
			$menu .="</ul>";
		}
		$menu .= "</li></ul>";
		return $menu;
	}
}
if ( ! function_exists('multiactionmenu')) {
	/* for grid action  menu generate */
	function multiactionmenu($value = array(),$size,$des = array(),$title = array(),$mname = null,$iconcolor = null,$mcount) {
		$CI =& get_instance();
		$menu = "";
		$menustart = '';
		$menuadd = '';
		$menumore = '';
		$i=1;
		$m=0;
		$k=0;
		if($mcount == 1){
			$id = 'tab'.$mcount.'class';
			$class= 'toggle-view tabaction';
		} else {
			$id = 'tab'.$mcount.'class';
			$class= 'tabclass hidedisplay';
		}
		$menustart .= "<ul id='".$id."' class='".$class."' style='position:relative;left:-2px;'><li class='action-icons' style='position:relative;top: -10px;line-height: 1em;left:10px;'>";
		foreach($value as $key => $value) {
			if($i <= $size) {
				$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
				$name = strtolower($fname);
				if($name == 'add') {
					$menuadd .= '<span id="'.$mname.$name.'icon" title="'.$title[$m].'" class="icon-box add-form-for-2nd-head '.$name.'iconclass ">'.$title[$m].' </span>'.PHP_EOL;
				} else {
					if($k==0){
						$menumore .= '<div class="drop-container advanceddropbox dropdown-button more-btn-for-2nd-head"  data-activates="'.$mname.'advanceddrop" style="left: 2px;"><span class="icon-box" style="position:relative;"><i title="help" id="viewcategory" class="material-icons" style="font-size: 1.6rem;line-height: 1.0;color: #fff;left:-8px;top:0px;position:relative;">more_horiz</i></span></div>';
					}
					$k++;
				}
				$m++;
			}
			$i++;
		}
		if($k>0) {
			$menumore .="</ul>";
		}
		$menu .=$menustart.$menuadd.$menumore;
		$menu .= "</li></ul>";
		return $menu;
	}
}
if ( ! function_exists('actionmoremenu')) {
	/* action more menu generate */
	function actionmoremenu($value = array(),$size,$attribute = array(),$descrp = array(),$title = array(),$mname = null,$iconcolor = null) {
		$CI =& get_instance();
		$actmenu = "";
		$attr = getattributes($attribute);
		$i=1;
		$x = $size;
		foreach($value as $key => $value) {
			if($i > $size) {
				$fname = preg_replace('/[^A-Za-z0-9]/', '', $value);
				$name = strtolower($fname);
				$actmenu .= '<li><a '.$attr.'><span id="'.$mname.$name.'icon" title="'.$title[$x].'" class="'.$iconcolor.$descrp[$x].' '.$name.'iconclass"> </span></a></li>'.PHP_EOL;
				$x++;
			}
			$i++;
		}
		return $actmenu;
	}
}

if( ! function_exists('viewgriddatagenerate')) {
	/* Grid data view generate for base modules */
	function viewgriddatagenerate($tabledata,$primarytable,$primaryid,$width,$height,$footername,$checkbox = 'false') {
		
		$CI =& get_instance();
		//width set
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($tabledata[0]['colmodelname']);
		$columnswidth = array_sum($tabledata[0]['colsize'])+5;
		//width set
		$leftmenu = 75;
		$a = $width - $leftmenu;
		$b = (75 / 100) * $width;
		$c = (25 / 100) * $a;
		if($reccount <= 4){
			//$width = $b;
			$columnswidth = $b;
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$nsize = $addcolsize;
		if($addcolsize >75) {
			$addcolsize = $addcolsize-75;
		}
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		//height set
		if($height<=485) {
			$datarowheight = $height-40;
		}else  if($height<=700) {
			$datarowheight = $height-135-70;
		} else  if($height >= 794 ) {
			$datarowheight = $height-135-70-10;
		} else {
			if(empty($tabledata[3]['datainfo'])) {
				$datarowheight = $height-135-70-50;
			} else {
				$datarowheight = $height-135-70-50;
			}
		}
		if($reccount <= 4){
			$colsize = ($columnswidth-76) /$reccount;
		}
		//header generation
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		
		//ading extra 50 % for serila number width
		if($nsize == 0){
			$columnswidth = $columnswidth+75;
		}
		
		//
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;" data-leftmenu='.$leftmenu.' data-withmimusleft='.$a.' data-width75='.$b.' data-with25='.$c.'><ul class="inline-list">';
		//$header .= '<li style="width:200px;">&nbsp;</li>';
		$i=0;
		$modname='';
		$m=0;
		foreach ($tabledata[0]['colname'] as $headname) {
			if($reccount <= 4){
				$colsize = $colsize;
			} else {
				$colsize =$tabledata[0]['colsize'][$i]+$addcolsize;
			}
			if($footername != 'empty') {
				$modname = strtolower(substr($footername,0,-6));
			} else {
				$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][$i]));
			}
			if($checkbox == 'true' && $m==0) {
				$header .='<li id="headcheckbox'.$m.'" data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			if($i == 0) {
				$header .='<li id="headserialnum'.$i.'" data-uitype="2" data-fieldname="gridserialnumfield" data-width="35" data-class="serialnumber-class" data-viewtype="1" style="width:75px;">S.No</li>';
				$header .='';
			}
			
			$header .='<li id="headcol'.$i.'" class="headerresizeclass '.$modname.'headercolsort" data-class="'.$tabledata[0]['colmodelname'][$i].'-class" data-sortorder="ASC" data-colwidth="'.$columnswidth.'" data-addcolsize="'.$addcolsize.'" data-sortcolname="'.$tabledata[0]['colmodelindex'][$i].'" style="width:'.($colsize).'px;" data-width="'.($colsize).'" data-viewcolumnid="'.$tabledata[0]['colid'][$i].'" data-viewid="'.$tabledata[6].'" data-position="'.$i.'">'.$headname.'<i class="material-icons">sort</i></li>';
			$i++;
			
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">';
		$data = $tabledata[1]->result();
		if( empty($data) ) {
			//$content .= '&nbsp;&nbsp;&nbsp;&nbsp;<span class="nocontentspan">No Record(s) Available</span>';
			$content .=  norecordscontentfunction();
		}
		//content generation
		$count = 0;
		$k=1;
		if(count($tabledata[0]) > 0) {
			$count = count($tabledata[0]['colmodelname']);
		}
		foreach($tabledata[1]->result() as $row) {
			//print_r($row);
			$content .= '<div class="data-rows" id="'.$row->$primaryid.'"><ul class="inline-list">';
			$rowid = $row->$primaryid;
			for($i=0;$i<$count;$i++) {
				if($reccount <= 4){
					$colsize = $colsize;
				} else {
					$colsize = $tabledata[0]['colsize'][$i]+$addcolsize;
				}
				if($checkbox == 'true' && $i==0) {
					$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" /></li>';
				}
				if($i == 0){
					if($tabledata[2]['curpage'] <= 1) {
						$content .= '<li style="width:75px;" class="serialnumber-class">'.$k.'</li>';
					} else{
						$nval = $tabledata[2]['curpage'] - 1;
						$var = ($tabledata[2]['records']*$nval)+$k;
						$content .= '<li style="width:75px;" class="serialnumber-class">'.$var.'</li>';
					}
					
				}
				$dataname=$tabledata[0]['colmodelname'][$i];
				if($tabledata[0]['colmodeluitype'][$i] == 8) {
					$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
				} else if($tabledata[0]['colmodeluitype'][$i] == 31) {
					$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
				} else if($tabledata[0]['colmodeluitype'][$i] == 20) {
					$val = $row->$dataname.$row->rolename.$row->empgrpname;
				} else {
					if($tabledata[0]['colmodelname'][$i]=='commonid') {
						$value = $row->$dataname;
						$table = $tabledata[0]['coltablename'][$i];
						$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
						$val = (($val!='')?$val:'');
					} else {
						$val = $row->$dataname;
					} 
				} 
				$align = is_float($val)? 'textcenter' : 'textleft';
				if($tabledata[0]['colmodelname'][$i] == 'billlevelimages') {
					$content .= '<li class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class" id="maintableimagepreview" name="maintableimagepreview" maintablerowid="'.$row->$primaryid.'" style="width:'.($colsize).'px;">';
					$content .= '<i class="material-icons">radio_button_checked</i><a href="" style="display: none" target="_blank"></a></li>';
				} else {
					$content .= '<li class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class more_information" title="'.$val.'" style="width:'.($colsize).'px;">'.$val.'</li>';
				}
			}
			$k++;
			$content .= '</ul></div>';
		}
		$content .='</div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer="";
		$device = $CI->Basefunctions->deviceinfo();
		if($footername == 'empty') {
			$footername = $modname;
		}
		if(empty($tabledata[3]['datainfo'])) {
			if(empty($tabledata[2])) {
				$footer .= '';
			} else {
				$footer .='<div class="large-12 footer-contentnew">';
				if($device=='phone') {
								
							} else {
				$footer .= '<span class="rvcdropdown large-5 medium-5 small-6">';
				$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span class="large-3" ><span style="display:inline-block;color:#000000;font-size:15px;font-weight:bold;"> Rows</span>';
				$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
				$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
				foreach($rows as $row) {
					$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
					$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
					if($tabledata[2]['records']==$row){
						$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
					}
				}
				$footer .= '</select>';
				$footer .= $pagecount;
				if($device=='phone') {
								
							} else {
				$footer .= '<span class="large-3" style="background-color:#fff9c5;color:#000000;background-color: #fff9c4;color: #000000;padding-top: 5px;border-radius:4px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;">
				<span style="color:#000000;font-size:15px;">&nbsp;Total Records :</span><span style="font-weight: bold;">  '.$tabledata[2]['recordcount'].'</span>&nbsp;</span>';
				}
				$footer .= '</span></span>';
							}
							if($device=='phone') {
									$footer .= '<span class="rppdropdown2mobile large-3 medium-3 small-6">';
								
							} else {
									$footer .= '<span class="rppdropdown2 large-3 medium-3 small-6">';
							}
			
				$footer.='<span class="paging-box gridfootercontainer">';
				if($tabledata[2]['curpage'] > 1) {
					$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
				} else {
					$footer .= '<span id="prev" class="prev""><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
				}
				$spage=1;
				$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
				//$footer .= '<span class="paging" data-pagenum="'.$pagenum.'">'.$pagenum.' of '.$tabledata[2]['totalpage'].'</span>';
				if($pagenum > 1){
					$backno = $pagenum-1;
					if($backno > 0){
						$footer .= '<span class="" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" >'.$backno.'</span>&nbsp;&nbsp;';
					}
				}
				$footer .= '<span class="paging" style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" data-pagenum="'.$pagenum.'">'.$pagenum.'</span>&nbsp;&nbsp;';
				if($tabledata[2]['totalpage'] > 2){
					$nextno = $pagenum+1;
					if($tabledata[2]['curpage'] > $nextno){
						$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;">'.$nextno.'</span>&nbsp;&nbsp;';
					}
				}
				if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
					$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span style="color: #1893e7;">Next</span></span>';
				} else {
					$footer .= '<span id="next" class="next"><span style="color: #1893e7;">Next</span></span>';
				}
			$footer .= '</span>';
					$footer .= '</span>';
				$footer .='</div>';
			}
		} else { 
			$footer .='<div class="footer-contentnew">';
			$footer .= '<span class="rvcdropdown large-6 medium-6 small-12">';
			$footer .= '<span class="large-3" style="background-color:#fff9c4;"> <span style="color:#000000;">&nbsp;Total Records</span> '.$tabledata[2]['recordcount'].'&nbsp;</span>';
			$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span class="large-3" ><span style="display:inline-block;color:#000000"> Rows</span>';
			$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
			$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
			foreach($rows as $row) {
				$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
				$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
				if($tabledata[2]['records']==$row){
					$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
				}
			}
			$footer .= '</select>';
			$footer .= $pagecount;
			$footer .= '</span></span>';
			$footer .= '<span class="rppdropdown large-6 medium-6 small-12">';
			$footer.='<span class="paging-box gridfootercontainer"><span class="large-2">';
			if($tabledata[2]['curpage'] > 1) {
				$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			} else {
				$footer .= '<span id="prev" class="prev""><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			}
			$footer .= '</span>';
			$m=1;
			$spage=1;
			$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
			//$footer .= '<span class="paging" data-pagenum="'.$pagenum.'">'.$pagenum.' of '.$tabledata[2]['totalpage'].'</span>';
			if($pagenum > 1){
				$backno = $pagenum-1;
				if($backno > 0){
					$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;position:relative;" >'.$backno.'</span>&nbsp;&nbsp;';
				}
			}
			$footer .= '<span class="large-2" style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;position:relative;" data-pagenum="'.$pagenum.'">'.$pagenum.'</span>&nbsp;&nbsp;';
			if($tabledata[2]['totalpage'] > 2){
				$nextno = $pagenum+1;
				if($tabledata[2]['curpage'] > $nextno){
					$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;position:relative;">'.$nextno.'</span>&nbsp;&nbsp;';
				}
			}
			$footer .= '<span class="large-2">';
			if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
				$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span style="color: #1893e7;">Next</span></span>';
			} else {
				$footer .= '<span id="next" class="next""><span style="color: #1893e7;">Next</span></span>';
			}
			$footer .= '</span></div></div>';
		}
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if( ! function_exists('mobileviewgriddatagenerate')) {
	/* Mobile data view generate for base modules */
	function mobileviewgriddatagenerate($tabledata,$primarytable,$primaryid,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* Height set */
		if(empty($tabledata[3]['datainfo'])) {
			if($height > '470') {
				$height = $height-99;
			}
		} else {
			$height = $height-99;
		}
		$count = 0;
		if(count($tabledata[0]) > 0) {
			$count = count($tabledata[0]['colmodelname']);
		}
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][0]));
		/* Data row content */
		//row header icon
		$icon = $CI->Basefunctions->generalinformaion('module','moduleicon','moduleid',$tabledata[5]);
		$content = '<div class="row-content gridcontent" style="height:'.$height.'px"><div class="wrappercontent">';
		$datas = $tabledata[1]->result();
		$array = array();
		if( empty($datas) ) {
			//$content .='&nbsp;&nbsp;&nbsp;&nbsp;<span class="nocontentspan">No Record(s) Available</span>';
			$content .=  norecordscontentfunction();
		}
		foreach($tabledata[1]->result() as $row) {
			$rowid = $row->$primaryid;
			if($checkbox == 'true') {
				$content .='<div class="bottom-wrap"><span class="content mobrowcheckbox"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" /></span></div>';
			}
			$content .= '<div class="data-rows waves-effect waves-ripple" id="'.$rowid.'">';
			if( !empty($tabledata[4]) ) {
				$content .= '<div class="top-wrap"><div class="icon-container"><div class="icons random-bgcolor">A</div></div><div class="data-content">';
				$m=0;
				for($j=0;$j<2;$j++) {
					$array[$j] = $tabledata[0]['colid'][$j];
					if($m==0) {
						$content .= '<div class="topcontent">'.$tabledata[0]['colname'][$j].' : '.$row->$tabledata[0]['colmodelname'][$j].'</div>';
						$m=1;
					} else {
						$content .= '<div class="topcontent">'.$row->$tabledata[0]['colmodelname'][$j].'</div>';
					}
				}
				$content .= '</div></div>';
			}
			$content .= '<div class="bottom-wrap">';
			for($i=0;$i<$count;$i++) {
				$dataname=$tabledata[0]['colmodelname'][$i];
				if( !in_array( $tabledata[0]['colid'][$i],$array ) ) {
					if($tabledata[0]['colmodeluitype'][$i] == 8) {
						$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
					} else if($tabledata[0]['colmodeluitype'][$i] == 31) {
						$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
					} else if($tabledata[0]['colmodeluitype'][$i] == 20) {
						$val = $row->$dataname.$row->rolename.$row->empgrpname;
					} else {
						if($tabledata[0]['colmodelname'][$i]=='commonid') {
							$value = $row->$dataname;
							$table = $tabledata[0]['coltablename'][$i];
							$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
							$val = (($val!='')?$val:'');
						} else {
							$val = $row->$dataname;
						}
					}
					if($val!='') {
						$content .= '<span class="content">'.$tabledata[0]['colname'][$i].'</span><span class="content content-right"> '.$val.' </span>';
					}
				}
			}
			$content .= '</div></div>';
		}
		$content .='</div></div>';
		/* pagination */
		$paginationdata = $tabledata[1]->result();
		if(!empty($paginationdata)) {
			$pagenum = ($tabledata[2]['curpage'] == '0' && $tabledata[2]['curpage']<= 1) ? '1' : $tabledata[2]['curpage'];
			$content .= '<div class="pagination"> <ul id="'.$modname.'pgnum">';
			if($tabledata[2]['curpage'] > 1) {
				$content .= '<li class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><i class="material-icons">keyboard_arrow_up
</i></li>';
			} else {
				$content .= '<li class="prev last "><i class="material-icons">arrow_drop_up</i></li>';
			}
			$content .= '<li class="active">'.$pagenum.'</li>';
			if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
				$content .= '<li class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><i class="material-icons">keyboard_arrow_down
</i></li></ul></li>';
			} else {
				$content .= '<li class="next last"><i class="material-icons">arrow_drop_down</i></li></ul></li>';
			}
			$content .= '</ul></div>';
		}
		$datas = $content;
		$footer='';
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if( ! function_exists('girdviewdatagenerate')) {
	/* Grid view data generate for non base modules */
	function girdviewdatagenerate($tabledata,$colinfo,$primarytable,$primaryid,$modid,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* width set */
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		if($tabledata[2]=='jsongroup') {
			$columnswidth = array_sum($colinfo['colsize'])+5;
			$columnswidth -= 200;
		} else {
			$columnswidth = array_sum($colinfo['colsize'])+5;
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		/* height set */
		if($height<=485) {
			$datarowheight =$height-43;
		} else {
			if(empty($tabledata[3]['datainfo'])) {
				$datarowheight = $height-135-70;
			} else {
				$datarowheight = $height-135-70;
			}
		}
		/* header generation */
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$columnswidth = $columnswidth+75;
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/','',$modulename));
		if($checkbox == 'true') {
			$header .='<li data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
		}
		foreach ($colinfo['colname'] as $headname) {
			if($tabledata[2]=='jsongroup') {
				if($tabledata[3] != $headname) {
					if($i == 0) {
						$header .='<li id="headserialnum'.$i.'" data-uitype="2" data-fieldname="innergridserialnumfield" data-width="35" data-class="gridserialnumboxclass" data-viewtype="1" style="width:75px;">S.No</li>';
					}
					$cfield = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colname'][$i]));
					$header .='<li id="'.$modname.'headcol'.$i.'" data-fieldname="'.$cfield.'" class="'.$modname.'headercolsort '.$colinfo['colmodelname'][$i].'-class" data-class="'.$colinfo['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$colinfo['colmodelindex'][$i].'" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;" data-width="'.($colinfo['colsize'][$i]+$addcolsize).'" data-position="'.$i.'" data-viewtype="1">'.$headname.'</li>';
				}
			} else {
				if($i == 0) {
					$header .='<li id="headserialnum'.$i.'" data-uitype="2" data-fieldname="gridserialnumfield" data-width="35" data-class="gridserialnumboxclass" data-viewtype="1" style="width:75px;">S.No</li>';
				}
				$cfield = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colname'][$i]));
				$header .='<li id="'.$modname.'headcol'.$i.'" data-fieldname="'.$cfield.'"  class="'.$modname.'headercolsort '.$colinfo['colmodelname'][$i].'-class" data-class="'.$colinfo['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$colinfo['colmodelindex'][$i].'" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;" data-width="'.($colinfo['colsize'][$i]+$addcolsize).'" data-position="'.$i.'" data-viewtype="1">'.$headname.'</li>';
			}
			$i++;
		}
		$header .='</ul></div>';
		/* content generation */
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent sortable" id="'.$modname.'-sort" style="width:'.$columnswidth.'px;">';
		if($tabledata['2']=='json') {
			if(empty($tabledata[0])) {
				$content .= norecordscontentfunction();
			}
		} else if($tabledata['2']=='jsongroup') {
			if( empty($tabledata[0]) ) {
				$content .= norecordscontentfunction();
			}
		} else {
			$data = $tabledata[0]->result();
			if( empty($data) ) {
				$content .= norecordscontentfunction();
			}
		}
		$count = 0;
		$k=1;
		if(count($colinfo['colmodelname']) > 0) {
			$count = count($colinfo['colmodelname']);
		}
		if($tabledata[2]=='json') {
			foreach($tabledata[0] as $row) {
				$content .= '<div class="data-rows" id="'.$row['id'].'">
							<ul class="inline-list">';
				$count = (count($row)-1);
				for($i=0;$i<$count;$i++) {
					//echo $i."==".$count."<br>";
					if($checkbox == 'true' && $i==0) {
						$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$row['id'].'" name="'.$modname.'_rowchkbox'.$row['id'].'" data-rowid="'.$row['id'].'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$row['id'].'" /></li>';
					}
					if($i == 0){
						if($tabledata[1]['curpage'] <= 1) {
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$k.'</li>';
						} else{
							$nval = $tabledata[1]['curpage'] - 1;
							$var = ($tabledata[1]['records']*$nval)+$k;
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$var.'</li>';
						}
					}
					
					if($colinfo['uitype'][$i] == 8) {
						$val = $CI->Basefunctions->userdateformatconvert($row[$i]);
					} else if($colinfo['uitype'][$i] == 31) {
						$val = $CI->Basefunctions->userdatetimeformatconversion($row[$i]);
					} else if($colinfo['uitype'][$i] == 20) {
						$val = $row[$i];
					} else {
						if($colinfo['colmodelname'][$i]=='commonid') {
							$value = $row[$i];
							$table = $colinfo['coltablename'][$i];
							$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
							$val = (($val!='')?$val:'');
						} if($colinfo['colmodelname'][$i]=='attributevalues') {
							$value = $row[$i];
							$att_val = array_filter(explode('_',$value));
							$val ='';
							if(count($att_val) > 0) {
								$newatt_val=implode(',',$att_val);
								$val = $CI->Basefunctions->getattributevalues($newatt_val);
							}
							$val = (($val!='')?$val:'');
						} else {
							$val = $row[$i];
						}
					}
					if($checkbox == 'TEXT' && $i==$count-1) {
						if($modname == 'stonecharge') {
							$align =is_float($val)? 'textcenter' : 'textleft';
							$content .='<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;"><input type="text" id="'.$modname.'_rowtext'.$row['id'].'" name="'.$modname.'_rowtext'.$row['id'].'" data-rowid="'.$row['id'].'" data-validation-engine="validate[required,custom[number],min[1],decval[2]]" maxlength="10" class="'.$modname.'__rowtextclass rowtext" value="'.$val.'" ></li>';
						} else {
							$align =is_float($val)? 'textcenter' : 'textleft';
							$content .='<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;"><input type="text" id="'.$modname.'_rowtext'.$row['id'].'" name="'.$modname.'_rowtext'.$row['id'].'" data-rowid="'.$row['id'].'" class="'.$modname.'__rowtextclass rowtext" value="'.$val.'" ></li>';
						}
					} else {
						if($modulename == 'Orderitem' && $colinfo['colmodelname'][$i] == 'tagimage') { 
							$align =is_float($val)? 'textcenter' : 'textleft';
							$content .= '<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class tagimageoverlaypreview" id="tagimageoverlaypreview" data-count="'.$count.'" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;">';
							$content .= '<i class="material-icons">radio_button_checked</i>';
							$content .= '<a href="'.$val.'" style="display: none" target="_blank">'.$val.'</a>';
							$content .= '</li>';
						} else {
							$align =is_float($val)? 'textcenter' : 'textleft';
							$content .= '<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class" data-count="'.$count.'" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;">'.$val.'</li>';
						}
					}
				}
				$k++;
				$content .= '</ul></div>';
			}
		} else if($tabledata[2]=='jsongroup') {
			$prev = '';
			$m=0;
			foreach($tabledata[0] as $row) {
				if($prev!=$row[$tabledata[3]]) {
					$m++;
					$content .= '<div class="datarow-header"><ul class="inline-list">';
					$content .= '<li class="textleft" style="width:'.$columnswidth.'px;"><span class="rowgroupsheader" data-groupid="'.$m.'">-</span>'.$row[$tabledata[3]].'</li>';
					$content .= '</ul></div>';
					$prev = $row[$tabledata[3]];
				}
				$count = (count($row)-2);
				$content .= '<div class="data-rows rowgroup'.$m.'" id="'.$row['id'].'">
							<ul class="inline-list">';
				for($i=0;$i<$count;$i++) {
					if($checkbox == 'true' && $i==0) {
						$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$row['id'].'" name="'.$modname.'_rowchkbox'.$row['id'].'" data-rowid="'.$row['id'].'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$row['id'].'" /></li>';
					}
					if($i == 0){
						if($tabledata[1]['curpage'] <= 1) {
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$k.'</li>';
						} else{
							$nval = $tabledata[1]['curpage'] - 1;
							$var = ($tabledata[1]['records']*$nval)+$k;
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$var.'</li>';
						}
					}
					if($tabledata[3]!=$colinfo['colmodelname'][$i]) {
						if($colinfo['uitype'][$i] == 8) {
							$val = $CI->Basefunctions->userdateformatconvert($row[$i]);
						} else if($colinfo['uitype'][$i] == 31) {
							$val = $CI->Basefunctions->userdatetimeformatconversion($row[$i]);
						} else if($colinfo['uitype'][$i] == 20) {
							$val = $row[$i];
						} else {
							if($colinfo['colmodelname'][$i]=='commonid') {
								$value = $row[$i];
								$table = $colinfo['coltablename'][$i];
								$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
								$val = (($val!='')?$val:'');
							} if($colinfo['colmodelname'][$i]=='attributevalues') {
								$value = $row[$i];
								$att_val = array_filter(explode('_',$value));
								$val ='';
								if(count($att_val) > 0) {
									$newatt_val=implode(',',$att_val);
									$val = $CI->Basefunctions->getattributevalues($newatt_val);
								}
								$val = (($val!='')?$val:'');
							} else {
								$val = $row[$i];
							}
						}
						$align = is_float($val)? 'textcenter' : 'textleft';
						$content .= '<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;">'.$val.'</li>';
					}
				}
				$k++;
				$content .= '</ul></div>';
			}
		} else {
			foreach($tabledata[0]->result() as $row) {
				$content .= '<div class="data-rows" id="'.$row->$primaryid.'">
							<ul class="inline-list">';
				$rowid = $row->$primaryid;
				for($i=0;$i<$count;$i++) {
					$dataname = $colinfo['colmodelname'][$i];
					if($checkbox == 'true' && $i==0) {
						$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" /></li>';
					}
					if($i == 0){
						if($tabledata[1]['curpage'] <= 1) {
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$k.'</li>';
						} else{
							$nval = $tabledata[1]['curpage'] - 1;
							$var = ($tabledata[1]['records']*$nval)+$k;
							$content .= '<li style="width:75px;" class="serialnumber-class">'.$var.'</li>';
						}
					}
					
					if($colinfo['uitype'][$i] == 8) {
						$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
					} else if($colinfo['uitype'][$i] == 31) {
						$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
					} else if($colinfo['uitype'][$i] == 20) {
						$val = $row->$dataname.$row->rolename.$row->empgrpname;
					} else {
						if($colinfo['colmodelname'][$i]=='commonid') {
							$value = $row->$dataname;
							$table = $colinfo['coltablename'][$i];
							$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
							$val = (($val!='')?$val:'');
						} if($colinfo['colmodelname'][$i]=='attributevalues') {
							$value = $row->$dataname;
							$att_val = array_filter(explode('_',$value));
							$val ='';
							if(count($att_val) > 0) {
								$newatt_val=implode(',',$att_val);
								$val = $CI->Basefunctions->getattributevalues($newatt_val);
							}
							$val = (($val!='')?$val:'');
						} else {
							$val = $row->$dataname;
						}
					}
					$align = is_float($val)? 'textcenter' : 'textleft';
					$content .= '<li class="'.$align.' '.$colinfo['colmodelname'][$i].'-class" style="width:'.($colinfo['colsize'][$i]+$addcolsize).'px;">'.$val.'</li>';
				}
				$k++;
				$content .= '</ul></div>';
			}
		}
		$content .='</div></div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		if(!empty($tabledata[1])) {
			$footer .='<div class="large-12 footer-contentnew">';
			$footer .= '<span class="rvcdropdown large-6 medium-6 small-12">';
			$footer .= '<span style="background-color:#fff9c5;color:#000000;background-color: #fff9c4;color: #000000;padding-top: 5px;border-radius:4px;padding-bottom: 8px;padding-left: 5px;padding-right: 5px;"> <span style="color:#000000">Total record </span> : '.$tabledata[1]['recordcount'].'</span>';
			$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="display:inline-block;color:#000000"> Rows</span>';
			$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
			$footer .= '<select id="'.$modname.'pgrowcount" class="pagedropdown">';
			//$footer .= '<option value="10">10</option><option value="20">20</option><option value="30">30</option><option value="100">100</option>';
			$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="100">100</span>';
			foreach($rows as $row) {
				$class = ($tabledata[1]['records']==$row)?" selected='selected'":"";
				$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
				if($tabledata[1]['records']==$row){
					$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
				}
			}
			$footer .= '</select>';
			$footer .= $pagecount;
			$footer .= '</span>';
			$footer .= '<span class="rppdropdown large-6 medium-6 small-12">';
			$footer.='<span class="paging-box gridfootercontainer">';
			if($tabledata[1]['curpage'] > 1) {
				$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[1]['curpage']-1).'"><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			} else {
				$footer .= '<span id="prev" class="prev""><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			}
			$m=1;
			$spage=1;
			$pagenum = ($tabledata[1]['curpage'] == '0' || $tabledata[1]['curpage']<= 1 || $tabledata[1]['curpage']<= '') ? '1' : $tabledata[1]['curpage'];
			//$footer .= '<span class="paging" data-pagenum="'.$pagenum.'">'.$pagenum.' of '.$tabledata[1]['totalpage'].'</span>';\
			//$footer .= '<span class="icon-box" data-pagenum="'.$pagenum.'">|</span>';
			if($pagenum > 1){
				$backno = $pagenum-1;
				if($backno > 0){
					$footer .= '<span class="" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" >'.$backno.'</span>&nbsp;&nbsp;';
				}
			}
			$footer .= '<span class="paging" style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" data-pagenum="'.$pagenum.'">'.$pagenum.'</span>&nbsp;&nbsp;';
			if($tabledata[1]['totalpage'] > 2){
				$nextno = $pagenum+1;
				if($tabledata[1]['curpage'] > $nextno){
					$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;">'.$nextno.'</span>&nbsp;&nbsp;';
				}
			}
			if( ($tabledata[1]['curpage'] < $tabledata[1]['totalpage']) && ($tabledata[1]['totalpage']>'1') ) {
				$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[1]['curpage']+1).'"><span style="color: #1893e7;">Next</span></span>';
			} else {
				$footer .= '<span id="next" class="next""><span style="color: #1893e7;">Next</span></span>';
			}
			$footer .= '</span>';
			$footer .='</div>';
		} else {
			$footer = '<div class="summary-blue"></div>';
		}
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if( ! function_exists('mobilegirdviewdatagenerate')) {
	/* Mobile Grid view data generate for non base modules */
	function mobilegirdviewdatagenerate($tabledata,$colinfo,$primarytable,$primaryid,$modid,$modulename,$width,$height,$checkbox='false') {
		$CI =& get_instance();
		/* content generation */
		//$height = $height-105;
		//$height = $height-35;
		$count = 0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		/* Data row content */
		/* Row header icon */
		$icon = $CI->Basefunctions->generalinformaion('module','moduleicon','moduleid',$modid);
		if($tabledata[2]=='json') {
			$content = '<div class="row-content gridcontent " style="height:'.$height.'px"><div id="'.$modname.'-sort"  class="wrappercontent sortable">';
			if(empty($tabledata[0])) {
				$content .= norecordscontentfunction();
			}
			foreach($tabledata[0] as $row) {
				$content .= '<div class="data-rows  waves-effect waves-ripple" id="'.$row['id'].'">';
				if($checkbox == 'true') {
					$content .='<div class="bottom-wrap"><span class="content mobrowcheckbox"><input type="checkbox" id="'.$modname.'_rowchkbox'.$row['id'].'" name="'.$modname.'_rowchkbox'.$row['id'].'" data-rowid="'.$row['id'].'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$row['id'].'" /></span></div>';
				}
				$content .= '<div class="bottom-wrap">';
				$count = count($row)-1;
				for($i=0;$i<$count;$i++) {
					if($colinfo['uitype'][$i] == 8) {
						$val = $CI->Basefunctions->userdateformatconvert($row[$i]);
					} else if($colinfo['uitype'][$i] == 31) {
						$val = $CI->Basefunctions->userdatetimeformatconversion($row[$i]);
					} else if($colinfo['uitype'][$i] == 20) {
						$val = $row[$i];
					} else {
						if($colinfo['colmodelname'][$i]=='commonid') {
							$value = $row[$i];
							$table = $colinfo['coltablename'][$i];
							$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
							$val = (($val!='')?$val:'');
						} if($colinfo['colmodelname'][$i]=='attributevalues') {
							$value = $row[$i];
							$att_val = array_filter(explode('_',$value));
							$val ='';
							if(count($att_val) > 0) {
								$newatt_val=implode(',',$att_val);
								$val = $CI->Basefunctions->getattributevalues($newatt_val);
							}
							$val = (($val!='')?$val:'');
						} else {
							$val = $row[$i];
						}
					}
					if($val!='') {
						//$content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].' : '.$val.'</span>';
						$content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].'</span><span class="content content-right '.$colinfo['colmodelname'][$i].'-class"> '.$val.' </span>';
					}
				}
				$content .= '</div></div>';
			}
			$content .='</div></div>';
		} else if($tabledata[2]=='jsongroup') {
			$content = '<div class="row-content gridcontent"  style="height:'.$height.'px"><div id="'.$modname.'-sort" class="wrappercontent sortable">';
			if(empty($tabledata[0])) {
				$content .= norecordscontentfunction();
			}
			$prev = '';
			$m=0;
			foreach($tabledata[0] as $row) {
				$count = count($row)-2;
				if($prev!=$row[$tabledata[3]]) {
					$m++;
					$content .= '<div class="datarow-header"><div class="bottom-wrap">';
					$content .= '<span class="content"><span class="rowgroupsheader" data-groupid="'.$m.'">-</span>'.$row[$tabledata[3]].'</span>';
					$content .= '</div></div>';
					$prev = $row[$tabledata[3]];
				}	
				$content .= '<div class="data-rows rowgroup'.$m.'  waves-effect waves-ripple" id="'.$row['id'].'">';
				if($checkbox == 'true') {
					$content .='<div class="bottom-wrap"><span class="content mobrowcheckbox"><input type="checkbox" id="'.$modname.'_rowchkbox'.$row['id'].'" name="'.$modname.'_rowchkbox'.$row['id'].'" data-rowid="'.$row['id'].'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$row['id'].'" /></span></div>';
				}
				$content .= '<div class="bottom-wrap">';
				for($i=0;$i<$count;$i++) {
					if($tabledata[3]!=$colinfo['colmodelname'][$i]) {
						if($colinfo['uitype'][$i] == 8) {
							$val = $CI->Basefunctions->userdateformatconvert($row[$i]);
						} else if($colinfo['uitype'][$i] == 31) {
							$val = $CI->Basefunctions->userdatetimeformatconversion($row[$i]);
						}  else if($colinfo['uitype'][$i] == 20) {
							$val = $row[$i];
						} else {
							if($colinfo['colmodelname'][$i]=='commonid') {
								$value = $row[$i];
								$table = $colinfo['coltablename'][$i];
								$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
								$val = (($val!='')?$val:'');
							} if($colinfo['colmodelname'][$i]=='attributevalues') {
								$value = $row[$i];
								$att_val = array_filter(explode('_',$value));
								$val ='';
								if(count($att_val) > 0) {
									$newatt_val=implode(',',$att_val);
									$val = $CI->Basefunctions->getattributevalues($newatt_val);
								}
								$val = (($val!='')?$val:'');
							} else {
								$val = $row[$i];
							}
						}
						if($val!='') {
							//$content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].' : '.$val.'</span>';
							  $content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].'</span><span class="content content-right '.$colinfo['colmodelname'][$i].'-class"> '.$val.' </span>';
						}
					}
				}
				$content .= '</div></div>';
			}
			$content .='</div></div>';
		} else {
			$content = '<div class="row-content gridcontent" style="height:'.$height.'px"><div id="'.$modname.'-sort" class="wrappercontent sortable">';
			if(empty($tabledata[0])) {
				$content .= norecordscontentfunction();
			}
			$count = count($colinfo['colmodelname']);
			foreach($tabledata[0]->result() as $row) {
				$rowid = $row->$primaryid;
				$content .= '<div class="data-rows  waves-effect waves-ripple" id="'.$rowid.'">';
				if($checkbox == 'true') {
					$content .='<div class="bottom-wrap"><span class="content mobrowcheckbox"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" /></span></div>';
				}
				$content .= '<div class="bottom-wrap">';
				for($i=0;$i<$count;$i++) {
					$dataname = $colinfo['colmodelname'][$i];
					if($colinfo['uitype'][$i] == 8) {
						$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
					} else if($colinfo['uitype'][$i] == 31) {
						$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
					}  else if($colinfo['uitype'][$i] == 20) {
						$val = $row->$dataname.$row->rolename.$row->empgrpname;
					} else {
						if($colinfo['colmodelname'][$i]=='commonid') {
							$value = $row->$dataname;
							$table = $colinfo['coltablename'][$i];
							$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
							$val = (($val!='')?$val:'');
						} if($colinfo['colmodelname'][$i]=='attributevalues') {
							$value = $row->$dataname;
							$att_val = array_filter(explode('_',$value));
							$val ='';
							if(count($att_val) > 0) {
								$newatt_val=implode(',',$att_val);
								$val = $CI->Basefunctions->getattributevalues($newatt_val);
							}
							$val = (($val!='')?$val:'');
						} else {
							$val = $row->$dataname;
						}
					}
					if($val!='') {
						//$content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].' : '.$val.'</span>';
						$content .= '<span class="content '.$colinfo['colmodelname'][$i].'-class">'.$colinfo['colname'][$i].'</span><span class="content content-right '.$colinfo['colmodelname'][$i].'-class"> '.$val.' </span>';
					}
				}
				$content .= '</div></div>';
			}
			$content .='</div></div>';
		}
		/* pagination */
		if(!empty($tabledata[1])) {
			$pagenum = ($tabledata[1]['curpage'] == '0' && $tabledata[1]['curpage']<= 1) ? '1' : $tabledata[1]['curpage'];
			$content .= '<div class="pagination"> <ul id="'.$modname.'pgnum">';
			if($tabledata[1]['curpage'] > 1) {
				$content .= '<li class="prev pvpagnumclass" data-pagenum="'.($tabledata[1]['curpage']-1).'"><i class="material-icons">keyboard_arrow_up
</i></li>';
			} else {
				$content .= '<li class="prev last"><i class="material-icons">arrow_drop_up</i></li>';
			}
			$content .= '<li class="active">'.$pagenum.'</li>';
			if( ($tabledata[1]['curpage'] < $tabledata[1]['totalpage']) && ($tabledata[1]['totalpage']>'1') ) {
				$content .= '<li class="next pvpagnumclass" data-pagenum="'.($tabledata[1]['curpage']+1).'"><i class="material-icons">arrow_drop_down</i></li></ul></li>';
			} else {
				$content .= '<li class="next last"><i class="material-icons">keyboard_arrow_down</i></li></ul></li>';
			}
			$content .= '</ul></div>';
		}
		$datas = $content;
		$footer = '';
		/* Footer */
		/* $footer = '<div class="summary-blue"> &nbsp;';
		$footer .='</div>'; */
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if( ! function_exists('localviewgridheadergenerate')) {
	/* Grid view data generate for local gird [non base modules] */
	function localviewgridheadergenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* width & height set */
		$addcolsize = 0;
		$reccount = count($colinfo['colmodelname']);
		$columnswidth = 5;
		$reccount = 0;
		$active =0;
		foreach ($colinfo['colmodelviewtype'] as $type) {
			$columnswidth += $type=='1'? 150 : 0;
			$reccount++;
			if($type == '1') {
				$active++;
			}
		}
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$active) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		/* height set */
		if($height<=420) {
			$datarowheight = $height-40;
		} else {
			$datarowheight = $height-40;
		}
		/* header generation */
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {
				$header .='<li data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;" id="'.$modname.'headcheckbox"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			$cmodname = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelname'][$i]));
			$cmodindex = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelindex'][$i]));
			$cfield = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['fieldname'][$i]));
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';
			if($colinfo['colmodeluitype'][$i] == '252') {
				$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$cmodname.'-class '.$modname.'headercolsort" data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" style="width:'.(60+$addcolsize).'px;'.$viewtype.'" data-width="'.(60+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			} else {
				$header .='<li id="'.$modname.'headcol'.$i.'" class="'.$cmodname.'-class '.$modname.'headercolsort" data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" style="width:'.(150+$addcolsize).'px;'.$viewtype.'" data-width="'.(150+$addcolsize).'" data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'">'.$headname.'</li>';
			}
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;"></div></div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer = '';
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if( ! function_exists('mobileviewformtogriddatagenerate')) {
	/* Mobile view inner grid */
	function mobileviewformtogriddatagenerate($colinfo,$modulename,$width,$height,$checkbox = 'false') {
		$CI =& get_instance();
		/* Height set */
		if($height > '460') {
			$height = $height-105;
		}else {
			$height = $height;
		}		
		$mdivopen ='<div class="grid-view">';
		$content = '<div class="header-caption" style="display:none">';
		$i=0;
		$m=0;
		$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $modulename));
		foreach ($colinfo['fieldlabel'] as $headname) {
			if($checkbox == 'true' && $m==0) {		
				$content .='<span data-uitype="13" data-fieldname="gridcheckboxfield" data-class="gridcheckboxclass" style="width:35px;" data-viewtype="1" id="'.$modname.'headcheckbox"<input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="content"/></span>';
				$m=1;
			}
			$cmodname = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelname'][$i]));
			$cmodindex = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['colmodelindex'][$i]));
			$cfield = strtolower(preg_replace('/[^A-Za-z0-9]/', '',$colinfo['fieldname'][$i]));			
			$viewtype = ($colinfo['colmodelviewtype'][$i]=='0') ? ' display:none':'';			
			$content .= '<span id="'.$modname.'headcol'.$i.'"  data-class="'.$cmodname.'-class" data-sortcolname="'.$cmodindex.'" data-sortorder="ASC" data-uitype="'.$colinfo['colmodeluitype'][$i].'" data-fieldname="'.$cfield.'" class="'.$cmodname.'-class '.$modname.'headercolsort content" style="width:150px;'.$viewtype.'"  data-viewtype="'.$colinfo['colmodelviewtype'][$i].'" data-position="'.$i.'" data-label="'.$headname.' ">'.$headname.':</span>';			
			$i++;
		}		
		$content .='</div><div class="row-content gridcontent" style="height:'.$height.'px;"><div class="wrappercontent"></div></div>';
		$mdivclose = '</div>';
		$datas = $content;
		/* Footer generation */
		$footer = '<div class="summary-blue">';
		$footer .= '</div>';
		return array('content'=>$datas,'footer'=>$footer);
	}
}

if ( ! function_exists('tabgroupgenerate')) {
	/* tab group generation */
	function tabgroupgenerate($ulattr = array(),$uladdinfo = null,$tabgrpattr = array(),$tabstatus = null,$dataname,$modtabgrp = array()) {
		$uloption = getattributes($ulattr);
		$uldata = "<ul ".$uloption." ".trim($uladdinfo).">".PHP_EOL;
		$liattr = getattributes($tabgrpattr);
		$m = 1;
		$litab = "";
		foreach($modtabgrp as $value) {
			if($m==1) {
				foreach($tabgrpattr as $key => $values) {
					$class = $key;
					$val = $values;
				}
				$litab .= '<li '.$class.'="'.$val.' '.$tabstatus.' ftab" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['usertabgrpname'].'</span></li>'.PHP_EOL;
			} else {
				$litab .= '<li  '.$liattr.' data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['usertabgrpname'].'</span></li>'.PHP_EOL;
			}
			$m++;
		}
		$litab .= '<li class="morelabel tab-title sidebaricons hidedisplay" data-subform="x"><span>More</span></li>';
		$litab .= '<li id="moretab" class="tab-title moretab hidedisplay"><span class="dropdown-button" data-activates="tabgroupmoredropdown">More<i class="material-icons">arrow_drop_down</i></span><ul id="tabgroupmoredropdown" class="dropdown-content"></ul></li>';
		$ulc = close("ul");
		$reult = $uldata.$litab.$ulc;
		return $reult;
	}
}
if ( ! function_exists('mobiletabgroupgenerate')) {
	/* tab group generation */
	function mobiletabgroupgenerate($ulattr = array(),$uladdinfo = null,$tabgrpattr = array(),$tabstatus = null,$dataname,$modtabgrp = array()) {
		$uloption = getattributes($ulattr);
		$uldata = "<ul ".$uloption." ".trim($uladdinfo).">".PHP_EOL;
		$liattr = getattributes($tabgrpattr);
		$m = 1;
		$litab = "";
		foreach($modtabgrp as $value) {
			if($m==1) {
				foreach($tabgrpattr as $key => $values) {
					$class = $key;
					$val = $values;
				}
				$litab .= '<li '.$class.'="'.$val.' '.$tabstatus.' ftab" data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
			} else {
				$litab .= '<li  '.$liattr.' data-'.$dataname.'="'.$m.'" id="tab'.$m.'"><span class="waves-effect waves-ripple waves-light">'.$value['tabgrpname'].'</span></li>'.PHP_EOL;
			}
			$m++;
		}
		$ulc = close("ul");
		$reult = $uldata.$litab.$ulc;
		return $reult;
	}
}
if ( ! function_exists('steppertabgroupgenerate')) {
	/* tab group generation */
	function steppertabgroupgenerate($ulattr = array(),$uladdinfo = null,$tabgrpattr = array(),$tabstatus = null,$dataname,$modtabgrp = array()) {
		$uloption = getattributes($ulattr);
		$liattr = getattributes($tabgrpattr);
		$m = 1;
		$litab = "";
		$divdata = '';
		foreach($modtabgrp as $value) {
			if($m==1) {
				foreach($tabgrpattr as $key => $values) {
					$class = $key;
					$val = $values;
				}
				$divdata .= '<div '.$class.'="mdl-stepper-step active-step '.$val.' '.$tabstatus.' ftab" data-'.$dataname.'="'.$m.'" id="tab'.$m.'">'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-circle"><span>'.$m.'</span></div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-title">'.$value['tabgrpname'].'</div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-bar-left"></div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-bar-right"></div>'.PHP_EOL;
				$divdata .= close("div");
			} else {
				$divdata .= '<div class="mdl-stepper-step"  data-'.$dataname.'="'.$m.'" id="tab'.$m.'">'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-circle"><span>'.$m.'</span></div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-title">'.$value['tabgrpname'].'</div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-bar-left"></div>'.PHP_EOL;
				$divdata .= '<div class="mdl-stepper-bar-right"></div>'.PHP_EOL;
				$divdata .= close("div");
			}
			$m++;
		}
		
		$reult = $divdata;
		return $reult;
	}
}
if ( ! function_exists('textspan')) {
	/* text block generate */
	function textspan($divoption = array(),$lablecont,$laboption = array(),$name,$dvalue = null, $txtoptions = array()) {
		$butgen = "";
		$divgen = divopen($divoption);
		$labelgen = label($lablecont,$laboption);
		$textboxgen = text($name,$dvalue,$txtoptions);
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $textboxgen;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}
if ( ! function_exists('datetimetextspan')) {
	/* date time picker block generate */
	function datetimetextspan($divoption = array(),$lablecont,$laboption = array(),$name,$dvalue = null, $txtoptions = array()) {
		$butgen = "";
		$divgen = divopen($divoption);
		$labelgen = label($lablecont,$laboption);
		$textboxgen = text($name,$dvalue,$txtoptions);
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $textboxgen;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}
if ( ! function_exists('tagspan')) {
	/* text block generate */
	function tagspan($divoption = array(),$lablecont,$laboption = array(),$name,$dvalue = null, $txtoptions = array()) {
		$butgen = "";
		$divgen = divopen($divoption);
		$labelgen = label($lablecont,$laboption);
		$textboxgen = text($name,$dvalue,$txtoptions);
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $textboxgen;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}
if ( ! function_exists('passwordspan')) {
	/* password block generate */
	function passwordspan($divoption = array(),$lablecont,$laboption = array(),$name,$dvalue = null, $passwdoptions = array()) {
		$butgen = "";
		$divgen = divopen($divoption);
		$labelgen = label($lablecont,$laboption);
		$textboxgen = password($name,$dvalue,$passwdoptions);
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $textboxgen;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('textareaspan')) {
	/* text area block generate */
	function textareaspan($divoption = array(),$lablecont,$laboption = array(),$name,$dvalue = null, $txtoptions = array()) {
		$butgen = "";
		$divgen = divopen($divoption);
		$labelgen = label($lablecont,$laboption);
		$textboxgen = textarea($name,$dvalue,$txtoptions);
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $textboxgen;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('dropdownspan')) {
	/* drop down block generate */
	function dropdownspan($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$hidname = null,$hidval = null) {
		$hidgen = "";
		$labelgen = "";
		$divgen = divopen($divattr);
		if($labval!="") {
			$labelgen = label($labval,$labattr);
		}
		$dropdowngen = dropdown($name,$value,$valoption,$dataattr,$multiple,$option,$defval=array());
		if($hidname!="") {
			$hidgen = hidden($hidname,$hidval);
		}
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $dropdowngen;
		$divblock = $divblock . $hidgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('dropdownspandefault')) {
	/* drop down block generate with default value */
	function dropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval = array(),$hidname = null,$hidval = null) {
		$hidgen = "";
		$labelgen = "";
		$divgen = divopen($divattr);
		if($labval!="") {
			$labelgen = label($labval,$labattr);
		}
		$dropdowngen = dropdown($name,$value,$valoption,$dataattr,$multiple,$option,$defval);
		if($hidname!="") {
			$hidgen = hidden($hidname,$hidval);
		}
		$divclose = close('div');
		
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $dropdowngen;
		$divblock = $divblock . $hidgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('groupdropdownspandefault')) {
	/* group drop down block generate with default val set option */
	function groupdropdownspandefault($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$defval = array(),$hidname = null,$hidval = null) {
		$hidgen = "";
		$divgen = divopen($divattr);
		$labelgen = label($labval,$labattr);
		$grpdropdowngen = groupdropdown($name,$value,$valoption,$dataattr,$multiple,$option,$defval);
		if($hidname!="") {
			$hidgen = hidden($hidname,$hidval);
		}
		$divclose = close('div');
		
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $grpdropdowngen;
		$divblock = $divblock . $hidgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('groupdropdownspanuserspecific')) {
	/* group drop down block generate for user specific grouping */
	function groupdropdownspanuserspecific($divattr,$labval,$labattr,$name,$value,$valoption,$dataattr,$multiple,$option,$groupid,$hidname = null,$hidval = null) {
		$hidgen = "";
		$divgen = divopen($divattr);
		$labelgen = label($labval,$labattr);
		$grpdropdowngen = groupdropdownuserspecific($name,$value,$valoption,$dataattr,$multiple,$option,$groupid,$hidname);
		$hidtxtname = explode('|',$hidname);
		$hidtxtval = explode('|',$hidval);
		$m=0;
		foreach($hidtxtname as $txtname) {
			if($txtname!="") {
				$hidgen = $hidgen.PHP_EOL.hidden($txtname,$hidtxtval[$m]);
			}
			$m++;
		}
		$divclose = close('div');
		//final block
		$divblock = $divgen.PHP_EOL;
		$divblock = $divblock . $labelgen.PHP_EOL;
		$divblock = $divblock . $grpdropdowngen;
		$divblock = $divblock . $hidgen.PHP_EOL;
		$divblock = $divblock . $divclose.PHP_EOL;
		return $divblock;
	}
}

if ( ! function_exists('image')) {
	/* image */
	function image($name,$imgnamepath,$imgoption = array()) {
		$CI =& get_instance();
		$attr = getattributes($imgoption);
		$action = $CI->config->site_url($imgnamepath);	
		$img = '<img id="'.$name.'" name="'.$name.'" '.$attr.' src="'.$action.'">';
		return $img;
	}
}

if ( ! function_exists('divopen')) {
	/* for div open */
	function divopen($divoption = array()) {
		$divattrinfo = getattributes($divoption);
		return '<div '.$divattrinfo.'>';
	}
}

if ( ! function_exists('spanopen')) {
	/* for span open */
	function spanopen($spanoption = array()) {
		$spanattrinfo = getattributes($spanoption);
		return '<span '.$spanattrinfo.'>';
	}
}

if ( ! function_exists('span')) {
	/* for span */ 
	function span($value,$spanoption = array()) {
		$spanattrinfo = getattributes($spanoption);
		$spanopn = '<span '.$spanattrinfo.'>';
		$spanclose = close('span');
		$spanblock = $spanopn.$value.$spanclose;
		return $spanblock;
	}
}

if ( ! function_exists('close')) {
	/* for div/span and other close tag */
	function close($name) {
		$close = "</".$name.">";
		return $close;
	}
}

if ( ! function_exists('label')) {
	/* for label */
	function label( $value,$options=array() ) {
		$labattrinfo = getattributes($options);
		return '<label '.$labattrinfo.'>'.$value.'</label>';
	}
}

if ( ! function_exists('button')) {
	/* button */
	function button($name,$options = array()) {
		if(isset($name) != "") {
			$content = input('button',$name,'',$options);
			return '<input '.$content.' />';
		} else {
			return false;
		}
	}
}

if ( ! function_exists('text')) {
	/* text box */
	function text($name,$dvalue = null, $options = array()) {
		if(isset($name) != "") {
			$content = input('text',$name,$dvalue,$options);
			return '<input '.$content.' />';
		} else {
			return false;
		}
	}
}
if ( ! function_exists('password')) {
	/* password */
	function password($name,$dvalue = null, $options = array()) {
		if(isset($name) != "") {
			$content = input('password',$name,$dvalue,$options);
			return '<input '.$content.' />';
		} else {
			return false;
		}
	}
}

if ( ! function_exists('datetext')) {
	/* date text */
	function datetext($name,$value = null,$options = array()) {
		$ddattroption = ddgetattribute($options);
		$dateelemet = '<input type="text" name="'.$name.'" id="'.$name.'" '.$ddattroption.'>';
		return $dateelemet;
	}
}

if ( ! function_exists('datebutton')) {
	/* date button */
	function datebutton($type,$name,$value,$options = array()) {
		$dataopt = getattributes($options);
		$buttonelement = '<button name="'.$name.'" type="'.$type.'" id="'.$name.'" '.$dataopt.'>'.$value;
		$buttonclose = close("button");
		return $buttonelement.$buttonclose;
	}
}

if ( ! function_exists('textarea')) {
	/* text area */
	function textarea($name,$value = null,$options=array()) {
		$dataopt = ddgetattribute($options);
		$string = htmlspecialchars($value);
		//In case htmlspecialchars misses these.
		$txtstring = str_replace(array("'",'"'), array("&#39;","&quot;"), $string);
		$txtaraelement = '<textarea name="'.$name.'" id="'.$name.'" '.$dataopt.'>';
		$txtaraclose = close("textarea");
		$txtarea = $txtaraelement.''.$txtstring.''.$txtaraclose;
		return $txtarea;
	}
}

if ( ! function_exists('hidden')) {
	/* hidden text box */
	function hidden($name,$dvalue = null) {
		$options=array();
		if(isset($name) != "") {
			$content = input('hidden',$name,$dvalue,$options);
			return '<input '.$content.' />';
		} else {
			return false;
		}
	}
}

if ( ! function_exists('dropdown')) {
	/* drop down */
	function dropdown($name,$value = array(),$valoption,$dataattr = array(),$multiple = null,$option = array(),$defval = array()) {
		$multipleopt="";
		$ddname = $name;
		if($multiple == "Yes" || $multiple == "1") {
			$multipleopt = "multiple=multiple";
			$ddname = $name.'[]';
		}
		$ddattroption = ddgetattribute($option);
		$ddopenelement = "<select name='".$ddname."' id='".$name."' ".$ddattroption." ".$multipleopt.">";
		//$ddopenelement .= "<option value='1'>select</option>";
		$ddopenelement .= "<option></option>";
		if($valoption == "value") {
			foreach($value as $key => $val) {
				//data attribute generate
				$dataattropt = dddataattibute($dataattr, $key, $val);
				$dataattroption="";
				if(count($dataattropt)>0) {
					$dataattroption = implode(' ',$dataattropt);
				}
				if(in_array($key,$defval)) {
					$selected = 'selected="selected"';
				} else {
					$selected = "";
				}
				$ddopenelement .= "<option value='".$val."' ".$dataattroption." ".$selected.">".$val."</option>";
			}
		} else {
			foreach($value as $key => $val) {
				//data attribute generate
				$dataattropt = dddataattibute($dataattr, $key, $val);
				$dataattroption="";
				if(count($dataattropt)>0) {
					$dataattroption = implode(' ',$dataattropt);
				}
				if(in_array($key,$defval)) {
					$selected = 'selected="selected"';
				} else {
					$selected = "";
				}
				$ddopenelement .= "<option value='".$key."' ".$dataattroption." ".$selected.">".$val."</option>";
			}
		}
		$ddopenelement .= "</select>";
		return $ddopenelement;
	}
}

if ( ! function_exists('viewdropdown')) {
	/* drop down */
	function viewdropdown($name,$value = array(),$valoption,$dataattr = array(),$multiple = null,$option = array()) {
		$multipleopt="";
		$ddname = $name;
		if($multiple == "Yes" || $multiple == "1") {
			$multipleopt = "multiple=multiple";
		}
		$ddattroption = ddgetattribute($option);
		$ddopenelement = "<select name='".$name."' id='".$name."' ".$ddattroption." ".$multipleopt.">";
		//$ddopenelement .= "<option value='1'>select</option>";
		$ddopenelement .= "<option></option>";
		if($valoption == "value") {
			foreach($value as $key => $val) {
				//data attribute generate
				$dataattropt = dddataattibute($dataattr, $key, $val);
				$dataattroption="";
				if(count($dataattropt)>0) {
					$dataattroption = implode(' ',$dataattropt);
				}
				$ddopenelement .= "<option value='".$val."' ".$dataattroption.">".$val."</option>";
			}
		} else {
			foreach($value as $key => $val) {
				//data attribute generate
				$dataattropt = dddataattibute($dataattr, $key, $val);
				$dataattroption="";
				if(count($dataattropt)>0) {
					$dataattroption = implode(' ',$dataattropt);
				}
				$ddopenelement .= "<option value='".$key."' ".$dataattroption.">".$val."</option>";
			}
		}
		$ddopenelement .= "</select>";
		return $ddopenelement;
	}
}

if ( ! function_exists('groupdropdown')) {
	/* group drop down */
	function groupdropdown($name,$value = array(),$valoption,$dataattr = array(),$multiple = null,$option = array(),$defval = array()) {
		$multipleopt="";
		$ddname = $name;
		if($multiple == "Yes" || $multiple == "1") {
			$multipleopt = "multiple=multiple";
			$ddname = $name.'[]';
		}
		$ddattroption = ddgetattribute($option);
		$ddopenelement = "<select name='".$ddname."' id='".$name."' ".$ddattroption." ".$multipleopt.">";
		//$ddopenelement .= "<option value='1'>select</option>";
		$ddopenelement .= "<option></option>";
		$a = "pclass";
		if($valoption == "value") {
			foreach($value as $key => $gvalue) {
				$ddopenelement .='<optgroup label="'.$key.'" class="'.str_replace(' ','',$key).'">'.PHP_EOL;
				foreach($gvalue as $key => $val) {
					//data attribute generate
					$dataattropt = dddataattibute($dataattr, $key, $val);
					$dataattroption="";
					if(count($dataattropt)>0) {
						$dataattroption = implode(' ',$dataattropt);
					}
					if(in_array($key,$defval)) {
						$selected = 'selected="selected"';
					} else {
						$selected = "";
					}
					$ddopenelement .= "<option value='".$val."' ".$dataattroption." class='".$key.$a."' ".$selected.">".$val."</option>";
				}
				$ddopenelement .= '</optgroup>'.PHP_EOL;
			}
		} else {
			foreach($value as $key => $gvalue) {
				$ddopenelement .='<optgroup label="'.$key.'" class="'.str_replace(' ','',$key).'" >'.PHP_EOL;
				foreach($gvalue as $key => $val) {
					//data attribute generate
					$dataattropt = dddataattibute($dataattr, $key, $val);
					$dataattroption="";
					if(count($dataattropt)>0) {
						$dataattroption = implode(' ',$dataattropt);
					}
					if(in_array($key,$defval)) {
						$selected = 'selected="selected"';
					} else {
						$selected = "";
					}
					$ddopenelement .= "<option value='".$key."' ".$dataattroption." class='".$key.$a."' ".$selected.">".$val."</option>";
				}
				$ddopenelement .= '</optgroup>'.PHP_EOL;
			}
		}
		$ddopenelement .= "</select>";
		return $ddopenelement;
	}
}

if ( ! function_exists('groupdropdownuserspecific')) {
	/* group drop down for user specific */
	function groupdropdownuserspecific($name,$value = array(),$valoption,$dataattr = array(),$multiple = null,$option = array(),$groupid,$hidname) {
		$multipleopt="";
		$ddname = $name;
		if($multiple == "Yes" || $multiple == "1") {
			$multipleopt = "multiple=multiple";
			$ddname = $name.'[]';
		}
		$dataidhidname = explode('|',$hidname);
		if($dataidhidname[0] == "") {
			$dataidhidname[0] = "dataidhidden";
		}
		$ddattroption = ddgetattribute($option);
		$ddopenelement = "<select name='".$ddname."' id='".$name."' ".$ddattroption." ".$multipleopt.">";
		//$ddopenelement .= "<option value='1' data-".$dataidhidname[0]."='1' data-ddid='1'>select</option>";
		$ddopenelement .= "<option></option>";
		$a = "pclass";
		$i = 0;
		if($valoption == "value") {
			foreach($value as $lablkey => $gvalue) {
				if( count($gvalue) >0 ) {
					$ddopenelement .='<optgroup label="'.$lablkey.'" class="'.str_replace(' ','',$lablkey).'">'.PHP_EOL;
					foreach($gvalue as $key => $val) {
						//data attribute generate
						$dataattropt = dddataattibute($dataattr, $key, $val);
						$dataattroption="";
						if(count($dataattropt)>0) {
							$dataattroption = implode(' ',$dataattropt);
						}
						$ddopenelement .= "<option value='".$groupid[$i].":".$key."' ".$dataattroption." class='".$key.$a."' data-".$dataidhidname[0]."=".$groupid[$i]." data-ddid='".$key."'>".$val."</option>";
						$i++;
					}
					$ddopenelement .= '</optgroup>'.PHP_EOL;
				}
			}
		} else {
			foreach($value as $key => $gvalue) {
				if( count($gvalue) >0 ) {
					$ddopenelement .='<optgroup label="'.$key.'" class="'.str_replace(' ','',$key).'" >'.PHP_EOL;
					foreach($gvalue as $key => $val) {
						//data attribute generate
						$dataattropt = dddataattibute($dataattr, $key, $val);
						$dataattroption="";
						if(count($dataattropt)>0) {
							$dataattroption = implode(' ',$dataattropt);
						}
						$ddopenelement .= "<option value='".$groupid[$i].":".$key."' ".$dataattroption." class='".$key.$a."' data-".$dataidhidname[0]."=".$groupid[$i]." data-ddid='".$key."'>".$val."</option>";
						$i++;
					}
					$ddopenelement .= '</optgroup>'.PHP_EOL;
				}
			}
		}
		$ddopenelement .= "</select>";
		return $ddopenelement;
	}
}

if ( ! function_exists('input')) {
	/* input manipulation */
	function input($type,$name,$dvalue,$options= array()) {
		if(isset($dvalue) && $dvalue!="") {
			$elementkey =array('name','type','value','id');
			$elementval = array(''.$name.'',''.$type.'',''.$dvalue.'',''.$name.'');
		} else {
			$elementkey =array('name','type','id');
			$elementval = array(''.$name.'',''.$type.'',''.$name.'');
		}
		$dataattrinfo = array_combine($elementkey, $elementval);
		$attributes = array_merge($dataattrinfo,$options);
		$attrinfo = getattributes($attributes);
		return $attrinfo;
	}
}

if ( ! function_exists('ddgetattribute')) {
	/* generate drop down attributes */
	function ddgetattribute($option) {
		$attr=array();
		$dataattr=array();
		$ddattrinfo="";
		foreach($option as $key => $value) {
			if (is_array($value)) {
				if($key == "data") {
					 $dataattr = getdataattributes($value);
				}
			} else {
				if($value!="") {
					$attr[] = $key."='".$value."' ";
				}
			}
		}
		$dddattr = array_merge($attr,$dataattr);
		if(sizeof($dddattr) > 0) {
			$ddattrinfo = implode(' ',$dddattr);
		}
		return $ddattrinfo;
	}
}

if ( ! function_exists('getdataattributes')) {
	/* generate data attributes */
	function getdataattributes($attributes) {
		$attrinfo="";
		$dattr=array();
		foreach($attributes as $key => $value) {
			if($value!="") {
				$dattr[] = "data-".$key."='".$value."' ";
			}
		}
		return $dattr;
	}
}
if ( ! function_exists('getattributes')) {
	/* generate attributes */
	function getattributes($attributes) {
		$attrinfo="";
		$attr=array();
		foreach($attributes as $key => $value) {
			if($value != "") {
				$attr[] = $key."='".$value."' ";
			}
		}
		if(sizeof($attr) > 0) {
			$attrinfo = implode(' ',$attr);
		}
		return $attrinfo;
	}
}
if ( ! function_exists('dddataattibute')) {
	/* data attribute generate for dropdown */
	function dddataattibute($option,$id,$name) {
		$ddattr = array();
		foreach ($option as $key => $val) {
			if($val == "id") {
				$ddattr[] = 'data-'.$key.' = "'.$id.'"';
			} else {
				$ddattr[] = 'data-'.$key.' = "'.$name.'"';
			}
		}
		return $ddattr;
	}
}


if( ! function_exists('reportgriddatagenerate')) {
	/* Grid data view generate for base modules */
	function reportgriddatagenerate($tabledata,$primarytable,$primaryid,$width,$height,$footername,$checkbox = 'false') {
		$CI =& get_instance();
		$fieldhideshowids = $CI->Basefunctions->get_company_settings('reportscolumnfieldhideshow');
		$salessalesdetailid = explode(',',$fieldhideshowids);
		//width set
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($tabledata[0]['colmodelname']);
		$columnswidth = array_sum($tabledata[0]['colsize'])+5;
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		//height set
		if($height<=420) {
			$datarowheight = $height-35;
		} else {
			if(empty($tabledata[3]['datainfo'])) {
				$datarowheight = $height-135;
			} else {
				$datarowheight = $height-135;
			}
		}
		//header generation
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		$columnswidth = $columnswidth+75;
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		//$header .= '<li style="width:200px;">&nbsp;</li>';
		$i=0;
		$modname='';
		$m=0;
		foreach ($tabledata[0]['colname'] as $headname) {
			if($footername != 'empty') {
				$modname = strtolower(substr($footername,0,-6));
			} else {
				$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][$i]));
			}
			//$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][$i]));
			if($i == 0) {
				$header .='<li id="headserialnum'.$i.'" data-uitype="2" data-fieldname="gridserialnumfield" data-width="35" data-class="gridserialnumboxclass" data-viewtype="1" style="width:75px;">S.No</li>';
			}
			if($checkbox == 'true' && $m==0) {
				$header .='<li id="headcheckbox'.$m.'" data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}
			if(in_array($tabledata[0]['colid'][$i],$salessalesdetailid)) {
				$header .='<li id="headcol'.$i.'" class="headerresizeclass '.$modname.'headercolsort" data-class="'.$tabledata[0]['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$tabledata[0]['colmodelindex'][$i].'" style="width:0px;display:none;" data-width="'.($tabledata[0]['colsize'][$i]+$addcolsize).'" data-viewcolumnid="'.$tabledata[0]['colid'][$i].'" data-viewid="'.$tabledata[6].'" data-position="'.$i.'">'.$headname.'<i class="material-icons">sort</i></li>';
			} else {
				$header .='<li id="headcol'.$i.'" class="headerresizeclass '.$modname.'headercolsort" data-class="'.$tabledata[0]['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$tabledata[0]['colmodelindex'][$i].'" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;" data-width="'.($tabledata[0]['colsize'][$i]+$addcolsize).'" data-viewcolumnid="'.$tabledata[0]['colid'][$i].'" data-viewid="'.$tabledata[6].'" data-position="'.$i.'">'.$headname.'<i class="material-icons">sort</i></li>';
			}
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">';
		$data = $tabledata[1]->result();
		if( empty($data) ) {
			$content .=  norecordscontentfunction();
		}
		//content generation
		$count = 0;
		if(count($tabledata[0]) > 0) {
			$count = count($tabledata[0]['colmodelname']);
		}
		$sumlevel='';
		$k=0;
		$m=1;
		$grouparr=$tabledata[8];
		$grouplevel=count(array_filter($grouparr));
		$repeativearr=array('','','','','','','');
		$colsummaryclass = '';
		$applyclasname = '';
		foreach($tabledata[1]->result() as $row) {
			$rowsummaryclass = '';
			if($primaryid == 'salesid') {
				$subtableid = 'salesdetailid';
				if(isset($row->$subtableid)) {
					$content .= '<div class="data-rows" id="'.$row->$subtableid.'"><ul class="sundar inline-list">';
					$rowid = $row->$subtableid;
				} else {
					$content .= '<div class="data-rows" id="'.$row->$primaryid.'"><ul class="sundar inline-list">';
					$rowid = $row->$primaryid;
				}
			} else {
				$content .= '<div class="data-rows" id="'.$row->$primaryid.'"><ul class="sundar inline-list">';
				$rowid = $row->$primaryid;
			}
			for($i=0;$i<$count;$i++) {
				if($i == 0){
					if($tabledata[2]['curpage'] <= 1) {
						$content .= '<a href="#" data-name="serialnumber"><li style="width:75px;" class="serialnumber-class">'.$m.'</li></a>';
					} else{
						$nval = $tabledata[2]['curpage'] - 1;
						$var = ($tabledata[2]['records']*$nval)+$m;
						$content .= '<a href="#" data-name="serialnumber"><li style="width:75px;" class="serialnumber-class">'.$var.'</li></a>';
					}
				}
				if($i<$grouplevel && $grouplevel > 0) {
					$srkeys = array_keys($grouparr,$tabledata[0]['colmodelname'][$i]);
					if ($srkeys !== false && isset($srkeys[0])) {
						$colsummaryclass="colsummaryclass".$srkeys[0];
					} else {
						$colsummaryclass='';
					}
				} else {
					$colposition='0';
					$colsummaryclass='';
				}
				$datarepeatclass = '';
				$dataname = $tabledata[0]['colmodelname'][$i];
				if($i<$grouplevel){	
					if(in_array($tabledata[0]['colmodelname'][$i],$grouparr)) {  					
						if($row->$dataname != $repeativearr[$i]) {
							$repeativearr[$i]=$row->$dataname;					
						} else {	
							$datarepeatclass = 'reportdatashowshide';
						}
					}
				}
			    if($checkbox == 'true' && $i==0) {
					$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox '.$rowsummaryclass.' '.$colsummaryclass.'" value="'.$rowid.'" /></li>';
				}
				if($tabledata[0]['colmodeluitype'][$i] == 8) {
					if($row->$dataname==='-'|| $row->$dataname =='Summary' || $row->$dataname =='Total') {
						$val = $row->$dataname;
					} else {
						$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
					}
				} else if($tabledata[0]['colmodeluitype'][$i] == 20) {
					$val = $row->$dataname.$row->rolename.$row->empgrpname;
				} else if($tabledata[0]['colmodeluitype'][$i] == 31) {
					$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
				} else {
					if($tabledata[0]['colmodelname'][$i]=='commonid') {
						$value = $row->$dataname;
						$table = $tabledata[0]['coltablename'][$i];
						$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
						$val = (($val!='')?$val:'');
					} else {
						$val = $row->$dataname;
					}
				}
				if($i<$grouplevel) {	
					if(in_array($tabledata[0]['colmodelname'][$i],$grouparr)) {
						$tabularoverlay="onclick='tabularoverlay(this)'";
					}
				} else {
					$tabularoverlay='';
				}
				$align = is_float($val)? 'textcenter' : 'textleft';
				if(in_array($tabledata[0]['colid'][$i],$salessalesdetailid)) {
					$content .= '<a href="#" '.$tabularoverlay.' data-name="'.$tabledata[0]['colmodelname'][$i].'" > <li  class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class  '.$rowsummaryclass.'  '.$colsummaryclass.' '.$datarepeatclass.' " style="width:0px;display:none;">'.$val.'</li></a>';
				} else if($tabledata[0]['colmodelname'][$i] == 'tagimage' && $tabledata[0]['colmodeluitype'][$i] == 16) {
					if($tabledata[0]['colid'][$i] == '4772') {
						$tabledeclare = 'salesrpttagimagepreview';
					} else if($tabledata[0]['colid'][$i] == '5103') {
						$tabledeclare = 'stockrpttagimagepreview';
					} else {
						$tabledeclare = 'noimagetopreview';
					}
					$content .= '<a '.$tabularoverlay.' data-name="'.$tabledata[0]['colmodelname'][$i].'" target="_blank"> <li  class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class  '.$rowsummaryclass.'  '.$colsummaryclass.' '.$datarepeatclass.' " id="'.$tabledeclare.'" name="'.$tabledeclare.'" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;"><i class="material-icons">radio_button_checked</i></li></a>';
				} else {
					$content .= '<a href="#" '.$tabularoverlay.' data-name="'.$tabledata[0]['colmodelname'][$i].'" > <li  class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class  '.$rowsummaryclass.'  '.$colsummaryclass.' '.$datarepeatclass.' " style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;">'.$val.'</li></a>';
				}
			}
			$sumlevel='';
			$applyclasname='';
			$rowsummaryclass = '';
			$colsummaryclass='';
			$colposition='';
			$content .= '</ul></div>';
			$k++;
			$m++;
		}
		//exit;
		//echo $content;exit;
		$content .='</div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer="";
		if($footername == 'empty') {
			$footername = $modname;
		}
	if(empty($tabledata[3]['datainfo'])) {
			if(empty($tabledata[2])) {
				$footer .= '';
			} else {
				$footer .='<div class="large-12 footer-contentnew">';
				$footer .= '<span class="rvcdropdown large-6 medium-6 small-12">';
				$footer .= '<span style="display:inline-block;background-color:#fff9c4;"> <span style="color:#000000">Total record </span> : '.$tabledata[2]['recordcount'].'</span>';
				$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="display:inline-block;color:#000000"> Rows </span>';
				$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
				$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
				foreach($rows as $row) {
					$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
					$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
					if($tabledata[2]['records']==$row){
						$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
					}
				}
				$footer .= '</select>';
				$footer .= $pagecount;
				$footer .= '</span>';
				$footer .= '<span class="rppdropdown large-6 medium-6 small-12">';
				$footer.='<span class="paging-box gridfootercontainer"><span class="icon-boxfooter">';
				if($tabledata[2]['curpage'] != '') {
					$tabledata[2]['curpage'] = $tabledata[2]['curpage'];
				} else {
					$tabledata[2]['curpage'] = '1';
				}
				if($tabledata[2]['curpage'] > 1) {
					$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><i title="" class="material-icons">keyboard_arrow_left</i><span>Back</span></span>';
				} else {
					$footer .= '<span id="prev" class="prev""><span>Back</span></span>';
				}
				$footer .= '</span>';
				$m=1;
				$spage=1;
				$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
				//$footer .= '<span class="paging" data-pagenum="'.$pagenum.'">'.$pagenum.' of '.$tabledata[2]['totalpage'].'</span>';
				$footer .= '<span class="icon-box paging" data-pagenum="'.$pagenum.'">|</span>';
				$footer .= '<span class="icon-boxfooter">';
				if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
					$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span>Next</span><i title="" class="material-icons" id="">keyboard_arrow_right</i></span>';
				} else {
					$footer .= '<span id="next" class="next""><span>Next</span></span>';
				}
				$footer .= '</span>';
				$footer .='</span>';
				$footer .='</div>';
			}
		} else { 
			$footer .='<div class="large-12 footer-contentnew">';
			$footer .= '<span class="rvcdropdown large-6 medium-6 small-12">';
			$footer .= '<span style="display:inline-block;background-color:#fff9c4;"> <span style="color:#000000">Total record </span>:'.$tabledata[2]['recordcount'].'</span>';
			$footer .= '</span>';
			$footer .= '<span class="rppdropdown large-6 medium-6 small-12">';
			$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="display:inline-block;color:#000000"> Rows </span>';
			$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
			$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
			foreach($rows as $row) {
				$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
				$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
				if($tabledata[2]['records']==$row){
					$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
				}
			}
			$footer .= '</select>';
			$footer .= $pagecount;
			$footer.='<span class="pagingpaging-box gridfootercontainer"><span class="icon-box">';
			if($tabledata[2]['curpage'] > 1) {
				$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><i title="" class="material-icons">keyboard_arrow_left</i><span>Back</span></span>';
			} else {
				$footer .= '<span id="prev" class="prev""><span>Back</span></span>';
			}
			$footer .= '</span>';
			$m=1;
			$spage=1;
			$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
			//$footer .= '<span class="paging" data-pagenum="'.$pagenum.'">'.$pagenum.' of '.$tabledata[2]['totalpage'].'</span>';
			$footer .= '<span class="paging icon-box" data-pagenum="'.$pagenum.'">|</span>';
			$footer .= '<span class="icon-boxfooter">';
			 if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
				$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span>Next</span><i title="" class="material-icons" id="">keyboard_arrow_right</span>';
			} else {
				$footer .= '<span id="next" class="next""><span>Next</span></span>';
			} 
			$footer .= '</span>'; 
			$footer .= '</span></div></div>';
		}	
		return array('content'=>$datas,'footer'=>$footer);
	}
}
if( ! function_exists('newviewgriddatagenerate')) {
	/* Grid data view generate for base modules */
	function chkviewgriddatagenerate($tabledata,$primarytable,$primaryid,$width,$height,$footername,$checkbox = 'false',$arrcheckbox) {
		$CI =& get_instance(); 
		//width set
		$columnswidth = 1200;
		$addcolsize = 0;
		$reccount = count($tabledata[0]['colmodelname']);
		$columnswidth = array_sum($tabledata[0]['colsize'])+5;
		$extrasize = ($columnswidth>$width)?0 : ($width-$columnswidth);
		$addcolsize = $extrasize!=0 ? round($extrasize/$reccount) : 0;
		$columnswidth = ($columnswidth>$width)?$columnswidth : $width;
		//height set
		if($height<=485) {
			$datarowheight ='414';
		} else {
			if(empty($tabledata[3]['datainfo'])) {
				$datarowheight = $height-133;
			} else {
				$datarowheight = $height-133;
			}
		}
		//header generation
		if($checkbox == 'true') {
			$columnswidth = $columnswidth+35;
		}
		
		$mdivopen ='<div class="grid-view">';
		$header = '<div class="header-caption" style="width:'.$columnswidth.'px;"><ul class="inline-list">';
		//$header .= '<li style="width:200px;">&nbsp;</li>';
		$i=0;
		$modname='';
		$m=0;
		$headercount=COUNT($tabledata[0]['colname']);
		foreach ($tabledata[0]['colname'] as $headname) {
		if($footername != 'empty') {
				$modname = strtolower(substr($footername,0,-6));
			} else {
				$modname = strtolower(preg_replace('/[^A-Za-z0-9]/', '', $tabledata[0]['modname'][$i]));
			}
			if($checkbox == 'true' && $m==0) {
				$header .='<li id="headcheckbox'.$m.'" data-uitype="13" data-fieldname="gridcheckboxfield" data-width="35" data-class="gridcheckboxclass" data-viewtype="1" style="width:35px;"><input type="checkbox" id="'.$modname.'_headchkbox" name="'.$modname.'_headchkbox" class="'.$modname.'_headchkboxclass headercheckbox" /></li>';
				$m=1;
			}			
			$header .='<li id="headcol'.$i.'" class="headerresizeclass '.$modname.'headercolsort" data-class="'.$tabledata[0]['colmodelname'][$i].'-class" data-sortorder="ASC" data-sortcolname="'.$tabledata[0]['colmodelindex'][$i].'" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;" data-width="'.($tabledata[0]['colsize'][$i]+$addcolsize).'" data-viewcolumnid="'.$tabledata[0]['colid'][$i].'" data-viewid="'.$tabledata[6].'" data-position="'.$i.'">'.$headname.'<i class="material-icons">sort</i></li>';
			
			$i++;
		}
		$header .='</ul></div>';
		$content = '<div class="gridcontent" style="height:'.$datarowheight.'px; overflow:auto;"><div class="data-content wrappercontent" style="width:'.$columnswidth.'px;">';
		$data = $tabledata[1]->result();
		if( empty($data) ) {
			$content .=  norecordscontentfunction();
		}
		//content generation
		$count = 0;
		if(count($tabledata[0]) > 0) {
			$count = count($tabledata[0]['colmodelname']);
		}
		foreach($tabledata[1]->result() as $row) {
			$content .= '<div class="data-rows" id="'.$row->$primaryid.'">
							<ul class="inline-list">';
			$rowid = $row->$primaryid;
			for($i=0;$i<$count;$i++) {
				/*Account Group Checked Values */
				if($checkbox == 'true' && $i==0) {
					if(in_array($rowid,$arrcheckbox))
					{$checked="checked";
					}
					else
					{$checked="";
					}
					$content .='<li style="width:35px;"><input type="checkbox" id="'.$modname.'_rowchkbox'.$rowid.'" name="'.$modname.'_rowchkbox'.$rowid.'" data-rowid="'.$rowid.'" class="'.$modname.'_rowchkboxclass rowcheckbox" value="'.$rowid.'" '.$checked.' /></li>';
				}
				$dataname = $tabledata[0]['colmodelname'][$i];				
				if($tabledata[0]['colmodeluitype'][$i] == 8) {
					$val = $CI->Basefunctions->userdateformatconvert($row->$dataname);
				} else if($tabledata[0]['colmodeluitype'][$i] == 31) {
					$val = $CI->Basefunctions->userdatetimeformatconversion($row->$dataname);
				} else if($tabledata[0]['colmodeluitype'][$i] == 20) {
					$val = $row->$dataname.$row->rolename.$row->empgrpname;
				} else {
					if($tabledata[0]['colmodelname'][$i]=='commonid') {
						$value = $row->$dataname;
						$table = $tabledata[0]['coltablename'][$i];
						$val = $CI->Basefunctions->getrelatedmodulefieldvalues($value,$primarytable,$table,$row->$primaryid);
						$val = (($val!='')?$val:'');
					} else {
						$val = $row->$dataname;
					}
				}
				$align = is_float($val)? 'textcenter' : 'textleft';
				$content .= '<li class="'.$align.' '.$tabledata[0]['colmodelname'][$i].'-class" style="width:'.($tabledata[0]['colsize'][$i]+$addcolsize).'px;">'.$val.'</li>';
			}
			$content .= '</ul></div>';
		}
		$content .='</div>';
		$mdivclose = '</div>';
		$datas = $mdivopen.$header.$content.$mdivclose;
		$footer="";
		if($footername == ''){
			$footername = $modname;
		}
		if(empty($tabledata[3]['datainfo'])) {
			if(empty($tabledata[2])) {
				$footer .= '';
			} else {
				$footer .='<div class="large-12 footer-contentnew">';
				$footer .= '<span class="rvcdropdown large-6 medium-6 small-12">';
				$footer .= '<span style="display:inline-block;background-color:#fff9c4;"> <span style="color:#000000">Total record </span> : '.$tabledata[2]['recordcount'].'</span>';
				$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="display:inline-block;color:#000000"> Rows </span>';
				$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
				$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
				foreach($rows as $row) {
					$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
					$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
					if($tabledata[2]['records']==$row){
						$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
					}
				}
				$footer .= '</select>';
				$footer .= $pagecount;
				$footer .= '</span>';
				$footer .= '<span class="rppdropdown large-6 medium-6 small-12">';
				$footer.='<span class="paging-box gridfootercontainer">';
				if($tabledata[2]['curpage'] > 1) {
					$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
				} else {
					$footer .= '<span id="prev" class="prev""><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
				}
				$m=1;
				$spage=1;
				$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
				//$footer .= '<span class="icon-box" data-pagenum="'.$pagenum.'">|</span>';
				if($pagenum > 1){
					$backno = $pagenum-1;
					if($backno > 0){
						$footer .= '<span class="" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" >'.$backno.'</span>&nbsp;&nbsp;';
					}
				}
				$footer .= '<span class="paging" style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" data-pagenum="'.$pagenum.'">'.$pagenum.'</span>&nbsp;&nbsp;';
				if($tabledata[2]['totalpage'] > 2){
					$nextno = $pagenum+1;
					if($tabledata[2]['curpage'] > $nextno){
						$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;">'.$nextno.'</span>&nbsp;&nbsp;';
					}
				}
				if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
					$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span style="color: #1893e7;">Next</span></span>';
				} else {
					$footer .= '<span id="next" class="next""><span style="color: #1893e7;">Next</span></span>';
				}
				$footer .='</span>';
				$footer .='</div>';
			}
		} else { 
			$footer .='<div class="footer-contentnew">';
			$footer .= '<span class="rppdropdown">';
			$footer .= '<span style="display:inline-block;background-color:#fff9c4;"> <span style="color:#000000">Total record</span> : '.$tabledata[2]['recordcount'].'</span>';
			$footer .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="display:inline-block;color:#000000"> Rows</span>';
			$rows = array('10','20','30','50','100','200','300','500','1000','3000','5000','10000','15000');
			$footer .= '<select id="'.$footername.'pgrowcount" class="pagedropdown">';
			foreach($rows as $row) {
				$class = ($tabledata[2]['records']==$row)?" selected='selected'":"";
				$footer .= '<option value="'.$row.'" '.$class.'>'.$row.'</option>';
				if($tabledata[2]['records']==$row){
					$pagecount = '<span class="icon-box pagerowcount paginationhidedisplay" data-rowcount="'.$row.'">'.$row.'</span>';
				}
			}
			$footer .= '</select>';
			$footer .= $pagecount;
			$footer .= '</span>';
			$footer .= '<span class="rppdropdown large-6">';
			$footer.='<span class="paging-box gridfootercontainer">';
			if($tabledata[2]['curpage'] > 1) {
				$footer .= '<span id="prev" class="prev pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']-1).'"><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			} else {
				$footer .= '<span id="prev" class="prev""><span style="color: #1893e7;">Back</span></span>&nbsp;&nbsp;';
			}
			$m=1;
			$spage=1;
			$pagenum = ($tabledata[2]['curpage'] == '0' || $tabledata[2]['curpage']<= 1 || $tabledata[2]['curpage']<= '') ? '1' : $tabledata[2]['curpage'];
			//$footer .= '<span class="icon-box" data-pagenum="'.$pagenum.'">|</span>';
			if($pagenum > 1){
				$backno = $pagenum-1;
				if($backno > 0){
					$footer .= '<span class="" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" >'.$backno.'</span>&nbsp;&nbsp;';
				}
			}
			$footer .= '<span class="paging" style="background-color: #1893e7;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;" data-pagenum="'.$pagenum.'">'.$pagenum.'</span>&nbsp;&nbsp;';
			if($tabledata[2]['totalpage'] > 2){
				$nextno = $pagenum+1;
				if($tabledata[2]['curpage'] > $nextno){
					$footer .= '<span class="large-2" style="background-color: #87d4f6;border-radius: 2px;color: #fff;  font-size: 13px;height:22px;padding-top:0px;padding-left:10px;padding-right:10px;padding-bottom: 2px;position:relative;">'.$nextno.'</span>&nbsp;&nbsp;';
				}
			}
			if( ($tabledata[2]['curpage'] < $tabledata[2]['totalpage']) && ($tabledata[2]['totalpage']>'1') ) {
				$footer .= '<span id="next" class="next pvpagnumclass" data-pagenum="'.($tabledata[2]['curpage']+1).'"><span style="color: #1893e7;">Next</span></span>';
			} else {
				$footer .= '<span id="next" class="next""><span style="color: #1893e7;">Next</span></span>';
			}
			$footer .='</span>';
			$footer .= '</span></div>';
		}
		return array('content'=>$datas,'footer'=>$footer);
	}
	function norecordscontentfunction() {
		$norecordcontent = '<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows" ><ul class="inline-list" style="padding-top:5px;padding-left:385px"><li style="padding-top:4px;padding-bottom: 4px;background-color: #ffebed;border-radius: 5px;border: 1px solid #d7a7b3;">No Records Available</li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>
					<div class="emptydata-rows"><ul class="inline-list"><li></li></ul></div>';
		return $norecordcontent;
	}
}
?>