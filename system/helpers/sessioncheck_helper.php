<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('sessionchecker'))
{
    function sessionchecker($moduleid) { 
	// Checking Role & plan based page access
	//pageaccesschecker($moduleid);
		
		// get the superobject
		$CI =& get_instance();
		$CI->load->model('Base/Basefunctions'); 
		//echo "<pre>";print_r($CI->session->userdata);exit;
		// call the session library
		$logged = $CI->session->userdata('logged_in');
		$logaccstatus = $CI->session->userdata('UserAccount');
		if(isset($logged) && isset($logaccstatus))//checks userdata exits
		{
			if(isset($logged['empid']))//checks employee is set
			{
				if($logged['empid'] > 0)//checks valid employee
				{
					if($moduleid==1) {
						if($logaccstatus=='inactive') {
							accouuntexpirepage();
						}
					} else {
						$modstatus = $CI->Basefunctions->checkusermodule($moduleid,$logged['empid']);
						if($modstatus=='Yes') {
							if($logaccstatus=='inactive') {
								accouuntexpirepage();
							}
						} else {
							logoutpage();
						}
					}
				} else {
					logoutpage();
				}
			} else {
				logoutpage();
			}
		} else {
			logoutpage();
		}
    }
	function logoutpage() {
		$CI =& get_instance();
		$hostname = $CI->Basefunctions->gethostdataname();
		if($_SERVER['HTTP_HOST']!=$hostname) {
			$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
			$hname = $_SERVER['HTTP_HOST'];
			$hnames = explode('.',$hname);
			$cnt = count($hnames);
			$host = $hnames[($cnt-2)].'.'.$hnames[($cnt-1)];
			redirect('http'.$secure.'://'.$host.'/login.php');
		} else {
			redirect(base_url().'Login','location');
		}
	}
	function accouuntexpirepage() {
		$CI =& get_instance();
		$hostname = $CI->Basefunctions->gethostdataname();
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		$hname = $_SERVER['HTTP_HOST'];
		if($_SERVER['HTTP_HOST']!=$hostname) {
			redirect('http'.$secure.'://'.$hname.'/crm/Home/accountexpire','refresh');
		} else {
			redirect('http'.$secure.'://'.$hname.'/salesneuroncrm/Home/accountexpire','location');
		}
	}
	function sessionchecktimeinterval() {
		$a='live';
		// get the superobject
		$CI =& get_instance();
		// call the session library
		$logged = $CI->session->userdata('logged_in');
		$logaccstatus = $CI->session->userdata('UserAccount');
		if (isset($logged) && isset($logaccstatus))//checks userdata exits
		{
			if(isset($logged['empid']))//checks employee is set
			{
				if($logged['empid'] > 0)//checks valida employee
				{
					$a='live';
				} else {
					$a='logout';
				}
			} else {
				$a='logout';
			}		
		} else {
			$a='logout';
		}
		return $a;
    }
	function pageaccesschecker($moduleid)
	{
		$CI =& get_instance();
		$userdata = $CI->session->userdata('user_data');
		$logged = $CI->session->userdata('logged_in');
		$CI->load->model('Base/Basefunctions');
		$pagechecking=$CI->Basefunctions->pageaccesschecker($moduleid);
		if($pagechecking['rolevalid']==0 and $pagechecking['planvalid']==0)
		{
			//redirect('login','refresh');
			logoutpage();
		}
	}
}