<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class CI_CSVReader {
    var $fields;        /** columns names retrieved after parsing */
    var $separator = ',';    /** separator used to explode each line */
    /**
     * Parse a text containing CSV formatted data.
     *
     * @access    public
     * @param    string
     * @return    array
     */
    public function parse_text($p_Text) {
        $lines = explode("\n", $p_Text);
        return $this->parse_lines($lines,',');
    }
    /**
     * Parse a file containing CSV formatted data.
     *
     * @access    public
     * @param    string
     * @return    array
     */
    public function parse_file($p_Filepath) {
		$lines = array();
		if( $p_Filepath!='' && file_exists($p_Filepath) && is_readable($p_Filepath) ) {
			$lines = file($p_Filepath);
			return $lines;
		} else {
			return $lines;
		}
    }
	/* header( "Content-Type: text/html; charset=utf-8");
	$csvContent = file_get_contents( $fileName );
	$encoding = mb_detect_encoding( $csvContent,array("UTF-8","UTF-32","UTF-32BE","UTF-32LE","UTF-16","UTF-16BE","UTF-16LE"),TRUE );
	if( $fileEncoding !== "UTF-8" ) {
		$csvContent = mb_convert_encoding($csvContent, "UTF-8", $fileEncoding );
	}
	foreach( explode( PHP_EOL, $csvContent ) as $item ) {
		var_dump($item );
	} */
    /**
     * Parse an array of text lines containing CSV formatted data.
     *
     * @access    public
     * @param    array
     * @return    array
     */
    public function parse_lines($p_filepath,$delimiter) {
		$p_CSVLines = $this->parse_file($p_filepath);
        $content = array();
        $dcontent = FALSE;
		$header = array();
        foreach( $p_CSVLines as $line_num => $line ) {
            if( $line != '' ) { // skip empty lines
				$dataline = utf8_encode($line);
                $elements1 = explode($delimiter,$dataline);
                $elements2 = explode($delimiter,$dataline);
				if($elements1>=$elements2) {
					$elements = $elements1;
				} else {
					$elements = $elements2;
				}
                if( !is_array($dcontent) ) { // the first line contains fields names
                    $header = $elements;
                    $dcontent = array();
                } else {
                    $item = array();
                    foreach( $header as $id => $field ) { //value arrange [key value is column title]
                        if( isset($elements[$id]) ) {
							$element = preg_replace('/"+/', '', $elements[$id]); //remove " in header
							$title = preg_replace('/"+/', '', $field); //remove " in header
							$key = trim($title);
							$data = trim($element);
                            $item[$key] = $data;
                        }
                    }
                    $content[] = $item;
                }
            }
        }
        return $content;
    }
	/** parse file and get title 
	@access    public
	* @param    array
	* @return    array
	**/
	public function parse_lineheader($p_filepath,$separator) {
		$p_CSVLines = $this->parse_file($p_filepath);
        $header = FALSE;
        foreach( $p_CSVLines as $line_num => $line ) {
            if( $line != '' ) { // skip empty lines
                $elements = explode($separator, $line);
                if( !is_array($header) ) { // the first line contains fields names
					$header = array();
					foreach($elements as $headval) {
						$val = preg_replace('/"+/', '', $headval);
						array_push($header,trim($val));
					}
                }
            }
        }
        return $header;
	}
}