<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class CI_Convert {
	var $words = array();
	var $places = array();
	var $amount_in_words;
	var $decimal;
	var $decimal_len;
	
	public function numtowordconvert($amount,$currency="Dollar",$subunit="Cents") {
		$this->assign($currency);
		
		$temp = (string)$amount;
		$pos = strpos($temp,".");
		if ($pos) {
			$temp = substr($temp,0,$pos);
			$this->decimal = strstr((string)$amount,".");
			$this->decimal_len = strlen($this->decimal) - 2;
			$this->decimal = substr($this->decimal,1,$this->decimal_len+1);
		}
		$len = strlen($temp)-1;
		$ctr = 0;
		$indcurchk = 0;
		$arr = array();
		if($currency != "Rupees") {
			while ($len >= 0) {
				if ($len >= 2) {
					$arr[$ctr++] = substr($temp, $len-2, 3);
					$len -= 3;
				} else {
					$arr[$ctr++] = substr($temp,0,$len+1);
					$len = -1;
				}
			}
		} else {
			while ($len >= 0) {
				if ($len >= 2) {
					if($indcurchk == 0) {
						$arr[$ctr++] = substr($temp, $len-2, 3);
						$len -= 3;
						$indcurchk=1;
					} else {
						$arr[$ctr++] = substr($temp, $len-1, 2);
						$len -= 2;
					}
				} else {
					$arr[$ctr++] = substr($temp,0,$len+1);
					$len = -1;
				}
			}
		}
		$str = "";
		for ($i=count($arr)-1; $i>=0; $i--) {
			$figure = $arr[$i];
			$sub = array(); $temp="";
			for ($y=0; $y<strlen(trim($figure)); $y++) {
				$sub[$y] = substr($figure,$y,1);
			}
			$len = count($sub);
			if ($len==3) {
				if ($sub[0]!="0") {
					$temp .= ((strlen($str)>0)?" ":"") . trim($this->words[$sub[0]]) . " Hundred ";
				}
				$temp .= $this->processTen($sub[1], $sub[2]);
			} elseif ($len==2) {
				$temp .= $this->processTen($sub[0], $sub[1]);
			} else {
				$temp .= trim($this->words[$sub[0]]);
			}
			if (strlen($temp)>0) {
				$str .= $temp . $this->places[$i];
			}
		}
		$str .= " " . $currency;
		
		if ($this->decimal_len>0) {
			//$str .= " And " . $this->decimal . "/" . $this->denominator($this->decimal_len+1) .  " Cents";
			$decstr = $this->decimalcurrency($this->decimal,$currency);
			if($decstr!="") {
				$str .= " And " .$this->decimalcurrency($this->decimal,$currency)." ".$subunit;
			}
		}
		$this->amount_in_words = $str;
		return $str;
	}
	public function decimalcurrency($amount,$currency) {
		$this->assign($currency);
		
		$temp = (string)$amount;
		$pos = strpos($temp,".");
		if ($pos) {
			$temp = substr($temp,0,$pos);
			$this->decimal = strstr((string)$amount,".");
			$this->decimal_len = strlen($this->decimal) - 2;
			$this->decimal = substr($this->decimal,1,$this->decimal_len+1);
		}
		$len = strlen($temp)-1;
		$ctr = 0;
		$arr = array();
		while ($len >= 0) {
			if ($len >= 2) {
				$arr[$ctr++] = substr($temp, $len-2, 3);
				$len -= 3;
			} else {
				$arr[$ctr++] = substr($temp,0,$len+1);
				$len = -1;
			}
		}
		
		$str = "";
		for ($i=count($arr)-1; $i>=0; $i--) {
			$figure = $arr[$i];
			$sub = array(); $temp="";
			for ($y=0; $y<strlen(trim($figure)); $y++) {
				$sub[$y] = substr($figure,$y,1);
			}
			$len = count($sub);
			if ($len==3) {
				if ($sub[0]!="0") {
					$temp .= ((strlen($str)>0)?" ":"") . trim($this->words[$sub[0]]) . " Hundred";
				}
				$temp .= $this->processTen($sub[1], $sub[2]);
			} elseif ($len==2) {
				$temp .= $this->processTen($sub[0], $sub[1]);
			} else {
				$temp .= $words[$sub[0]];
			}
			if (strlen($temp)>0) {
				$str .= $temp . $this->places[$i];
			}
		}
		return $str;
	}
	public function denominator($x) {
		$temp = "1";
		for ($i=1; $i<=$x; $i++) {
			$temp .= "0";
		}
		return $temp;
	}
	
	public function display() {
		echo $this->amount_in_words;
	}

	public function processTen($sub1, $sub2) {
		if ($sub1=="0") {
			if ($sub2=="0") {
				return "";
			} else {
				return $this->words[$sub2];
			}
		} elseif ($sub1!="1") {
			if ($sub2!="0") {
				return $this->words[$sub1."0"] . $this->words[$sub2];
			} else {
				return $this->words[$sub1 . $sub2];
			}
		} else {
			if ($sub2=="0") {
				return $this->words["10"];
			} else {
				return $this->words[$sub1 . $sub2];
			}
		}
	}

	public function assign($currency) {
		$this->words["0"] = " Zero";
		$this->words["1"] = " One";
		$this->words["2"] = " Two";
		$this->words["3"] = " Three";
		$this->words["4"] = " Four";
		$this->words["5"] = " Five";
		$this->words["6"] = " Six";
		$this->words["7"] = " Seven";
		$this->words["8"] = " Eight";
		$this->words["9"] = " Nine";
		$this->words["10"] = " Ten";
		
		$this->words["11"] = " Eleven";
		$this->words["12"] = " Twelve";
		$this->words["13"] = " Thirten";
		$this->words["14"] = " Fourten";
		$this->words["15"] = " Fiften";
		$this->words["16"] = " Sixten";
		$this->words["17"] = " Seventen";
		$this->words["18"] = " Eighten";
		$this->words["19"] = " Nineten";
		$this->words["20"] = " Twenty";
		
		$this->words["30"] = " Thirty";
		$this->words["40"] = " Forty";
		$this->words["50"] = " Fifty";
		$this->words["60"] = " Sixty";
		$this->words["70"] = " Seventy";
		$this->words["80"] = " Eighty";
		$this->words["90"] = " Ninety";
		
		$this->places[0] = "";
		if($currency == 'Rupees') {
			$this->places[1] = " Thousand";
			$this->places[2] = " Lakh";
			$this->places[3] = " Crore";
		} else {
			$this->places[1] = " Thousand";
			$this->places[2] = " Million";
			$this->places[3] = " Billion";
			$this->places[4] = " Thrillion";
			$this->places[5] = " Quadrillion";
			$this->places[6] = " Quintillion";
			$this->places[7] = " Sextillion";
			$this->places[8] = " Septillion";
			$this->places[9] = " Octillion";
			$this->places[10] = " Nonillion";
		}
	}
	//currency formatter
	public function formatcurrency($floatcurr, $curr = "INR") {
        $currencies['ARS'] = array(2,',','.');          //  Argentine Peso
        $currencies['AMD'] = array(2,'.',',');          //  Armenian Dram
        $currencies['AWG'] = array(2,'.',',');          //  Aruban Guilder
        $currencies['AUD'] = array(2,'.',' ');          //  Australian Dollar
        $currencies['BSD'] = array(2,'.',',');          //  Bahamian Dollar
        $currencies['BHD'] = array(3,'.',',');          //  Bahraini Dinar
        $currencies['BDT'] = array(2,'.',',');          //  Bangladesh, Taka
        $currencies['BZD'] = array(2,'.',',');          //  Belize Dollar
        $currencies['BMD'] = array(2,'.',',');          //  Bermudian Dollar
        $currencies['BOB'] = array(2,'.',',');          //  Bolivia, Boliviano
        $currencies['BAM'] = array(2,'.',',');          //  Bosnia and Herzegovina, Convertible Marks
        $currencies['BWP'] = array(2,'.',',');          //  Botswana, Pula
        $currencies['BRL'] = array(2,',','.');          //  Brazilian Real
        $currencies['BND'] = array(2,'.',',');          //  Brunei Dollar
        $currencies['CAD'] = array(2,'.',',');          //  Canadian Dollar
        $currencies['KYD'] = array(2,'.',',');          //  Cayman Islands Dollar
        $currencies['CLP'] = array(0,'','.');           //  Chilean Peso
        $currencies['CNY'] = array(2,'.',',');          //  China Yuan Renminbi
        $currencies['COP'] = array(2,',','.');          //  Colombian Peso
        $currencies['CRC'] = array(2,',','.');          //  Costa Rican Colon
        $currencies['HRK'] = array(2,',','.');          //  Croatian Kuna
        $currencies['CUC'] = array(2,'.',',');          //  Cuban Convertible Peso
        $currencies['CUP'] = array(2,'.',',');          //  Cuban Peso
        $currencies['CYP'] = array(2,'.',',');          //  Cyprus Pound
        $currencies['CZK'] = array(2,'.',',');          //  Czech Koruna
        $currencies['DKK'] = array(2,',','.');          //  Danish Krone
        $currencies['DOP'] = array(2,'.',',');          //  Dominican Peso
        $currencies['XCD'] = array(2,'.',',');          //  East Caribbean Dollar
        $currencies['EGP'] = array(2,'.',',');          //  Egyptian Pound
        $currencies['SVC'] = array(2,'.',',');          //  El Salvador Colon
        $currencies['ATS'] = array(2,',','.');          //  Euro
        $currencies['BEF'] = array(2,',','.');          //  Euro
        $currencies['DEM'] = array(2,',','.');          //  Euro
        $currencies['EEK'] = array(2,',','.');          //  Euro
        $currencies['ESP'] = array(2,',','.');          //  Euro
        $currencies['EUR'] = array(2,',','.');          //  Euro
        $currencies['FIM'] = array(2,',','.');          //  Euro
        $currencies['FRF'] = array(2,',','.');          //  Euro
        $currencies['GRD'] = array(2,',','.');          //  Euro
        $currencies['IEP'] = array(2,',','.');          //  Euro
        $currencies['ITL'] = array(2,',','.');          //  Euro
        $currencies['LUF'] = array(2,',','.');          //  Euro
        $currencies['NLG'] = array(2,',','.');          //  Euro
        $currencies['PTE'] = array(2,',','.');          //  Euro
        $currencies['GHC'] = array(2,'.',',');          //  Ghana, Cedi
        $currencies['GIP'] = array(2,'.',',');          //  Gibraltar Pound
        $currencies['GTQ'] = array(2,'.',',');          //  Guatemala, Quetzal
        $currencies['HNL'] = array(2,'.',',');          //  Honduras, Lempira
        $currencies['HKD'] = array(2,'.',',');          //  Hong Kong Dollar
        $currencies['HUF'] = array(0,'','.');           //  Hungary, Forint
        $currencies['ISK'] = array(0,'','.');           //  Iceland Krona
        $currencies['INR'] = array(2,'.',',');          //  Indian Rupee
        $currencies['IDR'] = array(2,',','.');          //  Indonesia, Rupiah
        $currencies['IRR'] = array(2,'.',',');          //  Iranian Rial
        $currencies['JMD'] = array(2,'.',',');          //  Jamaican Dollar
        $currencies['JPY'] = array(0,'',',');           //  Japan, Yen
        $currencies['JOD'] = array(3,'.',',');          //  Jordanian Dinar
        $currencies['KES'] = array(2,'.',',');          //  Kenyan Shilling
        $currencies['KWD'] = array(3,'.',',');          //  Kuwaiti Dinar
        $currencies['LVL'] = array(2,'.',',');          //  Latvian Lats
        $currencies['LBP'] = array(0,'',' ');           //  Lebanese Pound
        $currencies['LTL'] = array(2,',',' ');          //  Lithuanian Litas
        $currencies['MKD'] = array(2,'.',',');          //  Macedonia, Denar
        $currencies['MYR'] = array(2,'.',',');          //  Malaysian Ringgit
        $currencies['MTL'] = array(2,'.',',');          //  Maltese Lira
        $currencies['MUR'] = array(0,'',',');           //  Mauritius Rupee
        $currencies['MXN'] = array(2,'.',',');          //  Mexican Peso
        $currencies['MZM'] = array(2,',','.');          //  Mozambique Metical
        $currencies['NPR'] = array(2,'.',',');          //  Nepalese Rupee
        $currencies['ANG'] = array(2,'.',',');          //  Netherlands Antillian Guilder
        $currencies['ILS'] = array(2,'.',',');          //  New Israeli Shekel
        $currencies['TRY'] = array(2,'.',',');          //  New Turkish Lira
        $currencies['NZD'] = array(2,'.',',');          //  New Zealand Dollar
        $currencies['NOK'] = array(2,',','.');          //  Norwegian Krone
        $currencies['PKR'] = array(2,'.',',');          //  Pakistan Rupee
        $currencies['PEN'] = array(2,'.',',');          //  Peru, Nuevo Sol
        $currencies['UYU'] = array(2,',','.');          //  Peso Uruguayo
        $currencies['PHP'] = array(2,'.',',');          //  Philippine Peso
        $currencies['PLN'] = array(2,'.',' ');          //  Poland, Zloty
        $currencies['GBP'] = array(2,'.',',');          //  Pound Sterling
        $currencies['OMR'] = array(3,'.',',');          //  Rial Omani
        $currencies['RON'] = array(2,',','.');          //  Romania, New Leu
        $currencies['ROL'] = array(2,',','.');          //  Romania, Old Leu
        $currencies['RUB'] = array(2,',','.');          //  Russian Ruble
        $currencies['SAR'] = array(2,'.',',');          //  Saudi Riyal
        $currencies['SGD'] = array(2,'.',',');          //  Singapore Dollar
        $currencies['SKK'] = array(2,',',' ');          //  Slovak Koruna
        $currencies['SIT'] = array(2,',','.');          //  Slovenia, Tolar
        $currencies['ZAR'] = array(2,'.',' ');          //  South Africa, Rand
        $currencies['KRW'] = array(0,'',',');           //  South Korea, Won
        $currencies['SZL'] = array(2,'.',', ');         //  Swaziland, Lilangeni
        $currencies['SEK'] = array(2,',','.');          //  Swedish Krona
        $currencies['CHF'] = array(2,'.','\'');         //  Swiss Franc 
        $currencies['TZS'] = array(2,'.',',');          //  Tanzanian Shilling
        $currencies['THB'] = array(2,'.',',');          //  Thailand, Baht
        $currencies['TOP'] = array(2,'.',',');          //  Tonga, Paanga
        $currencies['AED'] = array(2,'.',',');          //  UAE Dirham
        $currencies['UAH'] = array(2,',',' ');          //  Ukraine, Hryvnia
        $currencies['USD'] = array(2,'.',',');          //  US Dollar
        $currencies['VUV'] = array(0,'',',');           //  Vanuatu, Vatu
        $currencies['VEF'] = array(2,',','.');          //  Venezuela Bolivares Fuertes
        $currencies['VEB'] = array(2,',','.');          //  Venezuela, Bolivar
        $currencies['VND'] = array(0,'','.');           //  Viet Nam, Dong
        $currencies['ZWD'] = array(2,'.',' ');          //  Zimbabwe Dollar

       
        if ($curr == "INR"){    
            return $this->formatinr($floatcurr);
        } else {
            return number_format($floatcurr,$currencies[$curr][0],$currencies[$curr][1],$currencies[$curr][2]);
        }
    }
	//indian currency
	public function formatinr($input) {
		//CUSTOM FUNCTION TO GENERATE ##,##,###.##
		$dec = "";
		$pos = strpos($input,".");
		if ($pos === false){
			//no decimals   
		} else {
			//decimals
			$dec = substr(round(substr($input,$pos),2),1);
			$input = substr($input,0,$pos);
		}
		$num = substr($input,-3); //get the last 3 digits
		$input = substr($input,0, -3); //omit the last 3 digits already stored in $num
		while(strlen($input) > 0) //loop the process - further get digits 2 by 2
		{
			$num = substr($input,-2).",".$num;
			$input = substr($input,0,-2);
		}
		return $num . $dec;
	}
}
?>