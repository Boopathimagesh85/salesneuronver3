<?php
require_once 'leadlicence.php';
session_start();
$appfolder = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
$appfolder = explode('/',$appfolder);//explode(DIRECTORY_SEPARATOR,$appfolder);
$foldername = $_SERVER['DOCUMENT_ROOT'].'/'.$appfolder[1].'/snsessionlog/';
$cisess_cookie = $_COOKIE['sn_ci_session'];
$filepath = $foldername.'sn_ci_session'.$cisess_cookie;
$fp = fopen($filepath,'r+b')or die('file does not exit');
$data = fread($fp,filesize($filepath));
session_decode($data);
$data = $_SESSION;
$dataname = $data['UserDataname'];
$branchids = $data['BranchIds'];
$currentuserid = $data['logged_in']['empid'];
session_destroy();
global $CONF;
$masterdb = $CONF['sql_db'];
$branchid = $branchids['BranchId'];
//client db connection
$conn = mysqli_connect($CONF['sql_host'], $CONF['sql_user'], $CONF['sql_passwd']);
mysqli_select_db($conn,$dataname);
 //-----master db conncetion for add on credti fetch-----//
$con = mysqli_connect($CONF['sql_host'], $CONF['sql_user'], $CONF['sql_passwd']);
mysqli_select_db($con,$masterdb);
// master company id fetch
$mastercompany = mysqli_query($conn,"SELECT `mastercompanyid` FROM company LEFT OUTER JOIN branch ON branch.companyid = company.companyid WHERE branch.branchid = $branchid AND branch.status =1 AND company.status =1 LIMIT 0 , 1") or die('mastercompany fetch error');
$crowcount=$mastercompany->num_rows;
if($crowcount > 0) {
	while ($crow = mysqli_fetch_array($mastercompany)) {
		$mastercomapnyid =  $crow['mastercompanyid'];
	}
} else {
	$mastercomapnyid = 2;
}
$sql = "select addonavailablecredit from salesneuronaddonscredit where addonstypeid=1 and mastercompanyid=$mastercomapnyid and status=1";
$query = $conn->query($sql);
$rowcount=$query->num_rows;
if($rowcount > 0) {
	while ($row = $query->fetch_assoc()) {
		$credit =  $row['addonavailablecredit'];
	}
} else {
	$credit = 1;
}
$maxcomsizeinkb = $credit*1024*1024;
mysqli_close($con); 
//-----master db connection  close here-----//
 $i=0;
if (!($current = mysqli_query($conn,"select filename_size from crmfileinfo where status=1"))) {
	echo("Error description: " . mysqli_error($conn)); die;
}
while ($row = mysqli_fetch_array($current)) {
	$array[] = $row[0];
}
$sum = 0;
if(!empty($array)) {
	foreach($array as $ar){
		$mem = preg_replace("/[^0-9,.]/", "", $ar);
		$size = explode(',', $ar);
		$total = array_sum($size);
		$sum+= $total;
	}
}
$totalfilesize = $sum;	//Consumed Size //1048576
if($maxcomsizeinkb >= $totalfilesize) {
	//If directory doesnot exists create it.
	$output_dir = "uploads/";
	$maxsize    = 5120;
	$acceptable = array(
		'image/jpeg',
		'image/jpg',
		'image/png',
		'image/bmp',
		'image/gif',
		'image/psd',
		'image/tiff',
		'image/tiff',
		'image/jp2',
		'image/vnd.wap.wbmp',
		'image/xbm',
		'image/vnd.microsoft.icon'
	);
	$fileCount = 0;
	if(isset($_FILES["myfile"])) {
		$error =$_FILES["myfile"]["error"]; {
			//print_r($_FILES["myfile"]);
			if(!is_array($_FILES["myfile"]['name'])) {
				//$fileCount = count($_FILES["myfile"]['name']);//single file
				$fileName = $_FILES["myfile"]["name"];
				$fname = str_replace(',', '', $fileName);
				$pos = strripos($fname,'.');
				$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
				$type= $_FILES['myfile']['type'];
				$size= $_FILES['myfile']['size'];
				$fsize = $size / 1024;
				$fsize = round($fsize,2);
				if((in_array($_FILES['myfile']['type'], $acceptable)) && (!empty($_FILES["myfile"]["type"]))) {
					if(($fsize >= $maxsize) || ($fsize == 0)) {
						if($fsize == 0){
							$errors = 'LowSize';
						} else {
							echo '----';
							$errors = 'Size';
						}
						echo $errors;
					} else {
						move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $newfileName);
						echo json_encode(array('fname'=>$fname,'fsize'=>$fsize.' KB','ftype'=>$type,'path'=>$output_dir.$newfileName));
					}		
				} else {
					if(($fsize >= $maxsize) || ($fsize == 0)) {
						if($fsize == 0){
							$errors = 'LowSize';
						} else {
							$errors = 'Size';
						}
						echo $errors;
					} else {
						move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $newfileName);
						echo json_encode(array('fname'=>$fname,'fsize'=>$fsize.' KB','ftype'=>$type,'path'=>$output_dir.$newfileName));
					}
				}
	      	} else { //multiple file
				$fileCount = count($_FILES["myfile"]['name']);
				if($fileCount > 10) {
					for($i=0; $i < $fileCount; $i++) {
						$fileName = $_FILES["myfile"]["name"][$i];
						$fname = str_replace(',', '', $fileName);
						$type= $_FILES['myfile']['type'][$i];
						$size= $_FILES['myfile']['size'][$i]; 
						$fsize = ($size) / 1024;
						$fsize = round($fsize,2);
						$pos = strripos($fileName,'.');
						$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
						if((in_array($_FILES['myfile']['type'], $acceptable)) && (!empty($_FILES["myfile"]["type"]))) {
							if(($fsize >= $maxsize) || ($fsize == 0)) {
								if($fsize == 0){
									$errors = 'LowSize';
								} else {
									$errors = 'Size';
								}
								$errors = 'Size';
								echo $errors;
							} else {
								move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $newfileName);
								echo json_encode(array('fname'=>$fname,'fsize'=>$fsize.' KB','ftype'=>$type,'path'=>$output_dir.$newfileName));
							}
						} else {
							if(($fsize >= $maxsize) || ($fsize == 0)) {
								if($fsize == 0){
									$errors = 'LowSize';
								} else {
									$errors = 'Size';
								}
								$errors = 'Size';
								echo $errors;
							} else {
								move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $newfileName);
								echo json_encode(array('fname'=>$fname,'fsize'=>$fsize.' KB','ftype'=>$type,'path'=>$output_dir.$newfileName));
							}
						}
					}
				} else {
					echo 'MaxFile';
				}
			}
		}
	} else if(isset($_FILES["file"])) {
		$error = $_FILES["file"]["error"];
		$secure = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "");
		if($_SERVER['HTTP_HOST']=='localhost') {
			$url = "http".$secure."://".$_SERVER['HTTP_HOST'].'/';
			$url.= 'salesneuroncrm/';
		} else {
			$url = "http".$secure."://".$_SERVER['HTTP_HOST'].'/';
			$url.= 'crm/';
		}
		if(!is_array($_FILES["file"]['name'])) {
			$fileCount = count($_FILES["file"]['name']);//single file
			$fileName = $_FILES["file"]["name"];
			$fname = str_replace(',', '', $fileName);
			$pos = strripos($fname,'.');
			$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
			$type= $_FILES['file']['type'];
			$size= $_FILES['file']['size']; 
			$fsize = $size / 1024;
			$fsize = round($fsize,2);
			if((in_array($_FILES['file']['type'], $acceptable)) && (!empty($_FILES["file"]["type"]))) {
				if(($_FILES['file']['size'] >= $maxsize) || ($_FILES["file"]["size"] == 0)) {
					$errors = 'Size';
					echo $errors;
				} else {
					move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $newfileName);
					echo json_encode(array('link'=>$url.$output_dir.$newfileName));
				}
			} else {
				move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $newfileName);
				echo json_encode(array('link'=>$url.$output_dir.$newfileName));
			} 
      	} else { //multiple file
			$fileCount = count($_FILES["file"]['name']);
			if($fileCount > 10) {
				for($i=0; $i < $fileCount; $i++) {
					$fileName = $_FILES["file"]["name"][$i];
					$fname = str_replace(',', '', $fileName);
					$type= $_FILES['file']['type'][$i];
					$size= $_FILES['file']['size'][$i]; 
					$fsize = ($size) / 1024;
					$fsize = round($fsize,2);
					$pos = strripos($fileName,'.');
					$newfileName = substr_replace($fname, '_'.time(), $pos, 0);
					if((in_array($_FILES['file']['type'], $acceptable)) && (!empty($_FILES["file"]["type"]))) {
						if(($_FILES['file']['size'] >= $maxsize) || ($_FILES["file"]["size"] == 0)) {
							$errors = 'Size';
							echo $errors;
						} else {
							move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $newfileName);
							echo json_encode(array('link'=>$url.$output_dir.$newfileName));
						}
					} else {
						move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $newfileName);
						echo json_encode(array('link'=>$url.$output_dir.$newfileName));
					}
				}
			} else {
				echo 'MaxFile';
			}
		}
	}
} else {
	echo 'MaxSize';
}
?>