<html>
<head>  
	<title>Sales Neuron</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<?php
		$appfolder = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		$base_url = "http://" . $_SERVER['SERVER_NAME'] . $appfolder;
	?>
	<link rel="shortcut icon" href="<?php echo $base_url;?>img/favi.ico"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url;?>css/allinonecss.css"  media="screen" />
	<script src="<?php echo $base_url;?>js/plugins/allpluginheader.min.js"></script>
	<script src="<?php echo $base_url;?>js/plugins/allpluginbottom.min.js"></script>
	<script type="text/javascript">
		<?php
			/* $CI = &get_instance();
			$device = $CI->Basefunctions->deviceinfo(); */
			$device = "desktop";
			if($device == 'phone') {
				$devicename = 'phone';
			} else {
				$devicename = 'desktop';
			}
		?>
		var deviceinfo = '<?php echo $devicename; ?>';
   </script>	
   <script>
		var base_url = '<?php echo $base_url;?>';
		$(document).ready(function() {
			{// Foundation Initialization
				$(document).foundation();
			}
			$('body').fadeIn(1000);
			$('#username').focus();
			/* On enter installer trigger*/
			$('#loginpassword,#password').keypress(function(e) {
				if(e.which == 13) {
					$("#installersubmit").trigger('click');
				}
			});
			$('#securityanswer').keypress(function(e){	
				if(e.which == 13) {
					$("#secanswersubmit").trigger('click');
				}
			});
			$('#retypepassword').keypress(function(e){
				if(e.which == 13) {
					$("#changepasswordsubmit").trigger('click');
				}
			});
			/*Alert Close*/	
			$("#alertscloseup").click(function(){
				$("#alerts").fadeOut();
			});
			// Username checking
			$("#username").change(function() {
				var username = $('#username').val();
				if(username != '') {
					$("#processoverlay").show();
					$.ajax({
						type:"POST",
						url:base_url+"Installer/verifyusername",
						data:"username="+username,
						async:false,
						cache:false,
						success: function(data) {
							if(data == 'no') {
								alert('Email Id is not activated. Please contact salesneuron team!');
								$('#username').val('');
								$('#username').focus();
							}
							$("#processoverlay").hide();
						},
					});
				}
			});
			{/* Installer button */
				$('#installersubmit').prop("disabled",false);
				$("#installersubmit").click(function() {
					$("#installerindexvalidationspan").validationEngine('validate');
				});
				$("#installerindexvalidationspan").validationEngine({
					onSuccess: function() {
						$("#processoverlay").show();
						$('#installersubmit').prop("disabled", false);
						var installer = $("#installerform").serialize();
						var amp = '&';
						var installeruser = amp + installer;
						var domain = base_url;
						$.ajax({
							type:"POST",
							url:base_url+"Installer/verifyinstalleruserdata",
							data:"installerdata="+ installeruser,
							async:false,
							cache:false,
							success: function(data) {
								$("#processoverlay").hide();
								if(data == 'Success') {
								} else if(data == 'Failed') {
									alert('Entered License Key is not matched. Please contact salesneuron team!');
									$('#licensekey').val('').focus();
									return false;
								}
							},
						});
					},
					onFailure: function() {
						alertpopup('Enter Correct Emailid / License Key');
						$("#processoverlay").hide();
					}
				});
			}
			{//  On failed Mail processing submit button backtoforgetpassword -GRA
				$("#backtoforgetpassword").click(function(){
					$('#forgetusername').val('');
					$("#headingchange").text('Forgot Password');
					$(".mailprocessdiv").hide();
					$(".forgetpasswordformdiv").fadeIn();
				});
			}
			{// After mail sent success move to login page submit button
				$("#submitlogin").click(function(){
					$('#username').val('');
					$('#password').val('');
					$("#headingchange").text('Login');
					$(".forgotpasswordsendsuccessdiv").hide();
					$(".installerformdiv").fadeIn();
				});
			}
			{// on Click Logo Come to log In screen
				$("#homescreenlogo").click(function() {
					$(".forgetpasswordformdiv,.securityqaformdiv,.changepasswordformdiv").hide();
					$(".installerformdiv").fadeIn();
				});
			}
			{/* Signup link  */
				$("#signuplink").click(function() {
					$("#headingchange").text('Sign Up');
					$(".installerformdiv").hide();
					$(".signupformdiv").fadeIn();
					$("#signupname").focus();
				});
			}
			
			{//product reactivation activation
				$('#prodactivationclose').click(function(){
					$('#reactivationoverlay').hide();
				});
				//product reactivation
				$('#productrenewalbtn').click(function(){
					$("#productreactivatevalidate").validationEngine('validate');
				});
				$("#productreactivatevalidate").validationEngine({
					onSuccess: function() {
						$('#productrenewalbtn').prop("disabled", true);
						var uname = $('#username').val();
						var activatedata=$("#productreactivateform").serialize();
						var amp='&';
						var datas=amp+activatedata+amp+'username='+uname;
						$.ajax({
							type:"POST",
							url:base_url+"Installation/productreactivation",
							data:"datasets="+datas,
							dataType:'json',
							success: function(data) {
								if(data.status=='TRUE') {
									$('#reactivationoverlay').hide();
									$('#installersubmit').trigger('click');
									$('#productrenewalbtn').prop("disabled", false);
								} else {
									$('#productrenewalbtn').prop("disabled", false);
									alertpopup(data.status);
								}
							},
						});
					},
					onFailure: function() {
						alertpopup('Please enter mandatory field values.');
					}
				});
			}
		});
		function alertpopup(txtmsg) {
			$(".alertinputstyle").val(txtmsg);
			$('#alerts').fadeIn('fast');
		}
		//access mailer
		function emaillogin(mail,p,domain) {
			var file='erpmail/rclogin.php';
			$('body').append('<form id="emaillogin" style="display:none;" name="emaillogin" method="post" action="'+base_url+''+file+'"><input type="hidden" name="mdomain" id="mdomain"/><input type="hidden" name="email" id="email"/><input type="hidden" name="pp" id="pp" value=""/><input type="hidden" name="action" id="action" value=""/><input type="submit"/></form>');
			$('#action').val('login');
			$('#pp').val(p);
			$('#email').val(mail);
			$('#mdomain').val(domain);
			$('#emaillogin').submit();
		}
	</script>
</head>
<?php 
/* $CI = &get_instance();
$device = $CI->Basefunctions->deviceinfo(); */
$device = "desktop";
?>
<body style='background:#f2f3fa !important'>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row" style="max-width:93.5%;top: 20% !important;position: relative;left: 0% !important;">
		<div class="large-12 columns">
		<?php 	if($device=='phone') { ?>
			<div class="large-4 medium-8 small-8 small-offset-1 columns" style="position: relative;left: 13% !important;">
				<img src="<?php echo $base_url;?>/img/homepagelogo.png" id="homescreenlogo" style="cursor:pointer"/>
			</div>
		<?php } else  { ?>
			<div class="large-4 medium-4 small-4 columns large-centered" style=" position: relative;left: 7.5% !important;">
				<img src="<?php echo $base_url;?>/img/homepagelogo.png" id="homescreenlogo" style="cursor:pointer"/>
			</div>
		<?php } ?>
		</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">&nbsp;</div>
		<div class="large-12 columns">
			<div class="large-4 small-12 columns large-centered ">
				<div class="large-12 columns paddingzero borderstyle" style="background:#fff;width: 100% !important;">
					<div class="large-12 columns headerformcaptionstyle" id="headingchange" style="text-align:center;font-size:1.2rem;top:10px;position:relative;">Installer</div>
					<!-- login form -->
					<form id="installerform" action="" class="installerformdiv">
						<span class="validationEngineContainer" id="installerindexvalidationspan">
							<div class="input-field large-12 columns">
								<input type="text" id="username" name="username" value="" class="validate[required,custom[email],maxSize[50]]" data-prompt-position="bottomLeft">
								<label for="username">Email Address<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="input-field large-12 columns">
								<input type="text" id="licensekey" name="licensekey" value="" class="validate[required,maxSize[100]]" data-prompt-position="bottomLeft">
								<label for="licensekey">License Key<span class="mandatoryfildclass">*</span></label>
							</div>
							<div class="large-12 columns">&nbsp;</div>
							<div class="large-12 columns centertext">							
								<input type="button" class="btn alertbtnyes" value="Activation" id="installersubmit" name="installersubmit" />
							</div>
						</span>
					</form>
					<div class="large-12 columns mailprocessdiv centertext hidedisplay">
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">
							<span id="processspinner" class="fa  fa-spinner fa-spin" title="processing"> </span>
							<span id="processing" class="" style="font-family: Segoe UI; font-size: 0.9rem; color:gray;" title="Processing"> Processing...</span>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns backtoforgetpassword hidedisplay">							
							<input type="button" class="btn homepageloginbtn" value="back" id="backtoforgetpassword"/>
						</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
						<div class="large-12 columns">&nbsp;</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="large-12 columns" style="position:absolute;">
			<div class="overlay" id="successalerts">	
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>		
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="row">&nbsp;</div>
				<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns overlayborder">
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
							<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i> </span> Alert</div>
						</div>
						<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
						<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
							<span class="alertinputstyle">Company Create Successfully !!! Redirecting To Home Page..</span>
							<div class="large-12 column">&nbsp;</div>
						</div>
					</div>			
					<div class="row">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="failurealerts">	
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-3 medium-6 small-10 large-centered medium-centered small-centered columns overlayborder">
				<div class="row">&nbsp;</div>
				<div class="row">
				<div class="large-12 columns alertsheadercaptionstyle" style="text-align:left">
					<div class="small-12 columns"><span style="color:#ffffff;padding-right:0.5rem"><i class="material-icons">warning</i></span> Alert</div>
				</div>
				<div class="large-12 columns" style="background:#f5f5f5">&nbsp;</div>
					<div class="large-12 large-centered columns" style="background:#f5f5f5;text-align:center">
						<span class="alertinputstyle">Sorry Company Not Created !!!</span>
						<div class="large-12 column">&nbsp;</div>
					</div>
				</div>			
				<div class="row">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- Template Print Overlay  Overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="reactivationoverlay" style="overflow-y: scroll;overflow-x: hidden">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>		
			<div class="large-4 medium-6 large-centered medium-centered columns" >
				<form method="POST" name="productreactivateform" style="" id="productreactivateform" action="" enctype="" class="overlayborder">
					<span id="productreactivatevalidate" class="validationEngineContainer"> 
						<div class="row">&nbsp;</div>
						<div class="alert-panel">
							<div class="alertmessagearea">
								<div class="alert-title">Product Renewal</div>
								<div class="alert-message">
									<div class="input-field overlayfield ">  
										<span class="firsttab" tabindex="1000"></span>
								        <input type="text" id="userprodrenewalkey" name="userprodrenewalkey" class="validate[required] ffield" value="" tabindex="1001"/>
										<label for="userprodrenewalkey">Product Renewal Key<span class="mandatoryfildclass">*</span></label>
									</div>
									<div style="padding-top:10px;"><span class="mandatoryfildclass">*Your Product is expired.Please enter product renewal key</span></div>
								</div>
							</div>
							<div class="alertbuttonarea">
								<input type="button" class="alertbtn" value="Activate" name="productrenewalbtn" id="productrenewalbtn" tabindex="1002">
								<input type="button" class="alertbtn flloop" value="Cancel" name="cancel" id="prodactivationclose" tabindex="1003">
								<span class="lasttab" tabindex="1004"></span>
							</div>
						</div>
						<div class="row">&nbsp;</div>
					</span>
				</form>
			</div>
		</div>
	</div>
	<!--- process overlay-->
	<div class="large-12 columns" style="position:absolute;">
		<div class="overlay" id="processoverlay" style="overflow-y: hidden;overflow-x: hidden;z-index:100;">		
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div><div class="row">&nbsp;</div>
			<div class="row">&nbsp;</div>
			<div class="large-2 columns end large-centered">
				<div class="large-12 columns card-panel" style="text-align: left; background: #fff;padding-bottom: 0px; ">
					<div class="small-12 large-12 medium-12 columns paddingzero">
						<span><div class="spinner"></div></span><span style="display:inline-block;height:35px;vertical-align:middle;text-align: center;width: 100%;">&nbsp;&nbsp;&nbsp;  Processing...</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>   
</html>